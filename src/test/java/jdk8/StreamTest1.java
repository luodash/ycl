package jdk8;

import java.util.ArrayList;
import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;

import com.google.common.collect.Lists;

import jdk8.bean.Test_User;

public class StreamTest1 {

	@Test
	public void t() {
		
		Test_User Test_User = new Test_User();
		Test_User.setAge(20);
		Test_User.setRealName("ddd");
		
		Test_User Test_User1 = new Test_User();
		Test_User1.setAge(2);
		Test_User1.setRealName("ccc");
		
		Test_User Test_User2 = new Test_User();
		Test_User2.setAge(20);
		Test_User2.setRealName("aaa");
		
		ArrayList<Test_User> Test_Users = Lists.newArrayList(Test_User,Test_User1,Test_User2);
		
		Map<Object, List<Test_User>> o = Test_Users.stream().collect(Collectors.groupingBy(u -> u.getAge()));
		System.out.println(o);
		
		
		Supplier<Stream<String>> ss = () -> Stream.of("9","10","5","3","6","1");
		
		Stream<String> s1 = ss.get();
		
		Double dd = s1.collect(Collectors.averagingDouble(s -> Integer.valueOf(s)));
		System.out.println(dd);
		
		s1 = ss.get();
		DoubleSummaryStatistics oo = s1.collect(Collectors.summarizingDouble(s -> Integer.valueOf(s)));
		System.out.println(oo);
		
		
		s1 = ss.get();
		String pp = s1.collect(Collectors.joining(","));
		System.out.println(pp);
		
		
		
	}
}
