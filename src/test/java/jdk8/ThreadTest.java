package jdk8;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.concurrent.locks.StampedLock;
import java.util.stream.IntStream;

import jdk8.bean.Test_User;


public class ThreadTest {

	public static Callable<String> callable(String result, long sleepSeconds) {
		return () -> {
			TimeUnit.SECONDS.sleep(sleepSeconds);
			return result;
		};
	}

	ReentrantLock lock = new ReentrantLock();
	int count = 0;

	void increment() {
		lock.lock();
		try {
			count++;

		} finally {
			lock.unlock();
		}
	}
	
	public void t7() {
		ExecutorService executor = Executors.newFixedThreadPool(10);

		Semaphore semaphore = new Semaphore(5);

		Runnable longRunningTask = () -> {
		    boolean permit = false;
		    try {
		        permit = semaphore.tryAcquire(1, TimeUnit.SECONDS);
		        if (permit) {
		            System.out.println("Semaphore acquired");
		            sleep(1);
		        } else {
		            System.out.println("Could not acquire semaphore");
		        }
		    } catch (InterruptedException e) {
		        throw new IllegalStateException(e);
		    } finally {
		        if (permit) {
		            semaphore.release();
		        }
		    }
		};

		IntStream.range(0, 10)
		    .forEach(i -> executor.submit(longRunningTask));

//		stop(executor);
		
		
		
	}
	
	

	public void t6() {

		ExecutorService executor = Executors.newFixedThreadPool(2);
		StampedLock lock = new StampedLock();

		executor.submit(() -> {
			long stamp = lock.tryOptimisticRead();
			try {
				System.out.println("Optimistic Lock Valid: " + lock.validate(stamp));
				TimeUnit.SECONDS.sleep(1);
				System.out.println("Optimistic Lock Valid: " + lock.validate(stamp));
				TimeUnit.SECONDS.sleep(3);
				System.out.println("Optimistic Lock Valid: " + lock.validate(stamp));
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				lock.unlock(stamp);
			}
		});

		executor.submit(() -> {
			long stamp = lock.writeLock();
			try {
				System.out.println("Write Lock acquired");
				TimeUnit.SECONDS.sleep(2);
			}catch (Exception e) {
				e.printStackTrace();
			} finally {
				lock.unlock(stamp);
				System.out.println("Write done");
			}
		});

		stop(executor);

	}

	public void t4() {
		ExecutorService executor = Executors.newFixedThreadPool(2);

		IntStream.range(1, 1000).forEach(i -> executor.submit(this::increment));

		stop(executor);

		System.out.println(count);

	}

	/**
	 * readlock
	 */
	public void t5() {
		ExecutorService executor = Executors.newFixedThreadPool(2);
		Map<String, String> map = new HashMap<>();
		ReadWriteLock lock = new ReentrantReadWriteLock();

		executor.submit(() -> {
			lock.writeLock().lock();
			try {
				TimeUnit.SECONDS.sleep(1);
				map.put("foo", "bar");
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				lock.writeLock().unlock();
			}
		});

		Runnable readTask = () -> {
			lock.readLock().lock();
			try {
				System.out.println(map.get("foo"));
				TimeUnit.SECONDS.sleep(1);
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				lock.readLock().unlock();
			}
		};

		executor.submit(readTask);
		executor.submit(readTask);

		stop(executor);

	}

	public static void t3() {
		try {

			ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);

			Runnable task = () -> System.out.println("This is a task");

			ScheduledFuture<?> future = executor.schedule(task, 3, TimeUnit.SECONDS);

			TimeUnit.MILLISECONDS.sleep(1000);

			long remainingDelay = future.getDelay(TimeUnit.MILLISECONDS);

			System.out.printf("Remaining Delay: %sms", remainingDelay);
			System.out.println();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void t2() {

		try {

			ExecutorService executor = Executors.newWorkStealingPool();

			List<Callable<String>> callables = Arrays.asList(callable("task1", 2), callable("task2", 1),
					callable("task3", 3));

			String result = executor.invokeAny(callables);
			System.out.println(result);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void t1() {

		try {

			Callable<Integer> task = () -> {
				try {
					TimeUnit.SECONDS.sleep(10);
					return 123;
				} catch (InterruptedException e) {
					throw new IllegalStateException("task interrupted", e);
				}
			};

			ExecutorService executor = Executors.newFixedThreadPool(1);
			Future<Integer> future = executor.submit(task);

			Integer result = null;
			while (!future.isDone()) {
				result = future.get();
			}

			System.out.print("result: " + result);

			executor.shutdown();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void t() {
		try {

			ExecutorService executor = Executors.newWorkStealingPool();

			List<Callable<Test_User>> callables = Arrays.asList(() -> {
				Test_User u = new Test_User();
				u.setId(100l);
				;
				return u;
			}, () -> {
				Test_User u = new Test_User();
				u.setId(2l);
				return u;
			});

			Long value = executor.invokeAll(callables).stream().map(f -> {
				try {
					System.out.println(Thread.currentThread().getName());
					return f.get().getId();
				} catch (Exception e) {
					e.printStackTrace();
				}
				return 0l;
			}).reduce(0l, (sum, p) -> sum = sum + p, (s1, s2) -> (s1 + s2));

			System.out.println(value);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	void stop(ExecutorService executor) {
		try {
			executor.shutdown();
			executor.awaitTermination(1, TimeUnit.MINUTES);
		} catch (InterruptedException e) {
			System.err.println("tasks interrupted");
		} finally {
			if (!executor.isTerminated()) {
				System.err.println("cancel non-finished tasks");
			}
			executor.shutdownNow();
		}
	}
	
	void sleep(int sp) {
		try {
			TimeUnit.SECONDS.sleep(sp);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void main(String dd[]) {
		ThreadTest test = new ThreadTest();
		test.t7();
	}
}
