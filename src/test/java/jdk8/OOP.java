package jdk8;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;

public class OOP {

	@Test
	public void t1() throws IOException {
		String str = String.join(":", "foobar", "foo", "bar");
		System.out.println(str);

		str = str.chars().distinct().mapToObj(c -> String.valueOf((char) c)).sorted().collect(Collectors.joining());
		System.out.println(str);

		///////////////////////////////////////////////////////////////////////

		try (Stream<Path> stream = Files.list(Paths.get(""))) {
			String joined = stream.map(String::valueOf).filter(path -> !path.startsWith(".")).sorted()
					.collect(Collectors.joining("; "));
			System.out.println("List: " + joined);
		}

		Path start = Paths.get("");
		int maxDepth = 5;

		Stream<Path> stream = Files.find(start, maxDepth, (path, attr) -> String.valueOf(path).endsWith(".js"));

		String joined = stream.sorted().map(String::valueOf).collect(Collectors.joining("; "));
		System.out.println("Found: " + joined);

		stream.close();
	}

	@Test
	public void t2() throws IOException {
		Path start = Paths.get("");
		int maxDepth = 5;

		Stream<Path> stream = Files.walk(start, maxDepth);

		String joined = stream.map(String::valueOf).filter(path -> path.endsWith(".js")).sorted()
				.collect(Collectors.joining("; "));
		System.out.println("walk(): " + joined);

		stream.close();
	}
}
