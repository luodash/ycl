package jdk8;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.IntBinaryOperator;
import java.util.stream.IntStream;

import org.junit.Test;

public class AtomicTest {

	void stop(ExecutorService executor) {
		try {
			executor.shutdown();
			executor.awaitTermination(1, TimeUnit.MINUTES);
		} catch (InterruptedException e) {
			System.err.println("tasks interrupted");
		} finally {
			if (!executor.isTerminated()) {
				System.err.println("cancel non-finished tasks");
			}
			executor.shutdownNow();
		}
	}
	
	@Test
	public void t1() {
		AtomicInteger atomicInt = new AtomicInteger(0);

//		ExecutorService executor = Executors.newFixedThreadPool(2);
//
//		IntStream.range(0, 1000)
//		    .forEach(i -> executor.submit(atomicInt::incrementAndGet));
//
//		stop(executor);
//
//		System.out.println(atomicInt.get());    // => 1000
		
		atomicInt.accumulateAndGet(0, (m,b)-> m+b);
		atomicInt.accumulateAndGet(4, (m,b)-> m+b);
		atomicInt.accumulateAndGet(0, (m,b)-> m+b);
		atomicInt.accumulateAndGet(1, (m,b)-> m+b);
		;
		System.out.println(atomicInt.get());
	}
	
	IntBinaryOperator io = (m, b) -> {
		System.out.println("m :" + m + "b :" + b);
		return m + b;

	};
	
	@Test
	public void t2() {
		AtomicInteger atomicInt = new AtomicInteger(0);

		ExecutorService executor = Executors.newFixedThreadPool(2);

		IntStream.range(0, 1000)
		    .forEach(i -> {
		        Runnable task = () ->
		            atomicInt.accumulateAndGet(i, (n, m) -> n + m);
		        executor.submit(task);
		    });

		stop(executor);

		System.out.println(atomicInt.get());
		
	}
	
	@Test
	public void t3() {
		
		
		
	}
}
