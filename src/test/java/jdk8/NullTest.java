package jdk8;

import java.util.Optional;

import org.junit.Test;

public class NullTest {

	class Outer {
		Nested nested;

		Nested getNested() {
			return nested;
		}
	}

	class Nested {
		Inner inner;

		Inner getInner() {
			return inner;
		}
	}

	class Inner {
		String foo;

		String getFoo() {
			return foo;
		}
	}

	@Test
	public void t1() {
		Outer outer = new Outer();

		String value = 
				Optional.of(outer).map(Outer::getNested).map(Nested::getInner).map(Inner::getFoo).orElse("not found");
		
		System.out.println(value);
		
		
	}
}
