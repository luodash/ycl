package jdk8;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.Test;

import jdk8.bean.Test_Role;

public class StreamTest {

	@Test
	public void t() {

		try {
			

			List<String> myList = Arrays.asList("a8", "a2", "b1", "c2", "c1","IIIII","a9999");

			List bigList = myList.stream().filter(s -> s.startsWith("c"))
					// .map(String::toUpperCase)
					.map(s -> s.toUpperCase()).sorted().collect(Collectors.toList());

			System.out.println(bigList);
			
			
			

			boolean a = myList.stream().anyMatch((s1) -> s1.startsWith("a"));

			Stream.of("a1", "a2", "a3").findFirst().ifPresent(System.out::println); // a1

			Optional<String> op = myList.stream().reduce((s1, s2) -> s1 + "&&" + s2);

			System.out.println(op.get());
			
			List<Integer> intList = IntStream.range(1, 5).boxed().collect(Collectors.toList());
			
			System.out.println("intList" + intList);

			
			Supplier<Stream<String>> streamSupplier = () -> myList.stream()
					.filter(s -> s.startsWith("a"));
			
			
			List list = null; 
			
			streamSupplier.get().filter(s -> s.length() > 4).map(s -> s.chars()).
			forEach(s -> {List list_1 = s.boxed().collect(Collectors.toList());System.out.println(list_1);;});;
			
			//.ifPresent( s -> s.boxed().collect(Collectors.toList()));;

			System.out.println(list);
			
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@Test
	public void t1() {
		Set<Test_Role> sr = new HashSet<Test_Role>();
		Test_Role r = null;
		r = new Test_Role();r.setId(100L);
		
		Test_Role r1 = null;
		r1 = new Test_Role();r1.setId(200L);
		
		sr.add(r1);sr.add(r);
		
		List<Test_Role> list = sr.stream().sorted((o1,o2) -> o1.getId().compareTo(o2.getId())).collect(Collectors.toList());
		
		System.out.println(list);
		
		
		
		
		
//		sr.stream().map(menu -> menu.getId());//.map(id -> id.toString()).collect(Collectors.joining(","));
		
	}
}
