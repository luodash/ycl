package jdk8;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ForkJoinPool;

import org.junit.Before;
import org.junit.Test;

public class ConcurrentMapTest {

	ConcurrentMap<String, String> map = new ConcurrentHashMap<>();

	ConcurrentHashMap<String, String> map_parallel = new ConcurrentHashMap<>();

	@Before
	public void tb() {
		map.put("foo", "bar");
		map.put("han", "solo");
		map.put("r2", "d2");
		map.put("c3", "p0");

		map_parallel.put("foo", "bar");
		map_parallel.put("han", "solo");
		map_parallel.put("r2", "d2");
		map_parallel.put("c3", "p0");

		System.out.println(map);
	}

	public void t1() {
		String value = map.putIfAbsent("c5", "p1");
		System.out.println(value);
		System.out.println(map.get("c5"));
	}

	public void t2() {

		map.replaceAll((key, value) -> "r2".equals(key) ? "d3" : value);
		System.out.println(map);

	}

	public void t3() {

		map.compute("foo", (key, value) -> value + value);
		System.out.println(map);

	}

	public void t4() {

		map.merge("foo", "boo", (oldVal, newVal) -> newVal);
		System.out.println(map);
	}

	public void t5() {
		
//		int count = ForkJoinPool.getCommonPoolParallelism();

		map_parallel.forEach(1, (key, value) -> System.out.printf("key: %s; value: %s; thread: %s\n", key, value,
				Thread.currentThread().getName()));
	}
	
	
	
	public void t6() {
		
		int count = ForkJoinPool.getCommonPoolParallelism();

		String result = map_parallel.searchValues(1, value -> {
		    System.out.println(Thread.currentThread().getName());
		    if (value.length() > 3) {
		        return value;
		    }
		    return null;
		});

		System.out.println("Result: " + result);
	}
	
	@Test
	public void t7() {
		
		String result = map_parallel.reduce(1,
			    (key, value) -> {
//			        System.out.println("Transform: " + Thread.currentThread().getName());
			        return key + "=" + value;
			    },
			    (s1, s2) -> {
//			        System.out.println("Reduce: " + Thread.currentThread().getName());
			        return s1 + ", " + s2;
			    });

			System.out.println("Result: " + result);

	}
}
