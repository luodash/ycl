package jdk8;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapTest {

	public static void main(String[] args) {

		List<String> myList = Arrays.asList("a8", "a2", "b1", "c2", "c1");

		Map<Integer, String> map = new HashMap<>();

		for (int i = 0; i < 10; i++)
			map.putIfAbsent(i, "val" + i);

		System.out.println("myList : " + myList);
		System.out.println("map : " + map);

//		map.forEach((id, val) -> System.out.println(val));
		
		
		map.computeIfPresent(3, (num, val) -> val + num);
		map.get(3);             // val33

		System.out.println("map.containsKey(9):" + map.containsKey(9));
		map.computeIfPresent(9, (num, val) -> null);
		System.out.println("map.containsKey(9):" + map.containsKey(9));     // false

		map.computeIfAbsent(23, num -> "val" + num);

		System.out.println("map.get(23) :" + map.get(23));
		
		System.out.println("map.get(3) :" + map.get(3));
		map.computeIfAbsent(3, num -> "bam");
//		map.get(3);             // val33
		System.out.println("map.get(3) :" + map.get(3));
		
		System.out.println("map.get(9) :" + map.get(9));
		map.merge(9, "val9", (value, newValue) -> value.concat(newValue));
		System.out.println("map.get(9) :" + map.get(9));
		map.merge(9, "concat", (value, newValue) -> value.concat(newValue));
		System.out.println("map.get(9) :" + map.get(9));
		
		
		
	}

}
