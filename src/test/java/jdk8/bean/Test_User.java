package jdk8.bean;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Date;
import java.util.Optional;

@Getter
@Setter
public class Test_User implements Comparable<Test_User>  {

	private long id;
	
	private Optional<String> name;
	
	private String realName;

	private Date date;
	
	private Test_Role role;
	
	private int age;

	public Test_User() {
		
	}
	
	public Test_User(Integer i) {
		this.id = i;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}


	@Override
	public int compareTo(Test_User o) {
		return 0;
	}



	

}
