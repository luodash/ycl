package jdk8.bean;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class Test_Role {
	
	private Long id;

	private String roleName;

	
}
