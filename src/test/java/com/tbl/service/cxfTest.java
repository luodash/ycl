package com.tbl.service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.google.common.collect.Maps;
import com.tbl.common.utils.*;
import com.tbl.modules.agv.util.Rbsc;
import com.tbl.modules.wms.constant.Constant;
import com.tbl.modules.wms.constant.EmumConstant;
import com.tbl.modules.wms.constant.JaxbUtil;
import com.tbl.modules.wms.constant.XmlToolsUtil;
import com.tbl.modules.wms.dao.baseinfo.CarDAO;
import com.tbl.modules.wms.dao.baseinfo.DishDAO;
import com.tbl.modules.wms.dao.baseinfo.FactoryAreaDAO;
import com.tbl.modules.wms.dao.baseinfo.ShelfDAO;
import com.tbl.modules.wms.dao.interfacelog.InterfaceDoDAO;
import com.tbl.modules.wms.dao.inventory.InventoryRegistrationDAO;
import com.tbl.modules.wms.dao.pda.PdaDAO;
import com.tbl.modules.wms.dao.storageinfo.StorageInfoDAO;
import com.tbl.modules.wms.entity.baseinfo.Car;
import com.tbl.modules.wms.entity.baseinfo.Dish;
import com.tbl.modules.wms.entity.baseinfo.FactoryArea;
import com.tbl.modules.wms.entity.baseinfo.Shelf;
import com.tbl.modules.wms.entity.interfacelog.InterfaceDo;
import com.tbl.modules.wms.entity.inventory.InventoryRegistration;
import com.tbl.modules.wms.entity.responsexml.location.LocationWsinterface;
import com.tbl.modules.wms.entity.responsexml.location.LocationXml;
import com.tbl.modules.wms.entity.storageinfo.StorageInfo;
import com.tbl.modules.wms.service.Impl.baseinfo.ShelfServiceImpl;
import com.tbl.modules.wms.service.baseinfo.CarService;
import com.tbl.modules.wms.service.instorage.InstorageDetailService;
import com.tbl.modules.wms.service.instorage.InstorageService;
import com.tbl.modules.wms.service.outstorage.AllOutStorageService;
import com.tbl.modules.wms.service.outstorage.ExchangePrintDetailService;
import com.tbl.modules.wms.service.outstorage.OutStorageDetailService;
import com.tbl.modules.wms.service.pda.PdaService;
import com.tbl.modules.wms.service.webserviceutil.MapToXml;
import com.tbl.modules.wms.service.webservice.client.WmsService;
import com.tbl.modules.wms.service.webservice.server.FewmService;
import com.tbl.modules.wms.util.qrcode.QRCodeUtil;
import net.sf.json.JSONObject;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import javax.xml.bind.JAXBException;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;


/**
 * 测试类
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class cxfTest {
    @Autowired
    CarDAO carDAO;
    @Autowired
    CarService carService;
    @Autowired
    ExchangePrintDetailService exchangePrintDetailService;
    @Autowired
    PdaService pdaService;
    @Autowired
    AllOutStorageService allOutStorageService;
    @Autowired
    StorageInfoDAO storageInfoDAO;
    @Autowired
    WmsService wmsService;
    @Autowired
    FewmService fewmService;
    @Autowired
    ShelfDAO shelfDAO;
    @Autowired
    InstorageDetailService instorageDetailService;
    @Autowired
    ShelfServiceImpl simpl;
    @Autowired
    InventoryRegistrationDAO inventoryRegistrationDAO;
    @Autowired
    DishDAO dishDAO;
    @Autowired
    FactoryAreaDAO factoryAreaDAO;
    @Autowired
    InterfaceDoDAO InterfaceDoDAO;
    @Autowired
    OutStorageDetailService outStorageDetailService;
    @Autowired
    PdaDAO pdaDAO;
    @Autowired
    RedisUtils redisUtils;
    @Autowired
    InstorageService instorageService;
    @Autowired
    JdbcTemplate jdbcTemplate;
    /**
     * url
     */
    @Value("${spring.datasource.druid.first.url}")
    public static String URL;

    @Test
    public void t1(){
        Car car = carService.selectOne(new EntityWrapper<Car>().eq("CODE", "HC271"));

        Float xsize = 9181.7F;
        Float ysize = 3252.1F;

        //获取坐落在库位区间内
        List<Shelf> shelfList = pdaDAO.getUwbShelf(xsize, ysize, Arrays.asList(car.getWarehouseArea().split(",")));
        //标签的点不在库位内部
        if(shelfList==null||shelfList.size()==0) {
            shelfList = shelfDAO.findAllShelfByArea(Arrays.asList(car.getWarehouseArea().split(",")));
            if(shelfList!=null&&shelfList.size()>0){
                shelfList.sort((o1, o2) -> (int)(Math.sqrt(Math.pow((xsize - o1.getMidxsize()), 2) + Math.pow((ysize - o1.getMidysize()), 2))-
                Math.sqrt(Math.pow((xsize - o2.getMidxsize()), 2) + Math.pow((ysize - o2.getMidysize()), 2))));
            }
        }
        Shelf shelf = shelfList.get(0);


    }

    @Test
    public void t1_1(){
        System.out.println(UUID.randomUUID().toString().replace("-","").toUpperCase());
    }

    @Test
    public void t1_2(){
        System.out.println(redisUtils.get("aa1"));
    }

    @Test
    public void t1_3(){
        StorageInfo storageInfo = new StorageInfo();
        storageInfo.setMeter("123");
        storageInfoDAO.insert(storageInfo);
    }

    @Test
    public void t1_4(){
        List<StorageInfo> ds = storageInfoDAO.selectByMap(new HashMap<String, Object>(){{
            put("id", 12312412424214214L);

        }});
        System.out.println(ds);
    }

    @Test
    public void t1_5(){
        Map<String,Object> mapTotal = Maps.newHashMap();
        mapTotal.put("msg","成功");
        mapTotal.put("count",1);
        mapTotal.put("code",0);
        Map<String,Object> data2 = Maps.newHashMap();
        List<Map<String, Object>> data = new ArrayList<>();
        data2.put("asn",1);
        List<Map<String,Object>> sonlist = new ArrayList<>();
        sonlist.add(new HashMap<String, Object>(){{
            put("属性1",11);
            put("属性2",22);
            put("属性3",33);
        }});
        sonlist.add(new HashMap<String, Object>(){{
            put("属性1","a");
            put("属性2","b");
            put("属性3","c");
        }});
        data2.put("asn_son",sonlist);
        data.add(data2);


        Map<String,Object> data22 = Maps.newHashMap();
        data22.put("asn",1);
        List<Map<String,Object>> sonlist1 = new ArrayList<>();
        sonlist1.add(new HashMap<String, Object>(){{
            put("属性1",111);
            put("属性2",221);
            put("属性3",331);
        }});
        sonlist1.add(new HashMap<String, Object>(){{
            put("属性1","z");
            put("属性2","x");
            put("属性3","c");
        }});
        data22.put("asn_son",sonlist1);
        data.add(data22);


        mapTotal.put("data",data);


        System.out.println(com.alibaba.fastjson.JSONObject.toJSONString(mapTotal));
    }

    @Test
    public void t1_6(){
        Map<String, Object> batch = Maps.newIdentityHashMap();
        Map<String, Object> qdgroup;
        qdgroup = Maps.newHashMap();
        qdgroup.put("qacode", 1);
        qdgroup.put("deliverno", 2);
        //new String必须加
        batch.put(new String("batch"), qdgroup);


        qdgroup = Maps.newHashMap();
        qdgroup.put("qacode", 0);
        qdgroup.put("deliverno", 9);
        //new String必须加
        batch.put(new String("batch"), qdgroup);


        //调出库接口
        Map<String, Object> xmlMap = new HashMap<>(1);
        xmlMap.put("line", new HashMap<String, Object>(5) {{
            put("printcode", 123);
            put("type", 456);
            put("outno", 678);
            put("outtime", 890);
            put("userno", 90);
            put("batchs", batch);
        }});

        System.out.println(new String(Objects.requireNonNull(MapToXml.callMapToXML(xmlMap))));
    }

    @Test
    public void t2_1(){
        jdbcTemplate.update("update  yddl_factory_area set code = '1' ");
    }

    @Test
    public void t2(){
        String[] players = {"zhangsan","lisi","wangwu","zhaoliu","chenqi","wuba","hujiu","yuanshi","jiangshiyi"};
        //使用匿名内部类排序
        Arrays.sort(players, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return (o1.compareTo(o2));
            }
        });

        //使用lambda
        Arrays.sort(players, Comparator.naturalOrder());
    }

    @Test
    public void t3(){
        Shelf shelfEntity = new Shelf();
        shelfEntity.setState(0);
        shelfDAO.update(shelfEntity, new EntityWrapper<Shelf>().eq("CODE", null));
    }

    @Test
    public void t4() {
            String xml = "";

            xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                    "<wsinterface>\n" +
                    "<header>\n" +
                    "<code>0</code>\n" +
                    "<msg>成功</msg>\n" +
                    "</header>\n" +
                    "<body>\n" +
                    "<data>\n" +
                    "  <location>\n" +
                    "   <id>59366</id>\n" +
                    "   <code>ZLQ1-12/F001-A01</code>\n" +
                    "   <name>ZLQ1-12/总厂零头库]]]]><![CDATA[></name>\n" +
                    "   <warehouseno><![CDATA[F001</warehouseno>\n" +
                    "   <orgid>92</orgid>\n" +
                    "  </location>\n" +
                    "</data>\n" +
                    "</body>\n" +
                    "</wsinterface>\n";
            String subXml = XmlToolsUtil.getSubXml(xml);

            if("false".equals(subXml)) {
                return;
            }

            LocationWsinterface locationWsinterface = null;
            try {
                locationWsinterface = JaxbUtil.xmlToBean(subXml, LocationWsinterface.class);
            } catch (JAXBException e) {
                e.printStackTrace();
            }
            List<LocationXml> locationXmls = locationWsinterface.getBody().getData().getLocationXmlList();
            List<Shelf> updateList = new ArrayList<>();
            List<Shelf> addList = new ArrayList<>();

            locationXmls.forEach(a -> {
                Shelf shelf = new Shelf();
                shelf.setCodeComplete(a.getCode());
                Shelf newShelf = shelfDAO.selectOne(shelf);
                if (newShelf != null) {
                    newShelf.setWarehouseCode(a.getWarehouseno());
                    newShelf.setFactoryCode(a.getOrgid());
                    updateList.add(newShelf);
                } else {
                    shelf.setWarehouseCode(a.getWarehouseno());
                    shelf.setState(1);
                    shelf.setFactoryCode(a.getOrgid());
                    addList.add(shelf);
                }
            });
            if (updateList.size() > 0) {
                shelfDAO.updateBatches(updateList);
            }
            if (addList.size() > 0) {
                shelfDAO.insertBatches(addList);
            }
    }

    @Test
    public void t5(){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        // 获取前月的第一天
        Calendar cale = Calendar.getInstance();
        cale.add(Calendar.MONTH, -3);
        cale.set(Calendar.DAY_OF_MONTH, 0);
        System.out.println(format.format(cale.getTime()));
    }

    @Test
    public void t6(){
        DateUtils.getDay(DateUtils.addDateDays(DateUtils.getMounthLaterLastDay(),1));
    }

    ExecutorService executorService= Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors()*2);
    @Test
    public void t7(){
            Future<String> future = executorService.submit(() -> {
                try {
                    TimeUnit.SECONDS.sleep(10);
                } catch (InterruptedException e) {
                    System.out.println("任务被中断。");
                }
                return  "OK";
            });

            try {
                String result = future.get(2, TimeUnit.SECONDS);
            } catch (InterruptedException | ExecutionException | TimeoutException e) {
                future.cancel(true);
                System.out.println("任务超时。");
            }finally {
                System.out.println("清理资源。");
            }

    }

    @Test
    public void t8(){
        Map<String, Object> xmlMap = JsontoMap.parseJSON2Map(JSONObject.fromObject("{\"line\":{\"batchs\":{\"batch\":{\"qacode\":\"C0319070804\",\"deliverno\":\"050793\"},\"batch\":{\"qacode\":\"F0119071109\",\"deliverno\":\"050793\"},\"batch\":{\"qacode\":\"C0419071008-01\",\"deliverno\":\"065043\"},\"batch\":{\"qacode\":\"B0219070307\",\"deliverno\":\"050793\"},\"batch\":{\"qacode\":\"R2419070803\",\"deliverno\":\"065043\"}},\"userno\":\"065043\",\"outtime\":\"2019-11-05 12:48:59\",\"outno\":\"XSLKD201911040788\"}}"));
        String dsf = MapToXml.multilayerMapToXml(xmlMap,false);
        System.out.println(EmumConstant.mainInterfaceScanning.NO.getName()+"#####################"+EmumConstant.mainInterfaceScanning.YES.getCode());
    }

    @Test
    public void t9(){
        List<Shelf > shelfList = shelfDAO.selectList(new EntityWrapper<Shelf>().eq("STATE",123));
        shelfList.forEach(s -> {
            System.out.println("111");
        });
        List<Shelf> lstShelf = shelfList.stream().filter(shelf -> shelf.getRepeatOccupancy()==3).collect(Collectors.toList());

    }

    @Test
    public void t10(){
        List<InventoryRegistration> lstAdd = new LinkedList<>(), lstUpdate = new LinkedList<>();
        InventoryRegistration inventoryRegistration = new InventoryRegistration();
        inventoryRegistration.setPlanId("1");
        inventoryRegistration.setRfid("1");
        inventoryRegistration.setQacode("1");
        inventoryRegistration.setShelfid("1");
        inventoryRegistration.setWarehousecode("1");
        inventoryRegistration.setUploadTime(new Date());
        inventoryRegistration.setUserCode("1");
        inventoryRegistration.setMeter("1");
        inventoryRegistration.setMaterialcode("1");
        inventoryRegistration.setResult("1");
        inventoryRegistration.setOrg("1");
        inventoryRegistration.setState(Long.valueOf(EmumConstant.isInventoryed.YES.getCode()));//已盘点
        inventoryRegistration.setEditmeter("1");
        inventoryRegistration.setEditshelf("1");

        //获取盘点明细，如果不存在就新增；存在就更新
        InventoryRegistration inventoryRegistration1 = inventoryRegistration;
        if (inventoryRegistration1 == null) {
            lstAdd.add(inventoryRegistration);
        } else {
            inventoryRegistration1.setId(10012520L);
            inventoryRegistration1.setPlanId("1");
            inventoryRegistration1.setRfid("1");
            inventoryRegistration1.setQacode("1");
            inventoryRegistration1.setShelfid("1");
            inventoryRegistration1.setWarehousecode("1");
            inventoryRegistration1.setUploadTime(new Date());
            inventoryRegistration1.setUserCode("1");
            inventoryRegistration1.setMeter("1");
            inventoryRegistration1.setMaterialcode("1");
            inventoryRegistration1.setResult("1");
            inventoryRegistration1.setOrg("1");
            inventoryRegistration1.setState(Long.valueOf(EmumConstant.isInventoryed.YES.getCode()));//已盘点
            inventoryRegistration1.setEditmeter("1");
            inventoryRegistration1.setEditshelf("1");
            lstUpdate.add(inventoryRegistration1);
        }
        if(lstUpdate.size()>0){
            inventoryRegistrationDAO.updateBatches(lstUpdate);
        }
        if(lstAdd.size()>0){
            inventoryRegistrationDAO.insertBatches(lstAdd);
        }
    }

    @Test
    public void t11(){
        List<Shelf> recommendList = simpl.selectRecommentList("92", "F001", "630", "全木盘", "HC264",
                "", "D", "", 0, 0);

    }

    @Test
    public void t12(){
        Dish dish = dishDAO.selectOne(new Dish() {{
            setCode("1326020138");
        }});
        List<FactoryArea> factorySerialnumList = factoryAreaDAO.selectByMap(new HashMap<String,Object>(){{
            put("CODE","92");
        }});
        //流水号
        String serialnum = (factorySerialnumList !=null && factorySerialnumList.size()>0 && StringUtils.isNotBlank(factorySerialnumList.get(0).getSerialnum()))
                ? factorySerialnumList.get(0).getSerialnum() : "0000";
        serialnum = String.format("%04d", Integer.parseInt(serialnum) + 1);
        //盘号：库位编码第一位字母+盘类型+盘大小+日期标识+流水号(虚拟盘除外，虚拟盘用盘具编码作为盘号)
        String drumno = "NC11-11".charAt(0)+
                    ("铁木盘".equals(dish.getDrumType())? Constant.STRING_ONE:"全木盘".equals(dish.getDrumType()) ? Constant.STRING_TWO : Constant.STRING_THREE) +
                    new DecimalFormat("000").format(Long.parseLong(dish.getOuterDiameter() !=null ? dish.getOuterDiameter() : "0")/10) +
                    new SimpleDateFormat("yyyyMMdd").format(new Date()).substring(2) +
                    serialnum;
    }

    /**
     * 调用存储过程
     */
    @Test
    public void t12_1(){
        Connection conn = JDBCUtils.getConnection();
        PreparedStatement pst = null;

        CallableStatement proc = null; // 创建执行存储过程的对象
        try {
            /*proc = conn.prepareCall("{ call insert_procedure(?,?) }");
            proc.setString(1, "1"); // 设置第一个输入参数
            proc.setString(2, "hello call"); // 设置第一个输入参数
            proc.execute();// 执行*/

            proc = conn.prepareCall("{ call EBS2WMS_CUX_INV_OP () }");
            proc.execute();// 执行

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                // 关闭IO流
                proc.close();
                JDBCUtils.closeAll(null, pst, conn);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Test
    public void test13(){
        ExecutorService executorService = Executors.newCachedThreadPool();
        executorService.execute(() -> {
            while(true){
                System.out.println("1");
                System.out.println("2");
                if(true){
                    System.out.println("interrupted");
                    break;
                }
                System.out.println("3");
            }
            System.out.println("4");
        });
        executorService.shutdown();
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("6");
    }

    @Test
    public void test14(){
        pdaService.storageinStorageinSuccess("000010322425", "AE14-20", 1L, "2020-02-03", "0", "92", "F001");
    }

    @Test
    public void test15(){
        Collection collection = new ArrayList();
        collection.add(4);
        collection.add(2);
        collection.add(8);
        Collection collection2 = new ArrayList();
        collection2.add(2);
        collection2.add(3);
        collection2.add(7);
        // 在调用者中  留下交集的部分
        collection.retainAll(collection2);
        System.out.println(collection);
    }

    @Test
    public void test16(){
        InterfaceDo interfaceDo = InterfaceDoDAO.selectById(10001961);
        interfaceDo.setParamsinfo(interfaceDo.getParamsinfo().replace("LA1202002280028", "LA1202002280000"));
        InterfaceDoDAO.updateById(interfaceDo);
    }

    @Test
    public void test17(){
        int i = 20, j = 20 ;
        while (i > j){
            try {
                TimeUnit.MILLISECONDS.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(i);
            i--;
        }

        System.out.println("22222");
    }

    @Test
    public void test18(){
        Map<String, Object> map = Maps.newIdentityHashMap();
        map.put("a", "1");
        System.out.println(map);
        map = new IdentityHashMap<>();
        System.out.println(map);
    }

    @Test
    public void test19(){
        pdaService.storageinStorageinScan("F001","","93","G0220041206-03","0","HC268",
                "2",1L);
    }

    @Test
    public void test20(){
        Map<String, Object> map = Maps.newHashMap();
        System.out.println((String)map.get("storAreaCode"));
    }

    @Test
    public void test21(){
        // 存放在二维码中的内容
        String text = "我是小铭";
        // 生成的二维码的路径及名称
        String destPath = "F:/360Downloads/二维码图片/1.jpg";
        String str = null;
        try {
            //生成二维码
            QRCodeUtil.encode(text, destPath);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 打印出解析出的内容
        System.out.println(str);

    }

    @Test
    public void test22(){
        pdaService.wzlzPrint("苏D716KP", "92","93");

    }

    @Test
    public void test23(){
        String gdsf = new DecimalFormat("000").format(Long.valueOf("3000" != null ? "3000" : "0") / 10);
        System.out.println(gdsf);
    }

    @Test
    public void test24(){
        ScheduledExecutorService executorService = new ScheduledThreadPoolExecutor(1,
                new BasicThreadFactory.Builder().namingPattern("example-schedule-pool-%d").daemon(true).build());
        executorService.execute(() -> {
            System.out.println("111111111");
        });
    }

    @Test
    public void test25(){
        Map<String, Object> xmlMap = new HashMap<>(1);
        xmlMap.put("line", new HashMap<String, Object>(2) {{
            put("materialno", "");
            put("materialname", "");
        }});
        wmsService.FEWMS001(xmlMap);
    }

    @Test
    public void test26(){
        Double differenceValue = Double.valueOf(".234");
        Double.parseDouble(".214");
    }

    @Test
    public void test27(){
        Map<String, Object> jsonMap = Maps.newHashMap();
        jsonMap.put("red","1");
        jsonMap.put("yellow","1");
        jsonMap.put("green","1");
        jsonMap.put("alarm","1");
        String sd = Rbsc.httpPostWithJSON("http://10.102.11.242:7080/api/HHAms/UpdateLed", com.alibaba.fastjson.JSONObject.toJSONString(jsonMap));
        System.out.println(sd);
        Map<String,Object> retuenmap = (Map<String, Object>) com.alibaba.fastjson.JSONObject.parse(sd);
        System.out.println(retuenmap.get("success"));
        System.out.println(retuenmap.get("errCode"));
        System.out.println(retuenmap.get("errMsg"));
    }

    @Test
    public void test28(){
        exchangePrintDetailService.printWzlz("苏BG6086","93","92");
    }

    @Test
    public void test29(){
        Map<String, Object> map = Maps.newHashMap();
        map.put("LAYER", new BigDecimal(2123.12321));
        String.valueOf(map.get("LAYER"));
    }

    @Test
    public void test30(){

    }

    @Test
    public void test_AddTask(){
        Map<String,Object> jsonMap;
        jsonMap = Maps.newHashMap();
        jsonMap.put("whNoFrom","1");
        jsonMap.put("whNoTo","1");
        jsonMap.put("sysName","1");
        jsonMap.put("deviceCode","1");
        jsonMap.put("taskNo","1");
        jsonMap.put("batchNo","1");
        jsonMap.put("orderNo","1");
        jsonMap.put("locationFrom","1");
        jsonMap.put("locationTo","1");
        jsonMap.put("priority","1");
        jsonMap.put("extParam","1");
        jsonMap.put("ext1","1");
        jsonMap.put("ext2","1");
        jsonMap.put("ext3","1");

        List<Map<String, Object>> areaFromList = new ArrayList<>();
        areaFromList.add(new HashMap<String, Object>(){{
            put("areaCode","222");
        }});
        areaFromList.add(new HashMap<String, Object>(){{
            put("areaCode","333");
        }});
        jsonMap.put("areaFrom",areaFromList);

        List<Map<String, Object>> areaTo = new ArrayList<>();
        areaTo.add(new HashMap<String, Object>(){{
            put("areaCode","555");
        }});
        areaTo.add(new HashMap<String, Object>(){{
            put("areaCode","666");
        }});
        jsonMap.put("areaTo",areaTo);
        String sendPostParams = com.alibaba.fastjson.JSONObject.toJSONString(jsonMap);
        Rbsc.httpPostWithJSON("http://10.102.11.242:7080/", sendPostParams);
        System.out.println(sendPostParams);
    }

    @Test
    public void test_DeleteTask(){
        List<Map<String, Object>> deleteTaskList = new ArrayList<>();
        deleteTaskList.add(new HashMap<String, Object>(){{
            put("taskNo","222");
        }});
        deleteTaskList.add(new HashMap<String, Object>(){{
            put("taskNo","333");
        }});

        String sendPostParams = com.alibaba.fastjson.JSONObject.toJSONString(deleteTaskList);
        Rbsc.httpPostWithJSON("http://10.102.11.242:7080/", sendPostParams);
        System.out.println(sendPostParams);
    }

    @Test
    public void test_ReadStockStatus(){
        Map<String, Object> readMap = Maps.newHashMap();
        readMap.put("station","222");

        String sendPostParams = com.alibaba.fastjson.JSONObject.toJSONString(readMap);
        Rbsc.httpPostWithJSON("http://10.102.11.242:7080/", sendPostParams);
        System.out.println(sendPostParams);
    }

    @Test
    public void test_UpdateLed(){
        Map<String, Object> alarmMap = Maps.newHashMap();
        alarmMap.put("red","1");
        alarmMap.put("yellow","1");
        alarmMap.put("green","1");
        alarmMap.put("alarm","1");

        String sendPostParams = com.alibaba.fastjson.JSONObject.toJSONString(alarmMap);
        Rbsc.httpPostWithJSON("http://10.102.11.242:7080/", sendPostParams);
        System.out.println(sendPostParams);
    }
}


