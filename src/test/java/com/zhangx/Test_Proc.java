package com.zhangx;

import com.tbl.modules.wms2.dao.workorder.WorkOrderDAO;
import freemarker.ext.beans.HashAdapter;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Test_Proc {
    @Autowired
    WorkOrderDAO workOrderDAO;

    /**
     * 存储过程apps.cux_wms_outside_pkg.create_rcv调用
     * 1）定义dao xml WorkOrderDao.xml
     * 2)定义接口 WorkOrderDAO.java
     *
     * 注意事项：
     * 输出因为定义的结果为 map oracle 中不区分大小写，所以输出全部要用大写获取(即使定义的是小写 比如 retCode 获取时候要RETCODE)，
     * 但是输入要和定义一致 (比如定义 poId  map.put("poId","值")，不能map.put("POID","值"));
     *
     * 测试的时候先在test里面单个测试每个存储过程，然后写成方法
     * 封装到service里面在进行组合，单个的public 组合里面调用的可以private，但组合的要public给控制器调用
     * 最后在去控制器里面封装
     */
    @Test
    public Map t_create_rcv(){
        //模拟页面传给控制器的json对象
       /* String json="{\"poId\":\"\",\"poId1\":\"\",}";
        //json对象 JSONObject json数组 JSONArray ,jsonArray= JSONArray.fromObject(json); jsonArray.get(0);
        JSONObject jsonObject= JSONObject.fromObject(json);
        Map map1=(Map)JSONObject.toBean(jsonObject, Map.class);*/
        //输入参数 手工赋值
        Map map=new HashMap<>();
        map.put("poId","值");//输出参数不能少
        map.put("a","值");//输出参数不能少
        map.put("b","值");//输出参数不能少
        //这个实际不需要返回map
        //Map<String,String> resultMap = workOrderDAO.createReceive(mapIn); //对应的存储过程apps.cux_wms_outside_pkg.create_rcv
        try {
            workOrderDAO.createReceive(map); //对应的存储过程apps.cux_wms_outside_pkg.create_rcv
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
        printResult(map);//输出结果
        //输出结果判断
        if(map.get("RETCODE").toString().equals("S")) {
            System.out.println("调用成功，并成功执行！");//
        }
        if(map.get("RETCODE").toString().equals("E")) {
            System.out.println("调用失败，错误信息"+map.get("RETMESS").toString());//
        }
        return map;
    }

    /**
     * apps.cux_wms_outside_pkg.rcv_process
     */
    @Test
    public Map t_rcv_process(){
        Map map=new HashMap<>();
        map.put("poId","值");//输出参数不能少
        map.put("a","值");//输出参数不能少
        map.put("b","值");//输出参数不能少
        //这个实际不需要返回map
        //Map<String,String> resultMap = workOrderDAO.createReceive(mapIn); //对应的存储过程apps.cux_wms_outside_pkg.create_rcv
        try {
            workOrderDAO.receiveProcess(map); //对应的存储过程apps.cux_wms_outside_pkg.create_rcv
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
        printResult(map);//输出结果
        //输出结果判断
        if(map.get("RETCODE").toString().equals("S")) {
            System.out.println("调用成功，并成功执行！");//
        }
        if(map.get("RETCODE").toString().equals("E")) {
            System.out.println("调用失败，错误信息"+map.get("RETMESS").toString());//
        }
        return map;
    }

    //////组合测试
    public void t_create_rcv_And_t_rcv_process() {
        Map map=new HashMap();
        map=t_rcv_process(); //组合的时候把输入参数作为参数 t_rcv_process(map)
        //输出结果判断
        if(map.get("RETCODE").toString().equals("S")) {
            System.out.println("调用成功，并成功执行！");//
            map=t_create_rcv();////组合的时候把输入参数作为参数 t_create_rcv(map)
            if(map.get("RETCODE").toString().equals("S")) {
                System.out.println("调用成功，并成功执行！");//
            }
        }
    }



















    //输出结果到控制台 包括输入 输出要定义 #{retCode,mode=OUT,jdbcType=VARCHAR,javaType=String}, ---x_ret_code
    //            #{retMess,mode=OUT,jdbcType=VARCHAR,javaType=String} ---x_ret_mess
    private void printResult(Map<String,String> resultMap)
    {
        System.out.println("---------------输出----------------------");
        //x_ret_code x_ret_mess
        try{
            if(resultMap!=null){
                System.out.println("输出代码 (x_ret_code); 值 = " + resultMap.get("RETCODE"));//要用大写
                System.out.println("输出代码 (x_ret_mess); 值 = " + resultMap.get("RETMESS"));//要用大写
            }
        }catch (Exception e)
        {
            System.out.println("没有输入参数");
        }

        System.out.println("---------------所有参数----------------------");
        for (String key : resultMap.keySet()) {
            System.out.println("key = " + key + ", value = " + resultMap.get(key));
        }
    }
}
