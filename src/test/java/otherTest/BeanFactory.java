package otherTest;

public interface BeanFactory {

    Object getBean(String name);

}
