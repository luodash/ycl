(function(){
	var $layerAlertWindow = null;
	window.page_interval = [];
	$(document).ajaxError(function(event, jqXHR, ajaxSettings, thrownError ){
		var responseStatus = jqXHR.status;
		var responseStatusText = jqXHR.statusText;
		if(responseStatus==500)
			layer.confirm("系统内部错误，请联系管理员！");
		
	});
	
	var menuUrl =  context_path + "/login/getMenu";
    //加载菜单
    $(function(){
    	$(".sidebar-item").hover(function(){
    		$(".sidebar-item .sub-menu.service-list").css("max-width","1100px");
    	},function(){
    		$(".sidebar-item .sub-menu.service-list").css("max-width", "0px");
    	});
    });
    
	var init_new = function(menus){
		 var parent  =  $("#sidebar>div");
		 var trs ="", tr_index = -1;
		 for(let i = 0 ,l = menus.length; i < l; i++){
		     if( parseInt(i/6) != tr_index){
		    	 if(tr_index != -1) trs += "</tr>";
		    	 tr_index =  parseInt(i/6);
		    	 trs += "<tr>";
		     }
		     trs +='<td class="sub-nav-section"><h3>'+menus[i].menuName+'</h3><ul>';
		     if(menus[i].subMenu){
		    	 for(let j = 0; j < menus[i].subMenu.length; j++){
		    		 trs +="<li id="+menus[i].subMenu[j].menuId+"><a><i class='fa "+( menus[i].subMenu[j].menuIcon  ?　menus[i].subMenu[j].menuIcon:'iconfont  fa-angle-right')　+"'></i>"+menus[i].subMenu[j].menuName+"</a></li>";
		    		 let data = menus[i].subMenu[j];
		    		 let id = data.menuId;
		    		 parent.on("click", "#"+id, function(){
		  		    	if(data.menuUrl && !data.menuUrl.startsWith("#")){
		  			    	for( var i in page_interval ){
		  			    		 clearInterval(page_interval[i]);
		  			    	}
		  		    		layer.closeAll();
		  			    	layer.msg('加载中', {icon: 16});
			  			  	if( $("#breadcrumb .current:contains('"+data.menuName+"')").length == 0 ){
					    		var _li =$(`<a href="#" class="current">${data.menuName}</a>`);
						    	$("#breadcrumb").append(_li);
						    	_li.on("click",(function(data){
						    		return function(){
						    			$(".container-fluid [page-data]").hide();
							    		$(".container-fluid [page-data='"+data.menuId+"']").show();
										$(window).resize();
						    		}
						    	}(data)));
						    	
					    	}else{
					    		$(".container-fluid [page-data='"+data.menuId+"']").remove();
					    	}
		  			    	$("#sidebar .active").removeClass("active");
		  			    	$(this).addClass("active");
		  				    $.ajax({
		  				    	url:context_path + "/" +data.menuUrl,
		  				    	type:"POST",
		  				    	success:function(data){
		  				    		$(".sidebar-item .sub-menu.service-list").css("max-width", "0px");
		  				    		layer.closeAll();
		  				    		if( data && data.url ){
		  	 							  $.get( context_path+data.url ).done(function( data ){
		  								        if( data ){
		  								        	//将网页添加进来
		  								        	$(".container-fluid [page-data]").hide();
		  								        	$(".container-fluid").append("<div page-data='"+id+"'>"+data+"</div>");
		  								        	//初始化菜单按钮
		  								        	initButtonState();
		  										}
		  								  });
		  							
		  							}else if(data){
		  								if( data.indexOf("admin_login") != -1 ){
	  										window.location =  context_path + "/login/logout.do";
	  						    			return false;
	  						    		}
		  								$(".container-fluid [page-data]").hide();
		  								$(".container-fluid").append("<div page-data='"+id+"'>"+data+"</div>");
		  							}
		  				    	},
		  				    	error:function(e){
		  				    		if(e.status==200)
		  				    			window.location.href = window.location.href; 
		  				    		layer.closeAll();
		  				    		layer.msg('加载失败！',{icon:2});
		  				    	}
		  				    	
		  				    });
		  		    	}
		  			});
		    	 }
		     };
		     trs +='</ul></td>';
		 }
		var table='<div class="sub-menu service-list" id="serviceList" ><table><tbody>'+trs+'</tbody></table></div>';
	}
    
	var init = function( menus ,pid ){
		var user_menu = [];
		$.ajax({
			 url:context_path + "/user/getUserMenu",
			 async: false,
             success: function( data){
            	 if(data){
            		 for(var i = 0; i< data.length ; i++){
            			 user_menu[ data[i]["menu_id"] ] = data[i];
            		 }
            	 }else{
            		 user_menu  = []; 
            	 }
            
             }
	    });
		if( user_menu.length == 0 ) return;
		
	    for(var i = 0 ,l = menus.length; i < l; i++){
		    let data = menus[i];
		    let _class = data.subMenu && data.subMenu.length ? "submenu" : ""
		    let _si_ = false;
		    let __menuId = data.menuI;
		    if(  user_menu[data.menuId] ) _si_ = true;
		    if( data.parentId && user_menu[data.parentId]) _si_ = true;
		    if( _class ){
	    	  for( var k =0; k < data.subMenu.length; k++ ){
		    	    if(  user_menu[  data.subMenu[k].menuId  ] ) _si_ = true;
			    }
			}
		    if(_si_ == true){
		    	 let _li = `<li id="${data.menuId}" class="${_class}" pid="${data.parentId}"><a><i class="fa ${data.menuIcon || 'fa-angle-right'}"></i> <span>${data.menuName}</span>${data.menuIcon ? '':""}</a> </li>`;
				    var parent  =  $("#sidebar>ul");
				    if( pid ){
				    	!$(pid).find("ul").length &&  $(pid).append("<ul></ul>");
				    	parent = $(pid).find("ul");
				    }
				    let id = data.menuId;
				    parent.append( _li ).on("click", "#"+id, function(){
				    	if(data.menuUrl && !data.menuUrl.startsWith("#")){
		  			    	for( var i in page_interval ){
		  			    		 clearInterval(page_interval[i]);
		  			    	}
				    		layer.closeAll();
					    	layer.msg('加载中', {icon: 16});
					     	if( $("#breadcrumb .current:contains('"+data.menuName+"')").length == 0 ){
					    		var _li =$(`<a id="removeTab${data.menuId}" href="#" class="current" >${data.menuName}<i id="removePage${data.menuId}" class="icon icon-remove"></i></a>`);
						    	$("#breadcrumb").append(_li);
						    	_li.on("click",(function(data){
                                    _li.addClass('tabActive').siblings().removeClass('tabActive');
						    		return function(){
						    			$(".container-fluid [page-data]").hide();
							    		$(".container-fluid [page-data='"+data.menuId+"']").show();
							    		$(window).resize();
						    		}
						    	}(data)));
						    	
						    	$("#removePage"+data.menuId).on("click",(function(data){
                                    return function(){
                                        $(".container-fluid [page-data='"+data.menuId+"']").remove();
                                        $("#removeTab"+data.menuId).remove();
                                        if($(".container-fluid > div").length > 0){
                                        	$("#breadcrumb >a:last").addClass("tabActive").siblings().removeClass("tabActive");
                                        	$(".container-fluid > div:last").css("display", "block").siblings().css("display", "none");
                                        }
                                    }
                                }(data)));

                                $("#removeTab"+data.menuId).on("click",(function(data){
                                    return function(){
                                        var isActive = $(this).hasClass(".tabActive");
                                        //console.log(isActive);
                                        if(!isActive){
                                            $(this).addClass("tabActive").siblings().removeClass("tabActive");
                                        }
                                    }
                                }(data)));
						    	
					    	}else{
					    		$(".container-fluid [page-data='"+data.menuId+"']").remove();
					    	}
					    	$("#sidebar .active").removeClass("active");
					    	$(this).addClass("active");
						    $.ajax({
						    	url:context_path + "/" +data.menuUrl,
						    	type:"POST",
						    	success:function(data){
						    		layer.closeAll();
						    		if( data && data.url ){
			 							  $.get( context_path+data.url ).done(function( data ){
										        if( data ){
										        	//将网页添加进来
										        	$(".container-fluid [page-data]").hide();
		  										   	$(".container-fluid").append("<div page-data='"+id+"'>"+data+"</div>");
		  										   	//初始化菜单按钮
		  										   	initButtonState();
												}
										  });
									}else if(data){
										if( data.indexOf("admin_login") != -1 ){
											window.location =  context_path + "/login/logout.do";
							    			return false;
							    		}
										$(".container-fluid [page-data]").hide();
										$(".container-fluid").append("<div page-data='"+id+"'>"+data+"</div>");
									}
						    	},
						    	error:function(e){
						    		if(e.status==200) 
						    			window.location.href = window.location.href; 
						    		layer.closeAll();
						    		layer.msg('加载失败！',{icon:2});
						    	}
						    });
				    	}
					});
				if( _class  ) init(data.subMenu, "#"+data.menuId );
		    }
		}
	};
    
	$.ajax({
	  type : "get",
      url : menuUrl,
      success : function( data ){
	     if(data && $.type(data)== "array" && data.length ) init_new( data ); init( data );
	  }
	});
	
	//初始化按钮状态：根据用户的权限判断
	function initButtonState(){
		$.ajax({
			url:context_path+"/user/getSessionUser",
			type:"POST",
			dataType:"JSON",
			success:function(data){
				if(data){
					for ( var d in data) {
						if(d.endWith("Qx")){
							if(data[d]==1){
								//有权限
							}else{
								//没有权限
								$("button.btn-"+d).attr("disabled","disabled");
								$("button.btn-"+d).attr("title","没有该权限");
							}
						}
					}
				}
			}
		});
	}
	
	//加载菜单--end
	var openLayer = function( url, title, area ){
		$.post(context_path + url, function(str){
			  layer.open({
			    type : 1,
			    title : title, 
			    area : area ||"600px",
			    skin : "layui-layer-molv",
			    moveType: 1, //拖拽风格，0是默认，1是传统拖动
			    content: str //注意，如果str是object，那么需要字符拼接。
			  });
		}).error(function() {
			layer.closeAll();
    		layer.msg('加载失败！',{icon:2});
		});
	}
	$("#profile-messages").on("click","li", function(){
		if( $("i", this).hasClass("icon-check") )
			openLayer('/index/pwd' , "设置密码");
		if( $("i", this).hasClass("icon-user") )
			openLayer('/index/userInfo' , "个人资料");
		if( $("i", this).hasClass("icon-external-link") )
			openLayer('/techbloom/platform/main/productCode.jsp' , "代码生成", ['800px', '645px']);
	});
	$(function(){
		 $.get( context_path+"/index/main_page" ).done(function( data ){
	        if( data ){										   
	        	 $(".container-fluid [page-data]").hide();
				 $(".container-fluid").append("<div page-data='main_page'>"+data+"</div>");
			}
	    });
	})
}());