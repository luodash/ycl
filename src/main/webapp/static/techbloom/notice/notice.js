var oriData; 
var _grid;
var openwindowtype;
var noticeid="";
var $queryWindow;  //查询窗口对象
$("#notice_list_operationTime").daterangepicker({
	'applyClass' : 'btn-sm btn-success',
	'cancelClass' : 'btn-sm btn-default',
	'opens' : 'left',
	'format' :'YYYY-MM-DD',
	'timePicker' :false,
	'timePickerIncrement' : 5,
	'timePicker12Hour' : false,
	locale: {
		fromLabel: '从',
		toLabel: '到',
		applyLabel: '确定',
		cancelLabel: '取消'
	}
});
_grid = jQuery("#notice_list_grid-table").jqGrid({
	url : context_path + '/notice/getlist.do',
    datatype : "json",
    colNames : [ '主键','标题','内容','创建人','时间','备注'],
    colModel : [ 
    			 {name:	'id', index :'id',width:50,hidden:true},
                 {name : 'title',index : 'title',width : 50}, 	
                 {name: 'notice',index : 'notice',width : 60},
                 {name : 'userName',index : 'createuser',width : 60}, 
                 {name : 'createtime', index : 'createtime',width : 50,
                     formatter:function(cellValu,option,rowObject){
                         if (cellValu != null) {
                             return getFormatDateByLong(new Date(cellValu), "yyyy-MM-dd HH:mm");
                         } else {
                             return "";
                         }
                     }
				 },
                 {name : 'remark',index : 'remark',width : 90} 
               ],
    rowNum : 20,
    rowList : [ 10, 20, 30 ],
    pager : '#notice_list_grid-pager',
    sortname : 'id',
    sortorder : "desc",
    altRows: true,
    viewrecords : true,
    caption : "公告列表",
    hidegrid:false,
    multiselect:true,
    loadComplete : function(data) 
    {
    	var table = this;
    	setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
    	oriData = data;
    },
    emptyrecords: "没有相关记录",
    loadtext: "加载中...",
    pgtext : "页码 {0} / {1}页",
    recordtext: "显示 {0} - {1}共{2}条数据"
});

jQuery("#notice_list_grid-table").navGrid('#notice_list_grid-pager',{edit:false,add:false,del:false,search:false,refresh:false})
.navButtonAdd('#notice_list_grid-pager',{  
	caption:"",   
	buttonicon:"fa fa-refresh green",   
	onClickButton: function(){   
		$("#notice_list_grid-table").jqGrid('setGridParam', 
				{
			 		postData: {queryJsonString:"",page:1} //发送数据 
				}
		).trigger("reloadGrid");
	}
});

$(window).on('resize.jqGrid', function () {
	$("#notice_list_grid-table").jqGrid( 'setGridWidth', $(window).width()-$("#sidebar").width() -7);
	$("#notice_list_grid-table").jqGrid( 'setGridHeight', ($(window).height()-$("#notice_list_table_toolbar").outerHeight(true)- 
	$("#notice_list_grid-pager").outerHeight(true)-$("#user-nav").height()-$("#breadcrumb").height()-$(".ui-jqgrid-labels").height()-35 ) );
});

$(window).triggerHandler('resize.jqGrid');

/*打开添加页面*/
function openAddPage(){
	openwindowtype=0;
	layer.load(2);
	$.post(context_path+'/notice/notice_edit', {}, function(str){
		$queryWindow = layer.open({
			    title : "公告添加", 
		    	type: 1,
		    	skin : "layui-layer-molv",
		    	area : "600px",
		    	shade: 0.6, //遮罩透明度
	    	    moveType: 1, //拖拽风格，0是默认，1是传统拖动
		    	content: str,//注意，如果str是object，那么需要字符拼接。
		    	success:function(layero, index){
		    		layer.closeAll('loading');
		    	}
			});
		}).error(function() {
			layer.closeAll();
    		layer.msg('加载失败！',{icon:2});
		});
}
var noticeid="";
/*打开编辑页面*/
function openEditPage(){
	openwindowtype=1;
	var selectAmount = getGridCheckedNum("#notice_list_grid-table");
	if(selectAmount==0){
		layer.msg("请选择一条记录！",{icon:2});
		return;
	}else if(selectAmount>1){
		layer.msg("只能选择一条记录！",{icon:8});
		return;
	}
	layer.load(2);
	noticeid = $('#notice_list_grid-table').jqGrid('getGridParam','selrow');
	$.post(context_path+'/notice/notice_edit', {}, function(str){
		$queryWindow = layer.open({
			    title : "公告修改",
		    	type: 1,
		    	skin : "layui-layer-molv",
		    	area : ['630px','540px'],
		    	shade: 0.6, //遮罩透明度
	    	    moveType: 1, //拖拽风格，0是默认，1是传统拖动
		    	content: str,//注意，如果str是object，那么需要字符拼接。
		    	success:function(layero, index){
		    		layer.closeAll('loading');
		    	}
			});
		}).error(function() {
			layer.msg('加载失败,请检查网络！',{icon:2});
			layer.closeAll('loading');
		});
}

//重新加载表格
function gridReload(){
	_grid.trigger("reloadGrid");  //重新加载表格  
}


/**
 * 打开查询界面
 */
function openQueryPage(){
	var queryParam = iTsai.form.serialize($('#notice_list_queryForm'));
	//接着执行刷新列表的操作
	var times = queryParam.operationTime.split(" - ");
	queryParam.operationTime = times[0];
	queryParam.operationTime_end = times[1];
	queryLogListByParam(queryParam);
}

/**
 * 查询功能:获取查询页面中的值，并将值放入列表页面中隐藏的form
 * @param jsonParam     查询页面传递过来的json对象
 */
function queryLogListByParam(jsonParam){
	var queryJsonString = JSON.stringify(jsonParam);         //将json对象转换成json字符串
	//执行查询操作
	$("#notice_list_grid-table").jqGrid('setGridParam', 
			{
		postData: {queryJsonString:queryJsonString} //发送数据 
			}
	).trigger("reloadGrid");

}
/**
 * 根据主键删除表格记录
 */
function deleteNotice(){
	var selectAmount = getGridCheckedNum("#notice_list_grid-table");
	if(selectAmount==0){
		layer.msg("请选择一条记录！",{icon:2});
		return;
	}
	layer.confirm('确定删除？', /*显示的内容*/
		{
		  shift: 6,
		  moveType: 1, //拖拽风格，0是默认，1是传统拖动
		  title:"操作提示",  /*弹出框标题*/
		  icon: 3,      /*消息内容前面添加图标*/
		  btn: ['确定', '取消']/*可以有多个按钮*/
		}, function(index, layero){
		   //确定按钮的回调
			//获取表格中选中的用户记录
			var ids=$('#notice_list_grid-table').jqGrid('getGridParam','selarrrow');
			$.ajax({
				url:context_path+"/notice/delNotice?ids="+ids,
				type:"POST",
				data:{ids : ids},
				dataType:"json",
				success:function(data){
					if(data){
						layer.msg("操作成功!",{icon:1});
						//刷新用户列表
  						$("#notice_list_grid-table").jqGrid('setGridParam', 
							{
								postData: {queryJsonString:""} //发送数据 
							}
						).trigger("reloadGrid");
						layer.close(index);
					}else{
						layer.msg("操作失败!",{icon:2});
					}
				}
			});
			
		}, function(index){
		  //取消按钮的回调
		  layer.close(index);
		});
}

function initUser(userId){
	//初始化用户信息
		$.ajax({
			url:context_path+"/user/getUser?tm="+new Date(),
			type:"POST",
			data:{userId : userId},
			dataType:"JSON",
			success:function(data){
				if(data){
					
					var user = data;
					$("#notice_list_userId").select2("data", {id: user.userId, text: user.name});
					//初始化部门下来框和角色下拉框的值
					initDept();
				}
			}
		});
}
function initDept(){
	//初始化部门下来框和角色下拉框的值
	$.ajax({
		url:context_path+"/user/getSomeInfo?tm="+new Date(),
		type:"POST",
		data:{ userId: $("#notice_list_userId").val() },
		dataType:"JSON",
		async:false,
		success:function(data){
			if(data){
				console.info("调用   notic.js 中的 init   ")
                if(data[factoryCode]){//给厂区
                    $("#notice_list_factoryCode").select2("data", {id: data['factoryCode'], text: data['factoryCode']});
                }
			}
		}
	});
}


