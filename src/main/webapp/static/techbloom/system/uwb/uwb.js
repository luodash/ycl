var uwb_list_oriData;
var uwb_list_grid;
var $queryWindow;  //查询窗口对象
var uwb_list_dynamicDefalutValue="60cce761390a4e51bc4636271b52ab94";

uwb_list_grid = jQuery("#uwb_list_grid-table").jqGrid({
	url : context_path + '/uwb/list.do',
    datatype : "json",
    colNames : [ 'ID','标签号', 'XSIZE','YSIZE','创建时间' ],
    colModel : [ 
		 {name : 'id',index : 'id',hidden : true},
		 {name : 'tag',index : 'tag',width : 60},
		 {name : 'xsize',index : 'xsize',width : 50},
		 {name : 'ysize',index : 'ysize',width : 60},
		 {name : 'createtime',index : 'createtime',width : 70}
    ],
    rowNum : 20,
    rowList : [ 10, 20, 30 ],
    pager : '#uwb_list_grid-pager',
    sortname : 'id',
    sortorder : "desc",
    altRows: true,
    viewrecords : true,
    hidegrid:false,
    multiselect:true,
    multiboxonly: true,
    beforeRequest:function (){
        dynamicGetColumns(uwb_list_dynamicDefalutValue,"uwb_list_grid-table",$(window).width()-$("#sidebar").width() -7);
        //重新加载列属性
    },
    loadComplete : function(data){
        var table = this;
        setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
		uwb_list_oriData = data;
    },
    emptyrecords: "没有相关记录",
    loadtext: "加载中...",
    pgtext : "页码 {0} / {1}页",
    recordtext: "显示 {0} - {1}共{2}条数据"
});

jQuery("#uwb_list_grid-table").navGrid('#uwb_list_grid-pager',
{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd('#uwb_list_grid-pager',{
	caption:"",   
	buttonicon:"fa fa-refresh green",   
	onClickButton: function(){   
		$("#uwb_list_grid-table").jqGrid('setGridParam', {
			postData: {queryJsonString:""} //发送数据 
		}).trigger("reloadGrid");
	}
}).navButtonAdd('#uwb_list_grid-pager',{
	  caption: "",
	  buttonicon:"fa  icon-cogs",   
      onClickButton : function (){
    	  jQuery("#uwb_list_grid-table").jqGrid('columnChooser',{
    		  done: function(perm, cols){
                  dynamicColumns(cols,uwb_list_dynamicDefalutValue);
    			  $("#uwb_list_grid-table").jqGrid( 'setGridWidth', $(window).width()-$("#sidebar").width() -7);
    		  }
    	  });
      }
});

$(window).on("resize.jqGrid", function () {
	$("#uwb_list_grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);
	$("#uwb_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-$(".query_box").outerHeight(true)-
	$("#uwb_list_yy").outerHeight(true)-$("#uwb_list_fixed_tool_div").outerHeight(true)- $("#uwb_list_grid-pager").outerHeight(true)-
	$("#gview_uwb_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
});
$(window).triggerHandler('resize.jqGrid');


/**
 * 查询功能:获取查询页面中的值，并将值放入列表页面中隐藏的form
 * @param jsonParam     查询页面传递过来的json对象
 */
function uwb_list_queryLogListByParam(jsonParam){
	var queryJsonString = JSON.stringify(jsonParam);         //将json对象转换成json字符串
	//执行查询操作
	$("#uwb_list_grid-table").jqGrid('setGridParam', 
	{
		postData: {queryJsonString:queryJsonString} //发送数据 
	}).trigger("reloadGrid");
}

/*打开添加页面*/
function uwb_list_openAddPage(){
	layer.load(2);
	$.post(context_path+'/uwb/uwbAdd', {}, function(str){
		$queryWindow = layer.open({
			    title : "UWB标签号添加",
		    	type: 1,
		    	skin : "layui-layer-molv",
		    	area : "600px",
		    	shade: 0.6, //遮罩透明度
	    	    moveType: 1, //拖拽风格，0是默认，1是传统拖动
		    	content: str,//注意，如果str是object，那么需要字符拼接。
		    	success:function(layero, index){
		    		layer.closeAll('loading');
		    	}
			});
		}).error(function() {
			layer.closeAll();
    		layer.msg('加载失败！',{icon:2});
		});
}

//导出
function uwb_list_exportLogFile(){
    $("#uwb_list_ids").val($('#uwb_list_grid-table').jqGrid('getGridParam','selarrrow'));
    $("#uwb_list_hiddenForm").submit();
}

/**
 * 查询按钮点击事件
 */
function uwb_list_openQueryPage(){
	var queryParam = iTsai.form.serialize($('#uwb_list_query_form'));
	uwb_list_queryLogListByParam(queryParam);
}

function uwb_list_reset(){
    $("#uwb_list_username").val("");
    $("#uwb_list_name").val("");
    $("#uwb_list_email").val("");
    uwb_list_openQueryPage();
}

$("#uwb_list_name").keypress(function (e) {
	if (e.which == 13) {
		uwb_list_openQueryPage();
	}
});


