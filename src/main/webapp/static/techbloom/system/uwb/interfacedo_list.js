var interfacedo_list_grid;
var $queryWindow;  //查询窗口对象
var interfacedo_list_dynamicDefalutValue="60cce761390a4e51bc4636271b52ab94";
var interfacedo_list_queryFormData = iTsai.form.serialize($("#interfacedo_list_hiddenQueryForm"));

$(function  (){
	$(".toggle_tools").click();
});

$("input").keypress(function (e) {
	if (e.which === 13) {
		interfacedo_list_openQueryPage();
	}
});

interfacedo_list_grid = jQuery("#interfacedo_list_grid-table").jqGrid({
	url : context_path + '/interfaceDo/list.do',
	datatype : "json",
	colNames : [ 'ID','状态','质保号','批次号','单据号','接口编码', '接口名称','创建时间','最新执行时间','请求报文','返回报文','首次失败原因','执行次数' ],
	colModel : [
		{name : 'id',index : 'id',hidden : true},
		{name : 'issuccess',index : 'issuccess',width : 100,formatter:function(cellvalue){
				// 是否成功（0，新增，1，成功，2，失败，3，进行中）
				if(cellvalue==='0'){
					return "<span style='color:saddlebrown;font-weight:bold;'>新增</span>";
				}else if(cellvalue==='1'){
					return "<span style='color:green;font-weight:bold;'>成功</span>";
				}else if(cellvalue==='2'){
					return "<span style='color:red;font-weight:bold;'>失败</span>";
				}else if(cellvalue==='3'){
					return "<span style='color:orange;font-weight:bold;'>挂起</span>";
				}else if(cellvalue==='4'){
					return "<span style='color:mediumvioletred;font-weight:bold;'>失败（在线）</span>";
				}
			}
		},
		{name : 'qacode',index : 'qacode',width : 160},
		{name : 'batchNo',index : 'batchNo',width : 160},
		{name : 'shipno',index : 'shipno',width : 160},
		{name : 'interfacecode',index : 'interfacecode',width : 100},
		{name : 'interfacename',index : 'interfacename',width : 180},
		{name : 'createtime',index : 'createtime',width : 170},
		{name : 'operatetime',index : 'operatetime',width : 170},
		{name : 'paramsinfo',index : 'paramsinfo',width : 160},
		{name : 'returncode',index : 'returncode',width : 160},
		{name : 'flasecontent',index : 'flasecontent',width : 260},
		{name : 'executenum',index : 'executenum',width : 60}

	],
	rowNum : 20,
	rowList : [ 10, 20, 30 ],
	pager : '#interfacedo_list_grid-pager',
	sortname : 'id',
	sortorder : "desc",
	altRows: true,
	viewrecords : true,
	hidegrid:false,
	autowidth:false,
	multiselect:true,
	multiboxonly: true,
	shrinkToFit:false,
	autoScroll: true,
	beforeRequest:function (){
		dynamicGetColumns(interfacedo_list_dynamicDefalutValue,"interfacedo_list_grid-table",$(window).width()-$("#sidebar").width() -7);
		//重新加载列属性
	},
	loadComplete : function(data){
		var table = this;
		setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
		$("#interfacedo_list_grid-table").closest(".ui-jqgrid-bdiv").css({ 'overflow-x': 'auto' });
	},
	emptyrecords: "没有相关记录",
	loadtext: "加载中...",
	pgtext : "页码 {0} / {1}页",
	recordtext: "显示 {0} - {1}共{2}条数据"
});

jQuery("#interfacedo_list_grid-table").navGrid('#interfacedo_list_grid-pager',
	{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd('#interfacedo_list_grid-pager',{
	caption:"",
	buttonicon:"fa fa-refresh green",
	onClickButton: function(){
		$("#interfacedo_list_grid-table").jqGrid('setGridParam', {
			postData: {queryJsonString:""} //发送数据 
		}).trigger("reloadGrid");
	}
}).navButtonAdd('#interfacedo_list_grid-pager',{
	caption: "",
	buttonicon:"fa  icon-cogs",
	onClickButton : function (){
		jQuery("#interfacedo_list_grid-table").jqGrid('columnChooser',{
			done: function(perm, cols){
				dynamicColumns(cols,interfacedo_list_dynamicDefalutValue);
				$("#interfacedo_list_grid-table").jqGrid( 'setGridWidth', $(window).width()-$("#sidebar").width() -7);
			}
		});
	}
});

$(window).on("resize.jqGrid", function () {
	$("#interfacedo_list_grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);
	$("#interfacedo_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-$("#interfacedo_list_table_toolbar").outerHeight(true)-
		$("#interfacedo_list_yy").outerHeight(true)- $("#interfacedo_list_fixed_tool_div").outerHeight(true)-
		$("#interfacedo_list_grid-pager").outerHeight(true)- $("#gview_interfacedo_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
});
$(window).triggerHandler('resize.jqGrid');

//重新加载表格
function gridReload(){
	interfacedo_list_grid.trigger("reloadGrid");  //重新加载表格
}

/**
 * 查询功能:获取查询页面中的值，并将值放入列表页面中隐藏的form
 * @param jsonParam     查询页面传递过来的json对象
 */
function interfacedo_list_queryLogListByParam(jsonParam){
	var queryJsonString = JSON.stringify(jsonParam);         //将json对象转换成json字符串
	//执行查询操作
	$("#interfacedo_list_grid-table").jqGrid('setGridParam', {
		postData: {queryJsonString:queryJsonString} //发送数据 
	}).trigger("reloadGrid");
}

/**打开编辑页面*/
function interfacedo_list_openEditPage(){
	var selectAmount = getGridCheckedNum("#interfacedo_list_grid-table");
	if(selectAmount==0){
		layer.msg("请选择一条记录！",{icon:2});
		return;
	}else if(selectAmount>1){
		layer.msg("只能选择一条记录！",{icon:8});
		return;
	}

	layer.load(2);
	$.post(context_path+'/interfaceDo/interfacedo_edit', {
		id:$('#interfacedo_list_grid-table').jqGrid('getGridParam','selrow')
	}, function(str){
		$queryWindow = layer.open({
			title : "修改接口请求参数",
			type: 1,
			skin : "layui-layer-molv",
			area : [window.screen.width-150+"px",document.body.clientHeight-150+"px"],
			shade: 0.6, //遮罩透明度
			moveType: 1, //拖拽风格，0是默认，1是传统拖动
			content: str,//注意，如果str是object，那么需要字符拼接。
			success:function(layero, index){
				layer.closeAll('loading');
			}
		});
	}).error(function() {
		layer.closeAll();
		layer.msg('加载失败！',{icon:2});
	});
}

/**重新上传*/
function interfacedo_list_setConcal(){
	var ids = jQuery("#interfacedo_list_grid-table").jqGrid("getGridParam", "selarrrow");
	if (ids == null || ids.length === 0){
		layer.msg('请至少选择一条记录！',{icon:2});
		return;
	}
	$.post(context_path+'/interfaceDo/setConcal.do?ids='+ids,{},function success(){
		layer.msg("更新成功！", {icon: 1,time:1000});
		jQuery("#interfacedo_list_grid-table").trigger("reloadGrid");
	});
}

/**手动设为成功*/
function interfacedo_list_setSuccess(){
	var ids = jQuery("#interfacedo_list_grid-table").jqGrid("getGridParam", "selarrrow");
	if (ids == null || ids.length === 0){
		layer.msg('请至少选择一条记录！',{icon:2});
		return;
	}
	$.post(context_path+'/interfaceDo/setSuccess.do?ids='+ids,{},function success(){
		layer.msg("更新成功！", {icon: 1,time:1000});
	});
	jQuery("#interfacedo_list_grid-table").trigger("reloadGrid");
}

/**打开详情列表页面*/
function interfacedo_list_openInfoPage(){
	var selectAmount = getGridCheckedNum("#interfacedo_list_grid-table");
	if(selectAmount==0){
		layer.msg("请选择一条记录！",{icon:2});
		return;
	}else if(selectAmount>1){
		layer.msg("只能选择一条记录！",{icon:8});
		return;
	}
	layer.load(2);
	$.post(context_path+'/interfaceDo/toDetailList.do', {
		id:jQuery("#interfacedo_list_grid-table").jqGrid("getGridParam", "selrow")
	}, function(str){
		$queryWindow = layer.open({
			title : "接口详情",
			type: 1,
			skin : "layui-layer-molv",
			area : [window.screen.width-150+"px",document.body.clientHeight-150+"px"],
			shade: 0.6, //遮罩透明度
			moveType: 1, //拖拽风格，0是默认，1是传统拖动
			content: str,//注意，如果str是object，那么需要字符拼接。
			success:function(layero, index){
				layer.closeAll("loading");
			}
		});
	}).error(function() {
		layer.closeAll();
		layer.msg("加载失败！",{icon:2});
	});
}

/**
 * 查询按钮点击事件
 */
function interfacedo_list_openQueryPage(){
	var queryParam = iTsai.form.serialize($('#interfacedo_list_query_form'));
	interfacedo_list_queryLogListByParam(queryParam);
}

/**重置*/
function interfacedo_list_reset(){
	iTsai.form.deserialize($("#interfacedo_list_query_form"),interfacedo_list_queryFormData);
	$("#interfacedo_list_query_form #interfacedo_list_success").select2("val","");
	interfacedo_list_openQueryPage();
}


//状态
$("#interfacedo_list_query_form #interfacedo_list_success").select2({
	placeholder: "选择状态",
	minimumInputLength:0,   //至少输入n个字符，才去加载数据
	allowClear: true,  //是否允许用户清除文本信息
	delay: 250,
	formatNoMatches:"没有结果",
	formatSearching:"搜索中...",
	formatAjaxError:"加载出错啦！",
	ajax : {
		url: context_path+"/BaseDicType/getKindList",
		type:"POST",
		dataType : 'json',
		delay : 250,
		data: function (term,pageNo) {     //在查询时向服务器端传输的数据
			term = $.trim(term);
			return {
				queryString: term,    //联动查询的字符
				pageSize: 15,    //一次性加载的数据条数
				pageNo:pageNo,    //页码
				description:"INTERFACE_DO_SUCCESS"
			}
		},
		results: function (data,pageNo) {
			var res = data.result;
			if(res.length>0){   //如果没有查询到数据，将会返回空串
				var more = (pageNo*15)<data.total; //用来判断是否还有更多数据可以加载
				return {
					results:res,more:more
				};
			}else{
				return {
					results:{}
				};
			}
		},
		cache : true
	}
});


