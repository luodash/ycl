// var oriData;
// var _grid;
//
// var dynamicDefalutValue="eb2f71f889ef4c8e885dda8c5b292b64";
//
// var openwindowtype = 0; //打开窗口类型：0新增，1修改
//
// var zTree;   //树形对象
// var selectTreeId = 0;   //存放选中的树节点id
// //树的相关设置
// var setting = {
//     check: {
//         enable: true
//     },
//     data: {
//         simpleData: {
//             enable: true
//         }
//     },
//     edit: {
//         enable: false,
//         drag:{
//             isCopy:false,
//             isMove:false
//         }
//     },
//     callback: {
//         onAsyncSuccess: zTreeOnAsyncSuccess
//     },
//     async: {
//         enable: true,
//         url:context_path+"/role/getMenulistByRoleId",
//         autoParam:["id"],
//         otherParam: {"rid":null},
//         type: "POST"
//     },//异步加载数据
// };
//
// //ztree加载成功之后的回调函数
// function zTreeOnAsyncSuccess(event, treeId, treeNode, msg) {
//     zTree.expandAll(true);
// };


// _grid = jQuery("#grid-table").jqGrid({
//     url : context_path + '/role/roleList.do',
//     datatype : "json",
//     colNames : [ 'PDA权限' ],
//     colModel : [
//
//         {name : 'editQx',index : 'EDIT_QX',width : 20,formatter:editQxF}
//
//     ],
//     rowNum : 20,
//     rowList : [ 10, 20, 30 ],
//     pager : '#grid-pager',
//     altRows: true,
//     viewrecords : true,
//     hidegrid:false,
//     multiselect:false,
//     multiboxonly: true,
//     beforeRequest:function (){
//         dynamicGetColumns(dynamicDefalutValue,'grid-table', $(window).width()-$("#sidebar").width() -7);
//         //重新加载列属性
//     },
//     loadComplete : function(data)
//     {
//         var table = this;
//         setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
//         oriData = data;
//
//         /*//初始化开关控件
//         $(".bootstrap-switch").bootstrapSwitch();*/
//     },
//     emptyrecords: "没有相关记录",
//     loadtext: "加载中...",
//     pgtext : "页码 {0} / {1}页",
//     recordtext: "显示 {0} - {1}共{2}条数据"
// });

jQuery("#grid-test").navGrid('#grid-pager',{edit:false,add:false,del:false,search:false,refresh:false})
    .navButtonAdd('#grid-pager',{
        caption:"",
        buttonicon:"fa fa-refresh green",
        onClickButton: function(){
            $("#grid-table").jqGrid('setGridParam',
                {
                    postData: {queryJsonString:""} //发送数据
                }
            ).trigger("reloadGrid");
        }
    })
    .navButtonAdd('#grid-pager',{
        caption: "",
        buttonicon:"fa  icon-cogs",
        onClickButton : function (){
            jQuery("#grid-table").jqGrid('columnChooser',{
                done: function(perm, cols){
                    dynamicColumns(cols,dynamicDefalutValue);
                    //cols页面获取隐藏的列,页面表格的值
                    $("#grid-table").jqGrid( 'setGridWidth', $(window).width()-$("#sidebar").width() -7);
                }
            });
        }
    });



// $(window).on('resize.jqGrid', function () {
//     //$("#grid-table").jqGrid( 'setGridWidth', $(window).width()-$("#sidebar").width() -7);
//     $("#grid-table").jqGrid( 'setGridWidth', 100);
//     //$("#grid-table").jqGrid( 'setGridHeight', ($(window).height()-$("#table_toolbar").outerHeight(true)- $("#grid-pager").outerHeight(true)-$("#user-nav").height()-$("#breadcrumb").height()-$(".ui-jqgrid-labels").height()-35 ) );
//     $("#grid-table").jqGrid( 'setGridHeight', 100 );
//
// });
//
// $(window).triggerHandler('resize.jqGrid');


// //重新加载表格
// function gridReload()
// {
//     _grid.trigger("reloadGrid");  //重新加载表格
// }


/***
 * 更改权限
 * @param obj
 * @param roleId
 * @param qxName
 */
function changeQX(obj,roleId,qxName)
{
    var value = obj.checked ? "1" : "0";
    $.ajax({
        type : "POST",
        url : context_path + '/role/changeQX.do?tm=' + new Date().getTime(),
        data : {
            "roleId" : roleId,
            "qxName" : qxName,
            "qxValue" : value
        },
        dataType : 'json',
        cache : false,
        success : function(data)
        {
            if (!data) {
                Dialog.error("更新权限异常，请尝试重新操作或联系系统管理员！");
            }
        }
    });

}


/**
 * 生成编辑权限样式
 * @returns {String}
 */
function editQxF(cellvalue, options, rowObject) {
    var _editQx = rowObject.editQx;
    var checkInfo = (_editQx == 1) ? " checked='checked' " : "";
    return "<input id=\"id-button-borders\" " + checkInfo + " onclick=\"changeQX(this,"
        + rowObject.roleId + ",'EDIT_QX'"
        + ")\" type=\"checkbox\" class=\"ace ace-switch ace-switch-8\" /><span class=\"lbl middle\"></span>";
}
