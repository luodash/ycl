var printer_list_grid;
var $queryWindow;  //查询窗口对象
var printer_list_dynamicDefalutValue="60cce761390a4e51bc4636271b52ab94";
var printer_list_queryFormData = iTsai.form.serialize($("#printer_list_hiddenQueryForm"));

$(function  (){
	$(".toggle_tools").click();
});

$("input").keypress(function (e) {
	if (e.which === 13) {
		printer_list_openQueryPage();
	}
});

printer_list_grid = jQuery("#printer_list_grid-table").jqGrid({
	url : context_path + '/printer/list.do',
    datatype : "json",
    colNames : [ 'ID','单据类型','层级','代码','打印机' ],
    colModel : [ 
		{name : 'id',index : 't1.ID',hidden : true},
		{name : 'typeStr',index : 't1.TYPE',width : 260},
		{name : 'hierarchyStr',index : 't1.HIERARCHY',width : 260},
		{name : 'code',index : 't1.CODE',width : 260},
		{name : 'printerStr',index : 't1.PRINTER',width : 200}
    ],
    rowNum : 20,
    rowList : [ 10, 20, 30 ],
    pager : '#printer_list_grid-pager',
    sortname : 'id',
    sortorder : "desc",
    altRows: true,
    viewrecords : true,
    hidegrid:false,
	autowidth:false,
	multiselect:true,
    multiboxonly: true,
    beforeRequest:function (){
        dynamicGetColumns(printer_list_dynamicDefalutValue,"printer_list_grid-table",$(window).width()-$("#sidebar").width() -7);
        //重新加载列属性
    },
    loadComplete : function(data){
        var table = this;
        setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
    },
    emptyrecords: "没有相关记录",
    loadtext: "加载中...",
    pgtext : "页码 {0} / {1}页",
    recordtext: "显示 {0} - {1}共{2}条数据"
});

jQuery("#printer_list_grid-table").navGrid('#printer_list_grid-pager', {edit:false,add:false,del:false,search:false,refresh:false})
.navButtonAdd('#printer_list_grid-pager',{
	caption:"",   
	buttonicon:"fa fa-refresh green",   
	onClickButton: function(){   
		$("#printer_list_grid-table").jqGrid('setGridParam', {
			postData: {queryJsonString:""} //发送数据 
		}).trigger("reloadGrid");
	}
}).navButtonAdd('#printer_list_grid-pager',{
	  caption: "",
	  buttonicon:"fa  icon-cogs",   
      onClickButton : function (){
    	  jQuery("#printer_list_grid-table").jqGrid('columnChooser',{
    		  done: function(perm, cols){
                  dynamicColumns(cols,printer_list_dynamicDefalutValue);
    			  $("#printer_list_grid-table").jqGrid( 'setGridWidth', $(window).width()-$("#sidebar").width() -7);
    		  }
    	  });
      }
});

$(window).on("resize.jqGrid", function () {
	$("#printer_list_grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);
	$("#printer_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-$(".query_box").outerHeight(true)-
	$("#printer_list_yy").outerHeight(true)-$("#printer_list_fixed_tool_div").outerHeight(true)-10-
	$("#printer_list_grid-pager").outerHeight(true)-$("#gview_printer_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
});
$(window).triggerHandler('resize.jqGrid');

/**
 * 查询功能:获取查询页面中的值，并将值放入列表页面中隐藏的form
 * @param jsonParam     查询页面传递过来的json对象
 */
function printer_list_queryLogListByParam(jsonParam){
	//将json对象转换成json字符串
	var queryJsonString = JSON.stringify(jsonParam);
	//执行查询操作
	$("#printer_list_grid-table").jqGrid('setGridParam', {
		postData: {queryJsonString:queryJsonString} //发送数据 
	}).trigger("reloadGrid");
}

/**打开添加页面*/
function printer_list_openAddPage(){
	$.post(context_path+'/printer/printer_edit', {
		id:-1
	}, function(str){
		$queryWindow = layer.open({
			title : "新增打印机设置",
			type: 1,
			skin : "layui-layer-molv",
			area : [800+"px",400+"px"],
			shade: 0.6, //遮罩透明度
			moveType: 1, //拖拽风格，0是默认，1是传统拖动
			content: str,//注意，如果str是object，那么需要字符拼接。
			success:function(layero, index){
				layer.closeAll('loading');
			}
		});
	}).error(function() {
		layer.closeAll();
		layer.msg('加载失败！',{icon:2});
	});
}

/**打开编辑页面*/
function printer_list_openEditPage(){
	var selectAmount = getGridCheckedNum("#printer_list_grid-table");
	if(selectAmount==0){
		layer.msg("请选择一条记录！",{icon:2});
		return;
	}else if(selectAmount>1){
		layer.msg("只能选择一条记录！",{icon:8});
		return;
	}

	layer.load(2);
	$.post(context_path+'/printer/printer_edit', {
		id:$('#printer_list_grid-table').jqGrid('getGridParam','selrow')
	}, function(str){
		$queryWindow = layer.open({
		    title : "修改打印机设置",
	    	type: 1,
	    	skin : "layui-layer-molv",
	    	area : [800+"px",400+"px"],
	    	shade: 0.6, //遮罩透明度
    	    moveType: 1, //拖拽风格，0是默认，1是传统拖动
	    	content: str,//注意，如果str是object，那么需要字符拼接。
	    	success:function(layero, index){
	    		layer.closeAll('loading');
	    	}
		});
	}).error(function() {
		layer.closeAll();
		layer.msg('加载失败！',{icon:2});
	});
}

/**
 * 查询按钮点击事件
 */
function printer_list_openQueryPage(){
	var queryParam = iTsai.form.serialize($('#printer_list_query_form'));
	printer_list_queryLogListByParam(queryParam);
}

/**重置*/
function printer_list_reset(){
	$("#printer_list_query_form #printer_list_hierarchy").select2("val","");
	$("#printer_list_query_form #printer_list_type").select2("val","");
	$("#printer_list_query_form #printer_list_printer").select2("val","");
	iTsai.form.deserialize($("#printer_list_query_form"),printer_list_queryFormData);
    printer_list_openQueryPage();
}

//层级
$("#printer_list_query_form #printer_list_hierarchy").select2({
	placeholder: "选择层级",
	minimumInputLength:0,   //至少输入n个字符，才去加载数据
	allowClear: true,  //是否允许用户清除文本信息
	delay: 250,
	formatNoMatches:"没有结果",
	formatSearching:"搜索中...",
	formatAjaxError:"加载出错啦！",
	ajax : {
		url: context_path+"/BaseDicType/getKindList",
		type:"POST",
		dataType : 'json',
		delay : 250,
		data: function (term,pageNo) {     //在查询时向服务器端传输的数据
			term = $.trim(term);
			return {
				queryString: term,    //联动查询的字符
				pageSize: 15,    //一次性加载的数据条数
				pageNo:pageNo,    //页码
				description:'HIERARCHY'
			}
		},
		results: function (data,pageNo) {
			var res = data.result;
			if(res.length>0){   //如果没有查询到数据，将会返回空串
				var more = (pageNo*15)<data.total; //用来判断是否还有更多数据可以加载
				return {
					results:res,more:more
				};
			}else{
				return {
					results:{}
				};
			}
		},
		cache : true
	}
});

//打印单据类型
$("#printer_list_query_form #printer_list_type").select2({
	placeholder: "选择层级",
	minimumInputLength:0,   //至少输入n个字符，才去加载数据
	allowClear: true,  //是否允许用户清除文本信息
	delay: 250,
	formatNoMatches:"没有结果",
	formatSearching:"搜索中...",
	formatAjaxError:"加载出错啦！",
	ajax : {
		url: context_path+"/BaseDicType/getKindList",
		type:"POST",
		dataType : 'json',
		delay : 250,
		data: function (term,pageNo) {     //在查询时向服务器端传输的数据
			term = $.trim(term);
			return {
				queryString: term,    //联动查询的字符
				pageSize: 15,    //一次性加载的数据条数
				pageNo:pageNo,    //页码
				description:'CIRCULATION_SHEET'
			}
		},
		results: function (data,pageNo) {
			var res = data.result;
			if(res.length>0){   //如果没有查询到数据，将会返回空串
				var more = (pageNo*15)<data.total; //用来判断是否还有更多数据可以加载
				return {
					results:res,more:more
				};
			}else{
				return {
					results:{}
				};
			}
		},
		cache : true
	}
});

//打印机编码
$("#printer_list_query_form #printer_list_printer").select2({
	placeholder: "选择层级",
	minimumInputLength:0,   //至少输入n个字符，才去加载数据
	allowClear: true,  //是否允许用户清除文本信息
	delay: 250,
	formatNoMatches:"没有结果",
	formatSearching:"搜索中...",
	formatAjaxError:"加载出错啦！",
	ajax : {
		url: context_path+"/BaseDicType/getKindList",
		type:"POST",
		dataType : 'json',
		delay : 250,
		data: function (term,pageNo) {     //在查询时向服务器端传输的数据
			term = $.trim(term);
			return {
				queryString: term,    //联动查询的字符
				pageSize: 15,    //一次性加载的数据条数
				pageNo:pageNo,    //页码
				description:'PRINTER'
			}
		},
		results: function (data,pageNo) {
			var res = data.result;
			if(res.length>0){   //如果没有查询到数据，将会返回空串
				var more = (pageNo*15)<data.total; //用来判断是否还有更多数据可以加载
				return {
					results:res,more:more
				};
			}else{
				return {
					results:{}
				};
			}
		},
		cache : true
	}
});