/**查询的方法**/
function moveAlloinfo_list_queryInstoreListByParam(jsonParam){
    iTsai.form.deserialize($('#moveAlloinfo_list_hiddenQueryForm'),jsonParam);   //将json对象反序列化到列表页面中隐藏的form中
    var queryParam = iTsai.form.serialize($('#moveAlloinfo_list_hiddenQueryForm'));
    var queryJsonString = JSON.stringify(queryParam);         //将json对象转换成json字符串
    //执行查询操作
    $("#moveAlloinfo_list_grid-table").jqGrid('setGridParam',
        {
            postData: {queryJsonString:queryJsonString} //发送数据
        }
    ).trigger("reloadGrid");
}

/***点击查看**/
function moveAlloinfo_list_openInfoPage(){
    var selectAmount = getGridCheckedNum("#moveAlloinfo_list_grid-table");
    if(selectAmount==0){
        layer.msg("请选择一条记录！",{icon:2});
        return;
    }else if(selectAmount>1){
        layer.msg("只能选择一条记录！",{icon:8});
        return;
    }
    layer.load(2);

    $.post(context_path + "/moveAllo/toInfo.do", {
        id:jQuery("#moveAlloinfo_list_grid-table").jqGrid("getGridParam", "selrow")
    }, function (str){
        $queryWindow=layer.open({
            title : "查看列表",
            type:1,
            skin : "layui-layer-molv",
            area : "1400px",
            shade : 0.6, //遮罩透明度
            moveType : 1, //拖拽风格，0是默认，1是传统拖动
            anim : 2,
            content : str,
            success: function (layero, index) {
                layer.closeAll('loading');
            }
        });
    });
}

/**打开新增页面**/
function moveAlloinfo_list_openAddPage(){
    layer.load(2);
    $.post(context_path+'/moveAllo/toAdd.do', {}, function(str){
        $queryWindow = layer.open({
            title : "添加",
            type: 1,
            skin : "layui-layer-molv",
            area : [window.screen.width-150+"px",document.body.clientHeight-150+"px"],
            shade: 0.6, //遮罩透明度
            moveType: 1, //拖拽风格，0是默认，1是传统拖动
            content: str,//注意，如果str是object，那么需要字符拼接。
            success:function(layero, index){
                layer.closeAll("loading");
            }
        });
    }).error(function() {
        layer.closeAll();
        layer.msg("加载失败！",{icon:2});
    });
}

/**打开编辑页面**/
function moveAlloinfo_list_openEditPage(){
    var selectAmount = getGridCheckedNum("#moveAlloinfo_list_grid-table");
    if(selectAmount==0){
        layer.msg("请选择一条记录！",{icon:2});
        return;
    }else if(selectAmount>1){
        layer.msg("只能选择一条记录！",{icon:8});
        return;
    }

    var rowDataState = jQuery("#moveAlloinfo_list_grid-table").jqGrid('getRowData',
        jQuery("#moveAlloinfo_list_grid-table").jqGrid('getGridParam', 'selrow')).alloState;
    if (rowDataState.indexOf("新建")==-1) {
        layer.msg("该调拨单已经开始，不能编辑！",{icon:8});
        return;
    }
    layer.load(2);
    $.post(context_path+'/moveAllo/editInfo.do', {
        id:jQuery("#moveAlloinfo_list_grid-table").jqGrid('getGridParam', 'selrow')
    }, function(str){
        $queryWindow = layer.open({
            title : "编辑",
            type: 1,
            skin : "layui-layer-molv",
            area : [window.screen.width-150+"px",document.body.clientHeight-150+"px"],
            shade: 0.6, //遮罩透明度
            moveType: 1, //拖拽风格，0是默认，1是传统拖动
            content: str,//注意，如果str是object，那么需要字符拼接。
            success:function(layero, index){
                layer.closeAll("loading");
            }
        });
    }).error(function() {
        layer.closeAll();
        layer.msg("加载失败！",{icon:2});
    });
}

/**导出**/
function moveAlloinfo_list_toExcel(){
    $("#moveAlloinfo_list_hiddenForm #moveAlloinfo_list_ids").val(jQuery("#moveAlloinfo_list_grid-table").jqGrid("getGridParam", "selarrrow"));
    $("#moveAlloinfo_list_hiddenForm #moveAlloinfo_list_queryAllCode").val($("#moveAlloinfo_list_queryForm #moveAlloinfo_list_allCode").val());
    $("#moveAlloinfo_list_hiddenForm #moveAlloinfo_list_queryFactoryCodeOut").val($("#moveAlloinfo_list_queryForm #moveAlloinfo_list_factoryCodeOut").val());
    $("#moveAlloinfo_list_hiddenForm #moveAlloinfo_list_queryWarehouseOut").val($("#moveAlloinfo_list_queryForm #moveAlloinfo_list_warehouseOut").val());
    $("#moveAlloinfo_list_hiddenForm #moveAlloinfo_list_queryFactoryCodeIn").val($("#moveAlloinfo_list_queryForm #moveAlloinfo_list_factoryCodeIn").val());
    $("#moveAlloinfo_list_hiddenForm #moveAlloinfo_list_queryWarehouseIn").val($("#moveAlloinfo_list_queryForm #moveAlloinfo_list_warehouseIn").val());
    $("#moveAlloinfo_list_hiddenForm #moveAlloinfo_list_queryStartTime").val($("#moveAlloinfo_list_queryForm #moveAlloinfo_list_startTime").val());
    $("#moveAlloinfo_list_hiddenForm #moveAlloinfo_list_queryEndTime").val($("#moveAlloinfo_list_queryForm #moveAlloinfo_list_endTime").val());
    $("#moveAlloinfo_list_hiddenForm #moveAlloinfo_list_queryCreater").val($("#moveAlloinfo_list_queryForm #moveAlloinfo_list_creater").val());
    $("#moveAlloinfo_list_hiddenForm #moveAlloinfo_list_queryExportExcelIndex").val(moveAlloinfo_list_exportExcelIndex);
    $("#moveAlloinfo_list_hiddenForm").submit();
}

/**刷新列表**/
function moveAlloinfo_list_gridReload(){
    $("#moveAlloinfo_list_grid-table").jqGrid('setGridParam',
        {
            url : context_path + '/moveAllo/alloctionPageList.do',
            postData: {queryJsonString:""} //发送数据  :选中的节点
        }
    ).trigger("reloadGrid");
}

/**根据id删除**/
function moveAlloinfo_list_deleteByIds(){
    var checkedNum = getGridCheckedNum("#moveAlloinfo_list_grid-table","id");  //选中的数量
    if(checkedNum==0){
        layer.alert("请至少选择一条记录！");
    }else{
        var rowDataState = jQuery("#moveAlloinfo_list_grid-table").jqGrid('getRowData',
            jQuery("#moveAlloinfo_list_grid-table").jqGrid('getGridParam', 'selrow')).alloState;
        if (rowDataState.indexOf("新建")==-1) {
            layer.msg("该调拨单已经开始，不能删除！",{icon:8});
            return;
        }
        layer.confirm("确定删除选择的调拨单数据",function(){
            $.ajax({
                type:"POST",
                url:context_path + "/moveAllo/deleteByIds?ids="+jQuery("#moveAlloinfo_list_grid-table").jqGrid('getGridParam', 'selarrrow'),
                dataType:"json",
                success:function(data){
                    if(data.result){
                        layer.msg(data.msg);
                        moveAlloinfo_list_gridReload();   //重新加载详情列表
                    }else{
                        layer.msg(data.msg);
                    }
                }
            })
        });
    }
}