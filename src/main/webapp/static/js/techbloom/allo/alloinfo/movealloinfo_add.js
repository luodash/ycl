
//重新加载详情列表
function reloadDetailTableList(){
    $("#movealloinfo_add_grid-table-c").jqGrid('setGridParam', {
        url:context_path + "/moveAllo/detailList.do?id="+$("#movealloinfo_add_id").val(),
    }).trigger("reloadGrid");
}

//删除详情中的数据
function delDetail(){
    var checkedNum = getGridCheckedNum("#movealloinfo_add_grid-table-c","id");  //选中的数量
    if(checkedNum==0){
        layer.alert("请选择一条记录！");
    }else{
        layer.confirm("确定删除选择的详情数据",function(){
            $.ajax({
                type:"POST",
                url:context_path+"/moveAllo/deleteAllocationDetailIds?ids="+jQuery("#movealloinfo_add_grid-table-c").jqGrid('getGridParam','selarrrow'),
                dataType:"json",
                success:function(data){
                    if(data.result){
                        layer.msg(data.msg, {icon: 1,time:1000});
                        reloadDetailTableList()  //重新加载详情列表
                        movealloinfo_list_gridReload();
                    }else{
                        layer.msg(data.msg);
                    }
                }
            })
        });
    }
}

/**刷新列表**/
function movealloinfo_list_gridReload(){
    $("#moveAlloinfo_list_grid-table").jqGrid('setGridParam', {
        url : context_path + '/moveAllo/alloctionPageList.do',
        postData: {queryJsonString:""} //发送数据  :选中的节点
    }).trigger("reloadGrid");
}

/**选择质保号*/
$("#movealloinfo_add_qaCode").select2({
    placeholder : "请选择质保单号",//文本框的提示信息
    minimumInputLength : 0, //至少输入n个字符，才去加载数据
    allowClear : true, //是否允许用户清除文本信息
    multiple: true,
    closeOnSelect:false,
    formatNoMatches: "没有结果",
    formatSearching: "搜索中...",
    formatAjaxError: "加载出错啦！",
    ajax : {
        url : context_path + '/moveAllo/getQaCode',
        dataType : 'json',
        delay : 250,
        data : function(term, pageNo) { //在查询时向服务器端传输的数据
            term = $.trim(term);
            selectParam = term;
            return {
                queryString : term, //联动查询的字符
                warehouseId:$("#movealloinfo_add_baseInfor #movealloinfo_add_outWarehouseId").val(),    //调出仓库主键
                factoryCode:$("#movealloinfo_add_baseInfor #movealloinfo_add_factoryCode").val(),   //调出厂区编码
                pageSize : 15, //一次性加载的数据条数
                pageNo : pageNo //页码
            }
        },
        results : function(data, pageNo) {
            if($('#movealloinfo_add_baseInfor').valid()) {
                var res = data.result;
                /*if ($("#movealloinfo_add_inWarehouseId").val() == $("#movealloinfo_add_outWarehouseId").val()) {
                    layer.alert("请选择不同的调入仓库与调出仓库！")
                } else {*/
                //如果没有查询到数据，将会返回空串
                if (res.length > 0) {
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {
                            "id": "0",
                            "text": "没有更多结果"
                        }
                    };
                }
                /*}*/
            }else {
                layer.alert("请选择完整的出入厂区和仓库！")
            }
        },
        cache : true
    }
});

$("#movealloinfo_add_qaCode").on("change",function(e){
    var datas = $("#movealloinfo_add_qaCode").select2("val");
    selectData = datas;
    var selectSize = datas.length;
    if(selectSize>1){
        var $tags = $("#movealloinfo_add_qaCode .select2-choices");
        //$("#s2id_materialInfor").html(selectSize+"个被选中");
        var $choicelist = $tags.find(".select2-search-choice");
        var $clonedChoice = $choicelist[0];
        $tags.children(".select2-search-choice").remove();
        $tags.prepend($clonedChoice);
        $tags.find(".select2-search-choice").find("div").html(selectSize+"个被选中");
        $tags.find(".select2-search-choice").find("a").removeAttr("tabindex");
        $tags.find(".select2-search-choice").find("a").attr("href","#");
        $tags.find(".select2-search-choice").find("a").attr("onclick","removeChoice();");
    }
    //执行select的查询方法
    $("#movealloinfo_add_qaCode").select2("search",selectParam);
});

//添加质保单号
function addDetail(){
    if(selectData!=0){
        //将选中的物料添加到数据库中
        $.ajax({
            type:"POST",
            url:context_path + "/moveAllo/saveAlloctionDetail",
            data:{
                qaCodeIds:selectData.toString(),    //主键
                factoryCode:$("#movealloinfo_add_baseInfor #movealloinfo_add_factoryCode").val(),
                outWarehouseId:$("#movealloinfo_add_baseInfor #movealloinfo_add_outWarehouseId").val(),
                factoryCodeIn:$("#movealloinfo_add_baseInfor #movealloinfo_add_factoryCodeIn").val(),
                inWarehouseId:$("#movealloinfo_add_baseInfor #movealloinfo_add_inWarehouseId").val(),
                type:'add',
                allCode:allCode
            },
            dataType:"json",
            success:function(data){
                removeChoice();   //清空下拉框中的值
                if(data.result!=null){
                    allCode = data.allCode;
                    layer.msg(data.msg, {icon: 1,time:1000});
                    $("#movealloinfo_add_grid-table-c").jqGrid('setGridParam', {
                        url:context_path + "/moveAllo/detailList.do?id=" + data.id
                    }).trigger("reloadGrid");
                    movealloinfo_list_gridReload();
                    $("#movealloinfo_add_baseInfor #movealloinfo_add_id").val(data.id);
                }else{
                    layer.alert("添加失败");
                }
            }
        });
    }else{
        layer.alert("请选择线盘！");
    }
}

$("#movealloinfo_add_baseInfor #movealloinfo_add_factoryCode").select2({
    placeholder: "选择厂区",
    minimumInputLength: 0, //至少输入n个字符，才去加载数据
    allowClear: true, //是否允许用户清除文本信息
    delay: 250,
    formatNoMatches: "没有结果",
    formatSearching: "搜索中...",
    formatAjaxError: "加载出错啦！",
    ajax: {
        url: context_path + "/factoryArea/getFactoryList",
        type: "POST",
        dataType: 'json',
        delay: 250,
        data: function (term, pageNo) { //在查询时向服务器端传输的数据
            term = $.trim(term);
            return {
                queryString: term, //联动查询的字符
                pageSize: 15, //一次性加载的数据条数
                pageNo: pageNo //页码
            }
        },
        results: function (data, pageNo) {
            var res = data.result;
            if (res.length > 0) { //如果没有查询到数据，将会返回空串
                var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                return {
                    results: res,
                    more: more
                };
            } else {
                return {
                    results: {}
                };
            }
        },
        cache: true
    }
});

$("#movealloinfo_add_baseInfor #movealloinfo_add_factoryCodeIn").select2({
    placeholder: "选择厂区",
    minimumInputLength: 0, //至少输入n个字符，才去加载数据
    allowClear: true, //是否允许用户清除文本信息
    delay: 250,
    formatNoMatches: "没有结果",
    formatSearching: "搜索中...",
    formatAjaxError: "加载出错啦！",
    ajax: {
        url: context_path + "/factoryArea/getFactoryList",
        type: "POST",
        dataType: 'json',
        delay: 250,
        data: function (term, pageNo) { //在查询时向服务器端传输的数据
            term = $.trim(term);
            return {
                queryString: term, //联动查询的字符
                pageSize: 15, //一次性加载的数据条数
                pageNo: pageNo //页码
            }
        },
        results: function (data, pageNo) {
            var res = data.result;
            if (res.length > 0) { //如果没有查询到数据，将会返回空串
                var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                return {
                    results: res,
                    more: more
                };
            } else {
                return {
                    results: {}
                };
            }
        },
        cache: true
    }
});

$("#movealloinfo_add_baseInfor #movealloinfo_add_inWarehouseId").select2({
    placeholder: "选择调出仓库",
    minimumInputLength: 0, //至少输入n个字符，才去加载数据
    allowClear: true, //是否允许用户清除文本信息
    delay: 250,
    formatNoMatches: "没有结果",
    formatSearching: "搜索中...",
    formatAjaxError: "加载出错啦！",
    ajax: {
        url: context_path + "/car/selectWarehouse",
        type: "POST",
        dataType: 'json',
        delay: 250,
        data: function (term, pageNo) { //在查询时向服务器端传输的数据
            term = $.trim(term);
            return {
                queryString: term, //联动查询的字符
                factoryCodeId:factorySelectIn,
                pageSize: 15, //一次性加载的数据条数
                pageNo: pageNo //页码
            }
        },
        results: function (data, pageNo) {
            var res = data.result;
            if (res.length > 0) { //如果没有查询到数据，将会返回空串
                var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                return {
                    results: res,
                    more: more
                };
            } else {
                return {
                    results: {}
                };
            }
        },
        cache: true
    }
});

$("#movealloinfo_add_baseInfor #movealloinfo_add_outWarehouseId").select2({
    placeholder: "选择调入仓库",
    minimumInputLength: 0, //至少输入n个字符，才去加载数据
    allowClear: true, //是否允许用户清除文本信息
    delay: 250,
    formatNoMatches: "没有结果",
    formatSearching: "搜索中...",
    formatAjaxError: "加载出错啦！",
    ajax: {
        url: context_path + "/car/selectWarehouse",
        type: "POST",
        dataType: 'json',
        delay: 250,
        data: function (term, pageNo) { //在查询时向服务器端传输的数据
            term = $.trim(term);
            return {
                queryString: term, //联动查询的字符
                pageSize: 15, //一次性加载的数据条数
                factoryCodeId:factorySelect,
                pageNo: pageNo //页码
            }
        },
        results: function (data, pageNo) {
            var res = data.result;
            if (res.length > 0) { //如果没有查询到数据，将会返回空串
                var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                return {
                    results: res,
                    more: more
                };
            } else {
                return {
                    results: {}
                };
            }
        },
        cache: true
    }
});

$("#movealloinfo_add_baseInfor #movealloinfo_add_factoryCode").on("change",function(e){
    $("#movealloinfo_add_baseInfor #movealloinfo_add_outWarehouseId").empty();
    factorySelect = $("#movealloinfo_add_baseInfor #movealloinfo_add_factoryCode").val();
    $.ajax({
        type:"POST",
        url:context_path + "/warehouselist/getFirstWarehouse.do",
        data:{
            factoryCode:factorySelect
        },
        dataType:"json",
        success:function(data){
            $("#movealloinfo_add_baseInfor #movealloinfo_add_outWarehouseId").select2("data", {
                id: data.id,
                text: data.warehouseCodeName
            });
        }
    });
});

$("#movealloinfo_add_baseInfor #movealloinfo_add_factoryCodeIn").on("change",function(e){
    $("#movealloinfo_add_baseInfor #movealloinfo_add_inWarehouseId").empty();
    factorySelectIn = $("#movealloinfo_add_baseInfor #movealloinfo_add_factoryCodeIn").val();
    $.ajax({
        type:"POST",
        url:context_path + "/warehouselist/getFirstWarehouse.do",
        data:{
            factoryCode:factorySelectIn
        },
        dataType:"json",
        success:function(data){
            $("#movealloinfo_add_baseInfor #movealloinfo_add_inWarehouseId").select2("data", {
                id: data.id,
                text: data.warehouseCodeName
            });
        }
    });
});