//保存按钮
function saveFormInfo(formdata){
    $("#alloinfo_edit_formSave").attr("disabled","disabled");
    $(window).triggerHandler('resize.jqGrid');
    $.ajax({
        type:"POST",
        url:context_path + "/allotCon/saveAllocation",
        data:formdata,
        dataType:"json",
        success:function(data){
            if(data.result){
                reloadDetailTableList();   //重新加载详情列表
                layer.alert("调拨单保存成功");
                $("#alloinfo_edit_formSave").removeAttr("disabled");
                alloinfo_list_gridReload();
            }else{
                layer.alert("调拨单保存失败");
            }
        }
    })
}

//重新加载详情列表
function reloadDetailTableList(){
    $("#alloinfo_edit_grid-table-c").jqGrid('setGridParam',
        {
            url:context_path + "/allotCon/detailList.do?id="+$("#alloinfo_edit_id").val(),
        }
    ).trigger("reloadGrid");
}

//提交
function outformSubmitBtn(){
    if($('#alloinfo_edit_baseInfor #alloinfo_edit_id').val()==-1){
        layer.alert("请先保存单据");
        return;
    }else {
        //后台修改对应的数据
        layer.confirm("确定提交,提交之后数据不能修改", function () {
            $.ajax({
                type:"POST",
                url:context_path+"/allotCon/subAlloction",
                dataType:"json",
                data:{
                    id:$('#alloinfo_edit_baseInfor #alloinfo_edit_id').val()
                },
                data:$('#alloinfo_edit_baseInfor').serialize(),
                success:function(data){
                    if(data.result){
                        layer.closeAll();
                        layer.msg("提交成功",{icon:1,time:1200});
                        alloinfo_list_gridReload();
                    }else{
                        layer.alert(data.msg);
                    }
                }
            })
        });
    }
}

//删除详情中的数据
function delDetail(){
    var checkedNum = getGridCheckedNum("#alloinfo_edit_grid-table-c","id");  //选中的数量
    if(checkedNum==0){
        layer.alert("请选择一条记录！");
    }else{
        layer.confirm("确定删除选择的数据",function(){
            $.ajax({
                type:"POST",
                url:context_path + "/allotCon/deleteAllocationDetailIds?ids="+jQuery("#alloinfo_edit_grid-table-c").jqGrid('getGridParam', 'selarrrow'),
                dataType:"json",
                success:function(data){
                    if(data.result){
                        alloinfo_list_gridReload();
                        layer.msg(data.msg);
                        reloadDetailTableList();   //重新加载详情列表
                    }else{
                        layer.msg(data.msg);
                    }
                }
            })
        });
    }

}

/**刷新列表**/
function alloinfo_list_gridReload(){
    iTsai.form.deserialize($("#alloinfo_list_hiddenQueryForm"), iTsai.form.serialize($("#alloinfo_list_queryForm")));
    var queryJsonString = JSON.stringify(iTsai.form.serialize($("#alloinfo_list_hiddenQueryForm")));
    $("#alloinfo_list_grid-table").jqGrid('setGridParam',
        {
            url : context_path + '/allotCon/alloctionPageList.do',
            postData: {queryJsonString:queryJsonString} //发送数据  :选中的节点
        }
    ).trigger("reloadGrid");
}