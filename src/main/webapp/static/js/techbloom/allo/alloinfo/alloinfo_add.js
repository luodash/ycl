//保存按钮
function saveFormInfo(formdata){
    var id = $("#alloinfo_add_id").val();
    if(id!=-1){
        layer.confirm("确定修改,修改之后当前质保单号将会删除", function (){
            $("#alloinfo_add_formSave").attr("disabled","disabled");
            $(window).triggerHandler('resize.jqGrid');
            $.ajax({
                type:"POST",
                url:context_path + "/allotCon/saveAllocation",
                data:formdata,
                dataType:"json",
                success:function(data){
                    if(data.result){
                        reloadDetailTableList()
                        $("#alloinfo_add_id").val(data.allocationId)
                        layer.alert("调拨单保存成功");
                        $("#alloinfo_add_formSave").removeAttr("disabled");
                        alloinfo_list_gridReload();
                    }else{
                        layer.alert("调拨单保存失败");
                    }
                }
            })
        })
    }else{
        $("#alloinfo_add_formSave").attr("disabled","disabled");
        $(window).triggerHandler('resize.jqGrid');
        $.ajax({
            type:"POST",
            url:context_path + "/allotCon/saveAllocation",
            data:formdata,
            dataType:"json",
            success:function(data){
                if(data.result){
                    reloadDetailTableList()
                	$("#alloinfo_add_id").val(data.allocationId)
                    layer.alert("调拨单保存成功");
                    $("#alloinfo_add_formSave").removeAttr("disabled");
                    alloinfo_list_gridReload();
                }else{
                    layer.alert("调拨单保存失败");
                }
            }
        })
    }
}

//重新加载详情列表
function reloadDetailTableList(){
    $("#alloinfo_add_grid-table-c").jqGrid('setGridParam',
        {
            url:context_path + "/allotCon/detailList.do?id="+$("#alloinfo_add_id").val(),
        }
    ).trigger("reloadGrid");
}

//删除详情中的数据
function delDetail(){
    var checkedNum = getGridCheckedNum("#alloinfo_add_grid-table-c","id");  //选中的数量
    if(checkedNum==0){
        layer.alert("请选择一条记录！");
    }else{
        var ids = jQuery("#alloinfo_add_grid-table-c").jqGrid('getGridParam', 'selarrrow');
        layer.confirm("确定删除选择的详情数据",function(){
            $.ajax({
                type:"POST",
                url:context_path + "/allotCon/deleteAllocationDetailIds?ids="+ids,
                dataType:"json",
                success:function(data){
                    if(data.result){
                        layer.msg(data.msg, {icon: 1,time:1000});
                        reloadDetailTableList()  //重新加载详情列表
                        alloinfo_list_gridReload();
                    }else{
                        layer.msg(data.msg);
                    }
                }
            })
        });
    }
}

//提交
function outformSubmitBtn(){
    if($('#alloinfo_add_baseInfor #alloinfo_add_id').val()==-1){
        layer.alert("请先保存单据");
        return;
    }else {
        var formdata = $('#alloinfo_add_baseInfor').serialize();
        //后台修改对应的数据
        layer.confirm("确定提交,提交之后数据不能修改", function () {
            $.ajax({
                type:"POST",
                url:context_path+"/allotCon/subAlloction",
                dataType:"json",
                data:{id:$('#alloinfo_add_baseInfor #alloinfo_add_id').val()},
                data:formdata,
                success:function(data){
                    if(data.result){
                        layer.closeAll();
                        layer.msg("提交成功",{icon:1,time:1200});
                        alloinfo_list_gridReload();
                    }else{
                        layer.alert(data.msg);
                    }
                }
            })
        });
    }
}

/**刷新列表**/
function alloinfo_list_gridReload(){
    $("#alloinfo_list_grid-table").jqGrid('setGridParam',
        {
            url : context_path + '/allotCon/alloctionPageList.do',
            postData: {queryJsonString:""} //发送数据  :选中的节点
        }
    ).trigger("reloadGrid");
}