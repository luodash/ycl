/**绑定行车**/
function bind(id,carCode){
    if(carCode!='-1'){
        layer.alert("请勿重复绑定行车");
    }else{
        $.post(context_path + "/allostorageout/carbind.do?detailId="+id+"&infoId="+$("#allooutstorage_execute_id1").val(), {}, function (str){
            $queryWindow=layer.open({
                title : "行车绑定",
                type:1,
                skin : "layui-layer-molv",
                area : "600px",
                shade : 0.6, //遮罩透明度
                moveType : 1, //拖拽风格，0是默认，1是传统拖动
                anim : 2,
                content : str,
                success: function (layero, index) {
                    layer.closeAll('loading');
                }
            });
        });
    }
}

/**开始拣货**/
function detailStart(id){
    $.post(context_path + "/allostorageout/detailStart?id="+id, {}, function (str){
        if(str.result){
            layer.msg(str.msg,{icon:1,time:1200});
            reloadDetailTableList();
        }else{
            layer.alert(str.msg);
        }
    });
}


/**拣货完成**/
function detailSuccess(id){
    $.post(context_path + "/allostorageout/detailSuccess?id="+id, {}, function (str){
        iTsai.form.deserialize($("#allooutstorage_list_hiddenQueryForm"), iTsai.form.serialize($("#allooutstorage_list_queryForm")));
        var queryParam = iTsai.form.serialize($("#allooutstorage_list_hiddenQueryForm"));
        var queryJsonString = JSON.stringify(queryParam);
        if(str.result){
            layer.msg(str.msg,{icon:1,time:1200});
            reloadDetailTableList();
            $("#allooutstorage_list_grid-table").jqGrid("setGridParam",
                {
                    postData: {queryJsonString:queryJsonString} //发送数据
                }
            ).trigger("reloadGrid");
        }else{
            layer.alert(str.msg);
        }
    });
}

//重新加载详情列表
function reloadDetailTableList(){
    $("#allooutstorage_execute_grid-table-c").jqGrid('setGridParam',
        {
            url:context_path + "/allostorageout/detailList.do?id="+id,
        }
    ).trigger("reloadGrid");
}