/**查询的方法**/
function queryInstoreListByParam(jsonParam){
    //序列化表单：iTsai.form.serialize($('#frm'))
    //反序列化表单：iTsai.form.deserialize($('#frm'),json)
    iTsai.form.deserialize($('#allooutstorage_list_hiddenQueryForm'),jsonParam);   //将json对象反序列化到列表页面中隐藏的form中
    var queryParam = iTsai.form.serialize($('#allooutstorage_list_hiddenQueryForm'));
    var queryJsonString = JSON.stringify(queryParam);         //将json对象转换成json字符串
    //执行查询操作
    $("#allooutstorage_list_grid-table").jqGrid('setGridParam',
        {
            postData: {queryJsonString:queryJsonString} //发送数据
        }
    ).trigger("reloadGrid");
}

/**执行**/
function allooutstorage_list_execute(){
    var selectAmount = getGridCheckedNum("#allooutstorage_list_grid-table");
    if(selectAmount==0){
        layer.msg("请选择一条记录！",{icon:2});
        return;
    }else if(selectAmount>1){
        layer.msg("只能选择一条记录！",{icon:8});
        return;
    }
    var id = $("#allooutstorage_list_grid-table").jqGrid('getGridParam','selrow');
    var rowData = jQuery("#allooutstorage_list_grid-table").jqGrid('getRowData', id).state;
    $.post(context_path + "/allostorageout/execute.do?id="+id, {}, function (str){
        $queryWindow=layer.open({
            title : "出库执行",
            type:1,
            skin : "layui-layer-molv",
            area : "1400px",
            shade : 0.6, //遮罩透明度
            moveType : 1, //拖拽风格，0是默认，1是传统拖动
            anim : 2,
            content : str,
            success: function (layero, index) {
                layer.closeAll('loading');
            }
        });
    });
}

/***点击查看**/
function allooutstorage_list_openInfoPage(){
    var selectAmount = getGridCheckedNum("#allooutstorage_list_grid-table");
    if(selectAmount==0){
        layer.msg("请选择一条记录！",{icon:2});
        return;
    }else if(selectAmount>1){
        layer.msg("只能选择一条记录！",{icon:8});
        return;
    }
    layer.load(2);
    var selectid = jQuery("#allooutstorage_list_grid-table").jqGrid("getGridParam", "selrow");
    $.post(context_path + "/allostorageout/toInfo.do?id="+selectid, {}, function (str){
        $queryWindow=layer.open({
            title : "查看列表",
            type:1,
            skin : "layui-layer-molv",
            area : "1400px",
            shade : 0.6, //遮罩透明度
            moveType : 1, //拖拽风格，0是默认，1是传统拖动
            anim : 2,
            content : str,
            success: function (layero, index) {
                layer.closeAll('loading');
            }
        });
    });
}

/**打开编辑页面**/
function allooutstorage_list_openEditPage(){
    var selectAmount = getGridCheckedNum("#allooutstorage_list_grid-table");
    if(selectAmount==0){
        layer.msg("请选择一条记录！",{icon:2});
        return;
    }else if(selectAmount>1){
        layer.msg("只能选择一条记录！",{icon:8});
        return;
    }
    var id = jQuery("#allooutstorage_list_grid-table").jqGrid('getGridParam', 'selrow');
    var rowData = jQuery("#allooutstorage_list_grid-table").jqGrid('getRowData', id).state;
    layer.load(2);
    $.post(context_path+'/allostorageout/editInfo.do?id='+id, {}, function(str){
        $queryWindow = layer.open({
            title : "编辑",
            type: 1,
            skin : "layui-layer-molv",
            area : "900px",
            shade: 0.6, //遮罩透明度
            moveType: 1, //拖拽风格，0是默认，1是传统拖动
            content: str,//注意，如果str是object，那么需要字符拼接。
            success:function(layero, index){
                layer.closeAll("loading");
            }
        });
    }).error(function() {
        layer.closeAll();
        layer.msg("加载失败！",{icon:2});
    });
}

/**导出**/
function allooutstorage_list_toExcel(){
    $("#allooutstorage_list_hiddenForm #allooutstorage_list_ids").val(jQuery("#allooutstorage_list_grid-table").jqGrid("getGridParam", "selarrrow"));
    $("#allooutstorage_list_hiddenForm #allooutstorage_list_queryAllCode").val($("#allooutstorage_list_queryForm #allooutstorage_list_allCode").val());
    $("#allooutstorage_list_hiddenForm #allooutstorage_list_queryStartTime").val($("#allooutstorage_list_queryForm #allooutstorage_list_startTime").val());
    $("#allooutstorage_list_hiddenForm #allooutstorage_list_queryEndTime").val($("#allooutstorage_list_queryForm #allooutstorage_list_endTime").val());
    $("#allooutstorage_list_hiddenForm #allooutstorage_list_queryExportExcelIndex").val(allooutstorage_list_exportExcelIndex);
    $("#allooutstorage_list_hiddenForm").submit();
}

/**刷新列表**/
function gridReload(){
    $("#allooutstorage_list_grid-table").jqGrid('setGridParam',
        {
            url : context_path + '/allostorageout/storageListData.do',
            postData: {queryJsonString:""} //发送数据  :选中的节点
        }
    ).trigger("reloadGrid");
}