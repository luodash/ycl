//保存按钮
function saveFormInfo(formdata){
    $("#allooutstorage_edit_formSave").attr("disabled","disabled");
    $(window).triggerHandler('resize.jqGrid');
    $.ajax({
        type:"POST",
        url:context_path + "/allostorageout/saveOutStorage",
        data:formdata,
        dataType:"json",
        success:function(data){
            if(data.result){
                reloadDetailTableList();   //重新加载详情列表
                layer.alert("发货单保存成功");
                $("#allooutstorage_edit_formSave").removeAttr("disabled");
                gridReload();
            }else{
                layer.alert("发货单保存失败");
            }
        }
    })
}

//重新加载详情列表
function reloadDetailTableList(){
    $("#allooutstorage_edit_grid-table-c").jqGrid('setGridParam',
        {
            url:context_path + "/allostorageout/detailList.do?id="+$("#allooutstorage_edit_id").val(),
        }
    ).trigger("reloadGrid");
}

//提交
function outformSubmitBtn(){
    if($('#allooutstorage_edit_baseInfor #allooutstorage_edit_id').val()==-1){
        layer.alert("请先保存单据");
        return;
    }else {
        var formdata = $('#allooutstorage_edit_baseInfor').serialize();
        //后台修改对应的数据
        layer.confirm("确定提交,提交之后数据不能修改", function () {
            $.ajax({
                type:"POST",
                url:context_path+"/allostorageout/subOutStorage",
                dataType:"json",
                data:{id:$('#allooutstorage_edit_baseInfor #allooutstorage_edit_id').val()},
                data:formdata,
                success:function(data){
                    if(data.result){
                        layer.closeAll();
                        layer.msg("提交成功",{icon:1,time:1200});
                        gridReload();
                    }else{
                        layer.alert(data.msg);
                    }
                }
            })
        });
    }
}

//删除详情中的数据
function delDetail(){
    var checkedNum = getGridCheckedNum("#allooutstorage_edit_grid-table-c","id");  //选中的数量
    if(checkedNum==0){
        layer.alert("请选择一条记录！");
    }else{
        var ids = jQuery("#allooutstorage_edit_grid-table-c").jqGrid('getGridParam', 'selarrrow');
        layer.confirm("确定删除选择的出库详情",function(){
            $.ajax({
                type:"POST",
                url:context_path + "/allostorageout/delOutstorageDetail?ids="+ids,
                dataType:"json",
                success:function(data){
                    if(data.result){
                        layer.msg(data.msg);
                        reloadDetailTableList();   //重新加载详情列表
                    }else{
                        layer.msg(data.msg);
                    }
                }
            })
        });
    }

}