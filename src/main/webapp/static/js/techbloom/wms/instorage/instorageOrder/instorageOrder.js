/**
 * 新增入库单
 */
function addAllocateOrder(){
	$.get( context_path + "/instorageOrder/toAdd.do?id=-1").done(function(data){
		layer.open({
		    title : "入库单添加", 
	    	type:1,
	    	skin : "layui-layer-molv",
	    	area : ['780px', '620px'],
	    	shade : 0.6, //遮罩透明度
		    moveType : 1, //拖拽风格，0是默认，1是传统拖动
		    anim : 2,
		    //url : data.url,
		    content : data
	        /*url: context_path + "/role/toAdd.do",*/
	    	
		});
	});
}
/**
 * 编辑入库单
 */
function editAllocateOrder(){
	var checkedNum = getGridCheckedNum("#instorageOrder_list_grid-table","id");
	if(checkedNum == 0){
    	layer.alert("请选择一条要编辑的入库单！");
    	return false;
    } else if(checkedNum >1){
    	layer.alert("只能选择一条入库单进行编辑操作！");
    	return false;
    } else {
    	var saleId = jQuery("#instorageOrder_list_grid-table").jqGrid('getGridParam', 'selrow');
    	var rowDate = jQuery("#instorageOrder_list_grid-table").jqGrid('getRowData', saleId).state;
    	if(rowDate!='<span style="color:#d15b47;font-weight:bold;">未提交</span>'){
    		layer.msg("该单据不可修改",{time:1200,icon:2});
    		return ;
    	}
    	$.get( context_path + "/instorageOrder/toAdd.do?id="+saleId).done(function(data){
    	  layer.open({
    	  title : "入库单编辑", 
    	  type:1,
    	  skin : "layui-layer-molv",
    	  area : ['750px', '650px'],
    	  shade : 0.6, //遮罩透明度
    	  moveType : 1, //拖拽风格，0是默认，1是传统拖动
    	  anim : 2,
    	  content : data
    	  });
    	 });
    		
    }
}


/**
 * 删除入库单
 */
function delOutStorageOrder(){
	var checkedNum = getGridCheckedNum("#instorageOrder_list_grid-table","id");  //选中的数量
	if(checkedNum==0){
		layer.alert("请至少选择一条要删除的入库单！");
	}else{
		//从数据库中删除选中的入库单，并刷新入库单表格
		var ids = jQuery("#instorageOrder_list_grid-table").jqGrid('getGridParam', 'selarrrow');
		//弹出确认窗口
		layer.confirm("确定删除选中的入库单？", function() {
    		$.ajax({
    			type : "POST",
    			url:context_path + "/instorageOrder/delete?ids="+ids,
    			dataType : 'json',
    			cache : false,
    			success : function(data) {
    				layer.closeAll();
    				if(data.result){
    				
						layer.msg("入库单删除成功！", {icon: 1,time:1000});
    				}else{
    					layer.msg(data.msg, {icon: 2,time:1000});
    				}
    				_grid.trigger("reloadGrid");  //重新加载表格
    			}
    		});
    	});
	} 
}

//查看出库单详情
function viewOutstorage(){
	var checkedNum = getGridCheckedNum("#instorageOrder_list_grid-table","id");
	if(checkedNum == 0){
    	Dialog.alert("请选择一条要查看的出库单！");
    	return false;
    } else if(checkedNum >1){
    	Dialog.alert("只能选择一条出库单进行查看操作！");
    	return false;
    } else {
    	var Instorageid = jQuery("#instorageOrder_list_grid-table").jqGrid('getGridParam', 'selrow');
    	Dialog.open({
		    title: "出库单查看", 
		    width: 1000,  height: '800',
		    url: context_path + "/outStorage/viewOutStorageDetail?outStoreID="+Instorageid,
		    theme : "simple",
			drag : true
		});
    }
}

/**
 * 入库单查询功能:获取查询页面中的值，并将值放入列表页面中隐藏的form
 * @param jsonParam     查询页面传递过来的json对象
 */
function queryInstoreListByParam(jsonParam){
	iTsai.form.deserialize($('#instorageOrder_list_hiddenQueryForm'),jsonParam);   //将json对象反序列化到列表页面中隐藏的form中
	var queryParam = iTsai.form.serialize($('#instorageOrder_list_hiddenQueryForm'));
	var queryJsonString = JSON.stringify(queryParam);         //将json对象转换成json字符串
	//执行查询操作
	$("#instorageOrder_list_grid-table").jqGrid('setGridParam', 
			{
				postData: {queryJson:queryJsonString} //发送数据 
			}
	  ).trigger("reloadGrid");
}


/**
 * 显示提示窗口
 * @param msg   显示信息
 * @param delay 持续时间,结束之后窗口消失
 */
function showTipMsg(msg,delay){
	Dialog.tip( msg, {type: "loading", delay: delay} );
}

/** 表格刷新 */
function gridReload(){
	 _grid.trigger("reloadGrid");  //重新加载表格  
}

function viewDetailList(){
	var checkedNum = getGridCheckedNum("#instorageOrder_list_grid-table","id");
	if(checkedNum == 0){
    	layer.alert("请选择一条要查看的入库单！");
    	return false;
    } else if(checkedNum >1){
    	layer.alert("只能选择一条入库单进行查看操作！");
    	return false;
    } else {
    	var id = jQuery("#instorageOrder_list_grid-table").jqGrid('getGridParam', 'selrow');
    	$.get( context_path + "/instorageOrder/viewDetail?id="+id).done(function(data){
			layer.open({
			    title : "入库单查看", 
		    	type:1,
		    	skin : "layui-layer-molv",
		    	area : ['750px', '650px'],
		    	shade : 0.6, //遮罩透明度
			    moveType : 1, //拖拽风格，0是默认，1是传统拖动
			    anim : 2,
			    content : data
			});
		});  	   	
    }
}
