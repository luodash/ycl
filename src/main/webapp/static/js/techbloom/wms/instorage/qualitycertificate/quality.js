function add(){
    $.get( context_path + "/qualityCertificate/toAddQuaity?id=-1").done(function(data){
        layer.open({
            title : "质检单添加",
            type:1,
            skin : "layui-layer-molv",
            area : ['780px', '620px'],
            shade : 0.6, //遮罩透明度
            moveType : 1, //拖拽风格，0是默认，1是传统拖动
            anim : 2,
            content : data
        });
    });
}
function gridReload(){
    $("#qualitycertificate_list_grid-table").jqGrid('setGridParam',
        {
            url : context_path + '/qualityCertificate/list',
            postData: {queryJsonString:""} //发送数据  :选中的节点
        }
    ).trigger("reloadGrid");
}

function edit(){
    var checkedNum = getGridCheckedNum("#qualitycertificate_list_grid-table", "id");
    if (checkedNum == 0) {
        layer.alert("请选择一个要编辑的质检单！");
        return false;
    } else if (checkedNum > 1) {
        layer.alert("只能选择一个质检单进行编辑操作！");
        return false;
    }else {
        var id = jQuery("#qualitycertificate_list_grid-table").jqGrid('getGridParam', 'selrow');
        var rowDate = jQuery("#qualitycertificate_list_grid-table").jqGrid('getRowData', id).state;
        if (rowDate == '<span style="color:green;font-weight:bold;">质检完成</span>') {
            layer.alert("该单据已经质检完成，不可编辑");
            return false;
        }else{
            $.get( context_path + "/qualityCertificate/toAddQuaity?id="+id).done(function(data){
                layer.open({
                    title : "质检单编辑",
                    type:1,
                    skin : "layui-layer-molv",
                    area : ['780px', '620px'],
                    shade : 0.6, //遮罩透明度
                    moveType : 1, //拖拽风格，0是默认，1是传统拖动
                    anim : 2,
                    content : data
                });
            });
        }
    }
}
//删除质检单
function del(){
    var checkedNum = getGridCheckedNum("#qualitycertificate_list_grid-table", "id");  //选中的数量
    if (checkedNum == 0) {
        layer.alert("请选择一个要删除的质检单！");
    }else{
        var ids = jQuery("#qualitycertificate_list_grid-table").jqGrid('getGridParam', 'selarrrow');
        layer.confirm("确定删除选中的质检单?",function(){
            $.ajax({
                type: "POST",
                url: context_path + "/qualityCertificate/deleteQuaity?ids=" + ids,
                dataType: "json",
                success:function(data){
                    if (data.result) {
                        layer.closeAll();
                        //弹出提示信息
                        layer.msg(data.msg,{icon:1,time:1200});
                        $("#qualitycertificate_list_grid-table").jqGrid('setGridParam',
                            {
                                postData: {queryJsonString:""} //发送数据
                            }
                        ).trigger("reloadGrid");
                    }else{
                        layer.msg(data.msg,{icon:2,time:1200});
                    }
                }
            })
        });
    }
}
function queryInstoreListByParam(jsonParam){
    //序列化表单：iTsai.form.serialize($('#frm'))
    //反序列化表单：iTsai.form.deserialize($('#frm'),json)
    iTsai.form.deserialize($('#qualitycertificate_list_hiddenQueryForm'),jsonParam);   //将json对象反序列化到列表页面中隐藏的form中
    var queryParam = iTsai.form.serialize($('#qualitycertificate_list_hiddenQueryForm'));
    var queryJsonString = JSON.stringify(queryParam);         //将json对象转换成json字符串
    //执行查询操作
    $("#qualitycertificate_list_grid-table").jqGrid('setGridParam',
        {
            postData: {queryJsonString:queryJsonString} //发送数据
        }
    ).trigger("reloadGrid");
}

function exportQuality(){
    var selectid = jQuery("#qualitycertificate_list_grid-table").jqGrid("getGridParam", "selarrrow");;
    $("#qualitycertificate_list_ids").val(selectid);
    $("#qualitycertificate_list_hiddenQueryForm").submit();
}
function printQuality(){
    var checkedNum = getGridCheckedNum("#qualitycertificate_list_grid-table", "id");
    var id="";
    if (checkedNum == 0) {
        layer.alert("请选择要打印的质检单！");
        return false;
    }else{
        id = jQuery("#qualitycertificate_list_grid-table").jqGrid('getGridParam', 'selarrrow');
    }
    var url = context_path + "/qualityCertificate/printQuality?id=" + id;
    window.open(url);
}

//拆分
function spiltQuality(){
    var checkedNum = getGridCheckedNum("#qualitycertificate_list_grid-table", "id");
    if (checkedNum == 0) {
        layer.alert("请选择一个要拆分的质检单！");
        return false;
    } else if (checkedNum > 1) {
        layer.alert("只能选择一个质检单！");
        return false;
    }else{
        var id = jQuery("#qualitycertificate_list_grid-table").jqGrid('getGridParam', 'selrow');
        var rowDate = jQuery("#qualitycertificate_list_grid-table").jqGrid('getRowData', id).state;
        if(rowDate == '<span style="color:green;font-weight:bold;">质检完成</span>'){
            $.get( context_path + "/qualityCertificate/spiltQuality?id="+id).done(function(data){
                layer.open({
                    title : "质检单拆分",
                    type:1,
                    skin : "layui-layer-molv",
                    area : ['900px', '650px'],
                    shade : 0.6, //遮罩透明度
                    moveType : 1, //拖拽风格，0是默认，1是传统拖动
                    anim : 2,
                    content : data
                });
            });
        }else{
            layer.alert("只能选择已经质检完成的质检单进行拆分");
        }
    }
}

