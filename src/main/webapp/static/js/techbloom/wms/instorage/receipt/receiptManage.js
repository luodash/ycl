/**
 * 新增收货订单
 */
function addReceiptOrder(){
	$.get( context_path + "/ASNmanage/toAdd.do?id=-1").done(function(data){
		layer.open({
		    title : "收货订单添加", 
	    	type:1,
	    	skin : "layui-layer-molv",
	    	area : ['780px', '620px'],
	    	shade : 0.6, //遮罩透明度
		    moveType : 1, //拖拽风格，0是默认，1是传统拖动
		    anim : 2,
		    content : data
		});
	});
}
/**
 * 编辑收货订单
 */
function editAllocateOrder(){
	var checkedNum = getGridCheckedNum("#receiptmanage_list_grid-table","id");
	if(checkedNum == 0){
    	layer.alert("请选择一条要编辑的收货订单！");
    	return false;
    } else if(checkedNum >1){
    	layer.alert("只能选择一条收货订单进行编辑操作！");
    	return false;
    } else {
    	var saleId = jQuery("#receiptmanage_list_grid-table").jqGrid('getGridParam', 'selrow'); 
    	var rowDate = jQuery("#receiptmanage_list_grid-table").jqGrid('getRowData', saleId).state;
    	if(rowDate!='<span style="color:grey;font-weight:bold;">未提交</span>'){
    		layer.msg("该单据不可修改",{time:1200,icon:2});
    		return;
    	}
	$.get( context_path + "/ASNmanage/toAdd.do?id="+saleId).done(function(data){
    	  layer.open({
	    	  title : "收货订单编辑", 
	    	  type:1,
	    	  skin : "layui-layer-molv",
	    	  area : ['780px', '620px'],
	    	  shade : 0.6, //遮罩透明度
	    	  moveType : 1, //拖拽风格，0是默认，1是传统拖动
	    	  anim : 2,
	    	  content : data
    	  });
	 });
    		
    }
}
//收货单提交
function addReceiptOrder12(){
	$.get( context_path + "/ASNmanage/toAdd.do?id=-1").done(function(data){
		layer.open({
		    title : "收货单添加", 
	    	type:1,
	    	skin : "layui-layer-molv",
	    	area : ['780px', '620px'],
	    	shade : 0.6, //遮罩透明度
		    moveType : 1, //拖拽风格，0是默认，1是传统拖动
		    anim : 2,
		    content : data
		});
	});
}

/**
 * 删除收货订单
 */
function delOutStorageOrder(){
	var checkedNum = getGridCheckedNum("#receiptmanage_list_grid-table","id");  //选中的数量
	if(checkedNum==0){
		layer.alert("请至少选择一条要删除的收货单！");
	}else{
		//从数据库中删除选中的入库单，并刷新入库单表格
		var ids = jQuery("#receiptmanage_list_grid-table").jqGrid('getGridParam', 'selarrrow');
		//弹出确认窗口
		layer.confirm("确定删除选中的收货订单？", function() {
    		$.ajax({
    			type : "POST",
    			url:context_path + "/ASNmanage/delete?ids="+ids,
    			dataType : 'json',
    			cache : false,
    			success : function(data) {
    				layer.closeAll();
    				if(data.result){
						layer.msg("收货订单删除成功！", {icon: 1,time:1000});
    				}else{
    					layer.msg(data.msg, {icon: 2,time:1000});
    				}
    				_grid.trigger("reloadGrid");  //重新加载表格
    			}
    		});
    	});
	} 
}


/**
 * 打开查询界面
 */
function openOutstoreListSearchPage(){
	var queryBean = iTsai.form.serialize($('#receiptmanage_list_hiddenQueryForm'));   //获取form中的值：json对象
	var queryJsonString = JSON.stringify(queryBean);         //将json对象转换成json字符串
	Dialog.open({
	    title: "出库单条件查询", 
	    width: 600,  height: '400',
	    url: context_path + "/outStorage/toQueryPage?outstoreString="+queryJsonString,
	    theme : "simple",
		drag : true
	});
}


/**
 * 收货单查询功能:获取查询页面中的值，并将值放入列表页面中隐藏的form
 * @param jsonParam     查询页面传递过来的json对象
 */
function queryInstoreListByParam(jsonParam){
	//序列化表单：iTsai.form.serialize($('#frm'))
	//反序列化表单：iTsai.form.deserialize($('#frm'),json)
	iTsai.form.deserialize($('#receiptmanage_list_hiddenQueryForm'),jsonParam);   //将json对象反序列化到列表页面中隐藏的form中
	var queryParam = iTsai.form.serialize($('#receiptmanage_list_hiddenQueryForm'));
	var queryJsonString = JSON.stringify(queryParam);         //将json对象转换成json字符串
	//执行查询操作
	$("#receiptmanage_list_grid-table").jqGrid('setGridParam', 
			{
				postData: {queryJson:queryJsonString} //发送数据 
			}
	  ).trigger("reloadGrid");
}


/**
 * 显示提示窗口
 * @param msg   显示信息
 * @param delay 持续时间,结束之后窗口消失
 */
function showTipMsg(msg,delay){
	Dialog.tip( msg, {type: "loading", delay: delay} );
}

/** 表格刷新 */
function gridReload(){
	 _grid.trigger("reloadGrid");  //重新加载表格  
}
//查看
function showView(){
	var checkedNum = getGridCheckedNum("#receiptmanage_list_grid-table","id");
	if(checkedNum == 0){
    	layer.alert("请选择一条要查看的收货订单！");
    	return false;
    } else if(checkedNum >1){
    	layer.alert("只能选择一条收货订单进行查看！");
    	return false;
    } else {
    	var saleId = jQuery("#receiptmanage_list_grid-table").jqGrid('getGridParam', 'selrow'); 
    	$.get( context_path + "/ASNmanage/toView.do?id="+saleId).done(function(data){
    	  layer.open({
    	  title : "收货订单查看", 
    	  type:1,
    	  skin : "layui-layer-molv",
    	  area : ['750px', '650px'],
    	  shade : 0.6, //遮罩透明度
    	  moveType : 1, //拖拽风格，0是默认，1是传统拖动
    	  anim : 2,
    	  content : data
    	  });
    	 });
    }
}
function receiptSure(){
	var checkedNum = getGridCheckedNum("#receiptmanage_list_grid-table","id");
	if(checkedNum == 0){
    	layer.alert("请选择一条要收货的收货订单！");
    	return false;
    } else if(checkedNum >1){
    	layer.alert("只能选择一条收货订单进行收货操作！");
    	return false;
    } else {
    	var saleId = jQuery("#receiptmanage_list_grid-table").jqGrid('getGridParam', 'selrow'); 
    	var rowDate = jQuery("#receiptmanage_list_grid-table").jqGrid('getRowData', saleId).state;
    	if(rowDate!='<span style="color:#d15b47;font-weight:bold;">未收货</span>'){
    		layer.msg("该单据状态不可收货",{time:1200,icon:2});
    		return;
    	}
    	var saleId = jQuery("#receiptmanage_list_grid-table").jqGrid('getGridParam', 'selrow'); 
    	$.get( context_path + "/ASNmanage/toReceipt.do?id="+saleId).done(function(data){
    	  layer.open({
    	  title : "收货订单收货", 
    	  type:1,
    	  skin : "layui-layer-molv",
    	  area : "750px",
    	  shade : 0.6, //遮罩透明度
    	  moveType : 1, //拖拽风格，0是默认，1是传统拖动
    	  anim : 2,
    	  content : data
    	  });
      });   		
    }
}
