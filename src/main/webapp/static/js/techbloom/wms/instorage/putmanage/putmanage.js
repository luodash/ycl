//新增上架单
function addPutManage(){
	$.get( context_path + "/putManage/toAdd.do?id=-1").done(function(data){
		layer.open({
		    title : "上架单添加", 
	    	type:1,
	    	skin : "layui-layer-molv",
	    	area : ['780px', '620px'],
	    	shade : 0.6, //遮罩透明度
		    moveType : 1, //拖拽风格，0是默认，1是传统拖动
		    anim : 2,
		    content : data
	    	
		});
	});
}
//编辑上架单
function editPutManage(){
	var checkedNum = getGridCheckedNum("#putmanage_list_grid-table","id");
	if(checkedNum == 0){
    	layer.alert("请选择一条要编辑的上架单！");
    	return false;
    } else if(checkedNum >1){
    	layer.alert("只能选择一条上架单进行编辑操作！");
    	return false;
    } else {
    	var saleId = jQuery("#putmanage_list_grid-table").jqGrid('getGridParam', 'selrow');
    	var rowDate = jQuery("#putmanage_list_grid-table").jqGrid('getRowData', saleId).state;
    	if(rowDate!='<span style="color:#EEC211;font-weight:bold;">未提交</span>'){
    		layer.msg("该单据不可修改",{time:1200,icon:2});
    		return;
    	}
    	$.get( context_path + "/putManage/toAdd.do?id="+saleId).done(function(data){
    	  layer.open({
    	  title : "上架单编辑", 
    	  type:1,
    	  skin : "layui-layer-molv",
    	  area : ['750px', '650px'],
    	  shade : 0.6, //遮罩透明度
    	  moveType : 1, //拖拽风格，0是默认，1是传统拖动
    	  anim : 2,
    	  content : data
    	  });
    	 });
    		
    }
}
//上架单删除
function delPutManage(){
	var checkedNum = getGridCheckedNum("#putmanage_list_grid-table","id");  //选中的数量
	if(checkedNum==0){
		layer.alert("请至少选择一条要删除的上架单！");
	}else{
		//从数据库中删除选中的入库单，并刷新入库单表格
		var ids = jQuery("#grid-table").jqGrid('getGridParam', 'selarrrow');
		//弹出确认窗口
		layer.confirm("确定删除选中的上架单？", function() {
    		$.ajax({
    			type : "POST",
    			url:context_path + "/putManage/delete?ids="+ids,
    			dataType : 'json',
    			cache : false,
    			success : function(data) {
    				layer.closeAll();
    				if(data.result){
						layer.msg("上架单删除成功！", {icon: 1,time:1000});
    				}else{
    					layer.msg(data.msg, {icon: 2,time:1000});
    				}
    				_grid.trigger("reloadGrid");  //重新加载表格
    			}
    		});
    	});
	} 

}
/*上架单导出*/
function excelPutManage(){
    var selectid = jQuery("#putmanage_list_grid-table").jqGrid('getGridParam', 'selarrrow');
    $("#putmanage_list_hiddenForm #putmanage_list_ids").val(selectid);
    $("#putmanage_list_hiddenForm").submit();
}
//上架单查询功能
function queryInstoreListByParam(jsonParam){
	iTsai.form.deserialize($('#putmanage_list_queryForm'),jsonParam);   //将json对象反序列化到列表页面中隐藏的form中
	var queryParam = iTsai.form.serialize($('#putmanage_list_queryForm'));
	var queryJsonString = JSON.stringify(queryParam);         //将json对象转换成json字符串
	//执行查询操作
	$("#putmanage_list_grid-table").jqGrid('setGridParam', 
			{
				postData: {queryJson:queryJsonString} //发送数据 
			}
	  ).trigger("reloadGrid");
}
/**
 * 显示提示窗口
 * @param msg   显示信息
 * @param delay 持续时间,结束之后窗口消失
 */
function showTipMsg(msg,delay){
	Dialog.tip( msg, {type: "loading", delay: delay} );
}
/** 表格刷新 */
function gridReload(){
	 _grid.trigger("reloadGrid");
}
function viewDetailList(){
	var checkedNum = getGridCheckedNum("#putmanage_list_grid-table","id");
	if(checkedNum == 0){
    	layer.alert("请选择一条要查看的上架单！");
    	return false;
    } else if(checkedNum >1){
    	layer.alert("只能选择一条上架单进行查看操作！");
    	return false;
    } else {
    	var id = jQuery("#putmanage_list_grid-table").jqGrid('getGridParam', 'selrow');
    	$.get( context_path + "/putManage/viewDetail?id="+id).done(function(data){
			layer.open({
			    title : "上架单查看", 
		    	type:1,
		    	skin : "layui-layer-molv",
		    	area : ['750px', '650px'],
		    	shade : 0.6, //遮罩透明度
			    moveType : 1, //拖拽风格，0是默认，1是传统拖动
			    anim : 2,
			    content : data
			});
		});  	   	
    }
}
//上架
function put(){
	var checkedNum = getGridCheckedNum("#putmanage_list_grid-table","id");
	if(checkedNum == 0){
    	layer.alert("请选择一条要上架的上架单！");
    	return false;
    } else if(checkedNum >1){
    	layer.alert("只能选择一条上架单进行操作！");
    	return false;
    } else {
    	var saleId = jQuery("#putmanage_list_grid-table").jqGrid('getGridParam', 'selrow');
    	var rowDate = jQuery("#putmanage_list_grid-table").jqGrid('getRowData', saleId).state;
    	if(rowDate=='<span style="color:#EEC211;font-weight:bold;">未提交</span>'){
    		layer.msg("该单据未提交，不可进行上架操作",{time:1200,icon:2});
    		return;
    	}
    	$.get( context_path + "/putManage/toPut.do?id="+saleId).done(function(data){
    	  layer.open({
    	  title : "上架操作", 
    	  type:1,
    	  skin : "layui-layer-molv",
    	  area : ['750px', '650px'],
    	  shade : 0.6, //遮罩透明度
    	  moveType : 1, //拖拽风格，0是默认，1是传统拖动
    	  anim : 2,
    	  content : data
    	  });
    	 });
    }
}
function toSure(){
	var checkedNum = getGridCheckedNum("#putmanage_list_grid-table","id");
	if(checkedNum == 0){
    	layer.alert("请选择一条要操作的上架单！");
    	return false;
    } else if(checkedNum >1){
    	layer.alert("只能选择一条上架单进行操作！");
    	return false;
    } else {
    	var saleId = jQuery("#putmanage_list_grid-table").jqGrid('getGridParam', 'selrow');
    	var rowDate = jQuery("#putmanage_list_grid-table").jqGrid('getRowData', saleId).state;
    	if(rowDate=='<span style="color:#EEC211;font-weight:bold;">未提交</span>'){
    		layer.msg("该单据未提交，不可进行上架操作",{time:1200,icon:2});
    		return;
    	}
    	$.get( context_path + "/putManage/toSure.do?id="+saleId).done(function(data){
    	  layer.open({
    	  title : "上架操作", 
    	  type:1,
    	  skin : "layui-layer-molv",
    	  area : ['750px', '650px'],
    	  shade : 0.6, //遮罩透明度
    	  moveType : 1, //拖拽风格，0是默认，1是传统拖动
    	  anim : 2,
    	  content : data
    	  });
    	 });
    }
}
