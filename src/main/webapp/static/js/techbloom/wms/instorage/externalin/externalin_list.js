
/**
 * 查询按钮点击事件
 */
 function externalin_list_queryOk(){
	 var queryParam = iTsai.form.serialize($("#externalin_list_queryForm"));
	 //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
	 externalin_list_queryByParam(queryParam);
 }

function externalin_list_queryByParam(jsonParam) {
    iTsai.form.deserialize($("#externalin_list_hiddenQueryForm"), jsonParam);
    var queryParam = iTsai.form.serialize($("#externalin_list_hiddenQueryForm"));
    var queryJsonString = JSON.stringify(queryParam);
    $("#externalin_list_grid-table").jqGrid("setGridParam",
        {
            postData: {queryJsonString: queryJsonString}
        }
    ).trigger("reloadGrid");
}

function externalin_list_reset(){
     $("#externalin_list_queryForm #externalin_list_org").select2("val","");
	 iTsai.form.deserialize($("#externalin_list_queryForm"),externalin_list_queryForm_data); 
	 externalin_list_queryByParam(externalin_list_queryForm_data);
}

/**
 * 新增外采单
 */
function externalin_list_addExternal(){
	$.get( context_path + "/externalIn/toAdd.do?id=-1").done(function(data){
		$queryWindow = layer.open({
		    title : "外采单添加", 
	    	type:1,
	    	skin : "layui-layer-molv",
	    	area : ['780px', '620px'],
	    	shade : 0.6, //遮罩透明度
		    moveType : 1, //拖拽风格，0是默认，1是传统拖动
		    anim : 2,
		    content : data
		});
	});
}

function exportExecl(){
    var ids = jQuery("#externalin_list_grid-table").jqGrid("getGridParam", "selarrrow");
    $("#externalin_list_hiddenForm #externalin_list_queryEntityNo").val($("#externalin_list_queryForm #externalin_list_entityNo").val());
    $("#externalin_list_hiddenForm #externalin_list_ids").val(ids);
    $("#externalin_list_hiddenForm").submit();
}

/**
 * 编辑外采单
 */
function externalin_list_editExternal(){
	var checkedNum = getGridCheckedNum("#externalin_list_grid-table","id");
	if(checkedNum == 0){
    	layer.alert("请选择一条要编辑的外采单！");
    	return false;
    } else if(checkedNum >1){
    	layer.alert("只能选择一条外采单进行编辑操作！");
    	return false;
    } else {
    	var id = jQuery("#externalin_list_grid-table").jqGrid("getGridParam","selrow");
    	var rowData = $("#externalin_list_grid-table").jqGrid('getRowData',id);
    	if(rowData.state.indexOf("未确认")==-1){
    		layer.alert("只能对未确认的外采单进行编辑操作！");
        	return false;
    	}
    	$.get( context_path + "/externalIn/toAdd.do?id="+id).done(function(data){
			$queryWindow = layer.open({
			    title : "外采单编辑", 
		    	type:1,
		    	skin : "layui-layer-molv",
		    	area : ['900px', '650px'],
		    	shade : 0.6, //遮罩透明度
			    moveType : 1, //拖拽风格，0是默认，1是传统拖动
			    anim : 2,
			    content : data
			});
		});
    }
}

/*导出*/
function excelPutManage(){
    var selectid2 = jQuery("#externalin_list_grid-table").jqGrid('getGridParam', 'selarrrow');
	var selectid = getGridCheckedId("#externalin_list_grid-table","deliveryId");
    $("#externalin_list_hiddenForm #externalin_list_ids").val(selectid);
    $("#externalin_list_hiddenForm").submit();
}


/**
 * 删除外采单
 */
function externalin_list_delExternal(){
	var checkedNum = getGridCheckedNum("#externalin_list_grid-table","id");  //选中的数量
	if(checkedNum==0){
        layer.msg("请选择至少一条记录！",{icon:2});
        return;
    }
	var ids = jQuery("#externalin_list_grid-table").jqGrid('getGridParam', 'selarrrow');
	var rowData;
	for (var i = 0; i < ids.length; i++) {
		rowData = $("#externalin_list_grid-table").jqGrid('getRowData',ids[i]);
		if(rowData.state.indexOf("未确认")==-1){
			layer.msg("已确认的数据无法删除！",{icon:2});
			return;
		}
	}
	//弹出确认窗口
	layer.confirm( '确定删除选中的外采单？', 
        function(){
			$.ajax({
				type:"POST",
				url:context_path + "/externalIn/delExternalin?ids="+ids,
				dataType:"json",
				success:function(data){
					if(Boolean(data.result)){
						gridReload();//重新加载表格 
						//弹出提示信息
						showTipMsg("外采单删除成功！",1200);
					}else{
						layer.msg("外采单删除失败！",{icon:7,time:1200});
					}
				}
			});
        }
    ); 
}

/** 表格刷新 */
function gridReload(){
	_grid.trigger("reloadGrid");  //重新加载表格  
}

function showTipMsg(msg,delay){
	layer.msg(msg, {icon: 1,time:delay});
}

//确认入库（确认装包）
function externalin_list_submitExternal() {
	var checkedNum = getGridCheckedNum("#externalin_list_grid-table","id");
	if(checkedNum==0){
        layer.msg("请选择一条记录！",{icon:2});
        return;
    }else if(checkedNum>1){
        layer.msg("只能选择一条记录！",{icon:8});
        return;
    }
	var id = jQuery("#externalin_list_grid-table").jqGrid('getGridParam', 'selrow');
	var rowData = $("#externalin_list_grid-table").jqGrid('getRowData',id);
	if(rowData.state.indexOf("未确认")==-1){
		layer.alert("已确认的外采单无需重复操作！");
    	return false;
	}
	$submitWindow = layer.confirm( '确定提交选中的外采单？', function(){
        $.ajax({
            url: context_path + "/externalIn/changeState?id="+id,
            type: "POST",
            dataType: "JSON",
            beforeSend:function(XMLHttpRequest){
                $("#externalin_list_doing").html("<span style='color:#FF0000;font-weight:bold;'>请勿进行其他操作，正在处理，请稍后······</span>"); 
            },
            success: function (data) {
                if (Boolean(data.result)) {
                    layer.msg("保存成功！", {icon: 1});
                    //关闭当前窗口
                    layer.close($submitWindow);
                    //刷新列表
                    $("#externalin_list_grid-table").jqGrid('setGridParam',{
                    	postData: {queryJsonString: ""}
                 	}).trigger("reloadGrid");
                } else {
                    layer.alert("保存失败，请稍后重试！", {icon: 2});
                }
            },
            complete:function(XMLHttpRequest,textStatus){
                $("#externalin_list_doing").empty(); 
            },
            error:function(XMLHttpRequest){
            	$("#externalin_list_doing").empty(); 
        		alert(XMLHttpRequest.readyState);
        		alert("出错啦！！！");
        	}
        });
	  }
   );
}