//表头保存
function saveFormInfo(formdata){
    //$("#formSave").attr("disabled","disabled");
    $(window).triggerHandler('resize.jqGrid');
    //保存表头
    $.ajax({
        type:"POST",
        url:context_path + "/qualityCertificate/saveQuality",
        data:formdata,
        dataType:"json",
        success:function(data){
            if(data){
                if($('#qualitycertificate_spit_baseInfor #qualitycertificate_spit_id').val()==-1){
                    $('#qualitycertificate_spit_baseInfor #qualitycertificate_spit_qualityNo').val(data.qualityNo);
                    $('#qualitycertificate_spit_baseInfor #qualitycertificate_spit_id').val(data.id);
                }
                reloadDetailTableList();   //重新加载详情列表
                layer.alert("单据保存成功");
                $("#qualitycertificate_spit_formSave").removeAttr("disabled");
                gridReload();
            }else{
                layer.alert("单据保存失败");
            }
        }
    })
}

//刷新详情表
function reloadDetailTableList(){
    $("#qualitycertificate_spit_grid-table-c").jqGrid('setGridParam',
        {
            url:context_path + '/qualityCertificate/DetailList',
            postData: {qId:$('#qualitycertificate_spit_baseInfor #qualitycertificate_spit_id').val(),queryJsonString:""}
        }
    ).trigger("reloadGrid");
}

function delDetail(){
    var checkedNum = getGridCheckedNum("#qualitycertificate_spit_grid-table-c","id");  //选中的数量
    if(checkedNum==0){
        layer.alert("请选择一条记录！");
    }else{
        var ids = jQuery("#qualitycertificate_spit_grid-table-c").jqGrid('getGridParam', 'selarrrow');
        layer.confirm("确定删除选中的物料",function(){
            $.ajax({
                type:"POST",
                url:context_path + "/qualityCertificate/delDetail?ids="+ids,
                dataType:"json",
                success:function(data){
                    if(data.result){
                        reloadDetailTableList();   //重新加载详情列表
                        layer.msg(data.msg);
                    }else{
                        layer.alert(data.msg);
                    }
                }
            })
        });
    }
}

function spiltOk(){
    var checkedNum = getGridCheckedNum("#qualitycertificate_spit_grid-table-d","id");  //选中的数量
    if(checkedNum==0){
        layer.alert("请选择数据进行拆分！");
    }else{
        var ids = jQuery("#qualitycertificate_spit_grid-table-d").jqGrid('getGridParam', 'selarrrow');
        layer.confirm("确定拆分选中的物料",function(){
            $.ajax({
                type:"POST",
                url:context_path + "/qualityCertificate/createInstoragePlan?ids="+ids+'&id='+
                $('#qualitycertificate_spit_baseInfor #qualitycertificate_spit_id').val()+'&goodsId='+
                $('#qualitycertificate_spit_baseInfor #qualitycertificate_spit_goodsId').val(),
                dataType:"json",
                success:function(data){
                    if(data.result){
                        layer.alert(data.msg);
                        $("#qualitycertificate_spit_grid-table-d").jqGrid('setGridParam',
                            {
                                url:context_path + '/qualityCertificate/getSpiltList',
                                postData: {qId:$('#qualitycertificate_spit_baseInfor #qualitycertificate_spit_id').val(),queryJsonString:""}
                            }
                        ).trigger("reloadGrid");
                    }else{
                        layer.alert(data.msg);
                    }
                }
            })
        });
    }
}