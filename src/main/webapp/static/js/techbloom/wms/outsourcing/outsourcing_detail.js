//保存按钮
function saveFormInfo(formdata){
    $("#formSave").attr("disabled","disabled");
    $(window).triggerHandler('resize.jqGrid');
    $.ajax({
        type:"POST",
        url:context_path + "/outStoragePlan/saveOutPlan",
        data:formdata,
        dataType:"json",
        success:function(data){
            if(data.result){
                if($('#baseInfor #id').val()==-1){
                    $('#baseInfor #planCode').val(data.planCode);
                    $('#baseInfor #id').val(data.id);
                }
                reloadDetailTableList();   //重新加载详情列表
                layer.alert("单据保存成功");
                $("#formSave").removeAttr("disabled");
                gridReload();
            }else{
                layer.alert("单据保存失败");
            }
        }
    })
}
function reloadDetailTableList(){
    $("#grid-table-c").jqGrid('setGridParam',
        {
            url:context_path + '/outStoragePlan/DetailList',
            postData: {qId:$('#baseInfor #id').val(),queryJsonString:""}
        }
    ).trigger("reloadGrid");
}

//提交
function outformSubmitBtn(){
    if($('#baseInfor #id').val()==-1){
        layer.alert("请先保存单据");
        return;
    }else {
        var formdata = $('#baseInfor').serialize();
        //后台修改对应的数据
        layer.confirm("确定提交,提交之后数据不能修改,库存为0的数据将会自动删除", function () {
            $.ajax({
                type:"POST",
                url:context_path+"/outStoragePlan/setOutPlanState",
                dataType:"json",
                data:{id:$('#baseInfor #id').val(),outType:$('#baseInfor #outType').val()},
                data:formdata,
                success:function(data){
                    if(data.result){
                        layer.closeAll();
                        layer.msg("提交成功",{icon:1,time:1200});
                        gridReload();
                    }else{
                        layer.alert(data.msg);
                    }
                }
            })
        });
    }
}
//确认拆分
function querySpilt(){
    var checkedNum = getGridCheckedNum("#grid-table-d","id");  //选中的数量
    if(checkedNum==0){
        layer.alert("请选择一条记录！");
    }else{
        var ids = jQuery("#grid-table-d").jqGrid('getGridParam', 'selarrrow');
        layer.confirm("确定拆分数据",function(){
            $.ajax({
                type:"POST",
                url:context_path + "/outStoragePlan/createOutStorage?ids="+ids+'&id='+$("#baseIn #id").val(),
                dataType:"json",
                success:function(data){
                    if(data.result){
                        layer.alert(data.msg);
                        $("#grid-table-d").jqGrid('setGridParam',
                            {
                                url:context_path + '/outStoragePlan/getSpitlist',
                                postData: {qId:$('#baseIn #id').val(),queryJsonString:""}
                            }
                        ).trigger("reloadGrid");
                    }else{
                        layer.alert(data.msg);
                    }
                }
            })
        });
    }
}