var oriData;
var _grid;
function queryEngListByParam(jsonParam) {
    iTsai.form.deserialize($('#stockAlarm_list_hiddenQueryForm'), jsonParam);
    var queryParam = iTsai.form.serialize($('#stockAlarm_list_hiddenQueryForm'));
    var queryJsonString = JSON.stringify(queryParam);
    $("#stockAlarm_list_grid-table").jqGrid('setGridParam', {
        postData: {queryJsonString: queryJsonString} //发送数据
    }).trigger("reloadGrid");
}
var _queryForm_data = iTsai.form.serialize($('#stockAlarm_list_queryForm'));
function queryOk() {
    var queryParam = iTsai.form.serialize($('#stockAlarm_list_queryForm'));
    queryEngListByParam(queryParam);
}
function reset(){
	iTsai.form.deserialize($('#stockAlarm_list_queryForm'),_queryForm_data); 
	$("#stockAlarm_list_queryForm #stockAlarm_list_warehouseId").select2("val", "");
	queryEngListByParam(_queryForm_data);
	
}

$('#stockAlarm_list_warehouseIdSelect').change(function(){
	$('#stockAlarm_list_queryForm #stockAlarm_list_warehouseId').val($('#stockAlarm_list_warehouseIdSelect').val());
});
	
function toExcel(){
	var idd = jQuery("#stockAlarm_list_grid-table").jqGrid('getGridParam', 'selarrrow');
	$("#stockAlarm_list_ids").val(idd);
	$("#stockAlarm_list_hiddenForm").submit();
}	