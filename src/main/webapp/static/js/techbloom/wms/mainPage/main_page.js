$(".echarts").css("height", ($(".container-fluid").height()/2-85)+"px");
	                    
var myChart1 = echarts.init(document.getElementById('main1'));
$.ajax({
	url:context_path+"/goods/getShelfUseProLine",
	type:"POST",
	data:{},
	dataType:"JSON",
	async:false,
	success:function(data){
		var dataArray = new Array();
		var dateArray = new Array();
		for(var i=0;i<data.uselist.length;i++)
			dataArray.push(data.uselist[i].warehouseName);
		for(var i=0;i<data.uselist.length;i++)
			dateArray.push(data.uselist[i].usePro);
		var option1 = {
			grid:{
				x : 50,
				y : 30,
				x2 : 50,
				y2:30
			} ,
		    tooltip : {
		        trigger: 'axis'
		    },
		    legend: {
		    	show:false,
		        data:['占用率']
		    },
		    xAxis : [
		        {
		            type : 'category',
		            boundaryGap : false,
		            data : dataArray
		        }
		    ],
		    yAxis : [
		        {
		            type : 'value'
		        }
		    ],
		    series : [
		        {
		            name:'占用率',
		            type:'line',
		            smooth:true,
		            itemStyle: {
		            	normal: {
		                    color : '#2f85d0',
		            		lineStyle: {      
			            		color:"#2f85d0"
			            	},
		            		areaStyle: {
		            			color:"rgba(47, 133, 208, 0.4)"
		            	    },
		            	    nodeStyle: {      
			            		color:"#2f85d0"
			            	}
		                }
		             },
		            data:dateArray
		        }
		    ]
		};
		myChart1.setOption(option1);
	},
	error:function() {
         alert("失败，请稍后再试！");
    }
});

var myChart2 = echarts.init(document.getElementById('main2'));
$.ajax({
	url:context_path+"/storageInfo/getLibraryAge",
	type:"POST",
	data:{},
	dataType:"JSON",
	async:false,
	success:function(data){
		var option2 = {
		    tooltip : {
		        trigger: 'item',
		        formatter: "{a} <br/>{b} : {c} ({d}%)"
		    },
		    legend: {
		        show:false,
		        orient : 'vertical',
		        x : 'left',
		        data:['一年','两年','三年','其它']
		    },
		    series : [
		        {
		            name:'库龄占比',
		            type:'pie',
		            center: ['50%', '50%'],
		            radius : ['50%', '70%'],
		            itemStyle : {
		                normal : {
		                    label : {
		                        show : false
		                    },
		                    labelLine : {
		                        show : false
		                    }
		                }
		            
		            },
		            data:[
		                {value:data.oneYear, name:'一年', itemStyle:	{normal: { color : '#2f85d0'}}},
		                {value:data.twoYear, name:'两年', itemStyle:	{normal: { color : '#18abf5'}}},
		                {value:data.threeYear, name:'三年', itemStyle:	{normal: { color : 'orange'}}},
		                {value:data.otherYear, name:'其它', itemStyle:	{normal: { color : 'red'}}}
		            ]
		        }
		    ]
		};
		myChart2.setOption(option2);
		$("#one").html(data.oneYear*100+"%");
		$("#two").html(data.twoYear*100+"%");
		$("#three").html(data.threeYear*100+"%");
		$("#other").html(data.otherYear*100+"%");
	},
	error:function() {
         alert("失败，请稍后再试！");
    }
});

var myChart3 = echarts.init(document.getElementById('main3'));
$.ajax({
	url:context_path+"/outsrorageCon/getPersonOutnum",
	type:"POST",
	data:{},
	dataType:"JSON",
	async:false,
	success:function(data){
		var dataArray = new Array();
		var dateArray = new Array();
		for(var i=0;i<data.nameList.length;i++){
			dateArray.push(data.nameList[i]);
		}
		for(var i=0;i<data.outDataList.length;i++){
			dataArray.push(data.outDataList[i]);
		}
		var option3 = {
			grid:{
				x : 50,
				y : 30,
				x2 : 50,
				y2 : 30
			} ,
		    tooltip : {
		        trigger: 'axis'
		    },
		    legend: {
		        show:false,
		        data:['出库量']
		    },
		    xAxis : [
		        {
		            type : 'category',
		            data : dateArray
		        }
		    ],
		    yAxis : [
		        {
		            type : 'value'
		        }
		    ],
		    series : [
		        {
		            name:'出库量',
		            type:'bar',
	                itemStyle: {
		            	normal: {
		            		color:"#18abf5"
		                }
		             },
		            data:dataArray
		        }
		    ]
		};
		myChart3.setOption(option3);
	},
	error:function() {
         alert("失败，请稍后再试！");
    }
});

$(".btn_left").click(function(){
	if($(".btn_right").hasClass("btn_active")) {
		$(".btn_right").removeClass("btn_active")
	}
	$(".btn_left").addClass("btn_left btn_active");
	$.ajax({
		url:context_path+"/outsrorageCon/getPersonOutnum",
		type:"POST",
		data:{},
		dataType:"JSON",
		async:false,
		success:function(data){
			$("#main3").html("");
			var dataArray = new Array();
			var dateArray = new Array();
			for(var i=0;i<data.nameList.length;i++){
				dateArray.push(data.nameList[i]);
			}
			for(var i=0;i<data.outDataList.length;i++){
				dataArray.push(data.outDataList[i]);
			}
			var option3 = {
				grid:{
					x : 50,
					y : 30,
					x2 : 50,
					y2 : 30
				} ,
			    tooltip : {
			        trigger: 'axis'
			    },
			    legend: {
			        show:false,
			        data:['出库量']
			    },
			    xAxis : [
			        {
			            type : 'category',
			            data : dateArray
			        }
			    ],
			    yAxis : [
			        {
			            type : 'value'
			        }
			    ],
			    series : [
			        {
			            name:'出库量',
			            type:'bar',
		                itemStyle: {
			            	normal: {
			            		color:"#18abf5"
			                }
			             },
			            data:dataArray
			        }
			    ]
			};
			myChart3 = echarts.init(document.getElementById('main3'));
			myChart3.setOption(option3);
		},
		error:function() {
	         alert("失败，请稍后再试！");
	    }
	});
});

$(".btn_right").click(function(){
	if($(".btn_left").hasClass("btn_active")) {
		$(".btn_left").removeClass("btn_active")
	}
	$(".btn_right").addClass("btn_right btn_active");
	$.ajax({
		url:context_path+"/outsrorageCon/getPersonInnum",
		type:"POST",
		data:{},
		dataType:"JSON",
		async:false,
		success:function(data){
			$("#main3").html("");
			var dataArray = new Array();
			var dateArray = new Array();
			for(var i=0;i<data.nameList.length;i++){
				dateArray.push(data.nameList[i]);
			}
			for(var i=0;i<data.inDataList.length;i++){
				dataArray.push(data.inDataList[i]);
			}
			var option3 = {
				grid:{
					x : 50,
					y : 30,
					x2 : 50,
					y2 : 30
				} ,
			    tooltip : {
			        trigger: 'axis'
			    },
			    legend: {
			        show:false,
			        data:['入库量']
			    },
			    xAxis : [
			        {
			            type : 'category',
			            data : dateArray
			        }
			    ],
			    yAxis : [
			        {
			            type : 'value'
			        }
			    ],
			    series : [
			        {
			            name:'入库量',
			            type:'bar',
		                itemStyle: {
			            	normal: {
			            		color:"#18abf5"
			                }
			             },
			            data:dataArray
			        }
			    ]
			};
			myChart3 = echarts.init(document.getElementById('main3'));
			myChart3.setOption(option3);
		},
		error:function() {
	         alert("失败，请稍后再试！");
	    }
	});
});

$(".btn_right").trigger("click");