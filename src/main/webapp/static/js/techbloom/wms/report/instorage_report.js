var dynamicDefalutValue="e15fbbd5b8fc44678eff1bb361ed6b08";
function querySummaryByParam(jsonParam) {
    iTsai.form.deserialize($('#hiddenQueryForm'), jsonParam);
    var queryParam = iTsai.form.serialize($('#hiddenQueryForm'));
    var queryJsonString = JSON.stringify(queryParam);
    $("#grid-table").jqGrid('setGridParam', {
        postData: {queryJsonString: queryJsonString}
    }).trigger("reloadGrid");
}

$(function () {
   _grid = jQuery("#grid-table").jqGrid({
    		url: context_path + '/inReport/list',
            datatype: "json",
            colNames: ['id', '库区','物料编号', '物料名称', '单位', '入库数量'],
            colModel: [
                {name: 'id', index: 'id', width: 55, hidden: true},
                {name: 'warehouseName', index: 'warehouseName', width: 48},
                {name: 'materialNo', index: 'materialNo', width: 48},
                {name: 'materialName', index: 'materialName', width: 50},
                {name: 'materialUnit', index: 'materialUnit', width: 20},
                {name: 'inAmount', index: 'inAmount', width: 30,formatter: "number", formatoptions: {thousandsSeparator:",", defaulValue:"",decimalPlaces:2}}
            ],
            rowNum: 20,
            rowList: [10, 20, 30],
            pager: '#grid-pager',
            sortname: 'iod.id',
            sortorder: "asc",
            altRows: true,
            viewrecords: true,
            autowidth: true,
            multiselect: true,
            multiboxonly: true,
            beforeRequest:function (){
               dynamicGetColumns(dynamicDefalutValue,"grid-table",$(window).width()-$("#sidebar").width() -7);
               //重新加载列属性
            },
            loadComplete: function (data) {
                var table = this;
                setTimeout(function () {
                    updatePagerIcons(table);
                    enableTooltips(table);
                }, 0);
                oriData = data;
            },
            emptyrecords: "没有相关记录",
            loadtext: "加载中...",
            pgtext: "页码 {0} / {1}页",
            recordtext: "显示 {0} - {1}共{1}条数据"
        });
        jQuery("#grid-table").navGrid('#grid-pager', {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false
        }).navButtonAdd('#grid-pager', {
                    caption: "",
                    buttonicon: "ace-icon fa fa-refresh green",
                    onClickButton: function () {
                        $("#grid-table").jqGrid('setGridParam',
                                {
                                    postData: {queryJsonString: ""} //发送数据
                                }
                        ).trigger("reloadGrid");
                    }
                })
            .navButtonAdd('#grid-pager',{
                caption: "",
                buttonicon:"fa  icon-cogs",
                onClickButton : function (){
                    jQuery("#grid-table").jqGrid('columnChooser',{
                        done: function(perm, cols){
                            dynamicColumns(cols,dynamicDefalutValue);
                            $("#grid-table").jqGrid( 'setGridWidth', $("#grid-div").width());
                        }
                    });
                }
            });
    $(window).on("resize.jqGrid", function () {
        $("#grid-table").jqGrid("setGridWidth", $("#grid-div").width() );
        $("#grid-table").jqGrid("setGridHeight",$(".container-fluid").height()-3- $("#yy").outerHeight(true)- $("#fixed_tool_div").outerHeight(true)- 
		$("#grid-pager").outerHeight(true)- 
		$("#gview_grid-table .ui-jqgrid-hdiv").outerHeight(true));
    });
    $(window).triggerHandler("resize.jqGrid");
});
$(function(){
		//初始化时间控件
  $('.date-picker').datepicker({
		autoclose: true,
		todayHighlight: true
	});
});

$('#queryForm #materialId').select2({
   placeholder: "选择物料",
   minimumInputLength:0,   //至少输入n个字符，才去加载数据
   allowClear: true,  //是否允许用户清除文本信息
   delay: 250,
   formatNoMatches:"没有结果",
   formatSearching:"搜索中...",
   formatAjaxError:"加载出错啦！",
   ajax : {
  		url: context_path+"/material/getMaterialSelectList",
  		type:"POST",
  		dataType : 'json',
  		delay : 250,
  		data: function (term,pageNo) {     //在查询时向服务器端传输的数据
  	           term = $.trim(term);
               return {
               queryString: term,    //联动查询的字符
               pageSize: 15,    //一次性加载的数据条数
               pageNo:pageNo,    //页码
               time:new Date()   //测试
               }
  	     },
  	    results: function (data,pageNo) {
  	        	var res = data.result;
     	        if(res.length>0){   //如果没有查询到数据，将会返回空串
     	        var more = (pageNo*15)<data.total; //用来判断是否还有更多数据可以加载
     	        return {
     	               results:res,more:more
     	              };
     	        }else{
     	            return {
     	              results:{}
     	            };
     	        }
  	      },
  		cache : true
  	}
});

$("#materialId").on("change.select2",function(){
      $("#materialId").trigger("keyup")}
);

function queryOk(){
	var queryParam = iTsai.form.serialize($('#queryForm'));
	querySummaryByParam(queryParam);		
}
function reset(){
	$("#queryForm #materialId").select2("data",null).trigger("change");
	$("#queryForm #warehouseId").val("").trigger("change");
	$("#grid-table").jqGrid('setGridParam', {
        postData: {queryJsonString:""} //发送数据
     }).trigger("reloadGrid");		
}

$('#queryForm .mySelect2').select2();

$('#warehouseId').change(function(){
	$('#queryForm #warehouseId').val($('warehouseId').val());
});

function toExcel(){
	var idd = jQuery("#grid-table").jqGrid('getGridParam', 'selarrrow');
	$("#ids").val(idd);
	$("#hiddenForm").submit();
}