
//置为不可复用
function goods_list_unmltiplexing() {
	const selectAmount = getGridCheckedNum("#goods_list_grid-table");
	if (selectAmount<1){
		/*layer.confirm("确定设置所有库位吗？", function() {
			$.post(context_path+'/goods/unMltiplexing.do',{},function success(){
				layer.msg('更新成功',{icon:1,time:1200});
				jQuery("#goods_list_grid-table").trigger("reloadGrid");
			});
		});*/
		layer.msg("请选择要设置的库位！",{icon:2,time:1200});
	}else{
		layer.confirm("确定设置所选库位吗？", function() {
			$.post(context_path+'/goods/unMltiplexing.do?ids='+jQuery("#goods_list_grid-table").jqGrid("getGridParam", "selarrrow"),{
			},function success(){
				layer.msg('更新成功',{icon:1,time:1200});
				jQuery("#goods_list_grid-table").trigger("reloadGrid");
			});
		});
	}

}

//置为可复用
function goods_list_mltiplexing() {
	const selectAmount = getGridCheckedNum("#goods_list_grid-table");
	if (selectAmount<1){
		/*layer.confirm("确定设置所有库位吗？", function() {
			$.post(context_path+'/goods/mltiplexing.do',{},function success(){
				layer.msg('更新成功',{icon:1,time:1200});
				jQuery("#goods_list_grid-table").trigger("reloadGrid");
			});
		});*/
		layer.msg("请选择要设置的库位！",{icon:2,time:1200});
	}else {
		layer.confirm("确定设置所选库位吗？", function() {
			$.post(context_path+'/goods/mltiplexing.do?ids='+jQuery("#goods_list_grid-table").jqGrid("getGridParam", "selarrrow"),{
			},function success(){
				layer.msg('更新成功',{icon:1,time:1200});
				jQuery("#goods_list_grid-table").trigger("reloadGrid");
			});
		});
	}

}

/**打开添加页面*/
function goods_list_openAddPage(){
	$.post(context_path + "/goods/toAdd.do", {}, function (str){
		$queryWindow = layer.open({
			title: "库位添加",
			type: 1,
			skin: "layui-layer-molv",
			area: "600px",
			shade: 0.6, //遮罩透明度
			moveType: 1, //拖拽风格，0是默认，1是传统拖动
			anim: 2,
			content: str,
			success: function (layero, index) {
				layer.closeAll('loading');
			}
		});
	});
}

/**打开编辑页面*/
function goods_list_openEditPage(){
	const selectAmount = getGridCheckedNum("#goods_list_grid-table");
	if(selectAmount===0){
		layer.msg("请选择一条记录！",{icon:2});
		return;
	}else if(selectAmount>1){
		layer.msg("只能选择一条记录！",{icon:8});
		return;
	}
	layer.load(2);
	$.post(context_path+'/goods/toAdd.do', {
		id:jQuery("#goods_list_grid-table").jqGrid("getGridParam", "selrow")
	}, function(str){
		$queryWindow = layer.open({
			title: "库位修改",
			type: 1,
			skin: "layui-layer-molv",
			area: "600px",
			shade: 0.6, //遮罩透明度
			moveType: 1, //拖拽风格，0是默认，1是传统拖动
			content: str,//注意，如果str是object，那么需要字符拼接。
			success: function (layero, index) {
				layer.closeAll("loading");
			}
		});
	}).error(function() {
		layer.closeAll();
		layer.msg("加载失败！",{icon:2});
	});
}

//删除
function goods_list_deletes(){
	const checkedNum = getGridCheckedNum("#goods_list_grid-table", "id");  //选中的数量
	if (checkedNum === 0) {
		layer.alert("请选择一个要删除的库位！");
	} else {
		layer.confirm("确定删除选中的库位？", function() {
			$.ajax({
				type : "POST",
				url : context_path + "/goods/deleteCars.do?ids="+jQuery("#goods_list_grid-table").jqGrid("getGridParam", "selarrrow") ,
				dataType : "json",
				cache : false,
				success : function(data) {
					layer.closeAll();
					if (Boolean(data.result)) {
						layer.msg(data.msg, {icon: 1,time:1000});
					}else{
						layer.msg(data.msg, {icon: 7,time:1000});
					}
					jQuery("#goods_list_grid-table").trigger("reloadGrid");  //重新加载表格
				}
			});
		});
	}
}