/**
 * 新增客户
 */
function addCustomer() {
	$.post(context_path + '/customerinfo/toAdd.do', {}, function (str){
		$queryWindow=layer.open({
		    title : "客户添加", 
	    	type:1,
	    	skin : "layui-layer-molv",
	    	area : "600px",
	    	shade : 0.6, //遮罩透明度
		    moveType : 1, //拖拽风格，0是默认，1是传统拖动
		    anim : 2,
		    content : str,
		    success: function (layero, index) {
                layer.closeAll('loading');
            }
		});
	});
}
/**
 * 修改客户
 */
function editCustomer() {
    var checkedNum = getGridCheckedNum("#customer_list_grid-table", "id");
    if (checkedNum == 0) {
       layer.alert("请选择一个要编辑的客户！");
        return false;
    } else if (checkedNum > 1) {
    	layer.alert("只能选择一个客户进行编辑操作！");
        return false;
    } else {
        var customerid = jQuery("#customer_list_grid-table").jqGrid('getGridParam', 'selrow');
        $.post(context_path + '/customerinfo/toAdd.do?id='+customerid, {}, function (str){
    		$queryWindow=layer.open({
    		    title : "客户编辑", 
    	    	type:1,
    	    	skin : "layui-layer-molv",
    	    	area : "600px",
    	    	shade : 0.6, //遮罩透明度
    		    moveType : 1, //拖拽风格，0是默认，1是传统拖动
    		    anim : 2,
    		    content : str,
    		    success: function (layero, index) {
                    layer.closeAll('loading');
                }
    		});
    	});
    }
}
/** 表格刷新 */
function gridReload() {
    _grid.trigger("reloadGrid");  //重新加载表格
}
/**
 * 显示提示窗口
 * @param msg   显示信息
 * @param delay 持续时间,结束之后窗口消失
 */
function showTipMsg(msg, delay) {
	layer.msg(msg, {icon: 1,time:delay});
}
/**
 *删除客户
 */
function delCustomer() {
    var checkedNum = getGridCheckedNum("#customer_list_grid-table", "id");  //选中的数量
    if (checkedNum == 0) {
        layer.alert("请选择一个要删除的客户！");
    } else {
        //从数据库中删除选中的物料，并刷新物料表格
        var ids = jQuery("#customer_list_grid-table").jqGrid('getGridParam', 'selarrrow');
        layer.confirm("确定删除选中的客户？", function() {
    		$.ajax({
    			type : "POST",
    			url : context_path + '/customerinfo/deleteCustomer.do?ids=' + ids,
    			dataType : 'json',
    			cache : false,
    			success : function(data) {
    				layer.closeAll();
    				if (Boolean(data.result)) {
    					layer.msg(data.msg, {icon: 1,time:1000});
    				}else{
    					layer.msg(data.msg, {icon: 7,time:2000});
    				}
    				_grid.trigger("reloadGrid");  //重新加载表格
    			}
    		});
    	});
    }
}
/**
 * 入库单查询功能:获取查询页面中的值，并将值放入列表页面中隐藏的form
 * @param jsonParam     查询页面传递过来的json对象
 */
function queryCustomerListByParam(jsonParam) {
    iTsai.form.deserialize($('#customer_list_hiddenQueryForm'), jsonParam);   //将json对象反序列化到列表页面中隐藏的form中
    var queryParam = iTsai.form.serialize($('#customer_list_hiddenQueryForm'));
    var queryJsonString = JSON.stringify(queryParam);         //将json对象转换成json字符串
    //执行查询操作
    $("#customer_list_grid-table").jqGrid('setGridParam',
        {
            postData: {queryJsonString: queryJsonString} //发送数据
        }
    ).trigger("reloadGrid");
}
function disableAddButton() {
    if ($("#customer_list_customerForm").valid()) { 
        $("#customer_list_customerButton").attr("disabled", "disabled");
        $("#customer_list_customerForm").submit();
    }
}
