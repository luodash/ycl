/**
 * 显示提示窗口
 * @param msg   显示信息
 * @param delay 持续时间,结束之后窗口消失
 */
function showTipMsg(msg, delay) {
	layer.msg(msg, {icon: 1,time:delay});
}
/**
 * 打开查询界面
 */
function openAreaListSearchPage() {
    var queryBean = iTsai.form.serialize($('#area_list_hiddenQueryForm'));   //获取form中的值：json对象
    var queryJsonString = JSON.stringify(queryBean);         //将json对象转换成json字符串
    Dialog.open({
        title: "库区查询",
        width: 400, height: '260',
        url: context_path + "/area/toQueryPage?jsonString=" + queryJsonString,
        theme: "simple",
        drag: true
    });
}


/**
 * 入库单查询功能:获取查询页面中的值，并将值放入列表页面中隐藏的form
 * @param jsonParam     查询页面传递过来的json对象
 */
function queryAreaListByParam(jsonParam) {
    iTsai.form.deserialize($('#area_list_hiddenQueryForm'), jsonParam);   //将json对象反序列化到列表页面中隐藏的form中
    var queryParam = iTsai.form.serialize($('#area_list_hiddenQueryForm'));
    var queryJsonString = JSON.stringify(queryParam);         //将json对象转换成json字符串
    //执行查询操作
    $("#area_list_grid-table").jqGrid('setGridParam',
        {
            postData: {areaId: "", queryJsonString: queryJsonString} //发送数据
        }
    ).trigger("reloadGrid");
}

/**
 * 重新加载列表数据
 * @param parentTreeId
 */
function reloadGridData(areaId) {
    $("#area_list_grid-table").jqGrid('setGridParam',
        {
            postData: {areaId: areaId, queryJsonString: ""} //发送数据  :选中的节点
        }
    ).trigger("reloadGrid");
}

function disableAddButton() {
    if ($("#area_list_AreaForm").valid()) {
        $("#area_list_areaButton").attr("disabled", "disabled");
        $("#area_list_AreaForm").submit();
    }
}