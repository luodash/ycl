/**物料类别添加*/
function add() {
 var level=$("#material_type_list_level").val();
 if(parseInt(level)>4){
	 layer.alert("不可在该层级下继续添加物料类别！");
	 return;
 }
 
 if(selectTreeId == 0){
	 layer.alert("不可在该层级下继续添加物料类别!")
 }else{
		$.get(context_path + "/materialType/toAddMaterialType?id=-1&parentId="+selectTreeId).done(function(data){
	    	layer.open({
			    title : "物料类别添加",  
		    	type:1,
		    	skin : "layui-layer-molv",
		    	area : "600px",
		    	shade : 0.6, //遮罩透明度
			    moveType : 1, //拖拽风格，0是默认，1是传统拖动
			    anim : 2,
			    content : data
		    	
			});
	    });
	 }
}

/** 物料类别编辑  */
function editMaterial(){
	var checkedNum = getGridCheckedNum("#material_type_list_grid-table","id");
	if(checkedNum == 0){
    	layer.alert("请选择一个要编辑的物料类别！");
    	return false;
    } else if(checkedNum >1){
    	layer.alert("只能选择一个物料类别进行编辑操作！");
    	return false;
    } else {
    	var materialid = jQuery("#material_type_list_grid-table").jqGrid('getGridParam', 'selrow');
    	//判断类别如果有子类，则不可以被修改
    	$.ajax({
			type:"POST",
			url:context_path + "/materialType/isInUse?mTypeId="+materialid,
			dataType:"json",
			success:function(data){
				if(Boolean(data.result)){
					$.get(context_path + "/materialType/toAddMaterialType?id="+materialid+"&parentId="+selectTreeId).done(function(data){
				    	layer.open({
						    title : "物料类别编辑",  
					    	type:1,
					    	skin : "layui-layer-molv",
					    	area : "600px",
					    	shade : 0.6, //遮罩透明度
						    moveType : 1, //拖拽风格，0是默认，1是传统拖动
						    anim : 2,
						    content : data
						});
				    });
				}else{
					if(data.msg!=""){
						layer.alert("其中<br>{<br>"+data.msg+"}");
					}
				}
			}
		});
    }
}
/*物料类别导出*/
function excelMaterial(){
    var selectid = getId("#material_type_list_grid-table","id");
    function getId(_grid,_key){
        var idAddr = jQuery("#material_type_list_grid-table").jqGrid('getGridParam', 'selarrrow');
        return idAddr;
    }
    $("#material_type_list_ids").val(selectid);
    $("#material_type_list_hiddenForm").submit();
}
/** 物料类别删除  */
function delMaterial(){
	var checkedNum = getGridCheckedNum("#material_type_list_grid-table","id");  //选中的数量
	if(checkedNum==0){
		layer.alert("请选择一个要删除的物料类别！");
	}else{
		//从数据库中删除选中的物料类别，并刷新物料类别表格
		var ids = jQuery("#material_type_list_grid-table").jqGrid('getGridParam', 'selarrrow');
	    var  node=complexTree.getNodeByParam("id",ids[0]);
	    if(node==null)
	    	selectTreeId = complexTree.getSelectedNodes()[0].id;
	    else
	    	selectTreeId=node.pid;
		//弹出确认窗口
		layer.confirm( '确定删除选中的物料类别？', 
            function(){
				$.ajax({
					type:"POST",
					url:context_path + "/materialType/delMaterialType?ids="+ids,
					dataType:"json",
					success:function(data){
						if(Boolean(data.result)){
							//弹出提示信息
							showTipMsg("物料类别删除成功！",1200);
							complexTree.reAsyncChildNodes(complexTree.getNodeByParam("id",selectTreeId), "refresh",false);
							gridReload();//重新加载表格 
							if(data.returnData!="")
								layer.alert("其中<br>{<br>"+data.returnData+"}");
						}else{
							//showTipMsg("物料类别删除失败！",1200);
							if(data.returnData!="")
								//弹出确认框
								layer.alert("其中<br>{<br>"+data.returnData+"}");
							else
								//弹出确认框
								layer.alert("删除失败");
						}
					}
				});
            }
        );
	}
}


/**
 * 打开查询界面
 */
function openMaterialTypeSearchPage(){
	var queryBean = iTsai.form.serialize($('#material_type_list_hiddenQueryForm'));   //获取form中的值：json对象
	var queryJsonString = JSON.stringify(queryBean);     //将json对象转换成json字符串
	$.get(context_path + "/materialType/toQueryPage?jsonString="+queryJsonString).done(function(data){
    	layer.open({
		    title : "物料类别查询",  
	    	type:1,
	    	skin : "layui-layer-molv",
	    	area : ['400px', '200px'],
	    	shade : 0.6, //遮罩透明度
		    moveType : 1, //拖拽风格，0是默认，1是传统拖动
		    anim : 2,
		    content : data
		});
    });
}


/**
 * 查询功能:获取查询页面中的值，并将值放入列表页面中隐藏的form
 * @param jsonParam     查询页面传递过来的json对象
 */
function queryTypeListByParam(jsonParam){
	iTsai.form.deserialize($('#material_type_list_hiddenQueryForm'),jsonParam);   //将json对象反序列化到列表页面中隐藏的form中
	var queryParam = iTsai.form.serialize($('#material_type_list_hiddenQueryForm'));
	var queryJsonString = JSON.stringify(queryParam);     //将json对象转换成json字符串
	//执行查询操作
	$("#material_type_list_grid-table").jqGrid('setGridParam', 
			{
				postData: {typeId:"",jsonString:queryJsonString} //发送数据 
			}
	  ).trigger("reloadGrid");
}


/** 表格刷新 */
function gridReload(){
	jQuery("#material_type_list_grid-table").trigger("reloadGrid");  //重新加载表格  
}

/**
 * 显示提示窗口
 * @param msg   显示信息
 * @param delay 持续时间,结束之后窗口消失
 */
function showTipMsg(msg,delay){
	layer.msg(msg, {icon: 1,time:delay});
}

//重新加载页面
function reloadWindow(){
	window.location.reload();
}

