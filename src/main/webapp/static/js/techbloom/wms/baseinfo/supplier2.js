/**
 * 新增供应商
 */
function addSupplier() {
	$.post(context_path + '/supplierinfo/toAdd.do', {}, function (str){
		$queryWindow=layer.open({
		    title : "供应商添加", 
	    	type:1,
	    	skin : "layui-layer-molv",
	    	area : "740px",
	    	shade : 0.6, //遮罩透明度
		    moveType : 1, //拖拽风格，0是默认，1是传统拖动
		    anim : 2,
		    content : str,
		    success: function (layero, index) {
                layer.closeAll('loading');
            }
		});
	});
}

/**
 * 修改客户
 */
function editSupplier() {
    var checkedNum = getGridCheckedNum("#container_list_grid-table", "id");
    if (checkedNum == 0) {
       layer.alert("请选择一个要编辑的客户！");
        return false;
    } else if (checkedNum > 1) {
    	layer.alert("只能选择一个客户进行编辑操作！");
        return false;
    } else {
        var id = jQuery("#container_list_grid-table").jqGrid('getGridParam', 'selrow');
        $.post(context_path + '/supplierinfo/toAdd.do?id='+id, {}, function (str){
    		$queryWindow=layer.open({
    		    title : "供应商编辑", 
    	    	type:1,
    	    	skin : "layui-layer-molv",
    	    	area : "740px",
    	    	shade : 0.6, //遮罩透明度
    		    moveType : 1, //拖拽风格，0是默认，1是传统拖动
    		    anim : 2,
    		    content : str,
    		    success: function (layero, index) {
                    layer.closeAll('loading');
                }
    		});
    	});
    }
}
/** 表格刷新 */
function gridReload() {
    _grid.trigger("reloadGrid");  //重新加载表格
}

/**
 * 显示提示窗口
 * @param msg   显示信息
 * @param delay 持续时间,结束之后窗口消失
 */
function showTipMsg(msg, delay) {
    layer.tip(msg, {type: "loading", delay: delay});
}

/**
 *删除供应商
 */
function delSupplier() {
    var checkedNum = getGridCheckedNum("#container_list_grid-table", "id");  //选中的数量
    if (checkedNum == 0) {
    	layer.alert("请选择一个要删除的供应商！");
    } else {
        //从数据库中删除选中的供应商，并刷新供应商表格
        var ids = jQuery("#container_list_grid-table").jqGrid('getGridParam', 'selarrrow');
        layer.confirm("确定删除选中的供应商？", function() {
    		$.ajax({
    			type : "POST",
    			url : context_path + '/supplierinfo/deleteSupplier.do?ids='+ids ,
    			dataType : 'json',
    			cache : false,
    			success : function(data) {
    				layer.closeAll();
    				if (Boolean(data.result)) {
    					layer.msg(data.msg, {icon: 1,time:1000});
    				}else{
    					layer.msg(data.msg, {icon: 7,time:2000});
    					
    				}
    				_grid.trigger("reloadGrid");  //重新加载表格
    			}
    		});
    	});
        
    }
}

function disableAddButton() {
    if ($("#container_list_supplierForm").valid()) { 
        $("#container_list_supplierButton").attr("disabled", "disabled");
        $("#container_list_supplierForm").submit();
    }
}
