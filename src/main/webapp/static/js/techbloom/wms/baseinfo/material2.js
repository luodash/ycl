var childquer;
var childEditDiv;
var user_msg = {
	"100":"<h2>保存物料成功！</h2>是否继续添加？<br/>选择”是“：继续添加<br/>选择”否“：退出添加界面"
};

/**物料添加*/
function add() {
	$.get( context_path + "/material/toAddMaterial?id=-1").done(function(data){
		childquer=layer.open({
		    title : "物料添加", 
	    	type:1,
	    	skin : "layui-layer-molv",
	    	area : "740px",
	    	shade : 0.6, //遮罩透明度
		    moveType : 1, //拖拽风格，0是默认，1是传统拖动
		    anim : 2,
		    content : data
		});
	});
}

/** 物料编辑  */
function editMaterial(){
	var checkedNum = getGridCheckedNum("#material_list_grid-table","id");
	if(checkedNum == 0){
    	layer.alert("请选择一个要编辑的物料！");
    	return false;
    } else if(checkedNum >1){
    	layer.alert("只能选择一个物料进行编辑操作！");
    	return false;
    } else {
    	var materialid = jQuery("#material_list_grid-table").jqGrid('getGridParam', 'selrow');
		$.get( context_path + "/material/toAddMaterial?id="+materialid).done(function(data){
			$queryWindow=layer.open({
			    title : "物料编辑", 
		    	type:1,
		    	skin : "layui-layer-molv",
		    	area : "740px",
		    	shade : 0.6, //遮罩透明度
			    moveType : 1, //拖拽风格，0是默认，1是传统拖动
			    anim : 2,
			    content : data
			});
		});
    }
}

/** 物料删除  */
function delMaterial() {
    var checkedNum = getGridCheckedNum("#material_list_grid-table", "id");  //选中的数量
    if (checkedNum == 0) {
        layer.alert("请选择一个要删除的物料！");
    } else {
        //从数据库中删除选中的物料，并刷新物料表格
        var ids = jQuery("#material_list_grid-table").jqGrid('getGridParam', 'selarrrow');
        layer.confirm("确定删除选中的物料？", function() {
    		$.ajax({
    			type : "POST",
    			url : context_path + "/material/delMaterial?ids="+ids,
    			dataType : 'json',
    			cache : false,
    			success : function(data) {
    				layer.closeAll();
    				if (Boolean(data.result)) {
    					layer.msg("物料删除成功！", {icon: 1,time:1000});
    				}else{
    					layer.msg(data.msg, {icon: 7,time:2800});   					
    				}
    				_grid.trigger("reloadGrid");  //重新加载表格
    			}
    		});
    	});
    }
}



/*
 * json字符串转json对象：jQuery.parseJSON(jsonStr);
 * json对象转json字符串：JSON.stringify(jsonObj);
*/

/**
 * 打开查询界面
 */
function opeMaterialListSearchPage(){
	var queryBean = iTsai.form.serialize($('#material_list_hiddenQueryForm'));   //获取form中的值：json对象
	var queryJsonString = JSON.stringify(queryBean);         //将json对象转换成json字符串
	$.get(context_path + "/material/toQueryPage?materialString="+queryJsonString).done(function(data){
		layer.open({
		    title : "物料条件查询", 
	    	type:1,
	    	skin : "layui-layer-molv",
	    	area : ['400px', '400px'],
	    	shade : 0.6, //遮罩透明度
		    moveType : 1, //拖拽风格，0是默认，1是传统拖动
		    anim : 2,
		    content : data
		});
	});
	
}


/**
 * 查询功能:获取查询页面中的值，并将值放入列表页面中隐藏的form
 * @param jsonParam     查询页面传递过来的json对象
 */
function queryInstoreListByParam(jsonParam){
	//序列化表单：iTsai.form.serialize($('#frm'))
	//反序列化表单：iTsai.form.deserialize($('#frm'),json)
	iTsai.form.deserialize($('#material_list_queryForm'),jsonParam);   //将json对象反序列化到列表页面中隐藏的form中
	var queryParam = iTsai.form.serialize($('#material_list_queryForm'));
	var queryJsonString = JSON.stringify(queryParam);         //将json对象转换成json字符串
	//执行查询操作
	$("#material_list_grid-table").jqGrid('setGridParam', 
			{
				postData: {jsonString:queryJsonString} //发送数据 
			}
	  ).trigger("reloadGrid");
}

/** 表格刷新 */
function gridReload(){
	 _grid.trigger("reloadGrid");  //重新加载表格  
}

/**
 * 显示提示窗口
 * @param msg   显示信息
 * @param delay 持续时间,结束之后窗口消失
 */
function showTipMsg(msg,delay){
	layer.msg(msg, {icon: 1,time:delay});
}
//物料导入
function materialExportIn(){
	$.get( context_path + "/material/toExportIn").done(function(data){
		childquer=layer.open({
		    title : "物料导入", 
	    	type:1,
	    	skin : "layui-layer-molv",
	    	area : ['420px', '160px'],
	    	shade : 0.6, //遮罩透明度
		    moveType : 1, //拖拽风格，0是默认，1是传统拖动
		    anim : 2,
		    content : data
		});
	});
}
