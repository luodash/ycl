
function materials_list_queryOk(){
	//执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
	materials_list_queryByParam(iTsai.form.serialize($("#materials_list_queryForm")));
}

function materials_list_reset(){
	iTsai.form.deserialize($("#materials_list_queryForm"),materials_list_queryForm_data);
	$("#materials_list_queryForm #materials_list_industry").select2("val","");
	$("#materials_list_queryForm #materials_list_category").select2("val","");
	$("#materials_list_queryForm #materials_list_gradeTwo").select2("val","");
	$("#materials_list_queryForm #materials_list_gradeThree").select2("val","");
	materials_list_industry = '';
	materials_list_category = '';
	materials_list_gradeTwo = '';
	materials_list_gradeThree = '';
	//执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
	materials_list_queryByParam(materials_list_queryForm_data);
}

//同步数据
function materials_list_synchronousdata(){
	$.ajax({
		type: "GET",
		url: context_path+"/materials/synchronousdata.do",
		dataType: "json",
		success: function(data){
			if(data.result){
				layer.msg(data.msg,{icon:1,time:1200});
				materials_list_grid.trigger("reloadGrid");
			}else{
				layer.msg(data.msg,{icon:2,time:1200});
			}
		},
		error:function(XMLHttpRequest){
			alert(XMLHttpRequest.readyState);
			alert("出错啦！！！");
		}
	});
}

function materials_list_queryByParam(jsonParam) {
	iTsai.form.deserialize($("#materials_list_hiddenQueryForm"), jsonParam);
	var queryParam = iTsai.form.serialize($("#materials_list_hiddenQueryForm"));
	var queryJsonString = JSON.stringify(queryParam);
	$("#materials_list_grid-table").jqGrid("setGridParam", {
		postData: {queryJsonString: queryJsonString}
	}).trigger("reloadGrid");
}

//导出功能
function materials_list_toExcel(){
	$("#materials_list_hiddenForm #materials_list_ids").val(jQuery("#materials_list_grid-table").jqGrid("getGridParam", "selarrrow"));
	$("#materials_list_hiddenForm #materials_list_queryName").val($("#materials_list_queryForm #materials_list_name").val());
	$("#materials_list_hiddenForm #materials_list_queryExportExcelIndex").val(materials_list_exportExcelIndex);
	$("#materials_list_hiddenForm").submit();
}

/**打开编辑页面**/
function material_edit(){
	var selectAmount = getGridCheckedNum("#materials_list_grid-table");
	if(selectAmount===0){
		layer.msg("请选择一条记录！",{icon:2});
		return;
	}else if(selectAmount>1){
		layer.msg("只能选择一条记录！",{icon:8});
		return;
	}

	layer.load(2);
	$.post(context_path+'/materials/edit.do', {
		id:jQuery("#materials_list_grid-table").jqGrid('getGridParam', 'selrow')
	}, function(str){
		$queryWindow = layer.open({
			title : "编辑",
			type: 1,
			skin : "layui-layer-molv",
			area : [window.screen.width-400+"px",document.body.clientHeight-300+"px"],
			shade: 0.6, //遮罩透明度
			moveType: 1, //拖拽风格，0是默认，1是传统拖动
			content: str,//注意，如果str是object，那么需要字符拼接。
			success:function(layero, index){
				layer.closeAll("loading");
			}
		});
	}).error(function() {
		layer.closeAll();
		layer.msg("加载失败！",{icon:2});
	});
}

//行业
$("#materials_list_queryForm #materials_list_industry").select2({
	placeholder: "选择行业",
	minimumInputLength:0,   //至少输入n个字符，才去加载数据
	allowClear: true,  //是否允许用户清除文本信息
	delay: 250,
	formatNoMatches:"没有结果",
	formatSearching:"搜索中...",
	formatAjaxError:"加载出错啦！",
	ajax : {
		url: context_path+"/materials/getIndustry",
		type:"POST",
		dataType : 'json',
		delay : 250,
		data: function (term,pageNo) {     //在查询时向服务器端传输的数据
			term = $.trim(term);
			return {
				queryString : term,    //联动查询的字符
				pageSize : 15,    //一次性加载的数据条数
				pageNo : pageNo    //页码
			}
		},
		results: function (data,pageNo) {
			const res = data.result;
			if(res.length>0){   //如果没有查询到数据，将会返回空串
				const more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
				return {
					results:res,
					more:more
				};
			}else{
				return {
					results:{}
				};
			}
		},
		cache : true
	}
});

//类别
$("#materials_list_queryForm #materials_list_category").select2({
	placeholder: "选择类别",
	minimumInputLength:0,   //至少输入n个字符，才去加载数据
	allowClear: true,  //是否允许用户清除文本信息
	delay: 250,
	formatNoMatches:"没有结果",
	formatSearching:"搜索中...",
	formatAjaxError:"加载出错啦！",
	ajax : {
		url: context_path+"/materials/getCategory",
		type:"POST",
		dataType : 'json',
		delay : 250,
		data: function (term,pageNo) {     //在查询时向服务器端传输的数据
			term = $.trim(term);
			return {
				queryString : term,    //联动查询的字符
				pageSize : 15,    //一次性加载的数据条数
				pageNo : pageNo,    //页码
				industry : materials_list_industry	//行业
			}
		},
		results: function (data,pageNo) {
			const res = data.result;
			if(res.length>0){   //如果没有查询到数据，将会返回空串
				const more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
				return {
					results:res,more:more
				};
			}else{
				return {
					results:{}
				};
			}
		},
		cache : true
	}
});

//材料二级
$("#materials_list_queryForm #materials_list_gradeTwo").select2({
	placeholder: "选择二级材料",
	minimumInputLength:0,   //至少输入n个字符，才去加载数据
	allowClear: true,  //是否允许用户清除文本信息
	delay: 250,
	formatNoMatches:"没有结果",
	formatSearching:"搜索中...",
	formatAjaxError:"加载出错啦！",
	ajax : {
		url: context_path+"/materials/getGradeTwo",
		type:"POST",
		dataType : 'json',
		delay : 250,
		data: function (term,pageNo) {     //在查询时向服务器端传输的数据
			term = $.trim(term);
			return {
				queryString : term,    //联动查询的字符
				pageSize : 15,    //一次性加载的数据条数
				pageNo : pageNo,    //页码
				industry : materials_list_industry,	//行业
				category : materials_list_category	//物料类别
			}
		},
		results: function (data,pageNo) {
			const res = data.result;
			if(res.length>0){   //如果没有查询到数据，将会返回空串
				const more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
				return {
					results:res,more:more
				};
			}else{
				return {
					results:{}
				};
			}
		},
		cache : true
	}
});

//材料三级
$("#materials_list_queryForm #materials_list_gradeThree").select2({
	placeholder: "选择三级材料",
	minimumInputLength:0,   //至少输入n个字符，才去加载数据
	allowClear: true,  //是否允许用户清除文本信息
	delay: 250,
	formatNoMatches:"没有结果",
	formatSearching:"搜索中...",
	formatAjaxError:"加载出错啦！",
	ajax : {
		url: context_path+"/materials/getGradeThree",
		type:"POST",
		dataType : 'json',
		delay : 250,
		data: function (term,pageNo) {     //在查询时向服务器端传输的数据
			term = $.trim(term);
			return {
				queryString : term,    //联动查询的字符
				pageSize : 15,    //一次性加载的数据条数
				pageNo : pageNo,    //页码
				industry : materials_list_industry,	//行业
				category : materials_list_category,	//物料类别
				grateTwo : materials_list_gradeTwo	//二级材料
			}
		},
		results: function (data,pageNo) {
			const res = data.result;
			if(res.length>0){   //如果没有查询到数据，将会返回空串
				const more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
				return {
					results:res,
					more:more
				};
			}else{
				return {
					results:{}
				};
			}
		},
		cache : true
	}
});

$("#materials_list_queryForm #materials_list_industry").on("change",function(e){
	$("#materials_list_queryForm #materials_list_category").select2("val","");
	materials_list_category = '';
	$("#materials_list_queryForm #materials_list_gradeTwo").select2("val","");
	materials_list_gradeTwo = '';
	$("#materials_list_queryForm #materials_list_gradeThree").select2("val","");
	materials_list_gradeThree = '';
	materials_list_industry = $("#materials_list_queryForm #materials_list_industry").val();
});

$("#materials_list_queryForm #materials_list_category").on("change",function(e){
	$("#materials_list_queryForm #materials_list_gradeTwo").select2("val","");
	materials_list_gradeTwo = '';
	$("#materials_list_queryForm #materials_list_gradeThree").select2("val","");
	materials_list_gradeThree = '';
	materials_list_category = $("#materials_list_queryForm #materials_list_category").val();
});

$("#materials_list_queryForm #materials_list_gradeTwo").on("change",function(e){
	$("#materials_list_queryForm #materials_list_gradeThree").select2("val","");
	materials_list_gradeThree = '';
	materials_list_gradeTwo = $("#materials_list_queryForm #materials_list_gradeTwo").val();
});
