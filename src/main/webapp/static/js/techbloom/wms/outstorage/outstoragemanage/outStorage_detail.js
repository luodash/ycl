function saveFormInfo(formdata){
    $("#formSave").attr("disabled","disabled");
    $(window).triggerHandler('resize.jqGrid');
    $.ajax({
        type:"POST",
        url:context_path + "/outStorage/saveOutStorage",
        data:formdata,
        dataType:"json",
        success:function(data){
            if(data.result){
                if($('#formInfoContent #id').val()==-1) {
                    $('#formInfoContent #id').val(data.id);
                    $('#formInfoContent #documentNoEdit').val(data.documentNo);
                }
                layer.alert("单据保存成功");
                reloadDetailTableList();
                $("#formSave").removeAttr("disabled");
                gridReload();
            }else{
                layer.alert("单据保存失败");
            }
        }
    })
}
//删除详情
function delOutStorageDetail(){
    var checkedNum = getGridCheckedNum("#grid-table-detail","detailId");  //选中的数量
    if(checkedNum==0){
        layer.alert("请选择一条记录！");
    }else{
        var ids = jQuery("#grid-table-detail").jqGrid('getGridParam', 'selarrrow');
        alert(ids);
        layer.confirm("确定删除选择的物料",function(){
            $.ajax({
                type:"POST",
                url:context_path + "/outStorage/delDetail?ids="+ids,
                dataType:"json",
                success:function(data){
                    if(data.result){
                        layer.msg(data.msg);
                        reloadDetailTableList();   //重新加载详情列表
                    }else{
                        layer.msg(data.msg);
                    }
                }
            })
        });
    }
}

function  reloadDetailTableList(){
    $("#grid-table-detail").jqGrid('setGridParam',
        {
            url:context_path + '/outStorage/outStorageDetaillist',
            postData: {id:$("#formInfoContent #id").val(),outPlanId:$("#formInfoContent #outPlanId").val(),queryJsonString:""}
        }
    ).trigger("reloadGrid");
}
//物料的添加
function addOutStorageMaterial(){
    if($("#formInfoContent #id").val()==-1){
        layer.alert("请先保存表单");
        return;
    }
    if(selectData!=0){
        $.ajax({
            type: "POST",
            url: context_path + '/outStorage/saveDetail',
            data: {
                id: $('#formInfoContent #id').val(),
                materialId: selectData.toString()
            },
            dataType: "json",
            success:function(data){
                removeChoice();   //清空下拉框中的值
                if(data.result!=null){
                    if(data.result!=null){
                        layer.alert(data.msg);
                        reloadDetailTableList();
                    }else{
                        layer.alert("添加失败");
                    }
                }
            }
        })

    }
}
