/*查看入库单详情*/
function viewDetailList(id) {
	$.get(context_path + "/getbills/viewGetbillsDetail?id=" + id).done(
	function(data) {
		layer.open({
			title : "下架单查看",
			type : 1,
			skin : "layui-layer-molv",
			area : [ '780px', '620px' ],
			shade : 0.6, // 遮罩透明度
			moveType : 1, // 拖拽风格，0是默认，1是传统拖动
			anim : 2,
			content : data
		});
	});
}

/**
 * 下架
 */
function getComplete() {
	var checkedNum = getGridCheckedNum("#getbills_list_grid-table", "id");
	if (checkedNum == 0) {
		layer.alert("请选择一条要确认完成的下架单！");
		return false;
	} else if (checkedNum > 1) {
		layer.alert("只能选择一条下架单进行操作！");
		return false;
	} else {
		var id = jQuery("#getbills_list_grid-table").jqGrid('getGridParam', 'selrow');
		var rowData = jQuery("#getbills_list_grid-table").jqGrid('getRowData', id);
		var getbillsStatus = rowData.getbillsStatus;
		layer.confirm('确定完成下架？<br>注：请确认已完成下架操作！', function() {
			$.ajax({
				type : "POST",
				url : context_path + "/getbills/getComplete?getId=" + id,
				dataType : "json",
				success : function(data) {
					if (Boolean(data.result)) {
						gridReload();// 重新加载表格
						layer.alert(data.msg);
					} else {
						layer.alert("操作失败！");
					}
				}
			});
		});
	}
}

var _queryForm_data = iTsai.form.serialize($('#getbills_list_queryForm'));
function queryOk() {
	// var formJsonParam = $('#queryForm').serialize();
	var queryParam = iTsai.form.serialize($('#getbills_list_queryForm'));
	// 执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
	queryGetbillsListByParam(queryParam);

}
$('#getbills_list_customerIdSelect').change(function() {
	$('#getbills_list_queryForm #getbills_list_customerId').val($('#getbills_list_customerIdSelect').val());
});

// 初始化下拉框:库区选择
// 当选择下框的时候，将选中的值赋值给隐藏的输入框中，方便form表单获取
$('#getbills_list_warehouseIdSelect').change(function() {
	$('#getbills_list_queryForm #getbills_list_warehouseId').val($('#getbills_list_warehouseIdSelect').val());
});
function reset() {
	iTsai.form.deserialize($('#getbills_list_queryForm'), _queryForm_data);
	$("#getbills_list_customerIdSelect").val("").trigger('change');
	$("#getbills_list_warehouseIdSelect").val("").trigger('change');
	$("#getbills_list_queryForm #getbills_list_customerId").select2("val","");
	$("#getbills_list_queryForm #getbills_list_warehouseId").select2("val","");
	queryGetbillsListByParam(_queryForm_data);

}

/** 表格刷新 */
function gridReload() {
	_grid.trigger("reloadGrid"); // 重新加载表格
}

function queryGetbillsListByParam(jsonParam) {
	iTsai.form.deserialize($('#getbills_list_hiddenQueryForm'), jsonParam); // 将json对象反序列化到列表页面中隐藏的form中
	var queryParam = iTsai.form.serialize($('#getbills_list_hiddenQueryForm'));
	var queryJsonString = JSON.stringify(queryParam); // 将json对象转换成json字符串
	// 执行查询操作
	$("#getbills_list_grid-table").jqGrid('setGridParam', {
		postData : {
			queryJsonString : queryJsonString
		}
	// 发送数据
	}).trigger("reloadGrid");
}