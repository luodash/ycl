//保存按钮
function saveFormInfo(formdata){
    $("#outstorageplan_spilt_formSave").attr("disabled","disabled");
    $(window).triggerHandler('resize.jqGrid');
    $.ajax({
        type:"POST",
        url:context_path + "/outStoragePlan/saveOutPlan",
        data:formdata,
        dataType:"json",
        success:function(data){
            if(data.result){
                if($('#outstorageplan_spilt_baseInfor #outstorageplan_spilt_id').val()==-1){
                    $('#outstorageplan_spilt_baseInfor #outstorageplan_spilt_planCode').val(data.planCode);
                    $('#outstorageplan_spilt_baseInfor #outstorageplan_spilt_id').val(data.id);
                }
                reloadDetailTableList();   //重新加载详情列表
                layer.alert("单据保存成功");
                $("#outstorageplan_spilt_formSave").removeAttr("disabled");
                gridReload();
            }else{
                layer.alert("单据保存失败");
            }
        }
    })
}
function reloadDetailTableList(){
    $("#outstorageplan_spilt_grid-table-c").jqGrid('setGridParam',
        {
            url:context_path + '/outStoragePlan/DetailList',
            postData: {qId:$('#outstorageplan_spilt_baseInfor #outstorageplan_spilt_id').val(),queryJsonString:""}
        }
    ).trigger("reloadGrid");
}

//删除详情中的数据
function delDetail(){
    var checkedNum = getGridCheckedNum("#outstorageplan_spilt_grid-table-c","id");  //选中的数量
    if(checkedNum==0){
        layer.alert("请选择一条记录！");
    }else{
        var ids = jQuery("#outstorageplan_spilt_grid-table-c").jqGrid('getGridParam', 'selarrrow');
        layer.confirm("确定删除选择的物料",function(){
            $.ajax({
                type:"POST",
                url:context_path + "/outStoragePlan/delDetail?ids="+ids,
                dataType:"json",
                success:function(data){
                    if(data.result){
                        layer.msg(data.msg);
                        reloadDetailTableList();   //重新加载详情列表
                    }else{
                        layer.msg(data.msg);
                    }
                }
            })
        });
    }
}

//提交
function outformSubmitBtn(){
    if($('#outstorageplan_spilt_baseInfor #outstorageplan_spilt_id').val()==-1){
        layer.alert("请先保存单据");
        return;
    }else {
        var formdata = $('#outstorageplan_spilt_baseInfor').serialize();
        //后台修改对应的数据
        layer.confirm("确定提交,提交之后数据不能修改,库存为0的数据将会自动删除", function () {
            $.ajax({
                type:"POST",
                url:context_path+"/outStoragePlan/setOutPlanState",
                dataType:"json",
                data:{id:$('#baseInfor #id').val(),outType:$('#outstorageplan_spilt_baseInfor #outstorageplan_spilt_outType').val()},
                data:formdata,
                success:function(data){
                    if(data.result){
                        layer.closeAll();
                        layer.msg("提交成功",{icon:1,time:1200});
                        gridReload();
                    }else{
                        layer.alert(data.msg);
                    }
                }
            })
        });
    }
}
//确认拆分
function querySpilt(){
    var checkedNum = getGridCheckedNum("#outstorageplan_spilt_grid-table-d","id");  //选中的数量
    if(checkedNum==0){
        layer.alert("请选择一条记录！");
    }else{
        var ids = jQuery("#outstorageplan_spilt_grid-table-d").jqGrid('getGridParam', 'selarrrow');
        layer.confirm("确定拆分数据",function(){
            $.ajax({
                type:"POST",
                url:context_path + "/outStoragePlan/createOutStorage?ids="+ids+'&id='+$("#outstorageplan_spilt_baseIn #outstorageplan_spilt_id").val(),
                dataType:"json",
                success:function(data){
                    if(data.result){
                        layer.alert(data.msg);
                        $("#outstorageplan_spilt_grid-table-d").jqGrid('setGridParam',
                            {
                                url:context_path + '/outStoragePlan/getSpitlist',
                                postData: {qId:$('#outstorageplan_spilt_baseIn #outstorageplan_spilt_id').val(),queryJsonString:""}
                            }
                        ).trigger("reloadGrid");
                    }else{
                        layer.alert(data.msg);
                    }
                }
            })
        });
    }
}