/**
 * 新增发货单
 */
function addDelivery(){
	$.get( context_path + "/delivery/toAdd.do?id=-1").done(function(data){
		layer.open({
		    title : "发货单添加", 
	    	type:1,
	    	skin : "layui-layer-molv",
	    	area : ['780px', '620px'],
	    	shade : 0.6, //遮罩透明度
		    moveType : 1, //拖拽风格，0是默认，1是传统拖动
		    anim : 2,
		    content : data
		});
	});
}


/**
 * 编辑发货单
 */
function edit(){
	var checkedNum = getGridCheckedNum("#delivery_list_grid-table","id");
	if(checkedNum == 0){
    	layer.alert("请选择一条要编辑的发货单！");
    	return false;
    } else if(checkedNum >1){
    	layer.alert("只能选择一条发货单进行编辑操作！");
    	return false;
    } else {
    	var id = getGridCheckedId("#delivery_list_grid-table","deliveryId");
    	$.get( context_path + "/delivery/toAdd.do?id="+id).done(function(data){
			layer.open({
			    title : "发货单编辑", 
		    	type:1,
		    	skin : "layui-layer-molv",
		    	area : ['900px', '650px'],
		    	shade : 0.6, //遮罩透明度
			    moveType : 1, //拖拽风格，0是默认，1是传统拖动
			    anim : 2,
			    content : data
			});
		});
    }
}

/*上架单导出*/
function excelPutManage(){
    var selectid2 = jQuery("#delivery_list_grid-table").jqGrid('getGridParam', 'selarrrow');
	var selectid = getGridCheckedId("#delivery_list_grid-table","deliveryId");
    $("#delivery_list_hiddenForm #delivery_list_ids").val(selectid);
    $("#delivery_list_hiddenForm").submit();
}

//修改预约单状态
function editOrderStatus(){
	var checkedNum = getGridCheckedNum("#delivery_list_grid-table","id");
	if(checkedNum == 0){
    	layer.alert("请选择一条要修改状态的发货单！");
    	return false;
    } else if(checkedNum >1){
    	layer.alert("只能选择一条发货单进行修改状态操作！");
    	return false;
    } else {
    	var outStorageId = jQuery("#delivery_list_grid-table").jqGrid('getGridParam', 'selrow'); 
    	$.ajax({
    		type:"POST",
    		url:context_path + "/outStorage/isDropDoc",
    		data:{outStorageId:outStorageId},
    		dataType:"json",
    		success:function(data){
    			if(data.result){
    				//废弃的单据
    				layer.alert("废弃的单据不可以再操作！");
    			}else{
    				$.ajax({
    		    		type:"POST",
    		    		url:context_path + "/outStorage/getOutstoreByID",
    		    		data:{outStorageId:outStorageId},
    		    		dataType:"json",
    		    		success:function(data){
    		    			if(data.result){
    		    				if(data.returnData.status==2){
    		    					layer.alert("该发货单已审核，不可再做修改！<br>但是你还是可以通过查看功能对该发货单进行查看");
    		    					return;
    		    				}
    		    				if(data.returnData.status==1){
    		    					layer.alert("该发货单已提交，不可再做修改！<br>但是你还是可以通过查看功能对该发货单进行查看");
    		    					return;
    		    				}
    		    			}
    		    			$.get( context_path + "/outStorage/toModeStatus?id="+outStorageId+"&preStatus="+data.returnData.preStatus).done(function(data){
    		    				layer.open({
    		    				    title : "发货单预约状态修改", 
    		    			    	type:1,
    		    			    	skin : "layui-layer-molv",
    		    			    	area : ['450px', '200px'],
    		    			    	shade : 0.6, //遮罩透明度
    		    				    moveType : 1, //拖拽风格，0是默认，1是传统拖动
    		    				    anim : 2,
    		    				    content : data
    		    				});
    		    			});
    		    		}
    		    	});
    			}
    		}
    	});
    }
}


/**
 * 删除发货单
 */
function delOutStorageOrder(){
	var checkedNum = getGridCheckedNum("#delivery_list_grid-table","id");  //选中的数量
	if(checkedNum==0){
		layer.alert("请至少选择一条要删除的发货单！");
	}else{
		//从数据库中删除选中的入库单，并刷新入库单表格
		var ids = getGridCheckedId("#delivery_list_grid-table","deliveryId");
		//弹出确认窗口
		layer.confirm( '确定删除选中的发货单？', 
            function(){
				$.ajax({
					type:"POST",
					url:context_path + "/delivery/delDelivery?ids="+ids,
					dataType:"json",
					success:function(data){
						if(Boolean(data.result)){
							gridReload();//重新加载表格 
								//弹出提示信息
								showTipMsg("发货单删除成功！",1200);
						}else{
							//showTipMsg("发货单删除失败！",1200);
							layer.msg("发货单删除失败！",{icon:7,time:1200});
						}
					}
				});
            }
        ); 
	} 
}


//查看发货单详情
function viewDetail(){
	var checkedNum = getGridCheckedNum("#grid-table","id");
	if(checkedNum == 0){
    	layer.alert("请选择一条要查看的发货单！");
    	return false;
    } else if(checkedNum >1){
    	layer.alert("只能选择一条发货单进行查看操作！");
    	return false;
    } else {
    	var id = getGridCheckedId("#delivery_list_grid-table","deliveryId");
    	$.get( context_path + "/delivery/viewDetail?deliveryId="+id).done(function(data){
    		layer.open({
    		    title : "发货单查看", 
    	    	type:1,
    	    	skin : "layui-layer-molv",
    	    	area : ['850px', '650px'],
    	    	shade : 0.6, //遮罩透明度
    		    moveType : 1, //拖拽风格，0是默认，1是传统拖动
    		    anim : 2,
    		    content : data
    		});
    	});
    }
}


/**
 *设置工程默认值 
 */
function selectProject(value){
	$("#delivery_list_projectId").val(value);
}

/**
 * 设置发货类型默认值
 */
function selectoutstorageType(value){
   $("#delivery_list_outstorageType").val();
}

/**
 * 设置库区
 */
function selectWarehouseArea(value){
	$("#delivery_list_areaId").val(value);
} 
$(function(){ 
	 //初始化下拉框:入库类别选择
	  //当选择下框的时候，将选中的值赋值给隐藏的输入框中，方便form表单获取
	  $('#delivery_list_instorageTypeSelect').change(function(){
		 $('#delivery_list_instorageType').val($('#delivery_list_instorageTypeSelect').val());
	  });
	  
	//初始化下拉框:供应商选择
	  //当选择下框的时候，将选中的值赋值给隐藏的输入框中，方便form表单获取
	  $('#delivery_list_supplierIdSelect').change(function(){
		 $('#delivery_list_supplierId').val($('#delivery_list_supplierIdSelect').val());
	  });
	  
	//初始化下拉框:库位选择
	  //当选择下框的时候，将选中的值赋值给隐藏的输入框中，方便form表单获取
	  $('#delivery_list_warehouseIdSelect').change(function(){
		 $('#delivery_list_warehouseId').val($('#delivery_list_warehouseIdSelect').val());
	  });
	  
	  //初始化下拉框:物料选择
});

/**
 * 打开查询界面
 */
function openOutstoreListSearchPage(){
	var queryBean = iTsai.form.serialize($('#delivery_list_hiddenQueryForm'));   //获取form中的值：json对象
	var queryJsonString = JSON.stringify(queryBean);  //将json对象转换成json字符串
	Dialog.open({
	    title: "发货单条件查询", 
	    width: 600,  height: '400',
	    url: context_path + "/outStorage/toQueryPage?outstoreString="+queryJsonString,
	    theme : "simple",
		drag : true
	});
}


/**
 * 入库单查询功能:获取查询页面中的值，并将值放入列表页面中隐藏的form
 * @param jsonParam     查询页面传递过来的json对象
 */
function queryInstoreListByParam(jsonParam){
	//序列化表单：iTsai.form.serialize($('#frm'))
	//反序列化表单：iTsai.form.deserialize($('#frm'),json)
	iTsai.form.deserialize($('#delivery_list_hiddenQueryForm'),jsonParam);   //将json对象反序列化到列表页面中隐藏的form中
	var queryParam = iTsai.form.serialize($('#delivery_list_hiddenQueryForm'));
	var queryJsonString = JSON.stringify(queryParam);         //将json对象转换成json字符串
	//执行查询操作
	$("#delivery_list_grid-table").jqGrid('setGridParam', 
			{
				postData: {queryJsonString:queryJsonString} //发送数据 
			}
	  ).trigger("reloadGrid");
}


/**
 * 显示提示窗口
 * @param msg   显示信息
 * @param delay 持续时间,结束之后窗口消失
 */
function showTipMsg(msg,delay){
	layer.msg(msg, {icon: 1,time:delay});
}

/** 表格刷新 */
function gridReload(){
	 _grid.trigger("reloadGrid");  //重新加载表格  
}