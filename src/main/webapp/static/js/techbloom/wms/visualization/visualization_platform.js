
var visualization_platform_carCode, visualization_platform_mission;
var visualization_platform_pointArray,visualization_platform_polygonArray,visualization_platform_greenIcon,visualization_platform_latlngs2,
    visualization_platform_x,visualization_platform_y,visualization_platform_oldx,visualization_platform_oldy,visualization_platform_targetXSIZE,
    visualization_platform_targetYSIZE,visualization_platform_av1;
var visualization_platform_GridJson = [],visualization_platform_GridDataJson =[],visualization_platform__area = "", visualization_platform_titleIcon ="",
    visualization_platform_polygon="";
var visualization_platform_TargetShelfJson = [],visualization_platform_TargetShelfDataJson =[];
var visualization_platform_regBoxGridJson = [],visualization_platform_regBoxGridDataJson =[];
var visualization_platform_map_h,visualization_platform_map_w;
var visualization_platform_bounds =[[0,0], [5500, 28000]];
var visualization_platform_colors ={
    "0" : "#52C41A",
    "1" : "#FFAE0E",
    "2" : "#0A6DD7",
    "3" : "#722ED1",
    "4" : "#F5222D",
};

// leaflet.js, 初始化地图
var visualization_platform_map = L.map("map2",{
    crs: L.CRS.Simple, // 简单模式
    zoom: -4,           // 初始缩放比例
    minZoom: -8,
    zoomSnap: 0.25,     // 缩放系数
    maxZoom:1
});

visualization_platform_map.fitBounds(visualization_platform_bounds);//设置包含可能具有最大缩放级别的给定地理边界的地图视图
visualization_platform_map.setZoom(-1.5);
// 地图缩放事件结束后执行
visualization_platform_map.on("zoomend", function(){
    // alert(map.getZoom());
});

/**
 *   _______ (x1,y1)
 * |       |
 * |_______|
 * (x0,y0)
 *
 * y1为区域 右上角点的y坐标轴
 *   grids=[ {
 *      id:1,
 *      area:[[10,10],[20,20]],
 *      color: "blue"       // 如果不设置颜色，默认灰色
 *   } ];
 *
 * */
function visualization_platform_loadGrid(grids){
    //清空地图数据
    for(var i in visualization_platform_GridJson){
        visualization_platform_GridJson.hasOwnProperty(i) &&   visualization_platform_GridJson[i].remove();
    }
    visualization_platform_GridJson = [];
    visualization_platform_GridDataJson = [];

    visualization_platform__area && visualization_platform__area.remove();          // 清空地图
    visualization_platform_titleIcon && visualization_platform_titleIcon.remove();  // 清空图标
    visualization_platform_polygon && visualization_platform_polygon.remove();      // 清空边框

    //实际的仓库区域（场景）
    var latlngs = [[0, 0],[visualization_platform_map_w, 0],[visualization_platform_map_w, visualization_platform_map_h],[0, visualization_platform_map_h]];
    //根据上面的范围画边框
    visualization_platform_polygon = L.polygon(latlngs, {color: '#ccc',weight:3, fillOpacity:0}).addTo(visualization_platform_map);
    _icon = L.divIcon({
        className: 'my-div-icon',
        iconSize:30
    });

    // 根据grids来画库位
    if( grids && grids.length >0 ){
        for(var i = 0; i < grids.length; i++){
            //库位初始化
            var rect = L.rectangle(grids[i].area, {
                color: "#444",
                fillColor: grids[i].color,
                weight: 0,
                fillOpacity: 1
            }).addTo(visualization_platform_map);
            // 记录数据，用于清空
            visualization_platform_GridJson[grids[i].id] = rect;
            visualization_platform_GridDataJson[grids[i].id] = grids[i];
        }
    }
}