
   var GridJson = [],GridDataJson =[],_area = "", titleIcon ="", px = 16, polygon="";
   var TargetShelfJson = [],TargetShelfDataJson =[];
   var regBoxGridJson = [],regBoxGridDataJson =[];
   var carCode, factoryWarehouseAreaName;
   var wms_map_h,wms_map_w;
   var bounds = [[0,0], [5500, 28000]];
   var colors ={
       "0" : "#52C41A",
       "1" : "#FFAE0E",
       "2" : "#0A6DD7",
       "3" : "#722ED1",
       "4" : "#F5222D",
       "5" : "#808080"
   };

   // leaflet.js, 初始化地图
   var map = L.map("map",{
       crs: L.CRS.Simple, // 简单模式
       zoom: -4,           // 初始缩放比例
       minZoom: -8,
       zoomSnap: 0.25,     // 缩放系数
       maxZoom:1
   }), rotate = false;
   
   map.fitBounds(bounds);//设置包含可能具有最大缩放级别的给定地理边界的地图视图
   map.setZoom(-1);
   // 地图缩放事件结束后执行
   map.on("zoomend", function(){
	  if( map.getZoom() < -1.5 ){
	      $("canvas").hide();
      }else{
          $("canvas").show();
	     // map.addLayer(ciLayer);
      }
   });

    /**
     *   _______ (x1,y1)
     * |       |
     * |_______|
     * (x0,y0)
     * 
     * y1为区域 右上角点的y坐标轴
     *   grids=[ {
     *      id:1,
     *      area:[[10,10],[20,20]],
     *      color: "blue"       // 如果不设置颜色，默认灰色
     *   } ];
     *
     * */
   function wms_visualization_loadGrid(grids,wareArea, openDetail){
       //清空地图数据
        for(var i in GridJson){
            GridJson.hasOwnProperty(i) &&   GridJson[i].remove();
        }
        GridJson = [];
        GridDataJson = [];

       _area && _area.remove();          // 清空地图
       titleIcon && titleIcon.remove();  // 清空图标
       polygon && polygon.remove();      // 清空边框
       $("canvas").remove();

       var latlngs = [[0, 0],[wms_map_w, 0],[wms_map_w, wms_map_h],[0,wms_map_h]];  //实际的仓库区域（场景）
       //根据上面的范围画边框
       polygon = L.polygon(latlngs, {color: '#ccc',weight:3, fillOpacity:0}).addTo(map);
       _icon = L.divIcon({
           html: factoryWarehouseAreaName,
           className: 'my-div-icon',
           iconSize:30
       });

       // 根据grids来画库位
       if( grids && grids.length >0 ){
          var ciLayer = L.canvasIconLayer({}) , markers =[]
          for(var i = 0; i < grids.length; i++){
              //库位初始化
              var rect = L.rectangle(grids[i].area, {
                  color: "#444",
                  fillColor: grids[i].color,
                  weight:1,
                  fillOpacity: 1
              }).addTo(map);

              var area = grids[i].area, x = 0, y = 0, rotate = false;
              if( Math.abs(area[1][0]-area[0][0]) < Math.abs(area[1][1]-area[0][1]) ){
                  x = area[0][0] - 15;
                  y = area[1][1]/2 + area[0][1]/2;
                  rotate = false;
              }else{
                  x = area[1][0]/2+ area[0][0]/2;
                  y = area[1][1]+15;
                  rotate = true;
              }
              var myIcon = L.divIcon({
                  html: grids[i].code,
                  className: 'my-div-icon',
                  iconSize:30,
                  rotate: rotate
              });
              var marker = L.marker([x, y], { icon: myIcon });
              markers.push(marker);
              rect.on('click', (function( p ) {
                  return function(){
                       /*openDetail( p );*/
                      $.post(context_path+'/visualization/toDetail.do', {
                          id : p.id
                      }, function(str){
                          $queryWindow = layer.open({
                              title : "库位信息",
                              type: 1,
                              skin : "layui-layer-molv",
                              area : "600px",
                              shade: 0.6, //遮罩透明度
                              moveType: 1, //拖拽风格，0是默认，1是传统拖动
                              content: str,//注意，如果str是object，那么需要字符拼接。
                              success:function(layero, index){
                                  layer.closeAll("loading");
                              }
                          });
                      }).error(function() {
                          layer.closeAll();
                          layer.msg("加载失败！",{icon:2});
                      });
                  }
              }( grids[i] )));
              // 记录数据，用于清空
              GridJson[grids[i].id] = rect;
              GridDataJson[grids[i].id] = grids[i];
          }
           ciLayer.addTo(map);
           ciLayer.addLayers(markers);
       }
   }