
   var GridJson = [],GridDataJson =[],_area = "", titleIcon ="", px = 16, polygon="";
   var TargetShelfJson = [],TargetShelfDataJson =[];
   var regBoxGridJson = [],regBoxGridDataJson =[];

   var map_h,map_w;
   var forward = 5500,after = 28000;

   if(wareArea==='D') {//老远东D
       map_h= 27167.4;
       map_w= 5300;
   }else if(wareArea==='A') {//老远东A
       map_h= 27246.6;
       map_w= 5240.4;
   }else if(wareArea.indexOf('FA')>-1||wareArea.indexOf('FB')>-1||wareArea.indexOf('FC')>-1) {//复合F西
       map_h= 16167;
       map_w= 3363.5;
   }else if(wareArea.indexOf('FD')>-1||wareArea.indexOf('FE')>-1||wareArea.indexOf('FF')>-1||wareArea.indexOf('FG')>-1) {//复合F东
       map_h= 27246.6;
       map_w= 5240.4;
   }else if(wareArea==='X') {//新远东X
       map_h= 32716.94;
       map_w= 5024.5;
   }else if(wareArea==='NA') {//新远东（特殊）NA
       map_h= 15056.89;
       map_w= 5125;
   }else if(wareArea==='NB') {//新远东（特殊）NB
       map_h= 15137;
       map_w= 3828;
   }else if(wareArea.indexOf('NC')>-1||wareArea.indexOf('NE')>-1) {//新远东（特殊）NC、NE
       map_h= 43802;
       map_w= 4872;
   }else if(wareArea.indexOf('ND')>-1||wareArea.indexOf('NF')>-1) {//新远东（特殊）ND、NF
       map_h= 44012;
       map_w= 3825;
   }
   var bounds =[[0,0], [map_w, map_h]];
   
   // leaflet.js, 初始化地图
   var map = L.map("map",{
       crs: L.CRS.Simple, // 简单模式
       zoom: -5,           // 初始缩放比例
       minZoom: -8,
       zoomSnap: 0.39,     // 缩放系数
       maxZoom:1
   });
   
   map.fitBounds(bounds);//设置包含可能具有最大缩放级别的给定地理边界的地图视图
   // 地图缩放事件结束后执行
   map.on("zoomend", function(){
	  // alert(map.getZoom());
   });
   
    /**
     *   _______ (x1,y1)
     * |       |
     * |_______|
     * (x0,y0)
     * 
     * y1为区域 右上角点的y坐标轴
     *   grids=[ {
     *      id:1,
     *      area:[[10,10],[20,20]],
     *      color: "blue"       // 如果不设置颜色，默认灰色
     *   } ];
     *
     * */
   function visualization_index_loadGrid(grids,wareArea){
       removeAllGrid();                  // 清空地图数据
       _area && _area.remove();          // 清空地图
       titleIcon && titleIcon.remove();  // 清空图标
       polygon && polygon.remove();      // 清空边框
       creatBox();

       // 根据grids来画库位
       if( grids && grids.length >0 ){
          for(var i = 0; i < grids.length; i++){
              // 记录数据，用于清空
              GridJson[grids[i].id] = L.rectangle(grids[i].area, {
                  color: "#444",
                  fillColor: grids[i].color,
                  weight: 0,
                  fillOpacity: 1
              }).addTo(map);
              GridDataJson[grids[i].id] = grids[i];
          }
       }
   }

   //清空地图数据的方法
   function removeAllGrid(){
	   for(var i in GridJson){
		   GridJson.hasOwnProperty(i) &&   GridJson[i].remove();
       }
	   GridJson = [];
	   GridDataJson = [];
   }

   // 用于创建场景环境
   var creatBox = function(){
        var latlngs = [[0, 0],[map_w, 0],[map_w, map_h],[0,map_h]];  //实际的仓库区域（场景）
        //根据上面的范围画边框
        polygon = L.polygon(latlngs, {color: '#ccc',weight:3, fillOpacity:0}).addTo(map);
        _icon = L.divIcon({
            html: factoryWarehouseAreaName,
            className: 'my-div-icon',
            iconSize:30
        });
        //设置字样的位置
        //titleIcon = L.marker([3500,-3000], { icon: _icon }).addTo(map);
        /*_area  =  L.rectangle([[0,0],[5320,-5320]], {//这里的坐标是定灰色区域大小的
            color: "#ccc",
            fillColor: "#e8e8e8",
            weight: 1,
            fillOpacity: 0.5,
        }).addTo(map);*/
    }
   
   
