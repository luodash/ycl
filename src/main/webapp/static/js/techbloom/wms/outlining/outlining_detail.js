function reloadDetailTableList(){
    $("#grid-table-c").jqGrid('setGridParam',
        {
            url:context_path + '/outLiNing/DetailList',
            postData: {qId:$('#baseInfor #id').val(),queryJsonString:""}
        }
    ).trigger("reloadGrid");
}

//删除详情中的数据
function delDetail(){
    var checkedNum = getGridCheckedNum("#grid-table-c","id");  //选中的数量
    if(checkedNum==0){
        layer.alert("请选择一条记录！");
    }else{
        var ids = jQuery("#grid-table-c").jqGrid('getGridParam', 'selarrrow');
        layer.confirm("确定删除选择的物料",function(){
            $.ajax({
                type:"POST",
                url:context_path + "/outStoragePlan/delDetail?ids="+ids,
                dataType:"json",
                success:function(data){
                    if(data.result){
                        layer.msg(data.msg);
                        reloadDetailTableList();   //重新加载详情列表
                    }else{
                        layer.msg(data.msg);
                    }
                }
            })
        });
    }

}

//提交
function outformSubmitBtn(){
    if($('#baseInfor #id').val()==-1){
        layer.alert("请先保存单据");
        return;
    }else {
        var formdata = $('#baseInfor').serialize();
        //后台修改对应的数据
        layer.confirm("确定提交,提交之后数据不能修改,库存为0的数据将会自动删除", function () {
            $.ajax({
                type:"POST",
                url:context_path+"/outStoragePlan/setOutPlanState",
                dataType:"json",
                data:{id:$('#baseInfor #id').val(),outType:$('#baseInfor #outType').val()},
                data:formdata,
                success:function(data){
                    if(data.result){
                        layer.closeAll();
                        layer.msg("提交成功",{icon:1,time:1200});
                        gridReload();
                    }else{
                        layer.alert(data.msg);
                    }
                }
            })
        });
    }
}
//确认拆分
function querySpilt(){
    var checkedNum = getGridCheckedNum("#grid-table-d","id");  //选中的数量
    if(checkedNum==0){
        layer.alert("请选择一条记录！");
    }else{
        var ids = jQuery("#grid-table-d").jqGrid('getGridParam', 'selarrrow');
        layer.confirm("确定拆分数据",function(){
            $.ajax({
                type:"POST",
                url:context_path + "/outStoragePlan/createOutStorage?ids="+ids+'&id='+$("#baseIn #id").val(),
                dataType:"json",
                success:function(data){
                    if(data.result){
                        layer.alert(data.msg);
                        $("#grid-table-d").jqGrid('setGridParam',
                            {
                                url:context_path + '/outStoragePlan/getSpitlist',
                                postData: {qId:$('#baseIn #id').val(),queryJsonString:""}
                            }
                        ).trigger("reloadGrid");
                    }else{
                        layer.alert(data.msg);
                    }
                }
            })
        });
    }
}