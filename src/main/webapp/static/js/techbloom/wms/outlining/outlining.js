//刷新列表
function gridReload(){
    $("#grid-table").jqGrid('setGridParam',
        {
            url : context_path + '/crossDockPlan/toList',
            postData: {queryJsonString:""} //发送数据  :选中的节点
        }
    ).trigger("reloadGrid");
}

//导出
function exportCrossPlan(){
    var ids = jQuery("#grid-table").jqGrid("getGridParam", "selarrrow");
    $("#hiddenQueryForm #ids").val(ids);
    $("#hiddenQueryForm").submit();
}

//打印
function printCrossPlan(){
    var checkedNum = getGridCheckedNum("#grid-table", "id");
    var id="";
    if (checkedNum == 0) {
        layer.alert("请选择一个要打印的越库计划单！");
        return false;
    }else{
        id = jQuery("#grid-table").jqGrid('getGridParam', 'selarrrow');
    }
    var url = context_path + "/crossDockPlan/printCrossPlan?id=" + id;
    window.open(url);
}

//拆分
function spiltOutPlan(){
    var checkedNum = getGridCheckedNum("#grid-table", "id");
    if (checkedNum == 0) {
        layer.alert("请选择一个要编辑的出库库计划单！");
        return false;
    }else if (checkedNum > 1) {
        layer.alert("只能选择一个出库库计划进行编辑操作！");
        return false;
    }else {
        var id = jQuery("#grid-table").jqGrid('getGridParam', 'selrow');
        var rowData = jQuery("#grid-table").jqGrid('getRowData', id).type;
        if (rowData == '<span style="color:red;font-weight:bold;">未提交</span>') {
            layer.alert("该单据还未提交,不能进行拆分");
            return false;
        } else {
            $.get(context_path + "/outStoragePlan/toSplitOutPlan?id=" + id).done(function (data) {
                layer.open({
                    title: "拆分页面",
                    type: 1,
                    skin: "layui-layer-molv",
                    area: ['780px', '620px'],
                    shade: 0.6, //遮罩透明度
                    moveType: 1, //拖拽风格，0是默认，1是传统拖动
                    anim: 2,
                    content: data
                });
            });
        }
    }
}
function viewDetailList(){
	var checkedNum = getGridCheckedNum("#grid-table","id");
	if(checkedNum == 0)
	{
    	layer.alert("请选择一条要查看的外协入库单！");
    	return false;
    } else if(checkedNum >1){
    	layer.alert("只能选择一条外协入库单进行查看操作！");
    	return false;
    } else {
    	var id = jQuery("#grid-table").jqGrid('getGridParam', 'selrow');
    	$.get( context_path + "/outLiNing/viewDetail?id="+id).done(function(data){
			layer.open({
			    title : "外协入库查看", 
		    	type:1,
		    	skin : "layui-layer-molv",
		    	area : ['780px', '620px'],
		    	shade : 0.6, //遮罩透明度
			    moveType : 1, //拖拽风格，0是默认，1是传统拖动
			    anim : 2,
			    content : data
			});
		});  	   	
    }
}