
//查询
function queryInstoreListByParam(jsonParam){
    //序列化表单：iTsai.form.serialize($('#frm'))
    //反序列化表单：iTsai.form.deserialize($('#frm'),json)
    iTsai.form.deserialize($('#crossdock_list_hiddenQueryForm'),jsonParam);
    var queryParam = iTsai.form.serialize($('#crossdock_list_hiddenQueryForm'));
    var queryJsonString = JSON.stringify(queryParam);         //将json对象转换成json字符串
    //执行查询操作
    $("#crossdock_list_grid-table").jqGrid('setGridParam',
        {
            postData: {queryJsonString:queryJsonString} //发送数据
        }
    ).trigger("reloadGrid");
}
//添加出库单
function addCrossDockOrder(){
    $.get( context_path + "/crossDock/toAddCrossDock?id=-1").done(function(data){
        layer.open({
            title : "越库单添加",
            type:1,
            skin : "layui-layer-molv",
            area : ['780px', '620px'],
            shade : 0.6, //遮罩透明度
            moveType : 1, //拖拽风格，0是默认，1是传统拖动
            anim : 2,
            content : data
        });
    });
}
//编辑
function editCrossDockOrder(){
    var checkedNum = getGridCheckedNum("#crossdock_list_grid-table", "id");
    if (checkedNum == 0) {
        layer.alert("请选择一个要编辑的出库单！");
        return false;
    }else if (checkedNum > 1) {
        layer.alert("只能选择一个出库单进行编辑操作！");
        return false;
    }else{
        var id = jQuery("#crossdock_list_grid-table").jqGrid('getGridParam', 'selrow');
        var rowData = jQuery("#crossdock_list_grid-table").jqGrid('getRowData',id).status;
        if(rowData == '<span style="color:#d15b47;font-weight:bold;">未提交</span>'){
            $.get(context_path + "/crossDock/toAddCrossDock?id="+id).done(function (data) {
                layer.open({
                    title: "出库单编辑",
                    type: 1,
                    skin: "layui-layer-molv",
                    area: ['780px', '620px'],
                    shade: 0.6, //遮罩透明度
                    moveType: 1, //拖拽风格，0是默认，1是传统拖动
                    anim: 2,
                    content: data
                });
            });
        }else{
            layer.alert("只能选择未提交的数据进行编辑");
            return false;
        }
    }
}
//删除出库单
function delCrossDockOrder(){
    var checkedNum = getGridCheckedNum("#crossdock_list_grid-table", "id");
    if (checkedNum == 0) {
        layer.alert("请选择一个要删除的出库单！");
        return false;
    }else{
        var ids = jQuery("#crossdock_list_grid-table").jqGrid('getGridParam', 'selarrrow');
        layer.confirm("确定删除选中的出库单?",function(){
            $.ajax({
                type: "POST",
                url: context_path + "/crossDock/delCrossDock?ids=" + ids,
                dataType: "json",
                success:function(data){
                    if (data.result) {
                        //弹出提示信息
                        layer.msg(data.msg);
                        $("#crossdock_list_grid-table").jqGrid('setGridParam',
                            {
                                postData: {queryJsonString:""} //发送数据
                            }
                        ).trigger("reloadGrid");
                    }else{
                        layer.msg(data.msg);
                    }
                }
            })
        });
    }
}

function gridReload(){
    $("#crossdock_list_grid-table").jqGrid('setGridParam',
        {
            url : context_path + '/crossDock/list.do',
            postData: {queryJsonString:""} //发送数据  :选中的节点
        }
    ).trigger("reloadGrid");
}

//导出数据
function exportCrossDock(){
    var ids = jQuery("#crossdock_list_grid-table").jqGrid("getGridParam", "selarrrow");
    $("#crossdock_list_hiddenQueryForm #crossdock_list_ids").val(ids);
    $("#crossdock_list_hiddenQueryForm").submit();
}

function printOutStorage(){
    var checkedNum = getGridCheckedNum("#crossdock_list_grid-table", "id");
    if (checkedNum == 0) {
        layer.alert("请选择一个要打印的出库单！");
        return false;
    }else{
        id = jQuery("#crossdock_list_grid-table").jqGrid('getGridParam', 'selarrrow');
    }
    var url = context_path + "/crossDock/printCrossDock?id=" + id;
    window.open(url);
}

function comfirmCross(id){
    var rowData = jQuery("#crossdock_list_grid-table").jqGrid('getRowData', id).status;
    if(rowData == '<span style="color:#d15b47;font-weight:bold;">未提交</span>'){
        layer.alert("请先提交单据");
        return false;
    }else{
        layer.confirm("确认开始越库吗?", function () {
            $.ajax({
                type: "POST",
                url: context_path + "/crossDock/comfirmCross?id=" + id,
                dataType: "json",
                success: function (data) {
                    if (data.result) {
                        //弹出提示信息
                        layer.msg(data.msg);
                        $("#crossdock_list_grid-table").jqGrid('setGridParam',
                            {
                                postData: {queryJsonString: ""} //发送数据
                            }
                        ).trigger("reloadGrid");
                    } else {
                        layer.msg(data.msg);
                    }
                }
            })
        });
    }
}

function viewDetailList(){
	var checkedNum = getGridCheckedNum("#crossdock_list_grid-table","id");
	if(checkedNum == 0){
    	layer.alert("请选择一条要查看的出库单！");
    	return false;
    } else if(checkedNum >1){
    	layer.alert("只能选择一条出库单进行查看操作！");
    	return false;
    } else {
    	var id = jQuery("#crossdock_list_grid-table").jqGrid('getGridParam', 'selrow');
    	$.get( context_path + "/crossDock/viewDetail?id="+id).done(function(data){
			layer.open({
			    title : "出库单查看", 
		    	type:1,
		    	skin : "layui-layer-molv",
		    	area : ['750px', '650px'],
		    	shade : 0.6, //遮罩透明度
			    moveType : 1, //拖拽风格，0是默认，1是传统拖动
			    anim : 2,
			    content : data
			});
		});  	   	
    }
}

