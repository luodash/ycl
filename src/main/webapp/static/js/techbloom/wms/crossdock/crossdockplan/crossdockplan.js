//查询的方法
function queryInstoreListByParam(jsonParam){
    //序列化表单：iTsai.form.serialize($('#frm'))
    //反序列化表单：iTsai.form.deserialize($('#frm'),json)
    iTsai.form.deserialize($('#crossdockPlan_list_hiddenQueryForm'),jsonParam);   //将json对象反序列化到列表页面中隐藏的form中
    var queryParam = iTsai.form.serialize($('#crossdockPlan_list_hiddenQueryForm'));
    var queryJsonString = JSON.stringify(queryParam);         //将json对象转换成json字符串
    //执行查询操作
    $("#crossdockPlan_list_grid-table").jqGrid('setGridParam',
        {
            postData: {queryJsonString:queryJsonString} //发送数据
        }
    ).trigger("reloadGrid");
}

//添加界面
function addCrossPlan(){
    $.get( context_path + "/crossDockPlan/toAddCrossPlan?id=-1").done(function(data){
        layer.open({
            title : "越库计划添加",
            type:1,
            skin : "layui-layer-molv",
            area : ['780px', '620px'],
            shade : 0.6, //遮罩透明度
            moveType : 1, //拖拽风格，0是默认，1是传统拖动
            anim : 2,
            content : data
        });
    });
}
//编辑界面
function editCrossPlan(){
    var checkedNum = getGridCheckedNum("#crossdockPlan_list_grid-table", "id");
    if (checkedNum == 0) {
        layer.alert("请选择一个要编辑的越库库计划单！");
        return false;
    }else if (checkedNum > 1) {
        layer.alert("只能选择一个越库库计划进行编辑操作！");
        return false;
    }else {
        var id = jQuery("#crossdockPlan_list_grid-table").jqGrid('getGridParam', 'selrow');
        var rowData = jQuery("#crossdockPlan_list_grid-table").jqGrid('getRowData', id).type;
        if (rowData != '<span style="color:red;font-weight:bold;">未提交</span>') {
            layer.alert("只能选择状态为未提交的数据进行编辑操作");
            return false;
        } else {
            $.get(context_path + "/crossDockPlan/toAddCrossPlan?id=" + id).done(function (data) {
                layer.open({
                    title: "越库计划编辑",
                    type: 1,
                    skin: "layui-layer-molv",
                    area: ['780px', '620px'],
                    shade: 0.6, //遮罩透明度
                    moveType: 1, //拖拽风格，0是默认，1是传统拖动
                    anim: 2,
                    content: data
                });
            });
        }
    }
}
//删除越库计划表
function delCrossPlan(){
    var checkedNum = getGridCheckedNum("#crossdockPlan_list_grid-table", "id");
    if (checkedNum == 0) {
        layer.alert("请选择一个要删除的越库库计划单！");
        return false;
    }else{
        var ids = jQuery("#grid-table").jqGrid('getGridParam', 'selarrrow');
        layer.confirm("确定删除选中的越库计划单?",function(){
            $.ajax({
                type: "POST",
                url: context_path + "/crossDockPlan/delCrossPlan?ids=" + ids,
                dataType: "json",
                success:function(data){
                    if (data.result) {
                        layer.closeAll();
                        //弹出提示信息
                        layer.msg(data.msg);
                        $("#crossdockPlan_list_grid-table").jqGrid('setGridParam',
                            {
                                postData: {queryJsonString:""} //发送数据
                            }
                        ).trigger("reloadGrid");
                    }else{
                        layer.msg(data.msg);
                    }
                }
            })
        });
    }
}
//刷新列表
function gridReload(){
    $("#crossdockPlan_list_grid-table").jqGrid('setGridParam',
        {
            url : context_path + '/crossDockPlan/toList',
            postData: {queryJsonString:""} //发送数据  :选中的节点
        }
    ).trigger("reloadGrid");
}

//导出
function exportCrossPlan(){
    var ids = jQuery("#crossdockPlan_list_grid-table").jqGrid("getGridParam", "selarrrow");
    $("#crossdockPlan_list_hiddenQueryForm #crossdockPlan_list_ids").val(ids);
    $("#crossdockPlan_list_hiddenQueryForm").submit();
}

//打印
function printCrossPlan(){
    var checkedNum = getGridCheckedNum("#crossdockPlan_list_grid-table", "id");
    var id="";
    if (checkedNum == 0) {
        layer.alert("请选择一个要打印的越库计划单！");
        return false;
    }else{
        id = jQuery("#crossdockPlan_list_grid-table").jqGrid('getGridParam', 'selarrrow');
    }
    var url = context_path + "/crossDockPlan/printCrossPlan?id=" + id;
    window.open(url);
}

//拆分
function spiltOutPlan(){
    var checkedNum = getGridCheckedNum("#crossdockPlan_list_grid-table", "id");
    if (checkedNum == 0) {
        layer.alert("请选择一个要编辑的出库库计划单！");
        return false;
    }else if (checkedNum > 1) {
        layer.alert("只能选择一个出库库计划进行编辑操作！");
        return false;
    }else {
        var id = jQuery("#crossdockPlan_list_grid-table").jqGrid('getGridParam', 'selrow');
        var rowData = jQuery("#crossdockPlan_list_grid-table").jqGrid('getRowData', id).type;
        if (rowData == '<span style="color:red;font-weight:bold;">未提交</span>') {
            layer.alert("该单据还未提交,不能进行拆分");
            return false;
        } else {
            $.get(context_path + "/outStoragePlan/toSplitOutPlan?id=" + id).done(function (data) {
                layer.open({
                    title: "拆分页面",
                    type: 1,
                    skin: "layui-layer-molv",
                    area: ['780px', '620px'],
                    shade: 0.6, //遮罩透明度
                    moveType: 1, //拖拽风格，0是默认，1是传统拖动
                    anim: 2,
                    content: data
                });
            });
        }
    }
}

function viewDetailList(){
	var checkedNum = getGridCheckedNum("#crossdockPlan_list_grid-table","id");
	if(checkedNum == 0){
    	layer.alert("请选择一条要查看的越库计划！");
    	return false;
    } else if(checkedNum >1){
    	layer.alert("只能选择一条越库计划进行查看操作！");
    	return false;
    } else {
    	var id = jQuery("#crossdockPlan_list_grid-table").jqGrid('getGridParam', 'selrow');
    	$.get( context_path + "/crossDockPlan/viewDetail?id="+id).done(function(data){
			layer.open({
			    title : "越库计划查看", 
		    	type:1,
		    	skin : "layui-layer-molv",
		    	area : ['780px', '620px'],
		    	shade : 0.6, //遮罩透明度
			    moveType : 1, //拖拽风格，0是默认，1是传统拖动
			    anim : 2,
			    content : data
			});
		});  	   	
    }
}