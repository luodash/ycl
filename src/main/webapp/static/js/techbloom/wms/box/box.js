var boxCheckType = "";
var boxCheckId = "";

/** 表格刷新 */
function gridReload() {
    _grid.trigger("reloadGrid");  //重新加载表格
}

/**
 * 新增盘点任务
 */
function addBox() {
    $.get(context_path + "/box/toBoxAddView.do").done(function (data) {
        layer.open({
            title: "新增翻包",
            type: 1,
            skin: "layui-layer-molv",
            area: ['780px', '620px'],
            shade: 0.6, //遮罩透明度
            moveType: 1, //拖拽风格，0是默认，1是传统拖动
            anim: 2,
            content: data
        });
    });
}

/**
 * 编辑盘点任务
 */
function editBox() {
    var checkedNum = getGridCheckedNum("#box_grid-table", "id");
    if (checkedNum == 0) {
        layer.alert("请选择一条要编辑的翻包任务！");
        return false;
    } else if (checkedNum > 1) {
        layer.alert("只能选择一条翻包任务进行编辑操作！");
        return false;
    } else {
        boxCheckId = jQuery("#box_grid-table").jqGrid('getGridParam', 'selrow');
        $.get(context_path + "/box/toBoxAddView.do?id=" + boxCheckId).done(function (data) {
            layer.open({
                title: "翻包修改",
                type: 1,
                skin: "layui-layer-molv",
                area: ['780px', '620px'],
                shade: 0.6, //遮罩透明度
                moveType: 1, //拖拽风格，0是默认，1是传统拖动
                anim: 2,
                content: data
            });
        });
    }
}

/*上架单导出*/
function excelBox(){
    var selectid = jQuery("#box_grid-table").jqGrid('getGridParam', 'selarrrow');
    $("#box_hiddenForm #box_ids").val(selectid);
    $("#box_hiddenForm").submit();
}
function devanBox() {
    var checkedNum = getGridCheckedNum("#box_grid-table", "id");
    if (checkedNum == 0) {
        layer.alert("请选择一条要翻包的任务！");
        return false;
    } else if (checkedNum > 1) {
        layer.alert("只能选择一条任务进行翻包操作！");
        return false;
    } else {
        //从数据库中选中拆箱的单据，并刷新装箱单表格
        var ids = jQuery("#box_grid-table").jqGrid('getGridParam', 'selarrrow');
        //弹出确认窗口
        layer.confirm("确定拆除此箱？", function () {
            $.ajax({
                type: "POST",
                url: context_path + "/box/devanBox?ids=" + ids,
                dataType: 'json',
                cache: false,
                success: function (data) {
                    layer.closeAll();
                    if (data.result) {
                        layer.msg(data.message);
                    } else {
                        layer.alert(data.message);
                    }
                    _grid.trigger("reloadGrid");  //重新加载表格
                }
            });
        });
    }
}

//时间空间初始化
$(".date-picker").datetimepicker({format: 'YYYY-MM-DD HH:mm:ss', useMinutes: true, useSeconds: true});

/**
 * 获取盘点方式
 */
$("#box_boxedType").select2({
    placeholder: "选择翻包类型",
    minimumInputLength: 0, //至少输入n个字符，才去加载数据
    allowClear: true, //是否允许用户清除文本信息
    delay: 250,
    formatNoMatches: "没有结果",
    formatSearching: "搜索中...",
    formatAjaxError: "加载出错啦！",
    ajax: {
        url: context_path + "/box/getBoxedTypeList",
        type: "POST",
        dataType: 'json',
        delay: 250,
        data: function (term, pageNo) { //在查询时向服务器端传输的数据
            term = $.trim(term);
            return {
                queryString: term, //联动查询的字符
                pageSize: 15, //一次性加载的数据条数
                pageNo: pageNo, //页码
                time: new Date()
                //测试
            }
        },
        results: function (data, pageNo) {
            var res = data.result;
            if (res.length > 0) { //如果没有查询到数据，将会返回空串
                var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                return {
                    results: res,
                    more: more
                };
            } else {
                return {
                    results: {}
                };
            }
        },
        cache: true
    }

});

$("#box_boxedType").on("change", function () {
    $("#box_instorageId").select2("val", "");
    $("#box_locationId").select2("val", "");
});

$('#box_locationId').select2({
    placeholder: "请选择库位",//文本框的提示信息
    minimumInputLength: 0, //至少输入n个字符，才去加载数据
    allowClear: true, //是否允许用户清除文本信息
    multiple: false,
    closeOnSelect: false,
    ajax: {
        url: context_path + '/box/getBoxLocationList',
        dataType: 'json',
        delay: 250,
        data: function (term, pageNo) { //在查询时向服务器端传输的数据
            term = $.trim(term);
            selectParam = term;
            return {
                /* docId : $("#baseInfor #id").val(), */
                queryString: term, //联动查询的字符
                pageSize: 15, //一次性加载的数据条数
                pageNo: pageNo, //页码
                time: new Date(),
                content: $("#box_boxedType").val()
                //测试
            }
        },
        results: function (data, pageNo) {
            var res = data.result;
            if (res.length > 0) { //如果没有查询到数据，将会返回空串
                var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                return {
                    results: res,
                    more: more
                };
            } else {
                return {
                    results: {
                        "id": "0",
                        "text": "没有更多结果"
                    }
                };
            }

        },
        cache: true
    }
});

/**
 * 初始化盘点人员下拉列表
 */
$("#box_boxUserId").select2({
    placeholder: "选择翻包人员",
    minimumInputLength: 0, //至少输入n个字符，才去加载数据
    allowClear: true, //是否允许用户清除文本信息
    delay: 250,
    formatNoMatches: "没有结果",
    formatSearching: "搜索中...",
    formatAjaxError: "加载出错啦！",
    ajax: {
        url: context_path + "/ASNmanage/getSelectUser",
        type: "POST",
        dataType: 'json',
        delay: 250,
        data: function (term, pageNo) { //在查询时向服务器端传输的数据
            term = $.trim(term);
            return {
                queryString: term, //联动查询的字符
                pageSize: 15, //一次性加载的数据条数
                pageNo: pageNo, //页码
                time: new Date()
                //测试
            }
        },
        results: function (data, pageNo) {
            var res = data.result;
            if (res.length > 0) { //如果没有查询到数据，将会返回空串
                var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                return {
                    results: res,
                    more: more
                };
            } else {
                return {
                    results: {}
                };
            }
        },
        cache: true
    }

});
/**
 * 根据
 */
$("#box_stockCheckTypeNo").on("change", function () {
    stockCheckType = this.value;
});


/**
 * 盘点任务新增表单验证
 */
$("#box_boxForm").validate({
    ignore: "",
    rules: {
        "box_boxedType": {
            required: true,
        },
        "box_boxTime": {
            required: true,
        },
        "box_boxUserId": {
            required: true,
        }
    },
    messages: {
        "box_boxedType": {
            required: "装箱类型必选!",
        },
        "box_boxTime": {
            required: "装箱时间必填!",
        },
        "box_boxUserId": {
            required: "装箱人员必填!",
        }
    },
    errorClass: "help-inline",
    errorElement: "span",
    highlight:function(element, errorClass, validClass) {
        $(element).parents('.control-group').addClass('error');
    },
    unhighlight: function(element, errorClass, validClass) {
        $(element).parents('.control-group').removeClass('error');
    }
});

/**
 * 保存盘点任务
 */
$("#box_formSave").click(function () {
    var bean = $("#box_boxForm").serialize();
    if ($("#box_boxedType").val() == "WMS_BOXTYPE_OUT") {
        if ($("#box_instorageId").val() === '') {
            layer.msg("库外装箱,入库单据必选!", {icon: 5});
            return ;
        }
    }
    if ($("#box_boxedType").val() == "WMS_BOXTYPE_IN") {
        if ($("#box_locationId").val() === '') {
            layer.msg("库内装箱,装箱库位必选!", {icon: 5});
            return ;
        }
    }
    if ($('#box_boxForm').valid()) {
        saveOrUpdateBox(bean);
    }
});

/**
 * 保存盘点任务
 */
$("#box_formSubmit").click(function () {
    $.ajax({
        url: context_path + "/box/submitBox",
        type: "post",
        data: {boxId: $("#box_boxId").val()},
        dataType: "JSON",
        success: function (data) {
            if (data.result) {
            	layer.closeAll();
                layer.msg("操作成功！", {icon: 1, time: 1200});
                _grid.trigger("reloadGrid");
            } else {
                layer.msg(data.message, {icon: 2, time: 1200});
            }
        }
    })
});

/**
 * 保存修改盘点任务
 * @param bean 盘点信息
 */
function saveOrUpdateBox(bean) {
    $.ajax({
        url: context_path + "/box/saveOrUpdateBox",
        type: "post",
        data: bean,
        dataType: "JSON",
        success: function (data) {
            if (data.result) {
                $("#box_boxId").val(data.id);
                boxCheckId = data.id;
                gridReload();
                _grid_detail.trigger("reloadGrid");
                layer.msg("操作成功！", {icon: 1, time: 1200});
            } else {
                layer.msg(data.message, {icon: 2, time: 1200});
            }
        }
    });
}

/**
 * 删除装箱
 */
function deleteBox() {
    var checkedNum = getGridCheckedNum("#box_grid-table", "id");  //选中的数量
    if (checkedNum == 0) {
        layer.alert("请至少选择一条要删除的装箱信息！");
    } else {
        //从数据库中删除选中的入库单，并刷新入库单表格
        var ids = jQuery("#box_grid-table").jqGrid('getGridParam', 'selarrrow');
        //弹出确认窗口
        layer.confirm("确定删除选中的装箱信息？", function () {
            $.ajax({
                type: "POST",
                url: context_path + "/box/deleteBox?boxCheckIds=" + ids,
                dataType: 'json',
                cache: false,
                success: function (data) {
                    layer.closeAll();
                    if (data.result) {
                        layer.msg("装箱删除成功！", {icon: 1, time: 1000});
                    } else {
                        layer.alert(data.message, {icon: 2, time: 1000});
                    }
                    _grid.trigger("reloadGrid");  //重新加载表格
                }
            });
        });
    }
}

/**
 * 提交盘点任务
 */
function confirmStockCheck() {
    var checkedNum = getGridCheckedNum("#box_grid-table", "id");  //选中的数量
    if (checkedNum == 0) {
        layer.alert("请至少选择一条要提交的盘点任务！");
    } else {
        //从数据库中删除选中的入库单，并刷新入库单表格
        var ids = jQuery("#box_grid-table").jqGrid('getGridParam', 'selarrrow');
        //弹出确认窗口
        layer.confirm("确定提交选中的盘点任务？", function () {
            $.ajax({
                type: "POST",
                url: context_path + "/stockCheck/confirmStockCheck?boxCheckIds=" + ids,
                dataType: 'json',
                cache: false,
                success: function (data) {
                    layer.closeAll();
                    if (data.result) {
                        layer.msg(data.message, {icon: 1, time: 1000});
                    } else {
                        layer.alert(data.message, {icon: 2, time: 1000});
                    }
                    _grid.trigger("reloadGrid");  //重新加载表格
                }
            });
        });
    }
}

/**
 * 审核盘点任务
 */
function auditStockCheck() {
    var checkedNum = getGridCheckedNum("#box_grid-table", "id");  //选中的数量
    if (checkedNum == 0) {
        layer.alert("请至少选择一条要提交的盘点任务！");
    } else {
        //从数据库中删除选中的入库单，并刷新入库单表格
        var ids = jQuery("#box_grid-table").jqGrid('getGridParam', 'selarrrow');
        //弹出确认窗口
        layer.confirm("确定提交选中的盘点任务？", function () {
            $.ajax({
                type: "POST",
                url: context_path + "/stockCheck/auditStockCheck?boxCheckIds=" + ids,
                dataType: 'json',
                cache: false,
                success: function (data) {
                    layer.closeAll();
                    if (data.result) {
                        layer.msg(data.message, {icon: 1, time: 1000});
                    } else {
                        layer.alert(data.message, {icon: 2, time: 1000});
                    }
                    _grid.trigger("reloadGrid");  //重新加载表格
                }
            });
        });
    }
}

//删除货架详情
function delBoxDetail() {
    var checkedNum = getGridCheckedNum("#box_grid-table-c", "id");  //选中的数量
    if (checkedNum == 0) {
        layer.alert("请至少选择一条要删除的装箱详情！");
    } else {
        //从数据库中删除选中的盘点任务，并刷新盘点任务表格
        var stockCheckDetailIds = jQuery("#box_grid-table-c").jqGrid('getGridParam', 'selarrrow');
        //弹出确认窗口
        layer.confirm("确定删除选中的装箱详情？", function () {
            $.ajax({
                url: context_path + '/box/deleteBoxDetail?boxDetailIds=' + stockCheckDetailIds,
                type: "POST",
                data: {id: $("#box_boxCheckId").val()},
                dataType: "JSON",
                success: function (data) {
                    if (data.result) {
                        layer.msg("装箱详情删除成功！", {icon: 1, time: 1000});
                        //重新加载详情表格
                        $("#box_grid-table-c").jqGrid('setGridParam',
                            {
                                postData: {boxCheckId: $("#box_boxCheckId").val()} //发送数据  :选中的节点
                            }
                        ).trigger("reloadGrid");
                    }
                    else {
                        layer.alert(data.message, {icon: 2, time: 1000});
                    }
                }
            });
        });
    }
}

/**
 * 导出盘点任务
 */
function exportStockCheckTask() {
    $("#box_stockCheckNo1").val($("#box_stockCheckNo").val());
    $("#box_exportStockCheckForm").submit();
}


