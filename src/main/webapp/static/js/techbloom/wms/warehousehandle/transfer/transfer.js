/**
 * 新增移库任务
 */
function addTransfer(){
	$.get( context_path + "/transfer/toAdd.do?id=-1").done(function(data){
		layer.open({
		    title : "移库任务添加", 
	    	type:1,
	    	skin : "layui-layer-molv",
	    	area : "750px",
	    	shade : 0.6, //遮罩透明度
		    moveType : 1, //拖拽风格，0是默认，1是传统拖动
		    anim : 2,
		    //url : data.url,
		    content : data
	        /*url: context_path + "/role/toAdd.do",*/
	    	
		});
	});
}
/**
 * 编辑移库任务
 */
function editAllocateOrder(){
	var checkedNum = getGridCheckedNum("#grid-table","id");
	if(checkedNum == 0){
    	layer.alert("请选择一条要编辑的移库任务！");
    	return false;
    } else if(checkedNum >1){
    	layer.alert("只能选择一条移库任务进行编辑操作！");
    	return false;
    } else {
    	var saleId = jQuery("#grid-table").jqGrid('getGridParam', 'selrow'); 
    	var rowDate = jQuery("#grid-table").jqGrid('getRowData', saleId).state;
    	if(rowDate!='<span style="color:#d15b47;font-weight:bold;">未提交</span>'){
    		layer.msg("该单据不可修改",{time:1200,icon:2});
    		return ;
    	}
    	$.get( context_path + "/transfer/toAdd.do?id="+saleId).done(function(data){
    	  layer.open({
    	  title : "移库任务编辑", 
    	  type:1,
    	  skin : "layui-layer-molv",
    	  area : "750px",
    	  shade : 0.6, //遮罩透明度
    	  moveType : 1, //拖拽风格，0是默认，1是传统拖动
    	  anim : 2,
    	  content : data
    	  });
    	 });
    		
    }
}

function infoAllocateOrder(){
    var checkedNum = getGridCheckedNum("#grid-table","id");
    if(checkedNum == 0){
        layer.alert("请选择一条要编辑的移库任务！");
        return false;
    } else if(checkedNum >1){
        layer.alert("只能选择一条移库任务进行编辑操作！");
        return false;
    } else {
        var saleId = jQuery("#grid-table").jqGrid('getGridParam', 'selrow');
        var rowDate = jQuery("#grid-table").jqGrid('getRowData', saleId).state;
        if(rowDate!='<span style="color:#d15b47;font-weight:bold;">未提交</span>'){
            layer.msg("该单据不可修改",{time:1200,icon:2});
            return ;
        }
        $.get( context_path + "/transfer/toView.do?id="+saleId).done(function(data){
            layer.open({
                title : "移库任务查看",
                type:1,
                skin : "layui-layer-molv",
                area : "750px",
                shade : 0.6, //遮罩透明度
                moveType : 1, //拖拽风格，0是默认，1是传统拖动
                anim : 2,
                content : data
            });
        });

    }
}


/**
 * 删除移库任务
 */
function delOutStorageOrder(){
	var checkedNum = getGridCheckedNum("#grid-table","id");  //选中的数量
	if(checkedNum==0){
		layer.alert("请至少选择一条要删除的移库任务！");
	}else{
		//从数据库中删除选中的入库单，并刷新入库单表格
		var ids = jQuery("#grid-table").jqGrid('getGridParam', 'selarrrow');
		//弹出确认窗口
		layer.confirm("确定删除选中的移库任务？", function() {
    		$.ajax({
    			type : "POST",
    			url:context_path + "/transfer/delete?ids="+ids,
    			dataType : 'json',
    			cache : false,
    			success : function(data) {
    				layer.closeAll();
    				if(Boolean(data.result)){
						layer.msg("移库任务删除成功！", {icon: 1,time:1000});
    				}else{
    					layer.msg("单据已提交，不可删除！", {icon: 2,time:1000});
    				}
    				_grid.trigger("reloadGrid");  //重新加载表格
    			}
    		});
    	});
	} 
}


/**
 * 打开查询界面
 */
function openOutstoreListSearchPage(){
	var queryBean = iTsai.form.serialize($('#hiddenQueryForm'));   //获取form中的值：json对象
	var queryJsonString = JSON.stringify(queryBean);         //将json对象转换成json字符串
	Dialog.open({
	    title: "出库单条件查询", 
	    width: 600,  height: '400',
	    url: context_path + "/outStorage/toQueryPage?outstoreString="+queryJsonString,
	    theme : "simple",
		drag : true
	});
}


/**
 * 收货单查询功能:获取查询页面中的值，并将值放入列表页面中隐藏的form
 * @param jsonParam     查询页面传递过来的json对象
 */
function queryInstoreListByParam(jsonParam){
	//序列化表单：iTsai.form.serialize($('#frm'))
	//反序列化表单：iTsai.form.deserialize($('#frm'),json)
	iTsai.form.deserialize($('#hiddenQueryForm'),jsonParam);   //将json对象反序列化到列表页面中隐藏的form中
	var queryParam = iTsai.form.serialize($('#hiddenQueryForm'));
	var queryJsonString = JSON.stringify(queryParam);         //将json对象转换成json字符串
	//执行查询操作
	$("#grid-table").jqGrid('setGridParam', 
			{
				postData: {queryJson:queryJsonString} //发送数据 
			}
	  ).trigger("reloadGrid");
}


/**
 * 显示提示窗口
 * @param msg   显示信息
 * @param delay 持续时间,结束之后窗口消失
 */
function showTipMsg(msg,delay){
	Dialog.tip( msg, {type: "loading", delay: delay} );
}

/** 表格刷新 */
function gridReload()
{
	 _grid.trigger("reloadGrid");  //重新加载表格  
}
//开始移库
function startTran(){
	var checkedNum = getGridCheckedNum("#grid-table","id");
	if(checkedNum == 0){
    	layer.alert("请选择一条移库任务！");
    	return false;
    } else if(checkedNum >1){
    	layer.alert("只能选择一条移库任务进行操作！");
    	return false;
    } else {
    	var saleId = jQuery("#grid-table").jqGrid('getGridParam', 'selrow'); 
    	var rowDate = jQuery("#grid-table").jqGrid('getRowData', saleId).state;
    	if(rowDate=='<span style="color:#d15b47;font-weight:bold;">未提交</span>'){
    		layer.msg("该单据为提交，不可开始移库",{time:1200,icon:2});
    		return ;
    	}
    	$.get( context_path + "/transfer/toStartTran.do?id="+saleId).done(function(data){
    	  layer.open({
    	  title : "移库任务操作", 
    	  type:1,
    	  skin : "layui-layer-molv",
    	  area : ['750px', '650px'],
    	  shade : 0.6, //遮罩透明度
    	  moveType : 1, //拖拽风格，0是默认，1是传统拖动
    	  anim : 2,
    	  content : data
    	  });
    	 });    		
    }
}
//移库确认
function completeTran(){
	var checkedNum = getGridCheckedNum("#grid-table","id");
	if(checkedNum == 0){
    	layer.alert("请选择一条移库任务！");
    	return false;
    } else if(checkedNum >1){
    	layer.alert("只能选择一条移库任务进行操作！");
    	return false;
    } else {
    	var saleId = jQuery("#grid-table").jqGrid('getGridParam', 'selrow'); 
    	var rowDate = jQuery("#grid-table").jqGrid('getRowData', saleId).state;
    	if(rowDate=='<span style="color:#d15b47;font-weight:bold;">未提交</span>'){
    		layer.msg("该单据为提交，不可开始移库",{time:1200,icon:2});
    		return ;
    	}
    	$.get( context_path + "/transfer/toCompleteTran.do?id="+saleId).done(function(data){
    	  layer.open({
    	  title : "移库任务操作", 
    	  type:1,
    	  skin : "layui-layer-molv",
    	  area : ['750px', '650px'],
    	  shade : 0.6, //遮罩透明度
    	  moveType : 1, //拖拽风格，0是默认，1是传统拖动
    	  anim : 2,
    	  content : data
    	  });
       });   		
    }
}

