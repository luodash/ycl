var stockCheckType = "";
var lossOverId = "";

/** 表格刷新 */
function gridReload() {
    _grid.trigger("reloadGrid");  //重新加载表格
}

/**
 * 新增盘点结果
 */
function addStockLossOver() {
    $.get(context_path + "/stockLossOver/toStockLossAndOverAddView.do").done(function (data) {
        layer.open({
            title: "新增盘点结果",
            type: 1,
            skin: "layui-layer-molv",
            area: ['750px', '620px'],
            shade: 0.6, //遮罩透明度
            moveType: 1, //拖拽风格，0是默认，1是传统拖动
            anim: 2,
            content: data
        });
    });
}

/**
 * 编辑盘点记录
 */
function editStockLossOver() {
    var checkedNum = getGridCheckedNum("#grid-table", "id");
    if (checkedNum == 0) {
        layer.alert("请选择一条要编辑的盘点结果！");
        return false;
    } else if (checkedNum > 1) {
        layer.alert("只能选择一条盘点结果任进行编辑操作！");
        return false;
    } else {
        lossOverId = jQuery("#grid-table").jqGrid('getGridParam', 'selrow');
        $.get(context_path + "/stockLossOver/toStockLossAndOverAddView.do?id=" + lossOverId).done(function (data) {
            layer.open({
                title: "盘点记录编辑",
                type: 1,
                skin: "layui-layer-molv",
                area: ['750px', '620px'],
                shade: 0.6, //遮罩透明度
                moveType: 1, //拖拽风格，0是默认，1是传统拖动
                anim: 2,
                content: data
            });
        });
    }
}

//时间空间初始化
$(".date-picker").datetimepicker({format: 'YYYY-MM-DD HH:mm:ss', useMinutes: true, useSeconds: true});

/**
 * 获取盘点任务
 */
$("#stockCheckId").select2({
    placeholder: "选择盘点任务",
    minimumInputLength: 0, //至少输入n个字符，才去加载数据
    allowClear: true, //是否允许用户清除文本信息
    delay: 250,
    formatNoMatches: "没有结果",
    formatSearching: "搜索中...",
    formatAjaxError: "加载出错啦！",
    ajax: {
        url: context_path + "/stockLossOver/getStockCheckList",
        type: "POST",
        dataType: 'json',
        delay: 250,
        data: function (term, pageNo) { //在查询时向服务器端传输的数据
            term = $.trim(term);
            return {
                queryString: term, //联动查询的字符
                pageSize: 15, //一次性加载的数据条数
                pageNo: pageNo, //页码
                time: new Date()
                //测试
            }
        },
        results: function (data, pageNo) {
            var res = data.result;
            if (res.length > 0) { //如果没有查询到数据，将会返回空串
                var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                return {
                    results: res,
                    more: more
                };
            } else {
                return {
                    results: {}
                };
            }
        },
        cache: true
    }

});

/**
 * 初始化盘点人员下拉列表
 */
$("#lossOverUserId").select2({
    placeholder: "选择盘点人员",
    minimumInputLength: 0, //至少输入n个字符，才去加载数据
    allowClear: true, //是否允许用户清除文本信息
    delay: 250,
    formatNoMatches: "没有结果",
    formatSearching: "搜索中...",
    formatAjaxError: "加载出错啦！",
    ajax: {
        url: context_path + "/ASNmanage/getSelectUser",
        type: "POST",
        dataType: 'json',
        delay: 250,
        data: function (term, pageNo) { //在查询时向服务器端传输的数据
            term = $.trim(term);
            return {
                queryString: term, //联动查询的字符
                pageSize: 15, //一次性加载的数据条数
                pageNo: pageNo, //页码
                time: new Date()
                //测试
            }
        },
        results: function (data, pageNo) {
            var res = data.result;
            if (res.length > 0) { //如果没有查询到数据，将会返回空串
                var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                return {
                    results: res,
                    more: more
                };
            } else {
                return {
                    results: {}
                };
            }
        },
        cache: true
    }

});
/**
 * 根据
 */
$("#stockCheckTypeNo").on("change", function () {
    stockCheckType = this.value;
});


/**
 * 盘点任务新增表单验证
 */
$("#lossOverForm").validate({
    ignore: "",
    rules: {
        "stockCheckId": {
            required: true,
        },
        "lossOverTime": {
            required: true,
        },
        "lossOverUserId": {
            required: true,
        }
    },
    messages: {
        "stockCheckId": {
            required: "盘点任务必选!",
        },
        "lossOverTime": {
            required: "处理时间必选!",
        },
        "lossOverUserId": {
            required: "处理人员必填!",
        }
    },
    errorPlacement: function (error, element) {
        layer.msg(error.html(), {icon: 5});
    }
});
/**
 * 盘点任务新增表单验证
 */
$("#stockCheckForm").validate({
    ignore: "",
    rules: {
        "stockCheckMethodName": {
            required: true,
        },
        "stockCheckTime": {
            required: true,
        },
        "stockCheckUserName": {
            required: true,
        }
    },
    messages: {
        "stockCheckMethodName": {
            required: "盘点方式必选!",
        },
        "stockCheckTime": {
            required: "盘点时间必填!",
        },
        "stockCheckUserName": {
            required: "盘点人员必选!",
        }
    },
    errorPlacement: function (error, element) {
        layer.msg(error.html(), {icon: 5});
    }
});

/**
 * 保存盘点任务
 */
$("#formSave").click(function () {
    var bean = $("#lossOverForm").serialize();
    if ($('#lossOverForm').valid()) {
        saveOrUpdateLossOver(bean);
    }
});

/**
 * 保存修改盘点任务
 * @param bean 盘点信息
 */
function saveOrUpdateLossOver(bean) {
    $.ajax({
        url: context_path + "/stockLossOver/saveOrUpdateLossOver",
        type: "post",
        data: bean,
        dataType: "JSON",
        success: function (data) {
            if (data.result) {
                $("#lossOverNo").val(data.lossOverNo);
                $("#lossAndOverId").val(data.id);
                $("#stockCheckTask").val(data.stockCheckTask);
                lossOverId = data.id;
                gridReload();
                $("#grid-table-c").jqGrid('setGridParam',
                    {
                        url: context_path + '/stockLossOver/stockLossOverDetailList?lossOverId=' + $("#lossAndOverId").val(),
                        postData: {queryJsonString: ""} //发送数据  :选中的节点
                    }).trigger("reloadGrid");
                layer.msg("操作成功！", {icon: 1, time: 1200});
            } else {
                layer.msg(data.message, {icon: 2, time: 1200});
            }
        }
    });
}

/**
 * 删除盘点记录
 */
function deleteStockLossOver() {
    var checkedNum = getGridCheckedNum("#grid-table", "id");  //选中的数量
    if (checkedNum == 0) {
        layer.alert("请至少选择一条要删除的盘点记录！");
    } else {
        //从数据库中删除选中的入库单，并刷新入库单表格
        var ids = jQuery("#grid-table").jqGrid('getGridParam', 'selarrrow');
        //弹出确认窗口
        layer.confirm("确定删除选中的盘点记录？", function () {
            $.ajax({
                type: "POST",
                url: context_path + "/stockLossOver/deleteStockLossOver?lossOverId=" + ids,
                dataType: 'json',
                cache: false,
                success: function (data) {
                    layer.closeAll();
                    if (data.message.length==0) {
                        layer.msg("盘点记录删除成功！", {icon: 1, time: 1000});
                    } else {
                        layer.alert(data.message, {icon: 2, time: 1000});
                    }
                    _grid.trigger("reloadGrid");  //重新加载表格
                }
            });
        });
    }
}

/**
 * 盘点结果确认
 */
function comfirmStockLossOver(){
    layer.confirm("确定提交选中的盘点记录？", function () {
        $.ajax({
            type: "POST",
            url: context_path + "/stockLossOver/confirmStockLossOver?lossOverId=" + $("#lossAndOverId").val(),
            dataType: 'json',
            cache: false,
            success: function (data) {
                layer.closeAll();
                if (data.message.length==0) {
                    layer.msg("盘点记录提交成功！", {icon: 1, time: 1000});
                } else {
                    layer.alert(data.message, {icon: 2, time: 1000});
                }
                _grid.trigger("reloadGrid");  //重新加载表格
            }
        });
    })
}

//删除货架详情
function delLossOverDetail() {
    var checkedNum = getGridCheckedNum("#grid-table-c", "id");  //选中的数量
    if (checkedNum == 0) {
        layer.alert("请至少选择一条要删除的盘点结果详情！");
    } else {
        //从数据库中删除选中的盘点任务，并刷新盘点任务表格
        var lossOverDetailIds = jQuery("#grid-table-c").jqGrid('getGridParam', 'selarrrow');
        //弹出确认窗口
        layer.confirm("确定删除选中的盘点结果详情？", function () {
            $.ajax({
                url: context_path + '/stockLossOver/delLossOverDetail?lossOverDetailIds='+lossOverDetailIds,
                type: "POST",
                data: {id: $("#stockCheckId").val()},
                dataType: "JSON",
                success: function (data) {
                    if (data.result) {
                        layer.msg("盘点结果详情删除成功！", {icon: 1, time: 1000});
                        //重新加载详情表格
                        $("#grid-table-c").jqGrid('setGridParam',
                            {
                                postData: {lossOverId: $("#lossAndOverId").val()} //发送数据  :选中的节点
                            }
                        ).trigger("reloadGrid");
                    }
                    else {
                        layer.alert(data.message, {icon: 2, time: 1000});
                    }
                }
            });
        });
    }
}

/*盘点任务导出*/
function excelStocklossOver(){
    var selectid = jQuery("#grid-table").jqGrid('getGridParam', 'selarrrow');
    $("#ids").val(selectid);
    $("#exportStockCheckForm").submit();
}


