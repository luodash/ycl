/**
 * 新增销售订单
 */
function addAllocateOrder(){

	$.get( context_path + "/salemanage/toAdd.do?id=-1").done(function(data){
		layer.open({
		    title : "销售订单添加", 
	    	type:1,
	    	skin : "layui-layer-molv",
	    	area : ['780px', '620px'],
	    	shade : 0.6, //遮罩透明度
		    moveType : 1, //拖拽风格，0是默认，1是传统拖动
		    anim : 2,
		    content : data
		});
	});
}
/**
 * 编辑销售订单
 */
function editAllocateOrder(){
	var checkedNum = getGridCheckedNum("#salemanage_list_grid-table","id");
	if(checkedNum == 0){
    	layer.alert("请选择一条要编辑的销售订单！");
    	return false;
    } else if(checkedNum >1){
    	layer.alert("只能选择一条销售订单进行编辑操作！");
    	return false;
    } else {
    	var saleId = jQuery("#salemanage_list_grid-table").jqGrid('getGridParam', 'selrow'); 
		   $.ajax({
		    	type : "POST",
		    	url:context_path + "/salemanage/getSaleById?saleId="+saleId,
		    	dataType : 'json',
		    	cache : false,
		    	success : function(data) {
		    		 if(data.state==0){
				    	$.get( context_path + "/salemanage/toAdd.do?id="+saleId).done(function(data){
				    	  layer.open({
					    	  title : "销售订单编辑", 
					    	  type:1,
					    	  skin : "layui-layer-molv",
					    	  area : ['750px', '650px'],
					    	  shade : 0.6, //遮罩透明度
					    	  moveType : 1, //拖拽风格，0是默认，1是传统拖动
					    	  anim : 2,
					    	  content : data
				    	  });
				    	 });
			    	}else{
			    		layer.msg("只有未审核的单据可以编辑",{time:1200,icon:2});
			    	}
		    	}
			});	
    }
}

function addAllocateOrder(){
	$.get( context_path + "/salemanage/toAdd.do?id=-1").done(function(data){
		layer.open({
			title : "销售订单添加", 
			type:1,
			skin : "layui-layer-molv",
			area : ['780px', '620px'],
			shade : 0.6, //遮罩透明度
		    moveType : 1, //拖拽风格，0是默认，1是传统拖动
			anim : 2,
			content : data
		});
	});
}
/**
 * 生产出库计划
 */
function outPlan(){
	var checkedNum = getGridCheckedNum("#salemanage_list_grid-table","id");
	if(checkedNum == 0){
    	layer.alert("请选择一条销售订单！");
    	return false;
    }else if(checkedNum >1){
    	layer.alert("只能选择一条进行操作！");
    	return false;
    } else {
    	var saleId = jQuery("#salemanage_list_grid-table").jqGrid('getGridParam', 'selrow'); 
    	$.ajax({
			type : "POST",
			url:context_path + "/salemanage/getSaleById?saleId="+saleId,
			dataType : 'json',
			cache : false,
			success : function(data) {
				if(data.state==1||data.state==2||data.state==4){
				$.get( context_path + "/salemanage/toOutPlan.do?saleId="+saleId).done(function(data){
			    	  layer.open({
				    	  title : "出库计划生成", 
				    	  type:1,
				    	  skin : "layui-layer-molv",
				    	  area : ['750px', '450px'],
				    	  shade : 0.6, //遮罩透明度
				    	  moveType : 1, //拖拽风格，0是默认，1是传统拖动
				    	  anim : 2,
				    	  content : data
			    	  });
			    	 });
				}else{
					layer.alert("该销售订单不能进行拆分，请确认订单状态");
				}
			}
		});
    }
}



/**
 * 删除销售订单
 */
function delOutStorageOrder(){
	var checkedNum = getGridCheckedNum("#salemanage_list_grid-table","id");  //选中的数量
	if(checkedNum==0){
		layer.alert("请至少选择一条要删除的销售订单！");
	}else{
		//从数据库中删除选中的入库单，并刷新入库单表格
		var ids = jQuery("#salemanage_list_grid-table").jqGrid('getGridParam', 'selarrrow');
		//弹出确认窗口
		layer.confirm("确定删除选中的销售订单？", function() {
    		$.ajax({
    			type : "POST",
    			url:context_path + "/salemanage/delete?ids="+ids,
    			dataType : 'json',
    			cache : false,
    			success : function(data) {
    				layer.closeAll();
    				if(data.result){
						layer.msg("销售订单删除成功！", {icon: 1,time:1000});
    				}else{
    					layer.msg(data.msg, {icon: 2,time:1000});
    				}
    				_grid.trigger("reloadGrid");  //重新加载表格
    			}
    		});
    	});
	} 
}

//查看出库单详情
function viewOutstorage(){
	var checkedNum = getGridCheckedNum("#salemanage_list_grid-table","id");
	if(checkedNum == 0){
    	Dialog.alert("请选择一条要查看的出库单！");
    	return false;
    } else if(checkedNum >1){
    	Dialog.alert("只能选择一条出库单进行查看操作！");
    	return false;
    } else {
    	var Instorageid = jQuery("#salemanage_list_grid-table").jqGrid('getGridParam', 'selrow');
    	Dialog.open({
		    title: "出库单查看", 
		    width: 1000,  height: '800',
		    url: context_path + "/outStorage/viewOutStorageDetail?outStoreID="+Instorageid,
		    theme : "simple",
			drag : true
		});
    }
}

/**
 * 销售单查询功能:获取查询页面中的值，并将值放入列表页面中隐藏的form
 * @param jsonParam     查询页面传递过来的json对象
 */
function queryInstoreListByParam(jsonParam){
	iTsai.form.deserialize($('#salemanage_list_hiddenQueryForm'),jsonParam);   //将json对象反序列化到列表页面中隐藏的form中
	var queryParam = iTsai.form.serialize($('#salemanage_list_hiddenQueryForm'));
	var queryJsonString = JSON.stringify(queryParam);         //将json对象转换成json字符串
	//执行查询操作
	$("#salemanage_list_grid-table").jqGrid('setGridParam', 
			{
				postData: {queryJson:queryJsonString} //发送数据 
			}
	  ).trigger("reloadGrid");
}


/**
 * 显示提示窗口
 * @param msg   显示信息
 * @param delay 持续时间,结束之后窗口消失
 */
function showTipMsg(msg,delay){
	Dialog.tip( msg, {type: "loading", delay: delay} );
}

/** 表格刷新 */
function gridReload(){
	 _grid.trigger("reloadGrid");  //重新加载表格  
}
//审核
function auditOrder(){
	var checkedNum = getGridCheckedNum("#salemanage_list_grid-table","id");  //选中的数量
	if(checkedNum==0){
		layer.alert("请至少选择一条要审核的销售订单！");
	}else{
		//从数据库中删除选中的入库单，并刷新入库单表格
		var ids = jQuery("#salemanage_list_grid-table").jqGrid('getGridParam', 'selarrrow');
		//弹出确认窗口
		layer.confirm("确定选中的订单通过审核吗？", function() {
    		$.ajax({
    			type : "POST",
    			url:context_path + "/salemanage/auditOrder?ids="+ids,
    			dataType : 'json',
    			cache : false,
    			success : function(data) {
    				layer.closeAll();
    				if(data.result){
						layer.msg("销售订单审核成功！", {icon: 1,time:1000});
    				}else{
    					layer.alert(data.msg);
    				}
    				_grid.trigger("reloadGrid");  //重新加载表格
    			}
    		});
    	});
	} 
}

function addPull(){
	$.get( context_path + "/techbloom/wms/materialPull/pullPlan/pull_edit.jsp").done(function(data){
		layer.open({
		    title : "物料拉动添加", 
	    	type:1,
	    	skin : "layui-layer-molv",
	    	area : ['780px', '620px'],
	    	shade : 0.6, //遮罩透明度
		    moveType : 1, //拖拽风格，0是默认，1是传统拖动
		    anim : 2,
		    content : data
		});
	});
}

function toOutPlan(){
	var checkedNum = getGridCheckedNum("#salemanage_list_grid-table","id");
	if(checkedNum == 0){
    	layer.alert("请选择一条拉动计划！");
    	return false;
    }else if(checkedNum >1){
    	layer.alert("只能选择一条进行操作！");
    	return false;
    } else {
    	var saleId = jQuery("#salemanage_list_grid-table").jqGrid('getGridParam', 'selrow'); 
    	$.get( context_path + "/techbloom/wms/materialPull/pullPlan/outstorageplan_edit.jsp").done(function(data){
	    	  layer.open({
		    	  title : "出库计划手动生成", 
		    	  type:1,
		    	  skin : "layui-layer-molv",
		    	  area : ['900px', '650px'],
		    	  shade : 0.6, //遮罩透明度
		    	  moveType : 1, //拖拽风格，0是默认，1是传统拖动
		    	  anim : 2,
		    	  content : data
	    	  });
	    });
    }
}

/**
 * 出库计划自动拆分
 */
function outPlanAuto(){
	var checkedNum = getGridCheckedNum("#salemanage_list_grid-table","id");
	if(checkedNum == 0) {
    	layer.alert("请选择一条要自动生成出库计划的的销售订单！");
    	return false;
    } else {
        var saleIdS = jQuery("#salemanage_list_grid-table").jqGrid('getGridParam', 'selarrrow');
        for (var i = 0; i < saleIdS.length; i++) {
        	var rowData = $("#salemanage_list_grid-table").jqGrid("getRowData",saleIdS[i]);
            //选中行的状态
           if(rowData.state1!='4'){
        	   alert("只有状态为已锁定的订单，才能自动生成出库计划！");
        	   return false;
           }
        }
        
        $.ajax({
			type : "POST",
			url:context_path + "/salemanage/orderAuth?saleIdS="+saleIdS,
			dataType : 'json',
			cache : false,
			success : function(data) {
				if(data.result){
					layer.confirm(data.msg, function() {
			    		$.ajax({
			    			type : "POST",
			    			url:context_path + "/salemanage/outPlanAuto?saleIdS="+saleIdS,
			    			dataType : 'json',
			    			cache : false,
			    			success : function(data) {
			    				if(data.result){
									layer.alert(data.msg, {icon: 1,time:1000});
			    				}else{
			    					layer.msg(data.msg, {icon: 2,time:1000});
			    				}
			    				_grid.trigger("reloadGrid");  //重新加载表格
			    			}
			    		});
			    	});  
					
				}else{
					layer.alert(data.msg);
				}
			}
    	});
    	
    }
}


/**
 * 订单锁定
 */
function checkLock(){
	var checkedNum = getGridCheckedNum("#salemanage_list_grid-table","id");
	if(checkedNum == 0) {
    	layer.alert("请选择要锁定的销售订单！");
    	return false;
    } else {
        var saleIdS = jQuery("#salemanage_list_grid-table").jqGrid('getGridParam', 'selarrrow');
        for (var i = 0; i < saleIdS.length; i++) {
        	var rowData = $("#salemanage_list_grid-table").jqGrid("getRowData",saleIdS[i]);
            //选中行的状态
           if(rowData.state1!='1'){
        	   alert("只有状态是已审核的订单，才能被锁定");
        	   return false;
           }
        }
        
		layer.confirm("是否锁定选中订单",function() {
    		$.ajax({
    			type : "POST",
    			url:context_path + "/checkLock/updateLock/"+saleIdS,
    			dataType : 'json',
    			cache : false,
    			success : function(data) {
					layer.alert(data.msg);
    				_grid.trigger("reloadGrid");  //重新加载表格
    			}
    		});
    	});  
    }
}