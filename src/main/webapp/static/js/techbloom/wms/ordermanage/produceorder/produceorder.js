/**
 * 入库单查询功能:获取查询页面中的值，并将值放入列表页面中隐藏的form
 * @param jsonParam     查询页面传递过来的json对象
 */
function queryInstoreListByParam(jsonParam){
    iTsai.form.deserialize($('#pruduce_order_list_hiddenQueryForm'),jsonParam);   //将json对象反序列化到列表页面中隐藏的form中
    var queryParam = iTsai.form.serialize($('#pruduce_order_list_hiddenQueryForm'));
    var queryJsonString = JSON.stringify(queryParam);         //将json对象转换成json字符串
    //执行查询操作
    $("#pruduce_order_list_grid-table").jqGrid('setGridParam',
        {
            postData: {queryJsonString:queryJsonString} //发送数据
        }
    ).trigger("reloadGrid");
}

/**
 * 跳转到添加界面
 */
function add(){
    {
        $.get( context_path + "/produceOrder/toAddorder?id=-1").done(function(data){
            layer.open({
                title : "生产订单添加",
                type:1,
                skin : "layui-layer-molv",
                area : ['900px', '650px'],
                shade : 0.6, //遮罩透明度
                moveType : 1, //拖拽风格，0是默认，1是传统拖动
                anim : 2,
                content : data
            });
        });
    }
}

/**
 * 跳转到编辑界面
 */
function edit(){
    var checkedNum = getGridCheckedNum("#pruduce_order_list_grid-table", "id");
    if (checkedNum == 0) {
        layer.alert("请选择一个要编辑的生产订单！");
        return false;
    } else if (checkedNum > 1) {
        layer.alert("只能选择一个生成订单进行编辑操作！");
        return false;
    }else {
        var id = jQuery("#pruduce_order_list_grid-table").jqGrid('getGridParam', 'selrow');
        //获取选中行的状态
        var rowDate = jQuery("#pruduce_order_list_grid-table").jqGrid('getRowData', id).state;
        if (rowDate != '<span style="color:red;font-weight:bold;">未审核</span>') {
            layer.alert("只能选择状态为未审核的单据进行编辑");
        } else {
            $.get(context_path + "/produceOrder/toAddorder?id=" + id).done(function (data) {
                layer.open({
                    title: "生产订单编辑",
                    type: 1,
                    skin: "layui-layer-molv",
                    area: ['900px', '650px'],
                    shade: 0.6, //遮罩透明度
                    moveType: 1, //拖拽风格，0是默认，1是传统拖动
                    anim: 2,
                    content: data
                });
            });
        }
    }
}

/**
 * 删除
 */
function del(){
    var checkedNum = getGridCheckedNum("#pruduce_order_list_grid-table", "id");  //选中的数量
    if (checkedNum == 0) {
        layer.alert("请选择一个要删除的订单！");
    }else{
        var ids = jQuery("#pruduce_order_list_grid-table").jqGrid('getGridParam', 'selarrrow');
        layer.confirm("确定删除选中的订单?",function(){
            $.ajax({
                type: "POST",
                url: context_path + "/produceOrder/deleteCode?ids=" + ids,
                dataType: "json",
                success:function(data){
                    if (data.result) {
                        layer.closeAll();
                        //弹出提示信息
                        layer.msg(data.msg);
                        $("#pruduce_order_list_grid-table").jqGrid('setGridParam',
                            {
                                postData: {queryJsonString:""} //发送数据
                            }
                        ).trigger("reloadGrid");
                    }else{
                        layer.msg(data.msg);
                    }
                }
            })
        });
    }
}

function gridReload(){
    $("#pruduce_order_list_grid-table").jqGrid('setGridParam',
        {
            url : context_path + '/produceOrder/toList',
            postData: {queryJsonString:""} //发送数据  :选中的节点
        }
    ).trigger("reloadGrid");
}

function auditOrder(){
    var checkedNum = getGridCheckedNum("#pruduce_order_list_grid-table", "id");  //选中的数量
    if (checkedNum == 0) {
        layer.alert("请选择一个要审核的订单！");
    }else{
        var ids = jQuery("#pruduce_order_list_grid-table").jqGrid('getGridParam', 'selarrrow');
        layer.confirm("确定审核选中的订单?",function(){
            $.ajax({
                type: "POST",
                url: context_path + "/produceOrder/auditOrder?ids=" + ids,
                dataType: "json",
                success: function (data) {
                    if(data){
                        gridReload();//重新加载表格
                        layer.alert("操作成功");
                    }else{
                        layer.alert("操作失败");
                    }
                }
            })
        });
    }
}

//跳转到拆分页面
function spilt(){
    var checkedNum = getGridCheckedNum("#pruduce_order_list_grid-table", "id");
    if (checkedNum == 0) {
        layer.alert("请选择一个要拆分的生产订单！");
        return false;
    } else if (checkedNum > 1) {
        layer.alert("只能选择一个订单进行拆分操作！");
        return false;
    }else{
        var id = jQuery("#pruduce_order_list_grid-table").jqGrid('getGridParam', 'selrow');
        var rowDate = jQuery("#pruduce_order_list_grid-table").jqGrid('getRowData', id).state;
        if (rowDate == '<span style="color:green;font-weight:bold;">已审核</span>' || rowDate == '<span style="color:red;font-weight:bold;">计划中</span>') {
            $.get(context_path + "/produceOrder/toPlan?id=" + id).done(function (data) {
                layer.open({
                    title: "生产订单拆分",
                    type: 1,
                    skin: "layui-layer-molv",
                    area: ['900px', '650px'],
                    shade: 0.6, //遮罩透明度
                    moveType: 1, //拖拽风格，0是默认，1是传统拖动
                    anim: 2,
                    content: data
                });
            });
        }else {
            layer.alert("只能拆分已审核或者计划中的订单");
        }
    }
}

function exportExcel(){
    var selectid = jQuery("#pruduce_order_list_grid-table").jqGrid("getGridParam", "selarrrow");
    $("#pruduce_order_list_ids").val(selectid);
    $("#pruduce_order_list_hiddenQueryForm").submit();
}

function print(){
    var checkedNum = getGridCheckedNum("#pruduce_order_list_grid-table", "id");
    var id="";
    if (checkedNum == 0) {
        layer.alert("请选择一个要打印的生产订单！");
        return false;
    }else{
        id = jQuery("#pruduce_order_list_grid-table").jqGrid('getGridParam', 'selarrrow');
    }
    var url = context_path + "/produceOrder/print?id=" + id;
    window.open(url);
}


