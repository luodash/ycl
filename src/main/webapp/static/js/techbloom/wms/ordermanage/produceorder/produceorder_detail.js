function saveFormInfo(formdata){
    $("#produce_order_split_formSave").attr("disabled","disabled");
    $(window).triggerHandler('resize.jqGrid');
    //保存对应的生产订单
    $.ajax({
        type:"POST",
        url:context_path + "/produceOrder/save",
        data:formdata,
        dataType:"json",
        success:function(data){
            if(data){
                if($('#produce_order_split_baseInfor #produce_order_split_id').val()==-1){
                    $('#produce_order_split_baseInfor #produce_order_split_id').val(data.id);
                    $('#produce_order_split_baseInfor #produce_order_split_orderCode').val(data.orderCode);
                }
                reloadDetailTableList();   //重新加载详情列表
                layer.alert("单据保存成功");
                $("#produce_order_split_formSave").removeAttr("disabled");
                gridReload();
            }else{
                layer.alert("单据保存失败");
            }
        }
    })
}
//刷新详情表
function reloadDetailTableList(){
    $("#produce_order_split_grid-table-c").jqGrid('setGridParam',
        {
            url:context_path + '/produceOrder/getDetail',
            postData: {pId:$('#produce_order_split_baseInfor #produce_order_split_id').val(),queryJsonString:""}
        }
    ).trigger("reloadGrid");
}

//删除
function delDetail(){
    var checkedNum = getGridCheckedNum("#produce_order_split_grid-table-c","id");  //选中的数量
    if(checkedNum==0){
        layer.alert("请选择一条记录！");
    }else{
        var ids = jQuery("#produce_order_split_grid-table-c").jqGrid('getGridParam', 'selarrrow');
        layer.confirm("确定删除选中的产品",function(){
            $.ajax({
                type:"POST",
                url:context_path + "/produceOrder/delInstoreDetail?ids="+ids,
                dataType:"json",
                success:function(data){
                    if(data.result){
                        reloadDetailTableList();   //重新加载详情列表
                        layer.msg(data.msg);
                    }else{
                        layer.msg(data.msg);
                    }
                }
            })
        });
    }
}

function outPlan(){
    var checkedNum = getGridCheckedNum("#produce_order_split_grid-table-c","id");  //选中的数量
    if(checkedNum==0){
        layer.alert("请选择一条记录！");
    }else{
        var ids = jQuery("#produce_order_split_grid-table-c").jqGrid('getGridParam', 'selarrrow');
        var pId = $("#produce_order_split_baseInfor #produce_order_split_id").val()
        alert(pId);
        $.get( context_path + "/produceOrder/toPlan?ids="+ids+'&pId='+pId).done(function(data){
            layer.open({
                title : "编辑生成出库计划",
                type:1,
                skin : "layui-layer-molv",
                area : ['900px', '650px'],
                shade : 0.6, //遮罩透明度
                moveType : 1, //拖拽风格，0是默认，1是传统拖动
                anim : 2,
                content : data
            });
        });
    }
}
//生成出库计划单
function queryOk(){
    var checkedNum = getGridCheckedNum("#produce_order_split_grid-table-d","id");  //选中的数量
    if(checkedNum==0){
        layer.alert("请选择一条记录！");
    }else{
        var ids = jQuery("#produce_order_split_grid-table-d").jqGrid('getGridParam', 'selarrrow');
        layer.confirm("确认生成出库计划单?",function(){
            $.ajax({
                type:"POST",
                url:context_path + "/produceOrder/createOutPlan?ids="+ids+'&pId='+$("#produce_order_split_baseInfor #produce_order_split_id").val(),
                dataType:"json",
                success:function(data){
                    if(data.result){
                        layer.alert(data.msg);
                        $("#produce_order_split_grid-table-d").jqGrid('setGridParam',
                            {
                                url:context_path + '/produceOrder/getSplitDetail',
                                postData: {pId:$('#produce_order_split_baseInfor #produce_order_split_id').val()}
                            }
                        ).trigger("reloadGrid");
                    }else{
                        layer.alert(data.msg);
                    }
                }
            })
        });
    }
}
