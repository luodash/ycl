/**入库单添加*/
function add() 
{
	$.get( context_path + "/instorage/toAddInstorage?id=-1").done(function(data){
		layer.open({
		    title : "入库单添加", 
	    	type:1,
	    	skin : "layui-layer-molv",
	    	area : ['900px', '650px'],
	    	shade : 0.6, //遮罩透明度
		    moveType : 1, //拖拽风格，0是默认，1是传统拖动
		    anim : 2,
		    content : data
		});
	});  	
}

/** 入库单编辑  */
function editInstorage()
{
	var checkedNum = getGridCheckedNum("#grid-table","id");
	if(checkedNum == 0)
	{
    	layer.alert("请选择一条要编辑的入库单！");
    	return false;
    } else if(checkedNum >1){
    	layer.alert("只能选择一条入库单进行编辑操作！");
    	return false;
    } else {
    	var Instorageid = jQuery("#grid-table").jqGrid('getGridParam', 'selrow');
    	inEditDiv = $.ajax({
    		type:"POST",
    		url:context_path + "/instorage/isDropDoc",
    		data:{docId:Instorageid},
    		dataType:"json",
    		success:function(data){
    			if(data){
    				//是废弃的单据
    				layer.alert("该入库单为废弃单据，不可再做修改！");
    			}else{
    				//根据主键获取入库单信息
    		    	$.ajax({
    		    		type:"POST",
    		    		url:context_path + "/instorage/getInstoreByID",
    		    		data:{InstoreID:Instorageid},
    		    		dataType:"json",
    		    		success:function(data){
    		    			if(data.result){
    		    				if(data.returnData.instoreStatus==2){
    		    					layer.alert("该入库单已审核，不可再做修改！<br>但是你还是可以通过查看功能对该入库单进行查看");
    		    					return;
    		    				}
    		    				if(data.returnData.instoreStatus==1){
    		    					layer.alert("该入库单已提交，不可再做修改！<br>但是你还是可以通过查看功能对该入库单进行查看");
    		    					return;
    		    				}
    		    			}
    		    			$.get( context_path + "/instorage/toAddInstorage?id="+Instorageid).done(function(data){
    		    				layer.open({
    		    				    title : "入库单编辑", 
    		    			    	type:1,
    		    			    	skin : "layui-layer-molv",
    		    			    	area : ['900px', '650px'],
    		    			    	shade : 0.6, //遮罩透明度
    		    				    moveType : 1, //拖拽风格，0是默认，1是传统拖动
    		    				    anim : 2,
    		    				    content : data
    		    				});
    		    			});  	
    		    		}
    		    	});
    			}
    		}
    	});
    }
}

/** 入库单删除  */
function delInstorage(){
	var checkedNum = getGridCheckedNum("#grid-table","id");  //选中的数量
	if(checkedNum==0){
		layer.alert("请至少选择一条要删除的入库单！");
	}else{
		//从数据库中删除选中的入库单，并刷新入库单表格
		var ids = jQuery("#grid-table").jqGrid('getGridParam', 'selarrrow');
		//弹出确认窗口
		layer.confirm( '确定删除选中的入库单？', 
            function(){
				$.ajax({
					type:"POST",
					url:context_path + "/instorage/delInstorage?ids="+ids,
					dataType:"json",
					success:function(data){
						if(Boolean(data.result)){
							if(data.returnData!=""){
								layer.alert("其中"+data.returnData+"不可以被删除！<br><span>注：已审核或已提交的单据不可以再被修改和删除</span>");
							}else{
								//弹出提示信息
								showTipMsg("入库单删除成功！",1200);
							}
							gridReload();//重新加载表格 
						}else{
							showTipMsg("入库单删除失败！",1200);
						}
					}
				});
            }
        );
		
	}
	
}

/*查看入库单详情*/
function viewInstorage(){
	var checkedNum = getGridCheckedNum("#grid-table","id");
	if(checkedNum == 0)
	{
    	layer.alert("请选择一条要查看的入库单！");
    	return false;
    } else if(checkedNum >1){
    	layer.alert("只能选择一条入库单进行查看操作！");
    	return false;
    } else {
    	var Instorageid = jQuery("#grid-table").jqGrid('getGridParam', 'selrow');
		$.get( context_path + "/instorage/viewInstorageDetail?instoreID="+Instorageid).done(function(data){
			layer.open({
			    title : "入库单查看", 
		    	type:1,
		    	skin : "layui-layer-molv",
		    	area : ['850px', '650px'],
		    	shade : 0.6, //遮罩透明度
			    moveType : 1, //拖拽风格，0是默认，1是传统拖动
			    anim : 2,
			    content : data
			});
		});  	
    }
}
/**
 * 条码打印
 */
function printCode(){
	var checkedNum = getGridCheckedNum("#grid-table","id");
	if(checkedNum == 0)
	{
    	layer.alert("请选择一条要打印条码的入库单！");
    	return false;
    } else if(checkedNum >1){
    	layer.alert("只能选择一条入库单打印条码！");
    	return false;
    } else {
    	var Instorageid = jQuery("#grid-table").jqGrid('getGridParam', 'selrow');
		$.get( context_path + "/instorage/toPrintCode?instoreID="+Instorageid).done(function(data){
			layer.open({
			    title : "物料条码打印", 
		    	type:1,
		    	skin : "layui-layer-molv",
		    	area : ['850px', '450px'],
		    	shade : 0.6, //遮罩透明度
			    moveType : 1, //拖拽风格，0是默认，1是传统拖动
			    anim : 2,
			    content : data
			});
		});  	
    }
}
/**
 * 打开查询界面
 */
function openInstoreListSearchPage(){
	var queryBean = iTsai.form.serialize($('#hiddenQueryForm'));   //获取form中的值：json对象
	var queryJsonString = JSON.stringify(queryBean);         //将json对象转换成json字符串
	$.get( context_path + "/instorage/toQueryPage?instoreString="+queryJsonString).done(function(data){
		layer.open({
		    title : "入库单条件查询", 
	    	type:1,
	    	skin : "layui-layer-molv",
	    	area : ['600px', '400px'],
	    	shade : 0.6, //遮罩透明度
		    moveType : 1, //拖拽风格，0是默认，1是传统拖动
		    anim : 2,
		    content : data
		});
	});  		
}


/**
 * 入库单查询功能:获取查询页面中的值，并将值放入列表页面中隐藏的form
 * @param jsonParam     查询页面传递过来的json对象
 */
function queryInstoreListByParam(jsonParam){
	//序列化表单：iTsai.form.serialize($('#frm'))
	//反序列化表单：iTsai.form.deserialize($('#frm'),json)
	iTsai.form.deserialize($('#hiddenQueryForm'),jsonParam);   //将json对象反序列化到列表页面中隐藏的form中
	var queryParam = iTsai.form.serialize($('#hiddenQueryForm'));
	var queryJsonString = JSON.stringify(queryParam);         //将json对象转换成json字符串
	//执行查询操作
	$("#grid-table").jqGrid('setGridParam', 
			{
				postData: {queryJsonString:queryJsonString} //发送数据 
			}
	  ).trigger("reloadGrid");
}


/** 表格刷新 */
function gridReload()
{
	$("#grid-table").jqGrid('setGridParam', 
			{
		url : context_path + '/instorage/instorageList',
				postData: {queryJsonString:""} //发送数据  :选中的节点
			}
	  ).trigger("reloadGrid"); 
}
/**
 * 导出Excel*/
function toExcel(){
	var idd = jQuery("#grid-table").jqGrid('getGridParam', 'selarrrow');
	$("#ids").val(idd);
	$("#hiddenForm").submit();
}
/**
 * 打印*/
function printList() {
	var ids = jQuery("#grid-table").jqGrid('getGridParam', 'selarrrow');
    var url = context_path + "/stock/toPrint.do?ids=" + ids;
    window.open(url);
}
/**
 * 显示提示窗口
 * @param msg   显示信息
 * @param delay 持续时间,结束之后窗口消失
 */
function showTipMsg(msg,delay){
		layer.msg(msg, {icon: 1,time:delay});
}
