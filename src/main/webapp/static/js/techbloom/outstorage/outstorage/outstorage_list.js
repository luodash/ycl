//查询的方法
function outstorage_list_queryInstoreListByParam(jsonParam){
    iTsai.form.deserialize($('#outstorage_list_hiddenQueryForm'),jsonParam);   //将json对象反序列化到列表页面中隐藏的form中
    //执行查询操作
    $("#outstorage_list_grid-table").jqGrid('setGridParam', {
        postData: {queryJsonString:JSON.stringify(iTsai.form.serialize($('#outstorage_list_hiddenQueryForm')))} //发送数据
    }).trigger("reloadGrid");
}

//执行
function outstorage_list_execute(){
    var selectAmount = getGridCheckedNum("#outstorage_list_grid-table");
    if(selectAmount==0){
        layer.msg("请选择一条记录！",{icon:2});
        return;
    }else if(selectAmount>1){
        layer.msg("只能选择一条记录！",{icon:8});
        return;
    }

    $.post(context_path + "/outsrorageCon/execute.do", {
        id : $("#outstorage_list_grid-table").jqGrid('getGridParam','selrow')
    }, function (str){
        $queryWindow=layer.open({
            title : "出库执行",
            type:1,
            skin : "layui-layer-molv",
            area : "1400px",
            shade : 0.6, //遮罩透明度
            moveType : 1, //拖拽风格，0是默认，1是传统拖动
            anim : 2,
            content : str,
            success: function (layero, index) {
                layer.closeAll('loading');
            }
        });
    });
}

//点击查看
function outstorage_list_openInfoPage(){
    var selectAmount = getGridCheckedNum("#outstorage_list_grid-table");
    if(selectAmount==0){
        layer.msg("请选择一条记录！",{icon:2});
        return;
    }else if(selectAmount>1){
        layer.msg("只能选择一条记录！",{icon:8});
        return;
    }
    layer.load(2);

    $.post(context_path + "/outsrorageCon/toInfo.do", {
        id : jQuery("#outstorage_list_grid-table").jqGrid("getGridParam", "selrow")
    }, function (str){
        $queryWindow=layer.open({
            title : "查看列表",
            type:1,
            skin : "layui-layer-molv",
            area : "1400px",
            shade : 0.6, //遮罩透明度
            moveType : 1, //拖拽风格，0是默认，1是传统拖动
            anim : 2,
            content : str,
            success: function (layero, index) {
                layer.closeAll('loading');
            }
        });
    });
}

//打开编辑页面
function outstorage_list_openEditPage(){
    var selectAmount = getGridCheckedNum("#outstorage_list_grid-table");
    if(selectAmount==0){
        layer.msg("请选择一条记录！",{icon:2});
        return;
    }else if(selectAmount>1){
        layer.msg("只能选择一条记录！",{icon:8});
        return;
    }
    layer.load(2);

    $.post(context_path+'/outsrorageCon/editInfo.do', {
        id : jQuery("#outstorage_list_grid-table").jqGrid('getGridParam', 'selrow')
    }, function(str){
        $queryWindow = layer.open({
            title : "编辑",
            type: 1,
            skin : "layui-layer-molv",
            area : "1400px",
            shade: 0.6, //遮罩透明度
            moveType: 1, //拖拽风格，0是默认，1是传统拖动
            content: str,//注意，如果str是object，那么需要字符拼接。
            success:function(layero, index){
                layer.closeAll("loading");
            }
        });
    }).error(function() {
        layer.closeAll();
        layer.msg("加载失败！",{icon:2});
    });
}

//导出
function outstorage_list_toExcel(){
    $("#outstorage_list_hiddenForm #outstorage_list_ids").val(jQuery("#outstorage_list_grid-table").jqGrid("getGridParam", "selarrrow"));
    $("#outstorage_list_hiddenForm #outstorage_list_shipNo2").val($("#outstorage_list_queryForm #outstorage_list_shipNo").val());

    $("#outstorage_list_hiddenForm #outstorage_list_createTime2").val($("#outstorage_list_queryForm #outstorage_list_createTime").val());
    $("#outstorage_list_hiddenForm #outstorage_list_endTime2").val($("#outstorage_list_queryForm #outstorage_list_endTime").val());
    $("#outstorage_list_hiddenForm #outstorage__list_factcode2").val($("#outstorage_list_queryForm #outstorage__list_factcode").val());
    $("#outstorage_list_hiddenForm #outstorage_list_orderno2").val($("#outstorage_list_queryForm #outstorage_list_orderno").val());
    $("#outstorage_list_hiddenForm #outstorage_list_orderline2").val($("#outstorage_list_queryForm #outstorage_list_orderline").val());
    $("#outstorage_list_hiddenForm #outstorage_list_contractno2").val($("#outstorage_list_queryForm #outstorage_list_contractno").val());
    $("#outstorage_list_hiddenForm #outstorage_list_queryExportExcelIndex").val(outstorage_list_exportExcelIndex);

    $("#outstorage_list_hiddenForm").submit();
}


//补打
function outstorage_list_print(){
    if(printAjaxStatus==0){
        layer.msg("打印进行中，请稍后...",{icon:2});
        return;
    }
    if(getGridCheckedNum("#outstorage_list_grid-table")==0){
        layer.msg("请选择要打印的出库单！",{icon:2});
        return;
    }
    if(getGridCheckedNum("#outstorage_list_grid-table")>1){
        layer.msg("只能选择一个出库单补打！",{icon:2});
        return;
    }
    printAjaxStatus = 0;
    $.ajax({
        url: context_path + "/outsrorageCon/buprint",
        type: "POST",
        data: {
            id : jQuery("#outstorage_list_grid-table").jqGrid('getGridParam', 'selrow')    //发货单主键
        },
        dataType: "JSON",
        success: function (data) {
            if (Boolean(data.result)) {
                layer.msg(data.msg, {icon: 1});
            } else {
                layer.alert(data.msg, {icon: 2});
            }
            printAjaxStatus = 1;
        },
        error:function(XMLHttpRequest){
            alert(XMLHttpRequest.readyState);
            alert("出错啦！！！");
        }
    });
}