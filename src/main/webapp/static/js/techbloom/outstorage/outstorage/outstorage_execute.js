
//绑定行车
function bind(id,carCode){
    if(carCode!='-1'){
        layer.alert("请勿重复绑定行车");
    }else{
        $.post(context_path + "/outsrorageCon/carbind.do", 
        	{
	        	detailId:id,
	        	infoId:$("#outstorage_execute_id1").val()
        	}, function (str){
        		if(str==null||str==''){
        			layer.msg("该物料实际没有入库，不能执行出库！",{icon:8,time:2500});
        		}else{
                $queryWindow=layer.open({
                    title : "行车绑定",
                    type:1,
                    skin : "layui-layer-molv",
                    area : "600px",
                    shade : 0.6, //遮罩透明度
                    moveType : 1, //拖拽风格，0是默认，1是传统拖动
                    anim : 2,
                    content : str,
                    success: function (layero, index) {
                        layer.closeAll('loading');
                    }
                });
            }
        });
    }
}

//开始拣货
function detailStart(id){
    $.post(context_path + "/outsrorageCon/detailStart", {id:id}, function (str){
        if(str.result){
            layer.msg(str.msg,{icon:1,time:1200});
            reloadDetailTableList();
        }else{
            layer.alert(str.msg);
        }
    });
}

//完成出库（单条出库）
function detailSuccess(id){
    if(ajaxStatus==0){
        layer.msg("操作进行中，请稍后...",{icon:2});
        return;
    }
    //将标记设置为不可请求
    ajaxStatus = 0;
    var outtime = $("#outstorage_list_time").val();
    if (outtime == '' || outtime == null) {
        layer.alert("请选择出库时间！");
        ajaxStatus = 1;
        return;
    }

    layer.confirm("是否需要打印出库单？",{
            btn: ['是，打印', '不需要，直接出库']
        },function(){
            $.ajax({
                url: context_path + "/outsrorageCon/detailSuccess",
                type: "POST",
                data: {
                    id : id,
                    outtime : outtime,
                    isPrint : 3
                },
                dataType: "JSON",
                success: function (data) {
                    iTsai.form.deserialize($("#outstorage_list_hiddenQueryForm"), iTsai.form.serialize($("#outstorage_list_queryForm")));
                    var queryParam = iTsai.form.serialize($("#outstorage_list_hiddenQueryForm"));
                    var queryJsonString = JSON.stringify(queryParam);
                    if (Boolean(data.result)) {
                        layer.msg(data.msg, {icon: 1});
                        //刷新列表
                        $("#outstorage_execute_grid-table-c").jqGrid('setGridParam', {
                            postData: {queryJsonString: ""}
                        }).trigger("reloadGrid");

                        $("#outstorage_list_grid-table").jqGrid('setGridParam', {
                            postData: {queryJsonString: queryJsonString}
                        }).trigger("reloadGrid");
                    } else {
                        layer.alert(data.msg, {icon: 2});
                    }
                    ajaxStatus = 1;
                },
                error:function(XMLHttpRequest){
                    alert(XMLHttpRequest.readyState);
                    alert("出错啦！！！");
                }
            });
        },
        function(){
            $.ajax({
                url: context_path + "/outsrorageCon/detailSuccess",
                type: "POST",
                data: {
                    id : id,
                    outtime : outtime,
                    isPrint: 1
                },
                dataType: "JSON",
                success: function (data) {
                    iTsai.form.deserialize($("#outstorage_list_hiddenQueryForm"), iTsai.form.serialize($("#outstorage_list_queryForm")));
                    var queryParam = iTsai.form.serialize($("#outstorage_list_hiddenQueryForm"));
                    var queryJsonString = JSON.stringify(queryParam);
                    if (Boolean(data.result)) {
                        layer.msg(data.msg, {icon: 1});
                        //刷新列表
                        $("#outstorage_execute_grid-table-c").jqGrid('setGridParam', {
                            postData: {queryJsonString: ""}
                        }).trigger("reloadGrid");

                        $("#outstorage_list_grid-table").jqGrid('setGridParam', {
                            postData: {queryJsonString: queryJsonString}
                        }).trigger("reloadGrid");
                    } else {
                        layer.alert(data.msg, {icon: 2});
                    }
                    ajaxStatus = 1;
                },
                error:function(XMLHttpRequest){
                    alert(XMLHttpRequest.readyState);
                    alert("出错啦！！！");
                }
            });
        });

    /*$.post(context_path + "/outsrorageCon/toUpdateMeter.do?", {id:id}, function (str){
        $queryWindow=layer.open({
            title : "修改米数",
            type:1,
            skin : "layui-layer-molv",
            area : "600px",
            shade : 0.6, //遮罩透明度
            moveType : 1, //拖拽风格，0是默认，1是传统拖动
            anim : 2,
            content : str,
            success: function (layero, index) {
                layer.closeAll('loading');
            }
        });
    });*/
}



//重新加载详情列表
function reloadDetailTableList(){
    $("#outstorage_execute_grid-table-c").jqGrid('setGridParam', {
        url:context_path + "/outsrorageCon/detailList.do?id=" + id,
    }).trigger("reloadGrid");
}

//确认出库，批量
$('#outStorageOut').click(function () {
    if(ajaxStatus==0){
        layer.msg("操作进行中，请稍后...",{icon:3});
        return;
    }
    ajaxStatus = 0;    //将标记设置为不可请求

    var outtime = $("#outstorage_list_time").val();
    if (outtime == '' || outtime == null) {
        layer.alert("请选择出库时间！");
        ajaxStatus = 1;
        return;
    }
    if(getGridCheckedNum("#outstorage_execute_grid-table-c","id") == 0){
        layer.alert("请至少选中一条记录！");
        ajaxStatus = 1;
    }else{
        layer.confirm("是否需要打印出库单？",{
            btn: ['是，打印', '不需要，直接出库']
        },function(){
            $.ajax({
                type:"POST",
                url:context_path + "/outsrorageCon/outStorageOut?ids=" + $('#outstorage_execute_grid-table-c').jqGrid('getGridParam','selarrrow')
                    + "&outtime=" + outtime + "&isPrint=3",
                dataType:"json",
                success:function(data){
                    iTsai.form.deserialize($("#outstorage_list_hiddenQueryForm"), iTsai.form.serialize($("#outstorage_list_queryForm")));
                    var queryJsonString = JSON.stringify(iTsai.form.serialize($("#outstorage_list_hiddenQueryForm")));
                    if(data.result){
                        layer.msg(data.msg);
                        //重新加载详情列表
                        reloadDetailTableList();
                        $("#outstorage_list_grid-table").jqGrid("setGridParam", {
                            postData: {queryJsonString:queryJsonString} //发送数据
                        }).trigger("reloadGrid");
                    }else{
                        layer.alert(data.msg, {icon: 2});
                    }
                    ajaxStatus = 1;
                }
            })
        },
        function(){
            $.ajax({
                type:"POST",
                url:context_path + "/outsrorageCon/outStorageOut?ids=" + $('#outstorage_execute_grid-table-c').jqGrid('getGridParam','selarrrow')
                    + "&outtime=" + outtime + "&isPrint=1",
                dataType:"json",
                success:function(data){
                    iTsai.form.deserialize($("#outstorage_list_hiddenQueryForm"), iTsai.form.serialize($("#outstorage_list_queryForm")));
                    var queryJsonString = JSON.stringify(iTsai.form.serialize($("#outstorage_list_hiddenQueryForm")));
                    if(data.result){
                        layer.msg(data.msg);
                        //重新加载详情列表
                        reloadDetailTableList();
                        $("#outstorage_list_grid-table").jqGrid("setGridParam", {
                            postData: {queryJsonString:queryJsonString} //发送数据
                        }).trigger("reloadGrid");
                    }else{
                        layer.alert(data.msg, {icon: 2});
                    }
                    ajaxStatus = 1;
                }
            })
        });
    }
});