//保存按钮
function saveFormInfo(formdata){
    $("#outstorage_edit_formSave").attr("disabled","disabled");
    $(window).triggerHandler('resize.jqGrid');
    $.ajax({
        type:"POST",
        url:context_path + "/outsrorageCon/saveOutStorage",
        data:formdata,
        dataType:"json",
        success:function(data){
            if(data.result){
                reloadDetailTableList();   //重新加载详情列表
                layer.alert("发货单保存成功");
                $("#outstorage_edit_formSave").removeAttr("disabled");
                //重新加载表格
                $("#outstorage_list_grid-table").jqGrid("setGridParam",
                    {
                        postData: {queryJsonString:""}
                    }
                ).trigger("reloadGrid");
                layer.close($queryWindow);
            }else{
                layer.alert("发货单保存失败");
            }
        }
    })
}

//重新加载详情列表
function reloadDetailTableList(){
    $("#outstorage_edit_grid-table-c").jqGrid('setGridParam',
        {
            url:context_path + "/outsrorageCon/detailList.do?id="+$("#outstorage_edit_id").val(),
        }
    ).trigger("reloadGrid");
}

//提交
function outformSubmitBtn(){
    if($('#outstorage_edit_baseInfor #outstorage_edit_id').val()==-1){
        layer.alert("请先保存单据");
        return;
    }else {
        var formdata = $('#outstorage_edit_baseInfor').serialize();
        //后台修改对应的数据
        layer.confirm("确定提交,提交之后数据不能修改", function () {
            $.ajax({
                type:"POST",
                url:context_path+"/outsrorageCon/subOutStorage",
                dataType:"json",
                data:{id:$('#outstorage_edit_baseInfor #outstorage_edit_id').val()},
                data:formdata,
                success:function(data){
                    if(data.result){
                        layer.closeAll();
                        layer.msg("提交成功",{icon:1,time:1200});
                        //重新加载表格
                        $("#outstorage_list_grid-table").jqGrid("setGridParam",
                            {
                                postData: {queryJsonString:""}
                            }
                        ).trigger("reloadGrid");
                        layer.close($queryWindow);
                    }else{
                        layer.alert(data.msg);
                    }
                }
            })
        });
    }
}

//删除详情中的数据
function delDetail(){
    var checkedNum = getGridCheckedNum("#outstorage_edit_grid-table-c","id");  //选中的数量
    if(checkedNum==0){
        layer.alert("请选择一条记录！");
    }else{
        layer.confirm("确定删除选择的出库详情",function(){
            $.ajax({
                type:"POST",
                url:context_path + "/outsrorageCon/delOutstorageDetail?ids="+jQuery("#outstorage_edit_grid-table-c").jqGrid('getGridParam', 'selarrrow'),
                dataType:"json",
                success:function(data){
                    iTsai.form.deserialize($("#outstorage_list_hiddenQueryForm"), iTsai.form.serialize($("#outstorage_list_queryForm")));
                    var queryParam = iTsai.form.serialize($("#outstorage_list_hiddenQueryForm"));
                    var queryJsonString = JSON.stringify(queryParam);
                    if(data.result){
                        layer.msg(data.msg);
                        reloadDetailTableList();   //重新加载详情列表
                        //重新加载主列表
                        $("#outstorage_list_grid-table").jqGrid("setGridParam",
                            {
                                postData: {queryJsonString:queryJsonString} //发送数据  :选中的节点
                            }
                        ).trigger("reloadGrid");
                    }else{
                        layer.msg(data.msg);
                    }
                }
            })
        });
    }
}