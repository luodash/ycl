//保存按钮
function saveFormInfo(formdata){
    $("#delivery_add_formSave").attr("disabled","disabled");
    $(window).triggerHandler('resize.jqGrid');
    $.ajax({
        type:"POST",
        url:context_path + "/deleiveryCon/saveShipLoading",
        data:formdata,
        dataType:"json",
        success:function(data){
            if(data.result){
                $("#delivery_add_baseInfor #delivery_add_id").val(data.shipLoadingId)
                reloadDetailTableList();   //重新加载详情列表
                layer.alert("发运单保存成功");
                $("#delivery_add_formSave").removeAttr("disabled");
                gridReload()
            }else{
                layer.alert("发运单保存失败");
            }
        }
    })
}

//重新加载详情列表
function reloadDetailTableList(){
    $("#delivery_add_grid-table-c").jqGrid('setGridParam',
        {
            url:context_path + "/deleiveryCon/detailList.do?id="+$("#delivery_add_baseInfor #delivery_add_id").val(),
        }
    ).trigger("reloadGrid");
}

//删除详情中的数据
function delDetail(){
    var checkedNum = getGridCheckedNum("#delivery_add_grid-table-c","id");  //选中的数量
    if(checkedNum==0){
        layer.alert("请选择一条记录！");
    }else{
        var ids = jQuery("#delivery_add_grid-table-c").jqGrid('getGridParam', 'selarrrow');
        layer.confirm("确定删除选择的装车发运数据",function(){
            $.ajax({
                type:"POST",
                url:context_path + "/deleiveryCon/deleteShipLoadingDetailIds?ids="+ids,
                dataType:"json",
                success:function(data){
                    if(data.result){
                        layer.msg(data.msg);
                        reloadDetailTableList();   //重新加载详情列表
                    }else{
                        layer.msg(data.msg);
                    }
                }
            })
        });
    }

}