/**查询的方法**/
function delivery_list_queryInstoreListByParam(jsonParam){
    //序列化表单：iTsai.form.serialize($('#frm'))
    //反序列化表单：iTsai.form.deserialize($('#frm'),json)
    iTsai.form.deserialize($('#delivery_list_hiddenQueryForm'),jsonParam);   //将json对象反序列化到列表页面中隐藏的form中
    var queryParam = iTsai.form.serialize($('#delivery_list_hiddenQueryForm'));
    var queryJsonString = JSON.stringify(queryParam);         //将json对象转换成json字符串
    //执行查询操作
    $("#delivery_list_grid-table").jqGrid('setGridParam',
        {
            postData: {queryJsonString:queryJsonString} //发送数据
        }
    ).trigger("reloadGrid");
}

/***点击查看**/
function delivery_list_openInfoPage(){
    var selectAmount = getGridCheckedNum("#delivery_list_grid-table");
    if(selectAmount==0){
        layer.msg("请选择一条记录！",{icon:2});
        return;
    }else if(selectAmount>1){
        layer.msg("只能选择一条记录！",{icon:8});
        return;
    }
    layer.load(2);
    selectid = jQuery("#delivery_list_grid-table").jqGrid("getGridParam", "selrow");
    $.post(context_path + "/deleiveryCon/toInfo.do?id="+selectid, {}, function (str){
        $queryWindow=layer.open({
            title : "查看列表",
            type:1,
            skin : "layui-layer-molv",
            area : "800px",
            shade : 0.6, //遮罩透明度
            moveType : 1, //拖拽风格，0是默认，1是传统拖动
            anim : 2,
            content : str,
            success: function (layero, index) {
                layer.closeAll('loading');
            }
        });
    });
}

/**打开新增页面**/
function delivery_list_openAddPage(){
    layer.load(2);
    $.post(context_path+'/deleiveryCon/toAdd.do', {}, function(str){
        $queryWindow = layer.open({
            title : "添加",
            type: 1,
            skin : "layui-layer-molv",
            area : "800px",
            shade: 0.6, //遮罩透明度
            moveType: 1, //拖拽风格，0是默认，1是传统拖动
            content: str,//注意，如果str是object，那么需要字符拼接。
            success:function(layero, index){
                layer.closeAll("loading");
            }
        });
    }).error(function() {
        layer.closeAll();
        layer.msg("加载失败！",{icon:2});
    });
}

/**打开编辑页面**/
function delivery_list_openEditPage(){
    var selectAmount = getGridCheckedNum("#delivery_list_grid-table");
    if(selectAmount==0){
        layer.msg("请选择一条记录！",{icon:2});
        return;
    }else if(selectAmount>1){
        layer.msg("只能选择一条记录！",{icon:8});
        return;
    }
    var id = jQuery("#delivery_list_grid-table").jqGrid('getGridParam', 'selrow');
    var rowData = jQuery("#delivery_list_grid-table").jqGrid('getRowData', id).state;
    if (!rowData.indexOf("未提交")) {
        layer.alert("只能选择状态为未提交的数据进行编辑操作");
        return;
    }
    layer.load(2);
    $.post(context_path+'/deleiveryCon/editInfo.do?id='+id, {}, function(str){
        $queryWindow = layer.open({
            title : "编辑",
            type: 1,
            skin : "layui-layer-molv",
            area : "800px",
            shade: 0.6, //遮罩透明度
            moveType: 1, //拖拽风格，0是默认，1是传统拖动
            content: str,//注意，如果str是object，那么需要字符拼接。
            success:function(layero, index){
                layer.closeAll("loading");
            }
        });
    }).error(function() {
        layer.closeAll();
        layer.msg("加载失败！",{icon:2});
    });
}

/**导出**/
function delivery_list_toExcel(){
    var ids = jQuery("#delivery_list_grid-table").jqGrid("getGridParam", "selarrrow");
    $("#delivery_list_hiddenForm #delivery_list_ids").val(ids);
    $("#delivery_list_hiddenForm #delivery_list_queryShipNo").val($("#delivery_list_queryForm #delivery_list_shipNo").val());
    $("#delivery_list_hiddenForm").submit();
}

/**刷新列表**/
function gridReload(){
    $("#delivery_list_grid-table").jqGrid('setGridParam',
        {
            url : context_path + '/deleiveryCon/shipLoadingPageList.do',
            postData: {queryJsonString:""} //发送数据  :选中的节点
        }
    ).trigger("reloadGrid");
}

/**根据id删除**/
function delivery_list_deleteByIds(){
    var checkedNum = getGridCheckedNum("#delivery_list_grid-table","id");  //选中的数量
    if(checkedNum==0){
        layer.alert("请选择一条记录！");
        return
    }
    var ids = jQuery("#delivery_list_grid-table").jqGrid('getGridParam', 'selarrrow');
    for(var i=0;i<ids.length;i++){
        var idd = ids[i];
        var rowData = jQuery("#delivery_list_grid-table").jqGrid('getRowData', idd).state;
        if (rowData == '已提交') {
            layer.alert("已提交的数据请勿删除");
            return;
        }
    }
    layer.confirm("确定删除选择的装车发运数据",function(){
        $.ajax({
            type:"POST",
            url:context_path + "/deleiveryCon/deleteByIds?ids="+ids,
            dataType:"json",
            success:function(data){
                if(data.result){
                    layer.msg(data.msg);
                    gridReload();   //重新加载详情列表
                }else{
                    layer.msg(data.msg);
                }
            }
        })
    });
}