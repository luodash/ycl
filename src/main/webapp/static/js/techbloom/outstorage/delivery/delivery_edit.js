//保存按钮
function saveFormInfo(formdata){
    $("#delivery_edit_formSave").attr("disabled","disabled");
    $(window).triggerHandler('resize.jqGrid');
    $.ajax({
        type:"POST",
        url:context_path + "/deleiveryCon/saveShipLoading",
        data:formdata,
        dataType:"json",
        success:function(data){
            if(data.result){
                reloadDetailTableList();   //重新加载详情列表
                layer.alert("发货单保存成功");
                $("#delivery_edit_formSave").removeAttr("disabled");
                gridReload();
            }else{
                layer.alert("发货单保存失败");
            }
        }
    })
}

//重新加载详情列表
function reloadDetailTableList(){
    $("#delivery_edit_grid-table-c").jqGrid('setGridParam',
        {
            url:context_path + "/deleiveryCon/detailList.do?id="+$("#delivery_edit_id").val(),
        }
    ).trigger("reloadGrid");
}

//提交
function outformSubmitBtn(){
    if($('#delivery_edit_baseInfor #delivery_edit_id').val()==-1){
        layer.alert("请先保存单据");
        return;
    }else {
        var formdata = $('#delivery_edit_baseInfor').serialize();
        //后台修改对应的数据
        layer.confirm("确定提交,提交之后数据不能修改", function () {
            $.ajax({
                type:"POST",
                url:context_path+"/deleiveryCon/subShiploading",
                dataType:"json",
                data:{id:$('#delivery_edit_baseInfor #delivery_edit_id').val()},
                data:formdata,
                success:function(data){
                    if(data.result){
                        layer.closeAll();
                        layer.msg("提交成功",{icon:1,time:1200});
                        gridReload();
                    }else{
                        layer.alert(data.msg);
                    }
                }
            })
        });
    }
}

//删除详情中的数据
function delDetail(){
    var checkedNum = getGridCheckedNum("#delivery_edit_grid-table-c","id");  //选中的数量
    if(checkedNum==0){
        layer.alert("请选择一条记录！");
    }else{
        var ids = jQuery("#delivery_edit_grid-table-c").jqGrid('getGridParam', 'selarrrow');
        layer.confirm("确定删除选择的装车发运数据",function(){
            $.ajax({
                type:"POST",
                url:context_path + "/deleiveryCon/deleteShipLoadingDetailIds?ids="+ids,
                dataType:"json",
                success:function(data){
                    if(data.result){
                        layer.msg(data.msg);
                        reloadDetailTableList();   //重新加载详情列表
                    }else{
                        layer.msg(data.msg);
                    }
                }
            })
        });
    }

}