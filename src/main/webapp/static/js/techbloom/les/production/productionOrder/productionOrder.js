/**
 * 新增生产订单
 */
function add(){
	$.get(context_path + "/productionOrder/toAdd.do?id=-1").done(function(data){
		layer.open({
		    title : "生产订单添加", 
	    	type:1,
	    	skin : "layui-layer-molv",
	    	area : "600px",
	    	shade : 0.6, //遮罩透明度
		    moveType : 1, //拖拽风格，0是默认，1是传统拖动
		    anim : 2,
		    content : data
		});
	});
}
/**
 * 编辑生产订单
 */
function edit(){
	var checkedNum = getGridCheckedNum("#production_order_list_grid-table","id");
	if(checkedNum == 0){
    	layer.alert("请选择一条要编辑的生产订单！");
    	return false;
    } else if(checkedNum >1){
    	layer.alert("只能选择一条生产订单进行编辑操作！");
    	return false;
    } else {
    	var saleId = jQuery("#production_order_list_grid-table").jqGrid('getGridParam', 'selrow'); 
    	var rowDate = jQuery("#production_order_list_grid-table").jqGrid('getRowData', saleId).state;
    	
    	$.get( context_path + "/productionOrder/toAdd.do?id="+saleId).done(function(data){
    	  layer.open({
    	  title : "生产订单编辑", 
    	  type:1,
    	  skin : "layui-layer-molv",
    	  area : "600px",
    	  shade : 0.6, //遮罩透明度
    	  moveType : 1, //拖拽风格，0是默认，1是传统拖动
    	  anim : 2,
    	  content : data
    	  });
    	 }); 		
    }
}

/**
 * 删除生产订单
 */
function del(){
	var checkedNum = getGridCheckedNum("#production_order_list_grid-table","id");  //选中的数量
	if(checkedNum==0){
		layer.alert("请至少选择一条要删除的生产订单！");
	}else{
		//从数据库中删除选中的入库单，并刷新入库单表格
		var ids = jQuery("#production_order_list_grid-table").jqGrid('getGridParam', 'selarrrow');
		//弹出确认窗口
		layer.confirm("确定删除选中的生产订单？", function() {
    		$.ajax({
    			type : "POST",
    			url:context_path + "/productionOrder/delete?ids="+ids,
    			dataType : 'json',
    			cache : false,
    			success : function(data) {
    				layer.closeAll();
    				if(data.result){
						layer.msg("生产订单删除成功！", {icon: 1,time:1000});
    				}else{
    					layer.msg(data.msg, {icon: 2,time:1000});
    				}
    				_grid.trigger("reloadGrid");  //重新加载表格
    			}
    		});
    	});
	} 
}


/**
 * 打开查询界面
 */
function openOutstoreListSearchPage(){
	var queryBean = iTsai.form.serialize($('#production_order_list_hiddenQueryForm'));   //获取form中的值：json对象
	var queryJsonString = JSON.stringify(queryBean);//将json对象转换成json字符串
	Dialog.open({
	    title: "出库单条件查询", 
	    width: 600,  height: '400',
	    url: context_path + "/outStorage/toQueryPage?outstoreString="+queryJsonString,
	    theme : "simple",
		drag : true
	});
}


/**
 * 收货单查询功能:获取查询页面中的值，并将值放入列表页面中隐藏的form
 * @param jsonParam     查询页面传递过来的json对象
 */
function queryInstoreListByParam(jsonParam){
	//序列化表单：iTsai.form.serialize($('#frm'))
	//反序列化表单：iTsai.form.deserialize($('#frm'),json)
	iTsai.form.deserialize($('#production_order_list_hiddenQueryForm'),jsonParam);   //将json对象反序列化到列表页面中隐藏的form中
	var queryParam = iTsai.form.serialize($('#production_order_list_hiddenQueryForm'));
	var queryJsonString = JSON.stringify(queryParam);         //将json对象转换成json字符串
	//执行查询操作
	$("#production_order_list_grid-table").jqGrid('setGridParam', 
			{
				postData: {queryJsonString:queryJsonString} //发送数据 
			}
	  ).trigger("reloadGrid");
}


/**
 * 显示提示窗口
 * @param msg   显示信息
 * @param delay 持续时间,结束之后窗口消失
 */
function showTipMsg(msg,delay){
	Dialog.tip( msg, {type: "loading", delay: delay} );
}

/** 表格刷新 */
function gridReload(){
	 _grid.trigger("reloadGrid");  //重新加载表格  
}

