var oriData;
var _grid;
var openwindowtype = 0; //打开窗口类型：0新增，1修改
var  bomNo;
var type=0;
var dynamicDefalutValue="f3f29a2b17ac4c91a44bee2888bd65c5";//列表码

	_grid = jQuery("#artbom_list_grid-table").jqGrid({
		url : context_path + '/artBom/list.do',
	    datatype : "json",
	    colNames : [ '主键','版本号','清单编号', '清单名称','流程名称','创建时间','版本','备注'],
	    colModel : [
	                { name : 'bomId',index : 'bomId',hidden:true},
	                { name : 'version',index : 'version',hidden:true},
	                 {name : 'bomNo',index : 'bomNo',width : 60},
	                 {name : 'bomName',index : 'bomName',width : 50},
	                 {name : 'modelName',index : 'modelName',width : 50},
	                 {name : 'bomCreateTime',index : 'bomCreateTime',width : 100,formatter:function(cellVal,option,rowObject){
	                	 if(cellVal){
	                		 return cellVal.substring(0,19);
	                	 }else{
	                		 return "";
	                	 }

	                 }},
	                 {name : 'versionName',index : 'versionName'},
	                 {name : 'remark',index : 'REMARK'}
	               ],
	    rowNum : 20,
	    rowList : [ 10, 20, 30 ],
	    pager : '#artbom_list_grid-pager',
	    sortname : 'id',
	    sortorder : "desc",
        altRows: true,
        viewrecords : true,
        hidegrid:false,
        multiselect:true,
		beforeRequest:function (){
			dynamicGetColumns(dynamicDefalutValue,"artbom_list_grid-table",$(window).width()-$("#sidebar").width() -7);
			//重新加载列属性
		},
        loadComplete : function(data){
        	var table = this;
        	setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
        	oriData = data;
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
	});

jQuery("#artbom_list_grid-table").navGrid('#artbom_list_grid-pager',{edit:false,add:false,del:false,search:false,refresh:false})
.navButtonAdd('#artbom_list_grid-pager',{
	caption:"",
	buttonicon:"fa fa-refresh green",
	onClickButton: function(){
		$("#artbom_list_grid-table").jqGrid('setGridParam',
		{
			postData: {queryJsonString:""} //发送数据
		}
		).trigger("reloadGrid");
	}
}).navButtonAdd('#artbom_list_grid-pager',{
    caption: "",
    buttonicon:"fa  icon-cogs",
    onClickButton : function (){
        jQuery("#artbom_list_grid-table").jqGrid('columnChooser',{
            done: function(perm, cols){
                dynamicColumns(cols,dynamicDefalutValue);
                $("#artbom_list_grid-table").jqGrid( 'setGridWidth', $(window).width()-$("#sidebar").width() -7);
            }
        });
    }
});

$(window).on('resize.jqGrid', function () {
	$("#artbom_list_grid-table").jqGrid( 'setGridWidth', $(window).width()-$("#sidebar").width() -7);
	$("#artbom_list_grid-table").jqGrid( 'setGridHeight', ($(window).height()-$("#artbom_list_table_toolbar").outerHeight(true)- 
	$("#artbom_list_grid-pager").outerHeight(true)-$("#user-nav").height()-$("#breadcrumb").height()-$(".ui-jqgrid-labels").height()-115 ) );
});

$(window).triggerHandler('resize.jqGrid');


//重新加载表格
function gridReload(){
	_grid.trigger("reloadGrid");  //重新加载表格
}


/**
 * 查询功能:获取查询页面中的值，并将值放入列表页面中隐藏的form
 * @param jsonParam     查询页面传递过来的json对象
 */
function queryLogListByParam(jsonParam){
	var queryJsonString = JSON.stringify(jsonParam);         //将json对象转换成json字符串
	//执行查询操作
	$("#artbom_list_grid-table").jqGrid('setGridParam',
			{
		postData: {queryJsonString:queryJsonString} //发送数据
			}
	).trigger("reloadGrid");
}



/*打开添加页面*/
function openAddPage(){
	type=0;
	openwindowtype = 0;
	layer.load(2);
	$.post(context_path+'/artBom/toAdd', {}, function(str){
	$queryWindow = layer.open({
		    title : "清单添加",
	    	type: 1,
	    	skin : "layui-layer-molv",
	    	area: ['780px', '620px'],
	    	shade: 0.6, //遮罩透明度
	    	shadeClose: true,
    	    moveType: 1, //拖拽风格，0是默认，1是传统拖动
	    	content:str,//注意，如果str是object，那么需要字符拼接。
	    	success:function(layero, index){
	    		layer.closeAll('loading');
	    	}
		});
	}).error(function() {
		layer.closeAll();
		layer.msg('加载失败！',{icon:2});
	});
}

/*打开编辑页面*/
function openEditPage(){
	type=0;
	var selectAmount = getGridCheckedNum("#artbom_list_grid-table");
	if(selectAmount==0){
		layer.msg("请选择一条记录！",{icon:2});
		return;
	}else if(selectAmount>1){
		layer.msg("只能选择一条记录！",{icon:8});
		return;
	}

	var version =$("#artbom_list_grid-table").getRowData(jQuery("#artbom_list_grid-table").getGridParam("selarrrow")[0]).version;
	if(version!=0){
		layer.msg("已发布的版本不可编辑!",{time:1200,icon:2});
		return;
	}
	openwindowtype = 1;
	layer.load(2);
	var bomId = jQuery("#artbom_list_grid-table").jqGrid('getGridParam', 'selrow');
	$.post(context_path+'/artBom/toAdd?id='+bomId, {}, function(str){
		$queryWindow = layer.open({
			title : "工艺BOM编辑",
			type: 1,
		    skin : "layui-layer-molv",
		    area: ['780px', '620px'],
			shade: 0.6, //遮罩透明度
			moveType: 1, //拖拽风格，0是默认，1是传统拖动
			content: str,//注意，如果str是object，那么需要字符拼接。
			success:function(layero, index){
				layer.closeAll('loading');
			}
		});
	}).error(function() {
		layer.closeAll();
    	layer.msg('加载失败！',{icon:2});
	});
}

//bom删除
function deleteBom(){
	var selectAmount = getGridCheckedNum("#artbom_list_grid-table");
	if(selectAmount==0){
		layer.msg("请选择一条记录！",{icon:2});
		return;
	}
	//获取表格中选中的用户记录
	var ids = jQuery("#artbom_list_grid-table").jqGrid('getGridParam', 'selarrrow');
	layer.confirm('确定删除？', /*显示的内容*/
		{
		  shift: 6,
		  moveType: 1, //拖拽风格，0是默认，1是传统拖动
		  title:"操作提示",  /*弹出框标题*/
		  icon: 3,      /*消息内容前面添加图标*/
		  btn: ['确定', '取消']/*可以有多个按钮*/
		}, function(index, layero){
		   //确定按钮的回调
			$.ajax({
				url:context_path+"/artBom/delBom?ids="+ids,
				type:"POST",
				dataType:"json",
				success:function(data){
                    if(data.result){
                        layer.msg(data.msg,{icon:1,time:1200});
                        //刷新用户列表
                        $("#artbom_list_grid-table").jqGrid('setGridParam',
                            {
                                postData: {queryJsonString:""} //发送数据
                            }
                        ).trigger("reloadGrid");
                    }else{
                        layer.msg("删除失败!",{icon:2,time:1200});
                    }
				}
			});

		}, function(index){
		  //取消按钮的回调
		  layer.close(index);
		});
	}

/*工艺BOM导出*/
function excelartbom(){
    var selectid = jQuery("#artbom_list_grid-table").jqGrid('getGridParam', 'selarrrow');
    $("#artbom_list_ids").val(selectid);
    $("#artbom_list_hiddenForm").submit();
}
/**
 * 查询按钮点击事件
 */
function openQueryPage(){
	var queryParam = iTsai.form.serialize($('#artbom_list_query_form'));
	queryLogListByParam(queryParam);
}

function versionView(){
	var selectAmount = getGridCheckedNum("#artbom_list_grid-table");
	if(selectAmount==0){
		layer.msg("请选择一条记录！",{icon:2});
		return;
	}else if(selectAmount>1){
		layer.msg("只能选择一条记录！",{icon:8});
		return;
	}
	var bomId = jQuery("#artbom_list_grid-table").jqGrid('getGridParam', 'selrow');
	$.get( context_path + "/artBom/toViewDetail?id="+bomId).done(function(data){
		layer.open({
		    title : "工艺BOM查看",
	    	type:1,
	    	skin : "layui-layer-molv",
	    	area : ['750px', '650px'],
	    	shade : 0.6, //遮罩透明度
		    moveType : 1, //拖拽风格，0是默认，1是传统拖动
		    anim : 2,
		    content : data
		});
	});


}
function queryInstoreListByParam(jsonParam){
	var queryJsonString = JSON.stringify(jsonParam);         //将json对象转换成json字符串
	//执行查询操作
	$("#artbom_list_grid-table").jqGrid('setGridParam',
			{
				postData: {queryJsonString:queryJsonString} //发送数据
			}
	  ).trigger("reloadGrid");
}