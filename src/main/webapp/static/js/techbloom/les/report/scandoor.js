var oriData;
var _grid;
var $queryWindow;  //查询窗口对象
_grid = jQuery("#scandoor_grid-table").jqGrid({
    url : context_path + 'scanDoor/toView?type='+$("#scandoor_types").val(),
    datatype : "json",
    colNames : [ '主键','任务编号','送往产线','送往工位','送往货位','拣选货位','车辆编号','车辆名称','配送料箱','配送数量','创建时间','状态' ],
    colModel : [
        {name: 'id', index: 'id', width: 20, hidden: true},
        {name : 'deliveryCode',index : 'DELIVERYCODE',width : 75},
        {name : 'productLine',index : 'PRODUCTLINE',width : 75},
        {name : 'routeName',index : 'ROUTENAME',width : 75},
        {name : 'locationName',index : 'LOCATIONNAME',width :75},
        {name : 'pickLocation',index : 'PICKLOCATION',width :75},
        {name : 'carNo',index : 'CARNO',width : 75},
        {name : 'carName',index : 'CARNAME',width : 75},
        {name : 'conRfid',index : 'conRfid',width : 75},
        {name : 'amount',index : 'amount',width : 75},
        {name : 'createTime',index : 'CREATETIME',width : 75},
        {name : 'state',index : 'STATE',width : 60,
            formatter:function(cellValu,option,rowObject){
                if(cellValu==0) return "<span style='color:red;font-weight:bold;'>未提交</span>" ;
                if(cellValu==1) return "<span style='color:green;font-weight:bold;'>拣选</span>" ;
                if(cellValu==2) return "<span style='color:orange;font-weight:bold;'>扫描门</span>" ;
                if(cellValu==3) return "<span style='color:green;font-weight:bold;'>换箱下架</span>";
            }
        }
    ],
    rowNum : 20,
    rowList : [ 10, 20, 30 ],
    pager : '#scandoor_grid-pager',
    sortname : 'id',
    sortorder : "desc",
    altRows: true,
    viewrecords : true,
    caption : "拉动任务",
    hidegrid:false,
    multiselect:true,
    multiboxonly: true,
    loadComplete : function(data){
        var table = this;
        setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
        oriData = data;
    },
    emptyrecords: "没有相关记录",
    loadtext: "加载中...",
    pgtext : "页码 {0} / {1}页",
    recordtext: "显示 {0} - {1}共{2}条数据"
});

jQuery("#scandoor_grid-table").navGrid('#scandoor_grid-pager',{edit:false,add:false,del:false,search:false,refresh:false})
    .navButtonAdd('#scandoor_grid-pager',{
        caption:"",
        buttonicon:"fa fa-refresh green",
        onClickButton: function(){
            $("#scandoor_grid-table").jqGrid('setGridParam',
                {
                    postData: {queryJsonString:""} //发送数据
                }
            ).trigger("reloadGrid");
        }
    });
$(window).on('resize.jqGrid', function () {
    $("#scandoor_grid-table").jqGrid( 'setGridWidth', $(window).width()-$("#sidebar").width() -7);
    $("#scandoor_grid-table").jqGrid( 'setGridHeight', ($(window).height()-$("#scandoor_table_toolbar").outerHeight(true)- 
    $("#scandoor_grid-pager").outerHeight(true)-$("#user-nav").height()-$("#breadcrumb").height()-$(".ui-jqgrid-labels").height()-35 ) );
});

$(window).triggerHandler('resize.jqGrid');
//重新加载表格
function gridReloads(){
    _grid.trigger("reloadGrid");  //重新加载表格
}
page_interval.push( setInterval(gridReloads,2000));

