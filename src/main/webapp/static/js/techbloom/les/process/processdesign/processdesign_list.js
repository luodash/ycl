/**
 * 流程查询
 */
function openProcessQueryPage() {
    var queryJsonString = $("#processdesign_list_name").val();
    $("#processdesign_list_grid-table").jqGrid('setGridParam',
        {
            postData: {queryJsonString: queryJsonString} //发送数据
        }
    ).trigger("reloadGrid");
}

/**
 * 创建新流程
 */
function createNewProcess() {
    layer.load(2);
    $.post(context_path+'/processdesign/processdesign_add', {}, function(str){
        $queryWindow = layer.open({
            title : "流程新增",
            type: 1,
            skin : "layui-layer-molv",
            area : "600px",
            shade: 0.6, //遮罩透明度
            moveType: 1, //拖拽风格，0是默认，1是传统拖动
            content: str,//注意，如果str是object，那么需要字符拼接。
            success:function(layero, index){
                layer.closeAll('loading');
            }
        });
    }).error(function() {
        layer.closeAll();
        layer.msg('加载失败！',{icon:2});
    });
}

/**
 * 编辑模型
 */
function eidtSomeModel(id) {
    var idAddr;
    if(!(id===+id)){
        var checkedNum = getGridCheckedNum("#processdesign_list_grid-table", "id");
        idAddr = jQuery("#processdesign_list_grid-table").getGridParam("selarrrow");
        id=idAddr[0];
    }
    if(id==undefined){
        layer.msg("请选择一条记录",{icon:2});
        return
    }
    layer.open({
        skin : "layui-layer-molv",
        type: 2,
        title: '<img src="'+context_path+'/plugins/public_components/img/process.png" style=" margin-right: 2px;">路径定义',
        shadeClose: true,
        shade: 0.8,
        area: ['98%', '98%'],
        content:context_path+"/processdesign/editIndex?modelId="+id
    });
}

function deploySomeModel() {
    var idAddr = jQuery("#processdesign_list_grid-table").getGridParam("selarrrow");
    $.ajax({
        type: 'POST',
        url: context_path+'/processdesign/deployModel',
        data: {modelId: idAddr[0]},
        success: function (data) {
            layer.msg(data.result,{icon:1});
        },
        dataType: 'json'
    });
}

/**
 * 删除模型
 */
function deleteSomeModel(){
    var modelIds =jQuery("#processdesign_list_grid-table").getGridParam("selarrrow");
    if(modelIds.length>0){
        var modelId="";
        $.each(modelIds,function (index,value){
            modelId+=value+",";
        });
        modelId=modelId.substring(0,modelId.length-1);
        layer.confirm('确定删除？', /*显示的内容*/
            {
                shift: 6,
                moveType: 1, //拖拽风格，0是默认，1是传统拖动
                title:"操作提示",  /*弹出框标题*/
                icon: 3,      /*消息内容前面添加图标*/
                btn: ['确定', '取消']/*可以有多个按钮*/
            }, function(index, layero){
                $.ajax({
                    type: 'POST',
                    url: context_path+'/processdesign/deleteModelByModelIds',
                    data: {modelIds: modelId},
                    success: function (data) {
                        layer.msg(data.result,{icon:1});
                        $("#processdesign_list_grid-table").jqGrid('setGridParam',
                            {
                                postData: {queryJsonString:""} //发送数据
                            }
                        ).trigger("reloadGrid");
                    },
                    dataType: 'json'
                });
            }, function(index){
                //取消按钮的回调
                layer.close(index);
            });
    }
    else{
        layer.msg("请选择一条记录",{icon:2});
    }
}

/**
 * 获取流程类型
 */
$("#processdesign_list_processDesignForm #processdesign_list_modelType").select2({
    placeholder: "选择流程类型",
    minimumInputLength: 0, //至少输入n个字符，才去加载数据
    allowClear: true, //是否允许用户清除文本信息
    delay: 250,
    formatNoMatches: "没有结果",
    formatSearching: "搜索中...",
    formatAjaxError: "加载出错啦！",
    ajax: {
        url: context_path + "/dictionary/getDictionaryDetailListByType",
        type: "POST",
        dataType: 'json',
        delay: 250,
        data: function (term, pageNo) { //在查询时向服务器端传输的数据
            term = $.trim(term);
            return {
                queryString: term, //联动查询的字符
                pageSize: 15, //一次性加载的数据条数
                pageNo: pageNo, //页码
                time: new Date(),
                dictionaryType:'LES_PROCESSDESIGN_TYPE'
            }
        },
        results: function (data, pageNo) {
            var res = data.result;
            if (res.length > 0) { //如果没有查询到数据，将会返回空串
                var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                return {
                    results: res,
                    more: more
                };
            } else {
                return {
                    results: {}
                };
            }
        },
        cache: true
    }

});

//确定按钮点击事件
$("#processdesign_list_rocessDesign .field-button .btn.btn-info").off("click").on("click", function(){
    //流程设计保存
    if($("#processdesign_list_processDesignForm").valid()){
        saveProcessDesin($("#processdesign_list_processDesignForm").serialize());
    }
});

//取消按钮点击事件
$("#processdesign_list_rocessDesign .field-button .btn.btn-danger").off("click").on("click", function(){
    layer.close($queryWindow);
});

/**
 * 流程设计保存
 * @param bean
 */
function saveProcessDesin(bean){
    if(bean){
        $("#processdesign_list_rocessDesign .field-button .btn.btn-info").attr("disabled","disabled");
        $.ajax({
            url:context_path+"/processdesign/createModel?tm="+new Date(),
            type:"POST",
            data:bean,
            dataType:"JSON",
            success:function(data){
                $("#processdesign_list_rocessDesign .field-button .btn.btn-info").removeAttr("disabled");
                if(data.result){
                    layer.msg("保存成功！",{icon:1});
                    //刷新流程设计列表
                    $("#processdesign_list_grid-table").jqGrid('setGridParam',
                        {
                            postData: {queryJsonString:""} //发送数据
                        }
                    ).trigger("reloadGrid");
                    //关闭当前窗口
                    //关闭指定的窗口对象
                    layer.close($queryWindow);
                }else{
                    layer.msg("保存失败，请稍后重试！",{icon:2});
                }
            }
        });
    }else{
        layer.msg("出错啦！",{icon:2});
    }
}