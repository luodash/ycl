/**
 * 查看补货任务
 */
function showView(){
	var checkedNum = getGridCheckedNum("#grid-table","id");
	if(checkedNum == 0){
    	layer.alert("请选择一条要查看的补货任务！");
    	return false;
    } else if(checkedNum >1){
    	layer.alert("只能选择一条补货任务进行查看！");
    	return false;
    } else {
    	var id = jQuery("#grid-table").jqGrid('getGridParam', 'selrow'); 
    	$.get( context_path + "/setPull/viewDetail.do?id="+id).done(function(data){
    	  layer.open({
    	  title : "配送单查看", 
    	  type:1,
    	  skin : "layui-layer-molv",
    	  area : ['750px', '650px'],
    	  shade : 0.6, //遮罩透明度
    	  moveType : 1, //拖拽风格，0是默认，1是传统拖动
    	  anim : 2,
    	  content : data
    	  });
    	});   		
    }
}

/**
 * 添加配送单
 */
function addDistributionOrder(){

	$.get( context_path + "/setPull/toAdd.do?id=-1").done(function(data){
		layer.open({
		    title : "配送单添加", 
	    	type:1,
	    	skin : "layui-layer-molv",
	    	area : ['780px', '620px'],
	    	shade : 0.6, //遮罩透明度
		    moveType : 1, //拖拽风格，0是默认，1是传统拖动
		    anim : 2,
		    //url : data.url,
		    content : data
	        /*url: context_path + "/role/toAdd.do",*/
	    	
		});
	});
}
function editAllocateOrder(){
	var checkedNum = getGridCheckedNum("#grid-table","id");
	if(checkedNum == 0){
    	layer.alert("请选择一条要编辑的补货任务！");
    	return false;
    } else if(checkedNum >1){
    	layer.alert("只能选择一条补货任务进行编辑操作！");
    	return false;
    } else {
    	var setId = jQuery("#grid-table").jqGrid('getGridParam', 'selrow'); 
    	$.get( context_path + "/setPull/toAdd.do?id="+setId).done(function(data){
    	  layer.open({
    	  title : "配送单编辑", 
    	  type:1,
    	  skin : "layui-layer-molv",
    	  area : ['750px', '650px'],
    	  shade : 0.6, //遮罩透明度
    	  moveType : 1, //拖拽风格，0是默认，1是传统拖动
    	  anim : 2,
    	  content : data
    	  });
    	 });
    		
    }
}
//收货单提交
function addReceiptOrder12(){

	$.get( context_path + "/replenish/toAdd.do?id=-1").done(function(data){
		layer.open({
		    title : "收货单添加", 
	    	type:1,
	    	skin : "layui-layer-molv",
	    	area : ['780px', '620px'],
	    	shade : 0.6, //遮罩透明度
		    moveType : 1, //拖拽风格，0是默认，1是传统拖动
		    anim : 2,
		    //url : data.url,
		    content : data
	        /*url: context_path + "/role/toAdd.do",*/
	    	
		});
	});
}

/**
 * 删除补货任务
 */
function delOutStorageOrder(){
	var checkedNum = getGridCheckedNum("#grid-table","id");  //选中的数量
	if(checkedNum==0){
		layer.alert("请至少选择一条要删除的补货单！");
	}else{
		//从数据库中删除选中的入库单，并刷新入库单表格
		var ids = jQuery("#grid-table").jqGrid('getGridParam', 'selarrrow');
		//弹出确认窗口
		layer.confirm("确定删除选中的补货任务？", function() {
    		$.ajax({
    			type : "POST",
    			url:context_path + "/replenish/delete?ids="+ids,
    			dataType : 'json',
    			cache : false,
    			success : function(data) {
    				layer.closeAll();
    				if(data){
    				
						layer.msg("补货任务删除成功！", {icon: 1,time:1000});
    				}else{
    					layer.msg("补货任务删除失败！", {icon: 2,time:1000});
    				}
    				_grid.trigger("reloadGrid");  //重新加载表格
    			}
    		});
    	});
	} 
}


/**
 * 打开查询界面
 */
function openOutstoreListSearchPage(){
	var queryBean = iTsai.form.serialize($('#hiddenQueryForm'));   //获取form中的值：json对象
	var queryJsonString = JSON.stringify(queryBean);         //将json对象转换成json字符串
	Dialog.open({
	    title: "出库单条件查询", 
	    width: 600,  height: '400',
	    url: context_path + "/outStorage/toQueryPage?outstoreString="+queryJsonString,
	    theme : "simple",
		drag : true
	});
}


/**
 * 收货单查询功能:获取查询页面中的值，并将值放入列表页面中隐藏的form
 * @param jsonParam     查询页面传递过来的json对象
 */
function queryInstoreListByParam(jsonParam){
	//序列化表单：iTsai.form.serialize($('#frm'))
	//反序列化表单：iTsai.form.deserialize($('#frm'),json)
	iTsai.form.deserialize($('#hiddenQueryForm'),jsonParam);   //将json对象反序列化到列表页面中隐藏的form中
	var queryParam = iTsai.form.serialize($('#hiddenQueryForm'));
	var queryJsonString = JSON.stringify(queryParam);         //将json对象转换成json字符串
	//执行查询操作
	$("#grid-table").jqGrid('setGridParam', 
			{
				postData: {queryJson:queryJsonString} //发送数据 
			}
	  ).trigger("reloadGrid");
}


/**
 * 显示提示窗口
 * @param msg   显示信息
 * @param delay 持续时间,结束之后窗口消失
 */
function showTipMsg(msg,delay){
	Dialog.tip( msg, {type: "loading", delay: delay} );
}

/** 表格刷新 */
function gridReload()
{
	 _grid.trigger("reloadGrid");  //重新加载表格  
}



/**
 *开始补货
 * @param msg
 * @param delay
 */
function startTran(){
	var checkedNum = getGridCheckedNum("#grid-table","id");
	if(checkedNum == 0)
	{
  	layer.alert("请选择一条补货任务！");
  	return false;
  } else if(checkedNum >1){
  	layer.alert("只能选择一条补货任务进行操作！");
  	return false;
  } else {
  	var saleId = jQuery("#grid-table").jqGrid('getGridParam', 'selrow'); 
  	var rowDate = jQuery("#grid-table").jqGrid('getRowData', id).state;
  	if(rowDate=='<span style="color:#d15b47;font-weight:bold;">未提交</span>'){
  		layer.msg("该单据为提交，不可开始移库",{time:1200,icon:2});
  		return ;
  	}
  	$.get( context_path + "/transfer/toStartTran.do?id="+id).done(function(data){
  	  layer.open({
  	  title : "补货任务操作", 
  	  type:1,
  	  skin : "layui-layer-molv",
  	  area : ['750px', '650px'],
  	  shade : 0.6, //遮罩透明度
  	  moveType : 1, //拖拽风格，0是默认，1是传统拖动
  	  anim : 2,
  	  content : data
  	  });
  	 });
  		
  }
}

/**
 *移库确认
 * @param msg
 * @param delay
 */
function completeTran(){
	var checkedNum = getGridCheckedNum("#grid-table","id");
	if(checkedNum == 0)
	{
  	layer.alert("请选择一条补货任务！");
  	return false;
  } else if(checkedNum >1){
  	layer.alert("只能选择一条补货任务进行操作！");
  	return false;
  } else {
  	var saleId = jQuery("#grid-table").jqGrid('getGridParam', 'selrow'); 
  	var rowDate = jQuery("#grid-table").jqGrid('getRowData', id).state;
  	if(rowDate=='<span style="color:#d15b47;font-weight:bold;">未提交</span>'){
  		layer.msg("该单据为提交，不可开始移库",{time:1200,icon:2});
  		return ;
  	}
  	$.get( context_path + "/transfer/toCompleteTran.do?id="+id).done(function(data){
  	  layer.open({
  	  title : "补货任务操作", 
  	  type:1,
  	  skin : "layui-layer-molv",
  	  area : ['750px', '650px'],
  	  shade : 0.6, //遮罩透明度
  	  moveType : 1, //拖拽风格，0是默认，1是传统拖动
  	  anim : 2,
  	  content : data
  	  });
  	 });
  		
  }
}



function allocationWorker(){
    var checkedNum = getGridCheckedNum("#grid-table", "id");
    if (checkedNum == 0) {
       layer.alert("请选择一条要分配的补货任务！");
       return false;
    } else {
       var id = jQuery("#grid-table").jqGrid("getGridParam", "selarrrow");
       $.post(context_path + "/replenishTask/allocationWorkerView.do?id="+id, {}, function (str){
		   $queryWindow=layer.open({
		       title : "分配补货人", 
	    	   type:1,
	    	   skin : "layui-layer-molv",
	    	   area : "490px",
	    	   shade : 0.6, //遮罩透明度
		       moveType : 1, //拖拽风格，0是默认，1是传统拖动
		       anim : 2,
		       content : str,
		       success: function (layero, index) {
               layer.closeAll("loading");
            }
		});
	});
  }
}