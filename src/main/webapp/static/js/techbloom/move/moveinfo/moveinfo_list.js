/**查询的方法**/
function moveinfo_list_queryInstoreListByParam(jsonParam){
    iTsai.form.deserialize($("#moveinfo_list_hiddenQueryForm"), jsonParam);
    var queryParam = iTsai.form.serialize($("#moveinfo_list_hiddenQueryForm"));
    var queryJsonString = JSON.stringify(queryParam);
    //执行查询操作
    $("#moveinfo_list_grid-table").jqGrid('setGridParam',
        {
            postData: {queryJsonString:queryJsonString} //发送数据
        }
    ).trigger("reloadGrid");
}

/***点击查看**/
function moveinfo_list_openInfoPage(){
    var selectAmount = getGridCheckedNum("#moveinfo_list_grid-table");
    if(selectAmount==0){
        layer.msg("请选择一条记录！",{icon:2});
        return;
    }else if(selectAmount>1){
        layer.msg("只能选择一条记录！",{icon:8});
        return;
    }
    layer.load(2);
    $.post(context_path + "/move/toInfo.do", {
        id:jQuery("#moveinfo_list_grid-table").jqGrid("getGridParam", "selrow")
    }, function (str){
        $queryWindow=layer.open({
            title : "查看列表",
            type:1,
            skin : "layui-layer-molv",
            area : "1200px",
            shade : 0.6, //遮罩透明度
            moveType : 1, //拖拽风格，0是默认，1是传统拖动
            anim : 2,
            content : str,
            success: function (layero, index) {
                layer.closeAll('loading');
            }
        });
    });
}

/**打开新增页面**/
function moveinfo_list_openAddPage(){
    layer.load(2);
    $.post(context_path+'/move/toAdd.do', {}, function(str){
        $queryWindow = layer.open({
            title : "添加",
            type: 1,
            skin : "layui-layer-molv",
            area : "1200px",
            shade: 0.6, //遮罩透明度
            moveType: 1, //拖拽风格，0是默认，1是传统拖动
            content: str,//注意，如果str是object，那么需要字符拼接。
            success:function(layero, index){
                layer.closeAll("loading");
            }
        });
    }).error(function() {
        layer.closeAll();
        layer.msg("加载失败！",{icon:2});
    });
}

/**打开编辑页面**/
function moveinfo_list_openEditPage(){
    var selectAmount = getGridCheckedNum("#moveinfo_list_grid-table");
    if(selectAmount==0){
        layer.msg("请选择一条记录！",{icon:2});
        return;
    }else if(selectAmount>1){
        layer.msg("只能选择一条记录！",{icon:8});
        return;
    }
    var rowDataState = jQuery("#moveinfo_list_grid-table").jqGrid('getRowData',
        jQuery("#moveinfo_list_grid-table").jqGrid('getGridParam', 'selrow')).moveState;
    if (rowDataState.indexOf("新建")==-1) {
        layer.msg("该移库单已经开始，不能编辑！",{icon:8});
        return;
    }
    layer.load(2);
    $.post(context_path+'/move/editInfo.do', {
        id:jQuery("#moveinfo_list_grid-table").jqGrid('getGridParam', 'selrow')
    }, function(str){
        $queryWindow = layer.open({
            title : "编辑",
            type: 1,
            skin : "layui-layer-molv",
            area : "1200px",
            shade: 0.6, //遮罩透明度
            moveType: 1, //拖拽风格，0是默认，1是传统拖动
            content: str,//注意，如果str是object，那么需要字符拼接。
            success:function(layero, index){
                layer.closeAll("loading");
            }
        });
    }).error(function() {
        layer.closeAll();
        layer.msg("加载失败！",{icon:2});
    });
}

/**导出**/
function moveinfo_list_toExcel(){
    $("#moveinfo_list_hiddenForm #moveinfo_list_ids").val(jQuery("#moveinfo_list_grid-table").jqGrid("getGridParam", "selarrrow"));
    $("#moveinfo_list_hiddenForm #moveinfo_list_queryMoveCode").val($("#moveinfo_list_queryForm #moveinfo_list_moveCode").val());
    $("#moveinfo_list_hiddenForm #moveinfo_list_queryFactoryCode").val($("#moveinfo_list_queryForm #moveinfo_list_factoryCode").val());
    $("#moveinfo_list_hiddenForm #moveinfo_list_queryWarehouse").val($("#moveinfo_list_queryForm #moveinfo_list_warehouse").val());
    $("#moveinfo_list_hiddenForm #moveinfo_list_queryStartTime").val($("#moveinfo_list_queryForm #moveinfo_list_startTime").val());
    $("#moveinfo_list_hiddenForm #moveinfo_list_queryEndTime").val($("#moveinfo_list_queryForm #moveinfo_list_endTime").val());
    $("#moveinfo_list_hiddenForm #moveinfo_list_queryCreater").val($("#moveinfo_list_queryForm #moveinfo_list_creater").val());
    $("#moveinfo_list_hiddenForm #moveinfo_list_queryExportExcelIndex").val(moveinfo_list_exportExcelIndex);
    $("#moveinfo_list_hiddenForm").submit();
}

/**刷新列表**/
function gridReload(){
    $("#moveinfo_list_grid-table").jqGrid('setGridParam',
        {
            url : context_path + '/move/movePageList.do',
            postData: {queryJsonString:""} //发送数据  :选中的节点
        }
    ).trigger("reloadGrid");
}

/**根据id删除**/
function moveinfo_list_deleteByIds(){
    var checkedNum = getGridCheckedNum("#moveinfo_list_grid-table","id");  //选中的数量
    if(checkedNum==0){
        layer.alert("请选择一条记录！");
    }else{
        layer.confirm("确定删除选择的调拨单数据",function(){
            $.ajax({
                type:"POST",
                url:context_path + "/move/deleteByIds?ids="+jQuery("#moveinfo_list_grid-table").jqGrid('getGridParam', 'selarrrow'),
                dataType:"json",
                success:function(data){
                    if(data.result){
                        layer.msg(data.msg);
                        gridReload();   //重新加载详情列表
                    }else{
                        layer.msg(data.msg);
                    }
                }
            })
        });
    }
}