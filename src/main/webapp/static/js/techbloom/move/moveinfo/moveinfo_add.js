//保存按钮
function saveFormInfo(formdata){
    var id = $("#moveinfo_add_baseInfor #moveinfo_add_id").val();
    if(id!=-1){
        layer.confirm("确定修改,修改之后当前质保单号将会删除", function (){
            $("#moveinfo_add_formSave").attr("disabled","disabled");
            $(window).triggerHandler('resize.jqGrid');
            $.ajax({
                type:"POST",
                url:context_path + "/move/saveMoveStorage",
                data:formdata,
                dataType:"json",
                success:function(data){
                    if(data.result){
                        $("#moveinfo_add_id").val(data.allocationId)
                        layer.alert("移库单保存成功");
                        $("#moveinfo_add_formSave").removeAttr("disabled");
                        gridReload();
                    }else{
                        layer.alert("移库单保存失败");
                    }
                }
            })
        })
    }else{
        $("#moveinfo_add_formSave").attr("disabled","disabled");
        $(window).triggerHandler('resize.jqGrid');
        $.ajax({
            type:"POST",
            url:context_path + "/move/saveMoveStorage",
            data:formdata,
            dataType:"json",
            success:function(data){
                if(data.result){
                    $("#moveinfo_add_id").val(data.allocationId)
                    layer.alert("移库单保存成功");
                    $("#moveinfo_add_formSave").removeAttr("disabled");
                    gridReload();
                }else{
                    layer.alert("移库单保存失败");
                }
            }
        })
    }

}

//删除详情中的数据
function delDetail(){
    var checkedNum = getGridCheckedNum("#moveinfo_add_grid-table-c","id");  //选中的数量
    if(checkedNum==0){
        layer.alert("请至少选择一条记录！");
    }else{
        layer.confirm("确定删除选择的详情数据",function(){
            $.ajax({
                type:"POST",
                url:context_path + "/move/deleteMoveStorageDetailIds?ids="+jQuery("#moveinfo_add_grid-table-c").jqGrid('getGridParam', 'selarrrow'),
                dataType:"json",
                success:function(data){
                    if(data.result){
                        layer.msg(data.msg);
                        //重新加载详情表格
                        $("#moveinfo_add_grid-table-c").jqGrid("setGridParam",
                            {
                                postData: {id:$("#moveinfo_add_baseInfor #moveinfo_add_id").val()} //发送数据  :选中的节点
                            }
                        ).trigger("reloadGrid");
                    }else{
                        layer.msg(data.msg);
                    }
                }
            })
        });
    }
}


//提交
function outformSubmitBtn(){
    if($('#moveinfo_add_baseInfor #moveinfo_add_id').val()==-1){
        layer.alert("请先保存单据");
        return;
    }else {
        var formdata = $('#moveinfo_add_baseInfor').serialize();
        //后台修改对应的数据
        layer.confirm("确定提交,提交之后数据不能修改", function () {
            $.ajax({
                type:"POST",
                url:context_path+"/move/subMoveStorage",
                dataType:"json",
                data:formdata,
                success:function(data){
                    if(data.result){
                        layer.closeAll();
                        layer.msg("提交成功",{icon:1,time:1200});
                        gridReload();
                    }else{
                        layer.alert(data.msg);
                    }
                }
            })
        });
    }
}

/**刷新列表**/
function gridReload(){
    $("#moveinfo_list_grid-table").jqGrid('setGridParam',
        {
            url : context_path + '/move/movePageList.do',
            postData: {queryJsonString:""} //发送数据  :选中的节点
        }
    ).trigger("reloadGrid");
}

/**刷新列表**/
function moveinfo_list_gridReload(){
    $("#moveinfo_list_grid-table").jqGrid('setGridParam',
        {
            url : context_path + '/move/movePageList.do',
            postData: {queryJsonString:""} //发送数据  :选中的节点
        }
    ).trigger("reloadGrid");
}