//保存按钮
function saveFormInfo(formdata){
    $("#moveinfo_edit_formSave").attr("disabled","disabled");
    $(window).triggerHandler('resize.jqGrid');
    $.ajax({
        type:"POST",
        url:context_path + "/move/saveMoveStorage",
        data:formdata,
        dataType:"json",
        success:function(data){
            if(data.result){
                reloadDetailTableList();   //重新加载详情列表
                layer.alert("发货单保存成功");
                $("#moveinfo_edit_formSave").removeAttr("disabled");
                layer.close($queryWindow);
                gridReload();
            }else{
                layer.alert("发货单保存失败");
            }
        }
    })
}

//重新加载详情列表
function reloadDetailTableList(){
    $("#moveinfo_edit_grid-table-c").jqGrid('setGridParam',
        {
            url:context_path + "/move/detailList.do?id="+$("#moveinfo_edit_id").val(),
        }
    ).trigger("reloadGrid");
}

//提交
function outformSubmitBtn(){
    if($('#moveinfo_edit_baseInfor #moveinfo_edit_id').val()==-1){
        layer.alert("请先保存单据");
        return;
    }else {
        var formdata = $('#moveinfo_edit_baseInfor').serialize();
        //后台修改对应的数据
        layer.confirm("确定提交,提交之后数据不能修改", function () {
            $.ajax({
                type:"POST",
                url:context_path+"/move/subMoveStorage2",
                dataType:"json",
                data:formdata,
                success:function(data){
                    if(data.result){
                        layer.closeAll();
                        layer.msg("提交成功",{icon:1,time:1200});
                        gridReload();
                    }else{
                        layer.alert(data.msg);
                    }
                }
            })
        });
    }
}

//删除详情中的数据
function delDetail(){
    var checkedNum = getGridCheckedNum("#moveinfo_edit_grid-table-c","id");  //选中的数量
    if(checkedNum==0){
        layer.alert("请选择一条记录！");
    }else{
        layer.confirm("确定删除选择的调拨数据？",function(){
            $.ajax({
                type:"POST",
                url:context_path + "/move/deleteMoveStorageDetailIds?ids="+jQuery("#moveinfo_edit_grid-table-c").jqGrid('getGridParam', 'selarrrow'),
                dataType:"json",
                success:function(data){
                    if(data.result){
                        moveinfo_list_gridReload();
                        layer.msg(data.msg);
                        reloadDetailTableList();   //重新加载详情列表
                    }else{
                        layer.msg(data.msg);
                    }
                }
            })
        });
    }
}

/**刷新列表**/
function moveinfo_list_gridReload(){
    iTsai.form.deserialize($("#moveinfo_list_hiddenQueryForm"), iTsai.form.serialize($("#moveinfo_list_queryForm")));
    var queryParam = iTsai.form.serialize($("#moveinfo_list_hiddenQueryForm"));
    var queryJsonString = JSON.stringify(queryParam);
    $("#alloinfo_list_grid-table").jqGrid('setGridParam',
        {
            url : context_path + '/move/movePageList.do',
            postData: {queryJsonString:queryJsonString} //发送数据  :选中的节点
        }
    ).trigger("reloadGrid");
}