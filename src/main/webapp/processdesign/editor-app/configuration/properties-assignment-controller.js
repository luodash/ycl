/*
 * Activiti Modeler component part of the Activiti project
 * Copyright 2005-2014 Alfresco Software, Ltd. All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */


/*
 * action
 */

var inputactionCtrl =  [ '$scope', '$modal', function($scope, $modal) {

    // Config for the modal window
    var opts = {
        template:  'editor-app/configuration/properties/inputaction-popup.html?version=' + Date.now(),
        scope: $scope
    };

    // Open the dialog
    $modal(opts);
}];
var inputactionPopupCtrl = [ '$scope','$http', function($scope, $http) {
    
	$http({method: 'GET', url:  window.top.context_path +"/actiondefine/getActionList",params:{type: 1}}).
    success(function (data, status, headers, config) {
    	
 	   if(data){
 		   $scope.items = data.items;  
 	   }
    }).
    error(function (data, status, headers, config) {
     
    });
	$scope.save = function() {
        $scope.property.value = {};
        $scope.property.value = $scope.action.inputaction;
        if($scope.action.inputaction){
        	$http({method: 'GET', url:  window.top.context_path +"/actiondefine/saveTaskActionWebServiceUrl/"+$scope.editor.modelMetaData.modelId+"/"+ ($scope.editor.getSelection()[0] ? $scope.editor.getSelection()[0].toJSON().properties.overrideid : "process") +"/"+$scope.action.inputaction}).
            success(function (data, status, headers, config) {
         	   
            }).
            error(function (data, status, headers, config) {
             
            });
        }
        $scope.updatePropertyInModel($scope.property);
        $scope.close();
    };

    // Close button handler
    $scope.close = function() {
    	$scope.property.mode = 'read';
    	$scope.$hide();
    };
}];


var outputactionCtrl =  [ '$scope', '$modal', function($scope, $modal) {

    // Config for the modal window
    var opts = {
        template:  'editor-app/configuration/properties/outputaction-popup.html?version=' + Date.now(),
        scope: $scope
    };

    // Open the dialog
    $modal(opts);
}];
var outputactionPopupCtrl = [ '$scope','$http', function($scope, $http) {
    
	$http({method: 'GET', url:  window.top.context_path +"/actiondefine/getActionList",params:{type: 3}}).
    success(function (data, status, headers, config) {
 	   if(data){
 		   $scope.items =data.items; 
 	   }
    }).
    error(function (data, status, headers, config) {
     
    });
	$scope.save = function() {
        $scope.property.value = {};
        $scope.property.value = $scope.action.outputaction;
        if($scope.action.outputaction){
        	$http({method: 'GET', url:  window.top.context_path +"/actiondefine/saveTaskActionWebServiceUrl/"+$scope.editor.modelMetaData.modelId+"/"+ $scope.editor.getSelection()[0].toJSON().properties.overrideid+"/"+$scope.action.outputaction}).
            success(function (data, status, headers, config) {
         	   
            }).
            error(function (data, status, headers, config) {
             
            });
        }
        $scope.updatePropertyInModel($scope.property);
        $scope.close();
    };

    // Close button handler
    $scope.close = function() {
    	$scope.property.mode = 'read';
    	$scope.$hide();
    };
}];

var usertasktypeCtrl =  [ '$scope', '$modal', function($scope, $modal) {

    // Config for the modal window
    var opts = {
        template:  'editor-app/configuration/properties/usertasktype-popup.html?version=' + Date.now(),
        scope: $scope
    };

    // Open the dialog
    $modal(opts);
}];


var usertasktypePopupCtrl = [ '$scope','$http', function($scope, $http) {
	$http({method: 'GET', url:   KISBPM.URL.getIconPath() }).
    success(function (data, status, headers, config) {
 	   if(data){
 		   $scope.items = data; 
 	   }
    }).
    error(function (data, status, headers, config) {
     
    });
	
	$scope.save = function() {
		
      
        for(var i= 0; i < $scope.items.length; i++){
        	if($scope.items[i].id  == $scope.property.value  ){
        		var _path = $scope.items[i].path;
        		$scope.editor.getSelection()[0].node.querySelector("path").parentNode.innerHTML =_path; 
        	}
        }
        $scope.updatePropertyInModel($scope.property);
        $scope.close();
    };

    // Close button handler
    $scope.close = function() {
    	$scope.property.mode = 'read';
    	$scope.$hide();
    };
}];



var processactionCtrl =  [ '$scope', '$modal', function($scope, $modal) {

    // Config for the modal window
    var opts = {
        template:  'editor-app/configuration/properties/processaction-popup.html?version=' + Date.now(),
        scope: $scope
    };

    // Open the dialog
    $modal(opts);
}];
var processactionPopupCtrl = [ '$scope','$http', function($scope, $http) {
    
	$http({method: 'GET', url:  window.top.context_path +"/actiondefine/getActionList",params:{type: 2}}).
    success(function (data, status, headers, config) {
 	   if(data){
 		   $scope.items = data.items; 
 	   }
    }).
    error(function (data, status, headers, config) {
     
    });
	$scope.save = function() {
        $scope.property.value = {};
        $scope.property.value = $scope.action.processaction;
        if($scope.action.processaction){
        	$http({method: 'GET', url:  window.top.context_path +"/actiondefine/saveTaskActionWebServiceUrl/"+$scope.editor.modelMetaData.modelId+"/"+ $scope.editor.getSelection()[0].toJSON().properties.overrideid+"/"+$scope.action.processaction}).
            success(function (data, status, headers, config) {
         	   
            }).
            error(function (data, status, headers, config) {
             
            });
        }
        $scope.updatePropertyInModel($scope.property);
        $scope.close();
    };

    // Close button handler
    $scope.close = function() {
    	$scope.property.mode = 'read';
    	$scope.$hide();
    };
	    
}];
/*
 * Assignment
 */
var KisBpmAssignmentCtrl = [ '$scope', '$modal', function($scope, $modal) {

    // Config for the modal window
    var opts = {
        template:  'editor-app/configuration/properties/assignment-popup.html?version=' + Date.now(),
        scope: $scope
    };

    // Open the dialog
    $modal(opts);
}];

var KisBpmAssignmentPopupCtrl = [ '$scope','$http', function($scope, $http) {
    
	
	$http({method: 'GET', url:  window.top.context_path +"/processdesign/getProcessRouteList"}).
    success(function (data, status, headers, config) {
 	   if(data && data.length > 0 ){
 		   $scope.items = data; 
 	   }
    }).
    error(function (data, status, headers, config) {
     
    });
    // Put json representing assignment on scope
    if ($scope.property.value !== undefined && $scope.property.value !== null
        && $scope.property.value.assignment !== undefined
        && $scope.property.value.assignment !== null) 
    {
        $scope.assignment = $scope.property.value.assignment;
    } else {
        $scope.assignment = {};
    }

    if ($scope.assignment.candidateUsers == undefined || $scope.assignment.candidateUsers.length == 0)
    {
    	$scope.assignment.candidateUsers = [{value: ''}];
    }
    
    // Click handler for + button after enum value
    var userValueIndex = 1;
    $scope.addCandidateUserValue = function(index) {
        $scope.assignment.candidateUsers.splice(index + 1, 0, {value: 'value ' + userValueIndex++});
    };

    // Click handler for - button after enum value
    $scope.removeCandidateUserValue = function(index) {
        $scope.assignment.candidateUsers.splice(index, 1);
    };
    
    if ($scope.assignment.candidateGroups == undefined || $scope.assignment.candidateGroups.length == 0)
    {
    	$scope.assignment.candidateGroups = [{value: ''}];
    }
    
    var groupValueIndex = 1;
    $scope.addCandidateGroupValue = function(index) {
        $scope.assignment.candidateGroups.splice(index + 1, 0, {value: 'value ' + groupValueIndex++});
    };

    // Click handler for - button after enum value
    $scope.removeCandidateGroupValue = function(index) {
        $scope.assignment.candidateGroups.splice(index, 1);
    };

    $scope.save = function() {

        $scope.property.value = {};
        handleAssignmentInput($scope);
        $scope.property.value.assignment = $scope.assignment;
        if( $scope.assignment){
        	$http({method: 'GET', url:  window.top.context_path +"/processdesign/bindStationToModelRoute/"+$scope.editor.modelMetaData.modelId+"/"+ $scope.editor.getSelection()[0].toJSON().properties.overrideid+"/"+ $scope.assignment.assignee}).
            success(function (data, status, headers, config) {
         	   
            }).
            error(function (data, status, headers, config) {
             
            });
        }
        $scope.updatePropertyInModel($scope.property);
        $scope.close();
    };

    // Close button handler
    $scope.close = function() {
    	handleAssignmentInput($scope);
    	$scope.property.mode = 'read';
    	$scope.$hide();
    };
    
    var handleAssignmentInput = function($scope) {
    	if ($scope.assignment.candidateUsers)
    	{
	    	var emptyUsers = true;
	    	var toRemoveIndexes = [];
	        for (var i = 0; i < $scope.assignment.candidateUsers.length; i++)
	        {
	        	if ($scope.assignment.candidateUsers[i].value != '')
	        	{
	        		emptyUsers = false;
	        	}
	        	else
	        	{
	        		toRemoveIndexes[toRemoveIndexes.length] = i;
	        	}
	        }
	        
	        for (var i = 0; i < toRemoveIndexes.length; i++)
	        {
	        	$scope.assignment.candidateUsers.splice(toRemoveIndexes[i], 1);
	        }
	        
	        if (emptyUsers)
	        {
	        	$scope.assignment.candidateUsers = undefined;
	        }
    	}
        
    	if ($scope.assignment.candidateGroups)
    	{
	        var emptyGroups = true;
	        var toRemoveIndexes = [];
	        for (var i = 0; i < $scope.assignment.candidateGroups.length; i++)
	        {
	        	if ($scope.assignment.candidateGroups[i].value != '')
	        	{
	        		emptyGroups = false;
	        	}
	        	else
	        	{
	        		toRemoveIndexes[toRemoveIndexes.length] = i;
	        	}
	        }
	        
	        for (var i = 0; i < toRemoveIndexes.length; i++)
	        {
	        	$scope.assignment.candidateGroups.splice(toRemoveIndexes[i], 1);
	        }
	        
	        if (emptyGroups)
	        {
	        	$scope.assignment.candidateGroups = undefined;
	        }
    	}
    };
}];