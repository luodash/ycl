var KisBpmCustomparametersCtrl = [ '$scope', '$modal', '$timeout', '$translate', function($scope, $modal, $timeout, $translate) {

    // Config for the modal window
    var opts = {
        template:  'editor-app/configuration/properties/customparameters-popup.html?version=' + Date.now(),
        scope: $scope
    };

    // Open the dialog
    $modal(opts);
}];

var KisBpmCustomparametersPopupCtrl = [ '$scope', '$q', '$translate','$http', function($scope, $q, $translate, $http) {
	   
	var modelId = EDITOR.UTIL.getParameterByName('modelId');
	var modelUrl = KISBPM.URL.getModel(modelId);
	var  values = $scope.property.value ;
	$http({method: 'GET', url:  ACTIVITI.CONFIG.contextRoot +"/model/"+modelId+"/"+ $scope.editor.getSelection()[0].toJSON().properties.overrideid +"/bom"}).
       success(function (data, status, headers, config) {
    	   if(data && data.length > 0 ){
    		   jQuery("#grid-table").jqGrid({
       		    data :data,
       		    datatype:"local",
       		    styleUI: 'Bootstrap',
       		    colNames : [ '物料id', '物料编号','物料名称','物料数量' ],
       		    colModel : [
       		             {name : 'materialId',index : 'materialId', hidden: true},     
       		        {name : 'materialNo',index : 'materialNo'},      
       		        {name : 'materialName',index : 'materialName'},
       		        {name : 'materialNumber',index : 'materialNumber', editable: true,editrules: {number:true}
       		        }
       		       
       		    ],
       		    cellsubmit:"clientArray",
       		    cellEdit: true,
       		    pager : '#grid-pager',
       		    hidegrid:false,
       		    autowidth:true, 
       		    multiselect:true,
       		    pgbuttons: false,
       		    viewrecords : true,
       	        pginput:false,
       		    rowNum:1000000,
       		    emptyrecords: "没有相关记录",
       		    loadComplete : function(data) 
       	        {
       	        	var table = this;
       	        	oriData = data;
       	        	if(values){
       	        		var _values = values.split(",");
           	        	for(var i = 0; i < _values.length; i++){
           	     		  var materialNo = _values[i].split("(")[0];
           	     		   jQuery("#grid-table td:contains('"+materialNo+"')").parents("tr").find("input:checkbox").trigger("click");
           	     	    }
       	        	}
       	        	
       	        },
       	        onSelectRow : function(id,s){
       	        },
       		    loadtext: "加载中...",
       		    pgtext : "页码 {0} / {1}页",
       		    recordtext: "显示 {0} - {1}共{2}条数据"
       		});
    	   }
    	
       }).
       error(function (data, status, headers, config) {
         console.log('Error loading model with id ' + modelId + ' ' + data);
       });
 
    $scope.save = function() {
    	jQuery('#grid-table').jqGrid("editCell",0,0,true);
    	var idAddr = jQuery('#grid-table').getGridParam("selarrrow");
    	var _ids = "", arr=[], strs_save =[], strs_show =[];
    	if( idAddr && idAddr.length > 0 )
    	{
    	   for( var i = 0; i < idAddr.length; i++ )
    	   {
    	
    		   var data =jQuery('#grid-table').jqGrid('getRowData',idAddr[i]);
    		 //  if(data["materialNumber"] && data["materialNumber"] > 0){
    			   arr.push( data );
        		   strs_save.push(data["materialId"]+"M"+data["materialNumber"]);
        		   strs_show.push(data["materialName"]+"("+data["materialNumber"]+"个)");
    		//   }
    		   
    	   }
    	}
    	$http({method: 'POST', url:  window.top.context_path +"/bom/bindMaterialToModel?modelId="+$scope.editor.modelMetaData.modelId+"&node_id="+ $scope.editor.getSelection()[0].toJSON().properties.overrideid +"&materialInfo="+strs_save.join("N")+""}).
        success(function (data, status, headers, config) {
     	   
        }).
        error(function (data, status, headers, config) {
         
        });
    	$scope.property.value = strs_show.join(",") ;
        $scope.updatePropertyInModel($scope.property);
        $scope.close();
    };

    $scope.close = function() {
        $scope.property.mode = 'read';
        $scope.$hide();
    };
}];