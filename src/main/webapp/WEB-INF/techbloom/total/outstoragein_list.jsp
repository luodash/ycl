<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="outstoragein_list_grid-div">
    <form id="outstoragein_list_hiddenForm" action="<%=path%>/outTotal/toExcel.do" method="POST" style="display: none;">
        <input id="outstoragein_list_ids" name="ids" value=""/>
        <input id="outstoragein_list_queryExportExcelIndex" name="queryExportExcelIndex" value=""/>
        <input id="outstoragein_list_queryQacode" name="qaCode" value=""/>
        <input id="outstoragein_list_queryStartTime" name="queryStartTime" value=""/>
        <input id="outstoragein_list_queryEndTime" name="queryEndTime" value=""/>
        <input id="outstoragein_list_queryShipNo" name="shipNo" value=""/>
    </form>
    <form id="outstoragein_list_hiddenQueryForm" style="display:none;">
        <input name="qaCode" value=""/>
        <input name="queryStartTime" value="">
        <input name="queryEndTime" value=""/>
        <input name="shipNo" value=""/>
    </form>
    <c:if test="${operationCode.webSearch==1}">
    <div class="query_box" id="outstoragein_list_yy" title="查询选项">
        <form id="outstoragein_list_queryForm" style="max-width:100%;">
            <ul class="form-elements">
                <li class="field-group field-fluid3">
                    <label class="inline" for="outstoragein_list_qaCode" style="margin-right:20px;width: 100%;">
                        <span class="form_label" style="width:80px;">质保单号：</span>
                        <input id="outstoragein_list_qaCode" name="qaCode" type="text" style="width: calc(100% - 125px);" placeholder="质保单号">
                    </label>
                </li>
                <li class="field-group field-fluid3">
					<label class="inline" for="outstoragein_list_startTime" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:80px;">起始时间：</span>
						<input id="outstoragein_list_startTime" name="queryStartTime" class="form-control date-picker" type="text" style="width: calc(100% - 125px );" placeholder="出库时间起" />
					</label>			
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="outstoragein_list_endTime" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:80px;">截止时间：</span>
						<input id="outstoragein_list_endTime" name="queryEndTime" class="form-control date-picker" type="text" style="width: calc(100% - 125px );" placeholder="出库时间止" />
					</label>			
				</li>

                <li class="field-group-top field-group field-fluid3">
                    <label class="inline" for="outstoragein_list_shipNo" style="margin-right:20px;width:100%;">
                        <span class="form_label" style="width:78px;">发货单号：</span>
                        <input id="outstoragein_list_shipNo" name="shipNo" type="text" style="width: calc(100% - 155px);" placeholder="提货单号">
                    </label>
                </li>
            </ul>
            <div class="field-button" style="">
                <div class="btn btn-info" onclick="outstoragein_list_queryOk();">
                    <i class="ace-icon fa fa-check bigger-110"></i>查询
                </div>
                <div class="btn" onclick="outstoragein_list_reset();"><i class="ace-icon icon-remove"></i>重置</div>
                <a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>
            </div>
        </form>
    </div>
    </c:if>
    <div id="outstoragein_list_fixed_tool_div" class="fixed_tool_div">
        <div id="outstoragein_list_toolbar_" style="float:left;overflow:hidden;"></div>
    </div>
    <div style="overflow-x:auto"><table id="outstoragein_list_grid-table" style="width:100%;height:100%;"></table></div>
    <div id="outstoragein_list_grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
	var outstoragein_list_oriData;
	var outstoragein_list_grid;
    var outstoragein_list_exportExcelIndex;

	//时间控件
    $(".date-picker").datetimepicker({
        format: "YYYY-MM-DD HH:mm:ss" ,
        autoclose : true,
        todayHighlight : true
    });

    $("input").keypress(function (e) {
        if (e.which == 13) {
            outstoragein_list_queryOk();
        }
    });

	$(function  (){
	    $(".toggle_tools").click();
	});

	$("#outstoragein_list_toolbar_").iToolBar({
	    id: "outstoragein_list_tb_01",
	    items: [
	        {label: "导出",hidden:"${operationCode.webExport}"=="1",onclick:function(){outstoragein_list_toExecl();},iconClass:'icon-share'}
	   ]
	});
	
	var outstoragein_list_queryForm_data = iTsai.form.serialize($("#outstoragein_list_queryForm"));

    outstoragein_list_grid = jQuery("#outstoragein_list_grid-table").jqGrid({
	    url : context_path + "/outTotal/list.do",
	    datatype : "json",
	    colNames : ["id","质保单号","批次号","发货单号","物料编号","物料名称","盘号","销售经理","盘类型","盘规格","盘外径","盘内径","米数","仓库编码", "仓库",
            "库位","发货人","发货时间","出库人","出库时间"],
	    colModel : [
	    	{name : "id",index : "id",hidden : true},
	        {name : "qaCode",index : "qaCode",width:180},
	        {name : "batchNo",index : "batch_no",width:180},
            {name : "shipNo",index : "ship_no",width:180},
	        {name : "materialCode",index : "materialCode",width : 160},
	        {name : "materialName",index : "materialName",width :170},
            {name : "dishcode",index : "dishcode",width :170},
	        {name : "salesname",index : "salesname",width : 170},
	        {name : "drumType",index : "drumType",width : 50},
	        {name : "model",index : "model",width : 190},
            {name : "outerDiameter",index : "outerDiameter",width : 50},
            {name : "innerDiameter",index : "innerDiameter",width :50},
	        {name : "meter",index : "meter",width : 50},
	        {name : "warehouseCode",index : "warehouseCode",hidden : true},
	        {name : "warehouseName",index : "warehouseName",width : 100},
	        {name : "shelfCode",index : "shelfCode",width : 140},
            {name : "startstorageName",index : "startstorageName",width : 90},
            {name : "startStorageTime",index : "startStorageTime",width : 140},
            {name : "pickManName",index : "pickManName",width : 90},
            {name : "outStorageTime",index : "outStorageTime",width : 140}

	    ],
	    rowNum : 20,
	    rowList : [ 10, 20, 30 ],
	    pager : "#outstoragein_list_grid-pager",
	    sortname : "v.id",
	    sortorder : "asc",
        altRows: true,
        viewrecords : true,
        hidegrid:false,
    	autowidth:false,
        multiselect:true,
        multiboxonly:true,
        shrinkToFit:false,
        autoScroll: true,
        loadComplete : function(data){
           	var table = this;
           	setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            outstoragein_list_oriData = data;
           	$("#outstoragein_list_grid-table").closest(".ui-jqgrid-bdiv").css({ 'overflow-x': 'auto' });
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
	});

	jQuery("#outstoragein_list_grid-table").navGrid("#outstoragein_list_grid-pager",
    {edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#outstoragein_list_grid-pager",{
		caption:"",   
		buttonicon:"fa fa-refresh green",   
		onClickButton: function(){   
			$("#outstoragein_list_grid-table").jqGrid("setGridParam", 
					{
				      postData: {queryJsonString:""} //发送数据 
					}
			).trigger("reloadGrid");
		}
	}).navButtonAdd("#outstoragein_list_grid-pager",{
        caption: "",
        buttonicon:"fa icon-cogs",
        onClickButton : function (){
            jQuery("#outstoragein_list_grid-table").jqGrid("columnChooser",{
                done: function(perm, cols){
                    $("#outstoragein_list_grid-table").jqGrid("setGridWidth", $("#outstoragein_list_grid-div").width());
                    outstoragein_list_exportExcelIndex = perm;
                }
            });
        }
    });

	$(window).on("resize.jqGrid", function () {
		$("#outstoragein_list_grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);
		$("#outstoragein_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-10-
		$("#outstoragein_list_yy").outerHeight(true)-$("#outstoragein_list_fixed_tool_div").outerHeight(true)-
		$("#outstoragein_list_grid-pager").outerHeight(true)-$("#gview_outstoragein_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
	});
	
	$(window).triggerHandler("resize.jqGrid");

	/**
	 * 查询按钮点击事件
	 */
	 function outstoragein_list_queryOk(){
		 var queryParam = iTsai.form.serialize($("#outstoragein_list_queryForm"));
		 //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
		 outstoragein_list_queryByParam(queryParam);
	 }

	 function outstoragein_list_queryByParam(jsonParam) {
	    iTsai.form.deserialize($("#outstoragein_list_hiddenQueryForm"), jsonParam);
	    var queryParam = iTsai.form.serialize($("#outstoragein_list_hiddenQueryForm"));
	    var queryJsonString = JSON.stringify(queryParam); 
	    $("#outstoragein_list_grid-table").jqGrid("setGridParam",
	        {
	            postData: {queryJsonString: queryJsonString}
	        }
	    ).trigger("reloadGrid");
	 }
 
	 //清空重置
	function outstoragein_list_reset(){	
		 iTsai.form.deserialize($("#outstoragein_list_queryForm"),outstoragein_list_queryForm_data);
		 outstoragein_list_queryByParam(outstoragein_list_queryForm_data);
	}

    //出库类型
    $("#outstoragein_list_queryForm #outstoragein_list_outStorageType").select2({
        placeholder: "选择类型",
        multiple:true,
        minimumInputLength:0,   //至少输入n个字符，才去加载数据
        allowClear: true,  //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches:"没有结果",
        formatSearching:"搜索中...",
        formatAjaxError:"加载出错啦！",
        ajax : {
            url: context_path+"/BaseDicType/getOutStorageTypeList",
            type:"POST",
            dataType : 'json',
            delay : 250,
            data: function (term,pageNo) {     //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term,    //联动查询的字符
                    pageSize: 15,    //一次性加载的数据条数
                    pageNo:pageNo    //页码
                }
            },
            results: function (data,pageNo) {
                var res = data.result;
                if(res.length>0){   //如果没有查询到数据，将会返回空串
                    var more = (pageNo*15)<data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results:res,more:more
                    };
                }else{
                    return {
                        results:{}
                    };
                }
            },
            cache : true
        }
    });

	 //导出Excel报表
	function outstoragein_list_toExecl(){
	    $("#outstoragein_list_hiddenForm #outstoragein_list_ids").val(jQuery("#outstoragein_list_grid-table").jqGrid("getGridParam", "selarrrow"));
        $("#outstoragein_list_hiddenForm #outstoragein_list_queryExportExcelIndex").val(outstoragein_list_exportExcelIndex);
        $("#outstoragein_list_hiddenForm #outstoragein_list_queryStartTime").val($("#outstoragein_list_queryForm #outstoragein_list_startTime").val());
        $("#outstoragein_list_hiddenForm #outstoragein_list_queryEndTime").val($("#outstoragein_list_queryForm #outstoragein_list_endTime").val());
        $("#outstoragein_list_hiddenForm #outstoragein_list_queryQacode").val($("#outstoragein_list_queryForm #outstoragein_list_qaCode").val());
        $("#outstoragein_list_hiddenForm #outstoragein_list_queryShipNo").val($("#outstoragein_list_queryForm #outstoragein_list_shipNo").val());
	    $("#outstoragein_list_hiddenForm").submit();	
	}

</script>