<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="instoragein_list_grid-div">
    <form id="instoragein_list_hiddenForm" action="<%=path%>/inTotal/toExcel.do" method="POST" style="display: none;">
        <input id="instoragein_list_ids" name="ids" value=""/>
        <input id="instoragein_list_queryQaCode" name="queryQaCode" value=""/>
        <input id="instoragein_list_queryStartTime" name="queryStartTime" value=""/>
        <input id="instoragein_list_queryEndTime" name="queryEndTime" value=""/>
        <input id="instoragein_list_queryExportExcelIndex" name="queryExportExcelIndex" value=""/>
    </form>
    <form id="instoragein_list_hiddenQueryForm" style="display:none;">
        <input name="qaCode" value=""/>
        <input name="startTime" value=""/>
        <input name="endTime" value=""/>
    </form>
    <c:if test="${operationCode.webSearch==1}">
    <div class="query_box" id="instoragein_list_yy" title="查询选项">
        <form id="instoragein_list_queryForm" style="max-width:100%;">
            <ul class="form-elements">
                <li class="field-group field-fluid3">
                    <label class="inline" for="instoragein_list_qaCode" style="margin-right:20px;width: 100%;">
                        <span class="form_label" style="width:80px;">质保单号：</span>
                        <input id="instoragein_list_qaCode" name="qaCode" type="text" style="width: calc(100% - 85px);" placeholder="质保单号">
                    </label>
                </li>
                <li class="field-group field-fluid3">
					<label class="inline" for="instoragein_list_startTime" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:78px;">起始时间：</span>
						<input id="instoragein_list_startTime" name="startTime" class="form-control date-picker" type="text" style="width: calc(100% - 84px );" placeholder="起始时间" />
					</label>			
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="instoragein_list_endTime" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:78px;">截止时间：</span>
						<input id="instoragein_list_endTime" name="endTime" class="form-control date-picker" type="text" style="width: calc(100% - 84px );" placeholder="截止时间" />
					</label>			
				</li>
            </ul>
            <div class="field-button" style="">
                <div class="btn btn-info" onclick="instoragein_list_queryOk();">
                    <i class="ace-icon fa fa-check bigger-110"></i>查询
                </div>
                <div class="btn" onclick="instoragein_list_reset();"><i class="ace-icon icon-remove"></i>重置</div>
            </div>
        </form>
    </div>
    </c:if>
    <div id="instoragein_list_fixed_tool_div" class="fixed_tool_div">
        <div id="instoragein_list_toolbar_" style="float:left;overflow:hidden;"></div>
    </div>
    <div style="overflow-x:auto"><table id="instoragein_list_grid-table" style="width:100%;height:100%;"></table></div>
    <div id="instoragein_list_grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
    var instoragein_list_oriData;
    var instoragein_list_grid;
    var instoragein_list_exportExcelIndex;

    //时间控件
    $(".date-picker").datetimepicker({
        format: "YYYY-MM-DD HH:mm:ss" ,
        autoclose : true,
        todayHighlight : true
    });

    $("input").keypress(function (e) {
        if (e.which == 13) {
            instoragein_list_queryOk();
        }
    });

    $(function  (){
        $(".toggle_tools").click();
    });

    $("#instoragein_list_toolbar_").iToolBar({
        id: "instoragein_list_tb_01",
        items: [
            {label: "导出",hidden:"${operationCode.webExport}"=="1",onclick:function(){instoragein_list_toExecl();},iconClass:'icon-share'}
        ]
    });

    var instoragein_list_queryForm_data = iTsai.form.serialize($("#instoragein_list_queryForm"));

    instoragein_list_grid = jQuery("#instoragein_list_grid-table").jqGrid({
		url : context_path + "/inTotal/list.do",
	    datatype : "json",
	    colNames : [ "主键","质保单号","批次号","工单号","物料编号","物料名称","销售经理","入库时间","盘号","盘类型","盘规格","盘外径","盘内径", "米数","仓库","库位"],
	    colModel : [
	        {name : "id",index : "id",hidden:true},
	        {name : "qaCode",index : "qaCode",width : 140},
	        {name : "batchNo",index : "batchNo",width : 160},
            {name : "entityNo",index : "entityNo",width : 90},
	        {name : "materialCode",index : "materialCode",width : 140},
	        {name : "materialName",index : "materialName",width :270},
            {name : "salesName",index : "salesName",width : 120},
            {name : "instorageTime",index : "instorageTime",width : 140},
            {name : "dishcode",index : "dishcode",width : 180},
            {name : "drumType",index : "drumType",width : 80},
            {name : "model",index : "model",width : 160},
            {name : "outerDiameter",index : "outerDiameter",width : 60},
            {name : "innerDiameter",index : "innerDiameter",width : 60},
            {name : "meter",index : "meter",width : 50},
            {name : "warehouseCode",index : "warehouseCode",width : 100},
            {name : "shelfCode",index : "shelfCode",width : 140}
        ],
	    rowNum : 20,
	    rowList : [ 10, 20, 30 ],
	    pager : "#instoragein_list_grid-pager",
	    sortname : "v.id",
	    sortorder : "asc",
        altRows: true,
        viewrecords : true,
        hidegrid:false,
    	autowidth:false,
    	autoScroll: true,
        multiselect:true,
        multiboxonly:true,
        shrinkToFit:false,
        loadComplete : function(data){
           var table = this;
           setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            instoragein_list_oriData = data;
           $("#instoragein_list_grid-table").closest(".ui-jqgrid-bdiv").css({ 'overflow-x': 'auto' });
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
	});

	jQuery("#instoragein_list_grid-table").navGrid("#instoragein_list_grid-pager",
    {edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#instoragein_list_grid-pager",{
		caption:"",   
		buttonicon:"fa fa-refresh green",   
		onClickButton: function(){   
			$("#instoragein_list_grid-table").jqGrid("setGridParam", 
                {
                    postData: {queryJsonString:""} //发送数据
                }
			).trigger("reloadGrid");
		}
	}).navButtonAdd("#instoragein_list_grid-pager",{
        caption: "",
        buttonicon:"fa icon-cogs",
        onClickButton : function (){
            jQuery("#instoragein_list_grid-table").jqGrid("columnChooser",{
                done: function(perm, cols){
                    // dynamicColumns(cols,materials_list_dynamicDefalutValue);
                    $("#instoragein_list_grid-table").jqGrid("setGridWidth", $("#instoragein_list_grid-div").width());
                    instoragein_list_exportExcelIndex = perm;
                }
            });
        }
    });

    $(window).on("resize.jqGrid", function () {
        $("#instoragein_list_grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);
        $("#instoragein_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-10-
        $("#instoragein_list_yy").outerHeight(true)-$("#instoragein_list_fixed_tool_div").outerHeight(true)-
        $("#instoragein_list_grid-pager").outerHeight(true)-$("#gview_instoragein_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
    });

    $(window).triggerHandler("resize.jqGrid");

    /**
     * 查询按钮点击事件
     */
    function instoragein_list_queryOk(){
        var queryParam = iTsai.form.serialize($("#instoragein_list_queryForm"));
        //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
        instoragein_list_queryByParam(queryParam);
    }

    function instoragein_list_queryByParam(jsonParam) {
        iTsai.form.deserialize($("#instoragein_list_hiddenQueryForm"), jsonParam);
        var queryParam = iTsai.form.serialize($("#instoragein_list_hiddenQueryForm"));
        var queryJsonString = JSON.stringify(queryParam);
        $("#instoragein_list_grid-table").jqGrid("setGridParam",
            {
                postData: {queryJsonString: queryJsonString}
            }
        ).trigger("reloadGrid");
    }

    //清空重置
    function instoragein_list_reset(){
        iTsai.form.deserialize($("#instoragein_list_queryForm"),instoragein_list_queryForm_data);
        instoragein_list_queryByParam(instoragein_list_queryForm_data);
    }

    //导出Excel报表
    function instoragein_list_toExecl(){
        $("#instoragein_list_hiddenForm #instoragein_list_ids").val(jQuery("#instoragein_list_grid-table").jqGrid("getGridParam", "selarrrow"));
        $("#instoragein_list_hiddenForm #instoragein_list_queryQaCode").val($("#instoragein_list_queryForm #instoragein_list_qaCode").val());
        $("#instoragein_list_hiddenForm #instoragein_list_queryStartTime").val($("#instoragein_list_queryForm #instoragein_list_startTime").val());
        $("#instoragein_list_hiddenForm #instoragein_list_queryEndTime").val($("#instoragein_list_queryForm #instoragein_list_endTime").val());
        $("#instoragein_list_hiddenForm #instoragein_list_queryExportExcelIndex").val(instoragein_list_exportExcelIndex);
        $("#instoragein_list_hiddenForm").submit();
    }

    $("#instoragein_list_queryForm .mySelect2").select2();
</script>