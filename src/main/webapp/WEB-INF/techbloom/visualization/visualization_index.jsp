<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<link rel="stylesheet" href="<%=path%>/static/techbloom/system/scene/css/index.css"/>
<link rel="stylesheet" href="<%=path%>/static/techbloom/system/scene/css/leaflet.css"/>
<link rel="stylesheet" href="<%=path%>/static/techbloom/system/scene/css/Icon.Pulse.css"/>
<script src="<%=path%>/static/techbloom/system/scene/js/leaflet-src.js"></script>
<script src="<%=path%>/static/techbloom/system/scene/js/leaflet.js"></script>
<script src="<%=path%>/static/techbloom/system/scene/js/L.Icon.Pulse.js"></script>
<script src="<%=path%>/plugins/public_components/js/jquery-2.1.4.js"></script>
<script src="<%=path%>/plugins/public_components/js/bootstrap.min.js"></script>
<div class="scene_model">
  <div class="list"> </div>
  <div class="mapcontent">
<%--	  <div id="map" style="background: #fefefe;" ></div>--%>
    <div id="map" style="background: url('../static/techbloom/main/mainPage/images/20190830133357.jpg');background-repeat: no-repeat;background-size: 100% 100%;" ></div>
  </div>
</div>
<script>
var userId = "${userId}"
var carCode = "${carCode}";
var wareArea = "${wareArea}";
var mission = "${mission}";
var factoryWarehouseAreaName = "${factoryWarehouseAreaName}";
var pointArray,polygonArray,greenIcon,latlngs2;
var x,y,oldx,oldy,targetXSIZE,targetYSIZE;
var amplificationFactor = 0;
var av1,av2,av3;

$(function(){
    $.getScript(context_path+"/static/js/techbloom/wms/visualization/visualization_index.js",function(){});

	var colors ={
	  "0" : "#52C41A",   
	  "1" : "#FFAE0E",
	  "2" : "#0A6DD7",
	  "3" : "#722ED1",
	  "4" : "#F5222D",
	};
	
	var label_icon ={
	  "0" : [ "空位","kw.png" ,"kw1.png"  ]       // 空位
	};

    //返回场景，生成库位场景
	var $div = $("<div>"+factoryWarehouseAreaName+"</div>");
	$(".scene_model .list").append( $div );
 	$div.on("click",function(){
        $(".scene_model .list>div").removeClass("active");
        $(this).addClass("active");

        //画场地库位
        $.get(context_path+ "/visualization/getAllShelfTabs?carCode="+carCode).done(function( data ){
            var grids =[];
            if( data && data.length >0 ){
                for(var i = 0; i < data.length; i++){
                    if(data[i].visualTargetShelf==="1"){ //目标库位
                        grids.push({
                            color: colors[1],   // 根据状态设置颜色,目标库位为黄色
                            id : data[i].id,
                            icons : '',  //根据状态设置图标
                            area : [ [data[i].ysize1, data[i].xsize1], [data[i].ysize2, data[i].xsize2] ]
                        });

                        //画定位问号框架
                        targetXSIZE = data[i].midxsize;
                        targetYSIZE = data[i].midysize;
                        // 记录定位问号框架数据，用于清空
                        regBoxGridJson[0] = L.marker([targetYSIZE, targetXSIZE]).addTo(map).bindPopup('<h1>' + data[i].code +
                            '</h1>').openPopup();
                        regBoxGridDataJson[0] = grids[i];
                        av1 = grids[i];
                    }else{
                        grids.push({
                            color: colors[0],   // 根据状态设置颜色
                            id : data[i].id,
                            icons : '',  //根据状态设置图标
                            area : [ [data[i].ysize1, data[i].xsize1], [data[i].ysize2, data[i].xsize2] ]
                        });
                    }
                }
            }

            visualization_index_loadGrid(grids,wareArea);
        });
	});

	$(".scene_model .list>div:first").addClass("active");
	$(".scene_model .list>div:first").trigger("click");
	
    //定时刷新
    setInterval(function(){
        //画航车位置
        $.ajax({
            url: context_path + "/visualization/getCarLocation",
            type: "POST",
            data: {
                userId:userId,
                carCode:carCode
            },
            dataType: "JSON",
            success: function (data) {
                x = data.xsize*100;
                y = data.ysize*100;
                oldx = data.oldxsize*100;
                oldy = data.oldysize*100;

                //如果系统中航车的任务与刚开始打开界面中保留的任务不相同时，说明刷新了库位任务，那么就要重新画目标库位
                if (data.carMission !== mission){
                    //红色问号定位框清除
                    for(var i in regBoxGridJson){
                        regBoxGridJson.hasOwnProperty(i) &&  regBoxGridJson[i].remove();
                    }
                    regBoxGridJson = [];
                    regBoxGridDataJson = [];

                    //清空原目标库位
                    TargetShelfJson.hasOwnProperty(0) &&   TargetShelfJson[0].remove();
                    TargetShelfJson = [];
                    TargetShelfDataJson = [];

                    //画目标库位
                    $.get(context_path+ "/visualization/getShelfTarget?carCode="+carCode).done(function( data ){
                        var grids =[];
                        //目标库位加入grids
                        if( data ){
                            grids.push({
                                color: colors[1],   // 根据状态设置颜色
                                id : data.id,
                                icons : '',  //根据状态设置图标
                                area : [ [data.ysize1, data.xsize1], [data.ysize2, data.xsize2] ]
                            });
                        }

                        // 根据grids来画目标库位(目标库位就一个)
                        if( grids && grids.length >0 ){
                            //目标库位初始化
                            var rect = L.rectangle(grids[0].area, {
                                color: "#444",
                                fillColor: grids[0].color,
                                weight: 1,
                                fillOpacity: 1
                            }).addTo(map);
                            // 记录数据，用于清空
                            TargetShelfJson[grids[0].id] = rect;
                            TargetShelfDataJson[grids[0].id] = grids[0];

                            L.rectangle(av1.area, {
                                color: "#444",
                                fillColor: colors[0],
                                weight: 1,
                                fillOpacity: 1
                            }).addTo(map);

                            av1 = grids[0];
                        }

                        // 记录数据，用于清空
                        regBoxGridJson[0] = L.marker([data.midysize, data.midxsize]).addTo(map).bindPopup('<h1>' + data.code +
                            '</h1>').openPopup();
                        regBoxGridDataJson[0] = grids[0];

                    });
                    mission = data.carMission;
                }

                //误差数据过滤
                if(Math.abs(x-oldx)>200||Math.abs(y-oldy)>200) {
                    return;
                }

                if ( Math.sqrt( Math.pow(x-targetXSIZE,2) + Math.pow(y-targetYSIZE,2) )<1025.768){
                    //以挂钩为屏幕中心，并放大
                    if (amplificationFactor===0){
                        map.setView(
                            [y,x],map.getZoom()+2.1
                        );
                    }
                    amplificationFactor++;
                }else {
                    amplificationFactor = 0;
                    map.fitBounds(bounds);
                    map.setView(
                        //平板版
                        [map_w/2,map_h/2+2400],map.getZoom()
                        //电脑版
                        // [5500/2,28000/2],map.getZoom()
                    );
                }
                // 挂钩图标
                greenIcon = L.icon({
                    iconUrl: '<%=path%>/plugins/public_components/img/leaf-green.png',
                    iconSize: [(map.getZoom()+9)*5.125, (map.getZoom()+9)*6.375],
                });

                if(pointArray!=null&&pointArray!==''&&pointArray.length>0){
                    pointArray[0].remove();
                }
                if(polygonArray!=null&&polygonArray!==''&&polygonArray.length>0){
                    polygonArray[0].remove();
                }
                pointArray = new Array(1);
                polygonArray = new Array(1);
                pointArray[0] = L.marker([y,x], {icon: greenIcon}).addTo(map);
                latlngs2 = [[-200, x-500],[-200, x+500],[map_w+175, x+500],[map_w+175, x-500]];
                polygonArray[0] = L.polygon(latlngs2, {
                    color: '#D57400',
                    fillOpacity:0,
                    weight:3,
                }).addTo(map);
                //滑动鼠标或者点击放大按钮，执行重新设置中心点和缩放比例
                /*map.on("zoomend", function(){
                   map.setView(
                       [y,x],map.getZoom()+1
                   );
                });*/
            },
            error:function(XMLHttpRequest){
                alert(XMLHttpRequest.readyState);
                alert("出错啦！！！");
            }
        });
	}, 1000);

})
</script>
