<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<link rel="stylesheet" href="<%=path%>/static/techbloom/system/scene/css/index2.css"/>
<link rel="stylesheet" href="<%=path%>/static/techbloom/system/scene/css/leaflet.css"/>
<link rel="stylesheet" href="<%=path%>/static/techbloom/system/scene/css/Icon.Pulse.css"/>
<link rel="stylesheet" href="<%=path%>/plugins/public_components/css/select2.css"/>
<script src="<%=path%>/static/techbloom/system/scene/js/leaflet-src.js"></script>
<script src="<%=path%>/static/techbloom/system/scene/js/leaflet.js"></script>
<script src="<%=path%>/static/techbloom/system/scene/js/L.Icon.Pulse.js"></script>
<script src="<%=path%>/plugins/public_components/js/bootstrap.min.js"></script>

<style>

    #visualization_platform_area_page{
        height: 40px;
        line-height: 40px;
    }
    #selectInput{
        width: 200px;
    }
    .tools2{
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        padding: 0 11px;
        margin-top: 5px;
    }
    .tools2, .showNum {
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        align-items: center;
    }
    .showNum p{
        margin:0 0 0 15px;
    }
    .visualization_platform_map_model{
        position: fixed;
        width: 100%;
    }
</style>

<div class="visualization_platform_scene_model visualization_platform_map_model">
    <div class="visualization_platform_list"> </div>
    <div id="visualization_platform_area_page">
        <div class="tools2">
            <input id="selectInput"/>
        </div>
    </div>
    <div class="visualization_platform_mapcontent">
        <div id="map2" style="background: url('../../static/techbloom/main/mainPage/images/20190830133357.jpg') no-repeat;
            background-size: 100% 100%;" ></div>
    </div>
</div>
<script>
    $(function(){
        $.getScript(context_path+"/static/js/techbloom/wms/visualization/visualization_platform.js",function(){});

        $("#selectInput").select2({
            placeholder: "请选择区域",
            allowClear: true,
            data: [{id:'HC261',text:'总厂A场地'},{id:'HC263',text:'总厂D场地'}]
        });

        $("#selectInput").on("change",function() {
            visualization_platform_carCode = $(this).val();
            if(visualization_platform_carCode){
                //画场地库位
                $.get(context_path+ "/visualization/getAllShelfTabs?carCode="+ visualization_platform_carCode).done(function( data ){
                    visualization_platform_map.fitBounds(visualization_platform_bounds);
                    visualization_platform_map.setView(
                        //电脑版
                        [5500/2,28000/2], visualization_platform_map.getZoom()
                    );
                    var grids =[];
                    if( data && data.length >0 ){
                        var cd;
                        var colour;
                        if(data[0].codes=='D') {//老远东D
                            visualization_platform_map_h= 27167.4;
                            visualization_platform_map_w= 5300;
                            cd = 'D';
                        }else if(data[0].codes=='A') {//老远东A
                            visualization_platform_map_h= 27246.6;
                            visualization_platform_map_w= 5240.4;
                            cd = 'A';
                        }else if(data[0].codes=='FA'||data[0].codes=='FB'||data[0].codes=='FC') {//复合F西
                            visualization_platform_map_h= 16167;
                            visualization_platform_map_w= 3363.5;
                            cd = 'FX';
                        }else if(data[0].codes=='FD'||data[0].codes=='FE'||data[0].codes=='FF'||data[0].codes=='FG') {//复合F东
                            visualization_platform_map_h= 16536.5;
                            visualization_platform_map_w= 3537;
                            cd = 'FD';
                        }else if(data[0].codes=='X') {//新远东X
                            visualization_platform_map_h = 32716.94;
                            visualization_platform_map_w = 5024.5;
                            cd = 'X';
                        }else if(data[0].codes=='NA') {//新远东（特殊）NA
                            visualization_platform_map_h= 15056.89;
                            visualization_platform_map_w= 5125;
                            cd = 'NA';
                        }else if(data[0].codes=='NB') {//新远东（特殊）NB
                            visualization_platform_map_h= 15137;
                            visualization_platform_map_w= 3828;
                            cd = 'NB';
                        }else if(data[0].codes=='NC'||data[0].codes=='NE') {//新远东（特殊）NC、NE
                            visualization_platform_map_h= 43802;
                            visualization_platform_map_w= 4872;
                            cd = 'NC';
                        }else if(data[0].codes=='ND'||data[0].codes=='NF') {//新远东（特殊）ND、NF
                            visualization_platform_map_h= 44012;
                            visualization_platform_map_w= 3825;
                            cd = 'ND';
                        }

                        for(var i = 0; i < data.length; i++){
                            if(data[i].state==1){
                                colour = visualization_platform_colors[0];
                            }else if(data[i].state==199){
                                colour = visualization_platform_colors[5];
                            }else {
                                colour = visualization_platform_colors[1];
                            }
                            if(data[i].visualTargetShelf=="1"){ //目标库位
                                visualization_platform_mission = data[i].visualTargetShelf;
                                grids.push({
                                    color: visualization_platform_colors[4],   // 根据状态设置颜色
                                    id : data[i].id,
                                    icons : '',  //根据状态设置图标
                                    area : [ [data[i].ysize1, data[i].xsize1], [data[i].ysize2, data[i].xsize2] ]
                                });

                                //画定位问号框架
                                visualization_platform_targetXSIZE = data[i].midxsize;
                                visualization_platform_targetYSIZE = data[i].midysize;
                                var redBox = L.marker([visualization_platform_targetYSIZE, visualization_platform_targetXSIZE])
                                    .addTo(visualization_platform_map).bindPopup('<h1>'+data[i].code+'</h1>') .openPopup();

                                // 记录定位问号框架数据，用于清空
                                visualization_platform_regBoxGridJson[0] = redBox;
                                visualization_platform_regBoxGridDataJson[0] = grids[i];
                                visualization_platform_av1 = grids[i];
                            }else{
                                grids.push({
                                    color: colour,   // 根据状态设置颜色
                                    id : data[i].id,
                                    icons : '',  //根据状态设置图标
                                    area : [ [data[i].ysize1, data[i].xsize1], [data[i].ysize2, data[i].xsize2] ]
                                });
                            }
                        }
                    }

                    visualization_platform_loadGrid(grids);
                });
            }
        });

        $("#selectInput").val('HC261').trigger("change");

        //定时刷新
        setInterval(function(){
            //画航车位置
            $.ajax({
                url: context_path + "/visualization/getCarLocation",
                type: "POST",
                data: {
                    userId:'',
                    carCode:visualization_platform_carCode
                },
                dataType: "JSON",
                success: function (data) {
                    visualization_platform_x = data.xsize*100;
                    visualization_platform_y = data.ysize*100;
                    visualization_platform_oldx = data.oldxsize*100;
                    visualization_platform_oldy = data.oldysize*100;

                    //如果系统中航车的任务与刚开始打开界面中保留的任务不相同时，说明刷新了库位任务，那么就要重新画目标库位
                    if (data.carMission != visualization_platform_mission){
                        //红色问号定位框清除
                        for(var i in visualization_platform_regBoxGridJson){
                            visualization_platform_regBoxGridJson.hasOwnProperty(i) &&  visualization_platform_regBoxGridJson[i].remove();
                        }
                        visualization_platform_regBoxGridJson = [];
                        visualization_platform_regBoxGridDataJson = [];

                        //清空原目标库位
                        visualization_platform_TargetShelfJson.hasOwnProperty(0) && visualization_platform_TargetShelfJson[0].remove();
                        visualization_platform_TargetShelfJson = [];
                        visualization_platform_TargetShelfDataJson = [];

                        //画目标库位
                        $.get(context_path+ "/visualization/getShelfTarget?carCode="+visualization_platform_carCode).done(function( data ){
                            var grids =[];
                            //目标库位加入grids
                            if( data ){
                                grids.push({
                                    color: visualization_platform_colors[1],   // 根据状态设置颜色
                                    id : data.id,
                                    icons : '',  //根据状态设置图标
                                    area : [ [data.ysize1, data.xsize1], [data.ysize2, data.xsize2] ]
                                });
                            }

                            // 根据grids来画目标库位(目标库位就一个)
                            if( grids && grids.length >0 ){
                                //目标库位初始化
                                var rect = L.rectangle(grids[0].area, {
                                    color: "#444",
                                    fillColor: grids[0].color,
                                    weight: 1,
                                    fillOpacity: 1
                                }).addTo(visualization_platform_map);
                                // 记录数据，用于清空
                                visualization_platform_TargetShelfJson[grids[0].id] = rect;
                                visualization_platform_TargetShelfDataJson[grids[0].id] = grids[0];

                                L.rectangle(visualization_platform_av1.area, {
                                    color: "#444",
                                    fillColor: visualization_platform_colors[0],
                                    weight: 1,
                                    fillOpacity: 1
                                }).addTo(visualization_platform_map);

                                visualization_platform_av1 = grids[0];
                            }

                            //画红定位
                            var redBox = L.marker([data.midysize, data.midxsize]).addTo(visualization_platform_map) .bindPopup('<h1>'+data.code+'</h1>') .openPopup();
                            // 记录数据，用于清空
                            visualization_platform_regBoxGridJson[0] = redBox;
                            visualization_platform_regBoxGridDataJson[0] = grids[0];

                        });
                        visualization_platform_mission = data.carMission;
                    }

                    //误差数据过滤
                    if(Math.abs(visualization_platform_x-visualization_platform_oldx)>200||
                        Math.abs(visualization_platform_y-visualization_platform_oldy)>200) {
                        return;
                    }

                    // 挂钩图标
                    visualization_platform_greenIcon = L.icon({
                        iconUrl: '<%=path%>/plugins/public_components/img/leaf-green.png',
                        iconSize: [(visualization_platform_map.getZoom()+9)*5.125, (visualization_platform_map.getZoom()+9)*6.375],
                    });

                    if(visualization_platform_pointArray!=null&&visualization_platform_pointArray!=''&&visualization_platform_pointArray.length>0){
                        visualization_platform_pointArray[0].remove();
                    }
                    if(visualization_platform_polygonArray!=null&&visualization_platform_polygonArray!=''&&visualization_platform_polygonArray.length>0){
                        visualization_platform_polygonArray[0].remove();
                    }
                    visualization_platform_pointArray = new Array(1);
                    visualization_platform_polygonArray = new Array(1);
                    visualization_platform_pointArray[0] = L.marker([visualization_platform_y,visualization_platform_x],
                        {icon: visualization_platform_greenIcon}).addTo(visualization_platform_map);
                    visualization_platform_latlngs2 = [[-200, visualization_platform_x-500],[-200, visualization_platform_x+500],
                        [visualization_platform_map_w+175, visualization_platform_x+500],[visualization_platform_map_w+175, visualization_platform_x-500]];
                    visualization_platform_polygonArray[0] = L.polygon(visualization_platform_latlngs2, {
                        color: '#D57400',
                        fillOpacity:0,
                        weight:3,
                    }).addTo(visualization_platform_map);

                },
                error:function(XMLHttpRequest){
                    alert(XMLHttpRequest.readyState);
                    alert("出错啦！！！");
                }
            });
        }, 1000);
    })
</script>
