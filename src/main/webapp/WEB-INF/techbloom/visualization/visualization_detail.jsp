<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<div id="visualization_detail_page" class="row-fluid" style="height: inherit;">
	<form id="visualization_detail_visualizationForm" class="form-horizontal" style="overflow: auto; height: calc(100% - 70px);">
		<div class="control-group">
			<label class="control-label" >库位：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input class="span11" type="text" value="${storageInfo.shelfCode}"  />
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">质保号：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input class="span11" type="text" value="${storageInfo.qaCode}"  />
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">规格型号：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input class="span11" type="text" value="${storageInfo.model}"  />
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">米数：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input class="span11" type="text" value="${storageInfo.meter}"  />
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">盘大小：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input class="span11" type="text" value="${storageInfo.outerDiameter}"  />
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">订单号：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input class="span11" type="text" value="${storageInfo.orderno}"  />
				</div>
			</div>
		</div>
	</form>
	<div class="field-button" style="text-align: center;border-top: 0px;margin: 15px auto;">
		<span class="btn btn-danger" onclick="layer.closeAll();">
		   <i class="icon-remove"></i>&nbsp;关闭
		</span>
	</div>
</div>
