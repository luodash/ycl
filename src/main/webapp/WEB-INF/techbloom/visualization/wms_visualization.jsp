<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<link rel="stylesheet" href="<%=path%>/static/techbloom/system/scene/css/index.css"/>
<link rel="stylesheet" href="<%=path%>/static/techbloom/system/scene/css/leaflet.css"/>
<link rel="stylesheet" href="<%=path%>/static/techbloom/system/scene/css/Icon.Pulse.css"/>
<link rel="stylesheet" href="<%=path%>/plugins/public_components/css/select2.css"/>
<script src="<%=path%>/static/techbloom/system/scene/js/leaflet-src.js"></script>
<script src="<%=path%>/static/techbloom/system/scene/js/leaflet.js"></script>
<script src="<%=path%>/static/techbloom/system/scene/js/L.Icon.Pulse.js"></script>
<script src="<%=path%>/plugins/public_components/js/bootstrap.min.js"></script>

<style>
    #area_page{
        height: 40px;
        line-height: 40px;
    }
    .select2_input{
        width: 200px;
    }
    .tools{
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        padding: 0 11px;
    }
    .tools, .showNum {
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        align-items: center;
    }
    .showNum p{
        margin:0 0 0 15px;
    }
    .leaflet-map-pane canvas{
        z-index: 201;
        pointer-events: none;
    }
    .map_model{
        position: fixed;
        width: 100%;
    }
</style>
<div class="scene_model map_model">
    <div id="area_page">
        <div class="tools">
            <input class="select2_input "/>
            <span class="showNum">
                <p>库位总量:<span id="totalNum" type="text" ng-model="name" /></p>
                <p>已用数量:<span id="useNum" type="text" ng-model="name" /></p>
                <p>未用数量:<span id="unuseNum" type="text" ng-model="name" /></p>
                <p>禁用数量:<span id="prohibitNum" type="text" ng-model="name" /></p>
                <p>库位余量:<span id="usePro" type="text" ng-model="name" />%</p>
            </span>
        </div>
    </div>
  <div class="mapcontent">
    <div id="map" style="background: url('../../static/techbloom/main/mainPage/images/20190830133357.jpg');background-repeat: no-repeat;background-size: 100% 100%;" ></div>
  </div>
</div>
<script src="<%=path%>/plugins/public_components/js/leaflet.canvas-markers.js"></script>
<script>
$(function(){
    $.getScript(context_path+"/static/js/techbloom/wms/visualization/wms_visualization.js",function(){});

    $(".select2_input").select2({
        placeholder: "请选择区域",
        allowClear: true,
        data: [
            {id:'HC261',text:'总厂A场地'},{id:'HC263',text:'总厂D场地'},{id:'HC265',text:'新远东X场地'},{id:'HC271',text:'复合技术F场地东'},
            {id:'HC272',text:'复合技术F场地西'},{id:'HC267',text:'新远东N场地东（南）'},{id:'HC268',text:'新远东N场地东（北）'},
            {id:'HC269',text:'新远东N场地西（南）'}, {id:'HC270',text:'新远东N场地西（北）'}
        ]
    });

    $(".select2_input").on("change",function() {
        var carcode = $(this).val();
        var cd = 'A';
        if(carcode){
            // 清空画图重绘制，画场地库位
            $.get(context_path+ "/visualization/getAllShelfTabs?carCode="+carcode).done(function( data ){
                var grids =[];
                var colour;
                if( data && data.length >0 ){
                    if(data[0].codes==='D') {
                        //老远东D
                        wms_map_h= 27167.4;
                        wms_map_w= 5300;
                        cd = 'D';
                    }else if(data[0].codes==='A') {
                        //老远东A
                        wms_map_h= 27246.6;
                        wms_map_w= 5240.4;
                        cd = 'A';
                    }else if(data[0].codes==='FA'||data[0].codes==='FB'||data[0].codes==='FC') {
                        //复合F西
                        wms_map_h= 16167;
                        wms_map_w= 3363.5;
                        cd = 'FX';
                    }else if(data[0].codes==='FD'||data[0].codes==='FE'||data[0].codes==='FF'||data[0].codes==='FG') {
                        //复合F东
                        wms_map_h= 16536.5;
                        wms_map_w= 3537;
                        cd = 'FD';
                    }else if(data[0].codes==='X') {
                        //新远东X
                        wms_map_h = 32716.94;
                        wms_map_w = 5024.5;
                        cd = 'X';
                    }else if(data[0].codes==='NA') {
                        //新远东（特殊）NA
                        wms_map_h= 15056.89;
                        wms_map_w= 5125;
                        cd = 'NA';
                    }else if(data[0].codes==='NB') {
                        //新远东（特殊）NB
                        wms_map_h= 15137;
                        wms_map_w= 3828;
                        cd = 'NB';
                    }else if(data[0].codes==='NC'||data[0].codes==='NE') {
                        //新远东（特殊）NC、NE
                        wms_map_h= 43802;
                        wms_map_w= 4872;
                        cd = 'NC';
                    }else if(data[0].codes==='ND'||data[0].codes==='NF') {
                        //新远东（特殊）ND、NF
                        //完整值
                        wms_map_h= 44012;
                        wms_map_w= 3825;
                        cd = 'ND';
                    }

                    document.getElementById('useNum').innerHTML = data[0].useNum;
                    document.getElementById('unuseNum').innerHTML = data[0].unuseNum;
                    document.getElementById('totalNum').innerHTML = data[0].totalNum;
                    document.getElementById('usePro').innerHTML = data[0].usePro;
                    document.getElementById('prohibitNum').innerHTML = data[0].prohibitNum;

                    for(var i = 0; i < data.length; i++){
                        if(data[i].state===1){
                            colour = colors[0];
                        }else if(data[i].state===199){
                            colour = colors[5];
                        }else {
                            colour = colors[1];
                        }

                        grids.push({
                            color: colour,   // 根据状态设置颜色
                            id : data[i].id,
                            code: data[i].code,
                            icons : '',  //根据状态设置图标
                            area : [ [data[i].ysize1, data[i].xsize1], [data[i].ysize2, data[i].xsize2] ]
                        });
                    }
                }

                wms_visualization_loadGrid(grids, data[0].codes, function( data ){
                    alert(JSON.stringify(data));
                });
                //新远东N场地东（南）
                if(cd==='ND'||cd==='NA'||cd==='NB'){
                    map.setView(
                        [wms_map_w/2,wms_map_h/2],map.getZoom()
                    );
                }
                cd = '';
            });
        }
    });
    $(".select2_input").val('HC261').trigger("change");
})
</script>
