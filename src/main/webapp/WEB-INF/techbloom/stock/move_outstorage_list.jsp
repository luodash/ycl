<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="move_outstorage_list_grid-div">
    <form id="move_outstorage_list_hiddenForm" action="<%=path%>/car/toExcel.do" method="POST" style="display: none;">
        <input id="move_outstorage_list_ids" name="ids" value=""/>
    </form>
    <form id="move_outstorage_list_hiddenQueryForm" style="display:none;">
        <input id="move_outstorage_list_carNo" name="carNo" value=""/>
        <input id="move_outstorage_list_carName" name="carName" value="">
        <input id="move_outstorage_list_rfid" name="rfid" value=""/>
        <input id="move_outstorage_list_shelveId" name="shelveId" value="">
        <input id="move_outstorage_list_type" name="type" value="">
    </form>
    <div class="query_box" id="move_outstorage_list_yy" title="查询选项">
         <form id="move_outstorage_list_queryForm" style="max-width:100%;">
			 <ul class="form-elements">
				<li class="field-group field-fluid3">
					<label class="inline" for="move_outstorage_list_carNo" style="margin-right:20px;width: 100%;">
						<span class="form_label" style="width:80px;">发货单编号：</span>
						<input id="move_outstorage_list_carNo" name="carNo" type="text" style="width: calc(100% - 85px);" placeholder="发货单编号">
					</label>			
				</li>
				
			</ul>
			<div class="field-button" style="">
					<div class="btn btn-info" onclick="queryOk();">
				        <i class="ace-icon fa fa-check bigger-110"></i>查询
			        </div>
					<div class="btn" onclick="reset();"><i class="ace-icon icon-remove"></i>重置</div>
					<a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>
		        </div>
		  </form>		 
    </div>
    <div id="move_outstorage_list_fixed_tool_div" class="fixed_tool_div">
        <div id="move_outstorage_list___toolbar__" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="move_outstorage_list_grid-table" style="width:100%;height:100%;"></table>
    <div id="move_outstorage_list_grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
	var oriData; 
	var _grid;
	var openwindowtype = 0; //打开窗口类型：0新增，1修改
	var selectid;
	$(function  (){
	    $(".toggle_tools").click();
	});
	$("#move_outstorage_list___toolbar__").iToolBar({
	    id: "move_outstorage_list___tb__01",
	    items: [
	        {label: "执行", disabled: ( ${sessionUser.addQx} == 1 ? false : true), onclick:execute, iconClass:'glyphicon glyphicon-plus'},
	        {label: "查看", disabled: ( ${sessionUser.addQx} == 1 ? false : true), onclick:openInfoPage, iconClass:'glyphicon glyphicon-pencil'},
	        {label: "编辑", disabled: ( ${sessionUser.editQx} == 1 ? false : true),onclick: openEditPage, iconClass:'glyphicon glyphicon-pencil'},
	        {label: "导出", disabled: ( ${sessionUser.queryQx}==1 ? false : true),onclick:function(){toExcel();},iconClass:'icon-share'}
	   ]
	});
	var _queryForm_data = iTsai.form.serialize($("#move_outstorage_list_queryForm"));
	_grid = jQuery("#move_outstorage_list_grid-table").jqGrid({
				url : context_path + "/outsrorageCon/storageListData.do",
			    datatype : "json",
			    colNames : [ "主键","移库编号", "时间","销售经理","状态","行车编号"],
			    colModel : [ 
			                 {name : "id",index : "id",hidden:true},
			                 {name : "cid",index : "cid",width : 60},
			                 {name : "fhtime",index : "fhtime",width : 60},
			                 {name : "salePerson",index : "salePerson",width :70},
                            {name : 'status',index : 'status',width : 20,
                                formatter:function(cellValu,option,rowObject){
                                    if(cellValu=='生效'){
                                        return "<font color='red'>生效</font>";
                                    } if(cellValu=='拣货完成'||cellValu=='发运完成'){
                                        return "<font color='blue'>"+cellValu+"</font>";
                                    }
                                    return cellValu;
                                }},
                             {name : "car_code",index : "car_code",width : 60}
			               ],
			    rowNum : 20,
			    rowList : [ 10, 20, 30 ],
			    pager : "#move_outstorage_list_grid-pager",
			    sortname : "cid",
			    sortorder : "asc",
	            altRows: true,
	            viewrecords : true,
	            hidegrid:false,
	     	    autowidth:true, 
	            multiselect:true,
	            loadComplete : function(data)
	            {
	            	var table = this;
	            	setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
	            	oriData = data;
	            },
	            emptyrecords: "没有相关记录",
	            loadtext: "加载中...",
	            pgtext : "页码 {0} / {1}页",
	            recordtext: "显示 {0} - {1}共{2}条数据"
	});

	jQuery("#move_outstorage_list_grid-table").navGrid("#move_outstorage_list_grid-pager",{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#move_outstorage_list_grid-pager",{  
		caption:"",   
		buttonicon:"fa fa-refresh green",   
		onClickButton: function(){   
			$("#move_outstorage_list_grid-table").jqGrid("setGridParam", 
					{
				      postData: {queryJsonString:""} //发送数据 
					}
			).trigger("reloadGrid");
		}
	});

	$(window).on("resize.jqGrid", function () {
		$("#move_outstorage_list_grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);
		$("#move_outstorage_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-
		$("#move_outstorage_list_yy").outerHeight(true)-$("#move_outstorage_list_fixed_tool_div").outerHeight(true)-
		$("#move_outstorage_list_grid-pager").outerHeight(true)-$("#gview_move_outstorage_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
	});
	$(window).triggerHandler("resize.jqGrid");
	/*打开添加页面*/
	function openAddPage(){
		$.post(context_path + "/car/toAdd.do", {}, function (str){
			$queryWindow=layer.open({
			    title : "车辆添加", 
		    	type:1,
		    	skin : "layui-layer-molv",
		    	area : "600px",
		    	shade : 0.6, //遮罩透明度
			    moveType : 1, //拖拽风格，0是默认，1是传统拖动
			    anim : 2,
			    content : str,
			    success: function (layero, index) {
	                layer.closeAll('loading');
	            }
			});
		});
	}

	/**
	 * *
	 * 行车bind
	 * */
	function bind(){
	    $.post(context_path + "/outsrorageCon/carbind.do", {}, function (str){
	        $queryWindow=layer.open({
	            title : "行车绑定",
	            type:1,
	            skin : "layui-layer-molv",
	            area : "600px",
	            shade : 0.6, //遮罩透明度
	            moveType : 1, //拖拽风格，0是默认，1是传统拖动
	            anim : 2,
	            content : str,
	            success: function (layero, index) {
	                layer.closeAll('loading');
	            }
	        });
	    });
	}

	/**
	 * 执行
	 * */
	function execute(){
	    $.post(context_path + "/outsrorageCon/execute.do", {}, function (str){
	        $queryWindow=layer.open({
	            title : "出库执行",
	            type:1,
	            skin : "layui-layer-molv",
	            area : window.screen.width-20+"px",
	            shade : 0.6, //遮罩透明度
	            moveType : 1, //拖拽风格，0是默认，1是传统拖动
	            anim : 2,
	            content : str,
	            success: function (layero, index) {
	                layer.closeAll('loading');
	            }
	        });
	    });
	}

	function openInfoPage(){
	    var selectAmount = getGridCheckedNum("#move_outstorage_list_grid-table");
	    if(selectAmount==0){
	        layer.msg("请选择一条记录！",{icon:2});
	        return;
	    }else if(selectAmount>1){
	        layer.msg("只能选择一条记录！",{icon:8});
	        return;
	    }
	    openwindowtype = 1;
	    layer.load(2);
	    selectid = jQuery("#move_outstorage_list_grid-table").jqGrid("getGridParam", "selrow");
	    $.post(context_path + "/outsrorageCon/toInfo.do", {}, function (str){
	        $queryWindow=layer.open({
	            title : "查看列表",
	            type:1,
	            skin : "layui-layer-molv",
	            area : window.screen.width-20+"px",
	            shade : 0.6, //遮罩透明度
	            moveType : 1, //拖拽风格，0是默认，1是传统拖动
	            anim : 2,
	            content : str,
	            success: function (layero, index) {
	                layer.closeAll('loading');
	            }
	        });
	    });
	}

	/*打开编辑页面*/
	function openEditPage(){
		var selectAmount = getGridCheckedNum("#move_outstorage_list_grid-table");
		if(selectAmount==0){
			layer.msg("请选择一条记录！",{icon:2});
			return;
		}else if(selectAmount>1){
			layer.msg("只能选择一条记录！",{icon:8});
			return;
		}
		openwindowtype = 1;
		layer.load(2);
		selectid = jQuery("#move_outstorage_list_grid-table").jqGrid("getGridParam", "selrow");
		$.post(context_path+'/outsrorageCon/editInfo.do?id='+selectid, {}, function(str){
			$queryWindow = layer.open({
				title : "编辑",
				type: 1,
			    skin : "layui-layer-molv",
			    area : window.screen.width-20+"px",
				shade: 0.6, //遮罩透明度
				moveType: 1, //拖拽风格，0是默认，1是传统拖动
				content: str,//注意，如果str是object，那么需要字符拼接。
				success:function(layero, index){
					layer.closeAll("loading");
				}
			});
		}).error(function() {
			layer.closeAll();
	    	layer.msg("加载失败！",{icon:2});
		});
	}
	
	/**
	 * 查询按钮点击事件
	 */
	 function queryOk(){
		 var queryParam = iTsai.form.serialize($("#move_outstorage_list_queryForm"));
		 //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
		 queryByParam(queryParam);
	}
	
 	function queryByParam(jsonParam) {
	    iTsai.form.deserialize($("#move_outstorage_list_hiddenQueryForm"), jsonParam);
	    var queryParam = iTsai.form.serialize($("#move_outstorage_list_hiddenQueryForm"));
	    var queryJsonString = JSON.stringify(queryParam); 
	    $("#move_outstorage_list_grid-table").jqGrid("setGridParam",
	        {
	            postData: {queryJsonString: queryJsonString}
	        }
	    ).trigger("reloadGrid");
	}
 	
	function reset(){
		 iTsai.form.deserialize($("#move_outstorage_list_queryForm"),_queryForm_data); 
		 queryByParam(_queryForm_data);
	}
	
	function toExcel(){
	    var ids = jQuery("#move_outstorage_list_grid-table").jqGrid("getGridParam", "selarrrow");
	    $("#move_outstorage_list_hiddenForm #move_outstorage_list_ids").val(ids);
	    $("#move_outstorage_list_hiddenForm").submit();	
	}
	
    $("#move_outstorage_list_queryForm .mySelect2").select2();   
</script>