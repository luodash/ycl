<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="scrap_outstorage_list_grid-div">
    <form id="scrap_outstorage_list_hiddenForm" action="<%=path%>/scrapOutStorage/toExcel.do" method="POST" style="display: none;">
        <input id="scrap_outstorage_list_ids" name="ids" value=""/>
        <input id="scrap_outstorage_list_queryQacode" name="qaCode" value=""/>
        <input id="scrap_outstorage_list_queryStartTime" name="startTime" value=""/>
        <input id="scrap_outstorage_list_queryEndTime" name="endTime" value=""/>
    </form>
    <form id="scrap_outstorage_list_hiddenQueryForm" style="display:none;">
        <input  name="qaCode" value=""/>
        <input  name="startTime" value=""/>
        <input  name="endTime" value=""/>
    </form>
    <c:if test="${operationCode.webSearch==1}">
    <div class="query_box" id="scrap_outstorage_list_yy" title="查询选项">
        <form id="scrap_outstorage_list_queryForm" style="max-width:100%;">
            <ul class="form-elements">
                <li class="field-group field-fluid3">
                    <label class="inline" for="scrap_outstorage_list_qaCode" style="margin-right:20px;width: 100%;">
                        <span class="form_label" style="width:80px;">质保单号：</span>
                        <input id="scrap_outstorage_list_qaCode" name="qaCode" type="text" style="width: calc(100% - 85px);" placeholder="质保单号">
                    </label>
                </li>
                <li class="field-group field-fluid3">
                    <label class="inline" for="scrap_outstorage_list_startTime" style="margin-right:20px;width:100%;">
                        <span class="form_label" style="width:80px;">开始时间：</span>
                        <input type="text" class="form-control date-picker" id="scrap_outstorage_list_startTime" name="startTime" value="" 
                               style="width: calc(100% - 85px);" placeholder="报废时间起" />
                    </label>
                </li>
                <li class="field-group field-fluid3">
                    <label class="inline" for="scrap_outstorage_list_endTime" style="margin-right:20px;width:100%;">
                        <span class="form_label" style="width:80px;">结束时间：</span>
                        <input type="text" class="form-control date-picker" id="scrap_outstorage_list_endTime" name="endTime" value="" 
                               style="width: calc(100% - 85px);" placeholder="报废时间止" />
                    </label>
                </li>
            </ul>
            <div class="field-button" style="">
                <div class="btn btn-info" onclick="scrap_outstorage_list_queryOk();">
                    <i class="ace-icon fa fa-check bigger-110"></i>查询
                </div>
                <div class="btn" onclick="scrap_outstorage_list_reset();"><i class="ace-icon icon-remove"></i>重置</div>
            </div>
        </form>
    </div>
    </c:if>
    <div id="scrap_outstorage_list_fixed_tool_div" class="fixed_tool_div">
        <div id="scrap_outstorage_list_toolbar_" style="float:left;overflow:hidden;"></div>
    </div>
    <div style="overflow-x:auto"><table id="scrap_outstorage_list_grid-table" style="width:100%;height:100%;"></table></div>
    <div id="scrap_outstorage_list_grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
    var scrap_outstorage_list_oriData;
    var scrap_outstorage_list_grid;
    
    $(".date-picker").datetimepicker({
        format: "YYYY-MM-DD HH:mm:ss" ,
        autoclose : true,
        todayHighlight : true
    });
    
    $(function  (){
        $(".toggle_tools").click();
    });

    $("#scrap_outstorage_list_toolbar_").iToolBar({
        id: "scrap_outstorage_list_tb_01",
        items: [
            {label: "导出",hidden:"${operationCode.webExport}"=="1",onclick:function(){scrap_outstorage_list_toExcel();},iconClass:'icon-share'}
        ]
    });

    var scrap_outstorage_list_queryForm_data = iTsai.form.serialize($("#scrap_outstorage_list_queryForm"));

    scrap_outstorage_list_grid = jQuery("#scrap_outstorage_list_grid-table").jqGrid({
        url : context_path + "/scrapOutStorage/list.do",
        datatype : "json",
        colNames : ["id","质保单号","批次号","仓库","库位","物料名称","销售经理","报废时间","盘号","盘类型","盘规格","盘外径","米数"],
        colModel : [
            {name : "id",index : "id",hidden : true},
            {name : "qaCode",index : "qaCode",width:160},
            {name : "batchNo",index : "batchNo",width:160},
            {name : "warehouseName",index : "warehouseName",width : 160},
            {name : "shelfCode",index : "shelfCode",width : 110},
            {name : "materialName",index : "materialName",width :270},
            {name : "salesName",index : "salesname",width : 80},
            {name : "createTime",index : "createtime",width : 160},
            {name : "dishcode",index : "dishcode",width : 160},
            {name : "drumType",index : "drumType",width : 60},
            {name : "model",index : "model",width : 160},
            {name : "outerDiameter",index : "outerDiameter",width : 60},
            {name : "meter",index : "meter",width : 60}
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : "#scrap_outstorage_list_grid-pager",
        sortname : "id",
        sortorder : "asc",
        altRows: true,
        viewrecords : true,
        hidegrid:false,
        autowidth:true,
        multiselect:true,
        multiboxonly:true,
        shrinkToFit:false,
        autoScroll: true,
        loadComplete : function(data){
            var table = this;
            setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            scrap_outstorage_list_oriData = data;
            $("#scrap_outstorage_list_grid-table").closest(".ui-jqgrid-bdiv").css({ 'overflow-x': 'auto' });
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
    });

    jQuery("#scrap_outstorage_list_grid-table").navGrid("#scrap_outstorage_list_grid-pager",
        {edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#scrap_outstorage_list_grid-pager",{
        caption:"",
        buttonicon:"fa fa-refresh green",
        onClickButton: function(){
            $("#scrap_outstorage_list_grid-table").jqGrid("setGridParam", {
                postData: {queryJsonString:""} //发送数据
            }).trigger("reloadGrid");
        }
    }).navButtonAdd("#scrap_outstorage_list_grid-pager",{
        caption: "",
        buttonicon:"fa icon-cogs",
        onClickButton : function (){
            jQuery("#scrap_outstorage_list_grid-table").jqGrid("columnChooser",{
                done: function(perm, cols){
                    // dynamicColumns(cols,materials_list_dynamicDefalutValue);
                    $("#scrap_outstorage_list_grid-table").jqGrid("setGridWidth", $("#scrap_outstorage_list_grid-div").width());
                }
            });
        }
    });

    $(window).on("resize.jqGrid", function () {
        $("#scrap_outstorage_list_grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);
        $("#scrap_outstorage_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-10-
        $("#scrap_outstorage_list_yy").outerHeight(true)-$("#scrap_outstorage_list_fixed_tool_div").outerHeight(true)-
        $("#scrap_outstorage_list_grid-pager").outerHeight(true)-$("#gview_scrap_outstorage_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
    });

    $(window).triggerHandler("resize.jqGrid");

    /**
     * 查询按钮点击事件
     */
    function scrap_outstorage_list_queryOk(){
        var queryParam = iTsai.form.serialize($("#scrap_outstorage_list_queryForm"));
        //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
        scrap_outstorage_list_queryByParam(queryParam);
    }

    function scrap_outstorage_list_queryByParam(jsonParam) {
        iTsai.form.deserialize($("#scrap_outstorage_list_hiddenQueryForm"), jsonParam);
        var queryParam = iTsai.form.serialize($("#scrap_outstorage_list_hiddenQueryForm"));
        var queryJsonString = JSON.stringify(queryParam);
        $("#scrap_outstorage_list_grid-table").jqGrid("setGridParam", {
            postData: {queryJsonString: queryJsonString}
        }).trigger("reloadGrid");
    }

    //清空重置
    function scrap_outstorage_list_reset(){
        iTsai.form.deserialize($("#scrap_outstorage_list_queryForm"),scrap_outstorage_list_queryForm_data);
        scrap_outstorage_list_queryByParam(scrap_outstorage_list_queryForm_data);
    }

    //导出功能
    function scrap_outstorage_list_toExcel(){
        $("#scrap_outstorage_list_hiddenForm #scrap_outstorage_list_ids").val(jQuery("#scrap_outstorage_list_grid-table").jqGrid("getGridParam", "selarrrow"));
        $("#scrap_outstorage_list_hiddenForm #scrap_outstorage_list_queryQacode").val($("#scrap_outstorage_list_queryForm #scrap_outstorage_list_qaCode").val());
        $("#scrap_outstorage_list_hiddenForm #scrap_outstorage_list_queryStartTime").val($("#scrap_outstorage_list_queryForm #scrap_outstorage_list_startTime").val());
        $("#scrap_outstorage_list_hiddenForm #scrap_outstorage_list_queryEndTime").val($("#scrap_outstorage_list_queryForm #scrap_outstorage_list_endTime").val());
        $("#scrap_outstorage_list_hiddenForm").submit();
    }
</script>