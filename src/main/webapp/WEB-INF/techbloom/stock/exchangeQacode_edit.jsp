<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<div id ="exchangeQacode_edit_page" class="row-fluid" style="height: inherit;">
	<form id="exchangeQacode_edit_queryForm" class="form-horizontal"  style="overflow: auto; height: calc(100% - 70px);">
		<input type="hidden" name="id" id="exchangeQacode_edit_id" value="${id}">
		<div class="control-group">
			<label class="control-label" >质保号：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="exchangeQacode_edit_qaCode" name="qaCode" value="${qaCode}" placeholder="质保号">
				</div>
			</div>
		</div>

	</form>
	<div class="form-actions" style="text-align: right;border-top: 0px;margin: 0px;">
		<span  class="savebtn btn btn-success">保存</span>
		<span  class="btn btn-danger">取消</span>
	</div>
</div>

<script type="text/javascript">
    //表单校验
    $("#exchangeQacode_edit_queryForm").validate({
        rules:{
            "qaCode":{
                required:true
            }
        },
        messages:{
            "qaCode":{
                remote:"请输入新质保号！"
            }
        },
        errorClass: "help-inline",
        errorElement: "span",
        highlight:function(element, errorClass, validClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        }
    });

    //确定按钮点击事件
    $("#exchangeQacode_edit_page .btn-success").off("click").on("click", function(){
        //保存
        if($("#exchangeQacode_edit_queryForm").valid()){
            exchangeQacode_edit_saveUwb($("#exchangeQacode_edit_queryForm").serialize());
		}
    });

    //取消按钮点击事件
    $("#exchangeQacode_edit_page .btn-danger").off("click").on("click", function(){
        layer.close($queryWindow);
    });

    var ajaxStatus = 1;     //ajax请求状态：0不能请求，1可以请求

    //保存/修改
    function exchangeQacode_edit_saveUwb(bean){
        if(bean){
            if(ajaxStatus==0){
                layer.msg("保存中，请稍后...",{icon:2});
                return;
            }
            ajaxStatus = 0;
            $(".savebtn").attr("disabled","disabled");
            $.ajax({
                url:context_path+"/storageInfo/save",
                type:"POST",
                data:bean,
                dataType:"JSON",
                success:function(data){
                    if(data.result){
                        layer.msg(data.msg,{icon:1});
                        //刷新库存列表
						$("#stockList_grid-table").jqGrid('setGridParam', {
							postData: {
								queryJsonString:JSON.stringify(iTsai.form.serialize($('#stockList_queryForm')))
							} //发送数据
						}).trigger("reloadGrid");
                        //关闭指定的窗口对象
                        layer.close($queryWindow);
                    }else{
                        layer.msg("保存失败，请稍后重试！",{icon:2});
                    }
					ajaxStatus = 1; //将标记设置为可请求
					$(".savebtn").removeAttr("disabled");
                }
            });
        }else{
            layer.msg("出错啦！",{icon:2});
        }
    }

</script>