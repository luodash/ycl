<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="back_production_outstorage_grid-div">
    <form id="back_production_outstorage_hiddenForm" action="<%=path%>/backProductionOutStorage/toExcel.do" method="POST" style="display: none;">
        <input id="back_production_outstorage_ids" name="ids" value=""/>
        <input id="back_production_outstorage_queryQacode" name="qaCode" value=""/>
        <input id="back_production_outstorage_queryStartTime" name="startTime" value=""/>
        <input id="back_production_outstorage_queryEndTime" name="endTime" value=""/>
    </form>
    <form id="back_production_outstorage_hiddenQueryForm" style="display:none;">
        <input  name="qaCode" value=""/>
        <input  name="startTime" value=""/>
        <input  name="endTime" value=""/>
    </form>
    <c:if test="${operationCode.webSearch==1}">
    <div class="query_box" id="back_production_outstorage_yy" title="查询选项">
        <form id="back_production_outstorage_queryForm" style="max-width:100%;">
            <ul class="form-elements">
                <li class="field-group field-fluid3">
                    <label class="inline" for="back_production_outstorage_qaCode" style="margin-right:20px;width: 100%;">
                        <span class="form_label" style="width:80px;">质保单号：</span>
                        <input id="back_production_outstorage_qaCode" name="qaCode" type="text" style="width: calc(100% - 85px);" placeholder="质保单号">
                    </label>
                </li>
                <li class="field-group field-fluid3">
                    <label class="inline" for="back_production_outstorage_startTime" style="margin-right:20px;width:100%;">
                        <span class="form_label" style="width:80px;">开始时间：</span>
                        <input type="text" class="form-control date-picker" id="back_production_outstorage_startTime" name="startTime" value=""
                               style="width: calc(100% - 85px);" placeholder="退库时间起" />
                    </label>
                </li>
                <li class="field-group field-fluid3">
                    <label class="inline" for="back_production_outstorage_endTime" style="margin-right:20px;width:100%;">
                        <span class="form_label" style="width:80px;">结束时间：</span>
                        <input type="text" class="form-control date-picker" id="back_production_outstorage_endTime" name="endTime" value=""
                               style="width: calc(100% - 85px);" placeholder="退库时间止" />
                    </label>
                </li>
            </ul>
            <div class="field-button" style="">
                <div class="btn btn-info" onclick="back_production_outstorage_queryOk();">
                    <i class="ace-icon fa fa-check bigger-110"></i>查询
                </div>
                <div class="btn" onclick="back_production_outstorage_reset();"><i class="ace-icon icon-remove"></i>重置</div>
            </div>
        </form>
    </div>
    </c:if>
    <div id="back_production_outstorage_fixed_tool_div" class="fixed_tool_div">
        <div id="back_production_outstorage_toolbar_" style="float:left;overflow:hidden;"></div>
    </div>
    <div style="overflow-x:auto"><table id="back_production_outstorage_grid-table" style="width:100%;height:100%;"></table></div>
    <div id="back_production_outstorage_grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
	var back_production_outstorage_oriData;
	var back_production_outstorage_grid;
	var selectid;

    $(".date-picker").datetimepicker({
        format: "YYYY-MM-DD HH:mm:ss" ,
        autoclose : true,
        todayHighlight : true
    });

	$(function  (){
	    $(".toggle_tools").click();
	});

	$("#back_production_outstorage_toolbar_").iToolBar({
	    id: "back_production_outstorage_tb_01",
	    items: [
	        {label: "导出",hidden:"${operationCode.webExport}"=="1",onclick:function(){back_production_outstorage_toExcel();},iconClass:'icon-share'}
	   	]
	});
	var back_production_outstorage_queryForm_data = iTsai.form.serialize($("#back_production_outstorage_queryForm"));

    back_production_outstorage_grid = jQuery("#back_production_outstorage_grid-table").jqGrid({
	    url : context_path + "/backProductionOutStorage/list.do",
	    datatype : "json",
	    colNames : ["id","类型","质保单号","批次号","仓库","库位","物料名称","销售经理","退库时间","盘号","盘类型","盘规格","盘外径","米数"],
	    colModel : [
	    	{name : "id",index : "id",hidden : true},
            {name : "type",index : "type",width:190,formatter:function(cellvalue,option,rowObject){
                    if(cellvalue==1){
                        return "<span style=\"color:orange;font-weight:bold;\">生产退库</span>";
                    }else if(cellvalue==2){
                        return "<span style=\"color:saddlebrown;font-weight:bold;\">采购退货</span>";
                    }
                }
            },
            {name : "qaCode",index : "qaCode",width:190},
            {name : "batchNo",index : "batch_no",width:140},
	        {name : "warehouseName",index : "warehouseName",width : 120},
	        {name : "shelfCode",index : "shelfCode",width : 130},
	        {name : "materialName",index : "materialName",width : 360},
	        {name : "salesName",index : "salesname",width : 190},
	        {name : "createTime",index : "CREATETIME",width : 130},
            {name : "dishcode",index : "dishcode",width : 240},
	        {name : "drumType",index : "drumType",width : 100},
	        {name : "model",index : "model",width : 280},
            {name : "outerDiameter",index : "outerDiameter",width : 80},
	        {name : "meter",index : "meter",width : 80}
	    ],
	    rowNum : 20,
	    rowList : [ 10, 20, 30 ],
	    pager : "#back_production_outstorage_grid-pager",
	    sortname : "id",
	    sortorder : "asc",
        altRows: true,
        viewrecords : true,
        hidegrid:false,
    	autowidth:false,
        multiselect:true,
        multiboxonly:true,
        shrinkToFit:false,
        autoScroll: true,
        loadComplete : function(data){
           	var table = this;
           	setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            back_production_outstorage_oriData = data;
           	$("#back_production_outstorage_grid-table").closest(".ui-jqgrid-bdiv").css({ 'overflow-x': 'auto' });
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
	});

	jQuery("#back_production_outstorage_grid-table").navGrid("#back_production_outstorage_grid-pager",
	{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#back_production_outstorage_grid-pager",{  
		caption:"",   
		buttonicon:"fa fa-refresh green",   
		onClickButton: function(){   
			$("#back_production_outstorage_grid-table").jqGrid("setGridParam", {
                postData: {queryJsonString:""} //发送数据
            }).trigger("reloadGrid");
		}
	}).navButtonAdd("#back_production_outstorage_grid-pager",{
        caption: "",
        buttonicon:"fa icon-cogs",
        onClickButton : function (){
            jQuery("#back_production_outstorage_grid-table").jqGrid("columnChooser",{
                done: function(perm, cols){
                    $("#back_production_outstorage_grid-table").jqGrid("setGridWidth", $("#back_production_outstorage_grid-div").width());
                }
            });
        }
    });

	$(window).on("resize.jqGrid", function () {
		$("#back_production_outstorage_grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);
		$("#back_production_outstorage_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-10-
		$("#back_production_outstorage_yy").outerHeight(true)-$("#back_production_outstorage_fixed_tool_div").outerHeight(true)-
		$("#back_production_outstorage_grid-pager").outerHeight(true)-
		$("#gview_back_production_outstorage_grid-table .ui-jqgrid-hdiv").outerHeight(true));
	});
	
	$(window).triggerHandler("resize.jqGrid");

	/**
	 * 查询按钮点击事件
	 */
	 function back_production_outstorage_queryOk(){
		 var queryParam = iTsai.form.serialize($("#back_production_outstorage_queryForm"));
		 //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
		 back_production_outstorage_queryByParam(queryParam);
	}

	 function back_production_outstorage_queryByParam(jsonParam) {
	    iTsai.form.deserialize($("#back_production_outstorage_hiddenQueryForm"), jsonParam);
	    var queryParam = iTsai.form.serialize($("#back_production_outstorage_hiddenQueryForm"));
	    var queryJsonString = JSON.stringify(queryParam); 
	    $("#back_production_outstorage_grid-table").jqGrid("setGridParam",
	        {
	            postData: {queryJsonString: queryJsonString}
	        }
	    ).trigger("reloadGrid");
	}
 
	 //清空重置
	function back_production_outstorage_reset(){	
	     $("#back_production_outstorage_queryForm #back_production_outstorage_shelveId").select2("val","");
	     $("#back_production_outstorage_queryForm #back_production_outstorage_type").select2("val","");
		 iTsai.form.deserialize($("#back_production_outstorage_queryForm"),back_production_outstorage_queryForm_data); 
		 back_production_outstorage_queryByParam(back_production_outstorage_queryForm_data);
	}

    //导出功能
    function back_production_outstorage_toExcel(){
        $("#back_production_outstorage_hiddenForm #back_production_outstorage_ids")
            .val(jQuery("#back_production_outstorage_grid-table").jqGrid("getGridParam", "selarrrow"));
        $("#back_production_outstorage_hiddenForm #back_production_outstorage_queryQacode")
            .val($("#back_production_outstorage_queryForm #back_production_outstorage_qaCode").val());
        $("#back_production_outstorage_hiddenForm #back_production_outstorage_queryStartTime")
            .val($("#back_production_outstorage_queryForm #back_production_outstorage_startTime").val());
        $("#back_production_outstorage_hiddenForm #back_production_outstorage_queryEndTime")
            .val($("#back_production_outstorage_queryForm #back_production_outstorage_endTime").val());
        $("#back_production_outstorage_hiddenForm").submit();
    }
</script>