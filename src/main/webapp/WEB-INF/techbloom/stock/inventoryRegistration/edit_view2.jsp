<%@ page language="java" import="java.lang.*"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<div class="row-fluid" style="height: inherit;margin:0px;border: 0px">
	<form id="edit_view2_baseInfor" class="form-horizontal" id="edit_view2_baseInfor"  method="post" target="_ifr">
	</form>
	<div id="edit_view2_materialDiv" style="margin:10px;">
		<!-- 下拉框 -->
		<label class="inline" >盘点单号：</label>
		<input type="text" style="width:150px;margin-right:10px;" value="${planId}" readonly="readonly"/>

		<label class="inline" >盘点类型：</label>
		<input type="text" style="width:150px;margin-right:10px;" value="${planType}" readonly="readonly"/>
	</div>
	<!-- 表格div -->
	<div id="edit_view2_grid-div-c" style="width:100%;">
		<!-- 货物详情信息表格 -->
		<table id="edit_view2_grid-table-c" style="width:100%;height:100%;"></table>
		<!-- 表格分页栏 -->
		<div id="edit_view2_grid-pager-c"></div>
	</div>
</div>
<script type="text/javascript">
	var context_path = '<%=path%>';
	var id='${id}';
	var oriDataDetail;
	var _grid_detail;        //表格对象
	var lastsel2;
	$(".date-picker").datetimepicker({format: 'YYYY-MM-DD HH:mm:ss',useMinutes:true,useSeconds:true});
	

	_grid_detail=jQuery("#edit_view2_grid-table-c").jqGrid({
	    url : context_path + "/inventoryRegistration/detailList.do?id="+id,
	    datatype : "json",
	    colNames : [ "详情主键","质检单号","物料编码","RFID","米数","库区","库位","登记人id","登记人"],
	    colModel : [
	        {name : "id",index : "id",width : 20,hidden:true},
            {name : "qacode",index:"qacode",width : 20},
	        {name : "materialcode",index : "materialcode",width : 20},
	        {name : "rfid",index : "rfid",width : 20},
	        {name : "meter",index:"meter",width : 20},
	        {name : "warehousecode",index:"warehousecode",width : 20},
	        {name : "shelfid",index:"shelfid",width : 20},
	        {name : "userCode",index:"userCode",hidden:true},
	        {name : "userName",index:"userName",width : 20}
	    ],
         rowNum : 20,
         rowList : [ 10, 20, 30 ],
         pager : '#edit_view2_grid-pager-c',
         sortname : 'ID',
         sortorder : "asc",
         altRows: true,
         viewrecords : true,
         autowidth:true,
         multiselect:true,
		 multiboxonly: true,
         loadComplete : function(data) {
             var table = this;
             setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
             oriDataDetail = data;
             $(window).triggerHandler('resize.jqGrid');
         },
	     cellEdit: true,
	     cellsubmit : "clientArray",
  	     emptyrecords: "没有相关记录",
  	     loadtext: "加载中...",
  	     pgtext : "页码 {0} / {1}页",
  	     recordtext: "显示 {0} - {1}共{2}条数据",
    });
    //在分页工具栏中添加按钮
    $("#edit_view2_grid-table-c").navGrid("#edit_view2_grid-pager-c",
    {edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#edit_view2_grid-pager-c",{  
  	   caption:"",   
  	   buttonicon:"ace-icon fa fa-refresh green"  
  	});  	
  	$(window).on("resize.jqGrid", function () {
  		$("#edit_view2_grid-table-c").jqGrid("setGridWidth", $("#edit_view2_grid-div-c").width());
  		var height = $(".layui-layer-title",_grid_detail.parents(".layui-layer")).height()+
            $("#edit_view2_baseInfor").outerHeight(true)+$("#edit_view2_materialDiv").outerHeight(true)+
            $("#edit_view2_grid-pager-c").outerHeight(true)+$("#edit_view2_fixed_tool_div.fixed_tool_div.detailToolBar").outerHeight(true)+
            $("#gview_edit_view2_grid-table-c .ui-jqgrid-hbox").outerHeight(true);
            $("#edit_view2_grid-table-c").jqGrid("setGridHeight",_grid_detail.parents(".layui-layer").height()-height);
  	});
  	$(window).triggerHandler("resize.jqGrid");

  
	//将数据格式化成两位小数：四舍五入
	 function formatterNumToFixed(value,options,rowObj){
		if(value!=null){
			var floatNum = parseFloat(value);
			return floatNum.toFixed(2);
		}else{
			return "0.00";
		}
	 } 

</script>
