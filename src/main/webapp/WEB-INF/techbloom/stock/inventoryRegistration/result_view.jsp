<%@ page language="java" import="java.lang.*"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<div class="row-fluid" style="height: inherit;margin:0px;border: 0px">
	<form id="result_view_baseInfor" class="form-horizontal" id="result_view_baseInfor"  method="post" target="_ifr"></form>
	<div id="result_view_materialDiv" style="margin:10px;">
		<label class="inline" >盘点单号：</label>
		<input type="text" style="width:350px;margin-right:10px;" value="${planIds}" readonly="readonly"/>
		<!-- 导出按钮 -->
		<form id="hiddenForm" action="<%=path%>/inventoryRegistration/detailExcel.do" method="POST" style="display: none;">
			<input id="hiddenForm_ids" name="ids" value=""/>
			<input id="info_id" name="infoid" value="">
		</form>
		<span class="btn btn-info" id="export">
		   <i class="ace-icon fa fa-check bigger-110"></i>导出
		</span>
	</div>
	<!-- 表格div -->
	<div id="result_view_grid-div-c" style="width:100%;">
		<!-- 详情信息表格 -->
		<table id="result_view_grid-table-c" style="width:100%;height:100%;"></table>
		<!-- 表格分页栏 -->
		<div id="result_view_grid-pager-c"></div>
	</div>
</div>
<script type="text/javascript">
	var context_path = '<%=path%>';
	var id = '${ids}';	//盘点计划主键
	var oriDataDetail;
	var _grid_detail;    //表格对象

	_grid_detail=jQuery("#result_view_grid-table-c").jqGrid({
	    url : context_path + "/inventoryRegistration/resultList.do?id="+id,
	    datatype : "json",
	    colNames : [ "质保单号","批次号","盘外径","盘内径","盘规格","盘号","物料编码","物料名称","库存米数","盘点米数","库存库位","盘点库位","盘点人","盘点时间", "结果"],
	    colModel : [
			{name : "qaCode",index:"ys.qacode",width : 165},
			{name : "batchNo",index:"ys.batch_no",width : 160},
			{name : "outerDiameter",index:"d1.outerdiameter",width : 70},
			{name : "innerDiameter",index:"d1.innerdiameter",width : 70},
			{name : "model",index:"d1.model",width : 220},
	        {name : "dishcode",index : "yd.dishcode",width : 230},
			{name : "materialCode",index : "yd.materialcode",width : 130},
			{name : "materialName",index:"ym.name",width : 230},
	        {name : "meter",index:"ys.meter",width : 70},
			{name : "inventoryMeter",index:"ys.INVENTORY_METER",width : 70},
	        {name : "shelfCode",index:"ys.shelfcode",width : 130},
			{name : "inventoryShelf",index:"ys.inventory_shelf",width : 130},
	        {name : "inventoryName",index:"yr.user_code",width : 70},
			{name : "uploadTime",index:"yr.upload_time",width : 170},
	        {name : "result",index:"yr.result",width : 60,
				formatter:function(cellValu,option,rowObject){
					if(cellValu=='1'){
                        return "正常";
					}else if(cellValu=='2'){
                        return "<span style='color:red;font-weight:bold;'>盘盈</span>";
					}else if(cellValu=='3'){
                        return "<span style='color:green;font-weight:bold;'>盘亏</span>";
                    }else if(cellValu=='4'){
						return "错位正常";
					}else if(cellValu=='5'){
						return "<span style='color:mediumvioletred;font-weight:bold;'>错位盘盈</span>";
					}else if(cellValu=='6'){
						return "<span style='color:darkgreen;font-weight:bold;'>错位盘亏</span>";
					}else {
						return "<span style='color:black;font-weight:bold;'>漏盘</span>";
					}
	            }
	        }
	    ],
         rowNum : 20,
         rowList : [ 10, 20, 30 ],
         pager : '#result_view_grid-pager-c',
         sortname : 'ys.id',
         sortorder : "desc",
         altRows: true,
         viewrecords : true,
         autowidth:false,
         multiselect:true,
		 multiboxonly: true,
         loadComplete : function(data) {
             var table = this;
             setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
             oriDataDetail = data;
             $(window).triggerHandler('resize.jqGrid');
			 $("#result_view_grid-table-c").closest(".ui-jqgrid-bdiv").css({ 'overflow-x': 'auto' });
         },
	     cellEdit: true,
	     cellsubmit : "clientArray",
		 shrinkToFit:false,
		 autoScroll: true,
  	     emptyrecords: "没有相关记录",
  	     loadtext: "加载中...",
  	     pgtext : "页码 {0} / {1}页",
  	     recordtext: "显示 {0} - {1}共{2}条数据",
    });
    //在分页工具栏中添加按钮
    $("#result_view_grid-table-c").navGrid("#result_view_grid-pager-c", {edit:false,add:false,del:false,search:false,refresh:false})
	.navButtonAdd("#result_view_grid-pager-c",{
  	   caption:"",   
  	   buttonicon:"ace-icon fa fa-refresh green"  
  	});  	
  	$(window).on("resize.jqGrid", function () {
		$("#result_view_grid-table-c").jqGrid("setGridWidth", $("#result_view_grid-div-c").width() - 3 );
		$("#result_view_grid-table-c").jqGrid("setGridHeight", (document.documentElement.clientHeight - $("#result_view_grid-pager-c").height() - 380) );
  	});
  	$(window).triggerHandler("resize.jqGrid");

	$("#export").click(function(){
		$("#hiddenForm_ids").val(jQuery("#result_view_grid-table-c").jqGrid("getGridParam", "selarrrow"));
		$("#info_id").val(id);
		$("#hiddenForm").submit();
	});
  
	//将数据格式化成两位小数：四舍五入
	 function formatterNumToFixed(value,options,rowObj){
		if(value!=null){
			var floatNum = parseFloat(value);
			return floatNum.toFixed(2);
		}else{
			return "0.00";
		}
	 } 

</script>
