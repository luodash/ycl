<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="inventoryRegistration_list_grid-div">
    <form id="inventoryRegistration_list_hiddenForm" action="<%=path%>/inventoryPlan/toExcel.do" method="POST" style="display: none;">
        <input id="inventoryRegistration_list_ids" name="ids" value=""/>
    </form>
    <form id="inventoryRegistration_list_hiddenQueryForm" style="display:none;">
        <input  name="factorycode" value=""/>
        <input  name="code" value=""/>
        <input  name="startTime" value=""/>
        <input  name="endTime" value=""/>
    </form>
    <c:if test="${operationCode.webSearch==1}">
    <div class="query_box" id="inventoryRegistration_list_yy" title="查询选项">
         <form id="inventoryRegistration_list_queryForm" style="max-width:100%;">
			 <ul class="form-elements">
			 	<li class="field-group field-fluid3">
					<label class="inline" for="inventoryRegistration_list_code" style="margin-right:20px;width: 100%;">
						 <span class="form_label" style="width:80px;">盘点单号：</span>
						 <input id="inventoryRegistration_list_code" name="code" type="text" style="width: calc(100% - 85px);" placeholder="盘点单号">
					</label>
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="inventoryRegistration_list_factorycode" style="margin-right:20px;width: 100%;">
						 <span class="form_label" style="width:80px;">厂区：</span>
						 <input id="inventoryRegistration_list_factorycode" name="factorycode" type="text" style="width: calc(100% - 85px);" placeholder="厂区">
					</label>
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="inventoryRegistration_list_startTime" style="margin-right:20px;width: 100%;">
						 <span class="form_label" style="width:80px;">创建时间起：</span>
						 <input id="inventoryRegistration_list_startTime" class="form-control date-picker" name="startTime" type="text" style="width: calc(100% - 85px);" placeholder="创建时间起">
					</label>
				 </li>

				 <li class="field-group-top field-group field-fluid3">
					 <label class="inline" for="inventoryRegistration_list_endTime" style="margin-right:20px;width: 100%;">
						 <span class="form_label" style="width:80px;">创建时间止：</span>
						 <input id="inventoryRegistration_list_endTime" class="form-control date-picker" name="endTime" type="text" style="width:  calc(100% - 85px);" placeholder="创建时间止">
					 </label>
				</li>
			</ul>
			<div class="field-button" style="">
				<div class="btn btn-info" onclick="inventoryRegistration_list_queryOk();">
					<i class="ace-icon fa fa-check bigger-110"></i>查询
			    </div>
				<div class="btn" onclick="inventoryRegistration_list_reset();"><i class="ace-icon icon-remove"></i>重置</div>
				<a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>
		    </div>
		  </form>		 
    </div>
    </c:if>
    <div id="inventoryRegistration_list_fixed_tool_div" class="fixed_tool_div">
        <div id="inventoryRegistration_list_toolbar_" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="inventoryRegistration_list_grid-table" style="width:100%;height:100%;"></table>
    <div id="inventoryRegistration_list_grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
	var inventoryRegistration_list_oriData;
	var inventoryRegistration_list_grid;
	var inventoryRegistration_list_factoryselectid;

	$("input").keypress(function (e) {
		if (e.which == 13) {
			inventoryPlan_list_queryOk();
		}
	});

	//时间控件
	$(".date-picker").datetimepicker({
		format: "YYYY-MM-DD HH:mm:ss" ,
		autoclose : true,
		todayHighlight : true
	});
	
	$(function  (){
	    $(".toggle_tools").click();
	});

	$("#inventoryRegistration_list_toolbar_").iToolBar({
	    id: "inventoryRegistration_list_tb_01",
	    items: [
	        {label: "查看差异（可多选）",hidden:"${operationCode.webInfo}"=="1", onclick: inventoryRegistration_list_openResultPage, iconClass:'icon-search'}
	   ]
	});

	var inventoryRegistration_list_queryForm_Data = iTsai.form.serialize($("#inventoryRegistration_list_queryForm"));

	inventoryRegistration_list_grid = jQuery("#inventoryRegistration_list_grid-table").jqGrid({
			url : context_path + "/inventoryPlan/list.do",
		    datatype : "json",
		    colNames : [ "主键","盘点单号","厂区名称", "创建人","创建时间","执行人","状态"],
		    colModel : [ 
		    	{name : "id",index : "id",hidden:true},
	            {name : "code",index : "code",width : 50},
	            {name : "factoryname",index : "factoryname",width : 90},
	            {name : "creater",index : "creater",width:50},
	            {name : "createtime",index : "createtime",width:60},
	            {name : "userName",index : "userName",width :60},
	            {name : "state",index : "state",width : 60,
	            	formatter:function(cellValu,option,rowObject){
	            		if(cellValu=='0') {
	            			return '<span style="color:#FF0000;font-weight:bold;">新增</span>';
						}else if(cellValu=='2') {
	            			return '<span style="color:#76b86b;font-weight:bold;">已登记</span>';
						}else if(cellValu=='3') {
	                    	return '<span style="color:#76b86b;font-weight:bold;">已审核</span>';
						}
	            	}
	            }
	        ],
		    rowNum : 20,
		    rowList : [ 10, 20, 30 ],
		    pager : "#inventoryRegistration_list_grid-pager",
		    sortname : "id",
		    sortorder : "desc",
	        altRows: true,
	        viewrecords : true,
	        hidegrid:false,
	    	autowidth:true, 
	        multiselect:true,
	        multiboxonly:true,
	        loadComplete : function(data){
		        var table = this;
		        setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
				inventoryRegistration_list_oriData = data;
	        },
	        emptyrecords: "没有相关记录",
	        loadtext: "加载中...",
	        pgtext : "页码 {0} / {1}页",
	        recordtext: "显示 {0} - {1}共{2}条数据"
	});

	jQuery("#inventoryRegistration_list_grid-table").navGrid("#inventoryRegistration_list_grid-pager",
		{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#inventoryRegistration_list_grid-pager",{
		caption:"",   
		buttonicon:"fa fa-refresh green",   
		onClickButton: function(){   
			$("#inventoryRegistration_list_grid-table").jqGrid("setGridParam", {
			   postData: {queryJsonString:""} //发送数据
			}).trigger("reloadGrid");
		}
	}).navButtonAdd("#inventoryRegistration_list_grid-pager",{
        caption: "",
        buttonicon:"fa icon-cogs",
        onClickButton : function (){
            jQuery("#inventoryRegistration_list_grid-table").jqGrid("columnChooser",{
                done: function(perm, cols){
                    // dynamicColumns(cols,materials_list_dynamicDefalutValue);
                    $("#inventoryRegistration_list_grid-table").jqGrid("setGridWidth", $("#inventoryRegistration_list_grid-div").width());
                }
            });
        }
    });

	$(window).on("resize.jqGrid", function () {
		$("#inventoryRegistration_list_grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);
		$("#inventoryRegistration_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-10-
		$("#inventoryRegistration_list_yy").outerHeight(true)-$("#inventoryRegistration_list_fixed_tool_div").outerHeight(true)-
		$("#inventoryRegistration_list_grid-pager").outerHeight(true)- $("#gview_inventoryRegistration_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
	});

	$(window).triggerHandler("resize.jqGrid");

	/**打开查看记录页面*/
	function inventoryRegistration_list_openRecordPage(){
		var selectAmount = getGridCheckedNum("#inventoryRegistration_list_grid-table");
		if(selectAmount==0){
			layer.msg("请选择一条记录！",{icon:2});
			return;
		}else if(selectAmount>1){
			layer.msg("只能选择一条记录！",{icon:8});
			return;
		}
		layer.load(2);

		$.post(context_path+'/inventoryRegistration/infoView.do', {
			id : jQuery("#inventoryRegistration_list_grid-table").jqGrid("getGridParam", "selrow")
		}, function(str){
			$queryWindow = layer.open({
				title : "查看",
				type: 1,
			    skin : "layui-layer-molv",
			    area : window.screen.width-20+"px",
				shade: 0.6, //遮罩透明度
				moveType: 1, //拖拽风格，0是默认，1是传统拖动
				content: str,//注意，如果str是object，那么需要字符拼接。
				success:function(layero, index){
					layer.closeAll("loading");
				}
			});
		}).error(function() {
			layer.closeAll();
	    	layer.msg("加载失败！",{icon:2});
		});
	}
	
	//打开查看差异页面
	function inventoryRegistration_list_openResultPage(){
		var selectAmount = getGridCheckedNum("#inventoryRegistration_list_grid-table");
		if(selectAmount == 0){
			layer.msg("请选择至少一条记录！",{icon:2});
			return;
		}
		layer.load(2);

		$.post(context_path+'/inventoryRegistration/resultView.do?ids='+$('#inventoryRegistration_list_grid-table').jqGrid('getGridParam','selarrrow'), {
		}, function(str){
			$queryWindow = layer.open({
				title : "查看",
				type: 1,
			    skin : "layui-layer-molv",
			    area : window.screen.width-20+"px",
				shade: 0.6, //遮罩透明度
				moveType: 1, //拖拽风格，0是默认，1是传统拖动
				content: str,//注意，如果str是object，那么需要字符拼接。
				success:function(layero, index){
					layer.closeAll("loading");
				}
			});
		}).error(function() {
			layer.closeAll();
	    	layer.msg("加载失败！",{icon:2});
		});
	}

	/**
	 * 查询按钮点击事件
	 */
	function inventoryRegistration_list_queryOk(){
		 var queryParam = iTsai.form.serialize($("#inventoryRegistration_list_queryForm"));
		 //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
		 inventoryRegistration_list_queryByParam(queryParam);
	}
	
	function inventoryRegistration_list_queryByParam(jsonParam) {
	    iTsai.form.deserialize($("#inventoryRegistration_list_hiddenQueryForm"), jsonParam);
	    var queryParam = iTsai.form.serialize($("#inventoryRegistration_list_hiddenQueryForm"));
	    var queryJsonString = JSON.stringify(queryParam); 
	    $("#inventoryRegistration_list_grid-table").jqGrid("setGridParam",
	        {
	            postData: {queryJsonString: queryJsonString}
	        }
	    ).trigger("reloadGrid");
	}
	
	//重置
	function inventoryRegistration_list_reset(){
		 $("#inventoryRegistration_list_queryForm #inventoryRegistration_list_factorycode").select2("val","");
		 iTsai.form.deserialize($("#inventoryRegistration_list_queryForm"),inventoryRegistration_list_queryForm_Data);
		 inventoryRegistration_list_queryByParam(inventoryRegistration_list_queryForm_Data);
	}

	//导出excel
	function inventoryRegistration_list_toExcel(){
	    $("#inventoryRegistration_list_hiddenForm #inventoryRegistration_list_ids").val(jQuery("#inventoryRegistration_list_grid-table").jqGrid("getGridParam", "selarrrow"));
	    $("#").submit();
	}
	
	//厂区
	$("#inventoryRegistration_list_queryForm #inventoryRegistration_list_factorycode").select2({
	    placeholder: "选择厂区",
	    minimumInputLength:0,   //至少输入n个字符，才去加载数据
	    allowClear: true,  //是否允许用户清除文本信息
	    delay: 250,
	    formatNoMatches:"没有结果",
	    formatSearching:"搜索中...",
	    formatAjaxError:"加载出错啦！",
	    ajax : {
	        url: context_path+"/factoryArea/getFactoryList",
	        type:"POST",
	        dataType : 'json',
	        delay : 250,
	        data: function (term,pageNo) {     //在查询时向服务器端传输的数据
	            term = $.trim(term);
	            return {
	                queryString: term,    //联动查询的字符
	                pageSize: 15,    //一次性加载的数据条数
	                pageNo:pageNo   //页码
	            }
	        },
	        results: function (data,pageNo) {
	            var res = data.result;
	            if(res.length>0){   //如果没有查询到数据，将会返回空串
	                var more = (pageNo*15)<data.total; //用来判断是否还有更多数据可以加载
	                return {
	                    results:res,more:more
	                };
	            }else{
	                return {
	                    results:{}
	                };
	            }
	        },
	        cache : true
	    }
	});
	
    $("#inventoryRegistration_list_queryForm .mySelect2").select2();
    $("##inventoryRegistration_list_queryForm #inventoryRegistration_list_factorycode").change(function(){
    	inventoryRegistration_list_factoryselectid = $("#inventoryRegistration_list_queryForm #inventoryRegistration_list_factorycode").val();
    });
</script>