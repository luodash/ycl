<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="inventoryReview_list_grid-div">
    <form id="inventoryReview_list_hiddenForm" action="<%=path%>/inventoryPlan/toExcel.do" method="POST" style="display: none;">
        <input id="inventoryReview_list_ids" name="ids" value=""/>
        <input id="inventoryReview_list_ebsCode" name="ebsCode" value=""/>
    </form>
    <form id="inventoryReview_list_hiddenQueryForm" style="display:none;">
        <input  name="ebsCode" value=""/>
        <%--<input  name="state" value=""/>--%>
    </form>
    <c:if test="${operationCode.webSearch==1}">
    <div class="query_box" id="inventoryReview_list_yy" title="查询选项">
         <form id="inventoryReview_list_queryForm" style="max-width:100%;">
			 <ul class="form-elements">
				<li class="field-group field-fluid3">
					<label class="inline"  style="margin-right:20px;width: 100%;">
						<span class="form_label" style="width:80px;">EBS盘点号：</span>
						<input id="inventoryReview_list_factcode" name="ebsCode" type="text" style="width: calc(100% - 85px);" placeholder="EBS盘点单号" >
					</label>			
				</li>
				<%--<li class="field-group field-fluid3">
                    <label class="inline" for="inventoryReview_list_state" style="margin-right:20px;width: 100%;">
                        <span class="form_label" style="width:80px;">审核状态：</span>
                        <select id="inventoryReview_list_state" name="state" type="text" style="width: calc(100% - 85px);" placeholder="审核状态">
                            <option value="">--请选择--</option>
                            <c:forEach items="${stateList}" var="state">
                                <option value="${state.key}">${state.value}</option>
                            </c:forEach>
                        </select>
                    </label>
                </li>--%>
			</ul>
			<div class="field-button" style="">
                <div class="btn btn-info" onclick="inventoryReview_list_queryOk();">
                    <i class="ace-icon fa fa-check bigger-110"></i>查询
                </div>
                <div class="btn" onclick="inventoryReview_list_reset();"><i class="ace-icon icon-remove"></i>重置</div>
            </div>
		  </form>		 
    </div>
    </c:if>
    <div id="inventoryReview_list_fixed_tool_div" class="fixed_tool_div">
        <div id="inventoryReview_list_toolbar_" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="inventoryReview_list_grid-table" style="width:100%;height:100%;"></table>
    <div id="inventoryReview_list_grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
	var inventoryReview_list_oriData;
	var inventoryReview_list_grid;
	var inventoryReview_list_selectid;
	$(function  (){
	    $(".toggle_tools").click();
	});
	$("#inventoryReview_list_toolbar_").iToolBar({
	    id: "inventoryReview_list_tb_01",
	    items: [
	        {label: "审核",hidden:"${operationCode.webReview}"=="1", onclick:inventoryReview_list_openExaminePage, iconClass:'icon-asterisk'},
            {label: "导出",hidden:"${operationCode.webExport}"=="1", onclick:function(){inventoryReview_list_toExcel();},iconClass:'icon-share'}
	   ]
	});

    inventoryReview_list_grid = jQuery("#inventoryReview_list_grid-table").jqGrid({
		url : context_path + "/inventoryReview/list.do",
	    datatype : "json",
	    colNames : [ "主键","EBS盘点单号","WMS盘点单号","仓库","执行人","状态"],
	    colModel : [
	          {name : "id",index : "id",hidden:true},
              {name : "ebsCode",index : "ebs_code",width : 40},
              {name : "code",index : "code",width : 40},
	          {name : "factoryname",index : "ebs_code",width : 100},
              {name : "userName",index : "ebs_code",width : 120},
              {name : "stateName",index : "stateName",width : 20,
                    formatter:function(cellvalue,option,rowObject){
                        if (cellvalue) {
                            if (cellvalue.indexOf('1')>-1){
                                return '<span style="color:#76b86b;font-weight:bold;">审核中</span>';
                            }else if (cellvalue.indexOf('0')>-1){
                                if (cellvalue.indexOf('2')>-1){
                                    return '<span style="color:#76b86b;font-weight:bold;">审核中</span>';
                                }else {
                                    return '<span style="color:red;font-weight:bold;">新增任务</span>';
                                }
                            }else {
                                return '<span style="color:#FF0000;font-weight:bold;">已审核</span>';
                            }
                        }else {
                            return "<span style=\"color:grey;font-weight:bold;\">无数据</span>";
                        }
                    }
              }
        ],
	    rowNum : 20,
	    rowList : [ 10, 20, 30 ],
	    pager : "#inventoryReview_list_grid-pager",
	    sortname : "t.ebs_code",
	    sortorder : "desc",
        altRows: true,
        viewrecords : true,
        hidegrid:false,
    	autowidth:true, 
        multiselect:true,
        multiboxonly:true,
        loadComplete : function(data){
	        var table = this;
	        setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            inventoryReview_list_oriData = data;
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
	});

	jQuery("#inventoryReview_list_grid-table").navGrid("#inventoryReview_list_grid-pager", {edit:false,add:false,del:false,search:false,refresh:false})
    .navButtonAdd("#inventoryReview_list_grid-pager",{
		caption:"",   
		buttonicon:"fa fa-refresh green",   
		onClickButton: function(){   
			$("#inventoryReview_list_grid-table").jqGrid("setGridParam", {
                postData: {queryJsonString:""} //发送数据
            }).trigger("reloadGrid");
		}
	}).navButtonAdd("#inventoryReview_list_grid-pager",{
        caption: "",
        buttonicon:"fa icon-cogs",
        onClickButton : function (){
            jQuery("#inventoryReview_list_grid-table").jqGrid("columnChooser",{
                done: function(perm, cols){
                    // dynamicColumns(cols,materials_list_dynamicDefalutValue);
                    $("#inventoryReview_list_grid-table").jqGrid("setGridWidth", $("#inventoryReview_list_grid-div").width());
                }
            });
        }
    });

	$(window).on("resize.jqGrid", function () {
		$("#inventoryReview_list_grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);
		$("#inventoryReview_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-10-
		$("#inventoryReview_list_yy").outerHeight(true)-$("#inventoryReview_list_fixed_tool_div").outerHeight(true)-
		$("#inventoryReview_list_grid-pager").outerHeight(true)-$("#gview_inventoryReview_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
	});
	$(window).triggerHandler("resize.jqGrid");

	//打开审核页面
	function inventoryReview_list_openExaminePage(){
		var selectAmount = getGridCheckedNum("#inventoryReview_list_grid-table");
	    if(selectAmount==0){
	        layer.msg("请选择一条记录！",{icon:2});
	        return;
	    }else if(selectAmount>1){
	        layer.msg("只能选择一条记录！",{icon:8});
	        return;
	    }/* else if(rowData.state.indexOf("已审核")>0){
            layer.msg("已审核得数据无法再次审核！",{icon:8});
            return;
        } */
        //ebs盘点单号
		var rowId = $("#inventoryReview_list_grid-table").jqGrid("getGridParam","selrow");
		//wms盘点单号
		var rowData = jQuery("#inventoryReview_list_grid-table").jqGrid("getRowData",rowId);
		$.post(context_path + "/inventoryReview/toAdd.do", {id:rowId,code:rowData.code}, function (str){
			$queryWindow = layer.open({
			    title : "盘点审核",
		    	type:1,
		    	skin : "layui-layer-molv",
		    	area : window.screen.width-20+"px",
		    	shade : 0.6, //遮罩透明度
			    moveType : 1, //拖拽风格，0是默认，1是传统拖动
			    anim : 2,
			    content : str,
			    success: function (layero, index) {
	                layer.closeAll('loading');
	            }
			});
		});
	}

	/**
	 * 查询按钮点击事件
	 */
	 function inventoryReview_list_queryOk(){
		 var queryParam = iTsai.form.serialize($("#inventoryReview_list_queryForm"));
		 //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
		 inventoryReview_list_queryByParam(queryParam);
	}

	function inventoryReview_list_queryByParam(jsonParam) {
	    iTsai.form.deserialize($("#inventoryReview_list_hiddenQueryForm"), jsonParam);
	    var queryParam = iTsai.form.serialize($("#inventoryReview_list_hiddenQueryForm"));
	    var queryJsonString = JSON.stringify(queryParam); 
	    $("#inventoryReview_list_grid-table").jqGrid("setGridParam", {
            postData: {queryJsonString: queryJsonString}
        }).trigger("reloadGrid");
	}
	
	function inventoryReview_list_reset(){
		 iTsai.form.deserialize($("#inventoryReview_list_queryForm"), iTsai.form.serialize($("#inventoryReview_list_queryForm")));
         /*$("#inventoryReview_list_state option[value='']").attr("selected", "selected");*/
         $("#inventoryReview_list_factcode").val('');
		 inventoryReview_list_queryByParam(iTsai.form.serialize($("#inventoryReview_list_queryForm")));
	}
	
	function inventoryReview_list_toExcel(){
        var selectAmount = getGridCheckedNum("#inventoryReview_list_grid-table");
        if(selectAmount==0){
            layer.msg("请选择要导出的盘点单！",{icon:2});
            return;
        }else if(selectAmount>1){
            layer.msg("只能选择一个盘点单！",{icon:8});
            return;
        }

	    $("#inventoryReview_list_hiddenForm #inventoryReview_list_ids")
            .val(jQuery("#inventoryReview_list_grid-table").jqGrid('getRowData', jQuery("#inventoryReview_list_grid-table").jqGrid('getGridParam', 'selrow')).ebsCode);
        $("#inventoryReview_list_hiddenForm #inventoryReview_list_ebsCode").val($("#inventoryReview_list_queryForm #inventoryReview_list_factcode").val());
	    $("#inventoryReview_list_hiddenForm").submit();
	}

    $("#inventoryReview_list_queryForm .mySelect2").select2();   

</script>