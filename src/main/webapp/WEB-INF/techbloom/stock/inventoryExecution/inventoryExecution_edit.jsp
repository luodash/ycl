<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<div id="inventoryExecution_edit_page" class="row-fluid" style="height: inherit;">
	<form id="inventoryExecution_edit_carForm" class="form-horizontal" style="overflow: auto; height: calc(100% - 70px);">
		<input type="hidden" id="inventoryExecution_edit_id" name="id" value="${car.id}">		
		<div class="control-group">
			<label class="control-label" for="inventoryExecution_edit_task">盘点任务：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<select id="inventoryExecution_edit_task" name="task" data-placeholder="盘点任务" style="width:435px;">
						<option value="RW00001">RW00001</option>
						<option value="RW00002">RW00002</option>
						<option value="RW00003">RW00003</option>
					</select>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="inventoryExecution_edit_shielf">选择库位：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<select id="inventoryExecution_edit_shielf" name="shielf" data-placeholder="选择库位" style="width:435px;">
						<option value="KW00001">KW00001</option>
						<option value="KW00001">KW00002</option>
						<option value="KW00001">KW00003</option>
					</select>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="inventoryExecution_edit_product">选择商品：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<select id="inventoryExecution_edit_product" name="product" data-placeholder="选择商品" style="width:435px;">
						<option value="KW00001">商品1</option>
						<option value="KW00001">商品2</option>
						<option value="KW00001">商品3</option>
					</select>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="inventoryExecution_edit_amount">确认数量：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="inventoryExecution_edit_amount" name="amount" value="" placeholder="确认数量">
				</div>
			</div>
		</div>
	</form>
	<div class="field-button" style="text-align: center;border-top: 0px;margin: 15px auto;">
		<span class="btn btn-info" onclick="saveForm();">
		   <i class="ace-icon fa fa-check bigger-110"></i>保存
		</span>
		<span class="btn btn-danger" onclick="layer.closeAll();">
		   <i class="icon-remove"></i>&nbsp;取消
		</span>
	</div>
</div>
<script type="text/javascript">

	//确定按钮点击事件
    function saveForm() {
        if ($("#inventoryExecution_edit_carForm").valid()) {
            saveCarInfo($("#inventoryExecution_edit_carForm").serialize());
        }
    }
  	//保存/修改用户信息
    function saveCarInfo(bean) {
        $.ajax({
                url: context_path + "/inventoryExecution/saveCar",
                type: "POST",
                data: bean,
                dataType: "JSON",
                success: function (data) {
                    if (Boolean(data.result)) {
                        layer.msg("保存成功！", {icon: 1});
                        //关闭当前窗口
                        layer.close($queryWindow);
                        //刷新列表
                        $("#inventoryExecution_edit_grid-table").jqGrid('setGridParam',
                     {
                         postData: {queryJsonString: ""}
                     }).trigger("reloadGrid");
                    } else {
                        layer.alert("保存失败，请稍后重试！", {icon: 2});
                    }
                },
                error:function(XMLHttpRequest){
            		alert(XMLHttpRequest.readyState);
            		alert("出错啦！！！");
            	}
            });
    }

    $.ajax({
	 type:"POST",
	 url:context_path + "/inventoryExecution/getCarById",
	 data:{id:$("#inventoryExecution_edit_carForm #inventoryExecution_edit_id").val()},
	 dataType:"json",
	 success:function(data){
			  $("#inventoryExecution_edit_carForm #inventoryExecution_edit_shelveId").select2("data", {
 			  id: data.shelveId,
 			  text: data.shelveName
             });
		}
	});

$("#inventoryExecution_edit_allocations").select2({
    placeholder: "选择人员",
    minimumInputLength: 0, //至少输入n个字符，才去加载数据
    allowClear: true, //是否允许用户清除文本信息
    delay: 250,
    formatNoMatches: "没有结果",
    formatSearching: "搜索中...",
    formatAjaxError: "加载出错啦！",
    multiple: true,
    ajax: {
        url: context_path + "/inventoryExecution/getPerson",
        type: "POST",
        dataType: "json",
        delay: 250,
        data: function (term, pageNo) { //在查询时向服务器端传输的数据
            term = $.trim(term);
            return {
                queryString: term, //联动查询的字符
                pageSize: 15, //一次性加载的数据条数
                pageNo: pageNo, //页码
                shelveId: $("#inventoryExecution_edit_shelveId").val()
                //测试
            }
        },
        results: function (data, pageNo) {
            var res = data.result;
            if (res.length > 0) { //如果没有查询到数据，将会返回空串
                var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                return {
                    results: res,
                    more: more
                };
            } else {
                return {
                    results: {}
                };
            }
        },
        cache: true
    }

});
</script>