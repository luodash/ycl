<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="procurement_outstorage_list_grid-div">
    <form id="procurement_outstorage_list_hiddenForm" action="<%=path%>/procurementOutStorage/toExcel.do" method="POST" style="display: none;">
        <input id="procurement_outstorage_list_ids" name="ids" value=""/>
    </form>
    <form id="procurement_outstorage_list_hiddenQueryForm" style="display:none;">
        <input name="qaCode" value=""/>
        <input name="startTime" value=""/>
        <input name="endTime" value=""/>
    </form>
    <c:if test="${operationCode.webSearch==1}">
    <div class="query_box" id="procurement_outstorage_list_yy" title="查询选项">
        <form id="procurement_outstorage_list_queryForm" style="max-width:100%;">
            <ul class="form-elements">
                <li class="field-group field-fluid3">
                    <label class="inline" for="procurement_outstorage_list_qaCode" style="margin-right:20px;width: 100%;">
                        <span class="form_label" style="width:80px;">质保单号：</span>
                        <input id="procurement_outstorage_list_qaCode" name="qaCode" type="text" style="width: calc(100% - 85px);" placeholder="质保单号">
                    </label>
                </li>
                <li class="field-group field-fluid3">
                    <label class="inline" for="procurement_outstorage_list_startTime" style="margin-right:20px;width:100%;">
                        <span class="form_label" style="width:80px;">开始时间：</span>
                        <input type="text" class="form-control date-picker" id="procurement_outstorage_list_startTime" name="startTime" value="" style="width: calc(100% - 85px);" placeholder="开始时间" />
                    </label>
                </li>
                <li class="field-group field-fluid3">
                    <label class="inline" for="procurement_outstorage_list_endTime" style="margin-right:20px;width:100%;">
                        <span class="form_label" style="width:80px;">结束时间：</span>
                        <input type="text" class="form-control date-picker" id="procurement_outstorage_list_endTime" name="endTime" value="" style="width: calc(100% - 85px);" placeholder="结束时间" />
                    </label>
                </li>
            </ul>
            <div class="field-button" style="">
                <div class="btn btn-info" onclick="procurement_outstorage_list_queryOk();">
                    <i class="ace-icon fa fa-check bigger-110"></i>查询
                </div>
                <div class="btn" onclick="procurement_outstorage_list_reset();"><i class="ace-icon icon-remove"></i>重置</div>
            </div>
        </form>
    </div>
    </c:if>
    <div id="procurement_outstorage_list_fixed_tool_div" class="fixed_tool_div">
        <div id="procurement_outstorage_list_toolbar_" style="float:left;overflow:hidden;"></div>
    </div>
    <div style="overflow-x:auto"><table id="procurement_outstorage_list_grid-table" style="width:100%;height:100%;"></table></div>
    <div id="procurement_outstorage_list_grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
    var procurement_outstorage_list_oriData;
    var procurement_outstorage_list_grid;
    $(".date-picker").datetimepicker({format: "YYYY-MM-DD"});
    $(function  (){
        $(".toggle_tools").click();
    });

    $("#procurement_outstorage_list_toolbar_").iToolBar({
        id: "procurement_outstorage_list_tb_01",
        items: [
            {label: "导出",hidden:"${operationCode.webExport}"=="1",onclick:function(){procurement_outstorage_list_toExcel();},iconClass:'icon-share'}
        ]
    });
    var procurement_outstorage_list_queryForm_data = iTsai.form.serialize($("#procurement_outstorage_list_queryForm"));

    procurement_outstorage_list_grid = jQuery("#procurement_outstorage_list_grid-table").jqGrid({
        url : context_path + "/procurementOutStorage/list.do",
        datatype : "json",
        colNames : ["id","质保单号","批次号","物料名称","销售经理","出库时间","盘号","盘类型","盘规格","盘外径","米数","仓库","库位"],
        colModel : [
            {name : "id",index : "id",hidden : true},
            {name : "qaCode",index : "qaCode",width:80},
            {name : "batchNo",index : "batchNo",width:90},
            {name : "materialName",index : "materialName",width :170},
            {name : "salesName",index : "salesname",width : 70},
            {name : "outStorageTime",index : "outstoragetime",width : 140},
            {name : "dishcode",index : "dishcode",width : 60},
            {name : "drumType",index : "drumType",width : 60},
            {name : "model",index : "model",width : 160},
            {name : "outerDiameter",index : "outerDiameter",width : 60},
            {name : "meter",index : "meter",width : 60},
            {name : "warehouseName",index : "warehouseName",width : 160},
            {name : "shelfCode",index : "shelfCode",width : 160}
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : "#procurement_outstorage_list_grid-pager",
        sortname : "id",
        sortorder : "asc",
        altRows: true,
        viewrecords : true,
        hidegrid:false,
        autowidth:false,
        multiselect:true,
        multiboxonly:true,
        shrinkToFit:false,
        autoScroll: true,
        loadComplete : function(data){
            var table = this;
            setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            procurement_outstorage_list_oriData = data;
            $("#procurement_outstorage_list_grid-table").closest(".ui-jqgrid-bdiv").css({ 'overflow-x': 'auto' });
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
    });

    jQuery("#procurement_outstorage_list_grid-table").navGrid("#procurement_outstorage_list_grid-pager",
        {edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#procurement_outstorage_list_grid-pager",{
        caption:"",
        buttonicon:"fa fa-refresh green",
        onClickButton: function(){
            $("#procurement_outstorage_list_grid-table").jqGrid("setGridParam",
                {
                    postData: {queryJsonString:""} //发送数据
                }
            ).trigger("reloadGrid");
        }
    }).navButtonAdd("#procurement_outstorage_list_grid-pager",{
        caption: "",
        buttonicon:"fa icon-cogs",
        onClickButton : function (){
            jQuery("#procurement_outstorage_list_grid-table").jqGrid("columnChooser",{
                done: function(perm, cols){
                    $("#procurement_outstorage_list_grid-table").jqGrid("setGridWidth", $("#procurement_outstorage_list_grid-div").width());
                }
            });
        }
    });

    $(window).on("resize.jqGrid", function () {
        $("#procurement_outstorage_list_grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);
        $("#procurement_outstorage_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-10-
        $("#procurement_outstorage_list_yy").outerHeight(true)-$("#procurement_outstorage_list_fixed_tool_div").outerHeight(true)-
        $("#procurement_outstorage_list_grid-pager").outerHeight(true)-$("#gview_procurement_outstorage_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
    });

    $(window).triggerHandler("resize.jqGrid");

    /**
     * 查询按钮点击事件
     */
    function procurement_outstorage_list_queryOk(){
        var queryParam = iTsai.form.serialize($("#procurement_outstorage_list_queryForm"));
        //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
        procurement_outstorage_list_queryByParam(queryParam);
    }

    function procurement_outstorage_list_queryByParam(jsonParam) {
        iTsai.form.deserialize($("#procurement_outstorage_list_hiddenQueryForm"), jsonParam);
        var queryParam = iTsai.form.serialize($("#procurement_outstorage_list_hiddenQueryForm"));
        var queryJsonString = JSON.stringify(queryParam);
        $("#procurement_outstorage_list_grid-table").jqGrid("setGridParam",
            {
                postData: {queryJsonString: queryJsonString}
            }
        ).trigger("reloadGrid");
    }

    //清空重置
    function procurement_outstorage_list_reset(){
        iTsai.form.deserialize($("#procurement_outstorage_list_queryForm"),procurement_outstorage_list_queryForm_data);
        procurement_outstorage_list_queryByParam(procurement_outstorage_list_queryForm_data);
    }

    //导出功能
    function procurement_outstorage_list_toExcel(){
        var ids = jQuery("#procurement_outstorage_list_grid-table").jqGrid("getGridParam", "selarrrow");
        $("#procurement_outstorage_list_hiddenForm #procurement_outstorage_list_ids").val(ids);
        $("#procurement_outstorage_list_hiddenForm").submit();
    }
</script>