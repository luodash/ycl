<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<div id="inventoryPlan_view_page" class="row-fluid" style="height: inherit;">
	<table border="1" style="width:100%;">
		<tbody id="table01">
			<tr>
				<td style="width:40px;height:40px;background-color: #0d8ddb;">
					<img src="<%=path%>/plugins/public_components/img/huowei.jpg" style="width:40px;height: 30px">
					1-1
				</td>
				<td style="width:40px;height:40px;background-color: #0d8ddb;"></td>
			</tr>
			<tr>
				<td style="width:40px;height:40px;background-color: #0d8ddb;"></td>
				<td style="width:40px;height:40px;background-color: #0d8ddb;"></td>
			</tr>
		</tbody>
	</table>
</div>
<script type="text/javascript">
	var html = "";
	for(var i=1;i<=10;i++){
	    html+="<tr>";
	    for(var j=1;j<=10;j++){
	        html+="<td style='width:10%;height:100px;background-color: #0d8ddb;text-align:center;'>" +
                "<div style='width:100%;height:100%;'><img src='<%=path%>/plugins/public_components/img/huowei.png' style='width:100%;padding-bottom:10px;'>" +
                +i+"-"+j+
                "</div></td>";
		}
		html+="</tr>"
	}
	$("#inventoryPlan_view_table01").empty().append(html);


	//确定按钮点击事件
    function saveForm() {
        if ($("#inventoryPlan_view_carForm").valid()) {
            saveCarInfo($("#inventoryPlan_view_carForm").serialize());
        }
    }
  	//保存/修改用户信息
    function saveCarInfo(bean) {
        $.ajax({
                url: context_path + "/inventoryPlan/saveCar",
                type: "POST",
                data: bean,
                dataType: "JSON",
                success: function (data) {
                    if (Boolean(data.result)) {
                        layer.msg("保存成功！", {icon: 1});
                        //关闭当前窗口
                        layer.close($queryWindow);
                        //刷新列表
                        $("#inventoryPlan_view_grid-table").jqGrid('setGridParam',
                     {
                         postData: {queryJsonString: ""}
                     }).trigger("reloadGrid");
                    } else {
                        layer.alert("保存失败，请稍后重试！", {icon: 2});
                    }
                },
                error:function(XMLHttpRequest){
            		alert(XMLHttpRequest.readyState);
            		alert("出错啦！！！");
            	}
            });
    }
  	
</script>