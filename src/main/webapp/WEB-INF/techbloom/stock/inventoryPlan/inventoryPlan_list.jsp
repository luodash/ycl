<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="inventoryPlan_list_grid-div">
    <form id="inventoryPlan_list_hiddenForm" action="<%=path%>/inventoryPlan/toExcel.do" method="POST" style="display: none;">
        <input id="inventoryPlan_list_ids" name="ids" value=""/>
    </form>
    <form id="inventoryPlan_list_hiddenQueryForm" style="display:none;">
        <input  name="code" value=""/>
        <input  name="factorycode" value=""/>
        <input  name="startTime" value=""/>
        <input  name="endTime" value=""/>
    </form>
    <c:if test="${operationCode.webSearch==1}">
    <div class="query_box" id="inventoryPlan_list_yy" title="查询选项">
         <form id="inventoryPlan_list_queryForm" style="max-width:100%;">
			 <ul class="form-elements">
			 	<li class="field-group field-fluid3">
					<label class="inline" for="inventoryPlan_list_code" style="margin-right:20px;width: 100%;">
						 <span class="form_label" style="width:80px;">盘点单号：</span>
						 <input id="inventoryPlan_list_code" name="code" type="text" style="width: calc(100% - 85px);" placeholder="EBS盘点单号">
					</label>
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="inventoryPlan_list_factorycode" style="margin-right:20px;width: 100%;">
						 <span class="form_label" style="width:80px;">厂区：</span>
						 <input id="inventoryPlan_list_factorycode" name="factorycode" type="text" style="width: calc(100% - 85px);" placeholder="厂区">
					</label>
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="inventoryPlan_list_startTime" style="margin-right:20px;width: 100%;">
						 <span class="form_label" style="width:80px;">创建时间起：</span>
						 <input id="inventoryPlan_list_startTime" class="form-control date-picker" name="startTime" type="text" style="width: calc(100% - 85px);" placeholder="创建时间起">
					</label>
				 </li>

				 <li class="field-group-top field-group field-fluid3">
					 <label class="inline" for="inventoryPlan_list_endTime" style="margin-right:20px;width: 100%;">
						 <span class="form_label" style="width:80px;">创建时间止：</span>
						 <input id="inventoryPlan_list_endTime" class="form-control date-picker" name="endTime" type="text" style="width:  calc(100% - 85px);" placeholder="创建时间止">
					 </label>
				</li>
				<li class="field-group field-fluid3">
				 </li>
			</ul>
			<div class="field-button" style="">
				<div class="btn btn-info" onclick="inventoryPlan_list_queryOk();">
			        <i class="ace-icon fa fa-check bigger-110"></i>查询
		        </div>
				<div class="btn" onclick="inventoryPlan_list_reset();"><i class="ace-icon icon-remove"></i>重置</div>
				<a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>
		    </div>
		  </form>		 
    </div>
    </c:if>
    <div id="inventoryPlan_list_fixed_tool_div" class="fixed_tool_div">
        <div id="inventoryPlan_list_toolbar__" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="inventoryPlan_list_grid-table" style="width:100%;height:100%;"></table>
    <div id="inventoryPlan_list_grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
	var inventoryPlan_list_oriData;
	var inventoryPlan_list_grid;
	var inventoryPlan_list_selectid;

	$(function  (){
	    $(".toggle_tools").click();
	});

	$("input").keypress(function (e) {
		if (e.which == 13) {
			inventoryPlan_list_queryOk();
		}
	});
	
	//时间控件
	$(".date-picker").datetimepicker({
		format: "YYYY-MM-DD HH:mm:ss" ,
		autoclose : true,
		todayHighlight : true
	});
	
	$("#inventoryPlan_list_toolbar__").iToolBar({
	    id: "inventoryPlan_list_tb_01",
	    items: [
	        {label: "添加", hidden:"${operationCode.webAdd}"=="1", onclick:inventoryPlan_list_openAddPage, iconClass:'glyphicon glyphicon-plus'},
	        {label: "编辑",hidden:"${operationCode.webEdit}"=="1", onclick: inventoryPlan_list_openEditPage, iconClass:'glyphicon glyphicon-pencil'},
	        {label: "删除",hidden:"${operationCode.webDel}"=="1", onclick: inventoryPlan_list_deletePlan, iconClass:'glyphicon glyphicon-trash'}
	   ]
	});
	
	var inventoryPlan_list_queryForm_data = iTsai.form.serialize($("#inventoryPlan_list_queryForm"));

	inventoryPlan_list_grid = jQuery("#inventoryPlan_list_grid-table").jqGrid({
		url : context_path + "/inventoryPlan/list.do",
	    datatype : "json",
	    colNames : [ "主键","EBS盘点单号","WMS盘点单号","仓库","创建人","创建时间","执行人","状态"],
	    colModel : [ 
            {name : "id",index : "id",hidden:true},
			{name : "ebsCode",index : "ebs_code",width : 50},
            {name : "code",index : "code",width : 50},
            {name : "factoryname",index : "factoryname",width : 130},
            {name : "creater",index : "creater",width:20},
            {name : "createtime",index : "createtime",width:50},
            {name : "userName",index : "userName",width :90},
            {name : "state",index : "state",width : 30,
            	formatter:function(cellValu,option,rowObject){
            		if(cellValu=='0') {
            			return '<span style="color:#FF0000;font-weight:bold;">新增任务</span>';
					}else if(cellValu=='1') {
						return '<span style="color:#76b86b;font-weight:bold;">审核中</span>';
					}else if(cellValu=='2') {
                    	return '<span style="color:#76b86b;font-weight:bold;">已审核</span>';
					}
            	}
            }
        ],
	    rowNum : 20,
	    rowList : [ 10, 20, 30 ],
	    pager : "#inventoryPlan_list_grid-pager",
	    sortname : "id",
	    sortorder : "desc",
        altRows: true,
        viewrecords : true,
        hidegrid:false,
    	autowidth:true, 
        multiselect:true,
        multiboxonly:true,
        loadComplete : function(data){
           var table = this;
           setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
			inventoryPlan_list_oriData = data;
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
	});

	jQuery("#inventoryPlan_list_grid-table").navGrid("#inventoryPlan_list_grid-pager", {edit:false,add:false,del:false,search:false,refresh:false})
	.navButtonAdd("#inventoryPlan_list_grid-pager",{
		caption:"",   
		buttonicon:"fa fa-refresh green",   
		onClickButton: function(){
			$("#inventoryPlan_list_grid-table").jqGrid("setGridParam", {
		    	postData: {queryJsonString:""} //发送数据
			}).trigger("reloadGrid");
		}
	}).navButtonAdd("#inventoryPlan_list_grid-pager",{
        caption: "",
        buttonicon:"fa icon-cogs",
        onClickButton : function (){
            jQuery("#inventoryPlan_list_grid-table").jqGrid("columnChooser",{
                done: function(perm, cols){
                    $("#inventoryPlan_list_grid-table").jqGrid("setGridWidth", $("#inventoryPlan_list_grid-div").width());
                }
            });
        }
    });

	$(window).on("resize.jqGrid", function () {
		$("#inventoryPlan_list_grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);
		$("#inventoryPlan_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-10-
		$("#inventoryPlan_list_yy").outerHeight(true)-$("#inventoryPlan_list_fixed_tool_div").outerHeight(true)-
		$("#inventoryPlan_list_grid-pager").outerHeight(true)- $("#gview_inventoryPlan_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
	});
	
	$(window).triggerHandler("resize.jqGrid");
	
	//打开添加页面
	function inventoryPlan_list_openAddPage(){
		$.post(context_path + "/inventoryPlan/toAdd.do", {id:-1}, function (str){
			$queryWindow=layer.open({
			    title : "盘点计划添加",
		    	type:1,
		    	skin : "layui-layer-molv",
		    	area : window.screen.width-250+"px",
		    	shade : 0.6, //遮罩透明度
			    moveType : 1, //拖拽风格，0是默认，1是传统拖动
			    anim : 2,
			    content : str,
			    success: function (layero, index) {
	                layer.closeAll('loading');
	            }
			});
		});
	}
	
	//提交
	function inventoryPlan_list_tj(){
		var selectAmount = getGridCheckedNum("#inventoryPlan_list_grid-table");
		var rowData="",k=0;
		if(selectAmount<1){
			layer.msg("请选择一条记录！",{icon:2});
			return;
		}
		var ids = $("#inventoryPlan_list_grid-table").jqGrid('getGridParam','selarrrow');
		for(var i=0;i<ids.length;i++){
			rowData = $("#inventoryPlan_list_grid-table").jqGrid('getRowData',ids[i]);
			if(rowData.state.indexOf("已提交")>0) k++;
		}
		if(k==0){
			$.ajax({
				url:context_path+"/inventoryPlan/submit?ids="+ids,
				type:"post",
				dataType:"JSON",
				success:function (data){
					if(data.result){
						layer.msg(data.msg,{icon:1,time:1200});
						inventoryPlan_list_grid.trigger("reloadGrid");
					}else{
						layer.msg(data.msg,{icon:2,time:1200});
					}
				}
			});
		}else{
			layer.msg("已提交任务无法再次提交！", {icon: 7,time:1000});
		}
	}

	/*打开编辑页面*/
	function inventoryPlan_list_openEditPage(){
        var gData = jQuery("#inventoryPlan_list_grid-table").jqGrid('getGridParam','selarrrow');
        var rowData = $("#inventoryPlan_list_grid-table").jqGrid('getRowData',gData[0]);
		var selectAmount = getGridCheckedNum("#inventoryPlan_list_grid-table");
		if(selectAmount==0){
			layer.msg("请选择一条记录！",{icon:2});
			return;
		}else if(selectAmount>1){
			layer.msg("只能选择一条记录！",{icon:8});
			return;
		}else if(rowData.state.indexOf("新增")==-1){
            layer.msg("只能修改状态为新增的任务！",{icon:8});
            return;
        }
		layer.load(2);
		inventoryPlan_list_selectid = jQuery("#inventoryPlan_list_grid-table").jqGrid("getGridParam", "selrow");
		$.post(context_path+'/inventoryPlan/toAdd.do?id='+inventoryPlan_list_selectid, {}, function(str){
				$queryWindow = layer.open({
					title : "盘点计划编辑",
					type: 1,
				    skin : "layui-layer-molv",
				    area : window.screen.width-250+"px",
					shade: 0.6, //遮罩透明度
					moveType: 1, //拖拽风格，0是默认，1是传统拖动
					content: str,//注意，如果str是object，那么需要字符拼接。
					success:function(layero, index){
						layer.closeAll("loading");
					}
				});
			}).error(function() {
				layer.closeAll();
		    	layer.msg("加载失败！",{icon:2});
		});
	}
	
	//删除
	function inventoryPlan_list_deletePlan(){
	 	var checkedNum = getGridCheckedNum("#inventoryPlan_list_grid-table", "id");  //选中的数量
	 	var rowData="",k=0;
	 	var ids = $("#inventoryPlan_list_grid-table").jqGrid('getGridParam','selarrrow');
	 	for(var i=0;i<ids.length;i++){
			rowData = $("#inventoryPlan_list_grid-table").jqGrid('getRowData',ids[i]);
			if(rowData.state.indexOf("新增")==-1){
				layer.msg("只能删除状态为新增的任务！", {icon: 7,time:1000});
				return;
			}
		}
	    if (checkedNum == 0) {
	    	layer.alert("请选择一个要删除的盘点计划！");
	    } else {
	        var ids = jQuery("#inventoryPlan_list_grid-table").jqGrid("getGridParam", "selarrrow");
	        layer.confirm("确定删除选中的盘点计划？", function() {
	    		$.ajax({
	    			type : "POST",
	    			url : context_path + "/inventoryPlan/deleteCars.do?ids="+ids ,
	    			dataType : "json",
	    			cache : false,
	    			success : function(data) {
	    				layer.closeAll();
	    				if (Boolean(data.result)) {
	    					layer.msg(data.msg, {icon: 1,time:1000});
	    				}else{
	    					layer.msg(data.msg, {icon: 7,time:1000});
	    				}
						inventoryPlan_list_grid.trigger("reloadGrid");  //重新加载表格
	    			}
	    		});
	    	});
	    }  
	}

	function toView(){
	    $.post(context_path + "/inventoryPlan/toView.do", {}, function (str){
	        $queryWindow=layer.open({
	            title : "可视化窗口",
	            type:1,
	            skin : "layui-layer-molv",
	            area : "600px",
	            shade : 0.6, //遮罩透明度
	            moveType : 1, //拖拽风格，0是默认，1是传统拖动
	            anim : 2,
	            content : str,
	            success: function (layero, index) {
	                layer.closeAll('loading');
	            }
	        });
	        layer.full($queryWindow);
	    });
	}

	/**
	 * 查询按钮点击事件
	 */
	 function inventoryPlan_list_queryOk(){
		 var queryParam = iTsai.form.serialize($("#inventoryPlan_list_queryForm"));
		 //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
		 inventoryPlan_list_queryByParam(queryParam);
	}
	
	function inventoryPlan_list_queryByParam(jsonParam) {
	    iTsai.form.deserialize($("#inventoryPlan_list_hiddenQueryForm"), jsonParam);
	    var queryParam = iTsai.form.serialize($("#inventoryPlan_list_hiddenQueryForm"));
	    var queryJsonString = JSON.stringify(queryParam); 
	    $("#inventoryPlan_list_grid-table").jqGrid("setGridParam",
	        {
	            postData: {queryJsonString: queryJsonString}
	        }
	    ).trigger("reloadGrid");
	}
	
	//厂区
	$("#inventoryPlan_list_queryForm #inventoryPlan_list_factorycode").select2({
	    placeholder: "选择厂区",
	    minimumInputLength:0,   //至少输入n个字符，才去加载数据
	    allowClear: true,  //是否允许用户清除文本信息
	    delay: 250,
	    formatNoMatches:"没有结果",
	    formatSearching:"搜索中...",
	    formatAjaxError:"加载出错啦！",
	    ajax : {
	        url: context_path+"/factoryArea/getFactoryList",
	        type:"POST",
	        dataType : 'json',
	        delay : 250,
	        data: function (term,pageNo) {     //在查询时向服务器端传输的数据
	            term = $.trim(term);
	            return {
	                queryString: term,    //联动查询的字符
	                pageSize: 15,    //一次性加载的数据条数
	                pageNo:pageNo   //页码
	            }
	        },
	        results: function (data,pageNo) {
	            var res = data.result;
	            if(res.length>0){   //如果没有查询到数据，将会返回空串
	                var more = (pageNo*15)<data.total; //用来判断是否还有更多数据可以加载
	                return {
	                    results:res,more:more
	                };
	            }else{
	                return {
	                    results:{}
	                };
	            }
	        },
	        cache : true
	    }
	});
	
	//重置
	function inventoryPlan_list_reset(){
	     $("#inventoryPlan_list_queryForm #inventoryPlan_list_factorycode").select2("val","");
		 iTsai.form.deserialize($("#inventoryPlan_list_queryForm"),inventoryPlan_list_queryForm_data);
		 inventoryPlan_list_queryByParam(inventoryPlan_list_queryForm_data);
	}
	
	//导出excel
	function toExcel(){
	    $("#inventoryPlan_list_hiddenForm #inventoryPlan_list_ids").val(jQuery("#inventoryPlan_list_grid-table").jqGrid("getGridParam", "selarrrow"));
	    $("#inventoryPlan_list_hiddenForm").submit();	
	}

    $("#inventoryPlan_list_queryForm .mySelect2").select2();   
</script>