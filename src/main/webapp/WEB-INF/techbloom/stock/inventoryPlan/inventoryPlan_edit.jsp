<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<div id="inventoryPlan_edit_page" class="row-fluid" style="height: inherit;">
	<form id="inventoryPlan_edit_inventoryForm" class="form-horizontal" style="overflow: auto; height: calc(100% - 70px);">
		<input type="hidden" id="inventoryPlan_edit_id" name="id" value="${it.id}">
		<div class="control-group">
			<label class="control-label" for="inventoryPlan_edit_factorycode">盘点单号：</label>
			<div class="controls">
				<div class="required">
					<input type="text" class="span11" id="inventoryPlan_edit_ebsCode" name="ebsCode" value="${it.ebsCode}" placeholder="请输入EBS盘点单号" >
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="inventoryPlan_edit_factorycode">盘点仓库：</label>
			<div class="controls">
				<div class="required">
					<input type="text" class="span11" id="inventoryPlan_edit_factorycode" name="factorycode" placeholder="请选择要盘点的仓库" >
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="inventoryPlan_edit_userId">分配人员：</label>
			<div class="controls">
				<div class="required">
					<input class="span11" type="text" name="userId" id="inventoryPlan_edit_userId" placeholder="请选择要分配的人员" >
				</div>
			</div>
		</div>
	</form>
	<div class="field-button" style="text-align: center;border-top: 0px;margin: 15px auto;">
		<span class="btn btn-info" onclick="saveForm();">
		   <i class="ace-icon fa fa-check bigger-110"></i>保存
		</span>
		<span class="btn btn-danger" onclick="layer.closeAll();">
		   <i class="icon-remove"></i>&nbsp;取消
		</span>
	</div>
</div>
<script type="text/javascript">
	var selectData = 0;
	var selectParam = "";  //存放之前的查询条件

	$("#inventoryPlan_edit_inventoryForm").validate({
		rules:{
			"userId":{
				required : true
			}
		},
		messages:{
			"userId":{
				required : "请输入分配人！"
			}
		},
		errorClass: "help-inline",
		errorElement: "div",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
		}
	});

	//保存/修改用户信息
	function saveForm() {
		if($("#inventoryPlan_edit_inventoryForm #inventoryPlan_edit_factorycode").val()==''){
			layer.msg("请选择盘点仓库！",{icon:2});
			return;
		}
		if($("#inventoryPlan_edit_inventoryForm #inventoryPlan_edit_userId").val()==''){
			layer.msg("请选择分配人员！",{icon:2});
			return;
		}
		if($("#inventoryPlan_edit_inventoryForm #inventoryPlan_edit_ebsCode").val()==''){
			layer.msg("请输入EBS的盘点单号！",{icon:2});
			return;
		}

		$.ajax({
			url : context_path + "/inventoryPlan/save",
			type : "POST",
			data : $("#inventoryPlan_edit_inventoryForm").serialize(),
			dataType : "JSON",
			success: function (data) {
				if (Boolean(data.result)) {
					layer.msg(data.msg, {icon: 1});
					//关闭当前窗口
					layer.close($queryWindow);
					//刷新列表
					iTsai.form.deserialize($("#inventoryPlan_list_hiddenQueryForm"), iTsai.form.serialize($("#inventoryPlan_list_queryForm")));
					$("#inventoryPlan_list_grid-table").jqGrid('setGridParam', {postData: {
							queryJsonString: JSON.stringify(iTsai.form.serialize($("#inventoryPlan_list_hiddenQueryForm")))
						}}).trigger("reloadGrid");
				}else{
					layer.alert(data.msg, {icon: 2});
				}
			},
			error:function(XMLHttpRequest){
				alert(XMLHttpRequest.readyState);
				alert("出错啦！！！");
			}
		});
	}

	//编辑状态页面的赋值
	if($("#inventoryPlan_edit_inventoryForm #inventoryPlan_edit_id").val()!="-1"){
		$.ajax({
			type:"POST",
			url:context_path + "/inventoryPlan/getInventoryById",
			data:{
				id : $("#inventoryPlan_edit_inventoryForm #inventoryPlan_edit_id").val()
			},
			dataType:"json",
			success:function(data){
				if (data) {
					if (data.userList) {
						var obj = [];
						for (var i = 0; i < data.userList.length; i++) {
							obj.push({
								id: data.userList[i].userId,
								text: data.userList[i].name
							});
						}
						$("#inventoryPlan_edit_userId").select2("data", obj);
					}

					if (data.factoryList) {
						var obj = [];
						for (var i = 0; i < data.factoryList.length; i++) {
							obj.push({
								id: data.factoryList[i].id,
								text: data.factoryList[i].name
							});
						}
						$("#inventoryPlan_edit_factorycode").select2("data", obj);
					}
				}
				$("#s2id_factorycode .select2-choices").find(".select2-search-choice").find("a").attr("onclick","removeChoice1();")
			}
		});
	}

	$("#inventoryPlan_edit_userId").select2({
		placeholder: "选择人员",
		minimumInputLength: 0, //至少输入n个字符，才去加载数据
		allowClear: true, //是否允许用户清除文本信息
		delay: 250,
		formatNoMatches: "没有结果",
		formatSearching: "搜索中...",
		formatAjaxError: "加载出错啦！",
		multiple: true,
		ajax: {
			url: context_path + "/inventoryPlan/getPerson",
			type: "POST",
			dataType: "json",
			delay: 250,
			data: function (term, pageNo) { //在查询时向服务器端传输的数据
				term = $.trim(term);
				return {
					queryString: term, //联动查询的字符
					pageSize: 15, //一次性加载的数据条数
					pageNo: pageNo//页码
				}
			},
			results: function (data, pageNo) {
				var res = data.result;
				if (res.length > 0) { //如果没有查询到数据，将会返回空串
					var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
					return {
						results: res,
						more: more
					};
				} else {
					return {
						results: {}
					};
				}
			},
			cache: true
		}
	});

	//仓库拉框
	$("#inventoryPlan_edit_inventoryForm #inventoryPlan_edit_factorycode").select2({
		placeholder: "选择仓库",
		minimumInputLength:0,   //至少输入n个字符，才去加载数据
		allowClear: true,  //是否允许用户清除文本信息
		delay: 250,
		formatNoMatches:"没有结果",
		formatSearching:"搜索中...",
		formatAjaxError:"加载出错啦！",
		multiple: true,	//支持多选
		ajax : {
			url: context_path+"/warehouselist/getWarehouseList",
			type:"POST",
			dataType : 'json',
			delay : 250,
			data: function (term,pageNo) {     //在查询时向服务器端传输的数据
				term = $.trim(term);
				return {
					queryString: term,    //联动查询的字符
					pageSize: 15,    //一次性加载的数据条数
					pageNo:pageNo    //页码
				}
			},
			results: function (data,pageNo) {
				var res = data.result;
				if(res.length>0){   //如果没有查询到数据，将会返回空串
					var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
					return {
						results:res,
						more:more
					};
				}else{
					return {
						results:{}
					};
				}
			},
			cache : true
		}
	});

	$('#inventoryPlan_edit_userId').on("change",function(e){
		var datas=$("#inventoryPlan_edit_userId").select2("val");
		selectData = datas;
		var selectSize = datas.length;
		if(selectSize>1){
			var $tags = $("#s2id_userId .select2-choices");
			var $choicelist = $tags.find(".select2-search-choice");
			var $clonedChoice = $choicelist[0];
			$tags.children(".select2-search-choice").remove();
			$tags.prepend($clonedChoice);

			$tags.find(".select2-search-choice").find("div").html(selectSize+"个被选中");
			$tags.find(".select2-search-choice").find("a").removeAttr("tabindex");
			$tags.find(".select2-search-choice").find("a").attr("href","#");
			$tags.find(".select2-search-choice").find("a").attr("onclick","removeChoice2();");
		}
		//执行select的查询方法
		$("#inventoryPlan_edit_userId").select2("search",selectParam);
	});

	$('#inventoryPlan_edit_factorycode').on("change",function(e){
		var datas=$("#inventoryPlan_edit_factorycode").select2("val");
		selectData = datas;
		var selectSize = datas.length;
		if(selectSize>1){
			var $tags = $("#s2id_factorycode .select2-choices");
			var $choicelist = $tags.find(".select2-search-choice");
			var $clonedChoice = $choicelist[0];
			$tags.children(".select2-search-choice").remove();
			$tags.prepend($clonedChoice);

			$tags.find(".select2-search-choice").find("div").html(selectSize+"个被选中");
			$tags.find(".select2-search-choice").find("a").removeAttr("tabindex");
			$tags.find(".select2-search-choice").find("a").attr("href","#");
			$tags.find(".select2-search-choice").find("a").attr("onclick","removeChoice1();");
		}
		//执行select的查询方法
		$("#inventoryPlan_edit_factorycode").select2("search",selectParam);
	});


	//清空执行人多选框中的值
	function removeChoice2(){
		$("#s2id_userId .select2-choices").children(".select2-search-choice").remove();
		$("#inventoryPlan_edit_userId").select2("val","");
		selectData = 0;
	}

	//清空仓库多选框中的值
	function removeChoice1(){
		$("#s2id_factorycode .select2-choices").children(".select2-search-choice").remove();
		$("#inventoryPlan_edit_factorycode").select2("val","");
		selectData = 0;
	}

</script>