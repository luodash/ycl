<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<div id="split_page" class="row-fluid" style="height: inherit;">
	<form id="carForm" class="form-horizontal" style="overflow: auto; height: calc(100% - 70px);">
		<div class="control-group">
			<label class="control-label" for="split_qacode">质保单号：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="split_qacode" name="qacode" value="ZB00001" placeholder="质保单号" readonly>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="split_rfid">RFID：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="rfid" name="rfid" value="RF00001" placeholder="RFID" readonly>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="meter">米数：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="split_meter" name="meter" value="100" placeholder="米数" readonly>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="split_splitMeter">拆分米数：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="split_splitMeter" name="splitMeter" value="${allot.code }" placeholder="拆分米数">
				</div>
			</div>
		</div>
	</form>
	<div class="field-button" style="text-align: center;border-top: 0px;margin: 15px auto;">
		<span class="btn btn-info" onclick="saveForm();">
		   <i class="ace-icon fa fa-check bigger-110"></i>保存
		</span>
		<span class="btn btn-danger" onclick="layer.closeAll();">
		   <i class="icon-remove"></i>&nbsp;取消
		</span>
	</div>
</div>
<script type="text/javascript">
	//确定按钮点击事件
    function saveForm() {
        if ($("#split_carForm").valid()) {
            saveCarInfo($("#split_carForm").serialize());
        }
    }
  //保存/修改用户信息
    function saveCarInfo(bean) {
        $.ajax({
                url: context_path + "/driving/saveCar",
                type: "POST",
                data: bean,
                dataType: "JSON",
                success: function (data) {
                    if (Boolean(data.result)) {
                        layer.msg("保存成功！", {icon: 1});
                        //关闭当前窗口
                        layer.close($queryWindow);
                        //刷新列表
                        $("#split_grid-table").jqGrid('setGridParam',
                     {
                         postData: {queryJsonString: ""}
                     }).trigger("reloadGrid");
                    } else {
                        layer.alert("保存失败，请稍后重试！", {icon: 2});
                    }
                },
                error:function(XMLHttpRequest){
            		alert(XMLHttpRequest.readyState);
            		alert("出错啦！！！");
            	}
            });
    }
</script>