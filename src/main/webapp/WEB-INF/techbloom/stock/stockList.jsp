<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="stockList_grid-div">
    <form id="stockList_hiddenForm" action="<%=path%>/storageInfo/toExcel.do" method="POST" style="display: none;">
        <input id="stockList_ids" name="ids" value=""/>
        <input id="stockList_queryFactoryCode" name="queryFactoryCode" value=""/>
        <input id="stockList_queryWarehousecode" name="queryWarehousecode" value=""/>
        <input id="stockList_queryShelfCode" name="queryShelfCode" value=""/>
        <input id="stockList_queryMaterialName" name="queryMaterialName" value=""/>
        <input id="stockList_queryQaCode" name="queryQaCode" value=""/>
        <input id="stockList_queryBatchNo" name="queryBatchNo" value=""/>
        <input id="stockList_queryExportExcelIndex" name="queryExportExcelIndex" value=""/>
    </form>
    <form id="stockList_hiddenQueryForm" style="display:none;">
    	<input name="factoryCode" value=""/>
        <input name="warehousecode" value=""/>
        <input name="shelfCode" value=""/>
        <input name="materialname" value="">
        <input name="qaCode" value=""/>
        <input name="batchNo" value=""/>
    </form>
    <c:if test="${operationCode.webSearch==1}">
    <div class="query_box" id="stockList_yy" title="查询选项">
        <form id="stockList_queryForm" style="max-width:100%;">
            <ul class="form-elements">
                <li class="field-group field-fluid3">
                    <label class="inline" for="stockList_qaCode" style="margin-right:20px;width:100%;">
                        <span class="form_label" style="width:61px;">质保号：</span>
                        <input type="text" id="stockList_qaCode" name="qaCode" value="" style="width: calc(100% - 66px);" placeholder="质保号" />
                    </label>

                </li>
                <li class="field-group field-fluid3">
                    <label class="inline" for="stockList_batchNo" style="margin-right:20px;width:100%;">
                        <span class="form_label" style="width:61px;">批次号：</span>
                        <input id="stockList_batchNo" name="batchNo" type="text" style="width: calc(100% - 66px);" placeholder="批次号">
                    </label>

                </li>
                <li class="field-group field-fluid3">
                	<label class="inline" style="margin-right:20px;width:100%;">
                        <span class="form_label" style="width:61px;">库位：</span>
                        <input type="text" id="stockList_shelfCode"  name="shelfCode" value="" style="width: calc(100% - 66px);" placeholder="库位" />
                    </label>
                </li>
                
                <li class="field-group-top field-group field-fluid3">
					<label class="inline" for="stockList_materialname" style="margin-right:20px;width:100%;">
                        <span class="form_label" style="width:61px;">物料：</span>
                        <input id="stockList_materialname" name="materialname" type="text" style="width: calc(100% - 66px);" placeholder="物料">
                    </label>
				</li>
                <li class="field-group field-fluid3">
                    <label class="inline" for="stockList_factoryCode" style="margin-right:20px;width: 100%;">
                        <span class="form_label" style="width:61px;">厂区：</span>
                        <input id="stockList_factoryCode" name="factoryCode" type="text" style="width: calc(100% - 66px);" placeholder="厂区">
                    </label>
                </li>
                <li class="field-group field-fluid3">
                    <label class="inline" for="stockList_warehousecode" style="margin-right:20px;width:100%;">
                        <span class="form_label" style="width:61px;">仓库：</span>
                        <input id="stockList_warehousecode" name="warehousecode" type="text" style="width: calc(100% - 66px);" placeholder="仓库">
                    </label>
                </li>
            </ul>
            <div class="field-button" style="">
                <div class="btn btn-info" onclick="stockList_queryOk();">
                    <i class="ace-icon fa fa-check bigger-110"></i>查询
                </div>
                <div class="btn" onclick="stockList_reset();"><i class="ace-icon icon-remove"></i>重置</div>
                <a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>
            </div>
        </form>
    </div>
    </c:if>
    <div id="stockList_fixed_tool_div" class="fixed_tool_div">
        <div id="stockList_toolbar_" style="float:left;overflow:hidden;"></div>
    </div>
    <div style="overflow-x:auto"><table id="stockList_grid-table" style="width:100%;height:100%;"></table></div>
    <div id="stockList_grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
	var stockList_grid;
	var stockList_warehouseid_selectid,stockList_factory_selectid;
    var stockList_exportExcelIndex;
	
    //时间控件
    $(".date-picker").datetimepicker({format: "YYYY-MM-DD"});

    $("input").keypress(function (e) {
        if (e.which == 13) {
            stockList_queryOk();
        }
    });

    $(function  (){
        $(".toggle_tools").click();
    });

	$("#stockList_toolbar_").iToolBar({
	    id: "stockList_tb_01",
	    items: [
	        {label: "冻结", hidden:"${operationCode.webFreeze}"=="1",onclick: stockList_frozen, iconClass:'icon-stop'},
	        {label: "解冻", hidden:"${operationCode.webThaw}"=="1",onclick: stockList_unFrozen, iconClass:'icon-play'},
	        {label: "导出", hidden:"${operationCode.webExport}"=="1",onclick:stockList_toExcel, iconClass:'icon-share'},
            {label: "消库", hidden:"${operationCode.webQacodeChange}"=="1",onclick:stockList_exchangeqacode, iconClass:'icon-hdd'}
	   ]
	});

	var stockList_queryFormData = iTsai.form.serialize($("#stockList_queryForm"));

    stockList_grid = jQuery("#stockList_grid-table").jqGrid({
			url : context_path + "/storageInfo/storageListData",
		    datatype : "json",
		    colNames : [ "主键","质保单号","批次号","物料名称","厂区","仓库","库位","营销经理","订单号","订单行号","工单号","数量", "单位","盘类型","盘规格",
                "盘外径","盘内径","重量","颜色","段号","盘号","盘具编码","货物类型","状态"],
		    colModel : [ 
                {name : "id",index : "id",hidden:true},
                {name : "qaCode",index : "qaCode",width :160},
                {name : "batchNo",index : "batch_no",width :180},
                {name : "materialName",index : "materialName",width : 260},
                {name : "factoryName",index : "factoryName",width : 200},
                {name : "warehouseName",index : "warehouseName",width : 140},
                {name : "shelfCode",index : "shelfCode",width :140},
                {name : "salesName",index : "salesName",width :140},
                {name : "orderno",index : "orderno",width :130},
                {name : "orderline",index : "orderline",width : 60},
                {name : "entityNo",index : "entityNo",width : 100},
                {name : "meter",index : "meter",width : 60},
                {name : "unit",index : "unit",width :70},
                {name : "drumType",index : "drumType",width :70},
                {name : "model",index : "model",width :150},
                {name : "outerDiameter",index : "outerDiameter",width :70},
                {name : "innerDiameter",index : "innerDiameter",width :70},
                {name : "weight",index : "weight",width : 60},
                {name : "colour",index : "colour",width : 60},
                {name : "segmentno",index : "segmentno",width : 100},
                {name : "dishcode",index : "dishcode",width : 140},
                {name : "dishnumber",index : "dishnumber",width : 100},
                {name : "storageType",index : "storageType",width : 70,
                    formatter:function(cellValue,option,rowObject){
                    	if(cellValue==0){
                            return "正常生产";
                        }else if(cellValue==1){
                            return "销售退货";
                        }else if(cellValue==2){
                            return "外协采购";
                        }
                    }
                },
                {name : 'state',index : 'state',width : 70,
                     formatter:function(cellValu,option,rowObject){
                         return cellValu==1?"正常":cellValu==0?
                   		 "<span style='color:red;'>冻结（入库中）</span>": cellValu==2?
                         "<span style='color:red;'>锁定（出库中）</span>": cellValu==3?
                   		 "<span style='color:red;'>锁定（发货中）</span>": "";
                     }
                }
    		],
		    rowNum : 20,
		    rowList : [ 10, 20, 30 ],
		    pager : "#stockList_grid-pager",
		    sortname : "t.id",
		    sortorder : "asc",
            altRows: true,
            viewrecords : true,
            hidegrid:false,
     	    autowidth:false, 
            multiselect:true,
            multiboxonly: true,
            shrinkToFit:false,
            autoScroll: true,
            loadComplete : function(data){
            	var table = this;
            	setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            	$("#stockList_grid-table").closest(".ui-jqgrid-bdiv").css({ 'overflow-x': 'auto' });
            },
            emptyrecords: "没有相关记录",
            loadtext: "加载中...",
            pgtext : "页码 {0} / {1}页",
            recordtext: "显示 {0} - {1}共{2}条数据"
	});

	jQuery("#stockList_grid-table").navGrid("#stockList_grid-pager", {edit:false,add:false,del:false,search:false,refresh:false})
    .navButtonAdd("#stockList_grid-pager",{
		caption:"",   
		buttonicon:"fa fa-refresh green",   
		onClickButton: function(){   
			$("#stockList_grid-table").jqGrid("setGridParam", {
                postData: {queryJsonString:""} //发送数据
            }).trigger("reloadGrid");
		}
	}).navButtonAdd("#stockList_grid-pager",{
        caption: "",
        buttonicon:"fa icon-cogs",
        onClickButton : function (){
            jQuery("#stockList_grid-table").jqGrid("columnChooser",{
                done: function(perm, cols){
                    $("#stockList_grid-table").jqGrid("setGridWidth", $("#stockList_grid-div").width());
                    stockList_exportExcelIndex = perm;
                }
            });
        }
    });

	$(window).on("resize.jqGrid", function () {
		$("#stockList_grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);
		$("#stockList_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-10-
		$("#stockList_yy").outerHeight(true)-$("#stockList_fixed_tool_div").outerHeight(true)-
		$("#stockList_grid-pager").outerHeight(true)-$("#gview_stockList_grid-table .ui-jqgrid-hdiv").outerHeight(true));
	});
	
	$(window).triggerHandler("resize.jqGrid");
	
	//厂区
	$("#stockList_queryForm #stockList_factoryCode").select2({
	    placeholder: "选择厂区",
	    minimumInputLength:0,   //至少输入n个字符，才去加载数据
	    allowClear: true,  //是否允许用户清除文本信息
	    delay: 250,
	    formatNoMatches:"没有结果",
	    formatSearching:"搜索中...",
	    formatAjaxError:"加载出错啦！",
	    ajax : {
	        url: context_path+"/factoryArea/getFactoryList",
	        type:"POST",
	        dataType : 'json',
	        delay : 250,
	        data: function (term,pageNo) {     //在查询时向服务器端传输的数据
	            term = $.trim(term);
	            return {
	                queryString: term,    //联动查询的字符
	                pageSize: 15,    //一次性加载的数据条数
	                pageNo:pageNo    //页码
	            }
	        },
	        results: function (data,pageNo) {
	            var res = data.result;
	            if(res.length>0){   //如果没有查询到数据，将会返回空串
	                var more = (pageNo*15)<data.total; //用来判断是否还有更多数据可以加载
	                return {
	                    results:res,
                        more:more
	                };
	            }else{
	                return {
	                    results:{}
	                };
	            }
	        },
	        cache : true
	    }
	});
	
	//厂区变更
	$("#stockList_queryForm #stockList_factoryCode").on("change.select2",function(){
	    $("#stockList_queryForm #stockList_factoryCode").trigger("keyup");
	    $("#stockList_queryForm #stockList_warehousecode").select2("val","");
		//厂区编码
		stockList_factory_selectid = $("#stockList_queryForm #stockList_factoryCode").val();
	});
	
	//仓库变更
	$("#stockList_queryForm #stockList_warehousecode").on("change.select2",function(){
	    $("#stockList_queryForm #stockList_warehousecode").trigger("keyup");
		//仓库id
		stockList_warehouseid_selectid = $("#stockList_queryForm #stockList_warehousecode").val();
	});
	
	/*//库位
	$("#stockList_queryForm #stockList_shelfCode").select2({
        placeholder: "选择库位",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/goods/getShelfByFactoryWarehouse.do",
            type: "POST",
            dataType: 'json',
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                	warehouseId:stockList_warehouseid_selectid,
                	factoryCode:stockList_factory_selectid,
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo //页码
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });*/
	
	//解冻
	function stockList_unFrozen(){
		var checkedNum = getGridCheckedNum("#stockList_grid-table", "id");  //选中的数量
	    if (checkedNum == 0) {
	    	layer.alert("请选择一条要解冻的库存！");
	    } else {
			layer.confirm("确定解冻所选库存？", function() {
				$.ajax({
	    			type : "POST",
	    			url : context_path + "/storageInfo/unFrozen?ids="+jQuery("#stockList_grid-table").jqGrid("getGridParam", "selarrrow") ,
	    			dataType : "json",
	    			cache : false,
	    			success : function(data) {
	    				layer.closeAll();
	    				if(data.result){
							layer.msg(data.msg,{icon:1,time:1200});
						}else{
							layer.msg(data.msg,{icon:2,time:1200});
						}
                        stockList_grid.trigger("reloadGrid");
	    			}
	    		});
			});
	    }
	}
	
	//冻结
	function stockList_frozen(){
		var checkedNum = getGridCheckedNum("#stockList_grid-table", "id");  //选中的数量
	    if (checkedNum == 0) {
	    	layer.alert("请选择一条要冻结的库存！");
	    } else {
			layer.confirm("确定冻结所选库存？", function() {
				$.ajax({
	    			type : "POST",
	    			url : context_path + "/storageInfo/frozen?ids="+jQuery("#stockList_grid-table").jqGrid("getGridParam", "selarrrow") ,
	    			dataType : "json",
	    			cache : false,
	    			success : function(data) {
	    				layer.closeAll();
	    				if(data.result){
							layer.msg(data.msg,{icon:1,time:1200});
						}else{
							layer.msg(data.msg,{icon:2,time:1200});
						}
                        stockList_grid.trigger("reloadGrid");
	    			}
	    		});
			});
	    }
	}

    //消库
    function stockList_exchangeqacode(){
        var checkedNum = getGridCheckedNum("#stockList_grid-table", "id");  //选中的数量
        if (checkedNum == 0) {
            layer.alert("请选择要删除的库存！");
        } else {
            layer.confirm("确定删除所选库存？", function() {
                $.ajax({
                    type : "POST",
                    url : context_path + "/storageInfo/removeStorageInfo?ids="+jQuery("#stockList_grid-table").jqGrid("getGridParam", "selarrrow") ,
                    dataType : "json",
                    cache : false,
                    success : function(data) {
                        layer.closeAll();
                        if(data.result){
                            layer.msg(data.msg,{icon:1,time:1200});
                        }else{
                            layer.msg(data.msg,{icon:2,time:1200});
                        }
                        stockList_grid.trigger("reloadGrid");
                    }
                });
            });
        }
    }

	/**
	 * 查询按钮点击事件
	 */
	function stockList_queryOk(){
		 var queryParam = iTsai.form.serialize($("#stockList_queryForm"));
		 //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
		 stockList_queryByParam(queryParam);
	}
	
	//查询
	function stockList_queryByParam(jsonParam) {
	    iTsai.form.deserialize($("#stockList_hiddenQueryForm"), jsonParam);
	    var queryParam = iTsai.form.serialize($("#stockList_hiddenQueryForm"));
	    var queryJsonString = JSON.stringify(queryParam); 
	    $("#stockList_grid-table").jqGrid("setGridParam", {
            postData: {queryJsonString: queryJsonString}
        }).trigger("reloadGrid");
	}
	
	//重置
	function stockList_reset(){
		 stockList_factory_selectid='';
		 stockList_warehouseid_selectid='';
		 $("#stockList_queryForm #stockList_factoryCode").select2("val","");
		 $("#stockList_queryForm #stockList_warehousecode").select2("val","");
		 iTsai.form.deserialize($("#stockList_queryForm"),stockList_queryFormData);
		 stockList_queryByParam(stockList_queryFormData);
	}
	
	//导出Excel
	function stockList_toExcel(){
	    $("#stockList_hiddenForm #stockList_ids").val(jQuery("#stockList_grid-table").jqGrid("getGridParam", "selarrrow"));
        $("#stockList_hiddenForm #stockList_queryFactoryCode").val($("#stockList_queryForm #stockList_factoryCode").val());
        $("#stockList_hiddenForm #stockList_queryWarehousecode").val($("#stockList_queryForm #stockList_warehousecode").val());
        $("#stockList_hiddenForm #stockList_queryShelfCode").val($("#stockList_queryForm #stockList_shelfCode").val());
        $("#stockList_hiddenForm #stockList_queryMaterialName").val($("#stockList_queryForm #stockList_materialname").val());
        $("#stockList_hiddenForm #stockList_queryQaCode").val($("#stockList_queryForm #stockList_qaCode").val());
        $("#stockList_hiddenForm #stockList_queryBatchNo").val($("#stockList_queryForm #stockList_batchNo").val());
        $("#stockList_hiddenForm #stockList_queryExportExcelIndex").val(stockList_exportExcelIndex);
	    $("#stockList_hiddenForm").submit();
	}
	
	//选择仓库
	$("#stockList_queryForm #stockList_warehousecode").select2({
        placeholder: "选择仓库",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/warehouselist/getWarehouseByFactory",
            type: "POST",
            dataType: 'json',
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                	factoryCode : stockList_factory_selectid,
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo //页码
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });
	
    $("#stockList_queryForm .mySelect2").select2();   
</script>