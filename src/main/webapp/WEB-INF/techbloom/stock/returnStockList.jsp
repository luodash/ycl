<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<style type="text/css">
.background { 
    display: block; 
    width: 100%; 
    height: 100%; 
    opacity: 0.4; 
    filter: alpha(opacity=50); 
    background:white;
    position: absolute; 
    top: 0; 
    left: 0; 
    z-index: 2000; 
} 
.progressBar { 
    border: solid 2px #86A5AD; 
    background: white url(${pageContext.request.contextPath}/static/image/progressBar_m.gif) no-repeat 10px 10px; 
} 
.progressBar { 
    display: block; 
    width: 160px; 
    height: 28px; 
    position: fixed; 
    top: 50%; 
    left: 50%; 
    margin-left: -74px; 
    margin-top: -14px; 
    padding: 10px 10px 10px 50px; 
    text-align: left; 
    line-height: 27px; 
    font-weight: bold; 
    position: absolute; 
    z-index: 2001; 
} 
</style>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="returnStockList_background" class="background" style="display: none; "></div> 
<div id="returnStockList_progressBar" class="progressBar" style="display: none; ">数据加载中，请稍等...</div> 
<div id="returnStockList_grid-div">
    <form id="returnStockList_hiddenForm" action="<%=path%>/storageInfo/toExcel.do" method="POST" style="display: none;">
        <input id="returnStockList_ids" name="ids" value=""/>
    </form>
    <form id="returnStockList_hiddenQueryForm" style="display:none;">
        <input  name="factoryCode" value="">
        <input  name="qaCode" value=""/>
        <input  name="batchNo" value="">
        <input  name="orderno" value="">
        <input  name="batchNo" value="">
        <input  name="wokrshop" value="">

    </form>
     <c:if test="${operationCode.webSearch==1}">
    <div class="query_box" id="returnStockList_yy" title="查询选项">
        <form id="returnStockList_queryForm" style="max-width:100%;">
            <ul class="form-elements">
                <li class="field-group field-fluid3">
                    <label class="inline" for="returnStockList_qacode" style="margin-right:10px;width: 100%;">
                        <span class="form_label" style="width:80px;">质保号：</span>
                        <input type="text" id="returnStockList_qacode"  name="qaCode" value="" style="width: calc(100% - 85px);" placeholder="质保号" />
                    </label>
                </li>
                <li class="field-group field-fluid3">
                    <label class="inline" for="returnStockList_batchNo" style="margin-right:20px;width:100%;">
                        <span class="form_label" style="width:80px;">批次号：</span>
                        <input id="returnStockList_batchNo" name="batchNo" type="text" style="width: calc(100% - 85px);" placeholder="批次号">
                    </label>
                </li>
                <li class="field-group field-fluid3">
					<label class="inline" for="returnStockList_factoryCode" style="margin-right:20px;width: 100%;">
						 <span class="form_label" style="width:80px;">厂区：</span>
						 <input id="returnStockList_factoryCode" name="factoryCode" type="text" style="width: calc(100% - 85px);" placeholder="厂区">
					</label>
				</li>
				
				<li class="field-group-top field-group field-fluid3">
                    <label class="inline" for="returnStockList_orderno" style="margin-right:20px;width:100%;">
                        <span class="form_label" style="width:80px;">订单号：</span>
                        <input type="text" id="returnStockList_orderno"   name="orderno" value="" style="width: calc(100% - 85px);" placeholder="订单号" />
                    </label>
                </li>
                <li class="field-group field-fluid3">
                    <label class="inline" for="returnStockList_wokrshop" style="margin-right:20px;width:100%;">
                        <span class="form_label" style="width:80px;">车间：</span>
                        <input type="text" id="returnStockList_wokrshop"  name="wokrshop" value="" style="width: calc(100% - 85px);" placeholder="车间" />
                    </label>
                </li>
            </ul>
            <div class="field-button" style="">
                <div class="btn btn-info" onclick="returnStockList_queryOk();">
                    <i class="ace-icon fa fa-check bigger-110"></i>查询
                </div>
                <div class="btn" onclick="returnStockList_reset();"><i class="ace-icon icon-remove"></i>重置</div>
                <a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>
            </div>
        </form>
    </div>
     </c:if>
    <div id="returnStockList_fixed_tool_div" class="fixed_tool_div">
        <div id="returnStockList_toolbar_" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="returnStockList_grid-table" style="width:100%;height:100%;"></table>
    <div id="returnStockList_grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
	var returnStockList_oriData;
	var returnStockList_grid;

    $("input").keypress(function (e) {
        if (e.which == 13) {
            returnStockList_queryOk();
        }
    });

    //时间控件
    $(".date-picker").datetimepicker({format: "YYYY-MM-DD"});

    $(function  (){
        $(".toggle_tools").click();
    });

	$("#returnStockList_toolbar_").iToolBar({
	    id: "returnStockList_tb_01",
	    items: [
	    	{label: "退库",hidden:"${operationCode.webReturnStorage}"=="1", onclick:returnStockList_outStorage, iconClass:'icon-step-backward'}
	   ]
	});

	var returnStockList_queryForm_data = iTsai.form.serialize($("#returnStockList_queryForm"));

    returnStockList_grid = jQuery("#returnStockList_grid-table").jqGrid({
			url : context_path + "/storageInfo/returnStockListData",
		    datatype : "json",
		    colNames : [ "主键","质保号", "批次号","厂区","车间","订单号","订单行号","盘具编码","数量","单位","盘具规格", "颜色","重量","段号","类型"],
		    colModel : [ 
                {name : "id",index : "id",hidden:true},
                {name : "qaCode",index : "qaCode",width :140},
                {name : "batchNo",index : "BATCH_NO",width : 140},
                {name : "factoryName",index : "factoryName",width :180},
                {name : "workshopname",index : "workshopname",width :100},
                {name : "orderno",index : "orderno",width :120},
                {name : "orderline",index : "orderline",width :60},
                {name : "dishnumber",index : "dishnumber",width : 100},
                {name : "meter",index : "meter",width : 60},
                {name : "unit",index : "unit",width : 60},
                {name : "model",index : "model",width : 160},
                {name : "colour",index : "colour",width : 60},
                {name : "weight",index : "weight",width :70},
                {name : "segmentno",index : "segmentno",width :70},
                {name : "type",index : "type",hidden:true}
    		],
		    rowNum : 20,
		    rowList : [ 10, 20, 30 ],
		    pager : "#returnStockList_grid-pager",
		    sortname : "t.id",
		    sortorder : "asc",
            altRows: true,
            viewrecords : true,
            hidegrid:false,
     	    autowidth:false,
            multiselect:true,
            multiboxonly: true,
            shrinkToFit:false,
            autoScroll: true,
            loadComplete : function(data){
            	var table = this;
            	setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
                returnStockList_oriData = data;
                $("#returnStockList_grid-table").closest(".ui-jqgrid-bdiv").css({ 'overflow-x': 'auto' });
            },
            emptyrecords: "没有相关记录",
            loadtext: "加载中...",
            pgtext : "页码 {0} / {1}页",
            recordtext: "显示 {0} - {1}共{2}条数据"
	});

	jQuery("#returnStockList_grid-table").navGrid("#returnStockList_grid-pager",
        {edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#returnStockList_grid-pager",{
		caption:"",   
		buttonicon:"fa fa-refresh green",   
		onClickButton: function(){   
			$("#returnStockList_grid-table").jqGrid("setGridParam", 
				{
			      postData: {queryJsonString:""} //发送数据 
				}
			).trigger("reloadGrid");
		}
	}).navButtonAdd("#returnStockList_grid-pager",{
        caption: "",
        buttonicon:"fa icon-cogs",
        onClickButton : function (){
            jQuery("#returnStockList_grid-table").jqGrid("columnChooser",{
                done: function(perm, cols){
                    $("#returnStockList_grid-table").jqGrid("setGridWidth", $("#returnStockList_grid-div").width());
                }
            });
        }
    });

	$(window).on("resize.jqGrid", function () {
		$("#returnStockList_grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);
		$("#returnStockList_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-10-
		$("#returnStockList_yy").outerHeight(true)-$("#returnStockList_fixed_tool_div").outerHeight(true)-
		$("#returnStockList_grid-pager").outerHeight(true)-$("#gview_returnStockList_grid-table .ui-jqgrid-hdiv").outerHeight(true));
	});
	
	$(window).triggerHandler("resize.jqGrid");
	
	//厂区
	$("#returnStockList_queryForm #returnStockList_factoryCode").select2({
	    placeholder: "选择厂区",
	    minimumInputLength:0,   //至少输入n个字符，才去加载数据
	    allowClear: true,  //是否允许用户清除文本信息
	    delay: 250,
	    formatNoMatches:"没有结果",
	    formatSearching:"搜索中...",
	    formatAjaxError:"加载出错啦！",
	    ajax : {
	        url: context_path+"/factoryArea/getFactoryList",
	        type:"POST",
	        dataType : 'json',
	        delay : 250,
	        data: function (term,pageNo) {     //在查询时向服务器端传输的数据
	            term = $.trim(term);
	            return {
	                queryString: term,    //联动查询的字符
	                pageSize: 15,    //一次性加载的数据条数
	                pageNo:pageNo    //页码
	            }
	        },
	        results: function (data,pageNo) {
	            var res = data.result;
	            if(res.length>0){   //如果没有查询到数据，将会返回空串
	                var more = (pageNo*15)<data.total; //用来判断是否还有更多数据可以加载
	                return {
	                    results:res,more:more
	                };
	            }else{
	                return {
	                    results:{}
	                };
	            }
	        },
	        cache : true
	    }
	});

	//退库回生产线
	function returnStockList_outStorage(){
        //选中的数量
		var checkedNum = getGridCheckedNum("#returnStockList_grid-table", "id");
	    if (checkedNum == 0) {
	    	layer.alert("请选择一条要退库的库存！");
	    }else if(checkedNum > 1){
	    	layer.alert("只能选择一条要出库的库存！");
	    } else {
			var id=$('#returnStockList_grid-table').jqGrid('getGridParam','selrow');
			var rowData = $("#returnStockList_grid-table").jqGrid('getRowData',id);
			var ajaxbg = $("#returnStockList_background,#returnStockList_progressBar");
			layer.confirm("确定退库吗？", function() {
	    		layer.closeAll();
				$.ajax({
	    			type : "POST",
	    			url : context_path + "/storageInfo/returnStorage?id="+id+"&type="+rowData.type ,
	    			dataType : "json",
	    			cache : false,
	    			beforeSend:function() {
	                    ajaxbg.show(); 
	                },
	    			success : function(data) {
	    				ajaxbg.hide(); 
	    				if(data.result){
							layer.msg(data.msg,{icon:1,time:1200});
						}else{
							layer.msg(data.msg,{icon:2,time:1200});
						}
                        returnStockList_grid.trigger("reloadGrid");
	    			},
	    			 error:function(){
	                     ajaxbg.hide(); 
	                     layer.alert("出库失败！");
	                 }
	    		});
			});
	    }
	}

	/**
	 * 查询按钮点击事件
	 */
	function returnStockList_queryOk(){
		 var queryParam = iTsai.form.serialize($("#returnStockList_queryForm"));
		 //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
		 returnStockList_queryByParam(queryParam);
	}
	
	//查询
	function returnStockList_queryByParam(jsonParam) {
	    iTsai.form.deserialize($("#returnStockList_hiddenQueryForm"), jsonParam);
	    var queryParam = iTsai.form.serialize($("#returnStockList_hiddenQueryForm"));
	    var queryJsonString = JSON.stringify(queryParam); 
	    $("#returnStockList_grid-table").jqGrid("setGridParam",
	        {
	            postData: {queryJsonString: queryJsonString}
	        }
	    ).trigger("reloadGrid");
	}
	
	//重置
	function returnStockList_reset(){
        $("#returnStockList_queryForm #returnStockList_factoryCode").select2("val","");
		 iTsai.form.deserialize($("#returnStockList_queryForm"),returnStockList_queryForm_data); 
		 returnStockList_queryByParam(returnStockList_queryForm_data);
	}
	
	//导出Excel
	function toExcel(){
	    $("#returnStockList_hiddenForm #returnStockList_ids").val(jQuery("#returnStockList_grid-table").jqGrid("getGridParam", "selarrrow"));
	    $("#returnStockList_hiddenForm").submit();	
	}
	
    $("#returnStockList_queryForm .mySelect2").select2();   
   
</script>