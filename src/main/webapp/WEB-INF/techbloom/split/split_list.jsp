<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<style type="text/css">
.background { 
    display: block; 
    width: 100%; 
    height: 100%; 
    opacity: 0.4; 
    filter: alpha(opacity=50); 
    background:white;
    position: absolute; 
    top: 0; 
    left: 0; 
    z-index: 2000; 
} 
.progressBar { 
    border: solid 2px #86A5AD; 
    background: white url(${pageContext.request.contextPath}/static/image/progressBar_m.gif) no-repeat 10px 10px; 
} 
.progressBar { 
    display: block; 
    width: 160px; 
    height: 28px; 
    position: fixed; 
    top: 50%; 
    left: 50%; 
    margin-left: -74px; 
    margin-top: -14px; 
    padding: 10px 10px 10px 50px; 
    text-align: left; 
    line-height: 27px; 
    font-weight: bold; 
    position: absolute; 
    z-index: 2001; 
} 
</style>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="split_list_background" class="background" style="display: none; "></div> 
<div id="split_list_progressBar" class="progressBar" style="display: none; ">数据加载中，请稍等...</div> 
<div id="split_list_grid-div">
    <form id="split_list_hiddenForm" action="<%=path%>/storageInfo/toExcel.do" method="POST" style="display: none;">
        <input id="split_list_ids" name="ids" value=""/>
    </form>
    <form id="split_list_hiddenQueryForm" style="display:none;">
        <input name="qaCode" value=""/>
    </form>
     <c:if test="${operationCode.webSearch==1}">
    <div class="query_box" id="split_list_yy" title="查询选项">
        <form id="split_list_queryForm" style="max-width:100%;">
            <ul class="form-elements">
                <li class="field-group field-fluid3">
                    <label class="inline" for="split_list_qacode" style="margin-right:10px;width: 100%;">
                        <span class="form_label" style="width:80px;">质保号：</span>
                        <input type="text" id="split_list_qacode"  name="qaCode" value="" style="width: calc(100% - 85px);" placeholder="质保号" />
                    </label>
                </li>
            </ul>
            <div class="field-button" style="">
                <div class="btn btn-info" onclick="split_list_queryOk();">
                    <i class="ace-icon fa fa-check bigger-110"></i>查询
                </div>
                <div class="btn" onclick="split_list_reset();"><i class="ace-icon icon-remove"></i>重置</div>
                <%--<a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>--%>
            </div>
        </form>
    </div>
     </c:if>
    <div id="split_list_fixed_tool_div" class="fixed_tool_div">
        <div id="split_list_toolbar_" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="split_list_grid-table" style="width:100%;height:100%;"></table>
    <div id="split_list_grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
	var split_list_oriData;
	var split_list_grid;

    $("input").keypress(function (e) {
        if (e.which == 13) {
            split_list_queryOk();
        }
    });

    //时间控件
    $(".date-picker").datetimepicker({format: "YYYY-MM-DD"});

    $(function  (){
        $(".toggle_tools").click();
    });

	var split_list_queryForm_data = iTsai.form.serialize($("#split_list_queryForm"));

    split_list_grid = jQuery("#split_list_grid-table").jqGrid({
			url : context_path + "/split/list",
		    datatype : "json",
		    colNames : [ "主键","质保号", "批次号","物料编码","厂区","仓库","库位","数量","盘具编码", "盘号"],
		    colModel : [ 
                {name : "id",index : "id",hidden:true},
                {name : "qaCode",index : "qacode",width :180},
                {name : "batchNo",index : "batch_no",width : 180},
                {name : "materialCode",index : "materialcode",width :180},
                {name : "factoryName",index : "org",width :140},
                {name : "warehouseName",index : "warehousecode",width :140},
                {name : "shelfCode",index : "shelfcode",width :100},
                {name : "meter",index : "meter",width : 100},
                {name : "dishnumber",index : "dishnumber",width : 160},
                {name : "outerdiameter",index : "outerdiameter",width : 160}
    		],
		    rowNum : 20,
		    rowList : [ 10, 20, 30 ],
		    pager : "#split_list_grid-pager",
		    sortname : "t.id",
		    sortorder : "asc",
            altRows: true,
            viewrecords : true,
            hidegrid:false,
     	    autowidth:false,
            multiselect:true,
            multiboxonly: true,
            shrinkToFit:false,
            autoScroll: true,
            loadComplete : function(data){
            	var table = this;
            	setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
                split_list_oriData = data;
                $("#split_list_grid-table").closest(".ui-jqgrid-bdiv").css({ 'overflow-x': 'auto' });
            },
            emptyrecords: "没有相关记录",
            loadtext: "加载中...",
            pgtext : "页码 {0} / {1}页",
            recordtext: "显示 {0} - {1}共{2}条数据"
	});

	jQuery("#split_list_grid-table").navGrid("#split_list_grid-pager", {edit:false,add:false,del:false,search:false,refresh:false})
    .navButtonAdd("#split_list_grid-pager",{
		caption:"",   
		buttonicon:"fa fa-refresh green",   
		onClickButton: function(){   
			$("#split_list_grid-table").jqGrid("setGridParam", {
              postData: {queryJsonString:""} //发送数据
            }).trigger("reloadGrid");
		}
	}).navButtonAdd("#split_list_grid-pager",{
        caption: "",
        buttonicon:"fa icon-cogs",
        onClickButton : function (){
            jQuery("#split_list_grid-table").jqGrid("columnChooser",{
                done: function(perm, cols){
                    $("#split_list_grid-table").jqGrid("setGridWidth", $("#split_list_grid-div").width());
                }
            });
        }
    });

	$(window).on("resize.jqGrid", function () {
		$("#split_list_grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);
		$("#split_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-10-
		$("#split_list_yy").outerHeight(true)-$("#split_list_fixed_tool_div").outerHeight(true)-
		$("#split_list_grid-pager").outerHeight(true)-$("#gview_split_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
	});
	
	$(window).triggerHandler("resize.jqGrid");
	
	/**
	 * 查询按钮点击事件
	 */
	function split_list_queryOk(){
		 var queryParam = iTsai.form.serialize($("#split_list_queryForm"));
		 //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
		 split_list_queryByParam(queryParam);
	}
	
	//查询
	function split_list_queryByParam(jsonParam) {
	    iTsai.form.deserialize($("#split_list_hiddenQueryForm"), jsonParam);
	    var queryParam = iTsai.form.serialize($("#split_list_hiddenQueryForm"));
	    var queryJsonString = JSON.stringify(queryParam); 
	    $("#split_list_grid-table").jqGrid("setGridParam", {
            postData: {queryJsonString: queryJsonString}
        }).trigger("reloadGrid");
	}
	
	//重置
	function split_list_reset(){
        $("#split_list_queryForm #split_list_factoryCode").select2("val","");
		 iTsai.form.deserialize($("#split_list_queryForm"),split_list_queryForm_data); 
		 split_list_queryByParam(split_list_queryForm_data);
	}
	
    $("#split_list_queryForm .mySelect2").select2();
</script>