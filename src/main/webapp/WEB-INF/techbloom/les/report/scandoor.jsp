<%--
  Created by IntelliJ IDEA.
  User: HQKS-004-05
  Date: 2017/12/20
  Time: 10:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String context_path = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<%-- <%@include file="/techbloom/common/taglibs.jsp" %> --%>
<!-- jqgrid表格样式 -->
<link rel="stylesheet" href="<%=path%>/plugins/public_components/css/ui.jqgrid.css" />
<link rel="stylesheet" href="<%=path%>/plugins/public_components/css/myjqgrid.css" />
<script src="<%=path%>/plugins/public_components/js/jquery-2.1.4.js"></script> 
<script src="<%=path%>/plugins/public_components/js/jquery.jqGrid.src.js"></script>  <!-- jqgrid表格js -->
<script src="<%=path%>/plugins/public_components/js/grid.locale-cn.js"></script>
<script src="<%=path%>/static/techbloom/main/common.js"></script>


<script type="text/javascript" >
    var context_path='<%=context_path%>';
</script>
<style type="text/css">
    body {
        backgound-color: #FFFFFF;
        overflow: hidden;
    }
</style>
<div class="row-fluid" id="grid-div"
     style="position:relative;margin-top: 0px;">
    <div style="
    margin: 10px;
    display: flex;
	"><img src="<%=context_path%>/plugins/public_components/img/sm.png"><h1 style="
	    line-height: 20px;
	    margin-left: 5px;
	    color: #00468e;
	    font-size: 27px;
	">LES-扫描门</h1></div>
    <!-- 工具栏 -->
    <div class="row-fluid" id="table_toolbar" style="padding:5px 3px;" >
        <form id="query_form" class="form-horizontal" style="width: calc(100% - 80px)">
            <input id = "types" type="hidden" name = "types" value="${type}">
            <!--             <span  style="float: left;width: 45%;min-width: 80px;position: relative;"><input class="span12" type="text" name="routeName" id="routeName" placeholder="工位" style="min-height: 28px; border-right: 0px;"/><i class="icon-question-sign" style="position: absolute;top: 8px;right: 8px;color: #999;"></i></span> -->
            <!--             <span  style="float: left;width: 45%;min-width: 80px;position: relative;"><input class="span12" type="text" name="shevelName" id="shevelName" placeholder="货架名称" style="min-height: 28px; border-right: 0px;"/><i class="icon-question-sign" style="position: absolute;top: 8px;right: 8px;color: #999;"></i></span> -->

        </form>
        <!-- 在按钮上都添加上btn-addQX用来标记列表中的操作按钮，方便控制按钮的权限 -->
        <!--         <button class="btn btn-primary btn-queryQx" onclick="openLogQueryPage();"> -->
        <!--             查询<i class="fa fa-search" aria-hidden="true" style="margin-left:5px;"></i> -->
        <!--         </button> -->
    </div>
    <!-- 表格 -->
    <div class="row-fluid" style="padding:0 3px;">
        <!-- 表格数据 -->
        <table id="grid-table" style="width:100%;"></table>
        <!-- 表格底部 -->
        <div id="grid-pager"></div>
    </div>
</div>
<script type="text/javascript" src="<%=request.getContextPath()%>/static/js/techbloom/les/report/scandoor.js"></script>
