<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="grid-div">
    <form id="hiddenForm" action="<%=path%>/productTrack/toExcel" method="post" style="display: none;">
        <input id="ids" name="ids" value=""/>
    </form>
    <!-- 隐藏区域：存放查询条件 -->
    <form id="hiddenQueryForm" style="display:none;"/>
        <input id="processName" name="processName" type="hidden" />
        <input id="orderName" name="orderName" type="hidden">
        <input id="productName" name="productName" type="hidden" />
        <input id="batchNo" name="batchNo" type="hidden" />
        <input id="rfid" name="rfid" type="hidden" />
    </form>   
     <div class="query_box" id="yy" title="查询选项">
            <form id="queryForm" style="max-width:100%;">
			 <ul class="form-elements">
				<li class="field-group field-fluid3">
					<label class="inline" for="processName" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:80px;">路径名称：</span>
						<input id="processName" name="processName" type="text" style="width:calc(100% - 85px);" placeholder="路径名称" />
					</label>			
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="orderName" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:80px;">订单编号：</span>
						<input id="orderName" name="orderName" type="text" style="width:calc(100% - 85px);" placeholder="订单编号" />
					</label>				
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="productName" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:80px;">产品：</span>
						<input id="productName" name="productName" type="text" style="width:calc(100% - 85px);" placeholder="产品" />
					</label>			
				</li>
				<li class="field-group-top field-group field-fluid3">
					<label class="inline" for="batchNo" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:80px;">批次号：</span>
						<input type="text" id="batchNo" name="batchNo" style="width:calc(100% - 85px);" placeholder="批次号" />
					</label>				
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="rfid" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:80px;">标签：</span>
						<input type="text" id="rfid" name="rfid" style="width:calc(100% - 85px);" placeholder="标签" />
					</label>				
				</li>
				
			</ul>
			<div class="field-button" style="">
					<div class="btn btn-info" onclick="queryOk();">
				        <i class="ace-icon fa fa-check bigger-110"></i>查询
			        </div>
					<div class="btn" onclick="reset();"><i class="ace-icon icon-remove"></i>重置</div>
					<a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>
		        </div>
		  </form>		 
    </div>
    <div id="fixed_tool_div" class="fixed_tool_div">
        <div id="__toolbar__" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="grid-table" style="width:100%;margin: 0px !important;boder:0px !important"></table>
    <div id="grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
var context_path = '<%=path%>';
var oriData;
var _grid;
$(function  (){
    $(".toggle_tools").click();
});
$("#__toolbar__").iToolBar({
    id: "__tb__01",
    items: [
            {label: "删除", disabled: ( ${sessionUser.deleteQx} == 1 ? false : true),onclick: delTrack, iconClass:'glyphicon glyphicon-trash'},
            {label: "导出", disabled: ( ${sessionUser.queryQx}==1?false:true),onclick:function(){toExcel();},iconClass:' icon-share'}
           ]
});
$(function () {
    _grid = jQuery("#grid-table").jqGrid({
            url: context_path + "/productTrack/list.do",
            datatype: "json",
            colNames: ["主键", "产品名称", "产品编号", "订单名称",  "生产时间","流程主键", "所属路径", "标签", "状态","操作"],
            colModel: [
                {name: "id", index: "id", width: 20, hidden: true},
                {name: "productName", index: "productName", width: 60},
                {name: "productNo",index: "productNo",width:60},
                {name: "orderName",index: "orderName",width:65},
               // {name: "batchNo", index: "batchNo", width: 60},
                {name: "productionTime", index: "productionTime", width: 60},
                {name: "processId", index: "processId", width: 20, hidden: true},
                {name: "processName", index: "processName", width: 80},
                {name: "rfid", index: "rfid", width: 100},
                {name: "statusName", index: "statusName", width: 135},
                {name: "operation", index: "operation",
                    formatter: function (cellValu, option, rowObject) {
                        return "<div style='margin-bottom:5px' class='btn btn-xs btn-success' onclick='echoTimeBase(" + rowObject.id + ")'>详情</div>"
                    }
                }
            ],
            rowNum: 20,
            rowList: [10, 20, 30],
            pager: "#grid-pager",
            sortname: "pk.productionTime",
            sortorder: "desc",
            altRows: true,
            viewrecords: true,
            autowidth: true,
            multiselect: true,
            multiboxonly: true,
            loadComplete: function (data) {
                var table = this;
                setTimeout(function () {
                    updatePagerIcons(table);
                    enableTooltips(table);
                }, 0);
                oriData = data;
            },
            emptyrecords: "没有相关记录",
            loadtext: "加载中...",
            pgtext: "页码 {0} / {1}页",
            recordtext: "显示 {0} - {1}共{2}条数据"
        });
        //在分页工具栏中添加按钮
        jQuery("#grid-table").navGrid("#grid-pager", {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false
        }).navButtonAdd("#grid-pager", {
                    caption: "",
                    buttonicon: "ace-icon fa fa-refresh green",
                    onClickButton: function () {
                        $("#grid-table").jqGrid("setGridParam",
                                {
                                    postData: {queryJsonString: ""} //发送数据
                                }
                        ).trigger("reloadGrid");
                    }
                });
$(window).on("resize.jqGrid", function () {
            $("#grid-table").jqGrid("setGridWidth", $("#grid-div").width() );
            $("#grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-$("#yy").outerHeight(true)-$("#fixed_tool_div").outerHeight(true)-$("#grid-pager").outerHeight(true)
           -$("#gview_grid-table .ui-jqgrid-hdiv").outerHeight(true));
            });
        $(window).triggerHandler("resize.jqGrid");
});
var _queryForm_data = iTsai.form.serialize($("#queryForm"));
function queryOk(){
   var queryParam = iTsai.form.serialize($("#queryForm"));
   queryTrackListByParam(queryParam);		
}
function queryTrackListByParam(jsonParam){
        iTsai.form.deserialize($("#hiddenQueryForm"), jsonParam);
	    var queryParam = iTsai.form.serialize($("#hiddenQueryForm"));
	    var queryJsonString = JSON.stringify(queryParam); 
	    $("#grid-table").jqGrid("setGridParam",
	        {
	            postData: {queryJsonString: queryJsonString}
	        }
	    ).trigger("reloadGrid");
}
function reset(){
       $("#queryForm #processName").val("");
       $("#queryForm #orderName").val("");
       $("#queryForm #productName").val("");
       $("#queryForm #batchNo").val("");
       $("#queryForm #rfid").val("");       
	    $("#grid-table").jqGrid("setGridParam",
	        {
	            postData: {queryJsonString: ""}
	        }
	    ).trigger("reloadGrid");		
}
function delTrack(){
	 var checkedNum = getGridCheckedNum("#grid-table", "id");  //选中的数量
	    if (checkedNum == 0) {
	    	layer.alert("请选择一个要删除的追踪记录！");
	    } else {
	        var ids = jQuery("#grid-table").jqGrid("getGridParam", "selarrrow");
	        layer.confirm("确定删除选中的追踪记录？", function() {
	    		$.ajax({
	    			type : "POST",
	    			url : context_path + "/productTrack/deleteTrack.do?ids="+ids ,
	    			dataType : "json",
	    			cache : false,
	    			success : function(data) {
	    				layer.closeAll();
	    				if (Boolean(data.result)) {
	    					layer.msg(data.msg, {icon: 1,time:1000});
	    				}else{
	    					layer.msg(data.msg, {icon: 7,time:1000});    					
	    				}
	    				_grid.trigger("reloadGrid");  //重新加载表格
	    			}
	    		});
	    	});
	        
	    }  
}
function toExcel(){
    var ids = jQuery("#grid-table").jqGrid("getGridParam", "selarrrow");
    $("#hiddenForm #ids").val(ids);
    $("#hiddenForm").submit();	
}
 /**
     * 查看时间轴
     */
    function echoTimeBase(processInstance) {
        layer.open({
            skin: "layui-layer-molv",
            type: 2,
            title: '<img src="' + context_path + '/plugins/public_components/img/process.png" style=" margin-right: 2px;">产品追踪',
            shadeClose: true,
            shade: 0.8,
            area: ["780px", "520px"],
            content: context_path + "/productTrack/toTrackDetail?modelId=" + processInstance
        })
    };
    var interval = setInterval("refrash()",3000);
    page_interval.push( interval );
    function refrash(){
            $("#grid-table").jqGrid("setGridParam",
                {
                    postData: {queryJsonString: ""} //发送数据
                }
            ).trigger("reloadGrid");

	}
</script>
</html>