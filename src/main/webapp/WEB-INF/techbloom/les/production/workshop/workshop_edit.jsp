<%--
  Created by IntelliJ IDEA.
  User: HQKS-004-05
  Date: 2017/12/11
  Time: 16:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
    String path = request.getContextPath();
%>
<div id="shop_edit_page" class="row-fluid" style="height: inherit;">
    <form id="workshopForm" class="form-horizontal" style="overflow: auto; height: calc(100% - 70px);">
        <input type="hidden" id="shopid" name="id" value="${workshop.id}">
        <div class="control-group">
            <label class="control-label" for="workshopNo">车间编号：</label>
            <div class="controls">
                <div class="input-append span12 required">
                    <input type="text" class="span11" id="workshopNo" name="workshopNo" readonly="readonly" value="${workshop.workshopNo}" placeholder="车间编号">
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="workshopName">车间名称：</label>
            <div class="controls">
                <div class="input-append span12 required">
                    <input type="text" class="span11" id="workshopName" name="workshopName" value="${workshop.workshopName}" placeholder="车间名称">
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="remark">备&nbsp;&nbsp;注：</label>
            <div class="controls">
                <textarea type="text" class="span11" rows="3" cols="20" id="remark" name="remark" placeholder="备注">${workshop.remark}</textarea>
            </div>
        </div>
    </form>
    <div class="field-button" style="text-align: center;border-top: 0px;margin: 15px auto;">
        <span class="btn btn-info" onclick="saveForm();">
            <i class="ace-icon fa fa-check bigger-110"></i>保存
        </span>
        <span class="btn btn-danger" onclick="layer.closeAll();">
            <i class="icon-remove"></i>&nbsp;取消
        </span>
    </div>
</div>
<script type="text/javascript">
$("#workshopForm").validate({
        rules:{
            "workshopName":{
                required:true,
                remote:context_path + "/shop/existsNa.do?workshopName="+$("#workshopName").val()+"&shopid="+$("#shopid").val()
            }
        },
        messages:{
            "workshopName":{
                required:"请输入车间名称！",
                remote:"您输入的名称已经存在，请重新输入！"
            }
        },
        errorClass: "help-inline",
        errorElement: "span",
        highlight:function(element, errorClass, validClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        }
    });
    //确定按钮点击事件
    function saveForm() {
        if ($("#workshopForm").valid()) {
            saveworkshopinfo($("#workshopForm").serialize());
        }
    }
//保存修改车间信息
function saveworkshopinfo(bean) {
    $.ajax({
            url: context_path + "/shop/save?tm=" + new Date(),
            type:"POST",
            data:bean,
            dataType:"JSON",
            success:function (data) {
                if (Boolean(data.result)){
                    layer.msg("保存成功！",{icon: 1});
                    //关闭当前窗口
                    layer.close($queryWindow);
                    //刷新列表
                    gridReload();
                }else {
                    layer.alert("保存失败，请稍后重试！",{icon: 2});
                }
            },
            error:function (XMLHttpRequest) {
                alert(XMLHttpRequest.readyState);
                alert("出错了！！！！！");
            }
    });
}
</script>