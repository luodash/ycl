<%--
  Created by IntelliJ IDEA.
  User: HQKS-004-05
  Date: 2017/12/11
  Time: 17:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div id ="shop_add_page" class="row-fluid" style="height: inherit;">
    <form id="shopForm" class="form-horizontal"  style="overflow: auto; height: calc(100% - 70px);">
        <input type="hidden" name="id" id="id" value="${workshop.id}">
        <div class="control-group">
            <label class="control-label" for="workshop_no">车间编号：</label>
            <div class="controls">
                <div class="input-append span12 required" >
                    <input class="span11" type="text"  name="workshop_no" id="workshop_no" placeholder="后台自动生成" readonly="readonly" value="${workshop.workshop_no}">
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="workshop_name">车间名称：</label>
            <div class="controls">
                <div class="input-append span12 required">
                    <input class="span11" type="text" name="workshop_name" id="workshop_name"placeholder="车间名称" value="${workshop.workshop_name}">
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="remark">备注：</label>
            <div class="controls">
                <textarea class="span11" name="remark" id="remark" rows="3" cols="20" value="${workshop.remark}"></textarea>
            </div>
        </div>
    </form>
    <div class="form-actions" style="text-align: center;border-top: 0px;margin: 0px;">
        <span  class="savebtn btn btn-success">保存</span>
        <span  class="btn btn-danger">取消</span>
    </div>
</div>
<script type="text/javascript">
    (function(){
        $("#shopForm input[type=radio]").uniform();
        $("input[type=radio][name=gender][value=1]").attr("checked",true).trigger("click");
        //表单校验
        $("#shopForm").validate({
            rules:{
                "workshop_name":{
                    required:true,
                    maxlength:32,
                    remote:context_path + "/shop/hasSNA.do?id="+$('#id').val()
                },
                "remark":{
                    maxlength:256
                }
            },
            messages:{
                "workshop_name":{
                    required:"请输入车间名称",
                    remote:"您输入的名称已经存在，请重新输入！"
                }
            },
            errorClass: "help-inline",
            errorElement: "span",
            highlight:function(element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
            }
        });
        //确定按钮点击事件
        $("#shop_add_page .btn-success").off("click").on("click", function(){
            //车间保存
            if($("#shopForm").valid()){
                saveShopInfo($("#shopForm").serialize());
            }
        });

        //取消按钮点击事件
        $("#shop_add_page .btn-danger").off("click").on("click", function(){
            layer.close($queryWindow);
        });

        var ajaxStatus = 1;     //ajax请求状态：0不能请求，1可以请求
        //保存/修改车间信息
        function saveShopInfo(bean){
            if(bean){
                if(ajaxStatus==0){
                    layer.msg("操作进行中，请稍后...",{icon:2});
                    return;
                }
                ajaxStatus = 0;    //将标记设置为不可请求
                $(".savebtn").attr("disabled","disabled");
                $.ajax({
                    url:context_path+"/shop/save?tm="+new Date(),
                    type:"POST",
                    data:bean,
                    dataType:"JSON",
                    success:function(data){
                        ajaxStatus = 1; //将标记设置为可请求
                        $(".savebtn").removeAttr("disabled");
                        if(data.result){
                            layer.msg("保存成功！",{icon:1});
                            //刷新车间列表
                            $("#grid-table").jqGrid('setGridParam',
                                {
                                    postData: {queryJsonString:""} //发送数据
                                }
                            ).trigger("reloadGrid");
                            //关闭当前窗口
                            //关闭指定的窗口对象
                            layer.close($queryWindow);
                        }else{
                            layer.msg("保存失败，请稍后重试！",{icon:2});
                        }
                    }
                });
            }else{
                layer.msg("出错啦！",{icon:2});
            }
        }
    }())

</script>
