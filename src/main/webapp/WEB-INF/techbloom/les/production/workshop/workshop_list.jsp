<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<style type="text/css"></style>
<div id="grid-div">
    <form id="hiddenForm" action="<%=path%>/shop/toExcel" method="POST" style="display: none;">
        <input id="id" name="id" value=""/>
    </form>
    <form id="hiddenQueryForm" style="display:none;">
        <input name="workshopNo" id="workshopNo" value=""/>
        <input name="workshopName" id="workshopName" value="">
    </form>
     <div class="query_box" id="yy" title="查询选项">
            <form id="queryForm" style="max-width:100%;">
			 <ul class="form-elements">
				<li class="field-group field-fluid2">
					<label class="inline" for="workshopNo" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:92px;">车间编号：</span>
						<input type="text" name="workshopNo" id="workshopNo" value="" style="width: calc(100% - 97px );" placeholder="车间编号">
					</label>			
				</li>
				<li class="field-group field-fluid2">
					<label class="inline" for="workshopName" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:92px;">车间名称：</span>
						<input type="text" name="workshopName" id="workshopName" value="" style="width: calc(100% - 97px );" placeholder="车间名称">
					</label>					
				</li>
				
			</ul>
			<div class="field-button" style="">
						<div class="btn btn-info" onclick="queryOk();">
				            <i class="ace-icon fa fa-check bigger-110"></i>查询
			            </div>
						<div class="btn" onclick="reset();"><i class="ace-icon icon-remove"></i>重置</div>
		        </div>
		  </form>		 
    </div>
    <div id="fixed_tool_div" class="fixed_tool_div">
        <div id="__toolbar__" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="grid-table" style="width:100%;height:100%;"></table>
    <div id="grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
var oriData;
var _grid;
var openwindowtype = 0; //打开窗口类型：0新增，1修改
var workshopNo;
var type=0;
var _queryForm_data = iTsai.form.serialize($('#queryForm'));
$(function  (){
  //  $(".toggle_tools").click();
});
$("#__toolbar__").iToolBar({
    id: "__tb__01",
    items: [
        {label: "添加", disabled: ( ${ sessionUser.addQx} == 1 ? false : true), onclick:openAddPage, iconClass:'glyphicon glyphicon-plus'},
        {label: "编辑", disabled: ( ${ sessionUser.editQx} == 1 ? false : true),onclick: openEditPage, iconClass:'glyphicon glyphicon-pencil'},
        {label: "删除", disabled: ( ${ sessionUser.deleteQx} == 1 ? false : true),onclick: deleteBom, iconClass:'glyphicon glyphicon-trash'},
        {label: "导出", disabled: ( ${ sessionUser.queryQx}==1? false:true),onclick:toExcel,iconClass:' icon-share'}
    ]
});
_grid = jQuery("#grid-table").jqGrid({
    url : context_path + "/shop/list.do",
    datatype : "json",
    colNames : [ "主键", "车间编号","车间名称","备注"],
    colModel : [
        {name: "id",index : "id",hidden:true},
        {name: "workshopNo",index : "workshopNo",width : 50},
        {name: "workshopName",index : "workshopName",width : 60},
        {name : "remark",index : "remark"}
    ],
    rowNum : 20,
    rowList : [ 10, 20, 30 ],
    pager : "#grid-pager",
    sortname : "id",
    sortorder : "asc",
    altRows: true,
    viewrecords : true,
  //caption : "车间列表",
    autowidth: true,
    multiselect:true,
    multiboxonly: true,
    loadComplete : function(data){
        var table = this;
        setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
        oriData = data;
    },
    emptyrecords: "没有相关记录",
    loadtext: "加载中...",
    pgtext : "页码 {0} / {1}页",
    recordtext: "显示 {0} - {1}共{2}条数据"
});

jQuery("#grid-table").navGrid("#grid-pager",{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd('#grid-pager',{
    caption:"",
    buttonicon:"fa fa-refresh green",
    onClickButton: function(){
        $("#grid-table").jqGrid("setGridParam",
            {
                postData: {queryJsonString:""} //发送数据
            }
        ).trigger("reloadGrid");
    }
});

$(window).on("resize.jqGrid", function () {
    $("#grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);
    $("#grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-$("#yy").outerHeight(true)-$("#fixed_tool_div").outerHeight(true)-$("#grid-pager").outerHeight(true)
 //-$("#gview_grid-table .ui-jqgrid-titlebar").outerHeight(true)
   -$("#gview_grid-table .ui-jqgrid-hdiv").outerHeight(true));
});

$(window).triggerHandler('resize.jqGrid');
function gridReload()
{
    _grid.trigger("reloadGrid");  //重新加载表格
}

/**
 * 查询按钮点击事件
 */
function queryOk(){
    var queryParam = iTsai.form.serialize($('#queryForm'));
    queryShopByParam(queryParam);
}
/**
 * 重置按钮点击事件
 */
function reset(){
    iTsai.form.deserialize($('#queryForm'),_queryForm_data);
    queryShopByParam(_queryForm_data);
}
/**
 * 查询功能:获取查询页面中的值，并将值放入列表页面中隐藏的form
 * @param jsonParam     查询页面传递过来的json对象
 */
function queryShopByParam(jsonParam){
    var queryJsonString = JSON.stringify(jsonParam);         //将json对象转换成json字符串
    //执行查询操作
    $("#grid-table").jqGrid('setGridParam',
        {
            postData: {queryJsonString:queryJsonString}
        }
    ).trigger("reloadGrid");
}
/*打开添加页面*/
function openAddPage(){
    type=0;
    openwindowtype = 0;
    layer.load(2);
    $.post(context_path+'/shop/toUpdata.do?id=-1', {}, function(str){
        $queryWindow = layer.open({
            title : "车间添加",
            type: 1,
            skin : "layui-layer-molv",
            area : "600px",
            shade: 0.6, //遮罩透明度
            moveType: 1, //拖拽风格，0是默认，1是传统拖动
            content: str,//注意，如果str是object，那么需要字符拼接。
            success:function(layero, index){
                layer.closeAll('loading');
            }
        });
    }).error(function() {
        layer.closeAll();
        layer.msg('加载失败！',{icon:2});
    });
}
/*打开编辑页面*/
function openEditPage(){
    var selectAmount = getGridCheckedNum("#grid-table");
    if(selectAmount==0){
        layer.msg("请选择一条记录！",{icon:2});
        return;
    }else if(selectAmount>1){
        layer.msg("只能选择一条记录！",{icon:8});
        return;
    }
    openwindowtype = 1;
    layer.load(2);
    var id = jQuery("#grid-table").jqGrid('getGridParam', 'selarrrow');
    $.post(context_path+'/shop/toUpdata.do?id='+id, {}, function(str){
        $queryWindow = layer.open({
            title : "车间编辑",
            type: 1,
            skin : "layui-layer-molv",
            area : "600px",
            shade: 0.6, //遮罩透明度
            moveType: 1, //拖拽风格，0是默认，1是传统拖动
            content: str,//注意，如果str是object，那么需要字符拼接。
            success:function(layero, index){
                layer.closeAll('loading');
            }
        });
    }).error(function() {
        layer.closeAll();
        layer.msg('加载失败！',{icon:2});
    });
}
/*删除车间*/
function deleteBom() {
    var checkedNum = getGridCheckedNum("#grid-table", "id");  //选中的数量
    if (checkedNum == 0) {
        layer.alert("请选择一个要删除的车间！");
    } else {
        //从数据库中删除选中的车间，并刷新车间表格
        var ids = jQuery("#grid-table").jqGrid('getGridParam', 'selarrrow');
        layer.confirm("确定删除选中的车间？", function() {
            $.ajax({
                type : "POST",
                url : context_path + '/shop/deleteShop.do?ids='+ids ,
                dataType : 'json',
                cache : false,
                success : function(data) {
                    if (data.result) {
                        layer.msg(data.msg,{icon: 1,time:1000});
                    }else{
                        layer.msg(data.msg,{icon: 7,time:1000});
                    }
                    _grid.trigger("reloadGrid");  //重新加载表格
                }
            });
        });

    }
}
function toExcel(){
    var id = jQuery("#grid-table").jqGrid("getGridParam", "selarrrow");
    $("#hiddenForm #id").val(id);
    $("#hiddenForm").submit();
}
</script>