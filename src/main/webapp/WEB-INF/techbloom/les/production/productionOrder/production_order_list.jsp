<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
	<script type="text/javascript">
	    var context_path = '<%=path%>';
	</script>
	<style type="text/css">
	
	.query_box .field-button.two {
    padding: 0px;
    left: 650px;
    }
	</style>
</head>
<body style="overflow:hidden;">
   <div id="grid-div">
   <!-- 隐藏区域：存放查询条件 -->
		<form id="hiddenQueryForm"  action ="<%=path%>/productionOrder/excel" style="display:none;">
			<input name="ids" id="ids" value="" />
			<!-- 生产订单编号 -->
			<input id="orderNo" name="orderNo" value="" />
			<!-- 生产订单名称-->
			<input id="orderName" name="orderName" value="" >
			<!-- 生产产品-->
			<input id="product" name="productId" value="" > 
		</form>
		<div class="query_box" id="yy" title="查询选项">
            <form id="queryForm" style="max-width:100%;">
			 <ul class="form-elements">
				<li class="field-group field-fluid3">
					<label class="inline" for="orderNo" style="margin-right:20px; width: 100%;">
						<span class="form_label" style="width:92px;">生产订单编号：</span>
						<input type="text" name="orderNo" id="orderNo" value="" style="width: calc(100% - 96px);" placeholder="生产订单编号">
					</label>			
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="orderName" style="margin-right:20px; width: 100%;">
						<span class="form_label" style="width:92px;">生产订单名称：</span>
						<input type="text" name="orderName" id="orderName" value="" style="width: calc(100% - 96px);" placeholder="生产订单名称">
					</label>					
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="productId" style="margin-right:20px;width: 100%;">
						<span class="form_label" style="width:92px;">生产产品：</span>
						<input type="text" id="product" name="productId" style="width: calc(100% - 96px);" placeholder="生产产品">
					</label>					
				</li>
				
			</ul>
			<div class="field-button" style="">
						<div class="btn btn-info" onclick="queryOk();">
				            <i class="ace-icon fa fa-check bigger-110"></i>查询
			            </div>
						<div class="btn" onclick="reset();"><i class="ace-icon icon-remove"></i>重置</div>
		        </div>
		  </form>		 
    </div>
        <div id="fixed_tool_div" class="fixed_tool_div">
             <div id="__toolbar__" style="float:left;overflow:hidden;"></div>
        </div>
		<table id="grid-table" style="width:100%;height:100%;"></table>
		<div id="grid-pager"></div>
   </div>
</body>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript" src="<%=path%>/static/js/techbloom/les/production/productionOrder/productionOrder.js"></script>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriData; 
    var _grid;
    var dynamicDefalutValue="4a9dcc2bed974831a98a9012a103950e";//列表码
    $(function  (){
      //  $(".toggle_tools").click();
    });
    $("#__toolbar__").iToolBar({
   	  	 id:"__tb__01",
   	  	 items:[
   	  	    {label:"添加",disabled:(${sessionUser.addQx}==1?false:true),onclick:add,iconClass:'glyphicon glyphicon-plus'},
   	  	    {label:"编辑",disabled:(${sessionUser.editQx}==1?false:true),onclick:edit,iconClass:'glyphicon glyphicon-pencil'},
	   	  	{label:"删除",disabled:(${sessionUser.deleteQx}==1?false:true),onclick:del,iconClass:'glyphicon glyphicon-trash'},
	   	  	{label:"导出", disabled:(${sessionUser.queryQx} == 1 ? false : true),onclick:function () {var ids = jQuery("#grid-table").jqGrid('getGridParam', 'selarrrow'); $("#ids").val(ids); $("#hiddenQueryForm").submit();},iconClass:' icon-share'},  	  	
		  ]
	});
    
   $(function(){
		_grid = jQuery("#grid-table").jqGrid({
	       url : context_path + "/productionOrder/list.do",
	       datatype : "json",
	       colNames : [ "主键","生产订单编号","生产订单名称","开始时间", "完成时间","生产产品","生产总数量","正在生产数量","已完成数量","备注","状态"],
	       colModel : [ 
	                    {name : "id",index : "id",width : 20,hidden:true}, 
	                    {name : "orderNo",index : "orderNo",width : 40}, 
	                    {name : "orderName",index : "orderName",width : 40}, 
	                    {name : "startTime",index : "startTime",width : 40}, 
	                    {name : "completeTime",index : "completeTime",width : 40}, 
	                    {name : "productName",index : "productName",width : 40}, 
	                    {name : "amount",index : "amount",width : 40}, 
	                    {name : "lineAmount",index : "lineAmount",width : 40},
	                    {name : "completeAmount",index : "completeAmount",width : 40},
	                    {name : "remark",index : "remark",width : 40},
	                    {name : "state",index : "state",width : 40,
	                       formatter:function(cellValue,option,rowObject){
	                    		if(typeof cellValue == "number"){
	                    			if(cellValue==0){
	                    				return "<span style='color:#d15b47;font-weight:bold;'>未生产</span>";
	                    			}else if(cellValue==1){
	                    				return "<span style='color:#d15b47;font-weight:bold;'>正在生产</span>";
	                    			}else if(cellValue==2){
	                    				return "<span style='color:#76b86b;font-weight:bold;'>完成生产</span>";
	                    			}
	                    		}else{
	                    			return "异常";
	                    		}
	                    	}
	                    } 
	                  ],
	       rowNum : 20,
	       rowList : [ 10, 20, 30 ],
	       pager : '#grid-pager',
	       sortname : 'ID',
	       sortorder : "desc",
	       altRows: false, 
	       viewrecords : true,
	       autowidth:true,
	       multiselect:true,
		   multiboxonly: true,
           beforeRequest:function (){
                dynamicGetColumns(dynamicDefalutValue,"grid-table",$(window).width()-$("#sidebar").width() -7);
                //重新加载列属性
            },
	       loadComplete : function(data) {
	           var table = this;
	           setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
	           oriData = data;
	       },
	       emptyrecords: "没有相关记录",
	       loadtext: "加载中...",
	       pgtext : "页码 {0} / {1}页",
	       recordtext: "显示 {0} - {1}共{2}条数据"
	  });
	 //在分页工具栏中添加按钮
	  jQuery("#grid-table").navGrid('#grid-pager',{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd('#grid-pager',{  
		   caption:"",   
		   buttonicon:"ace-icon fa fa-refresh green",   
		   onClickButton: function(){
			   //jQuery("#grid-table").trigger("reloadGrid");  //重新加载表格  
			   $("#grid-table").jqGrid('setGridParam', 
						{
							postData: {queryJsonString:""} //发送数据 
						}
				  ).trigger("reloadGrid");
		   }
		}).navButtonAdd('#grid-pager',{
              caption: "",
              buttonicon:"fa  icon-cogs",
              onClickButton : function (){
                  jQuery("#grid-table").jqGrid('columnChooser',{
                      done: function(perm, cols){
                          dynamicColumns(cols,dynamicDefalutValue);
                          $("#grid-table").jqGrid( 'setGridWidth', $("#grid-div").width() - 3);
                      }
                  });
              }
          });
	  $(window).on('resize.jqGrid', function () {
		  $("#grid-table").jqGrid( 'setGridWidth', $("#grid-div").width() - 3 );
		  var height = $("#breadcrumb").outerHeight(true)+$(".query_box").outerHeight(true)+
            $("#fixed_tool_div").outerHeight(true)+
          //$("#gview_grid-table .ui-jqgrid-titlebar").outerHeight(true)+
            $("#gview_grid-table .ui-jqgrid-hbox").outerHeight(true)+
            $("#grid-pager").outerHeight(true)+$("#header").outerHeight(true);
            $("#grid-table").jqGrid( 'setGridHeight', (document.documentElement.clientHeight)-height );
	  })
	
	  $(window).triggerHandler('resize.jqGrid');
    }); 
   var _queryForm_data = iTsai.form.serialize($('#queryForm'));
	function queryOk(){
		//var formJsonParam = $('#queryForm').serialize();
		var queryParam = iTsai.form.serialize($('#queryForm'));
		//执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
		queryInstoreListByParam(queryParam);
		
	}
	
	function reset(){
		//var formJsonParam = $('#queryForm').serialize();
		iTsai.form.deserialize($('#queryForm'),_queryForm_data); 
		$("#allocateTypeSelect").val("").trigger('change');
		$("#inWarehouseSelect").val("").trigger('change');
		$("#outWarehouseSelect").val("").trigger('change');
		$("#queryForm #product").select2("val","");
		//var queryParam = iTsai.form.serialize($('#queryForm'));
		//执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
		queryInstoreListByParam(_queryForm_data);
		$("#allocateTypeSelect").find("option[text='所有类型']").attr("selected",true);
		$("#outWarehouseSelect").find("option[text='所有库区']").attr("selected",true);
		$("#inWarehouseSelect").find("option[text='所有库区']").attr("selected",true);
	}
 $('#queryForm .mySelect2').select2();
 $(".date-picker").datetimepicker({format: 'YYYY-MM-DD',useMinutes:true,useSeconds:true});
 function reloadGrid(){
   _grid.trigger("reloadGrid");
 }
 $("#queryForm #product").select2({
    	placeholder: "选择产品",
		minimumInputLength:0,   //至少输入n个字符，才去加载数据
	    allowClear: true,  //是否允许用户清除文本信息
		delay: 250,
		formatNoMatches:"没有结果",
		formatSearching:"搜索中...",
		formatAjaxError:"加载出错啦！",
		ajax : {
			url : context_path + "/salemanage/getMListByInId",
			type:"POST",
			dataType : "json",
			delay : 250,
			data: function (term,pageNo) {     //在查询时向服务器端传输的数据
	            term = $.trim(term);
                return {
                	queryString: term,    //联动查询的字符
                	pageSize: 15,    //一次性加载的数据条数
                    pageNo:pageNo,    //页码
                    time:new Date()   //测试
                 }
	        },
	        results: function (data,pageNo) {
	        		var res = data.result;
   	            if(res.length>0){   //如果没有查询到数据，将会返回空串
   	               var more = (pageNo*15)<data.total; //用来判断是否还有更多数据可以加载
   	               return {
   	                    results:res,more:more
   	                 };
   	            }else{
   	            	return {
   	            		results:{}
   	            	};
   	            }
	        },
			cache : true
		}

    });
</script>
</html>