<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<div id="supplier_edit_page" class="row-fluid" style="height: inherit;">
	<form id="editForm" class="form-horizontal" style="overflow: auto; height: calc(100% - 70px);">
		<input type="hidden" name="id" id="id" value="${order.id}" />
		<div class="control-group">
			<label class="control-label" for="shipper">生产订单编号：</label>
			<div class="controls">
				<div class="input-append span12">
					<input class="span11" type="text" name="orderNo" readonly id="orderNo" placeholder="后台自动生成" value="${order.orderNo}" />
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="orderId">原料订单：</label>
			<div class="controls">
				<div class="span12 required" style=" float: none !important;">
					<input type="text" class="span11" id="orderId" name="orderId" placeholder="选择原料订单" />
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="remark">备注：</label>
			<div class="controls">
				<div class="input-append span12">
					<input type="text" class="span11" id="remark" name="remark" value="${order.remark}" placeholder="联系人" />
				</div>
			</div>
		</div>
	</form>
	<div class="field-button" style="text-align: center;border-top: 0px;margin: 15px auto;">
		<span class="btn btn-info" onclick="saveForm();">
		   <i class="ace-icon fa fa-check bigger-110"></i>保存
		</span>
		<span class="btn btn-danger" onclick="layer.closeAll();">
		   <i class="icon-remove"></i>&nbsp;取消
		</span>
	</div>
</div>
<script type="text/javascript">
function saveForm(){
	   
	   if($("#editForm").valid()){
			saveUserInfo($("#editForm").serialize());
	   }
	}
	function saveUserInfo(bean){
  		if(bean){
  			$(".savebtn").attr("disabled","disabled");
  			$.ajax({
  				url:context_path+"/productionOrder/save?tm="+new Date(),
  				type:"POST",
  				data:bean,
  				dataType:"JSON",
  				success:function(data){
  					$(".savebtn").removeAttr("disabled");
  					if(Boolean(data.result)){
  						layer.msg("保存成功！",{icon:1});
  						//刷新用户列表
  						$("#grid-table").jqGrid('setGridParam', 
							{
								postData: {queryJsonString:""} //发送数据 
							}
						).trigger("reloadGrid");
  						//关闭当前窗口
  						layer.closeAll();
  					}else{
  						layer.msg("保存失败，请稍后重试！",{icon:2});
  					}
  				}
  			});
  		}else{
  			layer.msg("出错啦！",{icon:2});
  		}
  	}
(function(){
    $("#editForm #orderId").select2({
    	placeholder: "选择原料订单",
		minimumInputLength:0,   //至少输入n个字符，才去加载数据
	    allowClear: true,  //是否允许用户清除文本信息
		delay: 250,
		formatNoMatches:"没有结果",
		formatSearching:"搜索中...",
		formatAjaxError:"加载出错啦！",
		ajax : {
			url : context_path + "/productionOrder/getSelectOrder",
			type:"POST",
			dataType : "json",
			delay : 250,
			data: function (term,pageNo) {     //在查询时向服务器端传输的数据
	            term = $.trim(term);
                return {
                	queryString: term,    //联动查询的字符
                	pageSize: 15,    //一次性加载的数据条数
                    pageNo:pageNo,    //页码
                    time:new Date()   //测试
                 }
	        },
	        results: function (data,pageNo) {
	        		var res = data.result;
   	            if(res.length>0){   //如果没有查询到数据，将会返回空串
   	               var more = (pageNo*15)<data.total; //用来判断是否还有更多数据可以加载
   	               return {
   	                    results:res,more:more
   	                 };
   	            }else{
   	            	return {
   	            		results:{}
   	            	};
   	            }
	        },
			cache : true
		}

    });
    if($("#orderId").val()!=""){
  	//$("#productId").val($("#pId").val());
  	  $("#productId").select2("data",{
  	     id:$("#pid").val(),
  	     text:$("#pName").val()
  	  })
  	}
	//表单校验
	$("#editForm").validate({
	    ignore:"",
  		rules:{
  			"orderId" : {
  				required:true
  			} 
  		},
  		messages:{
  			"orderId":{
  				required:"请选择原料订单！"
  			}
  		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
		}
	});
	//取消按钮点击事件
	$("#user_edit_page .btn-danger").off("click").on("click", function(){
	    layer.closeAll();
	});
	var ajaxStatus = 1;     //ajax请求状态：0不能请求，1可以请求
	//保存/修改用户信息
  	
  	
}());
</script>