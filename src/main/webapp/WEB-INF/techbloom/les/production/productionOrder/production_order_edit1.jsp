<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div id ="user_edit_page" class="row-fluid" style="height: inherit;">
	<form id="editForm" class="form-horizontal"  style="overflow: auto; height: calc(100% - 70px);">
		<input type="hidden" name="id" id="orderId" value="${order.id}">
		<div class="control-group">
			<label class="control-label" for="orderNo">生产订单编号：</label>
			<div class="controls">
			<div class="input-append span12" > 
			      <input class="span11" type="text"  name="orderNo" readonly id="orderNo" placeholder="后台自动生成" value="${order.orderNo}">
			    </div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="orderName">生产订单名称：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input class="span11" type="text" name="orderName" id="orderName" value="${order.orderName}" value="" placeholder="生产订单名称" />
				</div>
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label" for="productId">生产产品：</label>
			<div class="controls required">
				<input class="span11 select2_input" type="text" name="productId" id="productId" ></input>
				<input  type="hidden" name="pid" id="pid" value="${order.productId}"/>
				<input  type="hidden" name="pName" id="pName" value="${order.productName}"/>
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label" for="amount">生产数量：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input class="span11" type="text" name="amount" id="amount"
						 placeholder="数量" value="${order.amount}"/>
				</div>
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label" for="remark">备注：</label>
			<div class="controls">
				<textarea class="span11" name="remark" id="remark" rows="3" cols="20">${order.remark}</textarea>
			</div>
		</div> 
		
	</form>
	<div class="form-actions" style="text-align: right;border-top: 0px;margin: 0px;">
	 <span  class="savebtn btn btn-success">保存</span>
     <span  class="btn btn-danger">取消</span>
	</div>
</div>

<script type="text/javascript">
(function(){
    $("#productId").select2({
    	placeholder: "选择产品",
		minimumInputLength:0,   //至少输入n个字符，才去加载数据
	    allowClear: true,  //是否允许用户清除文本信息
		delay: 250,
		formatNoMatches:"没有结果",
		formatSearching:"搜索中...",
		formatAjaxError:"加载出错啦！",
		ajax : {
			url : context_path + '/salemanage/getMListByInId',
			type:"POST",
			dataType : 'json',
			delay : 250,
			data: function (term,pageNo) {     //在查询时向服务器端传输的数据
	            term = $.trim(term);
                return {
                	queryString: term,    //联动查询的字符
                	pageSize: 15,    //一次性加载的数据条数
                    pageNo:pageNo,    //页码
                    time:new Date()   //测试
                 }
	        },
	        results: function (data,pageNo) {
	        		var res = data.result;
   	            if(res.length>0){   //如果没有查询到数据，将会返回空串
   	               var more = (pageNo*15)<data.total; //用来判断是否还有更多数据可以加载
   	               return {
   	                    results:res,more:more
   	                 };
   	            }else{
   	            	return {
   	            		results:{}
   	            	};
   	            }
	        },
			cache : true
		}

    });
    if($("#orderId").val()!=""){
  	//$("#productId").val($("#pId").val());
  	  $("#productId").select2("data",{
  	     id:$("#pid").val(),
  	     text:$("#pName").val()
  	  })
  	}
	//表单校验
	$("#editForm").validate({
  		rules:{
  			"orderName" : {
  				required:true,
  				maxlength:32,
  				remote:context_path+"/productionOrder/isHaveName.do?id="+$('#orderId').val()
  			},
  			"productId":{
  				required:true,
  			},
  			"amount":{
  			    required:true,
    			number: true
    		}, 
  		},
  		messages:{
  			"orderName":{
  				remote:"您输入的生产订单名称已经存在，请重新输入！"
  			}
  		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
		}
	});
	
	//确定按钮点击事件
    $("#user_edit_page .btn-success").off("click").on("click", function(){
		//用户保存
	  if($("#editForm").valid()){
			saveUserInfo($("#editForm").serialize());
		}
	});
	
	//取消按钮点击事件
	$("#user_edit_page .btn-danger").off("click").on("click", function(){
	    layer.closeAll();
	});
	var ajaxStatus = 1;     //ajax请求状态：0不能请求，1可以请求
	//保存/修改用户信息
  	function saveUserInfo(bean){
  		if(bean){
  			if(ajaxStatus==0){
  				layer.msg("保存中，请稍后...",{icon:2});
  				return;
  			}
  			ajaxStatus = 0;
  			$(".savebtn").attr("disabled","disabled");
  			$.ajax({
  				url:context_path+"/productionOrder/save?tm="+new Date(),
  				type:"POST",
  				data:bean,
  				dataType:"JSON",
  				success:function(data){
  					ajaxStatus = 1; //将标记设置为可请求
  					$(".savebtn").removeAttr("disabled");
  					if(data.result){
  						layer.msg("保存成功！",{icon:1});
  						//刷新用户列表
  						$("#grid-table").jqGrid('setGridParam', 
							{
								postData: {queryJsonString:""} //发送数据 
							}
						).trigger("reloadGrid");
  						//关闭当前窗口
  						layer.closeAll();
  					}else{
  						layer.msg("保存失败，请稍后重试！",{icon:2});
  					}
  				}
  			});
  		}else{
  			layer.msg("出错啦！",{icon:2});
  		}
  	}
  	
}());

</script>