﻿<%@ page language="java" import="java.lang.*"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
           <div class="main-content" style="height:100%">
								<div class="widget-header widget-header-large" id="div1">
									<!-- 隐藏的主键 -->
				   	        		<input type="hidden" id="bomId" name="id" value="${artBom.id }">
									<h3 class="widget-title grey lighter" style=' background: none; border-bottom: none; '>
										<i class="ace-icon fa fa-leaf green"></i>
										工艺BOM
									</h3>

									<!-- #section:pages/invoice.info -->
									<div class="widget-toolbar no-border invoice-info">
										<span class="invoice-info-label">BOM编号:</span> <span
											class="red">${artBom.bomNo }</span><br /> <span
											class="invoice-info-label">创建时间:</span> <span class="blue">${artBom.bomCreateTime}</span>
									</div>
								</div>
								
								<div class="widget-body" id="div2">
									<table style="width: 100%;font-size:16px;border-collapse:separate;border-spacing:2px 3px;margin-left: 20px">
									  <tr>
									  <td>
											<i class="ace-icon fa fa-caret-right blue"></i>
											BOM名称: 
											<b class="black">${artBom.bomName }</b>
									  </td>
									  <td>
											<i class="ace-icon fa fa-caret-right blue"></i>
											BOM版本:
											<b class="black">${artBom.versionName}-${artBom.version}</b>
									  </td>
									  </tr>
									  <tr>
									     <tr>
									       <td>
									          <i class="ace-icon fa fa-caret-right blue"></i>
											备注:
											<b class="black">${artBom.remark }</b>
									       </td>
									       <td>
									          <i class="ace-icon fa fa-caret-right blue"></i>
												状态 :
												<c:if test="${artBom.state==0 }"><span class="red">未生产</span></c:if>
											    <c:if test="${artBom.state==1 }"><span class="green">生产中</span></c:if>
									       </td>
									     </tr>
									  </tr>
									</table>
								 </div>
								<!-- 详情表格 -->
								<div id="grid-div-c">
								<!-- 物料信息表格 -->
									<table id="grid-table-c" style="width:100%;height:100%;"></table>
								<!-- 表格分页栏 -->
									<div id="grid-pager-c"></div>
							   </div>
							
				</div><!-- /.main-content -->
<script type="text/javascript">
var oriDataView;
var _grid_view;        //表格对象
 _grid_view=jQuery("#grid-table-c").jqGrid({
         url : context_path + '/artBom/detailList?bomId='+$("#bomId").val(),
         datatype : "json",
          colNames : [ '详情主键','工位','工时(单位:秒)','物料编号','物料名称','库位','库位物料信息','消耗数量','安全库存'],
         colModel : [ 
  					  {name : 'id',index : 'id',width : 20,hidden:true}, 
  					  {name : 'routeName',index:'routeName',width :20,
  					    cellattr: function(rowId, tv, rawObject, cm, rdata) {
                            //合并单元格
                            return 'id=\'routeName' + rowId + "\'";
                        } 
  					  },
  					  {name : 'workTime',index:'workTime',width :20,
  					     cellattr: function(rowId, tv, rawObject, cm, rdata) {
                            //合并单元格
                            return 'id=\'workTime' + rowId + "\'";
                        } 
  					  },
  					  {name : 'materialNo',index:'materialNo',width :20}, 
                      {name : 'materialName',index:'materialName',width : 20}, 
                      {name : 'locationName',index:'locationName',width : 20}, 
                      {name : 'locationInfo',index:'locationInfo',width : 20}, 
                      {name: 'consumeAmount', index: 'consumeAmount', width: 20},
                      {name : 'alarmAmount',index:'alarmAmount',width : 20}
                    ],
         rowNum : 20,
         rowList : [ 10, 20, 30 ],
         pager : '#grid-pager-c',
         sortname : 'ID',
         sortorder : "asc",
         altRows: true,
         viewrecords : true,
         autowidth:true,
         multiselect:true,
		 multiboxonly: true,
         loadComplete : function(data){
             var table = this;
             setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
             oriDataDetail = data;
             merge();
             $(window).triggerHandler('resize.jqGrid');
         },
       /* cellurl : context_path + "/ASNmanage/updateAmount", */
	   cellEdit: true,
	   cellsubmit : "clientArray",
  	   emptyrecords: "没有相关记录",
  	   loadtext: "加载中...",
  	   pgtext : "页码 {0} / {1}页",
  	   recordtext: "显示 {0} - {1}共{2}条数据",
    });
    //在分页工具栏中添加按钮
    $("#grid-table-c").navGrid('#grid-pager-c',{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd('#grid-pager-c',{  
  	   caption:"",   
  	   buttonicon:"ace-icon fa fa-refresh green",   
  	   onClickButton: function(){   
  	    $("#grid-table-c").jqGrid('setGridParam', 
				{
  	    			url:context_path + '/artBom/detailList?bomId='+$("#bomId").val(),
					postData: {queryJsonString:""} //发送数据  :选中的节点
				}
		  ).trigger("reloadGrid");
  	   }
  	});  	
    $(window).on("resize.jqGrid", function () {
  		$("#grid-table-c").jqGrid("setGridWidth", $("#grid-div-c").width() - 3 );
  		 var height =$(".layui-layer-title",_grid_view.parents(".layui-layer")).height()+
  		             $("#div1").outerHeight(true)+$("#div2").outerHeight(true)+
  		             $("#grid-pager-c").outerHeight(true)+
  		           //$("#fixed_tool_div.fixed_tool_div.detailToolBar").outerHeight(true)+
  		           //$("#gview_grid-table-c .ui-jqgrid-titlebar").outerHeight(true)+
  		             $("#gview_grid-table-c .ui-jqgrid-hbox").outerHeight(true);
                     $("#grid-table-c").jqGrid("setGridHeight",_grid_view.parents(".layui-layer").height()-height);
  	});
  	$(window).triggerHandler("resize.jqGrid");   
    //合并工位与工时列
    function merge() {
       var CellName="routeName";
           //得到显示到界面的id集合
           var mya = $("#grid-table-c").getDataIDs();
           //当前显示多少条
           var length = mya.length;
           for (var i = 0; i < length; i++) {
               //从上到下获取一条信息
               var before = $("#grid-table-c").jqGrid('getRowData', mya[i]);
               //定义合并行数
               var rowSpanTaxCount = 1;
               for (j = i + 1; j <= length; j++) {
                   //和上边的信息对比 如果值一样就合并行数+1 然后设置rowspan 让当前单元格隐藏
                   var end = $("#grid-table-c").jqGrid('getRowData', mya[j]);
                   if (before[CellName] == end[CellName]) {
                       rowSpanTaxCount++;
                       $("#grid-table-c").setCell(mya[j], CellName, '', { display: 'none' });
                       $("#grid-table-c").setCell(mya[j], "workTime", '', { display: 'none' });
                   } else {
                       rowSpanTaxCount = 1;
                       break;
                   }
                   //合并工位列
                   $("#" + CellName + "" + mya[i] + "").attr("rowspan", rowSpanTaxCount);
                   //合并工时列
                   $("#workTime" + mya[i] + "").attr("rowspan", rowSpanTaxCount);
               }
           }
       }
    
    function printDoc(){
	var url = context_path + "/getbills/printGetbillsDetail?putId="+$("#id").val();
	window.open(url);
}
 </script>
