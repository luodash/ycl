<%@ page language="java" import="java.lang.*"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
  <div class="row-fluid" style="height: inherit;margin:0px;border: 0px">
	<form id="baseInfor" class="form-horizontal"  method="post" target="_ifr">
		<!-- 隐藏的主键 -->
  	        <input type="hidden" id="bomId" name="id" value="${bom.id}">
  	        <input type="hidden" id="state" name="state" value="${bom.state}">
            <!-- <input type="hidden" id="type" name="type" value=""> -->
  	       
  	        <div  class="row" style="margin:0;padding:0;">
	             <div class="control-group span6" style="display: inline">
	                <label class="control-label" for="bomNo" > BOM编号：</label>
	                <div class="controls">
	                    <div class="input-append span12" >
	                        <input type="text" id="bomNo" class="span10" name="bomNo" placeholder="后台自动生成" value='${bom.bomNo}' readonly="readonly"/>
	                    </div>
	                </div>
	            </div>
	            <%--BOM名称--%>
	            <div class="control-group span6" style="display: inline">
	                <label class="control-label" for="bomName" >BOM名称：</label>
	                <div class="controls">
	                    <div class=" input-append required span12">
	                        <input class="span10" type="text" id="bomName" name="bomName"  placeholder="BOM名称" value="${bom.bomName}">
	                    </div>
	                </div>
	            </div>
           </div> 
  	       
  	       <div  class="row" style="margin:0;padding:0;">
	             <div class="control-group span6" style="display: inline">
	                <label class="control-label" for="modelId" > 生产流程：</label>
	                <div class="controls">
	                    <div class="span12 required" style=" float: none !important;">
	                        <input type="text" id="modelId" class="span10" name="modelId" placeholder="请选择流程"/>
	                    </div>
	                </div>
	            </div>
	             <div class="control-group span6" style="display: inline">
	                <label class="control-label" for="productId" > 产品：</label>
	                <div class="controls">
	                    <div class="span12 required" >
	                        <input type="text" id="productId" class="span10" name="productId" placeholder="请选择产品"/>
	                    </div>
	                </div>
	            </div>
            </div> 
  	       <div  class="row" style="margin:0;padding:0;">
            <%--备注--%>
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="remark" >备注：</label>
                <div class="controls">
                    <div class="input-append  span12" >
                        <input class="span10" type="text" id="remark" name="remark"
                               placeholder="备注" value="${bom.remark}">
                    </div>
                </div>
            </div>
          </div> 
  	       <div style="margin-left:10px;">
            <span class="btn btn-info" id="formSave">
		       <i class="ace-icon fa fa-check bigger-110"></i>保存
            </span>
            <span class="btn btn-info" id="formSubmit"">
		        <i class="ace-icon fa fa-check bigger-110"></i>&nbsp;版本发布
            </span>
           </div>
  	        
	</form>
	<div id="materialDiv" style="margin:10px;">
		<!-- 下拉框 -->
		<label class="inline" for="routeId">工位：</label>
		<input type="text" id = "routeId" name="routeId" 
		style="width:150px;margin-right:10px;" />
		<label class="inline" for="materialInfor">物料：</label>
		<input type="text" id = "materialInfor" name="materialInfor" 
		style="width:250px;margin-right:10px;" />
		
		<button id="addMaterialBtn" class="btn btn-xs btn-primary" onclick="addDetail();">
			<i class="icon-plus" style="margin-right:6px;"></i>添加
		</button>
	</div>
	<!-- 表格div -->
	<div id="grid-div-c" style="width:100%;">
		<!-- 	表格工具栏 -->
        <div id="fixed_tool_div" class="fixed_tool_div detailToolBar">
             <div id="__toolbar__-c" style="float:left;overflow:hidden;"></div>
        </div>
		<!-- 货物详情信息表格 -->
		<table id="grid-table-c" style="width:100%;height:100%;"></table>
		<!-- 表格分页栏 -->
		<div id="grid-pager-c"></div>
	</div>
</div>
<script type="text/javascript">
var context_path = '<%=path%>';
var oriDataDetail;
 var _grid_detail;        //表格对象
 var lastsel2;
 var selectData=0;
 var select_dataLocation;
$("#baseInfor").validate({
   ignore: "", 
   rules:{
      "bomName":{
         required:true,
      },
      "modelId":{
         required:true,
      },
      "productId":{
         required:true,
      }
   },
   messages: {
    "bomName":{
         required:"请填写bom名称",
      },
    "modelId":{
         required:"请选择流程",
      },
      "productId":{
         required:"请选择产品",
      }
   },
  errorClass: "help-inline",
    errorElement: "span",
    highlight:function(element, errorClass, validClass) {
        $(element).parents('.control-group').addClass('error');
    },
    unhighlight: function(element, errorClass, validClass) {
        $(element).parents('.control-group').removeClass('error');
    }
});
$("#formSubmit").click(function(){
  if($("#baseInfor #bomId").val()==""){
    layer.msg("请先保存表头信息！",{icon:2,time:1200});
    return;
  }
  $.ajax({
            url: context_path + "/artBom/saveVersionAuth?id="+$("#baseInfor #bomId").val(),
            type: "POST",
            dataType: "JSON",
            success: function (data) {
               if(data.result){
                    $.post(context_path + '/artBom/toSaveVersion?id='+$("#baseInfor #bomId").val(), {}, function (str) {
                        $queryWindow = layer.open({
                            title: "版本发布",
                            type: 1,
                            skin: "layui-layer-molv",
                            area: "450px",
                            shade: 0.6, //遮罩透明度
                            moveType: 1, //拖拽风格，0是默认，1是传统拖动
                            content: str,//注意，如果str是object，那么需要字符拼接。
                            success: function (layero, index) {
                                layer.closeAll('loading');
                            }
                        });
                    }).error(function () {
                        layer.closeAll();
                        layer.alert('加载失败！', {icon: 2});
                    });
               }else{
                 layer.alert(data.msg);
               }
            }
         });
});

$("#formSave").click(function(){
 var bean = $("#baseInfor").serialize();
if($('#baseInfor').valid()){
   saveOrUpdate(bean);
}
});

/* 保存、修改 */
function saveOrUpdate(bean) {
        $.ajax({
            url: context_path + "/artBom/saveOrUpdate",
            type: "POST",
            data: bean,
            dataType: "JSON",
            success: function (data) {
                if (data.result) {
                    layer.msg('表头保存成功！', {icon: 1});
                    $("#grid-table").jqGrid('setGridParam',
                        {
                            postData: {queryJsonString: ""} //发送数据
                        }
                    ).trigger("reloadGrid");
                    $("#baseInfor #bomNo").val(data.bomNo);
                    $("#baseInfor #bomId").val(data.bomid);
                    selectid = data.bomid;
                } else {
                    layer.msg('表头保存失败！', {icon: 2});
                }
            }
        });
    }
$('#routeId').select2({
			placeholder : "请选择工位",//文本框的提示信息
			minimumInputLength : 0, //至少输入n个字符，才去加载数据
			allowClear : true, //是否允许用户清除文本信息
			multiple: false,
			closeOnSelect:false,
			ajax : {
				url : context_path + '/artBom/getRoute',
				dataType : 'json',
				delay : 250,
				data : function(term, pageNo) { //在查询时向服务器端传输的数据
					term = $.trim(term);
					selectParam = term;
					return {
						/* docId : $("#baseInfor #id").val(), */
						queryString : term, //联动查询的字符
						pageSize : 15, //一次性加载的数据条数
						pageNo : pageNo, //页码
						time : new Date(),
						inPlanId:$("#baseInfor #inPlanId").val()
					//测试
					}
				},
				results : function(data, pageNo) {
					var res = data.result;
					if (res.length > 0) { //如果没有查询到数据，将会返回空串
						var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
						return {
							results : res,
							more : more
						};
					} else {
						return {
							results : {
								"id" : "0",
								"text" : "没有更多结果"
							}
						};
					}

				},
				cache : true
			}
		});    
$('#modelId').select2({
			placeholder : "请选择生产流程",//文本框的提示信息
			minimumInputLength : 0, //至少输入n个字符，才去加载数据
			allowClear : true, //是否允许用户清除文本信息
			closeOnSelect:false,
			ajax : {
				url : context_path + '/artBom/getModel',
				dataType : 'json',
				delay : 250,
				data : function(term, pageNo) { //在查询时向服务器端传输的数据
					term = $.trim(term);
					selectParam = term;
					return {
						/* docId : $("#baseInfor #id").val(), */
						queryString : term, //联动查询的字符
						pageSize : 15, //一次性加载的数据条数
						pageNo : pageNo, //页码
						time : new Date(),
						inPlanId:$("#baseInfor #inPlanId").val()
					//测试
					}
				},
				results : function(data, pageNo) {
					var res = data.result;
					if (res.length > 0) { //如果没有查询到数据，将会返回空串
						var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
						return {
							results : res,
							more : more
						};
					} else {
						return {
							results : {
								"id" : "0",
								"text" : "没有更多结果"
							}
						};
					}

				},
				cache : true
			}
		});

$('#productId').select2({
			placeholder : "请选择产品",//文本框的提示信息
			minimumInputLength : 0, //至少输入n个字符，才去加载数据
			allowClear : true, //是否允许用户清除文本信息
			closeOnSelect:false,
			ajax : {
				url : context_path + '/salemanage/getMListByInId',
				dataType : 'json',
				delay : 250,
				data : function(term, pageNo) { //在查询时向服务器端传输的数据
					term = $.trim(term);
					selectParam = term;
					return {
						/* docId : $("#baseInfor #id").val(), */
						queryString : term, //联动查询的字符
						pageSize : 15, //一次性加载的数据条数
						pageNo : pageNo, //页码
						time : new Date(),
						inPlanId:$("#baseInfor #inPlanId").val()
					//测试
					}
				},
				results : function(data, pageNo) {
					var res = data.result;
					if (res.length > 0) { //如果没有查询到数据，将会返回空串
						var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
						return {
							results : res,
							more : more
						};
					} else {
						return {
							results : {
								"id" : "0",
								"text" : "没有更多结果"
							}
						};
					}

				},
				cache : true
			}
		});


$('#materialInfor').select2({
			placeholder : "请选择物料",//文本框的提示信息
			minimumInputLength : 0, //至少输入n个字符，才去加载数据
			allowClear : true, //是否允许用户清除文本信息
			multiple: true,
			closeOnSelect:false,
			ajax : {
				url : context_path + '/salemanage/getMListByInId',
				dataType : 'json',
				delay : 250,
				data : function(term, pageNo) { //在查询时向服务器端传输的数据
					term = $.trim(term);
					selectParam = term;
					return {
						/* docId : $("#baseInfor #id").val(), */
						queryString : term, //联动查询的字符
						pageSize : 15, //一次性加载的数据条数
						pageNo : pageNo, //页码
						time : new Date(),
						inPlanId:$("#baseInfor #inPlanId").val()
					//测试
					}
				},
				results : function(data, pageNo) {
					var res = data.result;
					if (res.length > 0) { //如果没有查询到数据，将会返回空串
						var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
						return {
							results : res,
							more : more
						};
					} else {
						return {
							results : {
								"id" : "0",
								"text" : "没有更多结果"
							}
						};
					}

				},
				cache : true
			}
		});

		$('#materialInfor').on("change",function(e){
			var datas=$("#materialInfor").select2("val");
			selectData = datas;
			var selectSize = datas.length;
			if(selectSize>1){
				var $tags = $("#s2id_materialInfor .select2-choices");   //
				//$("#s2id_materialInfor").html(selectSize+"个被选中");
				var $choicelist = $tags.find(".select2-search-choice");
				var $clonedChoice = $choicelist[0];
				$tags.children(".select2-search-choice").remove();
				$tags.prepend($clonedChoice);
				
				$tags.find(".select2-search-choice").find("div").html(selectSize+"个被选中");
				$tags.find(".select2-search-choice").find("a").removeAttr("tabindex");
				$tags.find(".select2-search-choice").find("a").attr("href","#");
				$tags.find(".select2-search-choice").find("a").attr("onclick","removeChoice();");
			}
			//执行select的查询方法
			$("#materialInfor").select2("search",selectParam);
		});
		 //清空物料多选框中的值
 function removeChoice(){
	  $("#s2id_materialInfor .select2-choices").children(".select2-search-choice").remove();
	  $("#materialInfor").select2("val","");
	  selectData = 0;
	  
 }

//添加货物详情 
 function addDetail(){
	  if($("#bomId").val()==""){
      layer.msg("请先保存表头信息！",{icon:2,time:1200});
      return;
     }
     if($("#routeId").val()==""){
      layer.msg("请选择工位！",{icon:2,time:1200});
      return;
     }
	//  if(selectData!=0){
		  //将选中的物料添加到数据库中
		  $.ajax({
			  type:"POST",
			  url:context_path + '/artBom/saveDetail',
			  data:{bomId:$('#baseInfor #bomId').val(),materialIds:selectData.toString(),routeId:$("#routeId").val()},
			  dataType:"json",
			  success:function(data){
				  removeChoice();   //清空下拉框中的值
				  if(Boolean(data.result)){
						layer.msg("添加成功",{icon:1,time:1200});
					  //重新加载详情表格
					  reloadDetailTableList();  
				  }else{
					  layer.msg(data.msg,{icon:2,time:1200});
				  }
			  }
		  });
  }
  
  //单元格编辑成功后，回调的函数
	var editFunction = function eidtSuccess(XHR){
		 var data = eval("("+XHR.responseText+")"); 
		if(data["msg"]!=""){
			layer.alert(data["msg"]);
		}
		jQuery("#grid-table-c").jqGrid('setGridParam', 
				{
					postData: {
						id:$('#bomId').val(),
						queryJsonString:""
					} 
				}
		  ).trigger("reloadGrid");
	};
	
  
  	_grid_detail=jQuery("#grid-table-c").jqGrid({
         url : context_path + '/artBom/detailList',
         datatype : "json",
         data:{bomId:$("#baseInfor #bomId").val()},
         colNames : [ '详情主键','物料编号','物料名称','库位','消耗数量','安全库存','工时(单位:秒)','工位'],
         colModel : [ 
  					  {name : 'id',index : 'id',width : 20,hidden:true}, 
  					  {name : 'materialNo',index:'materialNo',width :20}, 
                      {name : 'materialName',index:'materialName',width : 20}, 
                      {name : 'locationId',index:'locationId',width : 20,
                         editable: true,
		                 edittype: "select",
		                 editoptions:{
		                 value:gettypesLocation(),  
		                 dataEvents:[{
		                     type:'focus',
		                     fn:function(e){
		                     var id  = $('#grid-table-c').jqGrid('getGridParam','selrow');
		                     $("#grid-table-c select option").remove();
		                     $.ajax({
						            type: "post",
						            async: false,
						            url: context_path + "/artBom/getXBLocation",
								    data:{id:id},
						            dataType: "json",
						            success: function (data) {
						                for (let i = 0; i < data.result.length; i++) {
						                     $("#grid-table-c select").prepend("<option value="+data.result[i].id+">"+data.result[i].locationName+"</option>")
						                }
						            }
						        })
		                     }
		                     },
		                     {
	                                  type: 'blur',     //blur,focus,change.............
	                                  fn: function (e) {
	                                	  var $element = e.currentTarget;
	                                	  var $elementId = $element.id;
	                                	  var rowid=$("#"+$elementId).parent().parent().attr("id");
	                                		  var reg = new RegExp("^[0-9]+(.[0-9]{1,2})?$");
	                                		  if (!reg.test($("#"+$elementId).val())) {
	                                			  layer.alert("非法的数量！(注：可以有两位小数的正实数)");
	                                			  return;
	                                		  }
	                                		  var locationId=$("#"+$elementId).val()
	                                	      modValue(rowid,"locationId",locationId)
	                                  }
	                              }
		                 ],
		                }, 
		                
		                formatter: function (cellValu, option, rowObject) {
		                    var vle = cellValu || "";
		                    if (select_dataLocation && select_dataLocation.result) {
		                         var flag=false;
		                        for (var i = 0; i < select_dataLocation.result.length; i++) {
		                            if (select_dataLocation.result[i].id == cellValu) {
		                                vle = select_dataLocation.result[i].locationName;
		                                flag=true;
		                            }
		                        }
		                        if(!flag){
		                          vle="<span style='color:red'>未绑定</span>";
		                        }
		                    }
		                    return vle;
		                }
                      
                      }, 
                      {name: 'consumeAmount', index: 'consumeAmount', width: 20,editable : true,editrules: {custom: true, custom_func: numberRegex},
                        editoptions: {
	                          size: 25,
	                          dataEvents: [
	                              {
	                                  type: 'blur',     //blur,focus,change.............
	                                  fn: function (e) {
	                                	  var $element = e.currentTarget;
	                                	  var $elementId = $element.id;
	                                	  var rowid=$("#"+$elementId).parent().parent().attr("id");
	                                		  var reg = new RegExp("^[0-9]+(.[0-9]{1,2})?$");
	                                		  if (!reg.test($("#"+$elementId).val())) {
	                                			  layer.alert("非法的数量！(注：可以有两位小数的正实数)");
	                                			  return;
	                                		  }
	                                		  var consumeAmount=$("#"+$elementId).val()
	                                	      modValue(rowid,"consumeAmount",consumeAmount)
	                                  }
	                              }
	                          ]
	                      },
	                      /* formatter: function (cellValu, option, rowObject) {
	                         if(cellValu==0){
	                            return "<span style='color:red'>0</span>";
	                         }else{
	                           return cellValu;
	                         }
	                      } */
                      },
                      {name : 'alarmAmount',index:'alarmAmount',width : 20,
                        editable : true,editrules: {custom: true, custom_func: numberRegex},
                        editoptions: {
	                          size: 25,
	                          dataEvents: [
	                              {
	                                  type: 'blur',     //blur,focus,change.............
	                                  fn: function (e) {
	                                	  var $element = e.currentTarget;
	                                	  var $elementId = $element.id;
	                                	  var rowid=$("#"+$elementId).parent().parent().attr("id");
	                                	 
	                                		  var reg = new RegExp("^[0-9]+(.[0-9]{1,2})?$");
	                                		  if (!reg.test($("#"+$elementId).val())) {
	                                			  layer.alert("非法的数量！(注：可以有两位小数的正实数)");
	                                			  return;
	                                		  }
	                                		  var alarmAmount=$("#"+$elementId).val();
	                                	      modValue(rowid,"alarmAmount",alarmAmount)
	                                  }
	                              }
	                          ]
	                      }
                      },
  					  {name : 'workTime',index:'workTime',width :20,editable : true,editrules: {custom: true, custom_func: numberRegex},
                        editoptions: {
	                          size: 25,
	                          dataEvents: [
	                              {
	                                  type: 'blur',     //blur,focus,change.............
	                                  fn: function (e) {
	                                	 var $element = e.currentTarget;
	                                	  var $elementId = $element.id;
	                                	  var rowid=$("#"+$elementId).parent().parent().attr("id");
	                                		  var reg = new RegExp("^[0-9]+(.[0-9]{1,2})?$");
	                                		  if (!reg.test($("#"+$elementId).val())) {
	                                			  layer.alert("非法的数量！(注：可以有两位小数的正实数)");
	                                			  return;
	                                		  }
	                                		  var workTime=$("#"+$elementId).val();
	                                	      modValue(rowid,"workTime",workTime)
	                                  }
	                              }
	                          ]
	                      },
  					     cellattr: function(rowId, tv, rawObject, cm, rdata) {
                            //合并单元格
                            return 'id=\'workTime' + rowId + "\'";
                        } 
  					  },
  					  {name : 'routeName',index:'routeName',width :20,
  					    cellattr: function(rowId, tv, rawObject, cm, rdata) {
                            //合并单元格
                            return 'id=\'routeName' + rowId + "\'";
                        } 
  					  }
                    ],
         rowNum : 20,
         rowList : [ 10, 20, 30 ],
         pager : '#grid-pager-c',
         sortname : 'ID',
         sortorder : "asc",
         altRows: true,
         viewrecords : true,
       //caption : "工艺BOM详情",
         autowidth:true,
         multiselect:true,
		 multiboxonly: true,
         loadComplete : function(data){
             var table = this;
             setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
             oriDataDetail = data;
             merge();
             $(window).triggerHandler('resize.jqGrid');
         },
       /* cellurl : context_path + "/ASNmanage/updateAmount", */
	   cellEdit: true,
	   cellsubmit : "clientArray",
  	   emptyrecords: "没有相关记录",
  	   loadtext: "加载中...",
  	   pgtext : "页码 {0} / {1}页",
  	   recordtext: "显示 {0} - {1}共{2}条数据",
    });
    //在分页工具栏中添加按钮
    $("#grid-table-c").navGrid('#grid-pager-c',{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd('#grid-pager-c',{  
  	   caption:"",   
  	   buttonicon:"ace-icon fa fa-refresh green",   
  	   onClickButton: function(){   
  	    $("#grid-table-c").jqGrid('setGridParam', 
				{
  	    			url:context_path + '/artBom/detailList?bomId='+$("#baseInfor #bomId").val(),
					postData: {queryJsonString:""} //发送数据  :选中的节点
				}
		  ).trigger("reloadGrid");
  	   }
  	});  	
  	$(window).on("resize.jqGrid", function () {
  		$("#grid-table-c").jqGrid("setGridWidth", $("#grid-div-c").width() - 3 );
  		var height = $(".layui-layer-title",_grid_detail.parents(".layui-layer")).height()+
                     $("#baseInfor").outerHeight(true)+$("#materialDiv").outerHeight(true)+
                     $("#grid-pager-c").outerHeight(true)+$("#fixed_tool_div.fixed_tool_div.detailToolBar").outerHeight(true)+
                   //$("#gview_grid-table-c .ui-jqgrid-titlebar").outerHeight(true)+
                     $("#gview_grid-table-c .ui-jqgrid-hbox").outerHeight(true);
                     $("#grid-table-c").jqGrid("setGridHeight",_grid_detail.parents(".layui-layer").height()-height);
  	});
  	$(window).triggerHandler("resize.jqGrid");
  	if($("#id").val()!=""){
  		//reloadDetailTableList();   //重新加载详情列表
  	}
       //合并工位与工时列
    function merge() {
       var CellName="routeName";
           //得到显示到界面的id集合
           var mya = $("#grid-table-c").getDataIDs();
           //当前显示多少条
           var length = mya.length;
           for (var i = 0; i < length; i++) {
               //从上到下获取一条信息
               var before = $("#grid-table-c").jqGrid('getRowData', mya[i]);
               //定义合并行数
               var rowSpanTaxCount = 1;
               for (j = i + 1; j <= length; j++) {
                   //和上边的信息对比 如果值一样就合并行数+1 然后设置rowspan 让当前单元格隐藏
                   var end = $("#grid-table-c").jqGrid('getRowData', mya[j]);
                   if (before[CellName] == end[CellName]) {
                       rowSpanTaxCount++;
                       $("#grid-table-c").setCell(mya[j], CellName, '', { display: 'none' });
                       $("#grid-table-c").setCell(mya[j], "workTime", '', { display: 'none' });
                   } else {
                       rowSpanTaxCount = 1;
                       break;
                   }
                   //合并工位列
                   $("#" + CellName + "" + mya[i] + "").attr("rowspan", rowSpanTaxCount);
                   //合并工时列
                   $("#workTime" + mya[i] + "").attr("rowspan", rowSpanTaxCount);
               }
           }
       }
   //修改 指定列的值
  function modValue(id,colname,value){
      $.ajax({
       		  url:context_path + '/artBom/updateInfo',
       		  type:"POST",
       		  data:{id:id,colname:colname,value:value},
       		  dataType:"json",
       		  success:function(data){
       		        if(!data.result){
       		           layer.alert(data.msg);
       		        }
       		       reloadDetailTableList();
       		      
       		  }
	     }); 
  }
//将数据格式化成两位小数：四舍五入
 function formatterNumToFixed(value,options,rowObj){
	if(value!=null){
		var floatNum = parseFloat(value);
		return floatNum.toFixed(2);
	}else{
		return "0.00";
	}
 } 
 //数量输入验证
function numberRegex(value, colname) {
    var regex = /^\d+\.?\d{0,2}$/;
    if (!regex.test(value)) {
        return [false, ""];
    }
    else  return [true, ""];
}
  function reloadDetailTableList(){
      
	  $("#grid-table-c").jqGrid('setGridParam', 
							{
								postData: {bomId:$("#baseInfor #bomId").val()} //发送数据  :选中的节点
							}
					  ).trigger("reloadGrid");
	}
	$(document).keyup(function(event){
  if(event.keyCode ==13){
    reloadDetailTableList();
  }
});
function gettypesLocation() {
        var str = "";
        $.ajax({
            type: "post",
            async: false,
            url: context_path + "/artBom/getXBLocation",
			data:{type:'WMS_LOCATION_XB'},
            dataType: "json",
            success: function (data) {
                select_dataLocation = data;
                for (let i = 0; i < data.result.length; i++) {
                    str += data.result[i].id + ":" + data.result[i].locationName + ";";
                }
                str = str.substring(0, str.length - 1);
            }
        })
        return str;
    }
    //工具栏
	  $("#__toolbar__-c").iToolBar({
	   	 id:"__tb__01",
	   	 items:[
	   	  	{label:"删除", onclick:delDetail},
	    ]
	  });
	//删除物料详情
	function delDetail(){
	   var ids = jQuery("#grid-table-c").jqGrid('getGridParam', 'selarrrow');
	   $.ajax({
	      url:context_path + '/artBom/deleteDetail?ids='+ids,
	      type:"POST",
	      dataType:"JSON",
	      success:function(data){
	         if(data.result){
	           layer.msg("操作成功！");
	            //重新加载详情表格
				reloadDetailTableList();
	         }
	      }
	   });
	}
	if($("#baseInfor #bomId").val()!=""){
	   $.ajax({
	      url:context_path + '/artBom/getArtBom?id='+$("#baseInfor #bomId").val(),
	      type:"POST",
	      dataType:"JSON",
	      success:function(data){
	         $("#productId").select2('data' ,{
	            id:data.artBom.productId,
	            text:data.artBom.productName
	         });
	          $("#modelId").select2('data' ,{
	            id:data.artBom.modelId,
	            text:data.artBom.modelName
	         });
	         reloadDetailTableList();
	       }
	      });
	}
  </script>
