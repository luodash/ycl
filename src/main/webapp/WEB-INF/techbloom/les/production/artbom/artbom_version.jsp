<%@ page language="java" pageEncoding="UTF-8"%>
<div class="row-fluid" style="height: inherit;">
  	 <form id="vsersionForm" class="form-horizontal"  style="overflow: auto; height: calc(100% - 270px);">
		<div class="control-group">
			<label class="control-label" for="versionName">版本名称：</label>
			<div class="controls">
				<div class="input-append span11 required" > 
			      <input class="span11" type="hidden"  name="bomId" id="bomId" value="${artBom.id}">
			      <input class="span11" type="text"  name="versionName" id="versionName" placeholder="版本名称">
			    </div> 
			</div>
		</div>
	</form>
	<div class="form-actions" style="text-align: right;border-top: 0px; margin: 0px;">
		<button class="btn btn-success" onclick="sureVersion();return false;">确定</button>
		<button class="btn btn-danger" onclick="layer.close($queryWindow);return false;">关闭</button>
	</div>
</div>
<script>
/* (function(){ */
   $("#vsersionForm").validate({
   ignore: "", 
   rules:{
      "versionName":{
         required:true,
          remote:{
            cache:false,
            async:false,
            url: context_path+"/artBom/isHaveVersion?id="+$('#vsersionForm #bomId').val()
         }
      },
      
   },
   messages: {
    "versionName":{
         required:"请填写bom版本名称",
         remote:"版本名称重复"
      },
   },
  errorClass: "help-inline",
    errorElement: "span",
    highlight:function(element, errorClass, validClass) {
        $(element).parents('.control-group').addClass('error');
    },
    unhighlight: function(element, errorClass, validClass) {
        $(element).parents('.control-group').removeClass('error');
    }
});
/* }); 
 */
//保存/修改
function sureVersion(){
     if($("#vsersionForm").valid()){
      $.ajax({
			url:context_path+"/artBom/saveVersion?tm="+new Date(),
			type:"POST",
			data:{id:$("#vsersionForm #bomId").val(),versionName:$("#vsersionForm #versionName").val()},
			dataType:"JSON",
			success:function(data){
				if(data.result){
					//关闭当前窗口
					layer.close($queryWindow)
					layer.msg(data.msg,{icon:1});
				}else{
					layer.alert(data.msg,{icon:2});
				}
			}
		});	
    }
	  

}

</script>