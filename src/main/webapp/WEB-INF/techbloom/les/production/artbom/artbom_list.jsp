<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
	<script type="text/javascript">
	    var context_path = '<%=path%>';
	</script>
	<style type="text/css">
	.query_box .field-button.two {
    padding: 0px;
    left: 650px;
    }
	</style>
</head>
<body style="overflow:hidden;">
   <div id="grid-div">
	   <form id="hiddenForm" action = "<%=path%>/artBom/exportExcel" method = "POST" style="display: none;">
		   <!-- 选中的用户 -->
		   <input id="ids" name="ids" value=""/>
	   </form>
		<div class="query_box" id="yy" title="查询选项">
            <form id="queryForm" style="max-width:100%;">
			 <ul class="form-elements">
				 <li class="field-group field-fluid3">
					 <label class="inline" for="version" style="margin-right:20px;width:100%;">
						 <span class="form_label" style="width:80px;">版本：</span>
						 <select id="version" class="mySelect2" style="width: calc(100% - 85px);" name="isquery">
							 <option value="">所有版本</option>
							 <option value="false">初始版本</option>
						 </select>
						 <%--<input type="text" id="version" name="version" value="" style="width: 200px;" placeholder="版本">--%>
					 </label>
				 </li>
				 <li class="field-group field-fluid3">
					<label class="inline" for="bomNo" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:80px;">BOM编号：</span>
						<input type="text" name="bomNo" id="bomNo" value="" style="width: calc(100% - 85px);" placeholder="BOM编号">
					</label>
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="bomName" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:80px;">BOM名称：</span>
						<input type="text" name="bomName" id="bomName" value="" style="width: calc(100% - 85px);" placeholder="BOM名称">
					</label>
				</li>
				<li class="field-group-top field-group field-fluid3">
					<label class="inline" for="modelId" style="margin-right:20px;width:100%">
						<span class="form_label" style="width:80px;">生产流程：</span>
						<input type="text" id="process" name="modelId" value="" style="width: calc(100% - 85px);" placeholder="生产流程">
					</label>
				</li>
			
			</ul>
				<div class="field-button" style="">
						<div class="btn btn-info" onclick="queryOk();">
				            <i class="ace-icon fa fa-check bigger-110"></i>查询
			            </div>
						<div class="btn" onclick="reset();"><i class="ace-icon icon-remove"></i>重置</div>
						<a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>
		        </div>
		  </form>
    </div>
        <div id="fixed_tool_div" class="fixed_tool_div">
             <div id="__toolbar__" style="float:left;overflow:hidden;"></div>
        </div>
		<table id="grid-table" style="width:100%;height:100%;"></table>
		<div id="grid-pager"></div>
   </div>
</body>
<script src="<%=request.getContextPath()%>/static/js/techbloom/les/production/artbom/artbom.js"></script>
<script type="text/javascript">
$('#queryForm .mySelect2').select2();
$("#__toolbar__").iToolBar({
    id: "__tb__01",
    items: [
            {label: "添加", disabled: ( ${sessionUser.addQx } == 1 ? false : true), onclick:openAddPage, iconClass:'glyphicon glyphicon-plus'},
            {label: "编辑", disabled: ( ${sessionUser.editQx} == 1 ? false : true),onclick: openEditPage, iconClass:'glyphicon glyphicon-pencil'},
            {label: "删除", disabled: ( ${sessionUser.deleteQx} == 1 ? false : true),onclick: deleteBom, iconClass:'glyphicon glyphicon-trash'},
            {label: "查看", disabled: ( ${sessionUser.deleteQx} == 1 ? false : true),onclick: versionView, iconClass:'icon-zoom-in'},
			{label:"导出",disabled:(${sessionUser.queryQx}==1?false:true),onclick:excelartbom,iconClass:'icon-share'}
           ]
});
$(function  (){
    $(".toggle_tools").click();
});
var _queryForm_data = iTsai.form.serialize($('#queryForm'));
function queryOk(){
	if($('#allVersion').is(':checked')){
	   $("#isquery").val("true");
	}else{
	    $("#isquery").val("false");
	}
		//var formJsonParam = $('#queryForm').serialize();
		var queryParam = iTsai.form.serialize($('#queryForm'));
		//执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
		queryInstoreListByParam(queryParam);
	}
function reset(){
		iTsai.form.deserialize($('#queryForm'),_queryForm_data);
		$("#queryForm #process").select2("val","");
		$("#allocateTypeSelect").val("").trigger('change');
		$("#inWarehouseSelect").val("").trigger('change');
		$("#outWarehouseSelect").val("").trigger('change');
		$("#queryForm #version").val("").select2();
		//var queryParam = iTsai.form.serialize($('#queryForm'));
		//执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
		queryInstoreListByParam(_queryForm_data);
		$("#allocateTypeSelect").find("option[text='所有类型']").attr("selected",true);
		$("#outWarehouseSelect").find("option[text='所有库区']").attr("selected",true);
		$("#inWarehouseSelect").find("option[text='所有库区']").attr("selected",true);
	}
	$("#queryForm #process").select2({
			placeholder : "请选择生产流程",//文本框的提示信息
			minimumInputLength : 0, //至少输入n个字符，才去加载数据
			allowClear : true, //是否允许用户清除文本信息
			closeOnSelect:false,
			ajax : {
				url : context_path + '/artBom/getModel',
				dataType : 'json',
				delay : 250,
				data : function(term, pageNo) { //在查询时向服务器端传输的数据
					term = $.trim(term);
					selectParam = term;
					return {
						/* docId : $("#baseInfor #id").val(), */
						queryString : term, //联动查询的字符
						pageSize : 15, //一次性加载的数据条数
						pageNo : pageNo, //页码
						time : new Date(),
						inPlanId:$("#baseInfor #inPlanId").val()
					//测试
					}
				},
				results : function(data, pageNo) {
					var res = data.result;
					if (res.length > 0) { //如果没有查询到数据，将会返回空串
						var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
						return {
							results : res,
							more : more
						};
					} else {
						return {
							results : {
								"id" : "0",
								"text" : "没有更多结果"
							}
						};
					}

				},
				cache : true
			}
		});
</script>