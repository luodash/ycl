<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="grid-div">
    <form id="hiddenForm" action="<%=path%>/productionLine/toExcel" method="POST" style="display: none;">
        <input id="ids" name="ids" value=""/>
    </form>
    <!-- 隐藏区域：存放查询条件 -->
    <form id="hiddenQueryForm" style="display:none;">
        <input id="lineNo" name="lineNo" value=""/>
        <input id="lineName" name="lineName" value=""/>
        <input id="device" name="device" value=""/>
    </form>
     <div class="query_box" id="yy" title="查询选项">
            <form id="queryForm" style="max-width:100%;">
			 <ul class="form-elements">
				<li class="field-group field-fluid3">
					<label class="inline" for="lineNo" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:80px;">产线编号：</span>
						<input type="text" name="lineNo" id="lineNo" value="" style="width: calc(100% - 85px);" placeholder="产线编号">
					</label>			
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="lineName" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:80px;">产线名称：</span>
						<input type="text" name="lineName" id="lineName" value="" style="width: calc(100% - 85px);" placeholder="产线名称">
					</label>					
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="device" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:80px;">设备：</span>
						<input type="text" name="device" id="device" value="" style="width: calc(100% - 85px);" placeholder="设备">
					</label>					
				</li>
				
			</ul>
			<div class="field-button" style="">
						<div class="btn btn-info" onclick="queryOk();">
				            <i class="ace-icon fa fa-check bigger-110"></i>查询
			            </div>
						<div class="btn" onclick="reset();"><i class="ace-icon icon-remove"></i>重置</div>
		        </div>
		  </form>		 
    </div>
    <div id="fixed_tool_div" class="fixed_tool_div">
        <div id="__toolbar__" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="grid-table" style="width:100%;margin: 0px !important;boder:0px !important"></table>
    <div id="grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriData;
    var _grid;
    var dynamicDefalutValue="c5c0897632d845a088fca56db9fab7e3";//列表码
    $(function  (){
     //   $(".toggle_tools").click();
    });
    $("#__toolbar__").iToolBar({
        id: "__tb__01",
        items: [
            {label: "添加", disabled: ( ${sessionUser.addQx } == 1 ? false : true), onclick:addLine, iconClass:'glyphicon glyphicon-plus'},
            {label: "编辑", disabled: ( ${sessionUser.editQx} == 1 ? false : true),onclick: editLine, iconClass:'glyphicon glyphicon-pencil'},
            {label: "删除", disabled: ( ${sessionUser.deleteQx} == 1 ? false : true),onclick: deleteLine, iconClass:'glyphicon glyphicon-trash'},
            {label: "导出", disabled: ( ${sessionUser.queryQx}==1?false:true),onclick:function(){toExcel();},iconClass:' icon-share'}
           ]
    });

    $(function () {
        _grid = jQuery("#grid-table").jqGrid({
            url: context_path + "/productionLine/list.do",
            datatype: "json",
            colNames: ["主键", "产线名称", "产线编号", "绑定设备IP", "备注","状态"],
            colModel: [
                {name: "id", index: "id", width: 20, hidden: true},
                {name: "lineName", index: "lineName", width: 60},
                {name: "lineNo",index: "lineNo",width:60},
                {name: "device",index: "device",width:65},
                {name: "remark", index: "remark", width: 60},
                {name : "status",index:"status",width:50,formatter:function(cellvalue,option,rowObject){
                    if(cellvalue==0){
                        return "<span style='color:grey;font-weight:bold;'>未提交</span>";
                    }
                    if(cellvalue==1){
                        return "<span style='color:green;font-weight:bold;'>已提交</span>";
                    }           
                  }
                }
            ],
            rowNum: 20,
            rowList: [10, 20, 30],
            pager: "#grid-pager",
            sortname: "lineNo",
            sortorder: "asc",
            altRows: true,
            viewrecords: true,
            autowidth: true,
            multiselect: true,
            multiboxonly: true,
            beforeRequest:function (){
                dynamicGetColumns(dynamicDefalutValue,"grid-table",$(window).width()-$("#sidebar").width() -7);
                //重新加载列属性
            },
            loadComplete: function (data) {
                var table = this;
                setTimeout(function () {
                    updatePagerIcons(table);
                    enableTooltips(table);
                }, 0);
                oriData = data;
            },
            emptyrecords: "没有相关记录",
            loadtext: "加载中...",
            pgtext: "页码 {0} / {1}页",
            recordtext: "显示 {0} - {1}共{2}条数据"
        });
        //在分页工具栏中添加按钮
        jQuery("#grid-table").navGrid("#grid-pager", {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false
        }).navButtonAdd("#grid-pager", {
                    caption: "",
                    buttonicon: "ace-icon fa fa-refresh green",
                    onClickButton: function () {
                        $("#grid-table").jqGrid("setGridParam",
                                {
                                    postData: {queryJsonString: ""} //发送数据
                                }
                        ).trigger("reloadGrid");
                    }
                }).navButtonAdd("#grid-pager",{
                caption: "",
                buttonicon:"fa  icon-cogs",
                onClickButton : function (){
                    jQuery("#grid-table").jqGrid("columnChooser",{
                        done: function(perm, cols){
                            dynamicColumns(cols,dynamicDefalutValue);
                            $("#grid-table").jqGrid("setGridWidth", $("#grid-div").width());
                        }
                    });
                }
            });
        $(window).on("resize.jqGrid", function () {
            $("#grid-table").jqGrid("setGridWidth", $("#grid-div").width() );
            $("#grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-$("#yy").outerHeight(true)-$("#fixed_tool_div").outerHeight(true)-$("#grid-pager").outerHeight(true)
           -$("#gview_grid-table .ui-jqgrid-hdiv").outerHeight(true));
        });

        $(window).triggerHandler("resize.jqGrid");
    });
    var _queryForm_data = iTsai.form.serialize($("#queryForm"));
	function queryOk(){
		var queryParam = iTsai.form.serialize($("#queryForm"));
		queryLineByParam(queryParam);		
	}
	function queryLineByParam(jsonParam) {
    iTsai.form.deserialize($("#hiddenQueryForm"), jsonParam);
    var queryParam = iTsai.form.serialize($("#hiddenQueryForm"));
    var queryJsonString = JSON.stringify(queryParam); 
    $("#grid-table").jqGrid("setGridParam",
        {
            postData: {queryJsonString: queryJsonString}
        }).trigger("reloadGrid");
    }
	function reset(){
		$("#queryForm #lineNo").val("").trigger("change");
        $("#queryForm #lineName").val("").trigger("change");
        $("#queryForm #device").select2("val","");
	    $("#grid-table").jqGrid("setGridParam", {
            postData: {queryJsonString:""}
        }).trigger("reloadGrid");		
	}
	function addLine(){
		$.post(context_path + "/productionLine/toEdit.do", {}, function (str){
		$queryWindow=layer.open({
		    title : "产线添加", 
	    	type:1,
	    	skin : "layui-layer-molv",
	    	area : "750px",
	    	shade : 0.6, //遮罩透明度
		    moveType : 1, //拖拽风格，0是默认，1是传统拖动
		    anim : 2,
		    content : str,
		    success: function (layero, index) {
                layer.closeAll("loading");
            }
		});
	 });
	}
	function editLine(){
	    var checkedNum = getGridCheckedNum("#grid-table", "id");
    if (checkedNum == 0) {
       layer.alert("请选择一个要编辑的产线！");
        return false;
    } else if (checkedNum > 1) {
    	layer.alert("只能选择一个产线进行编辑操作！");
        return false;
    } else {
        var id = jQuery("#grid-table").jqGrid("getGridParam","selrow");
        $.post(context_path + "/productionLine/toEdit.do?id="+id, {}, function (str){
    		$queryWindow=layer.open({
    		    title : "产线编辑", 
    	    	type:1,
    	    	skin : "layui-layer-molv",
    	    	area : "750px",
    	    	shade : 0.6, //遮罩透明度
    		    moveType : 1, //拖拽风格，0是默认，1是传统拖动
    		    anim : 2,
    		    content : str,
    		    success: function (layero, index) {
                    layer.closeAll("loading");
                }
    		});
    	});
      }
	}
	function deleteLine(){
	    var checkedNum = getGridCheckedNum("#grid-table", "id");  //选中的数量
        if (checkedNum == 0) {
           layer.alert("请选择一条要删除的产线！");
        } else {
        var ids = jQuery("#grid-table").jqGrid("getGridParam", "selarrrow");
        layer.confirm("确定删除选中的产线？", function() {
    		$.ajax({
    			type : "post",
    			url : context_path + "/productionLine/deleteLine.do?ids=" + ids,
    			dataType : "json",
    			cache : false,
    			success : function(data) {
    				layer.closeAll();
    				if (Boolean(data.result)) {
    					layer.msg(data.msg, {icon: 1,time:1000});
    				}else{
    					layer.msg(data.msg, {icon: 7,time:2000});
    				}
    				_grid.trigger("reloadGrid");  //重新加载表格
    			}
    		});
    	});
     }
	}
	$("#queryForm #device").select2({
        placeholder: "选择设备",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
       
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/productionLine/getDeviceList",
            type: "POST",
            dataType: "json",
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });
    function toExcel(){
        var ids = jQuery("#grid-table").jqGrid("getGridParam", "selarrrow");
        $("#hiddenForm #ids").val(ids);
        $("#hiddenForm").submit();	
    }
</script>
</html>