<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<div class="row-fluid" style="height: inherit;">
    <form id="routeForm" class="form-horizontal" style="overflow: auto; height: calc(100% - 70px);">
        <input type="hidden" id="id" name="id" value="${route.id }">
        <div class="control-group">
            <label class="control-label" for="routeNo">工位编号：</label>
            <div class="controls">
                <div class="input-append span12 required">
                    <c:choose>
						<c:when test="${edit==1}">
							<input type="text" class="span11" id="routeNo" name="routeNo" value="${route.routeNo }" placeholder="工位编号" readonly="readonly">
						</c:when>
						<c:otherwise>
							<input type="text" class="span11" id="routeNo" name="routeNo" placeholder="后台自动生成" readonly="readonly">
						</c:otherwise>
					</c:choose>
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="routeName">工位名称：</label>
            <div class="controls">
                <div class="input-append span12 required">
                    <input class="span11" type="text" name="routeName" id="routeName" value="${route.routeName }" placeholder="工位名称">
                </div>
            </div>
        </div>
        <div class="control-group">
			<label class="control-label" for="ip">绑定设备：</label>
			<div class="controls">
				<div class="required span12" style=" float: none !important;">
					<input type="text" class="span11" id="ip" name="ip" placeholder="绑定设备">
				</div>
			</div>
		</div>
        <div class="control-group">
            <label class="control-label" for="shelveId">货架：</label>
            <div class="controls">
                <div class="required">
                    <input class="span11" type="text" name="shelveId" id="shelveId" value="${route.shelveId }" placeholder="货架">
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="allocations">库位：</label>
            <div class="controls">
                <div class="required">
                    <input class="span11" type="text" name="allocations" id="allocations" placeholder="库位">
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="remark">备注：</label>
            <div class="controls">
                <input class="span11" type="text" name="remark" id="remark" value="${route.remark }" placeholder="备注">
            </div>
        </div>
    </form>
    <div class="field-button" style="text-align: center;border-top: 0px;margin: 15px auto;">
		<span class="btn btn-info" onclick="saveForm();">
		   <i class="ace-icon fa fa-check bigger-110"></i>保存
		</span>
		<span class="btn btn-danger" onclick="layer.closeAll();">
		   <i class="icon-remove"></i>&nbsp;取消
		</span>
	</div>
</div>
<script>
    var shelveId = $("#shelveId").val();
    if (openwindowtype == 1) {
        $.ajax({
            url: context_path + "/route/getRouteBeanById",
            type: "POST",
            dataType: "JSON",
            success: function (data) {
                if (data) {
                    for (var bomattr in data) {
                        $("#" + bomattr + "1").val(data[bomattr]);
                        if (bomattr == "shelf_id") {
                            shelveId = data[bomattr];
                        }
                    }
                }
                getIp();
                getLocationId();
            }
        });
    }
    $("#routeForm").validate({
        ignore: "",
        focusInvalid : true,
        rules: {
            "routeName": {
                required: true,
                maxlength: 32,
                remote: context_path + "/route/hasRouteName.do?id="+$("#routeid").val()+""
            },
          "shelf":{
               required:true,
            },
            "allocations":{
               required:true,
            },
            "remark": {
                maxlength: 256
            }
        },
        messages: {
            "routeName": {
                required:"工位名称不能为空!",
                remote: "工位名称重复!"
            },
           "allocations":{
               required:"请选择库位",
            },
            "shelf":{
               required:"请选择货架",
            },
            "remark": {
                maxlength: "备注不能超过256个字符！"
            }
        },
        errorClass: "help-inline",
        errorElement: "span",
        highlight: function (element, errorClass, validClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        }
    });
    //下拉框初始化
    //货架
    $("#shelveId").select2({
        placeholder: "选择货架",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/route/selectShelf",
            type: "POST",
            dataType: "json",
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }

    });

    $("#allocations").select2({
        placeholder: "选择库位",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        multiple: true,
        ajax: {
            url: context_path + "/route/getDeviceStuff",
            type: "POST",
            dataType: "json",
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    shelveId: $("#shelveId").val()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }

    });
  $.ajax({
	 type:"post",
	 url:context_path + "/route/findRouteById",
	 data:{id:$("#routeForm #id").val()},
	 dataType:"json",
	 success:function(data){
			  $("#routeForm #shelveId").select2("data", {
 			  id: data.shelveId,
 			  text: data.shelveName
             }); 
             $("#routeForm #ip").select2("data", {
 			  id: data.ip,
 			  text: data.deviceName
             });
		}
	});
   $("#productline_id").change(function () {
       $("#shelf").select2("val", ""); 
       $("#allocations").select2("val", "");
    });
    $("#shelf").change(function () {
        $("#allocations").select2("val", ""); 
        shelveId = $("#shelf").val();
    });
    $("#allocations").change(function () {
        if ($("#shelveId").val() == "") {
            layer.alert("请先选择货架");
            $("#allocations").select2("val", "");
        }
    })

    function getLocationId() {
        $.ajax({
            url: context_path + "/route/getLocationId",
            type: "post",
            dataType: "JSON",
            data: {
                id: routeid
            },
            async: false,
            success: function (data) {
                if (data) {
                    if (data.line) {
                        var obj = [];
                        for (let i = 0; i < data.line.length; i++) {
                            obj.push({
                                id: data.line[i].modelId,
                                text: data.line[i].processName
                            });
                        }
                        $("#allocations").select2("data", obj);
                    }
                }
            }
        });
    }


    function saveForm() {
        var bean = $("#routeForm").serialize();
        if ($("#routeForm").valid()) {
            saveOrUpdateForm(bean);
        }

    }

    function saveOrUpdateForm(bean) {
        $.ajax({
            url: context_path + "/route/saveRoute.do",
            type: "POST",
            data: bean,
            dataType: "JSON",
            success: function (data) {
                if (data) {
                    layer.closeAll();
                    layer.msg("保存成功！", {icon: 1});
	                $("#grid-table").jqGrid("setGridParam", {
                       postData: {queryJsonString:""}
                    }).trigger("reloadGrid");
                } else {
                    layer.msg("保存失败！", {icon: 2});
                }
            }
        });
    }
$.ajax({
	    type:"POST",
		url:context_path + "/route/getLocationInfo?routeId="+$("#routeForm #id").val(),
	    dataType:"json",
	    success:function(data){
	       if (data) {
                    if (data.info) {
                        var obj = [];
                        for (let i = 0; i < data.info.length; i++) {
                            obj.push({
                                id: data.info[i].id,
                                text: data.info[i].text
                            });
                        }
                        $("#routeForm #allocations").select2("data", obj);
                    }
                }
	    }
	  });
	  $("#routeForm #ip").select2({
        placeholder: "选择设备",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        width: 435,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/route/getDeviceId",
            type: "POST",
            dataType: "json",
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });
</script>