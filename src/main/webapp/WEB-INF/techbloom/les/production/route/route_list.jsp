<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="grid-div">
    <form id="hiddenForm" action="<%=path%>/route/toExcel" method="POST" style="display: none;">
        <input id="ids" name="ids" value=""/>
    </form>
    <!-- 隐藏区域：存放查询条件 -->
    <form id="hiddenQueryForm" style="display:none;">
        <!-- 客户编号 -->
        <input name="routeNo" id="routeNo" value=""/>
        <!-- 客户名称-->
        <input id="routeName" name="routeName" value="">
    </form>
    <div class="query_box" id="yy" title="查询选项">
            <form id="queryForm" style="max-width:100%;">
			 <ul class="form-elements">
				<li class="field-group field-fluid3">
					<label class="inline" for="routeNo" style="margin-right:20px;width: 100%;">
						<span class="form_label" style="width:80px;">工位编号：</span>
						<input type="text" name="routeNo" id="routeNo" value="" style="width: calc(100% - 85px);" placeholder="工位编号">
					</label>			
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="routeName" style="margin-right:20px;width: 100%;">
						<span class="form_label" style="width:80px;">工位名称：</span>
						<input type="text" name="routeName" id="routeName" value="" style="width: calc(100% - 85px);" placeholder="工位名称">
					</label>					
				</li>
				
			</ul>
			<div class="field-button" style="">
					<div class="btn btn-info" onclick="queryOk();">
				        <i class="ace-icon fa fa-check bigger-110"></i>查询
			        </div>
					<div class="btn" onclick="reset();"><i class="ace-icon icon-remove"></i>重置</div>
					<%--<a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>--%>
		        </div>
		 </form>		 
    </div>
    <div id="fixed_tool_div" class="fixed_tool_div">
        <div id="__toolbar__" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="grid-table" style="width:100%;margin: 0px !important;boder:0px !important"></table>
    <div id="grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriData;
    var _grid;
    var openwindowtype;
    var dynamicDefalutValue="0dfec398ed0b4e96976bb13c35825b8f";//列表码
    $(function  (){
        $(".toggle_tools").click();
    });
    $("#__toolbar__").iToolBar({
        id: "__tb__01",
        items: [
            {label: "添加", disabled: ( ${sessionUser.addQx } == 1 ? false : true), onclick:addRoute, iconClass:'glyphicon glyphicon-plus'},
            {label: "编辑", disabled: ( ${sessionUser.editQx} == 1 ? false : true),onclick: editRoute, iconClass:'glyphicon glyphicon-pencil'},
            {label: "删除", disabled: ( ${sessionUser.deleteQx} == 1 ? false : true),onclick: deleteRoute, iconClass:'glyphicon glyphicon-trash'},
            {label: "导出", disabled: ( ${sessionUser.queryQx}==1?false:true),onclick:function(){toExcel();},iconClass:' icon-share'}
           ]
    });

    $(function () {
        _grid = jQuery("#grid-table").jqGrid({
            url: context_path + "/route/list.do",
            datatype: "json",
            colNames: ["主键", "工位编号", "工位名称","设备", "货架", "库位", "备注"],
            colModel: [
                {name: "id", index: "id", width: 20, hidden: true},
                {name: "routeNo", index: "routeNo", width: 60},
                {name: "routeName",index: "routeName",width:60},
                {name: "ip",index: "ip",width:70},
                {name: "shelveName",index: "shelveName",width:70},
                {name: "allocationName", index: "allocationName", width: 70},
                {name: "remark", index: "remark", width: 60}
            ],
            rowNum: 20,
            rowList: [10, 20, 30],
            pager: "#grid-pager",
            sortname: "routeNo",
            sortorder: "asc",
            altRows: true,
            viewrecords: true,
            autowidth: true,
            multiselect: true,
            multiboxonly: true,
            beforeRequest:function (){
                dynamicGetColumns(dynamicDefalutValue,"grid-table",$(window).width()-$("#sidebar").width() -7);
                //重新加载列属性
            },
            loadComplete: function (data) {
                var table = this;
                setTimeout(function () {
                    updatePagerIcons(table);
                    enableTooltips(table);
                }, 0);
                oriData = data;
            },
            emptyrecords: "没有相关记录",
            loadtext: "加载中...",
            pgtext: "页码 {0} / {1}页",
            recordtext: "显示 {0} - {1}共{2}条数据"
        });
        //在分页工具栏中添加按钮
        jQuery("#grid-table").navGrid("#grid-pager", {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false
        }).navButtonAdd("#grid-pager", {
                    caption: "",
                    buttonicon: "ace-icon fa fa-refresh green",
                    onClickButton: function () {
                        $("#grid-table").jqGrid("setGridParam",
                                {
                                    postData: {queryJsonString: ""} //发送数据
                                }
                        ).trigger("reloadGrid");
                    }
                }).navButtonAdd('#grid-pager',{
                caption: "",
                buttonicon:"fa  icon-cogs",
                onClickButton : function (){
                    jQuery("#grid-table").jqGrid('columnChooser',{
                        done: function(perm, cols){
                            dynamicColumns(cols,dynamicDefalutValue);
                            $("#grid-table").jqGrid( 'setGridWidth',$("#grid-div").width());
                        }
                    });
                }
            });
        $(window).on("resize.jqGrid", function () {
            $("#grid-table").jqGrid("setGridWidth", $("#grid-div").width() );
            $("#grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-$("#yy").outerHeight(true)-$("#fixed_tool_div").outerHeight(true)-$("#grid-pager").outerHeight(true)
           -$("#gview_grid-table .ui-jqgrid-hdiv").outerHeight(true));
        });

        $(window).triggerHandler("resize.jqGrid");
    });
    var _queryForm_data = iTsai.form.serialize($('#queryForm'));
	function queryOk(){
		var queryParam = iTsai.form.serialize($("#queryForm"));
		queryLineByParam(queryParam);		
	}
	function queryLineByParam(jsonParam) {
    iTsai.form.deserialize($("#hiddenQueryForm"), jsonParam);
    var queryParam = iTsai.form.serialize($("#hiddenQueryForm"));
    var queryJsonString = JSON.stringify(queryParam); 
    $("#grid-table").jqGrid("setGridParam",
        {
            postData: {queryJsonString: queryJsonString}
        }).trigger("reloadGrid");
    }
	function reset(){
		$("#queryForm #routeNo").val("").trigger("change");
        $("#queryForm #routeName").val("").trigger("change");
	    $("#grid-table").jqGrid("setGridParam", {
            postData: {queryJsonString:""}
        }).trigger("reloadGrid");		
	}
	function addRoute(){
	    openwindowtype=0;
		$.post(context_path + "/route/toEdit.do", {}, function (str){
		$queryWindow=layer.open({
		    title : "工位添加", 
	    	type:1,
	    	skin : "layui-layer-molv",
	    	area : "600px",
	    	shade : 0.6, //遮罩透明度
		    moveType : 1, //拖拽风格，0是默认，1是传统拖动
		    anim : 2,
		    content : str,
		    success: function (layero, index) {
                layer.closeAll("loading");
            }
		});
	 });
	}
	function editRoute(){
	    var checkedNum = getGridCheckedNum("#grid-table", "id");
    if (checkedNum == 0) {
       layer.alert("请选择一个要编辑的工位！");
        return false;
    } else if (checkedNum > 1) {
    	layer.alert("只能选择一个工位进行编辑操作！");
        return false;
    } else {
        openwindowtype=1;
        var id = jQuery("#grid-table").jqGrid("getGridParam","selrow");
        $.post(context_path + "/route/toEdit.do?id="+id, {}, function (str){
    		$queryWindow=layer.open({
    		    title : "工位编辑", 
    	    	type:1,
    	    	skin : "layui-layer-molv",
    	    	area : "600px",
    	    	shade : 0.6, //遮罩透明度
    		    moveType : 1, //拖拽风格，0是默认，1是传统拖动
    		    anim : 2,
    		    content : str,
    		    success: function (layero, index) {
                    layer.closeAll("loading");
                }
    		});
    	});
      }
	}
	function deleteRoute(){
	   var checkedNum = getGridCheckedNum("#grid-table", "id");  //选中的数量
    if (checkedNum == 0) {
        layer.alert("请选择一个要删除的工位！");
    } else {
        //从数据库中删除选中的物料，并刷新物料表格
        var ids = jQuery("#grid-table").jqGrid("getGridParam", "selarrrow");
        layer.confirm("确定删除选中的工位？", function() {
    		$.ajax({
    			type : "post",
    			url : context_path + "/route/deleteRoute.do?ids=" + ids,
    			dataType : "json",
    			cache : false,
    			success : function(data) {
    				layer.closeAll();
    				if (Boolean(data.result)) {
    					layer.msg(data.msg, {icon: 1,time:1000});
    				}else{
    					layer.msg(data.msg, {icon: 7,time:2000});
    				}
    				_grid.trigger("reloadGrid");  //重新加载表格
    			}
    		});
    	});
     }
	}
    function toExcel(){
        var ids = jQuery("#grid-table").jqGrid("getGridParam", "selarrrow");
        $("#hiddenForm #ids").val(ids);
        $("#hiddenForm").submit();	
    }
</script>
</html>