<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<div id="grid-div">
    <!-- 隐藏区域：存放查询条件 -->
    <form id="hiddenQueryForm" action="<%=path%>/outStoragePlan/exportExcel" method="POST" style="display:none;">
        <!-- 流程名称 -->
        <input name="name" id="name" value="" />
    </form>
    <div class="query_box" id="yy" title="查询选项">
            <form id="queryForm_process" style="max-width:100%;">
			 <ul class="form-elements">
			    <li class="field-group field-fluid3">
					<label class="inline" for="name" style="margin-right:20px; width:100%">
						<span class="form_label" style="width:65px;">流程名称：</span>
						<input type="text" id="name" name="name" style="width:calc( 100% - 75px);" placeholder="流程编号"/>
					</label>			
				</li>				
			</ul>
			<div class="field-button">
					<div class="btn btn-info" onclick="queryOk();">
				        <i class="ace-icon fa fa-check bigger-110"></i>查询
			        </div>
				    <div class="btn" onclick="reset();"><i class="ace-icon icon-remove"></i>重置</div>
		     </div>
		  </form>		 
    </div>
    <div id="grid-div" style="width:100%;margin:0px auto;">
        <div id="fixed_tool_div" class="fixed_tool_div">
            <div id="__toolbar__" style="float:left;overflow:hidden;"></div>
        </div>
        <table id="grid-table_process" style="width:100%;height:100%;overflow:auto;"></table>
        <div id="grid-pager_process"></div>
    </div>
</div>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriData;
    var _grid_process;
    var dynamicDefalutValue="17deaa3c54204d90ad2e6f0f346039d1";
    $(function  (){
     //  $(".toggle_tools").click();
    });
    $("#__toolbar__").iToolBar({
        id:"__tb__01",
        items:[
        {label: "删除",disabled:(${sessionUser.deleteQx}==1?false:true),onclick:delProcess,iconClass:'icon-trash'}
        ]
    });

    function delProcess(){
        var checkedNum = jQuery("#grid-table_process").getGridParam("selarrrow");
        var deploymentIds="";
        $.each(checkedNum,function (index,item){
            var rowData = $("#grid-table_process").jqGrid("getRowData",item);
            deploymentIds+=rowData.deploymentId+",";
        });
        if(deploymentIds.length>0){
            deploymentIds=deploymentIds.substring(0,deploymentIds.length-1);
            layer.confirm('确定删除？',{
                shift: 6,
                moveType: 1, //拖拽风格，0是默认，1是传统拖动
                title:"操作提示",  /*弹出框标题*/
                icon: 3,      /*消息内容前面添加图标*/
                btn: ['确定', '取消']/*可以有多个按钮*/
            },function(index,layero){
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: context_path+'/process/deleteProcessDefined',
                    data: {ids: deploymentIds},
                    success: function (data) {
                        if(data.result){
                            layer.msg(data.msg,{icon:1,time:1000});
                        }else{
                            layer.msg(data.msg,{icon:2,time:1000});
                        }
                        $("#grid-table_process").jqGrid('setGridParam',
                            {
                                postData: {queryJsonString:""} //发送数据
                            }
                        ).trigger("reloadGrid");
                    },
                })
            },function(index){
                //取消按钮的回调
                layer.close(index);
            })
        }
        else{
            layer.msg("请选择一条记录！",{icon:2});
        }
    }
    _grid_process = jQuery("#grid-table_process").jqGrid({
        url : context_path + "/process/toList.do",
        datatype : "json",
        colNames : [ "主键ID","流程定义ID","发布ID","名称","KEY","部署时间","是否挂起","操作"],
        colModel : [
            {name: "id", index: "id", width: 50, hidden: true},
            {name: "processDefinitionId", index: "processDefinitionId", width: 120, sortable: false,hidden:true},
            {name: "deploymentId", index: "deploymentId", width: 50,hidden:true},
            {name: "processName", index: "processName", width: 60},
            {name: "key", index: "key", width: 70,hidden:true},
            {name: "deployTime", index: "deployTime", width: 70},
            {name: "handUp", index: "handUp", width: 70},
            {name: "opertion", index: "opertion", width: 70,
                formatter: function (cellValu, option, rowObject) {
                    return "<a target='_blank' href='"+context_path+"/process/resource/read?processDefinitionId=" + rowObject.processDefinitionId + "&resourceType=image'>" + rowObject.diagramResourceName.split(".")[1] + "</a>";
                }
            }
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : "#grid-pager_process",
        sortname : "USER_ID",
        sortorder : "desc",
        altRows: false,
        viewrecords : true,
      //caption : "流程列表",
        autowidth:true,
        multiselect:true,
        multiboxonly: true,
        beforeRequest:function (){
            dynamicGetColumns(dynamicDefalutValue,"grid-table_process",$(window).width()-$("#sidebar").width() -7);
            //重新加载列属性
        },
        loadComplete : function(data){
            var table = this;
            setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            oriData = data;
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
    });
    //在分页工具栏中添加按钮
    jQuery("#grid-table_process").navGrid("#grid-pager_process",{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd('#grid-pager_process',{
            caption:"",
            buttonicon:"ace-icon fa fa-refresh green",
            onClickButton: function(){
                $("#grid-table_process").jqGrid("setGridParam",
                    {
                        postData: {queryJsonString:""} //发送数据
                    }
                ).trigger("reloadGrid");
            }
        }).navButtonAdd("#grid-pager_process",{
            caption: "",
            buttonicon:"fa icon-cogs",
            onClickButton : function (){
                jQuery("#grid-table_process").jqGrid("columnChooser",{
                    done: function(perm, cols){
                        dynamicColumns(cols,dynamicDefalutValue);
                        $("#grid-table_process").jqGrid("setGridWidth", "setGridWidth", $("#grid-div").width() - 3);
                    }
                });
            }
        });
    $(window).on("resize.jqGrid", function () {
        $("#grid-table_process").jqGrid("setGridWidth", $("#grid-div").width() - 3 );
        var height = $("#breadcrumb").outerHeight(true)+$(".query_box").outerHeight(true)+
                     $("#fixed_tool_div").outerHeight(true)+
                   //$("#gview_grid-table_process .ui-jqgrid-titlebar").outerHeight(true)+
                     $("#gview_grid-table_process .ui-jqgrid-hbox").outerHeight(true)+
                     $("#grid-pager_process").outerHeight(true)+$("#header").outerHeight(true);
                     $("#grid-table_process").jqGrid("setGridHeight", (document.documentElement.clientHeight)-height);
    });
    $(window).triggerHandler("resize.jqGrid");
    function reset(){
        iTsai.form.deserialize($("#queryForm_process"),_queryForm_data);
        //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
        queryInstoreListByParam(_queryForm_data);
    }

    //查询的方法
    function queryInstoreListByParam(jsonParam){
        iTsai.form.deserialize($("#hiddenQueryForm"),jsonParam);   //将json对象反序列化到列表页面中隐藏的form中
        var queryParam = iTsai.form.serialize($("#hiddenQueryForm"));
        var queryJsonString = JSON.stringify(queryParam);         //将json对象转换成json字符串
        //执行查询操作
        $("#grid-table_process").jqGrid("setGridParam",
            {
                postData: {queryJsonString:queryJsonString} //发送数据
            }
        ).trigger("reloadGrid");
    }

    var _queryForm_data = iTsai.form.serialize($("#queryForm_process"));
    function queryOk(){
        var queryParam = iTsai.form.serialize($("#queryForm_process"));
        //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
        queryInstoreListByParam(queryParam);
    }
</script>