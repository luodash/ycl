<%--
  Created by IntelliJ IDEA.
  User: duckhyj
  Date: 2017/11/18
  Time: 11:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <script type="text/javascript">
        var context_path = '<%=path%>';
    </script>
    <style type="text/css">

        .query_box .field-button.two {
            padding: 0px;
            left: 330px;
        }
    </style>
</head>
<body style="">
<div id="grid-div">
    <!-- 隐藏区域：存放查询条件 -->
    <div class="query_box" id="yy" title="查询选项">
            <form id="queryForm" style="max-width:100%;">
			 <ul class="form-elements">
			    <li class="field-group field-fluid3">
					<label class="inline" for="key" style="margin-right:20px; width:100%;">
						<span class="form_label" style="width:65px;">流程编号：</span>
						<input type="text" id="key" name="key" style="width:calc(100% - 75px);" placeholder="流程编号"/>
					</label>			
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="name" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:65px;">流程名称：</span>
						<input type="text" id="name" name="name" style="width: calc(100% - 75px);" placeholder="流程名称">
					</label>
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="modelType" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:65px;">流程类型：</span>
						<input type="text" id="modelType" name="modelType" style="width: calc(100% - 75px);" placeholder="流程类型">
					</label>					
				</li>
				
			</ul>
			<div class="field-button" style="">
					<div class="btn btn-info" onclick="queryOk();">
				        <i class="ace-icon fa fa-check bigger-110"></i>查询
			        </div>
				    <div class="btn" onclick="reset();"><i class="ace-icon icon-remove"></i>重置</div>
		        </div>
		  </form>		 
    </div>
    <div id="fixed_tool_div" class="fixed_tool_div" style="margin:0px !important;border:0px !important;">
        <div id="__toolbar__" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="grid-table" style="width:100%;margin: 0px !important;boder:0px !important"></table>
    <div id="grid-pager" style="margin:0px !important;border:0px !important;"></div>
</div>
</body>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript" src="<%=path%>/static/js/techbloom/les/process/processdesign/processdesign.js"></script>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriData;
    var _grid;
    $(function  (){
      // $(".toggle_tools").click();
    });
    $("#__toolbar__").iToolBar({
        id:"__tb__01",
        items:[
            {label:"创建流程",disabled:(${sessionUser.addQx}==1?false:true),onclick:createNewProcess,iconClass:'glyphicon glyphicon-plus'},
            {label:"编辑流程",disabled:(${sessionUser.editQx}==1?false:true),onclick:eidtSomeModel,iconClass:'glyphicon glyphicon-pencil'},
            {label:"发布流程",disabled:(${sessionUser.deleteQx}==1?false:true),onclick:deploySomeModel,iconClass:'glyphicon icon-cloud-upload'},
            {label:"删除流程",disabled:(${sessionUser.deleteQx}==1?false:true),onclick:deleteSomeModel,iconClass:'glyphicon glyphicon-trash'}
    ]
    })
    $(function()
    {
        _grid = jQuery("#grid-table").jqGrid({
            url : context_path + '/processdesign/modelList.do',
            datatype : "json",
            colNames: ['模型ID', '流程名称', '流程编号','流程类型','创建时间', '上次更新时间', '操作'],
            colModel: [
                {name: 'id', index: 'id', width: 120, sortable: false,hidden:true},
                {name: 'name', index: 'name', width: 50},
                {name: 'key', index: 'key', width: 60},
                {name: 'category', index: 'category', width: 60},
                {name: 'createTime', index: 'createTime', width: 70,
                    formatter:function(cellValu,option,rowObject){
                        return formatDate(cellValu,'yyyy-MM-dd HH:mm:ss');
                    }
                },
                {name: 'lastUpdateTime', index: 'lastUpdateTime', width: 70,
                    formatter:function(cellValu,option,rowObject){
                        return formatDate(cellValu,'yyyy-MM-dd HH:mm:ss');
                    }
                },
                {name: 'version', index: 'version', width: 70,
                    formatter:function(cellValu,option,rowObject){
                        return "<a onclick='eidtSomeModel("+rowObject.id+")'>编辑</a>";
                    }
                }
            ],
            rowNum : 20,
            rowList : [ 10, 20, 30,50,200 ],
            pager : '#grid-pager',
            sortname : 'ID',
            sortorder : "desc",
            altRows: false,
            viewrecords : true,
            autowidth:true,
            multiselect:true,
            multiboxonly: true,
            loadComplete : function(data)
            {
                var table = this;
                setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
                oriData = data;
            },
            emptyrecords: "没有相关记录",
            loadtext: "加载中...",
            pgtext : "页码 {0} / {1}页",
            recordtext: "显示 {0} - {1}共{2}条数据"
        });
        //在分页工具栏中添加按钮
        jQuery("#grid-table").navGrid('#grid-pager',{edit:false,add:false,del:false,search:false,refresh:false})
            .navButtonAdd('#grid-pager',{
                caption:"",
                buttonicon:"ace-icon fa fa-refresh green",
                onClickButton: function(){
                    //jQuery("#grid-table").trigger("reloadGrid");  //重新加载表格
                    $("#grid-table").jqGrid('setGridParam',
                        {
                            postData: {queryJsonString:""} //发送数据
                        }
                    ).trigger("reloadGrid");
                }
            });
        $(window).on('resize.jqGrid', function () {
            $("#grid-table").jqGrid( 'setGridWidth', $("#grid-div").width() - 3 );
            $("#grid-table").jqGrid( 'setGridHeight', $(".container-fluid").height()-$("#boxQuery").outerHeight(true)-$("#fixed_tool_div").outerHeight(true)-$("#grid-pager").outerHeight(true)
         //-$("#gview_grid-table .ui-jqgrid-titlebar").outerHeight(true)
           -$("#gview_grid-table .ui-jqgrid-hbox").outerHeight(true));
        });

        $(window).triggerHandler('resize.jqGrid');
    });
    var _queryForm_data = iTsai.form.serialize($('#queryForm'));


    function queryOk(){
        //var formJsonParam = $('#queryForm').serialize();
        var queryParam = iTsai.form.serialize($('#queryForm'));
        //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
        queryStockCheckListByParam(queryParam);

    }

    /**
     * 盘点任务查询功能:获取查询页面中的值，并将值放入列表页面中隐藏的form
     * @param jsonParam     查询页面传递过来的json对象
     */
    function queryStockCheckListByParam(jsonParam){
        //序列化表单：iTsai.form.serialize($('#frm'))
        //反序列化表单：iTsai.form.deserialize($('#frm'),json)
        var queryJsonString = JSON.stringify(jsonParam);         //将json对象转换成json字符串
        //执行查询操作
        $("#grid-table").jqGrid('setGridParam',
            {
                postData: {queryJsonString:queryJsonString} //发送数据
            }
        ).trigger("reloadGrid");
    }

    $('#allocateTypeSelect').change(function(){
        $('#queryForm #allocateType').val($('#allocateTypeSelect').val());
    });

    $('#inWarehouseSelect').change(function(){
        $('#queryForm #inWarehouse').val($('#inWarehouseSelect').val());
    });

    $('#outWarehouseSelect').change(function(){
        $('#queryForm #outWarehouse').val($('#outWarehouseSelect').val());
    });
    function reset(){
        //var formJsonParam = $('#queryForm').serialize();
        iTsai.form.deserialize($('#queryForm'),_queryForm_data);
//         $("#allocateTypeSelect").val("").trigger('change');
//         $("#inWarehouseSelect").val("").trigger('change');
//         $("#outWarehouseSelect").val("").trigger('change');
//         $("#queryForm #type").select2("val","");
        //var queryParam = iTsai.form.serialize($('#queryForm'));
        //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
        queryStockCheckListByParam(_queryForm_data);
//         $("#allocateTypeSelect").find("option[text='所有类型']").attr("selected",true);
//         $("#outWarehouseSelect").find("option[text='所有库区']").attr("selected",true);
//         $("#inWarehouseSelect").find("option[text='所有库区']").attr("selected",true);
    }
    $('#queryForm .mySelect2').select2();
    $(".date-picker").datetimepicker({format: 'YYYY-MM-DD',useMinutes:true,useSeconds:true});
    function reloadGrid(){
        _grid.trigger("reloadGrid");
    }
    $("#queryForm #type").select2({
    placeholder: "选择流程类型",
    minimumInputLength: 0, //至少输入n个字符，才去加载数据
    allowClear: true, //是否允许用户清除文本信息
    delay: 250,
    formatNoMatches: "没有结果",
    formatSearching: "搜索中...",
    formatAjaxError: "加载出错啦！",
    ajax: {
        url: context_path + "/dictionary/getDictionaryDetailListByType",
        type: "POST",
        dataType: "json",
        delay: 250,
        data: function (term, pageNo) { //在查询时向服务器端传输的数据
            term = $.trim(term);
            return {
                queryString: term, //联动查询的字符
                pageSize: 15, //一次性加载的数据条数
                pageNo: pageNo, //页码
                time: new Date(),
                dictionaryType:'LES_PROCESSDESIGN_TYPE'
            }
        },
        results: function (data, pageNo) {
            var res = data.result;
            if (res.length > 0) { //如果没有查询到数据，将会返回空串
                var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                return {
                    results: res,
                    more: more
                };
            } else {
                return {
                    results: {}
                };
            }
        },
        cache: true
    }
});
</script>
</html>

