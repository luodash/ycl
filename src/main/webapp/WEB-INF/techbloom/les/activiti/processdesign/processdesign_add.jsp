<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
    String path = request.getContextPath();
%>
<div id ="rocessDesign" class="row-fluid" style="height: inherit;">
    <form id="processDesignForm" class="form-horizontal"  style="overflow: auto; height: calc(100% - 70px);">
        <input type="hidden" name="userId" id="userId" value="">
        <div class="control-group">
            <label class="control-label" for="name">流程名称:</label>
            <div class="controls">
                <div class="input-append span12 required" >
                    <input class="span11" type="text"  name="name" id="name" placeholder="路径名称">
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="key">流程编号:</label>
            <div class="controls">
                <div class="input-append span12 required">
                    <input class="span11" type="text" name="key" id="key"placeholder="路径编号">
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="modelType">流程类型:</label>
            <div class="controls">
                <div class="span12 required" style="float: none !important;">
                    <input class="span11 select2_input" type="text" name="modelType" id="modelType"
                           placeholder="流程类型"/>
                </div>
            </div>
        </div>
    </form>
    <div class="field-button" style="text-align: center;border-top: 0px;margin: 15px auto;">
		<span class="btn btn-info" onclick="setDisabled(this.id);">
		   <i class="ace-icon fa fa-check bigger-110"></i>保存
		</span>
        <span class="btn btn-danger" onclick="layer.closeAll();">
		   <i class="icon-remove"></i>&nbsp;取消
		</span>
    </div>
</div>
<script type="text/javascript" src="<%=path%>/static/js/techbloom/les/process/processdesign/processdesign.js"></script>
<script type="text/javascript">
    (function(){
        //表单校验
        $("#processDesignForm").validate({
            rules: {
                "name":{
                    required:true,
                    remote:context_path+"/processdesign/isExitSameName.do?userId="+$('#userId').val()
                },
                "key":{
                    required:true,
                    remote:context_path+"/processdesign/isExitSameKey.do?userId="+$('#userId').val()
                },
                "modelType":{
                    required:true
                }
            },
            messages:{
                "name":{
                    required:"流程名称为必填项!",
                    remote:"您输入的流程名称已经存在!"
                },
                "key":{
                    required:"流程编号为必填项!",
                    remote:"您输入的流程编号已经存在!"
                },
                "modelType":{
                    required:"流程类型必选!"
                }
            },
            errorClass: "help-inline",
            errorElement: "span",
            highlight:function(element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
            }
        });

        //确定按钮点击事件
        $("#user_add_page .btn-success").off("click").on("click", function(){
            //用户保存
            if($("#queryForm").valid()){
                saveUserInfo($("#queryForm").serialize());
            }
        });

        //取消按钮点击事件
        $("#user_add_page .btn-danger").off("click").on("click", function(){
            layer.close($queryWindow);
        });

        var ajaxStatus = 1;     //ajax请求状态：0不能请求，1可以请求
        //保存/修改用户信息
        function saveUserInfo(bean){
            console.dir(bean);
            if(bean){
                if(ajaxStatus==0){
                    layer.msg("操作进行中，请稍后...",{icon:2});
                    return;
                }
                ajaxStatus = 0;    //将标记设置为不可请求
                $(".savebtn").attr("disabled","disabled");
                $.ajax({
                    url:context_path+"/proccess/createModel?tm="+new Date(),
                    type:"POST",
                    data:bean,
                    dataType:"JSON",
                    success:function(data){
                        ajaxStatus = 1; //将标记设置为可请求
                        $(".savebtn").removeAttr("disabled");
                        if(data.result){
                            layer.msg("保存成功！",{icon:1});
                            //刷新用户列表
                            $("#grid-table").jqGrid('setGridParam',
                                {
                                    postData: {queryJsonString:""} //发送数据
                                }
                            ).trigger("reloadGrid");
                            //关闭当前窗口
                            //关闭指定的窗口对象
                            layer.close($queryWindow);
                        }else{
                            layer.msg("保存失败，请稍后重试！",{icon:2});
                        }
                    }
                });
            }else{
                layer.msg("出错啦！",{icon:2});
            }
        }
    }())

</script>