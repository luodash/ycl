<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<div id="moveout_list_grid-div">
    <!-- 隐藏区域：存放查询条件 -->
    <form id="moveout_list_hiddenForm" action="<%=path%>/moveout/materialExcel" method="POST" style="display:none;">
        <input id="moveout_list_ids" name="ids" value=""/>
        <input id="moveout_list_queryMoveCode" name="queryMoveCode" value=""/>
        <input id="moveout_list_queryStartTime" name="queryStartTime" value=""/>
        <input id="moveout_list_queryEndTime" name="queryEndTime" value=""/>
        <input id="moveout_list_queryCreater" name="queryCreater" value=""/>
        <input id="moveout_list_queryFactoryCode" name="queryFactoryCode" value=""/>
        <input id="moveout_list_queryWarehouse" name="queryWarehouse" value=""/>

        <input id="moveout_list_queryExportExcelIndex" name="queryExportExcelIndex" value=""/>
    </form>
    <form id="moveout_list_hiddenQueryForm" style="display:none;">
        <input name="moveCode" value=""/>
        <input name="startTime" value=""/>
        <input name="endTime" value=""/>
        <input name="creater" value=""/>
        <input name="factoryCode" value=""/>
        <input name="warehouse" value=""/>
    </form>
    <c:if test="${operationCode.webSearch==1}">
    <div class="query_box" id="moveout_list_yy" title="查询选项">
        <form id="moveout_list_queryForm" style="max-width:100%;">
            <ul class="form-elements">
                <li class="field-group field-fluid3">
                    <label class="inline" for="moveout_list_moveCode" style="margin-right:20px;width: 100%;">
                        <span class="form_label" style="width:80px;">移库单编号：</span>
                        <input id="moveout_list_moveCode" name="moveCode" type="text" style="width: calc(100% - 85px);" placeholder="移库单编号">
                    </label>
                </li>
                <li class="field-group field-fluid3">
                    <label class="inline" for="moveout_list_startTime" style="margin-right:20px;width:100%;">
                        <span class="form_label" style="width:80px;">创建时间起：</span>
                        <input type="text" class="form-control date-picker" id="moveout_list_startTime" name="startTime" value="" style="width: calc(100% - 85px);" placeholder="创建时间起" />
                    </label>
                </li>
                <li class="field-group field-fluid3">
                    <label class="inline" for="moveout_list_endTime" style="margin-right:20px;width:100%;">
                        <span class="form_label" style="width:80px;">创建时间止：</span>
                        <input type="text" class="form-control date-picker" id="moveout_list_endTime" name="endTime" value="" style="width: calc(100% - 85px);" placeholder="创建时间止" />
                    </label>
                </li>
                
                <li class="field-group-top field-group field-fluid3">
                    <label class="inline" for="moveout_list_factoryCode" style="margin-right:20px;width: 100%;">
						 <span class="form_label" style="width:80px;">厂区：</span>
						 <input id="moveout_list_factoryCode" name="factoryCode" type="text" style="width: calc(100% - 85px);" placeholder="厂区">
					</label>
                </li>
                <li class="field-group field-fluid3">
                    <label class="inline" for="moveout_list_warehouse" style="margin-right:20px;width: 100%;">
						 <span class="form_label" style="width:80px;">仓库：</span>
						 <input id="moveout_list_warehouse" name="warehouse" type="text" style="width:  calc(100% - 85px);" placeholder="仓库">
					 </label>
                </li>
                <li class="field-group field-fluid3">
                    <label class="inline" for="moveout_list_creater" style="margin-right:20px;width:100%;">
                        <span class="form_label" style="width:80px;">创建人：</span>
                        <input id="moveout_list_creater" name="creater" type="text" style="width: calc(100% - 85px);" placeholder="创建人">
                    </label>
                </li>
            </ul>
            <div class="field-button" style="">
                <div class="btn btn-info" onclick="moveout_list_queryOk();">
                    <i class="ace-icon fa fa-check bigger-110"></i>查询
                </div>
                <div class="btn" onclick="moveout_list_reset();"><i class="ace-icon icon-remove"></i>重置</div>
                <a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>
            </div>
        </form>
    </div>
    </c:if>
    <div id="moveout_list_fixed_tool_div" class="fixed_tool_div">
        <div id="moveout_list___toolbar__" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="moveout_list_grid-table" style="width:100%;height:100%;"></table>
    <div id="moveout_list_grid-pager"></div>
</div>

<script type="text/javascript" src="<%=path%>/static/js/techbloom/move/moveout/moveout_list.js"></script>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriData;
    var _grid;
    var moveout_list_dynamicDefalutValue="17e870237a2d470fb170daeafe6292e1";//列表码
    var moveout_list_factory_selectid;
    var moveout_list_exportExcelIndex;

    //时间控件
    $(".date-picker").datetimepicker({format: "YYYY-MM-DD"});
    
    $(function  (){
        $(".toggle_tools").click();
    });

    $("#moveout_list___toolbar__").iToolBar({
        id: "moveout_list___tb__01",
        items: [
    		{label: "执行", onclick:moveout_list_execute, iconClass:'icon-tag'},
    		{label: "查看", onclick:moveout_list_openInfoPage, iconClass:'icon-search'},
    		{label: "导出", onclick:function(){moveout_list_toExcel();},iconClass:'icon-share'}
    	]
    });

    var moveout_list_queryForm_Data = iTsai.form.serialize($('#moveout_list_queryForm'));

    _grid = jQuery("#moveout_list_grid-table").jqGrid({
        url : context_path + '/moveout/storageListData.do',
        datatype : "json",
        colNames : ["主键","移库单号","单据创建时间","厂区","创建人","单据状态"],
        colModel : [
            {name : "id",index : "id",hidden:true},
            {name : "moveCode",index : "moveCode",width : 60},
            {name : "moveTime",index : "moveTime",width : 60},
            {name : "factoryName",index : "factoryName",width : 60},
            {name : "userName",index : "userName",width : 60},
            {name : "moveState",index : "moveState",width : 60,formatter:function(cellvalue,option,rowObject){
                    if(cellvalue!=null) {
                        if (cellvalue.indexOf("2") > -1 || cellvalue.indexOf("3") > -1 || cellvalue.indexOf("4") > -1) {
                            return "<span style=\"color:blue;font-weight:bold;\">移库中</span>";
                        } else if (cellvalue.indexOf("1") > -1) {
                            if (cellvalue.indexOf("5") > -1) {
                                return "<span style=\"color:blue;font-weight:bold;\">移库中</span>";
                            } else {
                                return "<span style=\"color:red;font-weight:bold;\">新建</span>";
                            }
                        } else if (cellvalue.indexOf("5") > -1) {
                            return "<span style=\"color:green;font-weight:bold;\">完成</span>";
                        } else {
                            return "<span style=\"color:orange;font-weight:bold;\">其他</span>";
                        }
                    }else {
                        return "<span style=\"color:red;font-weight:bold;\">新建</span>";
                    }
                }
            }
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : '#moveout_list_grid-pager',
        sortname : 't1.id',
        sortorder : "desc",
        altRows: false,
        viewrecords : true,
        autowidth:true,
        multiselect:true,
        multiboxonly: true,
        beforeRequest:function (){
            dynamicGetColumns(moveout_list_dynamicDefalutValue,"moveout_list_grid-table",$(window).width()-$("#sidebar").width() -7);
            //重新加载列属性
        },
        loadComplete : function(data){
            var table = this;
            setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            oriData = data;
            $(window).triggerHandler('resize.jqGrid');
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
    });

    //在分页工具栏中添加按钮
    jQuery("#moveout_list_grid-table").navGrid("#moveout_list_grid-pager",
        {edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd('#moveout_list_grid-pager',{
        caption:"",
        buttonicon:"ace-icon fa fa-refresh green",
        onClickButton: function(){
            $("#moveout_list_grid-table").jqGrid("setGridParam",
                {
                    postData: {queryJsonString:""} //发送数据
                }
            ).trigger("reloadGrid");
        }
    }).navButtonAdd("#moveout_list_grid-pager",{
        caption: "",
        buttonicon:"fa icon-cogs",
        onClickButton : function (){
            jQuery("#moveout_list_grid-table").jqGrid("columnChooser",{
                done: function(perm, cols){
                    dynamicColumns(cols,moveout_list_dynamicDefalutValue);
                    $("#moveout_list_grid-table").jqGrid("setGridWidth", $("#moveout_list_grid-div").width()-3);
                    moveout_list_exportExcelIndex = perm;
                }
            });
        }
    });

    $(window).on("resize.jqGrid", function () {
		$("#moveout_list_grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);
		$("#moveout_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-10-
		$("#moveout_list_yy").outerHeight(true)-$("#moveout_list_fixed_tool_div").outerHeight(true)-
		$("#moveout_list_grid-pager").outerHeight(true)-$("#gview_moveout_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
	});

    //查询
    function moveout_list_queryOk(){
        var queryParam = iTsai.form.serialize($('#moveout_list_queryForm'));
        //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
        moveout_list_queryInstoreListByParam(queryParam);
    }

  	//厂区
    $("#moveout_list_queryForm #moveout_list_factoryCode").select2({
        placeholder: "选择厂区",
        minimumInputLength:0,   //至少输入n个字符，才去加载数据
        allowClear: true,  //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches:"没有结果",
        formatSearching:"搜索中...",
        formatAjaxError:"加载出错啦！",
        ajax : {
            url: context_path+"/factoryArea/getFactoryList",
            type:"POST",
            dataType : 'json',
            delay : 250,
            data: function (term,pageNo) {     //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term,    //联动查询的字符
                    pageSize: 15,    //一次性加载的数据条数
                    pageNo:pageNo    //页码
                }
            },
            results: function (data,pageNo) {
                var res = data.result;
                if(res.length>0){   //如果没有查询到数据，将会返回空串
                    var more = (pageNo*15)<data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results:res,more:more
                    };
                }else{
                    return {
                        results:{}
                    };
                }
            },
            cache : true
        }
    });

    //重置
    function moveout_list_reset(){
        $("#moveout_list_queryForm #moveout_list_moveCode").select2("val","");
        $("#moveout_list_queryForm #moveout_list_factoryCode").select2("val","");
        $("#moveout_list_queryForm #moveout_list_warehouse").select2("val","");
        iTsai.form.deserialize($("#moveout_list_queryForm"),moveout_list_queryForm_Data);
        moveout_list_queryInstoreListByParam(moveout_list_queryForm_Data);
    }

    $("#moveout_list_queryForm #moveout_list_factoryCode").on("change.select2",function(){
        $("#moveout_list_queryForm #moveout_list_factoryCode").trigger("keyup")
    	moveout_list_factory_selectid = $("#moveout_list_queryForm #moveout_list_factoryCode").val();
    });

    $("#moveout_list_queryForm #moveout_list_warehouse").select2({
        placeholder: "选择仓库",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/car/selectWarehouse",
            type: "POST",
            dataType: 'json',
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    factoryCodeId:moveout_list_factory_selectid
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });
</script>