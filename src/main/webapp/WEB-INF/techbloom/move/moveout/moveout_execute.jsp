<%@ page language="java" import="java.lang.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
    String path = request.getContextPath();
%>
<style>
    .floatLR{
        float: left;
        margin-right: 10px;
    }
</style>
<div class="row-fluid" style="height: inherit;margin:0px;border: 0px">
    <!-- 表格div -->
    <div id="moveout_execute_grid-div-c" style="width:100%;margin:10px auto;">
        <input id="moveout_execute_id1" type="hidden" name="id" value="${id}"/>
        <!-- 	表格工具栏 -->
        <div id="moveout_execute_fixed_tool_div" class="fixed_tool_div detailToolBar">
            <div id="moveout_execute___toolbar__-c" style="float:left;overflow:hidden;"></div>
        </div>
        <div style="margin-bottom:5px;margin-left: 25px;">
            <span class="btn btn-info" id="moveStorageOut">
		       <i class="ace-icon fa fa-check bigger-110"></i>确认出库
            </span>
        </div>
        <!-- 物料详情信息表格 -->
        <table id="moveout_execute_grid-table-c" style="width:100%;height:100%;"></table>
        <!-- 表格分页栏 -->
        <div id="moveout_execute_grid-pager-c"></div>
    </div>
</div>
<iframe src="about:blank" name="_ifr" height="0" class="hidden"></iframe>
<script type="text/javascript" src="<%=path%>/static/js/techbloom/move/moveout/moveout_execute.js"></script>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriDataDetail;
    var _grid_detail;        //表格对象
    var id=$("#moveout_execute_id1").val();

    _grid_detail=jQuery("#moveout_execute_grid-table-c").jqGrid({
        url : context_path + "/moveout/detailList.do?id="+id,
        datatype : "json",
        colNames : [ "详情主键","质保单号","批次号","行车编码","物料编码","物料名称","米数","仓库","库位","完成出库时间","拣货人","操作","状态"],
        colModel : [
            {name : "id",index : "id",hidden:true},
            {name : "qaCode",index:"qaCode",width : 30},
            {name : 'batchNo',index : 'batch_No',width : 30},
            {name : "carCode",index : "carCode",hidden:true},
            {name : "materialCode",index:"materialCode",width : 30},
            {name : "materialName",index:"materialName",width :30},
            {name : "meter",index:"meter",width : 10},
            {name : 'warehouseCode',index : 'warehouseCode',width : 20},
            {name : 'outShelfCode',index : 'outShelfCode',width : 30},
            {name : 'outstoragetime',index : 'outstoragetime',width : 30},
            {name : 'pickManName',index : 'pickManName',width : 20},
            {name : "cz",index : "cz",width :20,
                formatter:function(cellValu,option,rowObject){
                    var carCode = rowObject.carCode==null?"-1":'"'+rowObject.carCode+'"';
                    /*return "<button id='b1'class='btn btn-info floatLR' onclick='bind("+rowObject.id+","+carCode+")'>绑定行车</button>" +
                        "<button id='b2' class='btn btn-info floatLR' onclick='detailStart("+rowObject.id+")'>开始拣货</button>" +
                        "<button id='b3' class='btn btn-info floatLR' onclick='detailSuccess("+rowObject.id+")'>拣货完成</button>";*/
                    return "<button id='b3' class='btn btn-info floatLR' onclick='detailSuccess("+rowObject.id+")'>完成出库</button>";
                }
            },
            {name : 'state',index : 'state',width : 20,formatter:function(cellvalue,option,rowObject){
                    if(cellvalue=='1'){
                        return "<span style=\"color:red;font-weight:bold;\">未移库出库</span>";
                    }else if(cellvalue=='2'){
                        return "<span style=\"color:blue;font-weight:bold;\">开始移库出库</span>";
                    }else if(cellvalue=='3'){
                        return "<span style=\"color:brown;font-weight:bold;\">完成移库出库</span>";
                    }else if(cellvalue=='4'){
                        return "<span style=\"color:deepskyblue;font-weight:bold;\">开始移库入库</span>";
                    }else if(cellvalue=='5'){
                        return "<span style=\"color:green;font-weight:bold;\">完成移库入库</span>";
                    }
                }
            }
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : "#moveout_execute_grid-pager-c",
        sortname : "t1.id",
        sortorder : "asc",
        altRows: true,
        viewrecords : true,
        caption : "详情列表",
        autowidth:true,
        multiselect:true,
        multiboxonly: true,
        loadComplete : function(data) {
            var table = this;
            setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            oriDataDetail = data;
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据",
    });
    //在分页工具栏中添加按钮
    $("#moveout_execute_grid-table-c").navGrid("#moveout_execute_grid-pager-c",
        {edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#moveout_execute_grid-pager-c",{
        caption:"",
        buttonicon:"ace-icon fa fa-refresh green",
        onClickButton: function(){
            $("#moveout_execute_grid-table-c").jqGrid('setGridParam',
                {
                    url:context_path + "/moveout/detailList.do?id="+id,
                }
            ).trigger("reloadGrid");
        }
    });

    $(window).on("resize.jqGrid", function () {
        $("#moveout_execute_grid-table-c").jqGrid("setGridWidth", $("#moveout_execute_grid-div-c").width() - 3 );
        $("#moveout_execute_grid-table-c").jqGrid("setGridHeight", (document.documentElement.clientHeight - $("#moveout_execute_grid-pager-c").height() - 380) );
    });
    $(window).triggerHandler("resize.jqGrid");

    /**确认出库*/
    $('#moveStorageOut').click(function () {
        var checkedNum = getGridCheckedNum("#moveout_execute_grid-table-c","id");  //选中的数量
        if(checkedNum==0){
            layer.alert("请至少选中一条记录！");
        }else{
            layer.confirm("确定所有出库吗？",function(){
                $.ajax({
                    type:"POST",
                    url:context_path + "/moveout/moveStorageOut?ids="+$('#moveout_execute_grid-table-c').jqGrid('getGridParam','selarrrow'),
                    dataType:"json",
                    success:function(data){
                        iTsai.form.deserialize($("#moveout_list_hiddenQueryForm"), iTsai.form.serialize($("#moveout_list_queryForm")));
                        var queryParam = iTsai.form.serialize($("#moveout_list_hiddenQueryForm"));
                        var queryJsonString = JSON.stringify(queryParam);
                        if(data.result){
                            layer.msg(data.msg);
                            reloadDetailTableList();   //重新加载详情列表
                            $("#moveout_list_grid-table").jqGrid("setGridParam",
                                {
                                    postData: {queryJsonString:queryJsonString} //发送数据
                                }
                            ).trigger("reloadGrid");
                        }else{
                            layer.msg(data.msg);
                        }
                    }
                })
            });
        }
    });
</script>