<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<div id="movecar_bind_page" class="row-fluid" style="height: inherit;">
	<form id="movecar_bind_carForm" class="form-horizontal" style="overflow: auto; height: calc(100% - 70px);">
		<input type="hidden" id="movecar_bind_id" name="id" value="${detail.id}">
        <input type="hidden" id="movecar_bind_infoId1" name="infoId1" value="${infoId}">
		<div class="control-group">
			<label class="control-label" for="movecar_bind_carId">行车绑定：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<select id="movecar_bind_carId" name="carId" class="span9">
                        <c:forEach items="${carList}" var="tp">
                            <option value="${tp.id}" <c:if test="${detail.carCode==tp.code }">selected="selected"</c:if>>
                                    ${tp.name}</option>
                        </c:forEach>
					</select>
				</div>
			</div>
		</div>
	</form>
	<div class="field-button" style="text-align: center;border-top: 0px;margin: 15px auto;">
		<span class="btn btn-info" onclick="saveForm();">
		   <i class="ace-icon fa fa-check bigger-110"></i>保存
		</span>
		<span class="btn btn-danger" onclick="layer.closeAll();">
		   <i class="icon-remove"></i>&nbsp;取消
		</span>
	</div>
</div>
<script type="text/javascript">
$("#movecar_bind_carForm").validate({
		rules:{
			"movecar_bind_carId":{
  				required:true
  			}
  		},
  		messages:{
  			"movecar_bind_carId":{
  				required:"请选择绑定行车！"
  			}
  		},
  		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
		}
  	});
	//确定按钮点击事件
    function saveForm() {
        if ($("#movecar_bind_carForm").valid()) {
            saveCarInfo($("#movecar_bind_carForm").serialize());
        }
    }
  	//保存/修改行车信息
    function saveCarInfo(bean) {
        $.ajax({
                url: context_path + "/moveout/saveCar",
                type: "POST",
                data: bean,
                dataType: "JSON",
                success: function (data) {
                    if (Boolean(data.result)) {
                        layer.msg("保存成功！", {icon: 1});
                        var idd = $("#movecar_bind_infoId1").val();
                        //关闭当前窗口
                        layer.close($queryWindow);
                        //刷新列表
                        $("#movecar_bind_grid-table-c").jqGrid('setGridParam',
                            {
                                url:context_path + "/moveout/detailList.do?id="+idd,
                            }
                        ).trigger("reloadGrid");
                    } else {
                        layer.alert("保存失败，请稍后重试！", {icon: 2});
                    }
                },
                error:function(XMLHttpRequest){
            		alert(XMLHttpRequest.readyState);
            		alert("出错啦！！！");
            	}
            });
    }
</script>