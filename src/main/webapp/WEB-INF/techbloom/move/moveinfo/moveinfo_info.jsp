<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
%>
<div id="moveinfo_info_page" class="row-fluid" style="height: inherit;margin:0px">
    <form action="" class="form-horizontal" id="moveinfo_info_baseInfor" name="baseInfor" method="post" target="_ifr" >
        <input type="hidden" id="moveinfo_info_id" name="id" value="${info.id }">
        <div class="row" style="margin:0;padding:0;">
            <div class="control-groaup span6">
                <label class="control-label" for="moveinfo_info_warehouseId" >仓库：</label>
                <div class="controls">
                    <div class="required span12" >
                        <input class="span10 select2_input" type = "text" id="moveinfo_info_warehouseId" name="warehouseId" value="${info.warehouseId}" placeholder="选择仓库" disabled/>
                        <input type="hidden" id="moveinfo_info_inId" value="${info.warehouseId}">
                        <input type="hidden" id="moveinfo_info_inName" value="${info.warehouseCodeName}">
                    </div>
                </div>
            </div>
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="moveinfo_info_userName" >移库人员：</label>
                <div class="controls">
                    <div class="" >
                        <input type="text" id="moveinfo_info_userName" class="span10" name="userName" value="${info.userName}"  placeholder="移库人员" readonly/>
                    </div>
                </div>
            </div>
        </div>
        <%--<div class="row" style="margin:0;padding:0;">
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="moveinfo_info_info" >备注：</label>
                <div class="controls">
                    <div class="" >
                        <input type="text" id="moveinfo_info_info" class="span10" name="info" value="${info.info}" readonly="readonly" placeholder="备注" />
                    </div>
                </div>
            </div>
        </div>--%>
    </form>

    <form id="hiddenForm" action="<%=path%>/move/detailExcel.do" method="POST" style="display: none;">
        <input id="hiddenForm_ids" name="ids" value=""/>
        <input id="info_id" name="infoid" value="">
    </form>
    <div style="margin-bottom:5px;margin-left: 25px;">
        <span class="btn btn-info" id="export">
           <i class="ace-icon fa fa-check bigger-110"></i>导出
        </span>
    </div>

    <div id="moveinfo_info_grid-div-c" style="width:100%;margin:0px auto;">
        <div id="moveinfo_info_fixed_tool_div" class="fixed_tool_div detailToolBar">
            <div id="moveinfo_info___toolbar__-c" style="float:left;overflow:hidden;"></div>
        </div>
        <table id="moveinfo_info_grid-table-c" style="width:100%;height:100%;"></table>
        <div id="moveinfo_info_grid-pager-c"></div>
    </div>
</div>
<iframe src="about:blank" name="_ifr" height="0" class="hidden"></iframe>
<script type="text/javascript" src="<%=path%>/static/js/techbloom/move/moveinfo/moveinfo_info.js"></script>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriData;      //表格数据
    var _grid;        //表格对象
    var selectParam = "";  //存放之前的查询条件

    $('[data-rel=tooltip]').tooltip();

    $("#moveinfo_info_qaCode").select2({
        placeholder : "请选择质保单号",//文本框的提示信息
        minimumInputLength : 0, //至少输入n个字符，才去加载数据
        allowClear : true, //是否允许用户清除文本信息
        multiple: true,
        closeOnSelect:false,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax : {
            url : context_path + '/move/getQaCode',
            dataType : 'json',
            delay : 250,
            data : function(term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                selectParam = term;
                return {
                    queryString : term, //联动查询的字符
                    pageSize : 15, //一次性加载的数据条数
                    pageNo : pageNo //页码
                }
            },
            results : function(data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results : res,
                        more : more
                    };
                } else {
                    return {
                        results : {
                            "id" : "0",
                            "text" : "没有更多结果"
                        }
                    };
                }
            },
            cache : true
        }
    });

    //初始化表格
    _grid =  $("#moveinfo_info_grid-table-c").jqGrid({
        url : context_path + "/move/detailList.do?id="+$("#moveinfo_info_id").val(),
        datatype : "json",
        colNames : [ "详情主键","质保单号","批次号","物料编码","物料名称","盘类型","盘规格","数量","移出库位","移入库位","状态"],
        colModel : [
            {name : "id",index : "id",width : 20,hidden:true},
            {name : "qaCode",index:"qaCode",width : 15},
            {name : "batchNo",index:"batchNo",width : 20},
            {name : "materialCode",index:"materialCode",width : 20},
            {name : "materialName",index:"materialName",width : 30},
            {name : "drumType",index:"drumType",width : 15},
            {name : "model",index:"model",width : 30},
            {name : "meter",index:"meter",width : 10},
            {name : "outShelfCode",index:"outShelfCode",width : 25},
            {name : "inShelfCode",index:"inShelfCode",width : 25}, 
            {name : "state",index : "state",width : 20,
                formatter:function(cellValue,option,rowObject){
                    if(cellValue=='1'){
                        return "<span style='color:red;font-weight:bold;'>生效</span>";
                    }else if(cellValue=='2'){
                        return "<span style='color:blue;font-weight:bold;'>开始移库出库</span>";
                    }else if(cellValue=='3'){
                        return "<span style='color:green;font-weight:bold;'>完成移库出库</span>";
                    }else if(cellValue=='4'){
                        return "<span style='color:blue;font-weight:bold;'>开始移库入库</span>";
                    }else if(cellValue=='5'){
                        return "<span style='color:green;font-weight:bold;'>完成移库入库</span>";
                    }
                }
            }
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : "#moveinfo_info_grid-pager-c",
        sortname : "t1.id",
        sortorder : "asc",
        altRows: true,
        viewrecords : true,
        autowidth:true,
        multiselect:true,
        multiboxonly: true,
        loadComplete : function(data){
            var table = this;
            setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            oriData = data;
            $(window).triggerHandler("resize.jqGrid");
        },
        cellEdit: true,
        cellsubmit : "clientArray",
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据",
    });

    //在分页工具栏中添加按钮
    $("#moveinfo_info_grid-table-c").navGrid("#moveinfo_info_grid-pager-c",
    {edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#moveinfo_info_grid-pager-c",{
            caption:"",
            buttonicon:"ace-icon fa fa-refresh green",
            onClickButton: function(){
                $("#moveinfo_info_grid-table-c").jqGrid("setGridParam",
                    {
                        url:context_path + "/move/detailList.do?id="+$("#moveinfo_info_id").val(),
                    }
                ).trigger("reloadGrid");
            }
        });

    $(window).on("resize.jqGrid", function () {
        $("#moveinfo_info_grid-table-c").jqGrid("setGridWidth", $("#moveinfo_info_grid-div-c").width() - 3 );

        var height = $(".layui-layer-title",_grid.parents(".layui-layer")).height()+ $("#moveinfo_info_baseInfor").outerHeight(true)+
        $("#moveinfo_info_materialDiv").outerHeight(true)+ $("#moveinfo_info_grid-pager-c").outerHeight(true)+
        $("#moveinfo_info_fixed_tool_div.fixed_tool_div.detailToolBar").outerHeight(true)+
        $("#gview_moveinfo_info_grid-table-c .ui-jqgrid-hbox").outerHeight(true);

        $("#moveinfo_info_grid-table-c").jqGrid("setGridHeight",_grid.parents(".layui-layer").height()-height);
    });
    $(window).triggerHandler("resize.jqGrid");

    /**仓库选择*/
    $("#moveinfo_info_baseInfor #moveinfo_info_warehouseId").select2({
        placeholder: "选择仓库",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/move/selectWarehouse.do",
            type: "POST",
            dataType: 'json',
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });

    if($("#moveinfo_info_baseInfor #moveinfo_info_inId").val()!=""){
        $("#moveinfo_info_baseInfor #moveinfo_info_warehouseId").select2("data", {
            id: $("#moveinfo_info_baseInfor #moveinfo_info_inId").val(),
            text: $("#moveinfo_info_baseInfor #moveinfo_info_inName").val()
        });
    }

    $("#export").click(function(){
        $("#hiddenForm_ids").val(jQuery("#moveinfo_info_grid-table-c").jqGrid("getGridParam", "selarrrow"));
        $("#info_id").val($("#moveinfo_info_id").val());

        $("#hiddenForm").submit();
    });

</script>
