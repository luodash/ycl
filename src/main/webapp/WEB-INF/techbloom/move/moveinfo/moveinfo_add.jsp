<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
%>
<div id="moveinfo_add_page" class="row-fluid" style="height: inherit;margin:0px">
    <form action="" class="form-horizontal" id="moveinfo_add_baseInfor" name="baseInfor" method="post" target="_ifr" style="border-bottom: solid 2px #3b73af;">
        <input type="hidden" id="moveinfo_add_id" name="id" value="${moveStorageId}">
        <div class="row" style="margin:0;padding:0;">
        	<div class="control-groaup span6">
                <label class="control-label" for="moveinfo_add_factoryCode" >厂区：</label>
                <div class="controls">
                    <div class="required span12" >
                        <input class="span10 select2_input" type = "text" id="moveinfo_add_factoryCode" name="factoryCode" value="" placeholder="选择厂区"/>
                        <input type="hidden" id="moveinfo_add_factoryid">
                        <input type="hidden" id="moveinfo_add_factoryname">
                    </div>
                </div>
            </div>
            <div class="control-groaup span6">
                <label class="control-label" for="moveinfo_add_warehouseId" >仓库：</label>
                <div class="controls">
                    <div class="required span12" >
                        <input class="span10 select2_input" type = "text" id="moveinfo_add_warehouseId" name="warehouseId" value="" placeholder="选择仓库"/>
                        <input type="hidden" id="moveinfo_add_inId">
                        <input type="hidden" id="moveinfo_add_inName">
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin:0;padding:0;">
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="moveinfo_add_userName" >移库人员：</label>
                <div class="controls">
                    <div class="" >
                        <input type="text" id="moveinfo_add_userName" class="span10" name="userName" value="${userName}"  placeholder="移库人员" readonly/>
                    </div>
                </div>
            </div>
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="moveinfo_add_info" >备注：</label>
                <div class="controls">
                    <div class="" >
                        <input type="text" id="moveinfo_add_info" class="span10" name="info" value=""  placeholder="备注" />
                    </div>
                </div>
            </div>
        </div>
        <%--<div style="margin-bottom:5px;">
            <span class="btn btn-info" id="moveinfo_add_formSave">
		       <i class="ace-icon fa fa-check bigger-110"></i>保存
            </span>
            <span class="btn btn-info" id="moveinfo_add_outformSubmit" onclick="outformSubmitBtn();">
		        <i class="ace-icon fa fa-check bigger-110"></i>&nbsp;提交
            </span>
        </div>--%>
    </form>
    <div id="moveinfo_add_materialDiv" style="margin:10px;">
        <label class="inline" for="moveinfo_add_qaCode">质保单号：</label>
        <input type="text" id = "moveinfo_add_qaCode" name="qaCode" style="width:350px;margin-right:10px;" />
        <button id="moveinfo_add_addMaterialBtn" class="btn btn-xs btn-primary" onclick="addDetail();">
            <i class="icon-plus" style="margin-right:6px;"></i>添加
        </button>
    </div>
    <div id="moveinfo_add_grid-div-c" style="width:100%;margin:0px auto;">
        <div id="moveinfo_add_fixed_tool_div" class="fixed_tool_div detailToolBar">
            <div id="moveinfo_add___toolbar__-c" style="float:left;overflow:hidden;"></div>
        </div>
        <table id="moveinfo_add_grid-table-c" style="width:100%;height:100%;"></table>
        <div id="moveinfo_add_grid-pager-c"></div>
    </div>
</div>
<iframe src="about:blank" name="_ifr" height="0" class="hidden"></iframe>
<script type="text/javascript" src="<%=path%>/static/js/techbloom/move/moveinfo/moveinfo_add.js"></script>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriData;      //表格数据
    var _grid;        //表格对象
    var selectData = 0;   //存放物料选择框中的值
    var selectParam = "";  //存放之前的查询条件
    var factorySelect;
    var moveCode = "";   //移库单号

    $("#moveinfo_add_baseInfor").validate({
        rules: {
            "warehouseId": {
                required: true,
            },
        },
        messages: {
            "warehouseId": {
                required: "请选择仓库!",
            },
        },
        errorClass: "help-inline",
        errorElement: "span",
        highlight:function(element, errorClass, validClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        }
    })

    $("#moveinfo_add_formSave").click(function(){
        if($('#moveinfo_add_baseInfor').valid()){
            //通过验证：获取表单数据，保存表单信息
            saveFormInfo($('#moveinfo_add_baseInfor').serialize());
        }
    });

    //清空物料多选框中的值
    function removeChoice(){
        $("#s2id_qaCode .select2-choices").children(".select2-search-choice").remove();
        $("#moveinfo_add_qaCode").select2("val","");
        selectData = 0;
    }

    $('[data-rel=tooltip]').tooltip();

    $("#moveinfo_add_qaCode").select2({
        placeholder : "请选择质保单号",//文本框的提示信息
        minimumInputLength : 0, //至少输入n个字符，才去加载数据
        allowClear : true, //是否允许用户清除文本信息
        multiple: true,
        closeOnSelect:false,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax : {
            url : context_path + '/move/getQaCode',
            dataType : 'json',
            delay : 250,
            data : function(term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                selectParam = term;
                return {
                    queryString : term, //联动查询的字符
                    warehouseId:$("#moveinfo_add_baseInfor #moveinfo_add_warehouseId").val(),
                    factoryCode:$("#moveinfo_add_baseInfor #moveinfo_add_factoryCode").val(),
                    pageSize : 15, //一次性加载的数据条数
                    pageNo : pageNo, //页码
                    move : "add"
                }
            },
            results : function(data, pageNo) {
                if($('#moveinfo_add_baseInfor').valid()) {
                    var res = data.result;
                    if (res.length > 0) { //如果没有查询到数据，将会返回空串
                        var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                        return {
                            results: res,
                            more: more
                        };
                    } else {
                        return {
                            results: {
                                "id": "0",
                                "text": "没有更多结果"
                            }
                        };
                    }
                }else {
                    layer.alert("请选择厂区和仓库！")
                }
            },
            cache : true
        }
    });

    $("#moveinfo_add_qaCode").on("change",function(e){
        var datas=$("#moveinfo_add_qaCode").select2("val");
        selectData = datas;
        var selectSize = datas.length;
        if(selectSize>1){
            var $tags = $("#moveinfo_add_qaCode .select2-choices");
            var $choicelist = $tags.find(".select2-search-choice");
            var $clonedChoice = $choicelist[0];
            $tags.children(".select2-search-choice").remove();
            $tags.prepend($clonedChoice);
            $tags.find(".select2-search-choice").find("div").html(selectSize+"个被选中");
            $tags.find(".select2-search-choice").find("a").removeAttr("tabindex");
            $tags.find(".select2-search-choice").find("a").attr("href","#");
            $tags.find(".select2-search-choice").find("a").attr("onclick","removeChoice();");
        }
        //执行select的查询方法
        $("#moveinfo_add_qaCode").select2("search",selectParam);
    });

    //工具栏
    $("#moveinfo_add___toolbar__-c").iToolBar({
        id:"moveinfo_add___tb__01",
        items:[
            {label:"删除", onclick:delDetail}
        ]
    });


    //初始化表格
    _grid =  $("#moveinfo_add_grid-table-c").jqGrid({
        url : context_path + "/move/detailList.do",
        datatype : "json",
        colNames : ["详情主键","质保单号","批次号","物料编码","物料名称","盘类型","盘规格","数量","移出库位","移入库位","状态"],
        colModel : [
        	{name : "id",index : "id",width : 20,hidden:true},
            {name : "qaCode",index:"qaCode",width : 15},
            {name : "batchNo",index:"batchNo",width : 20},
            {name : "materialCode",index:"materialCode",width : 20},
            {name : "materialName",index:"materialName",width : 30},
            {name : "drumType",index:"drumType",width : 15},
            {name : "model",index:"model",width : 30},
            {name : "meter",index:"meter",width : 10},
            {name : "outShelfCode",index:"outShelfCode",width : 25},
            {name : "inShelfCode",index:"inShelfCode",width : 25}, 
            {name : "state",index : "state",width : 20,
                formatter:function(cellValue,option,rowObject){
                    if(cellValue=='1'){
                        return "<span style='color:red;font-weight:bold;'>生效</span>";
                    }else if(cellValue=='2'){
                        return "<span style='color:blue;font-weight:bold;'>开始移库出库</span>";
                    }else if(cellValue=='3'){
                        return "<span style='color:green;font-weight:bold;'>完成移库出库</span>";
                    }else if(cellValue=='4'){
                        return "<span style='color:blue;font-weight:bold;'>开始移库入库</span>";
                    }else if(cellValue=='5'){
                        return "<span style='color:green;font-weight:bold;'>完成移库入库</span>";
                    }
                }
            }
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : "#moveinfo_add_grid-pager-c",
        sortname : "t1.id",
        sortorder : "asc",
        altRows: true,
        viewrecords : true,
        autowidth:true,
        multiselect:true,
        multiboxonly: true,
        loadComplete : function(data){
            var table = this;
            setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            oriData = data;
            $(window).triggerHandler("resize.jqGrid");
        },
        cellEdit: true,
        cellsubmit : "clientArray",
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据",
    });
    //在分页工具栏中添加按钮
    $("#moveinfo_add_grid-table-c").navGrid("#moveinfo_add_grid-pager-c",
        {edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#moveinfo_add_grid-pager-c",{
            caption:"",
            buttonicon:"ace-icon fa fa-refresh green",
            onClickButton: function(){
                $("#moveinfo_add_grid-table-c").jqGrid("setGridParam",
                    {
                        url:context_path + "/move/detailList.do?id="+$("#moveinfo_add_id").val(),
                        postData: {id:$("#moveinfo_add_baseInfor #moveinfo_add_id").val(),queryJsonString:""} //发送数据  :选中的节点
                    }
                ).trigger("reloadGrid");
            }
        });

    $(window).on("resize.jqGrid", function () {
        $("#moveinfo_add_grid-table-c").jqGrid("setGridWidth", $("#moveinfo_add_grid-div-c").width() - 3 );
        var height = $(".layui-layer-title",_grid.parents(".layui-layer")).height()+
        $("#moveinfo_add_baseInfor").outerHeight(true)+
        $("#moveinfo_add_materialDiv").outerHeight(true)+
        $("#moveinfo_add_grid-pager-c").outerHeight(true)+
        $("#moveinfo_add_fixed_tool_div.fixed_tool_div.detailToolBar").outerHeight(true)+
        $("#gview_moveinfo_add_grid-table-c .ui-jqgrid-hbox").outerHeight(true);
        $("#moveinfo_add_grid-table-c").jqGrid("setGridHeight",_grid.parents(".layui-layer").height()-height);
    });
    $(window).triggerHandler("resize.jqGrid");


    //添加质保单号
    function addDetail(){
        if(selectData!=0){
            //将选中的物料添加到数据库中
            $.ajax({
                type:"POST",
                url:context_path + "/move/saveMoveStorageDetail",
                data:{
                    qaCodeIds:selectData.toString(),
                    factoryCode:$("#moveinfo_add_baseInfor #moveinfo_add_factoryCode").val(),
                    warehouseId:$("#moveinfo_add_baseInfor #moveinfo_add_warehouseId").val(),
                    type:'add',
                    moveCode:moveCode
                },
                dataType:"json",
                success:function(data){
                    removeChoice();   //清空下拉框中的值
                    if(data.result!=null){
                        moveCode = data.moveCode;
                        layer.msg(data.msg, {icon: 1,time:1000});
                        $("#moveinfo_add_grid-table-c").jqGrid('setGridParam',
                            {
                                url:context_path + "/move/detailList.do?id="+data.id,
                            }
                        ).trigger("reloadGrid");
                        //重新加载详情表格
                        moveinfo_list_gridReload();
                        $("#moveinfo_add_baseInfor #moveinfo_add_id").val(data.id);
                    }else{
                        layer.alert("添加失败");
                    }
                }
            });
        }else{
            layer.alert("请选择物料！");
        }
    }

    $("#moveinfo_add_baseInfor #moveinfo_add_warehouseId").select2({
        placeholder: "选择仓库",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/car/selectWarehouse",
            type: "POST",
            dataType: 'json',
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    factoryCodeId:factorySelect
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });
    
    $("#moveinfo_add_baseInfor #moveinfo_add_factoryCode").select2({
        placeholder: "选择厂区",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/factoryArea/getFactoryList",
            type: "POST",
            dataType: 'json',
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo //页码
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });
    
    $("#moveinfo_add_baseInfor #moveinfo_add_factoryCode").on("change",function(e){
    	$("#moveinfo_add_baseInfor #moveinfo_add_warehouseId").empty();
    	factorySelect = $("#moveinfo_add_baseInfor #moveinfo_add_factoryCode").val();
    	$.ajax({
            type:"POST",
            url:context_path + "/warehouselist/getFirstWarehouse.do",
            data:{factoryCode:factorySelect},
            dataType:"json",
            success:function(data){
		        $("#moveinfo_add_baseInfor #moveinfo_add_warehouseId").select2("data", {
		            id: data.id,
		            text: data.warehouseCodeName
		        });
            }
        });
    });

</script>
