<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<div id="movein_manual_in_page" class="row-fluid" style="height: inherit;">
	<form id="movein_manual_in_carForm" class="form-horizontal" style="overflow: auto; height: calc(100% - 70px);">
        <input type="hidden" id="movein_manual_in_id" name="id" value="${info.id}">
		<input type="hidden" id="movein_manual_in_warehouseId" value="${info.warehouseId}">
		<input type="hidden" id="movein_manual_in_carId" value="${info.carId}">

		<div class="control-group">
			<label class="control-label" >厂区：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input id="movestorage_manual_factory" name="factory" type="text" style="width: calc(100% - 85px);" value="${factory}" disabled>
				</div>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label" >仓库：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<select id="movestorage_manual_warehouseId" name="warehouseId" class="span9" disabled>
						<c:forEach items="${warehouseList}" var="tp">
							<option value="${tp.id}" <c:if test="${info.warehouseCode==tp.code }">selected="selected"</c:if>>${tp.warehouseCodeName}</option>
						</c:forEach>
					</select>
				</div>
			</div>
		</div>

        <div class="control-group">
            <label class="control-label" for="movein_manual_in_shelfId" >库位：</label>
            <div class="controls">
                <div class="required span12" >
                    <input class="span11 select2_input" type = "text" id="movein_manual_in_shelfId" name="shelfId" value="${info.shelfId}" placeholder="库位"/>
                    <input type="hidden" id="movein_manual_in_wid" value="${info.shelfId}">
                    <input type="hidden" id="movein_manual_in_wname" value="${info.shelfCode}">
                </div>
            </div>
        </div>
	</form>
	<div class="field-button" style="text-align: center;border-top: 0px;margin: 15px auto;">
		<span class="btn btn-info" onclick="saveForm();">
		   <i class="ace-icon fa fa-check bigger-110"></i>确认入库
		</span>
		<span class="btn btn-danger" onclick="layer.closeAll();">
		   <i class="icon-remove"></i>&nbsp;取消
		</span>
	</div>
</div>
<div id="movein_manual_in_doing"></div>
<script type="text/javascript">
    $("#movein_manual_in_carForm").validate({
        rules:{
            "shelfId":{
                required:true,
            },
        },
        messages:{
            "shelfId":{
                required:"请选择库位！",
            },
        },
        errorClass: "help-inline",
        errorElement: "span",
        highlight:function(element, errorClass, validClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        }
    });
	//确定按钮点击事件
    function saveForm() {
        if ($("#movein_manual_in_carForm").valid()){
            confirmInstorage($("#movein_manual_in_carForm").serialize());
        }
    }

    /**入库确认*/
    function confirmInstorage(bean) {
        $.ajax({
            url: context_path + "/movein/confirmInstorage",
            type: "POST",
            data: bean,
            dataType: "JSON",
            success: function (data) {
				iTsai.form.deserialize($("#movein_list_hiddenQueryForm"), iTsai.form.serialize($("#movein_list_queryForm")));
				var queryParam = iTsai.form.serialize($("#movein_list_hiddenQueryForm"));
				var queryJsonString = JSON.stringify(queryParam);
                if (Boolean(data.result)) {
                    layer.msg(data.msg, {icon: 1});
                    //关闭当前窗口
                    layer.close($queryWindow);
                    //刷新列表
                    $("#movein_list_grid-table").jqGrid('setGridParam',
                         {
                             postData: {queryJsonString: queryJsonString}
                         }).trigger("reloadGrid");
                } else {
                    layer.alert(data.msg, {icon: 2});
                }
            },
            complete:function(XMLHttpRequest,textStatus){
                $("#movein_manual_in_doing").empty(); 
            },
            error:function(XMLHttpRequest){
            	$("#movein_manual_in_doing").empty(); 
                alert(XMLHttpRequest.readyState);
                alert("出错啦！！！");
            }
        });
    }

    /**库位选择*/
    $("#movein_manual_in_carForm #movein_manual_in_shelfId").select2({
        placeholder: "选择仓库",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/movein/selectUnbindShelf.do",
            type: "POST",
            dataType: 'json',
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
					warehouseId: $("#movein_manual_in_warehouseId").val(),
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo //页码
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });

    if($("#movein_manual_in_carForm #movein_manual_in_wid").val()!=""){
        $("#movein_manual_in_carForm #movein_manual_in_shelfId").select2("data", {
            id: $("#movein_manual_in_carForm #movein_manual_in_wid").val(),
            text: $("#movein_manual_in_carForm #movein_manual_in_wname").val()
        });
    }

</script>