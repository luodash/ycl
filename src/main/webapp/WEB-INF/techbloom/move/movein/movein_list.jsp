<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="movein_list_grid-div">
    <form id="movein_list_hiddenForm" action="<%=path%>/movein/materialExcel" method="POST" style="display: none;">
        <input id="movein_list_ids" name="ids" value=""/>
        <input id="movein_list_queryqaCode" name="queryqaCode" value=""/>
        <input id="movein_list_querycreateTime" name="querycreateTime" value=""/>
        <input id="movein_list_queryendTime" name="queryendTime" value=""/>
        <input id="movein_list_queryExportExcelIndex" name="queryExportExcelIndex" value=""/>
    </form>
    <form id="movein_list_hiddenQueryForm" style="display:none;">
        <input name="qaCode" value=""/>
        <input name="createTime" value=""/>
        <input name="endTime" value=""/>
    </form>
    <c:if test="${operationCode.webSearch==1}">
    <div class="query_box" id="movein_list_yy" title="查询选项">
        <form id="movein_list_queryForm" style="max-width:100%;">
            <ul class="form-elements">
                <li class="field-group field-fluid3">
                    <label class="inline" for="movein_list_qaCode" style="margin-right:20px;width: 100%;">
                        <span class="form_label" style="width:80px;">质保单号：</span>
                        <input id="movein_list_qaCode" name="qaCode" type="text" style="width: calc(100% - 85px);" placeholder="质保单号">
                    </label>
                </li>
                <li class="field-group field-fluid3">
                    <label class="inline" for="movein_list_createTime" style="margin-right:20px;width:100%;">
                        <span class="form_label" style="width:80px;">入库时间起：</span>
                        <input type="text" class="form-control date-picker" id="movein_list_createTime" name="createTime" value="" style="width: calc(100% - 85px);" placeholder="开始时间" />
                    </label>
                </li>
                <li class="field-group field-fluid3">
                    <label class="inline" for="movein_list_endTime" style="margin-right:20px;width:100%;">
                        <span class="form_label" style="width:80px;">入库时间止：</span>
                        <input type="text" class="form-control date-picker" id="movein_list_endTime" name="endTime" value="" style="width: calc(100% - 85px);" placeholder="结束时间" />
                    </label>
                </li>
            </ul>
            <div class="field-button" style="">
                <div class="btn btn-info" onclick="movein_list_queryOk();">
                    <i class="ace-icon fa fa-check bigger-110"></i>查询
                </div>
                <div class="btn" onclick="movein_list_reset();"><i class="ace-icon icon-remove"></i>重置</div>
                <!-- <a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a> -->
            </div>
        </form>
    </div>
    </c:if>
    <div id="movein_list_fixed_tool_div" class="fixed_tool_div">
        <div id="movein_list___toolbar__" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="movein_list_grid-table" style="width:100%;height:100%;"></table>
    <div id="movein_list_grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
	var movein_list_oriData; 
	var _grid;
    var movein_list_exportExcelIndex;

	//时间控件
	$(".date-picker").datetimepicker({format: "YYYY-MM-DD"});

	$(function  (){
	    $(".toggle_tools").click();
	});

	$("#movein_list___toolbar__").iToolBar({
	    id: "movein_list___tb__01",
	    items: [
            {label: "手动入库", onclick:function(){movein_list_manual()},iconClass:'icon-download-alt'},
	        {label: "导出",onclick:function(){movein_list_exportExecl()},iconClass:'icon-share'}
	    ]
	});

	var movein_list_queryForm_Data = iTsai.form.serialize($("#movein_list_queryForm"));

	_grid = jQuery("#movein_list_grid-table").jqGrid({
		url : context_path + "/movein/list.do",
	    datatype : "json",
	    colNames : [ "主键","移库单号","入库时间","质保单号","批次号","状态","确认人"],
	    colModel : [
                {name : "id",index : "id",hidden:true},
                {name : "moveCode",index : "moveCode",width : 60},
                {name : "inStorageTime",index : "inStorageTime",width : 60},
                {name : "qaCode",index : "qaCode",width : 60},
                {name : "batchNo",index : "batchNo",width :70},
                {name : "state",index : "state",width : 60,
                     formatter:function(cellValue,option,rowObject){
                         if(cellValue=='1'){
                             return "<span style='color:red;font-weight:bold;'>未移库出库</span>";
                         }else if(cellValue=='2'){
                             return "<span style='color:blue;font-weight:bold;'>开始移库出库</span>";
                         }else if(cellValue=='3'){
                             return "<span style='color:orange;font-weight:bold;'>完成移库出库</span>";
                         }else if(cellValue=='4'){
                             return "<span style='color:brown;font-weight:bold;'>开始移库入库</span>";
                         }else if(cellValue=='5'){
                             return "<span style='color:green;font-weight:bold;'>完成移库入库</span>";
                         }
                     }
                },
                {name : "pickManName",index : "pickManName",width : 60}
        ],
	    rowNum : 20,
	    rowList : [ 10, 20, 30 ],
	    pager : "#movein_list_grid-pager",
	    sortname : "t1.id",
	    sortorder : "desc",
        altRows: true,
        viewrecords : true,
        hidegrid:false,
    	autowidth:true,
        multiselect:true,
        multiboxonly: true,
        loadComplete : function(data){
           	var table = this;
           	setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
           	movein_list_oriData = data;
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
	});

	jQuery("#movein_list_grid-table").navGrid("#movein_list_grid-pager",
    {edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#movein_list_grid-pager",{
		caption:"",   
		buttonicon:"fa fa-refresh green",   
		onClickButton: function(){   
			$("#movein_list_grid-table").jqGrid("setGridParam", 
					{
	                    url : context_path + "/movein/list.do",
					}
			).trigger("reloadGrid");
		}
	}).navButtonAdd("#movein_list_grid-pager",{
        caption: "",
        buttonicon:"fa icon-cogs",
        onClickButton : function (){
            jQuery("#movein_list_grid-table").jqGrid("columnChooser",{
                done: function(perm, cols){
                    // dynamicColumns(cols,materials_list_dynamicDefalutValue);
                    $("#movein_list_grid-table").jqGrid("setGridWidth", $("#movein_list_grid-div").width());
                    movein_list_exportExcelIndex = perm;
                }
            });
        }
    });

	$(window).on("resize.jqGrid", function () {
		$("#movein_list_grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);
		$("#movein_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-10-
		$("#movein_list_yy").outerHeight(true)-$("#movein_list_fixed_tool_div").outerHeight(true)-
		$("#movein_list_grid-pager").outerHeight(true)-$("#gview_movein_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
	});
	$(window).triggerHandler("resize.jqGrid");

	//excel报表导出
	function movein_list_exportExecl(){
	    $("#movein_list_hiddenForm #movein_list_ids").val(jQuery("#movein_list_grid-table").jqGrid("getGridParam", "selarrrow"));
	    $("#movein_list_hiddenForm #movein_list_queryqaCode").val($("#movein_list_queryForm #movein_list_qaCode").val());
        $("#movein_list_hiddenForm #movein_list_querycreateTime").val($("#movein_list_queryForm #movein_list_createTime").val());
        $("#movein_list_hiddenForm #movein_list_queryendTime").val($("#movein_list_queryForm #movein_list_endTime").val());
        $("#movein_list_hiddenForm #movein_list_queryExportExcelIndex").val(movein_list_exportExcelIndex);
	    $("#movein_list_hiddenForm").submit();
	}

    /**开始入库*/
	function movein_list_startin(){
	    var selectAmount = getGridCheckedNum("#movein_list_grid-table");
	    if(selectAmount==0){
	        layer.msg("请选择一条记录！",{icon:2});
	        return;
	    }else if(selectAmount>1){
	        layer.msg("只能选择一条记录！",{icon:8});
	        return;
	    }
	    var rowData = jQuery("#movein_list_grid-table").jqGrid('getRowData', $("#movein_list_grid-table").jqGrid('getGridParam','selrow')).state;
	    if (rowData.indexOf("开始移库入库")>-1) {
	        layer.alert("入库中,请勿重复提交");
	        return;
	    }
	    if (rowData.indexOf("完成移库入库")>-1) {
	        layer.alert("入库完成,请勿重复提交");
	        return;
	    }
	    $.post(context_path + "/movein/storagestart.do", {
	        id:jQuery("#movein_list_grid-table").jqGrid("getGridParam", "selrow")
        }, function (str){
	        $queryWindow=layer.open({
	            title : "仓库与行车确认",
	            type:1,
	            skin : "layui-layer-molv",
	            area : "600px",
	            shade : 0.6, //遮罩透明度
	            moveType : 1, //拖拽风格，0是默认，1是传统拖动
	            anim : 2,
	            content : str,
	            success: function (layero, index) {
	                layer.closeAll('loading');
	            }
	        });
	    });
	}

	//入库确认
	function movein_list_confirmCars(){
	    var selectAmount = getGridCheckedNum("#movein_list_grid-table");
	    if(selectAmount==0){
	        layer.msg("请选择一条记录！",{icon:2});
	        return;
	    }else if(selectAmount>1){
	        layer.msg("只能选择一条记录！",{icon:8});
	        return;
	    }
	    var rowData = jQuery("#movein_list_grid-table").jqGrid('getRowData', $("#movein_list_grid-table").jqGrid('getGridParam','selrow')).state;
	    if (rowData.indexOf("完成移库出库")>-1) {
	        layer.alert("请先执行开始入库操作！");
	        return;
	    }
	    if (rowData.indexOf("完成移库入库")>-1) {
	        layer.alert("入库完成,请勿重复提交！");
	        return;
	    }
	    $.post(context_path + "/movein/storageconfirm.do", {
	        id:jQuery("#movein_list_grid-table").jqGrid("getGridParam", "selrow")
        }, function (str){
	        $queryWindow=layer.open({
	            title : "入库确认",
	            type:1,
	            skin : "layui-layer-molv",
	            area : "600px",
	            shade : 0.6, //遮罩透明度
	            moveType : 1, //拖拽风格，0是默认，1是传统拖动
	            anim : 2,
	            content : str,
	            success: function (layero, index) {
	                layer.closeAll('loading');
	            }
	        });
	    });
	}

    /**手动入库*/
    function movein_list_manual(){
        var selectAmount = getGridCheckedNum("#movein_list_grid-table");
        if(selectAmount==0){
            layer.msg("请选择一条记录！",{icon:2});
            return;
        }else if(selectAmount>1){
            layer.msg("只能选择一条记录！",{icon:8});
            return;
        }
        var rowData = jQuery("#movein_list_grid-table").jqGrid('getRowData', $("#movein_list_grid-table").jqGrid('getGridParam','selrow')).state;
        if (rowData.indexOf('完成调拨入库')>-1) {
            layer.alert("入库完成,请勿重复提交！");
            return;
        }
        $.post(context_path + "/movein/storageManual.do", {
            id:jQuery("#movein_list_grid-table").jqGrid("getGridParam", "selrow")
        }, function (str){
            $queryWindow=layer.open({
                title : "手动入库",
                type:1,
                skin : "layui-layer-molv",
                area : "600px",
                shade : 0.6, //遮罩透明度
                moveType : 1, //拖拽风格，0是默认，1是传统拖动
                anim : 2,
                content : str,
                success: function (layero, index) {
                    layer.closeAll('loading');
                }
            });
        });
    }

	/**
	 * 查询按钮点击事件
	 */
	 function movein_list_queryOk(){
		 var queryParam = iTsai.form.serialize($("#movein_list_queryForm"));
		 //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
		 movein_list_queryByParam(queryParam);
	 }
	
	 function movein_list_queryByParam(jsonParam) {
        iTsai.form.deserialize($("#movein_list_hiddenQueryForm"), jsonParam);
        var queryParam = iTsai.form.serialize($("#movein_list_hiddenQueryForm"));
        var queryJsonString = JSON.stringify(queryParam);
        $("#movein_list_grid-table").jqGrid("setGridParam",
            {
                postData: {queryJsonString: queryJsonString}
            }
        ).trigger("reloadGrid");
	}

	//重置
	function movein_list_reset(){
	     $("#movein_list_queryForm #movein_list_shelveId").select2("val","");
	     $("#movein_list_queryForm #movein_list_type").select2("val","");
		 iTsai.form.deserialize($("#movein_list_queryForm"),movein_list_queryForm_Data);
		 movein_list_queryByParam(movein_list_queryForm_Data);
	}

</script>