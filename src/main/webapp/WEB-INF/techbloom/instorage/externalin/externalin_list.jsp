<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="externalin_list_grid-div">
    <form id="externalin_list_hiddenForm" action="<%=path%>/storagein/materialExcel" method="POST" style="display: none;">
        <input id="externalin_list_ids" name="ids" value=""/>
    </form>
    <form id="externalin_list_hiddenQueryForm" style="display:none;">
        <input id="externalin_list_org" name="org" value=""/>
        <input id="externalin_list_factoryId" name="factoryId" value=""/>
        <input id="externalin_list_rfid" name="rfid" value=""/>
        <input id="externalin_list_qaCode" name="qaCode" value=""/>
        <input id="externalin_list_batchNo" name="batchNo" value=""/>
    </form>
    <%-- <c:if test="${operationCode.webSearch==1}"> --%>
    <div class="query_box" id="externalin_list_yy" title="查询选项">
        <form id="externalin_list_queryForm" style="max-width:100%;">
            <ul class="form-elements">
                <li class="field-group field-fluid3">
                    <label class="inline" for="externalin_list_org" style="margin-right:20px;width: 100%;">
                        <span class="form_label" style="width:80px;">组织机构：</span>
                        <input id="externalin_list_org" name="org" type="text" style="width: calc(100% - 85px);" placeholder="组织机构">
                    </label>
                </li>
                <li class="field-group field-fluid3">
                    <label class="inline" for="externalin_list_factoryId" style="margin-right:20px;width: 100%;">
                        <span class="form_label" style="width:80px;">产线：</span>
                        <input type="text"  id="externalin_list_factoryId" name="factoryId" style="width: calc(100% - 85px);" placeholder="入库生产厂ID" />
                    </label>
                </li>
                <li class="field-group field-fluid3">
                    <label class="inline" for="externalin_list_rfid" style="margin-right:20px;width: 100%;">
                        <span class="form_label" style="width:80px;">RFID：</span>
                        <input type="text" id="externalin_list_rfid" name="rfid" style="width: calc(100% - 85px);" placeholder="RFID" />
                    </label>
                </li>
                <li class="field-group-top field-group field-fluid3">
                    <label class="inline" for="externalin_list_qaCode" style="margin-right:20px;width: 100%;">
                        <span class="form_label" style="width:80px;">质保单号：</span>
                        <input id="externalin_list_qaCode" name="qaCode" type="text" style="width: calc(100% - 85px);" placeholder="质保单号">
                    </label>
                </li>
                <li class="field-group field-fluid3">
                    <label class="inline" for="externalin_list_batchNo" style="margin-right:20px;width: 100%;">
                        <span class="form_label" style="width:80px;">批次号：</span>
                        <input id="externalin_list_batchNo" name="batchNo" type="text" style="width: calc(100% - 85px);" placeholder="批次号">
                    </label>
                </li>
            </ul>
            <div class="field-button" style="">
                <div class="btn btn-info" onclick="externalin_list_queryOk();">
                    <i class="ace-icon fa fa-check bigger-110"></i>查询
                </div>
                <div class="btn" onclick="externalin_list_reset();"><i class="ace-icon icon-remove"></i>重置</div>
                <a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>
            </div>
        </form>
    </div>
    <%-- </c:if> --%>
    <div id="externalin_list_fixed_tool_div" class="fixed_tool_div">
        <div id="externalin_list___toolbar__" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="externalin_list_grid-table" style="width:100%;height:100%;"></table>
    <div id="externalin_list_grid-pager"></div>
</div>
<div id="externalin_list_doing"></div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript" src="<%=path%>/static/js/techbloom/wms/instorage/externalin/externalin_list.js"></script>
<script type="text/javascript">
	var oriData; 
	var _grid;
	
	//时间控件
	$(".date-picker").datetimepicker({format: "YYYY-MM-DD"});
	$(function  (){
	    $(".toggle_tools").click();
	});
	
	$("#externalin_list___toolbar__").iToolBar({
	    id: "externalin_list___tb__01",
	    items: [
	        {label: "添加", onclick:function(){externalin_list_addExternal()},iconClass:'icon-share'},
	        {label: "编辑", onclick:function(){externalin_list_editExternal()},iconClass:'icon-share'},
	        {label: "删除", onclick:function(){externalin_list_delExternal()},iconClass:'icon-share'}
	   ]
	});
	
	var externalin_list_queryForm_data = iTsai.form.serialize($("#externalin_list_queryForm"));
	
	_grid = jQuery("#externalin_list_grid-table").jqGrid({
		url : context_path + "/externalIn/list.do",
	    datatype : "json",
	    colNames : [ "主键","质保单号","组织机构",/* "物料编码", */"物料名称","米数","RFID",/* "盘外径","盘内径","规格型号","营销经理工号","营销经理名称", */"批次号","产线编号","段号","入库状态"],
	    colModel : [
	          {name : "id",index : "id",hidden:true},
              {name : "qaCode",index : "qaCode",width : 60},
              {name : "orgName",index : "orgName",width : 60},
	          /* {name : "materialCode",index : "materialCode",width : 60}, */
	          {name : "materialName",index : "materialName",width :70},
              {name : "meter",index : "meter",width :70},
              {name : "rfid",index : "rfid",width : 60},
              /* {name : "outerDiameter",index : "outerDiameter",width : 60},
              {name : "innerDiameter",index : "innerDiameter",width : 60},
              {name : "model",index : "model",width : 60},
              {name : "salesCode",index : "salesCode",width : 60},
              {name : "salesName",index : "salesName",width : 60}, */
              {name : "batchNo",index : "batchNo",width : 60},
              {name : "factoryid",index : "factoryid",width : 60},
              {name : "segmentno",index : "segmentno",width : 60},
              {name : "state",index : "state",width : 60,formatter:function(cellvalue,option,rowObject){
	              	if(cellvalue==1) return '<span style="color:red;font-weight:bold;">未确认</span>';
	              	if(cellvalue==2) return "已包装";
	              	if(cellvalue==3) return "入库中";
	                if(cellvalue==4) return "入库完成";
	              }
              }
        ],
	    rowNum : 20,
	    rowList : [ 10, 20, 30 ],
	    pager : "#externalin_list_grid-pager",
	    sortname : "a.id",
	    sortorder : "asc",
        altRows: true,
        viewrecords : true,
        hidegrid:false,
    	autowidth:true,
        multiselect:true,
        multiboxonly: true,
        loadComplete : function(data){
         	var table = this;
           	setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
           	oriData = data;
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
	});

	jQuery("#externalin_list_grid-table").navGrid("#externalin_list_grid-pager",{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#externalin_list_grid-pager",{  
		caption:"",   
		buttonicon:"fa fa-refresh green",   
		onClickButton: function(){   
			$("#externalin_list_grid-table").jqGrid("setGridParam", {
            	url : context_path + "/externalIn/list.do",
			}).trigger("reloadGrid");
		}
	});

	$(window).on("resize.jqGrid", function () {
		$("#externalin_list_grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);
		$("#externalin_list_grid-table").jqGrid("setGridHeight", $(".container-fluid").height()-
		$("#externalin_list_yy").outerHeight(true)-$("#externalin_list_fixed_tool_div").outerHeight(true)-
		$("#externalin_list_grid-pager").outerHeight(true)-
		$("#gview_externalin_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
	});

	$(window).triggerHandler("resize.jqGrid");

</script>