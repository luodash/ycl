<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<div id="externalin_edit_page" class="row-fluid" style="height: inherit;">
	<form id="externalin_edit_externalinForm" class="form-horizontal" style="overflow: auto; height: calc(100% - 70px);">
		<input type="hidden" id="externalin_edit_id" name="id" value="${instorageDetail.id}">
		<div class="control-group">
			<label class="control-label" for="externalin_edit_qaCode">质保单号：</label>
			<div class="controls">
				<div class="span12 required">
					<input type="text" class="span11 select2_input" id="externalin_edit_qaCode" name="qaCode" 
					 value="${instorageDetail.qaCode}" placeholder="质保单号" >
					<%-- <input type="hidden" id="qacode_id" value="${instorageDetail.qacode_id}">
					<input type="hidden" id="qacode_name" value="${instorageDetail.qacode_name}"> --%>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="externalin_edit_materialCode">物料编码：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="externalin_edit_materialCode" name="materialCode" 
					value="${instorageDetail.materialCode}" placeholder="物料编码" readonly="readonly">
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="externalin_edit_materialName">物料名称：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="externalin_edit_materialName" name="materialName" 
					value="${instorageDetail.materialName}" placeholder="物料名称" readonly="readonly">
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="externalin_edit_meter">米数：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="externalin_edit_meter" name="meter" 
					value="${instorageDetail.meter}" placeholder="米数" readonly="readonly">
				</div>
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label" for="externalin_edit_outerDiameter">盘外径：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="externalin_edit_outerDiameter" name="outerDiameter" 
					value="${instorageDetail.outerDiameter}" placeholder="盘外经">
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="externalin_edit_innerDiameter">盘内经：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="externalin_edit_innerDiameter" name="innerDiameter" 
					value="${instorageDetail.innerDiameter}" placeholder="盘内经">
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="externalin_edit_model">规格型号：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="externalin_edit_model" name="model" 
					value="${instorageDetail.model}" placeholder="规格型号">
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="externalin_edit_salesCode">营销经理工号：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="externalin_edit_salesCode" name="salesCode" 
					value="${instorageDetail.salesCode}" placeholder="营销经理工号">
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="externalin_edit_salesName">营销经理名称：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="externalin_edit_salesName" name="salesName" 
					value="${instorageDetail.salesName}" placeholder="营销经理名称">
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="externalin_edit_factoryid">产线编号：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="externalin_edit_factoryid" name="factoryid" 
					value="${instorageDetail.factoryid}" placeholder="产线编号">
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="externalin_edit_segmentno">段号：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="externalin_edit_segmentno" name="segmentno" 
					value="${instorageDetail.segmentno}" placeholder="段号">
				</div>
			</div>
		</div>
	</form>
	<div class="field-button" style="text-align: center;border-top: 0px;margin: 15px auto;">
		<span class="btn btn-info" onclick="saveExternalinInfo();">
		   <i class="ace-icon fa fa-check bigger-110"></i>保存
		</span>
		<span class="btn btn-danger" onclick="layer.closeAll();">
		   <i class="icon-remove"></i>&nbsp;取消
		</span>
	</div>
</div>
<script type="text/javascript">
	var editOrAdd = "${editOrAdd}";

	$("#externalin_edit_externalinForm").validate({
		rules:{
            "qaCode":{required:true},
            "outerDiameter":{required:true},
            "innerDiameter":{required:true},
            "model":{required:true},
            "salesCode":{required:true},
            "salesName":{required:true},
            "factoryid":{required:true},
            "segmentno":{required:true}
  		},
  		messages:{
            "qaCode":{required:"请输入质保单号！"},
            "outerDiameter":{required:"请输入盘外经！"},
            "innerDiameter":{required:"请输入盘内经！"},
            "model":{required:"请输入规格型号！"},
            "salesCode":{required:"请输入营销经理工号！"},
            "salesName":{required:"请输入营销经理名称！"},
            "factoryid":{required:"请输入产线编号！"},
            "segmentno":{required:"请输入段号！"}
  		},
  		errorClass: "help-inline",
		errorElement: "div",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
		}
  	});
 	 //保存/修改信息
    function saveExternalinInfo() {
    	if ($("#externalin_edit_externalinForm").valid()) {
	        $.ajax({
	            url: context_path + "/externalIn/saveInstorageDetail",
	            type: "POST",
	            data: $("#externalin_edit_externalinForm").serialize(),
	            dataType: "JSON",
	            success: function (data) {
	                if (Boolean(data.result)) {
	                    layer.msg("保存成功！", {icon: 1});
	                    //关闭当前窗口
	                    layer.close($queryWindow);
	                    //刷新列表
	                    $("#externalin_list_grid-table").jqGrid('setGridParam',{
	                    	postData: {queryJsonString: ""}
	                 	}).trigger("reloadGrid");
	                } else {
	                    layer.alert("保存失败，请稍后重试！", {icon: 2});
	                }
	            },
	            error:function(XMLHttpRequest){
	        		alert(XMLHttpRequest.readyState);
	        		alert("出错啦！！！");
	        	}
	        });
    	}
    }

     //质保单号
    $("#externalin_edit_externalinForm #externalin_edit_qaCode").select2({
        placeholder: "选择质保单",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        //multiple: true,//多选
        ajax: {
            url: context_path + "/externalIn/selectQacode",
            type: "POST",
            dataType: 'json',
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });

    //质保单号变更同步数据修改
    $('#externalin_edit_externalinForm #externalin_edit_qaCode').change(function(){
    	$.ajax({
            type: "GET",
            url: context_path+"/externalIn/selectInfo",
            data: {externalcableId:$("#externalin_edit_externalinForm #externalin_edit_qaCode").val()},
            dataType: "json",
            success: function(data){
                $("#externalin_edit_materialCode").val(data.materialCode);
                $("#externalin_edit_materialName").val(data.materialName);
                $("#externalin_edit_meter").val(data.meter);
	        }
        });
	});
    
    
    //编辑时查询质保单号(两种方法都行)
    /* if($("#externalin_edit_externalinForm #qacode_id").val()!=""){
        $("#externalin_edit_externalinForm #externalin_edit_qaCode").select2("data", {
            id: $("#externalin_edit_externalinForm #qacode_id").val(),
            text: $("#externalin_edit_externalinForm #qacode_name").val()
        });
    } */
    
    if(editOrAdd!=-1){
	    $.ajax({
		   	type:"POST",
		   	url:context_path + "/externalIn/getQacode",
		   	data:{id:$("#externalin_edit_externalinForm #externalin_edit_id").val()},
		   	dataType:"json",
		   	success:function(data){
			 	$("#externalin_edit_externalinForm #externalin_edit_qaCode").select2("data", {
					id: data.id,
					text: data.text
		        });
		   	}
	   	});
    }

</script>