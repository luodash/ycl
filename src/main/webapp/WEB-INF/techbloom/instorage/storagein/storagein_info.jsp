<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
%>
<div id="storagein_info_page" class="row-fluid" style="height: inherit;margin:0px">
    <form action="" class="form-horizontal" id="storagein_info_baseInfor" name="baseInfor" method="post" target="_ifr" >
        <input type="hidden" id="storagein_info_id" name="id" value="${info.id }">
        <div class="row" style="margin:0;padding:0;">
            <div class="control-group span6" style="display: inline">
                <label class="control-label" >仓库：</label>
                <div class="controls">
                        <input type="text" class="span10" value="${info.warehouseCode }"  placeholder="仓库" />
                </div>
            </div>
            <div class="control-group span6" style="display: inline">
                <label class="control-label" >库位：</label>
                <div class="controls">
                        <input type="text" class="span10" value="${info.shelfCode}"  placeholder="库位" />
                </div>
            </div>
        </div>
        <div class="row" style="margin:0;padding:0;">
            <div class="control-group span6" style="display: inline">
                <label class="control-label" >行车：</label>
                <div class="controls">
                        <input type="text"  class="span10" value="${info.carCode}"  placeholder="行车" />
                </div>
            </div>
        </div>
    </form>
</div>
<iframe src="about:blank" name="_ifr" height="0" class="hidden"></iframe>
<%-- <script type="text/javascript">
    var context_path = '<%=path%>';
    var oriData;      //表格数据
    var _grid;        //表格对象
    var selectParam = "";  //存放之前的查询条件

    $('[data-rel=tooltip]').tooltip();

    //初始化表格
    _grid =  $("#storagein_info_grid-table-c").jqGrid({
        url : context_path + "/deleiveryCon/detailList.do?id="+$("#storagein_info_id").val(),
        datatype : "json",
        colNames : [ "详情主键","质保单号","物料编码","发货单号","米数"],
        colModel : [
            {name : "id",index : "id",width : 20,hidden:true},
            {name : "qaCode",index:"qaCode",width : 20},
            {name : "materialCode",index:"materialCode",width : 20},
            {name : "shipNo",index:"ship_No",width : 20},
            {name : "meter",index:"meter",width : 20},
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : "#storagein_info_grid-pager-c",
        sortname : "id",
        sortorder : "asc",
        altRows: true,
        viewrecords : true,
        autowidth:true,
        multiselect:true,
        multiboxonly: true,
        loadComplete : function(data){
            var table = this;
            setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            oriData = data;
            $(window).triggerHandler("resize.jqGrid");
        },
        cellEdit: true,
        cellsubmit : "clientArray",
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据",
    });
    //在分页工具栏中添加按钮
    $("#storagein_info_grid-table-c").navGrid("#storagein_info_grid-pager-c",{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#storagein_info_grid-pager-c",{
            caption:"",
            buttonicon:"ace-icon fa fa-refresh green",
            onClickButton: function(){
                $("#storagein_info_grid-table-c").jqGrid("setGridParam",
                    {
                        url:context_path + "/deleiveryCon/detailList.do?id="+$("#storagein_info_id").val(),
                    }
                ).trigger("reloadGrid");
            }
        });

    $(window).on("resize.jqGrid", function () {
        $("#storagein_info_grid-table-c").jqGrid("setGridWidth", $("#storagein_info_grid-div-c").width() - 3 );
        var height = $(".layui-layer-title",_grid.parents(".layui-layer")).height()+
                     $("#storagein_info_baseInfor").outerHeight(true)+$("#storagein_info_materialDiv").outerHeight(true)+
                     $("#storagein_info_grid-pager-c").outerHeight(true)+$("#storagein_info_fixed_tool_div.fixed_tool_div.detailToolBar").outerHeight(true)+
                     $("#gview_storagein_info_grid-table-c .ui-jqgrid-hbox").outerHeight(true);
                     $("#storagein_info_grid-table-c").jqGrid("setGridHeight",_grid.parents(".layui-layer").height()-height);
    });
    $(window).triggerHandler("resize.jqGrid");

</script> --%>
