<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="desktopWriter_list_grid-div">
    <form id="desktopWriter_list_hiddenQueryForm" style="display:none;">
        <input  name="batchNo" value=""/>
        <input  name="factoryCode" value=""/>
        <input  name="factoryId" value=""/>
        <input  name="orderNo" value=""/>
        <input  name="orderLine" value=""/>
        <input  name="entityNo" value=""/>
        <input  name="qaCode" value=""/>
        <input  name="time" value=""/>
    </form>
    <c:if test="${operationCode.webSearch==1}">
    <div class="query_box" id="desktopWriter_list_yy" title="查询选项">
        <form id="desktopWriter_list_queryForm" style="max-width:100%;">
            <ul class="form-elements">
                <li class="field-group field-fluid3">
                    <label class="inline" for="desktopWriter_list_qaCode" style="margin-right:20px;width: 100%;">
                        <span class="form_label" style="width:61px;">质保号：</span>
                        <input id="desktopWriter_list_qaCode" name="qaCode" type="text" style="width: calc(100% - 66px);" placeholder="质保号">
                    </label>
                </li>
                <li class="field-group field-fluid3">
                    <label class="inline" for="desktopWriter_list_batchNo" style="margin-right:20px;width:100%;">
                        <span class="form_label" style="width:61px;">批次号：</span>
                        <input id="desktopWriter_list_batchNo" name="batchNo" type="text" style="width: calc(100% - 66px);" placeholder="批次号">
                    </label>

                </li>
                <li class="field-group field-fluid3">
                    <label class="inline" for="desktopWriter_list_entityNo" style="margin-right:20px;width: 100%;">
                        <span class="form_label" style="width:61px;">工单号：</span>
                        <input id="desktopWriter_list_entityNo" name="entityNo" type="text" style="width: calc(100% - 66px);" placeholder="工单号">
                    </label>
                </li>

                <li class="field-group-top field-group field-fluid3">
                    <label class="inline" for="desktopWriter_list_orderNo" style="margin-right:20px;width: 100%;">
                        <span class="form_label" style="width:61px;">订单号：</span>
                        <input id="desktopWriter_list_orderNo" name="orderNo" type="text" style="width: calc(100% - 66px);" placeholder="订单号">
                    </label>
                </li>
                <li class="field-group field-group field-fluid3">
                    <label class="inline" for="desktopWriter_list_orderLine" style="margin-right:20px;width: 100%;">
                        <span class="form_label" style="width:61px;">订单行：</span>
                        <input id="desktopWriter_list_orderLine" name="orderLine" type="text" style="width: calc(100% - 66px);" placeholder="订单行">
                    </label>
                </li>
                <li class="field-group field-group field-fluid3">
                    <label class="inline" for="desktopWriter_list_factoryId" style="margin-right:20px;width:100%;">
                        <span class="form_label" style="width:61px;">车间号：</span>
                        <input id="desktopWriter_list_factoryId" name="factoryId" type="text" style="width: calc(100% - 66px);" placeholder="车间">
                    </label>
                </li>

                <li class="field-group-top field-group field-fluid3">
                    <label class="inline" for="desktopWriter_list_factoryCode" style="margin-right:20px;width:100%;">
                        <span class="form_label" style="width:61px;">厂区：</span>
                        <input id="desktopWriter_list_factoryCode" name="factoryCode" type="text" style="width: calc(100% - 66px);" placeholder="厂区">
                    </label>
                </li>
            </ul>
            <div class="field-button" style="">
                <div class="btn btn-info" onclick="desktopWriter_list_queryOk();">
                    <i class="ace-icon fa fa-check bigger-110"></i>查询
                </div>
                <div class="btn" onclick="desktopWriter_list_reset();"><i class="ace-icon icon-remove"></i>重置</div>
                <a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>
            </div>
        </form>
    </div>
    </c:if>
    <div id="desktopWriter_list_fixed_tool_div" class="fixed_tool_div">
        <div id="desktopWriter_list_toolbar_" style="float:left;overflow:hidden;"></div>
    </div>
    <div  style="overflow-x:auto"><table id="desktopWriter_list_grid-table" style="width:100%;height:100%;"></table></div>
    <div id="desktopWriter_list_grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
    var desktopWriter_list_oriData;
    var desktopWriter_list_grid;

    $("input").keypress(function (e) {
        if (e.which == 13) {
            desktopWriter_list_queryOk();
        }
    });

    $(function  (){
        $(".toggle_tools").click();
    });

    $("#desktopWriter_list_toolbar_").iToolBar({
        id: "desktopWriter_list_tb_01",
        items: [
            {label: "绑卡", hidden:"${operationCode.webBindingCard}"==="1",onclick: desktopWriter_list_rfidBind, iconClass:'icon-inbox'}
        ]
    });

    var desktopWriter_list_queryForm_data = iTsai.form.serialize($("#desktopWriter_list_queryForm"));

    desktopWriter_list_grid = jQuery("#desktopWriter_list_grid-table").jqGrid({
        url : context_path + "/rfIdBind/rfidList",
        datatype : "json",
        colNames : [ "主键","质保单号","批次号","盘号","厂区","车间","订单号","订单行","工单号","盘具规格型号","数量","单位","颜色", "物料编码","物料名称",
            "盘外径", "盘类型","货物类型"],
        colModel : [
            {name : "id",index : "id",width :70,hidden:true},
            {name : "qaCode",index : "qaCode",width :130},
            {name : "batchNo",index : "batchNo",width : 150},
            {name : "dishcode",index : "dishcode",width : 120},
            {name : "factoryName",index : "factoryName",width : 180},
            {name : "workshopname",index : "workshopname",width : 90},
            {name : "ordernum",index : "ordernum",width : 130},
            {name : "orderline",index : "orderline",width : 60},
            {name : "entityNo",index : "entityNo",width : 90},
            {name : "model",index : "model",width : 140},
            {name : "meter",index : "meter",width : 60},
            {name : "unit",index : "unit",width : 60},
            {name : "colour",index : "colour",width : 60},
            {name : "materialCode",index : "materialCode",width : 120},
            {name : "materialName",index : "materialName",width : 150},
            {name : "outerDiameter",index : "outerDiameter",width :70},
            {name : "drumType",index : "drumType",width :70},
            {name : "storagetype",index : "storagetype",width : 60,
                formatter:function(cellValue,option,rowObject){
                	if(cellValue===0){
                        return "正常生产";
                    }else if(cellValue===1){
                        return "销售退货";
                    }else if(cellValue===2){
                        return "外协采购";
                    }
                }
            }
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : "#desktopWriter_list_grid-pager",
        sortname : "t1.id",
        sortorder : "asc",
        altRows: true,
        viewrecords : true,
        hidegrid:false,
        autowidth:true,
        multiselect:true,
        multiboxonly: true,
        shrinkToFit:false,
        autoScroll: true,
        loadComplete : function(data){
            var table = this;
            setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            desktopWriter_list_oriData = data;
            $("#desktopWriter_list_grid-table").closest(".ui-jqgrid-bdiv").css({ 'overflow-x': 'auto' });
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
    });

    jQuery("#desktopWriter_list_grid-table").navGrid("#desktopWriter_list_grid-pager",
        {edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#desktopWriter_list_grid-pager",{
        caption:"",
        buttonicon:"fa fa-refresh green",
        onClickButton: function(){
            $("#desktopWriter_list_grid-table").jqGrid("setGridParam",
                {
                    postData: {queryJsonString:""} //发送数据
                }
            ).trigger("reloadGrid");
        }
    }).navButtonAdd("#desktopWriter_list_grid-pager",{
        caption: "",
        buttonicon:"fa icon-cogs",
        onClickButton : function (){
            jQuery("#desktopWriter_list_grid-table").jqGrid("columnChooser",{
                done: function(perm, cols){
                    $("#desktopWriter_list_grid-table").jqGrid("setGridWidth", $("#desktopWriter_list_grid-div").width());
                }
            });
        }
    });

    $(window).on("resize.jqGrid", function () {
        $("#desktopWriter_list_grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);
        $("#desktopWriter_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-10- $("#desktopWriter_list_yy").outerHeight(true)-
        $("#desktopWriter_list_fixed_tool_div").outerHeight(true)- $("#desktopWriter_list_grid-pager").outerHeight(true)-
        $("#gview_desktopWriter_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
    });

    $(window).triggerHandler("resize.jqGrid");

    //厂区
    $("#desktopWriter_list_queryForm #desktopWriter_list_factoryCode").select2({
        placeholder: "选择厂区",
        minimumInputLength:0,   //至少输入n个字符，才去加载数据
        allowClear: true,  //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches:"没有结果",
        formatSearching:"搜索中...",
        formatAjaxError:"加载出错啦！",
        ajax : {
            url: context_path+"/factoryArea/getFactoryList",
            type:"POST",
            dataType : 'json',
            delay : 250,
            data: function (term,pageNo) {     //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term,    //联动查询的字符
                    pageSize: 15,    //一次性加载的数据条数
                    pageNo:pageNo    //页码
                }
            },
            results: function (data,pageNo) {
                var res = data.result;
                //如果没有查询到数据，将会返回空串
                if(res.length>0){
                    //用来判断是否还有更多数据可以加载
                    var more = (pageNo*15)<data.total;
                    return {
                        results:res,more:more
                    };
                }else{
                    return {
                        results:{}
                    };
                }
            },
            cache : true
        }
    });

    //rfid绑定
    function desktopWriter_list_rfidBind(){
        //选中的数量
        var checkedNum = getGridCheckedNum("#desktopWriter_list_grid-table", "id");
        var rfid = jQuery("#desktopWriter_list_grid-table").jqGrid("getGridParam", "selrow");
        if (checkedNum == 0) {
            layer.alert("请选择一条要绑定的库存！");
        }else if(checkedNum>1){
            layer.alert("只能选择一条要绑定的库存！");
        } else {
            layer.confirm("确定绑定所选库存？", function() {
                $.ajax({
                    type : "POST",
                    url : context_path + "/desktopWriter/deskRfidBind.do?rfid="+rfid ,
                    dataType : "json",
                    cache : false,
                    success : function(data) {
                        layer.closeAll();
                        if(data.result){
                            layer.msg(data.msg,{icon:1,time:1200});
                        }else{
                            layer.msg(data.msg,{icon:2,time:1200});
                        }
                        desktopWriter_list_grid.trigger("reloadGrid");
                    }
                });
            });
        }
    }

    /**
     * 查询按钮点击事件
     */
    function desktopWriter_list_queryOk(){
        var queryParam = iTsai.form.serialize($("#desktopWriter_list_queryForm"));
        //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
        desktopWriter_list_queryByParam(queryParam);
    }

    //查询
    function desktopWriter_list_queryByParam(jsonParam) {
        iTsai.form.deserialize($("#desktopWriter_list_hiddenQueryForm"), jsonParam);
        var queryParam = iTsai.form.serialize($("#desktopWriter_list_hiddenQueryForm"));
        var queryJsonString = JSON.stringify(queryParam);
        $("#desktopWriter_list_grid-table").jqGrid("setGridParam",
            {
                postData: {queryJsonString: queryJsonString}
            }
        ).trigger("reloadGrid");
    }

    //重置
    function desktopWriter_list_reset(){
        $("#desktopWriter_list_queryForm #desktopWriter_list_factoryCode").select2("val","");
        iTsai.form.deserialize($("#desktopWriter_list_queryForm"),desktopWriter_list_queryForm_data);
        desktopWriter_list_queryByParam(desktopWriter_list_queryForm_data);
    }

    $("#desktopWriter_list_queryForm .mySelect2").select2();
</script>