<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="orderDisplay_list_grid-div">
    <form id="orderDisplay_list_hiddenForm" action="<%=path%>/orderDisplay/toExcel.do" method="POST" style="display: none;">
        <input id="orderDisplay_list_ids" name="ids" value=""/>
    </form>
    <form id="orderDisplay_list_hiddenQueryForm" style="display:none;">
        <input name = "createTime" value="">
        <input name = "endTime" value="" />
        <input name = "factoryarea" value="" />
        <input name = "mcode" value="" />
        <input name = "orderno" value="" />
        <input name = "orderline" value="" />
    </form>
    <c:if test="${operationCode.webSearch==1}">
        <div class="query_box" id="orderDisplay_list_yy" title="查询选项">
            <form id="orderDisplay_list_queryForm" style="max-width:100%;">
                <ul class="form-elements">
                    <li class="field-group field-fluid3">
                        <label class="inline" for="orderDisplay_list_endTime" style="margin-right:20px;width:100%;">
                            <span class="form_label" style="width:80px;">订单号：</span>
                            <input type="text" id="orderDisplay_list_ddh"  name="orderno" value="" style="width: calc(100% - 85px);" placeholder="订单号" />
                        </label>
                    </li>
                    <li class="field-group field-fluid3">
                        <label class="inline" for="orderDisplay_list_createTime" style="margin-right:20px;width:100%;">
                            <span class="form_label" style="width:80px;">开始时间：</span>
                            <input type="text" class="form-control date-picker" id="orderDisplay_list_createTime" name="createTime" value="" style="width: calc(100% - 85px);" placeholder="开始时间" />
                        </label>
                    </li>
                    <li class="field-group field-fluid3">
                        <label class="inline" for="orderDisplay_list_endTime" style="margin-right:20px;width:100%;">
                            <span class="form_label" style="width:80px;">结束时间：</span>
                            <input type="text" class="form-control date-picker" id="orderDisplay_list_endTime" name="endTime" value="" style="width: calc(100% - 85px);" placeholder="结束时间" />
                        </label>
                    </li>

                    <li class="field-group-top field-group field-fluid3">
                        <label class="inline" for="orderDisplay_list_cq" style="margin-right:20px;width: 100%;">
                            <span class="form_label" style="width:80px;">厂区：</span>
                            <input id="orderDisplay_list_cq" name="factoryarea" type="text" style="width: calc(100% - 85px);" placeholder="厂区">
                        </label>
                     </li>
                    <li class="field-group field-fluid3">
                        <label class="inline"  style="margin-right:20px;width:100%;">
                            <span class="form_label" style="width:80px;">物料编号：</span>
                            <input type="text" id="orderDisplay_list_wlbh"   name="mcode" value="" style="width: calc(100% - 85px);" placeholder="物料编号" />
                        </label>
                    </li>
                    
                </ul>
                <div class="field-button" style="">
                    <div class="btn btn-info" onclick="orderDisplay_list_queryOk();">
                        <i class="ace-icon fa fa-check bigger-110"></i>查询
                    </div>
                    <div class="btn" onclick="orderDisplay_list_reset();"><i class="ace-icon icon-remove"></i>重置</div>
                    <a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>
                </div>
            </form>
        </div>
    </c:if>
    <div id="orderDisplay_list_fixed_tool_div" class="fixed_tool_div">
        <div id="orderDisplay_list_toolbar_" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="orderDisplay_list_grid-table" style="width:100%;height:100%;"></table>
    <div id="orderDisplay_list_grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
    var orderDisplay_list_oriData;
    var orderDisplay_list_grid;

    $("input").keypress(function (e) {
        if (e.which == 13) {
            orderDisplay_list_queryOk();
        }
    });

    $(function  (){
        $(".toggle_tools").click();
    });

    $("#orderDisplay_list_toolbar_").iToolBar({
        id: "orderDisplay_list_tb_01",
        items: [
            {label: "查看",hidden:"${operationCode.webInfo}"=="1", onclick: orderDisplay_list_openInfoPage, iconClass:'icon-search'},
        ]
    });

    var orderDisplay_list_queryForm_Date = iTsai.form.serialize($("#orderDisplay_list_queryForm"));

    orderDisplay_list_grid = jQuery("#orderDisplay_list_grid-table").jqGrid({
        url : context_path + "/orderDisplay/list.do",
        datatype : "json",
        colNames : [ "主键","订单号","订单行号","物料编码","物料名称","营销经理名称","厂区","盘数","创建时间"],
        colModel : [
            {name : "id",index : "id",hidden:true},
            {name : "orderNo",index : "orderNo",width : 60 },
            {name : "orderline",index : "orderline",width : 40 },
            {name : "materialcode",index : "materialcode",width : 60 },
            {name : "materialname",index : "materialname",width : 90 },
            {name : "salesname",index : "salesname",width : 120 },
            {name : "orgname",index : "orgname",width : 110},
            {name : "dishnum",index : "dishnum",width : 30 },
            {name : "createTime",index : "createTime",width : 80}
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : "#orderDisplay_list_grid-pager",
        sortname : "id",
        sortorder : "asc",
        altRows: true,
        viewrecords : true,
        hidegrid : false,
        autowidth : false,
        multiselect : true,
        multiboxonly : true,
        loadComplete : function(data) {
            var table = this;
            setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            orderDisplay_list_oriData = data;
            $("#orderDisplay_list_grid-table").closest(".ui-jqgrid-bdiv").css({ 'overflow-x': 'auto' });
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
    });

    jQuery("#orderDisplay_list_grid-table").navGrid("#orderDisplay_list_grid-pager",
    {edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#orderDisplay_list_grid-pager",{
        caption:"",
        buttonicon:"fa fa-refresh green",
        onClickButton: function(){
            $("#orderDisplay_list_grid-table").jqGrid("setGridParam", {
                postData: {queryJsonString:""} //发送数据
            }).trigger("reloadGrid");
        }
    }).navButtonAdd("#orderDisplay_list_grid-pager",{
        caption: "",
        buttonicon:"fa icon-cogs",
        onClickButton : function (){
            jQuery("#orderDisplay_list_grid-table").jqGrid("columnChooser",{
                done: function(perm, cols){
                    // dynamicColumns(cols,materials_list_dynamicDefalutValue);
                    $("#orderDisplay_list_grid-table").jqGrid("setGridWidth", $("#orderDisplay_list_grid-div").width());
                }
            });
        }
    });

    $(window).on("resize.jqGrid", function () {
        $("#orderDisplay_list_grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);
        $("#orderDisplay_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-10- $("#orderDisplay_list_yy").outerHeight(true)-
        $("#orderDisplay_list_fixed_tool_div").outerHeight(true)- $("#orderDisplay_list_grid-pager").outerHeight(true)-
        $("#gview_orderDisplay_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
    });
    $(window).triggerHandler("resize.jqGrid");

    /*打开详情列表页面*/
    function orderDisplay_list_openInfoPage(){
        var selectAmount = getGridCheckedNum("#orderDisplay_list_grid-table");
        if(selectAmount==0){
            layer.msg("请选择一条记录！",{icon:2});
            return;
        }else if(selectAmount>1){
            layer.msg("只能选择一条记录！",{icon:8});
            return;
        }
        layer.load(2);
        $.post(context_path+'/orderDisplay/toDetailList.do', {
            id:jQuery("#orderDisplay_list_grid-table").jqGrid("getGridParam", "selrow")
        }, function(str){
            $queryWindow = layer.open({
                title : "订单详情",
                type: 1,
                skin : "layui-layer-molv",
                area : window.screen.width-20+"px",
                shade: 0.6, //遮罩透明度
                moveType: 1, //拖拽风格，0是默认，1是传统拖动
                content: str,//注意，如果str是object，那么需要字符拼接。
                success:function(layero, index){
                    layer.closeAll("loading");
                }
            });
        }).error(function() {
            layer.closeAll();
            layer.msg("加载失败！",{icon:2});
        });
    }

    /**
     * 查询按钮点击事件
     */
    function orderDisplay_list_queryOk(){
        var queryParam = iTsai.form.serialize($("#orderDisplay_list_queryForm"));
        //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
        orderDisplay_list_queryByParam(queryParam);
    }

    //查询
    function orderDisplay_list_queryByParam(jsonParam) {
        iTsai.form.deserialize($("#orderDisplay_list_hiddenQueryForm"), jsonParam);
        var queryParam = iTsai.form.serialize($("#orderDisplay_list_hiddenQueryForm"));
        var queryJsonString = JSON.stringify(queryParam);
        $("#orderDisplay_list_grid-table").jqGrid("setGridParam",
            {
                postData: {queryJsonString: queryJsonString}
            }
        ).trigger("reloadGrid");
    }

    //重置
    function orderDisplay_list_reset(){
        $("#orderDisplay_list_queryForm #orderDisplay_list_cq").select2("val","");
        iTsai.form.deserialize($("#orderDisplay_list_queryForm"),orderDisplay_list_queryForm_Date);
        orderDisplay_list_queryByParam(orderDisplay_list_queryForm_Date);
    }

    //导出excel
    function toExcel(){
        $("#orderDisplay_list_hiddenForm #orderDisplay_list_ids").val(jQuery("#orderDisplay_list_grid-table").jqGrid("getGridParam", "selarrrow"));
        $("#orderDisplay_list_hiddenForm").submit();
    }

    $(".date-picker").datetimepicker({
        format: "YYYY-MM-DD HH:mm:ss" ,
        autoclose : true,
        todayHighlight : true
    });

    function reloadGrid(){
        orderDisplay_list_grid.trigger("reloadGrid");
    }

    $("#orderDisplay_list_queryForm #orderDisplay_list_cq").select2({
        placeholder: "选择厂区",
        minimumInputLength:0,   //至少输入n个字符，才去加载数据
        allowClear: true,  //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches:"没有结果",
        formatSearching:"搜索中...",
        formatAjaxError:"加载出错啦！",
        ajax : {
            url: context_path+"/factoryArea/getFactoryList",
            type:"POST",
            dataType : 'json',
            delay : 250,
            data: function (term,pageNo) {     //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term,    //联动查询的字符
                    pageSize: 15,    //一次性加载的数据条数
                    pageNo:pageNo    //页码
                }
            },
            results: function (data,pageNo) {
                var res = data.result;
                if(res.length>0){   //如果没有查询到数据，将会返回空串
                    var more = (pageNo*15)<data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results:res,more:more
                    };
                }else{
                    return {
                        results:{}
                    };
                }
            },
            cache : true
        }
    });
</script>