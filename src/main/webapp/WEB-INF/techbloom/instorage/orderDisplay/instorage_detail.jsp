<%@ page language="java" import="java.lang.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<div class="row-fluid" style="height: inherit;margin:0px;border: 0px">
	<input type="hidden" value="${infoId}" id="instorage_detail_infoId">
	<!-- 表格div -->
	<div id="instorage_detail_grid-div-c" style="width:100%;margin:10px auto;">
		<!-- 	表格工具栏 -->
		<div id="instorage_detail_fixed_tool_div" class="fixed_tool_div detailToolBar">
			<div id="instorage_detail_toolbar_-c" style="float:left;overflow:hidden;"></div>
		</div>

		<form id="hiddenForm" action="<%=path%>/orderDisplay/detailExcel.do" method="POST" style="display: none;">
			<input id="hiddenForm_ids" name="ids" value=""/>
			<input id="info_id" name="infoid" value="">
		</form>
		<div style="margin-bottom:5px;margin-left: 25px;">
            <span class="btn btn-info" id="export">
		       <i class="ace-icon fa fa-check bigger-110"></i>导出
            </span>
		</div>

		<!-- 物料详情信息表格 -->
		<table id="instorage_detail_grid-table-c" style="width:100%;height:100%;"></table>
		<!-- 表格分页栏 -->
		<div id="instorage_detail_grid-pager-c"></div>
	</div>
</div>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var p2p;
    var oriDataDetail;
    var _grid_detail;        //表格对象
    var lastsel2;
    var outLocationId=0;

    _grid_detail=jQuery("#instorage_detail_grid-table-c").jqGrid({
        url : context_path + "/orderDisplay/detailList.do?id="+$("#instorage_detail_infoId").val(),
        datatype : "json",
        colNames : [ "详情主键","状态","工单号","批次号","质保单号","数量","单位","重量","颜色","段号","盘号","盘具编码","盘规格","盘外径"],
        colModel : [
            {name : "id",index : "id",hidden:true},
            {name : "state",index : "state",width : 20,
            	formatter:function(cellValue,option,rowObject){
                	if(cellValue==1){
                        return "<span style='color:red;font-weight:bold;'>未装包</span>";
                    }else if(cellValue==2){
                        return "<span style='color:orange;font-weight:bold;'>已包装</span>";
                    }else if(cellValue==3){
                        return "<span style='color:blue;font-weight:bold;'>入库中</span>";
                    }else if(cellValue==4){
                        return "<span style='color:green;font-weight:bold;'>入库完成</span>";
                    }else if(cellValue==5){
						return "<span style='color:green;font-weight:bold;'>已退库</span>";
					}
                }
            },
            {name : "entityNo",index : "entityNo",width : 20},
            {name : "batchNo",index:"batch_No",width : 40},
            {name : "qaCode",index:"qaCode",width : 40},
            {name : "meter",index:"meter",width : 10},
            {name : "unit",index:"unit",width : 10},
            {name : 'weight',index : 'weight',width : 20},
            {name : 'colour',index : 'colour',width : 10},
			{name : "segmentno",index:"segmentno",width : 20},
			{name : "dishcode",index:"dishcode",width : 30},
			{name : "dishnumber",index:"dishnumber",width : 30},
            {name : "model",index:"model",width : 40},
            {name : "outerDiameter",index:"outerDiameter",width : 20}
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : "#instorage_detail_grid-pager-c",
        sortname : "t1.id",
        sortorder : "asc",
        altRows: true,
        viewrecords : true,
        caption : "详情列表",
        autowidth:true,
        multiselect:true,
        multiboxonly: true,
        loadComplete : function(data){
            var table = this;
            setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            oriDataDetail = data;
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据",
    });
    //在分页工具栏中添加按钮
    $("#instorage_detail_grid-table-c").navGrid("#instorage_detail_grid-pager-c",
    {edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#instorage_detail_grid-pager-c",{
        caption:"",
        buttonicon:"ace-icon fa fa-refresh green",
        onClickButton: function(){
            $("#instorage_detail_grid-table-c").jqGrid('setGridParam', {
				url:context_path + "/orderDisplay/detailList.do?id="+$("#instorage_detail_infoId").val()
			}).trigger("reloadGrid");
        }
    });

    function bindRfid() {
        $.post(context_path + '/orderDisplay/toBindRfid.do', {}, function (str) {
            $queryWindow = layer.open({
                title: "RFID绑定",
                type: 1,
                skin: "layui-layer-molv",
                area: "600px",
                shade: 0.6, //遮罩透明度
                moveType: 1, //拖拽风格，0是默认，1是传统拖动
                content: str,//注意，如果str是object，那么需要字符拼接。
                success: function (layero, index) {
                    layer.closeAll("loading");
                }
            });
        }).error(function () {
            layer.closeAll();
            layer.msg("加载失败！", {icon: 2});
        })
    }

    $(window).on("resize.jqGrid", function () {
        $("#instorage_detail_grid-table-c").jqGrid("setGridWidth", $("#instorage_detail_grid-div-c").width() - 3 );
        $("#instorage_detail_grid-table-c").jqGrid("setGridHeight", (document.documentElement.clientHeight - 
        $("#instorage_detail_grid-pager-c").height() - 380) );
    });
    $(window).triggerHandler("resize.jqGrid");

    $("#instorage_detail_productId").change(function () {
        p2p=$("#instorage_detail_productId").val();
    })

    $("#instorage_detail_artBomId").change(function () {
        if ($("#instorage_detail_productId").val() == "") {
            layer.alert("请先选择产品！");
            $("#instorage_detail_artBomId").select2("val", "");
        }
    })

    $("#instorage_detail_outLocationId").change(function(){
        outLocationId = $("#instorage_detail_outLocationId").val();
    });

	$("#export").click(function(){
		$("#hiddenForm_ids").val(jQuery("#instorage_detail_grid-table-c").jqGrid("getGridParam", "selarrrow"));
		$("#info_id").val($("#instorage_detail_infoId").val());

		$("#hiddenForm").submit();
	});
</script>