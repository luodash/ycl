<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<div id ="interfacedo_edit_page" class="row-fluid" style="height: inherit;">
	<form id="interfacedo_edit_queryForm" class="form-horizontal"  style="overflow: auto; height: calc(100% - 70px);">
		<input type="hidden" name="id" id="interfacedo_edit_id" value="${id}">
		<div class="control-group">
			<label class="control-label" >请求报文：</label>
			<div class="controls">
				<div class="input-append span12 required" >
					<textarea id="paramsInfo" class="span11" type="text" name="paramsinfo" rows="20" cols="200" id="interfacedo_edit_requestparams" >${requestParams}</textarea>
				</div>
			</div>
		</div>

	</form>
	<div class="form-actions" style="text-align: right;border-top: 0px;margin: 0px;">
		<span  class="savebtn btn btn-success">保存</span>
		<span  class="btn btn-danger">取消</span>
	</div>
</div>

<script type="text/javascript">
    //表单校验
    $("#interfacedo_edit_queryForm").validate({
        rules:{
            "requestparams":{
                required:true
            }
        },
        messages:{
            "requestparams":{
                remote:"报文不能为空，请重新输入！"
            }
        },
        errorClass: "help-inline",
        errorElement: "span",
        highlight:function(element, errorClass, validClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        }
    });

    //确定按钮点击事件
    $("#interfacedo_edit_page .btn-success").off("click").on("click", function(){
        //保存
        if($("#interfacedo_edit_queryForm").valid()){
            interfacedo_edit_saveUwb($("#interfacedo_edit_queryForm").serialize());
		}
    });

    //取消按钮点击事件
    $("#interfacedo_edit_page .btn-danger").off("click").on("click", function(){
        layer.close($queryWindow);
    });

    var ajaxStatus = 1;     //ajax请求状态：0不能请求，1可以请求

    //保存/修改
    function interfacedo_edit_saveUwb(bean){
        if(bean){
            if(ajaxStatus==0){
                layer.msg("保存中，请稍后...",{icon:2});
                return;
            }
            ajaxStatus = 0;
            $(".savebtn").attr("disabled","disabled");
            $.ajax({
                url:context_path+"/interfaceDo/save",
                type:"POST",
                // data:bean,
				data : {
					paramsInfo: $("#paramsInfo").val().replace(/[<]+/g, "&lt;").replace(/[>]+/g, "&gt;"),
					id : $("#interfacedo_edit_id").val()
				},
                dataType:"JSON",
                success:function(data){
                    ajaxStatus = 1; //将标记设置为可请求
                    $(".savebtn").removeAttr("disabled");
                    if(data.result){
                        layer.msg("保存成功！",{icon:1});
                        //刷新离线接口列表
						$("#interfacedo_list_grid-table").jqGrid('setGridParam', {
							postData: {
								queryJsonString : JSON.stringify(iTsai.form.serialize($('#interfacedo_list_query_form')))
							} //发送数据
						}).trigger("reloadGrid");
                        //关闭指定的窗口对象
                        layer.close($queryWindow);
                    }else{
                        layer.msg("保存失败，请稍后重试！",{icon:2});
                    }
                }
            });
        }else{
            layer.msg("出错啦！",{icon:2});
        }
    }

</script>