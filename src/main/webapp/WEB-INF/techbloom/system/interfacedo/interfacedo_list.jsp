<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
	String path = request.getContextPath();
%>
<script type="text/javascript">
	var context_path = '<%=path%>';
</script>
<!-- 表格 -->
<div class="row-fluid" id="interfacedo_list_grid-div" style="position:relative;margin-top: 0px; float: left;">
	<form id="interfacedo_list_hiddenQueryForm" style="display:none;">
		<input name="interfacecode" value=""/>
		<input name="qaCode" value="">
		<input name="batchNo" value="">
		<input name="shipNo" value="">
		<input name="success" value="">
	</form>
	<!-- 工具栏 -->
	<c:if test="${operationCode.webSearch==1}">
		<div class="query_box" id="interfacedo_list_yy" title="查询选项">
			<form id="interfacedo_list_query_form"   style="max-width:  100%;">
				<ul class="form-elements">
					<li class="field-group field-fluid3">
						<label class="inline"  style="margin-right:20px;width:100%;">
							<span class="form_label" style="width:80px;">质保号：</span>
							<input type="text" id="interfacedo_list_qaCode"  name="qaCode" value="" style="width: calc(100% - 85px);" placeholder="质保号" />
						</label>
					</li>
					<li class="field-group field-fluid3">
						<label class="inline"  style="margin-right:20px;width:100%;">
							<span class="form_label" style="width:80px;">单据号：</span>
							<input id="interfacedo_list_shipNo" name="shipNo" type="text" style="width: calc(100% - 85px);" placeholder="单据号">
						</label>
					</li>
					<li class="field-group field-fluid3">
						<label class="inline" for="interfacedo_list_success" style="margin-right:20px;width: 100%;">
							<span class="form_label" style="width:80px;">状态：</span>
							<input id="interfacedo_list_success" name="success" type="text" style="width: calc(100% - 85px);" placeholder="状态">
						</label>
					</li>
					<li class="field-group-top field-group field-fluid3">
						<label class="inline" style="margin-right:20px;width:  100%;">
							<span class='form_label' style="width:80px;">接口编码：</span>
							<input type="text" id="interfacedo_list_interfacecode" name="interfacecode" value="" style="width: calc(100% - 85px);" placeholder="编码">
						</label>
					</li>
					<li class="field-group field-fluid3">
						<label class="inline"  style="margin-right:20px;width:100%;">
							<span class="form_label" style="width:80px;">批次号：</span>
							<input type="text" id="interfacedo_list_batchNo"  name="batchNo" value="" style="width: calc(100% - 85px);" placeholder="批次号" />
						</label>
					</li>
				</ul>
				<div class="field-button" style="">
					<div class="btn btn-info" onclick="interfacedo_list_openQueryPage();">
						<i class="ace-icon fa fa-check bigger-110"></i>查询
					</div>
					<div class="btn" onclick="interfacedo_list_reset();"><i class="ace-icon icon-remove"></i>重置</div>
					<a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>
				</div>
			</form>
		</div>
	</c:if>

	<div class="row-fluid" id="interfacedo_list_table_toolbar" style="padding:5px 3px;" >
		<c:if test="${operationCode.webEdit==1}"><button class=" btn btn-primary btn-editQx" onclick="interfacedo_list_openEditPage();">
			编辑<i class="fa fa-pencil" aria-hidden="true" style="margin-left:5px;"></i>
		</button></c:if>
		<c:if test="${operationCode.webInfo==1}"><button class=" btn btn-primary btn-editQx" onclick="interfacedo_list_openInfoPage();">
			查看<i class="icon-search" aria-hidden="true" style="margin-left:5px;"></i>
		</button></c:if>
		<c:if test="${operationCode.webReupload==1}"><button class=" btn btn-primary btn-editQx" onclick="interfacedo_list_setConcal();">
			重新上传<i class="icon-ok" aria-hidden="true" style="margin-left:5px;"></i>
		</button></c:if>
		<c:if test="${operationCode.webCancelupload==1}"><button class=" btn btn-primary btn-editQx" onclick="interfacedo_list_setSuccess();">
			取消上传<i class="icon-off" aria-hidden="true" style="margin-left:5px;"></i>
		</button></c:if>
	</div>
	<!-- 表格 -->
	<div class="row-fluid" style="padding:0 3px;">
		<!-- 表格数据 -->
		<table id="interfacedo_list_grid-table" style="width:100%;"></table>
		<!-- 表格底部 -->
		<div id="interfacedo_list_grid-pager"></div>
	</div>
</div>

<script src="<%=request.getContextPath()%>/static/techbloom/system/uwb/interfacedo_list.js"></script>
