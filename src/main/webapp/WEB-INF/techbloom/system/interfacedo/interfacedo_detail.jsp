<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div id ="interfacedo_info_page" class="row-fluid" style="height: inherit;">
	<form id="interfacedo_info_queryForm" class="form-horizontal"  style="overflow: auto; height: calc(100% - 70px);">
		<div class="control-group">
			<label class="control-label" >接口编号：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input class="span11" type = "text" value="${INTERFACECODE}"/>
				</div>
			</div>

			<label class="control-label" >质保号：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input class="span11" type = "text" value="${QACODES}"/>
				</div>
			</div>

			<label class="control-label" >批次号：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input class="span11" type = "text" value="${BATCH_NOES}"/>
				</div>
			</div>

			<label class="control-label" >执行时间：</label>
			<div class="controls">
				<div class="input-append span12 required" >
					<input class="span11" type = "text" value="${OPERATETIME}"/>
				</div>
			</div>

			<label class="control-label" >是否成功：</label>
			<div class="controls">
				<div class="input-append span12 required" >
					<input class="span11" type = "text" value="${ISSUCCESS}"/>
				</div>
			</div>

			<label class="control-label" >首次失败原因：</label>
			<div class="controls">
				<div class="input-append span12 required" >
					<textarea class="span11" type="text" rows="5" cols="100" >${FLASECONTENT}</textarea>
				</div>
			</div>

			<label class="control-label" >请求报文：</label>
			<div class="controls">
				<div class="input-append span12 required" >
					<textarea class="span11" type="text" rows="5" cols="100" >${PARAMSINFO}</textarea>
				</div>
			</div>

			<label class="control-label" >最新返回报文：</label>
			<div class="controls">
				<div class="input-append span12 required" >
					<textarea class="span11" type="text" rows="5" cols="100" >${RETURNCODE}</textarea>
				</div>
			</div>

		</div>

	</form>
</div>

<script type="text/javascript">

	//表单校验
	$("#interfacedo_info_queryForm").validate({
		rules:{
			"requestparams":{
				required:true
			}
		},
		messages:{
			"requestparams":{
				remote:"报文不能为空，请重新输入！"
			}
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
		}
	});

</script>