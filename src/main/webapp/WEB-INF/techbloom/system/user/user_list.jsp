<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
	String path = request.getContextPath();
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<!-- 表格 -->
<div class="row-fluid" id="user_list_grid-div" style="position:relative;margin-top: 0px; float: left;">
	<form id="user_list_hiddenForm" action="<%=path%>/user/toExcel" method="POST" style="display: none;">
		<!-- 选中的用户 -->
		<input id="user_list_ids" name="ids" value=""/>
	</form>
	<!-- 工具栏 -->
	<c:if test="${operationCode.webSearch==1}">
	<div class="query_box" id="user_list_yy" title="查询选项">
         <form id="user_list_query_form" style="max-width:100%;">
		 <ul class="form-elements">
			<!-- 货架编号 -->
			<li class="field-group field-fluid3">
				<label class="inline" for="user_list_username" style="margin-right:20px;width:  100%;">
					<span class='form_label'>用户名：</span>
					<input type="text" name="username" id="user_list_username" value="" style="width: calc(100% - 75px);" placeholder="用户名">
				</label>
				
			</li>
			<!-- 货架名称 -->
			<li class="field-group field-fluid3">
				<label class="inline" for="user_list_name" style="margin-right:20px;width:  100%;">
					<span class='form_label'>姓名：</span>
					<input type="text" name="name" id="user_list_name" value="" style="width: calc(100% - 75px);" placeholder="姓名">
				</label>
			</li>
			<li class="field-group field-fluid3">
				<label class="inline" for="user_list_email" style="margin-right:20px;width:  100%;">
					<span class='form_label'>邮箱：</span>
					<input type="text" name="email" id="user_list_email" value="" style="width: calc(100% - 75px);" placeholder="邮箱">
				</label>
			</li>
			<!-- 第二行 3个li标签 ，第一个li标签的class要加 field-group-top  ，  -->
			<li class=" field-group-top field-group field-fluid3">
				<label class="inline" for="user_list_phone" style="margin-right:20px;width:  100%;">
					<span class='form_label'>手机号：</span>
					<input type="text" name="phone" id="user_list_phone" value="" style="width: calc(100% - 75px);" placeholder="手机号">
				</label>
			</li>
		</ul>
			<div class="field-button" style="">
				<div class="btn btn-info" onclick="user_list_openQueryPage();">
		            <i class="ace-icon fa fa-check bigger-110"></i>查询
	            </div>
				<div class="btn" onclick="user_list_reset();"><i class="ace-icon icon-remove"></i>重置</div>
				<a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>
	        </div>
	  </form>
    </div>
    </c:if>
	<div class="row-fluid" id="user_list_table_toolbar" style="padding:5px 3px;" >
		<c:if test="${operationCode.webAdd==1}"><button class=" btn btn-primary btn-addQx" onclick="user_list_openAddPage();">
			添加<i class="fa fa-plus" aria-hidden="true" style="margin-left:5px;"></i>
		</button></c:if>
		<c:if test="${operationCode.webEdit==1}"><button class=" btn btn-primary btn-editQx" onclick="user_list_openEditPage();">
			编辑<i class="fa fa-pencil" aria-hidden="true" style="margin-left:5px;"></i>
		</button></c:if>
		<c:if test="${operationCode.webDel==1}"><button class=" btn btn-primary btn-deleteQx" onclick="user_list_deleteUser();">
			删除<i class="fa fa-trash" aria-hidden="true" style="margin-left:5px;"></i>
		</button></c:if>
		<c:if test="${operationCode.webExport==1}"><button class="col-md-1 btn btn-primary btn-queryQx" onclick="user_list_exportLogFile();">
			导出<i class="fa fa-share" aria-hidden="true" style="margin-left:5px;"></i>
		</button></c:if>
	</div>
	<!-- 表格 -->
	<div class="row-fluid" style="padding:0 3px;">
		<!-- 表格数据 -->
		<table id="user_list_grid-table" style="width:100%;"></table>
		<!-- 表格底部 -->
		<div id="user_list_grid-pager"></div>
	</div>
</div>

<script src="<%=request.getContextPath()%>/static/techbloom/system/user/user.js"></script>
