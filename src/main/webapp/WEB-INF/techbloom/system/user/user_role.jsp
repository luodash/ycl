<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div  id ="user_role_user_role_page" class="row-fluid" style="height:inherit;">
	<form id="user_role_roleForm" class="form-horizontal" onkeydown="if(event.keyCode===13)return false;" style=" height: calc(100% - 70px);">
		<input type="hidden" name="userId" id="user_userid" value="" >
		<input type="hidden" id="user_role_type01" value="1">
		<!-- 角色基础信息 -->
		<div style="padding-top: 20px;">
			<ul class="nav nav-tabs">
				<li role="presentation" class="active" id="user_role_tab01"><a href="javascript:;">权限范围</a></li>
			</ul>
		</div>
		<!-- 菜单树 -->
		<div  class="row" style="margin:0;padding:0;height:250px;">
			<div  class="row" style="margin:0;padding:0; overflow: auto; height:210px;">
				<ul id="user_role_menuTree" class="ztree" ></ul>
			</div>
		</div>
	</form>
	<!-- 底部按钮 -->
	<div class="form-actions" style="text-align: right;border-top: 0px;margin: 0px;">
		<span  class="btn btn-success">保存</span>
		<span  class="btn btn-danger">取消</span>
	</div>
</div>
<script type="text/javascript">
	//树的相关设置
	var setting = {
		check: {
			enable: true
		},
		data: {
			simpleData: {
				enable: true
			}
		},
		edit: {
			enable: false,
			drag:{
				isCopy:false,
				isMove:false
			}
		},
		callback: {
			onAsyncSuccess: zTreeOnAsyncSuccess
		},
		async: {
			enable: true,
			url:context_path+"/user/getUserRoleByUserId",
			autoParam:["id"],
			otherParam: {"userId":user_list_pdd,"type":usertype},
			type: "POST"
		},//异步加载数据
	};

	(function(){
		$("#user_role_tab01").unbind("click").on("click",function(){
			$("#user_role_tab01").addClass("active");
			$("#user_role_type01").val(1);
			setTree();
		});
		$("#user_role_roleForm  input[type=checkbox]").uniform();

		function setTree(){
			$.fn.zTree.init($("#user_role_menuTree"), setting);
			zTree = $.fn.zTree.getZTreeObj("user_role_menuTree");
		}

		var ajaxStatus = 1;     //ajax请求状态：0不能请求，1可以请求
		//确定按钮点击事件
		$("#user_role_user_role_page .btn-success").off("click").on("click", function(){
			//角色保存
			if($('#user_role_roleForm').valid()){
				//获取选中的菜单
				var nodes = zTree.getCheckedNodes(true);
				var menuids = "";
				for(var rolei=0,count = nodes.length;rolei<count; rolei++){
					var menuNode = nodes[rolei];
					if(menuNode.id!==""&&menuNode.id!=="M0"){
						if(menuids.length>0){
							menuids += ","+menuNode.id;
						}else{
							menuids += menuNode.id;
						}
					}
				}

				var selectid = getGridCheckedId("#role_list_grid-table","roleId");
				if(user_add_openwindowtype===1){
					//更新
					$("#role_edit_roleId").val(selectid);
				}

				var roleParam = $("#user_role_roleForm").serialize();
				if(ajaxStatus===0){
					layer.msg("操作进行中，请稍后...",{icon:2});
					return;
				}
				ajaxStatus = 0;    //将标记设置为不可请求

				//保存或修改用户信息
				if(user_add_openwindowtype===1?$("#user_edit_queryForm").serialize():$("#user_add_queryForm").serialize()){
					$(".savebtn").attr("disabled","disabled");
					$.ajax({
						url:context_path+"/user/save?userFactoryWarehouseIds="+menuids,
						type:"POST",
						data:user_add_openwindowtype===1?$("#user_edit_queryForm").serialize():$("#user_add_queryForm").serialize(),
						dataType:"JSON",
						success:function(data){
							ajaxStatus = 1; //将标记设置为可请求
							$(".savebtn").removeAttr("disabled");
							if(data.result){
								//刷新用户列表
								$("#user_list_grid-table").jqGrid('setGridParam', {
									postData: {queryJsonString:""} //发送数据
								}).trigger("reloadGrid");
								//关闭所有窗口
								layer.closeAll();
								layer.msg("保存成功！",{icon:1});
								/*//关闭指定的窗口对象
								layer.close($queryWindow);*/
							}else{
								layer.msg("保存失败，请稍后重试！",{icon:2});
							}
						}
					});
				}else{
					layer.msg("出错啦！",{icon:2});
				}
			}
			return false;
		});

		//取消按钮点击事件
		$("#user_role_user_role_page .btn-danger").off("click").on("click", function(){
			layer.close($queryWindow2);
			return false;
		});

		$("#user_role_tab01").click();
	}());

</script>
</html>
