<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div id ="user_add_page" class="row-fluid" style="height: inherit;">
	<form id="user_add_queryForm" class="form-horizontal"  style="overflow: auto; height: calc(100% - 70px);">
		<input type="hidden" name="userId" id="user_add_userId" value="">

		<div class="control-group">
			<label class="control-label" for="user_add_username">用户名：</label>
			<div class="controls">
			<div class="input-append span12 required" >
			      <input class="span11" type="text"  name="username" id="user_add_username" placeholder="用户名">
			    </div>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label" for="user_add_password">密码：</label>
			<div class="controls">
				<div class="input-append span12 required">
			      <input class="span11" type="password" name="password" id="user_add_password"placeholder="密码">

			    </div>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label" for="user_add_pwd_confirm">确认密码：</label>
			<div class="controls">
				<div class="input-append span12 required">
			      <input class="span11" type="password" name="pwd_confirm" id="user_add_pwd_confirm"placeholder="确认密码">
			    </div>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label">权限类别：</label>
			<div class="controls">
				<label><input type="radio" id="user_add_usertype1" name="usertype" value="1">成品</label>
				<label><input type="radio" id="user_add_usertype2" name="usertype" value="2">原材料</label>
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label" for="user_add_name">真实姓名：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input class="span11" type="text" name="name" id="user_add_name" value="" placeholder="真实姓名" />
				</div>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label">性别：</label>
			<div class="controls">
				<label><input type="radio" id="user_add_gender1" name="gender" value="1">男</label>
				<label><input type="radio" id="user_add_gender2" name="gender" value="0">女</label>
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label" for="user_add_roleId">所属角色：</label>
			<div class="controls">
				<input class="span11 select2_input" type="text" name="roleId" id="user_add_roleId" />
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label" for="user_add_phone">手机号码：</label>
			<div class="controls">
				<input class="span11" type="text" name="phone" id="user_add_phone" value="" placeholder="手机号码" />
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label" for="user_add_email">Email：</label>
			<div class="controls">
				<input class="span11" type="text" name="email" id="user_add_email" value="" placeholder="Email" />
			</div>
		</div>

		<div class="control-group">
			<label class="control-label" for="user_add_remark">备注：</label>
			<div class="controls">
				<textarea class="span11" name="remark" id="user_add_remark" rows="3" cols="20"></textarea>
			</div>
		</div>

		<%--<div class="control-group ">
			<label class="control-label" >权限范围：</label>
			<div class="controls required">
				<c:forEach items="${factlist}" var="p">
					<input type = "checkbox" name="factoryCode" value="${p.CODE}"
						   <c:forEach items="${factRelList}" varStatus="va"><c:if test="${factRelList[va.index]==p.CODE}">checked</c:if></c:forEach> />${p.NAME}
				</c:forEach>
			</div>
			<div class="controls">
				<div class=" btn btn-info" title="点击添加权限详细" id="user_add_initpwdbtn" >
					<i class="fa fa-eyedropper" aria-hidden="true"></i>&emsp;选择所属厂区和仓库
				</div>
			</div>
		</div>--%>

	</form>
	<div class="form-actions" style="text-align: right;border-top: 0px;margin: 0px;">
	 <span  class="btn btn-success" id="user_add_initpwdbtn">提交</span>
     <span  class="btn btn-danger">关闭</span>
	</div>
</div>
<script type="text/javascript">
	var oriData;
	var _grid;
	var $queryWindow;  //查询窗口对象
	var user_add_openwindowtype = 0; //打开窗口类型：0新增，1修改
	var zTree;   //树形对象
	var selectTreeId = 0;   //存放选中的树节点id
	var usertype;


	//ztree加载成功之后的回调函数
	function zTreeOnAsyncSuccess(event, treeId, treeNode, msg) {
		zTree.expandAll(true);
	}

	$(window).triggerHandler('resize.jqGrid');

(function(){
   	$("#user_add_queryForm input[type=radio]").uniform();
   	$("input[type=radio][name=gender][value='1']").attr("checked",true).trigger("click");
	$("input[type=radio][name=usertype][value='2']").attr("checked",true).trigger("click");

    //所属角色
    $("#user_add_roleId").select2({
        placeholder: "选择角色",
        minimumInputLength:0,   //至少输入n个字符，才去加载数据
        allowClear: true,  //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches:"没有结果",
        formatSearching:"搜索中...",
        formatAjaxError:"加载出错啦！",
        ajax : {
            url: context_path+"/role/getRoleList",
            type:"POST",
            dataType : 'json',
            delay : 250,
            data: function (term,pageNo) {     //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    roleName: term,    //联动查询的字符
                    pageSize: 15,    //一次性加载的数据条数
                    pageNo:pageNo,    //页码
                    time:new Date(),   //测试
					generalType: $('input[name="usertype"]:checked').val()
            }
            },
            results: function (data,pageNo) {
                var res = data.result;
                if(res.length>0){   //如果没有查询到数据，将会返回空串
                    var more = (pageNo*15)<data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results:res,more:more
                    };
                }else{
                    return {
                        results:{}
                    };
                }
            },
            cache : true
        }
    });

	jQuery.validator.addMethod("isMobile", function(value, element) {
		var length = value.length;
		var mobile = /^(13[0-9]{9})|(18[0-9]{9})|(14[0-9]{9})|(17[0-9]{9})|(15[0-9]{9})$/;
		return this.optional(element) || (length == 11 /*&& mobile.test(value)*/);
	}, "请正确填写您的手机号码");

    $("#user_add_roleId").on("change.select2",function(){
        $("#user_add_roleId").trigger("keyup")}
    );

	//表单校验
	$("#user_add_queryForm").validate({
  		rules:{
  			"username" : {
  				required:true,
  				maxlength:32,
  				remote:context_path+"/user/hasU.do?userId="+$('#user_add_userId').val()
  			},
  			"password":{
  				required:true,
  				maxlength:32
  			},
  			"pwd_confirm":{
  				required:true,
  				equalTo:"#user_add_password"
  			},
  			"name":{
  				required:true,
  				invalidChar:"'\"\\\\",
  				maxlength:32
  			},
  			"phone":{
				isMobile: true
    		},
  			"email":{
  				email : true,
  				maxlength:256
  			},
  			"roleId":{
  				required:true
  			},
  			"remark":{
  				maxlength:256
  			},
            "factoryCode":{
                required:true
            }
        },
  		messages:{
  			"username" : {
  				required:"请输入用户名",
  				remote:"您输入的用户名已经存在，请重新输入！"
  			},
  			"password":{
  				required:"请输入密码"
  			},
  			"pwd_confirm":{
  				required:"请输入确认密码",
  				equalTo:"与第一次密码不一致"
  			},
  			"name":{
  				required:"请输入姓名"
  			},
  			"roleId":{
  				required:"请选择角色"
  			},
            "factoryCode":{
                required:"请选择厂区"
            }
        },
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
		}
	});
	
	//确定按钮点击事件
    /*$("#user_add_page .btn-success").off("click").on("click", function(){
		//用户保存
	  if($("#user_add_queryForm").valid()){
	  	user_add_saveUserInfo($("#user_add_queryForm").serialize());
	  }
	});*/

	//取消按钮点击事件
	$("#user_add_page .btn-danger").off("click").on("click", function(){
	    layer.close($queryWindow);
	});


	//清空执行人多选框中的值
    function removeChoice2(){
        $("#s2id_userId .select2-choices").children(".select2-search-choice").remove();
        selectData = 0;
    }

	var ajaxStatus = 1;     //ajax请求状态：0不能请求，1可以请求

	//保存/修改用户信息
  	function user_add_saveUserInfo(bean){
  		if(bean){
  			if(ajaxStatus===0){
  				layer.msg("操作进行中，请稍后...",{icon:2});
  				return;
  			}
  			ajaxStatus = 0;    //将标记设置为不可请求
  			$(".savebtn").attr("disabled","disabled");
  			$.ajax({
  				url:context_path+"/user/save?tm="+new Date(),
  				type:"POST",
  				data:bean,
  				dataType:"JSON",
  				success:function(data){
  					ajaxStatus = 1; //将标记设置为可请求
  					$(".savebtn").removeAttr("disabled");
  					if(data.result){
  						layer.msg("保存成功！",{icon:1});
  						//刷新用户列表
  						$("#user_list_grid-table").jqGrid('setGridParam', 
							{
								postData: {queryJsonString:""} //发送数据 
							}
						).trigger("reloadGrid");
  						//关闭当前窗口
  						//关闭指定的窗口对象
  						layer.close($queryWindow);
  					}else{
  						layer.msg("保存失败，请稍后重试！",{icon:2});
  					}
  				}
  			});
  		}else{
  			layer.msg("出错啦！",{icon:2});
  		}
  	}


	//确定按钮点击事件
	$("#user_add_page #user_add_initpwdbtn").off("click").on("click", function(){
		if($("#user_add_queryForm").valid()){
			usertype = $('input:radio[name="usertype"]:checked').val();
			user_list_pdd = '';
			$.post(context_path+'/user/roleEdit', {}, function(str){
				$queryWindow2 = layer.open({
					title : "从属添加",
					type: 1,
					skin : "layui-layer-molv",
					area : "600px",
					shade: 0.6, //遮罩透明度
					moveType: 1, //拖拽风格，0是默认，1是传统拖动
					content: str,//注意，如果str是object，那么需要字符拼接。
					success:function(layero, index){
						layer.closeAll('loading');
					}
				});
			});
		}
	});

}())

</script>