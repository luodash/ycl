<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
	String path = request.getContextPath();
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<!-- 表格 -->
<div class="row-fluid" id="interfacelog_list_grid-div" style="position:relative;margin-top: 0px; float: left;">
    <form id="interfacelog_list_hiddenQueryForm" style="display:none;">
        <input name="interfacecode" value=""/>
        <input name="qaCode" value="">
    </form>
	<!-- 工具栏 -->
	<c:if test="${operationCode.webSearch==1}">
	<div class="query_box" id="interfacelog_list_yy" title="查询选项">
         <form id="interfacelog_list_query_form"   style="max-width:  100%;">
		 <ul class="form-elements">
			 <li class="field-group field-fluid3">
				 <label class="inline"  style="margin-right:20px;width:100%;">
					 <span class="form_label" style="width:80px;">质保号：</span>
					 <input type="text" id="interfacelog_list_qaCode"  name="qaCode" value="" style="width: calc(100% - 85px);" placeholder="质保号" />
				 </label>
			 </li>
			 <li class=" field-group field-fluid3">
				 <label class="inline" style="margin-right:20px;width:  100%;">
					 <span class='form_label' style="width:80px;">接口编码：</span>
					 <input type="text" id="interfacelog_list_interfacecode" name="interfacecode" value="" style="width: calc(100% - 85px);" placeholder="编码">
				 </label>
			 </li>
		</ul>
			<div class="field-button" style="">
				<div class="btn btn-info" onclick="interfacelog_list_openQueryPage();">
		            <i class="ace-icon fa fa-check bigger-110"></i>查询
	            </div>
				<div class="btn" onclick="interfacelog_list_reset();"><i class="ace-icon icon-remove"></i>重置</div>
				<%--<a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>--%>
	        </div>
	  </form>
    </div>
	</c:if>

	<div class="row-fluid" id="interfacelog_list_table_toolbar" style="padding:5px 3px;" >
		<c:if test="${operationCode.webInfo==1}"><button class=" btn btn-primary btn-editQx" onclick="interfacelog_list_openInfoPage();">
			查看<i class="icon-search" aria-hidden="true" style="margin-left:5px;"></i>
		</button></c:if>
	</div>
	<!-- 表格 -->
	<div class="row-fluid" style="padding:0 3px;">
		<!-- 表格数据 -->
		<table id="interfacelog_list_grid-table" style="width:100%;"></table>
		<!-- 表格底部 -->
		<div id="interfacelog_list_grid-pager"></div>
	</div>
</div>

<script type="text/javascript">
	var interfacelog_list_grid;
	var $queryWindow_interfacelog_list;  //查询窗口对象
	var interfacelog_list_dynamicDefalutValue="60cce761390a4e51bc4636271b52ab94";
	var interfacelog_list_queryFormData = iTsai.form.serialize($("#interfacelog_list_hiddenQueryForm"));

	$(function  (){
		$(".toggle_tools").click();
	});

	$("input").keypress(function (e) {
		if (e.which === 13) {
			interfacelog_list_openQueryPage();
		}
	});

	interfacelog_list_grid = jQuery("#interfacelog_list_grid-table").jqGrid({
		url : context_path + '/interfaceLog/list.do',
		datatype : "json",
		colNames : [ 'ID','接口编码', '接口名称','创建时间','请求报文','返回报文','质保号','批次号' ],
		colModel : [
			{name : 'id',index : 'id',hidden : true},
			{name : 'interfaceCode',index : 'interfacecode',width : 100},
			{name : 'interfaceName',index : 'interfacename',width : 180},
			{name : 'createTime',index : 'createtime',width : 170},
			{name : 'requestMessage',index : 'id',width : 280},
			{name : 'responseMessage',index : 'id',width : 280},
			{name : 'qacode',index : 'qacode',width : 130},
			{name : 'batchNo',index : 'batch_no',width : 130}

		],
		rowNum : 20,
		rowList : [ 10, 20, 30 ],
		pager : '#interfacelog_list_grid-pager',
		sortname : 'id',
		sortorder : "desc",
		altRows: true,
		viewrecords : true,
		hidegrid:false,
		autowidth:false,
		multiselect:true,
		multiboxonly: true,
		shrinkToFit:false,
		autoScroll: true,
		beforeRequest:function (){
			dynamicGetColumns(interfacelog_list_dynamicDefalutValue,"interfacelog_list_grid-table",$(window).width()-$("#sidebar").width() -7);
			//重新加载列属性
		},
		loadComplete : function(data){
			var table = this;
			<%--if ("${operationCode.generalType}"!=="1") {--%>
			<%--	$("#interfacelog_list_grid-table").hideCol("qaCode");--%>
			<%--	$("#interfacelog_list_grid-table").hideCol("batchNo");--%>
			<%--}--%>
			setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
			$("#interfacelog_list_grid-table").closest(".ui-jqgrid-bdiv").css({ 'overflow-x': 'auto' });
		},
		emptyrecords: "没有相关记录",
		loadtext: "加载中...",
		pgtext : "页码 {0} / {1}页",
		recordtext: "显示 {0} - {1}共{2}条数据"
	});

	jQuery("#interfacelog_list_grid-table").navGrid('#interfacelog_list_grid-pager',
	{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd('#interfacelog_list_grid-pager',{
		caption:"",
		buttonicon:"fa fa-refresh green",
		onClickButton: function(){
			$("#interfacelog_list_grid-table").jqGrid('setGridParam', {
				postData: {queryJsonString:""} //发送数据 
			}).trigger("reloadGrid");
		}
	}).navButtonAdd('#interfacelog_list_grid-pager',{
		caption: "",
		buttonicon:"fa  icon-cogs",
		onClickButton : function (){
			jQuery("#interfacelog_list_grid-table").jqGrid('columnChooser',{
				done: function(perm, cols){
					dynamicColumns(cols,interfacelog_list_dynamicDefalutValue);
					$("#interfacelog_list_grid-table").jqGrid( 'setGridWidth', $(window).width()-$("#sidebar").width() -7);
				}
			});
		}
	});

	$(window).on("resize.jqGrid", function () {
		$("#interfacelog_list_grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);
		$("#interfacelog_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-
		$("#interfacelog_list_table_toolbar").outerHeight(true)- $("#interfacelog_list_yy").outerHeight(true)-
		$("#interfacelog_list_fixed_tool_div").outerHeight(true)- $("#interfacelog_list_grid-pager").outerHeight(true)-
		$("#gview_interfacelog_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
	});
	$(window).triggerHandler('resize.jqGrid');

	//重新加载表格
	function gridReload(){
		interfacelog_list_grid.trigger("reloadGrid");  //重新加载表格
	}

	/**
	 * 获取查询页面中的值，并将值放入列表页面中隐藏的form
	 * @param jsonParam	查询页面传递过来的json对象
	 */
	function interfacelog_list_queryLogListByParam(jsonParam){
		var queryJsonString = JSON.stringify(jsonParam);         //将json对象转换成json字符串
		//执行查询操作
		$("#interfacelog_list_grid-table").jqGrid('setGridParam', {
			postData: {queryJsonString:queryJsonString} //发送数据 
		}).trigger("reloadGrid");
	}

	/**打开详情列表页面*/
	function interfacelog_list_openInfoPage(){
		var selectAmount = getGridCheckedNum("#interfacelog_list_grid-table");
		if(selectAmount===0){
			layer.msg("请选择一条记录！",{icon:2});
			return;
		}else if(selectAmount>1){
			layer.msg("只能选择一条记录！",{icon:8});
			return;
		}
		layer.load(2);
		$.post(context_path+'/interfaceLog/toDetailList.do', {
			id:jQuery("#interfacelog_list_grid-table").jqGrid("getGridParam", "selrow")
		}, function(str){
            $queryWindow_interfacelog_list = layer.open({
				title : "接口日志详情",
				type: 1,
				skin : "layui-layer-molv",
				area : [window.screen.width-150+"px",document.body.clientHeight-150+"px"],
				shade: 0.6, //遮罩透明度
				moveType: 1, //拖拽风格，0是默认，1是传统拖动
				content: str,//注意，如果str是object，那么需要字符拼接。
				success:function(layero, index){
					layer.closeAll("loading");
				}
			});
		}).error(function() {
			layer.closeAll();
			layer.msg("加载失败！",{icon:2});
		});
	}

	/**
	 * 查询按钮点击事件
	 */
	function interfacelog_list_openQueryPage(){
		var queryParam = iTsai.form.serialize($('#interfacelog_list_query_form'));
		interfacelog_list_queryLogListByParam(queryParam);
	}

	/**重置*/
	function interfacelog_list_reset(){
		iTsai.form.deserialize($("#interfacelog_list_query_form"),interfacelog_list_queryFormData);
		$("#interfacelog_list_query_form #interfacelog_list_success").select2("val","");
		interfacelog_list_openQueryPage();
	}

</script>
