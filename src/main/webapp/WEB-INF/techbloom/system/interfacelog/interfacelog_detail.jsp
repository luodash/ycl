<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div id ="interfacelog_info_page" class="row-fluid" style="height: inherit;">
	<form id="interfacelog_info_queryForm" class="form-horizontal"  style="overflow: auto; height: calc(100% - 70px);">
		<div class="control-group">
			<label class="control-label" >接口编号：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input class="span11" type = "text" value="${INTERFACECODE}"/>
				</div>
			</div>

			<label class="control-label" >质保号：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input class="span11" type = "text" value="${QACODES}"/>
				</div>
			</div>

			<label class="control-label" >创建时间：</label>
			<div class="controls">
				<div class="input-append span12 required" >
					<input class="span11" type = "text" value="${CREATETIME}"/>
				</div>
			</div>

			<label class="control-label" >请求报文：</label>
			<div class="controls">
				<div class="input-append span12 required" >
					<textarea class="span11" type="text" rows="5" cols="100" >${PARAMSINFO}</textarea>
				</div>
			</div>

			<label class="control-label" >返回报文：</label>
			<div class="controls">
				<div class="input-append span12 required" >
					<textarea class="span11" type="text" rows="5" cols="100" >${RETURNCODE}</textarea>
				</div>
			</div>

		</div>
	</form>
</div>

<script type="text/javascript">

</script>