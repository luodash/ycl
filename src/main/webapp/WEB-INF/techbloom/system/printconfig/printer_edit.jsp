<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<div id ="printer_edit_page" class="row-fluid" style="height: inherit;">
	<form id="printer_edit_queryForm" class="form-horizontal"  style="overflow: auto; height: calc(100% - 70px);">
		<input type="hidden" name="id" id="printer_edit_id" value="${id}">
		<div class="control-group">
			<label class="control-label" for="printer_edit_code">代码：</label>
			<div class="controls required">
				<input class="span11" type = "text" id="printer_edit_code" name="code" value="${printer.code}" placeholder="代码"/>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label" for="printer_edit_printer">打印机：</label>
			<div class="controls required">
				<input class="span11 select2_input" type="text" name="printer" id="printer_edit_printer" value="${printer.printer}" placeholder="打印机"/>
				<input type="hidden" id="printer_edit_printerid" value="${printer.printer}">
				<input type="hidden" id="printer_edit_printername" value="${printer.printerStr}">
			</div>
		</div>

		<div class="control-group">
			<label class="control-label" for="printer_edit_hierarchy">层级：</label>
			<div class="controls required">
				<input class="span11 select2_input" type="text" name="hierarchy" id="printer_edit_hierarchy" value="${printer.hierarchy}" placeholder="层级"/>
				<input type="hidden" id="printer_edit_hierarchyid" value="${printer.hierarchy}">
				<input type="hidden" id="printer_edit_hierarchyname" value="${printer.hierarchyStr}">
			</div>
		</div>

		<div class="control-group">
			<label class="control-label" for="printer_edit_type">单据类型：</label>
			<div class="controls required">
				<input class="span11 select2_input" type="text" name="type" id="printer_edit_type" value="${printer.type}" placeholder="单据类型"/>
				<input type="hidden" id="printer_edit_typeid" value="${printer.type}">
				<input type="hidden" id="printer_edit_typename" value="${printer.typeStr}">
			</div>
		</div>
	</form>
	<div class="form-actions" style="text-align: right;border-top: 0px;margin: 0px;">
		<span  class="savebtn btn btn-success">保存</span>
		<span  class="btn btn-danger">取消</span>
	</div>
</div>

<script type="text/javascript">
    //表单校验
    $("#printer_edit_queryForm").validate({
        rules:{
            "requestparams":{
                required:true
            }
        },
        messages:{
            "requestparams":{
                remote:"报文不能为空，请重新输入！"
            }
        },
        errorClass: "help-inline",
        errorElement: "span",
        highlight:function(element, errorClass, validClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        }
    });

    //确定按钮点击事件
    $("#printer_edit_page .btn-success").off("click").on("click", function(){
        //保存
        if($("#printer_edit_queryForm").valid()){
            printer_edit_saveUwb($("#printer_edit_queryForm").serialize());
		}
    });

    //取消按钮点击事件
    $("#printer_edit_page .btn-danger").off("click").on("click", function(){
        layer.close($queryWindow);
    });

    var ajaxStatus = 1;     //ajax请求状态：0不能请求，1可以请求

    //保存/修改
    function printer_edit_saveUwb(bean){
        if(bean){
            if(ajaxStatus==0){
                layer.msg("保存中，请稍后...",{icon:2});
                return;
            }
            ajaxStatus = 0;
            $(".savebtn").attr("disabled","disabled");
            $.ajax({
                url:context_path+"/printer/save",
                type:"POST",
                data:bean,
                dataType:"JSON",
                success:function(data){
                    ajaxStatus = 1; //将标记设置为可请求
                    $(".savebtn").removeAttr("disabled");
                    if(data.result){
                        layer.msg("保存成功！",{icon:1});
                        //刷新列表
						printer_list_openQueryPage();
                        //关闭指定的窗口对象
                        layer.close($queryWindow);
                    }else{
                        layer.msg("保存失败，请稍后重试！",{icon:2});
                    }
                }
            });
        }else{
            layer.msg("出错啦！",{icon:2});
        }
    }

	//层级
	$("#printer_edit_queryForm #printer_edit_hierarchy").select2({
		placeholder: "选择层级",
		minimumInputLength:0,   //至少输入n个字符，才去加载数据
		allowClear: true,  //是否允许用户清除文本信息
		delay: 250,
		formatNoMatches:"没有结果",
		formatSearching:"搜索中...",
		formatAjaxError:"加载出错啦！",
		ajax : {
			url: context_path+"/BaseDicType/getKindList",
			type:"POST",
			dataType : 'json',
			delay : 250,
			data: function (term,pageNo) {     //在查询时向服务器端传输的数据
				term = $.trim(term);
				return {
					queryString: term,    //联动查询的字符
					pageSize: 15,    //一次性加载的数据条数
					pageNo:pageNo,    //页码
					description:'HIERARCHY'
				}
			},
			results: function (data,pageNo) {
				var res = data.result;
				if(res.length>0){   //如果没有查询到数据，将会返回空串
					var more = (pageNo*15)<data.total; //用来判断是否还有更多数据可以加载
					return {
						results:res,more:more
					};
				}else{
					return {
						results:{}
					};
				}
			},
			cache : true
		}
	});

	//打印单据类型
	$("#printer_edit_queryForm #printer_edit_type").select2({
		placeholder: "选择层级",
		minimumInputLength:0,   //至少输入n个字符，才去加载数据
		allowClear: true,  //是否允许用户清除文本信息
		delay: 250,
		formatNoMatches:"没有结果",
		formatSearching:"搜索中...",
		formatAjaxError:"加载出错啦！",
		ajax : {
			url: context_path+"/BaseDicType/getKindList",
			type:"POST",
			dataType : 'json',
			delay : 250,
			data: function (term,pageNo) {     //在查询时向服务器端传输的数据
				term = $.trim(term);
				return {
					queryString: term,    //联动查询的字符
					pageSize: 15,    //一次性加载的数据条数
					pageNo:pageNo,    //页码
					description:'CIRCULATION_SHEET'
				}
			},
			results: function (data,pageNo) {
				var res = data.result;
				if(res.length>0){   //如果没有查询到数据，将会返回空串
					var more = (pageNo*15)<data.total; //用来判断是否还有更多数据可以加载
					return {
						results:res,more:more
					};
				}else{
					return {
						results:{}
					};
				}
			},
			cache : true
		}
	});

	//打印机编码
	$("#printer_edit_queryForm #printer_edit_printer").select2({
		placeholder: "选择层级",
		minimumInputLength:0,   //至少输入n个字符，才去加载数据
		allowClear: true,  //是否允许用户清除文本信息
		delay: 250,
		formatNoMatches:"没有结果",
		formatSearching:"搜索中...",
		formatAjaxError:"加载出错啦！",
		ajax : {
			url: context_path+"/BaseDicType/getKindList",
			type:"POST",
			dataType : 'json',
			delay : 250,
			data: function (term,pageNo) {     //在查询时向服务器端传输的数据
				term = $.trim(term);
				return {
					queryString: term,    //联动查询的字符
					pageSize: 15,    //一次性加载的数据条数
					pageNo:pageNo,    //页码
					description:'PRINTER'
				}
			},
			results: function (data,pageNo) {
				var res = data.result;
				if(res.length>0){   //如果没有查询到数据，将会返回空串
					var more = (pageNo*15)<data.total; //用来判断是否还有更多数据可以加载
					return {
						results:res,more:more
					};
				}else{
					return {
						results:{}
					};
				}
			},
			cache : true
		}
	});

	if($("#printer_edit_queryForm #printer_edit_printerid").val()!=""){
		$("#printer_edit_queryForm #printer_edit_printer").select2("data", {
			id: $("#printer_edit_queryForm #printer_edit_printerid").val(),
			text: $("#printer_edit_queryForm #printer_edit_printername").val()
		});
	}
	if($("#printer_edit_queryForm #printer_edit_hierarchyid").val()!=""){
		$("#printer_edit_queryForm #printer_edit_hierarchy").select2("data", {
			id: $("#printer_edit_queryForm #printer_edit_hierarchyid").val(),
			text: $("#printer_edit_queryForm #printer_edit_hierarchyname").val()
		});
	}
	if($("#printer_edit_queryForm #printer_edit_typeid").val()!=""){
		$("#printer_edit_queryForm #printer_edit_type").select2("data", {
			id: $("#printer_edit_queryForm #printer_edit_typeid").val(),
			text: $("#printer_edit_queryForm #printer_edit_typename").val()
		});
	}
</script>