<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
	String path = request.getContextPath();
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<!-- 表格 -->
<div class="row-fluid" id="printer_list_grid-div" style="position:relative;margin-top: 0px; float: left;">
    <form id="printer_list_hiddenQueryForm" style="display:none;">
        <input name="printer" value=""/>
        <input name="hierarchy" value="">
        <input name="type" value="">
    </form>
	<!-- 工具栏 -->
	<%--<c:if test="${operationCode.webSearch==1}">--%>
	<div class="query_box" id="printer_list_yy" title="查询选项">
         <form id="printer_list_query_form"   style="max-width:  100%;">
		 <ul class="form-elements">
			 <li class="field-group field-fluid3">
				 <label class="inline" for="printer_list_printer" style="margin-right:20px;width:100%;">
					 <span class="form_label" style="width:80px;">打印机：</span>
					 <input type="text" id="printer_list_printer"  name="printer" value="" style="width: calc(100% - 85px);" placeholder="打印机" />
				 </label>
			 </li>
			 <li class="field-group field-fluid3">
				 <label class="inline" for="printer_list_hierarchy" style="margin-right:20px;width: 100%;">
					 <span class="form_label" style="width:80px;">层级：</span>
					 <input id="printer_list_hierarchy" name="hierarchy" type="text" style="width:  calc(100% - 85px);" placeholder="层级">
				 </label>
			 </li>
			 <li class="field-group field-fluid3">
				 <label class="inline" for="printer_list_type" style="margin-right:20px;width: 100%;">
					 <span class="form_label" style="width:80px;">单据类型：</span>
					 <input id="printer_list_type" name="type" type="text" style="width: calc(100% - 85px);" placeholder="单据类型">
				 </label>
			 </li>
		</ul>
			<div class="field-button" style="">
				<div class="btn btn-info" onclick="printer_list_openQueryPage();">
		            <i class="ace-icon fa fa-check bigger-110"></i>查询
	            </div>
				<div class="btn" onclick="printer_list_reset();"><i class="ace-icon icon-remove"></i>重置</div>
				<%--<a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>--%>
	        </div>
	  </form>
    </div>
	<%--</c:if>--%>

	<div class="row-fluid" id="printer_list_table_toolbar" style="padding:5px 3px;" >
		<c:if test="${operationCode.webAdd==1}"><button class=" btn btn-primary btn-addQx" onclick="printer_list_openAddPage();">
			添加<i class="fa fa-plus" aria-hidden="true" style="margin-left:5px;"></i>
		</button></c:if>
		<c:if test="${operationCode.webEdit==1}"><button class=" btn btn-primary btn-editQx" onclick="printer_list_openEditPage();">
			编辑<i class="fa fa-pencil" aria-hidden="true" style="margin-left:5px;"></i>
		</button></c:if>
	</div>
	<!-- 表格 -->
	<div class="row-fluid" style="padding:0 3px;">
		<!-- 表格数据 -->
		<table id="printer_list_grid-table" style="width:100%;"></table>
		<!-- 表格底部 -->
		<div id="printer_list_grid-pager"></div>
	</div>
</div>

<script src="<%=request.getContextPath()%>/static/techbloom/system/printer/printer_list.js"></script>
