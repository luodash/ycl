<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
	String path = request.getContextPath();
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<!-- 表格 -->
<div class="row-fluid" id="systemlog_list_grid-div" style="position:relative;margin-top: 0px; float: left;">
    <form id="systemlog_list_hiddenQueryForm" style="display:none;">
        <input name="name" value=""/>
        <input name="startTime" value="">
		<input name="endTime" value="">
    </form>
	<!-- 工具栏 -->
	<c:if test="${operationCode.webSearch==1}">
	<div class="query_box" id="systemlog_list_yy" title="查询选项">
         <form id="systemlog_list_query_form"   style="max-width:  100%;">
		 <ul class="form-elements">
			 <li class="field-group field-fluid3">
				 <label class="inline"  style="margin-right:20px;width:100%;">
					 <span class="form_label" style="width:80px;">操作人：</span>
					 <input type="text" id="systemlog_list_name" name="name" value="" style="width: calc(100% - 85px);" placeholder="用户姓名" />
				 </label>
			 </li>
			 <li class="field-group field-fluid3">
				 <label class="inline" style="margin-right:20px;width:100%;">
					 <span class="form_label" style="width:80px;">时间起：</span>
					 <input type="text" class="form-control date-picker" id="systemlog_list_startTime" name="startTime" value=""
							style="width: calc(100% - 85px);" placeholder="操作时间起" />
				 </label>
			 </li>
			 <li class="field-group field-fluid3">
				 <label class="inline" style="margin-right:20px;width:100%;">
					 <span class="form_label" style="width:80px;">时间止：</span>
					 <input type="text" class="form-control date-picker" id="systemlog_list_endTime" name="endTime" value=""
							style="width: calc(100% - 85px);" placeholder="操作时间止" />
				 </label>
			 </li>
		</ul>
			<div class="field-button" style="">
				<div class="btn btn-info" onclick="systemlog_list_openQueryPage();">
		            <i class="ace-icon fa fa-check bigger-110"></i>查询
	            </div>
				<div class="btn" onclick="systemlog_list_reset();"><i class="ace-icon icon-remove"></i>重置</div>
				<%--<a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>--%>
	        </div>
	  </form>
    </div>
	</c:if>

	<div class="row-fluid" id="systemlog_list_table_toolbar" style="padding:5px 3px;" >
		<c:if test="${operationCode.webInfo==1}"><button class=" btn btn-primary btn-editQx" onclick="systemlog_list_openInfoPage();">
			查看<i class="icon-search" aria-hidden="true" style="margin-left:5px;"></i>
		</button></c:if>
	</div>
	<!-- 表格 -->
	<div class="row-fluid" style="padding:0 3px;">
		<!-- 表格数据 -->
		<table id="systemlog_list_grid-table" style="width:100%;"></table>
		<!-- 表格底部 -->
		<div id="systemlog_list_grid-pager"></div>
	</div>
</div>

<script type="text/javascript">
	var systemlog_list_grid;
	var $queryWindow_systemlog_list;  //查询窗口对象
	var systemlog_list_dynamicDefalutValue="60cce761390a4e51bc4636271b52ab94";
	var systemlog_list_queryFormData = iTsai.form.serialize($("#systemlog_list_hiddenQueryForm"));

	//时间控件
	$(".date-picker").datetimepicker({
		format: "YYYY-MM-DD HH:mm:ss" ,
		autoclose : true,
		todayHighlight : true
	});

	$(function  (){
		$(".toggle_tools").click();
	});

	$("input").keypress(function (e) {
		if (e.which === 13) {
			systemlog_list_openQueryPage();
		}
	});

	systemlog_list_grid = jQuery("#systemlog_list_grid-table").jqGrid({
		url : context_path + '/systemLog/list.do',
		datatype : "json",
		colNames : [ 'ID','操作人', '操作IP','操作','操作时间' ],
		colModel : [
			{name : 'userId',index : 'USER_ID',hidden : true},
			{name : 'userName',index : 'USER_ID',width : 300},
			{name : 'userip',index : 'userip',width : 380},
			{name : 'operation',index : 'OPERATION',width : 370},
			{name : 'operationTimeStr',index : 'OPERATION_TIME',width : 380}

		],
		rowNum : 20,
		rowList : [ 10, 20, 30 ],
		pager : '#systemlog_list_grid-pager',
		sortname : 'id',
		sortorder : "desc",
		altRows : true,
		viewrecords : true,
		hidegrid : false,
		autowidth : false,
		multiselect : true,
		multiboxonly : true,
		shrinkToFit : false,
		autoScroll : true,
		beforeRequest:function (){
			dynamicGetColumns(systemlog_list_dynamicDefalutValue,"systemlog_list_grid-table",$(window).width()-$("#sidebar").width() -7);
			//重新加载列属性
		},
		loadComplete : function(data){
			var table = this;
			setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
			$("#systemlog_list_grid-table").closest(".ui-jqgrid-bdiv").css({ 'overflow-x': 'auto' });
		},
		emptyrecords: "没有相关记录",
		loadtext: "加载中...",
		pgtext : "页码 {0} / {1}页",
		recordtext: "显示 {0} - {1}共{2}条数据"
	});

	jQuery("#systemlog_list_grid-table").navGrid('#systemlog_list_grid-pager',
	{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd('#systemlog_list_grid-pager',{
		caption:"",
		buttonicon:"fa fa-refresh green",
		onClickButton: function(){
			$("#systemlog_list_grid-table").jqGrid('setGridParam', {
				postData: {queryJsonString:""} //发送数据 
			}).trigger("reloadGrid");
		}
	}).navButtonAdd('#systemlog_list_grid-pager',{
		caption: "",
		buttonicon:"fa  icon-cogs",
		onClickButton : function (){
			jQuery("#systemlog_list_grid-table").jqGrid('columnChooser',{
				done: function(perm, cols){
					dynamicColumns(cols,systemlog_list_dynamicDefalutValue);
					$("#systemlog_list_grid-table").jqGrid( 'setGridWidth', $(window).width()-$("#sidebar").width() -7);
				}
			});
		}
	});

	$(window).on("resize.jqGrid", function () {
		$("#systemlog_list_grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);
		$("#systemlog_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-
		$("#systemlog_list_table_toolbar").outerHeight(true)- $("#systemlog_list_yy").outerHeight(true)-
		$("#systemlog_list_fixed_tool_div").outerHeight(true)- $("#systemlog_list_grid-pager").outerHeight(true)-
		$("#gview_systemlog_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
	});
	$(window).triggerHandler('resize.jqGrid');

	//重新加载表格
	function gridReload(){
		systemlog_list_grid.trigger("reloadGrid");  //重新加载表格
	}

	/**
	 * 获取查询页面中的值，并将值放入列表页面中隐藏的form
	 * @param jsonParam	查询页面传递过来的json对象
	 */
	function systemlog_list_queryLogListByParam(jsonParam){
		var queryJsonString = JSON.stringify(jsonParam);         //将json对象转换成json字符串
		//执行查询操作
		$("#systemlog_list_grid-table").jqGrid('setGridParam', {
			postData: {queryJsonString:queryJsonString} //发送数据 
		}).trigger("reloadGrid");
	}

	/**打开详情列表页面*/
	function systemlog_list_openInfoPage(){
		var selectAmount = getGridCheckedNum("#systemlog_list_grid-table");
		if(selectAmount===0){
			layer.msg("请选择一条记录！",{icon:2});
			return;
		}else if(selectAmount>1){
			layer.msg("只能选择一条记录！",{icon:8});
			return;
		}
		layer.load(2);
		$.post(context_path+'/interfaceLog/toDetailList.do', {
			id:jQuery("#systemlog_list_grid-table").jqGrid("getGridParam", "selrow")
		}, function(str){
            $queryWindow_systemlog_list = layer.open({
				title : "接口日志详情",
				type: 1,
				skin : "layui-layer-molv",
				area : [window.screen.width-150+"px",document.body.clientHeight-150+"px"],
				shade: 0.6, //遮罩透明度
				moveType: 1, //拖拽风格，0是默认，1是传统拖动
				content: str,//注意，如果str是object，那么需要字符拼接。
				success:function(layero, index){
					layer.closeAll("loading");
				}
			});
		}).error(function() {
			layer.closeAll();
			layer.msg("加载失败！",{icon:2});
		});
	}

	/**
	 * 查询按钮点击事件
	 */
	function systemlog_list_openQueryPage(){
		var queryParam = iTsai.form.serialize($('#systemlog_list_query_form'));
		systemlog_list_queryLogListByParam(queryParam);
	}

	/**重置*/
	function systemlog_list_reset(){
		iTsai.form.deserialize($("#systemlog_list_query_form"),systemlog_list_queryFormData);
		// $("#systemlog_list_query_form #systemlog_list_success").select2("val","");
		systemlog_list_openQueryPage();
	}

</script>
