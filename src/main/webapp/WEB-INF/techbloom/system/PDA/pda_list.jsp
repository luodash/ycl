﻿﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- 表格 -->
<style type="text/css">
    .checkbox {
        position: relative;
        display: inline-block;
        text-align: center;
        font-size: large;
        height: 20px;
    }
    .checkbox *{display:inline-block;vertical-align:middle}
    .checkbox:after, .checkbox:before {
        font-family: FontAwesome;
        -webkit-font-feature-settings: normal;
        -moz-font-feature-settings: normal;
        font-feature-settings: normal;
        -webkit-font-kerning: auto;
        -moz-font-kerning: auto;
        font-kerning: auto;
        -webkit-font-language-override: normal;
        -moz-font-language-override: normal;
        font-language-override: normal;
        font-stretch: normal;
        font-style: normal;
        font-synthesis: weight style;
        font-variant: normal;
        font-weight: normal;
        text-rendering: auto;
    }
    .checkbox label {
        width: 90px;
        height: 42px;
        background: #ccc;
        position: relative;
        display: inline-block;
        border-radius: 46px;
        -webkit-transition: 0.4s;
        transition: 0.4s;
    }
    .checkbox label:after {
        content: '';
        position: absolute;
        width: 50px;
        height: 50px;
        border-radius: 100%;
        left: 0;
        top: -5px;
        z-index: 2;
        background: #fff;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.2);
        -webkit-transition: 0.4s;
        transition: 0.4s;
    }
    .checkbox input {
        position: absolute;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        z-index: 5;
        opacity: 0;
        cursor: pointer;
    }

    .checkbox input:hover + label:after {
        box-shadow: 0 2px 15px 0 rgba(0, 0, 0, 0.2), 0 3px 8px 0 rgba(0, 0, 0, 0.15);
    }
    .checkbox input:checked + label:after {
        left: 40px;
    }

    .model-1 .checkbox input:checked + label {
        background: #376ecb;
    }
    .model-1 .checkbox input:checked + label:after {
        background: #4285F4;
    }
    hr{
        position: relative;
        border-bottom-width: 1000px;
        margin-top: 30px;
    }
</style>

<div class="row-fluid" id="pda_list_grid-div"
     style="position:relative;float: left;text-align: center;text-align:center">
    <div class="row-fluid" id="pda_list_grid-test" style="padding:5px 3px;margin:20px auto;">
        <section class="model-1">
            <div class="checkbox">
                <p style="display:inline;margin-top: 5px">PDA权限开关:</p>
                <input type="checkbox" id="pda_list_mean_test" name="mean_test" onclick="pda_list_changeQX()" checked="checked"/>
                <label></label>
            </div>
        </section>
        <hr />
    </div>
</div>

<script>
    $(function(){
    	$.ajax({
            type:'GET',
            dataType: "json",
            url:context_path+"/pdaPowerCon/padPower?flag=1",
            success:function(res){
                var status = res.value;
                if(status == 0)
                    //改变开关的状态
                    $("#pda_list_mean_test").prop("checked",false);
                if(status == 1)
                    $("#pda_list_mean_test").prop("checked",true);
            },
            error:function(){
                alert("发生错误");
            }
        });
    });

    function pda_list_changeQX() {
        $.ajax({
            type:'GET',
            dataType: "json",
            url:context_path+"/pdaPowerCon/padPower?flag=2",
            success:function(res){
                var status = res.value;
                if(status == 0)
                    //改变开关的状态
                    $("#pda_list_mean_test").prop("checked",false);
                if(status == 1)
                    $("#pda_list_mean_test").prop("checked",true);
            },
            error:function(){
                alert("发生错误");
            }
        });
    }
</script>