<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
	String path = request.getContextPath();
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<!-- 表格 -->
<div class="row-fluid" id="uwb_list_grid-div"
	style="position:relative;margin-top: 0px; float: left;">
	<form id="uwb_list_hiddenForm" action="<%=path%>/uwb/toExcel" method="POST" style="display: none;">
		<!-- 选中的标签 -->
		<input id="uwb_list_ids" name="ids" value=""/>
	</form>
	<!-- 工具栏 -->
	<c:if test="${operationCode.webSearch==1}">
	<div class="query_box" id="uwb_list_yy" title="查询选项">
         <form id="uwb_list_query_form"   style="max-width:  100%;">
		 <ul class="form-elements">
			<!-- 标签编号 -->
			<li class="field-group field-fluid3">
				<label class="inline" for="uwb_list_name" style="margin-right:20px;width:  100%;">
					<span class='form_label'>标签号：</span>
					<input type="text" name="name" id="uwb_list_name" value="" style="width: calc(100% - 75px);" placeholder="标签号">
					<input type="text" name="name2" id="uwb_list_name2" value="" style="display: none;" >
				</label>
			</li>
		</ul>
			<div class="field-button" style="">
				<div class="btn btn-info" onclick="uwb_list_openQueryPage();">
		            <i class="ace-icon fa fa-check bigger-110"></i>查询
	            </div>
				<div class="btn" onclick="uwb_list_reset();"><i class="ace-icon icon-remove"></i>重置</div>
	        </div>
	  </form>
    </div>
    </c:if>
	<div class="row-fluid" id="uwb_list_table_toolbar" style="padding:5px 3px;" >
		<c:if test="${operationCode.webAdd==1}"><button class=" btn btn-primary btn-addQx" onclick="uwb_list_openAddPage();">
			添加<i class="fa fa-plus" aria-hidden="true" style="margin-left:5px;"></i>
		</button></c:if>
		<c:if test="${operationCode.webExport==1}"><button class="col-md-1 btn btn-primary btn-queryQx" onclick="uwb_list_exportLogFile();">
			导出<i class="fa fa-share" aria-hidden="true" style="margin-left:5px;"></i>
		</button></c:if>
	</div>
	<!-- 表格 -->
	<div class="row-fluid" style="padding:0 3px;">
		<!-- 表格数据 -->
		<table id="uwb_list_grid-table" style="width:100%;"></table>
		<!-- 表格底部 -->
		<div id="uwb_list_grid-pager"></div>
	</div>
</div>

<script src="<%=request.getContextPath()%>/static/techbloom/system/uwb/uwb.js"></script>
