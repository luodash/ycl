<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div id ="uwb_edit_page" class="row-fluid" style="height: inherit;">
	<form id="uwb_edit_queryForm" class="form-horizontal"  style="overflow: auto; height: calc(100% - 70px);">
		<input type="hidden" name="id" id="uwb_edit_id" value="${id}">
		<div class="control-group">
			<label class="control-label" for="uwb_edit_tag">UWB标签号：</label>
			<div class="controls">
				<div class="input-append span12 required" >
					<input class="span11" type="text"   name="tag"  value="${tag}" id="uwb_edit_tag" placeholder="标签号">
				</div>
			</div>
		</div>

	</form>
	<div class="form-actions" style="text-align: right;border-top: 0px;margin: 0px;">
		<span  class="savebtn btn btn-success">保存</span>
		<span  class="btn btn-danger">取消</span>
	</div>
</div>

<script type="text/javascript">

    //表单校验
    $("#uwb_edit_queryForm").validate({
        rules:{
            "tag":{
                required:true
            }
        },
        messages:{
            "tag":{
                remote:"标签号不能为空，请重新输入！"
            }
        },
        errorClass: "help-inline",
        errorElement: "span",
        highlight:function(element, errorClass, validClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        }
    });

    //确定按钮点击事件
    $("#uwb_edit_page .btn-success").off("click").on("click", function(){
        //uwb保存
        if($("#uwb_edit_queryForm").valid())
            uwb_edit_saveUwb($("#uwb_edit_queryForm").serialize());
    });

    //取消按钮点击事件
    $("#uwb_edit_page .btn-danger").off("click").on("click", function(){
        layer.close($queryWindow);
    });
    var ajaxStatus = 1;     //ajax请求状态：0不能请求，1可以请求
    //保存/修改uwb信息
    function uwb_edit_saveUwb(bean){
        if(bean){
            if(ajaxStatus==0){
                layer.msg("保存中，请稍后...",{icon:2});
                return;
            }
            ajaxStatus = 0;
            $(".savebtn").attr("disabled","disabled");
            $.ajax({
                url:context_path+"/uwb/save?tm="+new Date(),
                type:"POST",
                data:bean,
                dataType:"JSON",
                success:function(data){
                    ajaxStatus = 1; //将标记设置为可请求
                    $(".savebtn").removeAttr("disabled");
                    if(data.result){
                        layer.msg("保存成功！",{icon:1});
                        //刷新用户列表
                        $("#uwb_list_grid-table").jqGrid('setGridParam',
                            {
                                postData: {queryJsonString:""} //发送数据
                            }
                        ).trigger("reloadGrid");
                        //关闭当前窗口
                        //关闭指定的窗口对象
                        layer.close($queryWindow);
                    }else{
                        layer.msg("保存失败，请稍后重试！",{icon:2});
                    }
                }
            });
        }else{
            layer.msg("出错啦！",{icon:2});
        }
    }

</script>