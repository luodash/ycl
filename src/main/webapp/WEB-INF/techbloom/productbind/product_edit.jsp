<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<div id="productbind_edit_page" class="row-fluid" style="height: inherit;">
	<form id="productbind_edit_productbindForm" class="form-horizontal" style="overflow: auto; height: calc(100% - 70px);">
		<input type="hidden" id="productbind_edit_id" name="id" value="${productBind.id}">
		<div class="control-group">
			<label class="control-label" >产线：</label>
			<div class="controls required">
				<input  type = "text" class="span11"  id="factoryId" name="factoryId" value="${productBind.factoryId}" placeholder="产线"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" >ip地址：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="ipaddress" name="ipaddress" value="${productBind.ipaddress}" placeholder="ip地址">
				</div>
			</div>
		</div>
	</form>
	<div class="field-button" style="text-align: center;border-top: 0px;margin: 15px auto;">
		<span class="btn btn-info" onclick="saveForm();">
		   <i class="ace-icon fa fa-check bigger-110"></i>保存
		</span>
		<span class="btn btn-danger" onclick="layer.closeAll();">
		   <i class="icon-remove"></i>&nbsp;取消
		</span>
	</div>
</div>
<script type="text/javascript">

	$("#productbind_edit_productbindForm").validate({
		rules:{
            "factoryId":{
                required:true
            },
            "ipaddress":{
                required:true
            }
  		},
  		messages:{
            "factoryId":{
                required:"请输入产线！"
            },
            "ipaddress":{
                required:"请输入ip地址！"
            }
  		},
  		errorClass: "help-inline",
		errorElement: "div",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
		}
  	});
	//确定按钮点击事件
    function saveForm() {
        if ($("#productbind_edit_productbindForm").valid()) {
            saveCarInfo($("#productbind_edit_productbindForm").serialize());
        }
    }
  	//保存/修改用户信息
    function saveCarInfo(bean) {
        $.ajax({
           url: context_path + "/productBind/saveProductBind",
           type: "POST",
           data: bean,
           dataType: "JSON",
           success: function (data) {
               if (Boolean(data.result)) {
                   layer.msg("保存成功！", {icon: 1});
                   //关闭当前窗口
                   layer.close($queryWindow);
                   //刷新列表
                   $("#productbind_list_grid-table").jqGrid('setGridParam',
					{
						postData: {queryJsonString: ""}
					}).trigger("reloadGrid");
               } else {
                   layer.alert("保存失败，请稍后重试！", {icon: 2});
               }
           },
           error:function(XMLHttpRequest){
       		alert(XMLHttpRequest.readyState);
       		alert("出错啦！！！");
		   }
       });
    }

</script>