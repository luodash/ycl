<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="productbind_list_grid-div">
   <%-- <form id="productbind_list_hiddenForm" action="<%=path%>/productbind/materialExcel" method="POST" style="display: none;">
        <input id="productbind_list_ids" name="ids" value=""/>
		<input name="queryCode" id="productbind_list_queryCode" value="">
        <input name="queryWarehouse" id="productbind_list_queryWarehouse" value="">
    </form>--%>
    <div id="productbind_list_fixed_tool_div" class="fixed_tool_div">
        <div id="productbind_list_toolbar_" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="productbind_list_grid-table" style="width:100%;height:100%;"></table>
    <div id="productbind_list_grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
	var productbind_list_oriData;
	var productbind_list_grid;

	$(function  (){
	    $(".toggle_tools").click();
	});
	
	$("#productbind_list_toolbar_").iToolBar({
	    id: "productbind_list_tb_01",
	    items: [
	        {label: "添加", hidden:"${operationCode.webAdd}"=="1", onclick:productbind_list_openAddPage, iconClass:'glyphicon glyphicon-plus'},
	        {label: "编辑",hidden:"${operationCode.webEdit}"=="1", onclick: productbind_list_openEditPage, iconClass:'glyphicon glyphicon-pencil'},
	        {label: "删除", hidden:"${operationCode.webDel}"=="1", onclick: productbind_list_deleteCars, iconClass:'glyphicon glyphicon-trash'}
	   ]
	});

    productbind_list_grid = jQuery("#productbind_list_grid-table").jqGrid({
			url : context_path + "/productBind/list.do",
		    datatype : "json",
		    colNames : [ "主键","产线", "ip地址"],
		    colModel : [ 
                 {name : "id",index : "id",hidden:true},
                 {name : "factoryId",index : "factoryId",width : 60},
                 {name : "ipaddress",index : "ipaddress",width : 60}
		    ],
		    rowNum : 20,
		    rowList : [ 10, 20, 30 ],
		    pager : "#productbind_list_grid-pager",
		    sortname : "id",
		    sortorder : "asc",
            altRows: true,
            viewrecords : true,
            hidegrid:false,
     	    autowidth:true, 
            multiselect:true,
            loadComplete : function(data) {
            	var table = this;
            	setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
                productbind_list_oriData = data;
            },
            emptyrecords: "没有相关记录",
            loadtext: "加载中...",
            pgtext : "页码 {0} / {1}页",
            recordtext: "显示 {0} - {1}共{2}条数据"
	});

	jQuery("#productbind_list_grid-table").navGrid("#productbind_list_grid-pager",
     {edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#productbind_list_grid-pager",{
		caption:"",   
		buttonicon:"fa fa-refresh green",   
		onClickButton: function(){   
			$("#productbind_list_grid-table").jqGrid("setGridParam", 
					{
				      postData: {queryJsonString:""} //发送数据 
					}
			).trigger("reloadGrid");
		}
	}).navButtonAdd("#productbind_list_grid-pager",{
        caption: "",
        buttonicon:"fa icon-cogs",
        onClickButton : function (){
            jQuery("#productbind_list_grid-table").jqGrid("columnChooser",{
                done: function(perm, cols){
                    // dynamicColumns(cols,materials_list_dynamicDefalutValue);
                    $("#productbind_list_grid-table").jqGrid("setGridWidth", $("#productbind_list_grid-div").width());
                }
            });
        }
    });

	$(window).on("resize.jqGrid", function () {
		$("#productbind_list_grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);
		$("#productbind_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-
		$("#productbind_list_yy").outerHeight(true)-$("#productbind_list_fixed_tool_div").outerHeight(true)-
		$("#productbind_list_grid-pager").outerHeight(true)-
		$("#gview_productbind_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
	});
	$(window).triggerHandler("resize.jqGrid");
	
	/**打开添加页面*/
	function productbind_list_openAddPage(){
		$.post(context_path + "/productBind/toAdd.do", {}, function (str){
			$queryWindow = layer.open({
			    title : "添加",
		    	type:1,
		    	skin : "layui-layer-molv",
		    	area : "600px",
		    	shade : 0.6, //遮罩透明度
			    moveType : 1, //拖拽风格，0是默认，1是传统拖动
			    anim : 2,
			    content : str,
			    success: function (layero, index) {
	                layer.closeAll('loading');
	            }
			});
		});
	}

	/**打开编辑页面*/
	function productbind_list_openEditPage(){
		var selectAmount = getGridCheckedNum("#productbind_list_grid-table");
		if(selectAmount==0){
			layer.msg("请选择一条记录！",{icon:2});
			return;
		}else if(selectAmount>1){
			layer.msg("只能选择一条记录！",{icon:8});
			return;
		}
		layer.load(2);
		$.post(context_path+'/productBind/toAdd.do', {
		    id : jQuery("#productbind_list_grid-table").jqGrid("getGridParam", "selrow")
        }, function(str){
			$queryWindow = layer.open({
				title : "编辑",
				type: 1,
			    skin : "layui-layer-molv",
			    area : "600px",
				shade: 0.6, //遮罩透明度
				moveType: 1, //拖拽风格，0是默认，1是传统拖动
				content: str,//注意，如果str是object，那么需要字符拼接。
				success:function(layero, index){
			   		layer.closeAll("loading");
			    }
			});
		}).error(function() {
			layer.closeAll();
	    	layer.msg("加载失败！",{icon:2});
		});
	}
	
	function productbind_list_deleteCars(){
		var checkedNum = getGridCheckedNum("#productbind_list_grid-table", "id");  //选中的数量
	    if (checkedNum == 0) {
	    	layer.alert("请选择一个要删除的绑定关系！");
	    } else {
	        layer.confirm("确定删除选中的绑定关系？", function() {
	    		$.ajax({
	    			type : "POST",
	    			url : context_path + "/productBind/deleteBind?ids="+jQuery("#productbind_list_grid-table").jqGrid("getGridParam", "selarrrow") ,
	    			dataType : "json",
	    			cache : false,
	    			success : function(data) {
	    				layer.closeAll();
	    				if (Boolean(data.result)) {
	    					layer.msg(data.msg, {icon: 1,time:1000});
	    				}else{
	    					layer.msg(data.msg, {icon: 7,time:1000});
	    				}
                        productbind_list_grid.trigger("reloadGrid");  //重新加载表格
	    			}
	    		});
	    	});
	    }  
	}

</script>