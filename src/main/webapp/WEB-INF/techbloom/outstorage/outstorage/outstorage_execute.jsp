<%@ page language="java" import="java.lang.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
    String path = request.getContextPath();
%>
<style>
    .floatLR{
        float: left;
        margin-right: 10px;
    }
</style>
<div class="row-fluid" style="height: inherit;margin:0px;border: 0px">
    <!-- 表格div -->
    <div id="outstorage_execute_grid-div-c" style="width:100%;margin:10px auto;">
        <input id="outstorage_execute_id1" type="hidden" name="id" value="${id}"/>
        <!-- 	表格工具栏 -->
        <div id="outstorage_execute_fixed_tool_div" class="fixed_tool_div detailToolBar">
            <div id="outstorage_execute_toolbar_-c" style="float:left;overflow:hidden;"></div>
        </div>
        <div style="margin-bottom:5px;margin-left: 25px;">
            <span class="btn btn-info" id="outStorageOut">
		       <i class="ace-icon fa fa-check bigger-110"></i>确认出库
            </span>
        </div>
        <!-- 出库单详情信息表格 -->
        <table id="outstorage_execute_grid-table-c" style="width:100%;height:100%;"></table>
        <!-- 表格分页栏 -->
        <div id="outstorage_execute_grid-pager-c"></div>
    </div>
</div>
<iframe src="about:blank" name="_ifr" height="0" class="hidden"></iframe>
<script type="text/javascript" src="<%=path%>/static/js/techbloom/outstorage/outstorage/outstorage_execute.js"></script>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriDataDetail;
    var _grid_detail;//表格对象
    var id = $("#outstorage_execute_id1").val();
    var ajaxStatus = 1;     //ajax请求状态：0不能请求，1可以请求

    //数量输入验证
    function numberRegex(value, colname) {
        var regex = /^\d+\.?\d{0,2}$/;
        if (!regex.test(value)) {
            return [false, ""];
        } else  {
            return [true, ""];
        }
    }

    _grid_detail = jQuery("#outstorage_execute_grid-table-c").jqGrid({
        url : context_path + "/outsrorageCon/detailList.do?id=" + id,
        datatype : "json",
        colNames : ["详情主键","状态","质保号","批次号","盘号","数量","单位","盘规格","颜色","重量","段号","操作"],
        colModel : [
        	{name : "id",index : "id",width : 20,hidden:true},
            {name : 'state',index : 'state',width : 20,
                formatter:function(cellValu,option,rowObject){
                    if(cellValu=='1'){
                        return "<span style=\"color:black;font-weight:bold;\">未出库</span>";
                    } if(cellValu=='2'){
                        return "<span style=\"color:blue;font-weight:bold;\">拣货中</span>";
                    } if(cellValu=='3'){
                        return "<span style=\"color:green;font-weight:bold;\">出库完成</span>";
                    } if(cellValu=='4'){
                        return "<span style=\"color:orange;font-weight:bold;\">预出库</span>";
                    }
                }
            },
            {name : "qaCode",index:"qaCode",width : 30},
            {name : "batchNo",index:"batch_No",width : 40},
            {name : "dishcode",index:"dishcode",width : 30},
            {name : "meter",index:"meter",width : 20},
            {name : "unit",index:"unit",width : 10},
            {name : 'model',index : 'model',width : 30},
            {name : 'colour',index : 'colour',width : 15},
            {name : 'weight',index : 'weight',width : 20},
            {name : 'segmentno',index : 'segmentno',width : 45},
            {name : "cz",index : "id",width :20,
                formatter:function(cellValu,option,rowObject){
                    var carCode = rowObject.carCode==null?"-1":'"'+rowObject.carCode+'"';
                    /*return "<button id='b1'class='btn btn-info floatLR' onclick='bind("+rowObject.id+","+carCode+")'>绑定行车</button>" +
                        "<button id='b2' class='btn btn-info floatLR' onclick='detailStart("+rowObject.id+")'>开始拣货</button>" +
                        "<button id='b3' class='btn btn-info floatLR' onclick='detailSuccess("+rowObject.id+")'>拣货完成</button>";*/
                    return "<button id='b1'+rowObject.id class='btn btn-info floatLR' onclick='detailSuccess("+rowObject.id+")'>出库完成</button>";
                }
            }
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : "#outstorage_execute_grid-pager-c",
        sortname : "t.id",
        sortorder : "asc",
        altRows: true,
        viewrecords : true,
        caption : "详情列表",
        autowidth:true,
        multiselect:true,
        multiboxonly: true,
        loadComplete : function(data) {
            var table = this;
            setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            oriDataDetail = data;
        },
        cellEdit: true,
        cellsubmit : "clientArray",
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据",
    });

    //在分页工具栏中添加按钮
    $("#outstorage_execute_grid-table-c").navGrid("#outstorage_execute_grid-pager-c", {edit:false,add:false,del:false,search:false,refresh:false})
    .navButtonAdd("#outstorage_execute_grid-pager-c",{
        caption:"",
        buttonicon:"ace-icon fa fa-refresh green",
        onClickButton: function(){
            $("#outstorage_execute_grid-table-c").jqGrid('setGridParam', {
                url:context_path + "/outsrorageCon/detailList.do?id="+id,
            }).trigger("reloadGrid");
        }
    });

    $(window).on("resize.jqGrid", function () {
        $("#outstorage_execute_grid-table-c").jqGrid("setGridHeight",400);
    });
    $(window).triggerHandler("resize.jqGrid");

</script>