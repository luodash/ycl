<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<script type="text/javascript">
	var context_path = '<%=path%>';
</script>
<body style="overflow:hidden;">
<div id="exchangePrint__page" class="row-fluid" style="height: inherit;">
<form id="exchange_list_hiddenQueryForm" style="display:none;">
	<input id="sddocno1" name="sddocno" value=""/>
	<input id="s_date1" name="s_date" value="">
	<input id="e_date1" name = "e_date" value="" />
</form>
<c:if test="${operationCode.webSearch==1}">
	<div class="query_box" id="exchangePrint_yy" title="查询选项">
		<form id="exchangePrint_queryForm" style="max-width:100%;">
			<ul class="form-elements">
				<li class="field-group field-fluid3">
					<label class="inline"  style="margin-right:20px;width: 100%;">
						<span class="form_label" style="width:80px;">流转单号：</span>
						<input id="shipNo" name="sddocno" type="text" style="width: calc(100% - 85px);" placeholder="物资流转单编号">
					</label>
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:100px;">开始打印时间：</span>
						<input type="text" class="form-control date-picker" id="s_date" name="s_date" value="" style="width: 160px;" placeholder="开始打印时间" />
					</label>
				</li>
				<li class="field-group field-fluid3">
					<label class="inline"  style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:100px;">结束打印时间：</span>
						<input type="text" class="form-control date-picker" id="e_date" name="e_date" value="" style="width: 160px;" placeholder="结束打印时间" />
					</label>
				</li>

				<li class="field-group-top field-group field-fluid3">
					<label class="inline"  style="margin-right:10px;width: 100%;">
						<span class="form_label" style="width:80px;">流出厂区：</span>

						<select id="s_factory" name="s_factory" type="text" style="width: calc(100% - 85px);" placeholder="厂区">
							<option value="">--请选择--</option>
							<c:forEach items="${factoryList}" var="factoryList">
								<option value="${factoryList.NAME}">${factoryList.NAME}</option>
							</c:forEach>
						</select>
					</label>
				</li>

				<li class=" field-group field-fluid3">
					<label class="inline"  style="margin-right:10px;width: 100%;">
						<span class="form_label" style="width:80px;">流转到：</span>
						<select id="e_factory" name="e_factory" type="text" style="width: calc(100% - 85px);" placeholder="厂区">
							<option value="">--请选择--</option>
							<c:forEach items="${factoryList}" var="factoryList">
								<option value="${factoryList.NAME}">${factoryList.NAME}</option>
							</c:forEach>
						</select>
					</label>
				</li>
			</ul>

			<div class="field-button" style="">
				<div class="btn btn-info" onclick="exchange_list_queryOk();">
					<i class="ace-icon fa fa-check bigger-110"></i>查询
				</div>
				<a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>
			</div>
		</form>
	</div>

	<div id="exchangePrint_list_fixed_tool_div" class="fixed_tool_div">
		<div id="exchangePrint_list_toolbar_" style="float:left;overflow:hidden;"></div>
	</div>
	<table id="exchangePrint_list_grid-table" style="width:100%;height:100%;" ></table>
	<div id="exchangePrint_list_grid-pager"></div>
</div>
</c:if>
</body>

<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">

    var exchange_oriData;
    var exchange_grid;
    var carNo = "";
    $(function  (){
        $(".toggle_tools").click();
    });

    $("#exchangePrint_list_toolbar_").iToolBar({
        id: "exchangePrint_list_tb_01",
        items: [
          /*  {label: "打印", hidden:"${operationCode.webPrint}"=="1", onclick:circulationPrint, iconClass:'icon-print'},*/
        ]
    });

	exchange_grid = jQuery("#exchangePrint_list_grid-table").jqGrid({
        url : context_path + "/exchange/datalist",
        datatype : "json",
        colNames : [ "物资流转单号", "车牌号","发货单号","发货厂区","目标厂区","打印时间"],
        colModel : [
            {name : "ODD_NUMBER",index : "ODD_NUMBER",width : 60},
            {name : "CAR",index : "CAR",width : 60},
			{name : "SHIP_NO",index : "SHIP_NO",width : 60},
            {name : "S_FACTORY_AREA",index : "S_FACTORY_AREA",width : 60},
            {name : "E_FACTORY_AREA",index : "E_FACTORY_AREA",width : 60},
			{name : "PRINT_TIME",index : "PRINT_TIME",width : 60},
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : "#exchangePrint_list_grid-pager",
        altRows: true,
        viewrecords : true,
        hidegrid:false,
        autowidth:true,
        multiselect:true,
        loadComplete : function(data) {
            var table = this;
            setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
			exchange_oriData = data;
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
    });

    jQuery("#exchangePrint_list_grid-table").navGrid("#exchangePrint_list_grid-pager", {edit:false,add:false,del:false,search:false,refresh:false})
	.navButtonAdd("#exchangePrint_list_grid-pager",{
        caption:"",
        buttonicon:"fa fa-refresh green",
        onClickButton: function(){
            $("#exchangePrint_list_grid-table").jqGrid("setGridParam", {
				postData: {queryJsonString:""} //发送数据
			}).trigger("reloadGrid");
        }
    }).navButtonAdd("#exchangePrint_list_grid-pager",{
        caption: "",
        buttonicon:"fa icon-cogs",
        onClickButton : function (){
            jQuery("#exchangePrint_list_grid-table").jqGrid("columnChooser",{
                done: function(perm, cols){
                    // dynamicColumns(cols,materials_list_dynamicDefalutValue);
                    $("#exchangePrint_list_grid-table").jqGrid("setGridWidth", $("#exchangePrint_list_grid-div").width());
                }
            });
        }
    });
    $(window).on("resize.jqGrid", function () {
        $("#exchangePrint_list_grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);
        $("#exchangePrint_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()- $("#exchangePrint_list_yy").outerHeight(true)-100-
		$("#exchangePrint_list_fixed_tool_div").outerHeight(true)- $("#exchangePrint_list_grid-pager").outerHeight(true)-
        $("#gview_exchangePrint_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
    });
    $(window).triggerHandler("resize.jqGrid");

    function getByteLen(val) {
        var len = 0;
        for (var i = 0; i < val.length; i++) {
            if (val[i].match(/[^\x00-\xff]/ig) != null) //全角
                len += 2;
            else
                len += 1;
        }
        return len;
    }

	/**查询*/
	function exchange_list_queryOk(){
		//执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
		exchange_list_queryInstoreListByParam(iTsai.form.serialize($('#exchangePrint_queryForm')));
	}

	function exchange_list_queryInstoreListByParam(jsonParam){
		iTsai.form.deserialize($('#exchange_list_hiddenQueryForm'),jsonParam);   //将json对象反序列化到列表页面中隐藏的form中
		//执行查询操作
		$("#exchangePrint_list_grid-table").jqGrid('setGridParam', {
			postData: {queryJsonString:JSON.stringify(iTsai.form.serialize($('#exchange_list_hiddenQueryForm')))} //发送数据
		}).trigger("reloadGrid");
	}

    /**
	 * 重新加载列表
     * @param outno
     * @param carno
     */
	function reloadlIst(outno,carno){
        var s_date = $("#s_date").val();
        var e_date = $("#e_date").val();
        $("#exchangePrint_list_grid-table").show();
        $("#exchangePrint_list_grid-table").jqGrid("setGridParam", {
			postData: {outno: outno,carno:carno,s_date:s_date,e_date:e_date}
		}).trigger("reloadGrid");
	}


	$(".date-picker").datetimepicker({
		format: "YYYY-MM-DD HH:mm:ss" ,
		autoclose : true,
		todayHighlight : true
	});

	function manual_today(){
		var today = new Date();
		var h = today.getFullYear();
		var m = today.getMonth()+1;
		var d = today.getDate();
		var hh = today.getHours();
		var mm = today.getMinutes();
		var ss = today.getSeconds();
		m = m<10?"0"+m:m;
		d = d<10?"0"+d:d;
		hh = hh < 10 ? "0" + hh:hh;
		mm = mm < 10 ? "0" +  mm:mm;
		ss = ss < 10 ? "0" + ss:ss;
		return h+"-"+m+"-"+d+" 00:00:00";
	}
	document.getElementById("s_date").value = manual_today();
</script>