<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
%>
<div id="outstorage_edit_page" class="row-fluid" style="height: inherit;margin:0px">
    <form action="" class="form-horizontal" id="outstorage_edit_baseInfor" name="baseInfor" method="post" target="_ifr" style="border-bottom: solid 2px #3b73af;">
        <input type="hidden" id="outstorage_edit_id" name="id" value="${info.id }">
        <div class="row" style="margin:0;padding:0;">
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="outstorage_edit_shipNo" >发货单编号：</label>
                <div class="controls">
                    <div class="required" >
                        <input type="text" id="outstorage_edit_shipNo" class="span10" name="shipNo" value="${info.shipNo }" readonly="readonly" placeholder="发货单编号" />
                    </div>
                </div>
            </div>
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="outstorage_edit_salesName" >销售经理：</label>
                <div class="controls">
                    <div class="required" >
                        <input type="text" id="outstorage_edit_salesName" class="span10" name="salesName" value="${info.salesName }" readonly="readonly" placeholder="销售经理" />
                    </div>
                </div>
            </div>
        </div>
        <%--<div style="margin-bottom:5px;">
            <span class="btn btn-info" id="outstorage_edit_formSave">
		       <i class="ace-icon fa fa-check bigger-110"></i>保存
            </span>
        </div>--%>
    </form>
    <div id="outstorage_edit_materialDiv" style="margin:10px;">
        <label class="inline" for="outstorage_edit_qaCode">质保单号：</label>
        <input type="text" id = "outstorage_edit_qaCode" name="qaCode" style="width:350px;margin-right:10px;" />
        <button id="outstorage_edit_addMaterialBtn" class="btn btn-xs btn-primary" onclick="addDetail();">
            <i class="icon-plus" style="margin-right:6px;"></i>添加
        </button>
    </div>
    <div id="outstorage_edit_grid-div-c" style="width:100%;margin:0px auto;">
        <div id="outstorage_edit_fixed_tool_div" class="fixed_tool_div detailToolBar">
            <div id="outstorage_edit___toolbar__-c" style="float:left;overflow:hidden;"></div>
        </div>
        <table id="outstorage_edit_grid-table-c" style="width:100%;height:100%;"></table>
        <div id="outstorage_edit_grid-pager-c"></div>
    </div>
</div>
<iframe src="about:blank" name="_ifr" height="0" class="hidden"></iframe>
<script type="text/javascript" src="<%=path%>/static/js/techbloom/outstorage/outstorage/outstorage_edit.js"></script>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriData;      //表格数据
    var _grid;        //表格对象
    var preStatus=${INSTORE.preStatus==null?0:INSTORE.preStatus};
    var selectData = 0;   //存放物料选择框中的值
    var selectParam = "";  //存放之前的查询条件

    //单据保存按钮点击事件
    $("#outstorage_edit_formSave").click(function(){
        if($('#outstorage_edit_baseInfor').valid()){
            //通过验证：获取表单数据，保存表单信息
            saveFormInfo($('#outstorage_edit_baseInfor').serialize());
        }
    });

    //清空物料多选框中的值
    function removeChoice(){
        $("#s2id_qaCode .select2-choices").children(".select2-search-choice").remove();
        $("#outstorage_edit_qaCode").select2("val","");
        selectData = 0;
    }

    $('[data-rel=tooltip]').tooltip();

    $("#outstorage_edit_qaCode").select2({
        placeholder : "请选择质保单号",//文本框的提示信息
        minimumInputLength : 0, //至少输入n个字符，才去加载数据
        allowClear : true, //是否允许用户清除文本信息
        multiple: true,
        closeOnSelect:false,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax : {
            url : context_path + '/outsrorageCon/getQaCode',
            dataType : 'json',
            delay : 250,
            data : function(term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                selectParam = term;
                return {
                    queryString : term, //联动查询的字符
                    pageSize : 15, //一次性加载的数据条数
                    pageNo : pageNo //页码
                }
            },
            results : function(data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results : res,
                        more : more
                    };
                } else {
                    return {
                        results : {
                            "id" : "0",
                            "text" : "没有更多结果"
                        }
                    };
                }
            },
            cache : true
        }
    });

    $("#outstorage_edit_qaCode").on("change",function(e){
        var datas=$("#outstorage_edit_qaCode").select2("val");
        selectData = datas;
        var selectSize = datas.length;
        if(selectSize>1){
            var $tags = $("#outstorage_edit_qaCode .select2-choices");
            var $choicelist = $tags.find(".select2-search-choice");
            var $clonedChoice = $choicelist[0];
            $tags.children(".select2-search-choice").remove();
            $tags.prepend($clonedChoice);
            $tags.find(".select2-search-choice").find("div").html(selectSize+"个被选中");
            $tags.find(".select2-search-choice").find("a").removeAttr("tabindex");
            $tags.find(".select2-search-choice").find("a").attr("href","#");
            $tags.find(".select2-search-choice").find("a").attr("onclick","removeChoice();");
        }
        //执行select的查询方法
        $("#outstorage_edit_qaCode").select2("search",selectParam);
    });

    //工具栏
    $("#outstorage_edit___toolbar__-c").iToolBar({
        id:"outstorage_edit___tb__01",
        items:[
            {label:"删除", onclick:delDetail}
        ]
    });


    //初始化表格
    _grid =  $("#outstorage_edit_grid-table-c").jqGrid({
        url : context_path + "/outsrorageCon/detailList.do?id="+$("#outstorage_edit_id").val(),
        datatype : "json",
        colNames : [ "详情主键","状态","质保号","批次号","盘号","数量","单位","盘规格","颜色","重量","段号"],
        colModel : [
            {name : "id",index : "id",width : 20,hidden:true},
            {name : 'state',index : 'state',width : 20,
                formatter:function(cellValu,option,rowObject){
                    if(cellValu=='1'){
                        return "<span style='color:black;font-weight:bold;'>未出库</span>";
                    } if(cellValu=='2'){
                        return "<span style='color:blue;font-weight:bold;'>拣货中</span>";
                    } if(cellValu=='3'){
                        return "<span style='color:green;font-weight:bold;'>出库完成</span>";
                    } if(cellValu=='4'){
                        return "<span style='color:green;font-weight:bold;'>出库完成</span>";
                    }
                    return cellValu;
                }
            },
            {name : "qaCode",index:"qaCode",width : 30},
            {name : "batchNo",index:"batch_No",width : 30},
            {name : "dishcode",index:"dishcode",width : 40},
            {name : "meter",index:"meter",width : 20},
            {name : "unit",index:"unit",width : 15},
            {name : 'model',index : 'model',width : 20},
            {name : 'colour',index : 'colour',width : 15},
            {name : 'weight',index : 'weight',width : 20},
            {name : 'segmentno',index : 'segmentno',width : 20}
            
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : "#outstorage_edit_grid-pager-c",
        sortname : "t.id",
        sortorder : "asc",
        altRows: true,
        viewrecords : true,
        autowidth:true,
        multiselect:true,
        multiboxonly: true,
        loadComplete : function(data){
            var table = this;
            setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            oriData = data;
            $(window).triggerHandler("resize.jqGrid");
        },
        cellEdit: true,
        cellsubmit : "clientArray",
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据",
    });
    //在分页工具栏中添加按钮
    $("#outstorage_edit_grid-table-c").navGrid("#outstorage_edit_grid-pager-c",
    	{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#outstorage_edit_grid-pager-c",{
            caption:"",
            buttonicon:"ace-icon fa fa-refresh green",
            onClickButton: function(){
                $("#outstorage_edit_grid-table-c").jqGrid("setGridParam",
                    {
                        url:context_path + "/outsrorageCon/detailList.do?id="+$("#outstorage_edit_id").val(),
                    }
                ).trigger("reloadGrid");
            }
        });

    $(window).on("resize.jqGrid", function () {
        $("#outstorage_edit_grid-table-c").jqGrid("setGridWidth", $("#outstorage_edit_grid-div-c").width() - 3 );
        $("#outstorage_edit_grid-table-c").jqGrid("setGridHeight", (document.documentElement.clientHeight - $("#outstorage_edit_grid-pager-c").height() - 380) );
    });
    $(window).triggerHandler("resize.jqGrid");


    //添加质保单号
    function addDetail(){
        if(selectData!=0){
            //将选中的物料添加到数据库中
            $.ajax({
                type:"POST",
                url:context_path + "/outsrorageCon/saveOutstorageDetail",
                data:{
                	id:$("#outstorage_edit_baseInfor #outstorage_edit_id").val(),
                	qaCodeIds:selectData.toString()
                },
                dataType:"json",
                success:function(data){
                    iTsai.form.deserialize($("#outstorage_list_hiddenQueryForm"), iTsai.form.serialize($("#outstorage_list_queryForm")));
                    var queryParam = iTsai.form.serialize($("#outstorage_list_hiddenQueryForm"));
                    var queryJsonString = JSON.stringify(queryParam);
                    removeChoice();   //清空下拉框中的值
                    if(data.result!=null){
                        layer.alert(data.msg);
                        reloadDetailTableList();
                        //重新加载表格
                        $("#outstorage_list_grid-table").jqGrid("setGridParam",
                            {
                                postData: {queryJsonString:queryJsonString}
                            }
                        ).trigger("reloadGrid");
                    }else{
                        layer.alert("添加失败");
                    }
                }
            });
        }else{
            layer.alert("请选择物料！");
        }
    }
</script>
