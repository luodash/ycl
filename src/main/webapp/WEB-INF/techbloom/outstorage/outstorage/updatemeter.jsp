<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<div id="updatemeter_page" class="row-fluid" style="height: inherit;">
	<form id="updatemeter_meterForm" class="form-horizontal" style="overflow: auto; height: calc(100% - 70px);">
        <input type="hidden" id="updatemeter_id" name="id" value="${info.id}">
		<div class="control-group">
			<div id="wrap" class="controls">
				<label style="margin-left: 40px;"><input type="radio" id="allOut" class="span10"  name="meter" value="1" />全部出库</label>
				<label style="margin-left: 200px;"><input type="radio" id="anMi" class="span10"  name="meter" value="2" />按米出库</label>
			</div>
		</div>
	</form>
	<div class="field-button" style="text-align: center;border-top: 0px;margin: 15px auto;">
		<span class="btn btn-info" onclick="saveForm();">
		   <i class="ace-icon fa fa-check bigger-110"></i>确认
		</span>
		<span class="btn btn-danger" onclick="layer.closeAll();">
		   <i class="icon-remove"></i>&nbsp;取消
		</span>
	</div>
</div>
<script type="text/javascript">
	var checkboxes = document.getElementsByName("meter");
	//默认选中
	checkboxes[0].checked = true;

	$("#updatemeter_meterForm").validate({
	rules:{
		"meter":{
				required:true,
			}
		},
		messages:{
			"meter":{
				required:"请输入要出库的米数",
			}
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
		}
	});

	//确定按钮点击事件
    function saveForm() {
        if ($("#updatemeter_meterForm").valid()){
        	var bean = $("#updatemeter_meterForm").serialize();
        	var gegnc = $('#wrap input[name="meter"]:checked').val();
        	$.ajax({
                url: context_path + "/outsrorageCon/detailSuccess",
                type: "POST",
                data: {
                	id:$('#updatemeter_meterForm #updatemeter_id').val(),
                	type:gegnc
                },
                dataType: "JSON",
                success: function (data) {
                    if (Boolean(data.result)) {
                        layer.msg(data.msg, {icon: 1});
                        //关闭当前窗口
                        layer.close($queryWindow);
                        //刷新列表
                        $("#outstorage_execute_grid-table-c").jqGrid('setGridParam',
	                     {
	                         postData: {queryJsonString: ""}
	                     }).trigger("reloadGrid");
                    } else {
                        layer.alert(data.msg, {icon: 2});
                    }
                },
                error:function(XMLHttpRequest){
            		alert(XMLHttpRequest.readyState);
            		alert("出错啦！！！");
            	}
            });
        }
    }

</script>