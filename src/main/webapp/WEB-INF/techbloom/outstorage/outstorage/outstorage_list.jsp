<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<div id="outstorage_list_grid-div">
    <!-- 隐藏区域：存放查询条件 -->
    <form id="outstorage_list_hiddenForm" action="<%=path%>/outsrorageCon/materialExcel" method="POST" style="display:none;">
        <input id="outstorage_list_ids" name="ids" value=""/>
        <input id="outstorage_list_shipNo2"     name="shipNo"          value=""/>
        <input id="outstorage_list_createTime2" name="createTime"      value="">
        <input id="outstorage_list_endTime2"    name = "endTime"       value="" />
        <input id="outstorage__list_factcode2"  name = "factoryarea"   value="" />
        <input id="outstorage_list_orderno2"    name = "orderno"       value="" />
        <input id="outstorage_list_orderline2"  name = "orderline"     value="" />
        <input id="outstorage_list_contractno2" name = "contractno"    value="" />
        <input id="outstorage_list_queryExportExcelIndex" name="queryExportExcelIndex" value=""/>
    </form>
    <form id="outstorage_list_hiddenQueryForm" style="display:none;">
        <input id="outstorage_list_shipNo1" name="shipNo" value=""/>
        <input id="outstorage_list_createTime1" name="createTime" value="">
        <input id="outstorage_list_endTime1" name = "endTime" value="" />
        <input id="outstorage__list_factcode1" name = "factoryarea" value="" />
        <input id="outstorage_list_orderno1" name = "orderno" value="" />
        <input id="outstorage_list_orderline1" name = "orderline" value="" />
        <input id="outstorage_list_contractno1" name = "contractno" value="" />
    </form>
    <c:if test="${operationCode.webSearch==1}">
        <div class="query_box" id="outstorage_list_yy" title="查询选项">
            <form id="outstorage_list_queryForm" style="max-width:100%;">
                <ul class="form-elements">
                    <li class="field-group field-fluid3">
                        <label class="inline" for="outstorage_list_shipNo" style="margin-right:20px;width: 100%;">
                            <span class="form_label" style="width:80px;">发货单编号：</span>
                            <input id="outstorage_list_shipNo" name="shipNo" type="text" style="width: calc(100% - 85px);" placeholder="发货单编号">
                        </label>
                    </li>
                    <li class="field-group field-fluid3">
                        <label class="inline" for="outstorage_list_createTime" style="margin-right:20px;width:100%;">
                            <span class="form_label" style="width:80px;">开始时间：</span>
                            <input type="text" class="form-control date-picker" id="outstorage_list_createTime" name="createTime" value="" style="width: calc(100% - 85px);" placeholder="开始时间" />
                        </label>
                    </li>
                    <li class="field-group field-fluid3">
                        <label class="inline" for="outstorage_list_endTime" style="margin-right:20px;width:100%;">
                            <span class="form_label" style="width:80px;">结束时间：</span>
                            <input type="text" class="form-control date-picker" id="outstorage_list_endTime" name="endTime" value="" style="width: calc(100% - 85px);" placeholder="结束时间" />
                        </label>
                    </li>


                    <li class="field-group-top field-group field-fluid3">
                        <label class="inline" for="outstorage__list_factcode" style="margin-right:20px;width: 100%;">
                            <span class="form_label" style="width:80px;">发货组织：</span>
                            <input id="outstorage__list_factcode" name="factoryarea" type="text" style="width: calc(100% - 85px);" placeholder="厂区">
                        </label>
                    </li>
                    <li class="field-group field-fluid3">
                        <label class="inline" for="outstorage_list_orderno" style="margin-right:20px;width:100%;">
                            <span class="form_label" style="width:80px;">订单号：</span>
                            <input type="text" id="outstorage_list_orderno"  name="orderno" value="" style="width: calc(100% - 85px);" placeholder="订单号" />
                        </label>
                    </li>
                    <li class="field-group field-fluid3">
                        <label class="inline" for="outstorage_list_orderline" style="margin-right:20px;width:100%;">
                            <span class="form_label" style="width:80px;">订单行号：</span>
                            <input type="text" id="outstorage_list_orderline"  name="orderline" value="" style="width: calc(100% - 85px);" placeholder="订单行号" />
                        </label>
                    </li>
                    <li class="field-group-top field-group field-fluid3">
                        <label class="inline" for="outstorage_list_contractno" style="margin-right:20px;width:100%;">
                            <span class="form_label" style="width:80px;">合同号：</span>
                            <input type="text" id="outstorage_list_contractno"  name="contractno" value="" style="width: calc(100% - 85px);" placeholder="合同号" />
                        </label>
                    </li>
                </ul>
                <div class="field-button" style="">
                    <div class="btn btn-info" onclick="outstorage_list_queryOk();">
                        <i class="ace-icon fa fa-check bigger-110"></i>查询
                    </div>
                    <div class="btn" onclick="outstorage_list_reset();"><i class="ace-icon icon-remove"></i>重置</div>
                    <a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>
                </div>
            </form>
        </div>
    </c:if>
    <div id="outstorage_list_fixed_tool_div" class="fixed_tool_div">
        <div id="outstorage_list_toolbar_" style="float:left;overflow:hidden;"></div>
        <input type="text" class="date-picker" id="outstorage_list_time" name="time" style="width: calc(33% - 40px);" placeholder="选择出库时间" />
    </div>
    <table id="outstorage_list_grid-table" style="width:100%;height:100%;"></table>
    <div id="outstorage_list_grid-pager"></div>
</div>

<script type="text/javascript" src="<%=path%>/static/js/techbloom/outstorage/outstorage/outstorage_list.js"></script>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var outstorage_list_oriData;
    var outstorage_list_grid;
    var outstorage_list_dynamicDefalutValue="17e870237a2d470fb170daeafe6292e1"; //列表码
    var outstorage_list_exportExcelIndex;
    var printAjaxStatus=1;

    $("input").keypress(function (e) {
        if (e.which == 13) {
            outstorage_list_queryOk();
        }
    });

    $("#outstorage_list_time").css("border-width","3");

    //时间控件
    $(".date-picker").datetimepicker({
        format: "YYYY-MM-DD HH:mm:ss" ,
        autoclose : true,
        todayHighlight : true
    });

    $(function  (){
        $(".toggle_tools").click();
    });

    $("#outstorage_list_toolbar_").iToolBar({
        id: "outstorage_list_tb_01",
        items: [
            {label: "编辑",hidden:"${operationCode.webEdit}"=="1", onclick: outstorage_list_openEditPage, iconClass:'glyphicon glyphicon-pencil'},
		    {label: "执行",hidden:"${operationCode.webExecute}"=="1", onclick:outstorage_list_execute, iconClass:'icon-tag'},
		    {label: "查看",hidden:"${operationCode.webInfo}"=="1", onclick:outstorage_list_openInfoPage, iconClass:'icon-search'},
		    {label: "导出",hidden:"${operationCode.webExport}"=="1", onclick:function(){outstorage_list_toExcel();},iconClass:'icon-share'},
            {label: "出库单补打",hidden:"${operationCode.webBuPrint}"=="1", onclick:function(){outstorage_list_print();},iconClass:'icon-print'}
    	]
    });

    var outstorage_list_queryForm_Data = iTsai.form.serialize($('#outstorage_list_queryForm'));

    outstorage_list_grid = jQuery("#outstorage_list_grid-table").jqGrid({
        url : context_path + '/outsrorageCon/storageListData.do',
        datatype : "json",
        colNames : ["主键","状态","发货组织","发货单号","打印时间","合同号","订单号","订单行号","运输公司","收货单位","销售经理"],
        colModel : [
            {name : "id",index : "id",hidden:true},
            {name : "storageState",index : "storageState",width : 30,formatter:function(cellvalue,option,rowObject){
	            	if(cellvalue!=null){
	                    /*if(cellvalue=='2') {
	                        return "<span style='color:blue;font-weight:bold;'>出库中</span>";
	                    }else if(cellvalue=='1') {
	                        return "<span style='color:black;font-weight:bold;'>未出库</span>";
	                    }else if(cellvalue=='3'){
	                        return "<span style='color:green;font-weight:bold;'>出库完成</span>";
	                    }*/
	                    //1未出库2出库中3出库完成4预出库
                        if(cellvalue.indexOf("2")>-1) {
                            return "<span style='color:blue;font-weight:bold;'>出库中</span>";//出库中
                        }else if(cellvalue.indexOf("4")>-1){//134
                            return "<span style='color:blue;font-weight:bold;'>预出库</span>";//预出库
                        }else if(cellvalue.indexOf("1")>-1 && cellvalue.indexOf("3")>-1){//13
                            return "<span style='color:black;font-weight:bold;'>出库中</span>";
                        }else if(cellvalue.indexOf("3")>-1){//全是3
                            return "<span style='color:green;font-weight:bold;'>出库完成</span>";//出库完成
                        }else{
                            return "<span style='color:red;font-weight:bold;'>未完成</span>";
                        }
	            	}else{
	            		return "<span style='color:black;font-weight:bold;'>无数据</span>";
	            	}
                }
            },
            {name : "name",index : "name",width : 45},
            {name : "shipNo",index : "shipNo",width : 60},
            {name : "printTime",index : "printTime",width : 60},
            {name : "contractNo",index : "CONTRACT_NO",width : 85},
            {name : "orderNo",index : "orderNo",width : 60},
            {name : "orderLine",index : "orderLine",width : 30},
            {name : "transportName",index : "transportName",width : 40},
            {name : "customer",index : "customer",width : 120},
            {name : "salesName",index : "salesName",width :40}
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : '#outstorage_list_grid-pager',
        sortname : 't1.id',
        sortorder : "desc",
        altRows: false,
        viewrecords : true,
        autowidth:true,
        multiselect:true,
        multiboxonly: true,
        beforeRequest:function (){
            dynamicGetColumns(outstorage_list_dynamicDefalutValue,"outstorage_list_grid-table",$(window).width()-$("#sidebar").width() -7);
            //重新加载列属性
        },
        loadComplete : function(data){
            var table = this;
            setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            outstorage_list_oriData = data;
            $(window).triggerHandler('resize.jqGrid');
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
    });

    //在分页工具栏中添加按钮
    jQuery("#outstorage_list_grid-table").navGrid("#outstorage_list_grid-pager",
    {edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd('#outstorage_list_grid-pager',{
        caption:"",
        buttonicon:"ace-icon fa fa-refresh green",
        onClickButton: function(){
            $("#outstorage_list_grid-table").jqGrid("setGridParam",
                {
                    postData: {queryJsonString:""} //发送数据
                }
            ).trigger("reloadGrid");
        }
    }).navButtonAdd("#outstorage_list_grid-pager",{
        caption: "",
        buttonicon:"fa icon-cogs",
        onClickButton : function (){
            jQuery("#outstorage_list_grid-table").jqGrid("columnChooser",{
                done: function(perm, cols){
                    $("#outstorage_list_grid-table").jqGrid("setGridWidth", $("#outstorage_list_grid-div").width());
                    outstorage_list_exportExcelIndex = perm;
                }
            });
        }
    });

    $(window).on("resize.jqGrid", function () {
        $("#outstorage_list_grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);

        $("#outstorage_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-10-
        $("#outstorage_list_yy").outerHeight(true)-$("#outstorage_list_fixed_tool_div").outerHeight(true)-
        $("#outstorage_list_grid-pager").outerHeight(true)-$("#gview_outstorage_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
    });


    /**查询*/
    function outstorage_list_queryOk(){
        //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
        outstorage_list_queryInstoreListByParam(iTsai.form.serialize($('#outstorage_list_queryForm')));
    }

    /**重置*/
    function outstorage_list_reset(){
        $("#outstorage_list_queryForm #outstorage__list_factcode").select2("val","");
        iTsai.form.deserialize($("#outstorage_list_queryForm"), outstorage_list_queryForm_Data);
        outstorage_list_queryInstoreListByParam(outstorage_list_queryForm_Data);
    }

    //厂区
    $("#outstorage_list_queryForm #outstorage__list_factcode").select2({
        placeholder: "选择厂区",
        minimumInputLength:0,   //至少输入n个字符，才去加载数据
        allowClear: true,  //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches:"没有结果",
        formatSearching:"搜索中...",
        formatAjaxError:"加载出错啦！",
        ajax : {
            url: context_path+"/factoryArea/getFactoryList",
            type:"POST",
            dataType : 'json',
            delay : 250,
            data: function (term,pageNo) {     //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term,    //联动查询的字符
                    pageSize: 15,    //一次性加载的数据条数
                    pageNo:pageNo    //页码
                }
            },
            results: function (data,pageNo) {
                var res = data.result;
                if(res.length>0){   //如果没有查询到数据，将会返回空串
                    var more = (pageNo*15)<data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results:res,more:more
                    };
                }else{
                    return {
                        results:{}
                    };
                }
            },
            cache : true
        }
    });
</script>