<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
%>
<div class="row" style="margin:0;padding:0;">
    <div class="control-group span6" style="display: inline">
        <label class="control-label" >车牌号：</label>
        <div class="controls">
            <div class="required" >
                <select id="carno" name="carno" >
                    <option value="--请选择--"></option>
                    <c:forEach  items="${carlist}" var="carList">
                        <option value="${carList.CAR}">${carList.CAR}</option>
                    </c:forEach>
                </select>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
     function getcarno(){
         var  myselect=document.getElementById("carno");
         var index=myselect.selectedIndex ;             // selectedIndex代表的是你所选中项的index
         return  myselect.options[index].value;
     }
</script>
