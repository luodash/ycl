<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
	String path = request.getContextPath();
%>
<script type="text/javascript">
	var context_path = '<%=path%>';
</script>
<%--<body style="overflow:hidden;">--%>
<%--<object  id="LODOP_OB" classid="clsid:2105C259-1E0C-4534-8141-A753534CB4CA" width=0 height=0>
	<embed id="LODOP_EM" type="application/x-print-lodop" width=0 height=0></embed>
</object>--%>
<div id="print_list_grid-div">
    <form id="print_list_hiddenQueryForm" style="display:none;">
        <input name="shipNo" value=""/>
        <input name="qaCode" value=""/>
    </form>
	<c:if test="${operationCode.webSearch==1}">
	<div class="query_box" id="print_list_yy" title="查询选项">
		<form id="print_queryForm" style="max-width:100%;">
			<ul class="form-elements">
				<li class="field-group field-fluid3">
					<label class="inline"  style="margin-right:20px;width: 100%;">
						<span class="form_label" style="width:80px;">质保号:</span>
						<input id="print_list_qaCode" name="qaCode" type="text" style="width: calc(100% - 85px);" placeholder="质保号">
					</label>
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="print_list_shipNo" style="margin-right:20px;width: 100%;">
						<span class="form_label" style="width:80px;">发货单编号:</span>
						<input id="print_list_shipNo" name="shipNo" type="text" style="width: calc(100% - 85px);" placeholder="发货单编号">
					</label>
				</li>
			</ul>
			<div class="field-button" style="">
				<div class="btn btn-info" onclick="print_list_queryOk();">
					<i class="ace-icon fa fa-check bigger-110"></i>查询
				</div>
				<div class="btn" onclick="print_list_reset();"><i class="ace-icon icon-remove"></i>重置</div>
			</div>
		</form>
	</div>
	</c:if>
	<div id="print_list_fixed_tool_div" class="fixed_tool_div">
		<div id="print_list_toolbar_" style="float:left;overflow:hidden;"></div>
		<%--<input type="text" class="date-picker" id="print_list_time" name="time"  placeholder="请选择出库时间" />--%>
	</div>
	<table id="print_list_grid-table" style="width:100%;height:100%;" ></table>
	<div id="print_list_grid-pager"></div>
</div>


<%--
<div id="body1" style="display:none;">
	<div id="div2">
		<TABLE border=1 cellSpacing=0 cellPadding=1 width="100%" style="border-collapse:collapse;page-break-after:always;"
		bordercolor="#333333">
			<thead>
			<TR>
				<TD width="10%">
					<DIV align=center><b>规格型号</b></DIV>
				</TD>
				<TD width="5%">
					<DIV align=center><b>数量</b></DIV>
				</TD>
				<TD width="5%">
					<DIV align=center><b>单位</b></DIV>
				</TD>
				<TD width="5%">
					<DIV align=center><b>颜色</b></DIV>
				</TD>
				<TD width="5%">
					<DIV align=center><b>库位</b></DIV>
				</TD>
				<TD width="8%">
					<DIV align=center><b>盘号</b></DIV>
				</TD>
				<TD width="8%">
					<DIV align=center><b>段号</b></DIV>
				</TD>
				<TD width="8%">
					<DIV align=center><b>出厂编号</b></DIV>
				</TD>
				<TD width="5%">
					<DIV align=center><b>重量</b></DIV>
				</TD>
				<TD width="6%">
					<DIV align=center><b>定制</b></DIV>
				</TD>
				<TD width="10%">
					<DIV align=center><b>备注</b></DIV>
				</TD>
			</TR>
			</thead>
			<TBODY id="print_tablecontent">

			</TBODY>
		</TABLE>
	</div>
</div>
</body>--%>

<script type="text/javascript" src="<%=path%>/static/js/techbloom/LodopFuncs.js"></script>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
	let print_oriData;
	let print_grid;

	$(".date-picker").css("border-width","3");

	$("input").keypress(function (e) {
		if (e.which === 13) {
			print_list_queryOk();
		}
	});

	//时间控件
	$(".date-picker").datetimepicker({
		format: "YYYY-MM-DD HH:mm:ss" ,
		autoclose : true,
		todayHighlight : true
	});

    $(function  (){
        $(".toggle_tools").click();
    });

    $("#print_list_toolbar_").iToolBar({
        id: "print_list_tb_01",
        items: [
            {label: "拣货下架",hidden:"${operationCode.webConfirmOutstorage}"==="1", onclick:outConfirm, iconClass:'icon-ok'},
			{label: "撤销下架",hidden:"${operationCode.webCancelOutstorage}"==="1", onclick:outCancel, iconClass:'icon-remove'}
            /*{label: "出库单补打",hidden:"${operationCode.webRePrint}"=="1", onclick:printOutDan2, iconClass:'icon-print'}*/
        ]
    });

	var print_list_queryForm_data = iTsai.form.serialize($("#print_queryForm"));

	print_grid = jQuery("#print_list_grid-table").jqGrid({
        url : context_path + "/outsrorageCon/outDanListData.do",
        datatype : "json",
        colNames : [ "主键","出库状态","质保号","发货单号","运输单号","物料编码","盘号","批次号","米数","单位","库位","拣货日期","下架日期","下架人",
			"出库日期","出库人"],
        colModel : [
            {name : "id",index : "id",hidden:true,key:true},
			{name : "state",index : "state",width : 80,formatter:function(cellvalue,option,rowObject){
					if(cellvalue===1){
						return "<span style='color:red;font-weight:bold;'>未出库</span>";
					}else if (cellvalue===2){
						return "<span style='color:orange;font-weight:bold;'>拣货中</span>";
					}else if (cellvalue===3){
						return "<span style='color:green;font-weight:bold;'>出库完成</span>";
					}else if (cellvalue===4){
						return "<span style='color:orchid;font-weight:bold;'>预初库</span>";
					}
				}
			},
            {name : "qaCode",index : "qacode",width : 160},
            {name : "shipNo",index : "ship_no",width : 160},
            {name : "transportNo",index : "transportno",width : 190},
			{name : "materialCode",index : "materialcode",width : 160},
            {name : "dishcode",index : "dishcode",width : 160},
            {name : "batchNo",index : "batch_no",width : 160},
            {name : "meter",index : "meter",width : 60},
            {name : "unit",index : "unit",width : 40},
			{name : "shelfCode",index : "shelfcode",width : 80},
			{name : "startpicktime",index : "STARTPICKTIME",width : 140},
			{name : "startStorageTime",index : "STARTSTORAGETIME",width : 140},
			{name : "startstorageName",index : "STARTSTORAGEID",width : 80},
			{name : "outStorageTime",index : "OUTSTORAGETIME",width : 140},
			{name : "pickManName",index : "OUTSTORAGEID",width : 80}
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : "#print_list_grid-pager",
		sortname : "t2.id",
		sortorder : "asc",
        altRows: true,
        viewrecords : true,
        hidegrid:false,
        autowidth:true,
        multiselect:true,
        multiboxonly: true,
		shrinkToFit:false,
		autoScroll: true,
        loadComplete : function(data) {
            var table = this;
            setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
			print_oriData = data;
			$("#print_list_grid-table").closest(".ui-jqgrid-bdiv").css({ 'overflow-x': 'auto' });
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
    });

    jQuery("#print_list_grid-table").navGrid("#print_list_grid-pager", {edit:false,add:false,del:false,search:false,refresh:false})
	.navButtonAdd("#print_list_grid-pager",{
        caption:"",
        buttonicon:"fa fa-refresh green",
        onClickButton: function(){
            $("#print_list_grid-table").jqGrid("setGridParam", {
				postData: {queryJsonString:""} //发送数据
			}).trigger("reloadGrid");
        }
    }).navButtonAdd("#print_list_grid-pager",{
        caption: "",
        buttonicon:"fa icon-cogs",
        onClickButton : function (){
            jQuery("#print_list_grid-table").jqGrid("columnChooser",{
                done: function(perm, cols){
                    // dynamicColumns(cols,materials_list_dynamicDefalutValue);
                    $("#print_list_grid-table").jqGrid("setGridWidth", $("#print_list_grid-div").width());
                }
            });
        }
    });

    $(window).on("resize.jqGrid", function () {
        $("#print_list_grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);

        $("#print_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-10-
		$("#print_list_yy").outerHeight(true)- $("#print_list_fixed_tool_div").outerHeight(true)-
		$("#print_list_grid-pager").outerHeight(true)- $("#gview_print_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
    });
    $(window).triggerHandler("resize.jqGrid");

    //var LODOP=getLodop(document.getElementById('LODOP_OB'),document.getElementById('LODOP_EM'));
    var LODOP=document.getElementById('LODOP_EM');

    //拣货下架
    function outConfirm(){
    	/*var outtime = $(".date-picker").val();
    	if (outtime == '' || outtime == null) {
			layer.alert("请选择出库时间！");
			return;
		}*/
        if(getGridCheckedNum("#print_list_grid-table","id") === 0){
            layer.alert("请至少选中一条记录！");
        }else{
            layer.confirm("确定下架吗？",function(){
                $.ajax({
                    type:"POST",
                    url:context_path + "/outsrorageCon/preConfirmOut?ids="+$('#print_list_grid-table').jqGrid('getGridParam','selarrrow'),
                    dataType:"json",
                    success:function(data){
                        iTsai.form.deserialize($("#print_list_hiddenQueryForm"), iTsai.form.serialize($("#print_queryForm")));
                        var queryParam = iTsai.form.serialize($("#print_list_hiddenQueryForm"));
                        var queryJsonString = JSON.stringify(queryParam);
                        if(data.result){
							layer.msg(data.msg, {icon: 1,time:2000});
                            $("#print_list_grid-table").jqGrid("setGridParam", {
								postData: {queryJsonString:queryJsonString} //发送数据
							}).trigger("reloadGrid");
                        }else{
							layer.msg(data.msg, {icon: 7,time:2000});
                        }
                    }
                })
            });
        }
    }

	//撤销下架
	function outCancel(){
		if(getGridCheckedNum("#print_list_grid-table","id") === 0){
			layer.alert("请至少选中一条记录！");
		}else{
			layer.confirm("确定撤销下架吗？",function(){
				$.ajax({
					type:"POST",
					url:context_path + "/outsrorageCon/preCancelOut?ids="+$('#print_list_grid-table').jqGrid('getGridParam','selarrrow'),
					dataType:"json",
					success:function(data){
						iTsai.form.deserialize($("#print_list_hiddenQueryForm"), iTsai.form.serialize($("#print_queryForm")));
						var queryParam = iTsai.form.serialize($("#print_list_hiddenQueryForm"));
						var queryJsonString = JSON.stringify(queryParam);
						if(data.result){
							layer.msg(data.msg, {icon: 1,time:2000});
							$("#print_list_grid-table").jqGrid("setGridParam", {
								postData: {queryJsonString:queryJsonString} //发送数据
							}).trigger("reloadGrid");
						}else{
							layer.msg(data.msg, {icon: 7,time:2000});
						}
					}
				})
			});
		}
	}

    //根据车牌号/运单号查询
	function print_list_queryOk(){
		var queryParam = iTsai.form.serialize($("#print_queryForm"));
		//执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
		print_list_queryByParam(queryParam);
	}

	function print_list_queryByParam(jsonParam) {
		iTsai.form.deserialize($("#print_list_hiddenQueryForm"), jsonParam);
		var queryParam = iTsai.form.serialize($("#print_list_hiddenQueryForm"));
		var queryJsonString = JSON.stringify(queryParam);
		$("#print_list_grid-table").jqGrid("setGridParam", {
			postData: {queryJsonString: queryJsonString}
		}).trigger("reloadGrid");
	}

    //打印发货单
    function printOutDan1() {
		var selectAmount = getGridCheckedNum("#print_list_grid-table");
		if(selectAmount===0){
			layer.msg("请选择一条记录！",{icon:2});
			return;
		}else if(selectAmount>1){
			layer.msg("只能选择一条记录！",{icon:8});
			return;
		}
		layer.load(2);

        var id = jQuery("#print_list_grid-table").jqGrid("getGridParam", "selrow");//出库明细表主键

        if(isprint==='1'){
			layer.msg("当前的运单已经打印过,不能反复打印，或点击补打再次打印！",{icon:8});
		}else{
            $.ajax({
                url: context_path + "/outsrorageCon/confirmPrint?id="+id,
                type: "POST",
                dataType: "JSON",
                success: function (data) {
                    CreatePrintPage1(data);
                },
                error:function(XMLHttpRequest){
                    alert(XMLHttpRequest.readyState);
                    alert("出错啦！！！");
                }
            });
		}
    }

    //补打
    function printOutDan2() {
        var outno = $("#print_list_shipNo").val();
        var id = jQuery("#print_list_grid-table").jqGrid("getGridParam", "selrow");
        var rowData = $("#print_list_grid-table").jqGrid('getRowData',id);
        var wayno = rowData.wayno;

        $.ajax({
            url: context_path + "/outsrorageCon/confirmPrint?outno="+outno+"&wayno="+wayno+"&type=2",
            type: "POST",
            dataType: "JSON",
            success: function (data) {
                CreatePrintPage2(data);
            },
            error:function(XMLHttpRequest){
                alert(XMLHttpRequest.readyState);
                alert("出错啦！！！");
            }
        });
    }

    function CreatePrintPage1(data) {
        var printList=data.printInfo;
        for(var i=0;i<printList.length;i++){
            var LODOP=getLodop();
            LODOP.PRINT_INIT("物流订单打印");
            LODOP.SET_PRINT_PAGESIZE(0,'240mm','140mm',"");//一开始用的是像素，后来都改成用mm为单位
            LODOP.ADD_PRINT_TEXT("3.39mm","64.4mm","77.55mm","9.6mm","远东电缆有限公司送货单");
            LODOP.SET_PRINT_STYLEA(0,"ItemType",1);
            var strStyle="<style> table,td,th {border-width: 1px;border-style: solid;border-collapse: collapse}</style>";
            var detailList=printList[i].detail;
			var html = '';
            for(var k=0;k<detailList.length;k++){
                var str0 = (detailList[k].MODEL === "undefinded"||detailList[k].MODEL == null)?"":detailList[k].MODEL;
                var str1 = (detailList[k].METER === "undefinded"||detailList[k].METER == null)?"":detailList[k].METER;
                var str2 = (detailList[k].UNIT === "undefinded"||detailList[k].UNIT == null)?"":detailList[k].UNIT;
                var str3 = (detailList[k].COLOUR === "undefinded"||detailList[k].COLOUR == null)?"":detailList[k].COLOUR;
                var str4 = (detailList[k].SHELFCODE === "undefinded"||detailList[k].SHELFCODE == null)?"":detailList[k].SHELFCODE;
                var str5 = (detailList[k].DISHCODE === "undefinded"||detailList[k].DISHCODE == null)?"":detailList[k].DISHCODE;
                var str6 = (detailList[k].SEGMENTNO === "undefinded"||detailList[k].SEGMENTNO == null)?"":detailList[k].SEGMENTNO;
                var str7 = (detailList[k].QACODE === "undefinded"||detailList[k].QACODE == null)?"":detailList[k].QACODE;
                var str8 = (detailList[k].WEIGHT === "undefinded"||detailList[k].WEIGHT == null)?"":detailList[k].WEIGHT;
                var str9 = (detailList[k].USERNAME === "undefinded"||detailList[k].USERNAME == null)?"":detailList[k].USERNAME;
                html += '<tr>' +
                    //规格型号
                    '<td>'+str0+'</td>' +
                    //数量
                    '<td>'+str1+'</td>' +
                    //单位
                    '<td>'+str2+'</td>' +
                    //颜色
                    '<td>'+str3+'</td>' +
                    //库位
                    '<td>'+str4+'</td>' +
                    //盘号
                    '<td>'+str5+' </td>' +
                    //段号
                    '<td>'+str6+'</td>' +
                    //出厂编号(质保号)
                    '<td>'+str7+'</td>' +
                    //重量
                    '<td>'+str8+'</td>' +
                    //定制
                    '<td>'+str9+'</td>' +
                    //备注
                    '<td></td></tr>'
            }
            $("#print_tablecontent").empty();
            $("#print_tablecontent").append(html);
            LODOP.ADD_PRINT_TEXT("15.29mm","12.54mm","25.13mm","5.37mm","合同号/订单号:");
            LODOP.SET_PRINT_STYLEA(0,"ItemType",1);
            var str = printList[i].CONTRACT_NO;
            if(str ==null || str === 'undefinded'){
				str = ''
			}
            var str1 = printList[i].ORDERNO;
            if(str1 ==null || str1 === 'undefinded'){
                str1 = ''
            }
            LODOP.ADD_PRINT_TEXT("15.29mm","35.18mm","45.56mm","5.37mm",str+"/"+str1);
            LODOP.SET_PRINT_STYLEA(0,"ItemType",1);

            LODOP.ADD_PRINT_TEXT("15.29mm","81.37mm","20.13mm","5.37mm","运输方:");
            LODOP.SET_PRINT_STYLEA(0,"ItemType",1);
            var str = printList[i].TRANSPORTNAME;
            if(str ==null || str === 'undefinded'){
                str = ''
            }
            LODOP.ADD_PRINT_TEXT("15.29mm","94.66mm","20.13mm","5.37mm",str);
            LODOP.SET_PRINT_STYLEA(0,"ItemType",1);

            var str = printList[i].SHIP_NO;
            if(str ==null || str === 'undefinded'){
                str = ''
            }
            LODOP.ADD_PRINT_BARCODE("6.03mm","172.28mm","41.54mm","30.37mm","QRCode","http://58.215.20.86:8088/yuandong/w/l/u.action?id="+str);
            LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
            LODOP.SET_PRINT_STYLEA(0, "FontSize", 32);
            LODOP.SET_PRINT_STYLEA(0, "Bold", 1);

            LODOP.ADD_PRINT_TEXT("21.11mm","12.54mm","25.13mm","5.37mm","收货单位(人):");
            LODOP.SET_PRINT_STYLEA(0,"ItemType",1);
            var str = printList[i].PICKUP;
            if(str ==null || str === 'undefinded'){
                str = ''
            }
            LODOP.ADD_PRINT_TEXT("21.11mm","35.98mm","13mm","5.37mm",str);
            LODOP.SET_PRINT_STYLEA(0,"ItemType",1);

            LODOP.ADD_PRINT_TEXT("21.11mm","83mm","20.13mm","5.37mm","发货库房:");
            LODOP.SET_PRINT_STYLEA(0,"ItemType",1);
            var str = printList[i].DELIVERYWAREHOUSE;
            if(str ==null || str === 'undefinded'){
                str = ''
            }
            LODOP.ADD_PRINT_TEXT("21.11mm","100mm","28.13mm","5.37mm",str);
            LODOP.SET_PRINT_STYLEA(0,"ItemType",1);

            LODOP.ADD_PRINT_TEXT("26.41mm","12.81mm","25.13mm","5.37mm","承办单位(人):");
            LODOP.SET_PRINT_STYLEA(0,"ItemType",1);
            var str = printList[i].UNDERTAKE;
            if(str ==null || str === 'undefinded'){
                str = ''
            }
            LODOP.ADD_PRINT_TEXT("26.41mm","35.68mm","55.99mm","5.37mm",str);
            LODOP.SET_PRINT_STYLEA(0,"ItemType",1);

            LODOP.ADD_PRINT_TEXT("26.41mm","83.81mm","20.13mm","5.37mm","出库日期:");
            LODOP.SET_PRINT_STYLEA(0,"ItemType",1);
            var str = printList[i].OUTDATE1;
            if(str ==null || str === 'undefinded'){
                str = ''
            }
            LODOP.ADD_PRINT_TEXT("25.41mm","100.68mm","55.99mm","5.37mm",str);
            LODOP.SET_PRINT_STYLEA(0,"ItemType",1);

            LODOP.ADD_PRINT_TEXT("32.41mm","12.81mm","20.13mm","5.37mm","片区要求:");
            LODOP.SET_PRINT_STYLEA(0,"ItemType",1);

            LODOP.ADD_PRINT_HTM("31mm","117mm","21mm","5mm","<span tdata='pageNO'>第#页</span>/<span tdata='pageCount'>共#页</span>");
			LODOP.SET_PRINT_STYLEA(0,"ItemType",1);

            LODOP.ADD_PRINT_HTM("31mm","146mm","20mm","5mm","单据号:");
            LODOP.SET_PRINT_STYLEA(0,"ItemType",1);

            var str = printList[i].SHIP_NO;
            if(str ==null || str === 'undefinded'){
                str = ''
            }
            LODOP.ADD_PRINT_HTM("31mm","161mm","43mm","5mm",str);
            LODOP.SET_PRINT_STYLEA(0,"ItemType",1);


            LODOP.ADD_PRINT_TABLE("39mm","12.5mm","195mm","83mm",strStyle+document.getElementById("div2").innerHTML);
            LODOP.SET_PRINT_STYLEA(0,"Vorient",3);

            LODOP.ADD_PRINT_TEXT("94mm","16.54mm","23.13mm","5.37mm","开票:");
            LODOP.SET_PRINT_STYLEA(0,"ItemType",1);

            var str = printList[i].INVOICE;
            if(str ==null || str === 'undefinded'){
                str = ''
            }
            LODOP.ADD_PRINT_TEXT("94mm","26.54mm","20.13mm","5.37mm",str);
            LODOP.SET_PRINT_STYLEA(0,"ItemType",1);

            LODOP.ADD_PRINT_TEXT("94mm","47.54mm","23.13mm","5.37mm","发货:");
            LODOP.SET_PRINT_STYLEA(0,"ItemType",1);

            var str = printList[i].DELIVERY;
            if(str ==null || str === 'undefinded'){
                str = ''
            }
            LODOP.ADD_PRINT_TEXT("94mm","60.18mm","20.56mm","5.37mm",str);
            LODOP.SET_PRINT_STYLEA(0,"ItemType",1);

            LODOP.ADD_PRINT_TEXT("94mm","79.54mm","23.13mm","5.37mm","承运:");
            LODOP.SET_PRINT_STYLEA(0,"ItemType",1);
            var str = printList[i].SHIPMENT;
            if(str ==null || str === 'undefinded'){
                str = ''
            }
            LODOP.ADD_PRINT_TEXT("94mm","91.18mm","20.56mm","5.37mm",str);
            LODOP.SET_PRINT_STYLEA(0,"ItemType",1);

            LODOP.ADD_PRINT_TEXT("94mm","113.54mm","23.13mm","5.37mm","门卫:");
            LODOP.SET_PRINT_STYLEA(0,"ItemType",1);
            LODOP.ADD_PRINT_TEXT("94mm","124.18mm","20.56mm","5.37mm","");
            LODOP.SET_PRINT_STYLEA(0,"ItemType",1);

            LODOP.ADD_PRINT_TEXT("94mm","146.54mm","23.13mm","5.37mm","经办:");
            LODOP.SET_PRINT_STYLEA(0,"ItemType",1);
            var str = printList[i].HANDLE;
            if(str ==null || str === 'undefinded'){
                str = ''
            }
            LODOP.ADD_PRINT_TEXT("94mm","155.18mm","20.56mm","5.37mm",str);
            LODOP.SET_PRINT_STYLEA(0,"ItemType",1);

            LODOP.ADD_PRINT_TEXT("94mm","178.54mm","23.13mm","5.37mm","收货:");
            LODOP.SET_PRINT_STYLEA(0,"ItemType",1);

            var str = printList[i].PICKUP;
            if(str ==null || str === 'undefinded'){
                str = ''
            }
            LODOP.ADD_PRINT_TEXT("94mm","188.18mm","20.56mm","5.37mm",str);
            LODOP.SET_PRINT_STYLEA(0,"ItemType",1);

            LODOP.ADD_PRINT_TEXT("100.29mm","16.54mm","26.13mm","5.37mm","本单货物总重:");
            LODOP.SET_PRINT_STYLEA(0,"ItemType",1);

            var str = printList[i].CONTRACT_NO;
            if(str ==null || str === 'undefinded'){
                str = ''
            }
            LODOP.ADD_PRINT_TEXT("100.29mm","39.18mm","45.56mm","5.37mm",str);
            LODOP.SET_PRINT_STYLEA(0,"ItemType",1);

            LODOP.ADD_PRINT_TEXT("100.29mm","76.54mm","20.13mm","5.37mm","接货人:");
            LODOP.SET_PRINT_STYLEA(0,"ItemType",1);

            var str = printList[i].PICKUP;
            if(str ==null || str === 'undefinded'){
                str = ''
            }
            LODOP.ADD_PRINT_TEXT("100.29mm","91.18mm","45.56mm","5.37mm",str);
            LODOP.SET_PRINT_STYLEA(0,"ItemType",1);

            LODOP.ADD_PRINT_TEXT("100.29mm","132.54mm","20.13mm","5.37mm","联系电话:");
            LODOP.SET_PRINT_STYLEA(0,"ItemType",1);

            var str = printList[i].PICKUPTEL;
            if(str ==null || str === 'undefinded'){
                str = ''
            }
            LODOP.ADD_PRINT_TEXT("100.29mm","148.18mm","45.56mm","5.37mm",str);
            LODOP.SET_PRINT_STYLEA(0,"ItemType",1);

            LODOP.ADD_PRINT_TEXT("106.29mm","16.54mm","20.13mm","5.37mm","送货地址:");
            LODOP.SET_PRINT_STYLEA(0,"ItemType",1);

            var str = printList[i].DELIVERYADDRESS;
            if(str ==null || str == 'undefinded'){
                str = ''
            }
            LODOP.ADD_PRINT_TEXT("106.29mm","36.18mm","155.56mm","5.37mm",str);
            LODOP.SET_PRINT_STYLEA(0,"ItemType",1);

            LODOP.ADD_PRINT_TEXT("112.29mm","16.54mm","20.13mm","5.37mm","提示:");
            LODOP.SET_PRINT_STYLEA(0,"ItemType",1);
            var str = printList[i].UNDERTAKE;
            if(str ==null || str === 'undefinded'){
                str = ''
            }
            var str1 = printList[i].UNDERTAKETEL;
            if(str1 ==null || str1 === 'undefinded'){
                str1 = ''
            }
            LODOP.ADD_PRINT_TEXT("112.29mm","29.18mm","155.56mm","5.37mm","请物流公司在卸车前联系承办单位(人):"+str+"   电话:"+str1+
					"，确认无误后方可卸车。");
            LODOP.SET_PRINT_STYLEA(0,"ItemType",1);
            //定制
            var str = printList[i].username;
            if(str ==null || str === 'undefinded'){
                str = ''
            }
            LODOP.ADD_PRINT_TEXT("112.29mm","181.54mm","20.13mm","5.37mm",str);
            LODOP.SET_PRINT_STYLEA(0,"ItemType",1);

            LODOP.ADD_PRINT_TEXT("120.29mm","16.54mm","195.13mm","5.37mm",
					"友情提醒:依据《道路交通安全法实施条例》规定:不得连续驾驶机动车超4小时未停车休息或停车休息时间少于20分钟，请勿疲劳驾驶。");
            LODOP.SET_PRINT_STYLEA(0,"ItemType",1);
            LODOP.ADD_PRINT_TEXT("126.29mm","132.54mm","75.13mm","5.37mm","买受人签收后7天内未提出书面异议，视为验收合格");
            LODOP.SET_PRINT_STYLEA(0,"ItemType",1);

            // $("#print_tablecontent").empty();
            LODOP.PRINT_SETUP();
            //PREVIEW
        }
    };


    function getByteLen(val) {
        var len = 0;
        for (var i = 0; i < val.length; i++) {
            if (val[i].match(/[^\x00-\xff]/ig) != null) //全角
                len += 2;
            else
                len += 1;
        }
        return len;
    }

	//重置
	function print_list_reset(){
		$("#print_queryForm #print_list_qaCode").val("");
		$("#print_queryForm #print_list_shipNo").val("");
		iTsai.form.deserialize($("#print_list_queryForm"),print_list_queryForm_data);
		print_list_queryByParam(print_list_queryForm_data);
	}

</script>