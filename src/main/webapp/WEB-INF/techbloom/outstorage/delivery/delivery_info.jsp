<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
%>
<div id="delivery_info_page" class="row-fluid" style="height: inherit;margin:0px">
    <form action="" class="form-horizontal" id="delivery_info_baseInfor" name="baseInfor" method="post" target="_ifr" >
        <input type="hidden" id="delivery_info_id" name="id" value="${info.id }">
        <div class="row" style="margin:0;padding:0;">
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="delivery_info_licensePlate" >车牌号：</label>
                <div class="controls">
                    <div class="required" >
                        <input type="text" id="delivery_info_licensePlate" class="span10" name="licensePlate" value="${info.licensePlate }"  placeholder="车牌号" />
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div id="delivery_info_grid-div-c" style="width:100%;margin:0px auto;">
        <div id="delivery_info_fixed_tool_div" class="fixed_tool_div detailToolBar">
            <div id="delivery_info___toolbar__-c" style="float:left;overflow:hidden;"></div>
        </div>
        <table id="delivery_info_grid-table-c" style="width:100%;height:100%;"></table>
        <div id="delivery_info_grid-pager-c"></div>
    </div>
</div>
<iframe src="about:blank" name="_ifr" height="0" class="hidden"></iframe>
<script type="text/javascript" src="<%=path%>/static/js/techbloom/outstorage/delivery/delivery_info.js"></script>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriData;      //表格数据
    var _grid;        //表格对象
    var selectParam = "";  //存放之前的查询条件

    $('[data-rel=tooltip]').tooltip();

    //初始化表格
    _grid =  $("#delivery_info_grid-table-c").jqGrid({
        url : context_path + "/deleiveryCon/detailList.do?id="+$("#delivery_info_id").val(),
        datatype : "json",
        colNames : [ "详情主键","质保单号","物料编码","发货单号","米数"],
        colModel : [
            {name : "id",index : "id",width : 20,hidden:true},
            {name : "qaCode",index:"qaCode",width : 20},
            {name : "materialCode",index:"materialCode",width : 20},
            {name : "shipNo",index:"ship_No",width : 20},
            {name : "meter",index:"meter",width : 20},
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : "#delivery_info_grid-pager-c",
        sortname : "id",
        sortorder : "asc",
        altRows: true,
        viewrecords : true,
        autowidth:true,
        multiselect:true,
        multiboxonly: true,
        loadComplete : function(data){
            var table = this;
            setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            oriData = data;
            $(window).triggerHandler("resize.jqGrid");
        },
        cellEdit: true,
        cellsubmit : "clientArray",
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据",
    });
    //在分页工具栏中添加按钮
    $("#delivery_info_grid-table-c").navGrid("#delivery_info_grid-pager-c",{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#delivery_info_grid-pager-c",{
            caption:"",
            buttonicon:"ace-icon fa fa-refresh green",
            onClickButton: function(){
                $("#delivery_info_grid-table-c").jqGrid("setGridParam",
                    {
                        url:context_path + "/deleiveryCon/detailList.do?id="+$("#delivery_info_id").val(),
                    }
                ).trigger("reloadGrid");
            }
        });

    $(window).on("resize.jqGrid", function () {
        $("#delivery_info_grid-table-c").jqGrid("setGridWidth", $("#delivery_info_grid-div-c").width() - 3 );
        var height = $(".layui-layer-title",_grid.parents(".layui-layer")).height()+
                     $("#delivery_info_baseInfor").outerHeight(true)+$("#delivery_info_materialDiv").outerHeight(true)+
                     $("#delivery_info_grid-pager-c").outerHeight(true)+$("#delivery_info_fixed_tool_div.fixed_tool_div.detailToolBar").outerHeight(true)+
                     $("#gview_delivery_info_grid-table-c .ui-jqgrid-hbox").outerHeight(true);
                     $("#delivery_info_grid-table-c").jqGrid("setGridHeight",_grid.parents(".layui-layer").height()-height);
    });
    $(window).triggerHandler("resize.jqGrid");

</script>
