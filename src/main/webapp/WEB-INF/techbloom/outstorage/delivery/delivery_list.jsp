<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<div id="delivery_list_grid-div">
    <!-- 隐藏区域：存放查询条件 -->
    <form id="delivery_list_hiddenForm" action="<%=path%>/deleiveryCon/materialExcel" method="POST" style="display:none;">
        <input id="delivery_list_ids" name="ids" value=""/>
        <input id="delivery_list_queryShipNo" name="queryShipNo" value=""/>
    </form>
    <form id="delivery_list_hiddenQueryForm" style="display:none;">
        <input id="delivery_list_shipNo" name="shipNo" value=""/>
        <input id="delivery_list_createTime" name="createTime" value=""/>
        <input id="delivery_list_endTime" name="endTime" value=""/>
    </form>
    <c:if test="${operationCode.webSearch==1}">
    <div class="query_box" id="delivery_list_yy" title="查询选项">
        <form id="delivery_list_queryForm" style="max-width:100%;">
            <ul class="form-elements">
                <!-- <li class="field-group field-fluid3">
                    <label class="inline" for="delivery_list_shipNo" style="margin-right:20px;width: 100%;">
                        <span class="form_label" style="width:80px;">发货单编号：</span>
                        <input id="delivery_list_shipNo" name="shipNo" type="text" style="width: calc(100% - 85px);" placeholder="发货单编号">
                    </label>
                </li> -->
                <li class="field-group field-fluid3">
                    <label class="inline" for="delivery_list_createTime" style="margin-right:20px;width:100%;">
                        <span class="form_label" style="width:80px;">装车时间起：</span>
                        <input type="text" class="form-control date-picker" id="delivery_list_createTime" name="createTime" value="" style="width: calc(100% - 85px);" placeholder="开始时间" />
                    </label>
                </li>
                <li class="field-group field-fluid3">
                    <label class="inline" for="delivery_list_endTime" style="margin-right:20px;width:100%;">
                        <span class="form_label" style="width:80px;">装车时间止：</span>
                        <input type="text" class="form-control date-picker" id="delivery_list_endTime" name="endTime" value="" style="width: calc(100% - 85px);" placeholder="结束时间" />
                    </label>
                </li>
            </ul>
            <div class="field-button" style="">
                <div class="btn btn-info" onclick="delivery_list_queryOk();">
                    <i class="ace-icon fa fa-check bigger-110"></i>查询
                </div>
                <div class="btn" onclick="delivery_list_reset();"><i class="ace-icon icon-remove"></i>重置</div>
            </div>
        </form>
    </div>
    </c:if>
    <div id="delivery_list_fixed_tool_div" class="fixed_tool_div">
        <div id="delivery_list___toolbar__" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="delivery_list_grid-table" style="width:100%;height:100%;"></table>
    <div id="delivery_list_grid-pager"></div>
</div>

<script type="text/javascript" src="<%=path%>/static/js/techbloom/outstorage/delivery/delivery_list.js"></script>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriData;
    var _grid;
    var delivery_list_dynamicDefalutValue="17e870237a2d470fb170daeafe6292e1";//列表码

    //时间控件
    $(".date-picker").datetimepicker({format: "YYYY-MM-DD"});
    $(function  (){
        $(".toggle_tools").click();
    });
    $(function  (){
        $(".toggle_tools").click();
    });
    $("#delivery_list___toolbar__").iToolBar({
        id: "delivery_list___tb__01",
        items: [
            {label: "新增", hidden:${operationCode.webAdd}==1,onclick: delivery_list_openAddPage, iconClass:'glyphicon glyphicon-pencil'},
            {label: "编辑", hidden:${operationCode.webEdit}==1,onclick: delivery_list_openEditPage, iconClass:'glyphicon glyphicon-pencil'},
            {label: "查看", hidden:${operationCode.webInfo}==1, onclick:delivery_list_openInfoPage, iconClass:'glyphicon glyphicon-pencil'},
            {label: "删除", hidden:${operationCode.webDel}==1, onclick:function(){delivery_list_deleteByIds();}, iconClass:'glyphicon glyphicon-pencil'},
            {label: "导出", hidden:${operationCode.webExport}==1,onclick:function(){delivery_list_toExcel();},iconClass:'icon-share'}
        ]
        });

    _grid = jQuery("#delivery_list_grid-table").jqGrid({
        url : context_path + '/deleiveryCon/shipLoadingPageList.do',
        datatype : "json",
        colNames : ["主键","装车时间" ,"装车人","车牌号","状态"],
        colModel : [
            {name : "id",index : "id",hidden:true},
            {name : "shipLoadTime",index : "shipLoadTime",width : 60},
            {name : "shipLoadUserName",index : "shipLoadUserName",width :70},
            {name : "licensePlate",index : "licensePlate",width : 60},
            {name : 'state',index : 'state',width : 20,
                formatter:function(cellValu,option,rowObject){
                    if(cellValu=='1'){
                        return '<span style="color:red;font-weight:bold;">未提交</span>';
                    } if(cellValu=='2'){
                        return "已提交";
                    }
                }}
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : '#delivery_list_grid-pager',
        sortname : 'id',
        sortorder : "desc",
        altRows: false,
        viewrecords : true,
        autowidth:true,
        multiselect:true,
        multiboxonly: true,
        beforeRequest:function (){
            dynamicGetColumns(delivery_list_dynamicDefalutValue,"delivery_list_grid-table",$(window).width()-$("#sidebar").width() -7);
            //重新加载列属性
        },
        loadComplete : function(data){
            var table = this;
            setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            oriData = data;
            $(window).triggerHandler('resize.jqGrid');
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
    });
    //在分页工具栏中添加按钮
    jQuery("#delivery_list_grid-table").navGrid("#delivery_list_grid-pager",{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd('#delivery_list_grid-pager',{
        caption:"",
        buttonicon:"ace-icon fa fa-refresh green",
        onClickButton: function(){
            $("#delivery_list_grid-table").jqGrid("setGridParam",
                {
                    postData: {queryJsonString:""} //发送数据
                }
            ).trigger("reloadGrid");
        }
    }).navButtonAdd("#delivery_list_grid-pager",{
        caption: "",
        buttonicon:"fa icon-cogs",
        onClickButton : function (){
            jQuery("#delivery_list_grid-table").jqGrid("columnChooser",{
                done: function(perm, cols){
                    dynamicColumns(cols,delivery_list_dynamicDefalutValue);
                    $("#delivery_list_grid-table").jqGrid("setGridWidth", $("#delivery_list_grid-div").width()-3);
                }
            });
        }
    });
    /* $(window).on("resize.jqGrid", function () {
        $("#delivery_list_grid-table").jqGrid("setGridWidth", $("#delivery_list_grid-div").width() - 3 );
        var height = $("#breadcrumb").outerHeight(true)+$(".query_box").outerHeight(true)+
            $("#delivery_list_fixed_tool_div").outerHeight(true)+
            $("#gview_delivery_list_grid-table .ui-jqgrid-hbox").outerHeight(true)+
            $("#delivery_list_grid-pager").outerHeight(true)+$("#header").outerHeight(true);
        $("#delivery_list_grid-table").jqGrid("setGridHeight", (document.documentElement.clientHeight)-height );
    }); */
    $(window).on("resize.jqGrid", function () {
		$("#delivery_list_grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);
		$("#delivery_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-
		$("#delivery_list_yy").outerHeight(true)-$("#delivery_list_fixed_tool_div").outerHeight(true)-
		$("#delivery_list_grid-pager").outerHeight(true)-$("#gview_delivery_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
	});
    var delivery_list_queryForm_data = iTsai.form.serialize($('#delivery_list_queryForm'));
    
    function delivery_list_queryOk(){
        var queryParam = iTsai.form.serialize($('#delivery_list_queryForm'));
        //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
        delivery_list_queryInstoreListByParam(queryParam);
    }

    function delivery_list_reset(){
        iTsai.form.deserialize($("#delivery_list_queryForm"),delivery_list_queryForm_data);
        $("#delivery_list_queryForm #delivery_list_shipNo").select2("val","");
        delivery_list_queryInstoreListByParam(delivery_list_queryForm_data);
    }
</script>