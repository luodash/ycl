
<%@ page language="java" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String sys_name = "远东电缆仓储管理系统";
	String sys_icon = "/plugins/public_components/img/logo.png";
%>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
	<meta charset="utf-8" />
	<title><%=sys_name%></title>
	<meta name="author" content="DeathGhost" />
	<link rel="shortcut icon" href="<%=path%>/<%=sys_icon%>" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="<%=path%>/static/techbloom/login/css/style.css" />
	
	<script src="<%=path%>/plugins/public_components/js/jquery-2.1.4.js"></script>
	<script src="<%=path%>/plugins/public_components/js/jquery.cookie.js"></script>
	<script src="<%=path%>/plugins/public_components/js/angular.js"></script>
</head>
<style>
 html,body {
	margin:0;
	overflow:hidden;
	width:100%;
	height:100%;
	display: flex;
    flex-direction: column;
}
@keyframes colorChange {
	0%,100% {
		opacity:0;
	}
	50% {
		opacity:.9;
	}
}

</style>
<body ng-app="login" >
    <div style="background: #fff; padding: 12px 0px 12px 70px;">
      <img  style="    width: 240px;" src="<%=path%>/plugins/public_components/img/logo.png"/>
    </div>
	<div class="login_main" style="
	    background-image: url(<%=path%>/plugins/public_components/img/bg.png);
	    padding: 0px;
	    box-sizing: border-box;
	    background-origin: content-box;
	    background-repeat: no-repeat;
	    background-size: 100% 100%;
	">
	<dl class="admin_login" ng-controller="loginController" style="    width: 435px;">
			
			<div class="login_content">
			<dt style="margin-bottom: 20px;">
			 <div style="    align-items: center;    margin: 0px auto;display: flex; flex-wrap: nowrap; flex-direction: row;width:310px;">
			    <img style="position: relative;width: 70px;margin-top: -12px;" src="<%=path%>/plugins/public_components/img/logo_new.png"></img>

				<h2 style="    font-weight: 600;  font-size: 20px; margin-left: 5px; line-height: 30px;color: #333; width: 200px;margin-top: 2px;margin-bottom: 0px"><%=sys_name%></h2>

			 </div>
			</dt>
			<dd class="user_icon" style=" margin-top: 0px;">
				<input type="text" placeholder="账号" ng-model="user.name" id="account"  autocomplete="off" class="login_txtbx" />
			</dd>
			<span class="error">{{user_error}}</span>
			<dd class="pwd_icon">
				<input type="password" placeholder="密码" ng-model="user.pwd"  autocomplete="off" class="login_txtbx" />
			</dd>
			<span class="error">{{pwd_error}}</span>
			<dd class="login-text">
				<input type="checkbox" ng-click="saveCookie()" ng-model="isremember" id="remember"> <label for="remember">记住密码</label> 
				<!-- <a class="forget_pwd">忘记密码？</a> -->
			</dd>
			<span class="error">{{login_error}}</span>
			<dd style=" margin-bottom: 0px;"> 
				<input type="button" value="登陆" ng-click="login()" class="submit_btn" />
			</dd>
			</div>
     </dl>
	</div>
	<div style="background: #fff; padding: 18px 0px 18px 70px;">
      <p style="    font-size: 13px;">© 2016-2019 华清科盛版权所有</p>
    </div>
</body>
<script>
   var context_path= "<%=path%>";
   $("#account").focus();
</script>
<script src="<%=path%>/static/techbloom/login/js/login.js"></script>
</html>
