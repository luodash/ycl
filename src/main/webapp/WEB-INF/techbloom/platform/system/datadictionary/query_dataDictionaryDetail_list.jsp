<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
  	<div style="padding-left:30px;">
   	    <form id="query_dataDictionaryDetail_list_queryForm-cc">
   	     <input type="hidden" id="query_dataDictionaryDetail_list_dictTypeNum" name="dictTypeNum" value="${dictTypeNum}">
			<ul class="form-elements">
				<!-- 字典类型编号 -->
				<li class="field-group">
					<label class="inline" for="query_dataDictionaryDetail_list_dictNum" style="margin-right:20px;">
						字典值编号：
						<input type="text" name="dictNum" id="query_dataDictionaryDetail_list_dictNum" value="${dictDetail.dictNum }"
						style="width: 200px;" placeholder="字典值编号"/>
					</label>
				</li>
				<!-- 字典类型名称 -->
				<li class="field-group">
					<label class="inline" for="query_dataDictionaryDetail_list_dictValue" style="margin-right:20px;">
						字典值&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;：
						<input type="text" name="dictValue" id="query_dataDictionaryDetail_list_dictValue" value="${dictDetail.dictValue }"
						style="width: 200px;" placeholder="字典类型名称"/>
					</label>
				</li>
				<!-- 底部工具按钮 -->
				<li class="field-group">
					<div class="field-button">
						<div class="btn btn-info" onclick="queryOkcc();">
				            <i class="ace-icon fa fa-check bigger-110"></i>确定
			            </div> &nbsp; &nbsp;
						<div class="btn btn_remove"  style="margin-left: 30px"><i class="icon-remove"></i>&nbsp;取消</div>
					</div>
				</li>
			</ul>
		</form>
  	</div>
  <script type="text/javascript">
  
   $(".btn_remove").on("click",function(){
       layer.close(childDiv);
    });
    
		function queryOkcc(){
			//var formJsonParam = $('#queryForm').serialize();
			var queryParam = iTsai.form.serialize($('#query_dataDictionaryDetail_list_queryForm-cc'));
			//执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
			queryDictDetailListByParam(queryParam);
			//操作成功,关闭当前子窗口
	    	layer.close(childDiv);
		}
		
  </script>
