<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript" src="<%=path%>/static/techbloom/system/datadictionary/dataDictionaryDeatil.js"></script>
<div id="dataDictionaryDetail_list_grid-div-c">
    <!-- 隐藏区域：存放查询条件 -->
    <form id="dataDictionaryDetail_list_hiddenQueryForm-c" style="display:none;">
        <input id="dataDictionaryDetail_list_dictTypeNumc" name="dictTypeNumc" value="${dictTypeNum }">
        <!-- 字典类型编号 -->
        <input id="dataDictionaryDetail_list_dictNum" name="dictNum" value=""/>
        <!-- 字典类型名称-->
        <input id="dataDictionaryDetail_list_dictValue" name="dictValue" value="">       
    </form>
     <div class="query_box" id="dataDictionaryDetail_list_yy" title="查询选项">
            <form id="dataDictionaryDetail_list_queryForm" style="max-width:100%;">
			 <ul class="form-elements">
				<li class="field-group field-fluid2">
					<label class="inline" for="dataDictionaryDetail_list_dictNum" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:65px;">字典编号：</span>
						<input type="text" name="dictNum" id="dataDictionaryDetail_list_dictNum" value="" style="width: calc(100% - 70px );" placeholder="字典编号">
					</label>			
				</li>
				<li class="field-group field-fluid2">
					<label class="inline" for="dataDictionaryDetail_list_dictValue" style="margin-right:20px;">
						<span class="form_label" style="width:65px;">字典值：</span>
						<input type="text" name="dictValue" id="dataDictionaryDetail_list_dictValue" value="" style="width:calc(100% - 70px );" placeholder="字典值">
					</label>					
				</li>
			
			</ul>
				<div class="field-button" style="">
					<div class="btn btn-info" onclick="queryOk();">
				        <i class="ace-icon fa fa-check bigger-110"></i>查询
			        </div>
				    <div class="btn" onclick="reset();"><i class="ace-icon icon-remove"></i>重置</div>
		        </div>
		  </form>		 
    </div>
    <div id="dataDictionaryDetail_list_fixed_tool_div" class="fixed_tool_div">
        <div id="dataDictionaryDetail_list___toolbar__-c" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="dataDictionaryDetail_list_grid-table-c" style="width:100%;height:100%;"></table>
    <div id="dataDictionaryDetail_list_grid-pager-c"></div>
</div>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriData;
    var _grid;
    var dynamicDefalutValue="9fb20fa0399446bbb7fa1b9462ee0cf6";
    $("#dataDictionaryDetail_list___toolbar__-c").iToolBar({
        id: "dataDictionaryDetail_list___tb__01",
        items: [
            {label: "添加", disabled: (${sessionUser.addQx} == 1 ? false : true), onclick:addDictDetail, iconClass:'icon-plus'},
    		{label: "编辑", disabled:(${sessionUser.editQx} == 1 ? false : true),onclick: editDictDetail, iconClass:'icon-pencil'},
    		{label: "删除", disabled:(${sessionUser.deleteQx} == 1 ? false : true),onclick: delDictDetail, iconClass:'icon-trash'}
    	]
    });

    $(function () {
        _grid = jQuery("#dataDictionaryDetail_list_grid-table-c").jqGrid({
            url: context_path + '/dictDetail/getList.do?dictTypeNum=' + $("#dataDictionaryDetail_list_hiddenQueryForm-c #dataDictionaryDetail_list_dictTypeNumc").val(),
            datatype: "json",
            colNames: ['字典值主键', '字典值编号', '字典值名称','父字典值编号', '排序码','状态','描述'],
            colModel: [
                {name: 'id', index: 'id', width: 20, hidden: true},
                {name: 'dictNum', index: 'dict_Num', width: 70},
                {name: 'dictValue', index: 'dict_Value', width: 50},
                {name: 'parentDictNum', index: 'parent_dict_Num', width: 50},
                {name: 'orderNum', index: 'order_Num', width: 50},
                {name: 'useFlag',index : 'use_Flag',width : 50,
							formatter:function(cellvalue, options, rowObject){
								if(cellvalue=="0"){
									return "生效";
								}else if(cellvalue=="1"){
									return "失效";
								}
							}
						},
                {name: 'description', index: 'description', width: 50}
            ],
            rowNum: 20,
            rowList: [10, 20, 30],
            pager: '#dataDictionaryDetail_list_grid-pager-c',
            sortname: 'order_num',
            sortorder: "asc",
            altRows: true,
            viewrecords: true,
            caption: "字典值列表",
            autowidth: true,
            multiselect: true,
            multiboxonly: true,
            beforeRequest:function (){
                dynamicGetColumns(dynamicDefalutValue,'dataDictionaryDetail_list_grid-table-c', $(window).width()-$("#sidebar").width() -7);
                //重新加载列属性
            },
            loadComplete: function (data) {
                var table = this;
                setTimeout(function () {
                    updatePagerIcons(table);
                    enableTooltips(table);
                }, 0);
                oriData = data;
            },
            emptyrecords: "没有相关记录",
            loadtext: "加载中...",
            pgtext: "页码 {0} / {1}页",
            recordtext: "显示 {0} - {1}共{2}条数据"
        });
        //在分页工具栏中添加按钮
        jQuery("#dataDictionaryDetail_list_grid-table-c").navGrid('#dataDictionaryDetail_list_grid-pager-c', {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false
        })
			.navButtonAdd('#dataDictionaryDetail_list_grid-pager-c', {
				caption: "",
				buttonicon: "ace-icon fa fa-refresh green",
				onClickButton: function () {
					//jQuery("#grid-table-c").trigger("reloadGrid");  //重新加载表格
					$("#dataDictionaryDetail_list_grid-table-c").jqGrid('setGridParam',
							{
								postData: {queryJsonString: ""} //发送数据
							}
					).trigger("reloadGrid");
				}
			})
            .navButtonAdd('#dataDictionaryDetail_list_grid-pager-c',{
                caption: "",
                buttonicon:"ace-icon fa icon-cogs",
                onClickButton : function (){
                    jQuery("#dataDictionaryDetail_list_grid-table-c").jqGrid('columnChooser',{
                        done: function(perm, cols){
                            dynamicColumns(cols,dynamicDefalutValue);
                            //cols页面获取隐藏的列,页面表格的值
                            $("#dataDictionaryDetail_list_grid-table-c").jqGrid( 'setGridWidth', $("#dataDictionaryDetail_list_grid-div-c").width() );
                        }
                    });
                }
            });
        $(window).on('resize.jqGrid', function () {
            $("#dataDictionaryDetail_list_grid-table-c").jqGrid('setGridWidth', $("#dataDictionaryDetail_list_grid-div-c").width() );
            $("#dataDictionaryDetail_list_grid-table-c").jqGrid('setGridHeight', (document.documentElement.clientHeight - 
            $("#dataDictionaryDetail_list_grid-pager-c").height() - 270 -(!$(".query_box").is(":hidden") ? $(".query_box").outerHeight(true) : 0) ));
        });

        $(window).triggerHandler('resize.jqGrid');
    });
    var _queryForm_data = iTsai.form.serialize($('#dataDictionaryDetail_list_queryForm-c'));
	function queryOkc(){
		var queryParam = iTsai.form.serialize($('#dataDictionaryDetail_list_queryForm-c'));
		//执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
		queryDictDetailListByParam(queryParam);
		
	}
	function resetc(){
		iTsai.form.deserialize($('#dataDictionaryDetail_list_queryForm-c'),_queryForm_data); 
		//执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
		queryDictDetailListByParam(_queryForm_data);
		
	}

</script>
