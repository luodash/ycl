<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<script type="text/javascript">
		var context_path = "<%=path%>";
		function result(res){
			 if(res.result){
				var childCon =layer.confirm("字典值信息"+res.msg, function() {
					gridReload();
					layer.close(childCon);
					layer.close(childDiv);
				});				
			}else{
				layer.alert(res.msg);
			} 
		}
	</script>
<div id="dataDictionaryDetail_edit_page" class="row-fluid" style="height: inherit;">
	<form action="<%=path %>/dictDetail/editDictDetail.do" id="dataDictionaryDetail_edit_dictDetailForm" name="dictDetailForm" class="form-horizontal" style="overflow: auto; height: calc(100% - 70px);">
		<input type="hidden" id="dataDictionaryDetail_edit_id" name="id" value="${dictDetail.id}">
   	    <input type="hidden" id="dataDictionaryDetail_edit_dictTypeNumcc" name="dictTypeNumcc" value="${dictTypeNum}">		
		<div class="control-group">
			<label class="control-label" for="dataDictionaryDetail_edit_dictNum">字典值编号：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<c:choose>
						<c:when test="${edit==1}">
							<input type="text" class="span11" id="dataDictionaryDetail_edit_dictNum" name="dictNum" value="${dictDetail.dictNum }" placeholder="字典类型编号" readonly="readonly" title="代码不可修改">
						</c:when>
						<c:otherwise>
							<input type="text" class="span11" id="dataDictionaryDetail_edit_dictNum" name="dictNum" placeholder="字典值编号">
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="dataDictionaryDetail_edit_dictValue">字典值名称：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="dataDictionaryDetail_edit_dictValue" name="dictValue" value="${dictDetail.dictValue }" placeholder="字典值名称">
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="dataDictionaryDetail_edit_orderNum">排序码：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="dataDictionaryDetail_edit_orderNum" name="orderNum" value="${dictDetail.orderNum }" placeholder="排序码">
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="dataDictionaryDetail_edit_parentDictNum">父字典编号：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="dataDictionaryDetail_edit_parentDictNum" name="parentDictNum" value="${dictDetail.parentDictNum }" placeholder="父字典编号">
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="dataDictionaryDetail_edit_useFlag">状态：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<select id="dataDictionaryDetail_edit_useFlag" name="useFlag" data-placeholder="选择状态" style="width: 438px;">
					     <option <c:if test="${dictDetail.useFlag=='0'}">selected</c:if> value="0">生效</option>
					     <option <c:if test="${dictDetail.useFlag=='1'}">selected</c:if> value="1">失效</option>
					</select>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="dataDictionaryDetail_edit_description">描述：</label>
			<div class="controls">
				<textarea class="span11" name="description" id="dataDictionaryDetail_edit_description" placeholder="描述">${dictDetail.description}</textarea>
			</div>
		</div>
	</form>
	<div class="field-button" style="text-align: center;border-top: 0px;margin: 15px auto;">
		<span class="btn btn-info" onclick="disableAddButton()" id="dataDictionaryDetail_edit_dictDetailButton">
		   <i class="ace-icon fa fa-check bigger-110"></i>保存
		</span>
		<span class="btn btn-danger">
		   <i class="icon-remove"></i>&nbsp;取消
		</span>
	</div>
</div>
<script type="text/javascript">
$(".btn btn-danger").on("click",function(){
       layer.close(childDiv);
    });
    
	$('#dataDictionaryDetail_edit_dictDetailForm').validate({
		rules:{
  			"dataDictionaryDetail_edit_dictNum":{//字典值编号
  				required:true,
  				/* accept1:"^[a-zA-Z0-9]+$", */
  				maxlength:50,
  				remote:"<%=path%>/dictDetail/isExitsDictDetail?checkType=dictNum&id="+$("#id").val()
  			},
  			"dataDictionaryDetail_edit_dictValue":{//字典值名称
  				required:true, 
  				maxlength:32,  
  				validChar:true,
  				remote:"<%=path%>/dictDetail/isExitsDictDetail?checkType=dictValue&id="+$("#id").val()
  				
  			},
  			"dataDictionaryDetail_edit_parentDictNum":{//父字典值
  				maxlength:32,  
  				validChar:true,
  				remote:"<%=path%>/dictDetail/isExitsParent?checkType=parentDictNum&id="+$("#id").val()
  				
  			},
  			"dataDictionaryDetail_edit_description":{
  			    maxlength:256
  			},
  			"dataDictionaryDetail_edit_orderNum":{//排序码
  				digits:true
  			}
  		},
  		messages:{
  			"dataDictionaryDetail_edit_dictNum":{
  				required:"请输入字典值编号！",
  				maxlength:"长度不能超过50个字符！",
  				remote:"您输入的字典值编号已经存在，请重新输入！"
  			},
  			"dataDictionaryDetail_edit_dictValue":{
  				required:"请输入字典值名称！", 
  				maxlength:"长度不能超过32个字符！",
  				remote:"您输入的字典值名称已经存在，请重新输入！"
  			},
  			"dataDictionaryDetail_edit_parentDictNum":{
  				required:"请输入父字典值编号！", 
  				maxlength:"长度不能超过32个字符！",
  				remote:"您输入的字典值名称不存在，请重新输入！"
  			},
  			"dataDictionaryDetail_edit_description":{
  			   maxlength:"字符长度最大不能超过256"
  			},
  			"dataDictionaryDetail_edit_orderNum":{//
  				accept1:"请输入整数！"
  			}
  		}
  	});
</script>