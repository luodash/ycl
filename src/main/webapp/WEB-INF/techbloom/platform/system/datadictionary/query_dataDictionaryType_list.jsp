<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
	<script type="text/javascript">
		var context_path = "<%=path%>";
	</script>
  	<div style="padding-left:30px;">
   	    <form id="query_dataDictionaryType_list_queryForm-c">
			<ul class="form-elements">
				<!-- 字典类型编号 -->
				<li class="field-group">
					<label class="inline" for="query_dataDictionaryType_list_dictTypeNum" style="margin-right:20px;">
						字典类型编号：
						<input type="text" name="dictTypeNum" id="query_dataDictionaryType_list_dictTypeNum" value="${dictType.dictTypeNum }"
						style="width: 200px;" placeholder="字典类型编号"/>
					</label>
				</li>
				<!-- 字典类型名称 -->
				<li class="field-group">
					<label class="inline" for="query_dataDictionaryType_list_dictTypeName" style="margin-right:20px;">
						字典类型名称：
						<input type="text" name="dictTypeName" id="query_dataDictionaryType_list_dictTypeName" value="${dictType.dictTypeName }"
						style="width: 200px;" placeholder="字典类型名称"/>
					</label>
				</li>
				<!-- 底部工具按钮 -->
				<li class="field-group">
					<div class="field-button">
						<div class="btn btn-info" onclick="queryOkc();">
				            <i class="ace-icon fa fa-check bigger-110"></i>确定
			            </div> &nbsp; &nbsp;
						<div class="btn" onclick="layer.closeAll();" style="margin-left: 30px"><i class="icon-remove"></i>&nbsp;取消</div>
					</div>
				</li>
			</ul>
		</form>
  	</div>
  <script type="text/javascript">
  
		function queryOkc(){
			//var formJsonParam = $('#queryForm').serialize();
			var queryParam = iTsai.form.serialize($('#query_dataDictionaryType_list_queryForm-c'));
			//执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
			queryDictTypeListByParam(queryParam);
			//操作成功,关闭当前子窗口
	    	layer.closeAll();
		}
		
  </script>
