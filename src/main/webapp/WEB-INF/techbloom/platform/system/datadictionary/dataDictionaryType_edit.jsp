<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<script type="text/javascript">
		var context_path = "<%=path%>";
		function result(res){
		     if(res.result){
				layer.confirm("字典类型信息"+res.msg, function() {
					gridReload();
					layer.closeAll();
				});
			}else{
				layer.alert(res.msg);
			} 
		}
	</script>
<div id="dataDictionaryType_edit_page" class="row-fluid" style="height: inherit;">
	<form action="<%=path %>/dictType/toSaveDictType.do" id="dataDictionaryType_edit_dictTypeForm" name="dictTypeForm" class="form-horizontal" style="overflow: auto; height: calc(100% - 70px);">
		<input type="hidden" id="dataDictionaryType_edit_id" name="id" value="${car.id}">		
		<div class="control-group">
			<label class="control-label" for="dataDictionaryType_edit_dictTypeNum">字典类型编号：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<c:choose>
						<c:when test="${edit==1}">
							<input type="text" class="span11" id="dataDictionaryType_edit_dictTypeNum" name="dictTypeNum" value="${dictType.dictTypeNum }" placeholder="字典类型编号" readonly="readonly" title="代码不可修改">
						</c:when>
						<c:otherwise>
							<input type="text" class="span11" id="dataDictionaryType_edit_dictTypeNum" name="dictTypeNum" placeholder="字典类型编号">
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="dataDictionaryType_edit_dictTypeName">字典类型名称：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="dataDictionaryType_edit_dictTypeName" name="dictTypeName" value="${dictType.dictTypeName }" placeholder="字典类型名称">
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="dataDictionaryType_edit_description">描述：</label>
			<div class="controls">
				<textarea class="span11" name="description" id="dataDictionaryType_edit_description" placeholder="描述">${dictType.description}</textarea>
			</div>
		</div>
	</form>
	<div class="field-button" style="text-align: center;border-top: 0px;margin: 15px auto;">
		<span class="btn btn-info" onclick="disableAddButton()" id="dataDictionaryType_edit_dictTypeButton">
		   <i class="ace-icon fa fa-check bigger-110"></i>保存
		</span>
		<span class="btn btn-danger" onclick="layer.closeAll();">
		   <i class="icon-remove"></i>&nbsp;取消
		</span>
	</div>
</div>
<script type="text/javascript">
$('#dataDictionaryType_edit_dictTypeForm').validate({
		rules:{
  			"dataDictionaryType_edit_dictTypeNum":{//字典类型编号
  				required:true,
  				maxlength:50,
  				remote:"<%=path%>/dictType/isExitsDictType?checkType=dictTypeNum&id="+$("#dataDictionaryType_edit_id").val()
  			},
  			"dataDictionaryType_edit_dictTypeName":{//字典类型名称
  				required:true, 
  				maxlength:32,  
  				validChar:true,
  				remote:"<%=path%>/dictType/isExitsDictType?checkType=dictTypeName&id="+$("#dataDictionaryType_edit_id").val()
  				
  			},
  			"dataDictionaryType_edit_description":{
  			    maxlength:256
  			}  
  		},
  		messages:{
  			"dataDictionaryType_edit_dictTypeNum":{
  				required:"请输入字典类型编号！",
  				maxlength:"长度不能超过50个字符！",
  				remote:"您输入的字典类型编号已经存在，请重新输入！"
  			},
  			"dataDictionaryType_edit_dictTypeName":{
  				required:"请输入字典类型名称！", 
  				maxlength:"长度不能超过32个字符！",
  				remote:"您输入的字典类型名称已经存在，请重新输入！"
  			},
  			"dataDictionaryType_edit_description":{
  			   maxlength:"字符长度最大不能超过256"
  			}  
  		}
  	});
</script>