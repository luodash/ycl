<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript" src="<%=path%>/static/techbloom/system/datadictionary/dataDictionaryType.js"></script>
<div id="dataDictionaryType_list_grid-div">
	<form id="dataDictionaryType_list_hiddenForm" action="<%=path%>/dictType/toExcel" method="POST" style="display: none;">
		<!-- 选中的用户 -->
		<input id="dataDictionaryType_list_ids" name="ids" value=""/>
	</form>
    <!-- 隐藏区域：存放查询条件 -->
    <form id="dataDictionaryType_list_hiddenQueryForm" style="display:none;">

        <!-- 字典类型编号 -->
        <input id="dataDictionaryType_list_dictTypeNum" name="dictTypeNum" value=""/>
        <!-- 字典类型名称-->
        <input id="dataDictionaryType_list_dictTypeName" name="dictTypeName" value="">

    </form>
     <div class="query_box" id="dataDictionaryType_list_yy" title="查询选项">
            <form id="dataDictionaryType_list_queryForm" style="max-width:100%;">
			 <ul class="form-elements">
				<li class="field-group field-fluid3">
					<label class="inline" for="dataDictionaryType_list_dictTypeNum" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:65px;">字典编号：</span>
						<input type="text" name="dictTypeNum" id="dataDictionaryType_list_dictTypeNum" value="" style="width: calc(100% - 70px );" placeholder="字典编号">
					</label>			
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="dataDictionaryType_list_dictTypeName" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:65px;">字典名称：</span>
						<input type="text" name="dictTypeName" id="dataDictionaryType_list_dictTypeName" value="" style="width: calc(100% - 70px );" placeholder="字典名称">
					</label>					
				</li>
			</ul>
				<div class="field-button" style="">
					<div class="btn btn-info" onclick="queryOk();">
				        <i class="ace-icon fa fa-check bigger-110"></i>查询
			        </div>
				    <div class="btn" onclick="reset();"><i class="ace-icon icon-remove"></i>重置</div>
		        </div>
		  </form>		 
    </div>
    <div id="dataDictionaryType_list_fixed_tool_div" class="fixed_tool_div">
        <div id="dataDictionaryType_list___toolbar__" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="dataDictionaryType_list_grid-table" style="width:100%;height:100%;"></table>
    <div id="dataDictionaryType_list_grid-pager"></div>
</div>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriData;
    var _grid;
    var dynamicDefalutValue="c4607cf9a55c4841aef34b11cbde2d36";
    $("#dataDictionaryType_list___toolbar__").iToolBar({
        id: "dataDictionaryType_list___tb__01",
        items: [
            {label: "添加", disabled: (${sessionUser.addQx} == 1 ? false : true), onclick:addDictType, iconClass:'icon-plus'},
    		{label: "编辑", disabled:(${sessionUser.editQx} == 1 ? false : true),onclick: editDictType, iconClass:'icon-pencil'},
    		{label: "删除", disabled:(${sessionUser.deleteQx} == 1 ? false : true),onclick: delDictType, iconClass:'icon-trash'},
    		{label: "查看", disabled:(${sessionUser.queryQx} == 1 ? false : true),onclick: selectDictTypeDetail, iconClass:'icon-zoom-in'},
    		{label: "导出", disabled: ( ${sessionUser.queryQx}==1?false:true),onclick:function(){toExcel();},iconClass:' icon-share'}

    	]
    });

    $(function () {
        _grid = jQuery("#dataDictionaryType_list_grid-table").jqGrid({
            url: context_path + '/dictType/list.do',
            datatype: "json",
            colNames: ['字典类型主键', '字典类型编号', '字典类型名称', '描述'],
            colModel: [
                {name: 'id', index: 'id', width: 20, hidden: true},
                {name: 'dictTypeNum', index: 'dict_Type_Num', width: 200, fixed: true},
                {name: 'dictTypeName', index: 'dict_Type_Name', width: 150,  fixed: true},
                {name: 'description', index: 'description'}
            ],
            rowNum: 20,
            rowList: [10, 20, 30],
            pager: '#dataDictionaryType_list_grid-pager',
            sortname: 'dict_type_num',
            sortorder: "desc",
            altRows: true,
            viewrecords: true,
            caption: "字典类型列表",
            multiselect: true,
            multiboxonly: true,
            beforeRequest:function (){
                dynamicGetColumns(dynamicDefalutValue,'dataDictionaryType_list_grid-table', $(window).width()-$("#sidebar").width() -7);
                //重新加载列属性
            },
            loadComplete: function (data) {
                var table = this;
                setTimeout(function () {
                    updatePagerIcons(table);
                    enableTooltips(table);
                }, 0);
                oriData = data;
            },
            emptyrecords: "没有相关记录",
            loadtext: "加载中...",
            pgtext: "页码 {0} / {1}页",
            recordtext: "显示 {0} - {1}共{2}条数据"
        });
        //在分页工具栏中添加按钮
        jQuery("#dataDictionaryType_list_grid-table").navGrid('#dataDictionaryType_list_grid-pager', {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false
        }).navButtonAdd('#dataDictionaryType_list_grid-pager', {
                    caption: "",
                    buttonicon: "ace-icon fa fa-refresh green",
                    onClickButton: function () {
                        //jQuery("#grid-table").trigger("reloadGrid");  //重新加载表格
                        $("#dataDictionaryType_list_grid-table").jqGrid('setGridParam',
                                {
                                    postData: {queryJsonString: ""} //发送数据
                                }
                        ).trigger("reloadGrid");
                    }
                }).navButtonAdd('#dataDictionaryType_list_grid-pager',{
                caption: "",
                buttonicon:"ace-icon fa icon-cogs",
                onClickButton : function (){
                    jQuery("#dataDictionaryType_list_grid-table").jqGrid('columnChooser',{
                        done: function(perm, cols){
                            dynamicColumns(cols,dynamicDefalutValue);
                            //cols页面获取隐藏的列,页面表格的值
                            $("#dataDictionaryType_list_grid-table").jqGrid( 'setGridWidth', $("#dataDictionaryType_list_grid-div").width() );
                        }
                    });
                }
            });
        $(window).on('resize.jqGrid', function () {
            $("#dataDictionaryType_list_grid-table").jqGrid('setGridWidth', $("#dataDictionaryType_list_grid-div").width() );
            $("#dataDictionaryType_list_grid-table").jqGrid('setGridHeight', (document.documentElement.clientHeight - 
            $("#dataDictionaryType_list_grid-pager").height() - 196 -(!$(".query_box").is(":hidden") ? $(".query_box").outerHeight(true) : 0) ));
        });

        $(window).triggerHandler('resize.jqGrid');
    });
    var _queryForm_data = iTsai.form.serialize($('#dataDictionaryType_list_queryForm'));
	function queryOk(){
		var queryParam = iTsai.form.serialize($('#dataDictionaryType_list_queryForm'));
		//执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
		queryDictTypeListByParam(queryParam);
		
	}
	function reset(){
		iTsai.form.deserialize($('#dataDictionaryType_list_queryForm'),_queryForm_data); 
		//执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
		queryDictTypeListByParam(_queryForm_data);
	}

</script>
