<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    <div id="settitle_div_content" style="margin-top:20px">
    	<form id="settitle_form_settitle" class="form-horizontal" onkeydown="if(event.keyCode==13)return false;" style=" height: calc(100% - 70px);">
								
			<div  class="row" style="margin:0;padding:0;">
				<div class="control-group">
					<label class="control-label" for="settitle_titleName">标题名称：</label>
					<div class="controls">
						<div class="input-append  required" > 
					      <input class="span11" type="text"  name="titleName" id="settitle_titleName" placeholder="标题名称" style="width:250px">
					    </div> 
					</div>
				</div>				
			</div>
			
			<div  class="row" style="margin:0;padding:0;">
				<div class="control-group">
					<label class="control-label" for="settitle_titlePic">标题图片：</label>
					<div class="controls">
						<div class="input-append " > 
							  <select  class="span11“ name="titlePic" id="settitle_titlePic" placeholder="标题图片" style="width:260px"></select>
 					    </div> 
					</div>
				</div>				
			</div>					
		</form>
    	
    	<div id="settitle_div_actions" style="text-align:right" class="form-actions">
    		<button class="btn btn-success" >保存</button>
    		<button class="btn btn-danger">取消</button>
    	</div>
    </div> 
    
    <script type="text/javascript">
    (function(){
    	var titleIconurl = context_path + "/plugins/base-bootstrap/img/"; //系统标题图标存放位置
    	
    	//标题图片下拉框初始化
    	$.ajax({
   	   		url:context_path+"/config/getSystemIconLists",
			type:"POST",
			dataType : 'JSON',
			success: function (data) {     //在查询时向服务器端传输的数据					
				if (data!= null && data.length>0){					
					for (var i=0;i<data.length;i++)
					{
						$("#settitle_titlePic").append("<option>" + data[i] + "</option>");
					}		
				}
	        }	       
   	   	}); 
    	    	
    	//初始化界面
    	$.ajax({
   	   		url:context_path+"/config/getTitle",
			type:"POST",
			dataType : 'JSON',
			success: function (data) {     //在查询时向服务器端传输的数据		
				if (data.result != null){		
				    $("#settitle_titleName").val(data.result.titleContent);	
				    $("#settitle_titlePic").val(data.result.titleIcon);				
				}
	        }	       
   	   	}); 
    	
    	    	
    	//保存设置
    	$("#settitle_div_content .btn-success").off("click").on("click",function(){
    		//不为空判断
    		var sys_titlename = $("#settitle_titleName").val();
    		var sys_titleicon = $("#settitle_titlePic").val(); 
    		if(sys_titlename == null || sys_titlename == ""){
    			layer.msg("标题名称为空!")
    			return;
    		}
    		
    		
    		//
    		$.ajax({
   	   			url:context_path+"/config/setTitle",
   	   			type:"POST",
   	   			data:{titleName : sys_titlename,titleIcon:sys_titleicon},
   	   			dataType:"JSON",
   	   			success:function(data){
   	   				if(data){
   	   					layer.msg("保存成功！",{icon:1});
   	   					//界面赋值
   	   					$("#settitle_test1").text(sys_titlename);
   	   					document.title = sys_titlename;
   	   					$("#settitle_id_systemTitle").text(sys_titlename);
   						$("#settitle_sys_titleIcon").attr("src",titleIconurl + sys_titleicon);
   	   				}
   	   				else{
   	   					layer.msg("保存失败！",{icon:2});
   	   				}
   	   			}
   	   		});
    	})
    	
    	//取消设置
    	$("#settitle_div_content .btn-danger").off("click").on("click",function(){
    		layer.close($queryWindow);
 	    	return false;
    	})
    	
   	}())
</script>   
    
    
    
