<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    <script type="text/javascript" src="<%=request.getContextPath()%>/static/techbloom/system/config/config.js" />
<div class="row-fluid" id="sysconfig_list_grid-div" style="position:relative;margin-top: 0px;    float: left; height: 100%;">
<style>
#menuTree{
	width: 200px;
	float: left;
	height: 100%;
    box-sizing: border-box;
    overflow: auto;
    border-right: 1px solid #ddd;
}
</style>

<!-- 左侧菜单树结构 -->
<Tui id="sysconfig_list_menuTree" type="ztree"  click-relate = "grid-table" click-relate-param = "menuId" label="菜单列表" ></Tui> 
<div style="float: left; width: calc(100% - 200px); height: 100%;">
 <Tui id="sysconfig_list_grid-table" type="table"  change-relate ="menuTree" label ="菜单列表"></Tui>
</div>

</div>