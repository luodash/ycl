<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    
    <div id="menu_add_div_content" style="margin-top:20px" class="row-fluid">	    
   	<form id="menu_add_form_setmenu" class="form-horizontal" onkeydown="if(event.keyCode==13)return false;" style="overflow:hidden; height: calc(100% - 70px);width:calc(100% - 80px);">
		<div  class="row" style="margin:0;padding:0;">	
			<input type="hidden" name="menuId" id="menu_add_menuId" value="0">
			<div class="control-group">
				<label class="control-label" for="menu_add_menuName">菜单名称：</label>
				<div class="controls">
					<div class="input-append span12 required" > 
				      <input class="span11" type="text"  name="menuName" id="menu_add_menuName" placeholder="菜单名称"/>
				    </div> 
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" for="menu_add_menuUrl">菜单URL：</label>
				<div class="controls">
					<div class="input-append span12 required" > 
				      <input class="span11" type="text"  name="menuUrl" id="menu_add_menuUrl" placeholder="菜单URL"/>
				    </div> 
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" for="menu_add_menuOrder">菜单排序：</label>
				<div class="controls">
					<div class="input-append span12 required" > 
				      <input class="span11" type="text"  name="menuOrder" id="menu_add_menuOrder"/>				    
				    </div> 
				</div>
			</div>
				
			<div class="control-group">
				<label class="control-label" for="menu_add_menuIcon">菜单图片：</label>
				<div class="controls">
					<div class="input-append span12" > 
					  <input class="span11" type="text"  name="menuIcon" id="menu_add_menuIcon" placeholder="菜单图片">
				    </div> 
				</div>
			</div>		
			
			<div class="control-group">
				<label class="control-label" for="menu_add_parentName">父菜单：</label>
				<div class="controls">
					<div class="input-append span12" > 
				      <input class="span11" type="text"  name="parentName" id="menu_add_parentName" placeholder="父菜单"/>		
				    </div> 
				</div>	
			</div>
			
		    <input type="hidden"  name="parentId" id="menu_add_parentId" style="width:0px；height:0px"/>		
						
			<div class="control-group">
			    <label class="control-label" for="vip">VIP：</label>
			    <div class="controls">
			    	<div class="input-append span12" >
			       		<label><input type="radio" id="menu_add_gender1" name="vip" value="0" checked="true">普通</label>
			       		<label><input type="radio" id="menu_add_gender2" name="vip" value="1" style="margin-left:80px">VIP</label>
			       </div>
			    </div>
			</div>
			
			<div class="control-group">
			    <label class="control-label" for="menu_add_remark">备 注：</label>
			    <div class="controls">
			    	<div class="input-append span12">
			       		<input class="span11" type="text"  name="remark" id="menu_add_remark" placeholder="备注"/>
			       </div>
			    </div>
			</div>
		</div>
	    </form>
	  	
	   	<div id="div_actions" style="text-align:right" class="form-actions">
	   		<button class="btn btn-success" >保存</button>
	   		<button class="btn btn-danger">取消</button>
	   	</div>
   </div> 
   
   <script> 
   var selectid = getGridCheckedId("#menu_add_grid-table","menuId");
   
	//标题图片下拉框初始化
	$.ajax({
	   	url:context_path+"/config/getSystemIconLists",
		type:"POST",
		dataType : 'JSON',
		success: function (data) {     //在查询时向服务器端传输的数据					
			if (data!= null && data.length>0){					
				for (var i=0;i<data.length;i++){
					$("#titlePic").append("<option>" + data[i] + "</option>");
				}		
			}
       }	       
	}); 


	   
   $('#menu_add_form_setmenu').validate({
		rules:{
			"menu_add_menuName":{
				required:true,
				maxlength:32,
				remote:context_path+"/config/hasMenuName.do"
			},
			"menu_add_menuUrl":{
				required:true,
				maxlength:32
			},
			"menu_add_menuOrder":{
				required:true,
				maxlength:256
			}
		},
		messages:{
			"menu_add_menuName":{
				required:"请输入菜单名称！",
				remote:"已经有相同的菜单名称，请重新输入！"
			},
			"menu_add_menuUrl":{
				required:"请输入菜单URL！"
			},
			"menu_add_menuOrder":{
				required:"请输入菜单排序！"
			}
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		}, 
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
		}
	 }); 
   
   
   //编辑界面赋值
   if (openwindowtype == 1){
	   $("#menu_add_menuId").val(selectid);
	   $.ajax({
			url:context_path+"/config/getMenuById",
			type:"POST",
			data:{menuId:selectid},
			dataType:"JSON",
			success:function(data){
				if(data){
					$("#menu_add_menuName").val(data.menuName);
					$("#menu_add_menuUrl").val(data.menuUrl);
					$("#menu_add_menuOrder").val(data.menuOrder);
					$("#menu_add_remark").val(data.remark);
					$("#menu_add_menuIcon").val(data.menuIcon);
					$("#menu_add_parentId").val(data.parentId);
					$("input[name='vip'][value='" + data.vip + "']").attr("checked",true);  
					//根据父节点id获取父节点名称					
					$.ajax({
						url:context_path+"/config/getMenuById",
						type:"POST",
						data:{menuId:data.parentId},
						dataType:"JSON",
						success:function(data){
							if(data){
								$("#menu_add_parentName").val(data.menuName);
							}
						}
					});	 
				}
			}
		});	 
   }
   
   $("#menu_add_parentName").click(function(){
	   layer.load(2);
	   $.post(context_sys_path+'/system/systemconfig/dg_menutree.jsp', {}, function(str){
		   $queryWindow = layer.open({
			    title : "选择父菜单", 
		    	type: 1,
		    	skin : "layui-layer-molv",
		    	area : "600px",
		    	shade: 0.6, //遮罩透明度
	   	    	moveType: 1, //拖拽风格，0是默认，1是传统拖动
		    	content: str,//注意，如果str是object，那么需要字符拼接。
		    	success:function(layero, index){
		    		layer.closeAll('loading');
		    	}
		   });
			
		}).error(function() {
			layer.closeAll();
			layer.msg('加载失败！',{icon:2});
		});
   
   })
         
   $("#menu_add_div_content .btn-success").off("click").on("click",function(){	
	   if(!$('#menu_add_form_setmenu').valid()){
		   return;
		}

	var menuBean = $("#menu_add_form_setmenu").serialize();
	
	 //初始化界面
   	$.ajax({
  	   		url:context_path+"/config/saveMenu",
			type:"POST",
			dataType : 'JSON',
			delay : 250,
			data:menuBean,
			success: function (data) {     //在查询时向服务器端传输的数据					
				if (data.result){
					layer.msg("保存成功!");	
					refreshWindow();
					layer.closeAll();
				}
				else{
					layer.msg("保存失败!");
				}
	        }	       
  	   	}); 
   })
   
    $("#menu_add_div_content .btn-danger").off("click").on("click",function(){
    	layer.close($queryWindow);
   })
   </script>