<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
   
    <div id="dg_menutree_div_content" style="margin-top:20px;height: calc(100% - 30px)" class="form-horizontal">
    	<div class="row-fluid" style="background-color: #f7f6f6;"  >
    		<i class="fa fa-sitemap" aria-hidden="true" style="margin: 10px 5px;" />
    		菜单列表
    	</div>
        <div  class="row-fluid">    	
	    	<ul id="dg_menuTree" class="ztree" >
	    	</ul>  
    	</div>
	</div>
        
    <script type="text/javascript">
    (function(){
    	//界面树结构赋值
        /**
		 * 菜单信息初始化
		 */
		var setting = {
			 check:{
		            enable:false
		        },
		        view: {
		            dblClickExpand: true,
		            expandSpeed: ""
		        },		
		    async: {
		        enable: true,//采用异步加载
		        url : context_path + "/config/menuLists?parentId=-1",
		        dataType : "json",
		        type: "POST"
		    },
		    data : {
		        key : {
		            title : "menuName",    
		            name : "menuName"
		        },
		        simpleData : {
		            enable : true,
		            idKey : "menuId",
		            pIdKey : "parentId",
		            rootPid : 000 
		        }
		    },
		    callback : {
		        onAsyncSuccess: zTreeOnAsyncSuccess, //异步加载完成调用
		        onClick:zTreeOnClick
		    }
		};
		
		//异步加载完成时运行，此方法将所有的节点打开
		function zTreeOnAsyncSuccess(event, treeId, msg) {
		    var treeObj = $.fn.zTree.getZTreeObj("dg_menuTree");
		    treeObj.expandAll(false);
		}
		
		//异步加载完成时运行，此方法将所有的节点打开
		function zTreeOnClick(event, treeId, treeNode) {
			 //alert(treeNode.menuId + ", " + treeNode.menuName);		
			 $('#parentName').val(treeNode.menuName);
			 $('#parentId').val(treeNode.menuId);
			 layer.close($queryWindow);
		}
		
		//初始化树
		$.fn.zTree.init($("#dg_menuTree"), setting,"");
		zTree = $.fn.zTree.getZTreeObj("dg_menuTree");
		       	
   	}())
</script>   
    
    
    
