<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>
<!-- 表格 -->
<div class="row-fluid" style="position:relative;margin-top: 0px;  height: 100%;  float: left;">
  <div id="home_config_leftdiv" style="background-color: rgb(255, 255, 255); float: left; width: 200px; border-right: 1px solid rgb(204, 204, 204); box-sizing: border-box; overflow-y: auto; height: 100%;">
	<div class="row-fluid" style="background-color:#f7f6f6;">
		<i class="fa fa-sitemap" aria-hidden="true" style="margin:10px 5px;"></i>角色列表
	</div>
    <div class="row-fluid">
			<ul id="home_config_treerole" class="ztree" ></ul>
	</div>
  </div>
  <div style="padding:0;margin:0; float:left; width:calc(100% - 200px); ">
  	<div class="row-fluid" id="home_config_table_toolbar" style="padding:5px 6px;">
			<button class="btn btn-primary btn-addQx" id="home_config_addModel" >
				添加<i class="fa fa-plus" aria-hidden="true" style="margin-left:5px;"></i>
			</button>
			<button class="btn btn-primary btn-editQx" id="home_config_editModel">
				编辑<i class="fa fa-pencil" aria-hidden="true" style="margin-left:5px;"></i>
			</button>
			<button class="btn btn-primary btn-deleteQx" id="home_config_deleteModel">
				删除<i class="fa fa-trash" aria-hidden="true" style="margin-left:5px;"></i>
			</button>
			
    </div>
    <div class="row-fluid" style="padding:0 1px;">
		<!-- 表格数据 -->
		<table id="home_config_model-table" style="width:100%;"></table>
		<!-- 表格底部 -->
		<div id="home_config_model-pager"></div>
	</div>
   </div>
</div>

<script>
 (function(){
	 var setting = {
			    data: {
			        simpleData: {
			            enable: true
			        }
			    },
			    callback: {
					onClick: function(evt, id ,obj){
						$("#home_config_model-table").jqGrid('setGridParam', 
								{
									postData: {roleId:obj.id,queryJsonString:""} //发送数据 
								}
						).trigger("reloadGrid");
					}
			    },
				async: {
					enable: true,
					url:context_path+"/role/getRoles",
					type: "POST"
				},
			};
	//初始化树
	 $.fn.zTree.init($("#treerole"), setting);

	//表格加载
	_grid = jQuery("#home_config_model-table").jqGrid({
		url : context_path + '/homeConfig/modelList.do',
		datatype : "json",
		styleUI: 'Bootstrap',
		colNames : [ '模块编号', '模块名称','标题'],
		colModel : [ 
            {name : 'modelKey',index : 'model_key',width : 60},
            {name : 'modelName',index : 'model_name',width : 70},
            {name : 'modelTitle',index : 'model_title',width : 90}
		],
          rowNum : 20,
          rowList : [ 10, 20, 30 ],
          pager : '#home_config_model-pager',
          altRows: true,
          viewrecords : true,
          caption : "模块列表",
          hidegrid:false,
          multiselect:true,
          multiboxonly: true,
          autowidth:true,
		  beforeRequest:function (){
			//dynamicGetColumns(dynamicDefalutValue,'grid-table', $(window).width()-$("#sidebar").width() -7);
			//重新加载列属性
		  },
          loadComplete : function(data) 
          {
          	var table = this;
          	setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
          	oriData = data;
          },
          emptyrecords: "没有相关记录",
          loadtext: "加载中...",
          pgtext : "页码 {0} / {1}页",
          recordtext: "显示 {0} - {1}共{2}条数据"
	});
	
	//在分页工具栏中添加按钮
	jQuery("#home_config_model-table").navGrid('#home_config_model-pager',{edit:false,add:false,del:false,search:false,refresh:false})
	.navButtonAdd('#home_config_model-pager',{  
		caption:"",   
		buttonicon:"fa fa-refresh green",   
		onClickButton: function(){
			$("#home_config_model-table").jqGrid('setGridParam', 
					{
						postData: {roleId:"",queryJsonString:""} //发送数据 
					}
			).trigger("reloadGrid");
			zTree = $.fn.zTree.getZTreeObj("treerole");
			zTree.cancelSelectedNode();
		}
	});
	$(window).on('resize.jqGrid', function () {
		$("#home_config_model-table").jqGrid( 'setGridWidth', $(window).width()-$("#sidebar").width()-$("#home_config_leftdiv").width()-7 );
		$("#home_config_model-table").jqGrid( 'setGridHeight', $(".container-fluid").height() -$(".ui-jqgrid-htable").outerHeight(true)- 
		$("#home_config_table_toolbar").outerHeight(true) - $(".ui-jqgrid-titlebar").outerHeight(true) - $("#home_config_model-pager").outerHeight(true) -2);
	});

	$(window).triggerHandler('resize.jqGrid');
	var openLayer = function( url, title, pram ){
		$.post(context_path + url, function(str){
			  layer.open({
			    type : 1,
			    title : title, 
			    area : "600px",
			    skin : "layui-layer-molv",
			    moveType: 1, //拖拽风格，0是默认，1是传统拖动
			    content: str, //注意，如果str是object，那么需要字符拼接。
			    success: function(layero, index){
			    	if(pram && pram.length ==1){
			    		$("#form_div #id").val(pram[0]);
			    		$.ajax({
			    			url:  context_path + "/homeConfig/getModel" ,
				  			type: "POST",
							data: {id : pram[0]},
							dataType:"JSON",
							success:function(data){
								Tui.setFormData($("#form_div") , data.model[0],true);
								$("#roles").select2("data", data.roles);
							}
			    		});
			    	}else{
			    		$("#form_div #id").val("");
			    	}
			    }
			  });
		}).error(function() {
			layer.closeAll();
    		layer.msg('加载失败！',{icon:2});
		});
	}
	$("#home_config_addModel").on("click",function(){
		openLayer("/homeConfig/config_edit","新增");
	});
	$("#home_config_editModel").on("click",function(){
		var selectAmount = $("#home_config_model-table").getGridParam("selarrrow")
		if(selectAmount.length == 0){
			layer.msg("请选择一条记录！",{icon:2});
			return;
		}else if(selectAmount.length > 1){
			layer.msg("只能选择一条记录！",{icon:8});
			return;
		}
        openLayer("/homeConfig/config_edit","修改", selectAmount);
	});
	$("#home_config_deleteModel").on("click",function(){
		var selectAmount = $("#home_config_model-table").getGridParam("selarrrow")
		if(selectAmount.length == 0){
			layer.msg("请选择一条记录！",{icon:2});
			return;
		}
		 $.ajax({
	  			url:  context_path + "/homeConfig/deleteModel" ,
	  			type: "POST",
				data: {model : selectAmount.join(",")},
				dataType:"JSON",
				success:function(data){
				  if(data && data.result) layer.msg("删除成功！",{icon:1}); 
				  else layer.msg("删除失败！",{icon:2});
				  var  zTree = $.fn.zTree.getZTreeObj("treerole");
				  var id = "",node =zTree.getSelectedNodes();
                  if(node && node.length >0){
                	  id = node[0].id
                  }
				  $("#home_config_model-table").jqGrid('setGridParam', 
							{
								postData: {roleId:id,queryJsonString:""} //发送数据 
							}
				  ).trigger("reloadGrid");
				}
 	  });
	});
	 
 })();

</script>