<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>

<div  id= "setting_page" style=" padding: 15px;">
 
  <div style="display:flex;padding: 10px;">
    <div class="row-fluid tui_box" id="config_edit_grid-div" >
      <form class="form-horizontal" id="config_edit_form_div">
        <tui type="inputText_hide" id="config_edit_id" name="id"> </tui>
        <tui type="inputText" id="config_edit_model_title" name="标题"> </tui>
        <tui type="inputText" id="config_edit_model_key" name="编号"> </tui>
        <tui type="inputText" id="config_edit_model_name" name="模块名称"> </tui>
        <tui type="inputText" id="config_edit_model_url" name="数据源"> </tui>
        <tui type="inputText" id="config_edit_model_updatetime" name="刷新间隙（s）"> </tui>
        <tui type="inputText" id="config_edit_model_height" name="面板高度" value="400px"> </tui>
  
        <tui type="radio" id="config_edit_layout"></tui>
        <tui type="select2" id="config_edit_roles" name="所属角色" > </tui> 
        <div class="form-actions" style="text-align: right;border-top: 0px;">
		    <span class="btn btn-info">
			   <i class="ace-icon fa fa-check bigger-110"></i>保存
			</span>
			<span class="btn btn-danger" onclick="layer.closeAll();">
			   <i class="icon-remove"></i>&nbsp;取消
			</span>
		</div>
      </form>
    </div>
  </div>
</div>


<link href="<%=request.getContextPath()%>/static/main/mainPage/css/main.css" rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath()%>/static/main/mainPage/css/jquery-fallr-1.3.css" rel="stylesheet" type="text/css" /> 
<script type="text/javascript" src="<%=request.getContextPath()%>/static/main/mainPage/js/jquery-fallr-1.3.pack.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/static/main/mainPage/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/static/main/mainPage/js/Jh.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/static/main/mainPage/js/charts.js"></script>

<script>
(function(){
	 Tui($("#config_edit_grid-div"), {
		   "layout" : {
	    	  name:"模块位置",
	    	  type:"radio",
	    	  values : [
	    		  {value:"left", name : "左", id:"layout"},
	    		  {value:"center", name : "中", id:"layout"},
	    		  {value: "right", name : "右", id:"layout"}
	    	  ], //数据源
		   },
		   "roles" :{
		    	url: context_path + "/role/getRoleList" ,
		    	select:{
		    		  multiple: true  
		    	}
		    }
	   });
    $(".btn-info").on("click",function(){
    	var Param =  $("#config_edit_form_div").serialize();
    	  $.ajax({
	  			url: context_path + "/homeConfig/save" ,
	  			type:"POST",
				data:Param,
				dataType:"JSON",
				success:function(data){
				  layer.closeAll();
				  if( data && data.result){
					layer.msg("保存成功！",{icon:1}); 
					
				  }else{
					layer.msg("保存失败！",{icon:2});
				  }
				
                  zTree = $.fn.zTree.getZTreeObj("treerole");
                  var id = "",node =zTree.getSelectedNodes();
                  if(node && node.length >0){
                	  id = node[0].id
                  }
				  $("#config_edit_model-table").jqGrid('setGridParam', 
							{
								postData: {roleId:id,queryJsonString:""} //发送数据 
							}
				  ).trigger("reloadGrid");
				}
    	  });
    });
	
}())
</script>