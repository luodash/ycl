<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div class="row-fluid" id="dept_list_grid-div" style="position:relative;margin-top: 0px;">
	<!-- 部门树 -->
	<div  id="dept_list_leftdiv" style="background-color:#ffffff; float:left; width:200px;     border-right: 1px solid #ccc;
    box-sizing: border-box; overflow-y:auto;">
		<div class="row-fluid" style="background-color:#f7f6f6;">
			<i class="fa fa-sitemap" aria-hidden="true" style="margin:10px 5px;"></i>&nbsp;部门列表
		</div>
		<div class="row-fluid">
			<ul id="dept_list_treeDemo" class="ztree" ></ul>
		</div>
	</div>
	<form id="dept_list_hiddenForm" action="<%=path%>/dept/toExcel" method="POST" style="display: none;">
		<!-- 选中的用户 -->
		<input id="dept_list_ids" name="ids" value=""/>
	</form>
	<!-- 表格内容 -->
	<div  style="padding:0;margin:0; float:left; width:calc(100% - 200px); ">
		<!-- 工具栏 -->
		<div class="row-fluid" id="dept_list_table_toolbar" style="padding:5px 6px;">
			<form id="dept_list_query_form" class="form-horizontal" style="width: calc(100% - 305px)">
		          <span  style="float: left;width: 35%;min-width: 80px;position: relative;">
		          <input class="span12" type="text"  name="departmentNo" id="dept_list_departmentNo" placeholder="部门编号" style="min-height: 28px; border-right: 0px;"/>
		          <i class="icon-question-sign" style="position: absolute;top: 8px;right: 8px;color: #999;"></i></span>
		          <span  style="float: left;width: 35%;min-width: 80px;position: relative;">
		          <input class="span12" type="text" name="departmentName" id="dept_list_departmentName" value="" placeholder="部门名称" style="min-height: 28px; border-right: 0px;"/>
		          <i class="icon-question-sign" style="position: absolute;top: 8px;right: 8px;color: #999;"></i></span>
		    </form>
		    <button class="btn btn-primary btn-queryQx" onclick="queryRow();">
				查询<i class="fa fa-search" aria-hidden="true" style="margin-left:5px;"></i>
			</button>
			<button class="btn btn-primary btn-addQx" onclick="openAddPage();">
				添加<i class="fa fa-plus" aria-hidden="true" style="margin-left:5px;"></i>
			</button>
			<button class="btn btn-primary btn-editQx" onclick="openEditPage();">
				编辑<i class="fa fa-pencil" aria-hidden="true" style="margin-left:5px;"></i>
			</button>
			<button class="btn btn-primary btn-deleteQx" onclick="deleteUser();">
				删除<i class="fa fa-trash" aria-hidden="true" style="margin-left:5px;"></i>
			</button>
			<button class="col-md-1 btn btn-primary btn-queryQx" onclick="exportLogFile();">
				导出<i class="fa fa-share" aria-hidden="true" style="margin-left:5px;"></i>
			</button>

		</div>
		<!-- 表格 -->
		<div class="row-fluid" style="padding:0 3px;">
			<!-- 表格数据 -->
			<table class="table" id="dept_list_grid-table" style="width:100%;"></table>
			<!-- 表格底部 -->
			<div id="dept_list_grid-pager"></div>
		</div>
	</div>
	
</div>
<script src="<%=request.getContextPath()%>/static/techbloom/system/department/department.js"></script>
<script>
function initLeftH(){
	$("#dept_list_leftdiv").height($(window).height()-$("#user-nav").height()-$("#breadcrumb").height());
}
$(window).on("resize",initLeftH());
</script>