<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
	<style type="text/css">
	</style>
  	<div style="padding-left:30px;">
   	    <form id="query_dept_list_queryForm-c">
			<ul class="form-elements">
				<!-- 部门编号 -->
				<li class="field-group">
					<label class="inline" for="query_dept_list_departmentNo" style="margin-right:20px;">
						部门编号：
						<input type="text" name="departmentNo" id="query_dept_list_departmentNo" value="${dept.departmentNo }"
						style="width: 200px;" placeholder="部门编号"/>
					</label>
				</li>
				<!-- 公司名称 -->
				<li class="field-group">
					<label class="inline" for="query_dept_list_departmentName" style="margin-right:20px;">
						部门名称：
						<input type="text" name="departmentName" id="query_dept_list_departmentName" value="${dept.departmentName }"
						style="width: 200px;" placeholder="部门名称"/>
					</label>
				</li>
				
				<!-- 底部工具按钮 -->
				<li class="field-group">
					<div class="field-button">
						<div class="btn btn-info" onclick="queryOkc();">
				            <i class="ace-icon fa fa-check bigger-110"></i>确定
			            </div> &nbsp; &nbsp;
						<div class="btn" onclick="layer.closeAll();"><i class="glyphicon glyphicon-remove"></i>&nbsp;取消</div>
					</div>
				</li>
			</ul>
		</form>
  	</div>
  <script type="text/javascript">
  
		function queryOkc(){
			var queryParam = iTsai.form.serialize($('#query_dept_list_queryForm-c'));
			//执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
			queryDeptListByParam(queryParam);
			//操作成功,关闭当前子窗口
	    	layer.closeAll();
		}
		
		function emptyInputValue(){
			var $inputs = $("input");
			for(var i = 0;i<$inputs.length;i++){
				$inputs[i].vale="";
			}
		}
  </script>
