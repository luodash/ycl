<%@ page language="java" pageEncoding="UTF-8"%>
<div class="row-fluid" style="height: inherit;">
  	 <form id="dept_edit_deptForm" class="form-horizontal"  style="overflow: auto; height: calc(100% - 70px);">
        <input type="hidden" name="parentId" id="dept_edit_parentId" value="0"><!-- 隐藏的父类别主键 -->
        <input type="hidden" id="dept_edit_deptId" name="deptId" value=""><!-- 隐藏的主键 -->
        <div class="control-group">
			<label class="control-label" for="dept_edit_departmentNo">部门编号：</label>
			<div class="controls">
				<div class="input-append span12 required" > 
			      <input class="span11" type="text"  name="departmentNo" id="dept_edit_departmentNo"placeholder="部门编号">
			    </div> 
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="dept_edit_departmentName">部门名称：</label>
			<div class="controls">
				<div class="input-append span12 required" > 
			      <input class="span11" type="text"  name="departmentName" id="dept_edit_departmentName"placeholder="部门名称">
			    </div> 
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="dept_edit_description">部门描述：</label>
			<div class="controls">
			      <input class="span11" type="text"  name="description" id="dept_edit_description"placeholder="部门描述">
			</div>
		</div>
	</form>
	
	<div class="form-actions" style="text-align: right;border-top: 0px; margin: 0px;">
		<button class="btn btn-success" onclick="saveForm();return false;">确定</button>
		<button class="btn btn-danger" onclick="layer.close($queryWindow);return false;">关闭</button>
	</div>
</div>
<script>
//选中的部门主键
var selectid = getGridCheckedId("#dept_edit_grid-table","dept_edit_deptId");
var nodes = zTree.getSelectedNodes();
//父类别
if(nodes.length==1) $("#dept_edit_parentId").val(nodes[0].id);
//主键
if(openwindowtype == 1) $("#dept_edit_deptId").val(selectid);
$.ajax({
	url:context_path+"/dept/getDeptById",
	type:"POST",
	data:{deptId:$("#dept_edit_deptId").val()},
	dataType:"JSON",
	success:function(data){
		if(data){
			for ( var deptattr in data) {
				if(deptattr!="parentId" && deptattr!="deptId")
					$("#"+deptattr, $("#dept_edit_deptForm")).val(data[deptattr]);
			}
			$("#dept_edit_departmentNo", $("#dept_edit_deptForm")).attr({
				"readonly":"readonly",
				"title":"部门编号不可以修改"
			});
		}
	}
});

$('#dept_edit_deptForm').validate({
	rules:{
		"dept_edit_departmentNo":{
			required:true,
			maxlength:32,
			remote:context_path+"/dept/hasDeptNo.do?_id="+$("#dept_edit_deptId").val()
		},
		"dept_edit_departmentName":{
			required:true,
			maxlength:32,
			remote:context_path+"/dept/hasDeptName.do?_id="+$("#dept_edit_deptId").val()
		},
		"dept_edit_description":{
			maxlength:256
		}
	},
	messages:{
		"dept_edit_departmentNo":{
			required:"请输入部门编号！",
			remote:"已经有相同的部门编号，请重新输入！"
		},
		"dept_edit_departmentName":{
			required:"请输入部门名称！",
			remote:"已经有相同的部门名称，请重新输入！"
		},
		"dept_edit_description":{
			maxlength:"部门描述不能超过256个字符！"
		}
	},
	errorClass: "help-inline",
	errorElement: "span",
	highlight:function(element, errorClass, validClass) {
		$(element).parents('.control-group').addClass('error');
	},
	unhighlight: function(element, errorClass, validClass) {
		$(element).parents('.control-group').removeClass('error');
	}
 });
 
//确定按钮点击事件
function saveForm(){
	if($('#dept_edit_deptForm').valid()){
		//layer.msg("校验通过！",{icon:1});
		saveDeptInfo($("#dept_edit_deptForm").serialize());
	}
}

var ajaxStatus = 1;     //ajax请求状态：0不能请求，1可以请求
//保存/修改用户信息
function saveDeptInfo(bean){
	if(bean){
		if(ajaxStatus==0){
			layer.msg("操作进行中，请稍后...",{icon:2});
			return;
		}
		ajaxStatus = 0;    //将标记设置为不可请求
			
		$.ajax({
			url:context_path+"/dept/save?tm="+new Date(),
			type:"POST",
			data:bean,
			dataType:"JSON",
			success:function(data){
				ajaxStatus = 1; //将标记设置为可请求
				if(data){
					layer.msg("保存成功！",{icon:1});
					//刷新左侧树
					//zTree.reAsyncChildNodes(null, "refresh");
					var node = zTree.getSelectedNodes()[0];
                    selectTreeId  =  node.id;
			        zTree.reAsyncChildNodes(node, "refresh",false);
					//刷新列表
					$("#dept_edit_grid-table").jqGrid('setGridParam', 
						{
							postData: {queryJsonString:""} //发送数据 
						}
					).trigger("reloadGrid");
					//关闭当前窗口
					//关闭指定的窗口对象
					layer.close($queryWindow);
				}else{
					layer.alert("保存失败，请稍后重试！",{icon:2});
				}
			}
		});
	}else{
		layer.msg("出错啦！",{icon:2});
	}
}
</script>