<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div  id ="role_edit_role_edit_page" class="row-fluid" style="height:inherit;">
	<form id="role_edit_roleForm" class="form-horizontal" onkeydown="if(event.keyCode==13)return false;" style=" height: calc(100% - 70px);">
		<input type="hidden" name="roleId" id="role_edit_roleId" value="" >
        <input type="hidden" id="role_edit_type01" value="1">
		<!-- 角色基础信息 -->
		<div  class="row" style="margin:0;padding:0;">
			<div  class="row" style="margin:0;padding:0;">
				<span class="span12" style="padding:10px;border-bottom:solid #009f95 1px;font-size:14px;">
					<i class="fa fa-users" aria-hidden="true"></i>&nbsp;角色信息
				</span>
			</div>
			<div  class="row" style="margin:0;padding:0;">
				<div class="control-group">
					<label class="control-label" for="role_edit_roleName">角色选择：</label>
					<div class="controls">
						<div class="input-append span12" >
							<label class="radio-inline">
								<input type="radio" name="generalType" id="inlineRadio1" value="1" checked="checked"> 成品
							</label>
							<label class="radio-inline" style="margin-left: 20px">
								<input type="radio" name="generalType" id="inlineRadio2" value="2"> 原材料
							</label>
						</div>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="role_edit_roleName">角色名称：</label>
					<div class="controls">
						<div class="input-append span12 required" > 
					      <input class="span11" type="text"  name="roleName" id="role_edit_roleName" placeholder="角色名称">
					    </div> 
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="role_edit_remark">备注：</label>
					<div class="controls">
						<div class="input-append span12" > 
					      <textarea class="span11" rows="4" cols="20"  name="remark" id="role_edit_remark" placeholder="备注"></textarea>
					    </div> 
					</div>
				</div>
			</div>
		</div>
		<div style="padding-top: 20px;">
            <ul class="nav nav-tabs">
                <li role="presentation" class="active" id="role_edit_tab01"><a href="javascript:;">WEB端</a></li>
                <li role="presentation" id="role_edit_tab02"><a href="javascript:;">PDA手持机</a></li>
                <li role="presentation" id="role_edit_tab03"><a href="javascript:;">PAD平板</a></li>
				<li role="presentation" id="role_edit_tab04" style="display: none"><a href="javascript:;">WEB端</a></li>
                <li role="presentation" id="role_edit_tab05" style="display: none"><a href="javascript:;">PDA手持机</a></li>
            </ul>
        </div>
		<!-- 菜单树 -->
		<div  class="row" style="margin:0;padding:0;height:250px;">
			<div  class="row" style="margin:0;padding:0; overflow: auto; height:210px;">
				<ul id="role_edit_menuTree" class="ztree" ></ul>
			</div>
		</div>
	</form>
	<!-- 底部按钮 -->
	<div class="form-actions" style="text-align: right;border-top: 0px;margin: 0px;">
		<span  class="btn btn-success">保存</span>
		<span  class="btn btn-danger">取消</span>
	</div>
</div>
<script type="text/javascript">
(function(){
    var selectid = getGridCheckedId("#role_list_grid-table","roleId");
    if(role_list_openwindowtype==1){
        //更新
        $("#role_edit_roleId").val(selectid);
        setting.async.otherParam ={"rid":selectid,"sysid":$("#index_sysid").val(),"type":role_edit_type01};
    }else{
        //新增
        setting.async.otherParam ={"rid":null,"sysid":$("#index_sysid").val(),"type":role_edit_type01};
    }

    $("#role_edit_tab01").unbind("click").on("click",function(){
        $("#role_edit_tab01").addClass("active");
        $("#role_edit_tab02").removeClass("active");
        $("#role_edit_tab03").removeClass("active");
        $("#role_edit_tab04").removeClass("active");
        $("#role_edit_tab05").removeClass("active");
        $("#role_edit_type01").val(1);

        if(role_list_openwindowtype==0){
            setting.async.otherParam ={"rid":null,"sysid":$("#index_sysid").val(),"type":1,"notInMenuNumber":2};
		}else{
            setting.async.otherParam ={"rid":selectid,"sysid":$("#index_sysid").val(),"type":1,"notInMenuNumber":2};
		}

        setTree();
    });
    $("#role_edit_tab02").unbind("click").on("click",function(){
        $("#role_edit_tab02").addClass("active");
        $("#role_edit_tab01").removeClass("active");
        $("#role_edit_tab03").removeClass("active");
        $("#role_edit_tab04").removeClass("active");
        $("#role_edit_tab05").removeClass("active");
        $("#role_edit_type01").val(2);
        if(role_list_openwindowtype==0){
            setting.async.otherParam ={"rid":null,"sysid":$("#index_sysid").val(),"type":2,"menuNumber":1};
        }else{
            setting.async.otherParam ={"rid":selectid,"sysid":$("#index_sysid").val(),"type":2,"menuNumber":1};
        }
        setTree();
    });
    $("#role_edit_tab03").unbind("click").on("click",function(){
        $("#role_edit_tab03").addClass("active");
        $("#role_edit_tab01").removeClass("active");
        $("#role_edit_tab02").removeClass("active");
        $("#role_edit_tab04").removeClass("active");
        $("#role_edit_tab05").removeClass("active");
        $("#role_edit_type01").val(3);
        if(role_list_openwindowtype==0){
            setting.async.otherParam ={"rid":null,"sysid":$("#index_sysid").val(),"type":3};
        }else{
            setting.async.otherParam ={"rid":selectid,"sysid":$("#index_sysid").val(),"type":3};
        }
        setTree();
    });
    //远东二期WEB
    $("#role_edit_tab04").unbind("click").on("click",function(){
        $("#role_edit_tab04").addClass("active");
        $("#role_edit_tab01").removeClass("active");
        $("#role_edit_tab02").removeClass("active");
        $("#role_edit_tab03").removeClass("active");
        $("#role_edit_tab05").removeClass("active");
        $("#role_edit_type01").val(4);
        if(role_list_openwindowtype==0){
            setting.async.otherParam ={"rid":null,"sysid":$("#index_sysid").val(),"type":1,"notInMenuNumber":1};
        }else{
            setting.async.otherParam ={"rid":selectid,"sysid":$("#index_sysid").val(),"type":1,"notInMenuNumber":1};
        }
        setTree();
    });
    //远东二期手持机
    $("#role_edit_tab05").unbind("click").on("click",function(){
        $("#role_edit_tab05").addClass("active");
        $("#role_edit_tab01").removeClass("active");
        $("#role_edit_tab02").removeClass("active");
        $("#role_edit_tab03").removeClass("active");
        $("#role_edit_tab04").removeClass("active");
        $("#role_edit_type01").val(5);
        if(role_list_openwindowtype==0){
            setting.async.otherParam ={"rid":null,"sysid":$("#index_sysid").val(),"type":2,"menuNumber":2};
        }else{
            setting.async.otherParam ={"rid":selectid,"sysid":$("#index_sysid").val(),"type":2,"menuNumber":2};
        }
        setTree();
    });
    $("#role_edit_roleForm  input[type=checkbox]").uniform();

    function setTree(){
        $.fn.zTree.init($("#role_edit_menuTree"), setting);
        zTree = $.fn.zTree.getZTreeObj("role_edit_menuTree");
    }
    //初始化树

    $.ajax({
        url:context_path+"/role/getRoleById",
        type:"POST",
        data:{rid : $("#role_edit_roleId").val()},
        dataType:"JSON",
        success:function(data){
            if(data){
                //动态设置单选框的选中
                if(data.generalType == 2){
                    $("#inlineRadio2").click();
                    $("#role_edit_tab04").click();
                }
                if(data.generalType != 0){//超级管理员
                    $('#inlineRadio1').attr("disabled",true);
                    $('#inlineRadio2').attr("disabled",true);
                }

                $("#role_edit_roleForm #role_edit_roleName").val(data.roleName);
                $("#role_edit_roleForm #role_edit_remark").val(data.remark);
            }


        }
    });
    $('#role_edit_roleForm').validate({
        rules:{
            "role_edit_roleName":{
                required:true,
                maxlength:32,
                minlength:2,
                remote:context_path+"/role/hasR.do?_id="+$("#roleId").val()
            },
            "role_edit_remark":{
                maxlength:256
            }
        },
        messages:{
            "role_edit_roleName":{
                required:"请输入角色名称",
                remote:"您输入的角色名已经存在，请重新输入！"
            }
        },
        errorClass: "help-inline",
        errorElement: "span",
        highlight:function(element, errorClass, validClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        }
     });

	var ajaxStatus = 1;     //ajax请求状态：0不能请求，1可以请求
	//确定按钮点击事件
    $("#role_edit_role_edit_page .btn-success").off("click").on("click", function(){
		//角色保存
		if($('#role_edit_roleForm').valid()){
			//获取选中的菜单
	    	var nodes = zTree.getCheckedNodes(true);
			var menuids = "";
			for(var rolei=0,count = nodes.length;rolei<count; rolei++){
				var menuNode = nodes[rolei];
				if(menuNode.id!=""&&menuNode.id!="M0"){
					if(menuids.length>0){
						menuids += ","+menuNode.id;
					}else{
						menuids += menuNode.id;
					}
				}
			}
			var roleParam = $("#role_edit_roleForm").serialize();

			if( $("#role_edit_roleName").val()==null || $("#role_edit_roleName").val()==""){
                layer.msg("请填写角色名称！");
			}else{
                if(ajaxStatus==0){
                    layer.msg("操作进行中，请稍后...",{icon:2});
                    return;
                }
                ajaxStatus = 0;    //将标记设置为不可请求
                $.ajax({
                    url:context_path+"/role/addRole?tm="+new Date(),
                    type:"POST",
                    data:roleParam+"&menuId="+menuids+"&type="+$("#role_edit_type01").val(),
                    dataType:"JSON",
                    success:function(data){
                        ajaxStatus = 1; //将标记设置为可请求
                        if(data){
                            //刷新表格
                            $("#role_list_grid-table").jqGrid('setGridParam',
                                {
                                    postData: {queryJsonString:""} //发送数据
                                }
                            ).trigger("reloadGrid");
                            layer.msg("操作成功！",{icon:1});
                            layer.close($queryWindow);
                        }else{
                            layer.alert("操作失败！",{icon:2});
                        }
                    }
                });
			}
		}
		return false;
	});
	
	//取消按钮点击事件
	$("#role_edit_role_edit_page .btn-danger").off("click").on("click", function(){
	    layer.close($queryWindow);
	    return false;
	});

	//远东一期切换二期切换
    $("input:radio[name='generalType']").change(function (){
        if($(this).val()=="1"){
            $("#role_edit_tab01").click();
            $("#role_edit_tab01").show();
            $("#role_edit_tab02").show();
            $("#role_edit_tab03").show();
            $("#role_edit_tab04").hide();
            $("#role_edit_tab05").hide();
        }else{
            $("#role_edit_tab04").click();
            $("#role_edit_tab01").hide();
            $("#role_edit_tab02").hide();
            $("#role_edit_tab03").hide();
            $("#role_edit_tab04").show();
            $("#role_edit_tab05").show();
        }
    });

    $("#role_edit_tab01").click();
}());

</script>
</html>
