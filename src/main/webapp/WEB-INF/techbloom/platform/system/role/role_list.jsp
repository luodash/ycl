﻿﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- 表格 -->
<div class="row-fluid" id="role_list_grid-div"
	style="position:relative;margin-top: 0px;  float: left;">
	<!-- 工具栏 -->
	<div class="row-fluid" id="role_list_table_toolbar" style="padding:5px 3px;" >
	  	<form id="role_list_query_form" class="form-horizontal" style="width: calc(100% - 300px)">
			<c:if test="${operationCode.webSearch==1}">
	          	<span  style="float: left;width: 100%;min-width: 80px;position: relative;">
	          	<input class="span12" type="text" onkeydown="if(event.keyCode==13) return false;" 
	          	name="roleName" id="role_list_roleName" placeholder="角色名" style="min-height: 28px; border-right: 0px;"/>
	          	<i class="icon-user" style="position: absolute;top: 8px;right: 8px;color: #999;"></i></span>
			</c:if>
		</form>
	    <!-- 在按钮上都添加上btn-addQX用来标记列表中的操作按钮，方便控制按钮的权限 -->
		<c:if test="${operationCode.webSearch==1}">
			<button class=" btn btn-primary btn-queryQx" onclick="role_list_openQueryPage();">
				查询<i class="fa fa-search" aria-hidden="true" style="margin-left:5px;"></i>
			</button>
		</c:if>
		<c:if test="${operationCode.webAdd==1}">
			<button class=" btn btn-primary btn-addQx" onclick="role_list_openAddPage();">
				添加<i class="fa fa-plus" aria-hidden="true" style="margin-left:5px;"></i>
			</button>
		</c:if>
			<button class=" btn btn-primary btn-editQx" onclick="role_list_openEditPage();">
				编辑<i class="fa fa-pencil" aria-hidden="true" style="margin-left:5px;"></i>
			</button>
		<c:if test="${operationCode.webDel==1}">
			<button class=" btn btn-primary btn-deleteQx" onclick="role_list_deleteRow();">
				删除<i class="fa fa-trash" aria-hidden="true" style="margin-left:5px;"></i>
			</button>
		</c:if>
	</div>
	<!-- 表格 -->
	<div class="row-fluid" style="padding:0 3px;">
		<!-- 表格数据 -->
		<table id="role_list_grid-table" style="width:100%;"></table>
		<!-- 表格底部 -->
		<div id="role_list_grid-pager"></div>
	</div>
</div>
<script src="<%=request.getContextPath()%>/static/techbloom/system/role/role.js"></script>
