<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="interface_log_grid-div">
    <form id="interface_log_hiddenForm" action="<%=path%>/interfaceLog/toExcel.do" method="POST" style="display: none;">
        <input id="interface_log_ids" name="ids" value=""/>
    </form>
    <form id="interface_log_hiddenQueryForm" style="display:none;">
        <input id="interface_log_address" name="address" value=""/>
        <input id="interface_log_startTime" name="startTime" value="">
    </form>
    <div class="query_box" id="interface_log_yy" title="查询选项">
            <form id="interface_log_queryForm" style="max-width:100%;">
			 <ul class="form-elements">
			   <li class="field-group field-fluid2">
					<label class="inline" for="address" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:80px;">服务类型：</span>
						<input type="text" id="interface_log_address" name="address" value="" style="width: calc(100% - 85px );" placeholder="选择服务类型">
					</label>				
				</li>
				<li class="field-group field-fluid2">
					<label class="inline" for="interface_log_startTime" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:80px;">操作时间：</span>
						<input class="form-control date-picker" id="interface_log_startTime" name="startTime" style="width: calc(100% - 85px );" type="text" placeholder="操作时间" />
					</label>			
				</li>
				
			</ul>
			<div class="field-button">
					<div class="btn btn-info" onclick="queryOk();">
				        <i class="ace-icon fa fa-check bigger-110"></i>查询
			        </div>
					<div class="btn" onclick="reset();"><i class="ace-icon icon-remove"></i>重置</div>
		        </div>
		  </form>		 
    </div>
    <div id="interface_log_fixed_tool_div" class="fixed_tool_div">
        <div id="interface_log___toolbar__" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="interface_log_grid-table" style="width:100%;height:100%;"></table>
    <div id="interface_log_grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
	var oriData; 
	var _grid;
	var openwindowtype = 0; //打开窗口类型：0新增，1修改
	var selectid;
	$(".date-picker").datetimepicker({format: "YYYY-MM-DD"});
	$("#interface_log___toolbar__").iToolBar({
	    id: "interface_log___tb__01",
	    items: [       
	        {label: "导出", disabled: ( ${sessionUser.queryQx}==1 ? false : true),onclick:function(){toExcel();},iconClass:'icon-share'}
	   ]
	});
	var _queryForm_data = iTsai.form.serialize($("#interface_log_queryForm"));
	_grid = jQuery("#interface_log_grid-table").jqGrid({
			url : context_path + "/interfaceLog/list.do",
		    datatype : "json",
		    colNames : ["主键","远程IP","远程MAC","服务类型","请求地址","请求类型","开始时间","结束时间","请求参数","请求结果","调用情况","错误记录"],
		    colModel : [ 
		                 {name : "id", index: "id", hidden: true},
		                 {name : "ip",index : "ip",width:55},
		                 {name : "mac",index : "mac",width : 55},
		                 {name : "service",index : "service",width : 60}, 
		                 {name : "address",index : "address",width : 60},
		                 {name : "type",index : "type",width :30}, 
		                 {name : "startTime",index : "startTime",width : 100}, 
		                 {name : "endTime",index : "endTime",width : 100},
		                 {name : "parameter",index : "parameter",width: 60},
		                 {name : "result",index : "result",width : 60},
		                 {name : "error",index:"error",width:50,formatter:function(cellvalue,option,rowObject){
                              if(cellvalue==0){
                                 return "<span style='color:green;font-weight:bold;'>调用成功</span>";
                               }
                              if(cellvalue==1){
                                 return "<span style='color:red;font-weight:bold;'>调用异常</span>";
                               }
                              }
                            }, 			                
		                 {name : "errorInfo",index : "errorInfo",width: 60} 
		               ],
		    rowNum : 20,
		    rowList : [ 10, 20, 30 ],
		    pager : "#interface_log_grid-pager",
		    sortname : "startTime",
		    sortorder : "desc",
            altRows: true,
            viewrecords : true,
            hidegrid:false,
     	    autowidth:true, 
            multiselect:true,
            loadComplete : function(data) {
            	var table = this;
            	setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            	oriData = data;
            },
            emptyrecords: "没有相关记录",
            loadtext: "加载中...",
            pgtext : "页码 {0} / {1}页",
            recordtext: "显示 {0} - {1}共{2}条数据"
	});

	jQuery("#interface_log_grid-table").navGrid("#interface_log_grid-pager",{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#interface_log_grid-pager",{  
		caption:"",   
		buttonicon:"fa fa-refresh green",   
		onClickButton: function(){   
			$("#interface_log_grid-table").jqGrid("setGridParam", 
					{
				postData: {queryJsonString:""} //发送数据 
					}
			).trigger("reloadGrid");
		}
	});

	$(window).on("resize.jqGrid", function () {
		$("#interface_log_grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);
		$("#interface_log_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-
		$("#interface_log_yy").outerHeight(true)-$("#interface_log_fixed_tool_div").outerHeight(true)-
		$("#interface_log_grid-pager").outerHeight(true)-
		$("#gview_interface_log_grid-table .ui-jqgrid-hdiv").outerHeight(true));
	});

	$(window).triggerHandler("resize.jqGrid");
	//重新加载表格
	function gridReload(){
		_grid.trigger("reloadGrid");  //重新加载表格  
	}
	/**
	 * 查询功能:获取查询页面中的值，并将值放入列表页面中隐藏的form
	 * @param jsonParam     查询页面传递过来的json对象
	 */
	function queryLogListByParam(jsonParam){
		var queryJsonString = JSON.stringify(jsonParam);         //将json对象转换成json字符串
		//执行查询操作
		$("#interface_log_grid-table").jqGrid("setGridParam", 
				{
			postData: {queryJsonString:queryJsonString} //发送数据 
				}
		).trigger("reloadGrid");
	}
	/**
	 * 查询按钮点击事件
	 */
	 function queryOk(){
		 var queryParam = iTsai.form.serialize($("#interface_log_queryForm"));
		 //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
		 queryLogsByParam(queryParam);
	}
	
	 function queryLogsByParam(jsonParam) {
	    iTsai.form.deserialize($("#interface_log_hiddenQueryForm"), jsonParam);
	    var queryParam = iTsai.form.serialize($("#interface_log_hiddenQueryForm"));
	    var queryJsonString = JSON.stringify(queryParam); 
	    $("#interface_log_grid-table").jqGrid("setGridParam",
	        {
	            postData: {queryJsonString: queryJsonString}
	        }
	    ).trigger("reloadGrid");
	}
	 
	function reset(){
		 iTsai.form.deserialize($("#interface_log_queryForm"),_queryForm_data);
		 $("#interface_log_queryForm #interface_log_address").select2("val",""); 
		 queryCarsByParam(_queryForm_data);
	}
	
	function toExcel(){
	    var ids = jQuery("#interface_log_grid-table").jqGrid("getGridParam", "selarrrow");
	    $("#interface_log_hiddenForm #interface_log_ids").val(ids);
	    $("#interface_log_hiddenForm").submit();	
	}
	
	$("#interface_log_queryForm #interface_log_address").select2({
        placeholder: "选择服务类型",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/interfaceLog/getSelectService",
            type: "POST",
            dataType: "json",
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });   
	
    $("#interface_log_queryForm .mySelect2").select2(); 
    
    $("#interface_log_type").change(function(){
        $("#interface_log_queryForm #interface_log_type").val($("#interface_log_type").val());
    });   
</script>