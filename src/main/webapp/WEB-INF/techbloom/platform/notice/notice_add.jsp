
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div  id ="notice_add_page" class="row-fluid" style="height:inherit;">
<form id="notice_add_noticeForm" class="form-horizontal" onkeydown="if(event.keyCode==13)return false;" style=" height: calc(100% - 70px);">
		<input type="hidden" name="noticeId" id="notice_add_noticeId" value="">
		<div  class="row" style="margin:0;padding:0;">
			<div class="control-group">
				<label class="control-label" for="notice_add_title">公告标题：</label>
				<div class="controls">
				<div class="input-append span12 required" > 
				      <input class="span11" type="text" name="title" id="notice_add_title" placeholder="公告标题">
				    </div> 
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="notice_add_notice">公告内容：</label>
				<div class="controls">
				<div class="input-append span12 required" > 
				      <textarea class="span11" name="notice" id="notice_add_notice" rows="3" cols="20"></textarea>
				    </div> 
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" for="notice_add_deptId">所属部门：</label>
				<div class="controls">
					<input class="span11 select2_input" type="text" id="notice_add_deptId" ></input>
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" for="notice_add_userId">所属用户：</label>
				<div class="controls">
					<input class="span11 select2_input" type="text" name="userId" id="notice_add_userId" />
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" for="notice_add_remark">备注：</label>
				<div class="controls">
					<textarea class="span11" name="remark" id="notice_add_remark" rows="3" cols="20"></textarea>
				</div>
			</div> 
		</div>
	</form>
	<div class="form-actions" style="text-align: right;border-top: 0px;margin: 0px;">
	 <span  class="savebtn btn btn-success">保存</span>
     <span  class="btn btn-danger">取消</span>
	</div>
</div>

<script type="text/javascript">
(function(){
   	$("#notice_add_queryForm input[type=radio]").uniform();
   	$("input[type=radio][name=gender][value=1]").attr("checked",true).trigger("click");
	
   //部门下拉框初始化
	$("#notice_add_deptId").select2({
    	placeholder: "选择部门",
		minimumInputLength:0,   //至少输入n个字符，才去加载数据
	    allowClear: true,  //是否允许用户清除文本信息
		delay: 250,
		formatNoMatches:"没有结果",
		formatSearching:"搜索中...",
		formatAjaxError:"加载出错啦！",
		ajax : {
			url: context_path+"/dept/getDeptList",
			type:"POST",
			dataType : 'json',
			delay : 250,
			data: function (term,pageNo) {     //在查询时向服务器端传输的数据
	            term = $.trim(term);
                return {
                	queryString: term,    //联动查询的字符
                	pageSize: 15,    //一次性加载的数据条数
                    pageNo:pageNo,    //页码
                    time:new Date()   //测试
                 }
	        },
	        results: function (data,pageNo) {
	        		var res = data.result;
   	            if(res.length>0){   //如果没有查询到数据，将会返回空串
   	               var more = (pageNo*15)<data.total; //用来判断是否还有更多数据可以加载
   	               return {
   	                    results:res,more:more
   	                 };
   	            }else{
   	            	return {
   	            		results:{}
   	            	};
   	            }
	        },
			cache : true
		}

    });
    //所属用户
    $("#notice_add_userId").select2({
    	placeholder: "选择用户",
		minimumInputLength:0,   //至少输入n个字符，才去加载数据
	    allowClear: true,  //是否允许用户清除文本信息
		delay: 250,
		formatNoMatches:"没有结果",
		formatSearching:"搜索中...",
		formatAjaxError:"加载出错啦！",
		ajax : {
			url: context_path+"/user/getUserList",
			type:"POST",
			dataType : 'json',
			delay : 250,
			data: function (term,pageNo) {     //在查询时向服务器端传输的数据
	            term = $.trim(term);
                return {
                	queryString: term,    //联动查询的字符
                	pageSize: 15,    //一次性加载的数据条数
                    pageNo:pageNo,    //页码
                    time:new Date(),
                	deptId:$("#notice_add_deptId").val()//测试
                 }
	        },
	        results: function (data,pageNo) {
	        		var res = data.result;
   	            if(res.length>0){   //如果没有查询到数据，将会返回空串
   	               var more = (pageNo*15)<data.total; //用来判断是否还有更多数据可以加载
   	               return {
   	                    results:res,more:more
   	                 };
   	            }else{
   	            	return {
   	            		results:{}
   	            	};
   	            }
	        },
			cache : true
		}

    });
    
    $("#notice_add_userId").on("change.select2",function(){
        $("#notice_add_userId").trigger("keyup")}
    );
    
    $("#dnotice_add_eptId").on("change.select2",function(){
        $("#notice_add_deptId").trigger("keyup")}
    );
    
	//表单校验
	$("#notice_add_noticeForm").validate({
  		rules:{
  			"notice_add_title" : {
  				required:true,
  				maxlength:32
  			},
  			"notice_add_notice":{
  				required:true,
  				maxlength:256
  			},
  			"notice_add_userId":{
  				required:true
  			},
  			"notice_add_remark":{
  				maxlength:256
  			},
  			"notice_add_deptId":{
  				required:true
  			} 
  		},
  		messages:{
  			"notice_add_title" : {
  				required:"请输入标题",
  				remote:"您输入的标题已经存在，请重新输入！"
  			},
  			"notice_add_notice":{
  				required:"请输入通知内容"
  			},
  			"notice_add_userId":{
  				required:"请选择用户"
  			},
  			"notice_add_remark":{
  				maxlength:256
  			},
  			"notice_add_deptId":{
  				required:"请选择部门"
  			} 
  		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
		}
	});
	
	//确定按钮点击事件
    $("#notice_add_page .btn-success").off("click").on("click", function(){
		//用户保存
	  if($("#notice_add_noticeForm").valid()){
			saveNoticeInfo($("#notice_add_noticeForm").serialize());
		}
	});
	
	//取消按钮点击事件
	$("#notice_add_page .btn-danger").off("click").on("click", function(){
	    layer.close($queryWindow);
	});
	
	var ajaxStatus = 1;     //ajax请求状态：0不能请求，1可以请求
	//保存/修改用户信息
  	function saveNoticeInfo(bean){
  		if(bean){
  			if(ajaxStatus==0){
  				layer.msg("操作进行中，请稍后...",{icon:2});
  				return;
  			}
  			ajaxStatus = 0;    //将标记设置为不可请求
  			$(".savebtn").attr("disabled","disabled");
  			$.ajax({
  				url:context_path+"/notice/addNotice?tm="+new Date(),
  				type:"POST",
  				data:bean,
  				dataType:"JSON",
  				success:function(data){
  					ajaxStatus = 1; //将标记设置为可请求
  					$(".savebtn").removeAttr("disabled");
  					if(data){
  						layer.msg("保存公告成功！",{icon:1});
  						//刷新用户列表
  						$("#notice_add_grid-table").jqGrid('setGridParam', 
							{
								postData: {queryJsonString:""} //发送数据 
							}
						).trigger("reloadGrid");
  						//关闭当前窗口
  						//关闭指定的窗口对象
  						layer.close($queryWindow);
  					}else{
  						layer.msg("保存失败，请稍后重试！",{icon:2});
  					}
  				}
  			});
  		}else{
  			layer.msg("出错啦！",{icon:2});
  		}
  	}
}())

</script>