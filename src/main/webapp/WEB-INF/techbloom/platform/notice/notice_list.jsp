<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
 <div class="row-fluid" id="notice_list_grid-div" style="position:relative; margin-top: 0px; float: left;">
	 <!-- 隐藏区域：存放查询条件 -->
	<form id="notice_list_hiddenQueryForm" style="display:none;" action="<%=path%>/log/exportLog" method="post" target="_ifr">
		<input id="notice_list_id" name="id" value="" />
		<!-- 公告标题 -->
		<input name="title" id="notice_list_title" />
		<!-- 公告时间 -->
		<input id="notice_list_createtime" name="createtime">
	</form>
	<!-- 工具栏 -->
      <div class="row-fluid" id="notice_list_table_toolbar" style="padding:5px 3px; ">
            <form id="notice_list_queryForm" class="form-horizontal"  style="width: calc(100% - 160px);">
	    	  <span style="float: left;width: 45%; min-width: 80px; position: relative;">
	    	  <input class="span12" type="text"  value="${NOTICE.title }" name="title" id="notice_list_noticetitle" placeholder="公告标题" style=" min-height: 28px;border-right: 0px;"/>
	    	  <i class="icon-question-sign" style="position: absolute;top: 8px;right: 8px;color: #999;"></i></span>
	    	  <span style="float: left;width: 20%; min-width: 80px; position: relative;">
	    	  <input class="span12" type="text" readonly title="只能选择不可输入" name="operationTime" id="notice_list_operationTime" value="" 
	    	  placeholder="开始时间-结束时间" style="min-height: 28px; border-right: 0px;cursor: pointer;" />
	    	  <i class="icon-calendar" style="position: absolute;top: 8px;right: 8px;color: #999;"></i>
	    	  </span>	    	  
	       </form>
	       <!-- 在按钮上都添加上btn-addQX用来标记列表中的操作按钮，方便控制按钮的权限 -->
           <button class="col-md-1 btn btn-primary btn-queryQx" onclick="openQueryPage();">查询<i class="fa fa-search" aria-hidden="true" style="margin-left:5px;"></i></button>
           <button class="col-md-1 btn btn-primary btn-queryQx" onclick="openAddPage();">添加<i class="fa fa-plus" aria-hidden="true" style="margin-left:5px;"></i></button>
           <button class=" btn btn-primary btn-editQx" onclick="openEditPage();">
			编辑<i class="fa fa-pencil" aria-hidden="true" style="margin-left:5px;"></i>
		</button>
		<button class=" btn btn-primary btn-deleteQx" onclick="deleteNotice();">
			删除<i class="fa fa-trash" aria-hidden="true" style="margin-left:5px;"></i>
		</button>
      </div>
      <!-- 表格 -->
      <div class="row-fluid" style="padding:0 3px;">
      	<table class="table" id="notice_list_grid-table" style="width:100%;" ></table>
      	<div id="notice_list_grid-pager"></div>
      </div>
</div>
<script src="<%=request.getContextPath()%>/static/techbloom/notice/notice.js"></script>