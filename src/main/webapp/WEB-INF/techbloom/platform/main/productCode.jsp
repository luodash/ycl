<%@ page language="java" pageEncoding="UTF-8"%>
<style>
.dark {
  color: #333333 !important;
}
.white {
  color: #ffffff !important;
}
.red {
  color: #dd5a43 !important;
}
.red2 {
  color: #e08374 !important;
}
.light-red {
  color: #ff7777 !important;
}
.blue {
  color: #478fca !important;
}
.light-blue {
  color: #93cbf9 !important;
}
.green {
  color: #69aa46 !important;
}
.light-green {
  color: #b0d877 !important;
}
.orange {
  color: #ff892a !important;
}
.orange2 {
  color: #feb902 !important;
}
.light-orange {
  color: #fcac6f !important;
}
.purple {
  color: #a069c3 !important;
}
.pink {
  color: #c6699f !important;
}
.pink2 {
  color: #d6487e !important;
}
.brown {
  color: #a52a2a !important;
}
.grey {
  color: #777777 !important;
}
</style>
<script>
/**
* jquery tips 提示插件 jquery.tips.js v0.1beta
*
* 使用方法
* $(selector).tips({   //selector 为jquery选择器
*  msg:'your messages!',    //你的提示消息  必填
*  side:1,  //提示窗显示位置  1，2，3，4 分别代表 上右下左 默认为1（上） 可选
*  color:'#FFF', //提示文字色 默认为白色 可选
*  bg:'#F00',//提示窗背景色 默认为红色 可选
*  time:2,//自动关闭时间 默认2秒 设置0则不自动关闭 可选
*  x:0,//横向偏移  正数向右偏移 负数向左偏移 默认为0 可选
*  y:0,//纵向偏移  正数向下偏移 负数向上偏移 默认为0 可选
* })
*
*
*/
(function ($) {
    $.fn.tips = function(options){
        var defaults = {
            side:1,
            msg:'',
            color:'#FFF',
            bg:'#F00',
            time:2,
            x:0,
            y:0
        }
        var options = $.extend(defaults, options);
        if (!options.msg||isNaN(options.side)) {
        throw new Error('params error');
    }
    if(!$('#jquery_tips_style').length){
        var style='<style id="jquery_tips_style" type="text/css">';
        style+='.jq_tips_box{padding:10px;position:absolute;overflow:hidden;display:inline;display:none;z-index:9999999999999999;}';
        style+='.jq_tips_arrow{display:block;width:0px;height:0px;position:absolute;}';
        style+='.jq_tips_top{border-left:10px solid transparent;left:20px;bottom:0px;}';
        style+='.jq_tips_left{border-top:10px solid transparent;right:0px;top:18px;}';
        style+='.jq_tips_bottom{border-left:10px solid transparent;left:20px;top:0px;}';
        style+='.jq_tips_right{border-top:10px solid transparent;left:0px;top:18px;}';
        style+='.jq_tips_info{word-wrap: break-word;word-break:normal;border-radius:4px;padding:5px 8px;max-width:130px;overflow:hidden;box-shadow:1px 1px 1px #999;font-size:12px;cursor:pointer;}';
        style+='</style>';
        $(document.body).append(style);
    }
        this.each(function(){
            var element=$(this);
            var element_top=element.offset().top,element_left=element.offset().left,element_height=element.outerHeight(true),element_width=element.outerWidth(true);
            options.side=options.side<1?1:options.side>4?4:Math.round(options.side);
            var sideName=options.side==1?'top':options.side==2?'right':options.side==3?'bottom':options.side==4?'left':'top';
            var tips=$('<div class="jq_tips_box"><i class="jq_tips_arrow jq_tips_'+sideName+'"></i><div class="jq_tips_info">'+options.msg+'</div></div>').appendTo(document.body);
            tips.find('.jq_tips_arrow').css('border-'+sideName,'10px solid '+options.bg);
            tips.find('.jq_tips_info').css({
                color:options.color,
                backgroundColor:options.bg
            });
            switch(options.side){
                case 1:
                    tips.css({
                        top:element_top-tips.outerHeight(true)+options.x,
                        left:element_left-10+options.y
                    });
                    break;
                case 2:
                    tips.css({
                        top:element_top-20+options.x,
                        left:element_left+element_width+options.y
                    });
                    break;
                case 3:
                    tips.css({
                        top:element_top+element_height+options.x,
                        left:element_left-10+options.y
                    });
                    break;
                case 4:
                    tips.css({
                        top:element_top-20+options.x,
                        left:element_left-tips.outerWidth(true)+options.y
                    });
                    break;
                default:
            }
            var closeTime;
            tips.fadeIn('fast').click(function(){
                clearTimeout(closeTime);
                tips.fadeOut('fast',function(){
                    tips.remove();
                })
            })
            if(options.time){
                closeTime=setTimeout(function(){
                    tips.click();
                },options.time*1000);
                tips.hover(function(){
                    clearTimeout(closeTime);
                },function(){
                    closeTime=setTimeout(function(){
                        tips.click();
                    },options.time*1000);
                })
            }
        });
        return this;
    };
})(jQuery);
</script>
<div class="row-fluid" id="code_page" style="overflow-x: hidden;">
<form id="productCode_form" action="#" method="get" class="form-horizontal">
  <div class="control-group">
    <label class="control-label">上级包名:</label>
    <div class="controls">
    <div class="input-append span12 required">
      <input class="span11" type="text" placeholder="这里输入包名  (请不要输入特殊字符,请用纯字母)" name="packageName" id="packageName">
     </div>
           <span style=" padding: 5px 0px;     float: left;">例如:com.fh.controller.<font color="red" style="font-weight: bold;">system</font>红色部分</span>
     
    </div>
  </div>
    <div class="control-group">
    <label class="control-label">选择表:</label>
    <div class="controls">
				<input class="span11 select2_input" type="text" name="tableId" id="tableId" ></input><%--
      <font color="red" style="   padding: 5px 0px;  float: left;font-weight: bold;">类名首字母必须为大写字母或下划线</font>
    --%></div>
  </div>
  
  <div class="control-group">
    <label class="control-label">处理类名:</label>
    <div class="controls">
      <input class="span11" type="text" placeholder="这里输入处理类名称" name="objectName" id="objectName">
      <font color="red" style="   padding: 5px 0px;  float: left;font-weight: bold;">类名首字母必须为大写字母或下划线</font>
    </div>
  </div>

  <div class="control-group">
    <label class="control-label">表前缀：</label>
    <div class="controls">
      <input class="span11" type="text" name="tabletop" id="tabletop" value="TB_" placeholder="这里输入表前缀" style="width:60px" title="表前缀">
    </div>
  </div>
  </form>
  <div class="row-fluid" >
		<!-- 表格数据 -->
		<table id="code_grid-table" style="width:100%;"></table>
		<!-- 表格底部 -->
		<div id="code_grid-pager"></div>
  </div>
  <div class="form-actions" style="text-align: right;border-top: 0px;    margin: 0px;">

    <span  class="btn btn-success">生成</span>
    <span  class="btn btn-danger">取消</span>
  </div>


</div>
<script type="text/javascript">
	$("#tableId").select2({
		placeholder: "选择表",
		minimumInputLength:0,   //至少输入n个字符，才去加载数据
	    allowClear: true,  //是否允许用户清除文本信息
		delay: 250,
		formatNoMatches:"没有结果",
		formatSearching:"搜索中...",
		formatAjaxError:"加载出错啦！",
		ajax : {
			url: context_path+"/createCode/getTableName",
			type:"POST",
			dataType : 'json',
			delay : 250,
			data: function (term,pageNo) {     //在查询时向服务器端传输的数据
	            term = $.trim(term);
	            return {
	            	queryString: term,    //联动查询的字符
	            	pageSize: 15,    //一次性加载的数据条数
	                pageNo:pageNo,    //页码
	                time:new Date()   //测试
	             }
	        },
	        results: function (data,pageNo) {
	        		var res = data.result;
		            if(res.length>0){   //如果没有查询到数据，将会返回空串
		               var more = (pageNo*15)<data.total; //用来判断是否还有更多数据可以加载
		               return {
		                    results:res,more:more
		                 };
		            }else{
		            	return {
		            		results:{}
		            	};
		            }
	        },
			cache : true
		}
	}).change(function(){
		var tableName = $("#tableId").val();
		$.ajax({
			url : context_path+"/createCode/getTableCol",
			data : {tablename : tableName},
			success : function( data ){
				$("#code_page #code_grid-table").jqGrid("clearGridData");
				$("#code_page #code_grid-table").jqGrid('setGridParam', 
						{
					        data: data
						}
					).trigger("reloadGrid");
			}
		});
	});
	var grid_data = 
	[
	];
	jQuery(function($) {
		var grid_selector = "#code_page #code_grid-table";
		var pager_selector = "#code_grid-pager";
		
		//resize to fit page size
		$(window).on('resize.jqGrid', function () {
			$(grid_selector).jqGrid( 'setGridWidth', $("#code_page #productCode_form").width()-5 );
	    })
	
		

	
	
	
		jQuery(grid_selector).jqGrid({
			
			data: grid_data,
			datatype: "local",
			height: 180,
			colNames:[ '属性名','类型','备注',  '默认值'],
			colModel:[
				{name:'attr_name',index:'attr_name', width:200, editable: true},
				{name:'attr_type',index:'attr_type',width:90, editable:true, edittype:"select",editoptions:{value:"String:String;Integer:Integer;Date:Date"}},
				{name:'remark',index:'remark', width:150, sortable:false,editable: true,edittype:"textarea", editoptions:{rows:"2",cols:"10"}},
			/*	{name:'isedit',index:'isedit', width:70, editable: true,edittype:"checkbox",editoptions: {value:"是:否"},unformat: aceSwitch},
			  */{name:'default_v',index:'default_v', width:90, editable: true},
				
			], 

			rowNum : 20,
		    rowList : [ 10, 20, 30 ],
		    pager : pager_selector,
            altRows: true,
            viewrecords : true,
            caption : "列表",
            hidegrid:false,
            loadComplete : function(data) 
            {
            	var table = this;
            	setTimeout(function(){updatePagerIcons(table);}, 0);
            	oriData = data;
            },
            emptyrecords: "没有相关记录",
            loadtext: "加载中...",
            pgtext : "页码 {0} / {1}页",
            recordtext: "显示 {0} - {1}共{2}条数据"
	
		});
		$(window).triggerHandler('resize.jqGrid');//trigger window resize to make the grid get the correct size
	
		//switch element when editing inline
		function aceRadio( cellvalue, options, cell ) {
			setTimeout(function(){
				$(cell) .find('input[type=checkbox]')
					.addClass('ace ace-switch ace-switch-5')
					.after('<span class="lbl"></span>');
			}, 0);
		}
		function aceSwitch( cellvalue, options, cell ) {
			setTimeout(function(){
				$(cell) .find('input[type=checkbox]')
					.addClass('ace ace-switch ace-switch-5')
					.after('<span class="lbl"></span>');
			}, 0);
		}

		jQuery(grid_selector).jqGrid(
				'navGrid',
				pager_selector,
				{ // navbar options
					edit : true,
					editicon :'ui-icon ace-icon fa fa-pencil blue',
					add : true,
					addicon : 'ace-icon fa fa-plus-circle purple',
					del : true,
					delicon : 'ui-icon ace-icon fa fa-trash-o red',
					search : false,
					refresh : false,
					view : false
				});
		function  isSame( name,id ){
			var s =  true;
			$("#code_page #code_grid-table tr td:nth-child(1)").each(function(i,elm){
				if( $(elm).html().toUpperCase() ==  name && id != (i+1+"") ){
					s = false;
				}
			})
			return s;
		}
		function valiD( id ){
			var dname = $("#attr_name").val(); 	 		 //属性名
			var dtype = $("#attr_type").val(); 	 		 //类型
			var dbz	  = $("#remark").val();   	 		 //备注
			var isQian = $("#isedit").val(); 		 //是否前台录入
			var ddefault = $("#default_v").val(); 	 //默认值
			
			if(dname==""){
				$("#attr_name").tips({
					side:3,
		            msg:'输入属性名',
		            bg:'#AE81FF',
		            time:2
		        });
				$("#attr_name").focus();
				return false;
			}else{
				dname = dname.toUpperCase();		//转化为大写
				if(isSame(dname,id)){
					var headstr = dname.substring(0,1);
					var pat = new RegExp("^[0-9]+$");
					if(pat.test(headstr)){
						$("#attr_name").tips({
							side:3,
				            msg:'属性名首字母必须为字母或下划线',
				            bg:'#AE81FF',
				            time:2
				        });
						$("#attr_name").focus();
						return false;
					}
				}else{
					
						$("#attr_name").tips({
							side:3,
				            msg:'属性名重复',
				            bg:'#AE81FF',
				            time:2
				        });
						$("#attr_name").focus();
						return false;
				}
			}
			
			if(dbz==""){
				$("#remark").tips({
					side:3,
		            msg:'输入备注',
		            bg:'#AE81FF',
		            time:2
		        });
				$("#dbz").focus();
				return false;
			}
		}
		$("#code_page #add_code_grid-table").on("click",function(){
			setTimeout(function(){
				$("#sData").off("click").on("click",function(){
			    	if( valiD() === false ) return false;
			    	
			    	var row_data  =iTsai.form.serialize($('#FrmGrid_code_grid-table'));
			    	$(grid_selector).addRowData($("#code_page #code_grid-table tr").length +1, row_data,"last");  
			    	$(".ui-jqdialog-titlebar-close:visible").click();
					return false;
			    })
			});
			
		});
		$("#code_page #edit_code_grid-table").on("click",function(){
			setTimeout(function(){
		
				$("#sData").off("click").on("click",function(){
					var id = $("#code_page #code_grid-table").getGridParam("selrow");
					if( valiD(id) === false ) return false;
			    	
			    	var row_data  =iTsai.form.serialize($('#FrmGrid_code_grid-table'));
			    	$(grid_selector).setRowData( id , row_data);  
			    	$(".ui-jqdialog-titlebar-close:visible").click();
					return false;
			    })
			});
			
		});
		$("#code_page #del_code_grid-table").on("click",function(){
			setTimeout(function(){
				$("#dData").off("click").on("click",function(){
			    	$(grid_selector).delRowData( $("#code_page #code_grid-table").getGridParam("selrow") );  
			    	$(".ui-jqdialog-titlebar-close:visible").click();
					return false;
			    })
			});
			
		});
		
		$("#code_page .btn-success").on("click",function(){
			
			if($("#packageName").val()==""){
				$("#packageName").tips({
					side:3,
		            msg:'输入包名',
		            bg:'#AE81FF',
		            time:2
		        });
				$("#packageName").focus();
				return false;
			}else{
				var pat = new RegExp("^[A-Za-z]+$");
				if(!pat.test($("#packageName").val())){
					$("#packageName").tips({
						side:3,
			            msg:'只能输入字母',
			            bg:'#AE81FF',
			            time:2
			        });
					$("#packageName").focus();
					return false;
				}
			}
			
			if($("#objectName").val()==""){
				$("#objectName").tips({
					side:3,
		            msg:'输入类名',
		            bg:'#AE81FF',
		            time:2
		        });
				$("#objectName").focus();
				return false;
			}else{
				var headstr = $("#objectName").val().substring(0,1);
				var pat = new RegExp("^[a-z0-9]+$");
				if(pat.test(headstr)){
					$("#objectName").tips({
						side:3,
			            msg:'类名首字母必须为大写字母或下划线',
			            bg:'#AE81FF',
			            time:2
			        });
					$("#objectName").focus();
					return false;
				}
			}
			
			if($("#code_page #code_grid-table .jqgrow ").length == 0){
				$("#code_page #code_grid-table").tips({
					side:3,
		            msg:'请添加属性',
		            bg:'#AE81FF',
		            time:2
		        });
				return false;
			}
			//var bean = iTsai.form.serialize($("#code_page #productCode_form"));
			var bean =  $("#code_page #productCode_form").serialize();
			bean += "&attrList="+ JSON.stringify ($("#code_page #code_grid-table").jqGrid("getGridParam","data"));
			window.location = context_path+"/createCode/proCode?"+bean;
			
		})
		
	
	});
</script>