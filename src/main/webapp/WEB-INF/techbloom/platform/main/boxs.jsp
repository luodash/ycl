<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>	
<%
	String pathl = request.getContextPath();
%>

 <link rel="stylesheet" href="<%=request.getContextPath()%>/static/css/main_page.css" type="text/css">
 <style>
/*  .box .box-content{
   height: 127px;
} */
.ibox-title {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background-color: #ffffff;
  
    border:none;
    color: inherit;
    margin-bottom: 0;
    padding: 5px 10px;
}
.ibox-title h5 {
    display: inline-block;
    font-size: 13px;
    margin: 0 0 7px;
    padding: 0;
    text-overflow: ellipsis;
    float: left;
    font-family: "open sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
    color: rgb(123, 123, 123);
}
.ibox , ibox-title{
    border-radius: 0px;
}
.ibox-content h1{
    color: inherit;
   font-weight:100;
}
.bg-primary, .bg-primary .ibox-content{
  background: #00acec !important; 
  color: #fff !important;

  }
.bg-primary{
     border: 1px solid #00acec !important; 
}
.bg-primary .title, .bg-primary .fa-arrow-circle-right{
     color: #00acec !important;
}

.bg-error, .bg-error .ibox-content{
  background: #d9534f !important; 
  color: #fff !important;

  }
.bg-error{
     border: 1px solid #d9534f !important; 
}
.bg-error .title, .bg-error .fa-arrow-circle-right{
     color: #d9534f !important;
}

.bg-success, .bg-success .ibox-content{
  background: #5cb85c !important; 
  color: #fff !important;

  }
.bg-success{
     border: 1px solid #5cb85c !important; 
}
.bg-success .title, .bg-success .fa-arrow-circle-right{
     color: #5cb85c !important;
}
 </style>
 <div class="row-fluid" style="float: none;">
 
    <div class="span12 box" style="padding:9px 20px;margin:0px;">
        
        <div class="row-fluid">
			<div class="span3">
				<div class="ibox float-e-margins bg-primary">
					<div class=" ibox-content">
						<div class="row">
							<div class="span3">
								<i class="fa  icon-upload-alt" style=" font-size: 4em;"></i>
							</div>
							<div class="span9 text-right">
								<div class="outAmount" style="font-size:  40px;">1</div>
								<div>出库任务 </div>
							</div>
						</div>
					</div>
					<div class="ibox-title" style=" border-radius: 0px;"  onClick="$('#sidebar #31').click()">
						<h5 class="title">查看详情</h5>
						<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
					</div>

				</div>
			</div>
			<div class="span3">
				<div class="ibox float-e-margins bg-success">
					<div class=" ibox-content">
						<div class="row">
							<div class="span3">
								<i class="fa  icon-download-alt" style=" font-size: 4em;"></i>
							</div>
							<div class="span9 text-right" >
								<div class="inAmount" style="font-size:  40px;">1</div>
								<div>入库任务</div>
							</div>
						</div>
					</div>
					<div class="ibox-title" style=" border-radius: 0px;" onClick="$('#sidebar #26').click()">
						<h5 class="title">查看详情</h5>
						<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
					</div>

				</div>
			</div>
			<div class="span3">
				<div class="ibox float-e-margins bg-error">
					<div class=" ibox-content">
						<div class="row">
							<div class="span3">
								<i class="fa  icon-warning-sign" style=" font-size: 4em;"></i>
							</div>
							<div class="span9 text-right">
								<div class="abnormalAmount" style="font-size:  40px;">0</div>
								<div>异常情况</div>
							</div>
						</div>
					</div>
					<div class="ibox-title"    onClick="$('#sidebar #107').click()" style=" border-radius: 0px;">
						<h5 class="title">查看详情</h5>
						<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
					</div>

				</div>
			</div>
			<!--  <div class="span3">
                    <div class="ibox float-e-margins bg-primary">
                       <div class=" ibox-content">
                            <h1 class="no-margins " onClick="$('#sidebar #31').click()"> <i class="fa  icon-upload-alt"></i><span class="outAmount">0</span></h1>
                          
                        </div>
                        <div class="ibox-title" style=" border-radius: 0px;">
                            
                            <h5 class="title">查看详情</h5>
                        </div>
                      
                    </div>
                </div>
                 <div class="span3">
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <h1 class="no-margins text-success"  onClick="$('#sidebar #26').click()"> <i class="fa  icon-download-alt"></i><span class="inAmount">0</span></h1>
                          
                        </div>
                        <div class="ibox-title" style=" border-radius: 0px;">
                            
                            <h5 class="title">查看详情</h5>
                        </div>
                      
                    </div>
                </div>
                 <div class="span3">
                    <div class="ibox float-e-margins">
                         <div class="ibox-content">
                            <h1 class="no-margins text-error"  onClick="$('#sidebar #107').click()"><i class="fa icon-warning-sign"></i> <span class="abnormalAmount">0</span></h1>
                         
                        </div>
                        <div class="ibox-title" style="  border-radius: 0px;">
                            
                            <h5 class="title">查看详情</h5> 
                        </div>
                       
                    </div>
                </div> -->
         
            <div class="span3">
               
                <div class="box-content box-statistic" style="    margin-bottom: 0px;">
                  		<!-- <ul class="list-group clear-list" style="    margin-bottom: 0px;">
                            <li class="list-group-item fist-item" style="padding: 2px 0px;"><i style="font-size:12px ;color: #23c6c8;" class="icon icon-hand-right"></i>出库计划</li>
                            <li class="list-group-item" style="padding: 2px 0px;"><i style="font-size:12px ;color: #23c6c8;" class="icon icon-hand-right"></i>入库任务</li>
                            <li class="list-group-item" style="padding: 2px 0px;border-bottom: 1px solid #e7eaec;"><i style="font-size:12px ;color: #23c6c8;" class="icon icon-hand-right"></i>入库任务</li>
                        </ul> -->
                        
                        	<c:forEach items="${quickmenuList}" var="a">
	                        	 <li id="${a.menuId }">
		                        	<a style="cursor:pointer;" href="#" onclick="$('#sidebar #${a.menuId }').click()">
											<i class="menu-icon fa fa-caret-right"></i> ${a.menuName}
									</a> <b class="arrow"></b>
								 </li>
							 </c:forEach>
                </div>
                <li class="list-group-item selmenu" style="    padding: 1px 0px; border: 1px #ccc dashed;    margin-top: 9px;text-align: center;color: #979393;"><i class="fa  icon-plus" style="font-size: 12px;"></i>添加快捷菜单</li>
            </div>
          
        </div>
    </div>


</div>
	
<script type="text/javascript">
new dh_menu($(".selmenu"));
function setData(){
$.ajax({
		url : context_path + "/login/getAllDate", 
		success : function( data ){
			if( data ){
				for(var i in data){
					$("."+i).html(data[i]);
				}
				
			}
		}
	})
}
setData();
setInterval(function(){
	setData();
}, 4000);


$(document).ready(function(){
	$.ajax({
			type:"post",
			url : context_path + "/login/getQuickMenu", 
			success : function( data ){
				return data ;
			}
		})
	})


</script>
<!--

//-->
</script>
