<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<style>
 .box{
   background: #fff;
    padding: 10px;
    margin: 10px;
    border: 1px solid #ccc;
    border-radius: 2px;
 }
 .title_box{
       padding: 0px 0px 10px 0px;
    margin-bottom: 10px;
    font-size: 14px;
    font-weight:800
 }
 .icon1{
    background: #18abf5;
 }
 .icon2{
    background: #2f85d0;
 }
 .icon3{
    background: orange;
 }
 .icon4{
    background: red;
 }
 .icon1, .icon2, .icon3, .icon4{
    width: 8px;
    height: 8px;
    display: block;
    border-radius: 50%;
 }
 .tl{
    display: flex;flex-direction: row;align-items: center;justify-content: space-between; width: 100%;
 }
 .btn_left{
     padding: 5px 13px;
    border-top-left-radius: 3px;
    border-bottom-left-radius: 3px;
    background: #ccc;
    font-size: 12px;
    color: #fff;
    cursor: pointer;
 }
  .btn_right{
    padding: 5px 13px;
    border-top-right-radius: 3px;
    border-bottom-right-radius: 3px;
    background: #ccc;
    font-size: 12px;
    color: #fff;
    cursor: pointer;
 }
 .btn_right:hover,.btn_left:hover{
    background: #b1b1b1;
 }
 .btn_active{
    background: #2f85d0;
 }
 .btn_active:hover{
     background: #2f85d0;
 }
</style>

<div class="box" style="width: calc(50% - 42px);float: left;">
    <div class="title_box">库位占用率</div>
    <div class="content_box">
        <div id="main1" class="echarts" style="width:100%; height:300px;"></div>
    </div>
</div>

<div class="box" style="width: calc(50% - 42px);float: left;">
    <div class="title_box">库龄</div>
    <div class="content_box" style=" display: flex;align-items: center;justify-content: space-evenly;">
       <div id="main2" class="echarts" style="width: 200px; height:300px;"></div>
       <div style="width: 120px;">
            <div class="tl"><span class="icon1"></span><span>1年</span><span id="one"></span></div>
            <div class="tl"><span class="icon2"></span><span>2年</span><span id="two"></span></div>
            <div class="tl"><span class="icon3"></span><span>3年</span><span id="three"></span></div>
            <div class="tl"><span class="icon4"></span><span>其他</span><span id="other"></span></div>
       </div>
    </div>
</div>
<div class="box" style="   width:calc(100% - 42px);float: left;">
    <div class="title_box" style="display: flex;">人员工作量
        <div style="margin-left: 20px;font-weight: normal;">
            <span class="btn_left">出库</span>
            <span class="btn_right btn_active">入库</span>
        </div>
    </div>
    <div class="content_box">
        <div id="main3" class="echarts" style="width:100%; height:300px;"></div>
    </div>
</div>
<script src="<%=request.getContextPath()%>/static/js/techbloom/wms/mainPage/main_page.js"></script>

