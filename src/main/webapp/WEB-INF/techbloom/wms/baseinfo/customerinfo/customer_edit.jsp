<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<div id="supplier_edit_page" class="row-fluid" style="height: inherit;">
	<form id="customerForm" class="form-horizontal" style="overflow: auto; height: calc(100% - 70px);">
		<input type="hidden" id="id" name="id" value="${customer.id}">
		<div class="control-group">
			<label class="control-label" for="shipper">货主：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="shipper" name="shipper" value="${customer.shipper }" placeholder="货主">
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="receiver">收货人：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="receiver" name="receiver" value="${customer.receiver }" placeholder="收货人名称">
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="code">代码：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<c:choose>
						<c:when test="${edit==1}">
							<input type="text" class="span11" id="code" name="code" value="${customer.code }" placeholder="代码" readonly="readonly" title="代码不可修改">
						</c:when>
						<c:otherwise>
							<input type="text" class="span11" id="code" name="code" placeholder="后台自动生成" readonly="readonly">
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="contact">联系人：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="contact" name="contact" value="${customer.contact}" placeholder="联系人">
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="contactWay">联系方式：</label>
			<div class="controls">
				<input type="text" class="span11" name="contactWay" id="contactWay" placeholder="联系方式" value="${customer.contactWay}" />
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="address">地址：</label>
			<div class="controls">
				<input type="text" class="span11" id="address" name="address" value="${customer.address }" placeholder="地址">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="website">网址：</label>
			<div class="controls">
				<input type="text" class="span11" name="website" id="website" placeholder="网址" value="${customer.website}">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="email">Email：</label>
			<div class="controls">
				<input type="text" class="span11" name="email" id="email" placeholder="邮箱" value="${customer.email}">
			</div>
		</div>
	</form>
	<div class="field-button" style="text-align: center;border-top: 0px;margin: 15px auto;">
		<span class="btn btn-info" onclick="saveForm();">
		   <i class="ace-icon fa fa-check bigger-110"></i>保存
		</span>
		<span class="btn btn-danger" onclick="layer.closeAll();">
		   <i class="icon-remove"></i>&nbsp;取消
		</span>
	</div>
</div>
<script type="text/javascript">
$("#customerForm").validate({
		rules:{
            "shipper":{
  				required:true
  			},
  			"receiver":{
  				required:true
  			},
  			"contact":{
  				required:true
  			},
  			"email":{
  				accept:"[a-zA-Z0-9_]@*.com|cn|net$",
  				maxlength:256
  			},
  			"contactWay":{
  				mobile: true
  			}
  		},
  		messages:{
  			"shipper":{
  				required:"请输入货主！"
  			},
  			"receiver":{
  				required:"请输入收货人名称！"
  			},
  			"contact":{
  				required:"请输入联系人！"
  			},
  			"email":{
  				accept:"邮箱格式不正确",
  				maxlength:"长度不能超过256个字符！"
  			},
  			"contactWay":{
  			   accept:"联系方式格式不正确"
  			}
  		},
  		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
		}
  	});
	//确定按钮点击事件
    function saveForm() {
        if ($("#customerForm").valid()) {
            saveCustomerInfo($("#customerForm").serialize());
        }
    }
//保存/修改用户信息
function saveCustomerInfo(bean) {
    $.ajax({
            url: context_path + "/customerinfo/toSaveCustomer?tm=" + new Date(),
            type: "POST",
            data: bean,
            dataType: "JSON",
            success: function (data) {
                if (Boolean(data.result)) {
                    layer.msg("保存成功！", {icon: 1});
                    //关闭当前窗口
                    layer.close($queryWindow);
                    //刷新列表
                    gridReload();
                } else {
                    layer.alert("保存失败，请稍后重试！", {icon: 2});
                }
            },
            error:function(XMLHttpRequest){
        		alert(XMLHttpRequest.readyState);
        		alert("出错啦！！！");
        	}
        });
}
</script>