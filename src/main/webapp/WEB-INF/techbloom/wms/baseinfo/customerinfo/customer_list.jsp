<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<script type="text/javascript" src="<%=path%>/static/js/techbloom/wms/baseinfo/customer.js"></script>
<div id="customer_list_grid-div">
     <form id="customer_list_hiddenForm" action="<%=path%>/customerinfo/toExcel" method="POST" style="display: none;">
           <input id="customer_list_ids" name="ids" value=""/>
     </form>
     <form id="customer_list_hiddenQueryForm" style="display:none;">
           <input id="customer_list_code" name="code" value=""/>
           <input id="customer_list_shipper" name="shipper" value="">
           <input id="customer_list_receiver" name="receiver" value=""/>
           <input id="customer_list_address" name="address" value="">
     </form>
     <div class="query_box" id="customer_list_yy" title="查询选项">
          <form id="customer_list_queryForm" style="max-width:100%;">
			    <ul class="form-elements">
				    <li class="field-group field-fluid3">
					    <label class="inline" for="customer_list_code" style="margin-right:20px;width:100%;">
						       <span class="form_label">代码：</span>
						       <input id="customer_list_code" name="code" type="text" style="width: calc(100% - 70px);" placeholder="代码">
					    </label>			
				    </li>
				    <li class="field-group field-fluid3">
					    <label class="inline" for="customer_list_shipper" style="margin-right:20px;width:100%;">
						       <span class="form_label">货主：</span>
						       <input id="customer_list_shipper" name="shipper" type="text" style="width: calc(100% - 70px);" placeholder="货主">
					    </label>					
				    </li>
				    <li class="field-group field-fluid3">
					    <label class="inline" for="customer_list_receiver" style="margin-right:20px;width:100%;">
						       <span class="form_label">收货人：</span>
						       <input id="customer_list_receiver" name="receiver" type="text" style="width: calc(100% - 70px);" placeholder="收货人">
					    </label>			
				    </li>
				    <li class="field-group-top field-group field-fluid3">
					    <label class="inline" for="customer_list_address" style="margin-right:20px;width:100%;">
						       <span class="form_label" >地址：</span>
						       <input id="customer_list_address" name="address" type="text" style="width: calc(100% - 70px);" placeholder="地址">
					    </label>					
				    </li>
				   
			    </ul>
			     <div class="field-button" style="">
					     <div class="btn btn-info" onclick="queryOk();">
				              <i class="ace-icon fa fa-check bigger-110"></i>查询
			             </div>
				         <div class="btn" onclick="reset();"><i class="ace-icon icon-remove"></i>重置</div>
				         <a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>
		            </div>
		  </form>		 
    </div>
    <div id="customer_list_fixed_tool_div" class="fixed_tool_div">
        <div id="customer_list___toolbar__" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="customer_list_grid-table" style="width:100%;margin: 0px !important;boder:0px !important"></table>
    <div id="customer_list_grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
var context_path = '<%=path%>';
var oriData;
var _grid;
var dynamicDefalutValue="f1136d9e8dd542dfbafe42819fdb5e08";

$(function  (){
    $(".toggle_tools").click();
});

$("#customer_list___toolbar__").iToolBar({
    id: "customer_list___tb__01",
    items: [
         {label: "添加", disabled: ( ${sessionUser.addQx } == 1 ? false : true), onclick:addCustomer, iconClass:'glyphicon glyphicon-plus'},
         {label: "编辑", disabled: ( ${sessionUser.editQx} == 1 ? false : true),onclick: editCustomer, iconClass:'glyphicon glyphicon-pencil'},
         {label: "删除", disabled: ( ${sessionUser.deleteQx} == 1 ? false : true),onclick: delCustomer, iconClass:'glyphicon glyphicon-trash'},
         {label: "导出", disabled: ( ${sessionUser.queryQx}==1?false:true),onclick:function(){toExcel();},iconClass:' icon-share'}
    ]
});

$(function () {
    _grid = jQuery("#customer_list_grid-table").jqGrid({
            url: context_path + "/customerinfo/list.do",
            datatype: "json",
            colNames: ["客户主键", "货主", "收货人", "代码", "联系人", "联系方式", "地址", "网址", "邮箱"],
            colModel: [
                {name: "id", index: "id", width: 20, hidden: true},
                {name: "shipper", index: "shipper", width: 60},
                {name: "receiver",index: "receiver",width:60},
                {name: "code",index: "code",width:65},
                {name: "contact", index: "CONTACT", width: 60},
                {name: "contactWay", index: "contactWay", width: 60},
                {name: "address", index: "address", width: 80},
                {name: "website", index: "website", width: 100},
                {name: "email", index: "EMAIL", width: 135}
            ],
            rowNum: 20,
            rowList: [10, 20, 30],
            pager: "#customer_list_grid-pager",
            sortname: "code",
            sortorder: "asc",
            altRows: true,
            viewrecords: true,
            autowidth: true,
            multiselect: true,
            multiboxonly: true,
			beforeRequest:function (){
				dynamicGetColumns(dynamicDefalutValue,"customer_list_grid-table",$(window).width()-$("#sidebar").width() -7);
				//重新加载列属性
			},
            loadComplete: function (data) {
                var table = this;
                setTimeout(function () {
                    updatePagerIcons(table);
                    enableTooltips(table);
                }, 0);
                oriData = data;
            },
            emptyrecords: "没有相关记录",
            loadtext: "加载中...",
            pgtext: "页码 {0} / {1}页",
            recordtext: "显示 {0} - {1}共{2}条数据"
        });
        //在分页工具栏中添加按钮
        jQuery("#customer_list_grid-table").navGrid("#customer_list_grid-pager", {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false
        }).navButtonAdd("#customer_list_grid-pager", {
                    caption: "",
                    buttonicon: "ace-icon fa fa-refresh green",
                    onClickButton: function () {
                        $("#customer_list_grid-table").jqGrid("setGridParam",
                                {
                                    postData: {queryJsonString: ""} //发送数据
                                }
                        ).trigger("reloadGrid");
                    }
                }).navButtonAdd("#customer_list_grid-pager",{
                caption: "",
                buttonicon:"fa  icon-cogs",
                onClickButton : function (){
                    jQuery("#customer_list_grid-table").jqGrid("columnChooser",{
                        done: function(perm, cols){
                            dynamicColumns(cols,dynamicDefalutValue);
                            $("#customer_list_grid-table").jqGrid("setGridWidth", $("#customer_list_grid-div").width());
                        }
                    });
                }
            });
        $(window).on("resize.jqGrid", function () {
            $("#customer_list_grid-table").jqGrid("setGridWidth", $("#customer_list_grid-div").width() );
            $("#customer_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-
            $("#customer_list_yy").outerHeight(true)-$("#customer_list_fixed_tool_div").outerHeight(true)-
            $("#customer_list_grid-pager").outerHeight(true)-$("#gview_customer_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
        });
        $(window).triggerHandler("resize.jqGrid");
    });
    
    var _queryForm_data = iTsai.form.serialize($('#customer_list_queryForm'));
	function queryOk(){
		var queryParam = iTsai.form.serialize($('#customer_list_queryForm'));
		queryCustomerListByParam(queryParam);
		
	}
	
	function reset(){
		iTsai.form.deserialize($('#customer_list_queryForm'),_queryForm_data); 
		queryCustomerListByParam(_queryForm_data);
		
	}
	
	function toExcel(){
		var selectid = getId("#customer_list_grid-table","id");
		function getId(_grid,_key){
		    var idAddr = jQuery(_grid).getGridParam("selarrrow");
		    var ids="";
		    ids=idAddr+",";
		    return ids.substring(0, ids.lastIndexOf(','));
		}
		$("#customer_list_ids").val(selectid);
		$("#customer_list_hiddenForm").submit();
	}
	
	function print(){
		var url = context_path + "/customerinfo/toPrint" ;
		window.open(url);
	}
	
	function toExcel(){
	    var ids = jQuery("#customer_list_grid-table").jqGrid("getGridParam", "selarrrow");
	    $("#customer_list_hiddenForm #customer_list_ids").val(ids);
	    $("#customer_list_hiddenForm").submit();	
	}
</script>
</html>