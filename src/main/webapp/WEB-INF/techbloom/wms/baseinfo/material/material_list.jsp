<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
	var context_path = '<%=path%>';
</script>
<!-- 物料管理页面 -->
<body style="overflow:hidden;">
	<div id="material_list_grid-div">
	<!-- 隐藏区域：存放查询条件 -->
	    <form id="material_list_hiddenTempForm" action="<%=path%>/material/downLoadFile" method="get"  style="display:none;">
	      <input type="hidden" id="material_list_materialId" name="materialId">
	      <input type="hidden" id="material_list_uploadtype" name="type">
	    </form>
		<form id="material_list_hiddenQueryFormprint" action="<%=path%>/material/standCropPrint" method="get"  style="display:none;">
			<input name="ids" id="ids" value="" />
			<!-- 物料编码 -->
			<input id="material_list_materialNoE" name="materialNo" value="">
			<!-- 物料名称 -->
			<input name="materialName" id="material_list_materialNameE" value="" />
			<!-- 规格 -->
			<input name="specification" id="material_list_materialSpecE" value="" />
			<!-- 封装 -->
			<input name="package" id="material_list_materialPackageE" value="" />
			<!-- 生产商 -->
			<input name="producer" id="material_list_materialProducerE" value="" />
			<!-- 品牌 -->
			<input name="brand" id="material_list_materialBrandE" value="" />
			<!-- 标准重量 -->
			<input name="weight" id="material_list_materialWeightE" value="" />
			<!-- 尺寸 -->
			<input name="size" id="material_list_materialSizeE" value="" />
			<!-- 状态描述 -->
			<input name="description" id="material_list_materialDescriptionE" value="" />
			<!-- 物料等级 -->
			<input name="level" id="material_list_materialLevelE" value="" />
			<!-- 单价 -->
			<input name="price" id="material_list_materialPrice" value="" />
			<!-- 供应商 -->
			<input name="supplyId" id="material_list_materialSupplyIdE" value="" />
			<!-- 代码 -->
			<input name="code" id="material_list_materialCodeE" value="" />
			<!-- 条码 -->
			<input name="batchNo" id="material_list_materialBatchNoE" value="" />
			<!-- 货品ABC类 -->
			<input name="materialTypeId" id="material_list_materialTypeIdE" value="" />
			<!-- 拣选分类 -->
			<input name="pickType" id="material_list_materialPickTypeE" value="" />
			<!-- 关键物料标识 -->
			<input name="mark" id="material_list_materialMarkE" value="" />
		</form>
		<form id="material_list_hiddenQueryFormExcel" action="<%=path%>/material/materialExcel" method="POST"  style="display:none;">
			<input name="ids" id="ids" value="" />
			<!-- 物料编码 -->
			<input id="material_list_materialNoE" name="materialNo" value="">
			<!-- 物料名称 -->
			<input name="materialName" id="material_list_materialNameE" value="" />
			<!-- 规格 -->
			<input name="specification" id="material_list_materialSpecE" value="" />
			<!-- 封装 -->
			<input name="package" id="material_list_materialPackageE" value="" />
			<!-- 生产商 -->
			<input name="producer" id="material_list_materialProducerE" value="" />
			<!-- 品牌 -->
			<input name="brand" id="material_list_materialBrandE" value="" />
			<!-- 标准重量 -->
			<input name="weight" id="material_list_materialWeightE" value="" />
			<!-- 尺寸 -->
			<input name="size" id="material_list_materialSizeE" value="" />
			<!-- 状态描述 -->
			<input name="description" id="material_list_materialDescriptionE" value="" />
			<!-- 物料等级 -->
			<input name="level" id="material_list_materialLevelE" value="" />
			<!-- 单价 -->
			<input name="price" id="material_list_materialPrice" value="" />
			<!-- 供应商 -->
			<input name="supplyId" id="material_list_materialSupplyIdE" value="" />
			<!-- 代码 -->
			<input name="code" id="material_list_materialCodeE" value="" />
			<!-- 条码 -->
			<input name="batchNo" id="material_list_materialBatchNoE" value="" />
			<!-- 货品ABC类 -->
			<input name="materialTypeId" id="material_list_materialTypeIdE" value="" />
			<!-- 拣选分类 -->
			<input name="pickType" id="material_list_materialPickTypeE" value="" />
			<!-- 关键物料标识 -->
			<input name="mark" id="material_list_materialMarkE" value="" />
		</form>
		<form id="material_list_hiddenQueryForm" style="display:none;">
			<!-- 物料编码 -->
			<input id="material_list_materialNoE" name="materialNo" value="">
			<!-- 物料名称 -->
			<input name="materialName" id="material_list_materialNameE" value="" />
			<!-- 规格 -->
			<input name="specification" id="material_list_materialSpecE" value="" />
			<!-- 封装 -->
			<input name="package" id="material_list_materialPackageE" value="" />
			<!-- 生产商 -->
			<input name="producer" id="material_list_materialProducerE" value="" />
			<!-- 品牌 -->
			<input name="brand" id="material_list_materialBrandE" value="" />
			<!-- 标准重量 -->
			<input name="weight" id="material_list_materialWeightE" value="" />
			<!-- 尺寸 -->
			<input name="size" id="material_list_materialSizeE" value="" />
			<!-- 状态描述 -->
			<input name="description" id="material_list_materialDescriptionE" value="" />
			<!-- 物料等级 -->
			<input name="level" id="material_list_materialLevelE" value="" />
			<!-- 单价 -->
			<input name="price" id="material_list_materialPrice" value="" />
			<!-- 供应商 -->
			<input name="supplyId" id="material_list_materialSupplyIdE" value="" />
			<!-- 代码 -->
			<input name="code" id="material_list_materialCodeE" value="" />
			<!-- 条码 -->
			<input name="batchNo" id="material_list_materialBatchNoE" value="" />
			<!-- 货品ABC类 -->
			<input name="materialTypeId" id="material_list_materialTypeIdE" value="" />
			<!-- 拣选分类 -->
			<input name="pickType" id="material_list_materialPickTypeE" value="" />
			<!-- 关键物料标识 -->
			<input name="mark" id="material_list_materialMarkE" value="" />
		</form>
		 <div class="query_box" id="material_list_yy" title="查询选项">
            <form id="material_list_queryForm" style="max-width:100%;">
			 <ul class="form-elements">
				<!-- 货架编号 -->
				<li class="field-group field-fluid3">
					<label class="inline" for="material_list_materialNo" style="margin-right:20px;width:100%;">
						<span class='form_label' style="width:80px;">物料编号：</span>
						<input type="text" name="materialNo" id="material_list_materialNo" value="" style="width: calc(100% - 85px);" placeholder="物料编号">
					</label>		
				</li>
				<!-- 货架名称 -->
				<li class="field-group field-fluid3">
					<label class="inline" for="material_list_materialName" style="margin-right:20px;width:100%;">
						<span class='form_label' style="width:80px;">物料名称：</span>
						<input type="text" name="materialName" id="material_list_materialName" value="" style="width: calc(100% - 85px);" placeholder="物料名称">
					</label>       					
				</li>
				<!-- 第二行 3个li标签 ，第一个li标签的class要加 field-group-top  ，  -->
				<li class=" field-group-top field-group field-fluid3">
					<label class="inline" for="material_list_brand" style="margin-right:20px;width:100%;">
						<span class='form_label' style="width:80px;">品牌：</span>
						<input type="text" name="brand" id="material_list_brand" value="" style="width: calc(100% - 85px);" placeholder="品牌">
					</label>					
				</li>			
				
			</ul>
			<div class="field-button" style="">
						<div class="btn btn-info" onclick="queryOk();">
				            <i class="ace-icon fa fa-check bigger-110"></i>查询
			            </div>
						<div class="btn" onclick="reset();"><i class="ace-icon icon-remove"></i>重置</div>
						<a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>
		        </div>
		  </form>		 
        </div>
        <div id="material_list_fixed_tool_div" class="fixed_tool_div">
             <div id="material_list___toolbar__" style="float:left;overflow:hidden;"></div>
        </div>
		<table id="material_list_grid-table" style="width:100%;height:100%;"></table>
		<div id="material_list_grid-pager"></div>
   </div>
</body>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript" src="<%=path%>/static/js/techbloom/wms/baseinfo/material2.js"></script>
<script type="text/javascript">
var context_path = '<%=path%>';
var oriData;      //表格数据
var _grid;        //表格对象
var materialId;
var dynamicDefalutValue="cbe154ed57f14f8e803c4d0982a4d17c";
$(function  (){
    $(".toggle_tools").click();
});
$("#material_list___toolbar__").iToolBar({
 	 id:"material_list___tb__01",
 	 items:[
 	    {label:"添加",disabled:(${sessionUser.addQx}==1?false:true),onclick:add,iconClass:'glyphicon glyphicon-plus'},
 	    {label:"编辑",disabled:(${sessionUser.editQx}==1?false:true),onclick:editMaterial,iconClass:'glyphicon glyphicon-pencil'},
  	  	{label:"删除",disabled:(${sessionUser.deleteQx}==1?false:true),onclick:delMaterial,iconClass:'glyphicon glyphicon-trash'} ,
  	  	{label:"导入",disabled:(${sessionUser.queryQx}==1?false:true),onclick:function(){materialExportIn();},iconClass:' icon-share'},
  	  	{label:"导出",disabled:(${sessionUser.queryQx}==1?false:true),onclick:function(){aaa();},iconClass:' icon-share'},
  	  	{label:"模板下载",disabled:(${sessionUser.queryQx}==1?false:true),onclick:function(){templet();},iconClass:'icon-cloud-download'},
  	  	{label:"打印",disabled:(${sessionUser.queryQx}==1?false:true),onclick:standCropPrint,iconClass:'icon-print'}
  	  ]
});


//打印功能
function standCropPrint() {
	var selectid = getId("#material_list_grid-table","id");
	function getId(_grid,_key){
	    var idAddr = jQuery(_grid).getGridParam("selarrrow");
	    var ids="";
	    ids=idAddr+",";
	    return ids;
	}
	if(selectid.length>1){
		selectid = selectid.substring(0,selectid.length-1);
		$("#material_list_hiddenQueryFormprint #material_list_ids").val(selectid);
	}
	var name = $("#material_list_queryForm #material_list_materialName").val();
	var materialNo = $("#material_list_queryForm #material_list_materialNo").val();
	var materialUnit = $("#material_list_queryForm #material_list_materialUnit").val();
	var materialTypeId = $("#material_list_queryForm #material_list_materialTypeId").val();
	var materialSpec = $("#material_list_queryForm #material_list_materialSpec").val();
	
	$("#material_list_hiddenQueryFormprint #material_list_materialNameE").val(name);
	$("#material_list_hiddenQueryFormprint #material_list_materialNoE").val(materialNo);
	$("#material_list_hiddenQueryFormprint #material_list_materialUnitE").val(materialUnit);
	$("#material_list_hiddenQueryFormprint #material_list_materialTypeIdE").val(materialTypeId);
	$("#material_list_hiddenQueryFormprint #material_list_materialSpecE").val(materialSpec);
	var url = context_path + "/material/toPrint.do?ids=" + $("#material_list_hiddenQueryFormprint #material_list_ids").val()+
			"&materialName="+name+"&materialNo="+materialNo+"&materialUnit="+materialUnit+"&materialTypeIds="+materialTypeId+
			"&materialSpec="+materialSpec;
    window.open(url);
	
}

function templet(){
  var newUrl=context_path+"/"+"material/downLoadFile"
  $("#material_list_hiddenTempForm").attr("action",newUrl);
  $("#material_list_hiddenTempForm").submit();
}
//导出功能
function aaa(){
	var selectid = getId("#material_list_grid-table","id");
	function getId(_grid,_key){
	    var idAddr = jQuery(_grid).getGridParam("selarrrow");
	    var ids="";
	    ids=idAddr+",";
	    return ids;
	}
	if(selectid.length>1){
		selectid = selectid.substring(0,selectid.length-1);
		$("#material_list_hiddenQueryFormExcel #material_list_ids").val(selectid);
	}
	var name = $("#material_list_queryForm #material_list_materialName").val();
	var materialNo = $("#material_list_queryForm #material_list_materialNo").val();
	var materialUnit = $("#material_list_queryForm #material_list_materialUnit").val();
	var materialTypeId = $("#material_list_queryForm #material_list_materialTypeId").val();
	var materialSpec = $("#material_list_queryForm #material_list_materialSpec").val();
	
	$("#material_list_hiddenQueryFormExcel #material_list_materialNameE").val(name);
	$("#material_list_hiddenQueryFormExcel #material_list_materialNoE").val(materialNo);
	$("#material_list_hiddenQueryFormExcel #material_list_materialUnitE").val(materialUnit);
	$("#material_list_hiddenQueryFormExcel #material_list_materialTypeIdE").val(materialTypeId);
	$("#material_list_hiddenQueryFormExcel #material_list_materialSpecE").val(materialSpec);
	$("#material_list_hiddenQueryFormExcel").submit();
}

$(function(){
	//初始化表格
	_grid = jQuery("#material_list_grid-table").jqGrid({
       url : context_path + "/material/materialList",
       datatype : "json",
       colNames : [ "物料主键","物料编码","物料名称","物料类别名称","供应商","单位","规格","生产商","品牌", "标准重量",
    	   			"尺寸","单价","拣选分类","等级","描述","图片操作","PDF操作"
    	   			],
       colModel : [
					{name : "id",index : "id",hidden:true}, 
					{name : "materialNo",index : "materialNo",width : 33}, 
                    {name : "materialName",index : "materialName",width : 33}, 
                    {name : "materialTypeName",index : "materialTypeName",width : 33}, 
                    {name : "supplyName",index : "supplyName",width : 33},
                    {name : "unit",index : "unit",width : 33},
                    {name : "specification",index : "specification",width : 33},                    
                    {name : "producer",index : "producer",width : 33}, 
                    {name : "brand",index : "brand",width : 33}, 
                    {name : "weight",index : "weight",width : 33},
                    {name : "size",index : "size",width : 33},                       
                    {name : "price",index : "price",width : 33},
                    {name : "pickTypeName",index : "pickTypeName",width : 33},
                    {name : "level",index : "level",width : 33},
                    {name : "description",index : "description",width : 33},
                    {name : "",index : "",width : 33,
                      formatter:function(cellValu,option,rowObject){
                        return "<div style='margin-bottom:2px' class='btn btn-xs btn-success' onclick='upload("+1+","+rowObject.id+");'>上传</div>"+
                               "<div style='margin-bottom:2px' class='btn btn-xs btn-success' onclick='download("+1+","+rowObject.id+");'>下载</div>";
                      }
                    },
                    {name : "",index : "",width : 33,
                      formatter:function(cellValu,option,rowObject){
                        return "<div style='margin-bottom:2px' class='btn btn-xs btn-success' onclick='upload("+2+","+rowObject.id+");'>上传</div>"+
                               "<div style='margin-bottom:2px' class='btn btn-xs btn-success' onclick='download("+2+","+rowObject.id+");'>下载</div>";
                      }
                    },
                    
                  ],
       rowNum : 20,
       rowList : [ 10, 20, 30 ],
       pager : "#material_list_grid-pager",
       sortname : "MATERIAL_NO",
       sortorder : "asc",
       altRows: true,
       viewrecords : true,
       autowidth:true,
       multiselect:true,
	   multiboxonly: true,
        beforeRequest:function (){
            dynamicGetColumns(dynamicDefalutValue,"material_list_grid-table",$(window).width()-$("#sidebar").width() -7);
            //重新加载列属性
        },
       loadComplete : function(data)
       {
           var table = this;
           setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
           oriData = data;
       },
	   emptyrecords: "没有相关记录",
	   loadtext: "加载中...",
	   pgtext : "页码 {0} / {1}页",
	   recordtext: "显示 {0} - {1}共{2}条数据"
  });
  //在分页工具栏中添加按钮
  jQuery("#material_list_grid-table").navGrid("#material_list_grid-pager",{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd('#material_list_grid-pager',{
	   caption:"",
	   buttonicon:"ace-icon fa fa-refresh green",
	   onClickButton: function(){
	     $("#material_list_grid-table").jqGrid("setGridParam",
				{
					postData: {jsonString:""} //发送数据
				}
		  ).trigger("reloadGrid");
	   }
	}).navButtonAdd("#material_list_grid-pager",{
          caption: "",
          buttonicon:"fa icon-cogs",
          onClickButton : function (){
              jQuery("#material_list_grid-table").jqGrid("columnChooser",{
                  done: function(perm, cols){
                      dynamicColumns(cols,dynamicDefalutValue);
                      $("#material_list_grid-table").jqGrid("setGridWidth", $("#material_list_grid-div").width());
                  }
              });
          }
      });

	$(window).on("resize.jqGrid", function () {
		$("#material_list_grid-table").jqGrid("setGridWidth", $("#material_list_grid-div").width() );
		$("#material_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-
		$("#material_list_yy").outerHeight(true)-$("#material_list_fixed_tool_div").outerHeight(true)-
		$("#material_list_grid-pager").outerHeight(true)-$("#gview_material_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
	})
	$(window).triggerHandler("resize.jqGrid");    
});
var _queryForm_data = iTsai.form.serialize($("#material_list_queryForm"));
function queryOk(){
	var queryParam = iTsai.form.serialize($("#material_list_queryForm"));
	//执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
	queryInstoreListByParam(queryParam);
	
}
function reset(){
	iTsai.form.deserialize($("#material_list_queryForm"),_queryForm_data); 
	//执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
	queryInstoreListByParam(_queryForm_data);
	
}
//弹出物料类别选择窗口
	function openSelectWindow(){
		$.get( context_path + "/material/getMaterialTypeList?mid="+(-1)).done(function(data){
			childDiv = layer.open({
			    title : "物料类别", 
		    	type:1,
		    	skin : "layui-layer-molv",
		    	area : ['500px', '300px'],
		    	shade : 0.6, //遮罩透明度
			    moveType : 1, //拖拽风格，0是默认，1是传统拖动
			    anim : 2,
			    content : data
			});
		});
	}
	
//为指定id的元素赋值
function setValueForElementById(elementId,value){
	$('#material_list_queryForm #'+elementId).val(value);
}
function upload(type,id){
  materialId=id;
  if(type==1){
  $.get( context_path + "/material/toUploadImg").done(function(data){
		childquer=layer.open({
		    title : "物料图片上传", 
	    	type:1,
	    	skin : "layui-layer-molv",
	    	area : "600px",
	    	shade : 0.6, //遮罩透明度
		    moveType : 1, //拖拽风格，0是默认，1是传统拖动
		    anim : 2,
		    content : data	    	
		});
	});
  }
  if(type==2){
  $.get( context_path + "/material/toUploadPDF").done(function(data){
		childquer=layer.open({
		    title : "物料PDF上传", 
	    	type:1,
	    	skin : "layui-layer-molv",
	    	area : "600px",
	    	shade : 0.6, //遮罩透明度
		    moveType : 1, //拖拽风格，0是默认，1是传统拖动
		    anim : 2,
		    content : data
		});
	});
  }
}
	function download(type,id) {  
        $.ajax({
         url:context_path + '/material/fileExists?type='+type+'&id='+id,
	     type:"POST",
	     dataType:"json",
		 contentType: false,  
		 processData: false,
	     success:function(data){
		     if(Boolean(data.result)){
		         var newUrl=context_path+"/"+"material/downLoadFilesPackage"
		         $("#material_list_materialId").val(id);
	             $("#material_list_uploadtype").val(type);
	             $("#material_list_hiddenTempForm").attr("action",newUrl);
	             $("#material_list_hiddenTempForm").submit();
		     }else{
		        layer.alert("未上传文件！");
		     }
	     }      
       })
    }
</script>