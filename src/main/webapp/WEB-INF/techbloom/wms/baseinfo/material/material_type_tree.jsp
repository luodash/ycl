<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
	<script type="text/javascript">
		var context_path = "<%=path%>";
		//后台返回之后调用的js方法
		function result(res){
			if(res!=null){
				if($("#material_type_tree_materialForm").val() == undefined){
					$("#material_type_tree_materialTypeName").val(res.typeName);
					$("#material_type_tree_materialTypeId").val(res.id);
				}else{
					$("#material_type_tree_materialForm #material_type_tree_materialTypeName").val(res.typeName);
					$("#material_type_tree_materialForm #material_type_tree_materialTypeId").val(res.id);
				}
				
		  		//关闭当前窗口
		  		layer.close(childEditDiv);
			}else{
				if($("#material_type_tree_materialForm").val() == undefined){
					$("#material_type_tree_materialTypeName").val("");
					$("#material_type_tree_materialTypeId").val("");
				}else{
					$("#material_type_tree_materialForm #material_type_tree_materialTypeName").val("");
					$("#material_type_tree_materialForm #material_type_tree_materialTypeId").val("");
				}
		  		//关闭当前窗口
		  		layer.close(childEditDiv);
			}
		}
	</script>
  	<!-- 物料类别树 -->
 <div class="container" style="width: 100%;height:100%;overflow: hidden;">
	<div id="material_type_tree_materialTreeDiv" class="ztree" style="height:184px;overflow-y:scroll;"></div>
    <form action="<%=path %>/material/getMaterialTypeId" id="material_type_tree_materialForm-c" name="materialTypeForm-c" method="post" target="_ifr" style="margin-left: -900px">
  	    <input type="hidden" id="material_type_tree_typeId" name="typeId" >
  	    <input type="hidden" id="material_type_tree_typeName" name="typeName" > 	  
	</form>
	<div class="field-button" style="text-align: center;border-top: 0px;margin: 15px auto;">
		<span class="btn btn-info" onclick="saveType();">
		   <i class="ace-icon fa fa-check bigger-110"></i>确认
		</span>
		<span class="btn btn-danger" onclick="layer.close(childEditDiv);">
		   <i class="icon-remove"></i>&nbsp;取消
		</span>
	</div>
 </div>
  <iframe src="about:blank" name="_ifr" height="0" class="hidden"></iframe>
<script type="text/javascript">
    // 声明一个根节点
    var dataJson = ${TYPETREE};
    var setting = {
        data: {
            simpleData: {
                enable: true,
                idKey: "id",
                pIdKey: "pid"
            },
            key: {
                name: "text"
            }
        },
        callback: {
            onClick: function(evt, id, node){
                $('#material_type_tree_typeId').val(node.id);
                $('#material_type_tree_typeName').val(node.text);
            }
        }
    };
    // 使用根节点对象的 loadData() 方法加载 JSON 数据
    var complexTree = $.fn.zTree.init($("#material_type_tree_materialTreeDiv"), setting, dataJson);;
</script>
  <script type="text/javascript">
  	function saveType(){
  	var typeId = $("#material_type_tree_materialForm-c #material_type_tree_typeId").val();
    	var typename = $("#material_type_tree_materialForm-c #material_type_tree_typeName").val();
    	if(typeId.indexOf("s_")!=-1){
    		layer.msg("该级别不可选择！",{time:1200,icon:2});
    	}else{
			$("#material_type_tree_materialForm #material_type_tree_materialTypeId").val(typeId);
			$("#material_type_tree_materialForm #material_type_tree_materialTypeName").val(typename);
			$.ajax({
				url: context_path + "/material/getmatTypeNo?materialTypeId="+$("#material_type_tree_materialForm-c #material_type_tree_typeId").val(),
				type: "POST",
				dataType: "JSON",
				success: function (data) {
					$("#material_type_tree_materialForm #material_type_tree_materialNo").val(data.data);
				}
			});
			layer.close(childEditDiv);
		}	
  	}
  </script>
