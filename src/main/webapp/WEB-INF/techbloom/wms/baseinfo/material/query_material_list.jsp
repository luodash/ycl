<%@ page language="java" pageEncoding="UTF-8"%>
<%-- <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> --%>
<%
	String path = request.getContextPath();
%>
	<script type="text/javascript">
		var context_path = "<%=path%>";
	</script>
	<link rel="stylesheet" href="<%=path%>/plugins/ace/assets/css/select2.css" />
	<link rel="stylesheet" href="<%=path%>/plugins/ace//assets/css/font-awesome.css" />
	<style type="text/css">
	</style>
  </head>
  <body>
  	<div style="padding-left:30px;">
   	    <form id="query_material_list_queryForm1">
			<ul class="form-elements">
				<!-- 物料名称 -->
				<li class="field-group">
					<label class="inline" for="query_material_list_materialName" style="margin-right:20px;">
						物料名称：
						<input type="text" name="materialName" id="query_material_list_materialName" value="${MATERIAL.materialName }"
						style="width: 200px;" placeholder="物料名称"/>
					</label>
				</li>
				<!-- 物料编号 -->
				<li class="field-group">
					<label class="inline" for="query_material_list_materialNo" style="margin-right:20px;">
						物料编号：
						<input type="text" name="materialNo" id="query_material_list_materialNo" value="${MATERIAL.materialNo }"
						style="width: 200px;" placeholder="物料编号"/>
					</label>
				</li>
				<!-- 物料单位 -->
				<li class="field-group">
					<label class="inline" for="query_material_list_materialUnit" style="margin-right:20px;">
						物料单位：
						<input type="text" name="materialUnit" id="query_material_list_materialUnit" value="${MATERIAL.materialUnit }"
						style="width: 200px;" placeholder="物料单位"/>
					</label>
				</li>
				<!-- 物料类别 -->
				<li class="field-group">
				<label class="inline" for="query_material_list_supplierIdSelect">
					物料类别：
					<!-- 隐藏的物料类别主键 -->
	       			<input type="hidden" id="query_material_list_materialTypeId" name="materialTypeId" value="${MTYPE.id }">
	       			<!-- 物料类别名称 -->
					<input type="text" title="点击选择物料类别" id="query_material_list_materialTypeName" name="materialTypeName" value="${MTYPE.typeName }"
					style="width: 200px;" onclick="openSelectWindow()" readonly="readonly" placeholder="点击选择物料类别" >
				</label>
				</li>
				
				<!-- 规格参数-->
				<li class="field-group">
				<label class="inline form-group" for="query_material_list_materialSpec" style="margin-right:20px;">
					规格参数：
					<input type="text" id="query_material_list_materialSpec" name="materialSpec" value="${MATERIAL.materialSpec }"
					style="width: 200px;" placeholder="规格参数">
				</label>
				</li>
				
				
				<!-- 底部工具按钮 -->
				<li class="field-group">
					<div class="field-button">
						<div class="btn btn-info" onclick="queryOk();">
				            <i class="ace-icon fa fa-check bigger-110"></i>确定
			            </div> &nbsp; &nbsp;
						<div class="btn" onclick="layer.closeAll();"><i class="icon-remove"></i>&nbsp;取消</div>
					</div>
				</li>
			</ul>
		</form>
  	</div>
  	<script type="text/javascript" src="<%=path%>/plugins/public_components/js/select2.js"></script>
  <script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
  <script type="text/javascript">
  
		function queryOk(){
			var queryParam = iTsai.form.serialize($('#query_material_list_queryForm1'));
			queryInstoreListByParam(queryParam);
	    	layer.closeAll();
		}
		
		//弹出物料类别选择窗口
		function openSelectWindow(){
			Dialog.open({
			    title: "物料类别", 
			    width: 500,  height: '500',
			    url: context_path + "/material/getMaterialTypeList?mid="+(-1),
			    theme : "simple",
				drag : true,
				clickMaskToClose: false
			});
		}

		//为指定id的元素赋值
		function setValueForElementById(elementId,value){
			$('#'+elementId).val(value);
		}
  </script>
