<%@ page language="java" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
%>
<script type="text/javascript">
	var context_path = "<%=path%>";
</script>
<div id="material_edit_page" class="row-fluid" style="height: inherit;">
   	<form action="" class="form-horizontal" id="materialForm" name="materialForm" method="post" target="_ifr">
   	    <input type="hidden" id="id" name="id" value="${MATERIAL.id }">
		<%--一行数据 --%>
		<div class="row" style="margin:0;padding:0;">
			<%--物料编号--%>
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="materialNo" >物料编号：</label>
				<div class="controls">
					<div class="input-append required span12" >
						<input type="text" id="materialNo" class="span10" name="materialNo" value="${MATERIAL.materialNo }"  placeholder="后台自动生成" readonly="readonly" />
					</div>
				</div>
			</div>
			<%--物料名称--%>
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="materialName" >物料名称：</label>
				<div class="controls">
					<div class="input-append required span12" >
						<input type="text" class="span10" id="materialName" name="materialName" value="${MATERIAL.materialName }" placeholder="物料名称" />
					</div>
				</div>
			</div>
		</div>
		<div class="row" style="margin:0;padding:0;">
			<%--物料类别--%>
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="materialTypeId" >物料类别：</label>
				<div class="controls">
					<div class="input-append required span12" >
						<input type="hidden" id="materialTypeId" name="materialTypeId" value="${MATERIAL.materialTypeId  }">
						<input class="span10" type="text" title="点击选择物料类别" id="materialTypeName" name="materialTypeName" value="${MTYPE.typeName }" style="" onclick="openSelectWindowc()" readonly="readonly" placeholder="点击选择物料类别" >
					</div>
				</div>
			</div>
			<%--供&nbsp;应&nbsp;商&nbsp;--%>
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="supplyId" >供&nbsp;应&nbsp;商&nbsp;：</label>
				<div class="controls">
					<div class="required span12" >
						<input class="span10 select2_input" type = "text" id="supplyId" name="supplyId" value="${MATERIAL.supplyId}" placeholder="供应商"/>
						<input type="hidden" id="supply" value="${MATERIAL.supplyId}">
						<input type="hidden" id="supplyName" value="${MATERIAL.supplyName}">
					</div>
				</div>
			</div>
		</div>
		<div class="row" style="margin:0;padding:0;">
		    <%--封装;--%>
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="unit" >物料单位：</label>
				<div class="controls">
					<div class="input-append span12" >
						<input type="text" class="span10" id="unit" name="unit" value="${MATERIAL.unit }" placeholder="物料单位" />
					</div>
				</div>
			</div>
			<%--物料规格--%>
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="specification" >物料规格：</label>
				<div class="controls">
					<div class="input-append span12">
						<input class="span10" type="text" id="specification" name="specification" value="${MATERIAL.specification }" placeholder="物料规格">
					</div>
				</div>
			</div>			 
		</div>
        <div class="row" style="margin:0;padding:0;">			
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="producer" >生&nbsp;产&nbsp;商&nbsp;：</label>
				<div class="controls">
					<div class="input-append span12" >
						<input type="text" class="span10" id="producer" name="producer" value="${MATERIAL.producer }"placeholder="生产商">
					</div>
				</div>
			</div>
			<%--品牌--%>
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="brand" >品牌：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
				<div class="controls">
					<div class="input-append span12" >
						<input type="text" class="span10" id="brand" name="brand" value="${MATERIAL.brand }" placeholder="品牌">
					</div>
				</div>
			</div>
		</div>		
		<div class="row" style="margin:0;padding:0;">		
			<%--封装;--%>
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="weight" >标准重量：</label>
				<div class="controls">
					<div class="input-append span12" >
						<input class="span10" type="text" id="weight" name="weight" value="${MATERIAL.weight }" placeholder="标准重量">
					</div>
				</div>
			</div>
			<%--尺寸--%>
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="size" >尺寸：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
				<div class="controls">
					<div class="input-append span12" >
						<input type="text" class="span10" id="size" name="size" value="${MATERIAL.size }" placeholder="尺寸">
					</div>
				</div>
			</div>
		</div>	
		<div class="row" style="margin:0;padding:0;">
			<%--单价--%>
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="price" >单价：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
				<div class="controls">
					<div class="input-append span12" >
						<input class="span10" type="text" id="price" name="price" value="${MATERIAL.price }" placeholder="单价">
					</div>
				</div>
			</div>
			<%--代码;--%>
		<%-- 	<div class="control-group span6" style="display: inline">
				<label class="control-label" for="code" >代码：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
				<div class="controls">
					<div class="input-append span12" >
						<input type="text" class="span10" id="code" name="code" value="${MATERIAL.code }" placeholder="代码">
					</div>
				</div>
			</div>
		</div>
		<div class="row" style="margin:0;padding:0;">
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="batchNo" >条码：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
				<div class="controls">
					<div class="input-append span12" >
						<input type="text" class="span10" id="batchNo" name=batchNo" value="${MATERIAL.batchNo }" placeholder="条码">
					</div>
				</div>
			</div> --%>
			<%--拣选分类--%>
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="pickType" >拣选分类：</label>
				<div class="controls">
					<div class="required span12" >
						<input class="span10 select2_input" type = "text" id="pickType" name="pickType" value="${MATERIAL.pickType}" placeholder="拣选分类"/>
						<input type="hidden" id="pick" value="${MATERIAL.pickType}" />
						<input type="hidden" id="pickName" value="${MATERIAL.pickTypeName}" />
					</div>
				</div>
			</div>			
		</div>
		<div class="row" style="margin:0;padding:0;">			
			<%--物料等级--%>
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="level" >物料等级：</label>
				<div class="controls">
					<div class="input-append span12" >
						<input type="text" class="span10" id="level" name="level" value="${MATERIAL.level }" placeholder="物料等级">
					</div>
				</div>
			</div>
			<%--状态描述;--%>
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="weight" >状态描述：</label>
				<div class="controls">
					<div class="input-append span12" >
						<input type="text" class="span10" id="description" name="description" value="${MATERIAL.description }" placeholder="状态描述">
					</div>
				</div>
			</div>			
		</div>
  </form>
  <div class="field-button" style="text-align: center;border-top: 0px;margin: 15px auto;">
		<span class="btn btn-info" onclick="setDisabled(this.id);">
		   <i class="ace-icon fa fa-check bigger-110"></i>保存
		</span>
		<span class="btn btn-danger" onclick="layer.closeAll();">
		   <i class="icon-remove"></i>&nbsp;取消
		</span>
	</div>
</div>
<iframe src="about:blank" name="_ifr" height="0" class="hidden"></iframe>
<script type="text/javascript">
    function ckProPrice() {
        //判断商品价格
        var reg = /(^[+]?[1-9]\d*(\.\d{1,2})?$)|(^[+]?[0]{1}(\.\d{1,2})?$)/;
		if (!reg.test($("#materialForm #price").val())) {
            layer.alert("价格必须为合法数字(正数，最多两位小数)！");
			return false;
		} else {
			return true;
		}
    }
	$('#materialForm').validate({
		rules:{
  			"materialName":{//物料名称
  				required:true,
  				maxlength:50,
  				remote:"<%=path%>/material/isHaveMaterial?id="+$('#id').val()
  			},
  			"materialTypeName":{//物料类别名称
  				required:true,
  			},
  			"supplyId":{//物料类别名称
  				required:true,
  			}
  		},
  		messages:{
  			"materialName":{//物料名称
  				required:"请输入物料名称！",
  				maxlength:"长度不能超过50个字符！",
  				remote:"您输入的用户名已经存在，请重新输入！"
  			},
  			"materialTypeName":{//物料类别名称
  				required:"请选择物料类别"
   			},
            "supplyId":{//物料类别名称
                required:"供应商必填"
            }
  		},
	    errorClass: "help-inline",
	    errorElement: "span",
	    highlight:function(element, errorClass, validClass) {
		    $(element).parents('.control-group').addClass('error');
	    },
	    unhighlight: function(element, errorClass, validClass) {
		    $(element).parents('.control-group').removeClass('error');
	    }
  	});
	jQuery.validator.addMethod("valid",
			function(value,b, a) {
			var exp = new RegExp(a);
			return exp.test(value);
			},"只能输入数字");

	//弹出物料类别选择窗口
	function openSelectWindowc(){
		$.get( context_path + "/material/getMaterialTypeListEdit?mid="+$('#id').val()).done(function(data){
		childEditDiv = layer.open({
		    title : "物料类别",
	    	type:1,
	    	skin : "layui-layer-molv",
	    	area : ['500px', '300px'],
	    	shade : 0.6, //遮罩透明度
		    moveType : 1, //拖拽风格，0是默认，1是传统拖动
		    anim : 2,
		    content : data
		});
	});
}
 	function setDisabled(objId){
 	    if($("#materialForm").valid()){
 	        if($("#materialForm #price").val()){
 	            if(ckProPrice()){
                    $("#" + objId).attr("disabled", "disabled");
                    var mmName = $("#materialForm #materialName").val();
                    var param1 = {
                        id: $('#id').val(),
                        materianme: mmName
                    }
                    var mNomber = $("#materialForm #materialNo").val();
                    var param2 = {
                        id: $('#id').val(),
                        materialNo: mNomber
                    }
                    var mtName = $("#materialForm #materialTypeId").val();
                    $("#materialForm #materialTypeIdd").val(mtName);
                    $.post("<%=path%>/material/isHaveMaterial", param1, function (data) {
                        if (data) {
                            $.post("<%=path%>/material/isHaveMaterial", param2, function (data) {
                                if (data) {
                                    if (mtName !== null && mtName.length > 0) {
                                        $.ajax({
                                            cache: true,
                                            type: "POST",
                                            url: "<%=path %>/material/saveMaterial",
                                            data: $('#materialForm').serialize(),
                                            async: false,
                                            error: function (data) {
                                                $("#mSaveBtn").removeAttr("disabled");
                                                layer.alert("操作失败！", {icon: 7, time: 1000})
                                            },
                                            success: function (data) {
                                                if (data) {
                                                    layer.closeAll();
                                                    layer.alert("操作成功！", {icon: 1, time: 1000});
                                                    gridReload();
                                                    //操作成功,关闭当前子窗口
                                                    //layer.close(childquer);
                                                } else {
                                                    $("#mSaveBtn").removeAttr("disabled");
                                                    layer.alert("操作失败！", {icon: 7, time: 1000})
                                                }
                                            }
                                        });

                                    } else {
                                        $("#mSaveBtn").removeAttr("disabled");
                                    }
                                } else {
                                    $("#mSaveBtn").removeAttr("disabled");
                                }
                            });
                        } else {
                            $("#mSaveBtn").removeAttr("disabled");
                        }
                    });
				}
			}else{
                $("#" + objId).attr("disabled", "disabled");
                var mmName = $("#materialForm #materialName").val();
                var param1 = {
                    id: $('#id').val(),
                    materianme: mmName
                }
                var mNomber = $("#materialForm #materialNo").val();
                var param2 = {
                    id: $('#id').val(),
                    materialNo: mNomber
                }
                var mtName = $("#materialForm #materialTypeId").val();
                $("#materialForm #materialTypeIdd").val(mtName);
                $.post("<%=path%>/material/isHaveMaterial", param1, function (data) {
                    if (data) {
                        $.post("<%=path%>/material/isHaveMaterial", param2, function (data) {
                            if (data) {
                                if (mtName !== null && mtName.length > 0) {
                                    $.ajax({
                                        cache: true,
                                        type: "POST",
                                        url: "<%=path %>/material/saveMaterial",
                                        data: $('#materialForm').serialize(),
                                        async: false,
                                        error: function (data) {
                                            $("#mSaveBtn").removeAttr("disabled");
                                            layer.alert("操作失败！", {icon: 7, time: 1000})
                                        },
                                        success: function (data) {
                                            if (data) {
                                                layer.closeAll();
                                                layer.alert("操作成功！", {icon: 1, time: 1000});
                                                gridReload();
                                                //操作成功,关闭当前子窗口
                                                //layer.close(childquer);
                                            } else {
                                                $("#mSaveBtn").removeAttr("disabled");
                                                layer.alert("操作失败！", {icon: 7, time: 1000})
                                            }
                                        }
                                    });

                                } else {
                                    $("#mSaveBtn").removeAttr("disabled");
                                }
                            } else {
                                $("#mSaveBtn").removeAttr("disabled");
                            }
                        });
                    } else {
                        $("#mSaveBtn").removeAttr("disabled");
                    }
                });
			}
		}
 	}

	//为指定id的元素赋值
	function setValueForElementById(elementId,value){
		$('#'+elementId).val(value);
	}

	//选择供应商
	$("#supplyId").select2({
        placeholder: "选择供应商",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
             url: context_path + "/ASNmanage/getSelectSupply",
            type: "POST",
            dataType: "json",
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });
	if($("#materialForm #supply").val()!=""){
	   $("#materialForm #supplyId").select2("data", {
         id: $("#materialForm #supply").val(),
         text: $("#materialForm #supplyName").val()
      });
	}
	$.ajax({
	 type:"POST",
	 url:context_path + "/material/getMaterialById",
	 data:{id:$("#materialForm #id").val()},
	 dataType:"json",
	 success:function(data){
             $("#materialForm #pickType").select2("data", {
                id: data.pickType,
                text:data.pickTypeName
             });          
		}
	});
$("#materialForm #pickType").select2({
        placeholder: "选择拣选分类",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        width: 203,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/material/getSelectPickType",
            type: "POST",
            dataType: "json",
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });
  </script>
