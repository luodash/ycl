<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<style type="text/css">
h2 span {
	display: inline-block;
	padding: 0px 10px 5px 0;
	border-bottom: 2px solid #3a87ad;
	line-height: 1;
	*zoom: 1;
	font-size: 14px;
	font-weight: bold;
	margin-bottom: 5px;
}

.container {
	height: 70%;
	background: none;
	width: 100%;
}

#base_div, #urlDiv {
	top: 0;
	background: none;
	height: 60%;
	float: left;
}

#base_div {
	left: 0;
	height: 450px;
	border: 0;
	padding-left: 10px;
}

#urlDiv {
	height: 450px;
	padding-left: 40px;
}

.sub-Div {
	width: 100%;
	height: 30%;
	top: 10px;
	text-align: center;
}
</style>
<div style="padding: 0px 15px;">
	<form id="material_uploadImg_fileForm" name="fileForm" method="post"
		enctype="multipart/form-data" target="_ifr">
		<div class="form-title">
				选择文件
		 <span id="" class="form-note">请选择要上传的图片：</span>
		</div>
		<ul class="form-elements">
			<li class="field-group">
				<div class="control-group">
					<input class="span11" type="file" id="material_uploadImg_lefile" name="lefile" placeholder="选择文件" style="width: 200px; margin-left: 205px;" />
				</div>
			</li>
			<li class="field-group">
				<div class="field-button">
					<button class="btn btn-info" onclick="exportIn();" id="material_uploadImg_fileButton">
						<i class="ace-icon fa fa-check bigger-110"></i>上传
					</button>
					&nbsp; &nbsp;
					<div class="btn" onclick="javascript:layer.closeAll()">
						<i class="icon-remove"></i>&nbsp;取消
					</div>
				</div>
			</li>
		</ul>
	</form>
</div>
<iframe src="about:blank" name="_ifr" height="0" class="hidden"></iframe>
<script type="text/javascript">	
$('#material_uploadImg_putFile').click(function() {
	$("#material_uploadImg_lefile").click();
});

function exportIn(){
  if($("#material_uploadImg_lefile").get(0).files[0]!=undefined){
  	sub();
  }else{
    layer.msg("请先选择图片！",{icon:2,time:1200});
  }
}
function sub() {  
       var fileObj = document.getElementById("material_uploadImg_lefile").files[0]
       var form = new FormData();
		form.append("file", fileObj);// 文件对象   
        // jquery 表单提交   
        $.ajax({
         url:context_path + '/material/uploadMaterialImglFile?fileType=1&materialId='+materialId,
	     type:"POST",
	     data:form,
	     dataType:"json",
		 contentType: false,  
		 processData: false,
	     success:function(data){
	     if(data.result){
	       layer.closeAll();
	       layer.msg(data.msg,{time:1200,icon:1});
	       }else{
	       layer.msg(data.msg,{time:1200,icon:2});
	       }
	     }      
        });
        return false; // 必须返回false，否则表单会自己再做一次提交操作，并且页面跳转   
    }
</script>
