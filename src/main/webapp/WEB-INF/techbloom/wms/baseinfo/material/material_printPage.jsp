﻿<%@page import="java.math.BigDecimal"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"+ request.getServerName() + ":" + request.getServerPort()+ path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<%--<%@ include file="/techbloom/common/taglibs.jsp" %>--%>
<%@include file="../../../common/taglibs.jsp" %>
<script type="text/javascript">
	function printA4() {
		window.print();
	}
</script>
</head>
<body onload="printA4();">
	<c:forEach items="${materialList }" var="m" varStatus="mlist" step="10">
		<div style="height:100%">
			<br /><br />
			<h3 align="center" style="font-weight: bold;">物料列表</h3>
			<table style="border-collapse: collapse; border: none;width: 16.2cm;font-size: 12px;" align="center">
				<tr>
					<td style="border: solid #000 1px;text-align:center;width:5cm;">物料编码</td>
					<td style="border: solid #000 1px;text-align:center;width:5cm;">物料名称</td>
					<td style="border: solid #000 1px;text-align:center;width:5cm;">物料类别</td>
					<td style="border: solid #000 1px;text-align:center;width:5cm;">供应商</td>
					<td style="border: solid #000 1px;text-align:center;width:3cm;">单位</td>
					<td style="border: solid #000 1px;text-align:center;width:3cm;">规格</td>
					<td style="border: solid #000 1px;text-align:center;width:3cm;">生产商</td>
					<td style="border: solid #000 1px;text-align:center;width:3cm;">品牌</td>
					<td style="border: solid #000 1px;text-align:center;width:3cm;">标准重量</td>
					<td style="border: solid #000 1px;text-align:center;width:3cm;">尺寸</td>
					<td style="border: solid #000 1px;text-align:center;width:3cm;">单价</td>
					<td style="border: solid #000 1px;text-align:center;width:3cm;">拣选分类</td>
					<td style="border: solid #000 1px;text-align:center;width:3cm;">等级</td>
					<td style="border: solid #000 1px;text-align:center;width:3cm;">描述</td>
				</tr>
				<c:forEach items="${materialList }" var="m" varStatus="mlist" begin="${mlist.index}" end="${mlist.index+9}" step="1">
					<tr>
						<td style="border: solid #000 1px;text-align:center;">${m.materialNo}</td>
						<td style="border: solid #000 1px;text-align:center;">${m.materialName}</td>
						<td style="border: solid #000 1px;text-align:center;">${m.materialTypeName}</td>
						<td style="border: solid #000 1px;text-align:center;">${m.supplyName}</td>
						<td style="border: solid #000 1px;text-align:center;">${m.unit}</td>
						<td style="border: solid #000 1px;text-align:center;">${m.specification}</td>
						<td style="border: solid #000 1px;text-align:center;">${m.producer}</td>
						<td style="border: solid #000 1px;text-align:center;">${m.brand}</td>
						<td style="border: solid #000 1px;text-align:center;">${m.weight}</td>
						<td style="border: solid #000 1px;text-align:center;">${m.size}</td>
						<td style="border: solid #000 1px;text-align:center;">${m.price}</td>
						<td style="border: solid #000 1px;text-align:center;">${m.pickTypeName}</td>
						<td style="border: solid #000 1px;text-align:center;">${m.level}</td>
						<td style="border: solid #000 1px;text-align:center;">${m.description}</td>
					</tr>
				</c:forEach>
			</table>
			<br /><br />
		</div>
	</c:forEach>
</body>
</html>