<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>

	<script type="text/javascript">
	    var context_path = '<%=path%>';
	</script>
	<style type="text/css">
	</style>
</head>
<body style="overflow:hidden;">
	<div class="input-append" style="margin: 10px">
  	 <form id="material_exportIn_exportform" action="<%=path%>/material/uploadMaterialImglFile" target="_self" enctype="multipart/form-data" method="post" onsubmit="return false">
	   <label class="inline">
  	       请选择文件：
  	  </label>
  	    <input id="material_exportIn_lefile" type="file" name="file" style="display: none" onchange="change()">
  	    <input id="material_exportIn_fileType" name="fileType " type="hidden" value="3">
		<input id="material_exportIn_putFile" class="input-large" type="text" readonly="readonly" placeholder="请点击此处选择文件">
		<div id="material_exportIn_putFileSure" onclick="exportIn()" class="btn btn-info">
			<i class="ace-icon fa fa-upload bigger-110"></i>确认导入
		</div> &nbsp; &nbsp;
		</form>
	</div>
	<div style="color:red;"> <label class="inline">（请选择EXCEL文件）</label></div>
</body>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">

$('#material_exportIn_putFile').click(function() {
 $("#material_exportIn_lefile").click();
});

function exportIn(){
  if($("#material_exportIn_lefile").get(0).files[0]!=undefined){
  	sub();
  }else{
    layer.msg("请先选择EXCEL文件！",{icon:2,time:1200});
  }
}
	function sub() {
       var fileObj = document.getElementById("material_exportIn_lefile").files[0]
       var form = new FormData();
		form.append("file", fileObj);// 文件对象   
        // jquery 表单提交   
        $.ajax({
         url:context_path + '/material/uploadMaterialImglFile?fileType=3',
	     type:"POST",
	     data:form,
	     dataType:"json",
		 contentType: false,  
		 processData: false,
	     success:function(data){
		     if(data.result){
		       layer.closeAll();
		       layer.msg("导入成功！",{time:1200,icon:1});
		     }else{
		       layer.msg("导入失败",{time:1200,icon:2});
		     }
	     }
        
        });
        return false; // 必须返回false，否则表单会自己再做一次提交操作，并且页面跳转   
    }
    function change(){
      var name=$("#material_exportIn_lefile").get(0).files[0].name;
      $("#material_exportIn_putFile").val(name);
    }
</script>
</html>

