<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
	<script type="text/javascript">
		var context_path = "<%=path%>";
	</script>
  	<!-- 物料类别树 -->
  	<form action="<%=path %>/material/getMaterialTypeId" id="materialForm-c" name="materialTypeForm-c" method="post" target="_ifr">
  	  <input type="hidden" id="typeId" name="typeId" >
  	  <input type="hidden" id="typeName" name="typeName" >
	<div class="container">
		 <div id="materialTreeDiv" class="ztree"></div>
		<script type="text/javascript">
			// 声明一个根节点
			var dataJson = ${TYPETREE};
			var setting = {
					data: {
						simpleData: {
							enable: true,
							idKey: "id",
							pIdKey: "pid"
						},
						key: {
							name: "text"
						}
					},
					callback: {
						onClick: function(evt, id, node){
							$('#typeId').val(node.id);
							$('#typeName').val(node.text);
						}
					}
				};
			// 使用根节点对象的 loadData() 方法加载 JSON 数据
			 var complexTree = $.fn.zTree.init($("#materialTreeDiv"), setting, dataJson);; 
			
		</script>
		</div>
		<div class="field-button">
			<button class="btn btn-info" type="button">
	            <i class="ace-icon fa fa-check bigger-110"></i>确定
	           </button> &nbsp; &nbsp;
			<div class="btn btn_remove"><i class="icon-remove"></i>&nbsp;取消</div>
		</div>
	</form>
  <iframe src="about:blank" name="_ifr" height="0" class="hidden"></iframe>
  <script type="text/javascript">
  	
     $(".btn-info").on("click",function(){
    	var typeId = $("#materialForm-c #typeId").val();
    	var typename = $("#materialForm-c #typeName").val();
       	var param = {
    			typeId:$("#materialForm-c #typeId").val()
       	}
       	$.post("<%=path %>/material/getMaterialTypeIsEnd",param,function(data){
       		if(data){
       			$("#materialForm #materialTypeId").val(typeId);
       			$("#materialForm #materialTypeName").val(typename);
       			layer.close(childEditDiv);
       		}else{
       			layer.alert("请选择最底层分类");
       		}
       	});
    });
      $(".btn_remove").on("click",function(){
       layer.close(childEditDiv);
    });
  /* function closeThisWindow(){
	  ownerDialog.close();
  }
  $(".itree-row").click(function(){
	  var treeRow = this;
	  console.log(treeRow);
  }); */
  </script>
