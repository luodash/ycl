<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
   var context_path = '<%=path%>';
</script>
<script type="text/javascript" src="<%=path%>/static/js/techbloom/wms/baseinfo/supplier.js"></script>
<div id="grid-div">
	<form id="hiddenForm" action="<%=path%>/supplierinfo/supplierExcel" method="POST" style="display: none;">
		<!-- 选中的用户 -->
        <input id="supplier_list_ids" name="ids" value=""/>
    </form>
    <!-- 隐藏区域：存放查询条件 -->
    <form id="supplier_list_hiddenQueryForm" style="display:none;">
          <input id="supplier_list_supplierCode" name="supplierCode" value=""/>
          <input id="supplier_list_supplierName" name="supplierName" value="">
          <input id="supplier_list_supplierType" name="supplierType" value=""/>
          <input id="supplier_list_contact" name="contact" value="">
          <input id="supplier_list_website" name="website" value="">
    </form>
    <div class="query_box" id="supplier_list_aaa" title="查询选项">
         <form id="supplier_list_queryForm" style="max-width:100%;">
			   <ul class="form-elements">
				   <li class="field-group field-fluid3">
					   <label class="inline" for="supplier_list_supplierCode" style="margin-right:20px;width:100%;">
						      <span class="form_label" style="width:80px;">供应商代码：</span>
						      <input id="supplier_list_supplierCode" name="supplierCode" type="text" style="width: calc(100% - 85px);" placeholder="供应商代码">
					   </label>			
				   </li>
				   <li class="field-group field-fluid3">
					   <label class="inline" for="supplier_list_supplierName" style="margin-right:20px;width:100%;">
						      <span class="form_label" style="width:80px;">供应商名称：</span>
						      <input id="supplier_list_supplierName" name="supplierName" type="text" style="width:  calc(100% - 85px);" placeholder="供应商名称">
					   </label>			
				   </li>
				   <li class="field-group field-fluid3">
					   <label class="inline" for="supplier_list_contact" style="margin-right:20px;width:100%;">
						      <span class="form_label" style="width:80px;">联系人：</span>						
						      <input id="supplier_list_contact" name="contact" type="text" style="width:  calc(100% - 85px);" placeholder="联系人">
					   </label>			
				   </li>
				   <li class="field-group-top field-group field-fluid3">
					   <label class="inline" for="supplier_list_supplierType" style="margin-right:20px;width:100%;">
						      <span class="form_label" style="width:80px;">供应商类别：</span>
						      <input id="supplier_list_tt" name="supplierType" type="text" style="width:  calc(100% - 85px);" placeholder="选择供应商类别">
					   </label>			
				   </li>
				   <li class="field-group field-fluid3">
					   <label class="inline" for="supplier_list_website" style="margin-right:20px;width:100%;">
						      <span class="form_label" style="width:80px;">网址：</span>
						      <input id="supplier_list_website" name="website" type="text" style="width:  calc(100% - 85px);" placeholder="网址">
					   </label>			
				    </li>
				   
			  </ul>
			   <div class="field-button" style="">
				     <div class="btn btn-info" onclick="queryOk();">
			              <i class="ace-icon fa fa-check bigger-110"></i>查询
		             </div>
				     <div class="btn" onclick="reset();"><i class="ace-icon icon-remove"></i>重置</div>
				     <a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>
	            </div>
		  </form>		 
    </div>
    <div id="supplier_list_fixed_tool_div" class="fixed_tool_div">
        <div id="supplier_list___toolbar__" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="supplier_list_grid-table" style="width:100%;height:100%;"></table>
    <div id="supplier_list_grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
var context_path = '<%=path%>';
var oriData;
var _grid;
var dynamicDefalutValue="13543c0c40734162919a2dbafebce91a";

$(function  (){
    $(".toggle_tools").click();
});

function toExcel(){
	var selectid = getId("#supplier_list_grid-table","id");
	function getId(_grid,_key){
	    var idAddr = jQuery(_grid).getGridParam("selarrrow");
	    var ids="";
	    ids=idAddr+",";
	    return ids;
	}
	$("#supplier_list_ids").val(selectid);
	$("#supplier_list_hiddenForm").submit();
}

function standCropPrint() {
	var selectid = getId("#supplier_list_grid-table","id");
	function getId(_grid,_key){
	    var idAddr = jQuery(_grid).getGridParam("selarrrow");
	    var ids="";
	    ids=idAddr+",";
	    return ids;
	}
	$("#supplier_list_ids").val(selectid);
    var url = context_path + "/supplierinfo/standCropPrint?ids=" + $("#supplier_list_ids").val();
    window.open(url);
}

$("#supplier_list___toolbar__").iToolBar({
    id: "supplier_list___tb__01",
    items: [
        {label: "添加", disabled: (${sessionUser.addQx} == 1 ? false : true), onclick:addSupplier, iconClass:'glyphicon glyphicon-plus'},
        {label: "编辑", disabled: (${sessionUser.editQx} == 1 ? false : true),onclick: editSupplier, iconClass:'glyphicon glyphicon-pencil'},
        {label: "删除", disabled: (${sessionUser.deleteQx} == 1 ? false : true),onclick: delSupplier, iconClass:'glyphicon glyphicon-trash'},
        {label: "导出", disabled: (${sessionUser.queryQx}==1?false:true),onclick:function(){toExcel();},iconClass:' icon-share'}
   ]
});

$(function () {
        _grid = jQuery("#supplier_list_grid-table").jqGrid({
            url: context_path + "/supplierinfo/list.do",
            datatype: "json",
            colNames: ["主键", "供应商名称", "供应商代码", "供应商类别", "联系人", "联系方式", "地址", "网址", "邮箱", "付款方式"],
            colModel: [
                {name: "id", index: "id", width: 20, hidden: true},
                {name: "supplierName", index: "supplierName", width: 100},
                {name: "supplierCode", index: "supplierCode", width: 65},
                {name: "supplierTypeName", index: "supplierTypeName", width: 65},
                {name: "contact", index: "contact", width: 60},
                {name: "contactWay", index: "contactWay", width: 60},
                {name: "address", index: "address", width: 80},
                {name: "website", index: "website", width: 90},
                {name: "email", index: "email", width: 128},
                {name :"payWay",index:"payWay",width:40,                   
                    formatter: function (value,options,rowObject){
                        if (typeof value == "number") {
                        if(value==0){
               			return "<span style='font-weight:bold;'>现金</span>" ;
               		}else if(value==1){
               			return "<span style='font-weight:bold;'>信用卡</span>" ;
               		}else if(value==2){
               			return "<span style='font-weight:bold;'>转账</span>" ;
               		}else if(value==3){
               			return "<span style='font-weight:bold;'>网银</span>";
               		}else if(value==4){
               			return "<span style='font-weight:bold;'>在线支付</span>";
               		}else{
               			return "<span style='font-weight:bold;'>其他</span>";
               		}
                        }else{
                        return "" ;
                        }
                    }
                 }
            ],
            rowNum: 20,
            rowList: [10, 20, 30],
            pager: "#supplier_list_grid-pager",
            sortname: "supplierCode",
            sortorder: "asc",
            altRows: true,
            viewrecords: true,
            autowidth: true,
            multiselect: true,
            multiboxonly: true,
            beforeRequest:function (){
                dynamicGetColumns(dynamicDefalutValue,"supplier_list_grid-table",$(window).width()-$("#sidebar").width() -7);
                //重新加载列属性
            },
            loadComplete: function (data) {
                var table = this;
                setTimeout(function () {
                    updatePagerIcons(table);
                    enableTooltips(table);
                }, 0);
                oriData = data;
            },
            emptyrecords: "没有相关记录",
            loadtext: "加载中...",
            pgtext: "页码 {0} / {1}页",
            recordtext: "显示 {0} - {1}共{2}条数据"
        });
        //在分页工具栏中添加按钮
        jQuery("#supplier_list_grid-table").navGrid("#supplier_list_grid-pager", {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false
        }).navButtonAdd("#supplier_list_grid-pager", {
                caption: "",
                buttonicon: "ace-icon fa fa-refresh green",
                onClickButton: function () {
                    $("#supplier_list_grid-table").jqGrid("setGridParam",
                            {
                                postData: {queryJsonString: ""} //发送数据
                            }
                    ).trigger("reloadGrid");
                }
            }).navButtonAdd("#supplier_list_grid-pager",{
            caption: "",
            buttonicon:"fa  icon-cogs",
            onClickButton : function (){
                jQuery("#supplier_list_grid-table").jqGrid("columnChooser",{
                    done: function(perm, cols){
                        dynamicColumns(cols,dynamicDefalutValue);
                        $("#supplier_list_grid-table").jqGrid("setGridWidth", $("#supplier_list_grid-div").width());
                    }
                });
            }
        });
        $(window).on("resize.jqGrid", function () {
            $("#supplier_list_grid-table").jqGrid("setGridWidth", $("#supplier_list_grid-div").width() );
            $("#supplier_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-
            $("#supplier_list_aaa").outerHeight(true)-$("#supplier_list_fixed_tool_div").outerHeight(true)-
            $("#supplier_list_grid-pager").outerHeight(true)-$("#gview_supplier_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
        });

        $(window).triggerHandler("resize.jqGrid");
    });
var _queryForm_data = iTsai.form.serialize($("#supplier_list_queryForm"));

function queryOk(){
    var queryParam = iTsai.form.serialize($("#supplier_list_queryForm"));
//执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
    queryByParam(queryParam);
}

function reset(){
    iTsai.form.deserialize($("#supplier_list_queryForm"),_queryForm_data);
    $("#supplier_list_queryForm #supplier_list_tt").select2("val","");
    queryByParam(_queryForm_data);
}

function queryByParam(jsonParam) {
    iTsai.form.deserialize($('#supplier_list_hiddenQueryForm'), jsonParam);   //将json对象反序列化到列表页面中隐藏的form中
    var queryParam = iTsai.form.serialize($('#supplier_list_hiddenQueryForm'));
    var queryJsonString = JSON.stringify(queryParam);         //将json对象转换成json字符串
    //执行查询操作
    $("#supplier_list_grid-table").jqGrid('setGridParam',
        {
            postData: {queryJsonString: queryJsonString} //发送数据
        }
    ).trigger("reloadGrid");
}

$("#supplier_list_queryForm #supplier_list_tt").select2({
        placeholder: "选择供应商类型",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/supplierinfo/getTypeSelectList",
            type: "POST",
            dataType: "json",
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });
</script>