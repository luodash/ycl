<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<div id="supplier_edit_page" class="row-fluid" style="height: inherit;">
	
	<form id="supplierForm" class="form-horizontal" style="overflow: auto; height: calc(100% - 70px);">
		<input type="hidden" id="id" name="id" value="${supplier.id}">
		<div class="row" style="margin:0;padding:0;"><input type="text" style="width:0;height:0;float:left;padding: 0px;border: 0px;"/>
        <input type="password" style="width:0;height:0;float:left;padding: 0px;border: 0px;"/>
        </div>
		<div class="row" style="margin:0;padding:0;">
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="supplierName">供应商名称：</label>
				<div class="controls">
					<div class="input-append span12 required">
						<input type="text" class="span10" id="supplierName" name="supplierName" value="${supplier.supplierName }" placeholder="供应商名称">
					</div>
				</div>
			</div>
			<div class="control-group span6" style="display: inline">
				<label class="control-label">供应商代码：</label>
				<div class="controls">
					<div class="input-append span12 required">
						<c:choose>
							<c:when test="${edit==1}">
								<input type="text" class="span10" id="supplierCode" name="supplierCode" value="${supplier.supplierCode }" placeholder="供应商代码" readonly="readonly" title="代码不可修改">
							</c:when>
							<c:otherwise>
								<input type="text" class="span10" id="supplierCode" name="supplierCode" placeholder="后台自动生成" readonly="readonly">
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			</div>
		</div>
		<div class="row" style="margin:0;padding:0;">
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="supplierType">供应商类型：</label>
				<div class="controls">
					<select id="supplierType" name="supplierType" data-placeholder="请选择供应商类型" style="width:200px;">
						<c:forEach items="${supplierType}" var="tp">
							<option value="${tp.dictNum}"
								<c:if test="${supplier.supplierType==tp.dictNum }">selected="selected"</c:if>>${tp.dictValue}</option>
						</c:forEach>
					</select>
				</div>
			</div>
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="contact">联系人：</label>
				<div class="controls">
					<div class="input-append span12 required">
						<input type="text" class="span10" id="contact" name="contact" value="${supplier.contact}" placeholder="联系人">
					</div>
				</div>
			</div>
		</div>
		<div class="row" style="margin:0;padding:0;">
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="contactWay">手机号码：</label>
				<div class="controls">
					<input type="text" class="span10" name="contactWay" id="contactWay" placeholder="联系方式" value="${supplier.contactWay}" />
				</div>
			</div>
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="address">地址：</label>
				<div class="controls">
					<input type="text" class="span10" id="address" name="address" value="${supplier.address }" placeholder="地址">
				</div>
			</div>
		</div>
		<div class="row" style="margin:0;padding:0;">
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="website">网址：</label>
				<div class="controls">
					<input type="text" class="span10" name="website" id="website" placeholder="网址" value="${supplier.website}">
				</div>
			</div>
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="email">Email：</label>
				<div class="controls">
					<input type="text" class="span10" name="email" id="email" placeholder="邮箱" value="${supplier.email}">
				</div>
			</div>
		</div>
		<div class="row" style="margin:0;padding:0;">
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="payWay">付款方式：</label>
				<div class="controls">
					<select id="payWay" name="payWay" data-placeholder="请选择供应商类型" style="width:435px;">
						<option value="0"
							<c:if test="${supplier.payWay==0 }">selected="selected"</c:if>>现金</option>
						<option value="1"
							<c:if test="${supplier.payWay==1 }">selected="selected"</c:if>>信用卡</option>
						<option value="2"
							<c:if test="${supplier.payWay==2 }">selected="selected"</c:if>>转账</option>
						<option value="3"
							<c:if test="${supplier.payWay==3 }">selected="selected"</c:if>>网银</option>
						<option value="4"
							<c:if test="${supplier.payWay==4 }">selected="selected"</c:if>>在线支付</option>
						<option value="13"
							<c:if test="${supplier.payWay==13 }">selected="selected"</c:if>>其他</option>
					</select>
				</div>
			</div>
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="suserName">用户名：</label>
				<div class="controls">
					<div class="input-append span12 required">
						<input type="text" class="span10" name="suserName" autocomplete="off" id="suserName" placeholder="用户名" value="${supplier.suserName}">
					</div>
				</div>
			</div>
		</div>
		<div class="row" style="margin:0;padding:0;">
			<div id="yy" class="control-group span6" style="display: inline">
				<label class="control-label" for="suserName">密码：</label>
				<div class="controls">
					<div class="input-append span12 required">
						<input type="password" class="span10" autocomplete="off" name="spassWord" id="spassWord" placeholder="密码">
					</div>
				</div>
			</div>
			<div id="xx" class="control-group span6" style="display: inline">
				<label class="control-label" for="initpwd">密码初始化：</label>
				<div class="controls">
					<div class=" btn btn-danger" title="点击可以初始化用户密码" id="initpwdbtn" onclick="initpwd();">
						<i class="fa fa-refresh" aria-hidden="true"></i>&emsp;初始化
					</div>
				</div>
			</div>
		</div>
	</form>
	<div class="field-button" style="text-align: center;border-top: 0px;margin: 15px auto;">
		<span class="btn btn-info" onclick="saveForm();"> 
		    <i class="ace-icon fa fa-check bigger-110"></i>保存
		</span> 
		<span class="btn btn-danger" onclick="layer.closeAll();"> 
		    <i class="icon-remove"></i>&nbsp;取消
		</span>
	</div>
</div>
<script type="text/javascript">
var context_path = "<%=path%>";
if($("#id").val()!=""){
	      $("#yy").attr("style","display:none");
		  $("#xx").removeAttr("style","display:none");
	  }else{
          $("#yy").removeAttr("style","display:none");
          $("#xx").attr("style","display:none");
	  }


	$("#supplierForm").validate({
		rules:{
			"supplierName":{//供应商名称
  				required:true, 
  				maxlength:32,  
  				validChar:true,	
  			},
  			"email":{
  				accept:"[a-zA-Z0-9_]@*.com|cn|net$",
  				maxlength:256
  			},
  			"contactWay":{
  			   mobile: true
  			},
			"suserName":{
                required:true,
                remote:"<%=path%>/supplierinfo/existsUsername?id="+ $("#id").val()
			},
		    "spassWord" : {
								required : true
			}
		},
		messages : {
			"supplierName" : {
				required : "请输入供应商名称！",
				maxlength : "长度不能超过32个字符！"
			},
			"email" : {
				accept : "邮箱格式不正确",
				maxlength : "最大长度不能超过256"
			},
			"contactWay" : {
				accept : "联系方式格式不正确"
			},
			"suserName" : {
				required : "请输入供应商用户名",
			    remote : "供应商用户名已经存在"
			},
			"spassWord" : {
				required : "请输入密码"
			}
		},
	errorClass: "help-inline",
	errorElement: "span",
	highlight:function(element, errorClass, validClass) {
		$(element).parents('.control-group').addClass('error');
	},
	unhighlight: function(element, errorClass, validClass) {
		$(element).parents('.control-group').removeClass('error');
	}
					});
	jQuery.validator.addMethod("valid", function(value, b, a) {
		var exp = new RegExp(a);
		return exp.test(value);
	}, "只能输入数字");
	//确定按钮点击事件
	function saveForm() {
		if ($('#supplierForm').valid()) {
			saveSupplierInfo($("#supplierForm").serialize());
		}
	}

	function initpwd() {
		//密码初始化
		layer.confirm('确定初始化密码？<br> 注：初始化的密码为123456。', /*显示的内容*/
		{
			shift : 6,
			moveType : 1, //拖拽风格，0是默认，1是传统拖动
			title : "操作提示", /*弹出框标题*/
			icon : 3, /*消息内容前面添加图标*/
			btn : [ '确定', '取消' ]
		/*可以有多个按钮*/
		}, function(index, layero) {
			$.ajax({
				url : context_path + "/supplierinfo/initUserPass",
				type : "POST",
				data : {
					id : $("#id").val()
				},
				dataType : "JSON",
				success : function(data) {
					if (data) {
						layer.msg("操作成功!");
						layer.closeAll();
					} else {
						layer.msg("操作失败！");
					}
				}
			});
		}, function(index) {
			//取消按钮的回调
			layer.close(index);
		});

	}
	//保存/修改用户信息
	function saveSupplierInfo(bean) {
		$.ajax({
			url : context_path + "/supplierinfo/toSaveSupplier?tm="
					+ new Date(),
			type : "POST",
			data : bean,
			dataType : "JSON",
			success : function(data) {
				if (Boolean(data.result)) {
					layer.msg("保存成功！", {
						icon : 1
					});
					//关闭当前窗口
					layer.close($queryWindow);
					//刷新列表
					gridReload();
				} else {
					layer.alert("保存失败，请稍后重试！", {
						icon : 2
					});
				}
			},
			error : function(XMLHttpRequest) {
				alert(XMLHttpRequest.readyState);
				alert("出错啦！！！");
			}
		});

	}
	$("#supplierForm #payWay").select2({
    minimumInputLength:0,
    allowClear:true,
    delay:250,
    width:200,
    formatNoMatches:"没有结果",
	formatSearching:"搜索中...",
	formatAjaxError:"加载出错啦！"
    });
    $("#supplierForm #payWay").on("change.select2",function(){
       $("#supplierForm #payWay").trigger("keyup")}
    );
    $("#supplierForm #supplierType").select2({
    minimumInputLength:0,
    allowClear:true,
    delay:250,
    width:200,
    formatNoMatches:"没有结果",
	formatSearching:"搜索中...",
	formatAjaxError:"加载出错啦！"
    });
    $("#supplierForm #supplierType").on("change.select2",function(){
       $("#supplierForm #supplierType").trigger("keyup")}
    );	
</script>