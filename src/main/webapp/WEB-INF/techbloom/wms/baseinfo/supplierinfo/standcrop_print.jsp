<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="com.techbloom.wms.entity.baseinfo.Supplierinfo" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

    List<Supplierinfo> typeList =  (List<Supplierinfo>)request.getAttribute("typeList");
    String userName = (String)request.getAttribute("userName");
    String enterpriseName = "";
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    String date = sdf.format(new Date());
    int startIndex =0;
    int pageSize = 40;
    //判断list能打印几个单子
    int num = typeList.size()/pageSize;
    if(typeList.size()%pageSize != 0){
        num = num + 1;
    }
%>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=basePath%>">
    <title>现存量查询</title>
    <script type="text/javascript">
        function printA4(){
            window.print();
        }
    </script>
</head>
<body onload="printA4();">
<%
    double allAmount = 0;
    int index = 1;
    for(int a = 0; a < num; a++){
        //最大条数为
        int size = startIndex + pageSize;
        if(a == num -1){
            size = typeList.size();
        }
%>
<div style="height: 295mm;width: 210mm;border:0px solid #000;">
    <h3 align="center" style="font-weight: bolder;">供应商</h3>
    <div style="height:30px;width:400px;margin:0 auto;font-size: 14px;">
    </div><br/>
    <table style="border-collapse: collapse; border: none; width: 21cm;font-size: 12px;" align="center">
        <tr style="height: 32px">
            <th style="border: solid #000 1px;text-align:center;font-weight:bold;">供应商编号</th>
            <th style="border: solid #000 1px;text-align:left;font-weight:bold;">供应商名称</th>
            <th style="border: solid #000 1px;text-align:left;font-weight:bold;">联系人</th>
            <th style="border: solid #000 1px;text-align:center;font-weight:bold;">通讯地址</th>
            <th style="border: solid #000 1px;text-align:center;font-weight:bold;">电子邮件</th>
            <th style="border: solid #000 1px;text-align:center;font-weight:bold;">手机号码</th>
            <th style="border: solid #000 1px;text-align:center;font-weight:bold;">区号</th>
            <th style="border: solid #000 1px;text-align:center;font-weight:bold;">电话号码</th>
            <th style="border: solid #000 1px;text-align:center;font-weight:bold;">分机号码</th>
            <th style="border: solid #000 1px;text-align:center;font-weight:bold;">创建时间</th>
            <th style="border: solid #000 1px;text-align:center;font-weight:bold;">描述</th>
        </tr>
        <%
            if(typeList != null && typeList.size() > 0){
                for(int i= 0; i < size; i++){
                	Supplierinfo in  = (Supplierinfo)typeList.get(i);
        %>
        <tr style="height: 22px">
            <td style="border: solid #000 1px;text-align:center;">
                <%=i+1 %>
            </td>
            <td style="border: solid #000 1px;text-align:left;">
                <%=in.getSupplierNo() == null ? "" : in.getSupplierNo() %>
            </td>
            <td style="border: solid #000 1px;text-align:left">
                <%=in.getSupplierName() == null ? "" : in.getSupplierName() %>
            </td>
            <td style="border: solid #000 1px;text-align:center;">
                <%=in.getContact() == null ? "" : in.getContact() %>
            </td>
            <td style="border: solid #000 1px;text-align:center;">
                <%=in.getPostalAddress() == null ? "" : in.getPostalAddress() %>
            </td>
            <td style="border: solid #000 1px;text-align:center;">
                <%=in.getEmail() == null ? "" : in.getEmail() %>
            </td>
            <td style="border: solid #000 1px;text-align:center;">
                <%=in.getMobilePhone() == null ? "" : in.getMobilePhone() %>
            </td>
            <td style="border: solid #000 1px;text-align:center;">
                <%=in.getAreaCode() == null ? "" : in.getAreaCode() %>
            </td>
            <td style="border: solid #000 1px;text-align:center;">
                <%=in.getTelephoneNumber() == null ? "" : in.getTelephoneNumber() %>
            </td>
            <td style="border: solid #000 1px;text-align:center;">
                <%=in.getExtensionNumber() == null ? "" : in.getExtensionNumber() %>
            </td>
            <td style="border: solid #000 1px;text-align:center;">
                <%=in.getCreatDate() == null ? "" : in.getCreatDate() %>
            </td>
            <td style="border: solid #000 1px;text-align:right">
                <%=in.getDescription() %>
            </td>
        </tr>
        <%
                }
            }
        %>
    </table>
    <br/>
    <table style="border-collapse: collapse; border: none; width: 21cm;font-size: 14px;" align="center">
        <tr>
            <td style="text-align:center;">单位：<%=enterpriseName %></td>
            <td style="text-align:center;">制表：<%=userName %></td>
            <td style="text-align:center;">打印日期：<%=date %></td>
        </tr>
    </table>
</div>
<div style="font-size: 14px;margin-bottom:0px;text-align: center;">第<%=index++ %>页</div>
<%
        startIndex += pageSize;
    }
%>
</body>
</html>

