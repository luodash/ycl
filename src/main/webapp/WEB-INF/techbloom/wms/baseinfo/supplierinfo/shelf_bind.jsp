<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
%>
<div  id ="shelf_bind_role_add_page" class="row-fluid" style="height:inherit;">
    <form id="shelf_bind_materialTypeForm" class="form-horizontal" onkeydown="if(event.keyCode==13)return false;" style=" height: calc(100% - 70px);">
		<input type="hidden" id="shelf_bind_supplyId" name="supplyId" value="${supplyId}">
		<!-- 菜单树 -->
		<div  class="row" style="margin:0;padding:0;height:200px;">
			<div  class="row" style="margin:0;padding:0;">
				<span class="span12" style="padding:10px;border-bottom:solid #009f95 1px;font-size:14px;">
					<i class="fa fa-flag" aria-hidden="true"></i>&nbsp;货架绑定
				</span>
			</div>
			<div  class="row" style="margin:0;padding:0; overflow: auto; height:160px;">
				<ul id="shelf_bind_menuTree" class="ztree" ></ul>
			</div>
		</div>
	</form>
	<!-- 底部按钮 -->
	<div class="form-actions" style="text-align: right;border-top: 0px;margin: 0px;">
		<span  class="btn btn-success">保存</span>
		<span  class="btn btn-danger">取消</span>
	</div>
</div>
 <script type="text/javascript">
	var typeId=$("#shelf_bind_materialId").val();
	//初始化树
	var zTree;   //树形对象
   var selectTreeId = 0;   //存放选中的树节点id
		//树的相关设置
		var setting = {
		    check: {
		        enable: true	
		    },
		    data: {
		        simpleData: {
		            enable: true
		        }
		    },
		    edit: {
		        enable: false,
		        drag:{
		        	isCopy:false,
		        	isMove:false
		        }
		    },
		    callback: {
				onAsyncSuccess: zTreeOnAsyncSuccess
		    },
			async: {
				enable: true,
				url:context_path+"/supplierinfo/getLocationList",
				autoParam:["id"],
				otherParam: {"supplyId":$("#shelf_bind_supplyId").val()},
				type: "POST"
			},//异步加载数据
		};
	   //ztree加载成功之后的回调函数
		function zTreeOnAsyncSuccess(event, treeId, treeNode, msg) {
			zTree.expandAll(true);
		};
			
	
   		$.fn.zTree.init($("#shelf_bind_menuTree"), setting);
   		zTree = $.fn.zTree.getZTreeObj("shelf_bind_menuTree");
   	//取消按钮点击事件
	$("#shelf_bind_role_add_page .btn-danger").off("click").on("click", function(){
	    layer.closeAll();
	    return false;
	});
	
	var ajaxStatus = 1;     //ajax请求状态：0不能请求，1可以请求
	//确定按钮点击事件
    $("#shelf_bind_role_add_page .btn-success").off("click").on("click", function(){
		//角色保存
			//获取选中的菜单
	    	var nodes = zTree.getCheckedNodes(true);
			var menuids = "";
			for(var rolei=2,count = nodes.length;rolei<count; rolei++){
				var menuNode = nodes[rolei];
				if(menuNode.id>0){
					if(menuids.length>0){
						menuids += ","+menuNode.id;
					}else{
						menuids += menuNode.id;
					}
				}
			}
			var bean = $("#shelf_bind_materialTypeForm").serialize();
			
			if(ajaxStatus==0){
  				layer.msg("操作进行中，请稍后...",{icon:2});
  				return;
  			}
  			ajaxStatus = 0;    //将标记设置为不可请求
			$.ajax({
				url:context_path+"/supplierinfo/saveBind?tm="+new Date(),
				type:"POST",
				data:{id:$("#shelf_bind_supplyId").val(),locationIds:menuids},
				dataType:"JSON",
				success:function(data){
					ajaxStatus = 1; //将标记设置为可请求
					if(data){
						//刷新表格
						$("#shelf_bind_grid-table").jqGrid('setGridParam', 
								{
							postData: {queryJsonString:""} //发送数据 
								}
						).trigger("reloadGrid");
						layer.closeAll();
						layer.msg("操作成功！",{icon:1});
					}else{
						layer.alert("操作失败！",{icon:2});
					}
				}
			});
		return false;
	});
  </script>
</html>
