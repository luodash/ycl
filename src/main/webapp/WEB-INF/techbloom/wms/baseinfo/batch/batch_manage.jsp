<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<style type="text/css">
   h2 span{
   	 display:inline-block;
     padding:0px 10px 5px 0;
   	 border-bottom: 2px solid #3a87ad;
   	 line-height:1;
   	 *zoom:1;
   	 font-size:14px;
   	 font-weight:bold;
   	 margin-bottom:5px;
   }
   
   .container{
      height:70%;
      background:none;
      width:100%;
   }
   
   #batch_manage_base_div, #batch_manage_urlDiv{
      top:0;
      background:none;
      height:60%;
      float:left;
   }
   
   #batch_manage_base_div{
     left:0;
   	 height:450px;
   	 border:0;
   	 padding-left:10px;
   }
   #batch_manage_urlDiv{
     height:450px;
     padding-left:40px;
   }
   .sub-Div{
       width:100%;
       height:30%;
       top:10px;
       text-align:center;
   }
   .field-label{
       width:130px;
   }
</style>
  	<div style="padding-left:30px;">
   	 <form id="batch_manage_batchForm" name="batchForm" method="post" target="_ifr">
			<div class="form-title">
				批次规则
				<span id="" class="form-note">定义批次信息的生成规则：</span>
			</div>
			<ul class="form-elements">
				<li class="field-group">
					<label class="field-label" for="batch_manage_supplierLength">
						供应商代码字符数：
					</label>
					<div class="field-input">
						<input  type="text" name="supplierLength" id="batch_manage_supplierLength" placeholder="供应商代码字符数" style="width: 300px;" readonly="readonly"/>
					</div>
				</li>
				<li class="field-group">
					<label class="field-label" for="batch_manage_materialLength">
						物料编号字符数：
					</label>
					<div class="field-input">
						<input type="text" id="batch_manage_materialLength" name="materialLength" style="width: 300px;" placeholder="物料编号字符数" readonly="readonly">
					</div>
				</li>
				<li class="field-group">
					<label class="field-label" for="batch_manage_dateLength">
						时间字符数：
					</label>
					<div class="field-input">
						<input type="text"  name="dateLength" id="batch_manage_dateLength" placeholder="时间字符数" style="width: 300px;" readonly="readonly">
					</div>
				</li>
			</ul>
		</form>
  	</div>
<iframe src="about:blank" name="_ifr" height="0" class="hidden"></iframe>
<script type="text/javascript">
$(function(){
	$.ajax({
		type: "POST",
		url: "<%=path%>/batch/getBatchRule?id=1",
		dataType: "json",
		success: function(data){
			$("#batch_manage_supplierLength").val(data.supplierLength);
			$("#batch_manage_materialLength").val(data.materialLength);
			$("#batch_manage_dateLength").val(data.dateLength);
	   }
	});   
});
</script>
