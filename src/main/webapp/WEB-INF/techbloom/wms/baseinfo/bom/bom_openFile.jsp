<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<style type="text/css">
h2 span {
	display: inline-block;
	padding: 0px 10px 5px 0;
	border-bottom: 2px solid #3a87ad;
	line-height: 1;
	*zoom: 1;
	font-size: 14px;
	font-weight: bold;
	margin-bottom: 5px;
}

.container {
	height: 70%;
	background: none;
	width: 100%;
}

#bom_openFile_base_div, #bom_openFile_urlDiv {
	top: 0;
	background: none;
	height: 60%;
	float: left;
}

#bom_openFile_base_div {
	left: 0;
	height: 450px;
	border: 0;
	padding-left: 10px;
}

#bom_openFile_urlDiv {
	height: 450px;
	padding-left: 40px;
}

.sub-Div {
	width: 100%;
	height: 30%;
	top: 10px;
	text-align: center;
}
</style>
<div>
	<form id="bom_openFile_fileForm" name="fileForm" method="post"
		enctype="multipart/form-data" target="_ifr">
		<div class="form-title">
				选择文件
		 <span id="" class="form-note">请选择要上传的xls文件：</span>
		</div>
		<ul class="form-elements">
			<li class="field-group">
				<div class="control-group" style="text-align: center;">
					<input class="span11" type="file" name="file2" id="bom_openFile_file2" placeholder="选择文件" style="width: 200px;margin-left:50px;">
				</div>
			</li>
			<li class="field-group">
				<div class="field-button" style="text-align: center;">
					<button class="btn btn-info" onclick="uploadFile()" id="bom_openFile_fileButton">
						<i class="ace-icon fa fa-check bigger-110"></i>上传
					</button>
					&nbsp; &nbsp;
					<button class="btn btn-danger" onclick="layer.close($queryWindow);return false;">
						<i class="icon-remove"></i>&nbsp;取消
					</button>
				</div>
			</li>
		</ul>
	</form>
</div>
<iframe src="about:blank" name="_ifr" height="0" class="hidden"></iframe>
<script type="text/javascript">	
function uploadFile(){
	var fileObj = document.getElementById("bom_openFile_file2").files[0]; // 获取文件对象
	if(fileObj){
		var form = new FormData();
		form.append("file", fileObj);// 文件对象   
		$.ajax({
			url:context_path + "/bom/uploadBomExcelFile.do",
			type:"POST",
			data:form,
			dataType:"JSON",
			contentType: false,  
		    processData: false,
			success:function(data){
				if(Boolean(data.result)){
					layer.alert("上传成功！！！");
					$("#bom_openFile_grid-table").jqGrid('setGridParam', {
						postData : {
							queryJsonString : ""
						}
					}).trigger("reloadGrid");
					layer.close($queryWindow);
				}else{
					layer.alert(data.msg);
				}
			},
			error:function(XMLHttpRequest){
            	alert("出错啦！！！");
            }
		});
	}else {
		alert("未选择文件");
	}		
}
</script>
