﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<div class="main-content">
	<div class="main-content-inner">
		<div class="page-content" style="padding:0px;">
			<div >
				<div class="">
					<div class="space-6"></div>
					<div >
						<div class="col-sm-10 col-sm-offset-1">
							<div class="widget-box transparent">
								<div class="widget-header widget-header-large">
									<!-- 隐藏的入库单主键 -->
				   	        		<input type="hidden" id="view_bom_detail_bomkey" name="bomkey" value="${bom.bomId }">
									<h3 class="widget-title grey lighter" style=' background: none; border-bottom: none; '>
										<i class="ace-icon fa fa-leaf green"></i>
										bom
									</h3>

									<div class="widget-toolbar no-border invoice-info">
										<span class="invoice-info-label">bom版本号:</span>
										<span class="red">${bom.version }</span>
										<br/>
										<span class="invoice-info-label">bom版本名称:</span>
										<span class="red">${bom.versionName }</span>
									</div>
									
								</div>

								<div class="widget-body">
									<div class="widget-main ">
										<div >
											<div class="col-sm-12">
												<div  style="margin-left:10px;">
													<div style="line-height: 1;height: 15px;" class="col-xs-11 label label-lg label-info arrowed-in arrowed-right">
														<b>表单信息</b>
													</div>
												</div>
												<div>
													<ul class="list-unstyled spaced">
														<li>
															<i class="ace-icon fa fa-caret-right blue"></i>
															bom编号：
															<b class="black">${bom.bomNo }
															</b>
														</li>
														<li>
															<i class="ace-icon fa fa-caret-right blue"></i>
															bom名称：
															<b class="black">${bom.bomName }</b>
														</li>
														<li>
															<i class="ace-icon fa fa-caret-right blue"></i>
															生产流程：
															<b class="black">${bom.modelName }</b>
														</li>
														<li>
															<i class="ace-icon fa fa-caret-right blue"></i>
															备注：
															<b class="black">${bom.remark }</b>
														</li>
														
													</ul>
												</div>
											</div><!-- /.col -->
										</div><!-- /.row -->

										<div class="space"></div>
										<!-- 详情表格 -->
										<div id="view_bom_detail_grid-div-c">
										<!-- 物料信息表格 -->
											<table id="view_bom_detail_grid-table-c" style="width:100%;height:100%;"></table>
										<!-- 表格分页栏 -->
											<div id="view_bom_detail_grid-pager-c"></div>
									   </div>
										<div class="hr hr8 hr-double hr-dotted"></div>
										<div class="space-6"></div>
										<div class="well">
											<span>版本删除后不可恢复!</span>
											
												<div class="btn btn-sm btn-primary pull-right" onclick="updateInstoreStatus();">
													删除
												</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->
<script type="text/javascript">
var context_path = '<%=path%>';
var oriData;      //表格数据
var _grid_order;        //表格对象

	//初始化表格
  	_grid_order = jQuery("#view_bom_detail_grid-table-c").jqGrid({
         url: context_path + "/bom/bomDetailList.do?bom_id=" + $("#view_bom_detail_bomkey").val()+"&type=0",
         datatype : "json",
         colNames: ['主键', '物料名称', '单位', '数量', '绑定工位', '绑定库位', '报警数量'],
         colModel : [
  					  {name: 'id', index: 'id', hidden: true},
                      {name: 'materialName', index: 'materialName', width: 50},
                      {name: 'materialUnit', index: 'materialUnit', width: 60},
                      {name: 'amount',index: 'amount',width: 60  },
                      {name: 'routeName', index: 'routeName', width: 90},
                      {name: 'locationName',index: 'locationName', width: 90},
                      {name: 'alarmAmount',index: 'alarmAmount' ,width: 60}
                    ],
         rowNum : 20,
         rowList : [ 10, 20, 30 ],
         pager : '#view_bom_detail_grid-pager-c',
         sortname : 'ID',
         sortorder : "asc",
         altRows: true,
         viewrecords : true,
         caption : "物料列表",
         autowidth:true,
         multiselect:false,
		 multiboxonly: true,
		loadComplete : function(data)
         {
             var table = this;
             setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
             oriData = data;
         },
  	   emptyrecords: "没有相关记录",
  	   loadtext: "加载中...",
  	   pgtext : "页码 {0} / {1}页",
  	   recordtext: "显示 {0} - {1}共{2}条数据"
    });
    //在分页工具栏中添加按钮
    jQuery("#view_bom_detail_grid-table-c").navGrid('#view_bom_detail_grid-pager-c',{edit:false,add:false,del:false,search:false,refresh:false})
      .navButtonAdd('#view_bom_detail_grid-pager-c',{
  	   caption:"",
  	   buttonicon:"ace-icon fa fa-refresh green",
  	   onClickButton: function(){
  	    $("#view_bom_detail_grid-table-c").jqGrid('setGridParam',
				{
					postData: {instoreID:$('#id').val()} //发送数据  :选中的节点
				}
		  ).trigger("reloadGrid");
  	   }
  	});

  	$(window).on('resize.jqGrid', function () {
  		$("#view_bom_detail_grid-table-c").jqGrid( 'setGridWidth', $("#view_bom_detail_grid-div-c").width() - 3 );
  		$("#view_bom_detail_grid-table-c").jqGrid( 'setGridHeight', (document.documentElement.clientHeight - 
  		$("#view_bom_detail_grid-pager-c").height() - 380) );
  	});

  	$(window).triggerHandler('resize.jqGrid');

//将数据格式化成两位小数：四舍五入
function formatterNumToFixed(value,options,rowObj){
	if(value!=null){
		var floatNum = parseFloat(value);
		return floatNum.toFixed(2);
	}else{
		return "0.00";
	}
}
//删除
function updateInstoreStatus(){
	//弹出确认窗口
	layer.confirm( '确认删除？',
        function(){
		//确定按钮点击事件
			//后台修改表单状态为提交
		    $.ajax({
			    type:"POST",
			    url:context_path+"/bom/delBom",
			    data:{ids:$('#view_bom_detail_bomkey').val()},
			    dataType:"json",
			    success:function(data){
					 //刷新父窗口的表格数据
					 _grid.trigger("reloadGrid");  //重新加载表格  
					//关闭当前窗口
					layer.closeAll();
					if(data){
						layer.msg("版本删除成功！",{time:1200,icon:1});
					 }else{
						//修改成功
						layer.msg("版本删除失败!",{icon:1,time:1200});
					 }
			   }
		    });
        },
        function(){
        	//取消按钮点击事件
        },
        { title:'操作提示',
            yesLabel: '确定',
            noLabel:'取消',
            follow: document.getElementById('confirmBtn')
        }
    );
} 
</script>
