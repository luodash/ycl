<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<style type="text/css"></style>
<div id="bom_list_grid-div">
  <form id="bom_list_hiddenForm" action="<%=request.getContextPath()%>/bom/bomExcel" method="POST" style="display: none;">	 	
	<input id="bom_list_idd" name="idd" value=""/> 
  </form>
  <form id="bom_list_hiddenTempForm" action="<%=path%>/bom/downloadTemplate" method="get" style="display:none;">
	    <input type="hidden" id="bom_list_uploadtype" name="type" />
  </form> 
  <form id="bom_list_hiddenQueryForm" style="display:none;">
    <input name="bomNo" id="bom_list_bomNo" value="" />
    <input id="bom_list_bomName" name="bomName" value="" />
    <input id="bom_list_productId" name="productId" value="" />
  </form>
  <div class="query_box" id="bom_list_yy" title="查询选项">
            <form id="bom_list_queryForm" style="max-width:100%;">
			 <ul class="form-elements">
				<li class="field-group field-fluid3">
					<label class="inline" for="bom_list_bomNo" style="margin-right:20px;width: 100%;">
						<span class="form_label" style="width:80px;">BOM编号：</span>
						<input type="text" name="bomNo" id="bom_list_bomNo" value="" style="width: calc(100% - 85px);" placeholder="BOM编号">
					</label>			
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="bom_list_bomName" style="margin-right:20px;width: 100%;">
						<span class="form_label" style="width:80px;">BOM名称：</span>
						<input type="text" name="bomName" id="bom_list_bomName" value="" style="width:  calc(100% - 85px);" placeholder="BOM名称">
					</label>					
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="bom_list_productId" style="margin-right:20px;width: 100%;">
						<span class="form_label" style="width:80px;">产品：</span>
						<input type="text" name="productId" id="bom_list_productId" value="" style="width:calc(100% - 85px);" placeholder="产品">
					</label>					
				</li>
				
			</ul>
			<div class="field-button" style="">
						<div class="btn btn-info" onclick="queryOk();">
				            <i class="ace-icon fa fa-check bigger-110"></i>查询
			            </div>
						<div class="btn" onclick="reset();"><i class="ace-icon icon-remove"></i>重置</div>
		        </div>
		  </form>		 
    </div>
    <div id="bom_list_fixed_tool_div" class="fixed_tool_div">
        <div id="bom_list___toolbar__" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="bom_list_grid-table" style="width:100%;height:100%;"></table>
    <div id="bom_list_grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
	var oriData; 
	var _grid;
	var openwindowtype = 0; //打开窗口类型：0新增，1修改
	var bomNo;
	var type=0;
	var dynamicDefalutValue="8b59c54591ac41179d6f71265de6d8d3";
	var _queryForm_data = iTsai.form.serialize($("#queryForm"));
	$(function  (){
	    $(".toggle_tools").click();
	});
	$("#bom_list___toolbar__").iToolBar({
	    id: "bom_list___tb__01",
	    items: [
	        {label: "添加", disabled:(${sessionUser.addQx} == 1?false:true), onclick:openAddPage, iconClass:'glyphicon glyphicon-plus'},
	        {label: "编辑", disabled:(${sessionUser.editQx} == 1?false:true),onclick: openEditPage, iconClass:'glyphicon glyphicon-pencil'},
	        {label: "删除", disabled:(${sessionUser.deleteQx} == 1?false:true),onclick: deleteBom, iconClass:'glyphicon glyphicon-trash'},
	        {label: "导出", disabled:(${sessionUser.queryQx}==1?false:true),onclick:toExcel,iconClass:' icon-share'},
	        {label: "导入", disabled:(${sessionUser.queryQx}==1?false:true),onclick:uploadExcel,iconClass:'icon-upload'},
	        {label: "模板下载",disabled:(${sessionUser.queryQx}==1?false:true),onclick:function(){templet();},iconClass:'icon-cloud-download'}
	   ]
	});
_grid = jQuery("#bom_list_grid-table").jqGrid({
		url : context_path + "/bom/list.do",
	    datatype : "json",
	    colNames : [ "主键", "清单名称","清单编号","创建时间", "产品","备注","成本"],
	    colModel : [ 
	                {name: "id",index : "id",hidden:true},
	                {name: "bomName",index : "bomName",width : 50},
	                {name: "bomNo",index : "bomNo",width : 60}, 
	                {name: "bomcreatetime1",index : "bomCreateTime",width : 100,formatter:function(cellVal,option,rowObject){
	                	if(cellVal){
	                		return cellVal.substring(0,19);
	                	}else{
	                		return "";
	                	}	 
	                }},
	                {name: "productTypeName",index: "productTypeName",width: 50},
	                {name : "remark",index : "remark"},
	                {name : "cost",index: "cost",width: 50}
	               ],
	    rowNum : 20,
	    rowList : [ 10, 20, 30 ],
	    pager : "#bom_list_grid-pager",
	    sortname : "id",
	    sortorder : "desc",
           altRows: true,
           viewrecords : true,
           autowidth: true,
           multiselect:true,
           multiboxonly: true,
		beforeRequest:function (){
			dynamicGetColumns(dynamicDefalutValue,"bom_list_grid-table",$(window).width()-$("#sidebar").width() -7);
			//重新加载列属性
		},
           loadComplete : function(data) 
           {
           	var table = this;
           	setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
           	oriData = data;
           },
           emptyrecords: "没有相关记录",
           loadtext: "加载中...",
           pgtext : "页码 {0} / {1}页",
           recordtext: "显示 {0} - {1}共{2}条数据"
});

jQuery("#bom_list_grid-table").navGrid("#bom_list_grid-pager",{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd('#bom_list_grid-pager',{  
	caption:"",   
	buttonicon:"fa fa-refresh green",   
	onClickButton: function(){   
		$("#bom_list_grid-table").jqGrid("setGridParam", 
				{
			postData: {queryJsonString:""} //发送数据 
				}
		).trigger("reloadGrid");
	}
}).navButtonAdd("#bom_list_grid-pager",{
    caption: "",
    buttonicon:"fa  icon-cogs",
    onClickButton : function (){
        jQuery("#bom_list_grid-table").jqGrid("columnChooser",{
            done: function(perm, cols){
                dynamicColumns(cols,dynamicDefalutValue);
                $("#bom_list_grid-table").jqGrid( 'setGridWidth', $("#bom_list_grid-div").width()-7);
            }
        });
    }
});

$(window).on("resize.jqGrid", function () {
	$("#bom_list_grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);
	$("#bom_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-
	$("#bom_list_yy").outerHeight(true)-$("#bom_list_fixed_tool_div").outerHeight(true)-
	$("#bom_list_grid-pager").outerHeight(true)-$("#gview_bom_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
});

$(window).triggerHandler('resize.jqGrid');
function gridReload(){
	_grid.trigger("reloadGrid");  //重新加载表格  
}

/**
 * 查询按钮点击事件
 */
function queryOk(){
	var queryParam = iTsai.form.serialize($("#bom_list_queryForm"));
	queryBomByParam(queryParam);
}
/**
 * 重置按钮点击事件
 */
function reset(){
    $("#bom_list_queryForm #bom_list_productId").select2("val","");
	iTsai.form.deserialize($("#bom_list_queryForm"),_queryForm_data); 
	queryBomByParam(_queryForm_data);
}
/**
 * 查询功能:获取查询页面中的值，并将值放入列表页面中隐藏的form
 * @param jsonParam     查询页面传递过来的json对象
 */
function queryBomByParam(jsonParam){
	var queryJsonString = JSON.stringify(jsonParam);         //将json对象转换成json字符串
	//执行查询操作
	$("#bom_list_grid-table").jqGrid("setGridParam", 
			{
		postData: {queryJsonString:queryJsonString}
			}
	).trigger("reloadGrid");
}
function openAddPage(){
	type=0;
	openwindowtype = 0;
	layer.load(2);
	$.post(context_path+"/bom/toAdd.do?pId=0", {}, function(str){
		$queryWindow = layer.open({
			    title : "清单添加", 
		    	type: 1,
		    	skin : "layui-layer-molv",
		    	area : ["1100px","650px"],
		    	shade: 0.6, //遮罩透明度
		    	shadeClose: true,
	    	    moveType: 1, //拖拽风格，0是默认，1是传统拖动
		    	content:str,//注意，如果str是object，那么需要字符拼接。
		    	success:function(layero, index){
		    		layer.closeAll("loading");
		    	}
			});
		}).error(function() {
			layer.closeAll();
    		layer.msg("加载失败！",{icon:2});
		});
}

/*打开编辑页面*/
function openEditPage(){
	var selectAmount = getGridCheckedNum("#bom_list_grid-table");
	if(selectAmount==0){
		layer.msg("请选择一条记录！",{icon:2});
		return;
	}else if(selectAmount>1){
		layer.msg("只能选择一条记录！",{icon:8});
		return;
	}
	openwindowtype = 1;
	layer.load(2);
	var id = jQuery("#bom_list_grid-table").jqGrid("getGridParam", "selarrrow");
	$.post(context_path+"/bom/toAdd.do?id="+id, {}, function(str){
		$queryWindow = layer.open({
			title : "清单编辑", 
			type: 1,
		    skin : "layui-layer-molv",
		    area : ["1100px","650px"],
			shade: 0.6, //遮罩透明度
			moveType: 1, //拖拽风格，0是默认，1是传统拖动
			content: str,//注意，如果str是object，那么需要字符拼接。
			success:function(layero, index){
				   layer.closeAll("loading");
				    	}
				});
			}).error(function() {
				layer.closeAll();
		    	layer.msg("加载失败！",{icon:2});
		});
}
function deleteBom() {
    var checkedNum = getGridCheckedNum("#bom_list_grid-table", "id");  //选中的数量
    if (checkedNum == 0) {
    	layer.alert("请选择一个要删除的Bom！");
    } else {
        //从数据库中删除选中的供应商，并刷新供应商表格
        var ids = jQuery("#bom_list_grid-table").jqGrid("getGridParam", "selarrrow");
        layer.confirm("确定删除选中的Bom？", function() {
    		$.ajax({
    			type : "POST",
    			url : context_path + "/bom/deleteBom.do?ids="+ids ,
    			dataType : "json",
    			cache : false,
    			success : function(data) {
    				if (data.result) {  				
    					layer.msg(data.msg,{icon: 1,time:1000});
    				}else{
    					layer.msg(data.msg,{icon: 7,time:1000});
    				}
    				_grid.trigger("reloadGrid");  //重新加载表格
    			}
    		});
    	});
        
    }
}
function uploadExcel(){
	$.post(context_path+"/bom/toFileIn", {}, function(str){
		$queryWindow = layer.open({
			title : "上传文件", 
			type: 1,
		    skin : "layui-layer-molv",
		    area : "600px",
			shade: 0.6, //遮罩透明度
			moveType: 1, //拖拽风格，0是默认，1是传统拖动
			content: str,//注意，如果str是object，那么需要字符拼接。
			success:function(layero, index){
			   layer.closeAll("loading");
	    	}
		});
	}).error(function() {
		layer.closeAll();
    	layer.msg("加载失败！",{icon:2});
});
}
function toExcel(){
	var checkedNum = getGridCheckedNum("#bom_list_grid-table", "id");
	if (checkedNum == 0) {
    	layer.alert("请选择一个要导出的Bom！");
    }else if(checkedNum>1){
    	layer.alert("只能选择一个Bom！");
    }else{
    	var idd = jQuery("#bom_list_grid-table").jqGrid("getGridParam", "selrow");
    	$("#bom_list_idd").val(idd);
    	$("#bom_list_hiddenForm").submit();
    }	
}
$("#bom_list_queryForm #bom_list_productId").select2({
        placeholder: "选择产品",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/material/getMaterialSelectList",
            type: "POST",
            dataType: "json",
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });
   function templet(){
     $("#bom_list_hiddenTempForm").submit();
   }
</script>