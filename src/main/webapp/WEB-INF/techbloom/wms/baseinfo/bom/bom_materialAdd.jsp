<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<div id="detail_edit_page" class="row-fluid" style="height: inherit;">
	<form id="detailForm" class="form-horizontal" style="overflow: auto; height: calc(100% - 70px);">
		<input type="hidden" id="bomId" name="bomId" value="${bomId }">
		<div class="control-group">
			<label class="control-label" for="type">添加类型：</label>
			<div class="controls">
				<select id="type" name="type" data-placeholder="请选择添加类型" style="width:300px;">
					<option value="1" >类别添加</option>
					<option value="0" >物料添加</option>
				</select>
			</div>
		</div>
		<div class="control-group" id="xx">
			<label class="control-label" for="bomDetailNo">编号：</label>
			<div class="controls">
				<input type="text" id="bomDetailNo" name="bomDetailNo" style="width: 300px;" placeholder="后台自动生成" readonly="readonly" value="${bomDetailNo }">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="bomDetailName">添加内容：</label>
			<div class="controls" id="bbox">				
				<input type="text" id="bomDetailName" name="bomDetailName" style="width: 300px;" placeholder="添加类别">		
			</div>
			<div class="controls" id="zz">				
				<input id="materialId"  name="materialId" style="width:300px;"/>
			</div>
		</div>
	</form>
	<div class="field-button" style="text-align: center;border-top: 0px;margin: 15px auto;">
		<span class="btn btn-info" onclick="saveMaterial();">
		   <i class="ace-icon fa fa-check bigger-110"></i>保存
		</span>
		<span class="btn btn-danger" onclick="layer.close($queryWindow);return false;">
		   <i class="icon-remove"></i>&nbsp;取消
		</span>
	</div>
</div>
<script type="text/javascript">
var context_path= "<%=path%>";
var oriData; 
function saveMaterial(){
   if(currentNode.id==0&&$("#detailForm #type").val()==0){
       layer.alert("第一级只可添加类别，不可添加物料！");
   }else{
       saveMaterialInfo($("#detailForm").serialize());
   } 		
}
var pId = ${parentId};
function saveMaterialInfo(bean){
	var cId=${selectid}
	if(bean){
		$.ajax({
			url:context_path+"/bom/addMaterial?bomId="+cId+"&materialId="+$("#materialId").val()+"&parentId="+${parentId} ,
			type:"POST",
			data:bean,
			dataType:"JSON",
			success:function(data){
				if(Boolean(data.result)){
					layer.msg("保存成功！",{icon:1});
					//刷新列表
					$("#grid-table-m").jqGrid("setGridParam", 
						{
							postData: {queryJsonString:"",newId:cId} //发送数据 
						}
					).trigger("reloadGrid"); 
					refreshTree();
					layer.close($queryWindow);
				}else{
					layer.msg(data.msg,{icon:2});
				}
			}
		});
	}else{
		layer.msg("出错啦！",{icon:2});
	}
}
//查询物料
function queryMaterial(){
var queryParam = iTsai.form.serialize($("#querym_form"));
	var queryJsonString = JSON.stringify(queryParam);
	$("#grid-table-add").jqGrid("setGridParam", 
			{
				postData: {materialId:"",queryJsonString: queryJsonString} //发送数据 
			}
	).trigger("reloadGrid");
}


function initLeftH(){
	$("#leftdiv").height($(window).height()-$("#user-nav").height()-$("#breadcrumb").height());
}
$(window).on("resize",initLeftH());


$("#detailForm #type").val("1");
if($("#detailForm #type").val()==1){
				$("#xx").show();
				$("#bbox").show();
				$("#zz").hide();
			}else if($("#detailForm #type").val()==0){
				$("#xx").hide();
				$("#bbox").hide();
				$("#zz").show();
			}
$("#type").select2({
    minimumInputLength:0,
    allowClear:true,
    delay:250,
    width:300,
    formatNoMatches:"没有结果",
	formatSearching:"搜索中...",
	formatAjaxError:"加载出错啦！"
});
$("#type").on("change.select2",function(){
    $("#type").trigger("keyup")}
);
$("#materialId").select2({
	placeholder: "选择物料",
	minimumInputLength:0,   //至少输入n个字符，才去加载数据
    allowClear: true,  //是否允许用户清除文本信息
	delay: 250,
	width:300,
	formatNoMatches:"没有结果",
	formatSearching:"搜索中...",
	formatAjaxError:"加载出错啦！",
	ajax : {
		url: context_path+"/material/getMaterialSelectList",
		type:"POST",
		dataType : "json",
		delay : 250,
		data: function (term,pageNo) {     //在查询时向服务器端传输的数据
            term = $.trim(term);
            return {
            	queryString: term,    //联动查询的字符
            	pageSize: 15,    //一次性加载的数据条数
                pageNo:pageNo,    //页码
                time:new Date()   //测试
             }
        },
        results: function (data,pageNo) {
        		var res = data.result;
	            if(res.length>0){   //如果没有查询到数据，将会返回空串
	               var more = (pageNo*15)<data.total; //用来判断是否还有更多数据可以加载
	               return {
	                    results:res,more:more
	                 };
	            }else{
	            	return {
	            		results:{}
	            	};
	            }
        },
		cache : true
	}

});
$("#materialId").on("change.select2",function(){
    $("#materialId").trigger("keyup")}
);


$("#detailForm #type").change(function(){
	//将隐藏的存放内容主键的文本框的值清空
	$("#detailForm #bomDetailName").val("");
	//清空material下拉框选中的值
	$("#detailForm #materialId").select2("val", "");
	//根据盘点类型获取不同的盘点内容
	if($("#detailForm #type").val()==1){
		//显示类别，隐藏物料
		$("#xx").show();
		$("#bbox").show();
		$("#zz").hide();
	}else if($("#detailForm #type").val()==0){
		//显示物料，隐藏类别
		$("#xx").hide();
	    $("#bbox").hide();
	    $("#zz").show();
	}
});
</script>