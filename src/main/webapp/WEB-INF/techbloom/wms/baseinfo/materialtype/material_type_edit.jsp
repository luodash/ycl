<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<div id="material_type_edit_page" class="row-fluid" style="height: inherit;">
	<form id="materialTypeForm" class="form-horizontal" style="overflow: auto; height: calc(100% - 70px);">
		<input type="hidden" id="materialId" name="id" value="${MTYPE.id}" />
		<input type="hidden" id="delstatus" name="delstatus" value="${MTYPE.delstatus}" />
		<input type="hidden" id="materialTypeParentId" name="materialTypeParentId" value="${MTYPE.materialTypeParentId }">
		<div class="control-group">
			<label class="control-label" for="typeNo">类别编号：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input class="span11" type="text"  name="typeNo" id="typeNo" value="${MTYPE.typeNo }" placeholder="后台自动生成" readonly="readonly">
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="typeName">类别名称：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input class="span11" type="text"  name="typeName" id="typeName" value="${MTYPE.typeName }" placeholder="物料类别名称">
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="parentTypeName">所属类别：</label>
			<div class="controls">
				<input class="span11" type="text" value="${MTYPE.parentTypeName }" placeholder="物料类别名称" readonly ="readonly">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="remark">备注：</label>
			<div class="controls">
				 <textarea class="span11" rows="4" cols="20"  name="remark" id="remark"  style="width: 435px;" placeholder="备注">${MTYPE.remark }</textarea>
			</div>
		</div>
	</form>
	<div class="field-button" style="text-align: center;border-top: 0px;margin: 10px auto;">
		<span class="btn btn-info" onclick="saveForm();">
		   <i class="ace-icon fa fa-check bigger-110"></i>保存
		</span>
		<span class="btn btn-danger" onclick="layer.closeAll();">
		   <i class="icon-remove"></i>&nbsp;取消
		</span>
	</div>
</div>
<script type="text/javascript">
$("#materialTypeForm").validate({
        rules: {
            "typeName": {
                required: true,
                maxlength:50,
                remote:context_path+"/materialType/isHaveMaterialTypeName?id="+$('#materialId').val()
            },
        },
        messages: {
            "typeName": {
                required: "请填写类别名称!",
                maxlength:"长度不能超过50个字符！",
                remote:"您输入的类别名称已存在，请重新输入！"
            }
        },
        errorClass: "help-inline",
		errorElement: "span",
  		highlight: function (element) {//高亮显示指定的元素
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function (element) {
			$(element).parents('.control-group').removeClass('error');
		}
    });
	function saveForm() {
        if ($("#materialTypeForm").valid()) {
            saveMaterialType($("#materialTypeForm").serialize());
        }
    }
    function saveMaterialType(bean) {
        $.ajax({
                url: context_path + "/materialType/saveMaterialType.do",
                type: "POST",
                data: bean,
                dataType: "JSON",
                success: function (data) {
                    if (Boolean(data.result)) {
                        layer.msg("保存成功！", {icon: 1});
                        var node=complexTree.getNodeByParam("id",selectTreeId);
                        node.isParent=true;
	                    complexTree.reAsyncChildNodes(node, "refresh",false);
                        layer.closeAll();
                        $("#grid-table").jqGrid("setGridParam", 
								{
							postData: {queryJsonString:""} //发送数据 
								}
						).trigger("reloadGrid");
                    } else {
                        layer.alert("保存失败，请稍后重试！", {icon: 2});
                    }
                },
                error:function(XMLHttpRequest){
            		alert(XMLHttpRequest.readyState);
            		alert("出错啦！！！");
            	}
            });
    }
</script>