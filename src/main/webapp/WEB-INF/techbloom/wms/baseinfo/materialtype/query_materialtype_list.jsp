<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
	<script type="text/javascript">
		var context_path = "<%=path%>";
	</script>
	<link rel="stylesheet" href="<%=path%>/plugins/ace/assets/css/select2.css" />
	<link rel="stylesheet" href="<%=path%>/plugins/ace//assets/css/font-awesome.css" />
	<style type="text/css">
	</style>
  </head>
  <body>
  	<div style="padding-left:30px;">
   	    <form id="query_materialtype_list_queryForm1">
			<ul class="form-elements">
				<!-- 类别名称 -->
				<li class="field-group">
					<label class="inline" for="query_materialtype_list_typeName" style="margin-right:20px;">
						类别名称：
						<input type="text" name="typeName" id="query_materialtype_list_typeName" value="${MTYPE.typeName }"
						style="width: 200px;" placeholder="类别名称"/>
					</label>
				</li>
				<!-- 底部工具按钮 -->
				<li class="field-group">
					<div class="field-button">
						<div class="btn btn-info" onclick="queryOk();">
				            <i class="ace-icon fa fa-check bigger-110"></i>确定
			            </div> &nbsp; &nbsp;
						<div class="btn" onclick="layer.closeAll();"><i class="icon-remove"></i>&nbsp;取消</div>
					</div>
				</li>
			</ul>
		</form>
  	</div>
  	<script type="text/javascript" src="<%=path%>/plugins/public_components/js/select2.js"></script>
  <script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
  <script type="text/javascript">
  
		function queryOk(){
			//var formJsonParam = $('#queryForm').serialize();
			var queryParam = iTsai.form.serialize($('#query_materialtype_list_queryForm1'));
			//执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
			queryTypeListByParam(queryParam);
			//操作成功,关闭当前子窗口
	    	layer.closeAll();
		}
		
		//弹出物料类别选择窗口
		function openSelectWindow(){
			Dialog.open({
			    title: "物料类别", 
			    width: 500,  height: '500',
			    url: context_path + "/material/getMaterialTypeList?mid="+(-1),
			    theme : "simple",
				drag : true,
				clickMaskToClose: false
			});
		}

		//为指定id的元素赋值
		function setValueForElementById(elementId,value){
			$('#'+elementId).val(value);
		}
  </script>
