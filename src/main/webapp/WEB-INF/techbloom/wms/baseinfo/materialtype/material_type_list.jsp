<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

	<style type="text/css">
        #material_type_list_container {
            width: 100%;
            height: 100%;
            position: absolute;
        }

        #material_type_list_typeTreeDiv {
            width: 200px;
            height: 100%;
            border-right: solid 1px #cdcfd0;
            float: left;
            padding-left: 5px;
            padding-top: 10px;
            display: inline;
        }

        #material_type_list_grid-div {
            width: calc(100% - 220px);
            height: 100%;
            float: left;
            padding: 5px 0 5px 5px;
        }
    </style>

<!-- 物料类别管理页面 -->
	<div id="material_type_list_container">
	<!-- 物料类别树 -->
		<div id = "material_type_list_typeTreeDiv" class="ztree" style="overflow-y:auto;"></div>
		<!-- 隐藏区域：存放查询条件 -->
	<form id="material_type_list_hiddenForm" action="<%=path%>/materialType/materialTypeExcel" method="POST" style="display: none;">
	<!-- 选中的用户 -->
       <input type="text" id="material_type_list_ids" name="ids" value=""/>
       <input type="text" id="material_type_list_typeId" name="typeIdddd" value="">
    </form>
		<form id="material_type_list_hiddenQueryForm" style="display:none;">
			<!-- 类别名称 -->
			<input id="material_type_list_typeName" name="typeName" value="" />
			<!-- 类别层级 -->
			<input id="material_type_list_level" name="level" value="0" />
		</form>		
		<!-- 物料类别表格 -->
		<div id="material_type_list_grid-div">
			<!--表格工具栏 -->
			<div class="query_box" id="material_type_list_yy" title="查询选项">
            <form id="material_type_list_queryForm" style="max-width:100%;">
			 <ul class="form-elements">
				<li class="field-group field-fluid3">
					<label class="inline" for="material_type_list_typeName" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:100px;">物料类别名称：</span>
						<input type="text" name="typeName" id="material_type_list_typeName" value="" style="width: calc(100% - 105px);" placeholder="物料类别名称" />
					</label>			
				</li>				
			</ul>
			<div class="field-button" style="">
					<div class="btn btn-info" onclick="queryOk();">
				        <i class="ace-icon fa fa-check bigger-110"></i>查询
			        </div>
					<div class="btn" onclick="reset();"><i class="ace-icon icon-remove"></i>重置</div>
		        </div>
		 </form>		 
    </div>
	        <div id="material_type_list_fixed_tool_div" class="fixed_tool_div">
	            <div id="material_type_list___toolbar__" style="float:left;overflow:hidden;"></div>
	        </div>
			<!--物料类别信息表格 -->
			<table id="material_type_list_grid-table" style="width:100%;height:100%;"></table>
			<!--表格分页栏 -->
			<div id="material_type_list_grid-pager"></div>
	   </div>
   </div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript" src="<%=path%>/static/js/techbloom/wms/baseinfo/materialType.js"></script>
<script type="text/javascript">
var context_path = '<%=path%>';
var oriData;      //表格数据
//获取json数据
var selectTreeId = -1; //存放选中的树节点id
var complexTree;
var setting = {
		data: {
			simpleData: {
				enable: true,
				idKey: "id",
				pIdKey: "pid"
			},
			key: {
				name: "text"
			}
		},
		callback: {
			onClick: function(evt, id, node){
				reloadGridData(node.id);
				$("#material_type_list_level").val(node.level);
				$("#material_type_list_typeId").val(node.id);
			}
		},
        async: {
            enable: true,
            url:context_path+"/materialType/getMtypeTree",
            autoParam:["id"],
            otherParam:{"parentId":function (){
                return selectTreeId;}},
            type: "POST"
        }
	};
initTree();
//初始化树
function initTree() {
        $.ajax({
            type: "POST",
            url: context_path + "/materialType/getMtypeTree?parentId="+selectTreeId,
            dataType: "json",
            cache: false,
            success: function (dataJson) {
            	
                dataJson.push({
                    id: 0,
                    pid: 0,
                    text: "所属物料类别",
                    open:true
                });
	
			$(document).ready(function(){
				complexTree = $.fn.zTree.init($("#material_type_list_typeTreeDiv"), setting, dataJson);
				complexTree.expandNode(true);
			});
                //-----------------初始化树----------------------------
            }
        });
    }
    
    
//工具栏
$("#material_type_list___toolbar__").iToolBar({
 	 id:"material_type_list___tb__01",
 	 items:[
 	    {label:"添加",disabled:(${sessionUser.addQx}==1?false:true),onclick:add,iconClass:'glyphicon glyphicon-plus'},
 	    {label:"编辑",disabled:(${sessionUser.editQx}==1?false:true),onclick:editMaterial,iconClass:'glyphicon glyphicon-pencil'},
  	  	{label:"删除",disabled:(${sessionUser.deleteQx}==1?false:true),onclick:delMaterial,iconClass:'glyphicon glyphicon-trash'},
		{label:"导出",disabled:(${sessionUser.queryQx}==1?false:true),onclick:excelMaterial,iconClass:'icon-share'}
  	  ]
});
$(function(){
    $(".toggle_tools").click();
});
function standCropPrint() {
	var selectid = getId("#material_type_list_grid-table","id");
	function getId(_grid,_key){
	    var idAddr = jQuery(_grid).getGridParam("selarrrow");
	    var ids="";
	    ids=idAddr+",";
	    return ids;
	}
	$("#material_type_list_ids").val(selectid);
    var url = context_path + "/materialType/standCropPrint?ids=" + $("#material_type_list_ids").val()+"&typeIdddd="+$("#material_type_list_typeId").val();
    window.open(url);
}

function aaa(){
	var selectid = getId("#material_type_list_grid-table","id");
	function getId(_grid,_key){
	    var idAddr = jQuery(_grid).getGridParam("selarrrow");
	    var ids="";
	    ids=idAddr+",";
	    return ids;
	}
	$("#material_type_list_ids").val(selectid);
	$("#material_type_list_hiddenForm").submit();
}
/**
 * 初始化表格数据
 */
function initGridData(){
	//初始化表格数据：根据id获取子类别
	jQuery("#material_type_list_grid-table").jqGrid({
	       url : context_path + "/materialType/materialTypeList",
	       datatype : "json",
	       colNames : [ "物料类别主键","物料类别编码","物料类别名称","所属物料类别",  "备注"],
	       colModel : [
						{name : "id",index : "id",width : 55,hidden:true},
						{name : "typeNo",index : "materialTypeNo",width : 60},
	                    {name : "typeName",index : "materialTypeName",width : 60},
						{name : "parentTypeName",index : "parentTypeName",width : 60,
							formatter:function(cellvalue, options, rowObject){
								if(cellvalue==""){
									return "基础分类";
								}else{
									return cellvalue;
								}
							}
						},
	                    {name : "remark",index : "REMARK",width : 70}
	                  ],
	       rowNum : 20,
	       rowList : [ 10, 20, 30 ],
	       pager : "#material_type_list_grid-pager",
	       sortname : "id",
	       sortorder : "desc",
	       altRows: true,
	       viewrecords : true,
	       //caption : "物料类别列表",
	       autowidth:true,
	       multiselect:true,
           multiboxonly: true,
	       loadComplete : function(data)
	       {
	           var table = this;
	           setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
	           oriData = data;
	       },
		   emptyrecords: "没有相关记录",
		   loadtext: "加载中...",
		   pgtext : "页码 {0} / {1}页",
		   recordtext: "显示 {0} - {1} | 共{2}条数据"
	  });
}
var selectTreeId = 0;
/**
 * 重新加载表格数据
 */
function reloadGridData(parentTreeId){
	selectTreeId = parentTreeId;
	$("#material_type_list_grid-table").jqGrid("setGridParam",
			{
				postData: {typeId:parentTreeId,jsonString:""} //发送数据  :选中的节点
			}
	).trigger("reloadGrid");
}

$(function(){
	//初始化表格数据：根据id获取子类别
	initGridData();
    //在分页工具栏中添加按钮
    jQuery("#material_type_list_grid-table").navGrid("#material_type_list_grid-pager",{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#grid-pager",{
	   caption:"",
	   buttonicon:"fa fa-refresh",
	   onClickButton: function(){
		   jQuery("#grid-table").trigger("reloadGrid");  //重新加载表格
	   }
	});

	$(window).on("resize.jqGrid", function () {
		$("#grid-table").jqGrid("setGridWidth", $("#grid-div").width() );
		$("#grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-$("#material_type_list_yy").outerHeight(true)-
		$("#material_type_list_fixed_tool_div").outerHeight(true)-$("#material_type_list_grid-pager").outerHeight(true)-
		$("#gview_material_type_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
	})
	$(window).triggerHandler("resize.jqGrid");
});
var _queryForm_data = iTsai.form.serialize($("#material_type_list_queryForm"));
function queryOk(){
	var queryParam = iTsai.form.serialize($("#material_type_list_queryForm"));
	queryTypeListByParam(queryParam);
	
}
function reset(){
	iTsai.form.deserialize($("#material_type_list_queryForm"),_queryForm_data); 
	queryTypeListByParam(_queryForm_data);	
}
</script>

