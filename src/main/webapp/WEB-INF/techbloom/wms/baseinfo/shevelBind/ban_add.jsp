<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<script type="text/javascript">
    var context_path = "<%=path%>";
    function result(res,msg){
        if(res){
            layer.closeAll();
            layer.msg(msg,{icon:1,time:1200});
            //刷新用户列表
            reloadGridDatas();
        }else{
            layer.msg(msg,{icon:1,time:1200});
        }
    }
</script>
<div id="shevel_edit_page" class="row-fluid" style="height: inherit;">
	<form action="<%=path %>/wmsbind/saveGoods.do" id="areaRelationForm" method="post" target="_ifr" class="form-horizontal" style="overflow: auto; height: calc(100% - 70px); ">
		<input type="hidden" id="id" name="id" value="${ac.id}">
		<div class="control-group">
			<label class="control-label" for="stuff_id">所属库区货架：</label>
			<div class="controls">
				<input id="stuff_id" name="stuff_id" value="${sheve.id}" type="hidden">${sheve.shevelName}
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="locationCode">库位编号：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="locationCode" name="locationCode" value="${ac.locationCode}" readonly="readonly" placeholder="后台自动生成">
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="locationName">库位名称：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="locationName" name="locationName" value="${ac.locationName}" placeholder="库位名称">
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="row_number">库位行数：</label>
			<div class="controls">
			   <div class="input-append span12 required">
				<input type="text" class="span11" id="row_number" name="row_number" value="${(ac.row_number==null||ac.row_number==0)?1:ac.row_number}"
					<c:if test="${ac.id!=null}">readonly</c:if> placeholder="库位行数">
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="place_number">库位列数：</label>
			<div class="controls">
			   <div class="input-append span12 required">
				<input type="text" class="span11" id="place_number" name="place_number" value="${(ac.place_number==null||ac.place_number==0)?1:ac.place_number}"
					<c:if test="${ac.id!=null}">readonly</c:if> placeholder="库位列数">
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="instorageTypeSelect">库位类型：</label>
			<input type="hidden" id="type" name="type" value="${ac.type }">
			<div class="controls">
				<select class="mySelect2" id="instorageTypeSelect" name="instorageTypeSelect" data-placeholder="请选择库位类型" style="width:435px;">
					<option value=""></option>
					<c:forEach items="${inTypes}" var="in">
						 <option value="${in.dictNum}"
						     <c:if test = "${ac.type==in.dictNum }"> selected="selected"</c:if>>${in.dictValue}
						 </option>
					</c:forEach>
				</select>
			</div>
		</div>
		<div class="control-group">
			<label class="inline" for="confusionMethod" style="margin-left:35px;">
					混放策略：&emsp;
					<input type="radio" id = "confusionMethod0" name="confusionMethod" value="0" checked> 是
					<input type="radio" id = "confusionMethod1" name="confusionMethod" value="1"> 否
				</label>
				<label class="inline" for="locked" style="margin-left:35px;">
					出入库锁：&emsp;
					<input type="radio" id = "locked0" name="locked" value="0" checked> 是
					<input type="radio" id = "locke1" name="locked" value="1"> 否
				</label>
		</div>
		<div class="control-group">
		<label class="inline" for="classify" style="margin-left:35px;">
					ABC分类：&emsp;
					<input type="radio" id = "classify0" name="classify" value="0" checked> A
					<input type="radio" id = "classify1" name="classify" value="1"> B
					<input type="radio" id = "classify2" name="classify" value="2"> C
				</label>
				<label class="inline" for="carryingCapacity" style="margin-left:10px;">
					承载量：&emsp;
					<input type="text" id = "carryingCapacity" name="carryingCapacity" value="${ac.carryingCapacity}" style="width:263px;">
				</label>		
		</div>
		<div class="control-group">
			<label class="control-label" for="remark">备注：</label>
			<div class="controls">
				<textarea class="input-xlarge" name="remark" id="remark" placeholder="描述" style="width:435px;">${ac.remark}</textarea>
			</div>
		</div>
	</form>
	<div class="field-button" style="text-align: center;border-top: 0px;margin: 15px auto;">
		<span class="savebtn btn btn-success" onclick="disableAddButton()">保存</span>
		<span class="btn btn-danger" onclick="layer.closeAll();">取消</span>
	</div>
</div>
<iframe src="about:blank" name="_ifr" height="0" class="hidden"></iframe>
<script type="text/javascript">
var preStatus=${ac.confusionMethod==null?0:ac.confusionMethod};
      var locked = ${ac.locked==null?0:ac.locked};
      var classify = ${ac.classify==null?0:ac.classify};

      $("#areaRelationForm input[type=radio][name=confusionMethod][value="+preStatus+"]").attr("checked",true).trigger("click");
      $("#areaRelationForm input[type=radio][name=locked][value="+locked+"]").attr("checked",true).trigger("click");
      $("#areaRelationForm input[type=radio][name=classify][value="+classify+"]").attr("checked",true).trigger("click");



  	$('#areaRelationForm .mySelect2').select2();
  	
  	$('#areaRelationForm #instorageTypeSelect').change(function(){
  		$('#areaRelationForm #type').val($('#areaRelationForm #instorageTypeSelect').val());
  	})
  	
	$('#areaRelationForm').validate({
		rules:{
  			"locationName":{
  				required:true
  			},
  			"allocation":{
  				required:true
  			},
			"row_number":{			//行数
                required:true,
                maxlength:2,
                range:[1,10]
			},
            "place_number":{//库位列数
                required:true,
                maxlength:2,
                range:[1,10]
            },
  		},
  		messages:{
  			"locationName":{
  				required:"请输入库位名称！"
  			},
  			"allocation":{
  				required:"请输入库位信息！"
  			},
            "row_number":{			//行数
                required:"请输入行数！",
                maxlength:"不能超过两位数"
            },
            "place_number":{			//行数
                required:"请输入列数",
                maxlength:"不能超过两位数"
            },
  		}
  	});
  	function disableAddButton() {
	    if ($("#areaRelationForm").valid()) {
            $("#areaRelationButton").attr("disabled", "disabled");
            if ($("#type").val() == "") {
                layer.msg("请选择库位类型", {icon: 1, time: 1200});
            } else {
                if ($("#id").val() == "") {
                    $("#areaRelationForm").submit();
                } else {
                    layer.confirm('库位类型更改后要重新贴库位的条码', /*显示的内容*/
                        {
                            shift: 6,
                            moveType: 1, //拖拽风格，0是默认，1是传统拖动
                            title: "操作提示", /*弹出框标题*/
                            icon: 3, /*消息内容前面添加图标*/
                            btn: ['确定', '取消']/*可以有多个按钮*/
                        }, function (index, layero) {
                            $("#areaRelationForm").submit();
                        }, function (index) {
                            //取消按钮的回调
                            layer.close(index);
                        })
                }
            }
        }
	}

	/** 表格刷新 */
	function gridReload() {
	    _grid.trigger("reloadGrid");  //重新加载表格
	}
</script>