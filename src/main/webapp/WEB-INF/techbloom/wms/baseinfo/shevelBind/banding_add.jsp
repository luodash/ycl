<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id ="user_edit_page" class="row-fluid" style="height: inherit;">
	<form id="queryForm" class="form-horizontal"  style="overflow: auto; height: calc(100% - 70px);">
		<input type="hidden" name="id" id="id" value="${BIND.id }">
		<div class="control-group">
			<label class="control-label" for="name">货架名称：</label>
			<div class="controls">
			<div class="span12" > 
			      <input class="span11" type="text" name="name"
			       value="${BIND.shevelName }" readonly id="name" placeholder="货架 名称">
			    </div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="project_code">货架编号：</label>
			<div class="controls">
				<input class="span11" type="text" name="stuff_project_codecode" readonly id="project_code"
				 value="${BIND.shevelNo }" placeholder="货架编号" />
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="device_addr">主控制器地址：</label>
			<input type="hidden" id="hideDeviceAddr" value="${BIND.device_addr }" />
			<div class="controls">
				<input class="span11 select2_input" type="text" name="device_addr" id="device_addr" />
			</div>
		</div>
	</form>
	<div class="form-actions" style="text-align: right;border-top: 0px;margin: 0px;">
	 <span  class="btn btn-success">保存</span>
     <span  class="btn btn-danger">取消</span>
	</div>
</div>

<script type="text/javascript">
(function(){
	function getId(_grid,_key){
	    var idAddr = jQuery(_grid).getGridParam("selarrrow");
	    var ids="";
	    ids=idAddr.join(",");
	    return ids;
	}
	//确定按钮点击事件
    $("#user_edit_page .btn-success").off("click").on("click", function(){
		//用户保存
    	var selectdeviceInfo = $("#device_addr").select2("data");
		if(selectdeviceInfo){
			if(!selectdeviceAddr || selectdeviceAddr.length==0){
	    		layer.alert("请选择一个控制器进行绑定！");
	    		return;
	    	}
		}else{
			layer.alert("请选择一个控制器进行绑定！");
    		return;
		}
		saveUserInfo($("#queryForm").serialize());
	});
	
	//取消按钮点击事件
	$("#user_edit_page .btn-danger").off("click").on("click", function(){
	    layer.close(openWindowIndex);
	});
	
	//保存/修改控制器信息
  	function saveUserInfo(bean){
  		if(bean){
  			$.ajax({
  				url:context_path+"/wmsbind/saveBundInfo?interfaceAuthority="+interfaceAuthority,
  				type:"POST",
  				data:{id: selectCtrlId,device_addr:$("#device_addr").select2("data").addr,deviceType: 1 },
  				dataType:"JSON",
  				success:function(data){
  					if(data.result){
  						layer.msg("保存成功！",{icon:1});
  						reloadGridData();
  						//关闭指定的窗口对象
  						layer.close(openWindowIndex);
  					}else{
  						if(data.msg){
  							layer.msg(data.msg,{icon:2});
  						}else{
  							layer.msg("保存失败，请稍后重试！",{icon:2});
  						}
  					}
  				}
  			});
  		}else{
  			layer.msg("出错啦！",{icon:2});
  		}
  	}
}());

</script>