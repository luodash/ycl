<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id ="user_edit_page" class="row-fluid" style="height: inherit;">
	<form id="queryForm" class="form-horizontal"  style="overflow: auto; height: calc(100% - 70px);">
		<input type="hidden" name="id" id="id" value="${BIND.id }">  <!-- 绑定关系id -->
		<div class="control-group">
			<label class="control-label" for="project_name">货架名称：</label>
			<div class="controls">
			<div class="input-append span12 required" > 
			      <input class="span11" type="text" name="project_name" 
			      value="${BIND.shevelName }" readonly id="project_name" placeholder="货架 名称">
			    </div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="line_number">定位货架：</label>
			<div class="controls">
				<input class="span11" type="text" name="line_number" readonly id="line_number"
				 value="${BIND.line_number }"	 placeholder="定位货架" />
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="row_number">定位层：</label>
			<div class="controls">
				<input class="span11" type="text" name="row_number" readonly id="row_number"
				 value="${BIND.row_number }" placeholder="定位层" />
			</div>
		</div>
		<div class="control-group" id="lineNumberDiv">
			<label class="control-label" for="number">定位列：</label>
			<div class="controls">
				<input class="span11" type="text" name="number" readonly id="number"
				 value="${BIND.number }" placeholder="定位列" />
			</div>
		</div>
		<div class="control-group" id="wirelessDiv">
		    <label class="control-label">感应头：</label>
		    <input type="hidden" id="hideWirelessAddr" value="${BIND.wirless_addr }" />
		    <div class="controls">  
		    	 <label><input type="radio" id="wireless_addr_01" name="wireless_addr" value="01" checked="true">01</label>
			     <label><input type="radio" id="wireless_addr_02" name="wireless_addr" value="02">02</label>
			     <label><input type="radio" id="wireless_addr_03" name="wireless_addr" value="03">03</label>
		    </div>
		</div>
		<div class="control-group">
			<label class="control-label" id="deviceAddrLable" for="device_addr">位控制器地址：</label>
			<input type="hidden" id="hideDeviceAddr" value="${BIND.device_addr }" />
			<div class="controls">
				<input class="span11 select2_input" type="text" name="device_addr" id="device_addr" />
			</div>
		</div>
	</form>
	<div class="form-actions" style="text-align: right;border-top: 0px;margin: 0px;">
	 <span  class="btn btn-success">保存</span>
     <span  class="btn btn-danger">取消</span>
	</div>
</div>

<script type="text/javascript">
(function(){
	//初始化天线感应头的值
	$("#wireless_addr_"+$("#hideWirelessAddr").val()).trigger("click");
	
   
	//确定按钮点击事件
    $("#user_edit_page .btn-success").off("click").on("click", function(){
		//用户保存
    	var selectvalue = $("input[type=radio]:checked").val();   //获取单选框选中的值
    	var selectdeviceInfo = $("#device_addr").select2("data");
		if(selectdeviceInfo){
			if(!selectdeviceAddr || selectdeviceAddr.length==0){
	    		layer.alert("请选择一个控制器进行绑定！");
	    		return;
	    	}
		}else{
			layer.alert("请选择一个控制器进行绑定！");
    		return;
		}
		saveUserInfo(selectvalue);
	});
	
	//取消按钮点击事件
	$("#user_edit_page .btn-danger").off("click").on("click", function(){
	    layer.close(openWindowIndex);
	});
	//位控制器绑定关系保存
  	function saveUserInfo(wireless){
  			$.ajax({
  				url:context_path+"/wmsbind/saveBundInfo?interfaceAuthority="+interfaceAuthority,
  				type:"POST",
  				data:{id: selectCtrlId,device_addr:$("#device_addr").select2("data").addr,wireless_addr:wireless,deviceType: 0 },
  				dataType:"JSON",
  				success:function(data){
  					if(data.result){
  						layer.msg("保存成功！",{icon:1});
  						//刷新用户列表
  						reloadGridData();
  						//关闭当前窗口
  						//关闭指定的窗口对象
  						layer.close(openWindowIndex);
  					}else{
  						if(data.msg){
  							layer.msg(data.msg,{icon:2});
  						}else{
  							layer.msg("保存失败，请稍后重试！",{icon:2});
  						}
  					}
  				}
  			});
  	}
}());

</script>