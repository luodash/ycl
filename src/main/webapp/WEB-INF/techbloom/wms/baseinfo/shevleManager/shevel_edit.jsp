<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<div id="shevel_edit_page" class="row-fluid" style="height: inherit;">
	<form id="ShevelForm" class="form-horizontal" style="overflow: auto; height: calc(100% - 70px);">
		<input type="hidden" id="id" name="id" value="${shevel.id}">
  	        <input type="hidden" id="ip" name="ip" value="">
  	        <input type="hidden" id="name" name="name" value="">
  	        <input type="hidden" id="port" name="port" value="">
  	        <input type="hidden" id="interfaceAuthority" name="interfaceAuthority" value="">
		    <input type="hidden" value="${areaParent.id }" id="pIdddd" name="pIdddd">
		    <input type="hidden" value="${areaParent.parentId }" id="area_idddddd" name="area_idddddd">
		<div class="control-group">
			<label class="control-label" for="area_id">所属库区：</label>
			<div class="controls">
				<input id="area_id" name="area_id" value="${areaParent.id}" type="hidden">${shevel.warehouse_name}
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="shevelNo">货架编号：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="shevelNo" name="shevelNo" value="${shevel.shevelNo}" readonly="readonly" placeholder="后台自动生成">
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="shevelName">货架名称：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="shevelName" name="shevelName" value="${shevel.shevelName}" placeholder="货架名称">
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="row_number">货架行数：</label>
			<div class="controls">
			   <div class="input-append span12 required">
				<input type="text" class="span11" id="row_number" name="row_number" value="${(shevel.row_number==null||shevel.row_number==0)?1:shevel.row_number}"
					<c:if test="${shevel.id!=null}">readonly</c:if> placeholder="货架行数">
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="place_number">货架位数：</label>
			<div class="controls">
			   <div class="input-append span12 required">
				<input type="text" class="span11" id="place_number" name="place_number" value="${(shevel.place_number==null||shevel.place_number==0)?1:shevel.place_number}"
					<c:if test="${shevel.id!=null}">readonly</c:if> placeholder="货架位数">
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="instorageTypeSelect">库位类型：</label>
			<input type="hidden" id="type" name="type" value="${shevel.type }">
			<div class="controls">
				<select class="mySelect2" id="instorageTypeSelect" name="instorageTypeSelect" data-placeholder="请选择库位类型" style="width:435px;">
					<option value=""></option>
					<c:forEach items="${inTypes}" var="in">
						 <option value="${in.dictNum}"
						     <c:if test = "${shevel.type==in.dictNum }"> selected="selected"</c:if>>${in.dictValue}
						 </option>
					</c:forEach>
				</select>
			</div>
		</div>
		<div class="control-group">
			<label class="inline" for="confusionMethod" style="margin-left:35px;">
					混放策略：&emsp;
					<input type="radio" id = "confusionMethod0" name="confusionMethod" value="0" checked> 是
					<input type="radio" id = "confusionMethod1" name="confusionMethod" value="1"> 否
				</label>
				<label class="inline" for="locked" style="margin-left:35px;">
					出入库锁：&emsp;
					<input type="radio" id = "locked0" name="locked" value="0" checked> 是
					<input type="radio" id = "locke1" name="locked" value="1"> 否
				</label>
		</div>
		<div class="control-group">
		<label class="inline" for="classify" style="margin-left:35px;">
					ABC分类：&emsp;
					<input type="radio" id = "classify0" name="classify" value="0" checked> A
					<input type="radio" id = "classify1" name="classify" value="1"> B
					<input type="radio" id = "classify2" name="classify" value="2"> C
				</label>
				<label class="inline" for="carryingCapacity" style="margin-left:10px;">
					承载量：&emsp;
					<input type="text" id = "carryingCapacity" name="carryingCapacity" value="${shevel.carryingCapacity}" style="width:263px;">
				</label>		
		</div>
		<div class="control-group">
			<label class="control-label" for="remark">备注：</label>
			<div class="controls">
				<textarea class="input-xlarge" name="remark" id="remark" placeholder="描述" style="width:435px;">${shevel.remark}</textarea>
			</div>
		</div>
	</form>
	<div class="field-button" style="text-align: center;border-top: 0px;margin: 15px auto;">
		<span class="savebtn btn btn-success" onclick="disableAddButton('ShevelForm','ShevelButton')">保存</span>
		<span class="btn btn-danger" onclick="closeWindow();">取消</span>
	</div>
</div>
<script type="text/javascript">
var name=null;
  var ip=null;
  var port=null;
  var interfaceAuthority = "";

    var preStatus=${shevel.confusionMethod==null?0:shevel.confusionMethod};
    var locked = ${shevel.locked==null?0:shevel.locked};
    var classify = ${shevel.classify==null?0:shevel.classify};

    $("#ShevelForm input[type=radio][name=confusionMethod][value="+preStatus+"]").attr("checked",true).trigger("click");
    $("#ShevelForm input[type=radio][name=locked][value="+locked+"]").attr("checked",true).trigger("click");
    $("#ShevelForm input[type=radio][name=classify][value="+classify+"]").attr("checked",true).trigger("click");

    $('#ShevelForm .mySelect2').select2();
  
  $('#ShevelForm #instorageTypeSelect').change(function(){
  		$('#ShevelForm #type').val($('#ShevelForm #instorageTypeSelect').val());
  	})
  
  //var selectProjectId = 90;   //工程/货架主键
  $.ajax({
      type: "POST",
      url: context_path + "/wmsbind/getInterfaceAuthority?time="+new Date().getTime(),
      dataType: 'json',
      success: function (data) {
          interfaceAuthority=data.interfaceAuthority;
      }
  });

  $.ajax({
      type: "POST",
      url: context_path + '/wmsbind/getIp',
      dataType: 'json',
      cache: false,
      success: function (data) {
          name = data.name;
          ip = data.ip;
          port = data.port;
      }
  });

  /* 获取中间件基本信息结束 */
  
	$('#ShevelForm').validate({
		rules:{
  			"shevelNo":{//货架编号
  				maxlength:50
  				<%--remote:"<%=path%></wmsshevle/isExitsShevelNoOrName?id="+$("#id").val()--%>
  			},
  			"shevelName":{//货架名称
  				required:true, 
  				maxlength:32,  
  				validChar:true,
  				remote:"<%=path%>/wmsshevle/isExitsShevelNoOrName?id="+$("#id").val()
  			},
  			"line_number":{//货架列数
				required:true,
  				maxlength:2,
  				range:[1,10]
			},
			"row_number":{//货架行数
				required:true,
  				maxlength:2,
  				range:[1,10]
			},
			"place_number":{//库位个数
				required:true,
  				//accept:"^[1-3]$",
  				maxlength:1
			}
  		},
  		messages:{
  			"shevelNo":{
  				maxlength:"长度不能超过50个字符！",
  				remote:"您输入的货架编号已经存在，请重新输入！"
  			},
  			"shevelName":{
  				required:"请输入货架名称！", 
  				maxlength:"长度不能超过32个字符！",
  				remote:"您输入的货架名称已经存在，请重新输入！"
  			} ,
			"line_number":{//货架列数
				required:"请输入列数！",
  				maxlength:"不能超过两位数"
			},
			"row_number":{//货架行数
				required:"请输入行数！",
  				maxlength:"不能超过两位数"
			},
			"place_number":{//库位个数
				required:"请输入库位个数！",
  				maxlength:"不能超过一位数"
			}
  		},
	    errorClass: "help-inline",
	    errorElement: "span",
	    highlight:function(element, errorClass, validClass) {
		    $(element).parents('.control-group').addClass('error');
	    },
	    unhighlight: function(element, errorClass, validClass) {
		    $(element).parents('.control-group').removeClass('error');
	    }
  	});

	function setValueForElementById(elementId,value){
		$('#'+elementId).val(value);
	}
	
	
	function disableAddButton(formId,btnId) {
	    if($("#id").val()==""){
			save(formId,btnId);
		}else {
            layer.confirm('库位类型编辑后要重新贴库位的条码,确认更改?', /*显示的内容*/
                {
                    shift: 6,
                    moveType: 1, //拖拽风格，0是默认，1是传统拖动
                    title: "操作提示", /*弹出框标题*/
                    icon: 3, /*消息内容前面添加图标*/
                    btn: ['确定', '取消']/*可以有多个按钮*/
                }, function (index, layero) {
                    save(formId,btnId);
                }, function (index) {
                    //取消按钮的回调
                    layer.close(index);
                })
        }
	}
function cklinenum() {
    //判断商品价格
    var reg = /^\d{1,10}$/;
    if (!reg.test($("#ShevelForm #row_number").val())||!reg.test($("#ShevelForm #place_number").val())) {
        return false;
    } else {
        return true;
    }
}
	function save(formId,btnId){
        if ($("#"+formId).valid()) {
            if(cklinenum()==false){
                layer.msg("货架输入格式有误", {icon: 1, time: 1200});
                return;
			}else{
                if ($("#type").val() == "") {
                    layer.msg("请选择货架类型", {icon: 1, time: 1200});
                    return;
                }
                else {
                    $("#" + btnId).attr("disabled", "disabled");
                    $("#ShevelForm #interfaceAuthority").val(interfaceAuthority);
                    $("#ShevelForm #name").val(name);
                    $("#ShevelForm #ip").val(ip);
                    $("#ShevelForm #port").val(port);
                    var ss = $("#ShevelForm").serialize();
                    $.ajax({
                        type: "POST",
                        url: context_path + '/wmsshevle/toSaveShevel',
                        dataType: 'json',
                        data: {
                            ip: ip,
                            name: name,
                            port: port,
                            interfaceAuthority: interfaceAuthority,
                            pIdddd: $("#pIdddd").val(),
                            area_idddddd: $("#area_idddddd").val(),
                            type: $("#type").val()
                        },
                        data: ss,
                        success: function (data) {
                            if (data.result) {
                                layer.closeAll();
                                layer.msg(data.msg, {icon: 1, time: 1200});
                                //刷新左侧树
                                var node = zTree.getSelectedNodes()[0];
                                selectTreeId = node.id;
                                node.isParent = true;
                                zTree.reAsyncChildNodes(node, "refresh", false);
                                reloadGridDatas();
                            } else {
                                layer.msg(data.msg, {icon: 1, time: 1200});
                            }
                        }
                    })
                }
			}
        }
	}

	function closeWindow(){
		layer.close(openWindowIndex);
	}
</script>