<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
  <head>
    <title>仓库列表查询</title>
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
	<script type="text/javascript">
		var context_path = "<%=path%>";
	</script>
	<%@ include file="/techbloom/common/taglibs.jsp"%>
	<link rel="stylesheet" href="<%=path%>/plugins/ace/assets/css/select2.css" />
	<link rel="stylesheet" href="<%=path%>/plugins/ace//assets/css/font-awesome.css" />
	<style type="text/css">
	</style>
  </head>
  <body>
  	<div style="padding-left:30px;">
   	    <form id="query_area_list_queryForm">
			<ul class="form-elements">
				<!-- 库区编号 -->
				<li class="field-group">
					<label class="inline" for="query_area_list_warehouseNo" style="margin-right:20px;">
						仓库编号：
						<input type="text" name="warehouseNo" id="query_area_list_warehouseNo" value="${warehouseArea.warehouseNo }"
						style="width: 200px;" placeholder="仓库编号"/>
					</label>
				</li>
				<!-- 库区名称 -->
				<li class="field-group">
					<label class="inline" for="query_area_list_warehouseName" style="margin-right:20px;">
						仓库名称：
						<input type="text" name="warehouseName" id="query_area_list_warehouseName" value="${warehouseArea.warehouseName }"
						style="width: 200px;" placeholder="库区名称"/>
					</label>
				</li>
				
				<!-- 底部工具按钮 -->
				<li class="field-group">
					<div class="field-button">
						<div class="btn btn-info" onclick="queryOk();">
				            <i class="ace-icon fa fa-check bigger-110"></i>确定
			            </div> &nbsp; &nbsp;
						<div class="btn" onclick="ownerDialog.close();"><i class="icon-remove"></i>&nbsp;取消</div>
					</div>
				</li>
			</ul>
		</form>
  	</div>
  <script type="text/javascript" src="<%=path%>/plugins/public_components/js/select2.js"></script>
  <script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
  <script type="text/javascript">
  
		function queryOk(){
			var queryParam = iTsai.form.serialize($('#query_area_list_queryForm'));
			Dialog.openerWindow().queryAreaListByParam(queryParam);
	    	ownerDialog.close();
		}
		
  </script>
  </body>
</html>
