<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
  <head>
    <title>仓库列表</title>
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <style type="text/css">
	</style>
	<script type="text/javascript">
		var context_path = "<%=path%>";
		//后台返回之后调用的js方法
		function result(res){ 
			if(res!=null){
				//执行父窗口中的js方法： 
		  		Dialog.openerWindow().setValueForElementById('parentId',res.id);
		  		Dialog.openerWindow().setValueForElementById('parentNo',res.warehouseNo+" "+res.warehouseName); 
		  		//关闭当前窗口
		  	    ownerDialog.close();
			}else{
				Dialog.openerWindow().setValueForElementById('parentId',"");
		  		Dialog.openerWindow().setValueForElementById('parentNo',"");
		  		//关闭当前窗口
		  		ownerDialog.close();
			}
		}
	</script>
	<%@ include file="/techbloom/common/taglibs.jsp"%>
  </head>
  <body>
  	<!-- 库区信息树 -->
  	<form action="<%=path %>/area/getAreaParentId?id=javascript:selectedNode.id" id="AreaTypeForm" name="AreaTypeForm" method="post" target="_ifr">
	<div class="container">
		<div id="treeDiv">
		<script type="text/javascript">
			// 声明一个根节点
			var complexTree = new iTree({text:"仓库信息列表", icon:true});
			// 配置checkbox复选框上下级级联选择关系
			 complexTree.parentCheckedCascadeChild = true;
			//获取json数据
			var dataJson = ${treeJson};
			// 使用根节点对象的 loadData() 方法加载 JSON 数据
			complexTree.loadData(dataJson);
			//输出树目录
			complexTree.write();
			//展开所有节点   
			complexTree.expandAll(); 
			var selectedNode=complexTree.getSelected();
			//选择节点时，单选框也会被选中
			function setRadioChecked(radioId){
				  $('#itree-radio-'+radioId).attr('checked',true);
			}
		</script>
		</div>
		<div class="field-button">
			<button class="btn btn-info" type="submit">
	            <i class="ace-icon fa fa-check bigger-110"></i>确定
	           </button> &nbsp; &nbsp;
			<div class="btn" onclick="ownerDialog.close();"><i class="icon-remove"></i>&nbsp;取消</div>
	</div>
	</form>
  <iframe src="about:blank" name="_ifr" height="0" class="hidden"></iframe>
  <script type="text/javascript">
  function closeThisWindow(){
	  ownerDialog.close();
  }
  </script>
  </body>
</html>
