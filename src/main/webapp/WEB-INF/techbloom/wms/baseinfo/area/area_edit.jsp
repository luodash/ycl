<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<div id="area_edit_page" class="row-fluid" style="height: inherit;">
	<form id="AreaForm" class="form-horizontal" style="overflow: auto; height: calc(100% - 70px);">
		<input type="hidden" id="id" name="id" value="${warehouseArea.id}">
		<input type="hidden" id="parentId" name="parentId" value="${pId}">
		<div class="control-group">
			<label class="control-label" for="parentNo">库区编号：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="parentNo" name="parentNo" value="${warehouseArea.parentNo}" placeholder="所属仓库" readonly="readonly" >
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="warehouseNo"><c:if test="${param.parentId eq '0'}" >仓库</c:if><c:if test="${param.parentId ne '0'}" >库区</c:if>编号：<span class="field-required">*</span></label>
			<div class="controls">
				<div class="input-append span12 required">
				   <input type="text" class="span11" id="warehouseNo" name="warehouseNo" value="${warehouseArea.warehouseNo}" readonly placeholder="<c:if test="${param.parentId eq '0'}" >仓库</c:if><c:if test="${param.parentId ne '0'}" >库区</c:if>编号">
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="warehouseName"><c:if test="${param.parentId eq '0'}" >仓库</c:if><c:if test="${param.parentId ne '0'}" >库区</c:if>名称：<span class="field-required">*</span></label>
			<div class="controls">
				<input type="text" class="span11" id="warehouseName" name="warehouseName" value="${warehouseArea.warehouseName}" placeholder="<c:if test="${param.parentId eq '0'}" >仓库</c:if><c:if test="${param.parentId ne '0'}" >库区</c:if>名称">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="remark">备注：</label>
			<div class="controls">
				<textarea class="input-xlarge" name="remark" id="remark" style="width: 435px;" placeholder="描述">${warehouseArea.remark}</textarea>
			</div>
		</div>
	</form>
	<div class="field-button" style="text-align: center;border-top: 0px;margin: 15px auto;">
		<span class="btn btn-info" onclick="saveForm();">
		   <i class="ace-icon fa fa-check bigger-110"></i>保存
		</span>
		<span class="btn btn-danger" onclick="layer.closeAll();">
		   <i class="icon-remove"></i>&nbsp;取消
		</span>
	</div>
</div>
<script type="text/javascript">
$("#AreaForm").validate({
		rules:{
  			"warehouseNo":{//供应商编号
  				maxlength:50,
  				remote:"<%=path%>/area/isExitsAreaNoOrName?id="+$("#id").val()+"&parentId="+$("#parentId").val()
  			},
  			"warehouseName":{//供应商名称
  				required:true,
  				maxlength:32,
  				validChar:true,
  				remote:"<%=path%>/area/isExitsAreaNoOrName?id="+$("#id").val()+"&parentId="+$("#parentId").val()
  			} 
  		},
  		messages:{
  			"warehouseNo":{
  				maxlength:"长度不能超过50个字符！",
  				remote:"您输入的库区编号已经存在，请重新输入！"
  			},
  			"warehouseName":{
  				required:"请输入库区名称！", 
  				maxlength:"长度不能超过32个字符！",
  				remote:"您输入的库区名称已经存在，请重新输入！"
  			} 
  		},
	    errorClass: "help-inline",
	    errorElement: "span",
	    highlight:function(element, errorClass, validClass) {
		    $(element).parents('.control-group').addClass('error');
	    },
	    unhighlight: function(element, errorClass, validClass) {
		    $(element).parents('.control-group').removeClass('error');
	    }
  	});
  	/*
 * 显示所属库区界面
 */
function onSelectAreaWindow(){
	Dialog.open({
	    title: "仓库信息", 
	    width: 500,  height: "500",
	    url: context_path + "/area/getAreaTypeList?aid="+$("#id").val(),
	    theme : "simple",
		drag : true,
		clickMaskToClose: false
	});
}
//为指定id的元素赋值
	function setValueForElementById(elementId,value){
		$("#"+elementId).val(value);
	}
	function saveForm() {
        if ($("#AreaForm").valid()) {
            saveAreaInfo($("#AreaForm").serialize());
        }
    }
  //保存/修改用户信息
    function saveAreaInfo(bean) {
        $.ajax({
                url: context_path + "/area/toSaveArea.do",
                type: "POST",
                data: bean,
                dataType: "JSON",
                success: function (data) {
                    if (Boolean(data.result)) {
                        layer.msg(data.msg,{icon:1});
                        layer.close($queryWindow);
                        gridReload();
                    } else {
                        layer.alert(data.msg, {icon: 2});
                    }
                },
                error:function(XMLHttpRequest){
            		alert(XMLHttpRequest.readyState);
            		alert("出错啦！！！");
            	}
            });
    }
</script>