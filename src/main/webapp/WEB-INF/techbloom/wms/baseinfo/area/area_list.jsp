<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

    <style type="text/css">
        #area_list_container {
            width: 100%;
            height: 100%;
            position: absolute;
        }

        #area_list_areaTreeDiv {
            width: 200px;
            height: 100%;
            border-right: solid 1px #cdcfd0;
            float: left;
            padding-left: 5px;
            padding-top: 10px;
            display: inline;
        }

        #area_list_grid-div {
            width: calc(100% - 220px);
            height: 100%;
            float: left;
            padding: 5px 0 5px 5px;
        }
    </style>
<div id="container">
    <div id="area_list_areaTreeDiv" class="ztree" style="overflow-y:auto;">></div>
    <div id="area_list_grid-div">
        <!-- 隐藏区域：存放查询条件 -->
        <form id="area_list_hiddenQueryForm" style="display:none;">
            <!-- 仓库编号 -->
            <input id="area_list_warehouseNo" name="warehouseNo" value="" />
            <!-- 仓库名称-->
            <input id="area_list_warehouseName" name="warehouseName" value="" />
        </form>
        <form id="area_list_hiddenForm" action="<%=path%>/area/toExcel" method="POST" style="display: none;">
		<!-- 选中的用户 -->
        <input id="area_list_ids" name="ids" value="" />
        </form>
         <div class="query_box" id="area_list_yy" title="查询选项">
            <form id="area_list_queryForm" style="max-width:100%;">
			 <ul class="form-elements">
				<li class="field-group field-fluid2">
					<label class="inline" for="area_list_warehouseNo" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:70px;">库区编号：</span>
						<input type="text" name="warehouseNo" id="area_list_warehouseNo" value="" style="width: calc(100% - 75px);" placeholder="库区编号">
					</label>			
				</li>
				<li class="field-group field-fluid2">
					<label class="inline" for="area_list_warehouseName" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:70px;">库区名称：</span>
						<input type="text" name="warehouseName" id="area_list_warehouseName" value="" style="width: calc(100% - 75px);" placeholder="库区名称">
					</label>					
				</li>				
			</ul>
			<div class="field-button">
					<div class="btn btn-info" onclick="queryOk();">
				        <i class="ace-icon fa fa-check bigger-110"></i>查询
			        </div>
					<div class="btn" onclick="reset();"><i class="ace-icon icon-remove"></i>重置</div>
		        </div>
		 </form>		 
    </div>
        <div id="area_list_fixed_tool_div" class="fixed_tool_div">
            <div id="area_list___toolbar__" style="float:left;overflow:hidden;"></div>
        </div>
        <!--    仓库信息表格 -->
        <table id="area_list_grid-table" style="width:100%;height:100%;"></table>
        <!-- 	表格分页栏 -->
        <div id="area_list_grid-pager"></div>
    </div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript" src="<%=path%>/static/js/techbloom/wms/baseinfo/area.js"></script>
<script type="text/javascript">
    var zTree;
    var currentNode={id:0,pId:-1};
    var context_path = '<%=path%>';
    var oriData;      //表格数据
    var _grid;        //表格对象
    $(document).ready(function() {       
       initTree();
    });
    function initTree() {
	  $.ajax({
	        type : "POST",
	   		url : context_path + "/area/allTreeData?parentId="+currentNode.id,
	   		dataType : 'json',
	   		cache : false,
	   		success : function(dataJson) {
	            console.dir(dataJson);
			    dataJson.push({
			       id : 0,
			       pId : -1,
			       text : "仓库信息",
			       open : true
	           	});
	            zTree = $.fn.zTree.init($("#area_list_areaTreeDiv"), setting,dataJson);
	            zTree.expandAll(true);					
			}
	     });
	}
   var setting = {
        data : {
                simpleData : {
                    enable : true,
                    idKey : "id",
                    pIdKey : "pId"
                },
                key : {
                    name : "text"
                }
            },
            callback : {
                onClick : zTreeOnClick
            },
        async: {
            enable: true,
            url:context_path+"/area/allTreeData",
            autoParam:["id"],
            otherParam:{"parentId":function (){
                return currentNode.id;}},
            type: "POST"
        }
	};
function zTreeOnClick(event, treeId, treeNode){
    currentNode.id=treeNode.id;
	currentNode.pId=treeNode.pId;
	reload_device(treeNode.id, treeNode.pId,"");
}
function reload_device(id,pId, queryJsonString) {
    $("#area_list_grid-table").jqGrid('setGridParam',
        {
            postData: {queryJsonString: "",areaId:id} //发送数据
        }
    ).trigger("reloadGrid");
} 
    //工具栏
    $("#area_list___toolbar__").iToolBar({
        id: "area_list___tb__01",
        items: [
            {label: "添加", disabled: (${sessionUser.addQx} == 1 ? false : true), onclick:addArea, iconClass:'icon-plus'},
            {label: "编辑", disabled: (${sessionUser.editQx} == 1 ? false : true),onclick:editArea, iconClass:'icon-pencil'},
            {label: "删除", disabled: (${sessionUser.deleteQx} == 1 ? false : true),onclick:delArea, iconClass:'icon-trash'},
            {label: "导出", disabled: (${sessionUser.queryQx}==1?false:true),onclick:function(){toExcel();},iconClass:' icon-share'},
	        {label: "打印", disabled: (${sessionUser.queryQx}==1?false:true),onclick:function(){toPrint();},iconClass:' icon-print'}, 
          ]
    });

    $(function () {
        //初始化表格
        _grid = jQuery("#area_list_grid-table").jqGrid({
            url: context_path + "/area/areaList",
            datatype: "json",
            colNames: ["仓库主键", "编号", "名称","仓储类别", "备注", "父区域ID"],
            colModel: [
                {name: "id", index: "id", width: 55, hidden: true},
                {name: "warehouseNo", index: "WAREHOUSE_NO", width: 60},
                {name: "warehouseName", index: "WAREHOUSE_NAME", width: 50},
                {name: "areaType",index : "areaType",width : 50,
					formatter:function(cellvalue, options, rowObject){
						if(cellvalue=="0"){
							return "仓库";
						}else if(cellvalue=="1"){
							return "库区";
						}
					}
				},
                {name: "remark", index: "REMARK", width: 60},
                {name: "parentId", index: "PARENT_ID", width: 60, hidden: true}
            ],
            rowNum: 20,
            rowList: [10, 20, 30],
            pager: "#area_list_grid-pager",
            sortname: "WAREHOUSE_NO",
            sortorder: "desc",
            altRows: true,
            viewrecords: true,
            autowidth: true,
            multiselect: true,
            multiboxonly: true,
            loadComplete: function (data) {
                var table = this;
                setTimeout(function () {
                    updatePagerIcons(table);
                    enableTooltips(table);
                }, 0);
                oriData = data;
            },
            emptyrecords: "没有相关记录",
            loadtext: "加载中...",
            pgtext: "页码 {0} / {1}页",
            recordtext: "显示 {0} - {1}共{2}条数据"
        });
        //在分页工具栏中添加按钮
        jQuery("#area_list_grid-table").navGrid("#area_list_grid-pager", {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false
        }).navButtonAdd("#area_list_grid-pager", {
              caption: "",
              buttonicon: "ace-icon fa fa-refresh green",
              onClickButton: function () {
                  $("#area_list_grid-table").jqGrid("setGridParam",
                          {
                              postData: {areaId: "", queryJsonString: ""} //发送数据
                          }
                  ).trigger("reloadGrid");
              }
          });

        $(window).on("resize.jqGrid", function () {
            $("#area_list_grid-table").jqGrid("setGridWidth", $("#area_list_grid-div").width() - 3);
            $("#area_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-
            $("#area_list_yy").outerHeight(true)-$("#area_list_fixed_tool_div").outerHeight(true)-
            $("#area_list_grid-pager").outerHeight(true)-$("#gview_area_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
        });

        $(window).triggerHandler("resize.jqGrid");
    });
    var _queryForm_data = iTsai.form.serialize($("#area_list_queryForm"));
	function queryOk(){
		var queryParam = iTsai.form.serialize($("#area_list_queryForm"));
		queryAreaListByParam(queryParam);	
	}
	function reset(){
		iTsai.form.deserialize($("#queryForm"),_queryForm_data); 
		queryAreaListByParam(_queryForm_data);	
	}
	function toExcel(){
	var selectid = getId("#area_list_grid-table","id");
	function getId(_grid,_key){
	    var idAddr = jQuery(_grid).getGridParam("selarrrow");
	    return idAddr;
	}
	$("#area_list_ids").val(selectid);
	$("#area_list_hiddenForm").submit();
}
function toPrint(){
	var url = context_path + "/area/toPrint";
	window.open(url);
}
/** 表格刷新 */
function gridReload() {
	var node=zTree.getNodeByParam("id",currentNode.id);
	node.isParent=true;
    zTree.reAsyncChildNodes(node, "refresh",false);
    _grid.trigger("reloadGrid");  //重新加载表格
}
function addArea() {
    if(currentNode.pId==0 || currentNode.id==0){
		$.post(context_path + "/area/addArea.do?parentId="+currentNode.id, {}, function (str){
			$queryWindow=layer.open({
				title : "库区添加",
				type:1,
				skin : "layui-layer-molv",
				area : "600px",
				shade : 0.6, //遮罩透明度
				moveType : 1, //拖拽风格，0是默认，1是传统拖动
				anim : 2,
				content : str,
				success: function (layero, index) {
					layer.closeAll('loading');
				}
			});
		});
    }else{
        layer.alert("库区下面不能再添加库区");
	}
}

/**
 * 修改客户
 */
function editArea() {
    var checkedNum = getGridCheckedNum("#area_list_grid-table", "id");
    if (checkedNum == 0) {
       layer.alert("请选择一个要编辑的库区！");
        return false;
    } else if (checkedNum > 1) {
    	layer.alert("只能选择一个库区进行编辑操作！");
        return false;
    } else {
        var id = jQuery("#grid-table").jqGrid('getGridParam', 'selrow');
        $.post(context_path + "/area/addArea.do?id="+id+"&parentId="+currentNode.id, {}, function (str){
    		$queryWindow=layer.open({
    		    title : "库区编辑", 
    	    	type:1,
    	    	skin : "layui-layer-molv",
    	    	area : "600px",
    	    	shade : 0.6, //遮罩透明度
    		    moveType : 1, //拖拽风格，0是默认，1是传统拖动
    		    anim : 2,
    		    content : str,
    		    success: function (layero, index) {
                    layer.closeAll('loading');
                }
    		});
    	});
    }
}
/**
 *删除库区
 */
function delArea() {
    var checkedNum = getGridCheckedNum("#area_list_grid-table", "id");  //选中的数量
    if (checkedNum == 0) {
        layer.alert("请选择一个要删除的库区！");
    } else {
        //从数据库中删除选中的物料，并刷新物料表格
        var ids = jQuery("#area_list_grid-table").jqGrid('getGridParam', 'selarrrow');
        //弹出确认窗口
        layer.confirm('确定删除选中的库区？',
            function () {
                $.ajax({
                    type: "POST",
                    url: context_path + "/area/deleteArea.do?ids=" + ids,
                    dataType: "json",
                    success: function (result) {
                        if (result.result) {
                            //弹出提示信息
                            showTipMsg("库区删除成功！", 1200);
                            gridReload();
                        } else {
                            layer.msg(result.msg);
                        }
                        gridReload();//重新加载表格
                    }
                });
            }
        );
    }
}
</script>


