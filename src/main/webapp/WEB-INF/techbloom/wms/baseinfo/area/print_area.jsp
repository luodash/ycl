﻿<%@page import="java.math.BigDecimal"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<%--<%@ include file="/techbloom/common/taglibs.jsp"%>--%>
	<%@ include file="../../../common/taglibs.jsp"%>
<script type="text/javascript">
		function printA4(){
			window.print();
		}
	</script>
</head>
<body onload="printA4();">
	<c:forEach items="${areaList }" var="dd" varStatus="ddlist" step="10">
		<div style="height:100%">
		<br />
		<br />
		<h3 align="center" style="font-weight: bold;">仓库信息</h3>
		<!--table的最大高度为height:21cm;  -->
		<%-- <table style="width: 16.2cm;font-size: 12px;" align="center">
			<tr>
		  	    <td>模板名称：${template.teamplateName }</td>
		  	    <td>添加时间：${fn:substring(template.addTime, 0, 19)}</td>
		  	    <td>模板类型：${template.type }</td>
		  	    <td>添加用户：${template.userName }</td>
		  	</tr>
			
	  	</table> --%>
		<table style="border-collapse: collapse; border: none;width: 16.2cm;font-size: 12px;" align="center">
			<tr>
				<td style="border: solid #000 1px;text-align:center;width:3cm;">编号</td>
				<td style="border: solid #000 1px;text-align:center;width:8.2cm;">名称</td>
				<td style="border: solid #000 1px;text-align:center;width:2cm;">类别</td>
				<td style="border: solid #000 1px;text-align:center;width:3cm;">备注</td>
			</tr>
			<c:forEach items="${areaList }" var="d" varStatus="dlist" begin="${ddlist.index}" end="${ddlist.index+9}" step="1">
			
				<tr>
					<td style="border: solid #000 1px;text-align:center;">${d.warehouseNo}</td>
					<td style="border: solid #000 1px;text-align:center;">${d.warehouseName}</td>
					<td style="border: solid #000 1px;text-align:center;">${d.areaType}</td>
					<td style="border: solid #000 1px;text-align:center;">${d.remark}</td>
				</tr>
			</c:forEach>
			
		</table>
		
		<br/>
		<br/>
		</div>
	</c:forEach>
</body>
</html>

