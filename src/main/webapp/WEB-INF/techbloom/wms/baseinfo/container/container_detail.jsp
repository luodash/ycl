<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<style type="text/css">
    body {
        backgound-color: #FFFFFF;
        overflow: hidden;
    }
</style>
<div class="row-fluid" id="container_detail_grid-div1" style="width:99%;">
    <div class="row-fluid" id="container_detail_table_toolbar" style="padding:5px 3px;" >
        <button class="btn btn-primary btn-addQx" onclick="openAddPages();">
			添加物料<i class="fa fa-plus" aria-hidden="true" style="margin-left:5px;"></i>
		</button>
		<button class=" btn btn-primary btn-deleteQx" onclick="deleteDetail();">
			删除物料<i class="fa fa-trash" aria-hidden="true" style="margin-left:5px;"></i>
		</button>
    </div>
    <div class="row-fluid" style="padding:0 3px;">
        <table id="container_detail_grid-table1" style="width:100%;"></table>
        <div id="container_detail_grid-pager1"></div>
    </div>
</div>
<script type="text/javascript">
var oriData; 
var _grid;
var openwindowtype = 0; //打开窗口类型：0新增，1修改
_grid = jQuery("#container_detail_grid-table1").jqGrid({
	url : context_path + "/container/detailList?cId="+cId,
	datatype : "json",
	styleUI: 'Bootstrap',
	colNames : [ "主键","物料", "数量"],
	colModel : [ 
         {name : "id",index : "id",hidden:true}, 
         {name : "materialName",index : "materialName",width : 100}, 
         {name : "amount",index : "amount",width : 100,editable : true,editrules: {custom: true, custom_func: numberRegex}}
	],
    rowNum : 20,
    rowList : [ 10, 20, 30 ],
    pager : "#container_detail_grid-pager1",
    sortname : "id",
    sortorder : "desc",
    altRows: true,
    viewrecords : true,
    hidegrid:false,
    multiselect:true,
    autowidth:true, 
    loadComplete : function(data) {
    	var table = this;
    	setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
    	oriData = data;
    },
    cellurl : context_path + "/container/saveAmount",
    cellEdit: true,
    width: 1000,
    height: 400,
    emptyrecords: "没有相关记录",
    loadtext: "加载中...",
    pgtext : "页码 {0} / {1}页",
    recordtext: "显示 {0} - {1}共{2}条数据"
});
$(window).on("resize.jqGrid", function () {
  		$("#container_detail_grid-table1").jqGrid("setGridWidth", $("#container_detail_grid-div1").width());
  		var height = $(".layui-layer-title",_grid.parents(".layui-layer")).height()+
                     $("#container_detail_materialDiv").outerHeight(true)+
                     $("#container_detail_grid-pager1").outerHeight(true)+$("#container_detail_table_toolbar").outerHeight(true)+
                     $("#gview_container_detail_grid-table1 .ui-jqgrid-hdiv").outerHeight(true);
                     $("#container_detail_grid-table1").jqGrid("setGridHeight",_grid.parents(".layui-layer").height()-height);
  	});
jQuery("#container_detail_grid-table1").navGrid("#container_detail_grid-pager1",{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#container_detail_grid-page1",{  
	caption:"",   
	buttonicon:"fa fa-refresh green",   
	onClickButton: function(){
		$("#container_detail_grid-table1").jqGrid("setGridParam", 
				{
					postData: {materialId:getSelectNodeId(),queryJsonString:""} //发送数据 
				}
		).trigger("reloadGrid");
	}
});
$(window).triggerHandler("resize.jqGrid");
function openAddPages() {
    openwindowtype = 0;
    layer.load(2);
    $.post(context_path + "/container/toDetailEdit", {}, function (str) {
        $queryWindow = layer.open({
            title: "添加物料",
            type: 1,
            skin: "layui-layer-molv",
            area: "500px",
            shade: 0.6, //遮罩透明度
            moveType: 1, //拖拽风格，0是默认，1是传统拖动
            content: str,//注意，如果str是object，那么需要字符拼接。
            success: function (layero, index) {
                layer.closeAll("loading");
            }
        });
    }).error(function () {
        layer.msg("加载失败,请检查网络！", {icon: 2});
        layer.closeAll("loading");
    });
}
function deleteDetail(){
	var checkedNum = getGridCheckedNum("#container_detail_grid-table1", "id");  //选中的数量
    if (checkedNum == 0) {
    	layer.alert("请选择一个要删除的物料！");
    } else {
        var ids = jQuery("#container_detail_grid-table1").jqGrid("getGridParam", "selarrrow");
        layer.confirm("确定删除选中的物料？", function() {
    		$.ajax({
    			type : "POST",
    			url : context_path + "/container/deleteDetail.do?ids="+ids+"&currentId="+currentNode.id ,
    			dataType : "json",
    			cache : false,
    			success : function(data) {
    				if (Boolean(data.result)) {
    					layer.msg(data.msg, {icon: 1,time:1000});
    				}else{
    					layer.msg(data.msg, {icon: 7,time:1000});
    					
    				}
    				_grid.trigger("reloadGrid");  //重新加载表格
    			}
    		});
    	});
    } 
}
function getId(_grid,_key){
    var idAddr = jQuery(_grid).getGridParam("selarrrow");
    var ids="";
    ids=idAddr.join(",");
    return ids;
}
function queryRow(){
	var queryParam = iTsai.form.serialize($("#container_detail_query_form"));
	var queryJsonString = JSON.stringify(queryParam);
	$("#container_detail_grid-table1").jqGrid("setGridParam", {
	   postData: {queryJsonString: queryJsonString} //发送数据 
	}).trigger("reloadGrid");
}
function numberRegex(value, colname) {
    var regex = /^[1-9]\d*$/;
    if (!regex.test(value)) {
        return [false, "请输入整数"];
    }
    else  return [true, ""];
}
</script>