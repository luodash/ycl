<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<div id="type_edit_page" class="row-fluid" style="height: inherit;">
	<form id="typeForm" class="form-horizontal" style="overflow: auto; height: calc(100% - 70px);">
		<input type="hidden" id="id" name="id" value="${type.id}">
		<div class="control-group">
			<label class="control-label" for="typeName">容器类型名称：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="typeName" name="typeName" value="${type.typeName }" placeholder="容器类型名称">
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="typeNo">容器类型编号：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<c:choose>
						<c:when test="${edit==1}">
							<input type="text" class="span11" id="typeNo" name="typeNo" value="${type.typeNo }" placeholder="容器类型编号" readonly="readonly" title="代码不可修改">
						</c:when>
						<c:otherwise>
							<input type="text" class="span11" id="typeNo" name="typeNo" placeholder="后台自动生成" readonly="readonly">
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="description">容器描述：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="description" name="description" value="${type.description }" placeholder="容器描述">
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="weight">重量(g)：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="weight" name="weight" value="${type.weight }" placeholder="重量">
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="containerType">容器类型：</label>
			<div class="controls">
				<select id="containerType" name="containerType" data-placeholder="请选择供容器类型" style="width:435px;">
					<c:forEach items="${CONTAINERTYPE}" var="tp">
						<option value="${tp.dictNum}" <c:if test="${type.containerType==tp.dictNum }">selected="selected"</c:if>>
						${tp.dictValue}</option>
					</c:forEach>
				</select>
			</div>
		</div>		
		<div class="control-group">
			<label class="control-label" for="application">用途：</label>
			<div class="controls">
				<select id="application" name="application" data-placeholder="请选择用途" style="width:435px;">
					<c:forEach items="${APPLICATION}" var="tp">
						<option value="${tp.dictNum}" <c:if test="${type.application==tp.dictNum }">selected="selected"</c:if>>
						${tp.dictValue}</option>
					</c:forEach>
				</select>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="remark">备注：</label>
			<div class="controls">
				<input type="text" class="span11" class="span11"  name="remark" id="remark" placeholder="备注" value="${type.remark}" >
			</div>
		</div>
	</form>
	<div class="field-button" style="text-align: center;border-top: 0px;margin: 15px auto;">
		<span class="btn btn-info" onclick="saveForm();">
		   <i class="ace-icon fa fa-check bigger-110"></i>保存
		</span>
		<span class="btn btn-danger" onclick="layer.closeAll();">
		   <i class="icon-remove"></i>&nbsp;取消
		</span>
	</div>
</div>
<script type="text/javascript">
var treeNode={id:null,pId:null};
    $("#typeForm").validate({
	rules:{
		"typeName":{
				required:true, 
				maxlength:32,
				remote:"<%=path%>/container/existsTypeName?typeName="+$("#typeName").val()+"&id="+$("#id").val()
			},
		"weight":{
		        required:true,
		        number:true
		    },
		 "description":{
		        required:true
		    }
		},
		messages:{
			"typeName":{
				required:"请输入容器名称！", 
				maxlength:"长度不能超过32个字符！",
				remote:"已有相同的容器名称存在！"
			},
			"weight":{
			    required:"请输入重量！",
			    number:"只能输入数字！"
			},
			"description":{
			    required:"请输入容器描述！"
			} 
		},
	errorClass: "help-inline",
	errorElement: "span",
	highlight:function(element, errorClass, validClass) {
		$(element).parents('.control-group').addClass('error');
	},
	unhighlight: function(element, errorClass, validClass) {
		$(element).parents('.control-group').removeClass('error');
	}
	});

    //确定按钮点击事件
    function saveForm() {
        if ($("#typeForm").valid()) {
            saveTypeInfo($("#typeForm").serialize());
        }
    }


    //保存/修改用户信息
    function saveTypeInfo(bean) {
    	$.ajax({
            url: context_path + "/container/saveType.do",
            type: "POST",
            data: bean,
            dataType: "JSON",
            success: function (data) {
                if (Boolean(data.result)) {
                    layer.msg("保存成功！", {icon: 1});
                    //刷新列表
                    $("#grid-table").jqGrid("setGridParam",
                        {
                            postData: {queryJsonString: ""} //发送数据
                        }
                    ).trigger("reloadGrid");
                    refreshTree();
                    //关闭当前窗口
                    layer.close($queryWindow);
                } else {
                    layer.alert("保存失败，请稍后重试！", {icon: 2});
                }
            }
        });
    }
    $("#containerType").select2({
    minimumInputLength:0,
    allowClear:true,
    delay:250,
    width:438,
    formatNoMatches:"没有结果",
	formatSearching:"搜索中...",
	formatAjaxError:"加载出错啦！"
    });
    $("#containerType").on("change.select2",function(){
       $("#containerType").trigger("keyup")}
    );
    $("#application").select2({
    minimumInputLength:0,
    allowClear:true,
    delay:250,
    width:438,
    formatNoMatches:"没有结果",
	formatSearching:"搜索中...",
	formatAjaxError:"加载出错啦！"
    });
    $("#application").on("change.select2",function(){
       $("#application").trigger("keyup")}
    );
</script>