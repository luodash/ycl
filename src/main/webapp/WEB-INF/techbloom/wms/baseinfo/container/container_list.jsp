<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ path + "/";
%>
<script type="text/javascript">
var context_path = '<%=path%>';
</script>
<script type="text/javascript" src="<%=path%>/static/js/techbloom/wms/baseinfo/supplier2.js"></script>
<style type="text/css">
#container_list_container {
	width: 100%;
	height: 100%;
	position: absolute;
}
#container_list_containerTreeDiv {
	width: 200px;
	height: 100%;
	border-right: solid 1px #cdcfd0;
	float: left;
	padding-left: 5px;
	padding-top: 10px;
	display: inline;
}
#container_list_grid-div {
	width: calc(100% - 220px);
	height: 100%;
	float: left;
	padding: 5px 0 5px 5px;
}
</style>
<div id="container_list_container">
    <div id="container_list_containerTreeDiv" class="ztree" style="overflow-y: auto;"></div>
	<div id="container_list_grid-div">
	    <form id="container_list_hiddenForm" action="<%=path%>/container/toExcel.do" method="POST" style="display: none;">
            <input id="container_list_ids" name="ids" value="" />
            <input id="container_list_pId" name="pId" value="" />
        </form>
		<!-- 隐藏区域：存放查询条件 -->
		<form id="container_list_hiddenQueryForm" style="display: none;">
			<!-- 容器编号 -->
            <input id="container_list_containerNo" name="containerNo" value="" />
            <!-- 容器名称-->
            <input id="container_list_containerName" name="containerName" value="" />
		</form>
		<div class="query_box" id="container_list_yy" title="查询选项">
            <form id="container_list_queryForm" style="max-width:100%;">
			 <ul class="form-elements">
				<li class="field-group field-fluid2">
					<label class="inline" for="container_list_containerNo" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:65px;">容器编号：</span>
						<input type="text" name="containerNo" id="container_list_containerNo" value="" style="width: calc(100% - 70px);" placeholder="容器编号">
					</label>			
				</li>
				<li class="field-group field-fluid2">
					<label class="inline" for="container_list_containerName" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:65px;">容器名称：</span>
						<input type="text" name="containerName" id="container_list_containerName" value="" style="width: calc(100% - 70px);" placeholder="容器名称">
					</label>			
				</li>				
			</ul>
			<div class="field-button">
				<div class="btn btn-info" onclick="queryOk();">
		            <i class="ace-icon fa fa-check bigger-110"></i>查询
	            </div>
				<div class="btn" onclick="reset();"><i class="ace-icon icon-remove"></i>重置</div>
	        </div>
		  </form>		 
    </div>
		<div id="container_list_fixed_tool_div" class="fixed_tool_div">
			<div id="container_list___toolbar__" style="float: left; overflow: hidden;"></div>
		</div>
		<table id="container_list_grid-table" style="width: 100%; height: 100%;"></table>
		<div id="container_list_grid-pager"></div>
	</div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
var zTree = $.fn.zTree.getZTreeObj("container_list_containerTreeDiv");
var currentNode={id:0,pId:null};
var openwindowtype;
var _grid;
var dynamicDefalutValue="9e2dbcbc06594d4bad928568166afc8e";
$("#container_list___toolbar__").iToolBar({
    id: "container_list___tb__01",
    items: [
        {label: "添加", disabled: ( ${sessionUser.addQx} == 1 ? false : true), onclick:addContainer, iconClass:'glyphicon glyphicon-plus'},
        {label: "编辑", disabled: ( ${sessionUser.editQx} == 1 ? false : true),onclick: editContainer, iconClass:'glyphicon glyphicon-pencil'},
        {label: "删除", disabled: ( ${sessionUser.deleteQx} == 1 ? false : true),onclick: deleteContainer, iconClass:'glyphicon glyphicon-trash'},
        {label: "导出", disabled: ( ${sessionUser.queryQx}==1 ? false : true),onclick:function(){toExcel();},iconClass:'icon-share'}
   ]
});
var setting = {
       data : {
           simpleData : {
               enable : true,
               idKey : "id",
               pIdKey : "pId"
           },
           key : {
               name : "text"
           }
       },
       callback : {
           onClick : zTreeOnClick
       },
      async: {
           enable: true,
           url:context_path+"/container/allTreeData",
           autoParam:["id"],
           otherParam:{"pId":function (){return currentNode.id;}},
           type: "POST"
   }
};
function initTree() {
	$.ajax({
		type : "POST",
		url : context_path + "/container/allTreeData?pId="+currentNode.id,
		dataType : "json",
		cache : false,
		success : function(dataJson) {
			dataJson.push({
				id : 0,
				pId : -1,
				text : "基础分类",
				open : true
			});
                  zTree = $.fn.zTree.init($("#container_list_containerTreeDiv"), setting,dataJson);
                  zTree.expandAll(true);
		}
	});
}
function reload_device(id, pId, queryJsonString) {
	var colNames = "";
	var colModel = "";
	$.ajax({
			type : "post",
			url : context_path + "/container/getGroupHeaders",
			data : {
				"pId" : pId
			},
			success : function(data) {
			colNames = data.colNames;
			colModel = data.colModel;
			jQuery('#container_list_grid-table').GridDestroy();
			$("#container_list_grid-div").append('<div class="row-fluid" id="container_list_table_toolbar"></div>');
			$("#container_list_grid-div").append('<table id="container_list_grid-table" style="width:100%;height:100%;"></table>');
		    $("#container_list_grid-div").append('<div id="container_list_grid-pager"></div>');
		    if(pId==null){
				colModel[8]={name:'opertion',index:'opertion',width:90,
	        			formatter: function (cellValu, option, rowObject) {
	                        return "<div style='margin-bottom:5px' class='btn btn-xs btn-success' onclick='echoProcessReport(" + rowObject.id + ")'>详情</div>"
	        			}
	        		};
		    }
			_grid = jQuery("#container_list_grid-table").jqGrid({
					url : context_path+ "/container/list.do",
					datatype : "json",
					postData:{id:id,pId:pId,queryJsonString:queryJsonString},
					styleUI : "Bootstrap",
					colNames:colNames,
					colModel:colModel,
					rowNum : 20,
					rowList : [ 10, 20, 30 ],
					pager : "#container_list_grid-pager",
					sortname : "id",
					sortorder : "desc",
					altRows : true,
					viewrecords : true,
				  //caption : "容器列表",
					hidegrid : false,
					multiselect : true,
					autowidth : true,
					beforeRequest:function (){
						dynamicGetColumns(dynamicDefalutValue,"container_list_grid-table",$(window).width()-$("#sidebar").width() -7);
						//重新加载列属性
					},
					loadComplete : function(data) {
					var table = this;
					setTimeout(function() {
					updatePagerIcons(table);
					enableTooltips(table);
				}, 0);
					oriData = data;
			},
					emptyrecords : "没有相关记录",
					loadtext : "加载中...",
					pgtext : "页码 {0} / {1}页",
					recordtext : "显示 {0} - {1}共{1}条数据"
	});
	jQuery("#container_list_grid-table").navGrid("#container_list_grid-pager", {
			edit : false,
			add : false,
			del : false,
			search : false,
			refresh : false
	}).navButtonAdd("#container_list_grid-pager", {
		caption : "",
		buttonicon : "ace-icon fa fa-refresh green",
		onClickButton : function() {
	    $("#container_list_grid-table").jqGrid("setGridParam", {
		postData : {
			id : id,
			pId : pId,
			queryJsonString : queryJsonString
		}
		}).trigger("reloadGrid");
	}
}).navButtonAdd('#container_list_grid-pager',{
     caption: "",
     buttonicon:"fa  icon-cogs",
     onClickButton : function (){
         jQuery("#container_list_grid-table").jqGrid('columnChooser',{
             done: function(perm, cols){
                 dynamicColumns(cols,dynamicDefalutValue);
                 $("#container_list_grid-table").jqGrid( 'setGridWidth', $("#container_list_grid-div").width()-7);
             }
         });
     }
 });
$(window).on('resize.jqGrid',function() {		
	$("#container_list_grid-table").jqGrid('setGridWidth',$("#container_list_grid-div").width()-7);
	$("#container_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-
	$("#container_list_yy").outerHeight(true)-$("#container_list_fixed_tool_div").outerHeight(true)-
	$("#container_list_grid-pager").outerHeight(true)-$("#gview_container_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
});
$(window).triggerHandler('resize.jqGrid');
},dataType : "json"
});
}
function zTreeOnClick(event, treeId, treeNode) {
	currentNode.id=treeNode.id;
	currentNode.pId=treeNode.pId;
	reload_device(treeNode.id, treeNode.pId, "");
};
$(document).ready(
     function() {
        initTree();
        reload_device(0,null,"");
});
function addContainer(){
    if(currentNode.pId>0){
       layer.msg("容器下不可添加！",{icon:8});
	   return;
    }
	openwindowtype = 0;
    layer.load(2);
    if(currentNode.id==0){
    	$.post(context_path + "/container/toEditType.do", {}, function (str) {
        $queryWindow = layer.open({
            title: "容器添加",
            type: 1,
            skin: "layui-layer-molv",
            area: "600px",
            shade: 0.6, // 遮罩透明度
            moveType: 1, // 拖拽风格，0是默认，1是传统拖动
            content: str,// 注意，如果str是object，那么需要字符拼接。
            success: function (layero, index) {
                layer.closeAll('loading');
            }
        });
    }).error(function () {
        layer.msg("加载失败,请检查网络！", {icon: 2});
        layer.closeAll('loading');
    });
  }else{
    	$.post(context_path + '/container/toEditContainer.do?pId='+currentNode.id, {}, function (str) {
            $queryWindow = layer.open({
                title: "容器添加",
                type: 1,
                skin: "layui-layer-molv",
                area: "600px",
                shade: 0.6, // 遮罩透明度
                moveType: 1, // 拖拽风格，0是默认，1是传统拖动
                content: str,// 注意，如果str是object，那么需要字符拼接。
                success: function (layero, index) {
                    layer.closeAll('loading');
                }
            });
        }).error(function () {
            layer.msg("加载失败,请检查网络！", {icon: 2});
            layer.closeAll('loading');
        });
    }	  
}
function editContainer(){
	var selectAmount = getGridCheckedNum("#container_list_grid-table");
	if(selectAmount==0){
		layer.msg("请选择一条记录！",{icon:2});
		return;
	}else if(selectAmount>1){
		layer.msg("只能选择一条记录！",{icon:8});
		return;
	}
	openwindowtype = 1;
	layer.load(2);
	var id = jQuery("#container_list_grid-table").jqGrid('getGridParam', 'selrow');
	if(currentNode.id==0){
		$.post(context_path+'/container/toEditType.do?id='+id, {}, function(str){
			$queryWindow = layer.open({
				    title : "容器编辑", 
			    	type: 1,
			    	skin : "layui-layer-molv",
			    	area : "600px",
			    	shade: 0.6, // 遮罩透明度
		    	    moveType: 1, // 拖拽风格，0是默认，1是传统拖动
			    	content: str,// 注意，如果str是object，那么需要字符拼接。
			    	success:function(layero, index){
			    		layer.closeAll('loading');
			    	}
				});
			}).error(function() {
				layer.msg("加载失败,请检查网络！",{icon:2});
				layer.closeAll('loading');
			});
	}else{
		$.post(context_path+'/container/toEditContainer.do?id='+id+"&pId="+currentNode.id, {}, function(str){
			$queryWindow = layer.open({
				    title : "容器编辑", 
			    	type: 1,
			    	skin : "layui-layer-molv",
			    	area : "600px",
			    	shade: 0.6, // 遮罩透明度
		    	    moveType: 1, // 拖拽风格，0是默认，1是传统拖动
			    	content: str,// 注意，如果str是object，那么需要字符拼接。
			    	success:function(layero, index){
			    		layer.closeAll('loading');
			    	}
				});
			}).error(function() {
				layer.msg("加载失败,请检查网络！",{icon:2});
				layer.closeAll("loading");
			});
	}	  
}
function deleteContainer(){
	var checkedNum = getGridCheckedNum("#container_list_grid-table", "id");  //选中的数量
    if (checkedNum == 0) {
    	layer.alert("请选择一个要删除的容器！");
    	return;
    } 
    var ids = jQuery("#container_list_grid-table").jqGrid('getGridParam', 'selarrrow');
       layer.confirm("确定删除选中的容器？", function() {
   		$.ajax({
   			type : "POST",
   			url : context_path + '/container/deleteContainer.do?ids='+ids+'&currentId='+currentNode.id ,
   			dataType : 'json',
   			cache : false,
   			success : function(data) {
   				layer.closeAll();
   				if (Boolean(data.result)) {
   					layer.msg(data.msg, {icon: 1,time:1000});
   					refreshTree();
   				}else{
   					layer.msg(data.msg, {icon: 7,time:2000});  					
   				}
   				_grid.trigger("reloadGrid");  //重新加载表格
   			}
   		});
   	});
}
function queryOk(){
	var queryParam = iTsai.form.serialize($('#container_list_queryForm'));
	queryContainerListByParam(queryParam);
}
function queryContainerListByParam(jsonParam) {
    iTsai.form.deserialize($('#container_list_hiddenQueryForm'), jsonParam);
    var queryParam = iTsai.form.serialize($('#container_list_hiddenQueryForm'));
    var queryJsonString = JSON.stringify(queryParam); 
    $("#container_list_grid-table").jqGrid('setGridParam',
        {
            postData: {queryJsonString: queryJsonString,pId:currentNode.pId}
        }
    ).trigger("reloadGrid");
}
function reset(){
	$("#container_list_queryForm #container_list_containerNo").val("").trigger("change");
	$("#container_list_queryForm #container_list_containerName").val("").trigger("change");
	$("#container_list_grid-table").jqGrid('setGridParam', {
	       postData: {queryJsonString:"",pId:currentNode.pId} //发送数据
	    }).trigger("reloadGrid");
}
var cId="";
function echoProcessReport(id){
	cId=id;
	$.post(context_path+'/container/toContainerDetail', {}, function(str){
	$queryWindow = layer.open({
		    title : "详情列表", 
	    	type: 1,
	    	skin : "layui-layer-molv",
	    	area : "750px",
	    	shade: 0.6, // 遮罩透明度
    	    moveType: 1, // 拖拽风格，0是默认，1是传统拖动
	    	content: str,// 注意，如果str是object，那么需要字符拼接。
	    	success:function(layero, index){
	    		layer.closeAll('loading');
	    	}
		});
	}).error(function() {
		layer.closeAll();
   		layer.msg('加载失败！',{icon:2});
	});
}
function ztreeOnAsyncSuccess(event, treeId, treeNode){  
    var url = context_path+ '/container/allTreeData.do?pId=';  
    if(treeNode == undefined){  
        url += "0";  
    } else{  
        url += treeNode.id;  
    }  
    $.ajax({  
        type : "post",  
        url : url,  
        data : "",  
        dataType : "json",  
        async : true,  
        success : function(jsonData) {                
            if (jsonData != null) {       
                var data = jsonData.unitList;  
                if(data != null && data.length != 0){  
                    if(treeNode == undefined){  
                        treeObj.addNodes(null,data,true);// 如果是根节点，那么就在null后面加载数据  
                    }  
                    else{  
                        treeObj.addNodes(treeNode,data,true);//如果是加载子节点，那么就是父节点下面加载  
                    }  
                }  
                treeObj.expandNode(treeNode,true, false, false);// 将新获取的子节点展开  
            }  
        },  
        error : function() {  
            alert("请求错误！");  
        }  
    });  
      
};
function refreshTree() {
	var node=zTree.getNodeByParam("id",currentNode.id);
	node.isParent = true;
	zTree.reAsyncChildNodes(node, "refresh",false);
}
function toExcel(){
    var ids = jQuery("#container_list_grid-table").jqGrid("getGridParam", "selarrrow");
    $("#container_list_hiddenForm #container_list_ids").val(ids);
    $("#container_list_hiddenForm #container_list_pId").val(currentNode.pid);
    $("#container_list_hiddenForm").submit();	
}
</script>