<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<div id="container_edit_page" class="row-fluid" style="height: inherit;">
	<form id="containerForm" class="form-horizontal" style="overflow: auto; height: calc(100% - 70px);">
		<input type="hidden" id="id" name="id" value="${container.id}">
   	    <input type="hidden" id="pId" name="pId" value="${pId}">
		<div class="control-group">
			<label class="control-label" for="containerName">容器名称：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="containerName" name="containerName" value="${container.containerName }" placeholder="容器名称">
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="containerNo">容器编号：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<c:choose>
						<c:when test="${edit==1}">
							<input type="text" class="span11" id="containerNo" name="containerNo" value="${container.containerNo }" placeholder="容器编号" readonly="readonly" title="代码不可修改">
						</c:when>
						<c:otherwise>
							<input type="text" class="span11" id="containerNo" name="containerNo" placeholder="后台自动生成" readonly="readonly">
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="rfid">rfid编号：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="rfid" name="rfid" value="${container.rfid }" placeholder="rfid编号">
				</div>
			</div>
		</div>
	</form>
	<div class="field-button" style="text-align: center;border-top: 0px;margin: 15px auto;">
		<span class="btn btn-info" onclick="saveForm();">
		   <i class="ace-icon fa fa-check bigger-110"></i>保存
		</span>
		<span class="btn btn-danger" onclick="layer.closeAll();">
		   <i class="icon-remove"></i>&nbsp;取消
		</span>
	</div>
</div>
<script type="text/javascript">
var parentTree=$.fn.zTree.getZTreeObj("containerTreeDiv");
var selectNode={id:0,pId:'${pId}'};

$("#containerForm").validate({
	rules:{
		"containerName":{//供应商名称
				required:true, 
				maxlength:32,
				remote:"<%=path%>/container/existsContainerName?containerName="+$("#containerName").val()+"&id="+$("#id").val()
			},
		"rfid":{
				required:true, 
				maxlength:32,
			}
		},
		
		messages:{
			"containerName":{
				required:"请输入容器名称！", 
				maxlength:"长度不能超过32个字符！",
				remote:"已有相同的容器名称存在！"
			},
			"rfid":{
				required:"请输入RFID！", 
				maxlength:"长度不能超过32个字符！",
				remote:"已有相同的RFID存在！"
			}
		},
	errorClass: "help-inline",
	errorElement: "span",
	highlight:function(element, errorClass, validClass) {
		$(element).parents('.control-group').addClass('error');
	},
	unhighlight: function(element, errorClass, validClass) {
		$(element).parents('.control-group').removeClass('error');
	}
	});
function saveForm(){
	if($('#containerForm').valid()){
		saveContainerInfo($("#containerForm").serialize());
	}
}
function saveContainerInfo(bean){
	$.ajax({
        url: context_path + "/container/saveContainer",
        type: "POST",
        data: bean,
        dataType: "JSON",
        success: function (data) {
            if (Boolean(data.result)) {
                layer.msg("保存成功！", {icon: 1});
                $("#grid-table").jqGrid('setGridParam',
                     {
                         postData: {queryJsonString: ""} //发送数据
                     }).trigger("reloadGrid");
                refreshTree();
                layer.close($queryWindow);
            } else {
            	layer.msg(data.msg);
                //layer.alert("保存失败，请稍后重试！", {icon: 2});
            }
        },
        error:function(XMLHttpRequest){
    		alert("出错啦！！！");
    	}
    });
}
</script>