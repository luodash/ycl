<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<div id="detail_edit_page" class="row-fluid" style="height: inherit;">
	<form id="detailForm" class="form-horizontal" style="overflow: auto; height: calc(100% - 70px);">
		<input type="hidden" id="cId" name="cId" value="">
		<div class="control-group">
			<label class="control-label" for="materialId">选择物料：</label>
			<div class="controls">
			    <div class="required span12" style=" float: none !important;">
				    <input id="materialId" name="materialId" type="text" style="width:347px;"/>
			    </div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="amount">数量：</label>
			<div class="controls">
			    <div class="input-append span12 required">
				   <input id="amount" name="amount" class="span11" type="text"/>
			    </div>
			</div>
		</div>
	</form>
	<div class="field-button" style="text-align: center;border-top: 0px;margin: 10px auto;">
		<span class="btn btn-info" onclick="saveForm();">
		   <i class="ace-icon fa fa-check bigger-110"></i>保存
		</span>
		<span class="btn btn-danger" onclick="layer.close($queryWindow);return false;">
		   <i class="icon-remove"></i>&nbsp;取消
		</span>
	</div>
</div>
<script type="text/javascript">
  var selectid = getId("#grid-table","id");
  function getId(_grid,_key){
      var idAddr = jQuery(_grid).getGridParam("selarrrow");
      var ids="";
      ids=idAddr.join(",");
      return ids;
  }
  //主键
  if(openwindowtype == 1){
  	$("#detailId").val(selectid);
  }

  $("#detailForm").validate({
    ignore:"",
  	rules:{
  	    "materialId":{
  	        required:true
  	    }, 
  		"amount":{
  			required:true,
  			regExp:/^[1-9]\d*$/
  		}
  	},
  	messages:{
  	    "materialId":{
  	        required:"请选择物料！"
  	    },
  		"amount":{
  			required:"请输入数量！",
  			regExp:"只能输入正整数" 
  		}
  	},
  	errorClass: "help-inline",
  	errorElement: "span",
  	highlight:function(element, errorClass, validClass) {
  		$(element).parents(".control-group").addClass("error");
  	},
  	unhighlight: function(element, errorClass, validClass) {
  		$(element).parents(".control-group").removeClass("error");
  	}
   });
   
  //确定按钮点击事件
  function saveForm(){
  	if($("#detailForm").valid()){
  		saveMaterialInfo($("#detailForm").serialize());
  	}
  }
  //保存/修改用户信息
  function saveMaterialInfo(bean){
  	if(bean){
  		$.ajax({
  			url:context_path+"/container/saveDetail?cId="+cId,
  			type:"POST",
  			data:bean,
  			dataType:"json",
  			success:function(data){
  				if(Boolean(data.result)){
  					layer.msg("保存成功！",{icon:1});
  					//刷新列表
  					$("#grid-table1").jqGrid('setGridParam', 
  						{
  							postData: {queryJsonString:""} //发送数据 
  						}
  					).trigger("reloadGrid"); 
  					$("#grid-table").jqGrid("setGridParam", 
  						{
  							postData: {queryJsonString:""} //发送数据 
  						}
  					).trigger("reloadGrid"); 					
  					layer.close($queryWindow);
  				}else{
  					layer.alert(data.msg,{icon:2});
  				}
  			}
  		});
  	}else{
  		layer.msg("出错啦！",{icon:2});
  	}
  }
  //选择物料
      $("#materialId").select2({
      	placeholder: "选择物料",
  		minimumInputLength:0,   //至少输入n个字符，才去加载数据
  	    allowClear: true,  //是否允许用户清除文本信息
  		delay: 250,
  		formatNoMatches:"没有结果",
  		formatSearching:"搜索中...",
  		formatAjaxError:"加载出错啦！",
  		ajax : {
  			url: context_path+"/material/getMaterialSelectList",
  			type:"POST",
  			dataType :"json",
  			delay : 250,
  			data: function (term,pageNo) {     //在查询时向服务器端传输的数据
  	            term = $.trim(term);
                  return {
                  	queryString: term,    //联动查询的字符
                  	pageSize: 15,    //一次性加载的数据条数
                      pageNo:pageNo,    //页码
                      time:new Date()   //测试
                   }
  	        },
  	        results: function (data,pageNo) {
  	        		var res = data.result;
     	            if(res.length>0){   //如果没有查询到数据，将会返回空串
     	               var more = (pageNo*15)<data.total; //用来判断是否还有更多数据可以加载
     	               return {
     	                    results:res,more:more
     	                 };
     	            }else{
     	            	return {
     	            		results:{}
     	            	};
     	            }
  	        },
  			cache : true
  		}

      });
      $("#materialId").on("change.select2",function(){
          $("#materialId").trigger("keyup")}
      );
</script>