<%--
  Created by IntelliJ IDEA.
  User: duckhyj
  Date: 2017/12/18
  Time: 12:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path=request.getContextPath();
%>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="<%=path%>/static/css/tip.css"/>
    <%--<script src="https://code.jquery.com/jquery-2.2.4.js"></script>--%>
    <script src="<%=path%>/static/js/techbloom/wms/platform/tip.js"></script>
    <script src="<%=path%>/static/js/techbloom/wms/platform/datePlan.js"></script>
    <style>
        .box_o{
            fill: #fff;
        }
    </style>
</head>
<body style=" background: #fbfbfb;">
 <div class ="query_box" title = "查询选项" style="margin:0px;">
      <form id="scheduleForm">
			<label class="inline" for="date" style="margin-right:20px;">按日期查询：
				<input class="form-control date-picker" id="date" name="date" type="text" value="${day }" placeholder="选择日期" style="width:200px;"/> 
				<span class="input-group-addon"> <i class="fa fa-calendar bigger-110"></i></span>
			</label>
			<div class="btn btn-info" onclick="queryOk();">
			    <i class="ace-icon fa fa-check bigger-110"></i>查询
		    </div>
		</form>
</div>
<div id="box"></div>
</body>
<script>
    $(".date-picker").datetimepicker({format: "YYYY-MM-DD"});
    var datePlan = new DatePlan( document.getElementById("box") );
    datePlan.init({
        url:"<%=path%>/platform/showSchedule",
        data:{day:$("#date").val()},
        width: $(".container-fluid").width(),
        hoverBox: function(data){
            console.dir(data);
        },
        clickBox: function(data ){
            console.dir(data);
        }
    });  
    function queryOk(){
    datePlan.reloadPlan({
            data:{day:$("#date").val()}
        });   
    }
    $("#box").width($(".container-fluid").width());
    $(window).on("resize",function(){
    	 datePlan.reloadPlan({
    		 width:$(".container-fluid").width()
         });   
    });
    
</script>
</html>
