<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<script type="text/javascript">
	var context_path = "<%=path%>";
</script>
<div id="platform_order_edit_page" class="row-fluid" style="height: inherit;">
   	<form action="" class="form-horizontal" id="orderForm" name="materialForm" method="post" target="_ifr">
   	    <input type="hidden" id="id" name="id" value="${order.id}" />
   	    <input type="hidden" id="orderNo" name="orderNo" value="${order.orderNo}" />
		<input type="hidden" id="status" name="status" value="${order.status}" />
		<input type="hidden" id="purchaseId" name="purchaseId" value="${order.purchaseId }" />
		<input type="hidden" id="item" name="type" value="${item }" />
		<input type="hidden" id="tttt" name="tttt" value="${orderType }" />	
		<div class="row" style="margin:0;padding:0;">
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="platformName" >月台：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
				<div class="controls">
					<div class="input-append span12">
						<input type="text" class="span10" id="platformName" name="platformName" value="${order.platformName }" readonly="readonly" />				        
					</div>
				</div>
			</div>
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="status" >状态：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
				<div class="controls">
					<div class="input-append span12">
						<input type="text" class="span10" id="statusName" name="statusName" value="已审核" readonly="readonly" />				        
					</div>
				</div>
			</div>							
		</div>
		<div class="row" style="margin:0;padding:0;">
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="orderTypeName" >预约类型：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
				<div class="controls">
					<div class="input-append span12">
						<input type="text" class="span10" id="orderTypeName" name="orderTypeName" value="${order.orderTypeName }" readonly="readonly" />
					</div>
				</div>
			</div>
			<div id="fa" class="control-group span6" style="display: inline">
				<label class="control-label" for="purchaseName" >装车发运单：&nbsp;&nbsp;&nbsp;</label>
				<div class="controls">
					<div class="input-append span12">
						<input type="text" class="span10" id="purchaseName" name="purchaseName" value="${order.purchaseName }" readonly="readonly" />
					</div>
				</div>
			</div>
			<div id="shou" class="control-group span6" style="display: inline">
				<label class="control-label" for="purchaseName" >收货单：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
				<div class="controls">
					<div class="input-append span12">
						<input type="text" class="span10" id="purchaseName" name="purchaseName" value="${order.purchaseName }" readonly="readonly"  />
					</div>
				</div>
			</div>			
		</div>
		<div class="row" style="margin:0;padding:0;">
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="materialTypeId" >预约到场时间：</label>
				<div class="controls">
					<input class="form-control date-picker" id="startTime" name="startTime" type="text" value="${order.startTime }" placeholder="预约到场时间" style="width:203px;" readonly="readonly" /> 
				</div>
			</div>
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="supplyId" >计划离场时间：</label>
				<div class="controls">
				    <input class="form-control date-picker" id="endTime" name="endTime" type="text" value="${order.endTime }" placeholder="计划离场时间" style="width:203px;" readonly="readonly" /> 				    
				</div>
			</div>
		</div>		
		<div class="row" style="margin:0;padding:0;">
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="specification" >司机姓名：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
				<div class="controls">
					<div class="input-append span12" >
						<input  type="text" class="span10" name="driver" id="driver" placeholder="司机姓名" value="${order.driver}" readonly="readonly" />
					</div>
				</div>
			</div>
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="packageName" >车牌号：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
				<div class="controls">
					<div class="input-append span12" >
						<input type="text" class="span10" id="license" name="license" value="${order.license }" placeholder="车牌号" readonly="readonly" />
					</div>
				</div>
			</div>
		</div>
		<div class="row" style="margin:0;padding:0;">
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="brand" >车型：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
				<div class="controls">
					<div class="input-append span12" >
						<input type="text" class="span10"  name="carType" id="carType" placeholder="车型" value="${order.carType}" readonly="readonly" />
					</div>
				</div>
			</div>
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="weight" >备注：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
				<div class="controls">
					<div class="input-append span12" >
						<input type="text" class="span10" id="remark" name="remark" value="${order.remark }" placeholder="备注" readonly="readonly" />
					</div>
				</div>
			</div>
		</div>		
  </form>
  <div class="field-button" style="text-align: center;border-top: 0px;margin: 15px auto;">
		<span class="btn btn-info" onclick="saveForm();">
		   <i class="ace-icon fa fa-check bigger-110"></i>保存
		</span>
		<span class="btn btn-danger" onclick="layer.closeAll();">
		   <i class="icon-remove"></i>&nbsp;取消
		</span>
	</div>
</div>
<iframe src="about:blank" name="_ifr" height="0" class="hidden"></iframe>
<script type="text/javascript">	
function saveForm(){}
if($("#orderForm #tttt").val()=="fahuo"){
   $("#fa").show();
   $("#shou").hide();
}else if($("#orderForm #tttt").val()=="shouhuo"){
   $("#fa").hide();
   $("#shou").show();
}else{
   $("#fa").hide();
   $("#shou").hide();
}
$("#orderForm #orderType").change(function(){
	$("#orderForm #ffff").select2("val", "");
	$("#orderForm #ssss").select2("val", "");
	if($("#orderForm #orderType").val()=="WMS_PLATFORMORDER_FH"){
        $("#fa").show();
        $("#shou").hide();
    }else if($("#orderForm #orderType").val()=="WMS_PLATFORMORDER_SH"){
        $("#fa").hide();
        $("#shou").show();
    }else{
        $("#fa").hide();
        $("#shou").hide();
    }
});
</script>