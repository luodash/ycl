<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<div class="main-content">
	<div class="main-content-inner">
		<div class="page-content" style="padding:0px;">
			<div >
				<div class="">
					<!-- PAGE CONTENT BEGINS -->
					<div class="space-6"></div>
					<div >
						<div class="col-sm-10 col-sm-offset-1">
							<!-- #section:pages/invoice -->
							<div class="widget-box transparent">
								<div class="widget-header widget-header-large">
									<!-- 隐藏的发货单主键 -->
				   	        		<input type="hidden" id="delivery" name="delivery" value="${delivery.deliveryId }">
									<h3 class="widget-title grey lighter" style=' background: none; border-bottom: none; '>
										<i class="ace-icon fa fa-leaf green"></i>
										发货单
									</h3>

									<!-- #section:pages/invoice.info -->
									<div class="widget-toolbar no-border invoice-info">
										<span class="invoice-info-label">发货单编号:</span>
										<span class="red">${delivery.deliveryNo }</span>

										<br />
										<span class="invoice-info-label">发货日期:</span>
										<span class="blue">${fn:substring(delivery.deliverydate, 0, 19)}</span>
									</div>
									<!-- 打印按钮 -->
									<div class="widget-toolbar hidden-480">
										<a href="#" onclick="printDoc();">
											<i class="ace-icon fa fa-print"></i>
										</a>
									</div>

									<!-- /section:pages/invoice.info -->
								</div>

								<div class="widget-body">
									<div class="widget-main ">
										<div >
											<div class="col-sm-12">
												<div  style="margin-left:10px;">
													<div style="line-height: 1;height: 15px;" class="col-xs-11 label label-lg label-info arrowed-in arrowed-right">
														<b>表单信息</b>
													</div>
												</div>
												<div>
													<ul class="list-unstyled spaced">
														<li>
															<i class="ace-icon fa fa-caret-right blue"></i>
															发货单：
															<b class="black">${delivery.deliveryNo}
															</b>
														</li>
														<li>
															<i class="ace-icon fa fa-caret-right blue"></i>
															质检人：
															<b class="black">${delivery.customerName }</b>
														</li>
													</ul>
												</div>
											</div><!-- /.col -->
										</div><!-- /.row -->

										<div class="space"></div>
										<!-- 详情表格 -->
										<div id="grid-div-c">
										<!-- 入库单信息表格 -->
										<table id="grid-table-c"></table>
										<!-- 表格分页栏 -->
										<div id="grid-pager-c"></div>
									   </div>
										<div class="hr hr8 hr-double hr-dotted"></div>
										<div >
											<div class="col-sm-7 pull-left">附加信息 </div>
											<div class="col-sm-5 pull-right">
												<h4 class="pull-right">
													发货状态 :
													<c:if test="${delivery.status==0 }"><span class="red">未发货</span></c:if>
											        <c:if test="${delivery.status==1 }"><span class="green">已发货</span></c:if>
												</h4>
											</div>
										</div>

										<div class="space-6"></div>
										<div class="well">
											<span>请认真核实入库单信息，确保正确无误!</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->
<script type="text/javascript">
var context_path = '<%=path%>';
var oriData1;      //表格数据
var _grid_order;        //表格对象

// var instoreStatus = ${INSTORE.instoreStatus};   //入库单状态
// var intype = '${INSTORE.instorageType}';   //入库单类型
/* $(function()
{ */
	//初始化表格
  	_grid_order = jQuery("#grid-table-c").jqGrid({
         url : context_path + '/delivery/deliveryDetaillist?id='+$("#delivery").val(),
         datatype : "json",
         colNames : [ '主键','物料编号','物料名称','物料数量'],
         colModel : [
  					  {name : 'detailId',index : 'id',width : 55,hidden:true}, 
  					  {name : 'materialNo',index:'materialNo',width : 50}, 
                      {name : 'materialName',index:'materialName',width : 50}, 
                      {name : 'amount',index:'amount',width : 50},
                    ],
         rowNum : 20,
         rowList : [ 10, 20, 30 ],
         pager : '#grid-pager-c',
         sortname : 'ID',
         sortorder : "asc",
         altRows: true,
         viewrecords : true,
         caption : "物料列表",
         autowidth:true,
         multiselect:false,
		 multiboxonly: true,
		loadComplete : function(data)
         {
             var table = this;
             setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
             oriData1 = data;
         },
  	   emptyrecords: "没有相关记录",
  	   loadtext: "加载中...",
  	   pgtext : "页码 {0} / {1}页",
  	   recordtext: "显示 {0} - {1}共{2}条数据"
    });
    //在分页工具栏中添加按钮
    jQuery("#grid-table-c").navGrid('#grid-pager-c',{edit:false,add:false,del:false,search:false,refresh:false})
      .navButtonAdd('#grid-pager-c',{
  	   caption:"",
  	   buttonicon:"ace-icon fa fa-refresh green",
  	   onClickButton: function(){
  	    $("#grid-table-c").jqGrid('setGridParam',
				{
					postData: {qId:$('#id').val()} //发送数据  :选中的节点
				}
		  ).trigger("reloadGrid");
  	   }
  	});

  	$(window).on('resize.jqGrid', function () {
  		$("#grid-table-c").jqGrid( 'setGridWidth', $("#grid-div-c").width() - 3 );
  		$("#grid-table-c").jqGrid( 'setGridHeight', (document.documentElement.clientHeight - $("#grid-pager-c").height() - 380) );
  	});

  	$(window).triggerHandler('resize.jqGrid');
//});

//将数据格式化成两位小数：四舍五入
function formatterNumToFixed(value,options,rowObj){
	if(value!=null){
		var floatNum = parseFloat(value);
		return floatNum.toFixed(2);
	}else{
		return "0.00";
	}
}

//提交按钮点击事件
function updateInstoreStatus(){
	//弹出确认窗口
	layer.confirm( '确认提交？',
        function(){
		//确定按钮点击事件
			//后台修改表单状态为提交
		    $.ajax({
			    type:"POST",
			    url:context_path+"/instorage/setInstoreState",
			    data:{instoreID:$('#id').val()},
			    dataType:"json",
			    success:function(data){
			    	if(data.msg!=""){
						  layer.alert(data.msg);
						  return;
					 }
					 if(Boolean(data.result)){
						//修改成功
						//弹出提示，
						//Dialog.openerWindow().showTipMsg("单据提交成功",1200);
						layer.msg("单据提交成功!",{icon:1,time:1200});
						//刷新父窗口的表格数据
						gridReload();
						//关闭当前窗口
						layer.closeAll();
					 }
			   }
		    });
        },
        function(){
        	//取消按钮点击事件
        },
        { title:'操作提示',
            yesLabel: '确定',
            noLabel:'取消',
            follow: document.getElementById('confirmBtn')
        }
    );
}

function printDoc(){
	var url = context_path + "/instorage/printInstorageDetail?instoreID="+$("#id").val();
	window.open(url);
}
</script>
