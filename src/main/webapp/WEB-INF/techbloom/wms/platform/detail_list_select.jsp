﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<div id="grid-div-cc">
    <!-- 	表格搜索栏 -->
    <div class="inline" name="detail-search-params" style="margin-left:10px;margin-top:5px;vertical-align:middle;">
        <!-- 隐藏的发货单主键 -->
        <input type="hidden" id="deliveryId" name="deliveryId" value="${deliveryId}">
        <form class="inline" id="queryParamForm">
            <label class="form-group" for="MaterialNO" style="margin-right:20px;">
                物料编号：
                <input type="text" id="MaterialNO" name="MaterialNO"
                       style="width: 200px;" placeholder="物料编号">
            </label>
            <label class="form-group" for="MaterialName" style="margin-right:20px;">
                物料名称：
                <input type="text" id="MaterialName" name="MaterialName"
                       style="width: 200px;" placeholder="物料名称">
            </label>
        </form>
        <button class="btn btn-sm btn-primary" onclick="searchFunction();">
            <i class="icon-search" style="margin-right:4px;"></i><strong>搜索</strong>
        </button>

        <button class="btn btn-sm btn-primary" id="addDetailOKBtn" style="width:100px;">
            <i class="icon-ok" style="margin-right:4px;"></i><strong>确定</strong>
        </button>
    </div>

    <!--    物料信息表格 -->
    <table id="grid-table-cc" style="width:100%;height:100%;"></table>
    <!-- 	表格分页栏 -->
    <div id="grid-pager-cc"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriData2;      //表格数据
    var _grid_list;        //表格对象

    $(function () {
        //初始化表格
        _grid_list = jQuery("#grid-table-cc").jqGrid({
            url: context_path + '/delivery/materialList',
            datatype: "json",
            colNames: ['物料主键', '物料名称', '物料编号', '物料单位'],
            colModel: [
                {name: 'id', index: 'id', width: 55, hidden: true},
                {name: 'materialName', index: 'MATERIAL_NAME', width: 60, sortable: false},
                {name: 'materialNo', index: 'MATERIAL_NO', width: 60, sortable: false},
                {name: 'materialUnit', width: 40, sortable: false},
                
            ],
            rowNum: 20,
            rowList: [10, 20, 30],
            pager: '#grid-pager-cc',
            altRows: true,
            viewrecords: true,
            caption: "物料列表",
            autowidth: true,
            multiselect: true,
            multiboxonly: true,
            loadComplete: function (data) {
                var table = this;
                setTimeout(function () {
                    updatePagerIcons(table);
                    enableTooltips(table);
                }, 0);
                oriData2 = data;
            },
            emptyrecords: "没有相关记录",
            loadtext: "加载中...",
            pgtext: "页码 {0} / {1}页",
            recordtext: "显示 {0} - {1}共{2}条数据"
        });

        //在分页工具栏中添加按钮
        jQuery("#grid-table-cc").navGrid('#grid-pager-cc', {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false
        })
                .navButtonAdd('#grid-pager-cc', {
                    caption: "",
                    buttonicon: "ace-icon fa fa-refresh green",
                    onClickButton: function () {
                        _grid_list.trigger("reloadGrid");  //重新加载表格
                    }
                });
        //根据窗口相关参数调整表格的大小
        $(window).on('resize.jqGrid', function () {
            $("#grid-table-cc").jqGrid('setGridWidth', $("#grid-div-cc").width() - 3);
            $("#grid-table-cc").jqGrid('setGridHeight', (document.documentElement.clientHeight - $("#grid-pager-cc").height() - 130));
        });

        $(window).triggerHandler('resize.jqGrid');
    });

    //物料搜索按钮
    function searchFunction() {
        var queryParam = iTsai.form.serialize($('#queryParamForm'));
        var queryJsonString = JSON.stringify(queryParam);         //将json对象转换成json字符串
        $("#grid-table-cc").jqGrid('setGridParam',
                {
                    postData: {jsonString: queryJsonString} //发送数据
                }
        ).trigger("reloadGrid");
    }


    //确定添加按钮
    $('#addDetailOKBtn').click(function () {
        var ids = jQuery("#grid-table-cc").jqGrid('getGridParam', 'selarrrow');
        if (ids.length > 0) {
            $.ajax({
                type: "POST",
                url: context_path + '/delivery/addDetails',
                data: {deliveryId: $('#deliveryId').val(), detailIds: ids.join(",")},
                dataType: "json",
                success: function (data) {
                    if (data.result) {
                        showTipMsg("添加成功！", 1200);
                        //调用父窗口中的js方法，重新加载入库单详情表
                        reloadDetailTableList();
                        /* if (data.returnData != "") {
                            layer.alert("其中" + data.returnData + " 已添加！<br>如果需要修改个数，你可以直接点击表格进行修改");
                        } */
                        //操作成功,关闭当前子窗口
                        layer.close(chilDiv);
                    }else{
                     layer.alert(data.msg,{icon:2,time:1200});
                    }
                }
            });
        } else {
            layer.alert(data.msg,{icon:2,time:1200});
        } 
    });
</script>

