<%@ page language="java" import="java.lang.*"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
  <div class="row-fluid" style="height: inherit;margin:0px;border: 0px">
	<form id="platForm" class="form-horizontal" target="_ifr" style="margin-right:10px;">
		<input type="hidden" id="id" name="id" value="${platform.id}" />
		<input type="hidden" id="platformNo" name="platformNo" value="${platform.platformNo}" />
  	    <input type="hidden" id="status" name="status" value="${platform.status}" />
		<div class="row" style="margin:0;padding:0;">
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="platformName">月台名称：</label>
				<div class="controls">
					<div class="input-append span12 required">
						<input id="platformName" name="platformName" type="text" class="span11" value="${platform.platformName}" placeholder="月台名称" />
					</div>
				</div>
			</div>
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="operateTime">最大收货数量：</label>
				<div class="controls">
					<div class="input-append span12 required">
						<input id="dailyMax" name="dailyMax" type="text" class="span11" placeholder="该月台单天收货或发货的最大数量" value="${platform.dailyMax}" />
					</div>
				</div>
			</div>
		</div>		
		<div class="row" style="margin:0;padding:0;">
		   <label class="control-label" for="week">开&nbsp;放&nbsp;日&nbsp;：</label>
		   <div class="controls">
		       <input type="checkbox" name="week" value="1" <c:if test="${platform.sun==true }">checked="checked"</c:if> />周日
		       <input type="checkbox" name="week" value="2" <c:if test="${platform.mon==true }">checked="checked"</c:if> />周一
		       <input type="checkbox" name="week" value="3" <c:if test="${platform.tue==true }">checked="checked"</c:if> />周二
		       <input type="checkbox" name="week" value="4" <c:if test="${platform.wed==true }">checked="checked"</c:if> />周三
		       <input type="checkbox" name="week" value="5" <c:if test="${platform.thu==true }">checked="checked"</c:if> />周四
		       <input type="checkbox" name="week" value="6" <c:if test="${platform.fri==true }">checked="checked"</c:if> />周五
		       <input type="checkbox" name="week" value="7" <c:if test="${platform.sat==true }">checked="checked"</c:if> />周六
		   </div>
		</div>
		<div style="margin-left:10px;">
			<span class="btn btn-info" id="formSave" onclick="saveForm();">
				<i class="ace-icon fa fa-check bigger-110"></i>保存
			</span>
		</div>
	</form>
	<div id="grid-div-c" style="width:100%;margin:10px auto;">
        <div id="fixed_tool_div" class="fixed_tool_div detailToolBar">
             <div id="__toolbar__-c" style="float:left;overflow:hidden;"></div>
        </div>
		<table id="grid-table-c" style="width:100%;height:100%;"></table>
		<div id="grid-pager-c"></div>
	</div>
</div>
<script type="text/javascript">
var pId=$("#platForm #id").val();
var context_path = "<%=path%>";
var oriDataDetail;
var _grid_detail;
$("#platForm").validate({
   rules:{
      "platformName":{
         required:true,
         remote:"<%=path%>/platform/existsName?id="+$("#id").val()
      },
      "everyMax":{
         required:true,
         digits:true
      },
      "dailyMax":{
         required:true,
         digits:true
      }
   },
   messages: {
     "platformName":{
         required:"请输入月台名称！",
         remote:"已存在相同的月台名称！"
      },
      "everyMax":{
         required:"请输入单次最大数量！",
         digits:"只能输入正整数！"
      },
      "dailyMax":{
         required:"请输入单日最大数量！",
         digits:"只能输入正整数！"
      }
   },
   errorClass: "help-inline",
   errorElement: "span",
   highlight:function(element, errorClass, validClass) {
          $(element).parents('.control-group').addClass('error');
	 },
  unhighlight: function(element, errorClass, validClass) {
		  $(element).parents('.control-group').removeClass('error');
   }
});

function saveForm(){
    var bean = $("#platForm").serialize();
    if($("#platForm").valid()){
      savePlatform(bean);
   }
}


function savePlatform(bean){
   $.ajax({
      url:context_path+"/platform/savePlatform.do",
      type:"post",
      data:bean,
      dataType:"JSON",
      success:function (data){
          if(Boolean(data.result)){      
               layer.msg("保存表头成功！",{icon:1,time:1200});
               $("#platForm #id").val(data.platformId);
               pId=data.platformId;
               $("#grid-table").jqGrid("setGridParam",{
                  postData: {queryJsonString:""}
               }).trigger("reloadGrid");
          }else{
           layer.msg(data.msg,{icon:2,time:2000});
          }
        }
   });
}
_grid_detail=jQuery("#grid-table-c").jqGrid({
      url : context_path + "/platform/detailList?pId="+pId,
      datatype : "json",
      colNames : [ "详情主键","开始时间","结束时间","备注"],
      colModel : [ 
  				{name : "id",index : "id",width : 20,hidden:true}, 					  
  				{name : "startTime",index:"startTime",width :60}, 
                {name : "endTime",index:"endTime",width : 60},
                {name : "remark",index:"remark",width :40}
               ],
      rowNum : 20,
      rowList : [ 10, 20, 30 ],
      pager : "#grid-pager-c",
      sortname : "startTime",
      sortorder : "asc",
      altRows: true,
      viewrecords : true,
      autowidth:true,
      multiselect:true,
	  multiboxonly: true,
      loadComplete : function(data) {
          var table = this;
          setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
          oriDataDetail = data;
      },
  	  emptyrecords: "没有相关记录",
  	  loadtext: "加载中...",
  	  pgtext : "页码 {0} / {1}页",
  	  recordtext: "显示 {0} - {1}共{2}条数据",
});
//在分页工具栏中添加按钮
$("#grid-table-c").navGrid('#grid-pager-c',{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd('#grid-pager-c',{  
  	caption:"",   
  	buttonicon:"ace-icon fa fa-refresh green",   
  	onClickButton: function(){   
  	$("#grid-table-c").jqGrid('setGridParam', {
  	    url:context_path + "/salemanage/detailList?saleId="+saleId,
		    postData: {queryJsonString:""} //发送数据  :选中的节点
		}).trigger("reloadGrid");
  	   }
  	});
  	
  	$(window).on('resize.jqGrid', function () {
  		$("#grid-table-c").jqGrid( "setGridWidth", $("#grid-div-c").width() - 3 );
  		$("#grid-table-c").jqGrid( "setGridHeight", (document.documentElement.clientHeight - $("#grid-pager-c").height() - 268) );
  	});
  	$(window).triggerHandler("resize.jqGrid"); 
 function addDetail(){
	  if($("#platForm #id").val()==""){
		  layer.alert("请先保存表单信息！");
		  return;
	  }
	  $.post(context_path+"/platform/toAddDetail.do?pId="+$("#platForm #id").val(), {}, function(str){
		$queryWindow = layer.open({
			title : "添加预约时间", 
			type: 1,
		    skin : "layui-layer-molv",
		    area : "600px",
			shade: 0.6, //遮罩透明度
			moveType: 1, //拖拽风格，0是默认，1是传统拖动
			content: str,//注意，如果str是object，那么需要字符拼接。
			success:function(layero, index){
				   layer.closeAll('loading');
				    	}
				});
			}).error(function() {
				layer.closeAll();
		    	layer.msg("加载失败！",{icon:2});
		});	
  }
	  $("#__toolbar__-c").iToolBar({
	   	 id:"__tb__01",
	   	 items:[
	   	    {label: "添加", disabled: (${sessionUser.addQx} == 1 ? false : true), onclick:addDetail, iconClass:'glyphicon glyphicon-plus'},
	   	  	{label: "删除", disabled: (${sessionUser.deleteQx} == 1 ? false : true),onclick: deleteDetail, iconClass:'glyphicon glyphicon-trash'}
	    ]
	  });
	function deleteDetail(){
	   var checkedNum = getGridCheckedNum("#grid-table-c", "id");
       if (checkedNum == 0) {
    	  layer.alert("请选择一条要删除的预约记录！");
    	  return;
       }
	   var ids = jQuery("#grid-table-c").jqGrid("getGridParam", "selarrrow");
	   layer.confirm("确定删除选中的月台详情？", function(){
	   $.ajax({
	      url:context_path + "/platform/deleteDetail?ids="+ids,
	      type:"POST",
	      dataType:"JSON",
	      success:function(data){
	         if(Boolean(data.result)){
	           layer.msg(data.msg, {icon: 1,time:1000});
	           $("#grid-table-c").jqGrid("setGridParam",{
                  url:context_path + "/platform/detailList?pId="+pId,
				  postData: {queryJsonString:""}
               }).trigger("reloadGrid");
	         }else{
	           layer.msg(data.msg, {icon: 7,time:2000});
	         }
	      }
	   });
	 });	   
  } 
  </script>
