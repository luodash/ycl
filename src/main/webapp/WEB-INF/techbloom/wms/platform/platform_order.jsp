<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = "<%=path%>";
</script>
<style type="text/css"></style>
<div id="grid-div">
    <form id="hiddenForm" action="<%=path%>/platform/toOrderExcel.do" method="POST" style="display: none;">
        <input id="ids" name="ids" value=""/>
    </form>
    <form id="hiddenQueryForm" style="display:none;">
        <input id="orderNo" name="orderNo" value="" />
        <input id="platformId" name="platformId" value="" />
        <input id="startTime" name="startTime" value="" />
        <input id="orderType" name="orderType" value="" />
        <input id="purchaseId" name="purchaseId" value="" />
    </form>
    <div class="query_box" id="yy" title="查询选项">
            <form id="queryForm" style="max-width:100%;">           
			 <ul class="form-elements">
				<li class="field-group field-fluid3">
					<label class="inline" for="orderNo" style="margin-right:20px;width:100%;">
						<span class='form_label' style="width:80px;">预约编号：</span>
						<input id="orderNo" name="orderNo" type="text" style="width: calc(100% - 85px);" placeholder="预约编号" />
					</label>					
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="platformId" style="margin-right:20px;width:100%;">
						<span class='form_label' style="width:80px;">月台：</span>
						<input id="platformId" name="platformId" type="text" style="width: calc(100% - 85px);" placeholder="月台" />
					</label>					
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="orderType" style="margin-right:20px;width:100%;">
						<span class='form_label' style="width:80px;">预约日期：</span>
						<input id="startTime" name="startTime" class="form-control date-picker" type="text" style="width: calc(100% - 85px);" />
					</label>				
				</li>
				<li class="field-group-top field-group field-fluid3">
					<label class="inline" for="orderType" style="margin-right:20px;width:100%;">
						<span class='form_label' style="width:80px;">预约类型：</span>
						<input id="orderType" name="orderType" type="text" style="width: calc(100% - 85px);" />
					</label>				
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="purchaseId" style="margin-right:20px;width:100%;">
						<span class='form_label' style="width:80px;">单据编号：</span>
						<input id="purchaseId" name="purchaseId" type="text" style="width: calc(100% - 85px);" />
					</label>				
				</li>							
			</ul>
			<div class="field-button">
						<div class="btn btn-info" onclick="queryOk();">
				            <i class="ace-icon fa fa-check bigger-110"></i>查询
			            </div>
						<div class="btn" onclick="reset();"><i class="ace-icon icon-remove"></i>重置</div>
						<a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>
		        </div>
		  </form>		 
    </div>
    <div id="fixed_tool_div" class="fixed_tool_div">
         <div id="__toolbar__" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="grid-table" style="width:100%;height:100%;"></table>
    <div id="grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
var context_path = '<%=path%>';
var _grid;
var dynamicDefalutValue="04564e7824db45d298542d3bcfb63017";
$(function(){
    $(".toggle_tools").click();
});
$(".date-picker").datetimepicker({format: "YYYY-MM-DD"});
$("#__toolbar__").iToolBar({
    id: "__tb__01",
    items: [
      //{label: "添加", disabled: (${sessionUser.addQx} == 1 ? false : true), onclick:addOrder, iconClass:'glyphicon glyphicon-plus'},
      //{label: "编辑", disabled: (${sessionUser.editQx} == 1 ? false : true),onclick: editOrder, iconClass:'glyphicon glyphicon-pencil'},
        {label: "删除", disabled: (${sessionUser.deleteQx} == 1 ? false : true),onclick: deleteOrder, iconClass:'glyphicon glyphicon-trash'},
        {label: "审核", onclick: verify, iconClass:'icon-ok'},
        {label: "导出", disabled: ( ${sessionUser.queryQx}==1 ? false : true),onclick:function(){toExcel();},iconClass:'icon-share'}
   ]
});
$(function(){
        _grid = jQuery("#grid-table").jqGrid({
            url: context_path + "/platform/listOrder.do",
            datatype: "json",
            colNames: ["主键","预约编号","月台","预约类型","单据编号","预约到场时间","计划离场时间","司机姓名","车牌号","车型","备注","状态"],
            colModel: [
                {name: "id", index: "id", width: 20, hidden: true},
                {name: "orderNo", index: "orderNo", width: 50},
                {name: "platformName", index: "platformName", width: 70},
                {name: "orderTypeName", index: "orderTypeName", width: 40},
                {name: "purchaseName", index: "purchaseName", width: 50},         
                {name: "startTime", index: "startTime", width: 100},
                {name: "endTime", index: "endTime", width: 100},
                {name: "driver", index: "driver", width: 40},
                {name: "license", index: "license", width: 50},
                {name: "carType", index: "carType", width: 50},               
                {name: "remark", index: "remark", width: 100},
                {name: "status", index: "status", width:30, formatter: function (value,options,rowObject){
                    if (typeof value == "number") {
                            if(value==0){
                    			return "<span style='color:gray;font-weight:bold;'>未审核</span>" ;
                    		}else if(value==1){
                    			return "<span style='color:green;font-weight:bold;'>已审核</span>" ;
                    		}
                          }else{
                             return "" ;
                             }
                         }
                     }             
            ],
            rowNum: 20,
            rowList: [10, 20, 30],
            pager: "#grid-pager",
            sortname: "startTime",
            sortorder: "desc",
            altRows: true,
            viewrecords: true,
            autowidth: true,
            multiselect: true,
            multiboxonly: true,
            beforeRequest:function (){
                dynamicGetColumns(dynamicDefalutValue,"grid-table",$(window).width()-$("#sidebar").width() -7);
                //重新加载列属性
            },
            loadComplete: function (data) {
                var table = this;
                setTimeout(function () {
                    updatePagerIcons(table);
                    enableTooltips(table);
                }, 0);
                oriData = data;
            },
            emptyrecords: "没有相关记录",
            loadtext: "加载中...",
            pgtext: "页码 {0} / {1}页",
            recordtext: "显示 {0} - {1}共{2}条数据"
        });
        jQuery("#grid-table").navGrid("#grid-pager", {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false
        }).navButtonAdd("#grid-pager", {
                    caption: "",
                    buttonicon: "ace-icon fa fa-refresh green",
                    onClickButton: function () {
                        $("#grid-table").jqGrid("setGridParam",
                                {
                                    postData: {queryJsonString: ""} //发送数据
                                }
                        ).trigger("reloadGrid");
                    }
                }).navButtonAdd("#grid-pager",{
                caption: "",
                buttonicon:"fa  icon-cogs",
                onClickButton : function (){
                    jQuery("#grid-table").jqGrid("columnChooser",{
                        done: function(perm, cols){
                            dynamicColumns(cols,dynamicDefalutValue);
                            $("#grid-table").jqGrid("setGridWidth",$("#grid-div").width());
                        }
                    });
                }
            });
        $(window).on("resize.jqGrid", function () {
            $("#grid-table").jqGrid("setGridWidth", $("#grid-div").width() );
            $("#grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-$("#yy").outerHeight(true)-$("#fixed_tool_div").outerHeight(true)-$("#grid-pager").outerHeight(true)-35);
        });
        $(window).triggerHandler("resize.jqGrid");
    });
function queryOk(){
   var queryParam = iTsai.form.serialize($("#queryForm"));
   queryOrderByParam(queryParam);
}
function queryOrderByParam(jsonParam){
    iTsai.form.deserialize($("#hiddenQueryForm"), jsonParam);   //将json对象反序列化到列表页面中隐藏的form中
    var queryParam = iTsai.form.serialize($("#hiddenQueryForm"));
    var queryJsonString = JSON.stringify(queryParam);         //将json对象转换成json字符串
    //执行查询操作
    $("#grid-table").jqGrid("setGridParam",
        {
            postData: {queryJsonString: queryJsonString} //发送数据
        }
    ).trigger("reloadGrid");
}
function reset(){
   $("#queryForm #orderNo").val("");
   $("#queryForm #platformId").select2("val","");
   $("#queryForm #startTime").val("");
   $("#queryForm #orderType").select2("val","");
   $("#queryForm #purchaseId").select2("val","");
   $("#grid-table").jqGrid("setGridParam",
        {
            postData: {queryJsonString:""} //发送数据
        }
    ).trigger("reloadGrid");
}
function addOrder(){
   $.post(context_path+"/platform/toEditOrder.do?", {}, function(str){
		$queryWindow = layer.open({
			title : "月台预约添加", 
			type: 1,
		    skin : "layui-layer-molv",
		    area : "750px",
			shade: 0.6, //遮罩透明度
			moveType: 1, //拖拽风格，0是默认，1是传统拖动
			content: str,//注意，如果str是object，那么需要字符拼接。
			success:function(layero, index){
				   layer.closeAll("loading");
				   }
			  });
			}).error(function() {
				layer.closeAll();
		    	layer.msg("加载失败！",{icon:2});
		});	
}
function editOrder(){
    var checkedNum = getGridCheckedNum("#grid-table","id");
	if(checkedNum == 0){
    	layer.alert("请选择一个要编辑的预约！");
    	return false;
    } else if(checkedNum >1){
    	layer.alert("只能选择一个预约进行编辑操作！");
    	return false;
    } else {
    	var orderId = jQuery("#grid-table").jqGrid("getGridParam","selrow"); 
		$.post(context_path+"/platform/toEditOrder.do?id="+orderId, {}, function(str){
		$queryWindow = layer.open({
			title : "月台预约编辑", 
			type: 1,
		    skin : "layui-layer-molv",
		    area : "750px",
			shade: 0.6, 
			moveType: 1, 
			content: str,
			success:function(layero, index){
				  layer.closeAll("loading");
				}
			});
			}).error(function() {
			   layer.closeAll();
		       layer.msg("加载失败",{icon:2});
		});	
    }
}
function deleteOrder(){
    var checkedNum = getGridCheckedNum("#grid-table", "id");  //选中的数量
    if (checkedNum == 0) {
    	layer.alert("请选择一个要删除的预约记录！");
    } else {
        var ids = jQuery("#grid-table").jqGrid("getGridParam", "selarrrow");
        layer.confirm("确定删除选中的预约记录？", function() {
    		$.ajax({
    			type : "POST",
    			url : context_path + "/platform/deleteOrder.do?ids="+ids ,
    			dataType : "json",
    			cache : false,
    			success : function(data) {
    				layer.closeAll();
    				if (Boolean(data.result)) {
    					layer.msg(data.msg, {icon: 1,time:1000});
    				}else{
    					layer.msg(data.msg, {icon: 7,time:2000});  					
    				}
    				_grid.trigger("reloadGrid");  //重新加载表格
    			}
    		});
    	});
        
    }
}
function verify(){
    var checkedNum = getGridCheckedNum("#grid-table", "id");  //选中的数量
    if (checkedNum == 0) {
    	layer.alert("请选择一个要审核的预约记录！");
    } else {
        var ids = jQuery("#grid-table").jqGrid("getGridParam", "selarrrow");
    	layer.confirm("确定审核选中的预约记录？", function() {
    	    $.ajax({
    		    type : "POST",
    		    url : context_path + "/platform/setStatus.do?ids="+ids ,
    		    dataType : "json",
    		    cache : false,
    		    success : function(data) {
    			    layer.closeAll();
    			    if (Boolean(data.result)) {
    				    layer.msg(data.msg, {icon: 1,time:1000});
    			    }else{
    				    layer.msg(data.msg, {icon: 7,time:2000});  					
    			    }
    			    _grid.trigger("reloadGrid");  //重新加载表格
    		    }
    	    });               	
    	});    	       
    }
}
$("#queryForm #platformId").select2({
        placeholder: "选择月台",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/platform/getSelectPlatform",
            type: "POST",
            dataType: "json",
            delay: 250,
            data: function (term, pageNo) {
                term = $.trim(term);
                return {
                    queryString: term,
                    pageSize: 15,
                    pageNo: pageNo,
                    time: new Date()
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) {
                    var more = (pageNo * 15) < data.total;
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });
    $("#queryForm #orderType").select2({
        placeholder: "选择预约类型",
        minimumInputLength: 0,
        allowClear: true, 
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/platform/getSelectOrderType",
            type: "POST",
            dataType: "json",
            delay: 250,
            data: function (term, pageNo) {
                term = $.trim(term);
                return {
                    queryString: term,
                    pageSize: 15, 
                    pageNo: pageNo,
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { 
                    var more = (pageNo * 15) < data.total;
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });
    $("#queryForm #purchaseId").select2({
        placeholder: "选择单据编号",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/platform/getSelectBill",
            type: "POST",
            dataType: "json",
            delay: 250,
            data: function (term, pageNo) {
                term = $.trim(term);
                return {
                    queryString: term,
                    pageSize: 15,
                    pageNo: pageNo,
                    orderType: $("#queryForm #orderType").val()
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });
    $("#queryForm #purchaseId").change(function () {
        if ($("#queryForm #orderType").val() == "") {
            layer.alert("请先选择预约类型！");
            $("#queryForm #purchaseId").select2("val","");
        }
    });
    $("#queryForm #orderType").change(function () {
        $("#queryForm #purchaseId").select2("val","");
    });      
    function toExcel(){
        layer.confirm("确认导出？", function() {
            var ids = jQuery("#grid-table").jqGrid("getGridParam", "selarrrow");
            $("#hiddenForm #ids").val(ids);
            $("#hiddenForm").submit();
            layer.closeAll();
        });   	
    }
</script>