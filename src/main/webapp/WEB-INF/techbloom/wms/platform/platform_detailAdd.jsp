<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<div id="detail_add_page" class="row-fluid" style="height: inherit;">
	<form id="detailForm" class="form-horizontal" style="overflow: auto; height: calc(100% - 70px);">
		<input type="hidden" id="pId" name="pId" value="${pId}">
		<div class="control-group">
			<label class="control-label" for="startTime">开始时间：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input class="form-control date-picker" id="startTime" name="startTime" style="width: 435px;" type="text" placeholder="开始时间" /> 
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="endTime">结束时间：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input class="form-control date-picker" id="endTime" name="endTime" style="width: 435px;" type="text" placeholder="结束时间" /> 
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="remark">备注：</label>
			<div class="controls">
				<input type="text" class="span11" id="remark" name="remark" placeholder="备注">
			</div>
		</div>
	</form>
	<div class="field-button" style="text-align: center;border-top: 0px;margin: 15px auto;">
		<span class="btn btn-info" onclick="saveDetail();">
		   <i class="ace-icon fa fa-check bigger-110"></i>保存
		</span>
		<span class="btn btn-danger" onclick="layer.close($queryWindow);return false;">
		   <i class="icon-remove"></i>&nbsp;取消
		</span>
	</div>
</div>
<script type="text/javascript">
$(".date-picker").datetimepicker({format: "HH:mm:ss",useMinutes:true,useSeconds:true,pickDate:false});
$("#detailForm").validate({
		rules:{
            "startTime":{
  				required:true
  			},
  			"endTime":{
  				required:true
  			}
  		},
  		messages:{
  			"startTime":{
  				required:"请输入开始时间！"
  			},
  			"endTime":{
  				required:"请输入结束时间！"
  			}
  		},
	    errorClass: "help-inline",
	    errorElement: "span",
	    highlight:function(element, errorClass, validClass) {
		    $(element).parents('.control-group').addClass('error');
	    },
	    unhighlight: function(element, errorClass, validClass) {
		    $(element).parents('.control-group').removeClass('error');
	    }
  	});
function saveDetail(){
    if($("#detailForm").valid()){
    var detail=$("#detailForm").serialize();
	  $.ajax({
      url:context_path+"/platform/saveDetail.do",
      type:"post",
      data:detail,
      dataType:"JSON",
      success:function (data){
          if(Boolean(data.result)){      
               layer.msg("保存成功！",{icon:1,time:1200});
               pId=data.pId
               $("#grid-table-c").jqGrid("setGridParam",{
                  url:context_path + "/platform/detailList?pId="+data.pId,
				  postData: {queryJsonString:""}
               }).trigger("reloadGrid");
               layer.close($queryWindow);
          }else{
           layer.msg(data.msg,{icon:2,time:2000});
          }
        }
   });
   }     
}
</script>