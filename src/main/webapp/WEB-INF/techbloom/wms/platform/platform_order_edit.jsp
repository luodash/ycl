<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<script type="text/javascript">
	var context_path = "<%=path%>";
</script>
<div id="platform_order_edit_page" class="row-fluid" style="height: inherit;">
   	<form action="" class="form-horizontal" id="orderForm" name="materialForm" method="post" target="_ifr">
   	    <input type="hidden" id="id" name="id" value="${order.id}" />
   	    <input type="hidden" id="orderNo" name="orderNo" value="${order.orderNo}" />
		<input type="hidden" id="status" name="status" value="${order.status}" />
		<input type="hidden" id="purchaseId" name="purchaseId" value="${order.purchaseId }" />
		<input type="hidden" id="item" name="type" value="${item }" />
		<input type="hidden" id="tttt" name="tttt" value="${orderType }" />	
		<div class="row" style="margin:0;padding:0;">
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="materialNo" >月台：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
				<div class="controls">
					<div class="span12 required" style=" float: none !important;">
						<input type="text" class="span10" id="platformId" name="platformId" value="${order.platformId }" style="width: 300px !important;">				        
					</div>
				</div>
			</div>						
		</div>
		<div class="row" style="margin:0;padding:0;">
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="orderType" >预约类型：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
				<div class="controls">
					<div class="span12 required" style="float: none !important;">
						<input type="text" class="span10" id="orderType" name="orderType"/>
					</div>
				</div>
			</div>
			<div id="fa" class="control-group span6" style="display: inline">
				<label class="control-label" for="purchaseId" >装车发运单：&nbsp;&nbsp;&nbsp;</label>
				<div class="controls">
					<div class="span12 required" style="float:none !important;" >
						<input type="text" class="span10" id="ffff" name="ffff">
					</div>
				</div>
			</div>
			<div id="shou" class="control-group span6" style="display: inline">
				<label class="control-label" for="purchaseId" >收货单：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
				<div class="controls">
					<div class="span12 required" style="float:none !important;" >
						<input type="text" class="span10" id="ssss" name="ssss">
					</div>
				</div>
			</div>			
		</div>
		<div class="row" style="margin:0;padding:0;">
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="materialTypeId" >预约到场时间：</label>
				<div class="controls">
					<input class="form-control date-picker" id="startTime" name="startTime" type="text" value="${order.startTime }" placeholder="预约到场时间" style="width:203px;"/> 
					<span class="input-group-addon"> <i class="fa fa-calendar bigger-110"></i></span>
				</div>
			</div>
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="supplyId" >计划离场时间：</label>
				<div class="controls">
				    <input class="form-control date-picker" id="endTime" name="endTime" type="text" value="${order.endTime }" placeholder="计划离场时间" style="width:203px;"/> 				    
					<span class="input-group-addon"> <i class="fa fa-calendar bigger-110"></i></span>
				</div>
			</div>
		</div>		
		<div class="row" style="margin:0;padding:0;">
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="specification" >司机姓名：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
				<div class="controls">
					<div class="input-append span12 required" >
						<input  type="text" class="span10" name="driver" id="driver" placeholder="司机姓名" value="${order.driver}"/>
					</div>
				</div>
			</div>
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="packageName" >车牌号：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
				<div class="controls">
					<div class="input-append span12 required" >
						<input type="text" class="span10" id="license" name="license" value="${order.license }" placeholder="车牌号">
					</div>
				</div>
			</div>
		</div>
		<div class="row" style="margin:0;padding:0;">
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="brand" >车型：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
				<div class="controls">
					<div class="input-append span12" >
						<input type="text" class="span10"  name="carType" id="carType" placeholder="车型" value="${order.carType}" >
					</div>
				</div>
			</div>
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="weight" >备注：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
				<div class="controls">
					<div class="input-append span12" >
						<input type="text" class="span10" id="remark" name="remark" value="${order.remark }" placeholder="备注">
					</div>
				</div>
			</div>
		</div>		
  </form>
  <div class="field-button" style="text-align: center;border-top: 0px;margin: 15px auto;">
		<span class="btn btn-info" onclick="saveForm();">
		   <i class="ace-icon fa fa-check bigger-110"></i>保存
		</span>
		<span class="btn btn-danger" onclick="layer.closeAll();">
		   <i class="icon-remove"></i>&nbsp;取消
		</span>
	</div>
</div>
<iframe src="about:blank" name="_ifr" height="0" class="hidden"></iframe>
<script type="text/javascript">
$(".date-picker").datetimepicker({format: "YYYY-MM-DD HH:mm:ss",useMinutes:true,useSeconds:true});
	$("#orderForm").validate({
	    ignore: function(i,dom){
            var r_dom ="";
            if(dom.id == "ffff" && $("#orderForm #orderType").val() == "WMS_PLATFORMORDER_SH"){
                r_dom = dom;
            }else if(dom.id == "ssss" && $("#orderForm #orderType").val() == "WMS_PLATFORMORDER_FH"){
                r_dom = dom;
            }
            return r_dom;
        },
		rules:{
		    "platformId":{
  				required:true	
  			},
  			"orderType":{
  			    required:true
  			},
  			"ffff":{
  			    required:true
  			},
  			"ssss":{
  			    required:true
  			},
  			"driver":{
  				required:true
  			},
  			"license":{
  			   required: true
  			},
			"startTime":{
  				required:true	
  			},
			"endTime":{
  				required:true	
  			}
  		},
  		messages:{
  			"platformId":{
  				required: "请选择月台！"	
  			},
  			"orderType":{
  			    required: "请选择预约类型！"
  			},
  			"ffff":{
  			    required: "请选择装车发运单！"
  			},
  			"ssss":{
  			    required: "请选择预收货通知单！"
  			},
  			"driver":{
  				required: "请输入司机姓名！"	
  			},
  			"license":{
  			   required: "请输入车牌号！"	
  			},
			"startTime":{
  				required: "请输入到场时间"		
  			},
			"endTime":{
  				required: "请输入离场时间！"		
  			}  
  		},
  		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
		}
  	}); 	
    function saveForm() {
        if ($("#orderForm").valid()) {
            saveOrderInfo($("#orderForm").serialize());
        }
    }
    function saveOrderInfo(bean) {
        $.ajax({
                url: context_path + "/platform/saveOrder.do",
                type: "POST",
                data: bean,
                dataType: "JSON",
                success: function (data) {
                    if (Boolean(data.result)) {                       
                        layer.closeAll();
                        $("#grid-table").jqGrid("setGridParam",{
                          postData: {queryJsonString:""} //发送数据
                        }).trigger("reloadGrid");
                        layer.msg(data.msg, {icon: 1});
                        $("#scheduleForm #date").val(data.orderDay);
                        queryOk();
                    } else {
                        layer.alert(data.msg, {icon: 2});
                    }
                },
                error:function(XMLHttpRequest){
            		alert(XMLHttpRequest.readyState);
            		alert("出错啦！！！");
            	}
            });

    }  
    $("#orderForm #platformId").select2({
        placeholder: "选择月台",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        width: 203,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/platform/getSelectPlatform",
            type: "POST",
            dataType: "json",
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });   
    $("#orderForm #ffff").select2({
        placeholder: "选择装车发运单号",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        width: 203,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/platform/getSelectDelivery",
            type: "POST",
            dataType: "json",
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });
    $("#orderForm #ssss").select2({
        placeholder: "选择预收货通知单",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        width: 203,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/platform/getSelectASN",
            type: "POST",
            dataType: "json",
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });
    $("#orderForm #orderType").select2({
        placeholder: "选择预约类型",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        width: 203,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/platform/getSelectOrderType",
            type: "POST",
            dataType: "json",
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });
     $.ajax({
	 type:"POST",
	 url:context_path + "/platform/getOrderById",
	 data:{id:$("#orderForm #id").val()},
	 dataType:"json",
	 success:function(data){
			 $("#orderForm #platformId").select2("data", {
 			    id: data.platformId,
 			    text: data.platformName
             }); 
             $("#orderForm #orderType").select2("data", {
                id: data.orderType,
                text:data.orderTypeName
             });
             if($("#orderForm #tttt").val()=="fahuo"){
                  $("#fa").show();
                  $("#shou").hide();
                  $("#orderForm #ffff").select2("data", {
                       id: data.purchaseId,
                       text:data.purchaseName
                  });
             }else if($("#orderForm #tttt").val()=="shouhuo"){
                  $("#fa").hide();
                  $("#shou").show();
                  $("#orderForm #ssss").select2("data", {
                       id: data.purchaseId,
                       text:data.purchaseName
                  });
             }else{
                  $("#fa").hide();
                  $("#shou").hide();
             }
		}
	});
function echoPlatform(){
    $.post(context_path+"/platform/echoPlatform.do", {}, function(str){
        $queryWindow = layer.open({
            title : "月台查看",
            type: 1,
            skin : "layui-layer-molv",
            area : ["1100px","650px"],
            shade: 0.6, //遮罩透明度
            moveType: 1, //拖拽风格，0是默认，1是传统拖动
            content: str,//注意，如果str是object，那么需要字符拼接。
            success:function(layero, index){
                layer.closeAll('loading');
            }
        });
    }).error(function() {
        layer.closeAll();
        layer.msg("加载失败！",{icon:2});
    });
}
if($("#orderForm #tttt").val()=="fahuo"){
                  $("#fa").show();
                  $("#shou").hide();
             }else if($("#orderForm #tttt").val()=="shouhuo"){
                  $("#fa").hide();
                  $("#shou").show();
             }else{
                  $("#fa").hide();
                  $("#shou").hide();
             }
$("#orderForm #orderType").change(function(){
	$("#orderForm #ffff").select2("val", "");
	$("#orderForm #ssss").select2("val", "");
	if($("#orderForm #orderType").val()=="WMS_PLATFORMORDER_FH"){
        $("#fa").show();
        $("#shou").hide();
    }else if($("#orderForm #orderType").val()=="WMS_PLATFORMORDER_SH"){
        $("#fa").hide();
        $("#shou").show();
    }else{
        $("#fa").hide();
        $("#shou").hide();
    }
});
$("#orderForm #ffff").change(function(){
	$("#orderForm #purchaseId").val($("#orderForm #ffff").val());
});
$("#orderForm #ssss").change(function(){
	$("#orderForm #purchaseId").val($("#orderForm #ssss").val());
});
</script>
