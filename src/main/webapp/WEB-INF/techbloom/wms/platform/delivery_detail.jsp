<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<div id="delivery_detail_grid-div">
    <div id="delivery_detail_grid-div" style="width:100%;margin:0px auto;">
        <div id="delivery_detail_fixed_tool_div" class="fixed_tool_div">
            <div id="delivery_detail___toolbar__" style="float:left;overflow:hidden;"></div>
        </div>
        <table id="grid-table_delivery_detail" style="width:100%;height:100%;overflow:auto;"></table>
        <div id="grid-pager_delivery_detail"></div>
    </div>
</div>
<script type="text/javascript">
    var oriData_deliveryDetail;
    var _grid_deliveryDetail;

    //表格加载
    _grid_deliveryDetail = jQuery("#grid-table_delivery_detail").jqGrid({
        url : context_path + '/deliveryList/getDetail.do?id='+ssdd,
        datatype : "json",
        styleUI: 'Bootstrap',
        colNames : [ '主键','批次号', '数量','库位信息','rfid编号','状态'],
        colModel : [
            {name : 'id',index : 'id',hidden:true},
            {name : 'batchNo',index : 'batchNo',width : 100},
            {name : 'amount',index : 'amount',width : 100,editable : true},
            {name : 'allocation',index : 'allocation',width : 100},
            {name : 'rfid',index : 'rfid',width : 100},
            {name : 'state',index : 'state',width : 100,formatter:function(cellVal,option,rowObject){
                    var obj = {
                        "0":"未进行",
                        "1":"<i style='color:red'>进行中<i>",
                        "2":"<i style='color:red'>完成<i>",
                        null:"<i style='color:red'><i>"
                    };
                    return "<a>"+obj[cellVal]+"</a>";
                }
            },
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : '#grid-pager_delivery_detail',
        sortname : 'dd.id',
        sortorder : "desc",
        altRows: true,
        viewrecords : true,
        hidegrid:false,
        multiselect:true,
        autowidth:true,
        loadComplete : function(data)
        {
            var table = this;
            setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            oriData_deliveryDetail = data;
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
    });

    //在分页工具栏中添加按钮
    jQuery("#grid-table1").navGrid('#grid-pager_delivery_detail',{edit:false,add:false,del:false,search:false,refresh:false})
        .navButtonAdd('#grid-pager_delivery_detail',{
            caption:"",
            buttonicon:"fa fa-refresh green",
            onClickButton: function(){
                $("#grid-table1").jqGrid('setGridParam',
                    {
                        postData: {queryJsonString:""} //发送数据
                    }
                ).trigger("reloadGrid");
            }
        });
    $(window).triggerHandler('resize.jqGrid');
</script>