<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<style type="text/css"></style>
<div id="grid-div">
	<form id="hiddenForm" action = "<%=path%>/platform/toExcel" method = "POST" style="display: none;">
		<input id="ids" name="ids" value=""/>
	</form>
    <form id="hiddenQueryForm" style="display:none;">
        <input name="platformNo" id="platformNo" value=""/>
        <input id="platformName" name="platformName" value="">
    </form>
    <div class="query_box" id="yy" title="查询选项">
            <form id="queryForm" style="max-width:100%;">
			 <ul class="form-elements">
				<li class="field-group field-fluid2">
					<label class="inline" for="platformNo" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:65px;">月台编号：</span>
						<input type="text" name="platformNo" id="platformNo" value="" style="width: calc(100% - 70px);" placeholder="月台编号">
					</label>			
				</li>
				<li class="field-group field-fluid2">
					<label class="inline" for="platformName" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:65px;">月台名称：</span>
						<input type="text" name="platformName" id="platformName" value="" style="width: calc(100% - 70px);" placeholder="月台名称">
					</label>			
				</li>
				
			</ul>
			<div class="field-button">
						<div class="btn btn-info" onclick="queryOk();">
				            <i class="ace-icon fa fa-check bigger-110"></i>查询
			            </div>
						<div class="btn" onclick="reset();"><i class="ace-icon icon-remove"></i>重置</div>
		        </div>
		  </form>		 
    </div>
    <div id="fixed_tool_div" class="fixed_tool_div">
        <div id="__toolbar__" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="grid-table" style="width:100%;height:100%;"></table>
    <div id="grid-pager"></div>
</div>
</body>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
var context_path = '<%=path%>';
var _grid;
$(function  (){
    $(".toggle_tools").click();
});
$("#__toolbar__").iToolBar({
    id: "__tb__01",
    items: [
        {label: "添加", disabled: (${sessionUser.addQx} == 1 ? false : true), onclick:addPlatform, iconClass:'glyphicon glyphicon-plus'},
        {label: "编辑", disabled: (${sessionUser.editQx} == 1 ? false : true), onclick: editPlatform, iconClass:'glyphicon glyphicon-pencil'},
        {label: "删除", disabled: (${sessionUser.deleteQx} == 1 ? false : true), onclick: deletePlatform, iconClass:'glyphicon glyphicon-trash'},
		{label: "导出", disabled: (${sessionUser.queryQx} == 1 ? false:true), onclick:excelPlatform,iconClass:'icon-share'}
        ]
});
$(function () {
        _grid = jQuery("#grid-table").jqGrid({
            url: context_path + "/platform/list.do",
            datatype: "json",
            colNames: ["主键", "月台编号", "月台名称","最大收货数量", "可预约状态"],
            colModel: [
                {name: "id", index: "id", width: 20, hidden: true},
                {name: "platformNo", index: "platformNo", width: 65},
                {name: "platformName", index: "platformName", width: 100},
                {name: "dailyMax", index: "dailyMax", width: 30},
                {name: "status",index:"status",width:50,formatter:function(cellvalue,option,rowObject){
                      if(cellvalue==0){
                          return "<span style='color:red;font-weight:bold;'>不可预约</span>";
                      }
                      if(cellvalue==1){
                          return "<span style='color:green;font-weight:bold;'>可预约</span>";
                      }
                    }
                 }             
            ],
            rowNum: 20,
            rowList: [10, 20, 30],
            pager: "#grid-pager",
            sortname: "platformNo",
            sortorder: "desc",
            altRows: true,
            viewrecords: true,
            autowidth: true,
            multiselect: true,
            multiboxonly: true,
            loadComplete: function (data) {
                var table = this;
                setTimeout(function () {
                    updatePagerIcons(table);
                    enableTooltips(table);
                }, 0);
                oriData = data;
            },
            emptyrecords: "没有相关记录",
            loadtext: "加载中...",
            pgtext: "页码 {0} / {1}页",
            recordtext: "显示 {0} - {1}共{2}条数据"
        });
        jQuery("#grid-table").navGrid("#grid-pager", {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false
        }).navButtonAdd("#grid-pager", {
                    caption: "",
                    buttonicon: "ace-icon fa fa-refresh green",
                    onClickButton: function () {
                        $("#grid-table").jqGrid("setGridParam",
                                {
                                    postData: {queryJsonString: ""} //发送数据
                                }
                        ).trigger("reloadGrid");
                    }
                });
        $(window).on("resize.jqGrid", function () {
            $("#grid-table").jqGrid("setGridWidth", $("#grid-div").width() );
            $("#grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-$("#yy").outerHeight(true)-$("#fixed_tool_div").outerHeight(true)-$("#grid-pager").outerHeight(true)
           -$("#gview_grid-table .ui-jqgrid-hdiv").outerHeight(true));
        });

        $(window).triggerHandler("resize.jqGrid");
    });
function queryOk(){
   var queryParam = iTsai.form.serialize($("#queryForm"));
   queryPlatformByParam(queryParam);
}
function queryPlatformByParam(jsonParam){
    iTsai.form.deserialize($("#hiddenQueryForm"), jsonParam);   //将json对象反序列化到列表页面中隐藏的form中
    var queryParam = iTsai.form.serialize($("#hiddenQueryForm"));
    var queryJsonString = JSON.stringify(queryParam);         //将json对象转换成json字符串
    //执行查询操作
    $("#grid-table").jqGrid("setGridParam",
        {
            postData: {queryJsonString: queryJsonString} //发送数据
        }
    ).trigger("reloadGrid");
}
function reset(){
   $("#queryForm #platformNo").val("");
   $("#queryForm #platformName").val("");
   $("#grid-table").jqGrid("setGridParam",
        {
            postData: {queryJsonString:""} //发送数据
        }
    ).trigger("reloadGrid");
}
function addPlatform(){
   $.post(context_path+"/platform/toEdit.do?platformId=-1", {}, function(str){
		$queryWindow = layer.open({
			title : "月台添加", 
			type: 1,
		    skin : "layui-layer-molv",
		    area : ["750px","650px"],
			shade: 0.6, //遮罩透明度
			moveType: 1, //拖拽风格，0是默认，1是传统拖动
			content: str,//注意，如果str是object，那么需要字符拼接。
			success:function(layero, index){
				   layer.closeAll("loading");
				   }
			  });
			}).error(function() {
				layer.closeAll();
		    	layer.msg("加载失败！",{icon:2});
		});	
}
function editPlatform(){
    var checkedNum = getGridCheckedNum("#grid-table","id");
	if(checkedNum == 0)
	{
    	layer.alert("请选择一个要编辑的月台！");
    	return false;
    } else if(checkedNum >1){
    	layer.alert("只能选择一个月台进行编辑操作！");
    	return false;
    } else {
    	var platformId = jQuery("#grid-table").jqGrid("getGridParam", "selrow"); 
		$.post(context_path+"/platform/toEdit.do?platformId="+platformId, {}, function(str){
		$queryWindow = layer.open({
			title : "月台编辑", 
			type: 1,
		    skin : "layui-layer-molv",
		    area : ["750px","650px"],
			shade: 0.6, 
			moveType: 1, 
			content: str,
			success:function(layero, index){
				   layer.closeAll("loading");
				    	}
				});
			}).error(function() {
				layer.closeAll();
		    	layer.msg("加载失败！",{icon:2});
		});	
    }
}
function deletePlatform(){
    var checkedNum = getGridCheckedNum("#grid-table", "id");  //选中的数量
    if (checkedNum == 0) {
    	layer.alert("请选择一个要删除的月台！");
    } else {
        var ids = jQuery("#grid-table").jqGrid("getGridParam", "selarrrow");
        layer.confirm("确定删除选中的月台？", function() {
    		$.ajax({
    			type : "POST",
    			url : context_path + "/platform/deletePlatform.do?ids="+ids ,
    			dataType : "json",
    			cache : false,
    			success : function(data) {
    				layer.closeAll();
    				if (Boolean(data.result)) {
    					layer.msg(data.msg, {icon: 1,time:1000});
    				}else{
    					layer.msg(data.msg, {icon: 7,time:2000});  					
    				}
    				_grid.trigger("reloadGrid");  //重新加载表格
    			}
    		});
    	});
        
    }
}
function viewSchedule(id){
$.post(context_path+"/platform/viewSchedule.do?pId="+id, {}, function(str){
		$queryWindow = layer.open({
			    title : "预约时间表", 
		    	type: 1,
		    	skin : "layui-layer-molv",
		    	area : ["1100px","650px"],
		    	shade: 0.6, //遮罩透明度
		    	shadeClose: true,
	    	    moveType: 1, //拖拽风格，0是默认，1是传统拖动
		    	content:str,//注意，如果str是object，那么需要字符拼接。
		    	success:function(layero, index){
		    		layer.closeAll("loading");
		    	}
			});
		}).error(function() {
			layer.closeAll();
    		layer.msg("加载失败！",{icon:2});
		});
}

/*月台导出*/
function excelPlatform(){
    var selectid = jQuery("#grid-table").jqGrid("getGridParam","selarrrow");
    $("#ids").val(selectid);
    $("#hiddenForm").submit();
}
</script>