﻿<%@page import="java.math.BigDecimal"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<%@ include file="/techbloom/common/taglibs.jsp"%>
<script type="text/javascript">
		function printA4(){
			window.print();
		}
	</script>
</head>
<body onload="printA4();">
	<c:forEach items="${detailList }" var="d" varStatus="dlist" step="10">
		<div style="height:100%">
		<br />
		<br />
		<h3 align="center" style="font-weight: bold;">台套化配置详情</h3>
		<table style="border-collapse: collapse; border: none;width: 16.2cm;font-size: 12px;" align="center">
			<tr>
				<td style="border: solid #000 1px;text-align:center;width:3cm;">物料编号</td>
				<td style="border: solid #000 1px;text-align:center;width:8.2cm;">物料名称</td>
				<td style="border: solid #000 1px;text-align:center;width:3cm;">数量</td>			
			</tr>
			<c:forEach items="${detailList }" var="d" varStatus="dlist" begin="${dlist.index}" end="${dlist.index+9}" step="1">		
				<tr>
					<td style="border: solid #000 1px;text-align:center;">${d.materialNo}</td>
					<td style="border: solid #000 1px;text-align:center;">${d.materialName}</td>
					<td style="border: solid #000 1px;text-align:center;">${d.amount}</td>
				</tr>
			</c:forEach>			
		</table>		
		<br/>
		<br/>
		</div>
	</c:forEach>
</body>
</html>

