<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="grid-div">
    <form id="hiddenForm" action="<%=path%>/sortPlan/toExcel.do" method="POST" style="display: none;">
        <input id="ids" name="ids" value=""/>
    </form>
    <!-- 隐藏区域：存放查询条件 -->
    <form id="hiddenQueryForm" style="display:none;">
        <input id="planNo" name="planNo" value=""/>
        <input id="planTime" name="planTime" value="">
        <input id="finishedTime" name="finishedTime" value=""/>
        <input id="lineId" name="lineId" value="">
    </form>
     <div class="query_box" id="yy" title="查询选项">
            <form id="queryForm" style="max-width:100%;">
			 <ul class="form-elements">
				<li class="field-group field-fluid3">
					<label class="inline" for="planNo" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:80px;">计划单号：</span>
						<input type="text" name="planNo" id="planNo" value="" style="width: calc(100% - 85px);" placeholder="计划单号">
					</label>			
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="planTime" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:80px;">开始时间：</span>
						<input class="form-control date-picker" id="startTime" name="startTime" type="text" placeholder="开始时间" style="width:calc(100% - 85px);"/> 
					</label>				
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="finishedTime" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:80px;">结束时间：</span>
						<input class="form-control date-picker" id="finishedTime" name="finishedTime" type="text" placeholder="结束时间" style="width:calc(100% - 85px);"/> 
					</label>			
				</li>
				<li class="field-group-top field-group field-fluid3">
					<label class="inline" for="lineId" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:80px;">生产线：</span>
						<input type="text" name="lineId" id="lineId" value="" style="width: calc(100% - 85px);" placeholder="操作人">
					</label>				
				</li>
				
			</ul>
			<div class="field-button" style="">
					<div class="btn btn-info" onclick="queryOk();">
				        <i class="ace-icon fa fa-check bigger-110"></i>查询
			        </div>
					<div class="btn" onclick="reset();"><i class="ace-icon icon-remove"></i>重置</div>
					<a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>
		        </div>
		  </form>		 
    </div>
    <div id="fixed_tool_div" class="fixed_tool_div">
        <div id="__toolbar__" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="grid-table" style="width:100%;margin: 0px !important;boder:0px !important"></table>
    <div id="grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
    $(".date-picker").datetimepicker({format: "YYYY-MM-DD HH:mm:ss",useMinutes:true,useSeconds:true});
    var context_path = '<%=path%>';
    var oriData;
    var _grid;
    var openwindowtype;
    var dynamicDefalutValue="f1840fe8727340368e22b37e4311409f";
    $(function  (){
        $(".toggle_tools").click();
    });
    $("#__toolbar__").iToolBar({
        id: "__tb__01",
        items : [
            {label: "添加", disabled: (${sessionUser.addQx} == 1 ? false : true), onclick: addPlan, iconClass:'glyphicon glyphicon-plus'},
            {label: "编辑", disabled: (${sessionUser.editQx} == 1 ? false : true), onclick: editPlan, iconClass:'glyphicon glyphicon-pencil'},
            {label: "删除", disabled: (${sessionUser.deleteQx} == 1 ? false : true), onclick: delPlan, iconClass:'glyphicon glyphicon-trash'},
            {label: "导出", disabled: ( ${sessionUser.queryQx}==1 ? false : true), onclick:function(){toExcel();},iconClass:' icon-share'}
           ]
    });

    $(function () {
        _grid = jQuery("#grid-table").jqGrid({
            url: context_path + "/sortPlan/list.do",
            datatype: "json",
            colNames: ["主键", "生产任务单号","计划生产时间", "生产线","状态","操作"],
            colModel: [
                {name: "id", index: "id", width: 20, hidden: true},
                {name: "planNo", index: "planNo", width: 60},
                {name: "planTime",index: "planTime",width:70},
                {name: "lineName", index: "lineName", width: 60},
                {name: "status", index: "status" ,width : 30,
                formatter:function(cellValue,option,rowObject){
                    if(typeof cellValue == "number"){
                        if(cellValue==0){
                            return "<span style='color:grey;font-weight:bold;'>待生产</span>";
                        }else if(cellValue==1){
                            return "<span style='color:blue;font-weight:bold;'>生产中</span>";
                        }else if(cellValue==2){
                            return "<span style='color:green;font-weight:bold;'>生产完成</span>";
                        }
                    }else{
                            return "未知";
                        }
                      }
                 },
                 {name:"operation",index:"operation",width:90,
	        			formatter: function (cellValu, option, rowObject) {
	                        return "<div style='margin-bottom:5px' class='btn btn-xs btn-success' onclick='viewDetail(" + rowObject.id + ")'>详情</div>"
	        			}
	        		}
            ],
            rowNum: 20,
            rowList: [10, 20, 30],
            pager: "#grid-pager",
            sortname: "sp.planNo",
            sortorder: "asc",
            altRows: true,
            viewrecords: true,
            autowidth: true,
            multiselect: true,
            multiboxonly: true,
            beforeRequest:function (){
                dynamicGetColumns(dynamicDefalutValue,'grid-table', $(window).width()-$("#sidebar").width() -7);
                //重新加载列属性
            },
            loadComplete: function (data) {
                var table = this;
                setTimeout(function () {
                    updatePagerIcons(table);
                    enableTooltips(table);
                }, 0);
                oriData = data;
            },
            emptyrecords: "没有相关记录",
            loadtext: "加载中...",
            pgtext: "页码 {0} / {1}页",
            recordtext: "显示 {0} - {1}共{2}条数据"
        });
        //在分页工具栏中添加按钮
        jQuery("#grid-table").navGrid("#grid-pager", {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false
        }).navButtonAdd("#grid-pager", {
                    caption: "",
                    buttonicon: "ace-icon fa fa-refresh green",
                    onClickButton: function () {
                        $("#grid-table").jqGrid("setGridParam",
                                {
                                    postData: {queryJsonString: ""} //发送数据
                                }
                        ).trigger("reloadGrid");
                    }
                }).navButtonAdd("#grid-pager",{
                caption: "",
                buttonicon:"ace-icon fa icon-cogs",
                onClickButton : function (){
                    jQuery("#grid-table").jqGrid("columnChooser",{
                        done: function(perm, cols){
                            dynamicColumns(cols,dynamicDefalutValue);
                            //cols页面获取隐藏的列,页面表格的值
                            $("#grid-table").jqGrid("setGridWidth", $("#grid-div").width() );
                        }
                    });
                }
            });
        $(window).on("resize.jqGrid", function () {
            $("#grid-table").jqGrid("setGridWidth", $("#grid-div").width() );
            $("#grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-$("#yy").outerHeight(true)-$("#fixed_tool_div").outerHeight(true)-$("#grid-pager").outerHeight(true)
           -$("#gview_grid-table .ui-jqgrid-hdiv").outerHeight(true));
        });

        $(window).triggerHandler("resize.jqGrid");
    });
    
    var _queryForm_data = iTsai.form.serialize($("#queryForm"));
    
	function queryOk(){
		var queryParam = iTsai.form.serialize($("#queryForm"));
		queryByParam(queryParam);		
	}
	
	function queryByParam(jsonParam) {
	    iTsai.form.deserialize($("#hiddenQueryForm"), jsonParam);
	    var queryParam = iTsai.form.serialize($("#hiddenQueryForm"));
	    var queryJsonString = JSON.stringify(queryParam); 
	    $("#grid-table").jqGrid("setGridParam",
	        {
	            postData: {queryJsonString: queryJsonString}
	        }).trigger("reloadGrid");
    }
	
	function reset(){
	    $("#queryForm #planNo").val("").trigger("change");
		$("#queryForm #startTime").val("").trigger("change");
        $("#queryForm #finishedTime").val("").trigger("change");
        $("#queryForm #lineId").select2("val","");
	    $("#grid-table").jqGrid("setGridParam", {
            postData: {queryJsonString:""}
        }).trigger("reloadGrid");		
	}
	
	function addPlan(){
	    openwindowtype=0;
		$.post(context_path + "/sortPlan/toEdit.do", {}, function (str){
		$queryWindow=layer.open({
		    title : "生产计划添加", 
	    	type:1,
	    	skin : "layui-layer-molv",
	    	area : ["750px","600px"],
	    	shade : 0.6, //遮罩透明度
		    moveType : 1, //拖拽风格，0是默认，1是传统拖动
		    anim : 2,
		    content : str,
		    success: function (layero, index) {
                layer.closeAll("loading");
            }
		});
	 });
	}
	
	function editPlan(){
	    var checkedNum = getGridCheckedNum("#grid-table", "id");
    if (checkedNum == 0) {
       layer.alert("请选择一个要编辑的翻包出！");
        return false;
    } else if (checkedNum > 1) {
    	layer.alert("只能选择一个翻包出进行编辑操作！");
        return false;
    } else {
        openwindowtype=1;
        var id = jQuery("#grid-table").jqGrid("getGridParam","selrow");
        $.post(context_path + "/sortPlan/toEdit.do?id="+id, {}, function (str){
    		$queryWindow=layer.open({
    		    title : "生产计划编辑", 
    	    	type:1,
    	    	skin : "layui-layer-molv",
    	    	area : ["750px","600px"],
    	    	shade : 0.6, //遮罩透明度
    		    moveType : 1, //拖拽风格，0是默认，1是传统拖动
    		    anim : 2,
    		    content : str,
    		    success: function (layero, index) {
                    layer.closeAll("loading");
                }
    		});
    	});
      }
	}
	function delPlan(){
	    var checkedNum = getGridCheckedNum("#grid-table", "id");  //选中的数量
    if (checkedNum == 0) {
        layer.alert("请选择至少一条要删除的记录！");
    } else {
        var ids = jQuery("#grid-table").jqGrid("getGridParam", "selarrrow");
        layer.confirm("确定删除选中的记录？", function() {
    		$.ajax({
    			type : "post",
    			url : context_path + "/sortPlan/delPlan.do?ids=" + ids,
    			dataType : "json",
    			cache : false,
    			success : function(data) {
    				layer.closeAll();
    				if (Boolean(data.result)) {
    					layer.msg(data.msg, {icon: 1,time:1000});
    				}else{
    					layer.msg(data.msg, {icon: 7,time:2000});
    				}
    				_grid.trigger("reloadGrid");  //重新加载表格
    			}
    		});
    	});
     }
	}
	
	function viewDetail(planId){
	   $.post(context_path + "/sortPlan/toDetail.do?id="+planId, {}, function (str){
    		$queryWindow=layer.open({
    		    title : "生产计划详情", 
    	    	type:1,
    	    	skin : "layui-layer-molv",
    	    	area : ["750px","600px"],
    	    	shade : 0.6, //遮罩透明度
    		    moveType : 1, //拖拽风格，0是默认，1是传统拖动
    		    anim : 2,
    		    content : str,
    		    success: function (layero, index) {
                    layer.closeAll("loading");
                }
    		});
    	});
	}
	
	$("#queryForm #lineId").select2({
        placeholder: "选择生产线",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/productionLine/getSelectLine",
            type: "POST",
            dataType: "json",
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });
    function toExcel(){
        var ids = jQuery("#grid-table").jqGrid("getGridParam", "selarrrow");
        $("#hiddenForm #ids").val(ids);
        $("#hiddenForm").submit();	
    }
</script>
</html>