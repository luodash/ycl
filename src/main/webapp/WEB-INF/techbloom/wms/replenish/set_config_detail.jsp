<%@ page language="java" import="java.lang.*"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
	String path = request.getContextPath();
%>
<div class="main-content" style="height:100%">
	<div class="widget-header widget-header-large" id="div1">
		<input type="hidden" id="id" name="id" value="${set.id }" />
		<h3 class="widget-title grey lighter" style=' background: none; border-bottom: none; '>
			<i class="ace-icon fa fa-leaf green"></i>
			台套化配置详情
		</h3>
		<div class="widget-toolbar no-border invoice-info">
			<span class="invoice-info-label">套件编号：</span>
			<span class="red">${set.setNo }</span>
			<br />
			<span class="invoice-info-label">创建时间：</span>
			<span class="blue">${fn:substring(set.createTime, 0, 19)}</span>
		</div>
		<!-- 打印按钮 -->
		<div class="widget-toolbar hidden-480">
			<a href="#" onclick="printDoc();" title="详情打印">
				<i class="ace-icon fa fa-print"></i>
			</a>
		</div>
	</div>
	<div class="widget-body" id="div2">
		<table style="width: 100%;font-size:16px;border-collapse:separate;border-spacing:2px 3px;">
			<tr>
				<td>
					<i class="ace-icon fa fa-caret-right blue"></i>
					套件名称：
					<b class="black">${set.setName }</b>
				</td>
				<td>
					<i class="ace-icon fa fa-caret-right blue"></i>
					套件编号：
					<b class="black">${set.setNo }</b>
				</td>
			</tr>
			<tr>
				<td>
					<i class="ace-icon fa fa-caret-right blue"></i>
					创建人：
					<b class="black">${set.userName }</b>
				</td>
				<td>
					<i class="ace-icon fa fa-caret-right blue"></i>
					状态:
					<c:if test="${set.status==0 }"><span style="color:#d15b47;font-weight:bold;">失效</span></c:if>
					<c:if test="${set.status==1 }"><span style="color:#87b87f;font-weight:bold;">生效</span></c:if>
				</td>
			</tr>
		</table>
	</div>
	<div id="grid-div-c">
		<table id="grid-table-c" style="width:100%;height:100%;"></table>
		<div id="grid-pager-c"></div>
	</div>
</div>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriData;      //表格数据
    var _grid;        //表格对象
    $(function(){
        //初始化表格
        _grid = jQuery("#grid-table-c").jqGrid({
            url: context_path + "/setConfig/detailList.do?pId="+$("#id").val(),
            datatype: "json",
            colNames : [ "详情主键","物料编号","物料名称","数量"],
            colModel : [ 
  					  {name : "id",index : "id",width : 20,hidden:true}, 
  					  {name : "materialNo",index:"materialNo",width :20}, 
                      {name : "materialName",index:"materialName",width : 20}, 
                      {name : "amount", index: "amount", width: 20}
            ],
            rowNum: 20,
            rowList: [10, 20, 30],
            pager : "#grid-pager-c",
            sortname : "sd.id",
            sortorder : "asc",
            altRows: true,
            viewrecords: true,
            autowidth: true,
            multiselect: false,
            multiboxonly: true,
            loadComplete: function (data) {
                var table = this;
                setTimeout(function () {
                    updatePagerIcons(table);
                    enableTooltips(table);
                }, 0);
                oriData = data;
                $(window).triggerHandler("resize.jqGrid");
            },
            emptyrecords: "没有相关记录",
            loadtext: "加载中...",
            pgtext: "页码 {0} / {1}页",
            recordtext: "显示 {0} - {1}共{2}条数据"
        });
        //在分页工具栏中添加按钮
        jQuery("#grid-table-c").navGrid("#grid-pager-c", {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false
        }).navButtonAdd("#grid-pager-c", {
            caption: "",
            buttonicon: "ace-icon fa fa-refresh green",
            onClickButton: function () {
                $("#grid-table-c").jqGrid("setGridParam",
                    {
                        postData: {pId: $("#id").val()} //发送数据  :选中的节点
                    }
                ).trigger("reloadGrid");
            }
        });
        $(window).on("resize.jqGrid", function () {
            $("#grid-table-c").jqGrid("setGridWidth", $("#grid-div-c").width() - 3);
            var height = $(".layui-layer-title",_grid.parents(".layui-layer")).height()+
                         $("#div1").outerHeight(true)+$("#div2").outerHeight(true)+
                         $("#gview_grid-table-c .ui-jqgrid-hbox").outerHeight(true)+
                         $("#grid-pager-c").outerHeight(true);
                         $("#grid-table-c").jqGrid("setGridHeight", _grid.parents(".layui-layer").height()-height);
        });
    });
    function formatterNumToFixed(value,options,rowObj){
        if(value!=null){
            if(rowObj.id==-1){
                return "";
            }else{
                var floatNum = parseFloat(value);
                return floatNum.toFixed(2);
            }
        }else{
            return "0.00";
        }
    }
    function printDoc(){
        var url = context_path + "/setConfig/toPrintDetail?setId="+$("#id").val();
        window.open(url);
    }
</script>