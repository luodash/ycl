<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"+ request.getServerName() + ":" + request.getServerPort()+ path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="grid-div">
	<form id="hiddenForm" action="<%=path%>/setConfig/toExcel.do" method="POST" style="display: none;">
		<input id="ids" name="ids" value="" />
	</form>
	<!-- 隐藏区域：存放查询条件 -->
	<form id="hiddenQueryForm" style="display:none;">
		<input id="setName" name="setName" value="" /> 
		<input id="setNo" name="setNo" value="" />
	</form>
	<div class="query_box" id="yy" title="查询选项">
		<form id="queryForm" style="max-width:100%;">
			<ul class="form-elements">
				<li class="field-group field-fluid2">
				    <label class="inline" for="setName" style="margin-right:20px;width: 100%;"> 
				        <span class="form_label" style="width:80px;">套件名称：</span> 
				        <input type="text" name="setName" id="setName" value="" style="width: calc(100% - 85px);" placeholder="套件名称" />
				    </label>
				</li>
				<li class="field-group field-fluid2">
				    <label class="inline" for="setNo" style="margin-right:20px;width: 100%;"> 
				        <span class="form_label" style="width:80px;">套件编号：</span> 
				        <input type="text" name="setNo" id="setNo" value="" style="width: calc(100% - 85px);" placeholder="套件编号" />
				    </label>
				</li>
			</ul>
			<div class="field-button">
				<div class="btn btn-info" onclick="queryOk();">
					<i class="ace-icon fa fa-check bigger-110"></i>查询
				</div>
				<div class="btn" onclick="reset();">
					<i class="ace-icon icon-remove"></i>重置
				</div>
			</div>
		</form>
	</div>
	<div id="fixed_tool_div" class="fixed_tool_div">
		<div id="__toolbar__" style="float:left;overflow:hidden;"></div>
	</div>
	<table id="grid-table" style="width:100%;margin: 0px !important;boder:0px !important"></table>
	<div id="grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
	 $(".date-picker").datetimepicker({format: "YYYY-MM-DD HH:mm:ss",useMinutes:true,useSeconds:true});
    var context_path = '<%=path%>';
    var oriData;
    var _grid;
    var openwindowtype;
    var dynamicDefalutValue="f1840fe8727340368e22b37e4311409f";
    $(function(){
        $(".toggle_tools").click();
    });
    $("#__toolbar__").iToolBar({
        id: "__tb__01",
        items : [
            {label: "添加", disabled: (${sessionUser.addQx} == 1 ? false : true), onclick: addSet, iconClass:'glyphicon glyphicon-plus'},
            {label: "编辑", disabled: (${sessionUser.editQx} == 1 ? false : true), onclick: editSet, iconClass:'glyphicon glyphicon-pencil'},
            {label: "删除", disabled: (${sessionUser.deleteQx} == 1 ? false : true), onclick: delSet, iconClass:'glyphicon glyphicon-trash'},
            {label: "生效", onclick: enableSet, iconClass:'icon-ok'},
            {label: "失效", onclick: disableSet, iconClass:'icon-remove'},
            {label: "导出", disabled: ( ${sessionUser.queryQx}==1 ? false : true), onclick:function(){toExcel();},iconClass:' icon-share'}
           ]
    });
    $(function(){
        _grid = jQuery("#grid-table").jqGrid({
            url: context_path + "/setConfig/list.do",
            datatype: "json",
            colNames: ["主键", "套件名称","套件编号", "创建人","创建时间","状态","操作"],
            colModel: [
                {name: "id", index: "id", width: 20, hidden: true},
                {name: "setName", index: "setName", width: 60},
                {name: "setNo",index: "setNo",width:70},
                {name: "userName", index: "userName", width: 60},
                {name: "createTime", index: "createTime", width: 60},
                {name: "status", index: "status" ,width : 30,
                formatter:function(cellValue,option,rowObject){
                    if(typeof cellValue == "number"){
                        if(cellValue==0){
                            return "<span style='color:grey;font-weight:bold;'>失效</span>";
                        }else if(cellValue==1){
                            return "<span style='color:green;font-weight:bold;'>生效</span>";
                        }
                    }else{
                            return "未知";
                        }
                      }
                 },
                 {name:"operation",index:"operation",width:90,
	        			formatter: function (cellValu, option, rowObject) {
	                        return "<div style='margin-bottom:5px' class='btn btn-xs btn-success' onclick='viewDetail(" + rowObject.id + ")'>详情</div>"
	        			}
	        		}
            ],
            rowNum: 20,
            rowList: [10, 20, 30],
            pager: "#grid-pager",
            sortname: "sc.setNo",
            sortorder: "asc",
            altRows: true,
            viewrecords: true,
            autowidth: true,
            multiselect: true,
            multiboxonly: true,
            beforeRequest:function (){
                dynamicGetColumns(dynamicDefalutValue,"grid-table", $(window).width()-$("#sidebar").width() -7);
                //重新加载列属性
            },
            loadComplete: function (data) {
                var table = this;
                setTimeout(function () {
                    updatePagerIcons(table);
                    enableTooltips(table);
                }, 0);
                oriData = data;
            },
            emptyrecords: "没有相关记录",
            loadtext: "加载中...",
            pgtext: "页码 {0} / {1}页",
            recordtext: "显示 {0} - {1}共{2}条数据"
        });
        //在分页工具栏中添加按钮
        jQuery("#grid-table").navGrid("#grid-pager", {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false
        }).navButtonAdd("#grid-pager", {
                    caption: "",
                    buttonicon: "ace-icon fa fa-refresh green",
                    onClickButton: function () {
                        $("#grid-table").jqGrid("setGridParam",
                                {
                                    postData: {queryJsonString: ""} //发送数据
                                }
                        ).trigger("reloadGrid");
                    }
                }).navButtonAdd("#grid-pager",{
                caption: "",
                buttonicon:"ace-icon fa icon-cogs",
                onClickButton : function (){
                    jQuery("#grid-table").jqGrid("columnChooser",{
                        done: function(perm, cols){
                            dynamicColumns(cols,dynamicDefalutValue);
                            //cols页面获取隐藏的列,页面表格的值
                            $("#grid-table").jqGrid("setGridWidth", $("#grid-div").width() );
                        }
                    });
                }
            });
        $(window).on("resize.jqGrid", function () {
            $("#grid-table").jqGrid("setGridWidth", $("#grid-div").width() );
            $("#grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-$("#yy").outerHeight(true)-$("#fixed_tool_div").outerHeight(true)-$("#grid-pager").outerHeight(true)
           -$("#gview_grid-table .ui-jqgrid-hdiv").outerHeight(true));
        });

        $(window).triggerHandler("resize.jqGrid");
    });
    var _queryForm_data = iTsai.form.serialize($("#queryForm"));
	function queryOk(){
		var queryParam = iTsai.form.serialize($("#queryForm"));
		queryByParam(queryParam);		
	}
	function queryByParam(jsonParam) {
    iTsai.form.deserialize($("#hiddenQueryForm"), jsonParam);
    var queryParam = iTsai.form.serialize($("#hiddenQueryForm"));
    var queryJsonString = JSON.stringify(queryParam); 
    $("#grid-table").jqGrid("setGridParam",
        {
            postData: {queryJsonString: queryJsonString}
        }).trigger("reloadGrid");
    }
	function reset(){
	    $("#queryForm #setName").val("").trigger("change");
		$("#queryForm #setNo").val("").trigger("change");
	    $("#grid-table").jqGrid("setGridParam", {
            postData: {queryJsonString:""}
        }).trigger("reloadGrid");		
	}
	function addSet(){
	    openwindowtype=0;
		$.post(context_path + "/setConfig/toEdit.do", {}, function (str){
		$queryWindow=layer.open({
		    title : "台套添加", 
	    	type:1,
	    	skin : "layui-layer-molv",
	    	area : ["750px","600px"],
	    	shade : 0.6, //遮罩透明度
		    moveType : 1, //拖拽风格，0是默认，1是传统拖动
		    anim : 2,
		    content : str,
		    success: function (layero, index) {
                layer.closeAll("loading");
            }
		});
	 });
	}
	function editSet(){
	    var checkedNum = getGridCheckedNum("#grid-table", "id");
    if (checkedNum == 0) {
       layer.alert("请选择一个要编辑的记录！");
        return false;
    } else if (checkedNum > 1) {
    	layer.alert("只能选择一条记录进行编辑操作！");
        return false;
    } else {
        openwindowtype=1;
        var id = jQuery("#grid-table").jqGrid("getGridParam","selrow");
        $.post(context_path + "/setConfig/toEdit.do?id="+id, {}, function (str){
    		$queryWindow=layer.open({
    		    title : "台套编辑", 
    	    	type:1,
    	    	skin : "layui-layer-molv",
    	    	area : ["750px","600px"],
    	    	shade : 0.6, //遮罩透明度
    		    moveType : 1, //拖拽风格，0是默认，1是传统拖动
    		    anim : 2,
    		    content : str,
    		    success: function (layero, index) {
                    layer.closeAll("loading");
                }
    		});
    	});
      }
	}
	function delSet(){
	    var checkedNum = getGridCheckedNum("#grid-table", "id");  //选中的数量
        if (checkedNum == 0) {
            layer.alert("请选择至少一条要删除的记录！");
        } else {
            var ids = jQuery("#grid-table").jqGrid("getGridParam", "selarrrow");
            layer.confirm("确定删除选中的记录？", function() {
    		    $.ajax({
    			    type : "post",
    			    url : context_path + "/setConfig/delSet.do?ids=" + ids,
    			    dataType : "json",
    			    cache : false,
    			    success : function(data) {
    				    layer.closeAll();
    				    if (Boolean(data.result)) {
    					    layer.msg(data.msg, {icon: 1,time:1000});
    				    }else{
    					    layer.msg(data.msg, {icon: 7,time:2000});
    				    }
    				    $("#grid-table").jqGrid("setGridParam", {
                           postData: {queryJsonString:""}
                        }).trigger("reloadGrid");
    			    }
    		    });
    	    });
         }
	}
	function viewDetail(setId){
	   $.post(context_path + "/setConfig/toDetail.do?id="+setId, {}, function (str){
    		$queryWindow=layer.open({
    		    title : "台套化配置详情", 
    	    	type:1,
    	    	skin : "layui-layer-molv",
    	    	area : ["750px","600px"],
    	    	shade : 0.6, //遮罩透明度
    		    moveType : 1, //拖拽风格，0是默认，1是传统拖动
    		    anim : 2,
    		    content : str,
    		    success: function (layero, index) {
                    layer.closeAll("loading");
                }
    		});
    	});
	}
	function enableSet(){
	   var checkedNum = getGridCheckedNum("#grid-table", "id");  //选中的数量
       if (checkedNum == 0) {
           layer.alert("请选择至少一条要生效的记录！");
       } else {
        var ids = jQuery("#grid-table").jqGrid("getGridParam", "selarrrow");
        layer.confirm("确定生效选中的记录？", function() {
    		$.ajax({
    			type : "post",
    			url : context_path + "/setConfig/setStatus.do?status=1&ids="+ids,
    			dataType : "json",
    			cache : false,
    			success : function(data) {
    				layer.closeAll();
    				if (Boolean(data.result)) {
    					layer.msg("生效成功！", {icon: 1,time:1000});
    				}else{
    					layer.msg("生效失败！", {icon: 7,time:2000});
    				}
    				_grid.trigger("reloadGrid");  //重新加载表格
    			}
    		});
    	});
      }
	}
	function disableSet(){
	   var checkedNum = getGridCheckedNum("#grid-table", "id");  //选中的数量
    if (checkedNum == 0) {
        layer.alert("请选择至少一条要失效的记录！");
    } else {
        var ids = jQuery("#grid-table").jqGrid("getGridParam", "selarrrow");
        layer.confirm("确定失效选中的记录？", function() {
    		$.ajax({
    			type : "post",
    			url : context_path + "/setConfig/setStatus.do?status=0&ids="+ids,
    			dataType : "json",
    			cache : false,
    			success : function(data) {
    				layer.closeAll();
    				if (Boolean(data.result)) {
    					layer.msg("失效成功！", {icon: 1,time:1000});
    				}else{
    					layer.msg("失效失败！", {icon: 7,time:2000});
    				}
    				_grid.trigger("reloadGrid");  //重新加载表格
    			}
    		});
    	});
     }
	}
    function toExcel(){
        var ids = jQuery("#grid-table").jqGrid("getGridParam", "selarrrow");
        $("#hiddenForm #ids").val(ids);
        $("#hiddenForm").submit();	
    }
</script>