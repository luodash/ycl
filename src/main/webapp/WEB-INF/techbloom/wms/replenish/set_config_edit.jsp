<%@ page language="java" import="java.lang.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<div class="row-fluid" style="height: inherit;margin:0px;border: 0px">
	<form id="setForm" class="form-horizontal" target="_ifr" style="margin-right:10px;">
		<input type="hidden" id="id" name="id" value="${set.id}"> 
		<input type="hidden" id="setNo" name="setNo" value="${set.setNo}">
		<input type="hidden" id="createTime" name="createTime" value="${set.createTime}">		
		<input type="hidden" id="status" name="status" value="${set.status}">
		<div class="row" style="margin:0;padding:0;">	
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="setName">套件名称：</label>
				<div class="controls">
					<div class="input-append span12 required">
						<input type="text" class="span11" id="setName" name="setName" placeholder="套件名称" value="${set.setName}" />
					</div>
				</div>
			</div>
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="userId">创建人：</label>
				<div class="controls">
					<div class="span12 required" style=" float: none !important;">
						<input type="text" class="span11" id="userId" name="userId" placeholder="创建人" />
					</div>
				</div>
			</div>
		</div>
		<div style="margin-left:10px;">
			<span class="btn btn-info" id="formSave" onclick="saveForm();">
				<i class="ace-icon fa fa-check bigger-110"></i>保存
			</span>
		</div>
	</form>
	<div id="materialDiv" style="margin:10px;">
		<!-- 下拉框 -->
		<label class="inline" for="materialInfor">物料：</label> 
		    <input type="text" id="materialInfor" name="materialInfor" style="width:350px;margin-right:10px;" />
		<button id="addMaterialBtn" class="btn btn-xs btn-primary" onclick="addDetail();">
			<i class="icon-plus" style="margin-right:6px;"></i>添加
		</button>
	</div>
	<div id="grid-div-c" style="width:100%;margin:0px auto;">
		<div id="fixed_tool_div" class="fixed_tool_div detailToolBar">
			<div id="__toolbar__-c" style="float:left;overflow:hidden;"></div>
		</div>
		<table id="grid-table-c" style="width:100%;height:100%;"></table>
		<div id="grid-pager-c"></div>
	</div>
</div>
<script type="text/javascript">
var context_path = '<%=path%>';
var oriDataDetail;
var _grid_detail;
var lastsel2;
var dynamicDefalutValue="3163dc31b46f456d8f17d811d7f7f972";
var pdd=$("#outForm #packageId").val();
$(".date-picker").datetimepicker({format: "YYYY-MM-DD HH:mm:ss",useMinutes:true,useSeconds:true});
$("#setForm").validate({
   ignore: "", 
   rules:{
      "setName":{
         required:true
      },
      "userId":{
         required:true
      }
   },
   messages: {
      "setName":{
         required:"请输入套件名称！"
      },
      "userId":{
         required:"请选择操作人！"
      }   
   },
    errorClass: "help-inline",
    errorElement: "span",
    highlight:function(element, errorClass, validClass) {
        $(element).parents('.control-group').addClass('error');
    },
    unhighlight: function(element, errorClass, validClass) {
        $(element).parents('.control-group').removeClass('error');
    }
});

function saveForm() {
    var bean = $("#setForm").serialize();
    if ($("#setForm").valid()) {
        saveSet(bean);
    }     
}
function saveSet(bean) {
        $.ajax({
			url : context_path + "/setConfig/saveSet.do",
			type : "POST",
			data : bean,
			dataType : "JSON",
			success : function(data) {
				if (Boolean(data.result)) {
					layer.msg("保存表头成功！", {icon : 1});
					if($("#setForm #id").val()==""){
					   $("#setForm #id").val(data.setConfig.id);
					   $("#setForm #setNo").val(data.setConfig.setNo);
					   $("#setForm #status").val(data.setConfig.status);
					}
					$("#grid-table").jqGrid("setGridParam", {
                       postData: {queryJsonString:""}
                    }).trigger("reloadGrid");
				} else {
					layer.alert(data.msg, {icon : 2});
				}
			},
			error : function(XMLHttpRequest) {
				alert(XMLHttpRequest.readyState);
				alert("出错啦！！！");
			}
		});
    }
/* 保存、修改 */
function saveOrUpdate(bean){
   $.ajax({
   url:context_path+"/salemanage/save",
   type:"post",
   data:bean,
   dataType:"JSON",
   success:function (data){
      if(data.result){
      $("#saleId").val(data.saleId);
      $("#outForm #state").val("0");
      saleId=data.saleId;
      gridReload();
      layer.msg("操作成功！",{icon:1,time:1200});
      }else{
      layer.msg("操作失败！",{icon:2,time:1200});
      }
   }
   });

}
//单元格编辑成功后，回调的函数
	var editFunction = function eidtSuccess(XHR){
		 var data = eval("("+XHR.responseText+")"); 
		if(data["msg"]!=""){
			layer.alert(data["msg"]);
		}
		jQuery("#grid-table-c").jqGrid("setGridParam", 
				{
					postData: {
						id:$('#id').val(),
						queryJsonString:""
					} 
				}
		  ).trigger("reloadGrid");
	}; 
  	_grid_detail=jQuery("#grid-table-c").jqGrid({
         url : context_path + "/setConfig/detailList.do?pId="+$("#setForm #id").val(),
         datatype : "json",
         colNames : [ "详情主键","物料编号","物料名称","数量"],
         colModel : [ 
  					  {name : "id",index : "id",width : 20,hidden:true}, 
  					  {name : "materialNo",index:"materialNo",width :20}, 
                      {name : "materialName",index:"materialName",width : 20}, 
                      {name : "amount", index: "amount", width: 20,editable : true,editrules: {custom: true, custom_func: numberRegex},
                        editoptions: {
	                          size: 25,
	                          dataEvents: [
	                              {
	                                  type: "blur",     //blur,focus,change.............
	                                  fn: function (e) {
	                                	  var $element = e.currentTarget;
	                                	  var $elementId = $element.id;
	                                	  var rowid = $elementId.split("_")[0];
	                                	  var id=$element.parentElement.parentElement.children[1].textContent;
	                                	  var indocType = 1;
	                                	  var reg = new RegExp("^[0-9]+(.[0-9]{1,2})?$");
	                                	  if (!reg.test($("#"+$elementId).val())) {
	                                		  layer.alert("请输入正实数！(注：小数点后将舍去)");
	                                		  return;
	                                	   }
	                                	  $.ajax({
	                                		  url:context_path + "/setConfig/setAmount",
	                                		  type:"POST",
	                                		  data:{id:id,amount:$("#"+rowid+"_amount").val(),setId:$("#setForm #id").val()},
	                                		  dataType:"json",
	                                		  success:function(data){
	                                		        if(Boolean(data.result)){
	                                		           $("#grid-table-c").jqGrid("setGridParam", 
                                							{
                                					    		url : context_path + "/setConfig/detailList.do?pId="+$("#setForm #id").val(),
                                								postData: {queryJsonString:""} //发送数据  :选中的节点
                                							}
                                					  ).trigger("reloadGrid");
                                					  layer.msg("保存数量成功！");
	                                		        }else{
	                                		        layer.alert(data.msg);
	                                		        }	                                		                     		      
	                                		  }
	                                	  }); 
	                                  }
	                              }
	                          ]
	                      }
                      }
                ],
         rowNum : 20,
         rowList : [ 10, 20, 30 ],
         pager : "#grid-pager-c",
         sortname : "sd.id",
         sortorder : "asc",
         altRows: true,
         viewrecords : true,
         autowidth:true,
         multiselect:true,
		 multiboxonly: true,
         beforeRequest:function (){
            dynamicGetColumns(dynamicDefalutValue,"grid-table-c", $(window).width()-$("#sidebar").width() -7);
            //重新加载列属性
         },
         loadComplete : function(data){
             var table = this;
             setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
             oriDataDetail = data;
             $(window).triggerHandler("resize.jqGrid");
         },
	   cellEdit: true,
	   cellsubmit : "clientArray",
  	   emptyrecords: "没有相关记录",
  	   loadtext: "加载中...",
  	   pgtext : "页码 {0} / {1}页",
  	   recordtext: "显示 {0} - {1}共{2}条数据",
    });
    //在分页工具栏中添加按钮
    $("#grid-table-c").navGrid("#grid-pager-c",{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#grid-pager-c",{  
  	   caption:"",   
  	   buttonicon:"ace-icon fa fa-refresh green",   
  	   onClickButton: function(){   
  	    $("#grid-table-c").jqGrid("setGridParam", 
				{
  	    			url:context_path + "/salemanage/detailList?saleId="+saleId,
					postData: {queryJsonString:""} //发送数据  :选中的节点
				}
		  ).trigger("reloadGrid");
  	   }
  	}).navButtonAdd("#grid-pager-c",{
            caption: "",
            buttonicon:"ace-icon fa icon-cogs",
            onClickButton : function (){
                jQuery("#grid-table-c").jqGrid("columnChooser",{
                    done: function(perm, cols){
                        dynamicColumns(cols,dynamicDefalutValue);
                        //cols页面获取隐藏的列,页面表格的值
                        $("#grid-table-c").jqGrid("setGridWidth", $("#grid-div-c").width()-3 );
                    }
                });
            }
        });
  	
  	$(window).on("resize.jqGrid", function () {
  		$("#grid-table-c").jqGrid("setGridWidth", $("#grid-div-c").width());
  		var height = $(".layui-layer-title",_grid_detail.parents(".layui-layer")).height()+
                     $("#planForm").outerHeight(true)+$("#materialDiv").outerHeight(true)+
                     $("#grid-pager-c").outerHeight(true)+$("#fixed_tool_div.fixed_tool_div.detailToolBar").outerHeight(true)+
                     $("#gview_grid-table-c .ui-jqgrid-hdiv").outerHeight(true);
                     $("#grid-table-c").jqGrid("setGridHeight",_grid_detail.parents(".layui-layer").height()-height-3);
  	});
  	$(window).triggerHandler("resize.jqGrid"); 
//将数据格式化成两位小数：四舍五入
 function formatterNumToFixed(value,options,rowObj){
	if(value!=null){
		var floatNum = parseFloat(value);
		return floatNum.toFixed(2);
	}else{
		return "0.00";
	}
 } 
 //数量输入验证
function numberRegex(value, colname) {
    var regex = /^\d+\.?\d{0,2}$/;
    reloadDetailTableList();
    if (!regex.test(value)) {
        return [false, ""];
    }
    else  return [true, ""];
}
$("#materialInfor").on("change",function(e){
	var datas=$("#materialInfor").select2("val");
	selectData = datas;
	$("#materialInfor").select2("search",selectParam);
});
 //清空物料多选框中的值
 function removeChoice(){
	  $("#s2id_materialInfor .select2-choices").children(".select2-search-choice").remove();
	  $("#materialInfor").select2("val","");
	  selectData = 0;
	  
 }
//添加物料详情
 function addDetail(){
	  if($("#setForm #id").val()==""){
	      $("#materialInfor").select2("val","");
		  layer.alert("请先保存表单信息！");
		  return;
	  }
	     if(selectData!=""){
		  //将选中的物料添加到数据库中
		  $.ajax({
			  type:"POST",
			  url:context_path + "/setConfig/saveDetail.do",
			  data:{pId:$("#setForm #id").val(),materialId:selectData.toString()},
			  dataType:"json",
			  success:function(data){
				  removeChoice();   //清空下拉框中的值
				  if(Boolean(data.result)){
					  layer.msg("添加成功",{icon:1,time:1200});
					  //重新加载详情表格
					  reloadDetailTableList();
					  //$("#outForm #hasDetail").val("yes");
				  }else{
					  layer.msg(data.msg,{icon:2,time:1200});
				  }
			  }
		  });
	  }else{
		  layer.alert("请选择物料！");
	  } 
  }
  //工具栏
	  $("#__toolbar__-c").iToolBar({
	   	 id:"__tb__01",
	   	 items:[
	   	  	{label:"删除", onclick:delDetail},
	    ]
	  });
	//删除物料详情
	function delDetail(){
	   var checkedNum = getGridCheckedNum("#grid-table-c", "id");
	   if(checkedNum==0){
	      layer.alert("请先选择要删除的详情！");
	      return;
	   }
	       var ids = jQuery("#grid-table-c").jqGrid("getGridParam", "selarrrow");
	   $.ajax({
	      url:context_path + "/setConfig/delDetail.do?ids="+ids,
	      type:"POST",
	      dataType:"JSON",
	      success:function(data){
	         if(Boolean(data.result)){
	           layer.msg("操作成功！");
			   reloadDetailTableList();
			   //$("#outForm #hasDetail").val(data.hasDetail);
	         }else{
	           layer.msg(data.msg);
	         }
	      }
	   });  
	}
	 function reloadDetailTableList(){
          $("#grid-table-c").jqGrid("setGridParam", 
			    {
  	    			url:context_path + "/setConfig/detailList.do",
					postData: {pId:$("#setForm #id").val()} //发送数据  :选中的节点
				}).trigger("reloadGrid");
     }
   $("#materialInfor").select2({
        placeholder: "选择物料",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        width: 300,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/material/getMaterialSelectList",
            type: "POST",
            dataType: "json",
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });  
   $("#setForm #userId").select2({
        placeholder: "选择操作人",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        width: 200,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/setConfig/getUserSelect",
            type: "POST",
            dataType: "json",
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });
    $.ajax({
	 type:"POST",
	 url:context_path + "/setConfig/findSetById",
	 data:{id:$("#setForm #id").val()},
	 dataType:"json",
	 success:function(data){
			  $("#setForm #userId").select2("data", {
 			  id: data.userId,
 			  text: data.userName
             });
		}
	});
	function publish(){
	if($("#outForm #id").val()==""){
	   layer.alert("请先保存表头信息！");
	   return;
	}
	if($("#outForm #state").val()=="0"||$("#outForm #state").val()==""){
	   layer.confirm("确定提交,提交之后数据不能修改", function () {
	       $.ajax({
	   type:"post",
	   url:context_path+"/packageOut/publish.do?outId="+$("#outForm #id").val()+"&packageId="+$("#outForm #packageId").val(),
	   dataType:"json",
	   success:function(data){
	       if(Boolean(data.result)){
	            layer.msg(data.msg,{icon:1});
	            $("#grid-table").jqGrid("setGridParam", {
                    postData: {queryJsonString:""}
                }).trigger("reloadGrid");
                $("#outForm #state").val("1");	       
	       }else{
	            layer.msg(data.msg,{icon:2});
	       }	   
	      }	
	    });   
	 });	   
	}else{
	   layer.alert("单据已提交，请勿重复提交！");
            return;
	}
}
</script>