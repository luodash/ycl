<%@ page language="java" import="java.lang.*"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
	String path = request.getContextPath();
%>
<div class="main-content" style="height:100%;">
	<div class="widget-header widget-header-large" id="div1">
		<input type="hidden" id="id" name="id" value="${pull.id }" />
		<h3 class="widget-title grey lighter" style=' background: none; border-bottom: none; '>
			<i class="ace-icon fa fa-leaf green"></i>
			配送单
		</h3>
		<div class="widget-toolbar no-border invoice-info">
			<span class="invoice-info-label">配送单单编号：</span>
			<span class="red">${pull.taskNo }</span>
			<br />
			<span class="invoice-info-label">质检时间：</span>
			<span class="blue">${fn:substring(pull.startTime, 0, 19)}</span>
		</div>
		<!-- 打印按钮 -->
		<div class="widget-toolbar hidden-480">
			<a href="#" onclick="toPrint();" title="详情打印">
				<i class="ace-icon fa fa-print"></i>
			</a>
		</div>
	</div>
	<div class="widget-body" id="div2">
		<table style="width: 100%;font-size:16px;border-collapse:separate;border-spacing:2px 3px;">
			<tr>
				<td>
					<i class="ace-icon fa fa-caret-right blue"></i>
					任务单号：
					<b class="black">${pull.taskNo }</b>
				</td>
				<td>
					<i class="ace-icon fa fa-caret-right blue"></i>
					载具编号：
					<b class="black">${pull.carNo }</b>
				</td>
			</tr>
			<tr>
				<td>
					<i class="ace-icon fa fa-caret-right blue"></i>
					配送人：
					<b class="black">${pull.userName }</b>
				</td>
				<td>
					<i class="ace-icon fa fa-caret-right blue"></i>
					状态：
					<c:if test="${pull.state==0 }"><span style="color:grey;font-weight:bold;">未质检</span></c:if>
					<c:if test="${pull.state==1 }"><span style="color:brown;font-weight:bold;">待配送</span></c:if>
					<c:if test="${pull.state==2 }"><span style="color:blue;font-weight:bold;">配送中</span></c:if>
					<c:if test="${pull.state==3 }"><span style="color:green;font-weight:bold;">配送完成</span></c:if>
				</td>
			</tr>
		</table>
	</div>
	<!-- 详情表格 -->
	<div id="grid-div-c">
		<!-- 物料信息表格 -->
		<table id="grid-table-c" style="width:100%;height:100%;"></table>
		<!-- 表格分页栏 -->
		<div id="grid-pager-c"></div>
	</div>
</div>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriData;      //表格数据
    var _grid;        //表格对象
    var getbillsStatus = '${GETBILLS.getbillsStatus}';   //下架单状态
    $(function() {
        //初始化表格
        _grid = jQuery("#grid-table-c").jqGrid({
            url : context_path + "/setPull/detailList.do?pId="+$("#id").val(),
            datatype : "json",
            colNames : [ "详情主键","套件编号","套件名称","数量","配送工位"],
            colModel : [
                {name : "id",index : "id",width : 55,hidden:true},
                {name : "suiteNo",index:"suiteNo",width : 50},
                {name : "suiteName",index:"suiteName",width : 50},
                {name : "amount",index:"amount",width : 50},
                {name : "routeName",index:"routeName",width : 50}
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : "#grid-pager-c",
        sortname : "dd.id",
        sortorder : "desc",
        altRows: true,
        viewrecords: true,
        autowidth: true,
        multiselect: false,
        multiboxonly: true,
        loadComplete: function (data) {
            var table = this;
            setTimeout(function () {
               updatePagerIcons(table);
               enableTooltips(table);
            }, 0);
            oriData = data;
            $(window).triggerHandler("resize.jqGrid");
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext: "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
    });
        //在分页工具栏中添加按钮
        jQuery("#grid-table-c").navGrid("#grid-pager-c", {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false
        }).navButtonAdd("#grid-pager-c", {
            caption: "",
            buttonicon: "ace-icon fa fa-refresh green",
            onClickButton: function () {
                $("#grid-table-c").jqGrid("setGridParam",
                    {
                        postData: {PUTBILLSID: $("#id").val()} //发送数据  :选中的节点
                    }
                ).trigger("reloadGrid");
            }
        });

        $(window).on("resize.jqGrid", function () {
            $("#grid-table-c").jqGrid("setGridWidth", $("#grid-div-c").width() - 3);
            var height = $(".layui-layer-title",_grid.parents(".layui-layer")).height()+
                         $("#div1").outerHeight(true)+$("#div2").outerHeight(true)+
                         $("#gview_grid-table-c .ui-jqgrid-hdiv").outerHeight(true)+
                         $("#grid-pager-c").outerHeight(true);
                         $("#grid-table-c").jqGrid("setGridHeight", _grid.parents(".layui-layer").height()-height);
        });
    });

    //将数据格式化成两位小数：四舍五入
    function formatterNumToFixed(value,options,rowObj){
        if(value!=null){
            if(rowObj.id==-1){
                return "";
            }else{
                var floatNum = parseFloat(value);
                return floatNum.toFixed(2);
            }
        }else{
            return "0.00";
        }
    }

    function toPrint(){
        var url = context_path + "/setPull/toPrint.do?id="+$("#id").val();
        window.open(url);
    }
</script>