﻿<%@page import="java.math.BigDecimal"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<%@ include file="/techbloom/common/taglibs.jsp"%>
<script type="text/javascript">
		function printA4(){
			window.print();
		}
	</script>
</head>
<body onload="printA4();">
	<c:forEach items="${transList }" var="mt" varStatus="mtlist" step="4">
		<div style="height:100%">
		<br />
		<br />
		<h3 align="center" style="font-weight: bold;">复合交接详情</h3>
		<table style="border-collapse: collapse; border: none;width: 16.2cm;font-size: 12px;" align="center">
			<tr>
				<td style="border: solid #000 1px;text-align:center;width:3cm;">物料编号</td>
				<td style="border: solid #000 1px;text-align:center;width:8.2cm;">物料名称</td>
				<td style="border: solid #000 1px;text-align:center;width:3cm;">数量</td>
				<td style="border: solid #000 1px;text-align:center;width:6cm;">可交接数量</td>			
			</tr>
			<c:forEach items="${transList }" var="mt" varStatus="mtlist" begin="${mtlist.index}" end="${mtlist.index+9}" step="1">			
				<tr>
					<td style="border: solid #000 1px;text-align:center;">${mt.materialNo}</td>
					<td style="border: solid #000 1px;text-align:center;">${mt.materialName}</td>
					<td style="border: solid #000 1px;text-align:center;">${mt.amount}</td>
					<td style="border: solid #000 1px;text-align:center;">${mt.maxAmount}</td>
				</tr>
			</c:forEach>			
		</table>		
		<br/>
		<br/>
		</div>
	</c:forEach>
</body>
</html>

