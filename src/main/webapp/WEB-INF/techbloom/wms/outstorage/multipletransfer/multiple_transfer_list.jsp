<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="multiple_transfer_list_grid-div">
    <form id="multiple_transfer_list_hiddenForm" action="<%=path%>/multipleTransfer/toExcel" method="POST" style="display: none;">
        <input id="multiple_transfer_list_ids" name="ids" value=""/>
    </form>
    <!-- 隐藏区域：存放查询条件 -->
    <form id="multiple_transfer_list_hiddenQueryForm" style="display:none;">
        <input id="multiple_transfer_list_transferNo" name="transferNo" value=""/>
        <input id="multiple_transfer_list_type" name="type" value="">
        <input id="multiple_transfer_list_outId" name="outId" value=""/>
        <input id="multiple_transfer_list_userId" name="userId" value="">
    </form>
     <div class="query_box" id="multiple_transfer_list_yy" title="查询选项">
            <form id="multiple_transfer_list_queryForm" style="max-width:100%;">
			 <ul class="form-elements">
				<li class="field-group field-fluid3">
					<label class="inline" for="multiple_transfer_list_transferNo" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:65px;">交接编号：</span>
						<input type="text" id="multiple_transfer_list_transferNo" name="transferNo" value="" style="width: calc(100% - 70px);" placeholder="交接编号">
					</label>			
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="multiple_transfer_list_type" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:65px;">单据类型：</span>
						<input type="text" id="multiple_transfer_list_type" name="type" value="" style="width: calc(100% - 70px);" placeholder="单据类型">
					</label>					
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="multiple_transfer_list_outId" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:65px;">出库单：</span>
						<input type="text" id="multiple_transfer_list_outId" name="outId" value="" style="width: calc(100% - 70px);" placeholder="出库单">
					</label>			
				</li>
				<li class="field-group-top field-group field-fluid3">
					<label class="inline" for="multiple_transfer_list_userId" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:65px;">交接人：</span>
						<input type="text" name="userId" id="multiple_transfer_list_userId" value="" style="width: calc(100% - 70px);" placeholder="交接人">
					</label>					
				</li>
				
			</ul>
			<div class="field-button" style="">
					<div class="btn btn-info" onclick="queryOk();">
				        <i class="ace-icon fa fa-check bigger-110"></i>查询
			        </div>
				    <div class="btn" onclick="reset();"><i class="ace-icon icon-remove"></i>重置</div>
				    <a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>
		        </div>
		  </form>		 
    </div>
    <div id="multiple_transfer_list_fixed_tool_div" class="fixed_tool_div">
        <div id="multiple_transfer_list___toolbar__" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="multiple_transfer_list_grid-table" style="width:100%;margin: 0px !important;boder:0px !important"></table>
    <div id="multiple_transfer_list_grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
var context_path = '<%=path%>';
var oriData;
var _grid;
var dynamicDefalutValue="f1136d9e8dd542dfbafe42819fdb5e08";
$(function  (){
    $(".toggle_tools").click();
});
$("#multiple_transfer_list___toolbar__").iToolBar({
    id: "multiple_transfer_list___tb__01",
    items: [
            {label: "添加", disabled: ( ${sessionUser.addQx } == 1 ? false : true), onclick:addTrans, iconClass:'glyphicon glyphicon-plus'},
            {label: "编辑", disabled: ( ${sessionUser.editQx} == 1 ? false : true),onclick: editTrans, iconClass:'glyphicon glyphicon-pencil'},
            {label: "删除", disabled: ( ${sessionUser.deleteQx} == 1 ? false : true),onclick: delTrans, iconClass:'glyphicon glyphicon-trash'},
            {label: "导出", disabled: ( ${sessionUser.queryQx}==1?false:true),onclick:function(){toExcel();},iconClass:' icon-share'}
    ]
});

$(function () {
    _grid = jQuery("#multiple_transfer_list_grid-table").jqGrid({
            url: context_path + "/multipleTransfer/list.do",
            datatype: "json",
            colNames: ["主键", "交接编号", "单据类型", "单据编号", "交接人", "备注","状态","操作"],
            colModel: [
                {name: "id", index: "id", hidden: true},
                {name: "transferNo", index: "transferNo", width: 60},
                {name: "typeName",index: "typeName",width:60},
                {name: "outNo",index: "outNo",width:60},
                {name: "userName", index: "userName", width: 60},
                {name: "remark", index: "remark", width: 60},
                {name: "status", index : "status", width : 30,formatter:function(cellValue,option,rowObject){
                    if(typeof cellValue == "number"){
                        if(cellValue==0){
                            return "<span style='color:#d15b47;font-weight:bold;'>未提交</span>";
                        }else if(cellValue==1){
                            return "<span style='color:#87b87f;font-weight:bold;'>已提交</span>";
                        }else{
                           return "未知";
                        }
                    }else{
                        return "未知";
                    }    
                   }
                 },
                {name:"opertion",index:"opertion",width:40,
	        	     formatter: function (cellValu, option, rowObject) {
	                     return "<div style='margin-bottom:5px' class='btn btn-xs btn-success' onclick='viewDetailList(" + rowObject.id + ")'>详情</div>"
	        	    }
	            }
            ],
            rowNum: 20,
            rowList: [10, 20, 30],
            pager: "#multiple_transfer_list_grid-pager",
            sortname: "transferNo",
            sortorder: "asc",
            altRows: true,
            viewrecords: true,
            caption: "复核交接",
            autowidth: true,
            multiselect: true,
            multiboxonly: true,
			beforeRequest:function (){
				dynamicGetColumns(dynamicDefalutValue,"multiple_transfer_list_grid-table",$(window).width()-$("#sidebar").width() -7);
				//重新加载列属性
			},
            loadComplete: function (data) {
                var table = this;
                setTimeout(function () {
                    updatePagerIcons(table);
                    enableTooltips(table);
                }, 0);
                oriData = data;
            },
            emptyrecords: "没有相关记录",
            loadtext: "加载中...",
            pgtext: "页码 {0} / {1}页",
            recordtext: "显示 {0} - {1}共{2}条数据"
        });
        //在分页工具栏中添加按钮
        jQuery("#multiple_transfer_list_grid-table").navGrid("#multiple_transfer_list_grid-pager", {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false
        }).navButtonAdd("#multiple_transfer_list_grid-pager", {
                    caption: "",
                    buttonicon: "ace-icon fa fa-refresh green",
                    onClickButton: function () {
                        $("#multiple_transfer_list_grid-table").jqGrid("setGridParam",
                                {
                                    postData: {queryJsonString: ""} //发送数据
                                }
                        ).trigger("reloadGrid");
                    }
                }).navButtonAdd("#multiple_transfer_list_grid-pager",{
                caption: "",
                buttonicon:"fa  icon-cogs",
                onClickButton : function (){
                    jQuery("#multiple_transfer_list_grid-table").jqGrid("columnChooser",{
                        done: function(perm, cols){
                            dynamicColumns(cols,dynamicDefalutValue);
                            $("#multiple_transfer_list_grid-table").jqGrid("setGridWidth", $("#multiple_transfer_list_grid-div").width());
                        }
                    });
                }
            });
        $(window).on("resize.jqGrid", function () {
            $("#multiple_transfer_list_grid-table").jqGrid("setGridWidth", $("#multiple_transfer_list_grid-div").width() );
            $("#multiple_transfer_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-
            $("#multiple_transfer_list_yy").outerHeight(true)-$("#multiple_transfer_list_fixed_tool_div").outerHeight(true)-
            $("#multiple_transfer_list_grid-pager").outerHeight(true)-$("#gview_multiple_transfer_list_grid-table .ui-jqgrid-titlebar").outerHeight(true)-
            $("#gview_multiple_transfer_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
        });

        $(window).triggerHandler("resize.jqGrid");
    });
    var _queryForm_data = iTsai.form.serialize($("#multiple_transfer_list_queryForm"));
	function queryOk(){
		var queryParam = iTsai.form.serialize($("#multiple_transfer_list_queryForm"));
		queryByParam(queryParam);		
	}
	function queryByParam(jsonParam) {
    iTsai.form.deserialize($("#multiple_transfer_list_hiddenQueryForm"), jsonParam);   //将json对象反序列化到列表页面中隐藏的form中
    var queryParam = iTsai.form.serialize($("#multiple_transfer_list_hiddenQueryForm"));
    var queryJsonString = JSON.stringify(queryParam);         //将json对象转换成json字符串
    //执行查询操作
    $("#multiple_transfer_list_grid-table").jqGrid("setGridParam",
        {
            postData: {queryJsonString: queryJsonString} //发送数据
        }
    ).trigger("reloadGrid");
}
	function reset(){
		$("#multiple_transfer_list_queryForm #multiple_transfer_list_type").select2("val","");
		$("#multiple_transfer_list_queryForm #multiple_transfer_list_outId").select2("val","");
		$("#multiple_transfer_list_queryForm #multiple_transfer_list_userId").select2("val","");
		iTsai.form.deserialize($("#multiple_transfer_list_queryForm"),_queryForm_data); 
		queryByParam(_queryForm_data);		
	}
function addTrans(){
$.post(context_path + "/multipleTransfer/toEdit.do", {}, function (str){
		$queryWindow=layer.open({
		    title : "复核交接添加", 
	    	type:1,
	    	skin : "layui-layer-molv",
	    	area : "750px",
	    	shade : 0.6, //遮罩透明度
		    moveType : 1, //拖拽风格，0是默认，1是传统拖动
		    anim : 2,
		    content : str,
		    success: function (layero, index) {
                layer.closeAll("loading");
            }
		});
	});
}
function editTrans(){
	var checkedNum = getGridCheckedNum("#multiple_transfer_list_grid-table", "id");
    if (checkedNum == 0) {
       layer.alert("请选择一个要编辑的单据！");
        return false;
    } else if (checkedNum > 1) {
    	layer.alert("只能选择一个单据进行编辑操作！");
        return false;
    } else {
        var pId = jQuery("#multiple_transfer_list_grid-table").jqGrid("getGridParam","selrow");
        var rowData = jQuery("#multiple_transfer_list_grid-table").jqGrid("getRowData",pId).status;
        if(rowData == '<span style="color:#d15b47;font-weight:bold;">未提交</span>'){
            $.post(context_path + "/multipleTransfer/toEdit.do?pId="+pId, {}, function (str){
    		$queryWindow=layer.open({
    		    title : "复核交接编辑", 
    	    	type:1,
    	    	skin : "layui-layer-molv",
    	    	area : "750px",
    	    	shade : 0.6, //遮罩透明度
    		    moveType : 1, //拖拽风格，0是默认，1是传统拖动
    		    anim : 2,
    		    content : str,
    		    success: function (layero, index) {
                    layer.closeAll("loading");
                }
    		});
    	});
       }else{      
           layer.alert("只能选择未提交的数据进行编辑");
       }       
    }
}
function delTrans(){
	var checkedNum = getGridCheckedNum("#multiple_transfer_list_grid-table", "id");  //选中的数量
    if (checkedNum == 0) {
        layer.alert("请选择一个要删除的单据！");
    } else {
        //从数据库中删除选中的物料，并刷新物料表格
        var ids = jQuery("#multiple_transfer_list_grid-table").jqGrid("getGridParam", "selarrrow");
        layer.confirm("确定删除选中的单据？", function() {
    		$.ajax({
    			type : "POST",
    			url : context_path + "/multipleTransfer/delTrans.do?ids="+ ids,
    			dataType : "json",
    			cache : false,
    			success : function(data) {
    				layer.closeAll();
    				if (Boolean(data.result)) {
    					layer.msg(data.msg, {icon: 1,time:1000});
    				}else{
    					layer.msg(data.msg, {icon: 7,time:2000});
    				}
    				_grid.trigger("reloadGrid");  //重新加载表格
    			}
    		});
    	});
    }
}
$("#multiple_transfer_list_queryForm #multiple_transfer_list_type").select2({
        placeholder: "选择单据类型",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        width: 200,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/multipleTransfer/getSelectType",
            type: "POST",
            dataType: "json",
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });
    $("#multiple_transfer_list_queryForm #multiple_transfer_list_outId").select2({
        placeholder: "选择单据编号",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        width: 200,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/multipleTransfer/getSelectOut",
            type: "POST",
            dataType: "json",
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });
    $("#multiple_transfer_list_queryForm #multiple_transfer_list_userId").select2({
        placeholder: "选择操作人",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        width: 200,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/multipleTransfer/getSelectUser",
            type: "POST",
            dataType: "json",
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });
function toExcel(){
    var ids = jQuery("#multiple_transfer_list_grid-table").jqGrid("getGridParam", "selarrrow");
    $("#multiple_transfer_list_hiddenForm #multiple_transfer_list_ids").val(ids);
    $("#multiple_transfer_list_hiddenForm").submit();	
}
function viewDetailList(id) {
	$.get(context_path + "/multipleTransfer/viewDetail.do?id=" + id).done(
	function(data) {
		layer.open({
			title : "复核交接查看",
			type : 1,
			skin : "layui-layer-molv",
			area : ["780px", "620px"],
			shade : 0.6, // 遮罩透明度
			moveType : 1, // 拖拽风格，0是默认，1是传统拖动
			anim : 2,
			content : data
		});
	});
}
</script>
</html>