﻿<%@page import="java.math.BigDecimal"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<%@ include file="../../../common/taglibs.jsp"%>
<script type="text/javascript">
		function printA4(){
			window.print();
		}
	</script>
</head>
<body onload="printA4();">
	<c:forEach items="${getbillsList }" var="gb" varStatus="gblist" step="10">
		<div style="height:100%">
		<br />
		<br />
		<h3 align="center" style="font-weight: bold;">库存信息</h3>
		<table style="border-collapse: collapse; border: none;width: 16.2cm;font-size: 12px;" align="center">
			<tr>
				<td style="border: solid #000 1px;text-align:center;width:3cm;">物料编号</td>
				<td style="border: solid #000 1px;text-align:center;width:8.2cm;">物料名称</td>
				<td style="border: solid #000 1px;text-align:center;width:2cm;">批次号</td>
				<td style="border: solid #000 1px;text-align:center;width:3cm;">物料单位</td>
				<td style="border: solid #000 1px;text-align:center;width:3cm;">数量</td>
				<td style="border: solid #000 1px;text-align:center;width:6cm;">单价</td>
				<td style="border: solid #000 1px;text-align:center;width:3cm;">下架货位</td>
				<td style="border: solid #000 1px;text-align:center;width:3cm;">状态</td>				
			</tr>
			<c:forEach items="${getbillsList }" var="gb" varStatus="gblist" begin="${gblist.index}" end="${gblist.index+9}" step="1">
			
				<tr>
					<td style="border: solid #000 1px;text-align:center;">${gb.materialNo}</td>
					<td style="border: solid #000 1px;text-align:center;">${gb.materialName}</td>
					<td style="border: solid #000 1px;text-align:center;">${gb.batchNo}</td>
					<td style="border: solid #000 1px;text-align:center;">${gb.materialUnit}</td>
					<td style="border: solid #000 1px;text-align:center;">${gb.amount}</td>
					<td style="border: solid #000 1px;text-align:center;">${gb.price}</td>
					<td style="border: solid #000 1px;text-align:center;">${gb.allocation}</td>
					<td style="border: solid #000 1px;text-align:center;">${gb.state}</td>
				</tr>
			</c:forEach>			
		</table>		
		<br/>
		<br/>
		</div>
	</c:forEach>
</body>
</html>

