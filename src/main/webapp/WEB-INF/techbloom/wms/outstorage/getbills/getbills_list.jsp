﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<div id="getbills_list_grid-div">
    <!-- 隐藏区域：存放查询条件 -->
    <form id="getbills_list_hiddenQueryForm" style="display:none;">
        <!-- 下架单编号 -->
        <input id="getbills_list_documentNo" name="documentNo" value=""/>
        <!-- 出库单编号 -->
        <input id="getbills_list_outstorageNo" name="outstorageNo" value=""/>
        <!-- 客户商-->
        <input id="getbills_list_customerId" name="customerId" value=""/>
        <!-- 库区-->
        <input id="getbills_list_warehouseId" name="warehouseId" value="">
    </form>
    <div class="query_box" id="getbills_list_yy" title="查询选项">
            <form id="getbills_list_queryForm" style="max-width:100%;">
			 <ul class="form-elements">
				<li class="field-group field-fluid3">
					<label class="inline" for="getbills_list_documentNo" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:80px;">下架单编号：</span>
						<input id="getbills_list_documentNo" name="documentNo" type="text" style="width: calc(100% - 85px);" placeholder="下架单编号"/>
					</label>			
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="getbills_list_outstorageNo" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:80px;">出库单编号：</span>
						<input id="getbills_list_outstorageNo" name="outstorageNo" type="text" style="width: calc(100% - 85px);" placeholder="出库单编号">
					</label>				
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="getbills_list_warehouseId" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:80px;">库区：</span>
						<input id="getbills_list_warehouseId" name="warehouseId" type="text" style="width: calc(100% - 85px);" placeholder="库区">
					</label>			
				</li>
				<li class="field-group-top field-group field-fluid3">
					<label class="inline" for="getbills_list_customerId" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:80px;">客户：</span>
						<input id="getbills_list_customerId" name="customerId" type="text" style="width: calc(100% - 85px);" placeholder="客户">
					</label>				
				</li>
				
			</ul>
			<div class="field-button" style="">
					<div class="btn btn-info" onclick="queryOk();">
				        <i class="ace-icon fa fa-check bigger-110"></i>查询
			        </div>
					<div class="btn" onclick="reset();"><i class="ace-icon icon-remove"></i>重置</div>
					<a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>
		        </div>
		  </form>		 
    </div>
    <!--    物料信息表格 -->
    <table id="getbills_list_grid-table" style="width:100%;height:100%;"></table>
    <!-- 	表格分页栏 -->
    <div id="getbills_list_grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/static/js/techbloom/wms/outstorage/getbills/getbills.js"></script>
<script type="text/javascript">
  $(".mySelect2").select2();
  var context_path = '<%=path%>';
  var oriData;      //表格数据
  var _grid;        //表格对象
  var dynamicDefalutValue="bf967f547da541909a2c2b900c95d093";//列表码
  $(function  (){
     $(".toggle_tools").click();
  });
  $(function () {
    //初始化表格：显示上架单
    _grid = jQuery("#getbills_list_grid-table").jqGrid({
        url: context_path + "/getbills/getbillsList",
        datatype: "json",
        colNames: ["主键", "下架单编号","出库单编号", "客户", "库区名称", "下架时间", "状态","操作"],
        colModel: [
            {name: "id", index: "id", width: 20, hidden: true},
            {name: "documentNo", index: "DOCUMENT_NO", width: 40},
            {name: "outstorageNo", index: "OUTSTORAGE_NO", width: 40},
            {name: "customerName", width: 60, formatter: function (cellValue, options, rowObject) {
                if (rowObject.outstorageType === "WMS_OUTSTORAGE_LLCK") {
                    //领料出库，没有客户
                    return "暂无";
                } else {
                    return cellValue==null?"":cellValue;
                }
            }
            },
            {name: "warehouseName", width: 80, 
            	formatter: function (cellValue, options, rowObject) {
                           return cellValue==null?"":cellValue;
                 }
            },
            {name : "getbillsDate",index:"GETBILLS_DATE",width : 70,formatter:function(cellValu,option,rowObject){
                	if(undefined==cellValu)return"";
                	return cellValu.substring(0,19);
                }
            },
            {name: "getbillsStatus",index:"getBILLS_STATUS", width: 30,
                formatter: function (cellValue, option, rowObject) {
                    if (typeof cellValue == 'number') {
       					if(cellValue==0){
                			return "<span style='color:gray;font-weight:bold;'>未下架</span>" ;
                		}if(cellValue==1){
                			return "<span style='color:orange;font-weight:bold;'>下架中</span>" ;
                		}
                		if(cellValue==2){
                            return "<span style='color:green;font-weight:bold;'>下架完成</span>" ;
                        }

                    } else {
                        return "异常";
                    }
                }
            },
            {name:"opertion",index:"opertion",width:60,
	        	  formatter: function (cellValu, option, rowObject) {
	                  return "<div style='margin-bottom:5px' class='btn btn-xs btn-success' onclick='viewDetailList(" + rowObject.id + ")'>详情</div>"
	        	}
	        }
        ],
        rowNum: 20,
        rowList: [10, 20, 30],
        pager: "#getbills_list_grid-pager",
        sortname: "gb.id",
        sortorder: "asc",
        altRows: true,
        viewrecords: true,
        autowidth: true,
        multiselect: true,
        multiboxonly: true,
        beforeRequest:function (){
            dynamicGetColumns(dynamicDefalutValue,"getbills_list_grid-table",$(window).width()-$("#sidebar").width() -7);
            //重新加载列属性
        },
        loadComplete: function (data) {
            var table = this;
            setTimeout(function () {
                updatePagerIcons(table);
                enableTooltips(table);
            }, 0);
            oriData = data;
            $(window).triggerHandler('resize.jqGrid');
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext: "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
    });
    //在分页工具栏中添加按钮
    jQuery("#getbills_list_grid-table").navGrid("#getbills_list_grid-pager", {
        edit: false,
        add: false,
        del: false,
        search: false,
        refresh: false
    }).navButtonAdd("#getbills_list_grid-pager", {
                caption: "",
                buttonicon: "ace-icon fa fa-refresh green",
                onClickButton: function () {
                    $("#getbills_list_grid-table").jqGrid("setGridParam",
                            {
                                postData: {queryJsonString: ""} //发送数据
                            }
                    ).trigger("reloadGrid");
                }
            }).navButtonAdd("#getbills_list_grid-pager",{
            caption: "",
            buttonicon:"fa  icon-cogs",
            onClickButton : function (){
                jQuery("#getbills_list_grid-table").jqGrid("columnChooser",{
                    done: function(perm, cols){
                        dynamicColumns(cols,dynamicDefalutValue);
                        $("#getbills_list_grid-table").jqGrid("setGridWidth", $("#getbills_list_grid-div").width()-3);
                    }
                });
            }
        });

    $(window).on("resize.jqGrid", function () {
        $("#getbills_list_grid-table").jqGrid("setGridWidth", $("#getbills_list_grid-div").width() - 3);
        var height = $("#header").outerHeight(true)+
            $(".query_box").outerHeight(true)+
            $("#getbills_list_grid-pager").outerHeight(true)+$("#getbills_list_fixed_tool_div").outerHeight(true)+
            $("#breadcrumb").outerHeight(true)+
            $("#gview_getbills_list_grid-table .ui-jqgrid-hbox").outerHeight(true);
            $("#getbills_list_grid-table").jqGrid("setGridHeight", (document.documentElement.clientHeight) - height);
    });
});
$("#getbills_list_queryForm #getbills_list_warehouseId").select2({
        placeholder: "请选择库区",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/area/getSelectArea",
            type: "POST",
            dataType: "json",
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });
    
    $("#getbills_list_queryForm #getbills_list_customerId").select2({
        placeholder: "选择客户",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/getbills/getSelectCusom",
            type: "POST",
            dataType: 'json',
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }

    });
</script>

