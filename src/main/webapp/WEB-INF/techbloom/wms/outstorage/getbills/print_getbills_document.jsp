﻿<%@page import="java.math.BigDecimal"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<script type="text/javascript">
		function printA4(){
			window.print();
		}
</script>
</head>
<body onload="printA4();">
	<c:forEach items="${detailList }" var="dd" varStatus="ddlist" step="10">
		<div style="height:100%">
		<br/>
		<br/>
		<h3 align="center" style="font-weight: bold;">下架单</h3>
		<!--table的最大高度为height:21cm;  -->
		<table style="width: 16.2cm;font-size: 12px;" align="center">
		  	<tr>
		  	    <td>下架单号：${doc.documentNo }</td>
		  	    <%-- <td>上架日期：${fn:substring(doc.putbillsDate, 0, 19)}</td> --%>
		  	    <td>仓&emsp;&emsp;库：${doc.warehouseName }</td>
		  	</tr>
		  	<tr>
		  	    <td>客户：${doc.customerName }</td>
		  	    <td>下架人：${doc.getterName }</td>
		  	</tr>
	  	</table>
		<table style="border-collapse: collapse; border: none;width: 16.2cm;font-size: 12px;" align="center">
			<tr>
				<td style="border: solid #000 1px;width:3cm;text-align:center;">物料编号</td>
				<td style="border: solid #000 1px;width:8.2cm;text-align:center;">物料名称</td>
				<td style="border: solid #000 1px;width:2cm;text-align:center;">批次号</td>
				<td style="border: solid #000 1px;width:3cm;text-align:center;">物料单位</td>
				<td style="border: solid #000 1px;width:3cm;text-align:center;">数量</td>
				<td style="border: solid #000 1px;width:3cm;text-align:center;">单价</td>
			</tr>
			<c:forEach items="${detailList }" var="d" varStatus="dlist"  begin="${ddlist.index}" end="${ddlist.index+9}"  step="1"> <%--varStatus="dlist"  begin="${ddlist.index}" end="${ddlist.index+3}"  step="1"--%>
				<tr >
					<td style="border: solid #000 1px;text-align:center;">${d.materialNo}</td>
					<td style="border: solid #000 1px;text-align:center;">${d.materialName}</td>
					<td style="border: solid #000 1px;text-align:center;">${d.batchNo}</td>
					<td style="border: solid #000 1px;text-align:center;">${d.materialUnit}</td>
					<td style="border: solid #000 1px;text-align:center;">${d.amount }</td>
					<td style="border: solid #000 1px;text-align:center;">
						<fmt:formatNumber type="number" value="${d.price}" pattern="0.00" maxFractionDigits="2"/>
					</td>
				</tr>
				
			</c:forEach>
			<tr >
				<td style="border: solid #000 1px;text-align:center;">页合计:</td>
				<td style="border: solid #000 1px;text-align:center;"></td>
				<td style="border: solid #000 1px;text-align:center;"></td>
				<td style="border: solid #000 1px;text-align:center;"></td>
				<td style="border: solid #000 1px;text-align:center;">
					<c:set var="amount" value="0"></c:set>
					<c:forEach items="${detailList }" var="d" varStatus="dlist" begin="${ddlist.index}" end="${ddlist.index+9}" step="1">
						<c:set var="amount" value="${amount+(d.amount==null?0.00:d.amount) }"></c:set>
					</c:forEach>
					${amount}
				</td>
				<td style="border: solid #000 1px;text-align:center;"></td>
		<br/>
		<br/>
	</div>
	</c:forEach> 
</body>
</html>

