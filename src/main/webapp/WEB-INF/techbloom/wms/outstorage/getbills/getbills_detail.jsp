<%@ page language="java" import="java.lang.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
	String path = request.getContextPath();
%>
<div class="main-content" style="height:100%">
	<div class="widget-header widget-header-large" id="getbills_detail_div1">
		<input type="hidden" id="getbills_detail_id" name="id" value="${GETBILLS.id }" /> 
		<input type="hidden" id="getbills_detail_rfid" name="rfid" value="${rfid }" />
		<h3 class="widget-title grey lighter" style="background: none; border-bottom: none;">
			<i class="ace-icon fa fa-leaf green"></i> 下架单
		</h3>
		<div class="widget-toolbar no-border invoice-info">
			<span class="invoice-info-label">下架单编号：</span> 
			<span class="red">${GETBILLS.documentNo }</span>
			<br /> 
			<span class="invoice-info-label">下架时间：</span> 
			<span class="blue">${fn:substring(GETBILLS.getbillsDate, 0, 19)}</span>
		</div>
		<div class="widget-toolbar hidden-480">
			<a href="#" onclick="printDoc();" title="详情打印"> 
			    <i class="ace-icon fa fa-print"></i>
			</a>
		</div>
	</div>
	<div class="widget-body" id="getbills_detail_div2">
		<table style="width: 100%;font-size:16px;border-collapse:separate;border-spacing:2px 3px;">
			<tr>
				<td>
				    <i class="ace-icon fa fa-caret-right blue"></i> 客户：
				    <b class="black">${GETBILLS.customerName }</b>
				</td>
				<td>
				    <i class="ace-icon fa fa-caret-right blue"></i> 库区： 
				    <b class="black">${GETBILLS.warehouseName }</b>
				</td>
			</tr>
			<tr>
				<td>
				    <i class="ace-icon fa fa-caret-right blue"></i> 下架人： 
				    <b class="black">${GETBILLS.getterName }</b>
				</td>
				<td>
				    <i class="ace-icon fa fa-caret-right blue"></i> 状态: 
				    <c:if test="${GETBILLS.getbillsStatus==0 }">
				       <span style="color:#d15b47;font-weight:bold;">未下架</span>
					</c:if>					
					<c:if test="${GETBILLS.getbillsStatus==1 }">
						<span style="color:#87b87f;font-weight:bold;">下架中</span>
					</c:if> 					
					<c:if test="${GETBILLS.getbillsStatus==2 }">
						<span style="color:#76b86b;font-weight:bold;">下架完成</span>
					</c:if>
				</td>
			</tr>
		</table>
	</div>
	<div id="getbills_detail_grid-div-c">
		<table id="getbills_detail_grid-table-c" style="width:100%;height:100%;"></table>
		<div id="getbills_detail_grid-pager-c"></div>
	</div>
</div>
<script type="text/javascript">
	var context_path = '<%=path%>';
    var oriData;      //表格数据
    var _grid;        //表格对象
    var getbillsStatus = '${GETBILLS.getbillsStatus}';   //下架单状态
    $(function() {
        //初始化表格
        _grid = jQuery("#getbills_detail_grid-table-c").jqGrid({
            url: context_path + "/getbills/getbillsDetailList?getId=" + $("#getbills_detail_id").val(),
            datatype: "json",
            colNames: ["物料主键", "库位ID", "物料编号", "物料名称", "条码", "物料单位", "预计数量", "单价", "下架库位","实际下架数量","状态", "操作"],
            colModel: [
                {name: "id", index: "id", width: 55, hidden: true},
                {name: "locationId", index: "locationId", width: 30, hidden: true},
                {name: "materialNo", width: 50},
                {name: "materialName", width: 50},
                {name: "batchNo", width: 60},
                {name: "materialUnit", width: 40},
                {name: "amount", width: 40},
                {name: "price", width: 30, formatter: formatterNumToFixed},
                {name: "allocation", width: 90},
                {name : "realAmount",index:"realAmount",width : 50,editable : true,editrules: {custom: true, custom_func: numberRegex},
                    editoptions: {
                        size: 25, dataEvents: [{
                            type: "blur",
                            fn: function (e) {
                                var $element = e.currentTarget;
                                var $elementId = $element.id;
                                var rowid = $elementId.split("_")[0];
                                var reg = new RegExp("^[0-9]+(.[0-9]{1,2})?$");
                                if (!reg.test($("#" + $elementId).val())) {
                                    layer.alert("非法的数量！(注：可以有两位小数的正实数)");
                                    return;
                                }
                                var id = $("#getbills_detail_grid-table-c").jqGrid('getGridParam', 'selrow');
                                var amount = $("#getbills_detail_grid-table-c").jqGrid('getRowData', id).amount;
                                $.ajax({
                                    type: "POST",
                                    dataType: "json",
                                    url: context_path + '/getbills/savaRealAmount',
                                    data: {realAmount: $("#" + rowid + "_realAmount").val(),id: id,amount:amount},
                                    success: function (data) {
                                        if(!data.result) layer.alert(data.msg);
                                        $("#getbills_detail_grid-table-c").jqGrid('setGridParam',
                                            {
                                                url:context_path + '/getbills/getbillsDetailList',
                                                postData: {getId:$('#getbills_detail_id').val(),queryJsonString:""}
                                            }
                                        ).trigger("reloadGrid");
                                    }
                                })
                            }
                        }]
                    }
                },

                {name: "state", index: "state", width: 40,
                    formatter: function (value, options, rowObject) {
                        if (typeof value == "number") {
                            if (value == 0) {
                                return "<span style='color:gray;font-weight:bold;'>未下架</span>";
                            } else if (value == 1) {
                                return "<span style='color:orange;font-weight:bold;'>下架中</span>";
                            } else if (value == 2) {
                                return "<span style='color:green;font-weight:bold;'>下架完成</span>";
                            }else if (value == 3) {
                                return "<span style='color:red;font-weight:bold;'>异常完成</span>";
                            }
                        } else {
                            return "";
                        }
                    }
                },
                {name: 'opertion', index: 'opertion', width: 60,
                    formatter: function (cellValu, option, rowObject) {
                        if (rowObject.id == -1) {
                            return "";
                        } else {
                            if ($("#getbills_detail_rfid").val() == 0) {			//表示启用RFID
                                if (rowObject.state == 0) {
                                    return "<button style='margin-bottom:5px'   class='btn btn-xs btn-success' onclick='offTheShelf(" + rowObject.id + "," + rowObject.locationId + "," + rowObject.realAmount + "," + rowObject.amount + "," + $('#getbills_detail_rfid').val() + ")'>下架</button>"
                                } else {
                                    return "<button style='margin-bottom:5px' disabled='false' class='btn btn-xs btn-success' onclick='offTheShelf(" + rowObject.id + "," + rowObject.locationId + ","+ rowObject.realAmount + "," + rowObject.amount + "," + $('#getbills_detail_rfid').val() + ")'>下架</button>"
                                }
                            }
                            if ($("#getbills_detail_rfid").val() == 1) {			//表示不启用RFID
                                if(rowObject.state==0){
                                    return "<button style='margin-bottom:5px' class='btn btn-xs btn-success' onclick='offTheShelf(" + rowObject.id + "," + rowObject.locationId + ","+ rowObject.realAmount + "," + rowObject.amount + "," + $('#getbills_detail_rfid').val() + ")'>确认下架</button>"
                                }else{
                                    return "<button style='margin-bottom:5px' disabled='false' class='btn btn-xs btn-success' onclick='offTheShelf(" + rowObject.id + "," + rowObject.locationId + ","+ rowObject.realAmount + "," + rowObject.amount + "," + $('#getbills_detail_rfid').val() + ")'>确认下架</button>"
                                }
                            }
                        }
                    }
                }
            ],
            rowNum: 20,
            rowList: [10, 20, 30],
            pager: '#getbills_detail_grid-pager-c',
            sortname: 'ID',
            sortorder: "asc",
            cellEdit: true, //单元格bian'ji
            altRows: true,
            viewrecords: true,
            autowidth: true,
            multiselect: false,
            multiboxonly: true,
            loadComplete: function (data) {
                var table = this;
                setTimeout(function () {
                    updatePagerIcons(table);
                    enableTooltips(table);
                }, 0);
                oriData = data;
                $(window).triggerHandler('resize.jqGrid');
            },
            emptyrecords: "没有相关记录",
            loadtext: "加载中...",
            pgtext: "页码 {0} / {1}页",
            recordtext: "显示 {0} - {1}共{2}条数据"
        });
        //在分页工具栏中添加按钮
        jQuery("#getbills_detail_grid-table-c").navGrid('#getbills_detail_grid-pager-c', {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false
        }).navButtonAdd("#getbills_detail_grid-pager-c", {
            caption: "",
            buttonicon: "ace-icon fa fa-refresh green",
            onClickButton: function () {
                $("#getbills_detail_grid-table-c").jqGrid("setGridParam",
                    {
                        postData: {PUTBILLSID: $('#id').val()} //发送数据  :选中的节点
                    }
                ).trigger("reloadGrid");
            }
        });
        $(window).on("resize.jqGrid", function () {
            $("#getbills_detail_grid-table-c").jqGrid("setGridWidth", $("#getbills_detail_grid-div-c").width() - 3);
            var height = $(".layui-layer-title",_grid.parents(".layui-layer")).height()+
                         $("#getbills_detail_div1").outerHeight(true)+$("#getbills_detail_div2").outerHeight(true)+
                         $("#gview_getbills_detail_grid-table-c .ui-jqgrid-hbox").outerHeight(true)+
                         $("#getbills_detail_grid-pager-c").outerHeight(true);
                         $("#getbills_detail_grid-table-c").jqGrid("setGridHeight", _grid.parents(".layui-layer").height()-height);
        });
    });
    //将数据格式化成两位小数：四舍五入
    function formatterNumToFixed(value,options,rowObj){
        if(value!=null){
            if(rowObj.id==-1){
                return "";
            }else{
                var floatNum = parseFloat(value);
                return floatNum.toFixed(2);
            }
        }else{
            return "0.00";
        }
    }

    function printDoc(){
        var url = context_path + "/getbills/printGetbillsDetail?putId="+$("#getbills_detail_id").val();
        window.open(url);
    }

    function offTheShelf(id,locationId,realAmount,amount,state){
    	if(realAmount == 0){
    	    layer.msg("请输入实际下架数量！");
            return;
    	}
		var a = amount - realAmount;
		  if(a>0){
			      layer.confirm('当前库位物料数缺少<font color="red">  '+ a +' </font>，是否跳过后下架？', {
					  btn: ['跳过'] //按钮
					}, function(){
						
						$.ajax({
				            type : "POST",
				            url : context_path + '/getbills/setState.do?id=' + id+'&locationId='+locationId+'&state='+state+'&amount='+realAmount,
				            dataType : 'json',
				            cache : false,
				            success : function(data) {
				                if (Boolean(data.result)) {
				                    layer.msg(data.msg, {icon: 1, time: 1000});
				                } else {
				                    layer.msg(data.msg, {icon: 7, time: 1000});
				                }
				                $("#getbills_detail_grid-table-c").jqGrid('setGridParam',
				                    {
				                        postData: {queryJsonString: ""} //发送数据
				                    }
				                ).trigger("reloadGrid");
				
				                $("#getbills_detail_grid-table").jqGrid('setGridParam',
				                    {
				                        postData: {queryJsonString: ""} //发送数据
				                    }
				                ).trigger("reloadGrid");
				            }
				        }); 
							
					});
		      }else if(realAmount > amount){
		      		layer.msg("实际下架数量超出！");
            		return;
		      }else{
					$.ajax({
			            type : "POST",
			            url : context_path + '/getbills/setState.do?id=' + id+'&locationId='+locationId+'&state='+state+'&amount='+amount,
			            dataType : 'json',
			            cache : false,
			            success : function(data) {
			                if (Boolean(data.result)) {
			                    layer.msg(data.msg, {icon: 1, time: 1000});
			                } else {
			                    layer.msg(data.msg, {icon: 7, time: 1000});
			                }
			                $("#getbills_detail_grid-table-c").jqGrid('setGridParam',
			                    {
			                        postData: {queryJsonString: ""} //发送数据
			                    }
			                ).trigger("reloadGrid");
			
			                $("#getbills_detail_grid-table").jqGrid('setGridParam',
			                    {
			                        postData: {queryJsonString: ""} //发送数据
			                    }
			                ).trigger("reloadGrid");
			            }
			        }); 
		      }	
    }   
    //数量输入验证
    function numberRegex(value, colname) {
        var regex = /^\d+\.?\d{0,2}$/;
        if (!regex.test(value)) {
            layer.alert("非法的数据,请输入整数");
            return [false, ""];
        }
        else  return [true, ""];
    }
</script>