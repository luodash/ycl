<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <%-- <%@ include file="/techbloom/common/taglibs.jsp"%> --%>
    <script type="text/javascript">
        var context_path = '<%=path%>';
    </script>
    <style type="text/css">
        .query_box .field-button.two {
            padding: 0px;
            left: 650px;
        }
    </style>
</head>
<body style="overflow:hidden;">
<div class="panel_tab">
    <div class="row-fluid">
        <!-- 表格数据 -->
        <table id="grid-table_outstorage" style="width:100%;"></table>
        <!-- 表格底部 -->
        <div id="grid-pager_outstorage"></div>
    </div>
</div>
</body>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriData;
    var _grid;

    _grid = jQuery("#grid-table_outstorage").jqGrid({
        url : context_path + '/outStorage/list',
        datatype : "json",
        colNames : [ '出库单编号','库区','出库时间','计划单据','状态'],
        colModel : [
            {name : 'documentNo',index : 'documentNo',width : 50},
            {name : 'areaName',index : 'areaName',width : 40},
            {name : 'outTime',index : 'outTime',width : 65,
                formatter:function(cellValu,option,rowObject){
                    if(cellValu!=null &&cellValu!="" && cellValu.length>19){
                        return cellValu.substring(0,19);
                    }else{
                        return cellValu==null?"":cellValu;
                    }
                }
            },
            {name : 'planCode',index:'planCode',width : 40},
            {name : 'status',index : 'status',width : 30,
                formatter:function(cellValue,option,rowObject){
                    if(typeof cellValue == 'number'){
                        if(cellValue==-1){
                            return "<span style='color:#d15b47;font-weight:bold;'>未提交</span>";
                        }else if(cellValue==1){
                            return "<span style='color:#87b87f;font-weight:bold;'>已提交</span>";
                        }else if(cellValue==2){
                            return "<span style='color:#76b86b;font-weight:bold;'>已审核</span>";
                        }else if(cellValue==3){
                            return "<span style='color:#d15b47;font-weight:bold;'>已驳回</span>";
                        }else if(cellValue==4){
                            return "<span style='color:#76b86b;font-weight:bold;'>出库完成</span>";
                        }else if(cellValue==0){
                            return "<span style='color:gray;font-weight:bold;'>已废弃</span>";
                        }
                    }else{
                        return "未知";
                    }
                }
            },
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : '#grid-pager_outstorage',
        sortname : 'os.id',
        sortorder : "desc",
        altRows: false,
        viewrecords : true,
        autowidth:true,
        multiselect:false,
        multiboxonly: true,
        loadComplete : function(data)
        {
            var table = this;
            setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            oriData = data;
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
    });
    //在分页工具栏中添加按钮
    $(window).on("resize.jqGrid", function () {
        $("#grid-table_outstorage").jqGrid("setGridWidth", $("#grid-table_outstorage").parents(".itemContent").width()-2 );
        $("#grid-table_outstorage").jqGrid( "setGridHeight", 300);
    })

    $(window).triggerHandler("resize.jqGrid");


</script>
</html>