﻿<%@ page language="java" import="java.lang.*"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
	String path = request.getContextPath();
%>
<div class="main-content" style="height:100%">
	<div class="widget-header widget-header-large" id="outstorage_detail_div1">
		<!-- 隐藏的asn主键 -->
		<input type="hidden" id="outstorage_detail_id" name="id" value="${OUTSTORAGE.id }">
		<input type="hidden" id="outstorage_detail_outPlanId" name="outPlanId" value="${OUTSTORAGE.outPlanId }">
		<h3 class="widget-title grey lighter" style=' background: none; border-bottom: none; '>
			<i class="ace-icon fa fa-leaf green"></i>
			出库单
		</h3>

		<!-- #section:pages/invoice.info -->
		<div class="widget-toolbar no-border invoice-info">
			<span class="invoice-info-label">出库单号:</span>
			<span class="red">${OUTSTORAGE.documentNo }</span>
			<br />
			<span class="invoice-info-label">下架日期:</span>
			<span class="blue">${fn:substring(OUTSTORAGE.outTime, 0, 19)}</span>
		</div>
		<!-- 打印按钮 -->
		<div class="widget-toolbar hidden-480">
			<a href="#" onclick="printDoc();" title="详情打印">
				<i class="ace-icon fa fa-print"></i>
			</a>
		</div>
	</div>

	<div class="widget-body" id="outstorage_detail_div2">
		<table style="width: 100%;font-size:16px;border-collapse:separate;border-spacing:2px 3px;">
			<tr>
				<td>
					<i class="ace-icon fa fa-caret-right blue"></i>
					客户：
					<b class="black">${OUTSTORAGE.customerName }</b>
				</td>
				<td>
					<i class="ace-icon fa fa-caret-right blue"></i>
					库区:
					<b class="black">${OUTSTORAGE.areaName }</b>
				</td>
			</tr>
			<tr>
			<tr>
				<td>
					<i class="ace-icon fa fa-caret-right blue"></i>
					出库类型:
					<b class="black">${OUTSTORAGE.outstorageType }</b>
				</td>
				<td>
					<i class="ace-icon fa fa-caret-right blue"></i>
					计划单据:
					<b class="black">${OUTSTORAGE.planCode }</b>
				</td>
			</tr>
			<tr>
				<td>
					<i class="ace-icon fa fa-caret-right blue"></i>
					状态:
					<c:if test="${OUTSTORAGE.status==-1 }"><span style="color:#d15b47;font-weight:bold;">未提交</span></c:if>
					<c:if test="${OUTSTORAGE.status==1 }"><span style="color:#87b87f;font-weight:bold;">已提交</span></c:if>
					<c:if test="${OUTSTORAGE.status==2 }"><span style="color:#76b86b;font-weight:bold;">已审核</span></c:if>
					<c:if test="${OUTSTORAGE.status==3 }"><span style="color:#d15b47;font-weight:bold;">已驳回</span></c:if>
					<c:if test="${OUTSTORAGE.status==4 }"><span style="color:#76b86b;font-weight:bold;">出库完成</span></c:if>
					<c:if test="${OUTSTORAGE.status==0 }"><span style="color:gray;font-weight:bold;">已废弃</span></c:if>
				</td>

			</tr>
			</tr>
		</table>
	</div>
	<!-- 详情表格 -->
	<div id="outstorage_detail_grid-div-view">
		<!-- 物料信息表格 -->
		<table id="outstorage_detail_grid-table-view" style="width:100%;height:100%;"></table>
		<!-- 表格分页栏 -->
		<div id="outstorage_detail_grid-pager-view"></div>
	</div>
</div>
<script type="text/javascript">
var context_path = '<%=path%>';
var oriData;      //表格数据
var _grid;        //表格对象
$(function(){
	//初始化表格
  	_grid = jQuery("#outstorage_detail_grid-table-view").jqGrid({
  		url : context_path+ "/outStorage/outStorageDetaillist?id="+$("#outstorage_detail_id").val()+"&outPlanId="+$("#outstorage_detail_outPlanId").val() ,
        datatype : "json",
        colNames : [ '出库主键','物料主键','详情主键', '物料编号', '物料名称','物料单位','可领数量', '数量', '单价','推荐策略','推荐批次号' ],
        onCellSelect : function(ts,ri,ci,tdHtml,e){
            if(ri !=10) {
                return;
            }
            var rowData =  $('#outstorage_detail_grid-table-detail').jqGrid('getRowData',ts);
            var routName=rowData.materialNo;
            $("#outstorage_detail_grid-table-detail").setColProp('batchNo',{editoptions:{value:getBatchNo(routName,$("#outstorage_detail_areaIdEdit").val())}});
        },
        colModel : [
            {name:'documentId',index:'documentId',width:20,hidden:true},
            {name:'materialId',index:'materialId',width:20,hidden:true},
            {name:'id',index:'id',width:20,hidden:true},
            {name:'materialNo',index:'materialName',width:20},
            {name:'materialName',index:'materialName',width:40},
            {name:'unit',index:'unit',width:20},
            {name:'avalibleAmount',index:'avalibleAmount',width:20},
            {name:'amount',index:'amount',width:20},
            {name:'price',index:'price',width:20},
            {name :'methodId',index:'methodId',width:20,hidden:true},
            {name:'batchNo',index:'batchNo',hidden:true,width:20}
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : '#outstorage_detail_grid-pager-view',
        sortname : 'osd.ID',
        sortorder : "asc",
        altRows: true,
        viewrecords : true,
        autowidth:true,
        multiselect:true,
        multiboxonly: true,
        loadComplete : function(data){
            var table = this;
            setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            oriData = data;
            $(window).triggerHandler('resize.jqGrid');
        },
        cellEdit: true,
        cellsubmit : "clientArray",
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
    });
    //在分页工具栏中添加按钮
    jQuery("#outstorage_detail_grid-table-view").navGrid('#outstorage_detail_grid-pager-view',{edit:false,add:false,del:false,search:false,refresh:false})
      .navButtonAdd('#outstorage_detail_grid-pager-view',{
  	   caption:"",
  	   buttonicon:"ace-icon fa fa-refresh green",
  	   onClickButton: function(){
  	    $("#outstorage_detail_grid-table-view").jqGrid('setGridParam',
				{
					postData: {id:$('#id').val()} //发送数据  :选中的节点
				}
		  ).trigger("reloadGrid");
  	   }
  	});

  	$(window).on("resize.jqGrid", function () {
  		$("#outstorage_detail_grid-table-view").jqGrid("setGridWidth", $("#outstorage_detail_grid-div-view").width() - 3 );
		var height = $(".layui-layer-title",_grid.parents(".layui-layer")).height()+
                     $("#outstorage_detail_div1").outerHeight(true)+$("#outstorage_detail_div2").outerHeight(true)+
                     $("#gview_outstorage_detail_grid-table-view .ui-jqgrid-hbox").outerHeight(true)+
                     $("#outstorage_detail_grid-pager-view").outerHeight(true);
  		             $("#outstorage_detail_grid-table-view").jqGrid("setGridHeight", _grid.parents(".layui-layer").height()-height );
  	});
});

//将数据格式化成两位小数：四舍五入
function formatterNumToFixed(value,options,rowObj){
	if(value!=null){
		if(rowObj.id==-1){
			return "";
		}else{
			var floatNum = parseFloat(value);
			return floatNum.toFixed(2);
		}		
	}else{
		return "0.00";
	}
}
function printDoc(){
    var url = context_path + "/getbills/printGetbillsDetail?putId="+$("#outstorage_detail_id").val();
	window.open(url);
}
</script>