<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<div id="outstorage_list_grid-div">
    <!-- 隐藏区域：存放查询条件 -->
    <form id="outstorage_list_hiddenQueryForm" action="<%=path%>/outStorage/exportExcel" method="POST" style="display:none;">
        <!-- 出库单编号 -->
        <input id="outstorage_list_documentNo" name="documentNo" value="" />
        <!-- 出库类型-->
        <input id="outstorage_list_outstorageType" name="outstorageType" value="" >
        <!-- 库区-->
        <input id="outstorage_list_areaId" name="areaId" value="">
        <!-- 出库时间 -->
        <input id="outstorage_list_outTime" name="outTime" value="" />
        <!-- 客户 -->
        <input id="outstorage_list_customerId" name="customerId" value="" />
    </form>
    <div class="query_box" id="outstorage_list_yy" title="查询选项">
            <form id="outstorage_list_queryForm" style="max-width:100%;">
			 <ul class="form-elements">
				<li class="field-group field-fluid3">
					<label class="inline" for="outstorage_list_documentNo" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:92px;">出库单编号：</span>
						<input type="text" name="documentNo" id="outstorage_list_documentNo" value="" style="width: calc(100% - 97px);" placeholder="出库单编号">
					</label>			
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="outstorage_list_outTime" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:92px;">出库时间：</span>
						<input class="form-control date-picker" id="outstorage_list_outTime" name="outTime" style="width: calc(100% - 97px);" type="text" value="${outStorage.outTime}"placeholder="出库时间"/>
                        <span class="input-group-addon"></span>
					</label>					
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="outstorage_list_outstorageType" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:92px;">出库类型：</span>
						<input type="hidden" id="outstorage_list_outstorageType" name="outstorageType" value="${outStorage.outstorageType }">
                        <select class="mySelect2" id="outstorage_list_outstorageTypeSelect" data-placeholder="请选择出库类型" style="width:calc(100% - 97px);">
                            <option value="">所有类型</option>
                            <c:forEach items="${inTypes}" var="in">
                                <option value="${in.dictNum}" <c:if test="${outStorage.outstorageType==in.dictNum }">selected="selected"</c:if>>
                                        ${in.dictValue}
                                </option>
                            </c:forEach>
                        </select>
					</label>			
				</li>
				<li class="field-group-top field-group field-fluid3">
					<label class="inline" for="outstorage_list_areaId" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:92px;">库区：</span>
						<input id="outstorage_list_areaId" name="areaId" style="width: calc(100% - 97px);" type="text" placeholder="请选择库区"/>
                        <span class="input-group-addon"></span>
					</label>					
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="outstorage_list_customerId" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:92px;">客户：</span>
						<input type="text" name="customerId" id="outstorage_list_customerId" value="" style="width: calc(100% - 97px);" placeholder="请选择客户">
					</label>					
				</li>
				
			</ul>
			<div class="field-button" style="">
					<div class="btn btn-info" onclick="queryOk();">
				        <i class="ace-icon fa fa-check bigger-110"></i>查询
			        </div>
				    <div class="btn" onclick="reset();"><i class="ace-icon icon-remove"></i>重置</div>
				    <a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>
		        </div>
		  </form>		 
    </div>
    <div id="outstorage_list_grid-div-c" style="width:100%;margin:0px auto;">
    <div id="outstorage_list_fixed_tool_div" class="fixed_tool_div detailToolBar">
        <div id="outstorage_list___toolbar__" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="outstorage_list_grid-table" style="width:100%;height:100%;"></table>
    <div id="outstorage_list_grid-pager"></div>
    </div>
</div>
<script type="text/javascript" src="<%=path%>/static/js/techbloom/wms/outstorage/outstoragemanage/outStorage.js"></script>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriData;
    var _grid;
    var dynamicDefalutValue="5faa1a80f76247489408d0331f7457a9";//列表码
    
    $(function  (){
        $(".toggle_tools").click();
    });
    
    $("#outstorage_list___toolbar__").iToolBar({
        id:"outstorage_list___tb__01",
        items:[
            {label:"添加",disabled:(${sessionUser.addQx}==1?false:true),onclick:addOutStorageOrder,iconClass:'icon-plus'},
            {label:"编辑",disabled:(${sessionUser.editQx}==1?false:true),onclick:editOutStorageOrder,iconClass:'icon-pencil'},
            {label:"删除",disabled:(${sessionUser.deleteQx}==1?false:true),onclick:delOutStorageOrder,iconClass:'icon-trash'},
            {label: "查看", disabled:(${sessionUser.queryQx} == 1 ? false : true),onclick:viewDetailList, iconClass:'icon-zoom-in'},
            {label:"导出",disabled:(${sessionUser.deleteQx}==1?false:true),onclick:exportOutStorage,iconClass:'icon-share'},
            {label:"打印",disabled:(${sessionUser.deleteQx}==1?false:true),onclick:printOutStorage,iconClass:'icon-print'},
            {label:"生成下架单",disabled:(${sessionUser.deleteQx}==1?false:true),onclick:creatGetBill,iconClass:'fa fa-arrow-down'}
        ]
    });

    _grid = jQuery("#outstorage_list_grid-table").jqGrid({
        url : context_path + '/outStorage/list',
        datatype : "json",
        colNames : [ '出库单主键','出库单编号','出库类型','客户','库区','出库时间','计划单据','备注','状态'],
        colModel : [
            {name : 'id',index : 'id',width : 20,hidden:true},
            {name : 'documentNo',index : 'documentNo',width : 40},
            {name : 'outstorageType',index : 'outstorageType',width : 40},
            {name : 'customerName',index : 'customerName',width :40},
            {name : 'areaName',index : 'areaName',width : 40},
            {name : 'outTime',index : 'outTime',width : 65,
                formatter:function(cellValu,option,rowObject){
                    if(cellValu!="" && cellValu!=null && cellValu.length>19){
                        return cellValu.substring(0,19);
                    }else{
                        return cellValu==null?"":cellValu;
                    }
                }
            },
            {name : 'planCode',index:'planCode',width : 40},
            {name : 'remarks',index : 'remarks',width : 50,
                formatter:function(cellValue,option,rowObject){
                    if(cellValue!=null && cellValue.length > 8){
                        return "<span title="+cellValue+">"+cellValue.substring(0,8)+"<strong>...</strong></span>";
                    }else{
                        return "<span title="+cellValue+">"+(cellValue==null?"":cellValue)+"</span>";
                    }
                }
            },
            {name : 'status',index : 'status',width : 30,
                formatter:function(cellValue,option,rowObject){
                    if(typeof cellValue == 'number'){
                        if(cellValue==-1){
                            return "<span style='color:#d15b47;font-weight:bold;'>未提交</span>";
                        }else if(cellValue==1){
                            return "<span style='color:#87b87f;font-weight:bold;'>已提交</span>";
                        }else if(cellValue==2){
                            return "<span style='color:#76b86b;font-weight:bold;'>已审核</span>";
                        }else if(cellValue==4){
                            return "<span style='color:#76b86b;font-weight:bold;'>出库完成</span>";
                        }else if(cellValue==0){
                            return "<span style='color:gray;font-weight:bold;'>已废弃</span>";
                        }
                    }else{
                        return "未知";
                    }
                }
            },
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : '#outstorage_list_grid-pager',
        sortname : 'os.id',
        sortorder : "desc",
        altRows: false,
        viewrecords : true,
        autowidth:true,
        multiselect:true,
        multiboxonly: true,
        beforeRequest:function (){
            dynamicGetColumns(dynamicDefalutValue,"outstorage_list_grid-table",$(window).width()-$("#sidebar").width() -7);
            //重新加载列属性
        },
        loadComplete : function(data){
            var table = this;
            setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            oriData = data;
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
    });
    //在分页工具栏中添加按钮
    jQuery("#outstorage_list_grid-table").navGrid('#outstorage_list_grid-pager',{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd('#outstorage_list_grid-pager',{
            caption:"",
            buttonicon:"ace-icon fa fa-refresh green",
            onClickButton: function(){
                $("#outstorage_list_grid-table").jqGrid('setGridParam',
                    {
                        postData: {queryJsonString:""} //发送数据
                    }
                ).trigger("reloadGrid");
            }
        }).navButtonAdd('#outstorage_list_grid-pager',{
            caption: "",
            buttonicon:"fa  icon-cogs",
            onClickButton : function (){
                jQuery("#outstorage_list_grid-table").jqGrid('columnChooser',{
                    done: function(perm, cols){
                        dynamicColumns(cols,dynamicDefalutValue);
                        $("#outstorage_list_grid-table").jqGrid("setGridWidth", $("#outstorage_list_grid-div").width()-3);
                    }
                });
            }
        });
    
    $(window).on("resize.jqGrid", function () {
        $("#outstorage_list_grid-table").jqGrid("setGridWidth", $("#outstorage_list_grid-div").width() - 3 );
        var height = $("#header").outerHeight(true)+$(".query_box").outerHeight(true)+$("#outstorage_list_grid-pager").outerHeight(true)+
        $("#outstorage_list_fixed_tool_div").outerHeight(true)+$("#breadcrumb").outerHeight(true)+$("#gview_outstorage_list_grid-table .ui-jqgrid-hbox").outerHeight(true);
        $("#outstorage_list_grid-table").jqGrid("setGridHeight", (document.documentElement.clientHeight - height));
    })

    $(window).triggerHandler('resize.jqGrid');
    
    var _queryForm_data = iTsai.form.serialize($('#outstorage_list_queryForm'));
    
    function queryOk(){
        var queryParam = iTsai.form.serialize($('#outstorage_list_queryForm'));
        //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
        queryInstoreListByParam(queryParam);
    }

    $('#outstorage_list_queryForm .mySelect2').select2();
    $('#outstorage_list_outstorageTypeSelect').change(function(){
        $('#outstorage_list_queryForm #outstorage_list_outstorageType').val($('#outstorage_list_outstorageTypeSelect').val());
    });

    $('#outstorage_list_areaSelect').change(function(){
        $('#outstorage_list_queryForm #outstorage_list_areaId').val($('#outstorage_list_areaSelect').val());
    });
    
	$("#outstorage_list_queryForm #outstorage_list_areaId").select2({
        placeholder: "请选择库区",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/outStorage/getSelectArea",
            type: "POST",
            dataType: "json",
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });
	
    $("#outstorage_list_queryForm #outstorage_list_customerId").select2({
        placeholder: "请选择库区",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/outStorage/getSelectCusom",
            type: "POST",
            dataType: "json",
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });	
    
    function reset(){
        iTsai.form.deserialize($('#outstorage_list_queryForm'),_queryForm_data);
        $("#outstorage_list_areaSelect").val("").trigger('change');
        $("#outstorage_list_outstorageTypeSelect").val("").trigger('change');
        $("#outstorage_list_queryForm #outstorage_list_customerId").select2("val","");
        $("#outstorage_list_queryForm #outstorage_list_areaId").select2("val","");
        //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
        queryInstoreListByParam(_queryForm_data);
    }
    
    $(".date-picker").datetimepicker({format: 'YYYY-MM-DD',useMinutes:true,useSeconds:true});
</script>