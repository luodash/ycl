<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tbl.modules.wms.entity.outstorage.OutStoragePlan"%>
<%@page import="com.tbl.modules.wms.entity.outstorage.OutStoragePlanDetail"%>
<%@page import="com.mysql.jdbc.StringUtils" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
    List<OutStoragePlan> instorageList =(List<OutStoragePlan>)request.getAttribute("list");
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">

    <title>出库计划单</title>

    <script type="text/javascript">
        function aa(){
            window.print();
        }
    </script>
</head>
<body onload="aa();">
<%
    double allAmount = 0;
    if(instorageList != null && instorageList.size()>0){
        for(int b = 0;b <instorageList.size(); b++){
%>
<br/>
<h3 align="center" style="font-weight: bolder;padding-top: 5px;">出库计划单</h3>
<table style="width: 20.2cm;font-size: 12px;" align="center">
    <tr>
        <td>出库计划编号：<%=instorageList.get(b).getPlanCode()==null?"":instorageList.get(b).getPlanCode()%></td>
        <td>创建日期：<%=instorageList.get(b).getCreateTime()==null?"":instorageList.get(b).getCreateTime() %></td>
    </tr>
</table>
<!--table的最大高度为height:21cm;  -->
<table style="border-collapse: collapse; border: none;width: 20cm;font-size: 12px;" align="center">
    <tr>
        <td style="border: solid #000 1px;width:2cm;">序号</td>
        <td style="border: solid #000 1px;width:2cm;">物料编号</td>
        <td style="border: solid #000 1px;width:50cm;">物料名称</td>
        <td style="border: solid #000 1px;width:2cm;">数量</td>
        <td style="border: solid #000 1px;width:5cm;">状态</td>
    </tr>
    <%
        int len = instorageList.get(b).getDetailList().size();
        int s = 4 - len;
        double yhj = 0;
        double jeyhj = 0;
        for(int i= 0;i< len ;i++){
            List<OutStoragePlanDetail> dail = instorageList.get(b).getDetailList();
            //页合计数量
            yhj =  yhj + Double.parseDouble(dail.get(i).getAmount()==null||dail.get(i).getAmount().equals("")?"0":dail.get(i).getAmount().toString());

    %>
    <tr >
        <td style="border: solid #000 1px;"><%=i+1 %></td>
        <td style="border: solid #000 1px;"><%=dail.get(i).getMaterialNo()==null?"":dail.get(i).getMaterialNo() %></td>
        <td style="border: solid #000 1px;"><%=dail.get(i).getMaterialName()==null?"":dail.get(i).getMaterialName() %></td>
        <td style="border: solid #000 1px;"><%=dail.get(i).getAmount()==null?"":dail.get(i).getAmount() %></td>
        <td style="border: solid #000 1px;"><%=dail.get(i).getTypeValue()==null?"":dail.get(i).getTypeValue() %></td>
    </tr>
    <%
        }
        for(int i= 0;i< s ;i++){
    %>
    <tr >
        <td style="border: solid #000 1px;">&nbsp;</td>
        <td style="border: solid #000 1px;">&nbsp;</td>
        <td style="border: solid #000 1px;">&nbsp;</td>
        <td style="border: solid #000 1px;">&nbsp;</td>
        <td style="border: solid #000 1px;">&nbsp;</td>

    </tr>
    <%
        }
        yhj = Math.round(yhj * 10000) / 10000.0;
        jeyhj = Math.round(jeyhj * 10000) / 10000.0;

        double allamount = Double.parseDouble(instorageList.get(b).getAllAmount());
        allamount = Math.round(allamount * 10000) / 10000.0;
    %>
    <tr >
        <td style="border: solid #000 1px;white-space:nowrap;">页合计：</td>
        <td style="border: solid #000 1px;white-space:nowrap;"></td>
        <td style="border: solid #000 1px;white-space:nowrap;"></td>
        <td style="border: solid #000 1px;white-space:nowrap;"><%=yhj %></td>
        <td style="border: solid #000 1px;white-space:nowrap;"></td>

    </tr>
    <tr >
        <td style="border: solid #000 1px;white-space:nowrap;">合计：</td>
        <td style="border: solid #000 1px;white-space:nowrap;"></td>
        <td style="border: solid #000 1px;white-space:nowrap;"></td>
        <td style="border: solid #000 1px;white-space:nowrap;"><%=allamount %></td>
        <td style="border: solid #000 1px;white-space:nowrap;"></td>
    </tr>
</table>
<table style="border-collapse: collapse; border: none;width: 20.2cm;font-size: 12px;margin-bottom: 10px;" align="center">
</table>
<%

%>
<%
    if (1 <instorageList.size()&&b!=instorageList.size()-1){
%>
<div id="xx" style="height:80px;"></div>
<%
            }
        }
    }
%>
</body>
</html>

