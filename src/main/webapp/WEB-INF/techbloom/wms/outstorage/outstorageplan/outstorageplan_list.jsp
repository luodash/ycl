<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<div id="outstorageplan_list_grid-div">
    <!-- 隐藏区域：存放查询条件 -->
    <form id="outstorageplan_list_hiddenQueryForm" action="<%=path%>/outStoragePlan/exportExcel" method="POST" style="display:none;">
		<input name="ids" id="outstorageplan_list_ids" value="" />
        <input name="planCode" id="outstorageplan_list_planCode" value="" />
        <input id="outstorageplan_list_outType" name="outType" value="" />
		<input id="outstorageplan_list_saleId" name="saleId" value="" />
		<input id="outstorageplan_list_state" name="state" value="" />
    </form>
    <div class="query_box" id="outstorageplan_list_yy" title="查询选项">
            <form id="outstorageplan_list_queryForm" style="max-width:100%;">
			 <ul class="form-elements">
				<li class="field-group field-fluid3">
					<label class="inline" for="outstorageplan_list_planCode" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:92px;">出库计划编号：</span>
						<input type="text" id="outstorageplan_list_planCode" name="planCode" value="" style="width: calc(100% - 97px);" placeholder="出库计划编号">
					</label>			
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="outstorageplan_list_outType" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:92px;">出库类型：</span>
						<input type="hidden" id="outstorageplan_list_outType" name="outType" value="${outPlan.outType }"/>
                        <select class="mySelect2" style = "width:calc(100% - 97px);" id="outstorageplan_list_outType" name="outType" data-placeholder="请选择出库计划类型">
                            <option value=""></option>
                            <c:forEach items="${inTypes}" var="in">
                                <option value="${in.dictNum}" <c:if test="${outPlan.outType==in.dictNum }">selected="selected"</c:if>>
                                        ${in.dictValue}
                                </option>
                            </c:forEach>
                        </select>
					</label>					
				</li>								
				<li class="field-group field-fluid3">
					<label class="inline" for="outstorageplan_list_saleId" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:92px;">对应订单编号：</span>
						<select class="mySelect2" style = "width:calc(100% - 97px);" id="outstorageplan_list_saleId" name="saleId" data-placeholder="请选择出库计划类型">
                            <option value=""></option>
                            <c:forEach items="${sales}" var="sl">
                                <option value="${sl.id}" <c:if test="${outPlan.saleId==sl.id }">selected="selected"</c:if>>
                                        ${sl.text}
                                </option>
                            </c:forEach>
                        </select>
					</label>			
				</li>
				<li class="field-group-top field-group field-fluid3">
					<label class="inline" for="outstorageplan_list_state" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:92px;">订单类型：</span>
						<select class="mySelect2" style = "width:calc(100% - 97px);" id="outstorageplan_list_state" name="state" data-placeholder="请选择出库计划类型">
                            <option value="">所有类型</option>
                            <option value="0">生产订单生成</option>
                            <option value="1">销售订单生成</option>
                            <option value="2">手动添加</option>
                        </select>
					</label>					
				</li>
				
			</ul>
			<div class="field-button" style="">
					<div class="btn btn-info" onclick="queryOk();">
				        <i class="ace-icon fa fa-check bigger-110"></i>查询
			        </div>
				    <div class="btn" onclick="reset();"><i class="ace-icon icon-remove"></i>重置</div>
				    <a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>
		        </div>
		  </form>		 
    </div>
    <div id="outstorageplan_list_grid-div" style="width:100%;margin:0px auto;">
        <div id="outstorageplan_list_fixed_tool_div" class="fixed_tool_div">
            <div id="outstorageplan_list___toolbar__" style="float:left;overflow:hidden;"></div>
        </div>
        <table id="outstorageplan_list_grid-table" style="width:100%;height:100%;overflow:auto;"></table>
        <div id="outstorageplan_list_grid-pager"></div>
    </div>
</div>

<script type="text/javascript" src="<%=path%>/static/js/techbloom/wms/outstorage/outstorageplan/outstorageplan.js"></script>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriData;
    var _grid;
    var dynamicDefalutValue="17e870237a2d470fb170daeafe6292e1";//列表码
    $(function  (){
       $(".toggle_tools").click();
    });
    $("#outstorageplan_list_queryForm .mySelect2").select2();
    $("#outstorageplan_list___toolbar__").iToolBar({
        id:"outstorageplan_list___tb__01",
        items:[
            {label: "添加",disabled:(${sessionUser.addQx}==1?false:true),onclick:addOutPlan,iconClass:'icon-plus'},
            {label: "编辑",disabled:(${sessionUser.editQx}==1?false:true),onclick:editOutPlan,iconClass:'icon-pencil'},
            {label: "删除",disabled:(${sessionUser.deleteQx}==1?false:true),onclick:delOutPlan,iconClass:'icon-trash'},
            {label: "查看", disabled:(${sessionUser.queryQx} == 1 ? false : true),onclick:viewDetailList, iconClass:'icon-zoom-in'},
            {label: "导出",disabled:(${sessionUser.deleteQx}==1?false:true),onclick:exportOutPlan,iconClass:'icon-share'},
            {label: "打印",disabled:(${sessionUser.deleteQx}==1?false:true),onclick:printOutPlan,iconClass:' icon-print'},
            {label: "生成出库单",disabled:(${sessionUser.deleteQx}==1?false:true),onclick:spiltOutPlan,iconClass:' icon-plus'}

    ]
    });

    _grid = jQuery("#outstorageplan_list_grid-table").jqGrid({
        url : context_path + '/outStoragePlan/toList',
        datatype : "json",
        colNames : [ '出库单主键','出库计划编号','出库计划类型','出库时间','出库完成时间','订单类型','对应订单编号','备注','状态'],
        colModel : [
            {name : 'id',index : 'id',width : 20,hidden:true},
            {name : 'planCode',index : 'planCode',width : 40},
            {name : 'outType',index : 'outType',width : 40},
            {name : 'createTime',index : 'createTime',width :40,formatter:function(cellValu,option,rowObject){
                    if(cellValu){
                        return cellValu.substring(0,19);
                    }else{
                        return "";
                    }
                }},
            {name : 'finishTime',index : 'finishTime',width : 40,formatter:function(cellValu,option,rowObject){
                    if(cellValu){
                        return cellValu.substring(0,19);
                    }else{
                        return "";
                    }
                }},
            {name : 'state',index : 'state',width : 65,
                formatter:function(cellValu,option,rowObject){
                    if(cellValu==0){
                        return "生产订单生成";
                    }
                    if(cellValu==1){
                        return "销售订单生成";
                    }
                    if(cellValu==2){
                        return "手动生成";
                    }
                }
            },
            {name : 'orderName',index : 'orderName',width : 50},
            {name : 'remark',index : 'remark',width : 50,},
            {name :'type',index:'type',width:50,
                formatter:function(cellValu,option,rowObject){
                    if(cellValu==0){
                        return "<span style='color:red;font-weight:bold;'>未提交</span>" ;
                    }
                    if(cellValu==1){
                        return "<span style='color:green;font-weight:bold;'>已提交</span>" ;
                    }
                    if(cellValu==2){
                        return "<span style='color:orange;font-weight:bold;'>出库中</span>" ;
                    }
                    if(cellValu==3){
                        return "<span style='color:green;font-weight:bold;'>已完成</span>" ;
                    }
                }
            },
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : '#outstorageplan_list_grid-pager',
        sortname : 'op.id',
        sortorder : "desc",
        altRows: false,
        viewrecords : true,
        autowidth:true,
        multiselect:true,
        multiboxonly: true,
        beforeRequest:function (){
            dynamicGetColumns(dynamicDefalutValue,"outstorageplan_list_grid-table",$(window).width()-$("#sidebar").width() -7);
            //重新加载列属性
        },
        loadComplete : function(data){
            var table = this;
            setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            oriData = data;
            $(window).triggerHandler('resize.jqGrid');
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
    });
    //在分页工具栏中添加按钮
    jQuery("#outstorageplan_list_grid-table").navGrid("#outstorageplan_list_grid-pager",{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd('#outstorageplan_list_grid-pager',{
            caption:"",
            buttonicon:"ace-icon fa fa-refresh green",
            onClickButton: function(){
                $("#outstorageplan_list_grid-table").jqGrid("setGridParam",
                    {
                        postData: {queryJsonString:""} //发送数据
                    }
                ).trigger("reloadGrid");
            }
        }).navButtonAdd("#outstorageplan_list_grid-pager",{
            caption: "",
            buttonicon:"fa icon-cogs",
            onClickButton : function (){
                jQuery("#outstorageplan_list_grid-table").jqGrid("columnChooser",{
                    done: function(perm, cols){
                        dynamicColumns(cols,dynamicDefalutValue);
                        $("#outstorageplan_list_grid-table").jqGrid("setGridWidth", $("#outstorageplan_list_grid-div").width()-3);
                    }
                });
            }
        });
    $(window).on("resize.jqGrid", function () {
        $("#outstorageplan_list_grid-table").jqGrid("setGridWidth", $("#outstorageplan_list_grid-div").width() - 3 );
        var height = $("#breadcrumb").outerHeight(true)+$(".query_box").outerHeight(true)+
                     $("#outstorageplan_list_fixed_tool_div").outerHeight(true)+
                     $("#gview_outstorageplan_list_grid-table .ui-jqgrid-hbox").outerHeight(true)+
                     $("#outstorageplan_list_grid-pager").outerHeight(true)+$("#header").outerHeight(true);
                     $("#outstorageplan_list_grid-table").jqGrid("setGridHeight", (document.documentElement.clientHeight)-height );
    })


    //  });
    var _queryForm_data = iTsai.form.serialize($('#outstorageplan_list_queryForm'));
    function queryOk(){
        var queryParam = iTsai.form.serialize($('#outstorageplan_list_queryForm'));
        //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
        queryInstoreListByParam(queryParam);

    }

    $('#outstorageplan_list_queryForm .mySelect2').select2();
    $('#outstorageplan_list_outstorageTypeSelect').change(function(){
        $('#outstorageplan_list_queryForm #outstorageplan_list_outType').val($('#outstorageplan_list_outstorageTypeSelect').val());
    });

    $('#outstorageplan_list_areaSelect').change(function(){
        $('#outstorageplan_list_queryForm #outstorageplan_list_areaId').val($('#outstorageplan_list_areaSelect').val());
    });

    function reset(){
        iTsai.form.deserialize($("#outstorageplan_list_queryForm"),_queryForm_data);
        $("#outstorageplan_list_areaSelect").val("").trigger("change");
        $("#outstorageplan_list_outstorageTypeSelect").val("").trigger("change");
        $("#outstorageplan_list_queryForm #outstorageplan_list_outType").select2("val","");
        $("#outstorageplan_list_queryForm #outstorageplan_list_state").select2("val","");
        $("#outstorageplan_list_queryForm #outstorageplan_list_saleId").select2("val","");
        queryInstoreListByParam(_queryForm_data);
    }
    $(".date-picker").datetimepicker({format: "YYYY-MM-DD",useMinutes:true,useSeconds:true});
    
    $("#outstorageplan_list_queryForm #outstorageplan_list_ss").select2({
        placeholder: "请选择单号",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/outStoragePlan/getCode",
            type: "POST",
            dataType: "json",
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });
</script>