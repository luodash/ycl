<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
%>
<div id="outstorageplan_spilt_page" class="row-fluid" style="height: inherit;margin:0px">
    <form action="" class="form-horizontal" id="outstorageplan_spilt_baseIn" name="baseInfor" method="post" target="_ifr" style="border-bottom: solid 2px #3b73af;">
        <input type="hidden" id="outstorageplan_spilt_id" name="id" value="${outPlan.id}">
        <%--一行数据 --%>
        <div  class="row" style="margin:0;padding:0;">
            <%--物料编号--%>
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="outstorageplan_spilt_planCode" >出库计划编号：</label>
                <div class="controls">
                    <div class="" >
                        <input type="text" id="outstorageplan_spilt_planCode" class="span10" name="planCode" value="${outPlan.planCode }"  placeholder="后台自动生成" readonly="readonly" />
                    </div>
                </div>
            </div>
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="outstorageplan_spilt_createTime">出库时间：</label>
                <div class="controls">
                    <div class="">
                        <input class="form-control date-picker" id="outstorageplan_spilt_createTime" name="createTime" type="text" value="${fn:substring(outPlan.createTime,0,19)}" placeholder="出库时间" class="span8"/>
                    </div>
                </div>
            </div>
        </div>
        <div  class="row" style="margin:0;padding:0;">
            <div class="control-group span6"  id="outstorageplan_spilt_xx">
                <label class="control-label" for="outstorageplan_spilt_outType">出库计划类型：</label>
                <div class="controls">
                    <div class="" style=" float: none !important;">
                        <input type="hidden" id="outstorageplan_spilt_outType" name="outType" value="${outPlan.outType }"/>
                        <select class="mySelect2 span10 " style = "margin-left:0px;" id="outstorageplan_spilt_outstorageTypeSelect" name="outstorageTypeSelect" data-placeholder="请选择出库计划类型">
                            <option value=""></option>
                            <c:forEach items="${inTypes}" var="in">
                                <option value="${in.dictNum}" <c:if test="${outPlan.outType==in.dictNum }">selected="selected"</c:if>>
                                        ${in.dictValue}
                                </option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div id="outstorageplan_spilt_grid-div-d" style="width:100%;margin:0px;">
        <!-- 	表格工具栏 -->
        <div id="outstorageplan_spilt_fixed_tool_div" class="fixed_tool_div detailToolBar">
            <div id="outstorageplan_spilt___toolbar__-c" style="float:left;overflow:hidden;"></div>
        </div>
        <!-- 入库单信息表格 -->
        <table id="outstorageplan_spilt_grid-table-d" style="width:100%;height:100%;"></table>
        <!-- 表格分页栏 -->
        <div id="outstorageplan_spilt_grid-pager-d"></div>
    </div>
</div>
<script type="text/javascript" src="<%=path%>/static/js/techbloom/wms/outstorage/outstorageplan/outstorageplan_detail.js"></script>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriData;      //表格数据
    var _grid;        //表格对象

    $(".date-picker").datetimepicker({format: 'YYYY-MM-DD HH:mm:ss',useMinutes:true,useSeconds:true});

    //工具栏
    $("#outstorageplan_spilt___toolbar__-c").iToolBar({
        id:"outstorageplan_spilt___tb__01",
        items:[
            {label:"确认拆分", onclick:querySpilt}
        ]
    });


    //初始化表格
    _grid = $("#outstorageplan_spilt_grid-table-d").jqGrid({
        url : context_path + '/outStoragePlan/getSpitlist?qId='+$("#outstorageplan_spilt_baseIn #outstorageplan_spilt_id").val(),
        datatype : "json",
        colNames : [ '物料主键','物料编号','物料名称','物料数量','物料单位','剩余数量','计划出库数量'],
        colModel : [
            {name : 'id',index : 'id',width : 50,hidden:true},
            {name : 'materialNo',index:'materialNo',width : 50},
            {name : 'materialName',index:'materialName',width : 150},
            {name : 'amount',index:'amount',width : 50},
            {name : 'unit',index:'unit',width : 50},
            {name : 'restAmount',index:'restAmount',width : 50},
            {name :'lastAmount',width : 50,index:'lastAmount', editable: true, editrules:{custom: true, custom_func: myamountcheck},
                editoptions: {
                    size: 25, dataEvents: [{
                        type: 'blur',
                        fn: function (e) {
                            var $element = e.currentTarget;
                            var $elementId = $element.id;
                            var rowid = $elementId.split("_")[0];
                            var reg = new RegExp("^[0-9]+(.[0-9]{1,2})?$");
                            if (!reg.test($("#" + $elementId).val())) {
                                layer.alert("非法的数量！(注：可以有两位小数的正实数)");
                                return ;
                            }
                            var id = $("#outstorageplan_spilt_grid-table-d").jqGrid('getGridParam', 'selrow');
                            var rowData = $("#outstorageplan_spilt_grid-table-d").jqGrid('getRowData', id).restAmount;
                            $.ajax({
                                type: "POST",
                                dataType: "json",
                                url: context_path + '/outStoragePlan/updateAmount',
                                data: {lastAmount: $("#" + rowid + "_lastAmount").val(), amount: rowData, id: id},
                                success: function (data) {
                                    if (!data.result) {
                                        layer.alert(data.msg);
                                        $("#outstorageplan_spilt_grid-table-d").jqGrid('setGridParam',
                                            {
                                                url:context_path + '/outStoragePlan/getSpitlist',
                                                postData: {qId:$('#outstorageplan_spilt_baseIn #outstorageplan_spilt_id').val(),queryJsonString:""}
                                            }
                                        ).trigger("reloadGrid");
                                    }
                                }
                            })
                        }
                    }]
                }
            }
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : '#outstorageplan_spilt_grid-pager-d',
        sortname : 'ID',
        sortorder : "asc",
        altRows: true,
        viewrecords : true,
        caption : "物料列表",
        autowidth:true,
        multiselect:true,
        multiboxonly: true,
        loadComplete : function(data){
            var table = this;
            setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            oriData = data;
            $(window).triggerHandler('resize.jqGrid');
        },
        cellEdit: true,
        cellsubmit : "clientArray",
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据",
    });
    //在分页工具栏中添加按钮
    $("#outstorageplan_spilt_grid-table-d").navGrid('#outstorageplan_spilt_grid-pager-d',{edit:false,add:false,del:false,search:false,refresh:false})
        .navButtonAdd('#outstorageplan_spilt_grid-pager-d',{
            caption:"",
            buttonicon:"ace-icon fa fa-refresh green",
            onClickButton: function(){
                $("#outstorageplan_spilt_grid-table-d").jqGrid('setGridParam',
                    {
                        url:context_path + '/outStoragePlan/getSpitlist',
                        postData: {qId:$("#outstorageplan_spilt_baseIn #outstorageplan_spilt_id").val(),queryJsonString:""} //发送数据  :选中的节点
                    }
                ).trigger("reloadGrid");
            }
        });

    $(window).on('resize.jqGrid', function () {
        $("#outstorageplan_spilt_grid-table-d").jqGrid( 'setGridWidth', $("#outstorageplan_spilt_grid-div-d").width() - 3 );
        var height = $(".layui-layer-title",_grid.parents(".layui-layer")).outerHeight(true)+
            $("#outstorageplan_spilt_baseIn").outerHeight(true)+$("#outstorageplan_spilt_grid-pager-d").outerHeight(true)+
            $("#outstorageplan_spilt_fixed_tool_div.fixed_tool_div.detailToolBar").outerHeight(true)+
            $("#gview_outstorageplan_spilt_grid-table-d .ui-jqgrid-titlebar").outerHeight(true)+
            $("#gview_outstorageplan_spilt_grid-table-d .ui-jqgrid-hbox").outerHeight(true);
        $("#outstorageplan_spilt_grid-table-d").jqGrid( 'setGridHeight', _grid.parents(".layui-layer").height()-height );
    });


    //关闭当前窗口
    function closeWindowBtn(){
        layer.closeAll();
    }

    //物料详情数量编辑验证方法：只能为整数类型
    function myamountcheck(value, colname) {
        var reg = new RegExp("^[0-9]+(.[0-9]{1,2})?$");
        if (!reg.test(value)) {
            return [ false, "" ];
        }else {
            return [ true, "" ];
        }
    }
</script>
