<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
	<script type="text/javascript">
	    var context_path = '<%=path%>';
	</script>
	<style type="text/css">
	.query_box .field-button.two {
	    padding: 0px;
	    left: 650px;
    }
	</style>
</head>
<body style="overflow:hidden;">
   <div id="outstoragePlan_index_grid-div">
        <div id="outstoragePlan_index_fixed_tool_div" class="fixed_tool_div">
             <div id="outstoragePlan_index___toolbar__" style="float:left;overflow:hidden;"></div>
        </div>
		<table id="outstoragePlan_index_grid-table" style="width:100%;height:100%;"></table>
		<div id="outstoragePlan_index_grid-pager"></div>
   </div>
</body>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriData;
    var _grid;

    _grid = jQuery("#outstoragePlan_index_grid-table").jqGrid({
        url : context_path + '/outStoragePlan/toList',
        datatype : "json",
        colNames : [ '出库单主键','出库计划编号','出库计划类型','出库时间','出库完成时间','订单类型','对应订单编号','备注','状态'],
        colModel : [
            {name : 'id',index : 'id',width : 20,hidden:true},
            {name : 'planCode',index : 'planCode',width : 40},
            {name : 'outType',index : 'outType',width : 40},
            {name : 'createTime',index : 'createTime',width :40,formatter:function(cellValu,option,rowObject){
                if(cellValu){
                    return cellValu.substring(0,19);
                }else{
                    return "";
                }
            }},
            {name : 'finishTime',index : 'finishTime',width : 40,formatter:function(cellValu,option,rowObject){
                if(cellValu){
                    return cellValu.substring(0,19);
                }else{
                    return "";
                }
            }},
            {name : 'state',index : 'state',width : 65,
                formatter:function(cellValu,option,rowObject){
                    if(cellValu==0){
                        return "生产订单生成";
                    }
                    if(cellValu==1){
                        return "销售订单生成";
                    }
                    if(cellValu==2){
                        return "手动生成";
                    }
                }
            },
            {name : 'orderName',index : 'orderName',width : 50},
            {name : 'remark',index : 'remark',width : 50,},
            {name :'type',index:'type',width:50,
                formatter:function(cellValu,option,rowObject){
                    if(cellValu==0){
                        return "<span style='color:red;font-weight:bold;'>未提交</span>" ;
                    }
                    if(cellValu==1){
                        return "<span style='color:green;font-weight:bold;'>已提交</span>" ;
                    }
                    if(cellValu==2){
                        return "<span style='color:orange;font-weight:bold;'>出库中</span>" ;
                    }
                    if(cellValu==3){
                        return "<span style='color:green;font-weight:bold;'>已完成</span>" ;
                    }
                }
            },
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : '#outstoragePlan_index_grid-pager',
        sortname : 'op.id',
        sortorder : "desc",
        altRows: false,
        viewrecords : true,
        caption : "出库单列表",
        autowidth:true,
        multiselect:true,
        multiboxonly: true,
        beforeRequest:function (){
            dynamicGetColumns(dynamicDefalutValue,"outstoragePlan_index_grid-table",$(window).width()-$("#sidebar").width() -7);
            //重新加载列属性
        },
        loadComplete : function(data)
        {
            var table = this;
            setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            oriData = data;
            $(window).triggerHandler('resize.jqGrid');
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
    });
	 //在分页工具栏中添加按钮
	  jQuery("#outstoragePlan_index_grid-table").navGrid("#outstoragePlan_index_grid-pager",{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#outstoragePlan_index_grid-pager",{
		   caption:"",
		   buttonicon:"ace-icon fa fa-refresh green",
		   onClickButton: function(){
			   $("#outstoragePlan_index_grid-table").jqGrid("setGridParam",
						{
							postData: {queryJsonString:""} //发送数据
						}
				  ).trigger("reloadGrid");
		   }
		}).navButtonAdd("#outstoragePlan_index_grid-pager",{
              caption: "",
              buttonicon:"fa  icon-cogs",
              onClickButton : function (){
                  jQuery("#outstoragePlan_index_grid-table").jqGrid("columnChooser",{
                      done: function(perm, cols){
                          dynamicColumns(cols,dynamicDefalutValue);
                          $("#outstoragePlan_index_grid-table").jqGrid("setGridWidth", $("#outstoragePlan_index_grid-div").width()-3);
                      }
                  });
              }
          });
	  $(window).on("resize.jqGrid", function () {
		  $("#outstoragePlan_index_grid-table").jqGrid("setGridWidth", $("#outstoragePlan_index_grid-div").width() - 3 );
		  var height = $("#breadcrumb").outerHeight(true)+$("#outstoragePlan_index_yy").outerHeight(true)+
            $("#outstoragePlan_index_fixed_tool_div").outerHeight(true)+
            $("#gview_outstoragePlan_index_grid-table .ui-jqgrid-titlebar").outerHeight(true)+
            $("#gview_outstoragePlan_index_grid-table .ui-jqgrid-hbox").outerHeight(true)+
            $("#outstoragePlan_index_grid-pager").outerHeight(true)+$("#header").outerHeight(true);
            $("#outstoragePlan_index_grid-table").jqGrid( "setGridHeight", (document.documentElement.clientHeight)-height );
	  })
	
	  $(window).triggerHandler("resize.jqGrid");


</script>
</html>