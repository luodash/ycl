﻿<%@page import="java.math.BigDecimal"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<%@ include file="../../../techbloom/common/taglibs.jsp"%>
<base href="<%=basePath%>">
<title>库存查询单</title>
<script type="text/javascript">
		function printA4(){
			window.print();
		}
	</script>
</head>
<body onload="printA4();">
	<c:forEach items="${stockList }" var="s" varStatus="slist" step="10">
		<div style="height:100%">
		<br />
		<br />
		<h3 align="center" style="font-weight: bold;">库存信息</h3>
		<table style="border-collapse: collapse; border: none;width: 16.2cm;font-size: 12px;" align="center">
			<tr>
				<td style="border: solid #000 1px;text-align:center;width:3cm;">物料编号</td>
				<td style="border: solid #000 1px;text-align:center;width:8.2cm;">物料名称</td>
				<td style="border: solid #000 1px;text-align:center;width:2cm;">批次号</td>
				<td style="border: solid #000 1px;text-align:center;width:3cm;">数量</td>
				<td style="border: solid #000 1px;text-align:center;width:3cm;">入库数量</td>
				<td style="border: solid #000 1px;text-align:center;width:6cm;">库区</td>
				<td style="border: solid #000 1px;text-align:center;width:3cm;">质保期</td>
				<td style="border: solid #000 1px;text-align:center;width:3cm;">物料单位</td>
				<td style="border: solid #000 1px;text-align:center;width:3cm;">绑定库位</td>
				<td style="border: solid #000 1px;text-align:center;width:3cm;">rfid</td>				
				<td style="border: solid #000 1px;text-align:center;width:3cm;">装箱条码</td>
			</tr>
			<c:forEach items="${stockList }" var="s" varStatus="slist" begin="${slist.index}" end="${slist.index+9}" step="1">
				<tr>
					<td style="border: solid #000 1px;text-align:center;">${s.materialNo}</td>
					<td style="border: solid #000 1px;text-align:center;">${s.materialName}</td>
					<td style="border: solid #000 1px;text-align:center;">${s.batchNo}</td>
					<td style="border: solid #000 1px;text-align:center;">${s.amount}</td>
					<td style="border: solid #000 1px;text-align:center;">${s.inAmount}</td>
					<td style="border: solid #000 1px;text-align:center;">${s.warehouseName}</td>
					<td style="border: solid #000 1px;text-align:center;">${s.finaldate}</td>
					<td style="border: solid #000 1px;text-align:center;">${s.materialUnit}</td>
					<td style="border: solid #000 1px;text-align:center;">${s.allocation}</td>
					<td style="border: solid #000 1px;text-align:center;">${s.rfid}</td>
					<td style="border: solid #000 1px;text-align:center;">${s.boxBarCode}</td>
				</tr>
			</c:forEach>
		</table>		
		<br/>
		<br/>
		</div>
	</c:forEach>
</body>
</html>

