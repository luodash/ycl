<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<div id="generate_task_page" class="row-fluid" style="height: inherit;">
	<form id="taskForm" class="form-horizontal" style="overflow: auto; height: calc(100% - 70px);">
		<input type="hidden" id="ids" name="ids" value="${ids }">
		<div class="control-group">
			<label class="control-label" for="bomDetailNo">箱数：</label>
			<div class="controls">
				<input type="text" id="boxNum" name="boxNum" style="width: 300px;" placeholder="请输入每条补货任务的箱数" />
			</div>
		</div>
	</form>
	<div class="field-button" style="text-align: center;border-top: 0px;margin: 15px auto;">
		<span class="btn btn-info" onclick="taskOk();">
		   <i class="ace-icon fa fa-check bigger-110"></i>确定
		</span>
		<span class="btn btn-danger" onclick="layer.close($queryWindow);return false;">
		   <i class="icon-remove"></i>&nbsp;取消
		</span>
	</div>
</div>
<script type="text/javascript">
var context_path= '<%=path%>';
$("#taskForm").validate({
   rules:{
      "boxNum":{
         required:true,
         digits:true
      }
   },
   messages: {
      "boxNum":{
         required:"请输入箱数！",
         digits:"只能输入正整数！"
      }
   },
   errorClass: "help-inline",
   errorElement: "span",
   highlight:function(element, errorClass, validClass) {
          $(element).parents('.control-group').addClass('error');
	 },
  unhighlight: function(element, errorClass, validClass) {
		  $(element).parents('.control-group').removeClass('error');
   }
});
function taskOk(){
   if($("#taskForm").valid()){
       createTask();
   }
}
function createTask(){
$.ajax({
	 type:"POST",
	 url:context_path + "/replenishTask/toReplenishPlan",
	 data:{ids:$("#taskForm #ids").val(),boxNum:$("#taskForm #boxNum").val()},
	 dataType:"json",
	 success:function(data){
	     if(Boolean(data.result)){
	         layer.msg("生成补货任务成功！",{icon:1});
	         $("#grid-table").jqGrid("setGridParam", {
                postData: {queryJsonString:""}
             }).trigger("reloadGrid");
             layer.close($queryWindow);
	     }else{
	         layer.msg(data.msg,{icon:2});
	     }			  
	 }
  });
}
</script>