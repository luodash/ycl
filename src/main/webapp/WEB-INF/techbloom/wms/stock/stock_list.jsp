<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<body style="">
<div id="grid-div">
    <!-- 隐藏区域：存放查询条件 -->
    <form id="hiddenQueryForm" action="<%=path%>/InventoryStatistics/exportExcel" method="POST" style="display:none;">       
        <input id="areaId" name="areaId" value="">
        <input id="materialId" name="materialId" value=""/>
        <input id="batchNo" name="batchNo" value=""/>
        <input id="rfid" name="rfid" value="">
    </form>
    <form id="hiddenForm" action="<%=request.getContextPath()%>/stock/exportExcel" method="POST" style="display: none;">
		<!-- 选中的用户 -->
        <input id="ids" name="ids" value=""/>
    </form>
    <div class="query_box" id="yy" title="查询选项">
            <form id="queryForm" style="max-width:100%;">
			 <ul class="form-elements">
				<li class="field-group field-fluid3">
					<label class="inline" for="areaId" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:61px;">库区：</span>
						<input id="areaId" name="areaId" type="text" style="width: calc(100% - 66px);" placeholder="库区">
					</label>			
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="materialId" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:61px;">物料：</span>
						<input id="materialId" name="materialId" type="text" style="width: calc(100% - 66px);" placeholder="物料">
					</label>					
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="batchNo" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:61px;">批次号：</span>
						<input id="batchNo" name="batchNo" type="text" style="width: calc(100% - 66px);" placeholder="批次号">
					</label>			
				</li>
				<li class="field-group-top field-group field-fluid3">
					<label class="inline" for="rfid" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:61px;">rfid编号：</span>
						<input id="rfid" name="rfid" type="text" style="width: calc(100% - 66px);" placeholder="rfid编号">
					</label>					
				</li>
				
			</ul>
			<div class="field-button" style="">
					<div class="btn btn-info" onclick="queryOk();">
				        <i class="ace-icon fa fa-check bigger-110"></i>查询
			        </div>
				    <div class="btn" onclick="reset();"><i class="ace-icon icon-remove"></i>重置</div>
				    <a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>
		        </div>
		  </form>		 
    </div>
    <div id="fixed_tool_div" class="fixed_tool_div">
        <div id="__toolbar__" style="float:left;overflow:hidden;"></div>
    </div>
    <!--    物料信息表格 -->
    <table id="grid-table" style="width:100%;height:100%;"></table>
    <!-- 	表格分页栏 -->
    <div id="grid-pager"></div>
</div>
</body>
<script type="text/javascript" src="<%=path%>/static/js/techbloom/wms/stock/stock.js"></script>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriData;      //表格数据
    var _grid;        //表格对象
    var dynamicDefalutValue="78089bcc9e484023afa33a0addc6729d";//列表码
    $(function  (){
       $(".toggle_tools").click();
    });
    $(function () {
        //工具栏
        $("#__toolbar__").iToolBar({
                id: "__tb__01",
                items: [
                    {label: "导出", disabled: (${sessionUser.queryQx} == 1 ? false : true),onclick:toExcel,iconClass:' icon-share'},
                    {label: "打印", disabled: (${sessionUser.queryQx} == 1 ? false : true),onclick:printList,iconClass:' icon-print'},
                    {label: "删除", disabled: (${sessionUser.queryQx} == 1 ? false : true),onclick:stockDelete,iconClass:' icon-trash'}
               ]
    });
        //初始化表格
        _grid = jQuery("#grid-table").jqGrid({
            url: context_path + "/stock/list",
            datatype: "json",
            colNames: ["主键", "物料编号", "物料名称", "批次号","库存量", "入库数量","库区","质保期", "单位",  "绑定货架", "RFID","装箱条码"],
            colModel: [
                {name: "id", index: "id", width: 20, hidden: true},
                {name: "materialNo", index: "materialNo", width: 30},
                {name: "materialName", index: "materialName", width:30},
                {name: "batchNo", index: "batchNo", width: 70},
                {name: "amount", index: "amount", width: 20},
                {name: "inAmount", index: "inAmount", width: 30,hidden:true},
                {name: "warehouseName", index: "warehouseName", width: 75},
                {name: "finaldate", index: "finaldate", width: 35},
                {name: "materialUnit", index: "materialUnit", width: 20,},
                {name: "allocation", index: "allocation", width: 30},
                {name: "rfid", index: "rfid", width: 60},
                {name: "boxBarCode", index: "boxBarCode", width: 30}
            ],
            rowNum: 20,
            rowList: [10, 20, 30],
            pager: "#grid-pager",
            sortname: "id",
            sortorder: "desc",
            altRows: true,
            viewrecords: true,
            autowidth: true,
            multiselect: true,
            multiboxonly: true,
            beforeRequest:function (){
                dynamicGetColumns(dynamicDefalutValue,"grid-table",$(window).width()-$("#sidebar").width() -7);
                //重新加载列属性
            },
            loadComplete: function (data) {
                var table = this;
                setTimeout(function () {
                    updatePagerIcons(table);
                    enableTooltips(table);
                }, 0);
                oriData = data;
                $(window).triggerHandler('resize.jqGrid');
            },
            emptyrecords: "没有相关记录",
            loadtext: "加载中...",
            pgtext: "页码 {0} / {1}页",
            recordtext: "显示 {0} - {1}共{2}条数据"
        });
        //在分页工具栏中添加按钮
        jQuery("#grid-table").navGrid('#grid-pager', {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false
        }).navButtonAdd('#grid-pager', {
                caption: "",
                buttonicon: "ace-icon fa fa-refresh green",
                onClickButton: function () {
                    //jQuery("#grid-table").trigger("reloadGrid");  //重新加载表格
                    $("#grid-table").jqGrid('setGridParam',
                        {
                            postData: {queryJsonString: ""} //发送数据
                        }
                    ).trigger("reloadGrid");
                }
            }).navButtonAdd('#grid-pager',{
                caption: "",
                buttonicon:"fa  icon-cogs",
                onClickButton : function (){
                    jQuery("#grid-table").jqGrid('columnChooser',{
                        done: function(perm, cols){
                            dynamicColumns(cols,dynamicDefalutValue);
                            $("#grid-table").jqGrid( 'setGridWidth', $("#grid-div").width()-3);
                        }
                    });
                }
            });

        $(window).on("resize.jqGrid", function () {
            $("#grid-table").jqGrid("setGridWidth", $("#grid-div").width() - 3);
            $("#grid-table").jqGrid("setGridHeight", $(".container-fluid").height()-$(".query_box").outerHeight(true)-$("#fixed_tool_div").outerHeight(true)-$("#grid-pager").outerHeight(true)
           -$("#gview_grid-table .ui-jqgrid-hbox").outerHeight(true));
        });

        $(window).triggerHandler('resize.jqGrid');
    });
    var _queryForm_data = iTsai.form.serialize($('#queryForm'));
    function queryOk() {
        var queryParam = iTsai.form.serialize($('#queryForm'));
        queryInstoreListByParam(queryParam);
    }

    $('#queryForm .mySelect2').select2();
    $('#instorageTypeSelect').change(function () {
        $('#queryForm #instorageType').val($('#instorageTypeSelect').val());
    });
    $('#queryForm #areaId').change(function () {
        $('#hiddenQueryForm #areaId').val($('#queryForm #areaId').val());
    });
    function reset() {
        iTsai.form.deserialize($("#queryForm"), _queryForm_data);
        $("#queryForm #areaId").select2("val", "");
        $("#queryForm #materialId").select2("val", ""); 
        queryInstoreListByParam(_queryForm_data);

    }
    
    //库存删除
    function stockDelete(){
    	var checkedNum = getGridCheckedNum("#grid-table","id");  //选中的数量
		if(checkedNum==0 ||  checkedNum>1){
			layer.alert("请选择一条库存进行操作!");
    	}else{
    		//从数据库中删除选中的入库单，并刷新入库单表格
			var ids = jQuery("#grid-table").jqGrid('getGridParam', 'selarrrow');
			var rowData = $("#grid-table").jqGrid('getRowData',ids);
			if(rowData.amount==0){
				//弹出确认窗口
				layer.confirm("确定删除选中的库存？", function() {
		    		$.ajax({
		    			type : "POST",
		    			url:context_path + "/stock/stockDelete?ids="+ids,
		    			dataType : 'json',
		    			cache : false,
		    			success : function(data) {
		    				layer.closeAll();
		    				if(data.result){
								layer.msg("删除成功", {icon: 1,time:1000});
		    				}else{
		    					layer.msg("删除失败", {icon: 2,time:1000});
		    				}
		    				_grid.trigger("reloadGrid");  //重新加载表格
		    			}
		    		});
		    	})
			}else{
				layer.alert("当前库存数量不为0,不能删除");
			}
    	}
    }
    
    $("#queryForm #materialId").select2({
        placeholder: "请选择物料",//文本框的提示信息
        minimumInputLength: 0,   //至少输入n个字符，才去加载数据
        allowClear: true,  //是否允许用户清除文本信息
        ajax: {
            url: context_path + "/material/getMaterialSelectList",
            dataType: "json",
            delay: 250,
            data: function (term, pageNo) {     //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    docId: $("#id").val(),
                    queryString: term,    //联动查询的字符
                    pageSize: 15,    //一次性加载的数据条数
                    pageNo: pageNo,    //页码
                    time: new Date()//测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) {   //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res, more: more
                    };
                } else {
                    return {
                        results: {"id": "0", "text": "没有更多结果"}
                    };
                }

            },
            cache: true
        }
    });
    $("#queryForm #areaId").select2({
        placeholder: "请选择库区",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
   
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/area/getSelectArea",
            type: "POST",
            dataType: "json",
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });
</script>
