<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <%-- <%@ include file="/techbloom/common/taglibs.jsp"%> --%>
    <script type="text/javascript">
        var context_path = '<%=path%>';
    </script>
    <style type="text/css">

        .query_box .field-button.two {
            padding: 0px;
            left: 650px;
        }
    </style>
</head>
<div class="panel_tab">
    <div class="row-fluid">
        <!-- 表格数据 -->
        <table id="grid-table_stockIndex" style="width:100%;"></table>
        <!-- 表格底部 -->
        <div id="grid-pager_stockIndex"></div>
    </div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">

    var context_path = '<%=path%>';
    var oriData;
    var _grid;

    _grid = jQuery("#grid-table_stockIndex").jqGrid({
        url: context_path + "/stock/list",
        datatype: "json",
        colNames: [ "物料名称", "批次号","库存量", "库区"],
        colModel: [
            {name: "materialName", index: "materialName", width:30},
            {name: "batchNo", index: "batchNo", width: 70},
            {name: "amount", index: "amount", width: 20},
            {name: "warehouseName", index: "warehouseName", width: 75}
        ],
        rowNum: 20,
        rowList: [10, 20, 30],
        pager: "#grid-pager_stockIndex",
        sortname: "id",
        sortorder: "desc",
        altRows: true,
        viewrecords: true,
        autowidth: true,
        multiselect: false,
        multiboxonly: true,
        loadComplete: function (data) {
            var table = this;
            setTimeout(function () {
                updatePagerIcons(table);
                enableTooltips(table);
            }, 0);
            oriData = data;
            $(window).triggerHandler('resize.jqGrid');
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext: "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
    });
    $(window).on("resize.jqGrid", function () {
        $("#grid-table_stockIndex").jqGrid("setGridWidth",  $("#grid-table_stockIndex").parents(".itemContent").width()-2 );
        $("#grid-table_stockIndex").jqGrid( 'setGridHeight', 300);
    })

    $(window).triggerHandler("resize.jqGrid");


</script>
</html>