<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="grid-div">
    <form id="hiddenForm" action="<%=path%>/replenishmentList/toExcel.do" method="POST" style="display: none;">
        <input id="ids" name="ids" value=""/>
    </form>
    <form id="hiddenQueryForm" style="display:none;">
        <input id="materialNo" name="materialNo" value=""/>
        <input id="materialName" name="materialName" value="">
    </form>
    <div class="query_box" id="yy" title="查询选项">
         <form id="queryForm" style="max-width:100%;">
		    <ul class="form-elements">
				<li class="field-group field-fluid2">
					<label class="inline" for="materialNo" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:80px;">物料编号：</span>
						<input type="text" name="materialNo" id="materialNo" value="" style="width: calc(100% - 85px);" placeholder="物料编号">
					</label>			
				</li>
				<li class="field-group field-fluid2">
					<label class="inline" for="materialName" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:80px;">物料名称：</span>
						<input type="text" name="materialName" id="materialName" value="" style="width: calc(100% - 85px);" placeholder="物料名称">
					</label>				
				</li>				
			
			</ul>
				<div class="field-button">
					<div class="btn btn-info" onclick="queryOk();">
				        <i class="ace-icon fa fa-check bigger-110"></i>查询
			        </div>
					<div class="btn" onclick="reset();"><i class="ace-icon icon-remove"></i>重置</div>					
		        </div>
		  </form>		 
    </div>
    <div id="fixed_tool_div" class="fixed_tool_div">
        <div id="__toolbar__" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="grid-table" style="width:100%;margin: 0px !important;boder:0px !important"></table>
    <div id="grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriData;
    var _grid;
    var openwindowtype;
    var dynamicDefalutValue="f1840fe8727340368e22b37e4311409f";
    $(function  (){
        $(".toggle_tools").click();
    });
    $("#__toolbar__").iToolBar({
        id: "__tb__01",
        items: [           
            {label: "导出", disabled: ( ${sessionUser.queryQx}==1?false:true),onclick:function(){toExcel();},iconClass:' icon-share'},
            {label: "生成补货任务",disabled:(${sessionUser.addQx}==1?false:true),onclick:generateTask,iconClass:'icon-plus'}
           ]
    });
    $(function () {
        _grid = jQuery("#grid-table").jqGrid({
            url: context_path + "/replenishmentList/list.do",
            datatype: "json",
            colNames: ["主键","物料编号","物料名称","补货数量","备注"],
            colModel: [
                {name: "id", index: "id", hidden: true},
                {name: "materialNo", index: "materialNo", width: 50},
                {name: "materialName", index: "materialName", width: 50},               
                {name: "amount", index: "amount" ,width : 30},
                {name: "remark", index: "remark" ,width : 50}
            ],
            rowNum: 20,
            rowList: [10, 20, 30],
            pager: "#grid-pager",
            sortname: "rl.id",
            sortorder: "asc",
            altRows: true,
            viewrecords: true,
            autowidth: true,
            multiselect: true,
            multiboxonly: true,
            beforeRequest:function (){
                dynamicGetColumns(dynamicDefalutValue,'grid-table', $(window).width()-$("#sidebar").width() -7);
                //重新加载列属性
            },
            loadComplete: function (data) {
                var table = this;
                setTimeout(function () {
                    updatePagerIcons(table);
                    enableTooltips(table);
                }, 0);
                oriData = data;
            },
            emptyrecords: "没有相关记录",
            loadtext: "加载中...",
            pgtext: "页码 {0} / {1}页",
            recordtext: "显示 {0} - {1}共{2}条数据"
        });
        //在分页工具栏中添加按钮
        jQuery("#grid-table").navGrid("#grid-pager", {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false
        }).navButtonAdd("#grid-pager", {
                    caption: "",
                    buttonicon: "ace-icon fa fa-refresh green",
                    onClickButton: function () {
                        $("#grid-table").jqGrid("setGridParam",
                                {
                                    postData: {queryJsonString: ""} //发送数据
                                }
                        ).trigger("reloadGrid");
                    }
                }).navButtonAdd("#grid-pager",{
                caption: "",
                buttonicon:"ace-icon fa icon-cogs",
                onClickButton : function (){
                    jQuery("#grid-table").jqGrid("columnChooser",{
                        done: function(perm, cols){
                            dynamicColumns(cols,dynamicDefalutValue);
                            //cols页面获取隐藏的列,页面表格的值
                            $("#grid-table").jqGrid("setGridWidth", $("#grid-div").width() );
                        }
                    });
                }
            });
        $(window).on("resize.jqGrid", function () {
            $("#grid-table").jqGrid("setGridWidth", $("#grid-div").width() );
            $("#grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-$("#yy").outerHeight(true)-$("#fixed_tool_div").outerHeight(true)-$("#grid-pager").outerHeight(true)-
            $("#gview_grid-table .ui-jqgrid-hdiv").outerHeight(true));
        });
        $(window).triggerHandler("resize.jqGrid");
    });
    var _queryForm_data = iTsai.form.serialize($("#queryForm"));
	function queryOk(){
		var queryParam = iTsai.form.serialize($("#queryForm"));
		queryListByParam(queryParam);		
	}
	function queryListByParam(jsonParam) {
    iTsai.form.deserialize($("#hiddenQueryForm"), jsonParam);
    var queryParam = iTsai.form.serialize($("#hiddenQueryForm"));
    var queryJsonString = JSON.stringify(queryParam); 
    $("#grid-table").jqGrid("setGridParam",
        {
            postData: {queryJsonString: queryJsonString}
        }).trigger("reloadGrid");
    }
	function reset(){
		$("#queryForm #materialNo").val("").trigger("change");
        $("#queryForm #materialName").val("").trigger("change");
	    $("#grid-table").jqGrid("setGridParam", {
            postData: {queryJsonString:""}
        }).trigger("reloadGrid");		
	}
	function generateTask(){
	    var checkedNum = getGridCheckedNum("#grid-table", "id");
        if (checkedNum == 0) {
           layer.alert("请至少选择一条记录生成补货任务！");
           return false;
        } else {
           var ids = jQuery("#grid-table").jqGrid("getGridParam", "selarrrow");
           $.post(context_path + "/replenishmentList/toGenerateTask.do?ids="+ids, {}, function (str){
    		   $queryWindow=layer.open({
    		       title : "生成补货任务", 
    	    	   type:1,
    	    	   skin : "layui-layer-molv",
    	    	   area : "490px",
    	    	   shade : 0.6, //遮罩透明度
    		       moveType : 1, //拖拽风格，0是默认，1是传统拖动
    		       anim : 2,
    		       content : str,
    		       success: function (layero, index) {
                   layer.closeAll("loading");
                }
    		});
    	});
      }
	}
	
	//导出
    function toExcel(){
        //var ids = jQuery("#grid-table").jqGrid("getGridParam", "selarrrow");
        var ids = getGridCheckedId("#grid-table","id");
        $("#hiddenForm #ids").val(ids);
        $("#hiddenForm").submit();	
    }
</script>
</html>