﻿<%@ page language="java" import="java.lang.*"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
	String path = request.getContextPath();
%>
<div class="main-content" style="height:100%">
	<div class="widget-header widget-header-large" id="div1">
		<input type="hidden" id="id" name="id" value="${PLAN.id }" />
		<input type="hidden" id="receiptId" name="receiptId" value="${PLAN.receiptId }" />
		<h3 class="widget-title grey lighter" style="background: none; border-bottom: none;">
			<i class="ace-icon fa fa-leaf green"></i>
			越库计划
		</h3>
		<div class="widget-toolbar no-border invoice-info">
			<span class="invoice-info-label">计划编号：</span>
			<span class="red">${PLAN.planCode }</span>
			<br />
			<span class="invoice-info-label">计划时间：</span>
			<span class="blue">${fn:substring(PLAN.createTime, 0, 19)}</span>
		</div>
		<!-- 打印按钮 -->
		<div class="widget-toolbar hidden-480">
			<a href="#" onclick="printDoc();" title="详情打印">
				<i class="ace-icon fa fa-print"></i>
			</a>
		</div>
	</div>
	<div class="widget-body" id="div2">
		<table style="width: 100%;font-size:16px;border-collapse:separate;border-spacing:2px 3px;">
			<tr>
				<td>
					<i class="ace-icon fa fa-caret-right blue"></i>
					越库计划类型：
					<b class="black">${PLAN.text }</b>
				</td>
				<td>
					<i class="ace-icon fa fa-caret-right blue"></i>
					所属单据：
					<b class="black">${PLAN.orderName }</b>
				</td>
			</tr>
			<tr>
				<td>
					<i class="ace-icon fa fa-caret-right blue"></i>
					状态：
					<c:if test="${PLAN.type==0 }"><span style="color:#d15b47;font-weight:bold;">未提交</span></c:if>
					<c:if test="${PLAN.type==1 }"><span style="color:#87b87f;font-weight:bold;">已提交</span></c:if>
					<c:if test="${PLAN.type==2 }"><span style="color:#76b86b;font-weight:bold;">出库中</span></c:if>
					<c:if test="${PLAN.type==3 }"><span style="color:#76b86b;font-weight:bold;">出库完成</span></c:if>
				</td>
			</tr>
			</tr>
		</table>
	</div>
	<!-- 详情表格 -->
	<div id="grid-div-c">
		<!-- 物料信息表格 -->
		<table id="grid-table-c" style="width:100%;height:100%;"></table>
		<!-- 表格分页栏 -->
		<div id="grid-pager-c"></div>
	</div>
</div>
<script type="text/javascript">
var context_path = '<%=path%>';
var oriData;      //表格数据
var _grid;        //表格对象
$(function(){
	//初始化表格
  	_grid = jQuery("#grid-table-c").jqGrid({
        url : context_path + '/crossDockPlan/DetailList?qId='+$("#id").val()+'&rId='+$("#receiptId").val(),
        datatype : "json",
        colNames : [ '详情主键','物料主键','物料编号','物料名称','物料单位','可越库量','越库数量','状态'],
        colModel : [
            {name : 'id',index : 'id',width : 55,hidden:true},
            {name : 'materialId',index : 'materialId',width : 55,hidden:true},
            {name : 'materialNo',index:'materialNo',width : 50},
            {name : 'materialName',index:'materialName',width : 50},
            {name : 'unit',index:'unit',width : 50},
           // {name : 'stockAmount',index:'stockAmount',width:50},
            {name : 'avalibleAmount',index:'avalibleAmount',width:50},
            {name : 'amount',index:'amount',width : 50},
            {name : 'state',index: 'state',width:50,hidden:true,formatter:function(cellvalue,object,rowObject){
                    if(cellvalue == 0){
                        return "<span style='color:green;font-weight:bold;'>已出库</span>";
                    }if(cellvalue == 1){
                        return "<span style='color:red;font-weight:bold;'>未出库</span>";
                    }
                    if(cellvalue == 2){
                        return "<span style='color:orange;font-weight:bold;'>出库中</span>";
                    }
                }}
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : '#grid-pager-c',
        sortname : 'ID',
        sortorder : "asc",
        altRows: true,
        viewrecords : true,
        autowidth:true,
        multiselect:true,
        multiboxonly: true,
        loadComplete : function(data){
            var table = this;
            setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            oriData = data;
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据",
    });
    //在分页工具栏中添加按钮
    jQuery("#grid-table-c").navGrid('#grid-pager-c',{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd('#grid-pager-c',{
  	   caption:"",
  	   buttonicon:"ace-icon fa fa-refresh green",
  	   onClickButton: function(){
  	    $("#grid-table-c").jqGrid('setGridParam',
				{
					postData: {id:$('#id').val()} //发送数据  :选中的节点
				}
		  ).trigger("reloadGrid");
  	   }
  	});
  	$(window).on("resize.jqGrid", function () {
  		$("#grid-table-c").jqGrid("setGridWidth", $("#grid-div-c").width() - 3 );
        var height = $(".layui-layer-title",_grid.parents(".layui-layer")).height()+
                     $("#div1").outerHeight(true)+$("#div2").outerHeight(true)+
                   //$("#gview_grid-table-c .ui-jqgrid-titlebar").outerHeight(true)+
                     $("#gview_grid-table-c .ui-jqgrid-hbox").outerHeight(true)+
                     $("#grid-pager-c").outerHeight(true);
  		             $("#grid-table-c").jqGrid("setGridHeight", _grid.parents(".layui-layer").height()-height);
  	});
  	$(window).triggerHandler("resize.jqGrid");
});

//将数据格式化成两位小数：四舍五入
function formatterNumToFixed(value,options,rowObj){
	if(value!=null){
		if(rowObj.id==-1){
			return "";
		}else{
			var floatNum = parseFloat(value);
			return floatNum.toFixed(2);
		}		
	}else{
		return "0.00";
	}
}
function printDoc(){
    var url = context_path + "/getbills/printGetbillsDetail?putId="+$("#id").val();
	window.open(url);
}
</script>