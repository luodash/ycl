<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<div id="grid-div">
    <!-- 隐藏区域：存放查询条件 -->
    <form id="hiddenQueryForm" action="<%=path%>/crossDockPlan/exportExcel" method="POST" style="display:none;">
		<input name="ids" id="ids" value="" />
        <input id="outSourcingNo" name="outSourcingNo" value="" />
        <input id="operationTime" name="operationTime" value="" />
		<input id="remark" name="remark" value="" />
		<input id="supplierId" name="supplierId" value="" />
    </form>
    <div class="query_box" id="yy" title="查询选项">
         <form id="queryForm" style="max-width:100%;">
			 <ul class="form-elements">
				<li class="field-group field-fluid3">
					<label class="inline" for="outSourcingNo" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:92px;">外协出库编号：</span>
						<input type="text" id="outSourcingNo" name="outSourcingNo" style="width: calc(100% - 97px);" placeholder="外协出库编号">
					</label>			
				</li>											
				<li class="field-group field-fluid3">
					<label class="inline" for="operationTime" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:92px;">开始时间：</span>
						<input type="text" id="operationTime" name="operationTime" class="date-picker" style="width: calc(100% - 97px);" placeholder="开始时间">
					</label>			
				</li>							
				<li class="field-group field-fluid3">
					<label class="inline" for="remark" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:92px;">结束时间：</span>
						<input type="text" id="remark" name="remark" class="date-picker" style="width: calc(100% - 97px);" placeholder="结束时间">
					</label>			
				</li>
				<li class="field-group-top field-group field-fluid3">
					<label class="inline" for="supplierId" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:92px;">外协出库厂商：</span>
						<input type="text" id="supplierId" name="supplierId" style="width: calc(100% - 97px);" placeholder="外协出库厂商">
					</label>			
				</li>							
				
			</ul>
			<div class="field-button">
					<div class="btn btn-info" onclick="queryOk();">
				        <i class="ace-icon fa fa-check bigger-110"></i>查询
			        </div>
				    <div class="btn" onclick="reset();"><i class="ace-icon icon-remove"></i>重置</div>
				    <a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>
		        </div>
		  </form>		 
    </div>
    <div id="grid-div" style="width:100%;margin:0px auto;">
        <div id="fixed_tool_div" class="fixed_tool_div">
            <div id="__toolbar__" style="float:left;overflow:hidden;"></div>
        </div>
        <table id="grid-table" style="width:100%;height:100%;overflow:auto;"></table>
        <div id="grid-pager"></div>
    </div>
</div>
<script type="text/javascript" src="<%=path%>/static/js/techbloom/wms/outsourcing/outsourcing.js"></script>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriData;
    var _grid;
    var dynamicDefalutValue="17e870237a2d470fb170daeafe6292e1";//列表码
    $(function(){
       $(".toggle_tools").click();
    });
    $("#queryForm .mySelect2").select2();
    $("#__toolbar__").iToolBar({
        id:"__tb__01",
        items:[
            {label: "添加", disabled:(${sessionUser.addQx}==1?false:true),onclick:addCrossPlan,iconClass:'icon-plus'},
            {label: "编辑", disabled:(${sessionUser.editQx}==1?false:true),onclick:editCrossPlan,iconClass:'icon-pencil'},
            {label: "查看", disabled:(${sessionUser.queryQx} == 1? false : true),onclick:viewDetailList, iconClass:'icon-zoom-in'},
            {label: "打印", disabled:(${sessionUser.deleteQx}==1?false:true),onclick:printCrossPlan,iconClass:' icon-print'}
        ]
    });
    _grid = jQuery("#grid-table").jqGrid({
        url : context_path + "/outSourcing/toList",
        datatype : "json",
        colNames : [ "外协出库单主键","外协出库单号","外协出库厂商","操作人","操作时间","备注","状态"],
        colModel : [
            {name : "id",index : "id",width : 20,hidden:true},
            {name : "outSourcingNo",index : "outliningNo",width : 40},
            {name : "supplierName",index : "supplierName",width : 40},
            {name : "userName",index : "userName",width : 40},
            {name : "operationTime",index : "operationTime",width :40,
            	formatter:function(cellValu,option,rowObject){
                    if (cellValu != null) {
                        return getFormatDateByLong(new Date(cellValu), "yyyy-MM-dd");
                    } else {
                        return "";
                    }
                }
            },
           	{name : "remark",index : "remark",width : 50,},
            {name : "state",index : "state",width : 65,
                formatter:function(cellValu,option,rowObject){
                    if(cellValu==0){
                        return "生产订单生成";
                    }
                    if(cellValu==1){
                        return "销售订单生成";
                    }
                    if(cellValu==2){
                        return "手动生成";
                    }
                }
            },
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : "#grid-pager",
        sortname : "oc.id",
        sortorder : "desc",
        altRows: false,
        viewrecords : true,
        autowidth:true,
        multiselect:true,
        multiboxonly: true,
        beforeRequest:function (){
            dynamicGetColumns(dynamicDefalutValue,"grid-table",$(window).width()-$("#sidebar").width() -7);
            //重新加载列属性
        },
        loadComplete : function(data){
            var table = this;
            setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            oriData = data;
            $(window).triggerHandler("resize.jqGrid");
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
    });
    //在分页工具栏中添加按钮
    jQuery("#grid-table").navGrid("#grid-pager",{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd('#grid-pager',{
            caption:"",
            buttonicon:"ace-icon fa fa-refresh green",
            onClickButton: function(){
                //jQuery("#grid-table").trigger("reloadGrid");  //重新加载表格
                $("#grid-table").jqGrid("setGridParam",
                    {
                        postData: {queryJsonString:""} //发送数据
                    }
                ).trigger("reloadGrid");
            }
        }).navButtonAdd("#grid-pager",{
            caption: "",
            buttonicon:"fa icon-cogs",
            onClickButton : function(){
                jQuery("#grid-table").jqGrid("columnChooser",{
                    done: function(perm, cols){
                        dynamicColumns(cols,dynamicDefalutValue);
                        $("#grid-table").jqGrid("setGridWidth", $("#grid-div").width()-3);
                    }
                });
            }
        });
    $(window).on("resize.jqGrid", function() {
        $("#grid-table").jqGrid("setGridWidth", $("#grid-div").width() - 3 );
        var height = $("#breadcrumb").outerHeight(true)+$(".query_box").outerHeight(true)+
                     $("#fixed_tool_div").outerHeight(true)+
                     $("#gview_grid-table .ui-jqgrid-hbox").outerHeight(true)+
                     $("#grid-pager").outerHeight(true)+$("#header").outerHeight(true);
                     $("#grid-table").jqGrid("setGridHeight", (document.documentElement.clientHeight)-height);
    });
    //  });
    var _queryForm_data = iTsai.form.serialize($('#queryForm'));

    $('#queryForm .mySelect2').select2();
    $('#outstorageTypeSelect').change(function(){
        $('#queryForm #crossType').val($('#outstorageTypeSelect').val());
    });

    $('#areaSelect').change(function(){
        $('#queryForm #areaId').val($('#areaSelect').val());
    });
function queryOk(){
        var queryParam = iTsai.form.serialize($('#queryForm'));
        queryByParam(queryParam);

    }
//查询的方法
function queryByParam(jsonParam){
    //序列化表单：iTsai.form.serialize($('#frm'))
    //反序列化表单：iTsai.form.deserialize($('#frm'),json)
    iTsai.form.deserialize($('#hiddenQueryForm'),jsonParam);   //将json对象反序列化到列表页面中隐藏的form中
    var queryParam = iTsai.form.serialize($('#hiddenQueryForm'));
    var queryJsonString = JSON.stringify(queryParam);         //将json对象转换成json字符串
    //执行查询操作
    $("#grid-table").jqGrid("setGridParam",
        {
            postData: {queryJsonString:queryJsonString} //发送数据
        }
    ).trigger("reloadGrid");
}
    function reset(){
        $("#queryForm #supplierId").select2("val","");
        $("#queryForm #operationTime").val("");
        $("#queryForm #remark").val("");
        $("#queryForm #outSourcingNo").val("");
        queryByParam(_queryForm_data);
    }
    $(".date-picker").datetimepicker({format: "YYYY-MM-DD",useMinutes:true,useSeconds:true});
    
    $("#queryForm #supplierId").select2({
        placeholder: "选择供应商",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/ASNmanage/getSelectSupply",
            type: "POST",
            dataType: 'json',
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });
</script>