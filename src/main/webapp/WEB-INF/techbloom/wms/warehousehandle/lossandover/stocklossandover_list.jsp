<%--
  Created by IntelliJ IDEA.
  User: 汪长军
  Date: 2017/10/26
  Time: 14:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <script type="text/javascript">
        var context_path = '<%=path%>';
    </script>
    <style type="text/css">
        .query_box .field-button.two {
            padding: 0px;
            left: 330px;
        }
    </style>
</head>
<body style="overflow:hidden;">
<div id="grid-div">
    <!-- 隐藏区域：存放查询条件 -->
    <form id="exportStockCheckForm"  action ="<%=path%>/stockLossOver/exportStockCheckList" style="display:none;">
        <!-- 盘点任务编号 -->
        <input name="ids" id="ids" value="" />
    </form>
    <div class="query_box" id="yy" title="查询选项">
            <form id="queryForm" style="max-width:100%;">
			 <ul class="form-elements">
				<li class="field-group field-fluid2">
					<label class="inline" for="lossOverNo" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:92px;">盘点记录编号：</span>
						<input type="text" id="lossOverNo" name="lossOverNo" style="width: calc(100% - 97px);" placeholder="盘点记录编号">
					</label>			
				</li>
				<li class="field-group field-fluid2">
					<label class="inline" for="lossOverUserId" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:92px;">结果处理人员：</span>
						<input type="text" id="user" name="lossOverUserId" style="width: calc(100% - 97px);" placeholder="结果处理人员">
					</label>					
				</li>
				
			</ul>
			<div class="field-button" style="">
					<div class="btn btn-info" onclick="queryOk();">
				        <i class="ace-icon fa fa-check bigger-110"></i>查询
			        </div>
				    <div class="btn" onclick="reset();"><i class="ace-icon icon-remove"></i>重置</div>
		        </div>
		  </form>		 
    </div>
    <div id="fixed_tool_div" class="fixed_tool_div">
        <div id="__toolbar__" style="float:left;overflow:hidden;margin:0px !important;border: 0px  !important"></div>
    </div>
    <table id="grid-table" style="width:100%;height:100%;margin:0px !important;border: 0px  !important"></table>
    <div id="grid-pager" style="margin:0px !important;border: 0px  !important"></div>
</div>
</body>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript" src="<%=path%>/static/js/techbloom/wms/warehousehandle/stocklossandover/stocklossandover.js"></script>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriData;
    var _grid;
    var dynamicDefalutValue="8b974cde29ad428c9b05ec05d2409c37";
    $(function  (){
       $(".toggle_tools").click();
    });
    $("#__toolbar__").iToolBar({
        id:"__tb__01",
        items:[
            {label:"添加",disabled:(${sessionUser.addQx}==1?false:true),onclick:addStockLossOver,iconClass:'glyphicon glyphicon-plus'},
            {label:"编辑",disabled:(${sessionUser.editQx}==1?false:true),onclick:editStockLossOver,iconClass:'glyphicon glyphicon-pencil'},
            {label:"删除",disabled:(${sessionUser.deleteQx}==1?false:true),onclick:deleteStockLossOver,iconClass:'glyphicon glyphicon-trash'},
            {label:"导出",disabled:(${sessionUser.queryQx}==1?false:true),onclick:excelStocklossOver,iconClass:'icon-share'}
            ]
    });
    $(function()
    {
        _grid = jQuery("#grid-table").jqGrid({
            url : context_path + '/stockLossOver/stockLossOverList.do',
            datatype : "json",
            colNames : [ '盘亏盘盈主键','盘点记录编号','盘点任务编号', '盘点结果时间','结果处理人员','备注','状态'],
            colModel : [
                {name : 'id',index : 'id',width : 20,hidden:true},
                {name : 'lossOverNo',index : 'lossOverNo',width : 40},
                {name : 'stockCheckNo',index : 'stockCheckNo',width : 40},
                {name : 'lossOverTime',index : 'lossOverTime',width : 40},
                {name : 'lossOverUserName',index : 'lossOverUserName',width : 40},
                {name : 'remark',index : 'REMARK',width : 40},
                {name : 'lossOverState',index : 'lossOverState',width : 40,
                    formatter:function(cellValue,option,rowObject){
                        if(cellValue==0){
                            return "<span style='color:#d15b47;font-weight:bold;'>未提交</span>";
                        }else if(cellValue==1){
                            return "<span style='color:#76b86b;font-weight:bold;'>已提交</span>";
                        }
                    }
                }
            ],
            rowNum : 20,
            rowList : [ 10, 20, 30 ],
            pager : '#grid-pager',
            sortname : 'ID',
            sortorder : "desc",
            altRows: false,
            viewrecords : true,
            autowidth:true,
            multiselect:true,
            multiboxonly: true,
            beforeRequest:function (){
                dynamicGetColumns(dynamicDefalutValue,"grid-table",$(window).width()-$("#sidebar").width() -7);
                //重新加载列属性
            },
            loadComplete : function(data){
                var table = this;
                setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
                oriData = data;
            },
            emptyrecords: "没有相关记录",
            loadtext: "加载中...",
            pgtext : "页码 {0} / {1}页",
            recordtext: "显示 {0} - {1}共{2}条数据"
        });
        //在分页工具栏中添加按钮
        jQuery("#grid-table").navGrid('#grid-pager',{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd('#grid-pager',{
                caption:"",
                buttonicon:"ace-icon fa fa-refresh green",
                onClickButton: function(){
                    //jQuery("#grid-table").trigger("reloadGrid");  //重新加载表格
                    $("#grid-table").jqGrid('setGridParam',
                        {
                            postData: {queryJsonString:""} //发送数据
                        }
                    ).trigger("reloadGrid");
                }
            }).navButtonAdd('#grid-pager',{
                caption: "",
                buttonicon:"fa  icon-cogs",
                onClickButton : function (){
                    jQuery("#grid-table").jqGrid('columnChooser',{
                        done: function(perm, cols){
                            dynamicColumns(cols,dynamicDefalutValue);
                            $("#grid-table").jqGrid( 'setGridWidth', $("#grid-div").width()-3);
                        }
                    });
                }
            });
        $(window).on('resize.jqGrid', function () {
            $("#grid-table").jqGrid( 'setGridWidth', $("#grid-div").width() - 3 );
            $("#grid-table").jqGrid( 'setGridHeight', $(".container-fluid").height()-$(".query_box").outerHeight(true)-$("#fixed_tool_div").outerHeight(true)-$("#grid-pager").outerHeight(true)
           -$("#gview_grid-table .ui-jqgrid-hbox").outerHeight(true));
        })

        $(window).triggerHandler('resize.jqGrid');
    });
    var _queryForm_data = iTsai.form.serialize($('#queryForm'));
    function queryOk(){
        //var formJsonParam = $('#queryForm').serialize();
        var queryParam = iTsai.form.serialize($('#queryForm'));
        //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
        queryLossOverListByParam(queryParam);

    }

    /**
     * 盘点记录查询功能:获取查询页面中的值，并将值放入列表页面中隐藏的form
     * @param jsonParam     查询页面传递过来的json对象
     */
    function queryLossOverListByParam(jsonParam){
        //序列化表单：iTsai.form.serialize($('#frm'))
        //反序列化表单：iTsai.form.deserialize($('#frm'),json)
        var queryJsonString = JSON.stringify(jsonParam);         //将json对象转换成json字符串
        //执行查询操作
        $("#grid-table").jqGrid('setGridParam',
            {
                postData: {queryJson:queryJsonString} //发送数据
            }
        ).trigger("reloadGrid");
    }

    $('#allocateTypeSelect').change(function(){
        $('#queryForm #allocateType').val($('#allocateTypeSelect').val());
    });

    $('#inWarehouseSelect').change(function(){
        $('#queryForm #inWarehouse').val($('#inWarehouseSelect').val());
    });

    $('#outWarehouseSelect').change(function(){
        $('#queryForm #outWarehouse').val($('#outWarehouseSelect').val());
    });
    function reset(){
        //var formJsonParam = $('#queryForm').serialize();
        iTsai.form.deserialize($('#queryForm'),_queryForm_data);
        $("#allocateTypeSelect").val("").trigger('change');
        $("#inWarehouseSelect").val("").trigger('change');
        $("#outWarehouseSelect").val("").trigger('change');
        $("#queryForm #user").select2("val","");
        //var queryParam = iTsai.form.serialize($('#queryForm'));
        //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
        queryLossOverListByParam(_queryForm_data);
        $("#allocateTypeSelect").find("option[text='所有类型']").attr("selected",true);
        $("#outWarehouseSelect").find("option[text='所有库区']").attr("selected",true);
        $("#inWarehouseSelect").find("option[text='所有库区']").attr("selected",true);
    }
    $('#queryForm .mySelect2').select2();
    $(".date-picker").datetimepicker({format: 'YYYY-MM-DD',useMinutes:true,useSeconds:true});
    function reloadGrid(){
        _grid.trigger("reloadGrid");
    }
    $("#queryForm #user").select2({
    placeholder: "选择盘点人员",
    minimumInputLength: 0, //至少输入n个字符，才去加载数据
    allowClear: true, //是否允许用户清除文本信息
    delay: 250,
    formatNoMatches: "没有结果",
    formatSearching: "搜索中...",
    formatAjaxError: "加载出错啦！",
    ajax: {
        url: context_path + "/ASNmanage/getSelectUser",
        type: "POST",
        dataType: 'json',
        delay: 250,
        data: function (term, pageNo) { //在查询时向服务器端传输的数据
            term = $.trim(term);
            return {
                queryString: term, //联动查询的字符
                pageSize: 15, //一次性加载的数据条数
                pageNo: pageNo, //页码
                time: new Date()
                //测试
            }
        },
        results: function (data, pageNo) {
            var res = data.result;
            if (res.length > 0) { //如果没有查询到数据，将会返回空串
                var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                return {
                    results: res,
                    more: more
                };
            } else {
                return {
                    results: {}
                };
            }
        },
        cache: true
    }
});
</script>
</html>
