<%@ page language="java" import="java.lang.*" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
%>
<div class="row-fluid" style="height: inherit;margin:0px;border: 0px">
    <form action="" class="form-horizontal" id="lossOverForm" name="materialForm" method="post" target="_ifr">
        <input type="hidden" id="lossAndOverId" name="id" value="${lossOverId}">
        <input type="hidden" id="stockCheckTask"   value="">
        <%--一行数据 --%>
        <div class="row" style="margin:0;padding:0;">
            <%--  结果编号--%>
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="lossOverNo" >结果编号：</label>
                <div class="controls">
                    <div class="input-append  required span12" >
                        <input type="text" id="lossOverNo" class="span10" name="lossOverNo"   placeholder="结果编号" value="${lossOverNo}"/>
                    </div>
                </div>
            </div>
            <%--盘点任务--%>
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="stockCheckId" >盘点任务：</label>
                <div class="controls">
                    <div class="required span12" style=" float: none !important;">
                        <input type="text" class="span10" id="stockCheckId" name="stockCheckId" placeholder="盘点任务" />
                    </div>
                </div>
            </div>
        </div>

        <div class="row" style="margin:0;padding:0;">
            <%--处理时间--%>
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="lossOverTime" >处理时间：</label>
                <div class="controls">
                    <div class="required  span12">
                        <input class="form-control date-picker span10" type="text" id="lossOverTime" name="lossOverTime"  placeholder="处理时间">
                    </div>
                </div>
            </div>
            <%--处理人员;--%>
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="lossOverUserId" >处理人员：</label>
                <div class="controls">
                    <div class="required span12" style="float: none !important;">
                        <input id="lossOverUserId" name="lossOverUserId"
                               type="text" value=""
                               placeholder="处理人员" class="span10"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row" style="margin:0;padding:0;">
            <%--="备注--%>
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="remark" >备注：</label>
                <div class="controls">
                    <div class="span12" style=" float: none !important;">
                        <input class="span10" type="text"  id="remark" name="remark" placeholder="备注" >
                    </div>
                </div>
            </div>
        </div>
        <div style="margin-left:10px;">
            <span class="btn btn-info" id="formSave">
		       <i class="ace-icon fa fa-check bigger-110"></i>保存
            </span>
            <span class="btn btn-info" onclick="comfirmStockLossOver()">
		       <i class="ace-icon fa fa-check bigger-110"></i>提交
            </span>
        </div>
    </form>
    <div id="shelvesLocationDiv" style="margin: 10px;">
        <!-- 下拉框 -->
        <label class="inline" for="shelvesLocation">库位：</label>
        <input type="text" id="shelvesLocation" name="shelvesLocation"
               style="width:350px;margin-right:10px;"/>
        <button id="addShelvesLocationBtn" class="btn btn-xs btn-primary" onclick="addShelvesLocation();">
            <i class="icon-plus" style="margin-right:6px;"></i>添加
        </button>
    </div>
    <!-- 表格div -->
    <div id="grid-div-c" style="width:100%;margin:0px;border:0px;">
        <!-- 	表格工具栏 -->
        <div id="fixed_tool_div" class="fixed_tool_div detailToolBar">
            <div id="__toolbar__-c" style="float:left;overflow:hidden;"></div>
        </div>
        <!-- 物料详情信息表格 -->
        <table id="grid-table-c" style="width:100%;height:100%;"></table>
        <!-- 表格分页栏 -->
        <div id="grid-pager-c"></div>
    </div>
</div>
<script type="text/javascript" src="<%=path%>/static/js/techbloom/wms/warehousehandle/stocklossandover/stocklossandover.js"></script>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriDataDetail;
    var _grid_detail;        //表格对象
    var lastsel2;
    var selectData = "";
    //单元格编辑成功后，回调的函数
    var editFunction = function eidtSuccess(XHR) {
        var data = eval("(" + XHR.responseText + ")");
        if (data["msg"] != "") {
            layer.alert(data["msg"]);
        }
        jQuery("#grid-table-c").jqGrid('setGridParam',
            {
                postData: {
                    id: $('#id').val(),
                    queryJsonString: ""
                }
            }
        ).trigger("reloadGrid");
    };

    _grid_detail = jQuery("#grid-table-c").jqGrid({
        url: context_path + '/stockLossOver/stockLossOverDetailList?lossOverId=' + $("#lossAndOverId").val(),
        datatype: "json",
        colNames: ['盘点详情主键', '库位编号', '库位名称', '条码', '盘点数量'],
        colModel: [
            {name: 'id', index: 'id', width: 20, hidden: true},
            {name: 'locationNo', index: 'locationNo', width: 20},
            {name: 'locationName', index: 'locationName', width: 20},
            {name: 'materialBar', index: 'materialBar', width: 20},
           /* {name: 'materialNo', index: 'materialNo', width: 20},
            {name: 'materialName', index: 'materialName', width: 20},*/
            {
                name: 'stockCheckAmount', index: 'stockCheckAmount', width: 20, editable: true,
                editoptions: {
                    size: 25,
                    dataEvents: [
                        {
                            type: 'blur',     //blur,focus,change.............
                            fn: function (e) {
                                var $element = e.currentTarget;
                                var $elementId = $element.id;
                                var rowid = $elementId.split("_")[0];
                                var id = $element.parentElement.parentElement.children[1].textContent;
                                var indocType = 1;
                                var reg = new RegExp("^[0-9]+(.[0-9]{1,2})?$");
                                if (!reg.test($("#" + $elementId).val())) {
                                    layer.alert("非法的数量！(注：可以有两位小数的正实数)");
                                    return;
                                }
                                $.ajax({
                                    url: context_path + '/stockLossOver/updateStockCheckAmount',
                                    type: "POST",
                                    data: {
                                        lossOverDetailId: id,
                                        lossOverAmount: $("#" + rowid + "_stockCheckAmount").val()
                                    },
                                    dataType: "json",
                                    success: function (data) {
                                        if (!data.result) {
                                            layer.alert(data.message);
                                        }
                                        $("#grid-table-c").jqGrid('setGridParam',
                                            {
                                                postData: {queryJsonString: "", stockCheckId: $("#stockCheckId").val()} //发送数据  :选中的节点
                                            }
                                        ).trigger("reloadGrid");

                                    }
                                });
                            }
                        }
                    ]
                }
            }
        ],
        rowNum: 20,
        rowList: [10, 20, 30],
        pager: '#grid-pager-c',
        sortname: 'ID',
        sortorder: "asc",
        altRows: true,
        viewrecords: true,
        caption: "物料列表",
        autowidth: true,
        multiselect: true,
        multiboxonly: true,
        //   cellsubmit:'sadsadsad',
        cellurl: context_path + '/stockCheck/updateStockCheckAmount',
        loadComplete: function (data) {
            var table = this;
            setTimeout(function () {
                updatePagerIcons(table);
                enableTooltips(table);
            }, 0);
            oriDataDetail = data;
            $(window).triggerHandler('resize.jqGrid');
        },
        cellEdit: true,
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext: "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据",
    });
    //在分页工具栏中添加按钮
    $("#grid-table-c").navGrid('#grid-pager-c', {edit: false, add: false, del: false, search: false, refresh: false})
        .navButtonAdd('#grid-pager-c', {
            caption: "",
            buttonicon: "ace-icon fa fa-refresh green",
            onClickButton: function () {
                $("#grid-table-c").jqGrid('setGridParam',
                    {
                        url: context_path + '/stockLossOver/stockLossOverDetailList',
                        postData: {queryJsonString: "", lossOverId: $("#lossAndOverId").val()} //发送数据  :选中的节点
                    }
                ).trigger("reloadGrid");
            }
        });

    $(window).on('resize.jqGrid', function () {
        var height=$(".layui-layer-title",_grid_detail.parents(".layui-layer") ).outerHeight(true)+
            $("#lossOverForm").outerHeight(true)+$("#shelvesLocationDiv").outerHeight(true)+
            $("#grid-pager-c").outerHeight(true)+$("#fixed_tool_div.fixed_tool_div.detailToolBar").outerHeight(true)+$("#gview_grid-table-c .ui-jqgrid-titlebar").outerHeight(true)+$("#gview_grid-table-c .ui-jqgrid-hbox").outerHeight(true);
        $("#grid-table-c").jqGrid('setGridWidth', $("#grid-div-c").width()-3);
        $("#grid-table-c").jqGrid('setGridHeight',_grid_detail.parents(".layui-layer").outerHeight(true)-height);
    });
    $(window).triggerHandler('resize.jqGrid');

    $('#shelvesLocation').select2({
        placeholder: "请选择库位",//文本框的提示信息
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        multiple: true,
        closeOnSelect: false,
        ajax: {
            url: context_path + '/stockLossOver/getStockCheckLocationList',
            dataType: 'json',
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                selectParam = term;
                return {
                    /* docId : $("#baseInfor #id").val(), */
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date(),
                    content: $("#stockCheckTask").val()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {
                            "id": "0",
                            "text": "没有更多结果"
                        }
                    };
                }

            },
            cache: true
        }
    });

    $('#shelvesLocation').on("change", function (e) {
        var datas = $("#shelvesLocation").select2("val");
        selectData = datas;
        var selectSize = datas.length;
        if (selectSize > 1) {
            var $tags = $("#s2id_shelvesLocation .select2-choices");   //
            var $choicelist = $tags.find(".select2-search-choice");
            var $clonedChoice = $choicelist[0];
            $tags.children(".select2-search-choice").remove();
            $tags.prepend($clonedChoice);
            $tags.find(".select2-search-choice").find("div").html(selectSize + "个被选中");
            $tags.find(".select2-search-choice").find("a").removeAttr("tabindex");
            $tags.find(".select2-search-choice").find("a").attr("href", "#");
            $tags.find(".select2-search-choice").find("a").attr("onclick", "removeChoice();");
        }
        //执行select的查询方法
        $("#materialInfor").select2("search", selectParam);
    });


    //清空物料多选框中的值
    function removeChoice() {
        $("#s2id_shelvesLocation .select2-choices").children(".select2-search-choice").remove();
        $("#shelvesLocation").select2("val", "");
        selectData = 0;
    }

    //添加库位详情
    function addShelvesLocation() {
        if ($("#stockCheckId").val() == "") {
            layer.alert("请先保存表单信息！");
            return;
        }
        if (selectData != 0) {
            //将选中的物料添加到数据库中
            $.ajax({
                type: "POST",
                url: context_path + '/stockLossOver/addStockCheckDetail',
                data: {lossOverId: $('#lossAndOverId').val(), locationIds: selectData.toString()},
                dataType: "json",
                success: function (data) {
                    removeChoice();   //清空下拉框中的值
                    if (Boolean(data.result)) {
                        layer.msg("添加成功", {icon: 1, time: 1200});
                        //重新加载详情表格
                        $("#grid-table-c").jqGrid('setGridParam',
                            {
                                postData: {stockCheckId: $("#stockCheckForm #stockCheckId").val()} //发送数据  :选中的节点
                            }
                        ).trigger("reloadGrid");
                    } else {
                        layer.alert(data.msg, {icon: 2, time: 1200});
                    }
                }
            });
        } else {
            layer.alert("请选择库位！");
        }
    }

    //工具栏
    $("#__toolbar__-c").iToolBar({
        id: "__tb__01",
        items: [
            {label: "删除", onclick: delLossOverDetail},
        ]
    });


    $(document).ready(function () {
        //初始化盘点任务信息
        $.ajax({
            url: context_path + "/stockLossOver/getLossOverById?tm=" + new Date(),
            type: "POST",
            data: {lossOverId: $("#lossAndOverId").val()},
            dataType: "JSON",
            success: function (data) {
                if (data) {
                    //将盘点信息填充到form中
                    $("#lossOverNo").val(data.lossOverNo);
                    $("#lossOverTime").val(data.lossOverTime);
                    $("#stockCheckTask").val(data.stockCheckId);
                    $("#remark").val(data.remark);
                    $("#stockCheckId").select2("data", {
                        id: data.stockCheckId,
                        text: data.stockCheckNo
                    });
                    $("#lossOverUserId").select2("data", {id: data.lossOverUserId, text: data.lossOverUserName});
                }
            }
        });
    });

    /**
     * 盘点详情审核
     */
    function auditLossOverDetail(lossOverDetailId, lossOverState) {
        $.ajax({
            url: context_path + "/stockLossOver/auditLossOverDetail?tm=" + new Date(),
            type: "POST",
            data: {lossOverDetailId: lossOverDetailId, lossOverState: lossOverState},
            dataType: "JSON",
            success: function (data) {
                if (data.result) {
                    $("#grid-table-c").jqGrid('setGridParam',
                        {
                            postData: {stockCheckId: $("#stockCheckId").val()} //发送数据  :选中的节点
                        }
                    ).trigger("reloadGrid");
                }
                else {
                    layer.msg(data.message, {icon: 2, time: 1200});
                }
            }
        });
    }

</script>
