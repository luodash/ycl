<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="grid-div">
    <form id="hiddenForm" action="<%=path%>/outLiningStock/toExcel.do" method="POST" style="display: none;">
        <input id="ids" name="ids" value=""/>
    </form>
    <form id="hiddenQueryForm" style="display:none;">
        <input id="materialNo" name="materialNo" value=""/>
        <input id="materialName" name="carName" value="">
        <input id="rfid" name="rfid" value=""/>
        <input id="shelveId" name="shelveId" value="">
        <input id="type" name="type" value="">
    </form>
    <div class="query_box" id="yy" title="查询选项">
            <form id="queryForm" style="max-width:100%;">
			 <ul class="form-elements">
				<li class="field-group field-fluid3">
					<label class="inline" for="materialNo" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:80px;">物料编号：</span>
						<input id="materialNo" name="materialNo" type="text" style="width: calc(100% - 85px);" placeholder="物料编号">
					</label>			
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="materialName" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:80px;">物料名称：</span>
						<input id="materialName" name="materialName" type="text" style="width: calc(100% - 85px);" placeholder="物料名称">
					</label>				
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="outName" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:80px;">外协库名称：</span>
						<input id="outName" name="outName" type="text" style="width: calc(100% - 85px);" placeholder="外协库名称">
					</label>			
				</li>
			
			</ul>
				<div class="field-button" style="">
					<div class="btn btn-info" onclick="queryOk();">
				        <i class="ace-icon fa fa-check bigger-110"></i>查询
			        </div>
					<div class="btn" onclick="reset();"><i class="ace-icon icon-remove"></i>重置</div>
					<a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>
		        </div>
		  </form>		 
    </div>
    <div id="fixed_tool_div" class="fixed_tool_div">
        <div id="__toolbar__" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="grid-table" style="width:100%;height:100%;"></table>
    <div id="grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
var oriData; 
var _grid;
var openwindowtype = 0; //打开窗口类型：0新增，1修改
var selectid;
$(function  (){
    $(".toggle_tools").click();
});
$("#__toolbar__").iToolBar({
    id: "__tb__01",
    items: [
        {label: "导出", disabled: ( ${sessionUser.queryQx}==1 ? false : true),onclick:function(){toExcel();},iconClass:'icon-share'},
        {label: "打印",disabled:(${sessionUser.queryQx}==1?false:true),onclick:toPrint,iconClass:'icon-print'}
   ]
});
var _queryForm_data = iTsai.form.serialize($("#queryForm"));
_grid = jQuery("#grid-table").jqGrid({
				url : context_path + "/outLiningStock/list.do",
			    datatype : "json",
			    colNames : [ "主键","物料编号", "物料名称","供应商名称","外协库名称","数量","外协入库时间","外协出库时间"],
			    colModel : [ 
			                 {name : "id",index : "id",hidden:true},
			                 {name : "materialNo",index : "materialNo",width : 60}, 
			                 {name : "materialName",index : "materialName",width : 60},
			                 {name : "supplierName",index : "supplierName",width : 60},
			                 {name : "outName",index : "outName",width :70}, 
			                 {name : "amount",index : "amount",width : 60}, 
			                 {name : "inTime",index : "inTime",width : 60},
			                 {name : "outTime",index : "outTime",width: 60} 
			               ],
			    rowNum : 20,
			    rowList : [ 10, 20, 30 ],
			    pager : "#grid-pager",
			    sortname : "inTime",
			    sortorder : "desc",
	            altRows: true,
	            viewrecords : true,
	            hidegrid:false,
	     	    autowidth:true, 
	            multiselect:true,
	            loadComplete : function(data) {
	            	var table = this;
	            	setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
	            	oriData = data;
	            },
	            emptyrecords: "没有相关记录",
	            loadtext: "加载中...",
	            pgtext : "页码 {0} / {1}页",
	            recordtext: "显示 {0} - {1}共{2}条数据"
});

jQuery("#grid-table").navGrid("#grid-pager",{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd('#grid-pager',{  
	caption:"",   
	buttonicon:"fa fa-refresh green",   
	onClickButton: function(){   
		$("#grid-table").jqGrid("setGridParam", 
				{
			      postData: {queryJsonString:""} //发送数据 
				}
		).trigger("reloadGrid");
	}
});

$(window).on("resize.jqGrid", function () {
	$("#grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);
	$("#grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-$("#yy").outerHeight(true)-$("#fixed_tool_div").outerHeight(true)-$("#grid-pager").outerHeight(true)
   -$("#gview_grid-table .ui-jqgrid-hdiv").outerHeight(true));
});

$(window).triggerHandler("resize.jqGrid");
//重新加载表格
function gridReload(){
	_grid.trigger("reloadGrid");  //重新加载表格  
}
/**
 * 查询功能:获取查询页面中的值，并将值放入列表页面中隐藏的form
 * @param jsonParam     查询页面传递过来的json对象
 */
function queryLogListByParam(jsonParam){
	var queryJsonString = JSON.stringify(jsonParam);         //将json对象转换成json字符串
	//执行查询操作
	$("#grid-table").jqGrid("setGridParam", 
			{
		       postData: {queryJsonString:queryJsonString} //发送数据 
			}
	).trigger("reloadGrid");
}
/**
 * 查询按钮点击事件
 */
 function queryOk(){
	 var queryParam = iTsai.form.serialize($("#queryForm"));
	 //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
	 queryByParam(queryParam);
}
 function queryByParam(jsonParam) {
	    iTsai.form.deserialize($("#hiddenQueryForm"), jsonParam);
	    var queryParam = iTsai.form.serialize($("#hiddenQueryForm"));
	    var queryJsonString = JSON.stringify(queryParam); 
	    $("#grid-table").jqGrid("setGridParam",
	        {
	            postData: {queryJsonString: queryJsonString}
	        }
	    ).trigger("reloadGrid");
}
function reset(){
     $("#queryForm #shelveId").select2("val","");
     $("#queryForm #type").select2("val","");
	 iTsai.form.deserialize($("#queryForm"),_queryForm_data); 
	 queryByParam(_queryForm_data);
}
function toExcel(){
    var ids = jQuery("#grid-table").jqGrid("getGridParam", "selarrrow");
    $("#hiddenForm #ids").val(ids);
    $("#hiddenForm").submit();	
}
$("#queryForm #shelveId").select2({
        placeholder: "选择货架",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/car/selectShelf",
            type: "POST",
            dataType: "json" ,
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });   
    $("#queryForm .mySelect2").select2();   
    $("#type").change(function(){
        $("#queryForm #type").val($("#type").val());
    });
    function toPrint(){}   
</script>