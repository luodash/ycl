<%@ page language="java" import="java.lang.*"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
  <div class="widget-box" style="border:10px;margin:10px">
	<form id="baseInfor" style="width:870px;margin:10px auto;border-bottom: solid 2px #3b73af;">
	<div id="fromInfoContent" style="margin:10px auto;">
		<!-- 隐藏的移库主键 -->
  	        <input type="hidden" id="transferId" name="id" value="${transfer.id}">
  	        <input type="hidden" id="state" name="state" value="${transfer.state}">
  	        <div class="inline" style="margin-bottom:5px;">
  	        <label class="inline" for="transferNo" style="margin-right:20px;">
				移库单号：<span class="field-required">*</span>
				<input id="transferNo" name="transferNo" value="${transfer.transferNo}" placeholder="后台自动生成" readonly="readonly">
			</label>
			<label class="inline" for="transferName" style="margin-right:20px;">
				移库单据名称：<span class="field-required">*</span>
				<input id="transferName" name="transferName" value="${transfer.transferName}" readonly="readonly">
			</label>
			</div>
			<div class="inline" style="margin-bottom:5px;">
  	        <label class="inline" for="houseId" style="margin-right:20px;">
				库区&emsp;&emsp;：<span class="field-required">*</span>
				<input id="houseId" name="houseId" value="${transfer.houseName}" placeholder="库区" style="width:205px" readonly="readonly">
			</label>
			<label class="inline" for="transferTime" style="margin-right:20px;">
				移库时间：&emsp;&emsp;<span class="field-required">*</span>
				<div class="inline" style="width: 200px;vertical-align:middle;">
					<div class="input-group">
						<input class="form-control date-picker" id="transferTime" name="transferTime" style="width: 200px;" type="text" value="${transfer.transferTime}"
						 placeholder="移库时间" readonly="readonly"/>
						<span class="input-group-addon">
							<i class="fa fa-calendar bigger-110"></i>
						</span>
					</div>
				</div>
			</label>
			</div>
			<div class="inline" style="margin-bottom:5px;">
  	        <label class="inline" for="userId" style="margin-right:20px;">
				移库人员：&nbsp;&nbsp;
				<input id="userId" name="userId" value="${transfer.userName}" placeholder="移库人员" style="width:200px" readonly="readonly">
			</label>
			</div></br>
			<div class="inline" style="margin-bottom:5px;">
  	        <label class="inline" for="remark" style="margin-right:20px;">
				备注&emsp;&emsp;：&nbsp;&nbsp;
				<input id="remark" name="remark" value="${transfer.remark}" placeholder="备注" readonly="readonly">
			</label>
			
			</div>
		</div>
	</form>
	<!-- 表格div -->
	<div id="grid-div-c" style="width:100%;margin:10px auto;">
		<!-- 	表格工具栏 -->
        <div id="fixed_tool_div" class="fixed_tool_div detailToolBar">
             <div id="__toolbar__-c" style="float:left;overflow:hidden;"></div>
        </div>
		<!-- 物料详情信息表格 -->
		<table id="grid-table-c" style="width:100%;height:100%;"></table>
		<!-- 表格分页栏 -->
		<div id="grid-pager-c"></div>
	</div>
</div>
<script type="text/javascript">
var context_path = '<%=path%>';
var transferId=$("#transferId").val();
/* var cust=${sale.customer}; */
var oriDataDetail;
 var _grid_detail;        //表格对象
 var lastsel2;
 var outLocationId=0;

  	_grid_detail=jQuery("#grid-table-c").jqGrid({
         url : context_path + '/transfer/detailList?transferId='+transferId,
         datatype : "json",
         colNames : [ '详情主键','货物类型','物料编号','物料名称','箱子条码','标签','数量','移出库位','移入库位','状态'],
         colModel : [ 
  					  {name : 'id',index : 'id',width : 20,hidden:true}, 
  					  {name : 'outType',index : 'outType',width : 20,hidden:true},
  					  {name : 'materialNo',index:'materialNo',width :20}, 
                      {name : 'materialName',index:'materialName',width : 20}, 
                      {name : 'boxCode',index:'boxCode',width : 20}, 
                      {name : 'rfid',index:'rfid',width : 20}, 
                      {name: 'amount', index: 'amount', width: 15},
                       {name : 'outLocationName',index:'outLocationName',width : 20}, 
                       {name : 'inLocationName',index:'inLocationName',width : 20}, 
                       {name : 'state',index:'state',width : 20,
                          formatter:function(cellValue,option,rowObject){
	                    		if(typeof cellValue == 'number'){
	                    			if(cellValue==0){
	                    				return "<span style='color:#76b86b;font-weight:bold;'>未开始处理</span>";
	                    			}else if(cellValue==1){
	                    				return "<div style='margin-bottom:5px' class='btn btn-xs btn-success' onclick='competeTran("+rowObject.id+","+rowObject.outType+");'>确认完成</div>";
	                    			}else if(cellValue==2){
	                    				return "<span style='color:#44BB8C;font-weight:bold;'>处理完成</span>";
	                    			}
	                    		}else{
	                    			return "异常";
	                    		}
	                    	}
                       }, 
                    ],
         rowNum : 20,
         rowList : [ 10, 20, 30 ],
         pager : '#grid-pager-c',
         sortname : 'ID',
         sortorder : "asc",
         altRows: true,
         viewrecords : true,
         caption : "移库详情列表",
         autowidth:true,
         multiselect:true,
		 multiboxonly: true,
         loadComplete : function(data) 
         {
             var table = this;
             setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
             oriDataDetail = data;
         },
    //  cellurl : context_path + "/transfer/updateAmount",
	   cellEdit: true,
	   cellsubmit : "clientArray",
  	   emptyrecords: "没有相关记录",
  	   loadtext: "加载中...",
  	   pgtext : "页码 {0} / {1}页",
  	   recordtext: "显示 {0} - {1}共{2}条数据",
    });
    //在分页工具栏中添加按钮
    $("#grid-table-c").navGrid('#grid-pager-c',{edit:false,add:false,del:false,search:false,refresh:false})
      .navButtonAdd('#grid-pager-c',{  
  	   caption:"",   
  	   buttonicon:"ace-icon fa fa-refresh green",   
  	   onClickButton: function(){   
  	    $("#grid-table-c").jqGrid('setGridParam', 
				{
  	    			url:context_path + '/transfer/detailList?transferId='+transferId,
					postData: {queryJsonString:""} //发送数据  :选中的节点
				}
		  ).trigger("reloadGrid");
  	   }
  	});
  	
  	$(window).on('resize.jqGrid', function () {
  		$("#grid-table-c").jqGrid( 'setGridWidth', $("#grid-div-c").width() - 3 );
  		$("#grid-table-c").jqGrid( 'setGridHeight', (document.documentElement.clientHeight - $("#grid-pager-c").height() - 380) );
  	});
  	$(window).triggerHandler('resize.jqGrid');
  	if($("#id").val()!=""){
  		//reloadDetailTableList();   //重新加载详情列表
  	}
	 function reloadDetailTableList(){
       $("#grid-table-c").jqGrid('setGridParam', 
				{
  	    			url:context_path + '/transfer/detailList?transferId='+transferId,
					postData: {queryJsonString:""} //发送数据  :选中的节点
				}
		  ).trigger("reloadGrid");
		  }
    //确认完成   type 0未装箱1已装箱
     function competeTran(id,type){
         var tranId=$("#transferId").val();
         $.ajax({
	      url:context_path + '/transferInferface/completeTran/'+id,
	      type:"POST",
	      dataType:"JSON",
	      success:function(data){
	          if(data.result){
	              layer.msg(data.msg,{icon:1,time:1200});
	              reloadDetailTableList();
	              gridReload();
	          } else{
	              layer.msg(data.msg,{icon:2,time:1200});
	          }
	      }
	      }); 
    }
  </script>
