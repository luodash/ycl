<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <%-- <%@ include file="/techbloom/common/taglibs.jsp"%> --%>
	<script type="text/javascript">
	    var context_path = '<%=path%>';
        var dynamicDefalutValue="724b6295cd7848df94d6737a149d62f0";
	</script>
	<style type="text/css">
	
	.query_box .field-button.two {
    padding: 0px;
    left: 650px;
    }
	</style>
</head>
<body style="overflow:hidden;">
   <div id="grid-div">
   <!-- 隐藏区域：存放查询条件 -->
		<form id="hiddenQueryForm"  action ="<%=path%>/transfer/excel" style="display:none;">
			<!-- 移库单号 -->
			<input id="transferNo" name="transferNo" value="" />
			<input name="ids" id="ids" value="" />
			<!-- 单号名称-->
			<input id="transferName" name="transferName" value="" >
			<!-- 库区 -->
			<input id="area" name="houseId" value="" />
			<!-- 移库人 -->
			<input id="user" name="userId" value="" > 
		</form>
		<div class="query_box" id="yy" title="查询选项">
            <form id="queryForm" style="max-width:100%;">
			 <ul class="form-elements">
				<li class="field-group field-fluid3">
					<label class="inline" for="transferNo" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:65px;">移库单号：</span>
						<input type="text" id="transferNo" name="transferNo" style="width: calc(100% - 70px);" placeholder="移库单号">
					</label>			
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="transferName" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:65px;">单号名称：</span>
						<input type="text" id="transferName" name="transferName" style="width: calc(100% - 70px);" placeholder="单号名称">
					</label>					
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="houseId" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:65px;">库区：</span>
						<input type="text" id="area" name="houseId" style="width:calc(100% - 70px);" placeholder="库区"/>
					</label>			
				</li>
				<li class="field-group-top field-group field-fluid3">
					<label class="inline" for="userId" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:65px;">移库人：</span>
						<input type="text" id="user" name="userId" style="width: calc(100% - 70px);" placeholder="移库人">
					</label>					
				</li>
				
			</ul>
			<div class="field-button" style="">
					<div class="btn btn-info" onclick="queryOk();">
				        <i class="ace-icon fa fa-check bigger-110"></i>查询
			        </div>
				    <div class="btn" onclick="reset();"><i class="ace-icon icon-remove"></i>重置</div>
				    <a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>
		        </div>
		  </form>		 
    </div>
        <div id="fixed_tool_div" class="fixed_tool_div">
             <div id="__toolbar__" style="float:left;overflow:hidden;"></div>
        </div>
		<table id="grid-table" style="width:100%;height:100%;"></table>
		<div id="grid-pager"></div>
   </div>
</body>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript" src="<%=path%>/static/js/techbloom/wms/warehousehandle/transfer/transfer.js"></script>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriData; 
    var _grid;
    $(function  (){
       $(".toggle_tools").click();
    });
    $("#__toolbar__").iToolBar({
   	  	 id:"__tb__01",
   	  	 items:[
   	  	    {label:"添加",disabled:(${sessionUser.addQx}==1?false:true),onclick:addTransfer,iconClass:'glyphicon glyphicon-plus'},
   	  	    {label:"编辑",disabled:(${sessionUser.editQx}==1?false:true),onclick:editAllocateOrder,iconClass:'glyphicon glyphicon-pencil'},
    		{label:"查看",disabled:(${sessionUser.editQx}==1?false:true),onclick:infoAllocateOrder,iconClass:'glyphicon glyphicon-pencil'},
	   	  	{label:"删除",disabled:(${sessionUser.deleteQx}==1?false:true),onclick:delOutStorageOrder,iconClass:'glyphicon glyphicon-trash'},
	   	  	/*{label:"开始移库",disabled:(${sessionUser.deleteQx}==1?false:true),onclick:startTran,iconClass:'icon-pencil'},*/
	   	  	{label:"移库确认",disabled:(${sessionUser.deleteQx}==1?false:true),onclick:completeTran,iconClass:'icon-ok'},
	   	  	/*{label:"导出", disabled:(${sessionUser.queryQx} == 1 ? false : true),
			        onclick:function () {
                        var ids = jQuery("#grid-table").jqGrid("getGridParam", "selarrrow");
                        $("#hiddenQueryForm #ids").val(ids);
                        $("#hiddenQueryForm").submit();
			        },
			        iconClass:' icon-share'
			    },
			    {label:"打印", disabled:(${sessionUser.queryQx} == 1 ? false : true),onclick:function(){	
					  	  	var queryBean = iTsai.form.serialize($('#hiddenQueryForm'));   //获取form中的值：json对象
				            var queryJsonString = JSON.stringify(queryBean); 
				            var url = context_path + "/replenish/exportPrint?"+"ids=" + jQuery("#grid-table").jqGrid('getGridParam', 'selarrrow');;
				            window.open(url);  
				  	  	},
				  	  	iconClass:' icon-print'}*/
				   	  	
				   	  ]
			   	});
    if(adapteRfid==0){//启用
			$("#__tb__01 .itbtn-grp")[4].style.display='none';
		}else{//禁用
			//$("#__tb__01 .itbtn-grp")[3].style.display='none';
		}
   $(function(){
		_grid = jQuery("#grid-table").jqGrid({
	       url : context_path + '/transfer/list.do',
	       datatype : "json",
	       colNames : [ '主键','移库单号','库区','移库时间', '完成时间','移库人员','备注','状态'],
	       colModel : [ 
	                    {name : 'id',index : 'id',width : 20,hidden:true}, 
	                    {name : 'transferNo',index : 'transferNo',width : 40}, 
	                    /*{name : 'transferName',index : 'transferName',width : 40}, */
	                    {name : 'houseName',index : 'houseName',width : 40}, 
	                    {name : 'transferTime',index : 'transferTime',width : 40}, 
	                    {name : 'completeTime',index : 'completeTime',width : 40}, 
	                    {name : 'userName',index : 'userName',width : 40}, 
	                    {name : 'remark',index : 'REMARK',width : 40}, 
	                    {name : 'state',index : 'state',width : 40,
	                       formatter:function(cellValue,option,rowObject){
	                    		if(typeof cellValue == 'number'){
	                    			if(cellValue==0){
	                    				return "<span style='color:#d15b47;font-weight:bold;'>未提交</span>";
	                    			}else if(cellValue==1){
	                    				return "<span style='color:#76b86b;font-weight:bold;'>已提交</span>";
	                    			}else if(cellValue==3){
	                    				return "<span style='color:#E66B1A;font-weight:bold;'>移库完成</span>";
	                    			}
	                    		}else{
	                    			return "异常";
	                    		}
	                    	}
	                    } 
	                  ],
	       rowNum : 20,
	       rowList : [ 10, 20, 30 ],
	       pager : '#grid-pager',
	       sortname : 'ID',
	       sortorder : "desc",
	       altRows: false, 
	       viewrecords : true,
	       autowidth:true,
	       multiselect:true,
			multiboxonly: true,
            beforeRequest:function (){
                dynamicGetColumns(dynamicDefalutValue,"grid-table",$(window).width()-$("#sidebar").width() -7);
                //重新加载列属性
            },
	       loadComplete : function(data){
	           var table = this;
	           setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
	           oriData = data;
	       },
	       emptyrecords: "没有相关记录",
	       loadtext: "加载中...",
	       pgtext : "页码 {0} / {1}页",
	       recordtext: "显示 {0} - {1}共{2}条数据"
	  });
	 //在分页工具栏中添加按钮
	  jQuery("#grid-table").navGrid('#grid-pager',{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd('#grid-pager',{  
		   caption:"",   
		   buttonicon:"ace-icon fa fa-refresh green",   
		   onClickButton: function(){
			   //jQuery("#grid-table").trigger("reloadGrid");  //重新加载表格  
			   $("#grid-table").jqGrid('setGridParam', 
						{
							postData: {queryJsonString:""} //发送数据 
						}
				  ).trigger("reloadGrid");
		   }
		}).navButtonAdd('#grid-pager',{
              caption: "",
              buttonicon:"fa  icon-cogs",
              onClickButton : function (){
                  jQuery("#grid-table").jqGrid('columnChooser',{
                      done: function(perm, cols){
                          dynamicColumns(cols,dynamicDefalutValue);
                          $("#grid-table").jqGrid( 'setGridWidth', $("#grid-div").width()-3);
                      }
                  });
              }
          });
	  $(window).on('resize.jqGrid', function () {
		  $("#grid-table").jqGrid( 'setGridWidth', $("#grid-div").width() - 3 );
		  $("#grid-table").jqGrid( 'setGridHeight', (document.documentElement.clientHeight - $("#grid-pager").height() - 165  -(!$(".query_box").is(":hidden") ? $(".query_box").outerHeight(true) : 0) ) );
	  })
	
	  $(window).triggerHandler('resize.jqGrid');
    }); 
   var _queryForm_data = iTsai.form.serialize($('#queryForm'));
	function queryOk(){
		//var formJsonParam = $('#queryForm').serialize();
		var queryParam = iTsai.form.serialize($('#queryForm'));
		//执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
		queryInstoreListByParam(queryParam);
		
	}
	
	function reset(){
		//var formJsonParam = $('#queryForm').serialize();
		iTsai.form.deserialize($('#queryForm'),_queryForm_data); 
		$("#allocateTypeSelect").val("").trigger('change');
		$("#inWarehouseSelect").val("").trigger('change');
		$("#outWarehouseSelect").val("").trigger('change');
		$("#queryForm #user").select2("val","");
		$("#queryForm #area").select2("val","");
		//var queryParam = iTsai.form.serialize($('#queryForm'));
		//执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
		queryInstoreListByParam(_queryForm_data);
		$("#allocateTypeSelect").find("option[text='所有类型']").attr("selected",true);
		$("#outWarehouseSelect").find("option[text='所有库区']").attr("selected",true);
		$("#inWarehouseSelect").find("option[text='所有库区']").attr("selected",true);
	}
 $('#queryForm .mySelect2').select2();
 $(".date-picker").datetimepicker({format: 'YYYY-MM-DD',useMinutes:true,useSeconds:true});
 function reloadGrid(){
   _grid.trigger("reloadGrid");
 }
 function inOrder(){
   var checkedNum = getGridCheckedNum("#grid-table","id");
	if(checkedNum == 0)
	{
    	layer.alert("请选择一条入库计划！");
    	return false;
    }else if(checkedNum >1){
    	layer.alert("只能选择一条进行操作！");
    	return false;
    } else {
    	var inPlanId = jQuery("#grid-table").jqGrid('getGridParam', 'selrow'); 
    	$.ajax({
			type : "POST",
			url:context_path + "/replenish/getInPlanById?inPlanId="+inPlanId,
			dataType : 'json',
			cache : false,
			success : function(data) {
				if(data.state==1){
				$.get( context_path + "/replenish/toInOrder.do?inPlanId="+inPlanId).done(function(data){
			    	  layer.open({
			    	  title : "入库单生成", 
			    	  type:1,
			    	  skin : "layui-layer-molv",
			    	  area : ['750px', '450px'],
			    	  shade : 0.6, //遮罩透明度
			    	  moveType : 1, //拖拽风格，0是默认，1是传统拖动
			    	  anim : 2,
			    	  content : data
			    	  });
			    	 });
			}else{
				layer.alert("入库计划未提交！");
			}
			}
		});
    		
    }
 }
 $("#queryForm #user").select2({
        placeholder: "选择移库人员",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/ASNmanage/getSelectUser",
            type: "POST",
            dataType: 'json',
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }

    });
		$("#queryForm #area").select2({
        placeholder: "选择库区",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/area/getSelectArea",
            type: "POST",
            dataType: 'json',
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }

    });
</script>
</html>