<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tbl.modules.wms.entity.outstorage.OutStorage"%>
<%@page import="com.tbl.modules.wms.entity.outstorage.OutStorageDetail"%>
<%@page import="com.mysql.jdbc.StringUtils" %>
<%@ page import="com.tbl.modules.wms.entity.warehousehandle.StockCheck" %>
<%@ page import="com.tbl.modules.wms.entity.warehousehandle.StockCheckDetail" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
    List<StockCheck> stockCheckList =(List<StockCheck>)request.getAttribute("stockList");
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">

    <title>盘点打印</title>

    <script type="text/javascript">
        function aa(){
            window.print();
        }
    </script>
</head>
<body onload="aa();">
<%
    double allAmount = 0;
    if(stockCheckList != null && stockCheckList.size()>0){
        for(int b = 0;b <stockCheckList.size(); b++){
%>
<br/>
<h3 align="center" style="font-weight: bolder;padding-top: 5px;">盘点单据</h3>
<table style="width: 20.2cm;font-size: 12px;" align="center">
    <tr>
        <td>盘点编号：<%=stockCheckList.get(b).getStockCheckNo()%></td>
        <td>盘点方式：<%=stockCheckList.get(b).getStockCheckMethodName() %></td>
    </tr>
    <tr>
        <td>盘点时间：<%=stockCheckList.get(b).getStockCheckTime()%></td>
        <td>盘点人员：<%=stockCheckList.get(b).getStockCheckUserName() %></td>
    </tr>
</table>
<!--table的最大高度为height:21cm;  -->
<table style="border-collapse: collapse; border: none;width: 20cm;font-size: 12px;" align="center">
    <tr>
        <td style="border: solid #000 1px;width:2cm;">序号</td>
        <td style="border: solid #000 1px;width:2cm;">库位编号</td>
        <td style="border: solid #000 1px;width:50cm;">库位名称</td>
        <td style="border: solid #000 1px;width:2cm;">条码</td>
        <td style="border: solid #000 1px;width:5cm;">库存数量</td>
        <td style="border: solid #000 1px;width:5cm;">盘点数量</td>
    </tr>
    <%
        int len = stockCheckList.get(b).getStockCheckDetailList().size();
        int s = 4 - len;
        double yhj = 0;
        double jeyhj = 0;
        for(int i= 0;i< len ;i++){
            List<StockCheckDetail> detail = stockCheckList.get(b).getStockCheckDetailList();
            //页合计数量
            yhj =  yhj + Double.parseDouble(detail.get(i).getStockAmount()+"");
    %>
    <tr >
        <td style="border: solid #000 1px;"><%=i+1 %></td>
        <td style="border: solid #000 1px;"><%=detail.get(i).getLocationNo() %></td>
        <td style="border: solid #000 1px;"><%=detail.get(i).getLocationName()%></td>
        <td style="border: solid #000 1px;"><%=detail.get(i).getMaterialBar() %></td>
        <td style="border: solid #000 1px;"><%=detail.get(i).getStockAmount() %></td>
        <td style="border: solid #000 1px;"></td>
    </tr>
    <%
        }
        for(int i= 0;i< s ;i++){
    %>
    <tr >
        <td style="border: solid #000 1px;">&nbsp;</td>
        <td style="border: solid #000 1px;">&nbsp;</td>
        <td style="border: solid #000 1px;">&nbsp;</td>
        <td style="border: solid #000 1px;">&nbsp;</td>
        <td style="border: solid #000 1px;">&nbsp;</td>
        <td style="border: solid #000 1px;">&nbsp;</td>

    </tr>
    <%
        }
        yhj = Math.round(yhj * 10000) / 10000.0;
        jeyhj = Math.round(jeyhj * 10000) / 10000.0;

        double allamount = Double.parseDouble(stockCheckList.get(b).getAllAmount()+"");
        allamount = Math.round(allamount * 10000) / 10000.0;
    %>
    <tr >
        <td style="border: solid #000 1px;white-space:nowrap;">页合计：</td>
        <td style="border: solid #000 1px;white-space:nowrap;"></td>
        <td style="border: solid #000 1px;white-space:nowrap;"></td>
        <td style="border: solid #000 1px;white-space:nowrap;"></td>
        <td style="border: solid #000 1px;white-space:nowrap;"><%=yhj %></td>
        <td style="border: solid #000 1px;white-space:nowrap;"></td>

    </tr>
    <tr >
        <td style="border: solid #000 1px;white-space:nowrap;">合计：</td>
        <td style="border: solid #000 1px;white-space:nowrap;"></td>
        <td style="border: solid #000 1px;white-space:nowrap;"></td>
        <td style="border: solid #000 1px;white-space:nowrap;"></td>
        <td style="border: solid #000 1px;white-space:nowrap;"><%=allamount %></td>
        <td style="border: solid #000 1px;white-space:nowrap;"></td>
    </tr>
</table>
<table style="border-collapse: collapse; border: none;width: 20.2cm;font-size: 12px;margin-bottom: 10px;" align="center">
    <%--<tr>
        <td>制单人：<%=instorageList.get(b).getRealName()==null?"":instorageList.get(b).getRealName() %></td>
    </tr>--%>
</table>
<%

%>
<%
    if (1 <stockCheckList.size()&&b!=stockCheckList.size()-1){
%>
<div id="xx" style="height:80px;"></div>
<%
            }
        }
    }
%>
</body>
</html>

