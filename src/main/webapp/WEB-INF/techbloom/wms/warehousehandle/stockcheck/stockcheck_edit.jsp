<%@ page language="java" import="java.lang.*" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
%>
<div class="row-fluid" style="height: inherit;margin:0px;border: 0px">
    <form action="" class="form-horizontal" id="stockCheckForm" name="stockCheckForm" method="post" target="_ifr">
        <input type="hidden" id="stockCheckId" name="id" value="${check.id}" />
        <input type="hidden" id="stockCheckLoation" />
        <%--一行数据 --%>
        <div class="row" style="margin:0;padding:0;">
            <%--盘点编号--%>
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="stockCheckNumber" >盘点编号：</label>
                <div class="controls">
                    <div class="input-append required span12" >
                        <input type="text" id="stockCheckNumber" class="span10" name="stockCheckNo"   placeholder="自动生成" readonly value="${stockCheckNo}"/>
                    </div>
                </div>
            </div>
            <%--盘点方式--%>
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="stockCheckMethodNo" >盘点方式：</label>
                <div class="controls">
                    <div class="required span12" style=" float: none !important;">
                        <input type="text" class="span10" id="stockCheckMethodNo" name="stockCheckMethodNo" placeholder="盘点方式" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin:0;padding:0;">
            <%--盘点类型--%>
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="stockCheckType" >盘点类型：</label>
                <div class="controls">
                    <div class="required span12" style="float: none !important;">
                        <input class="span10" type="text" id="stockCheckType" name="stockCheckType"  placeholder="盘点内容">
                    </div>
                </div>
            </div>
            <%--盘点内容;--%>
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="stockCheckContent" >盘点内容：</label>
                <div class="controls">
                    <div class="required span12" >
                        <input class="span10" id="stockCheckContent" name="stockCheckContent" type="text" value="" placeholder="盘点内容" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin:0;padding:0;">
            <%--盘点人员--%>
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="stockCheckUserId" >盘点人员：</label>
                <div class="controls">
                    <div class="span12 required" style=" float: none !important;">
                        <input type="text" class="span10" id="stockCheckUserId" name="stockCheckUserId"  placeholder="盘点人员" />
                    </div>
                </div>
            </div>
            <%--盘点时间;--%>
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="stockCheckTime" >盘点时间：</label>
                <div class="controls">
                    <div class="required span12" >
                        <input class="form-control date-picker" id="stockCheckTime" name="stockCheckTimeStr" type="text" value="${check.stockCheckTimeStr}" placeholder="盘点时间" class="span10"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin:0;padding:0;">
                <%--备注--%>
                <div class="control-group span6" style="display: inline">
                    <label class="control-label" for="remark" >备注：</label>
                    <div class="controls">
                        <div class="span12" style=" float: none !important;">
                            <input class="span10" type="text"  id="remark" name="remark" placeholder="备注" >
                        </div>
                    </div>
                </div>
                <%--备注--%>
                <div class="control-group span6" style="display: inline">
                    <label class="control-label" for="stockCheckmode" >盘点类型：</label>
                    <div class="controls required">
                        <select name="stockCheckmode" id="stockCheckmode">
                            <option value="1" <c:if test="${'1'}">selected</c:if> >明盘</option>
                            <option value="2" <c:if test="${'2'}">selected</c:if> >暗盘</option>
                        </select>
                    </div>
                </div>
        </div>
        <div style="margin-left:10px;padding-bottom:10px;">
            <span class="btn btn-info" id="formSave">
		       <i class="ace-icon fa fa-check bigger-110"></i>保存
            </span>
            <span class="btn btn-info" onclick="confirmStockCheck()">
		       <i class="ace-icon fa fa-check bigger-110"></i>提交
            </span>
            <span class="btn btn-info" onclick="auditStockCheck()">
		       <i class="ace-icon fa fa-check bigger-110"></i>审核
            </span>
        </div>
    </form>
    <!-- 表格div -->
    <div id="grid-div-c" style="width:100%;margin:0px auto;">
        <!-- 	表格工具栏 -->
        <div id="fixed_tool_div" class="fixed_tool_div detailToolBar">
            <div id="__toolbar__-c" style="float:left;overflow:hidden;"></div>
        </div>
        <!-- 物料详情信息表格 -->
        <table id="grid-table-c" style="width:100%;height:100%;"></table>
        <!-- 表格分页栏 -->
        <div id="grid-pager-c"></div>
    </div>
</div>
<script type="text/javascript" src="<%=path%>/static/js/techbloom/wms/warehousehandle/stockcheck/stockcheck.js"></script>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriDataDetail;
    var _grid_detail;        //表格对象
    var lastsel2;
    var selectData = "";
    var selectParam ="";
    //单元格编辑成功后，回调的函数
    var editFunction = function eidtSuccess(XHR) {
        var data = eval("(" + XHR.responseText + ")");
        if (data["msg"] != "") {
            layer.alert(data["msg"]);
        }
        jQuery("#grid-table-c").jqGrid('setGridParam',
            {
                postData: {
                    id: $('#id').val(),
                    queryJsonString: ""
                }
            }
        ).trigger("reloadGrid");
    };

    _grid_detail = jQuery("#grid-table-c").jqGrid({
        url: context_path + "/stockCheck/stockCheckDetailList?stockCheckId=" + $("#stockCheckId").val(),
        datatype: "json",
        colNames: ["盘点详情主键", "库位", "物料编号","物料名称","批次号","库存数量", "盘点数量","操作"],
        colModel: [
            {name: "id", index: "id", width: 20, hidden: true},
            {name: "locationName", index: "locationName", width: 20},
            {name: "materialNo", index: "materialNo", width: 20},
            {name: "materialName", index: "materialName", width: 20},
            {name: "materialBar", index: "materialBar", width: 20},
            {name: "stockAmount", index: "stockAmount", width: 20},
            {name: "stockCheckAmount", index: "stockCheckAmount", width: 20},
            {name: "stockCheckDetailState", index: "stockCheckDetailState", width: 20,
                formatter:function(cellValu,option,rowObject){
                    if(cellValu==1){
                        return '<div style="margin-bottom:5px" class="btn btn-xs btn-success" onclick="auditStockCheckDetail('+rowObject.id+","+cellValu+')">复盘</div>';
                    }else if(cellValu==2){
                        return '<div style="margin-bottom:5px;background-color: grey" class="btn btn-xs">已审核</div>';
                    }else if(cellValu==3){
                        return '<div style="margin-bottom:5px;background-color: grey" class="btn btn-xs">复盘中</div>';
                    }else if(cellValu==0){
                        return '<div style="margin-bottom:5px;background-color: grey" class="btn btn-xs">未盘点</div>';
                    }
               }
            }     
        ],
        rowNum: 20,
        rowList: [10, 20, 30],
        pager: '#grid-pager-c',
        sortname: 'ID',
        sortorder: "asc",
        altRows: true,
        viewrecords: true,
        autowidth: true,
        multiselect: true,
        multiboxonly: true,
     //   cellsubmit:'sadsadsad',
        cellurl : context_path + '/stockCheck/updateStockCheckAmount',
        loadComplete: function (data) {
            var table = this;
            setTimeout(function () {
                updatePagerIcons(table);
                enableTooltips(table);
            }, 0);
            oriDataDetail = data;
            $(window).triggerHandler("resize.jqGrid");
        },
        cellEdit: true,
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext: "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据",
    });
    //在分页工具栏中添加按钮
    $("#grid-table-c").navGrid("#grid-pager-c", {edit: false, add: false, del: false, search: false, refresh: false}).navButtonAdd('#grid-pager-c', {
            caption: "",
            buttonicon: "ace-icon fa fa-refresh green",
            onClickButton: function () {
                $("#grid-table-c").jqGrid("setGridParam",
                    {
                        url: context_path + '/stockCheck/stockCheckDetailList',
                        postData: {queryJsonString: "",stockCheckId:$("#stockCheckId").val()} //发送数据  :选中的节点
                    }
                ).trigger("reloadGrid");
            }
        });

    $(window).on("resize.jqGrid", function () {
        //$("#grid-table-c").jqGrid('setGridWidth', $("#grid-div-c").width());
        //$("#grid-table-c").jqGrid('setGridHeight', (document.documentElement.clientHeight - $("#grid-pager-c").height() - 380));
        var height=$(".layui-layer-title",_grid_detail.parents(".layui-layer") ).height()+
            $("#stockCheckForm").outerHeight(true)+
            $("#grid-pager-c").outerHeight(true)+$("#fixed_tool_div.fixed_tool_div.detailToolBar").outerHeight(true)+$("#gview_grid-table-c .ui-jqgrid-hbox").outerHeight(true);
            $("#grid-table-c").jqGrid("setGridWidth", $("#grid-div-c").width()-3);
            $("#grid-table-c").jqGrid("setGridHeight",_grid_detail.parents(".layui-layer").height()-height);
    });
    $(window).triggerHandler("resize.jqGrid");

    //清空物料多选框中的值
    function removeChoice() {
        $("#s2id_shelvesLocation .select2-choices").children(".select2-search-choice").remove();
        $("#shelvesLocation").select2("val", "");
        selectData = 0;
    }

    //添加库位详情
    function addaddShelvesLocation() {
        if ($("#stockCheckId").val() == "") {
            layer.alert("请先保存表单信息！");
            return;
        }
        if (selectData != 0) {
            //将选中的物料添加到数据库中
            $.ajax({
                type: "POST",
                url: context_path + '/stockCheck/addStockCheckDetail',
                data: {stockCheckId: $('#stockCheckForm #stockCheckId').val(), locationIds: selectData.toString()},
                dataType: "json",
                success: function (data) {
                    removeChoice();   //清空下拉框中的值
                    if (Boolean(data.result)) {
                        layer.msg("添加成功", {icon: 1, time: 1200});
                        //重新加载详情表格
                        $("#grid-table-c").jqGrid("setGridParam",
                            {
                                postData: {stockCheckId: $("#stockCheckForm #stockCheckId").val()} //发送数据  :选中的节点
                            }
                        ).trigger("reloadGrid");
                    } else {
                        layer.alert(data.msg, {icon: 2, time: 1200});
                    }
                }
            });
        } else {
            layer.alert("请选择库位！");
        }
    }

    //工具栏
    $("#__toolbar__-c").iToolBar({
        id: "__tb__01",
        items: [
            {label: "删除", onclick: delStockCheckDetail},
        ]
    });


    $(document).ready(function () {
        //初始化盘点任务信息
        $.ajax({
            url: context_path + "/stockCheck/getStockCheckById?tm=" + new Date(),
            type: "POST",
            data: {stockCheckId: $("#stockCheckId").val()},
            dataType: "JSON",
            success: function (data) {
                console.log(data.stockCheckTime);
                if (data) {
                    //将盘点信息填充到form中
                    $("#stockCheckNumber").val(data.stockCheckNo);
                    $("#stockCheckContent").val(data.stockCheckContent);
                    $("#stockCheckLoation").val(data.stockCheckContent);
                    $("#remark").val(data.remark);
                    $("#stockCheckMethodNo").select2("data", {
                        id: data.stockCheckMethodNo,
                        text: data.stockCheckMethodName
                    });
                    $("#stockCheckmode").val(data.stockCheckmode);
                    $("#stockCheckUserId").select2("data", {id: data.stockCheckUserId, text: data.stockCheckUserName});
                    $("#stockCheckType").select2("data", {id: data.stockCheckType, text: data.stockCheckTypeNo});
                    $("#stockCheckContent").select2("data", {id: data.stockCheckContent, text: data.stockCheckContent.split(",").length+"被选中"});
                }
            }
        });
    });

    /**
     * 盘点详情审核
     */
    function auditStockCheckDetail(stockCheckId,stockCheckState){
        var replayComfirn=layer.confirm("确定是否复盘？", function () {
            $.ajax({
                url: context_path + "/stockCheck/auditStockCheckDetail?tm=" + new Date(),
                type: "POST",
                data: {stockCheckId:stockCheckId,stockCheckState:stockCheckState},
                dataType: "JSON",
                success: function (data) {
                    layer.close(replayComfirn);
                    if (data) {
                        $("#grid-table-c").jqGrid("setGridParam",
                            {
                                postData: {stockCheckId: $("#stockCheckId").val()} //发送数据  :选中的节点
                            }
                        ).trigger("reloadGrid");
                    }
                }
            });
        });
    }
</script>