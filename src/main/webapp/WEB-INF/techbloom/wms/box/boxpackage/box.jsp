<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
     var context_path = '<%=path%>';
</script>
<style type="text/css">
    .query_box .field-button.two {
        padding: 0px;
        left: 330px;
    }
</style>
<div id="box_grid-div">
    <form id="box_hiddenForm" action ="<%=path%>/box/exportExcel" method = "POST" style="display: none;">
        <!-- 选中的用户 -->
        <input id="box_ids" name="ids" value=""/>
    </form>
    <!-- 隐藏区域：存放查询条件 -->
    <form id="exportStockCheckForm"  style="display:none;">
        <!-- 盘点任务编号 -->
        <input name="stockCheckNo" id="box_stockCheckNo1" value="" />
    </form>
    <div class="query_box" id="box_yy" title="查询选项">
            <form id="box_queryForm" style="max-width:100%;">
			 <ul class="form-elements">
				<li class="field-group field-fluid3">
					<label class="inline" for="box_boxedBarCode" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:70px;">翻包条码：</span>
						<input type="text" id="box_BarCode" name="boxedBarCode" style="width: calc(100% - 75px);" placeholder="装箱条码">
					</label>			
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="box_boxedType" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:70px;">翻包类型：</span>
						<input type="text" id="box_type" name="boxedType" style="width: calc(100% - 75px);" placeholder="装箱类型">
					</label>					
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="box_locationId" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:65px;">库位：</span>
						<input type="text" id="box_allocation" name="locationId" style="width: calc(100% - 75px);" placeholder="库位">
					</label>			
				</li>
				<li class="field-group-top field-group field-fluid3">
					<label class="inline" for="box_boxUserId" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:70px;">翻包人员：</span>
						<input type="text" id="box_user" name="boxUserId" style="width: calc(100% - 75px);" placeholder="装箱人员">
					</label>					
				</li>				
			</ul>
			<div class="field-button">
					<div class="btn btn-info" onclick="queryOk();">
				        <i class="ace-icon fa fa-check bigger-110"></i>查询
			        </div>
				    <div class="btn" onclick="reset();"><i class="ace-icon icon-remove"></i>重置</div>
				    <a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>
		        </div>
		  </form>		 
    </div>
    <div id="box_fixed_tool_div" class="fixed_tool_div" style="margin:0px !important;border:0px !important;">
        <div id="box___toolbar__" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="box_grid-table" style="width:100%;margin: 0px !important;boder:0px !important"></table>
    <div id="box_grid-pager" style="margin:0px !important;border:0px !important;"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript" src="<%=path%>/static/js/techbloom/wms/box/box.js"></script>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriData;
    var _grid;
    $(function(){
       $(".toggle_tools").click();
    });
    $("#box___toolbar__").iToolBar({
        id:"box___tb__01",
        items:[
               {label:"添加",disabled:(${sessionUser.addQx}==1?false:true),onclick:addBox,iconClass:'glyphicon glyphicon-plus'},
               {label:"编辑",disabled:(${sessionUser.editQx}==1?false:true),onclick:editBox,iconClass:'glyphicon glyphicon-pencil'},
               {label:"删除",disabled:(${sessionUser.deleteQx}==1?false:true),onclick:deleteBox,iconClass:'glyphicon glyphicon-trash'},
               {label:"查看", disabled:(${sessionUser.queryQx} ==1?false : true),onclick:viewDetail, iconClass:'icon-zoom-in'},
               {label:"导出",disabled:(${sessionUser.queryQx}==1?false:true),onclick:excelBox,iconClass:'icon-share'},
               {label:"打印",disabled:(${sessionUser.deleteQx}==1?false:true),onclick:toPrint,iconClass:'icon-print'},
    		   {label:"拆箱",disabled:(${sessionUser.deleteQx}==1?false:true),onclick:devanBox,iconClass:'fa fa-arrow-down'}
            ]
        });
    $(function(){
        _grid = jQuery("#box_grid-table").jqGrid({
            url : context_path + "/box/boxList.do",
            datatype : "json",
            colNames : [ "盘点主键","翻包条码","翻包类型", "库位","翻包人员","备注","状态","装箱状况"],
            colModel : [
                {name : "id",index : "id",width : 20,hidden:true},
                {name : "boxedBarCode",index : "boxedBarCode",width : 40},
                {name : "boxedType",index : "boxedType",width : 40,
                    formatter:function(cellValu,option,rowObject){
                        if(cellValu=="WMS_BOXTYPE_OUT"){
                            return "<div style='margin-bottom:5px' >库外装箱</div>";
                        }else {
                            return "<div style='margin-bottom:5px;'>库内装箱</div>";
                        }
                    }
                },
                {name : "locationName",index : "locationName",width : 40},
                {name : "userName",index : "userName",width : 40},
                {name : "remark",index : "remark",width : 40},
                {name : "state",index : "state",width : 40,
                    formatter:function(cellValu,option,rowObject){
                        if(cellValu==0){
                            return "<span style='color:#EEC211;font-weight:bold;'>未提交</span>";
                        }else if(cellValu==1){
                            return "<span style='color:#76b86b;font-weight:bold;'>已提交</span>";
                        } else if(cellValu==2){
                            return "<span style='color:#76b86b;font-weight:bold;'>已上架</span>";
                        }
                    }
                },
				{name : "type",index : "type",width : 40,
                    formatter:function(cellValu,option,rowObject){
                        if(cellValu==0){
                            return "<div style='color:green;margin-bottom:5px'>有效</div>";
                        }
                        else if(cellValu==1) {
                            return "<div style='color:red;margin-bottom:5px;'>无效</div>";
                        }
                    }
				}
            ],
            rowNum : 20,
            rowList : [ 10, 20, 30 ],
            pager : "#box_grid-pager",
            sortname : "id",
            sortorder : "desc",
            altRows: false,
            viewrecords : true,
            autowidth:true,
            multiselect:true,
            multiboxonly: true,
            loadComplete : function(data){
                var table = this;
                setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
                oriData = data;
            },
            emptyrecords: "没有相关记录",
            loadtext: "加载中...",
            pgtext : "页码 {0} / {1}页",
            recordtext: "显示 {0} - {1}共{2}条数据"
        });
        //在分页工具栏中添加按钮
        jQuery("#box_grid-table").navGrid("#box_grid-pager",{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd('#box_grid-pager',{
                caption:"",
                buttonicon:"ace-icon fa fa-refresh green",
                onClickButton: function(){
                    $("#box_grid-table").jqGrid("setGridParam",
                        {
                            postData: {queryJsonString:""} //发送数据
                        }
                    ).trigger("reloadGrid");
                }
            });
        $(window).on("resize.jqGrid", function () {
            $("#box_grid-table").jqGrid("setGridWidth", $("#box_grid-div").width() - 3 );
            $("#box_grid-table").jqGrid("setGridHeight", $(".container-fluid").height()-$("#box_yy").outerHeight(true)-
            $("#box_fixed_tool_div").outerHeight(true)-$("#box_grid-pager").outerHeight(true)-
            $("#gview_box_grid-table .ui-jqgrid-hbox").outerHeight(true));
        });
        $(window).triggerHandler("resize.jqGrid");
    });
    var _queryForm_data = iTsai.form.serialize($("#box_queryForm"));
    function queryOk(){
        var queryParam = iTsai.form.serialize($("#box_queryForm"));
        queryByParam(queryParam);
    }
    /**
     * 盘点任务查询功能:获取查询页面中的值，并将值放入列表页面中隐藏的form
     * @param jsonParam     查询页面传递过来的json对象
     */
    function queryByParam(jsonParam){
        var queryJsonString = JSON.stringify(jsonParam);
        $("#box_grid-table").jqGrid("setGridParam",
            {
                postData: {queryJson:queryJsonString}
            }
        ).trigger("reloadGrid");
    }

    $("#allocateTypeSelect").change(function(){
        $("#queryForm #allocateType").val($("#allocateTypeSelect").val());
    });

    $("#box_inWarehouseSelect").change(function(){
        $("#box_queryForm #box_inWarehouse").val($("#box_inWarehouseSelect").val());
    });

    $("#box_outWarehouseSelect").change(function(){
        $("#box_queryForm #box_outWarehouse").val($("#box_outWarehouseSelect").val());
    });
    function reset(){
        iTsai.form.deserialize($("#box_queryForm"),_queryForm_data);
        $("#box_allocateTypeSelect").val("").trigger("change");
        $("#box_inWarehouseSelect").val("").trigger("change");
        $("#box_outWarehouseSelect").val("").trigger("change");
        //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
        $("#box_grid-table").jqGrid("setGridParam",
            {
                postData: {queryJson:""} //发送数据
            }
        ).trigger("reloadGrid");
        $("#box_allocateTypeSelect").find("option[text='所有类型']").attr("selected",true);
        $("#box_outWarehouseSelect").find("option[text='所有库区']").attr("selected",true);
        $("#box_inWarehouseSelect").find("option[text='所有库区']").attr("selected",true);
    }
    $("#box_queryForm .mySelect2").select2();
    $(".date-picker").datetimepicker({format: "YYYY-MM-DD",useMinutes:true,useSeconds:true});
    function reloadGrid(){
        _grid.trigger("reloadGrid");
    }
    $("#box_queryForm #box_type").select2({
    placeholder: "选择装箱类型",
    minimumInputLength: 0, //至少输入n个字符，才去加载数据
    allowClear: true, //是否允许用户清除文本信息
    delay: 250,
    formatNoMatches: "没有结果",
    formatSearching: "搜索中...",
    formatAjaxError: "加载出错啦！",
    ajax: {
        url: context_path + "/box/getBoxedTypeList",
        type: "POST",
        dataType: "json",
        delay: 250,
        data: function (term, pageNo) { //在查询时向服务器端传输的数据
            term = $.trim(term);
            return {
                queryString: term, //联动查询的字符
                pageSize: 15, //一次性加载的数据条数
                pageNo: pageNo, //页码
                time: new Date()
                //测试
            }
        },
        results: function (data, pageNo) {
            var res = data.result;
            if (res.length > 0) { //如果没有查询到数据，将会返回空串
                var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                return {
                    results: res,
                    more: more
                };
            } else {
                return {
                    results: {}
                };
            }
        },
        cache: true
    }
});
$("#box_queryForm #box_allocation").select2({
    placeholder: "请选择库位",//文本框的提示信息
    minimumInputLength: 0, //至少输入n个字符，才去加载数据
    allowClear: true, //是否允许用户清除文本信息
    multiple: false,
    closeOnSelect: false,
    ajax: {
        url: context_path + "/box/getBoxLocationList",
        dataType: "json",
        delay: 250,
        data: function (term, pageNo) { //在查询时向服务器端传输的数据
            term = $.trim(term);
            selectParam = term;
            return {
                /* docId : $("#baseInfor #id").val(), */
                queryString: term, //联动查询的字符
                pageSize: 15, //一次性加载的数据条数
                pageNo: pageNo, //页码
                time: new Date(),
                content: $("#box_boxedType").val()
                //测试
            }
        },
        results: function (data, pageNo) {
            var res = data.result;
            if (res.length > 0) { //如果没有查询到数据，将会返回空串
                var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                return {
                    results: res,
                    more: more
                };
            } else {
                return {
                    results: {
                        "id": "0",
                        "text": "没有更多结果"
                    }
                };
            }
        },
        cache: true
    }
});
$("#box_queryForm #box_user").select2({
    placeholder: "选择翻包人员",
    minimumInputLength: 0, //至少输入n个字符，才去加载数据
    allowClear: true, //是否允许用户清除文本信息
    delay: 250,
    formatNoMatches: "没有结果",
    formatSearching: "搜索中...",
    formatAjaxError: "加载出错啦！",
    ajax: {
        url: context_path + "/ASNmanage/getSelectUser",
        type: "POST",
        dataType: "json",
        delay: 250,
        data: function (term, pageNo) { //在查询时向服务器端传输的数据
            term = $.trim(term);
            return {
                queryString: term, //联动查询的字符
                pageSize: 15, //一次性加载的数据条数
                pageNo: pageNo, //页码
                time: new Date()
            }
        },
        results: function (data, pageNo) {
            var res = data.result;
            if (res.length > 0) { //如果没有查询到数据，将会返回空串
                var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                return {
                    results: res,
                    more: more
                };
            } else {
                return {
                    results: {}
                };
            }
        },
        cache: true
    }
});
function viewDetail(){
var checkedNum = getGridCheckedNum("#box_grid-table","id");
	if(checkedNum == 0){
    	layer.alert("请选择一条要查看的记录！");
    	return false;
    } else if(checkedNum >1){
    	layer.alert("只能选择一条记录进行查看操作！");
    	return false;
    } else {
    	var id = jQuery("#box_grid-table").jqGrid("getGridParam", "selrow");
    	$.get(context_path + "/box/viewDetail?id="+id).done(function(data){
			layer.open({
			    title : "翻包查看", 
		    	type:1,
		    	skin : "layui-layer-molv",
		    	area : ["750px", "650px"],
		    	shade : 0.6, //遮罩透明度
			    moveType : 1, //拖拽风格，0是默认，1是传统拖动
			    anim : 2,
			    content : data
			});
		});  	   	
    }
}
function toPrint(){
    var checkedNum = getGridCheckedNum("#box_grid-table","id");
    if(checkedNum == 0){
    	layer.alert("请选择一条翻包单据进行打印！");
    	return false;
    } else if(checkedNum >1){
    	layer.alert("只能选择一条翻包单据进行打印！");
    	return false;
    } else {
    	var id = jQuery("#box_grid-table").jqGrid("getGridParam", "selrow");
        $.ajax({
			type : "POST",
			url:context_path + "/box/printBarCode?boxId="+id,
			dataType : "json"
		});  	
    }        
}
</script>