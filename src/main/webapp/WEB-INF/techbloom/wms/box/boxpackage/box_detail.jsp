<%@ page language="java" import="java.lang.*"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
	String path = request.getContextPath();
%>
<div class="main-content" style="height:100%">
	<div class="widget-header widget-header-large" id="box_detail_div1">
		<input type="hidden" id="box_detail_id" name="id" value="${box.id }" />
		<h3 class="widget-title grey lighter" style=' background: none; border-bottom: none; '>
			<i class="ace-icon fa fa-leaf green"></i>
			翻包清单
		</h3>
		<div class="widget-toolbar no-border invoice-info">
			<span class="invoice-info-label">翻包条码</span>
			<span class="red">${box.boxedBarCode }</span>
			<br />
			<span class="invoice-info-label">翻包时间：</span>
			<span class="blue">${fn:substring(box.boxTime, 0, 19)}</span>
		</div>
		<!-- 打印按钮 -->
		<div class="widget-toolbar hidden-480">
			<a href="#" onclick="printDoc();" title="详情打印">
				<i class="ace-icon fa fa-print"></i>
			</a>
		</div>
	</div>
	<div class="widget-body" id="div2">
		<table style="width: 100%;font-size:16px;border-collapse:separate;border-spacing:2px 3px;">
			<tr>
				<td>
					<i class="ace-icon fa fa-caret-right blue"></i>
					翻包类型：
					<b class="black">${box.boxedTypeName }</b>
				</td>
				<td>
					<i class="ace-icon fa fa-caret-right blue"></i>
					库位：
					<b class="black">${box.locationName }</b>
				</td>
			</tr>
			<tr>
				<td>
					<i class="ace-icon fa fa-caret-right blue"></i>
					翻包人员：
					<b class="black">${box.userName }</b>
				</td>
				<td>
					<i class="ace-icon fa fa-caret-right blue"></i>
					状态：
					<c:if test="${box.state==0 }"><span style="color:#d15b47;font-weight:bold;">未提交</span></c:if>
					<c:if test="${box.state==1 }"><span style="color:#87b87f;font-weight:bold;">已提交</span></c:if>
					<c:if test="${box.state==2 }"><span style="color:#76b86b;font-weight:bold;">上架完成</span></c:if>
				</td>
			</tr>
		</table>
	</div>
	<!-- 详情表格 -->
	<div id="box_detail_grid-div-c">
		<!-- 物料信息表格 -->
		<table id="box_detail_grid-table-c" style="width:100%;height:100%;"></table>
		<!-- 表格分页栏 -->
		<div id="box_detail_grid-pager-c"></div>
	</div>
</div>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriData;      //表格数据
    var _grid;        //表格对象
    var getbillsStatus = "${GETBILLS.getbillsStatus}";   //下架单状态
    $(function() {
        //初始化表格
        _grid = jQuery("#box_detail_grid-table-c").jqGrid({
            url: context_path + "/box/boxDetailList?boxedId=" + $("#box_detail_id").val(),
            datatype: "json",
            colNames: ["翻包详情主键", "物料编号", "物料名称", "批次号", "数量","到期时间"],
            colModel: [
                        {name: "id", index: "id", width: 20, hidden: true},                       
                        {name: "materialNo", index: "materialNo", width: 20},
                        {name: "materialName", index: "materialName", width: 20},
                        {name: "materialBarCode", index: "materialBarCode", width: 20},
                        {name: "amount", index: "amount", width: 20},
                        {name: "finalDate", index: "finalDate", width: 30}
            ],
            rowNum: 20,
            rowList: [10, 20, 30],
            pager: "#box_detail_grid-pager-c",
            sortname: "id",
            sortorder: "desc",
            altRows: true,
            viewrecords: true,
            autowidth: true,
            multiselect: false,
            multiboxonly: true,
            loadComplete: function (data) {
                var table = this;
                setTimeout(function () {
                    updatePagerIcons(table);
                    enableTooltips(table);
                }, 0);
                oriData = data;
                $(window).triggerHandler('resize.jqGrid');
            },
            emptyrecords: "没有相关记录",
            loadtext: "加载中...",
            pgtext: "页码 {0} / {1}页",
            recordtext: "显示 {0} - {1}共{2}条数据"
        });
        //在分页工具栏中添加按钮
        jQuery("#box_detail_grid-table-c").navGrid('#box_detail_grid-pager-c', {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false
        }).navButtonAdd('#box_detail_grid-pager-c', {
            caption: "",
            buttonicon: "ace-icon fa fa-refresh green",
            onClickButton: function () {
                $("#box_detail_grid-table-c").jqGrid('setGridParam',
                    {
                        postData: {PUTBILLSID: $('#box_detail_id').val()} //发送数据  :选中的节点
                    }
                ).trigger("reloadGrid");
            }
        });
        $(window).on("resize.jqGrid", function() {
            $("#box_detail_grid-table-c").jqGrid("setGridWidth", $("#box_detail_grid-div-c").width() - 3);
            var height = $(".layui-layer-title",_grid.parents(".layui-layer")).height()+
                         $("#box_detail_div1").outerHeight(true)+$("#box_detail_div2").outerHeight(true)+
                         $("#gview_box_detail_grid-table-c .ui-jqgrid-hbox").outerHeight(true)+
                         $("#box_detail_grid-pager-c").outerHeight(true);
                         $("#box_detail_grid-table-c").jqGrid("setGridHeight", _grid.parents(".layui-layer").height()-height);
        });
    });

    //将数据格式化成两位小数：四舍五入
    function formatterNumToFixed(value,options,rowObj){
        if(value!=null){
            if(rowObj.id==-1){
                return "";
            }else{
                var floatNum = parseFloat(value);
                return floatNum.toFixed(2);
            }
        }else{
            return "0.00";
        }
    }

    function printDoc(){
        var url = context_path + "/box/toPrintDetail.do?id="+$("#box_detail_id").val();
        window.open(url);
    }

    function offTheShelf(id,locationId,amount,state){
		//prompt层
		layer.prompt({title: '输入实际下架数量，并确认', formType: 4}, function(val, index){
		  layer.close(index);
		  if(!isNaN(val)){
		      var a = amount - val
		      if(a>0){
			      layer.confirm('当前库位物料数缺少<font color="red">  '+ a +' </font>，是否跳过后下架？', {
					  btn: ['跳过'] //按钮
					}, function(){
						$.ajax({
				            type : "POST",
				            url : context_path + '/getbills/setState.do?id=' + id+'&locationId='+locationId+'&state='+state+'&amount='+val,
				            dataType : 'json',
				            cache : false,
				            success : function(data) {
				                if (Boolean(data.result)) layer.msg(data.msg, {icon: 1, time: 1000});
				                else layer.msg(data.msg, {icon: 7, time: 1000});
				                $("#box_detail_grid-table-c").jqGrid('setGridParam',{
			                        postData: {queryJsonString: ""} //发送数据
			                    }).trigger("reloadGrid");
				                $("#box_detail_grid-table").jqGrid('setGridParam',{
			                        postData: {queryJsonString: ""} //发送数据
			                    }).trigger("reloadGrid");
				            }
				        }); 
							
					  layer.msg('已跳过，请进入异常情况查看', {icon: 1});
					});
		      }else{
		      	layer.confirm('实际下架数量大于预期下架数量，下架将以预期数量为准，是否继续', {
					  btn: ['继续','取消'] //按钮
					}, function(){
							$.ajax({
				            type : "POST",
				            url : context_path + '/getbills/setState.do?id=' + id+'&locationId='+locationId+'&state='+state+'&amount='+amount,
				            dataType : 'json',
				            cache : false,
				            success : function(data) {
				                if (Boolean(data.result)) layer.msg(data.msg, {icon: 1, time: 1000});
				                else layer.msg(data.msg, {icon: 7, time: 1000});
				                $("#box_detail_grid-table-c").jqGrid('setGridParam',{
			                        postData: {queryJsonString: ""} //发送数据
			                    }).trigger("reloadGrid");
				                $("#box_detail_grid-table").jqGrid('setGridParam',{
			                        postData: {queryJsonString: ""} //发送数据
			                    }).trigger("reloadGrid");
				            }
				        }); 
					});
		      }
		  }else{
		       layer.msg("输入的不是数字");
		    }
		});
    }
</script>