﻿<%@page import="java.math.BigDecimal"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<script type="text/javascript" src="<%=path%>/plugins/public_components/js/jquery-ean13.min.js"></script>
<script type="text/javascript">
	$(function(){
		$("#box_printBarCode_ean").EAN13("4434567890123",{
			color:'rgba(0,0,0,0.8)'
		});
	});
</script>
<script type="text/javascript">
	 function printA4(){
		window.print();
	 } 
</script>
</head>
<body onload="printA4();">
        <div style="width:400px;margin:0 auto;">
            <canvas id="ean" width="400" height="200" ></canvas>
        </div>                         
		<div style="height:100%">
		<br />
		<br />		
		<table style="border-collapse: collapse; border: none;width: 16.2cm;font-size: 12px;" align="center">
			<tr>
				<td style="border: solid #000 1px;text-align:center;width:4cm;height:2cm;">苏州接货仓</td>
				<td style="border: solid #000 1px;text-align:center;width:4cm;height:2cm;">无锡分拨中心</td>
				<td style="border: solid #000 1px;text-align:center;width:2cm;height:2cm;"></td>		
			</tr>			
		</table>
		<table style="border-collapse: collapse; border: none;width: 16.2cm;font-size: 12px;" align="center">
			<tr>
				<td style="border: solid #000 1px;text-align:center;width:3.5cm;">常州薛家站</td>
				<td style="border: solid #000 1px;text-align:center;width:1.5cm;">9</td>
				<td style="border: solid #000 1px;text-align:center;width:2cm;">211早</td>
				<td style="border: solid #000 1px;text-align:center;width:3cm;">1/1</td>		
			</tr>			
		</table>
		<table style="border-collapse: collapse; border: none;width: 16.2cm;font-size: 12px;" align="center">
			<tr>
				<td style="border: solid #000 1px;text-align:center;width:5cm;">4月09日09:00-15:00送达</td>
				<td style="border: solid #000 1px;text-align:center;width:2cm;">在线支付</td>
				<td style="border: solid #000 1px;text-align:center;width:1cm;">客户签字</td>
				<td style="border: solid #000 1px;text-align:center;width:2cm;"></td>		
			</tr>			
		</table>		
		<br/>
		<br/>
	</div>
</body>
</html>