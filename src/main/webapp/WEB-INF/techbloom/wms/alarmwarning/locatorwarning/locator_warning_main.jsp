<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script src="<%=path%>/plugins/public_components/js/echarts.min.js"></script>

<style type="text/css">
    #chartDiv {
        width: 100%;
        height: 200px;
        border 1px solid #9c9c9c;
        padding: 10px;
    }
</style>
<div id="locator_warning_main_saturationAlarm" style="width: 98%;">
    <div id="locator_warning_main_chartDiv">
        <div id="locator_warning_main_main" style="width: 98%;height:200px;"></div>
    </div>
    <div id="locator_warning_main_grid-div">
        <table id="locator_warning_main_bh_grid-table" style="width:100%;height:100%;"></table>
        <div id="locator_warning_main_bh_grid-pager"></div>
    </div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriData;
    var _grid;
    _grid = jQuery("#locator_warning_main_bh_grid-table").jqGrid({
        url: context_path + '/locatorwarning/getAsoturationListData.do',
        datatype: "json",
        colNames: ['货位占用数量', '货位闲置数量', '货位占用百分比', '创建时间'],
        colModel: [
            {name: 'amount', index: 'amount', width: 60},
            {name: 'alrmAount', index: 'alrmAount', width: 50},
            {name: 'percent', index: 'percent', width: 64},
            {name: 'createtime', width: 70,
                formatter:function(cellValu,option,rowObject){
                    if (cellValu != null) {
                        return getFormatDateByLong(new Date(cellValu), "yyyy-MM-dd");
                    } else {
                        return "";
                    }
                }
            }
        ],
        rowNum: 20,
        rowList: [10, 20, 30],
        pager: '#locator_warning_main_bh_grid-pager',
        sortname: 'USER_ID',
        sortorder: "desc",
        altRows: true,
        viewrecords: true,
        autowidth: true,
        multiselect: true,
        multiboxonly: true,
        loadComplete: function (data) {
            var table = this;
            setTimeout(function () {
                updatePagerIcons(table);
                enableTooltips(table);
            }, 0);
            oriData = data;
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext: "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
    });
    //在分页工具栏中添加按钮
    jQuery("#locator_warning_main_bh_grid-table").navGrid('#locator_warning_main_bh_grid-pager', {edit: false, add: false, del: false, search: false, refresh: false})
        .navButtonAdd('#locator_warning_main_bh_grid-pager', {
            caption: "",
            buttonicon: "ace-icon fa fa-refresh green",
            onClickButton: function () {
                $("#locator_warning_main_bh_grid-table").jqGrid('setGridParam',
                    {
                        postData: {queryJsonString: ""} //发送数据
                    }
                ).trigger("reloadGrid");
            }
        });
    $(window).on('resize.jqGrid', function () {
        $("#locator_warning_main_bh_grid-table").jqGrid('setGridWidth', $("#locator_warning_main_grid-div").width() - 3);
        $("#locator_warning_main_bh_grid-table").jqGrid('setGridHeight', (document.documentElement.clientHeight - 
        $("#locator_warning_main_chartDiv").height() - $("#locator_warning_main_bh_grid-pager").height() - 345 - (!$(".query_box").is(":hidden") ? $(".query_box").outerHeight(true) : 0) ));
    });

    $(window).triggerHandler('resize.jqGrid');

    $(function () {
        //实时加载近五天的货位使用情况
        $.ajax({
            type: "POST",
            url: context_path + '/locatorwarning/getAreaHouseAmountData.do?tm=' + new Date().getTime(),
            dataType: 'json',
            cache: false,
            success: function (data) {
                option = {
                    title: {
                        text: '货位占用情况图表'
                    },
                    tooltip: {
                        trigger: 'axis'
                    },
                    legend: {
                        data: ['占用货位数', '剩余货位数']
                    },
                    grid: {
                        left: '3%',
                        right: '4%',
                        bottom: '3%',
                        containLabel: true
                    },
                    toolbox: {
                        show : true,
                        feature: {
                            magicType : {show: true},
                            saveAsImage: {}
                        }
                    },
                    calculable : true,
                    xAxis: {
                        type: 'category',
                        boundaryGap : false,
                        data: data.date
                    },
                    yAxis: {
                        type: 'value'
                    },
                    series: [
                        {
                            name: '占用货位数',
                            type: 'line',
                            data: data.amountList
                        },
                        {
                            name: '剩余货位数',
                            type: 'line',
                            data: data.alarmAmountList
                        }
                    ]
                };
                var myChart = echarts.init(document.getElementById('locator_warning_main_main'));
                myChart.setOption(option);
            }
        });
    });


</script>
</html>
