<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
    <script type="text/javascript">
        var context_path = '<%=path%>';
    </script>
    <script type="text/javascript" src="<%=path%>/static/js/techbloom/wms/alarmwarning/stockAlarm.js"></script>
    <style type="text/css">
    </style>
    <div id="stockAlarm_list_grid-div">
	<form id="stockAlarm_list_hiddenForm" action="<%=path%>/stockAlarm/toExcel" method="POST" style="display: none;">
        <input id="stockAlarm_list_ids" name="ids" value=""/>
    </form>
    <form id="stockAlarm_list_hiddenQueryForm" style="display:none;">
        <input name="batchNo" id="stockAlarm_list_batchNo" value=""/>
        <input id="stockAlarm_list_materialName" name="materialName" value=""/>
        <input name="finalDate" id="stockAlarm_list_finalDate" value=""/>
        <input id="stockAlarm_list_warehouseId" name="warehouseId" value=""/>
    </form>
    <div class="query_box" id="stockAlarm_list_yy" title="查询选项">
            <form id="stockAlarm_list_queryForm" style="max-width:100%;">
			 <ul class="form-elements">
				<li class="field-group field-fluid3">
					<label class="inline" for="stockAlarm_list_batchNo" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:65px;">批次号：</span>
						<input type="text" id="stockAlarm_list_batchNo" name="batchNo" style="width: calc(100% - 70px);" placeholder="批次号">
					</label>			
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="stockAlarm_list_materialName" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:65px;">物料名称：</span>
						<input type="text" id="stockAlarm_list_materialName" name="materialName" style="width: calc(100% - 70px);" placeholder="物料名称（编号）">
					</label>					
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="stockAlarm_list_finalDate" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:65px;">到期日期：</span>
						<input class="form-control date-picker" id="stockAlarm_list_finalDate" name="finalDate" type="text" 
							style="width:calc(100% - 70px);" data-date-format="yyyy-mm-dd" placeholder="到期日期"/>
					</label>			
				</li>
				<li class="field-group-top field-group field-fluid3">
					<label class="inline" for="stockAlarm_list_warehouseId" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:65px;">库区：</span>
						<input type="text" id="stockAlarm_list_warehouseId" name="warehouseId" style="width: calc(100% - 70px);" placeholder="补货人">
					</label>					
				</li>
				
			</ul>
			<div class="field-button" style="">
					<div class="btn btn-info" onclick="queryOk();">
				        <i class="ace-icon fa fa-check bigger-110"></i>查询
			        </div>
				    <div class="btn" onclick="reset();"><i class="ace-icon icon-remove"></i>重置</div>
		        </div>
		  </form>		 
    </div>
    <div id="stockAlarm_list_fixed_tool_div" class="fixed_tool_div">
        <div id="stockAlarm_list___toolbar__" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="stockAlarm_list_grid-table" style="width:100%;height:100%;"></table>
    <div id="stockAlarm_list_grid-pager"></div>
</div>
</body>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
var context_path = '<%=path%>';
var dynamicDefalutValue="825a0e81d3c340c29737622aa819233d";
$("#stockAlarm_list___toolbar__").iToolBar({
    id: "stockAlarm_list___tb__01",
    items: [
       	 { label:"导出", disabled:(${sessionUser.queryQx} == 1 ? false : true),onclick:function () {toExcel();}, iconClass:'icon-share'}
     ]
});
$(function () {	
	$(".mySelect2").select2();	
    //初始化时间控件
    $(".date-picker").datepicker({autoclose: true,todayHighlight: true});	
    _grid = jQuery("#stockAlarm_list_grid-table").jqGrid({
        url: context_path + "/stockAlarm/list",
        datatype: "json",
        colNames: ["物料批次号","物料编号", "物料名称", "物料单位","库存数量","到期日期","库位信息"],
        colModel: [
            {name: "batchNo", index: "batchNo", width: 100},
            {name: "materialNo", index: "material_No", width:40},
            {name: "materialName", index: "materialName", width:50},
            {name: "materialUnit", index: "materialUnit", width: 40},
            {name: "amount", index: "amount", width: 40,formatter: "number", formatoptions: {thousandsSeparator:",", defaulValue:"",decimalPlaces:2}},
            {name: "finalDate", index: "finaldate", width: 50},
            {name: "allocation", index: "allocation", width: 150}
        ],
        rowNum: 20,
        rowList: [10, 20, 30],
        pager: "#stockAlarm_list_grid-pager",
        sortname: "s.id",
        sortorder:"asc",
        altRows: true,
        viewrecords: true,
        autowidth: true,
        multiselect: true,
        beforeRequest:function (){
            dynamicGetColumns(dynamicDefalutValue,"stockAlarm_list_grid-table",$(window).width()-$("#sidebar").width() -7);
            //重新加载列属性
        },
        loadComplete: function (data) {
            var table = this;
            setTimeout(function () {
                updatePagerIcons(table);
                enableTooltips(table);
            }, 0);
            oriData = data;
        },
        autoScroll:true,
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext: "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1} / 共{2}条数据"
    });
    $(window).on("resize.jqGrid", function () {
        $("#stockAlarm_list_grid-table").jqGrid("setGridWidth" , $("#stockAlarm_list_grid-div").width() - 3);
        $("#stockAlarm_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-
        $("#stockAlarm_list_yy").outerHeight(true)-$("#stockAlarm_list_fixed_tool_div").outerHeight(true)-
        $("#stockAlarm_list_grid-pager").outerHeight(true)-$("#gbox_stockAlarm_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
    })
    //在分页工具栏中添加按钮
    jQuery("#stockAlarm_list_grid-table").navGrid("#stockAlarm_list_grid-pager", {
        edit: false,
        add: false,
        del: false,
        search: false,
        refresh: false
    }).navButtonAdd("#stockAlarm_list_grid-pager", {
                caption: "",
                buttonicon: "ace-icon fa fa-refresh green",
                onClickButton: function () {
                    $("#stockAlarm_list_grid-table").jqGrid("setGridParam",
                            {
                                postData: {queryJsonString: ""} //发送数据
                            }
                    ).trigger("reloadGrid");
                }
            }).navButtonAdd("#grid-pager",{
            caption: "",
            buttonicon:"fa  icon-cogs",
            onClickButton : function (){
                jQuery("#stockAlarm_list_grid-table").jqGrid("columnChooser",{
                    done: function(perm, cols){
                        dynamicColumns(cols,dynamicDefalutValue);
                        $("#stockAlarm_list_grid-table").jqGrid("setGridWidth", $("#stockAlarm_list_grid-div").width() - 3);
                    }
                });
            }
        });
    $(window).triggerHandler("resize.jqGrid");
});
$("#stockAlarm_list_queryForm #stockAlarm_list_warehouseId").select2({
        placeholder: "请选择库区",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/area/getSelectArea",
            type: "POST",
            dataType: "json",
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });
</script>