<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<div id="supplier_edit_page" class="row-fluid" style="height: inherit;">
	<form id="editForm" class="form-horizontal" style="overflow: auto; height: calc(100% - 70px);">
		<input type="hidden" name="id" id="replenishId" value="${replenish.id}" />
		<div class="control-group">
			<label class="control-label" for="replenishNo">补货任务编号：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input class="span11" type="text"  name="replenishNo" readonly id="replenishNo" placeholder="后台自动生成" value="${replenish.replenishNo}" />
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="replenishTime">补货时间：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input class="span11 date-picker" id="replenishTime" name="replenishTimeStr" type="text" value="" placeholder="补货时间" /> 
				    <span class="input-group-addon"> <i class="fa fa-calendar bigger-110"></i></span>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="userId">入库人：</label>
			<div class="controls">
				<div class="span12 required" style=" float: none !important;">
					<input class="span11 select2_input" type="text" id="userId" name="userId"  placeholder="入库人" />
	                <input type="hidden" name="user" id="user" value="${replenish.userId }" />
					<input type="hidden" name="userName" id="userName" value="${replenish.userName }" />
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="remark">备注：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<textarea class="span11" name="remark" id="remark" rows="3" cols="20">${replenish.remark}</textarea>
				</div>
			</div>
		</div>
	</form>
	<div class="field-button" style="text-align: center;border-top: 0px;margin: 15px auto;">
		<span class="btn btn-info" onclick="saveForm();">
		   <i class="ace-icon fa fa-check bigger-110"></i>保存
		</span>
		<span class="btn btn-danger" onclick="layer.closeAll();">
		   <i class="icon-remove"></i>&nbsp;取消
		</span>
	</div>
</div>
<script type="text/javascript">
$(".date-picker").datetimepicker({format: "YYYY-MM-DD HH:mm:ss",useMinutes:true,useSeconds:true});
//确定按钮点击事件
    function saveForm(){
	  if($("#editForm").valid()){
			saveUserInfo($("#editForm").serialize());
		}
	}
$("#editForm").validate({
  		rules:{
  			"replenishName" : {
  				required:true,
  				maxlength:32,
  				remote:context_path+"/replenishTask/isHaveName.do?id="+$('#replenishId').val()
  			},
  			"productId":{
  				required:true,
  			},
  			"amount":{
  			    required:true,
    			number: true
    		}, 
  		},
  		messages:{
  			"replenishName":{
  				remote:"您输入的补货计划名称已经存在，请重新输入！"
  			}
  		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
		}
	});
$("#userId").select2({
        placeholder: "选择入库人",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/ASNmanage/getSelectUser",
            type: "POST",
            dataType: 'json',
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }

    });
    var ajaxStatus = 1;     //ajax请求状态：0不能请求，1可以请求
	//保存/修改用户信息
  	function saveUserInfo(bean){
  		if(bean){
  			if(ajaxStatus==0){
  				layer.msg("保存中，请稍后...",{icon:2});
  				return;
  			}
  			ajaxStatus = 0;
  			$(".savebtn").attr("disabled","disabled");
  			$.ajax({
  				url:context_path+"/replenishTask/save?tm="+new Date(),
  				type:"POST",
  				data:bean,
  				dataType:"JSON",
  				success:function(data){
  					ajaxStatus = 1; //将标记设置为可请求
  					$(".savebtn").removeAttr("disabled");
  					if(Boolean(data.result)){
  					    alert("heihei");
  						layer.msg("保存成功！",{icon:1,time:2000});
  						//刷新用户列表
  						$("#grid-table").jqGrid("setGridParam", 
							{
								postData: {queryJsonString:""} //发送数据 
							}
						).trigger("reloadGrid");
  						//关闭当前窗口
  						layer.closeAll();
  					}else{
  						layer.msg("保存失败，请稍后重试！",{icon:2,time:2000});
  					}
  				}
  			});
  		}else{
  			layer.msg("出错啦！",{icon:2});
  		}
  	}
</script>