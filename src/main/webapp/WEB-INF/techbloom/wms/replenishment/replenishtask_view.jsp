<%@ page language="java" import="java.lang.*"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
   <div class="main-content" style="height:100%">
	<div class="widget-header widget-header-large" id="div1">
		<!-- 隐藏的asn主键 -->
	        		<input type="hidden" id="replenishId" name="id" value="${replenish.id }">
		<h3 class="widget-title grey lighter" style=' background: none; border-bottom: none; '>
			<i class="ace-icon fa fa-leaf green"></i>
			补货任务
		</h3>

		<!-- #section:pages/invoice.info -->
		<div class="widget-toolbar no-border invoice-info">
			<span class="invoice-info-label">补货编号:</span>
			<span class="red">${replenish.replenishNo }</span>
			<br />
			<span class="invoice-info-label">预计补货时间:</span>
			<span class="blue">${replenish.replenishTime}</span>
		</div>
		<!-- 打印按钮 -->
		<div class="widget-toolbar hidden-480">
			<a href="#" onclick="printDoc();">
				<i class="ace-icon fa fa-print"></i>
			</a>
		</div>
	</div>
	
	<div class="widget-body" id="div2">
		<table style="width: 100%;font-size:16px;border-collapse:separate;border-spacing:2px 3px;">
		  <tr>
		     <tr>
		       <td>
		          <i class="ace-icon fa fa-caret-right blue"></i>
				备注:
				<b class="black">${replenish.remark }</b>
		       </td>
		       <td>
		          <i class="ace-icon fa fa-caret-right blue"></i>
					状态 :
					<c:if test="${replenish.state==0 }"><span class="red">待执行</span></c:if>
			        <c:if test="${replenish.state==1 }"><span class="red">执行中</span></c:if>
			        <c:if test="${replenish.state==2 }"><span class="green">完成</span></c:if>
		       </td>
		     </tr>
		  </tr>
		</table>
	 </div>
	<!-- 详情表格 -->
	<div id="grid-div-c">
	<!-- 物料信息表格 -->
		<table id="grid-table-c" style="width:100%;height:100%;"></table>
	<!-- 表格分页栏 -->
		<div id="grid-pager-c"></div>
   </div>
			
</div><!-- /.main-content -->
<script type="text/javascript">
var oriDataView;
var _grid_view;        //表格对象
 _grid_view=jQuery("#grid-table-c").jqGrid({
         url : context_path + '/replenishTask/detailList?replenishId='+$("#replenishId").val(),
         datatype : "json",
         colNames : [ '详情主键','物料编号','物料名称','补货数量','装箱条码','所属库位'],
         colModel : [ 
  					  {name : 'id',index : 'id',width : 20,hidden:true}, 
  					  {name : 'materialNo',index:'materialNo',width :20}, 
                      {name : 'materialName',index:'materialName',width : 20}, 
                      {name: 'amount', index: 'amount', width: 20 },
                      {name : 'boxbarCode',index:'boxbarCode',width : 20},
                      {name : 'locationName',index:'locationName',width : 20},
                    ],
         rowNum : 20,
         rowList : [ 10, 20, 30 ],
         pager : '#grid-pager-c',
         sortname : 'ID',
         sortorder : "asc",
         altRows: true,
         viewrecords : true,  
         autowidth:true,
		 multiboxonly: true,
         loadComplete : function(data) {
             var table = this;
             setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
             oriDataDetail = data;
             $(window).triggerHandler('resize.jqGrid');
         },
       /* cellurl : context_path + "/ASNmanage/updateAmount", */
	   cellEdit: true,
	   cellsubmit : "clientArray",
  	   emptyrecords: "没有相关记录",
  	   loadtext: "加载中...",
  	   pgtext : "页码 {0} / {1}页",
  	   recordtext: "显示 {0} - {1}共{2}条数据",
    });
    
    $(window).on("resize.jqGrid", function () {
  		$("#grid-table-c").jqGrid("setGridWidth", $("#grid-div-c").width() - 3 );
  		 var height =$(".layui-layer-title",_grid_view.parents(".layui-layer")).height()+
  		 $("#div1").outerHeight(true)+$("#div2").outerHeight(true)+
  		 $("#grid-pager-c").outerHeight(true)+
  		  $("#gview_grid-table-c .ui-jqgrid-hbox").outerHeight(true);
          $("#grid-table-c").jqGrid("setGridHeight",_grid_view.parents(".layui-layer").height()-height);
  	});
  	$(window).triggerHandler("resize.jqGrid");
    function printDoc(){
	var url = context_path + "/ASNmanage/printInstorageDetail?instoreID="+$("#id").val();
	window.open(url);
}
 </script>
