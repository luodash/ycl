<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="storage_info_search-div">
    <form id="hiddenForm_excel01" action="<%=path%>/storageInfoSearch/toExcel2" method="POST" style="display: none;">
        <input id="idExcels" name="idExcels" value=""/>
        <input id="qaCodes" name="qaCode" value=""/>
    </form>

    <form id="hiddenQueryForm01" style="display:none;">
        <input id="qaCode01" name="qaCode" value=""/>
    </form>
        <div class="query_box" id="list_yy_ss" title="查询选项">
            <form id="list_queryForm_ss" style="max-width:100%;">
                <ul class="form-elements">
                    <li class="field-group field-fluid3">
                        <label class="inline" style="margin-right:20px;width:100%;">
                            <span class="form_label" style="width:78px;">质保单号：</span>
                            <input id="storageinfosearch_qaCode" name="qaCode" type="text" style="width: calc(100% - 84px );" placeholder="质保单号" />
                        </label>
                    </li>
                </ul>
                <div class="field-button" style="">
                    <div class="btn btn-info" onclick="storageinfosearch_queryOk();">
                        <i class="ace-icon fa fa-check bigger-110"></i>查询
                    </div>
                    <div class="btn" onclick="storageinfosearch_reset();"><i class="ace-icon icon-remove"></i>重置</div>
                    <%--<a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>--%>
                </div>
            </form>
        </div>
    <div id="list_fixed_tool_div_01" class="fixed_tool_div">
        <div id="storageinfosearch_toolbar_" style="float:left;overflow:hidden;"></div>
    </div>
    <div style="overflow-x:auto"><table id="storage_info_search-table" style="width:100%;height:100%;"></table></div>
    <div id="storage_info_search-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
    var storageinfosearch_oriData;
    var storageinfosearch_grid;
    //时间控件
    $(".date-picker").datetimepicker({format: "YYYY-MM-DD"});
    $(function  (){
        $(".toggle_tools").click();
    });

    $("#storageinfosearch_toolbar_").iToolBar({
        id: "storageinfosearch_tb_01",
        items: [
            {label: "导出",hidden:"${operationCode.webExport}"=="1",onclick:function(){storageinfosearch_toExecl();},iconClass:'icon-share'}
    	]
    });

    var list_queryForm_ss_data = iTsai.form.serialize($("#list_queryForm_ss"));

    storageinfosearch_grid = jQuery("#storage_info_search-table").jqGrid({
        url : context_path + "/storageInfoSearch/listData",
        datatype : "json",
        colNames : [ "主键","交易类型","质保号","批次号","盘号","数量","物料编码","盘具编码","创建时间"],
        colModel : [
            {name : "id",index : "id",hidden:true},
            {name : "stateStr",index : "stateStr",width : 80,formatter:function(cellvalue,option,rowObject){
	            	if(cellvalue == '11') {
	            	    return "生产退库";
                    }else if(cellvalue == '12') {
	            	    return "采购退货";
                    }else if(cellvalue == '13') {
	                    return "报废";
                    }else if(cellvalue == '21') {
	                    return "调拨入库";
                    }else if(cellvalue == '31') {
	                    return "调拨出库";
                    }else if(cellvalue == '43') {
	                    return "入库中";
                    }else if(cellvalue == '44') {
	                    return "入库完成";
                    }else if(cellvalue == '45') {
	                    return "已退库";
                    }else if(cellvalue == '51') {
	                    return "移库入库";
                    }else if(cellvalue == '61') {
	                    return "移库出库";
                    }else if(cellvalue == '72') {
                        return "拣货中";
                    }else if(cellvalue == '73') {
                        return "出库完成";
                    }else if(cellvalue == '74') {
                        return "预出库";
                    }else if(cellvalue == '81') {
                        return "拆分";
                    }else {
                        return "其他";
                    }
	            }
            },
            {name : "qaCode",index : "qaCode",width : 250},
            {name : "batchNo",index : "batch_no",width : 160},
            {name : "dishcode",index : "dishcode",width : 180},
            {name : "meter",index : "meter",width : 80},
            {name : "materialCode",index : "materialcode",width : 160},
            {name : "dishnumber",index : "dishnumber",width : 160},
            {name : "createtime",index : "createtime",width : 260}
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : "#storage_info_search-pager",
        sortname : "id",
        sortorder : "asc",
        altRows: true,
        viewrecords : true,
        hidegrid:false,
        autowidth:false,
        shrinkToFit:false,
        autoScroll: true,
        multiselect:true,
        multiboxonly:true,
        loadComplete : function(data){
            var table = this;
            setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            storageinfosearch_oriData = data;
            $("#storage_info_search-table").closest(".ui-jqgrid-bdiv").css({ 'overflow-x': 'auto' });
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
    });

    jQuery("#storage_info_search-table").navGrid("#storage_info_search-pager",
    {edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#storage_info_search-pager",{
        caption:"",
        buttonicon:"fa fa-refresh green",
        onClickButton: function(){
            $("#storage_info_search-table").jqGrid("setGridParam", {
                postData: {queryJsonString:""} //发送数据
            }).trigger("reloadGrid");
        }
    }).navButtonAdd("#storage_info_search-pager",{
        caption: "",
        buttonicon:"fa icon-cogs",
        onClickButton : function (){
            jQuery("#storage_info_search-table").jqGrid("columnChooser",{
                done: function(perm, cols){
                    $("#storage_info_search-table").jqGrid("setGridWidth", $("#storage_info_search-div").width());
                }
            });
        }
    });

    $(window).on("resize.jqGrid", function () {
        $("#storage_info_search-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);
        $("#storage_info_search-table").jqGrid("setGridHeight",  $(".container-fluid").height()-
        $("#list_yy_ss").outerHeight(true)-$("#list_fixed_tool_div_01").outerHeight(true)-40-
        $("#storage_info_search-pager").outerHeight(true)- $("#storage_info_search-table .ui-jqgrid-hdiv").outerHeight(true));
    });
    $(window).triggerHandler("resize.jqGrid");

    /**
     * 查询按钮点击事件
     */
    function storageinfosearch_queryOk(){
        var queryParam = iTsai.form.serialize($("#list_queryForm_ss"));
        //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
        storageinfosearch_queryByParam(queryParam);
    }

    function storageinfosearch_queryByParam(jsonParam) {
        iTsai.form.deserialize($("#hiddenQueryForm01"), jsonParam);
        var queryParam = iTsai.form.serialize($("#hiddenQueryForm01"));
        var queryJsonString = JSON.stringify(queryParam);
        $("#storage_info_search-table").jqGrid("setGridParam", {
            postData: {queryJsonString: queryJsonString}
        }).trigger("reloadGrid");
    }

    //清空重置
    function storageinfosearch_reset(){
        iTsai.form.deserialize($("#list_queryForm_ss"),list_queryForm_ss_data);
        storageinfosearch_queryByParam(list_queryForm_ss_data);
    }

    //导出Excel报表
    function storageinfosearch_toExecl(){
        $("#hiddenForm_excel01 #idExcels").val(jQuery("#storage_info_search-table").jqGrid("getGridParam", "selarrrow"));
        $("#hiddenForm_excel01 #qaCodes").val($("#list_queryForm_ss #storageinfosearch_qaCode").val());
        $("#hiddenForm_excel01").submit();
    }

    $("#list_queryForm_ss .mySelect2").select2();
</script>