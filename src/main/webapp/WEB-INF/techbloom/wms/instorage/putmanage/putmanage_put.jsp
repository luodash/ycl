<%@ page language="java" import="java.lang.*"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
  <div class="widget-box" style="border:10px;margin:10px">
	<form id="putmanage_put_baseInfor" style="width:870px;margin:10px auto;border-bottom: solid 2px #3b73af;">
	<div id="fromInfoContent" style="margin:10px auto;">
		<!-- 隐藏的上架单主键 -->
			<input type ="hidden" id = "putmanage_put_putId" name = "id" value = "${putm.id}">
			<input type ="hidden" id = "putmanage_put_state" name = "state" value = "${putm.state}">
  	        <div class="inline" style="margin-bottom:5px;">
  	        <label class="inline" for="putmanage_put_putNo" style="margin-right:20px;">
				上架单编号：<span class="field-required">*</span>
				<input id = "putmanage_put_putNo" name = "putNo" value  = "${putm.putNo }" placeholder="后台自动生成" readonly="readonly">
			</label>
			<label class="inline" for="putmanage_put_instorageId" style="margin-right:20px;">
				入库单&emsp;：<span class="field-required">*</span>
				<input type="text" name="InOrderNo" id="putmanage_put_InOrderNo" value="${putm.instorageNo }" readonly="readonly">
			</label>
			</div>
			<div class="inline" style="margin-bottom:5px;">
  	        <label class="inline" for="putmanage_put_houseId" style="margin-right:20px;">
				所属库区&emsp;：&nbsp;&nbsp;
				<input id="putmanage_put_houseName" name="houseName" readonly="readonly" value="${putm.warehouseName }">
				<input id="putmanage_put_instorageHouseId" type="hidden" name="houseId" value="${putm.houseId }">
			</label>
			<label class="inline" for="putmanage_put_instorageTime" style="margin-right:20px;">
				上架时间：<span class="field-required">*</span>
				<div class="inline" style="width: 200px;vertical-align:middle;">
					<div class="input-group">
						<input class="form-control date-picker" id="putmanage_put_instorageTime" name="putTime" style="width: 200px;" type="text" value="${putm.putTime}" placeholder="上架时间" readonly="readonly"/>
						<span class="input-group-addon">
							<i class="fa fa-calendar bigger-110"></i>
						</span>
					</div>
				</div>
			</label>
			</div>
			<div class="inline" style="margin-bottom:5px;">
  	        <label class="inline" for="putmanage_put_customer" style="margin-right:20px;">
				备注&emsp;&emsp;&emsp;：&nbsp;&nbsp;
				<input id="putmanage_put_remark" name="remark" value="${putm.remark}" placeholder="备注" readonly="readonly">
			</label>
			
			</div>
		</div>
	</form>
	<!-- 表格div -->
	<div id="putmanage_put_grid-div-c" style="width:100%;margin:10px auto;">
		<!-- 	表格工具栏 -->
        <div id="putmanage_put_fixed_tool_div" class="fixed_tool_div detailToolBar">
             <div id="putmanage_put___toolbar__-c" style="float:left;overflow:hidden;"></div>
        </div>
		<!-- 物料详情信息表格 -->
		<table id="putmanage_put_grid-table-c" style="width:100%;height:100%;"></table>
		<!-- 表格分页栏 -->
		<div id="putmanage_put_grid-pager-c"></div>
	</div>
</div>
<script type="text/javascript">
var context_path = '<%=path%>';
var putId=$("#putmanage_put_putId").val();
var oriDataDetail;
var _grid_detail; //表格对象
var lastsel2;

  	_grid_detail=jQuery("#putmanage_put_grid-table-c").jqGrid({
         url : context_path + '/putManage/detailList?putId='+putId,
         datatype : "json",
         colNames : [ '详情主键','货物编号','货物名称','批次号','箱号','数量','rfid标签','库位','类型','操作'],
         colModel : [ 
  					  {name : 'id',index : 'id',width : 20,hidden:true}, 
  					  {name : 'materialNo',index:'materialNo',width :20}, 
                      {name : 'materialName',index:'materialName',width : 20}, 
                      {name : 'batchNo',index:'batchNo',width : 30},
                      {name : 'boxCode',index:'boxCode',width : 30},
                      {name: 'amount', index: 'amount', width: 20},
                      {name : 'rfid',index:'rfid',width : 30},
                      {
		                name: 'locationName',
		                index: 'locationName',
		                width: 25,
		            }, 
		            {name : 'type',index:'type',width : 20,hidden: true},
		            {name : 'state',index:'state',width : 20,
                          formatter:function(cellValu,option,rowObject){
				            	if(rowObject.state==0){
				                   return "<div style='margin-bottom:5px' class='btn btn-xs btn-success' onclick='startPut("+rowObject.id+","+rowObject.type+");'>上架</div>";
				                }
				                if(rowObject.state==1){
                                      return "<div style='margin-bottom:5px' class='btn btn-xs btn-success' disabled='disabled'>正在上架</div>";
                                 }
				                if(rowObject.state==2){
				                    return "<div style='margin-bottom:5px' class='btn btn-xs btn-success' disabled='disabled'>上架完成</div>";
				                }
				            }
                      },
                    ],
         rowNum : 20,
         rowList : [ 10, 20, 30 ],
         pager : '#putmanage_put_grid-pager-c',
         sortname : 'ID',
         sortorder : "asc",
         altRows: true,
         viewrecords : true,
         caption : "物料列表",
         autowidth:true,
         afterSaveCell:function(rowid, cellname,value, iRow, iCol){
            reloadDetailTableList();
         },
         multiselect:true,
		 multiboxonly: true,
         loadComplete : function(data) 
         {
             var table = this;
             setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
             oriDataDetail = data;
         },
       cellurl : context_path + "/putManage/updateInfo",
	   cellEdit: true,
  	   emptyrecords: "没有相关记录",
  	   loadtext: "加载中...",
  	   pgtext : "页码 {0} / {1}页",
  	   recordtext: "显示 {0} - {1}共{2}条数据",
    });
    //在分页工具栏中添加按钮
    $("#putmanage_put_grid-table-c").navGrid('#putmanage_put_grid-pager-c',{edit:false,add:false,del:false,search:false,refresh:false})
      .navButtonAdd('#putmanage_put_grid-pager-c',{  
  	   caption:"",   
  	   buttonicon:"ace-icon fa fa-refresh green",   
  	   ondblClickRow: function(){   
  	    $("#putmanage_put_grid-table-c").jqGrid('setGridParam', 
				{
  	    			url:context_path + '/putManage/detailList?putId='+putId,
					postData: {queryJsonString:""} //发送数据  :选中的节点
				}
		  ).trigger("reloadGrid");
  	   }
  	});
  	
  	$(window).on('resize.jqGrid', function () {
  		$("#putmanage_put_grid-table-c").jqGrid( 'setGridWidth', $("#putmanage_put_grid-div-c").width() - 3 );
  		$("#putmanage_put_grid-table-c").jqGrid( 'setGridHeight', (document.documentElement.clientHeight - 
  		$("#putmanage_put_grid-pager-c").height() - 380) );
  	});
  	$(window).triggerHandler('resize.jqGrid');

if($("#putmanage_put_putId").val()!=""){

 $("#putmanage_put_customer").select2("data", {
   id: $("#putmanage_put_customerId").val(),
   text: $("#putmanage_put_customerName").val()
  });
}else{
  $('#putmanage_put_customer').val("")
}
 function reloadDetailTableList(){
      $("#putmanage_put_grid-table-c").jqGrid('setGridParam', {
 			url:context_path + '/putManage/detailList?putId='+putId,
			postData: {queryJsonString:""} //发送数据  :选中的节点
	}).trigger("reloadGrid");
  }
//开始上架 
 function startPut(id,type){
     $.ajax({
	      url:context_path + '/putManage/startPut?id='+id+"&type="+type,
	      type:"POST",
	      dataType:"JSON",
	      success:function(data){
	          if(data.result){
	              layer.msg(data.msg,{icon:1,time:1200});
	              reloadDetailTableList();
	              gridReload();
	          } else{
	              layer.msg(data.msg,{icon:2,time:1200});
	          }
	      }
      });
 }

  </script>
