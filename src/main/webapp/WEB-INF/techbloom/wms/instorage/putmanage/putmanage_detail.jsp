﻿<%@ page language="java" import="java.lang.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<div class="main-content" style="height:100%">
	<div class="widget-header widget-header-large" id="putmanage_detail_div1">
		<!-- 隐藏的主键 -->
		<input type="hidden" id="putmanage_detail_id" name="id" value="${PUTBILLS.id }">
		<h3 class="widget-title grey lighter"
			style=' background: none; border-bottom: none; '>
			<i class="ace-icon fa fa-leaf green"></i> 入库单
		</h3>
		<div class="widget-toolbar no-border invoice-info">
			<span class="invoice-info-label">上架单编号：</span> <span class="red">${PUTBILLS.putNo }</span><br />
			<span class="invoice-info-label">上架时间：</span> <span class="blue">${PUTBILLS.strPutTime}</span>
		</div>
		<div class="widget-toolbar hidden-480">
			<a href="#" onclick="printDoc();"> <i
				class="ace-icon fa fa-print"></i>
			</a>
		</div>
	</div>
	<div class="widget-body" id="putmanage_detail_div2">
		<table
			style="width: 100%;font-size:16px;border-collapse:separate;border-spacing:2px 3px;">
			<tr>
				<td><i class="ace-icon fa fa-caret-right blue"></i> 库区： <b
					class="black">${PUTBILLS.warehouseName }</b></td>
				<td><i class="ace-icon fa fa-caret-right blue"></i> 入库单编号： <b
					class="black">${PUTBILLS.instorageNo }</b></td>
			</tr>
			<tr>
			<tr>
				<td><i class="ace-icon fa fa-caret-right blue"></i> 备注： <b
					class="black">${PUTBILLS.remark }</b></td>
				<td><i class="ace-icon fa fa-caret-right blue"></i> 状态：<c:if
						test="${PUTBILLS.state==0 }">
						<span class="red">未上架</span>
					</c:if> <c:if test="${PUTBILLS.state==1 }">
						<span class="green">上架中</span>
					</c:if> <c:if test="${PUTBILLS.state==2 }">
						<span class="green">上架完成</span>
					</c:if></td>
			</tr>
			</tr>
		</table>
	</div>
	<!-- 详情表格 -->
	<div id="putmanage_detail_grid-div-c">
		<!-- 物料信息表格 -->
		<table id="putmanage_detail_grid-table-c" style="width:100%;height:100%;"></table>
		<!-- 表格分页栏 -->
		<div id="putmanage_detail_grid-pager-c"></div>
	</div>
</div>
<script type="text/javascript">
var oriDataView;
var _grid_view;        //表格对象
 _grid_view=jQuery("#putmanage_detail_grid-table-c").jqGrid({
        url : context_path + '/putManage/detailList?putId='+${PUTBILLS.id},
        datatype : "json",
        colNames : [ '详情主键','物料编号','物料名称','批次号','数量','rfid标签','库位','上架完成时间','状态'],
        colModel : [ 
 					 {name : 'id',index : 'id',width : 20,hidden:true}, 
 					 {name : 'materialNo',index:'materialNo',width :20}, 
                     {name : 'materialName',index:'materialName',width : 20}, 
                     {name : 'batchNo',index:'batchNo',width : 30},
                     {name : 'amount', index: 'amount', width: 10},
                     {name : 'rfid',index:'rfid',width : 27},
                     {name : 'locationName',index: 'locationName',width: 40},           
                     {name : 'completeTime',index: 'completeTime',width: 40,
                         formatter:function(cellValu,option,rowObject){
                             if (cellValu != null) {
                                 return getFormatDateByLong(new Date(cellValu), "yyyy-MM-dd HH:mm");
                             } else {
                                 return "";
                             }
                         }
					 },
                     {name : 'state',index:'state',width : 20,
                         formatter:function(cellValu,option,rowObject){
				            	if(rowObject.state==0){
				                   return "未上架";
				                }
				                if(rowObject.state==1){
                                     return "正在上架";
                                }
				                if(rowObject.state==2){
				                    return "上架完成";
				                }
				            }
                     },
                   ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : '#putmanage_detail_grid-pager-c',
        sortname : 'ID',
        sortorder : "asc",
        altRows: true,
        viewrecords : true,
        autowidth:true,
		multiboxonly: true,
        loadComplete : function(data){
             var table = this;
             setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
             oriDataDetail = data;
              $(window).triggerHandler("resize.jqGrid");
       },
	   cellEdit: true,
	   cellsubmit : "clientArray",
  	   emptyrecords: "没有相关记录",
  	   loadtext: "加载中...",
  	   pgtext : "页码 {0} / {1}页",
  	   recordtext: "显示 {0} - {1}共{2}条数据",
    });   
    $(window).on("resize.jqGrid", function () {
  		 $("#putmanage_detail_grid-table-c").jqGrid("setGridWidth", $("#putmanage_detail_grid-div-c").width() - 3 );
  		 var height =$(".layui-layer-title",_grid_view.parents(".layui-layer")).height()+
  		             $("#putmanage_detail_div1").outerHeight(true)+$("#putmanage_detail_div2").outerHeight(true)+
  		             $("#putmanage_detail_grid-pager-c").outerHeight(true)+
  		             $("#gview_putmanage_detail_grid-table-c .ui-jqgrid-hbox").outerHeight(true);
                     $("#putmanage_detail_grid-table-c").jqGrid("setGridHeight",_grid_view.parents(".layui-layer").height()-height);
  	});
  	$(window).triggerHandler("resize.jqGrid");
    function printDoc(){
	var url = context_path + "/getbills/printGetbillsDetail?putId="+$("#putmanage_detail_id").val();
	window.open(url);
}	
</script>