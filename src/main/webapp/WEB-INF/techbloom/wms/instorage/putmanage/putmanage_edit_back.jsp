<%@ page language="java" import="java.lang.*"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
  <div class="row-fluid" style="height: inherit;margin:0px;border: 0px">
	<form id="baseInfor" class="form-horizontal" id="baseInfor"  method="post" target="_ifr">
		<!-- 隐藏的上架单主键 -->
			<input type ="hidden" id = "putId" name = "id" value = "${putm.id}">
			<input type ="hidden" id = "state" name = "state" value = "${putm.state}">
  	        
  	        <div  class="row" style="margin:0;padding:0;">
             <div class="control-group span6" style="display: inline">
                <label class="control-label" for="putNo" > 上架单编号：</label>
                <div class="controls">
                    <div class="input-append span12" >
                        <input type="text" id="putNo" class="span10" name="putNo" placeholder="后台自动生成" value='${putm.putNo }' readonly="readonly"/>
                    </div>
                </div>
            </div>
            <%--入库单--%>
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="instorageId" >入库单：</label>
                <div class="controls">
                    <div class="required span12" style=" float: none !important;">
                        <input class="span10" type="text" id="instorageId" name="instorageId"  placeholder="入库单">
                        <input type="hidden" name="InOrderNo" id="InOrderNo" value="${putm.instorageNo }">
				        <input type="hidden" name="InOrderNoId" id="InOrderNoId" value="${putm.instorageId }">
                    </div>
                </div>
            </div>
           </div> 
  	        
  	        
  	        <div  class="row" style="margin:0;padding:0;">
             <div class="control-group span6" style="display: inline">
                <label class="control-label" for="houseName" > 所属库区：</label>
                <div class="controls">
                    <div class="input-append span12" >
                        <input type="text" id="houseName" class="span10" name="houseName" placeholder="所属库区" value='${putm.warehouse_name }' readonly="readonly"/>
				        <input id="instorageHouseId" type="hidden" name="houseId" value="${putm.houseId }">
                    </div>
                </div>
            </div>
            <%--上架时间--%>
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="putTime" >上架时间：</label>
                <div class="controls">
                    <div class="required span12" style=" float: none !important;">
                        <input class="form-control date-picker" type="text" id="putTime" name="putTime"  placeholder="上架时间" value="${putm.putTime }">
                    </div>
                </div>
            </div>
           </div> 
  	      
  	      
  	       <div  class="row" style="margin:0;padding:0;">
               <div class="control-group span6" style="display: inline">
                   <label class="control-label" for="userId" >操作人：</label>
                   <div class="controls">
                       <div class="span12" style=" float: none !important;">
                           <input class="span10" type="text" id="userId" name="userId"  placeholder="操作人">
                           <input type="hidden" name="user" id="user" value="${putm.userId }">
                           <input type="hidden" name="userName" id="userName" value="${putm.userName }">
                       </div>
                   </div>
               </div>

            <%--备注--%>
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="remark" >备注：</label>
                <div class="controls">
                    <div class="input-append   span12" >
                        <input class="span10" type="text" id="remark" name="remark"
                               placeholder="备注" value="${putm.remark}">
                    </div>
                </div>
            </div>
          </div> 
  	       
  	       
  	       <div style="margin-left:10px;">
            <span class="btn btn-info" id="formSave">
		       <i class="ace-icon fa fa-check bigger-110"></i>保存
            </span>
            <span class="btn btn-info" id="formSubmit" onclick = "refer();">
		        <i class="ace-icon fa fa-check bigger-110"></i>&nbsp;提交
            </span>
        </div>
	</form>
	<div id="materialDiv" style="margin:10px;height: 35px;overflow: hidden">
		<!-- 下拉框 -->
		<label class="inline" for="materialInfor">物料：</label>
		<select id="materialType" name="materialType" style="width:100px">
		 <option value=" ">请选择</option>
		 <option value="0">未装箱</option>
		 <option value="1">已装箱</option>
		</select>
		<input type="text" id = "materialInfor" name="materialInfor" style="width:400px;height:35px;" />
		<button id="addMaterialBtn" class="btn btn-xs btn-primary" onclick="addDetail();">
			<i class="icon-plus" style="margin-right:6px;"></i>添加
		</button>
	</div>
	<!-- 表格div -->
	<div id="grid-div-c" style="width:100%;margin:0px auto;">
		<!-- 	表格工具栏 -->
        <div id="fixed_tool_div" class="fixed_tool_div detailToolBar">
             <div id="__toolbar__-c" style="float:left;overflow:hidden;"></div>
        </div>
		<!-- 物料详情信息表格 -->
		<table id="grid-table-c" style="width:100%;height:100%;"></table>
		<!-- 表格分页栏 -->
		<div id="grid-pager-c"></div>
	</div>
</div>
<script type="text/javascript">
var context_path = '<%=path%>';
var putId=$("#putId").val();
/* var cust=${sale.customer}; */
var oriDataDetail;
 var _grid_detail;        //表格对象
 var lastsel2;
 $(".date-picker").datetimepicker({format: 'YYYY-MM-DD HH:mm:ss',useMinutes:true,useSeconds:true});
$("#baseInfor").validate({
   ignore: "", 
   rules:{
      "putTime":{
         required:true,
      },
      
      "instorageId":{
         required:true,
      },
   },
   messages: {
     "putTime":{
         required:"请选择上架时间！"
      },
    
      "instorageId":{
         required:"请选择入库单！",
      },
   },
   errorClass: "help-inline",
    errorElement: "span",
    highlight:function(element, errorClass, validClass) {
        $(element).parents('.control-group').addClass('error');
    },
    unhighlight: function(element, errorClass, validClass) {
        $(element).parents('.control-group').removeClass('error');
    }
});


$("#baseInfor #instorageId").select2({
		placeholder : "选择入库单编号",
		minimumInputLength : 0, //至少输入n个字符，才去加载数据
		allowClear : true, //是否允许用户清除文本信息
		delay : 250,
		formatNoMatches : "没有结果",
		formatSearching : "搜索中...",
		formatAjaxError : "加载出错啦！",
		ajax : {
			url : context_path + "/putManage/getInstorageNo",
			type : "POST",
			dataType : 'json',
			delay : 250,
			data : function(term, pageNo) { //在查询时向服务器端传输的数据
				term = $.trim(term);
				return {
					queryString : term, //联动查询的字符
					pageSize : 15, //一次性加载的数据条数
					pageNo : pageNo, //页码
					time : new Date()
				//测试
				}
			},
			results : function(data, pageNo) {
				var res = data.result;
				if (res.length > 0) { //如果没有查询到数据，将会返回空串
					var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
					return {
						results : res,
						more : more
					};
				} else {
					return {
						results : {}
					};
				}
			},
			cache : true
		}

	});

if($("#putId").val()!=""){
 $('#baseInfor #instorageId').select2("data",{
        id: $('#InOrderNoId').val(),
        text: $('#InOrderNo').val(),
 });
}
$("#baseInfor #instorageId").change(function(){
    var id=$("#baseInfor #instorageId").val();
    $.ajax({
	      url:context_path + '/putManage/getHouseName?inDocId='+id,
	      type:"POST",
	      dataType:"JSON",
	      success:function(data){
	             if(data.result){
	                 $("#baseInfor #instorageHouseId").val(data.houseId)
	                 $("#baseInfor #houseName").val(data.houseName)
	             }
	             }
	             });
})
$("#formSubmit").click(function(){
  if($("#putId").val()==""){
    layer.msg("请先保存表头信息！");
    return;
  }
  layer.confirm("确定提交单据吗？提交后的单据将不能修改与删除",function(){
  $.ajax({
   url:context_path+"/putManage/submitSure?putId="+$("#putId").val(),
   type:"post",
   dataType:"JSON",
   success:function (data){
      if(data.result){
      layer.closeAll();
      gridReload();
      layer.msg(data.msg,{icon:1,time:1200});
      }else{
      layer.msg(data.msg,{icon:2,time:1200});
      }
   }
   });
   });
  });



$("#formSave").click(function(){
 var bean = $("#baseInfor").serialize();
if($('#baseInfor').valid()){
   saveOrUpdate(bean);
}
});

/* 保存、修改 */
function saveOrUpdate(bean){
   $.ajax({
   url:context_path+"/putManage/save",
   type:"post",
   data:bean,
   dataType:"JSON",
   success:function (data){
      if(data.result){
      $("#putId").val(data.putId);
      $("#baseInfor #putNo").val(data.putNo);
      $("#baseInfor #state").val(data.state);
      putId=data.putId;
      gridReload();
      layer.msg("操作成功！",{icon:1,time:1200});
      }else{
      layer.msg("操作失败！",{icon:2,time:1200});
      }
   }
   });

}
//单元格编辑成功后，回调的函数
	var editFunction = function eidtSuccess(XHR){
		 var data = eval("("+XHR.responseText+")"); 
		if(data["msg"]!=""){
			layer.alert(data["msg"]);
		}
		jQuery("#grid-table-c").jqGrid('setGridParam', 
				{
					postData: {
						id:$('#id').val(),
						queryJsonString:""
					} 
				}
		  ).trigger("reloadGrid");
	};
	
 var detailId=0; 
 var flag=0;
 var select_data = '';
 var select_dataLocation="";
  	_grid_detail=jQuery("#grid-table-c").jqGrid({
         url : context_path + '/putManage/detailList?putId='+putId,
         datatype : "json",
         colNames : [ '详情主键','货物编号','货物名称','批次号','箱号','数量','最大数量','到期时间','rfid标签','选择上架库位','类型'],
         colModel : [ 
  					  {name : 'id',index : 'id',width : 20,hidden:true}, 
  					  {name : 'materialNo',index:'materialNo',width :20}, 
                      {name : 'materialName',index:'materialName',width : 20}, 
                      {name : 'batchNo',index:'batchNo',width : 30},
                      {name : 'boxCode',index:'boxCode',width : 30},
                      {name : 'amount', index: 'amount', width: 30,editable : true,editrules: {custom: true, custom_func: numberRegex},},
                      {name : 'maxAmount',index:'maxAmount',width : 30},
                      {name: 'finaldate', index: 'finaldate', width: 30,editable:true,editoptions:{
			                dataInit:function(e){
			                    $(e).datepicker({
			                        //language:"zh-CN",//语言
			                        autoclose: true,//自动关闭
			                        todayBtn: "linked",//
			                        format: "yyyy-mm-dd"//时间显示格式
			                    });
			                    $(this).click(function(e){//选中时间后隐藏
			                        $(e).parent().datepicker('hide');
			                    });
			                }
           				 }
            		  },
                      {name : 'rfid',index:'rfid',width : 30,editable : true,editrules:  {custom: true, custom_func: numberRegex},},
                      {
		                name: 'goodsLocation',
		                index: 'goodsLocation',
		                width: 25,
		                editable: true,
		                edittype: "select",
		                 editoptions:{
		                 value:gettypesLocation(),  
		                 dataEvents:[{
		                     type:'focus',
		                     fn:function(e){
		                     var id  = $('#grid-table-c').jqGrid('getGridParam','selrow');
		                     $("#grid-table-c select option").remove();
		                     $.ajax({
						            type: "post",
						            async: false,
						            url: context_path + "/putManage/getLocation?detailId="+id,
						            dataType: "json",
						            success: function (data) {
						                for (let i = 0; i < data.result.length; i++) {
						                     $("#grid-table-c select").prepend("<option value="+data.result[i].id+">"+data.result[i].locationName+"</option>")
						                }
						            }
						        })
		                     }
		                     }
		                 ],
		                }, 
		                
		                formatter: function (cellValu, option, rowObject) {
		                    var vle = cellValu || "";
		                    if (select_dataLocation && select_dataLocation.result) {
		                        for (var i = 0; i < select_dataLocation.result.length; i++) {
		                            if (select_dataLocation.result[i].id == cellValu) {
		                                vle = select_dataLocation.result[i].locationName;
		                            }
		                        }
		                    }
		                    return vle;
		                }
		            }, 
		            {name : 'type',index:'type',width : 20,hidden: true}
                    ],
         rowNum : 20,
         rowList : [ 10, 20, 30 ],
         pager : '#grid-pager-c',
         sortname : 'ID',
         sortorder : "asc",
         altRows: true,
         viewrecords : true,
         caption : "物料列表",
         autowidth:true,
         onCellSelect: function (rowid,iCol,cellcontent,e) {
            $("#grid-pager-c").setColProp('goodsLocation',{editoptions:{value:"0:未推荐;1:已推荐"}});
            
         },
         afterSaveCell:function(rowid, cellname,value, iRow, iCol){
            reloadDetailTableList();
         },
         multiselect:true,
		 multiboxonly: true,
         loadComplete : function(data) 
         {
             var table = this;
             setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
             oriDataDetail = data;
             $(window).triggerHandler('resize.jqGrid');
         },
       cellurl : context_path + "/putManage/updateInfo",
	   cellEdit: true,
	  // cellsubmit : "clientArray",
  	   emptyrecords: "没有相关记录",
  	   loadtext: "加载中...",
  	   pgtext : "页码 {0} / {1}页",
  	   recordtext: "显示 {0} - {1}共{2}条数据",
    });
    //在分页工具栏中添加按钮
    $("#grid-table-c").navGrid('#grid-pager-c',{edit:false,add:false,del:false,search:false,refresh:false})
      .navButtonAdd('#grid-pager-c',{  
  	   caption:"",   
  	   buttonicon:"ace-icon fa fa-refresh green",   
  	   ondblClickRow: function(){   
  	    $("#grid-table-c").jqGrid('setGridParam', 
				{
  	    			url:context_path + '/putManage/detailList?putId='+putId,
					postData: {queryJsonString:""} //发送数据  :选中的节点
				}
		  ).trigger("reloadGrid");
  	   }
  	});
  	
  	$(window).on('resize.jqGrid', function () {
  		$("#grid-table-c").jqGrid( 'setGridWidth', $("#grid-div-c").width() - 3 );
  		var height = $(".layui-layer-title",_grid_detail.parents(".layui-layer")).height()+
            $("#baseInfor").outerHeight(true)+$("#materialDiv").outerHeight(true)+
            $("#grid-pager-c").outerHeight(true)+$("#fixed_tool_div.fixed_tool_div.detailToolBar").outerHeight(true)+
            $("#gview_grid-table-c .ui-jqgrid-titlebar").outerHeight(true)+
            $("#gview_grid-table-c .ui-jqgrid-hbox").outerHeight(true);
          $("#grid-table-c").jqGrid('setGridHeight',_grid_detail.parents(".layui-layer").height()-height);
  	});
  	$(window).triggerHandler('resize.jqGrid');
  	if($("#id").val()!=""){
  		//reloadDetailTableList();   //重新加载详情列表
  	}
    if(adapteRfid==1){//禁用
			$("#grid-table-c").setGridParam().hideCol("rfid").trigger("reloadGrid");
			 $("#grid-table-c").setGridWidth($("#baseInfor").width()-3);
		}
  
//将数据格式化成两位小数：四舍五入
 function formatterNumToFixed(value,options,rowObj){
	if(value!=null){
		var floatNum = parseFloat(value);
		return floatNum.toFixed(2);
	}else{
		return "0.00";
	}
 } 
 //数量输入验证
function numberRegex(value, colname) {
    var regex = /^\d+\.?\d{0,2}$/;
   // reloadDetailTableList();
    if (!regex.test(value)) {
        return [false, ""];
    }
    else  return [true, ""];
}


/*      $('#materialInfor').select2({
			placeholder : "请选择物料",//文本框的提示信息
			minimumInputLength : 0, //至少输入n个字符，才去加载数据
			allowClear : true, //是否允许用户清除文本信息
			multiple: true,
			closeOnSelect:false,
			ajax : {
				url : context_path + '/putManage/getMListByInId',
				dataType : 'json',
				delay : 250,
				data : function(term, pageNo) { //在查询时向服务器端传输的数据
					term = $.trim(term);
					selectParam = term;
					return {
						queryString : term, //联动查询的字符
						pageSize : 15, //一次性加载的数据条数
						pageNo : pageNo, //页码
						time : new Date(),
						putId:$("#putId").val(),
						materialType : $("#materialType").val(), 
					//测试
					}
				},
				results : function(data, pageNo) {
					var res = data.result;
					if (res.length > 0) { //如果没有查询到数据，将会返回空串
						var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
						return {
							results : res,
							more : more
						};
					} else {
						return {
							results : {
								"id" : "0",
								"text" : "没有更多结果"
							}
						};
					}

				},
				cache : true
			}
		}); */
$("#userId").select2({
    placeholder: "选择操作人",
    minimumInputLength: 0, //至少输入n个字符，才去加载数据
    allowClear: true, //是否允许用户清除文本信息
    delay: 250,
    formatNoMatches: "没有结果",
    formatSearching: "搜索中...",
    formatAjaxError: "加载出错啦！",
    ajax: {
        url: context_path + "/ASNmanage/getSelectUser",
        type: "POST",
        dataType: 'json',
        delay: 250,
        data: function (term, pageNo) { //在查询时向服务器端传输的数据
            term = $.trim(term);
            return {
                queryString: term, //联动查询的字符
                pageSize: 15, //一次性加载的数据条数
                pageNo: pageNo, //页码
                time: new Date()
                //测试
            }
        },
        results: function (data, pageNo) {
            var res = data.result;
            if (res.length > 0) { //如果没有查询到数据，将会返回空串
                var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                return {
                    results: res,
                    more: more
                };
            } else {
                return {
                    results: {}
                };
            }
        },
        cache: true
    }

   });
		$('#materialInfor').on("change",function(e){
			var datas=$("#materialInfor").select2("val");
			selectData = datas;
			var selectSize = datas.length;
			if(selectSize>1){
				var $tags = $("#s2id_materialInfor .select2-choices");   //
				//$("#s2id_materialInfor").html(selectSize+"个被选中");
				var $choicelist = $tags.find(".select2-search-choice");
				var $clonedChoice = $choicelist[0];
				$tags.children(".select2-search-choice").remove();
				$tags.prepend($clonedChoice);
				
				$tags.find(".select2-search-choice").find("div").html(selectSize+"个被选中");
				$tags.find(".select2-search-choice").find("a").removeAttr("tabindex");
				$tags.find(".select2-search-choice").find("a").attr("href","#");
				$tags.find(".select2-search-choice").find("a").attr("onclick","removeChoice();");
			}else if (selectSize==1){
			   var $tags = $("#s2id_materialInfor .select2-choices");   //
				//$("#s2id_materialInfor").html(selectSize+"个被选中");
				var $choicelist = $tags.find(".select2-search-choice");
				var $clonedChoice = $choicelist[0];
				$tags.children(".select2-search-choice").remove();
				$tags.prepend($clonedChoice);
				var text=$tags.find(".select2-search-choice").find("div").html();
				if(text.length>40){
				  var text=$tags.find(".select2-search-choice").find("div").html("1个被选中");
				}
			}
			//执行select的查询方法
			$("#materialInfor").select2("search",selectParam);
		});
		
if($("#putId").val()!=""){

 $("#customer").select2("data", {
   id: $("#customerId").val(),
   text: $("#customerName").val()
  });
    $("#userId").select2("data", {
        id: $("#user").val(),
        text: $("#userName").val()
    });
}else{
  $('#customer').val("")
}
 //清空物料多选框中的值
 function removeChoice(){
	  $("#s2id_materialInfor .select2-choices").children(".select2-search-choice").remove();
	  $("#materialInfor").select2("val","");
	  selectData = 0;
	  
 }

//添加物料详情
 function addDetail(){
	  if($("#putId").val()==""){
		  layer.alert("请先保存表单信息！");
		  return;
	  }
	  if(selectData!=0){
		  //将选中的物料添加到数据库中
		  $.ajax({
			  type:"POST",
			  url:context_path + '/putManage/saveDetail',
			  data:{putId:$('#baseInfor #putId').val(),detailIds:selectData.toString(),materialType:$("#materialType").val()},
			  dataType:"json",
			  success:function(data){
				  removeChoice();   //清空下拉框中的值
				  if(Boolean(data.result)){
						layer.msg("添加成功",{icon:1,time:1200});
					  //重新加载详情表格
					 reloadDetailTableList();
				  }else{
					  layer.msg(data.msg,{icon:2,time:1200});
				  }
			  }
		  });
	  }else{
		  layer.alert("请选择物料！");
	  }
  }
  //工具栏
	  $("#__toolbar__-c").iToolBar({
	   	 id:"__tb__01",
	   	 items:[
	   	  	{label:"删除", onclick:delDetail},
	    ]
	  });
	//删除物料详情
	function delDetail(){
	   var ids = jQuery("#grid-table-c").jqGrid('getGridParam', 'selarrrow');
	   $.ajax({
	      url:context_path + '/putManage/deleteDetail?ids='+ids,
	      type:"POST",
	      dataType:"JSON",
	      success:function(data){
	         if(data.result){
	           layer.msg("操作成功！");
	            //重新加载详情表格
				reloadDetailTableList();
	         }
	      }
	   });
	}
	 function reloadDetailTableList(){
       $("#grid-table-c").jqGrid('setGridParam', 
				{
  	    			url:context_path + '/putManage/detailList?putId='+putId,
					postData: {queryJsonString:""} //发送数据  :选中的节点
				}
		  ).trigger("reloadGrid");
		  } 
 function startPut(id){
    $.ajax({
	      url:context_path + '/putManage/getDetailById?id='+id,
	      type:"POST",
	      dataType:"JSON",
	      success:function(data){
	             if(data.result){
	                        layer.confirm(data.msg, /*显示的内容*/
								        {
								            shift: 6,
								            moveType: 1, //拖拽风格，0是默认，1是传统拖动
								            title:"操作提示",  /*弹出框标题*/
								            icon: 3,      /*消息内容前面添加图标*/
								            btn: ['确定', '取消']/*可以有多个按钮*/
								        }, function(index, layero){
								            //确定按钮的回调
								            //获取表格中选中的用户记录
								            $.ajax({
								                url:context_path+"/putManage/startPut",
								                type:"POST",
								                data:{id : id},
								                dataType:"json",
								                success:function(data){
								                    if(data){
								                        layer.msg("操作成功!",{icon:1});
								                        //刷新列表
								                        reloadDetailTableList();
								                        layer.close(index);
								                    }else{
								                        layer.alert("删除失败!",{icon:2});
								                        layer.close(index);
								                    }
								                }
								            });
								
								        }, function(index){
								            //取消按钮的回调
								            layer.close(index);
								        });
	             }else{
	               layer.alert(data.msg,{icon:2});
	             }
	         
	      }
	      });
 }
 function completeMod(){
    alert(1);
 }
 
    function gettypesLocation() {
        var str = "";
        $.ajax({
            type: "post",
            async: false,
            url: context_path + "/putManage/getAllLocation",
            dataType: "json",
            success: function (data) {
                select_dataLocation = data;
                for (let i = 0; i < data.result.length; i++) {
                    str += data.result[i].id + ":" + data.result[i].locationName + ";";
                }
                str = str.substring(0, str.length - 1);
            }
        })
        return str;
    }



    $("#materialType").select2();
    /* $('#materialType').on("change",function(e){
      removeChoice();
    }); */
    $("#materialType").change(function(){
	    if(this.value=='0'){
	    	 $('#materialInfor').select2({
				placeholder : "请选择物料",//文本框的提示信息
				minimumInputLength : 0, //至少输入n个字符，才去加载数据
				allowClear : true, //是否允许用户清除文本信息
				multiple: true,
				closeOnSelect:false,
				ajax : {
					url : context_path + '/putManage/getMListByInId',
					dataType : 'json',
					delay : 250,
					data : function(term, pageNo) { //在查询时向服务器端传输的数据
						term = $.trim(term);
						selectParam = term;
						return {
							queryString : term, //联动查询的字符
							pageSize : 15, //一次性加载的数据条数
							pageNo : pageNo, //页码
							time : new Date(),
							putId:$("#putId").val(),
							materialType : $("#materialType").val(), 
						//测试
						}
					},
					results : function(data, pageNo) {
						var res = data.result;
						if (res.length > 0) { //如果没有查询到数据，将会返回空串
							var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
							return {
								results : res,
								more : more
							};
						} else {
							return {
								results : {
									"id" : "0",
									"text" : "没有更多结果"
								}
							};
						}
	
					},
					cache : true
				}
			});
	    }else if(this.value=='1'){
	    	$('#materialInfor').select2({
				placeholder : "请选择翻包号",//文本框的提示信息
				minimumInputLength : 0, //至少输入n个字符，才去加载数据
				allowClear : true, //是否允许用户清除文本信息
				multiple: true,
				closeOnSelect:false,
				ajax : {
					url : context_path + '/putManage/getMListByInId',
					dataType : 'json',
					delay : 250,
					data : function(term, pageNo) { //在查询时向服务器端传输的数据
						term = $.trim(term);
						selectParam = term;
						return {
							queryString : term, //联动查询的字符
							pageSize : 15, //一次性加载的数据条数
							pageNo : pageNo, //页码
							time : new Date(),
							putId:$("#putId").val(),
							materialType : $("#materialType").val(), 
						//测试
						}
					},
					results : function(data, pageNo) {
						var res = data.result;
						if (res.length > 0) { //如果没有查询到数据，将会返回空串
							var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
							return {
								results : res,
								more : more
							};
						} else {
							return {
								results : {
									"id" : "0",
									"text" : "没有更多结果"
								}
							};
						}
	
					},
					cache : true
				}
			});
	    }else{
	   	// removeChoice();
	    }
	    
    
    })
    
    
    
  </script>
