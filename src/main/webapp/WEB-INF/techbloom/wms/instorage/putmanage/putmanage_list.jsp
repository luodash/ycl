<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<body style="overflow:hidden;">
<div id = "putmanage_list_grid-div">
	<form id="putmanage_list_hiddenForm" action = "<%=path%>/putManage/exportExcel" method = "POST" style="display: none;">
		<!-- 选中的用户 -->
		<input id="putmanage_list_ids" name="ids" value=""/>
	</form>
		<!-- 隐藏区域,存放查询条件 -->
		<form id = "putmanage_list_hiddenQueryForm" style = "display:none;">
		<input id = "putmanage_list_id" name = "id" value = "">
		<!-- 上架单编号 -->
		<input name = "putNo" id = "putmanage_list_putNo" value = ""/>
		<!-- 入库单编号 -->
		<input name = "instorageNo" id = "putmanage_list_instorageNo" value = ""/>
		<!-- 库区 -->
		<input name = "WAREHOUSE_NAME" id = "putmanage_list_WAREHOUSE_NAME" value = ""/>
		</form>
		<div class="query_box" id="putmanage_list_yy" title="查询选项">
            <form id="putmanage_list_queryForm" style="max-width:100%;">
			 <ul class="form-elements">
				<li class="field-group field-fluid3">
					<label class="inline" for="putmanage_list_putNo" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:92px;">上架单编号：</span>
						<input type="text" id="putmanage_list_putNo" name="putNo" value="" style="width: calc(100% - 97px);" placeholder="上架单编号">
					</label>			
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="putmanage_list_instorageNo" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:92px;">入库单编号：</span>
						<input type="text" id="putmanage_list_instorageNo" name="instorageNo" value="" style="width:calc(100% - 97px);" placeholder="入库单编号">
					</label>					
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="putmanage_list_houseId" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:92px;">库区：</span>
						<input type="text" id="putmanage_list_houseId" name="houseId" value="" style="width: calc(100% - 97px);" placeholder="库区">
					</label>					
				</li>
				<li class="field-group-top field-group field-fluid3">
					<label class="inline" for="putmanage_list_putTime" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:92px;">上架时间：</span>
						<input type="text" class="form-control date-picker" id="putmanage_list_putTime" name="putTime" style="width: calc(100% - 97px);" placeholder="上架时间" />
					</label>			
				</li>
				
			</ul>
			<div class="field-button" style="">
					<div class="btn btn-info" onclick="queryOk();">
				        <i class="ace-icon fa fa-check bigger-110"></i>查询
			        </div>
				    <div class="btn" onclick="reset();"><i class="ace-icon icon-remove"></i>重置</div>
				    <a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>
		        </div>
		  </form>		 
    </div>
    <div id="putmanage_list_fixed_tool_div" class="fixed_tool_div">
        <div id="putmanage_list___toolbar__" style="float:left;overflow:hidden;"></div>
    </div>
    <!--    物料信息表格 -->
    <table id="putmanage_list_grid-table" style="width:100%;height:100%;"></table>
    <!-- 	表格分页栏 -->
    <div id="putmanage_list_grid-pager"></div>			
   </div>
</body>
  	<script type="text/javascript" src="<%=path%>/plugins/public_components/js/select2.js"></script>
 	<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
 	<script type="text/javascript" src="<%=path%>/static/js/techbloom/wms/instorage/putmanage/putmanage.js"></script>
	<script type="text/javascript">
	$(function  (){
       $(".toggle_tools").click();
    });
     $(".date-picker").datetimepicker({format: "YYYY-MM-DD"});
	//选择入库单编号
	$("#putmanage_list_queryForm #putmanage_list_ins").select2({
		placeholder : "选择入库单编号",
		minimumInputLength : 0, //至少输入n个字符，才去加载数据
		allowClear : true, //是否允许用户清除文本信息
		delay : 250,
		formatNoMatches : "没有结果",
		formatSearching : "搜索中...",
		formatAjaxError : "加载出错啦！",
		ajax : {
			url : context_path + "/putManage/getInstorageNo",
			type : "POST",
			dataType : 'json',
			delay : 250,
			data : function(term, pageNo) { //在查询时向服务器端传输的数据
				term = $.trim(term);
				return {
					queryString : term, //联动查询的字符
					pageSize : 15, //一次性加载的数据条数
					pageNo : pageNo, //页码
					time : new Date()
				//测试
				}
			},
			results : function(data, pageNo) {
				var res = data.result;
				if (res.length > 0) { //如果没有查询到数据，将会返回空串
					var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
					return {
						results : res,
						more : more
					};
				} else {
					return {
						results : {}
					};
				}
			},
			cache : true
		}

	});
	
	//选择库区
	$("#putmanage_list_warehouse").select2({
		placeholder : "选择库区",
		minimumInputLength : 0, //至少输入n个字符，才去加载数据
		allowClear : true, //是否允许用户清除文本信息
		delay : 250,
		formatNoMatches : "没有结果",
		formatSearching : "搜索中...",
		formatAjaxError : "加载出错啦！",
		ajax : {
			url : context_path + "/putManage/getWareHouse",
			type : "POST",
			dataType : 'json',
			delay : 250,
			data : function(term, pageNo) { //在查询时向服务器端传输的数据
				term = $.trim(term);
				return {
					queryString : term, //联动查询的字符
					pageSize : 15, //一次性加载的数据条数
					pageNo : pageNo, //页码
					time : new Date()
				//测试
				}
			},
			results : function(data, pageNo) {
				var res = data.result;
				if (res.length > 0) { //如果没有查询到数据，将会返回空串
					var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
					return {
						results : res,
						more : more
					};
				} else {
					return {
						results : {}
					};
				}
			},
			cache : true
		}

	});	
	var context_path = '<%=path%>';
    var oriData;      //表格数据
    var _grid;        //表格对象
    var dynamicDefalutValue="710fe7c0dd8c4d16b8c57ced6643e1c3";
     $(function()
     {
           $("#putmanage_list___toolbar__").iToolBar({
             id:"putmanage_list___tb__01",
             items:[
                {label:"添加",disabled:(${sessionUser.addQx}==1?false:true),onclick:addPutManage,iconClass:'icon-plus'},
          		{label:"编辑",disabled:(${sessionUser.editQx}==1?false:true),onclick:editPutManage,iconClass:'icon-pencil'},
          		{label:"删除",disabled:(${sessionUser.deleteQx}==1?false:true),onclick:delPutManage,iconClass:'icon-trash'},
          		{label:"上架亮灯",disabled:(${sessionUser.deleteQx}==1?false:true),onclick:put,iconClass:'icon-pencil'},
          		{label:"上架确认",disabled:(${sessionUser.deleteQx}==1?false:true),onclick:toSure,iconClass:'icon-ok'},
          		{label: "查看", disabled:(${sessionUser.queryQx} == 1 ? false : true),onclick:viewDetailList, iconClass:'icon-zoom-in'},
		 		{label:"导出",disabled:(${sessionUser.queryQx}==1?false:true),onclick:excelPutManage,iconClass:'icon-share'}
     	]
     	});
     	if(adapteRfid==0){//启用
			$("#putmanage_list___tb__01 .itbtn-grp")[4].style.display='none';
		}else{//禁用
			$("#putmanage_list___tb__01 .itbtn-grp")[3].style.display='none';
		}
        _grid = jQuery("#putmanage_list_grid-table").jqGrid({
            url : context_path + '/putManage/getPutManageList',
            datatype : "json",
            colNames : [ '主键','上架单编号','入库单编号','所属库区','上架时间','状态'],
            colModel : [
                {name : 'id',index : 'id',width : 100,hidden:true},
                {name : 'putNo',index : 'putNo',width : 100},
                {name : 'instorageNo',index:'instorageNo',width : 100 },
                {name : 'warehouseName',index:'warehouse',width : 100 },
                {name : 'putTime',index:'putTime',width : 100,
                    formatter:function(cellValu,option,rowObject){
                        if (cellValu != null) {
                            return getFormatDateByLong(new Date(cellValu), "yyyy-MM-dd HH:mm");
                        } else {
                            return "";
                        }
                    }
				},
                {name : 'state',index:'state',width:50,formatter:function(cellvalue,option,rowObject){
                    if(cellvalue==-1){
                        return "<span style='color:#EEC211;font-weight:bold;'>未提交</span>";
                    }
                    if(cellvalue==0){
                        return "<span style='color:#d15b47;font-weight:bold;'>未上架</span>";
                    }
                    if(cellvalue==1){
                        return "<span style='color:#76b86b;font-weight:bold;'>正在上架</span>";
                    }
                    if(cellvalue==2){
                        return "<span style='color:#76b86b;font-weight:bold;'>上架完成</span>";
                    }
                    }}
            ],
            rowNum : 20,
            rowList : [ 10, 20, 30 ],
            pager : '#putmanage_list_grid-pager',
            sortname : 'ID',
            sortorder : "desc",
            altRows: true,
            viewrecords : true,
            autowidth:true,
            multiselect:true,
            multiboxonly: true,
            beforeRequest:function (){
                dynamicGetColumns(dynamicDefalutValue,"putmanage_list_grid-table",$(window).width()-$("#sidebar").width() -7);
                //重新加载列属性
            },
            loadComplete : function(data)
            {
                var table = this;
                setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
                oriData = data;
            },
            emptyrecords: "没有相关记录",
            loadtext: "加载中...",
            pgtext : "页码 {0} / {1}页",
            recordtext: "显示 {0} - {1}共{2}条数据"
        });
        //在分页工具栏中添加按钮
        jQuery("#putmanage_list_grid-table").navGrid('#putmanage_list_grid-pager',{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd('#putmanage_list_grid-pager',{
                caption:"",
                buttonicon:"ace-icon fa fa-refresh green",
                onClickButton: function(){
                    $("#putmanage_list_grid-table").jqGrid('setGridParam',
                        {
                            postData: {queryJsonString:""} //发送数据
                        }
                    ).trigger("reloadGrid");
                }
            }).navButtonAdd('#putmanage_list_grid-pager',{
                caption: "",
                buttonicon:"fa  icon-cogs",
                onClickButton : function (){
                    jQuery("#putmanage_list_grid-table").jqGrid('columnChooser',{
                        done: function(perm, cols){
                            dynamicColumns(cols,dynamicDefalutValue);
                            $("#putmanage_list_grid-table").jqGrid( 'setGridWidth', $("#putmanage_list_grid-div").width()-3);
                        }
                    });
                }
            });

        $(window).on("resize.jqGrid", function () {
            $("#putmanage_list_grid-table").jqGrid("setGridWidth", $("#putmanage_list_grid-div").width() - 3 );
            var height = $("#breadcrumb").outerHeight(true)+$(".query_box").outerHeight(true)+
            $("#putmanage_list_fixed_tool_div").outerHeight(true)+
            $("#gview_putmanage_list_grid-table .ui-jqgrid-hbox").outerHeight(true)+
            $("#putmanage_list_grid-pager").outerHeight(true)+$("#header").outerHeight(true);
            $("#putmanage_list_grid-table").jqGrid("setGridHeight", (document.documentElement.clientHeight)-height );
        });

        $(window).triggerHandler('resize.jqGrid');
    });
    var _queryForm_data = iTsai.form.serialize($('#putmanage_list_queryForm'));
    function queryOk(){
        var queryParam = iTsai.form.serialize($('#putmanage_list_queryForm'));
        //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
        queryInstoreListByParam(queryParam);

    }
    function reset(){
        iTsai.form.deserialize($('#putmanage_list_queryForm'),_queryForm_data);
        $("#putmanage_list_queryForm #putmanage_list_houseId").select2("val","");
        $("#putmanage_list_queryForm #putmanage_list_ins").select2("val","");
        queryInstoreListByParam(_queryForm_data);
    }
			
	$("#putmanage_list_queryForm #putmanage_list_houseId").select2({
        placeholder: "请选择库区",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/instorageOrder/getSelectArea",
            type: "POST",
            dataType: 'json',
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });	
			
	</script>