<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<style type="text/css">	
	.query_box .field-button.two {
        padding: 0px;
        left: 650px;
    }
</style>
<body style="overflow:hidden;">
   <div id="receiptmanage_list_grid-div">
   <!-- 隐藏区域：存放查询条件 -->
		<form id="receiptmanage_list_hiddenQueryForm"  action ="<%=path%>/ASNmanage/excel" style="display:none;">
			<!-- 收货单编号 -->
			<input id="receiptmanage_list_ids" name="ids" value="" />
			<!-- 收货单名称 -->
			<input id="receiptmanage_list_receiptNo" name="receiptNo" value="" />
			<!-- 供应商 -->
			<input id="receiptmanage_list_supplyId" name="supplyId" value="" />
			<!-- 收货人 -->
			<input id="receiptmanage_list_receiptUser" name="receiptUser" value="" /> 
		</form>
		<div class="query_box" id="receiptmanage_list_yy" title="查询选项">
            <form id="receiptmanage_list_queryForm" style="max-width:100%;">
			 <ul class="form-elements">
				<li class="field-group field-fluid3">
					<label class="inline" for="receiptmanage_list_receiptNo" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:80px;">收货单编号：</span>
						<input type="text" name="receiptNo" id="receiptmanage_list_receiptNo" value="" style="width: calc(100% - 85px);" placeholder="收货单编号">
					</label>			
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="receiptmanage_list_supplyId" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:80px;">供应商：</span>
						<input type="text" id="receiptmanage_list_sss" name="supplyId" value="" style="width: calc(100% - 85px);" placeholder="供应商">
					</label>					
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="receiptmanage_list_receiptUser" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:80px;">收货人：</span>
						<input type="text" id="receiptmanage_list_rrr" name="receiptUser" value="" style="width: calc(100% - 85px);" placeholder="收货人">
					</label>					
				</li>
				
			</ul>
			<div class="field-button" style="">
						<div class="btn btn-info" onclick="queryOk();">
				            <i class="ace-icon fa fa-check bigger-110"></i>查询
			            </div>
						<div class="btn" onclick="reset();"><i class="ace-icon icon-remove"></i>重置</div>
		        </div>
		  </form>		 
    </div>
        <div id="receiptmanage_list_fixed_tool_div" class="fixed_tool_div">
             <div id="receiptmanage_list___toolbar__" style="float:left;overflow:hidden;"></div>
        </div>
		<table id="receiptmanage_list_grid-table" style="width:100%;height:100%;"></table>
		<div id="receiptmanage_list_grid-pager"></div>
   </div>
</body>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript" src="<%=path%>/static/js/techbloom/wms/instorage/receipt/receiptManage.js"></script>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriData; 
    var _grid;
    var dynamicDefalutValue="427ac722baef4159a1e7f40aa4887888";
    $(function  (){
       $(".toggle_tools").click();
    });
    $("#receiptmanage_list___toolbar__").iToolBar({
   	  	 id:"receiptmanage_list___tb__01",
   	  	 items:[
   	  	    {label:"添加",disabled:(${sessionUser.addQx}==1?false:true),onclick:addReceiptOrder,iconClass:'glyphicon glyphicon-plus'},
   	  	    {label:"编辑",disabled:(${sessionUser.editQx}==1?false:true),onclick:editAllocateOrder,iconClass:'glyphicon glyphicon-pencil'},
	   	  	{label:"删除",disabled:(${sessionUser.deleteQx}==1?false:true),onclick:delOutStorageOrder,iconClass:'glyphicon glyphicon-trash'},
	   	  	{label:"查看",disabled:(${sessionUser.deleteQx}==1?false:true),onclick:showView,iconClass:'glyphicon glyphicon-pencil'},
	   	  	{label:"收货",disabled:(${sessionUser.deleteQx}==1?false:true),onclick:receiptSure,iconClass:'glyphicon glyphicon-pencil'},
	   	  	{label:"导出", disabled:(${sessionUser.queryQx} == 1 ? false : true),onclick:function () {
                var selectid = jQuery("#receiptmanage_list_grid-table").jqGrid("getGridParam", "selarrrow");;
                $("#receiptmanage_list_ids").val(selectid);
	   	  	    $("#receiptmanage_list_hiddenQueryForm").submit();},iconClass:' icon-share'},
			{label:"打印", disabled:(${sessionUser.queryQx} == 1 ? false : true),onclick:function(){	
					  	  	var queryBean = iTsai.form.serialize($('#receiptmanage_list_hiddenQueryForm'));   //获取form中的值：json对象
				            var queryJsonString = JSON.stringify(queryBean); 
				            var url = context_path + "/ASNmanage/exportPrint?"+"ids=" + jQuery("#receiptmanage_list_grid-table").jqGrid('getGridParam', 'selarrrow');;
				            window.open(url);  
				  	  	},iconClass:' icon-print'}
				   	  	
				   	  ]
			   	});
    
   $(function(){
		_grid = jQuery("#receiptmanage_list_grid-table").jqGrid({
	       url : context_path + "/ASNmanage/list.do",
	       datatype : "json",
	       colNames : [ "收货单主键","收货单编号","紧急程度","供应商","预计收货时间", "实际收货时间","收货人","备注","状态"],
	       colModel : [ 
	                    {name : "id",index : "id",width : 20,hidden:true}, 
	                    {name : "receiptNo",index : "receiptNo",width : 40},
			   			{name : "urgency",index :"urgency",width : 10,formatter:function(cellValue,option,rowObject){
                                if(typeof cellValue == "number"){
                                    if(cellValue==1){
                                        return "<img style='width:30px;' src='<%=request.getContextPath()%>/plugins/public_components/img/green.png'/>";
                                    }else if(cellValue==2){
                                        return "<img style='width:30px;' src='<%=request.getContextPath()%>/plugins/public_components/img/yellow.png'/>";
                                    }else if(cellValue==3){
                                        return "<img style='width:30px;' src='<%=request.getContextPath()%>/plugins/public_components/img/red.png'/>";
                                    }
                                }
                            }},
	                    {name : "supplyName",index : "supplyName",width : 40},
	                    {name : "receiptTime",index : "receiptTime",width : 40,
                            formatter:function(cellValu,option,rowObject){
                                if (cellValu != null) {
                                    return getFormatDateByLong(new Date(cellValu), "yyyy-MM-dd HH:mm");
                                } else {
                                    return "";
                                }
                            }
						},
	                    {name : "realTime",index : "realTime",width : 40,
                            formatter:function(cellValu,option,rowObject){
                                if (cellValu != null) {
                                    return getFormatDateByLong(new Date(cellValu), "yyyy-MM-dd HH:mm");
                                } else {
                                    return "";
                                }
                            }
						},
	                    {name : "userName",index : "userName",width : 40},
	                    {name : "remark",index : "REMARK",width : 40}, 
	                    {name : "state",index : "state",width : 40,
	                       formatter:function(cellValue,option,rowObject){
	                    		if(typeof cellValue == "number"){
	                    			if(cellValue==0){
	                    				return "<span style='color:grey;font-weight:bold;'>未提交</span>";
	                    			}else if(cellValue==1){
	                    				return "<span style='color:#d15b47;font-weight:bold;'>未收货</span>";
	                    			}else if(cellValue==2){
	                    				return "<span style='color:#76b86b;font-weight:bold;'>已收货</span>";
	                    			}
	                    		}else{
	                    			return "异常";
	                    		}
	                    	}
	                    } 
	                  ],
	       rowNum : 20,
	       rowList : [ 10, 20, 30 ],
	       pager : "#receiptmanage_list_grid-pager",
	       sortname : "id",
	       sortorder : "desc",
	       altRows: false, 
	       viewrecords : true,
	       autowidth:true,
	       multiselect:true,
		   multiboxonly: true,
           beforeRequest:function (){
                dynamicGetColumns(dynamicDefalutValue,"receiptmanage_list_grid-table",$(window).width()-$("#sidebar").width() -7);
                //重新加载列属性
           },
	       loadComplete : function(data) {
	           var table = this;
	           setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
	           oriData = data;
	       },
	       emptyrecords: "没有相关记录",
	       loadtext: "加载中...",
	       pgtext : "页码 {0} / {1}页",
	       recordtext: "显示 {0} - {1}共{2}条数据"
	  });
	 //在分页工具栏中添加按钮
	  jQuery("#receiptmanage_list_grid-table").navGrid("#receiptmanage_list_grid-pager",{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#receiptmanage_list_grid-pager",{  
		   caption:"",   
		   buttonicon:"ace-icon fa fa-refresh green",   
		   onClickButton: function(){
			   $("#receiptmanage_list_grid-table").jqGrid("setGridParam", 
						{
							postData: {queryJsonString:""} //发送数据 
						}
				  ).trigger("reloadGrid");
		   }
		}).navButtonAdd("#receiptmanage_list_grid-pager",{
              caption: "",
              buttonicon:"fa icon-cogs",
              onClickButton : function (){
                  jQuery("#receiptmanage_list_grid-table").jqGrid("columnChooser",{
                      done: function(perm, cols){
                          dynamicColumns(cols,dynamicDefalutValue);
                          $("#receiptmanage_list_grid-table").jqGrid("setGridWidth", $("#receiptmanage_list_grid-div").width()-3);
                      }
                  });
              }
          });
	  $(window).on("resize.jqGrid", function () {
		  $("#receiptmanage_list_grid-table").jqGrid("setGridWidth", $("#receiptmanage_list_grid-div").width() - 3 );
		  var height = $("#breadcrumb").outerHeight(true)+$("#receiptmanage_list_yy").outerHeight(true)+
          $("#receiptmanage_list_fixed_tool_div").outerHeight(true)+
          $("#gview_receiptmanage_list_grid-table .ui-jqgrid-hbox").outerHeight(true)+
          $("#receiptmanage_list_grid-pager").outerHeight(true)+$("#header").outerHeight(true);
          $("#receiptmanage_list_grid-table").jqGrid("setGridHeight", (document.documentElement.clientHeight)-height );
	  })
	
	  $(window).triggerHandler("resize.jqGrid");
    }); 
   var _queryForm_data = iTsai.form.serialize($("#receiptmanage_list_queryForm"));
	function queryOk(){
		var queryParam = iTsai.form.serialize($("#receiptmanage_list_queryForm"));
		//执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
		queryInstoreListByParam(queryParam);		
	}
	
	function reset(){
		iTsai.form.deserialize($('#receiptmanage_list_queryForm'),_queryForm_data); 
		$("#receiptmanage_list_allocateTypeSelect").val("").trigger('change');
		$("#receiptmanage_list_inWarehouseSelect").val("").trigger('change');
		$("#receiptmanage_list_outWarehouseSelect").val("").trigger('change');
		$("#receiptmanage_list_queryForm #sss").select2("val","");
		$("#receiptmanage_list_queryForm #rrr").select2("val","");
		queryInstoreListByParam(_queryForm_data);
		$("#receiptmanage_list_allocateTypeSelect").find("option[text='所有类型']").attr("selected",true);
		$("#receiptmanage_list_outWarehouseSelect").find("option[text='所有库区']").attr("selected",true);
		$("#receiptmanage_list_inWarehouseSelect").find("option[text='所有库区']").attr("selected",true);
	}
 $('#receiptmanage_list_queryForm .mySelect2').select2();
 $(".date-picker").datetimepicker({format: 'YYYY-MM-DD',useMinutes:true,useSeconds:true});
 function reloadGrid(){
   _grid.trigger("reloadGrid");
 }
 $("#receiptmanage_list_queryForm #receiptmanage_list_rrr").select2({
        placeholder: "选择收货人",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/ASNmanage/getSelectUser",
            type: "POST",
            dataType: "json",
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }

    });  
    $("#receiptmanage_list_queryForm #receiptmanage_list_sss").select2({
        placeholder: "选择供应商",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/ASNmanage/getSelectSupply",
            type: "POST",
            dataType: "json",
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }

    });
</script>