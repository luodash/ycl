<%@ page language="java" import="java.lang.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<div class="main-content" style="height:100%">
	<div class="widget-header widget-header-large" id="receiptmanage_view_div1">
		<!-- 隐藏的asn主键 -->
		<input type="hidden" id="receiptmanage_view_receiptId" name="id" value="${receipt.id }">
		<h3 class="widget-title grey lighter"
			style=' background: none; border-bottom: none; '>
			<i class="ace-icon fa fa-leaf green"></i> ASN单据
		</h3>

		<div class="widget-toolbar no-border invoice-info">
			<span class="invoice-info-label">收货单编号:</span> <span class="red">${receipt.receiptNo }</span>
			<br /> <span class="invoice-info-label">预计收货时间:</span> <span class="blue">${receipt.receiptTime}</span>
		</div>
		<!-- 打印按钮 -->
		<div class="widget-toolbar hidden-480">
			<a href="#" onclick="printDoc();"> <i class="ace-icon fa fa-print"></i></a>
		</div>
	</div>

	<div class="widget-body" id="receiptmanage_view_div2">
		<table
			style="width: 100%;font-size:16px;border-collapse:separate;border-spacing:2px 3px;">
			<tr class="trClass">
				<td><i class="ace-icon fa fa-caret-right blue"></i> 供应商： <b
					class="black">${receipt.supplyName }</b></td>
				<td><i class="ace-icon fa fa-caret-right blue"></i> 收货人: <b
					class="black">${receipt.userName }</b></td>
			</tr>
			<tr>
			<tr>
				<td><i class="ace-icon fa fa-caret-right blue"></i> 备注: <b
					class="black">${receipt.remark }</b></td>
				<td><i class="ace-icon fa fa-caret-right blue"></i> 状态 : <c:if
						test="${receipt.state==0 }">
						<span class="red">未提交</span>
					</c:if> <c:if test="${receipt.state==1 }">
						<span class="red">未收货</span>
					</c:if> <c:if test="${receipt.state==2 }">
						<span class="green">已收货</span>
					</c:if></td>
			</tr>
			</tr>
			<tr>
				<td><i class="ace-icon fa fa-caret-right blue"></i> 紧急程度 : <c:if
						test="${receipt.urgency==1 }">
						<span class="green">普通</span>
					</c:if> <c:if test="${receipt.urgency==2 }">
						<span class="yellow">严重</span>
					</c:if> <c:if test="${receipt.urgency==3 }">
						<span class="red">紧急</span>
					</c:if></td>
				<td><i class="ace-icon fa fa-caret-right blue"></i> 库位 : <b
					class="red">${receipt.receptionName }</b></td>
			</tr>
		</table>
	</div>
	<!-- 详情表格 -->
	<div id="receiptmanage_view_grid-div-c">
		<!-- 物料信息表格 -->
		<table id="receiptmanage_view_grid-table-c" style="width:100%;height:100%;"></table>
		<!-- 表格分页栏 -->
		<div id="receiptmanage_view_grid-pager-c"></div>
	</div>

</div>
<!-- /.main-content -->
<script type="text/javascript">
	var oriDataView;
	var _grid_view;        //表格对象
 	_grid_view=jQuery("#receiptmanage_view_grid-table-c").jqGrid({
         url : context_path + '/ASNmanage/detailList?receiptId='+$("#receiptmanage_view_receiptId").val(),
         datatype : "json",
         colNames : [ '详情主键','物料编号','物料名称','批次号','数量','实际收货数量'],
         colModel : [ 
  					  {name : 'id',index : 'id',width : 20,hidden:true}, 
  					  {name : 'materialNo',index:'materialNo',width :20}, 
                      {name : 'materialName',index:'materialName',width : 20},
                      {name : 'batchNo', index: 'batchNo', width: 60 }, 
                      {name : 'amount', index: 'amount', width: 20 },
                      {name : 'actAmount',index:'actAmount',width : 20}
                    ],
         rowNum : 20,
         rowList : [ 10, 20, 30 ],
         pager : '#receiptmanage_view_grid-pager-c',
         sortname : 'ID',
         sortorder : "asc",
         altRows: true,
         viewrecords : true, 
         autowidth:true,
		 multiboxonly: true,
         loadComplete : function(data){
             var table = this;
             setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
             oriDataDetail = data;
              $(window).triggerHandler('resize.jqGrid');
         },
	   cellEdit: true,
	   cellsubmit : "clientArray",
  	   emptyrecords: "没有相关记录",
  	   loadtext: "加载中...",
  	   pgtext : "页码 {0} / {1}页",
  	   recordtext: "显示 {0} - {1}共{2}条数据",
    });
    
    $(window).on("resize.jqGrid", function () {
  		$("#receiptmanage_view_grid-table-c").jqGrid("setGridWidth", $("#receiptmanage_view_grid-div-c").width() - 3 );
  		 var height =$(".layui-layer-title",_grid_view.parents(".layui-layer")).height()+
  		             $("#receiptmanage_view_div1").outerHeight(true)+$("#receiptmanage_view_div2").outerHeight(true)+
  		             $("#receiptmanage_view_grid-pager-c").outerHeight(true)+
  		             $("#gview_receiptmanage_view_grid-table-c .ui-jqgrid-hbox").outerHeight(true);
                     $("#receiptmanage_view_grid-table-c").jqGrid("setGridHeight",_grid_view.parents(".layui-layer").height()-height);
  	});
  	$(window).triggerHandler("resize.jqGrid");
  function printDoc(){
	var url = context_path + "/ASNmanage/printInstorageDetail?instoreID="+$("#receiptmanage_view_id").val();
	window.open(url);
}
</script>