<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tbl.modules.wms.entity.instorage.Qualitybill"%>
<%@page import="com.tbl.modules.wms.entity.instorage.QualitybillDetail"%>
<%@page import="com.mysql.jdbc.StringUtils" %>
<%@ page import="com.google.common.base.Strings" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
    List<Qualitybill> instorageList =(List<Qualitybill>)request.getAttribute("list");
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">

    <title>质检单打印</title>

    <script type="text/javascript">
        function aa(){
            window.print();
        }
    </script>
</head>
<body onload="aa();">
<%
    double allAmount = 0;
    if(instorageList != null && instorageList.size()>0){
        for(int b = 0;b <instorageList.size(); b++){
%>
<br/>
<h3 align="center" style="font-weight: bolder;padding-top: 5px;">质检单</h3>
<table style="width: 20.2cm;font-size: 12px;" align="center">
    <tr>
        <td>质检编号：<%=instorageList.get(b).getQualityNo()==null?"":instorageList.get(b).getQualityNo()%></td>
        <td>质检日期：<%=instorageList.get(b).getCreatetime()==null?"":instorageList.get(b).getCreatetime() %></td>
    </tr>
</table>
<!--table的最大高度为height:21cm;  -->
<table style="border-collapse: collapse; border: none;width: 20cm;font-size: 12px;" align="center">
    <tr>
        <td style="border: solid #000 1px;width:2cm;">序号</td>
        <td style="border: solid #000 1px;width:2cm;">物料编号</td>
        <td style="border: solid #000 1px;width:50cm;">物料名称</td>
        <td style="border: solid #000 1px;width:2cm;">应收数量</td>
        <td style="border: solid #000 1px;width:2cm;">实收数量</td>
    </tr>
    <%
        int len = instorageList.get(b).getDetailList().size();
        int s = 4 - len;
        double yhj = 0;
        double jeyhj = 0;
        for(int i= 0;i< len ;i++){
            List<QualitybillDetail> dail = instorageList.get(b).getDetailList();
            //应收数量页合计数量
            String ss = dail.get(i).getAllAmount();
            if(Strings.isNullOrEmpty(ss)){
                ss = "0";
                yhj = yhj+Double.parseDouble(ss);
            }else{
                yhj =  yhj + Double.parseDouble(dail.get(i).getAllAmount());
            }
            //实收数量也合计数量
            String dd = dail.get(i).getAllQualityAmount();
            if(Strings.isNullOrEmpty(dd)){
                dd = "0";
                jeyhj = jeyhj+Double.parseDouble(dd);
            }else{
                jeyhj = jeyhj+Double.parseDouble(dail.get(i).getAllQualityAmount());
            }
    %>
    <tr >
        <td style="border: solid #000 1px;"><%=i+1 %></td>
        <td style="border: solid #000 1px;"><%=dail.get(i).getMaterialNo()==null?"":dail.get(i).getMaterialNo() %></td>
        <td style="border: solid #000 1px;"><%=dail.get(i).getMaterialName()==null?"":dail.get(i).getMaterialName() %></td>
        <td style="border: solid #000 1px;"><%=dail.get(i).getAllAmount()==null?"":dail.get(i).getAllAmount() %></td>
        <td style="border: solid #000 1px;"><%=dail.get(i).getAllQualityAmount()==null?"":dail.get(i).getAllQualityAmount() %></td>
    </tr>
    <%
        }
        for(int i= 0;i< s ;i++){
    %>
    <tr >
        <td style="border: solid #000 1px;">&nbsp;</td>
        <td style="border: solid #000 1px;">&nbsp;</td>
        <td style="border: solid #000 1px;">&nbsp;</td>
        <td style="border: solid #000 1px;">&nbsp;</td>
        <td style="border: solid #000 1px;">&nbsp;</td>
    </tr>
    <%
        }
        yhj = Math.round(yhj * 10000) / 10000.0;
        jeyhj = Math.round(jeyhj * 10000) / 10000.0;

        double allamount = Double.parseDouble(instorageList.get(b).getAmount());
        allamount = Math.round(allamount * 10000) / 10000.0;

        double allQualityAmount = Double.parseDouble(instorageList.get(b).getQualityAmount());
        allQualityAmount = Math.round(allQualityAmount * 10000) / 10000.0;

    %>
    <tr >
        <td style="border: solid #000 1px;white-space:nowrap;">页合计：</td>
        <td style="border: solid #000 1px;white-space:nowrap;"></td>
        <td style="border: solid #000 1px;white-space:nowrap;"></td>
        <td style="border: solid #000 1px;white-space:nowrap;"><%=yhj %></td>
        <td style="border: solid #000 1px;white-space:nowrap;"><%=jeyhj %></td>

    </tr>
    <tr >
        <td style="border: solid #000 1px;white-space:nowrap;">合计：</td>
        <td style="border: solid #000 1px;white-space:nowrap;"></td>
        <td style="border: solid #000 1px;white-space:nowrap;"></td>
        <td style="border: solid #000 1px;white-space:nowrap;"><%=allamount %></td>
        <td style="border: solid #000 1px;white-space:nowrap;"><%=allQualityAmount %></td>
    </tr>
</table>
<table style="border-collapse: collapse; border: none;width: 20.2cm;font-size: 12px;margin-bottom: 10px;" align="center">
    <%--<tr>
        <td>制单人：<%=instorageList.get(b).getRealName()==null?"":instorageList.get(b).getRealName() %></td>
    </tr>--%>
</table>
<%

%>
<%
    if (1 <instorageList.size()&&b!=instorageList.size()-1){
%>
<!--  <div style="width: 16.2cm;height:133px;">
    &nbsp;
 </div> -->
<div id="xx" style="height:80px;"></div>
<%
            }
        }
    }
%>
</body>
</html>

