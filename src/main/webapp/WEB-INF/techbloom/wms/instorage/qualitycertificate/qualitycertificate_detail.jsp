<%@ page language="java" import="java.lang.*"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
	String path = request.getContextPath();
%>
<div class="main-content" style="height:100%">
	<div class="widget-header widget-header-large" id="qualitycertificate_detail_div1">
		<!-- 隐藏的asn主键 -->
		<input type="hidden" id="qualitycertificate_detail_id" name="id" value="${quality.id }" />
		<h3 class="widget-title grey lighter" style=' background: none; border-bottom: none; '>
			<i class="ace-icon fa fa-leaf green"></i>
			质检单
		</h3>
		<div class="widget-toolbar no-border invoice-info">
			<span class="invoice-info-label">质检单编号:</span>
			<span class="red">${quality.qualityNo }</span>
			<br />
			<span class="invoice-info-label">质检时间:</span>
			<span class="blue">${fn:substring(quality.createtime, 0, 19)}</span>
		</div>
		<!-- 打印按钮 -->
		<div class="widget-toolbar hidden-480">
			<a href="#" onclick="printDoc();" title="详情打印">
				<i class="ace-icon fa fa-print"></i>
			</a>
		</div>
	</div>

	<div class="widget-body" id="div2">
		<table style="width: 100%;font-size:16px;border-collapse:separate;border-spacing:2px 3px;">
			<tr>
				<td>
					<i class="ace-icon fa fa-caret-right blue"></i>
					质检单编号：
					<b class="black">${quality.qualityNo }</b>
				</td>
				<td>
					<i class="ace-icon fa fa-caret-right blue"></i>
					ASN单号：
					<b class="black">${quality.goodsName }</b>
				</td>
			</tr>

			<tr>
				<td>
					<i class="ace-icon fa fa-caret-right blue"></i>
					质检人：
					<b class="black">${quality.userName }</b>
				</td>
				<td>
					<i class="ace-icon fa fa-caret-right blue"></i>
					质检状态：
					<c:if test="${quality.state==0 }"><span style="color:#d15b47;font-weight:bold;">未质检</span></c:if>
					<c:if test="${quality.state==1 }"><span style="color:#87b87f;font-weight:bold;">质检退回</span></c:if>
					<c:if test="${quality.state==2 }"><span style="color:#76b86b;font-weight:bold;">质检完成</span></c:if>
				</td>
			</tr>
			<tr>
				<td>
					<i class="ace-icon fa fa-caret-right blue"></i>
					库位：
					<b class="red">${quality.locationName }</b>
				</td>
			</tr>
		</table>
	</div>
	<!-- 详情表格 -->
	<div id="qualitycertificate_detail_grid-div-c">
		<!-- 物料信息表格 -->
		<table id="qualitycertificate_detail_grid-table-c" style="width:100%;height:100%;"></table>
		<!-- 表格分页栏 -->
		<div id="qualitycertificate_detail_grid-pager-c"></div>
	</div>
</div>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriData;      //表格数据
    var _grid;        //表格对象
    var getbillsStatus = '${GETBILLS.getbillsStatus}';   //下架单状态
    $(function() {
        //初始化表格
        _grid = jQuery("#qualitycertificate_detail_grid-table-c").jqGrid({
            url : context_path + "/qualityCertificate/DetailList?qId="+$("#qualitycertificate_detail_id").val(),
            datatype : "json",
            colNames : [ "物料主键","物料编号","物料名称","批次号","应收数量","实收数量","样本数量","合格数量","合格百分比(%)"],
            colModel : [
                {name : "id",index : "id",width : 55,hidden:true},
                {name : "materialNo",index:"materialNo",width : 50},
                {name : "materialName",index:"materialName",width : 50},
                {name : "batchNo",index:"batchNo",width : 100},
                {name : "amount",index:"amount",width : 50},
                {name : "qualityAmount",index:"qualityAmount",width : 50},
                {name : "sampleAmount",index:"sampleAmount",width : 50},
                {name : "eligibility",index:"eligibility",width : 50},
                {name : "rate",index:"rate",width : 50}
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : "#qualitycertificate_detail_grid-pager-c",
        sortname : "ID",
        sortorder : "asc",
        altRows: true,
        viewrecords: true,
        autowidth: true,
        multiselect: false,
        multiboxonly: true,
        loadComplete: function (data) {
            var table = this;
            setTimeout(function () {
               updatePagerIcons(table);
               enableTooltips(table);
            }, 0);
            oriData = data;
            $(window).triggerHandler("resize.jqGrid");
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext: "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
    });
        //在分页工具栏中添加按钮
        jQuery("#qualitycertificate_detail_grid-table-c").navGrid("#qualitycertificate_detail_grid-pager-c", {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false
        }).navButtonAdd("#qualitycertificate_detail_grid-pager-c", {
            caption: "",
            buttonicon: "ace-icon fa fa-refresh green",
            onClickButton: function () {
                $("#qualitycertificate_detail_grid-table-c").jqGrid("setGridParam",
                    {
                        postData: {PUTBILLSID: $("#qualitycertificate_detail_id").val()} //发送数据  :选中的节点
                    }
                ).trigger("reloadGrid");
            }
        });

        $(window).on("resize.jqGrid", function () {
            $("#qualitycertificate_detail_grid-table-c").jqGrid("setGridWidth", $("#qualitycertificate_detail_grid-div-c").width() - 3);
            var height = $(".layui-layer-title",_grid.parents(".layui-layer")).height()+
                $("#qualitycertificate_detail_div1").outerHeight(true)+$("#qualitycertificate_detail_div2").outerHeight(true)+
                $("#gview_qualitycertificate_detail_grid-table-c .ui-jqgrid-hbox").outerHeight(true)+
                $("#qualitycertificate_detail_grid-pager-c").outerHeight(true);
                $("#qualitycertificate_detail_grid-table-c").jqGrid("setGridHeight", _grid.parents(".layui-layer").height()-height);
        });
    });

    //将数据格式化成两位小数：四舍五入
    function formatterNumToFixed(value,options,rowObj){
        if(value!=null){
            if(rowObj.id==-1){
                return "";
            }else{
                var floatNum = parseFloat(value);
                return floatNum.toFixed(2);
            }
        }else{
            return "0.00";
        }
    }

    function printDoc(){
        var url = context_path + "/getbills/printGetbillsDetail?putId="+$("#qualitycertificate_detail_id").val();
        window.open(url);
    }

    function offTheShelf(id,locationId,amount,state){
        $.ajax({
            type : "POST",
            url : context_path + '/getbills/setState.do?id=' + id+'&locationId='+locationId+'&state='+state+'&amount='+amount,
            dataType : 'json',
            cache : false,
            success : function(data) {
                if (Boolean(data.result)) layer.msg(data.msg, {icon: 1, time: 1000});
                else layer.msg(data.msg, {icon: 7, time: 1000});
                $("#qualitycertificate_detail_grid-table-c").jqGrid('setGridParam',{
                    postData: {queryJsonString: ""} //发送数据
                }).trigger("reloadGrid");

                $("#qualitycertificate_detail_grid-table").jqGrid('setGridParam',{
                    postData: {queryJsonString: ""} //发送数据
                }).trigger("reloadGrid");
            }
        });
    }
</script>
