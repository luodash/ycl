<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<div id="qualitycertificate_list_grid-div">
    <form id="qualitycertificate_list_hiddenQueryForm" action="<%=path%>/qualityCertificate/exportExcel" method="POST" style="display:none;" >
        <input id="qualitycertificate_list_ids" name="ids" value="" />
        <input id="qualitycertificate_list_qualityNo" name="qualityNo" />
        <input id="qualitycertificate_list_receiptNumber" name="receiptNumber" />
        <input id="qualitycertificate_list_tt" name="createTime"  />
        <input id="qualitycertificate_list_qq" name="qualityUser"  />
    </form>
    <div class="query_box" id="qualitycertificate_list_yy" title="查询选项">
         <form id="qualitycertificate_list_queryForm" style="max-width:100%;">
			 <ul class="form-elements">
				<li class="field-group field-fluid3">
					<label class="inline" for="qualityNo" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:78px;">质检单号：</span>
						<input id="qualitycertificate_list_qualityNo" name="qualityNo" type="text" style="width:calc(100% - 84px );" placeholder="质检单号" />
					</label>			
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="qualitycertificate_list_receiptNumber" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:78px;">ASN单号：</span>
						<input id="qualitycertificate_list_receiptNumber" name="receiptNumber" type="text" style="width: calc(100% - 84px );" placeholder="ASN单号" />
					</label>					
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="qualitycertificate_list_createTime" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:78px;">质检时间：</span>
						<input id="qualitycertificate_list_tt" name="createTime" class="form-control date-picker" type="text" style="width: calc(100% - 84px );" placeholder="质检时间" />
					</label>			
				</li>
				<li class="field-group-top field-group field-fluid3">
					<label class="inline" for="qualitycertificate_list_qualityUser" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:78px;">质检人：</span>
						<input id="qualitycertificate_list_uu" name="qualityUser" type="text" style="width: calc(100% - 84px );" placeholder="质检人">
					</label>					
				</li>	
			</ul>
			<div class="field-button">
					<div class="btn btn-info" onclick="queryOk();">
				        <i class="ace-icon fa fa-check bigger-110"></i>查询
			        </div>
				    <div class="btn" onclick="reset();"><i class="ace-icon icon-remove"></i>重置</div>
				    <a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>
		        </div>
		  </form>		 
    </div>
    <div id="qualitycertificate_list_fixed_tool_div" class="fixed_tool_div">
        <div id="qualitycertificate_list___toolbar__" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="qualitycertificate_list_grid-table" style="width:100%;height:100%;"></table>
    <div id="qualitycertificate_list_grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/static/js/techbloom/wms/instorage/qualitycertificate/quality.js"></script>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriData;      //表格数据
    var _grid;        //表格对象
    var dynamicDefalutValue="bb51d81f207c45c380c3410cb78f0627";
    $(".date-picker").datetimepicker({format: "YYYY-MM-DD"});
    $(function(){
       $(".toggle_tools").click();
    });
    $(function(){
        //工具栏
        $("#qualitycertificate_list___toolbar__").iToolBar({
            id:"qualitycertificate_list___tb__01",
            items:[
                {label:"添加",disabled:(${sessionUser.addQx}==1?false:true),onclick:add,iconClass:'icon-plus'},
                {label:"编辑",disabled:(${sessionUser.editQx}==1?false:true),onclick:edit,iconClass:'icon-pencil'},
                {label:"删除",disabled:(${sessionUser.editQx}==1?false:true),onclick:del,iconClass:'icon-trash'},
                {label:"查看", disabled:(${sessionUser.queryQx} == 1 ? false : true),onclick:viewDetail, iconClass:'icon-zoom-in'},
                {label:"导出",disabled:(${sessionUser.queryQx}==1?false:true),onclick:exportQuality,iconClass:'icon-share'},
                {label:"打印",disabled:(${sessionUser.queryQx}==1?false:true),onclick:printQuality,iconClass:' icon-print'},
                {label:"生成入库计划",disabled:(${sessionUser.addQx}==1?false:true),onclick:spiltQuality,iconClass:'icon-plus'}
            ]
    });
        //初始化表格
        _grid = jQuery("#qualitycertificate_list_grid-table").jqGrid({
            url : context_path + "/qualityCertificate/list",
            datatype : "json",
            colNames : [ "主键","质检单编号","ASN单号","质检时间","质检人","质检状态"],
            colModel : [
                {name : "id",index : "id",width : 20,hidden:true},
                {name : "qualityNo",index : "qualityNo",width : 50},
                {name : "goodsName",index : "goodsName",width : 50},
                {name : "createtime",index : "createtime",width : 50,
                    formatter:function(cellValu,option,rowObject){
                        if (cellValu != null) {
                            return getFormatDateByLong(new Date(cellValu), "yyyy-MM-dd HH:mm");
                        } else {
                            return "";
                        }
                    }
				},
                {name : "userName",index :"userName",width:50},
                {name : "state",index : "state",width : 30,
                    formatter:function(cellvalue, options, rowObject){
                        if(cellvalue==0){
                            return "<span style='color:gray;font-weight:bold;'>未质检</span>" ;
                        }else if(cellvalue==1){
                            return "<span style='color:orange;font-weight:bold;'>质检退回</span>" ;
                        }else if(cellvalue==2){
                            return "<span style='color:green;font-weight:bold;'>质检完成</span>" ;
                        }
                    }
                }
            ],
            rowNum : 20,
            rowList : [ 10, 20, 30 ],
            pager : "#qualitycertificate_list_grid-pager",
            sortname : "q.ID",
            sortorder : "desc",
            altRows: true,
            viewrecords : true,
            autowidth:true,
            multiselect:true,
            multiboxonly: true,
            beforeRequest:function (){
                dynamicGetColumns(dynamicDefalutValue,"qualitycertificate_list_grid-table",$(window).width()-$("#sidebar").width() -7);
                //重新加载列属性
            },
            loadComplete : function(data){
                var table = this;
                setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
                oriData = data;
            },
            emptyrecords: "没有相关记录",
            loadtext: "加载中...",
            pgtext : "页码 {0} / {1}页",
            recordtext: "显示 {0} - {1}共{2}条数据"
        });
        //在分页工具栏中添加按钮
        jQuery("#qualitycertificate_list_grid-table").navGrid("#qualitycertificate_list_grid-pager",{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd('#qualitycertificate_list_grid-pager',{
                caption:"",
                buttonicon:"ace-icon fa fa-refresh green",
                onClickButton: function(){
                    $("#qualitycertificate_list_grid-table").jqGrid("setGridParam",
                        {
                            postData: {queryJsonString:""} //发送数据
                        }
                    ).trigger("reloadGrid");
                }
            }).navButtonAdd("#qualitycertificate_list_grid-pager",{
                caption: "",
                buttonicon:"fa icon-cogs",
                onClickButton : function (){
                    jQuery("#qualitycertificate_list_grid-table").jqGrid("columnChooser",{
                        done: function(perm, cols){
                            dynamicColumns(cols,dynamicDefalutValue);
                            $("#qualitycertificate_list_grid-table").jqGrid("setGridWidth", $("#qualitycertificate_list_grid-div").width()-3);
                        }
                    });
                }
            });
        $(window).on("resize.jqGrid", function () {
            $("#qualitycertificate_list_grid-table").jqGrid("setGridWidth", $("#qualitycertificate_list_grid-div").width() - 3 );
            var height = $("#breadcrumb").outerHeight(true)+$("#yy").outerHeight(true)+
                         $("#qualitycertificate_list_fixed_tool_div").outerHeight(true)+
                         $("#gview_qualitycertificate_list_grid-table .ui-jqgrid-hbox").outerHeight(true)+
                         $("#qualitycertificate_list_grid-pager").outerHeight(true)+$("#header").outerHeight(true);
                         $("#qualitycertificate_list_grid-table").jqGrid("setGridHeight", (document.documentElement.clientHeight)-height );
        });
        $(window).triggerHandler("resize.jqGrid");
    });
    var _queryForm_data = iTsai.form.serialize($("#qualitycertificate_list_queryForm"));
    function queryOk(){
        var queryParam = iTsai.form.serialize($("#qualitycertificate_list_queryForm"));
        //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
        queryInstoreListByParam(queryParam);

    }
    function reset(){
        iTsai.form.deserialize($('#qualitycertificate_list_queryForm'),_queryForm_data);
        $("#qualitycertificate_list_queryForm #qualitycertificate_list_uu").select2("val","");
        $("#qualitycertificate_list_queryForm #asn").select2("val","");
        //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
        queryInstoreListByParam(_queryForm_data);
    }
  $("#qualitycertificate_list_queryForm #qualitycertificate_list_uu").select2({
	    placeholder: "选择质检员",
	    minimumInputLength: 0, //至少输入n个字符，才去加载数据
	    allowClear: true, //是否允许用户清除文本信息
	    delay: 250,
	    formatNoMatches: "没有结果",
	    formatSearching: "搜索中...",
	    formatAjaxError: "加载出错啦！",
	    ajax: {
	        url: context_path + "/ASNmanage/getSelectUser",
	        type: "POST",
	        dataType: "json",
	        delay: 250,
	        data: function (term, pageNo) { //在查询时向服务器端传输的数据
	            term = $.trim(term);
	            return {
	                queryString: term, //联动查询的字符
	                pageSize: 15, //一次性加载的数据条数
	                pageNo: pageNo, //页码
	                time: new Date()
	            }
	        },
	        results: function (data, pageNo) {
	            var res = data.result;
	            if (res.length > 0) { //如果没有查询到数据，将会返回空串
	                var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
	                return {
	                    results: res,
	                    more: more
	                };
	            } else {
	                return {
	                    results: {}
	                };
	            }
	        },
	        cache: true
	    }
	
	});
	/*查看入库单详情*/
function viewDetail() {
    var checkedNum = getGridCheckedNum("#qualitycertificate_list_grid-table", "id");
	if (checkedNum == 0) {
		layer.alert("请选择一条要查看的质检单！");
		return false;
	} else if (checkedNum > 1) {
		layer.alert("只能选择一条质检单进行查看！");
		return false;
	} else {
	var id = jQuery("#qualitycertificate_list_grid-table").jqGrid("getGridParam","selrow");
	$.get(context_path + "/qualityCertificate/viewDetail?id=" + id).done(
			function(data) {
				layer.open({
					title : "质检单查看",
					type : 1,
					skin : "layui-layer-molv",
					area : [ "750px", "600px" ],
					shade : 0.6, // 遮罩透明度
					moveType : 1, // 拖拽风格，0是默认，1是传统拖动
					anim : 2,
					content : data
				});
			});	
	    }	
     }
</script>