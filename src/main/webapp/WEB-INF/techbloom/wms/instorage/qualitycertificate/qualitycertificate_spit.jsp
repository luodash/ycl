<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
%>
<div class="widget-box" style="border:0px;">
    <form id="qualitycertificate_spit_baseInfor" style="width:870px;margin:10px auto;border-bottom: solid 2px #3b73af;">
        <div id="qualitycertificate_spit_fromInfoContent">
            <!-- 隐藏的入库单主键 -->
            <input type="hidden" id="qualitycertificate_spit_id" name="id" value="${qc.id }">
            <div class="inline" style="margin-bottom:5px;">
                <label class="inline" for="qualitycertificate_spit_qualityNo" style="margin-right:20px;"> 质检单编号：<span class="field-required">*</span>
                    <input id="qualitycertificate_spit_qualityNo" name="qualityNo" value="${qc.qualityNo }" readonly="readonly" style="width:200px;" />
                </label>
            </div>
            <div class="inline" style="margin-bottom:5px;">
                <label class="inline" for="qualitycertificate_spit_goodsId" style="margin-right:20px;">ASN单号&emsp;：<span class="field-required">*</span>
                    <input type="hidden" id="qualitycertificate_spit_goodsId" name="goodsId" value="${qc.goodsId }">
                    <select readonly="readonly" class="mySelect2" id="qualitycertificate_spit_instorageTypeSelect" name="instorageTypeSelect" data-placeholder="请选择收货单" style="width:200px;">
                        <option value=""></option>
                        <c:forEach items="${goodsList}" var="in">
                            <option value="${in.id}" <c:if test="${qc.goodsId==in.id }">selected="selected"</c:if>>
                                    ${in.receiptNo}
                            </option>
                        </c:forEach>
                    </select>
                </label>
                <label class="inline" for="qualitycertificate_spit_qualityUser" style="margin-right:20px;">质检人：<span class="field-required">*</span>
                    <input type="hidden" id="qualitycertificate_spit_qualityUser" name="qualityUser" value="${qc.qualityUser }">
                    <select class="mySelect2" readonly="readonly" id="qualitycertificate_spit_qualityUserSelect" name="qualityUserSelect" data-placeholder="请选择质检人" style="width:200px;">
                        <option value=""></option>
                        <c:forEach items="${user}" var="in">
                            <option value="${in.userId}" <c:if test="${qc.qualityUser==in.userId }">selected="selected"</c:if>>${in.name}</option>
                        </c:forEach>
                    </select>
                </label>
            </div>
        </div>
    </form>
    <!-- 入库单搜索div -->
    <div id="qualitycertificate_spit_materialDiv" style="margin:10px;"></div>
    <!-- 表格div -->
    <div id="qualitycertificate_spit_grid-div-d" style="width:870px;margin:10px auto;">
        <!-- 隐藏区域：存放查询条件 -->
        <form id="qualitycertificate_spit_hiddenQueryForm" style="display:none;">
            <!-- 物料编号 -->
            <input name="materialName" id="qualitycertificate_spit_materialName" value="" />
            <!-- 物料名称-->
            <input id="qualitycertificate_spit_materialNo" name="materialNo" value="">
        </form>
        <!-- 	表格工具栏 -->
        <div id="qualitycertificate_spit_fixed_tool_div" class="fixed_tool_div detailToolBar">
            <div id="qualitycertificate_spit___toolbar__-c" style="float:left;overflow:hidden;"></div>
        </div>
        <!-- 入库单信息表格 -->
        <table id="qualitycertificate_spit_grid-table-d"></table>
        <!-- 表格分页栏 -->
        <div id="qualitycertificate_spit_grid-pager-d"></div>
    </div>
</div>
<iframe src="about:blank" name="_ifr" height="0" class="hidden"></iframe>
<script type="text/javascript" src="<%=path%>/static/js/techbloom/wms/instorage/qualitycertificate/quality_detail.js"></script>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriData;      //表格数据
    var _grid_detail;        //表格对象

    $(".date-picker").datetimepicker({format: "YYYY-MM-DD HH:mm:ss",useMinutes:true,useSeconds:true});
    $("#qualitycertificate_spit_baseInfor .mySelect2").select2();
    $("#qualitycertificate_spit_baseInfor #qualitycertificate_spit_instorageTypeSelect").change(function(){
        $("#qualitycertificate_spit_baseInfor #qualitycertificate_spit_goodsId").val($("#qualitycertificate_spit_baseInfor #qualitycertificate_spit_instorageTypeSelect").val());
    });
    //初始化下拉框:质检人选择
    $("#qualitycertificate_spit_baseInfor #qualitycertificate_spit_qualityUserSelect").change(function(){
        $("#qualitycertificate_spit_baseInfor #qualitycertificate_spit_qualityUser").val($("#qualitycertificate_spit_baseInfor #qualitycertificate_spit_qualityUserSelect").val());
    });
    //工具栏
    $("#qualitycertificate_spit___toolbar__-c").iToolBar({
        id:"qualitycertificate_spit___tb__01",
        items:[
            {label:"确认", onclick:spiltOk}
        ]
    });

    //初始化表格
_grid_detail=$("#qualitycertificate_spit_grid-table-d").jqGrid({
        url : context_path + "/qualityCertificate/getSpiltList?qId="+$("#qualitycertificate_spit_baseInfor #qualitycertificate_spit_id").val(),
        datatype : "json",
        colNames : [ "物料主键","物料编号","物料名称","物料数量","剩余数量","产品拆分数量"],
        colModel : [
            {name : "id",index : "id",width : 55,hidden:true},
            {name : "materialNo",index:"materialNo",width : 50},
            {name : "materialName",index:"materialName",width : 50},
            {name : "qualityAmount",index:"qualityAmount",width : 50},
            {name : "restAmount",index:"restAmount",width : 50},
            {name : "lastAmount",width : 50,index:"lastAmount", editable: true, editrules: {custom: true, custom_func: myamountcheck},
                editoptions: {
                    size: 25, dataEvents: [{
                        type: 'blur',
                        fn: function (e) {
                            var $element = e.currentTarget;
                            var $elementId = $element.id;
                            var rowid = $elementId.split("_")[0];
                            var reg = new RegExp("^([0-9]*|[0]{1,1})$");
                            if (!reg.test($("#"+$elementId).val())) {
                                layer.alert("数量输入错误！(注:只可以输入正整数和0)");
                                return;
                            }
                            var id = $("#qualitycertificate_spit_grid-table-d").jqGrid('getGridParam', 'selrow');
                            var rowData = $("#qualitycertificate_spit_grid-table-d").jqGrid('getRowData', id).restAmount;
                            $.ajax({
                                type: "POST",
                                dataType: "json",
                                url: context_path + '/qualityCertificate/updateAmount',
                                data: {lastAmount: $("#" + rowid + "_lastAmount").val(), amount: rowData, id: id},
                                success: function (data) {
                                    if (!data.result) {
                                        layer.alert(data.msg);
                                        $("#qualitycertificate_spit_grid-table-d").jqGrid('setGridParam',
                                            {
                                                url:context_path + '/qualityCertificate/getSpiltList',
                                                postData: {qId:$('#baseInfor #id').val(),queryJsonString:""}
                                            }
                                        ).trigger("reloadGrid");
                                    }
                                }

                            });
                        }
                    }]
                }
            }
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : "#qualitycertificate_spit_grid-pager-d",
        sortname : "id",
        sortorder : "asc",
        altRows: true,
        viewrecords : true,
        autowidth:true,
        multiselect:true,
        multiboxonly: true,
        loadComplete : function(data){
            var table = this;
            setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            oriData = data;
        },
        cellurl : context_path + "/qualityCertificate/saveamount",
        cellEdit: true,
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据",
    });
    //在分页工具栏中添加按钮
    $("#qualitycertificate_spit_grid-table-c").navGrid("#qualitycertificate_spit_grid-pager-d",{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#qualitycertificate_spit_grid-pager-d",{
            caption:"",
            buttonicon:"ace-icon fa fa-refresh green",
            onClickButton: function(){
                $("#qualitycertificate_spit_grid-table-d").jqGrid("setGridParam",
                    {
                        url:context_path + "/qualityCertificate/DetailList",
                        postData: {qId:$("#qualitycertificate_spit_baseInfor #qualitycertificate_spit_id").val(),queryJsonString:""} //发送数据  :选中的节点
                    }
                ).trigger("reloadGrid");
            }
        });
    $(window).on("resize.jqGrid", function () {
        $("#qualitycertificate_spit_grid-table-c").jqGrid("setGridWidth", $("#qualitycertificate_spit_grid-div-c").width()-3);
        var height = $(".layui-layer-title",_grid_detail.parents(".layui-layer")).height()+
                     $("#qualitycertificate_spit_baseInfor").outerHeight(true)+$("#qualitycertificate_spit_materialDiv").outerHeight(true)+
                     $("#qualitycertificate_spit_grid-pager-d").outerHeight(true)+$("#qualitycertificate_spit_fixed_tool_div.fixed_tool_div.detailToolBar").outerHeight(true)+
                     $("#gbox_qualitycertificate_spit_grid-table-d .ui-jqgrid-hdiv").outerHeight(true);
                     $("#qualitycertificate_spit_grid-table-d").jqGrid("setGridHeight",_grid_detail.parents(".layui-layer").height()-height);
    });
    $(window).triggerHandler("resize.jqGrid");
    if($("#qualitycertificate_spit_id").val()!=""){
        reloadDetailTableList();   //重新加载详情列表
    }
    //物料详情数量编辑验证方法：只能为整数类型
    function myamountcheck(value, colname) {
        var reg = new RegExp("^[0-9]+(.[0-9]{1,2})?$");
        if (!reg.test(value)) {
            return [ false, "" ];
        }else {
            return [ true, "" ];
        }
    }
    //关闭当前窗口
    function closeWindowBtn(){
        layer.closeAll();
    }
</script>