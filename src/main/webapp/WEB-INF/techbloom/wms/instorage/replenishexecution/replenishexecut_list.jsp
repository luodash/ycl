<%--
  Created by IntelliJ IDEA.
  User: HQKS-004-05
  Date: 2018/2/6
  Time: 10:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <script type="text/javascript">
        var context_path = '<%=path%>';
        var dynamicDefalutValue="27dda993bc9243238dca5aeeb294056a";
    </script>
    <style type="text/css">
        .query_box .field-button.two {
            padding: 0px;
            left: 650px;
        }
    </style>
</head>
<body style="overflow:hidden;">
<div id="replenishexecut_list_grid-div">
    <!-- 隐藏区域：存放查询条件 -->
    <form id="replenishexecut_list_hiddenQueryForm"  action ="<%=path%>/replenish/excel" style="display:none;">
        <!-- 补货任务编号 -->
        <input id="replenishexecut_list_ids" name="ids" value="" />
        <!-- 补货任务执行编号-->
        <input id="replenishexecut_list_replenishexecutNo" name="replenishexecutNo" value="" >
        <!-- 操作人-->
        <input id="replenishexecut_list_userId" name="userId" value="" >
        <!-- 开始时间-->
        <input id="replenishexecut_list_starttime" name="starttime" value="" >
        <!-- 结束时间-->
        <input id="replenishexecut_list_finishtime" name="finishtime" value="" >
        <!-- 物料名称-->
        <input id="replenishexecut_list_materialid" name="materialid" value="" >
    </form>
    <div class="query_box" id="replenishexecut_list_yy" title="查询选项">
        <form id="replenishexecut_list_queryForm" style="max-width:100%;">
            <ul class="form-elements">
                <li class="field-group field-fluid3">
                    <label class="inline" for="replenishexecut_list_replenishexecutNo" style="margin-right:20px;width:100%;">
                        <span class="form_label" style="width:92px;">补货执行编号：</span>
                        <input type="text" id="replenishexecut_list_replenishexecutNo" name="replenishexecutNo" style="width: calc(100% - 97px);" placeholder="补货执行编号">
                    </label>
                </li>
                <li class="field-group-top field-group field-fluid3">
                    <label class="inline" for="userId" style="margin-right:20px;width:100%;">
                        <span class="form_label" style="width:92px;">操作人：</span>
                        <input type="text" id="replenishexecut_list_userId" name="userId" style="width: calc(100% - 97px);" placeholder="操作人">
                    </label>
                </li>
                <li class="field-group field-fluid3">
                    <label class="inline" for="replenishexecut_list_starttime" style="margin-right:20px;width:100%;">
                        <span class="form_label" style="width:92px;">开始时间：</span>
                        <input type="text" id="replenishexecut_list_starttime" class="form-control date-picker" name="starttime" style="width: calc(100% - 97px);" placeholder="开始时间">
                    </label>
                </li>
                <li class="field-group-top field-group field-fluid3">
                    <label class="inline" for="replenishexecut_list_finishtime" style="margin-right:20px;width:100%;">
                        <span class="form_label" style="width:92px;">结束时间：</span>
                        <input type="text" id="replenishexecut_list_finishtime" class="form-control date-picker" name="finishtime" style="width: calc(100% - 97px);" placeholder="结束时间">
                    </label>
                </li>
                <li class="field-group-top field-group field-fluid3">
                    <label class="inline" for="replenishexecut_list_materialid" style="margin-right:20px;width:100%;">
                        <span class="form_label" style="width:92px;">物料名称：</span>
                        <input type="text" id="replenishexecut_list_materialid" name="materialid" style="width: calc(100% - 97px);" placeholder="物料名称">
                    </label>
                </li>
              
            </ul>
              <div class="field-button" style="">
                    <div class="btn btn-info" onclick="queryOk();">
                        <i class="ace-icon fa fa-check bigger-110"></i>查询
                    </div>
                    <div class="btn" onclick="reset();"><i class="ace-icon icon-remove"></i>重置</div>
                    <a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>
                </div>
        </form>
    </div>
    <div id="replenishexecut_list_fixed_tool_div" class="fixed_tool_div">
        <div id="replenishexecut_list___toolbar__" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="replenishexecut_list_grid-table" style="width:100%;height:100%;"></table>
    <div id="replenishexecut_list_grid-pager"></div>
</div>
</body>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript" src="<%=path%>/static/js/techbloom/wms/instorage/replenish/replenish2.js"></script>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriData;
    var _grid;
    $(function(){
        _grid = jQuery("#replenishexecut_list_grid-table").jqGrid({
            url : context_path + '/replenishexecut/list.do',
            datatype : "json",
            colNames : [ '主键','补货任务执行编号','补货管理单号','开始时间','结束时间', '操作人','操作'],
            colModel : [
                {name : 'id',index : 'id',width : 20,hidden:true},
                {name : 'replenishexecutNo',index : 'replenishexecutNo',width : 40},
                {name : 'replenishNo',index : 'replenishNo',width : 40},
                {name : 'startTime',index : 'startTime',width : 40},
                {name : 'finishTime',index : 'finishTime',width : 40},
                {name : 'userName',index : 'userName',width : 40},
                {name: 'process_Id', index: 'process_Id', width: 50,
                    formatter:function(cellVal,option,rowObject){
                        return '<div style="margin-bottom:5px" class="btn btn-xs btn-success" onclick="replenishOperate(' + rowObject.id + ')">详情</div>';
                    }
                }
            ],
            rowNum : 20,
            rowList : [ 10, 20, 30 ],
            pager : '#replenishexecut_list_grid-pager',
            sortname : 'ID',
            sortorder : "desc",
            altRows: false,
            viewrecords : true,
            autowidth:true,
            multiselect:true,
            multiboxonly: true,
            beforeRequest:function (){
                dynamicGetColumns(dynamicDefalutValue,"replenishexecut_list_grid-table",$(window).width()-$("#sidebar").width() -7);
                //重新加载列属性
            },
            loadComplete : function(data){
                var table = this;
                setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
                oriData = data;
            },
            emptyrecords: "没有相关记录",
            loadtext: "加载中...",
            pgtext : "页码 {0} / {1}页",
            recordtext: "显示 {0} - {1}共{2}条数据"
        });
        //在分页工具栏中添加按钮
        jQuery("#replenishexecut_list_grid-table").navGrid("#replenishexecut_list_grid-pager",{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd('#replenishexecut_list_grid-pager',{
                caption:"",
                buttonicon:"ace-icon fa fa-refresh green",
                onClickButton: function(){
                    $("#replenishexecut_list_grid-table").jqGrid('setGridParam',
                        {
                            postData: {queryJsonString:""} //发送数据
                        }
                    ).trigger("reloadGrid");
                }
            }).navButtonAdd("#replenishexecut_list_grid-pager",{
                caption: "",
                buttonicon:"fa icon-cogs",
                onClickButton : function (){
                    jQuery("#replenishexecut_list_grid-table").jqGrid("columnChooser",{
                        done: function(perm, cols){
                            dynamicColumns(cols,dynamicDefalutValue);
                            $("#replenishexecut_list_grid-table").jqGrid("setGridWidth", $("#replenishexecut_list_grid-div").width() - 3);
                        }
                    });
                }
            });
        $(window).on("resize.jqGrid", function () {
            $("#replenishexecut_list_grid-table").jqGrid("setGridWidth", $("#replenishexecut_list_grid-div").width() - 3 );
            var height = $("#breadcrumb").outerHeight(true)+$(".query_box").outerHeight(true)+
                         $("#replenishexecut_list_fixed_tool_div").outerHeight(true)+
                         $("#gview_replenishexecut_list_grid-table .ui-jqgrid-hbox").outerHeight(true)+
                         $("#replenishexecut_list_grid-pager").outerHeight(true)+$("#header").outerHeight(true);
                         $("#replenishexecut_list_grid-table").jqGrid('setGridHeight', (document.documentElement.clientHeight)-height );
        })

        $(window).triggerHandler('resize.jqGrid');
    });
    var _queryForm_data = iTsai.form.serialize($('#replenishexecut_list_queryForm'));
    function queryOk(){
        var queryParam = iTsai.form.serialize($('#replenishexecut_list_queryForm'));
        //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
        queryInstoreListByParam(queryParam);

    }

    function reset(){
        iTsai.form.deserialize($('#replenishexecut_list_queryForm'),_queryForm_data);
        $("#replenishexecut_list_queryForm #replenishexecut_list_userId").select2("val","");
        $("#replenishexecut_list_queryForm #replenishexecut_list_materialid").select2("val","");
        //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
        queryInstoreListByParam(_queryForm_data);
    }
    var ssdd="";
    function replenishOperate(replenishID){
        console.log(replenishID);
        ssdd = replenishID;
        $.get( context_path + "/replenishexecut/toView.do?id="+ssdd).done(function(data){
            layer.open({
                title : "补货执行任务查看",
                type:1,
                skin : "layui-layer-molv",
                area : ['750px', '650px'],
                shade : 0.6, //遮罩透明度
                moveType : 1, //拖拽风格，0是默认，1是传统拖动
                anim : 2,
                content : data
            });
        }).error(function() {
            layer.closeAll();
            layer.msg('加载失败！',{icon:2});
        });
    }
    function reloadGrid(){
        _grid.trigger("reloadGrid");
    }
    $("#replenishexecut_list_queryForm #replenishexecut_list_userId").select2({
        placeholder: "请选择补货人",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/ASNmanage/getSelectUser",
            type: "POST",
            dataType: "json",
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });
    $('#replenishexecut_list_queryForm #replenishexecut_list_materialid').select2({
        placeholder : "请选择物料",//文本框的提示信息
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax : {
            url : context_path + '/salemanage/getMListByInId',
            dataType : 'json',
            delay : 250,
            data : function(term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                selectParam = term;
                return {
                    queryString : term, //联动查询的字符
                    pageSize : 15, //一次性加载的数据条数
                    pageNo : pageNo, //页码
                    time : new Date()
                }
            },
            results : function(data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results : res,
                        more : more
                    };
                } else {
                    return {
                        results : {
                            "id" : "0",
                            "text" : "没有更多结果"
                        }
                    };
                }

            },
            cache : true
        }
    });
    $("#replenishexecut_list_queryForm .mySelect2").select2();
    $(".date-picker").datetimepicker({format: "YYYY-MM-DD"});
</script>
</html>
