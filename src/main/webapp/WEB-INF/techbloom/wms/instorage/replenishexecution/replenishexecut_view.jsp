<%--
  Created by IntelliJ IDEA.
  User: HQKS-004-05
  Date: 2018/2/6
  Time: 14:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.lang.*"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
    String path = request.getContextPath();
%>
<div class="main-content" style="height:100%">
    <div class="widget-header widget-header-large" id="replenishexecut_view_div1">
        <!-- 隐藏的asn主键 -->
        <input type="hidden" id="replenishexecut_view_replenishId" name="id" value="${replenish.id }">
        <h3 class="widget-title grey lighter" style=' background: none; border-bottom: none; '>
            <i class="ace-icon fa fa-leaf green"></i>
            补货任务执行单据
        </h3>
        <div class="widget-toolbar no-border invoice-info">
            <span class="invoice-info-label">补货执行编号:</span>
            <span class="red">${replenish.replenishexecutNo }</span>
            <br />
            <span class="invoice-info-label">完成时间:</span>
            <span class="blue">${replenish.finishTime}</span>
        </div>
    </div>

    <div class="widget-body" id="replenishexecut_view_div2">
        <table style="width: 100%;font-size:16px;border-collapse:separate;border-spacing:2px 3px;">
            <tr class = "trClass">
                <td>
                    <i class="ace-icon fa fa-caret-right blue"></i>
                    补货任务执行编号：
                    <b class="black">${replenish.replenishexecutNo }</b>
                </td>
                <td>
                    <i class="ace-icon fa fa-caret-right blue"></i>
                    操作人:
                    <b class="black">${replenish.userName }</b>
                </td>
            </tr>
        </table>
    </div>
    <!-- 详情表格 -->
    <div id="replenishexecut_view_grid-div-c">
        <!-- 物料信息表格 -->
        <table id="replenishexecut_view_grid-table-c" style="width:100%;height:100%;"></table>
        <!-- 表格分页栏 -->
        <div id="replenishexecut_view_grid-pager-c"></div>
    </div>
</div><!-- /.main-content -->
<script type="text/javascript">
    var oriDataView;
    var _grid_view;        //表格对象
    _grid_view=jQuery("#replenishexecut_view_grid-table-c").jqGrid({
        url : context_path + '/replenishexecut/detailList?replenishId='+$("#replenishexecut_view_replenishId").val(),
        datatype : "json",
        colNames : [ '详情主键','物料编号','物料名称','条码','数量','货位','结束时间'],
        colModel : [
            {name : 'id',index : 'id',width : 20,hidden:true},
            {name : 'materialNo',index:'materialNo',width :20},
            {name : 'materialName',index:'materialName',width : 20},
            {name : 'boxBarCode', index: 'boxBarCode', width: 40 },
            {name : 'amount',index:'amount',width : 20},
            {name : 'location',index:'location',width : 20},
            {name : 'finishTime',index:'finishTime',width : 20}
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : '#replenishexecut_view_grid-pager-c',
        sortname : 'ID',
        sortorder : "asc",
        altRows: true,
        viewrecords : true,
        autowidth:true,
        multiboxonly: true,
        loadComplete : function(data){
            var table = this;
            setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            oriDataDetail = data;
            $(window).triggerHandler('resize.jqGrid');
        },
        cellEdit: true,
        cellsubmit : "clientArray",
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据",
    });

    $(window).on("resize.jqGrid", function () {
        $("#replenishexecut_view_grid-table-c").jqGrid("setGridWidth", $("#replenishexecut_view_grid-div-c").width() - 3 );
        var height =$(".layui-layer-title",_grid_view.parents(".layui-layer")).height()+
                    $("#replenishexecut_view_div1").outerHeight(true)+$("#replenishexecut_view_div2").outerHeight(true)+
                    $("#replenishexecut_view_grid-pager-c").outerHeight(true)+
                    $("#gview_replenishexecut_view_grid-table-c .ui-jqgrid-hbox").outerHeight(true);
                    $("#replenishexecut_view_grid-table-c").jqGrid('setGridHeight',_grid_view.parents(".layui-layer").height()-height);
    });
    $(window).triggerHandler('resize.jqGrid');

</script>