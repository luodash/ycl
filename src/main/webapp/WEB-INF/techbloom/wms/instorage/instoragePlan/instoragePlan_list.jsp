<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
	<script type="text/javascript">
	    var context_path = '<%=path%>';
	</script>
	<style type="text/css">
		.query_box .field-button.two {
		    padding: 0px;
		    left: 650px;
	    }
	</style>
</head>
<body style="overflow:hidden;">
   <div id="instoragePlan_list_grid-div">
   <!-- 隐藏区域：存放查询条件 -->
		<form id="instoragePlan_list_hiddenQueryForm"  action ="<%=path%>/instoragePlan/excel" style="display:none;">
			<!-- 入库计划主键 -->
			<input id="instoragePlan_list_ids" name="ids" value="" />
			<!-- 入库计划编号 -->
			<input id="instoragePlan_list_inPlanNo" name="inPlanNo" value="" >
			<!-- 收货单名称-->
			<input id="instoragePlan_list_inPlanName" name="inPlanName" value="" >
			<!-- 入库计划编号 -->
			<input id="instoragePlan_list_type" name="inPlanType" value="" />
			<!-- 收货单名称-->
			<input id="instoragePlan_list_time" name="inPlanTime" value="" > 
		</form>
		<div class="query_box" id="instoragePlan_list_yy" title="查询选项">
            <form id="instoragePlan_list_queryForm" style="max-width:100%;">
			 <ul class="form-elements">
				<li class="field-group field-fluid3">
					<label class="inline" for="instoragePlan_list_inPlanNo" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:92px;">入库计划编号：</span>
						<input type="text" id="instoragePlan_list_inPlanNo" name="inPlanNo" value="" style="width: calc(100% - 97px);" placeholder="入库计划编号">
					</label>			
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="instoragePlan_list_inPlanName" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:92px;">入库计划名称：</span>
						<input type="text" id="instoragePlan_list_inPlanName" name="inPlanName" value="" style="width: calc(100% - 97px);" placeholder="入库计划名称">
					</label>					
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="instoragePlan_list_inPlanType" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:92px;">入库计划类型：</span>
						<input type="text" id="instoragePlan_list_type" name="inPlanType" value="" style="width: calc(100% - 97px);" placeholder="入库计划类型">
					</label>					
				</li>
				<li class="field-group-top field-group field-fluid3">
					<label class="inline" for="instoragePlan_list_inPlanTime" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:92px;">入库计划时间：</span>
						<input type="text" class="form-control date-picker" id="instoragePlan_list_time" name="inPlanTime" style="width: calc(100% - 97px);" placeholder="入库计划时间" />
					</label>			
				</li>
				
			</ul>
			<div class="field-button" style="">
					<div class="btn btn-info" onclick="queryOk();">
				        <i class="ace-icon fa fa-check bigger-110"></i>查询
			        </div>
				    <div class="btn" onclick="reset();"><i class="ace-icon icon-remove"></i>重置</div>
				    <a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>
		        </div>
		  </form>		 
    </div>
        <div id="instoragePlan_list_fixed_tool_div" class="fixed_tool_div">
             <div id="instoragePlan_list___toolbar__" style="float:left;overflow:hidden;"></div>
        </div>
		<table id="instoragePlan_list_grid-table" style="width:100%;height:100%;"></table>
		<div id="instoragePlan_list_grid-pager"></div>
   </div>
</body>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript" src="<%=path%>/static/js/techbloom/wms/instorage/instoragePlan/inPlan.js"></script>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriData; 
    var _grid;
    var dynamicDefalutValue="6807c01d9f4b4731ab776e4c5436b61e";
    $(function  (){
       $(".toggle_tools").click();
    });
    $("#instoragePlan_list___toolbar__").iToolBar({
   	  	 id:"instoragePlan_list___tb__01",
   	  	 items:[
   	  	    {label: "添加",disabled:(${sessionUser.addQx}==1?false:true),onclick:addReceiptOrder,iconClass:'glyphicon glyphicon-plus'},
   	  	    {label: "编辑",disabled:(${sessionUser.editQx}==1?false:true),onclick:editAllocateOrder,iconClass:'glyphicon glyphicon-pencil'},
	   	  	{label: "删除",disabled:(${sessionUser.deleteQx}==1?false:true),onclick:delOutStorageOrder,iconClass:'glyphicon glyphicon-trash'},
	   	    {label: "查看", disabled:(${sessionUser.queryQx} == 1 ? false : true),onclick:viewDetailList, iconClass:'icon-zoom-in'},
	   	  	{label: "生成入库单",disabled:(${sessionUser.deleteQx}==1?false:true),onclick:inOrder,iconClass:'glyphicon glyphicon-pencil'},
	   	  	{label: "导出", disabled:(${sessionUser.queryQx} == 1 ? false : true),onclick:function () {
                var selectid = jQuery("#instoragePlan_list_grid-table").jqGrid("getGridParam", "selarrrow");
                $("#instoragePlan_list_ids").val(selectid);
	   	  	    $("#instoragePlan_list_hiddenQueryForm").submit();},iconClass:' icon-share'},
			{label: "打印", disabled:(${sessionUser.queryQx} == 1 ? false : true),onclick:function(){	
		  	  	var queryBean = iTsai.form.serialize($('#instoragePlan_list_hiddenQueryForm'));   //获取form中的值：json对象
	            var queryJsonString = JSON.stringify(queryBean); 
	            var url = context_path + "/instoragePlan/exportPrint?"+"ids=" + jQuery("#instoragePlan_list_grid-table").jqGrid('getGridParam', 'selarrrow');;
	            window.open(url);  
	  	  	},
	  	  	iconClass:' icon-print'}
	   	  	
	   	  ]
	});
    
   $(function(){
		_grid = jQuery("#instoragePlan_list_grid-table").jqGrid({
	       url : context_path + "/instoragePlan/list.do",
	       datatype : "json",
	       colNames : [ "主键","入库计划编号","入库计划名称","入库计划类型", "入库计划时间","备注","状态"],
	       colModel : [ 
	                    {name : "id",index : "id",width : 20,hidden:true}, 
	                    {name : "inPlanNo",index : "inPlanNo",width : 40}, 
	                    {name : "inPlanName",index : "inPlanName",width : 40}, 
	                    {name : "inPlanTypeName",index : "inPlanTypeName",width : 40}, 
	                    {name : "inPlanTime",index : "inPlanTime",width : 40,
                            formatter:function(cellValu,option,rowObject){
                                if (cellValu != null) {
                                    return getFormatDateByLong(new Date(cellValu), "yyyy-MM-dd HH:mm");
                                } else {
                                    return "";
                                }
                            }
						},
	                    {name : "remark",index : "REMARK",width : 40}, 
	                    {name : "state",index : "state",width : 40,
	                       formatter:function(cellValue,option,rowObject){
	                    		if(typeof cellValue == "number"){
	                    			if(cellValue==0){
	                    				return "<span style='color:#d15b47;font-weight:bold;'>未提交</span>";
	                    			}else if(cellValue==1){
	                    				return "<span style='color:#76b86b;font-weight:bold;'>已提交</span>";
	                    			}
	                    		}else{
	                    			return "异常";
	                    		}
	                    	}
	                    } 
	                  ],
	       rowNum : 20,
	       rowList : [ 10, 20, 30 ],
	       pager : "#instoragePlan_list_grid-pager",
	       sortname : "id",
	       sortorder : "desc",
	       altRows: false, 
	       viewrecords : true,
	       autowidth:true,
	       multiselect:true,
			multiboxonly: true,
            beforeRequest:function (){
                dynamicGetColumns(dynamicDefalutValue,"instoragePlan_list_grid-table",$(window).width()-$("#sidebar").width() -7);
                //重新加载列属性
            },
	       loadComplete : function(data) 
	       {
	           var table = this;
	           setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
	           oriData = data;
	       },
	       emptyrecords: "没有相关记录",
	       loadtext: "加载中...",
	       pgtext : "页码 {0} / {1}页",
	       recordtext: "显示 {0} - {1}共{2}条数据"
	  });
	 //在分页工具栏中添加按钮
	  jQuery("#instoragePlan_list_grid-table").navGrid("#instoragePlan_list_grid-pager",{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd('#instoragePlan_list_grid-pager',{  
		   caption:"",   
		   buttonicon:"ace-icon fa fa-refresh green",   
		   onClickButton: function(){
			   $("#instoragePlan_list_grid-table").jqGrid("setGridParam", {
							postData: {queryJsonString:""} //发送数据 
						}
				  ).trigger("reloadGrid");
		   }
		}).navButtonAdd("#instoragePlan_list_grid-pager",{
              caption: "",
              buttonicon:"fa icon-cogs",
              onClickButton : function (){
                  jQuery("#instoragePlan_list_grid-table").jqGrid("columnChooser",{
                      done: function(perm, cols){
                          dynamicColumns(cols,dynamicDefalutValue);
                          $("#instoragePlan_list_grid-table").jqGrid("setGridWidth", $("#instoragePlan_list_grid-div").width()-3);
                      }
                  });
              }
          });
	  $(window).on("resize.jqGrid", function () {
		  $("#instoragePlan_list_grid-table").jqGrid("setGridWidth", $("#instoragePlan_list_grid-div").width() - 3 );
	      var height = $("#breadcrumb").outerHeight(true)+$(".query_box").outerHeight(true)+
            $("#instoragePlan_list_fixed_tool_div").outerHeight(true)+
            $("#gview_instoragePlan_list_grid-table .ui-jqgrid-hbox").outerHeight(true)+
            $("#instoragePlan_list_grid-pager").outerHeight(true)+$("#header").outerHeight(true);
            $("#instoragePlan_list_grid-table").jqGrid("setGridHeight", (document.documentElement.clientHeight)-height );
	  })
	
	  $(window).triggerHandler("resize.jqGrid");
    }); 
   	var _queryForm_data = iTsai.form.serialize($("#instoragePlan_list_queryForm"));
	function queryOk(){
		var queryParam = iTsai.form.serialize($("#instoragePlan_list_queryForm"));
		//执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
		queryInstoreListByParam(queryParam);
	}
	
	function reset(){
		iTsai.form.deserialize($("#instoragePlan_list_queryForm"),_queryForm_data); 
		$("#instoragePlan_list_allocateTypeSelect").val("").trigger("change");
		$("#instoragePlan_list_inWarehouseSelect").val("").trigger("change");
		$("#instoragePlan_list_outWarehouseSelect").val("").trigger("change");
		$("#instoragePlan_list_queryForm #type").select2("val","");
		//执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
		queryInstoreListByParam(_queryForm_data);
		$("#instoragePlan_list_allocateTypeSelect").find("option[text='所有类型']").attr("selected",true);
		$("#instoragePlan_list_outWarehouseSelect").find("option[text='所有库区']").attr("selected",true);
		$("#instoragePlan_list_inWarehouseSelect").find("option[text='所有库区']").attr("selected",true);
	}
 $("#instoragePlan_list_queryForm .mySelect2").select2();
 $(".date-picker").datetimepicker({format: "YYYY-MM-DD"});
 function reloadGrid(){
   _grid.trigger("reloadGrid");
 }
 function inOrder(){
   var checkedNum = getGridCheckedNum("#instoragePlan_list_grid-table","id");
	if(checkedNum == 0)
	{
    	layer.alert("请选择一条入库计划！");
    	return false;
    }else if(checkedNum >1){
    	layer.alert("只能选择一条进行操作！");
    	return false;
    } else {
    	var inPlanId = jQuery("#instoragePlan_list_grid-table").jqGrid("getGridParam", "selrow"); 
    	$.ajax({
			type : "POST",
			url:context_path + "/instoragePlan/getInPlanById?inPlanId="+inPlanId,
			dataType : "json",
			cache : false,
			success : function(data) {
				if(data.state==1){
				$.get( context_path + "/instoragePlan/toInOrder.do?inPlanId="+inPlanId).done(function(data){
			    	  layer.open({
			    	  title : "入库单生成", 
			    	  type:1,
			    	  skin : "layui-layer-molv",
			    	  area : ["750px", "450px"],
			    	  shade : 0.6, //遮罩透明度
			    	  moveType : 1, //拖拽风格，0是默认，1是传统拖动
			    	  anim : 2,
			    	  content : data
			    	  });
			    	 });
			}else{
				layer.alert("入库计划未提交！");
			}
			}
		});
    		
    }
 }
 $("#instoragePlan_list_queryForm #instoragePlan_list_type").select2({
        placeholder: "选择入库类型",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/instoragePlan/getSelectInType",
            type: "POST",
            dataType: 'json',
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }

    });
</script>
</html>