<%@ page language="java" import="java.lang.*"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<div class="row-fluid" style="height: inherit;margin:0px">
	 <form action="" class="form-horizontal" id="baseInfor" name="baseInfor" method="post" target="_ifr" style="border-bottom: solid 2px #3b73af;">
	     <input type="hidden" id="inPlanId" name="id" value="${inPlan.id}">
  	     <input type="hidden" id="state" name="state" value="${inPlan.state}">
         <div class="row" style="margin:0;padding:0;">
            <%--入库计划编号--%>
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="inPlanNo" >入库计划编号：</label>
                <div class="controls">
                    <div class="input-append span12" >
                        <input type="text" id="inPlanNo" class="span10" name="inPlanNo" value="${inPlan.inPlanNo }"  placeholder="后台自动生成" readonly="readonly" />
                    </div>
                </div>
            </div>
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="inPlanName">入库计划名称：</label>
                <div class="controls">
                    <div class="input-append span12 required">
                        <input id="inPlanName" name="inPlanName" type="text" value="${inPlan.inPlanName }" placeholder="入库计划名称" class="span10" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin:0;padding:0;">
	             <div class="control-group span6" style="display: inline">
	                <label class="control-label" for="inPlanType" >入库计划类型：</label>
	                <div class="controls">
	                    <div class="span12 required" >
	                        <input type="text" id="inPlanType" class="span10" name="inPlanType" placeholder="入库计划类型"/>
	                    </div>
	                </div>
	            </div>
	            <%-- 单据选择：--%>
	            <div class="control-group span6" style="display: inline;visibility:hidden" id="code">
	                <label class="control-label" for="qualityId">单据选择：</label>
	                <div class="controls">
	                    <div class="span12 required" style=" float: none !important;">
	                        <input id="qualityId" name="qualityId" type="text" class="span10"" placeholder="单据选择" />
                            <input id="inPlanTypeId" name="inPlanTypeId" type="hidden" value="${inPlan.inPlanType}" />
				            <input id="inPlanTypeName" name="inPlanTypeName" type="hidden" value="${inPlan.inPlanTypeName}" />
	                    </div>
	                </div>
	            </div>
           </div>        
  	       <div class="row" style="margin:0;padding:0;">
             <div class="control-group span6" style="display: inline">
                <label class="control-label" for="strInPlanTime" >入库计划时间：</label>
                <div class="controls">
                    <div class="required span12">
                        <input class="form-control date-picker" type="text" id="strInPlanTime" name="strInPlanTime"  placeholder="入库计划时间" value='${inPlan.strInPlanTime}'/>
                    </div>
                </div>
            </div>
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="remark" >备注：</label>
                <div class="controls">
                    <div class="input-append span12" >
                        <input class="span10" type="text" id="remark" name="remark"
                               placeholder="备注" value="${inPlan.remark}">
                    </div>
                </div>
            </div>
         </div> 	     
	     <div style="margin-left:10px;">
            <span class="btn btn-info" id="formSave">
		       <i class="ace-icon fa fa-check bigger-110"></i>保存
            </span>
            <span class="btn btn-info" id="formSubmit"">
		        <i class="ace-icon fa fa-check bigger-110"></i>&nbsp;提交
            </span>
          </div>	
	</form>
	<div id="materialDiv" style="margin:10px;">
		<!-- 下拉框 -->
		<label class="inline" for="materialInfor">物料：</label>
		<input type="text" id = "materialInfor" name="materialInfor" style="width:350px;margin-right:10px;" />
		<button id="addMaterialBtn" class="btn btn-xs btn-primary" onclick="addDetail();">
			<i class="icon-plus" style="margin-right:6px;"></i>添加
		</button>
	</div>
	<!-- 表格div -->
	<div id="grid-div-c" style="width:100%">
		<!-- 	表格工具栏 -->
        <div id="fixed_tool_div" class="fixed_tool_div detailToolBar">
             <div id="__toolbar__-c" style="float:left;overflow:hidden;"></div>
        </div>
		<!-- 物料详情信息表格 -->
		<table id="grid-table-c" style="width:100%;height:100%;"></table>
		<!-- 表格分页栏 -->
		<div id="grid-pager-c" style="margin:0px;"></div>
	</div>
</div>
<script type="text/javascript">
var context_path = '<%=path%>';
var inPlanId=$("#inPlanId").val();
var oriDataDetail;
 var _grid_detail;        //表格对象
 var lastsel2;
 var type;   //0质检单1出库单
 $(".date-picker").datetimepicker({format: 'YYYY-MM-DD HH:mm:ss',useMinutes:true,useSeconds:true});
$("#baseInfor").validate({
   ignore: function(i,dom){
            var r_dom ="";
            var outstorageTypeSelect_id = $("#baseInfor #inPlanType").val();
            if(dom.id == "qualityId" && outstorageTypeSelect_id == "WMS_INSTORAGE_PLAN_QT"){
                r_dom = dom;
            }
            return r_dom;
        },
   rules:{
      "inPlanName":{
         required:true,
         remote:{
            cache:false,
            async:false,
            url: "<%=path%>/instoragePlan/isHaveName?inPlanId="+$('#inPlanId').val()
         }
      },
      
      "inPlanType":{
         required:true,
      },
      "inPlanTime":{
         required:true,
      },
      "qualityId":{
        required : true
      },
   },
   messages: {
       "inPlanName":{
         required:"请输入名称！",
         remote:"名称重复！"
      },
      "inPlanType":{
         required:"请选择入库类型！"
      },
      "inPlanTime":{
         required:"请选择入库计划时间！",
      },
      "qualityId":{
        required : "请选择单据"
      },
   },
   errorClass: "help-inline",
    errorElement: "span",
    highlight:function(element, errorClass, validClass) {
        $(element).parents('.control-group').addClass('error');
    },
    unhighlight: function(element, errorClass, validClass) {
        $(element).parents('.control-group').removeClass('error');
    }
});
$("#inPlanType").change(function(){
   $("#baseInfor #qualityId").select2("val","");
  if($("#inPlanType").val()=="WMS_INSTORAGE_PLAN_CG"||$("#inPlanType").val()=="WMS_INSTORAGE_PLAN_FX"||$("#inPlanType").val()=="WMS_INSTORAGE_PLAN_HH"){
   //$("#qualityType").css("visibility","visible");
    $("#code").removeAttr("style","visibility:hidden");
  }else{
  //$("#qualityType").css("visibility","hidden");
  $("#code").attr("style","visibility:hidden");
  $("#baseInfor #qualityId").select2("val","");
  }
});


$("#formSubmit").click(function(){
  if($("#inPlanId").val()==""){
    layer.msg("请先保存表头信息！",{icon:2,time:1200});
    return;
  }
  layer.confirm("提交后的数据将不能修改，确认提交吗？",function(){
   $.ajax({
   url:context_path+"/instoragePlan/submit?inPlanId="+$("#inPlanId").val(),
   type:"post",
   dataType:"JSON",
   success:function (data){
      if(data.result){
      gridReload();
      layer.closeAll();
      layer.msg(data.msg,{icon:1,time:1200});
      }else{
      layer.alert(data.msg);
      }
	 }
	 });
   });
});
$("#formSave").click(function(){

  var bean = $("#baseInfor").serialize();
if($('#baseInfor').valid()){
    /* if($('#inPlanType').val()=="WMS_INSTORAGE_PLAN_CG"&&$("#qualityId").val()==""){
        layer.msg("采购入库必须选择质检单",{icon:2,time:1200});
        return ;
    } */
   saveOrUpdate(bean);
} 
});

/* 保存、修改 */
function saveOrUpdate(bean){
//$("#qualityId").select2("val","");
   $.ajax({
   url:context_path+"/instoragePlan/save",
   type:"post",
   data:bean,
   dataType:"JSON",
   success:function (data){
      if(data.result){
      $("#inPlanId").val(data.inPlanId);
      $("#baseInfor #inPlanNo").val(data.inPlanNo);
      $("#baseInfor #state").val(data.state);
      inPlanId=data.inPlanId;
      gridReload();
       $("#grid-table-c").jqGrid('setGridParam', 
           	{
              url : context_path + '/instoragePlan/detailList?inPlanId='+inPlanId,
              postData: {queryJsonString:""} //发送数据  :选中的节点
             } ).trigger("reloadGrid");
      layer.msg("操作成功！",{icon:1,time:1200});
      }else{
      layer.msg("操作失败！",{icon:2,time:1200});
      }
   }
   });

}
//单元格编辑成功后，回调的函数
	var editFunction = function eidtSuccess(XHR){
		 var data = eval("("+XHR.responseText+")"); 
		if(data["msg"]!=""){
			layer.alert(data["msg"]);
		}
		jQuery("#grid-table-c").jqGrid('setGridParam', 
				{
					postData: {
						id:$('#id').val(),
						queryJsonString:""
					} 
				}
		  ).trigger("reloadGrid");
	};
	
  
  	_grid_detail=jQuery("#grid-table-c").jqGrid({
         url : context_path + "/instoragePlan/detailList?inPlanId="+inPlanId,
         datatype : "json",
         colNames : [ "详情主键","物料编号","物料名称","批次号","数量",],
         colModel : [ 
  					  {name : "id",index : "id",width : 20,hidden:true}, 
  					  {name : "materialNo",index:"materialNo",width :20}, 
                      {name : "materialName",index:"materialName",width : 20}, 
                      {name : "batchNo",index:"batchNo",width : 100},
                      {name : "amount", index:"amount", width: 60,editable : true,editrules: {custom: true, custom_func: numberRegex},
                        editoptions: {
	                          size: 25,
	                          dataEvents: [
	                              {
	                                  type: 'blur',     //blur,focus,change.............
	                                  fn: function (e) {
	                                	  var $element = e.currentTarget;
	                                	  var $elementId = $element.id;
	                                	  var rowid = $elementId.split("_")[0];
	                                	  var id=$element.parentElement.parentElement.children[1].textContent;
	                                	  var indocType = 1;
	                                	 // var rowData = $("#grid-table-c").jqGrid('getRowData',rowid).id;
	                                		  var reg = new RegExp("^([0-9]*|[0]{1,1})$");
	                                		  if (!reg.test($("#"+$elementId).val())) {
	                                			  layer.alert("数量输入错误！(注:只可以输入正整数和0)");
	                                			  return;
	                                		  }
	                                	  $.ajax({
	                                		  url:context_path + '/instoragePlan/updateAmount',
	                                		  type:"POST",
	                                		  data:{id:id,amount:$("#"+rowid+"_amount").val()},
	                                		  dataType:"json",
	                                		  success:function(data){
	                                		        if(!data.result){
	                                		           layer.alert(data.msg);
	                                		        }
	                                		        $("#grid-table-c").jqGrid('setGridParam', 
                                							{
                                					    		url : context_path + '/instoragePlan/detailList?inPlanId='+inPlanId,
                                								postData: {queryJsonString:""} //发送数据  :选中的节点
                                							}
                                					  ).trigger("reloadGrid");
	                                		      
	                                		  }
	                                	  }); 
	                                  }
	                              }
	                          ]
	                      }
                      },
                    ],
         rowNum : 20,
         rowList : [ 10, 20, 30 ],
         pager : '#grid-pager-c',
         sortname : 'ID',
         sortorder : "asc",
         altRows: true,
         viewrecords : true,
         autowidth:true,
         multiselect:true,
		 multiboxonly: true,
         loadComplete : function(data) 
         {
             var table = this;
             setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
             oriDataDetail = data;
             $(window).triggerHandler('resize.jqGrid');
         },
	   cellEdit: true,
	   cellsubmit : "clientArray",
  	   emptyrecords: "没有相关记录",
  	   loadtext: "加载中...",
  	   pgtext : "页码 {0} / {1}页",
  	   recordtext: "显示 {0} - {1}共{2}条数据",
    });
    //在分页工具栏中添加按钮
    $("#grid-table-c").navGrid('#grid-pager-c',{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd('#grid-pager-c',{  
  	   caption:"",   
  	   buttonicon:"ace-icon fa fa-refresh green",   
  	   onClickButton: function(){   
  	    $("#grid-table-c").jqGrid('setGridParam', 
				{
  	    			url:context_path + '/instoragePlan/detailList?inPlanId='+inPlanId,
					postData: {queryJsonString:""} //发送数据  :选中的节点
				}
		  ).trigger("reloadGrid");
  	   }
  	});
  	
  	$(window).on('resize.jqGrid', function () {
  		$("#grid-table-c").jqGrid( 'setGridWidth', $("#grid-div-c").width());
  		var height=$(".layui-layer-title",_grid_detail.parents(".layui-layer") ).height()+
        $("#baseInfor").outerHeight(true)+$("#materialDiv").outerHeight(true)+
        $("#grid-pager-c").height()+$("#fixed_tool_div.fixed_tool_div.detailToolBar").height()+$("#gview_grid-table-c .ui-jqgrid-hbox").height();
        $("#grid-table-c").jqGrid('setGridWidth', $("#grid-div-c").width());
        console.log(_grid_detail.parents(".layui-layer").height());
  		$("#grid-table-c").jqGrid( 'setGridHeight', (_grid_detail.parents(".layui-layer").height() -height-8) );
  	});
  	$(window).triggerHandler('resize.jqGrid');
  	if($("#id").val()!=""){
  		//reloadDetailTableList();   //重新加载详情列表
  	}
function reloadDetailTableList(){   //重新加载详情列表
      $("#grid-table-c").jqGrid('setGridParam', 
				{
  	    			url:context_path + '/instoragePlan/detailList?inPlanId='+inPlanId,
					postData: {queryJsonString:""} //发送数据  :选中的节点
				}
		  ).trigger("reloadGrid");
  }
//将数据格式化成两位小数：四舍五入
 function formatterNumToFixed(value,options,rowObj){
	if(value!=null){
		var floatNum = parseFloat(value);
		return floatNum.toFixed(2);
	}else{
		return "0.00";
	}
 } 
 //数量输入验证
function numberRegex(value, colname) {
    var regex = /^\d+\.?\d{0,2}$/;
    reloadDetailTableList();
    if (!regex.test(value)) {
        return [false, ""];
    }
    else  return [true, ""];
}


     $('#materialInfor').select2({
			placeholder : "请选择物料",//文本框的提示信息
			minimumInputLength : 0, //至少输入n个字符，才去加载数据
			allowClear : true, //是否允许用户清除文本信息
			multiple: true,
			closeOnSelect:false,
			ajax : {
				url : context_path + '/instoragePlan/getMaterialList',
				dataType : 'json',
				delay : 250,
				data : function(term, pageNo) { //在查询时向服务器端传输的数据
					term = $.trim(term);
					selectParam = term;
					return {
						/* docId : $("#baseInfor #id").val(), */
						queryString : term, //联动查询的字符
						pageSize : 15, //一次性加载的数据条数
						pageNo : pageNo, //页码
						time : new Date(),
						inPlanId:$("#inPlanId").val()
						//qualityId:$("#qualityId").val()
					//测试
					}
				},
				results : function(data, pageNo) {
					var res = data.result;
					if (res.length > 0) { //如果没有查询到数据，将会返回空串
						var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
						return {
							results : res,
							more : more
						};
					} else {
						return {
							results : {
								"id" : "0",
								"text" : "没有更多结果"
							}
						};
					}

				},
				cache : true
			}
		});

		$('#materialInfor').on("change",function(e){
			var datas=$("#materialInfor").select2("val");
			selectData = datas;
			var selectSize = datas.length;
			if(selectSize>1){
				var $tags = $("#s2id_materialInfor .select2-choices");   //
				//$("#s2id_materialInfor").html(selectSize+"个被选中");
				var $choicelist = $tags.find(".select2-search-choice");
				var $clonedChoice = $choicelist[0];
				$tags.children(".select2-search-choice").remove();
				$tags.prepend($clonedChoice);
				
				$tags.find(".select2-search-choice").find("div").html(selectSize+"个被选中");
				$tags.find(".select2-search-choice").find("a").removeAttr("tabindex");
				$tags.find(".select2-search-choice").find("a").attr("href","#");
				$tags.find(".select2-search-choice").find("a").attr("onclick","removeChoice();");
			}
			//执行select的查询方法
			$("#materialInfor").select2("search",selectParam);
		});
		
		$("#inPlanType").select2({
        placeholder: "选择入库类型",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/instoragePlan/getSelectInType",
            type: "POST",
            dataType: 'json',
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }

    });
    
    $("#supplyId").select2({
        placeholder: "选择供应商",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/ASNmanage/getSelectSupply",
            type: "POST",
            dataType: 'json',
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }

    });
    
    
    
if($("#inPlanId").val()!=""){

 $("#inPlanType").select2("data", {
   id: $("#inPlanTypeId").val(),
   text: $("#inPlanTypeName").val()
  });
 if($("#inPlanType").val()=="WMS_INSTORAGE_PLAN_CG"){
 //$("#qualityType").css("visibility","visible");
 $("#code").attr("style","visibility:hidden");
 $.ajax({
	    type:"POST",
		url:context_path + '/instoragePlan/getQuerlityInfo?inPlanId='+$("#inPlanId").val(),
	    dataType:"json",
	    success:function(data){
	       if (data) {
                    if (data.info) {
                        var obj = [];
                        for (let i = 0; i < data.info.length; i++) {
                            obj.push({
                                id: data.info[i].id,
                                text: data.info[i].text
                            });
                        }
                        $("#qualityId").select2("data", obj);
                    }
                }
	    }
	  });
 }
}else{
  $('#customer').val("")
}
 //清空物料多选框中的值
 function removeChoice(){
	  $("#s2id_materialInfor .select2-choices").children(".select2-search-choice").remove();
	  $("#materialInfor").select2("val","");
	  selectData = 0;
	  
 }

//添加物料详情
 function addDetail(){
	  if($("#inPlanId").val()==""){
		  layer.alert("请先保存表单信息！");
		  return;
	  }
	  if(selectData!=0){
		  //将选中的物料添加到数据库中
		  $.ajax({
			  type:"POST",
			  url:context_path + '/instoragePlan/saveDetail',
			  data:{inPlanId:$('#baseInfor #inPlanId').val(),materialIds:selectData.toString(),type:$("#inPlanType").val()},
			  dataType:"json",
			  success:function(data){
				  removeChoice();   //清空下拉框中的值
				  if(Boolean(data.result)){
						layer.msg("添加成功",{icon:1,time:1200});
					  //重新加载详情表格
					  $("#grid-table-c").jqGrid('setGridParam', 
							{
								postData: {inPlanId:$("#baseInfor #inPlanId").val()} //发送数据  :选中的节点
							}
					  ).trigger("reloadGrid");
				  }else{
					  layer.msg(data.msg,{icon:2,time:1200});
				  }
			  }
		  });
	  }else{
		  layer.alert("请选择物料！");
	  }
  }
  //工具栏
	  $("#__toolbar__-c").iToolBar({
	   	 id:"__tb__01",
	   	 items:[
	   	  	{label:"删除", onclick:delDetail},
	    ]
	  });
	//删除物料详情
	function delDetail(){
	   var ids = jQuery("#grid-table-c").jqGrid('getGridParam', 'selarrrow');
	   $.ajax({
	      url:context_path + '/instoragePlan/deleteDetail?ids='+ids,
	      type:"POST",
	      dataType:"JSON",
	      success:function(data){
	         if(data.result){
	           layer.msg("操作成功！");
	            //重新加载详情表格
					  $("#grid-table-c").jqGrid('setGridParam', 
							{
								postData: {inPlanId:$("#baseInfor #inPlanId").val()} //发送数据  :选中的节点
							}
					  ).trigger("reloadGrid");
	         }
	      }
	   });
	}	
	$("#qualityId").select2({
        placeholder: "请选择质检单",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        multiple: true,
        ajax: {
            url : context_path + '/instoragePlan/getQualityOrder',
            type: "POST",
            dataType: 'json',
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    inPlanType:$("#baseInfor #inPlanType").val(),
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date(),
                   // stuffId: shelveId
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });	
</script>
