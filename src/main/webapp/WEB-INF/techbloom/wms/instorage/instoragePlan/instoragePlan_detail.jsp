﻿<%@ page language="java" import="java.lang.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<div class="main-content" style="height:100%">
	<div class="widget-header widget-header-large" id="instoragePlan_detail_div1">
		<input type="hidden" id="instoragePlan_detail_id" name="id" value="${PLAN.id }">
		<h3 class="widget-title grey lighter"
			style=' background: none; border-bottom: none; '>
			<i class="ace-icon fa fa-leaf green"></i> 入库计划
		</h3>
		<div class="widget-toolbar no-border invoice-info">
			<span class="invoice-info-label">入库计划编号:</span> <span class="red">${PLAN.inPlanNo }</span><br />
			<span class="invoice-info-label">计划时间:</span> <span class="blue">${PLAN.inPlanTime}</span>
		</div>
		<div class="widget-toolbar hidden-480">
			<a href="#" onclick="printDoc();"> <i
				class="ace-icon fa fa-print"></i>
			</a>
		</div>
	</div>
	<div class="widget-body" id="instoragePlan_detail_div2">
		<table style="width: 100%;font-size:16px;border-collapse:separate;border-spacing:2px 3px;">
			<tr>
				<td><i class="ace-icon fa fa-caret-right blue"></i> 入库计划名称： <b
					class="black">${PLAN.inPlanName }</b></td>
				<td><i class="ace-icon fa fa-caret-right blue"></i> 入库计划类型: <b
					class="black">${PLAN.inPlanTypeName }</b></td>
			</tr>
			<tr>
			<tr>
				<td><i class="ace-icon fa fa-caret-right blue"></i> 备注: <b
					class="black">${PLAN.remark }</b></td>
				<td><i class="ace-icon fa fa-caret-right blue"></i> 状态 : <c:if
						test="${PLAN.state==0 }">
						<span class="red">未提交</span>
					</c:if> <c:if test="${PLAN.state==1 }">
						<span class="green">已提交</span>
					</c:if></td>
			</tr>
			</tr>
		</table>
	</div>
	<div id="instoragePlan_detail_grid-div-c">
		<table id="instoragePlan_detail_grid-table-c" style="width:100%;height:100%;"></table>
		<div id="instoragePlan_detail_grid-pager-c"></div>
	</div>
</div>
<script type="text/javascript">
var oriDataView;
var _grid_view;        //表格对象
 _grid_view=jQuery("#instoragePlan_detail_grid-table-c").jqGrid({
         url : context_path + "/instoragePlan/detailList?inPlanId="+$("#instoragePlan_detail_id").val(),
        datatype : "json",
        colNames : [ "详情主键","物料编号","物料名称","批次号","数量",],
        colModel : [ 
 					 {name : "id",index : "id",width : 20,hidden:true}, 
 					 {name : "materialNo",index:"materialNo",width :30}, 
                     {name : "materialName",index:"materialName",width : 30},
                     {name : "batchNo",index:"batchNo",width : 100}, 
                     {name : "amount", index: "amount", width: 20}
                   ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : "#instoragePlan_detail_grid-pager-c",
        sortname : "id",
        sortorder : "desc",
        altRows: true,
        viewrecords : true,
        autowidth:true,
		multiboxonly: true,
        loadComplete : function(data) {
             var table = this;
             setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
             oriDataDetail = data;
             $(window).triggerHandler('resize.jqGrid');
        },
	   cellEdit: true,
	   cellsubmit : "clientArray",
  	   emptyrecords: "没有相关记录",
  	   loadtext: "加载中...",
  	   pgtext : "页码 {0} / {1}页",
  	   recordtext: "显示 {0} - {1}共{2}条数据",
    });
    
    $(window).on("resize.jqGrid", function () {
  		$("#instoragePlan_detail_grid-table-c").jqGrid("setGridWidth", $("#instoragePlan_detail_grid-div-c").width() - 3 );
  		var height = $(".layui-layer-title",_grid_view.parents(".layui-layer")).height()+
  		             $("#instoragePlan_detail_div1").outerHeight(true)+$("#instoragePlan_detail_div2").outerHeight(true)+
  		             $("#instoragePlan_detail_grid-pager-c").outerHeight(true)+
  		             $("#gview_instoragePlan_detail_grid-table-c .ui-jqgrid-hbox").outerHeight(true);
                     $("#instoragePlan_detail_grid-table-c").jqGrid("setGridHeight",_grid_view.parents(".layui-layer").height()-height);
  	});
  	$(window).triggerHandler("resize.jqGrid");
    function printDoc(){
	var url = context_path + "/getbills/printGetbillsDetail?putId="+$("#instoragePlan_detail_id").val();
	window.open(url);
}	
</script>