<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
	<script type="text/javascript">
	    var context_path = '<%=path%>';
	</script>
	<style type="text/css">
	
	.query_box .field-button.two {
    padding: 0px;
    left: 650px;
    }
	</style>
</head>
<div class="panel_tab">
    <div class="row-fluid">
        <!-- 表格数据 -->
        <table id="in_grid-table" style="width:100%;"></table>
        <!-- 表格底部 -->
        <div id="in_grid-pager"></div>
    </div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriData;
    var _grid;

    _grid = jQuery("#in_grid-table").jqGrid({
        url : context_path + "/instoragePlan/list.do",
        datatype : "json",
        colNames : [ "入库计划编号","入库计划类型", "入库计划时间","状态"],
        colModel : [
            {name : "inPlanNo",index : "inPlanNo",width : 40},
            {name : "inPlanTypeName",index : "inPlanTypeName",width : 40},
            {name : "inPlanTime",index : "inPlanTime",width : 40},
            {name : "state",index : "state",width : 40,
                formatter:function(cellValue,option,rowObject){
                    if(typeof cellValue == "number"){
                        if(cellValue==0){
                            return "<span style='color:#d15b47;font-weight:bold;'>未提交</span>";
                        }else if(cellValue==1){
                            return "<span style='color:#76b86b;font-weight:bold;'>已提交</span>";
                        }
                    }else{
                        return "异常";
                    }
                }
            }
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : "#in_grid-pager",
        sortname : "id",
        sortorder : "desc",
        altRows: false,
        viewrecords : true,
        autowidth:true,
        multiselect:false,
        multiboxonly: false,
        loadComplete : function(data)
        {
            var table = this;
            setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            oriData = data;
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
    });
	 //在分页工具栏中添加按钮
	  $(window).on("resize.jqGrid", function () {
		  $("#in_grid-table").jqGrid("setGridWidth",$("#in_grid-table").parents(".itemContent").width()-2);
          $("#in_grid-table").jqGrid( "setGridHeight", 380);
	  })
	  $(window).triggerHandler("resize.jqGrid");
</script>
</html>