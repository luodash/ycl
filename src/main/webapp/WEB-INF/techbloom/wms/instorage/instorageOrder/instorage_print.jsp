<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="com.tbl.modules.wms.entity.instorage.*" %>

<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

    List<InstorageOrderDetail> typeList =  (List<InstorageOrderDetail>)request.getAttribute("instorageList");
    //String userName = (String)request.getAttribute("userName");
    String enterpriseName = "";
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    String date = sdf.format(new Date());
    int startIndex =0;
    int pageSize = 1000;
    //判断list能打印几个单子
    int num = typeList.size()/pageSize;
    if(typeList.size()%pageSize != 0){
        num = num + 1;
    }
%>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=basePath%>">
    <title>入库单</title>
    <script type="text/javascript">
        function printA4(){
            window.print();
        }
    </script>
</head>
<body onload="printA4();">
<%
    double allAmount = 0;
    int index = 1;
    for(int a = 0; a < num; a++){
        //最大条数为
        int size = startIndex + pageSize;
        if(a == num -1){
            size = typeList.size();
        }
%>
<div style="height: 295mm;width: 210mm;border:0px solid #000;">
    <h3 align="center" style="font-weight: bolder;">物料详情</h3>
    <div style="height:30px;width:400px;margin:0 auto;font-size: 14px;">
    </div><br/>
    <table style="border-collapse: collapse; border: none; width: 21cm;font-size: 12px;" align="center">
        <tr style="height: 32px">
        	<th style="border: solid #000 1px;text-align:center;font-weight:bold;">序号</th>
        	<th style="border: solid #000 1px;text-align:center;font-weight:bold;">物料编号</th>
            <th style="border: solid #000 1px;text-align:center;font-weight:bold;">物料名称</th>
            <th style="border: solid #000 1px;text-align:left;font-weight:bold;">数量</th>
        </tr>
        <%
            if(typeList != null && typeList.size() > 0){
                for(int i= 0; i < size; i++){
                	InstorageOrderDetail sale  = (InstorageOrderDetail)typeList.get(i);
        %>
        <tr style="height: 22px">
            <td style="border: solid #000 1px;text-align:center;">
                <%=i+1 %>
            </td>
            <td style="border: solid #000 1px;text-align:left;">
                <%=sale.getMaterialNo() %>
            </td>
            <td style="border: solid #000 1px;text-align:left">
                <%=sale.getMaterialName() %>
            </td>
            <td style="border: solid #000 1px;text-align:center;">
                <%=sale.getAmount() %>
            </td>
        </tr>
        <%
                }
            }
        %>
    </table>
    <br/>
    <table style="border-collapse: collapse; border: none; width: 21cm;font-size: 14px;" align="center">
        <%-- <tr>
            <td style="text-align:center;">单位：<%=enterpriseName %></td>
            <td style="text-align:center;">制表：<%=userName %></td>
            <td style="text-align:center;">打印日期：<%=date %></td>
        </tr> --%>
    </table>
</div>
<div style="font-size: 14px;margin-bottom:0px;text-align: center;">第<%=index++ %>页</div>
<%
        startIndex += pageSize;
    }
%>
</body>
</html>

