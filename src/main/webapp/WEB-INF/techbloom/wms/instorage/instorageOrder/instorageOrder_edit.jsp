<%@ page language="java" import="java.lang.*"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<div class="row-fluid" style="height: inherit;margin:0px;border: 0px">
	<form id="baseInfor" class="form-horizontal" id="baseInfor"  method="post" target="_ifr">
		<!-- 隐藏的入库单主键 -->
  	        <input type="hidden" id="inId" name="id" value="${order.id}">
  	        <input type="hidden" id="state" name="state" value="${order.state}"> 	       
  	        <div class="row" style="margin:0;padding:0;">
	             <div class="control-group span6" style="display: inline">
	                <label class="control-label" for="instorageNo" > 入库单编号：</label>
	                <div class="controls">
	                    <div class="input-append span12" >
	                        <input type="text" id="instorageNo" class="span10" name="instorageNo" placeholder="后台自动生成" value='${order.instorageNo }' readonly="readonly"/>
	                    </div>
	                </div>
	            </div>
	            <%--库区--%>
	            <div class="control-group span6" style="display: inline">
	                <label class="control-label" for="instorageHouseId" >库区：</label>
	                <div class="controls">
	                    <div class="required span12" style=" float: none !important;">
	                        <input class="span10" type="text" id="instorageHouseId" name="instorageHouseId" placeholder="库区">	                       
	                    </div>
	                </div>
	            </div>
           </div>  	       
  	       <div class="row" style="margin:0;padding:0;">
	             <div class="control-group span6" style="display: inline">
	                <label class="control-label" for="inPlanId" >入库计划：</label>
	                <div class="controls">
	                    <div class="span12 required" >
	                        <input type="text" id="inPlanId" class="span10" name="inPlanId" placeholder="入库计划"/>
	                    </div>
	                </div>
	            </div>
	            <%--入库人--%>
	            <div class="control-group span6" style="display: inline">
	                <label class="control-label" for="userId" >入库人：</label>
	                <div class="controls">
	                    <div class="span12" style=" float: none !important;">
	                        <input class="span10" type="text" id="userId" name="userId"  placeholder="入库人">
	                        <input type="hidden" name="user" id="user" value="${order.userId }">
					        <input type="hidden" name="userName" id="userName" value="${order.userName }">
	                    </div>
	                </div>
	            </div>
           </div>   	       
  	       <div class="row" style="margin:0;padding:0;">
            <%--入库时间--%>
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="strInstorageTime" >入库时间：</label>
                <div class="controls">
                    <div class="required span12" style=" float: none !important;">
                        <input class="form-control date-picker" type="text" id="strInstorageTime" name="strInstorageTime"  placeholder="入库时间" value="${order.strInstorageTime }">
                    </div>
                </div>
            </div>
           </div>   	       
  	       <div  class="row" style="margin:0;padding:0;">
            <%--备注--%>
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="remark" >备注：</label>
                <div class="controls">
                    <div class="input-append   span12" >
                        <input class="span10" type="text" id="remark" name="remark" placeholder="备注" value="${order.remark}">
                    </div>
                </div>
            </div>
          </div> 
  	       <div style="margin-left:10px;">
            <span class="btn btn-info" id="formSave">
		       <i class="ace-icon fa fa-check bigger-110"></i>保存
            </span>
            <span class="btn btn-info" id="formSubmit"">
		        <i class="ace-icon fa fa-check bigger-110"></i>&nbsp;提交
            </span>
           </div> 	        
	</form>
	<div id="materialDiv" style="margin:10px;">
		<!-- 下拉框 -->
		<label class="inline" for="materialInfor">物料：</label>
		<input type="text" id = "materialInfor" name="materialInfor" style="width:350px;margin-right:10px;" />
		<button id="addMaterialBtn" class="btn btn-xs btn-primary" onclick="addDetail();">
			<i class="icon-plus" style="margin-right:6px;"></i>添加
		</button>
	</div>
	<!-- 表格div -->
	<div id="grid-div-c" style="width:100%;">
		<!-- 	表格工具栏 -->
        <div id="fixed_tool_div" class="fixed_tool_div detailToolBar">
             <div id="__toolbar__-c" style="float:left;overflow:hidden;"></div>
        </div>
		<!-- 货物详情信息表格 -->
		<table id="grid-table-c" style="width:100%;height:100%;"></table>
		<!-- 表格分页栏 -->
		<div id="grid-pager-c"></div>
	</div>
</div>
<script type="text/javascript">
var context_path = '<%=path%>';
var inId=$("#inId").val();
var oriDataDetail;
var _grid_detail;        //表格对象
var lastsel2;
$(".date-picker").datetimepicker({format: 'YYYY-MM-DD HH:mm:ss',useMinutes:true,useSeconds:true});
$("#baseInfor").validate({
   ignore: "", 
   rules:{
      
      "inPlanId":{
         required:true,
      },
      "instorageHouseId":{
         required:true,
      },
      "instorageTime":{
         required:true,
      },
      
   },
   messages: {
    "inPlanId":{
         required:"请选择入库计划",
      },
    "instorageHouseId":{
         required:"请选择库区",
      },
     "instorageTime":{
         required:"请选择日期！"
      },
   },
  errorClass: "help-inline",
    errorElement: "span",
    highlight:function(element, errorClass, validClass) {
        $(element).parents('.control-group').addClass('error');
    },
    unhighlight: function(element, errorClass, validClass) {
        $(element).parents('.control-group').removeClass('error');
    }
});
$("#formSubmit").click(function(){
  if($("#inId").val()==""){
    layer.msg("请先保存表头信息！",{icon:2,time:1200});
    return;
  }
   $.ajax({
   url:context_path+"/instorageOrder/submit?inId="+$("#inId").val(),
   type:"post",
   dataType:"JSON",
   success:function (data){
      if(data.result){
      layer.closeAll();
      layer.msg(data.msg,{icon:1,time:1200});
      gridReload();
      }else{
      layer.msg(data.msg,{icon:2,time:1200});
      }
   }
   });
});

$("#formSave").click(function(){
 var bean = $("#baseInfor").serialize();
if($('#baseInfor').valid()){
   saveOrUpdate(bean);
}
});

/* 保存、修改 */
function saveOrUpdate(bean){
   $.ajax({
   url:context_path+"/instorageOrder/save",
   type:"post",
   data:bean,
   dataType:"JSON",
   success:function (data){
      if(data.result){
      $('#baseInfor #inId').val(data.orderId);
      $("#baseInfor #instorageNo").val(data.orderNo);
      $("#baseInfor #state").val(data.state);
      inId=data.orderId;
      gridReload();
      layer.msg("操作成功！",{icon:1,time:1200});
      }else{
      layer.msg("操作失败！",{icon:2,time:1200});
      }
   }
   });

}
//单元格编辑成功后，回调的函数
	var editFunction = function eidtSuccess(XHR){
		 var data = eval("("+XHR.responseText+")"); 
		if(data["msg"]!=""){
			layer.alert(data["msg"]);
		}
		jQuery("#grid-table-c").jqGrid('setGridParam', 
				{
					postData: {
						id:$('#id').val(),
						queryJsonString:""
					} 
				}
		  ).trigger("reloadGrid");
	};	  
  	_grid_detail=jQuery("#grid-table-c").jqGrid({
         url : context_path + '/instorageOrder/detailList',
         datatype : "json",
         data:{inId:$('#baseInfor #inId').val()},
         colNames : [ '详情主键','货物编号','货物名称','批次号','入库数量','库存数量'],
         colModel : [ 
  					  {name : 'id',index : 'id',width : 20,hidden:true}, 
  					  {name : 'materialNo',index:'materialNo',width :20}, 
                      {name : 'materialName',index:'materialName',width : 20}, 
                      {name : 'materialCode',index:'materialCode',width : 20}, 
                      {name: 'amount', index: 'amount', width: 20,editable : true,editrules: {custom: true, custom_func: numberRegex},
                        editoptions: {
	                          size: 25,
	                          dataEvents: [
	                              {
	                                  type: 'blur',     //blur,focus,change.............
	                                  fn: function (e) {
	                                	  var $element = e.currentTarget;
	                                	  var $elementId = $element.id;
	                                	  var rowid = $elementId.split("_")[0];
	                                	  var id=$element.parentElement.parentElement.children[1].textContent;
	                                	  var indocType = 1;
	                                	 // var rowData = $("#grid-table-c").jqGrid('getRowData',rowid).id;
	                                		  var reg = new RegExp("^[0-9]+(.[0-9]{1,2})?$");
	                                		  if (!reg.test($("#"+$elementId).val())) {
	                                			  layer.alert("非法的数量！(注：可以有两位小数的正实数)");
	                                			  return;
	                                		  }
	                                	  $.ajax({
	                                		  url:context_path + '/instorageOrder/updateAmount',
	                                		  type:"POST",
	                                		  data:{id:id,amount:$("#"+rowid+"_amount").val()},
	                                		  dataType:"json",
	                                		  success:function(data){
	                                		        if(!data.result){
	                                		           layer.alert(data.msg);
	                                		        }
	                                		       reloadDetailTableList();  
	                                		      
	                                		  }
	                                	  }); 
	                                  }
	                              }
	                          ]
	                      }
                      },
                      {name : 'stockAmount',index:'stockAmount',width : 20}, 
                    ],
         rowNum : 20,
         rowList : [ 10, 20, 30 ],
         pager : '#grid-pager-c',
         sortname : 'ID',
         sortorder : "asc",
         altRows: true,
         viewrecords : true,
         autowidth:true,
         multiselect:true,
		 multiboxonly: true,
         loadComplete : function(data) {
             var table = this;
             setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
             oriDataDetail = data;
             $(window).triggerHandler('resize.jqGrid');
         },
	     cellEdit: true,
	     cellsubmit : "clientArray",
  	     emptyrecords: "没有相关记录",
  	     loadtext: "加载中...",
  	     pgtext : "页码 {0} / {1}页",
  	     recordtext: "显示 {0} - {1}共{2}条数据",
    });
    //在分页工具栏中添加按钮
    $("#grid-table-c").navGrid("#grid-pager-c",{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#grid-pager-c",{  
  	   caption:"",   
  	   buttonicon:"ace-icon fa fa-refresh green",   
  	   onClickButton: function(){   
  	      reloadDetailTableList();  
  	   }
  	});  	
  	$(window).on("resize.jqGrid", function () {
  		$("#grid-table-c").jqGrid("setGridWidth", $("#grid-div-c").width());
  		var height = $(".layui-layer-title",_grid_detail.parents(".layui-layer")).height()+
                     $("#baseInfor").outerHeight(true)+$("#materialDiv").outerHeight(true)+
                     $("#grid-pager-c").outerHeight(true)+$("#fixed_tool_div.fixed_tool_div.detailToolBar").outerHeight(true)+
                   //$("#gview_grid-table-c .ui-jqgrid-titlebar").outerHeight(true)+
                     $("#gview_grid-table-c .ui-jqgrid-hbox").outerHeight(true);
                     $("#grid-table-c").jqGrid("setGridHeight",_grid_detail.parents(".layui-layer").height()-height);
  	});
  	$(window).triggerHandler("resize.jqGrid");
  	if($('#baseInfor #inId').val()!=""){
  		reloadDetailTableList();   //重新加载详情列表
  	}

  
//将数据格式化成两位小数：四舍五入
 function formatterNumToFixed(value,options,rowObj){
	if(value!=null){
		var floatNum = parseFloat(value);
		return floatNum.toFixed(2);
	}else{
		return "0.00";
	}
 } 
 //数量输入验证
function numberRegex(value, colname) {
    var regex = /^\d+\.?\d{0,2}$/;
    reloadDetailTableList();  
    if (!regex.test(value)) {
        return [false, ""];
    }
    else  return [true, ""];
}


     $('#materialInfor').select2({
			placeholder : "请选择物料",//文本框的提示信息
			minimumInputLength : 0, //至少输入n个字符，才去加载数据
			allowClear : true, //是否允许用户清除文本信息
			multiple: true,
			closeOnSelect:false,
			ajax : {
				url : context_path + '/instorageOrder/getMListByInId',
				dataType : 'json',
				delay : 250,
				data : function(term, pageNo) { //在查询时向服务器端传输的数据
					term = $.trim(term);
					selectParam = term;
					return {
						/* docId : $("#baseInfor #id").val(), */
						queryString : term, //联动查询的字符
						pageSize : 15, //一次性加载的数据条数
						pageNo : pageNo, //页码
						time : new Date(),
						inPlanId:$("#baseInfor #inPlanId").val()
					//测试
					}
				},
				results : function(data, pageNo) {
					var res = data.result;
					if (res.length > 0) { //如果没有查询到数据，将会返回空串
						var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
						return {
							results : res,
							more : more
						};
					} else {
						return {
							results : {
								"id" : "0",
								"text" : "没有更多结果"
							}
						};
					}

				},
				cache : true
			}
		});

		$('#materialInfor').on("change",function(e){
			var datas=$("#materialInfor").select2("val");
			selectData = datas;
			var selectSize = datas.length;
			if(selectSize>1){
				var $tags = $("#s2id_materialInfor .select2-choices");   //
				//$("#s2id_materialInfor").html(selectSize+"个被选中");
				var $choicelist = $tags.find(".select2-search-choice");
				var $clonedChoice = $choicelist[0];
				$tags.children(".select2-search-choice").remove();
				$tags.prepend($clonedChoice);
				
				$tags.find(".select2-search-choice").find("div").html(selectSize+"个被选中");
				$tags.find(".select2-search-choice").find("a").removeAttr("tabindex");
				$tags.find(".select2-search-choice").find("a").attr("href","#");
				$tags.find(".select2-search-choice").find("a").attr("onclick","removeChoice();");
			}
			//执行select的查询方法
			$("#materialInfor").select2("search",selectParam);
		});
		
		$("#userId").select2({
        placeholder: "选择入库人",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/ASNmanage/getSelectUser",
            type: "POST",
            dataType: 'json',
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }

    });
    
    $("#instorageHouseId").select2({
        placeholder: "请选择库区",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/instorageOrder/getSelectArea",
            type: "POST",
            dataType: 'json',
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }

    });
    
    $("#inPlanId").select2({
        placeholder: "请选择入库计划",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/instorageOrder/getSelectPlan",
            type: "POST",
            dataType: 'json',
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }

    });
    
    
    
if($("#inId").val()!=""){
 $("#userId").select2("data", {
   id: $("#user").val(),
   text: $("#userName").val()
  });
   $.ajax({
	 type:"POST",
	 url:context_path + '/instorageOrder/getOrderById',
	 data:{inId:$('#baseInfor #inId').val()},
	 dataType:"json",
	 success:function(data){
			  $("#instorageHouseId").select2("data", {
 			  id: data.instorageHouseId,
 			  text: data.instorageHouseName
             }); 
             $("#inPlanId").select2("data", {
 			  id: data.inPlanId,
 			  text:data.inPlanNo
             });
             $("#materialInfor").select2("val","");
         reloadDetailTableList();
		}
	});
  
  
}else{
  $('#userId').val("")
}
 //清空物料多选框中的值
 function removeChoice(){
	  $("#s2id_materialInfor .select2-choices").children(".select2-search-choice").remove();
	  $("#materialInfor").select2("val","");
	  selectData = 0;
	  
 }

//添加货物详情 
 function addDetail(){
	  if($("#inId").val()==""){
		  layer.alert("请先保存表单信息！");
		  return;
	  }
	  if(selectData!=0){
		  //将选中的物料添加到数据库中
		  $.ajax({
			  type:"POST",
			  url:context_path + '/instorageOrder/saveDetail',
			  data:{inId:$('#baseInfor #inId').val(),detailIds:selectData.toString()},
			  dataType:"json",
			  success:function(data){
				  removeChoice();   //清空下拉框中的值
				  if(Boolean(data.result)){
						layer.msg("添加成功",{icon:1,time:1200});
					  //重新加载详情表格
					  reloadDetailTableList();  
				  }else{
					  layer.msg(data.msg,{icon:2,time:1200});
				  }
			  }
		  });
	  }else{
		  layer.alert("请选择物料！");
	  }
  }
  //工具栏
	  $("#__toolbar__-c").iToolBar({
	   	 id:"__tb__01",
	   	 items:[
	   	  	{label:"删除", onclick:delDetail},
	    ]
	  });
	//删除货物详情
	function delDetail(){
	   var ids = jQuery("#grid-table-c").jqGrid('getGridParam', 'selarrrow');
	   $.ajax({
	      url:context_path + '/instorageOrder/deleteDetail?ids='+ids,
	      type:"POST",
	      dataType:"JSON",
	      success:function(data){
	         if(data.result){
	           layer.msg("操作成功！");
	            //重新加载详情表格
					reloadDetailTableList();  
	         }
	      }
	   });
	}
	function reloadDetailTableList(){
	  $("#grid-table-c").jqGrid('setGridParam', {
		  postData: {inId:$("#baseInfor #inId").val()} //发送数据  :选中的节点
	  }).trigger("reloadGrid");
	}
$('#fromInfoContent .mySelect2').select2();
</script>
