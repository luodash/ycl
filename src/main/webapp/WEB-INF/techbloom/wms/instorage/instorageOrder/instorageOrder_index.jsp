<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <%-- <%@ include file="/techbloom/common/taglibs.jsp"%> --%>
	<script type="text/javascript">
	    var context_path = '<%=path%>';
	</script>
	<style type="text/css">
	
	.query_box .field-button.two {
	    padding: 0px;
	    left: 650px;
    }
	</style>
</head>
<body style="overflow:hidden;">
   <div id="instorageOrder_index_grid-div">
        <div id="instorageOrder_index_fixed_tool_div" class="fixed_tool_div">
             <div id="instorageOrder_index___toolbar__" style="float:left;overflow:hidden;"></div>
        </div>
		<table id="instorageOrder_index_grid-table" style="width:100%;height:100%;"></table>
		<div id="instorageOrder_index_grid-pager"></div>
   </div>
</body>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriData;
    var _grid;

    _grid = jQuery("#instorageOrder_index_grid-table").jqGrid({
        url : context_path + '/instorageOrder/list.do',
        datatype : "json",
        colNames : [ '入库单主键','入库单编号','入库计划编号','入库类型','入库时间', '入库人','库区','备注','状态'],
        colModel : [
            {name : 'id',index : 'id',width : 20,hidden:true},
            {name : 'instorageNo',index : 'instorageNo',width : 40},
            {name : 'inPlanNo',index : 'inPlanNo',width : 40},
            {name : 'instorageTypeName',index : 'instorageTypeName',width : 40},
            {name : 'instorageTime',index : 'instorageTime',width : 40},
            {name : 'userName',index : 'userName',width : 40},
            {name : 'instorageHouseName',index : 'instorageHouseName',width : 40},
            {name : 'remark',index : 'REMARK',width : 40},
            {name : 'state',index : 'state',width : 40,
                formatter:function(cellValue,option,rowObject){
                    if(typeof cellValue == 'number'){
                        if(cellValue==0){
                            return "<span style='color:#d15b47;font-weight:bold;'>未提交</span>";
                        }else if(cellValue==1){
                            return "<span style='color:#76b86b;font-weight:bold;'>已提交</span>";
                        }else if(cellValue==4){
                            return "<span style='color:#E66B1A;font-weight:bold;'>已入库</span>";
                        }
                    }else{
                        return "异常";
                    }
                }
            }
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : '#instorageOrder_index_grid-pager',
        sortname : 'ID',
        sortorder : "asc",
        altRows: false,
        viewrecords : true,
        caption : "入库单列表",
        autowidth:true,
        multiselect:true,
        multiboxonly: true,
        beforeRequest:function (){
            dynamicGetColumns(dynamicDefalutValue,"instorageOrder_index_grid-table",$(window).width()-$("#sidebar").width() -7);
            //重新加载列属性
        },
        loadComplete : function(data)
        {
            var table = this;
            setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            oriData = data;
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
    });
	 //在分页工具栏中添加按钮
	  jQuery("#instorageOrder_index_grid-table").navGrid("#instorageOrder_index_grid-pager",{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#instorageOrder_index_grid-pager",{
		   caption:"",
		   buttonicon:"ace-icon fa fa-refresh green",
		   onClickButton: function(){
			   $("#instorageOrder_index_grid-table").jqGrid("setGridParam",
						{
							postData: {queryJsonString:""} //发送数据
						}
				  ).trigger("reloadGrid");
		   }
		}).navButtonAdd("#instorageOrder_index_grid-pager",{
              caption: "",
              buttonicon:"fa  icon-cogs",
              onClickButton : function (){
                  jQuery("#instorageOrder_index_grid-table").jqGrid("columnChooser",{
                      done: function(perm, cols){
                          dynamicColumns(cols,dynamicDefalutValue);
                          $("#instorageOrder_index_grid-table").jqGrid("setGridWidth", $("#instorageOrder_index_grid-div").width()-3);
                      }
                  });
              }
          });
	  $(window).on("resize.jqGrid", function () {
		  $("#instorageOrder_index_grid-table").jqGrid("setGridWidth", $("#instorageOrder_index_grid-div").width() - 3 );
		  var height = $("#breadcrumb").outerHeight(true)+$("#instorageOrder_index_yy").outerHeight(true)+
            $("#instorageOrder_index_fixed_tool_div").outerHeight(true)+
            $("#gview_instorageOrder_index_grid-table .ui-jqgrid-titlebar").outerHeight(true)+
            $("#gview_instorageOrder_index_grid-table .ui-jqgrid-hbox").outerHeight(true)+
            $("#instorageOrder_index_grid-pager").outerHeight(true)+$("#header").outerHeight(true);
            $("#instorageOrder_index_grid-table").jqGrid( "setGridHeight", (document.documentElement.clientHeight)-height );
	  })
	
	  $(window).triggerHandler("resize.jqGrid");


</script>
</html>