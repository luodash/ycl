﻿<%@ page language="java" import="java.lang.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<div class="main-content" style="height:100%">
	<div class="widget-header widget-header-large" id="instorageOrder_detail_div1">
		<!-- 隐藏的主键 -->
		<input type="hidden" id="instorageOrder_detail_id" name="id" value="${INSTORAGE.id }">
		<input type="hidden" id="instorageOrder_detail_inPlanId" name="inPlanId"value="${INSTORAGE.inPlanId }">
		<h3 class="widget-title grey lighter" style=' background: none; border-bottom: none; '>
			<i class="ace-icon fa fa-leaf green"></i> 入库单
		</h3>
		<!-- #section:pages/invoice.info -->
		<div class="widget-toolbar no-border invoice-info">
			<span class="invoice-info-label">入库单编号:</span> <span class="red">${INSTORAGE.instorageNo }</span><br />
			<span class="invoice-info-label">入库时间:</span> <span class="blue">${INSTORAGE.instorageTime}</span>
		</div>
		<!-- 打印按钮 -->
		<div class="widget-toolbar hidden-480">
			<a href="#" onclick="printDoc();"> 
			   <i class="ace-icon fa fa-print"></i>
			</a>
		</div>
	</div>
	<div class="widget-body" id="instorageOrder_detail_div2">
		<table style="width: 100%;font-size:16px;border-collapse:separate;border-spacing:2px 3px;margin-left: 20px">
			<tr>
				<td>
				  <i class="ace-icon fa fa-caret-right blue"></i> 入库类型： 
				  <b class="black">${INSTORAGE.instorageTypeName }</b>
				</td>
				<td>
				   <i class="ace-icon fa fa-caret-right blue"></i> 入库人: 
				   <b class="black">${INSTORAGE.userName }</b>
				</td>
			</tr>
			<tr>
				<td>
				   <i class="ace-icon fa fa-caret-right blue"></i> 库区： 
				   <b class="black">${INSTORAGE.instorageHouseName }</b>
				</td>
				<td>
				   <i class="ace-icon fa fa-caret-right blue"></i> 入库计划单据: 
				   <b class="black">${INSTORAGE.inPlanNo }</b>
				</td>
			</tr>
			<tr>
			<tr>
				<td>
				   <i class="ace-icon fa fa-caret-right blue"></i> 备注: 
				   <b class="black">${INSTORAGE.remark }</b>
				</td>
				<td><i class="ace-icon fa fa-caret-right blue"></i> 状态 : <c:if
						test="${INSTORAGE.state==0 }">
						<span style="color: #d15b47; font-weight: bold;">未提交</span>
					</c:if> <c:if test="${INSTORAGE.state==1 }">
						<span style="color: #87b87f; font-weight: bold;">已提交</span>
					</c:if> <c:if test="${INSTORAGE.state==2 }">
						<span style="color: #76b86b; font-weight: bold;">已审核</span>
					</c:if> <c:if test="${INSTORAGE.state==3 }">
						<span style="color: #d15b47; font-weight: bold;">已驳回</span>
					</c:if> <c:if test="${INSTORAGE.state==4 }">
						<span style="color: #76b86b; font-weight: bold;">入库完成</span>
					</c:if> <c:if test="${INSTORAGE.state==5 }">
						<span style="color: gray; font-weight: bold;">已废弃</span>
					</c:if></td>
			</tr>
			</tr>
		</table>
	</div>
	<!-- 详情表格 -->
	<div id="instorageOrder_detail_grid-div-c">
		<!-- 物料信息表格 -->
		<table id="instorageOrder_detail_grid-table-c" style="width:100%;height:100%;"></table>
		<!-- 表格分页栏 -->
		<div id="instorageOrder_detail_grid-pager-c"></div>
	</div>
</div>
<script type="text/javascript">
var oriDataView;
var _grid_view;        //表格对象
 _grid_view=jQuery("#instorageOrder_detail_grid-table-c").jqGrid({
        url : context_path + '/instorageOrder/detailList?inId='+$("#instorageOrder_detail_id").val(),
        datatype : "json",
        colNames : [ '详情主键','物料编号','物料名称','批次号','入库数量'],
        colModel : [ 
 					  {name : 'id',index : 'id',width : 20,hidden:true}, 
 					  {name : 'materialNo',index:'materialNo',width :20}, 
                      {name : 'materialName',index:'materialName',width : 20}, 
                      {name : 'materialCode',index:'materialCode',width : 20}, 
                      {name : 'amount', index: 'amount', width: 20},
                   ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : '#instorageOrder_detail_grid-pager-c',
        sortname : 'ID',
        sortorder : "asc",
        altRows: true,
        viewrecords : true,
        autowidth:true,
		 multiboxonly: true,
         loadComplete : function(data) {
             var table = this;
             setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
             oriDataDetail = data;
              $(window).triggerHandler('resize.jqGrid');
         },
	   cellEdit: true,
	   cellsubmit : "clientArray",
  	   emptyrecords: "没有相关记录",
  	   loadtext: "加载中...",
  	   pgtext : "页码 {0} / {1}页",
  	   recordtext: "显示 {0} - {1}共{2}条数据",
    });
    
    $(window).on('resize.jqGrid', function () {
  		$("#instorageOrder_detail_grid-table-c").jqGrid( 'setGridWidth', $("#instorageOrder_detail_grid-div-c").width() - 3 );
  		 var height =$(".layui-layer-title",_grid_view.parents(".layui-layer")).height()+
  		 $("#instorageOrder_detail_div1").outerHeight(true)+$("#div2").outerHeight(true)+
  		 $("#instorageOrder_detail_grid-pager-c").outerHeight(true)+
  		  $("#gview_instorageOrder_detail_grid-table-c .ui-jqgrid-hbox").outerHeight(true);
          $("#instorageOrder_detail_grid-table-c").jqGrid('setGridHeight',_grid_view.parents(".layui-layer").height()-height);
  	});
  	$(window).triggerHandler('resize.jqGrid');
    function printDoc(){
	var url = context_path + "/getbills/printGetbillsDetail?putId="+$("#instorageOrder_detail_id").val();
	window.open(url);
}	
</script>