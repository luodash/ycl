<%@ page language="java" import="java.lang.*"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
	String path = request.getContextPath();
%>
<div class="main-content" style="height:100%">
	<div class="widget-header widget-header-large" id="salemanage_detail_div1">
		<input type="hidden" id="salemanage_detail_id" name="id" value="${sale.id }" />
		<h3 class="widget-title grey lighter" style="background: none; border-bottom: none;">
			<i class="ace-icon fa fa-leaf green"></i>
			销售订单
		</h3>
		<div class="widget-toolbar no-border invoice-info">
			<span class="invoice-info-label">订单编号：</span>
			<span class="red">${sale.saleNo }</span>
			<br />
			<span class="invoice-info-label">销售日期：</span>
			<span class="blue">${fn:substring(sale.saleDate, 0, 19)}</span>
		</div>
		<!-- 打印按钮 -->
		<div class="widget-toolbar hidden-480">
			<a href="#" onclick="printDoc();" title="详情打印">
				<i class="ace-icon fa fa-print"></i>
			</a>
		</div>
	</div>
	<div class="widget-body" id="salemanage_detail_div2">
		<table style="width: 100%;font-size:16px;border-collapse:separate;border-spacing:2px 3px;">
			<tr>
				<td>
					<i class="ace-icon fa fa-caret-right blue"></i>
					订单编号：
					<b class="black">${sale.saleNo }</b>
				</td>
				<td>
					<i class="ace-icon fa fa-caret-right blue"></i>
					订单名称：
					<b class="black">${sale.saleName }</b>
				</td>
			</tr>
			<tr>
				<td>
					<i class="ace-icon fa fa-caret-right blue"></i>
					客户：
					<b class="black">${sale.customerName }</b>
				</td>
				<td>
					<i class="ace-icon fa fa-caret-right blue"></i>
					地址：
					<b class="black">${sale.address }</b>
				</td>
			</tr>
			<tr>
				<td>
					<i class="ace-icon fa fa-caret-right blue"></i>
					联系方式：
					<b class="black">${sale.contact }</b>
				</td>
				<td>
					<i class="ace-icon fa fa-caret-right blue"></i>
					状态：
					<c:if test="${sale.state==0 }"><span style="color:grey;font-weight:bold;">未审核</span></c:if>
					<c:if test="${sale.state==1 }"><span style="color:brown;font-weight:bold;">已审核</span></c:if>
					<c:if test="${sale.state==2 }"><span style="color:blue;font-weight:bold;">计划中</span></c:if>
					<c:if test="${sale.state==3 }"><span style="color:green;font-weight:bold;">出库完成</span></c:if>
				</td>
			</tr>
			<tr>
				<td>
					<i class="ace-icon fa fa-caret-right blue"></i>
					备注：
					<b class="black">${sale.remark }</b>
				</td>
			</tr>
		</table>
	</div>
	<!-- 详情表格 -->
	<div id="salemanage_detail_grid-div-c">
		<!-- 物料信息表格 -->
		<table id="salemanage_detail_grid-table-c" style="width:100%;height:100%;"></table>
		<!-- 表格分页栏 -->
		<div id="salemanage_detail_grid-pager-c"></div>
	</div>
</div>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriData;      //表格数据
    var _grid;        //表格对象
    $(function() {
        //初始化表格
        _grid = jQuery("#salemanage_detail_grid-table-c").jqGrid({
            url : context_path + "/salemanage/detailList?saleId="+$("#salemanage_detail_id").val(),
            datatype : "json",
            colNames : [ "详情主键","物料编号","物料名称","数量","可拆分数量","出库计划数量"],
            colModel : [ 
  					     {name : "id",index : "id", hidden:true}, 
  					     {name : "materialNo",index:"materialNo",width :40}, 
                         {name : "materialName",index:"materialName",width : 40},
                         {name : "amount", index: "amount", width: 30}, 
                         {name : "splitAmount",index:"splitAmount",width : 30},
                         {name : "planAmount", index: "planAmount", width: 30}                        
                       ],
            rowNum : 20,
            rowList : [ 10, 20, 30 ],
            pager : "#salemanage_detail_grid-pager-c",
            sortname : "id",
            sortorder: "desc",
            altRows: true,
            viewrecords: true,
            autowidth: true,
            multiselect: false,
            multiboxonly: true,
            loadComplete: function (data){
                var table = this;
                setTimeout(function(){
                    updatePagerIcons(table);
                    enableTooltips(table);
                }, 0);
                oriData = data;
                $(window).triggerHandler("resize.jqGrid");
            },
            emptyrecords: "没有相关记录",
            loadtext: "加载中...",
            pgtext: "页码 {0} / {1}页",
            recordtext: "显示 {0} - {1}共{2}条数据"
        });
        //在分页工具栏中添加按钮
        jQuery("#salemanage_detail_grid-table-c").navGrid("#salemanage_detail_grid-pager-c", {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false
        }).navButtonAdd("#salemanage_detail_grid-pager-c", {
            caption: "",
            buttonicon: "ace-icon fa fa-refresh green",
            onClickButton: function () {
                $("#salemanage_detail_grid-table-c").jqGrid("setGridParam",
                    {
                        postData: {PUTBILLSID: $('#id').val()} //发送数据  :选中的节点
                    }
                ).trigger("reloadGrid");
            }
        });

        $(window).on("resize.jqGrid", function () {
            $("#salemanage_detail_grid-table-c").jqGrid("setGridWidth", $("#salemanage_detail_grid-div-c").width() - 3);
            var height = $(".layui-layer-title",_grid.parents(".layui-layer")).height()+
                         $("#salemanage_detail_div1").outerHeight(true)+$("#salemanage_detail_div2").outerHeight(true)+
                         $("#gview_salemanage_detail_grid-table-c .ui-jqgrid-hbox").outerHeight(true)+
                         $("#salemanage_detail_grid-pager-c").outerHeight(true);
                         $("#salemanage_detail_grid-table-c").jqGrid("setGridHeight", _grid.parents(".layui-layer").height()-height);
        });
    });
    function printDoc(){
        var url = context_path + "/getbills/printGetbillsDetail?putId="+$("#salemanage_detail_id").val();
        window.open(url);
    }
</script>