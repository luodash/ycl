<%@ page language="java" import="java.lang.*"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
  <div class="widget-box" style="border:10px;margin:10px">
   <input type="hidden" id="salemanage_outPlan_saleId" value="${saleId }">
	<!-- 表格div -->
	<div id="salemanage_outPlan_grid-div-c" style="width:100%;margin:10px auto;">
		<!-- 	表格工具栏 -->
        <div id="salemanage_outPlan_fixed_tool_div" class="fixed_tool_div detailToolBar">
             <div id="salemanage_outPlan___toolbar__-c" style="float:left;overflow:hidden;"></div>
        </div>
		<!-- 物料详情信息表格 -->
		<table id="salemanage_outPlan_grid-table-c" style="width:100%;height:100%;"></table>
		<!-- 表格分页栏 -->
		<div id="salemanage_outPlan_grid-pager-c"></div>
	</div>
</div>
<script type="text/javascript">
var context_path = '<%=path%>';
var saleId=$("#salemanage_outPlan_saleId").val();
var oriDataDetail;
 var _grid_detail;//表格对象
 var lastsel2;

  	_grid_detail=jQuery("#salemanage_outPlan_grid-table-c").jqGrid({
         url : context_path + '/salemanage/detailList?saleId='+saleId,
         datatype : "json",
         colNames : [ '详情主键','物料编号','物料名称','销售总数量','库存可用数量','可拆分数量','出库计划数量'],
         colModel : [ 
  					  {name : 'id',index : 'id',width : 20,hidden:true}, 
  					  {name : 'materialNo',index:'materialNo',width :20}, 
                      {name : 'materialName',index:'materialName',width : 20}, 
                      {name : 'amount',index:'amount',width : 20},
                      {name : 'stockAmount',index:'stockAmount',width : 20},
                      {name : 'splitAmount',index:'splitAmount',width : 20},
                      {name: 'planAmount', index: 'planAmount', width: 20,editable : true,editrules: {custom: true, custom_func: numberRegex},
                        editoptions: {
	                          size: 25,
	                          dataEvents: [
	                              {
	                                  type: 'blur',     //blur,focus,change.............
	                                  fn: function (e) {
	                                	  var $element = e.currentTarget;
	                                	  var $elementId = $element.id;
	                                	  var rowid = $elementId.split("_")[0];
	                                	  var id=$element.parentElement.parentElement.children[1].textContent;
	                                	  var indocType = 1;
	                                		  var reg = new RegExp("^[0-9]+(.[0-9]{1,2})?$");
	                                		  if (!reg.test($("#"+$elementId).val())) {
	                                			  layer.alert("非法的数量！(注：可以有两位小数的正实数)");
	                                			  return;
	                                		  }
	                                	  $.ajax({
	                                		  url:context_path + '/salemanage/updateSplitAmount',
	                                		  type:"POST",
	                                		  data:{id:id,planAmount:$("#"+rowid+"_planAmount").val()},
	                                		  dataType:"json",
	                                		  success:function(data){
	                                		        if(!data.result) layer.alert(data.msg);
	                                		        $("#salemanage_outPlan_grid-table-c").jqGrid('setGridParam', 
                                							{
                                					    		url : context_path + '/salemanage/detailList?saleId='+saleId,
                                								postData: {queryJsonString:""} //发送数据  :选中的节点
                                							}
                                					  ).trigger("reloadGrid");
	                                		  }
	                                	  }); 
	                                  }
	                              }
	                          ]
	                      }
                      },
                    ],
         rowNum : 20,
         rowList : [ 10, 20, 30 ],
         pager : '#salemanage_outPlan_grid-pager-c',
         sortname : 'ID',
         sortorder : "asc",
         altRows: true,
         viewrecords : true,
         caption : "物料列表",
         autowidth:true,
         multiselect:true,
		 multiboxonly: true,
         loadComplete : function(data) {
             var table = this;
             setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
             oriDataDetail = data;
         },
	   cellEdit: true,
	   cellsubmit : "clientArray",
  	   emptyrecords: "没有相关记录",
  	   loadtext: "加载中...",
  	   pgtext : "页码 {0} / {1}页",
  	   recordtext: "显示 {0} - {1}共{2}条数据",
    });
    //在分页工具栏中添加按钮
    $("#salemanage_outPlan_grid-table-c").navGrid('#salemanage_outPlan_grid-pager-c',{edit:false,add:false,del:false,search:false,refresh:false})
      .navButtonAdd('#salemanage_outPlan_grid-pager-c',{  
  	   caption:"",   
  	   buttonicon:"ace-icon fa fa-refresh green",   
  	   onClickButton: function(){   
  	    reloadDetailTableList();
  	   }
  	});
  	
  	$(window).on('resize.jqGrid', function () {
  		$("#salemanage_outPlan_grid-table-c").jqGrid( 'setGridWidth', $("#salemanage_outPlan_grid-div-c").width() - 3 );
  		$("#salemanage_outPlan_grid-table-c").jqGrid( 'setGridHeight', (document.documentElement.clientHeight - $("#salemanage_outPlan_grid-pager-c").height() - 380) );
  	});
  	$(window).triggerHandler('resize.jqGrid');

  
//将数据格式化成两位小数：四舍五入
 function formatterNumToFixed(value,options,rowObj){
	if(value!=null){
		var floatNum = parseFloat(value);
		return floatNum.toFixed(2);
	}else{
		return "0.00";
	}
 } 
 //数量输入验证
function numberRegex(value, colname) {
    var regex = /^\d+\.?\d{0,2}$/;
    reloadDetailTableList();
    if (!regex.test(value)) {
        return [false, ""];
    }
    else  return [true, ""];
}

  //工具栏
	  $("#salemanage_outPlan___toolbar__-c").iToolBar({
	   	 id:"salemanage_outPlan___tb__01",
	   	 items:[
	   	 	{label:"确认生成", onclick:outPlan}
	    ]
	  });
  
	function outPlan(){
	   var ids = jQuery("#salemanage_outPlan_grid-table-c").jqGrid('getGridParam', 'selarrrow');
	   var checkedNum = getGridCheckedNum("#salemanage_outPlan_grid-table-c","id");  //选中的数量
	  if(checkedNum==0){
		layer.alert("请至少选择一个物料！");
	  }else{
	  $.ajax({
    	type : "POST",
    	url:context_path + "/salemanage/isHavePlanAmount?ids="+ids,
    	dataType : 'json',
    	cache : false,
    	success : function(data) {
    	 if(data.result){
			    layer.confirm("确定选中的物料生成出库计划（库存不够将自动生成生产订单）？", function() {
		    		$.ajax({
		    			type : "POST",
		    			url:context_path + "/salemanage/outPlan?ids="+ids,
		    			dataType : 'json',
		    			cache : false,
		    			success : function(data) {
		    				//layer.closeAll();
		    				if(data){
								layer.msg(data.msg, {icon: 1,time:1000});
		    				}else{
		    					layer.msg(data.msg, {icon: 2,time:1000});
		    				}
		    				reloadDetailTableList(); //重新加载表格
		    			}
		    		});
		    	});
    	   }else{
   			  layer.alert(data.msg);
  			}
    	}
    	});
	   }
	   
	}
	function reloadDetailTableList(){
	  $("#salemanage_outPlan_grid-table-c").jqGrid('setGridParam', 
				{
  	    			url:context_path + '/salemanage/detailList?saleId='+saleId,
					postData: {queryJsonString:""} //发送数据  :选中的节点
				}
		  ).trigger("reloadGrid");
	}
  </script>
