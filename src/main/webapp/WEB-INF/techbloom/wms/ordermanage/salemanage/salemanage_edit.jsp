<%@ page language="java" import="java.lang.*"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<div class="row-fluid" style="height: inherit;margin:0px;border: 0px">
	<form id="baseInfor" class="form-horizontal" target="_ifr">
	   <input type="hidden" id="saleId" name="id" value="${sale.id}" />
  	   <input type="hidden" id="state" name="state" value="${sale.state}" />   
	   <div class="row" style="margin:0;padding:0;">
	        <%--销售编号--%>
	        <div class="control-group span6" style="display: inline">
	             <label class="control-label" for="saleNo" >销售编号：</label>
	             <div class="controls">
	                  <div class="input-append span12" >
	                       <input type="text" id="saleNo" class="span10" name="saleNo" placeholder="后台自动生成" readonly="readonly" value="${sale.saleNo}" />
	                  </div>
	             </div>
	         </div>
	         <div class="control-group span6" style="display: inline">
	              <label class="control-label" for="saleName" >销售订单名称：</label>
	              <div class="controls">
	                    <div class="input-append  span12 required" >
	                        <input type="text" id="saleName" class="span10" name="saleName" placeholder="销售订单名称" value="${sale.saleName}"  />
	                    </div>
	              </div>
	         </div>
	    </div>
  	    <div class="row" style="margin:0;padding:0;">
	         <div class="control-group span6" style="display: inline">
	              <label class="control-label" for="customer" >收货人(客户)：</label>
	              <div class="controls">
	                   <div class="span12 required">
	                        <input type="text" id="customer" class="span10" name="customer" placeholder="收货人(客户)" />
	                        <input id="customerName" type="hidden" name="customerName" value="${sale.customerName }" />
				            <input id="customerId" type="hidden" name="customerId" value="${sale.customer}" />
	                   </div>
	              </div>
	          </div>
	          <div class="control-group span6" style="display: inline">
	               <label class="control-label" for="saleDate" >销售日期：</label>
	               <div class="controls">
	                    <div class="input-append span12 required" >
	                        <input type="text" class="form-control date-picker" id="saleDate" class="span10" name="saleDate"   placeholder="销售日期" value="${sale.saleDate}"/>
	                    </div>
	                </div>
	           </div>
	      </div>  	      
	      <div class="row" style="margin:0;padding:0;">
	            <%--地址--%>
	           <div class="control-group span6" style="display: inline">
	                <label class="control-label" for="address" >地址：</label>
	                <div class="controls">
	                    <div class="span12" >
	                        <input id="address" type="text" class="span10" name="address" value="${sale.address }" placeholder="地址" />
	                    </div>
	                </div>
	            </div>
	            <div class="control-group span6" style="display: inline">
	                <label class="control-label" for="contact" >联系方式：</label>
	                <div class="controls">
	                    <div class="input-append span12" >
	                        <input type="text" id="contact" class="span10" name="contact" placeholder="联系方式" value="${sale.contact}" />
	                    </div>
	                </div>
	            </div>
	      </div>
	      <div class="row" style="margin:0;padding:0;">
            <%--备注--%>
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="remark" >备注：</label>
                <div class="controls">
                    <div class="input-append   span12" >
                        <input class="span10" type="text" id="remark" name="remark" placeholder="备注" value="${sale.remark}" />
                    </div>
                </div>
            </div>
            <%--计划发货时间--%>
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="order_p_time" >计划发货时间：</label>
                <div class="controls">
                    <div class="input-append   span12" >
                       <input type="text" class="form-control date-picker" id="order_p_time" class="span10" name="order_p_time"   placeholder="计划发货时间" value="${sale.order_p_time}"/>
                    </div>
                </div>
            </div>
        </div>        
        <div style="margin-left:10px;">
            <span class="btn btn-info" id="formSave">
		       <i class="ace-icon fa fa-check bigger-110"></i>保存
            </span>
        </div>       
	</form>		
	<div id="materialDiv" style="margin:10px;">
		<!-- 下拉框 -->
		<label class="inline" for="materialInfor">物料：</label>
		<input type="text" id = "materialInfor" name="materialInfor" style="width:350px;margin-right:10px;" />
		<button id="addMaterialBtn" class="btn btn-xs btn-primary" onclick="addDetail();">
			<i class="icon-plus" style="margin-right:6px;"></i>添加
		</button>
	</div>
	<!-- 表格div -->
	<div id="grid-div-c" style="width:100%;margin:0px auto;">
		<!-- 	表格工具栏 -->
        <div id="fixed_tool_div" class="fixed_tool_div detailToolBar">
             <div id="__toolbar__-c" style="float:left;overflow:hidden;"></div>
        </div>
		<!-- 物料详情信息表格 -->
		<table id="grid-table-c" style="width:100%;height:100%;"></table>
		<!-- 表格分页栏 -->
		<div id="grid-pager-c"></div>
	</div>
</div>
<script type="text/javascript">
var context_path = '<%=path%>';
var saleId=$("#saleId").val();
/* var cust=${sale.customer}; */
var oriDataDetail;
var _grid_detail;        //表格对象
var lastsel2;
$(".date-picker").datetimepicker({format: 'YYYY-MM-DD HH:mm:ss',useMinutes:true,useSeconds:true});
$("#baseInfor").validate({
   ignore: "", 
   rules:{
      "saleDate":{
         required:true,
      },
      "saleName":{
         required:true,
         remote:{
            cache:false,
            async:false,
            url: "<%=path%>/salemanage/isHaveName?saleId="+$("#saleId").val()
         }
      },
      "customer":{
         required:true,
      },
   },
   messages: {
     "saleDate":{
         required:"请选择日期！"
      },
      "saleName":{
         required:"请输入订单名称！",
         remote:"名称重复！"
      },
      "customer":{
         required:"请选择客户！",
      },
      
   },
   errorClass: "help-inline",
    errorElement: "span",
    highlight:function(element, errorClass, validClass) {
        $(element).parents('.control-group').addClass('error');
    },
    unhighlight: function(element, errorClass, validClass) {
        $(element).parents('.control-group').removeClass('error');
    }
});

$("#formSave").click(function(){
 var bean = $("#baseInfor").serialize();
if($('#baseInfor').valid()){
   saveOrUpdate(bean);
}
});

/* 保存、修改 */
function saveOrUpdate(bean){
   $.ajax({
   url:context_path+"/salemanage/save",
   type:"post",
   data:bean,
   dataType:"JSON",
   success:function (data){
      if(data.result){
      $("#saleId").val(data.saleId);
      $("#baseInfor #saleNo").val(data.saleNo);
      saleId=data.saleId;
      gridReload();
      layer.msg("操作成功！",{icon:1,time:1200});
      }else{
      layer.msg("操作失败！",{icon:2,time:1200});
      }
   }
   });

}
//单元格编辑成功后，回调的函数
	var editFunction = function eidtSuccess(XHR){
		 var data = eval("("+XHR.responseText+")"); 
		if(data["msg"]!=""){
			layer.alert(data["msg"]);
		}
		jQuery("#grid-table-c").jqGrid('setGridParam', 
				{
					postData: {
						id:$('#id').val(),
						queryJsonString:""
					} 
				}
		  ).trigger("reloadGrid");
	}; 
  	_grid_detail=jQuery("#grid-table-c").jqGrid({
         url : context_path + '/salemanage/detailList?saleId='+saleId,
         datatype : "json",
         colNames : [ '详情主键','物料编号','物料名称','数量',],
         colModel : [ 
  					  {name : 'id',index : 'id',width : 20,hidden:true}, 
  					  {name : 'materialNo',index:'materialNo',width :20}, 
                      {name : 'materialName',index:'materialName',width : 20}, 
                      //{name : 'amount',index:'amount',width : 20,editable : true,editrules: {custom: true, custom_func: numberRegex}},
                      {name: 'amount', index: 'amount', width: 60,editable : true,editrules: {custom: true, custom_func: numberRegex},
                        editoptions: {
	                          size: 25,
	                          dataEvents: [
	                              {
	                                  type: 'blur',     //blur,focus,change.............
	                                  fn: function (e) {
	                                	  var $element = e.currentTarget;
	                                	  var $elementId = $element.id;
	                                	  var rowid = $elementId.split("_")[0];
	                                	  var id=$element.parentElement.parentElement.children[1].textContent;
	                                	  var indocType = 1;
	                                	 // var rowData = $("#grid-table-c").jqGrid('getRowData',rowid).id;
	                                		  var reg = new RegExp("^[0-9]+(.[0-9]{1,2})?$");
	                                		  if (!reg.test($("#"+$elementId).val())) {
	                                			  layer.alert("非法的数量！(注：可以有两位小数的正实数)");
	                                			  return;
	                                		  }
	                                	  $.ajax({
	                                		  url:context_path + '/salemanage/updateAmount',
	                                		  type:"POST",
	                                		  data:{id:id,amount:$("#"+rowid+"_amount").val()},
	                                		  dataType:"json",
	                                		  success:function(data){
	                                		        if(!data.result){
	                                		           layer.alert(data.msg);
	                                		        }
	                                		        $("#grid-table-c").jqGrid('setGridParam', 
                                							{
                                					    		url : context_path + '/salemanage/detailList?saleId='+saleId,
                                								postData: {queryJsonString:""} //发送数据  :选中的节点
                                							}
                                					  ).trigger("reloadGrid");
	                                		      
	                                		  }
	                                	  }); 
	                                  }
	                              }
	                          ]
	                      }
                      },
                    ],
         rowNum : 20,
         rowList : [ 10, 20, 30 ],
         pager : '#grid-pager-c',
         sortname : 'ID',
         sortorder : "desc",
         altRows: true,
         viewrecords : true,
         autowidth:true,
         multiselect:true,
		 multiboxonly: true,
         loadComplete : function(data) {
             var table = this;
             setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
             oriDataDetail = data;
             $(window).triggerHandler('resize.jqGrid');
         },
	   cellEdit: true,
	   cellsubmit : "clientArray",
  	   emptyrecords: "没有相关记录",
  	   loadtext: "加载中...",
  	   pgtext : "页码 {0} / {1}页",
  	   recordtext: "显示 {0} - {1}共{2}条数据",
    });
    //在分页工具栏中添加按钮
    $("#grid-table-c").navGrid('#grid-pager-c',{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd('#grid-pager-c',{  
  	   caption:"",   
  	   buttonicon:"ace-icon fa fa-refresh green",   
  	   onClickButton: function(){   
  	    $("#grid-table-c").jqGrid('setGridParam', 
				{
  	    			url:context_path + '/salemanage/detailList?saleId='+saleId,
					postData: {queryJsonString:""} //发送数据  :选中的节点
				}
		  ).trigger("reloadGrid");
  	   }
  	});
  	
  	$(window).on("resize.jqGrid", function () {
  		$("#grid-table-c").jqGrid("setGridWidth", $("#grid-div-c").width() - 3 );
  		var height = $(".layui-layer-title",_grid_detail.parents(".layui-layer")).height()+
                     $("#baseInfor").outerHeight(true)+$("#materialDiv").outerHeight(true)+
                     $("#grid-pager-c").outerHeight(true)+$("#fixed_tool_div.fixed_tool_div.detailToolBar").outerHeight(true)+
                     $("#gview_grid-table-c .ui-jqgrid-hbox").outerHeight(true);
                     $("#grid-table-c").jqGrid("setGridHeight",_grid_detail.parents(".layui-layer").height()-height);
  	});
  	$(window).triggerHandler("resize.jqGrid");
  	if($("#id").val()!=""){
  		//reloadDetailTableList();   //重新加载详情列表
  	}  
//将数据格式化成两位小数：四舍五入
 function formatterNumToFixed(value,options,rowObj){
	if(value!=null){
		var floatNum = parseFloat(value);
		return floatNum.toFixed(2);
	}else{
		return "0.00";
	}
 } 
 //数量输入验证
function numberRegex(value, colname) {
    var regex = /^\d+\.?\d{0,2}$/;
    reloadDetailTableList();
    if (!regex.test(value)) {
        return [false, ""];
    }
    else  return [true, ""];
}
     $('#materialInfor').select2({
			placeholder : "请选择物料",//文本框的提示信息
			minimumInputLength : 0, //至少输入n个字符，才去加载数据
			allowClear : true, //是否允许用户清除文本信息
			multiple: true,
			closeOnSelect:false,
			ajax : {
				url : context_path + '/salemanage/getMListByInId',
				dataType : 'json',
				delay : 250,
				data : function(term, pageNo) { //在查询时向服务器端传输的数据
					term = $.trim(term);
					selectParam = term;
					return {
						/* docId : $("#baseInfor #id").val(), */
						queryString : term, //联动查询的字符
						pageSize : 15, //一次性加载的数据条数
						pageNo : pageNo, //页码
						time : new Date()
					//测试
					}
				},
				results : function(data, pageNo) {
					var res = data.result;
					if (res.length > 0) { //如果没有查询到数据，将会返回空串
						var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
						return {
							results : res,
							more : more
						};
					} else {
						return {
							results : {
								"id" : "0",
								"text" : "没有更多结果"
							}
						};
					}

				},
				cache : true
			}
		});

		$('#materialInfor').on("change",function(e){
			var datas=$("#materialInfor").select2("val");
			selectData = datas;
			var selectSize = datas.length;
			if(selectSize>1){
				var $tags = $("#s2id_materialInfor .select2-choices");   //
				//$("#s2id_materialInfor").html(selectSize+"个被选中");
				var $choicelist = $tags.find(".select2-search-choice");
				var $clonedChoice = $choicelist[0];
				$tags.children(".select2-search-choice").remove();
				$tags.prepend($clonedChoice);
				
				$tags.find(".select2-search-choice").find("div").html(selectSize+"个被选中");
				$tags.find(".select2-search-choice").find("a").removeAttr("tabindex");
				$tags.find(".select2-search-choice").find("a").attr("href","#");
				$tags.find(".select2-search-choice").find("a").attr("onclick","removeChoice();");
			}
			//执行select的查询方法
			$("#materialInfor").select2("search",selectParam);
		});		
    $("#baseInfor #customer").select2({
        placeholder: "选择收货人(客户)",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/salemanage/getSelectCusom",
            type: "POST",
            dataType: 'json',
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }

    });
if($("#saleId").val()!=""){

 $("#baseInfor #customer").select2("data", {
   id: $("#customerId").val(),
   text: $("#customerName").val()
  });
}else{
  $('#customer').val("")
}
 //清空物料多选框中的值
 function removeChoice(){
	  $("#s2id_materialInfor .select2-choices").children(".select2-search-choice").remove();
	  $("#materialInfor").select2("val","");
	  selectData = 0;	  
 }
//添加物料详情
 function addDetail(){
	  if($("#saleId").val()==""){
		  layer.alert("请先保存表单信息！");
		  return;
	  }
	  if(selectData!=0){
		  //将选中的物料添加到数据库中
		  $.ajax({
			  type:"POST",
			  url:context_path + '/salemanage/saveDetail',
			  data:{saleId:$('#baseInfor #saleId').val(),materialIds:selectData.toString()},
			  dataType:"json",
			  success:function(data){
				  removeChoice();   //清空下拉框中的值
				  if(Boolean(data.result)){
						layer.msg("添加成功",{icon:1,time:1200});
					  //重新加载详情表格
					 reloadDetailTableList();
				  }else{
					  layer.msg(data.msg,{icon:2,time:1200});
				  }
			  }
		  });
	  }else{
		  layer.alert("请选择物料！");
	  }
  }
  //工具栏
	  $("#__toolbar__-c").iToolBar({
	   	 id:"__tb__01",
	   	 items:[
	   	  	{label:"删除", onclick:delDetail},
	    ]
	  });
	//删除物料详情
	function delDetail(){
	   var ids = jQuery("#grid-table-c").jqGrid('getGridParam', 'selarrrow');
	   $.ajax({
	      url:context_path + '/salemanage/deleteDetail?ids='+ids,
	      type:"POST",
	      dataType:"JSON",
	      success:function(data){
	         if(data.result){
	           layer.msg("操作成功！");
	            //重新加载详情表格
				reloadDetailTableList();
	         }
	      }
	   });
	}
	 function reloadDetailTableList(){
       $("#grid-table-c").jqGrid('setGridParam', 
				{
  	    			url:context_path + '/salemanage/detailList?saleId='+saleId,
					postData: {queryJsonString:""} //发送数据  :选中的节点
				}).trigger("reloadGrid");
      } 
</script>