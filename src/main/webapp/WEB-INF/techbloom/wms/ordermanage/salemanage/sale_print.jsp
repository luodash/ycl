<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="com.tbl.modules.wms.entity.ordermanage.SaleDetail" %>
<%@ page import="com.tbl.modules.wms.entity.ordermanage.SaleOrder" %>
<%
	String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

    List<SaleOrder> typeList =  (List<SaleOrder>)request.getAttribute("saleList");
    //String userName = (String)request.getAttribute("userName");
    String enterpriseName = "";
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    String date = sdf.format(new Date());
    int startIndex =0;
    int pageSize = 1000;
    //判断list能打印几个单子
    int num = typeList.size()/pageSize;
    if(typeList.size()%pageSize != 0){
        num = num + 1;
    }
%>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=basePath%>">
    <title>销售订单</title>
    <script type="text/javascript">
        function printA4(){
            window.print();
        }
    </script>
</head>
<body onload="printA4();">
<%
	double allAmount = 0;
    int index = 1;
    for(int a = 0; a < num; a++){
        //最大条数为
        int size = startIndex + pageSize;
        if(a == num -1){
            size = typeList.size();
        }
%>
<div style="height: 295mm;width: 210mm;border:0px solid #000;">
    <h3 align="center" style="font-weight: bolder;">物料详情</h3>
    <div style="height:30px;width:400px;margin:0 auto;font-size: 14px;">
    </div><br/>
    <table style="border-collapse: collapse; border: none; width: 21cm;font-size: 12px;" align="center">
        <tr style="height: 32px">
        	<th style="border: solid #000 1px;text-align:center;font-weight:bold;">序号</th>
        	<th style="border: solid #000 1px;text-align:center;font-weight:bold;">销售订单编号</th>
            <th style="border: solid #000 1px;text-align:center;font-weight:bold;">销售订单名称</th>
            <th style="border: solid #000 1px;text-align:left;font-weight:bold;">物料编号</th>
            <th style="border: solid #000 1px;text-align:left;font-weight:bold;">物料名称</th>
            <th style="border: solid #000 1px;text-align:left;font-weight:bold;">销售日期</th>
            <th style="border: solid #000 1px;text-align:left;font-weight:bold;">收货人</th>
            <th style="border: solid #000 1px;text-align:left;font-weight:bold;">地址</th>
            <th style="border: solid #000 1px;text-align:left;font-weight:bold;">联系方式</th>
        </tr>
        <%
        	if(typeList != null && typeList.size() > 0){
                        for(int i= 0; i < size; i++){
                        	SaleOrder sale  = (SaleOrder)typeList.get(i);
        %>
        <tr style="height: 22px">
            <td style="border: solid #000 1px;text-align:center;">
                <%=i+1 %>
            </td>
            <td style="border: solid #000 1px;text-align:left;">
                <%=sale.getSaleNo() %>
            </td>
            <td style="border: solid #000 1px;text-align:left">
                <%=sale.getSaleName() %>
            </td>
            <td style="border: solid #000 1px;text-align:center;">
                <%=sale.getMaterialNo() %>
            </td>
            <td style="border: solid #000 1px;text-align:center;">
                <%=sale.getMaterialName() %>
            </td>
            <td style="border: solid #000 1px;text-align:center;">
                <%=sale.getSaleDate() %>
            </td>
            <td style="border: solid #000 1px;text-align:center;">
                <%=sale.getCustomerName() %>
            </td>
            <td style="border: solid #000 1px;text-align:center;">
                <%=sale.getAddress() %>
            </td>
            <td style="border: solid #000 1px;text-align:center;">
                <%=sale.getContact() %>
            </td>
        </tr>
        <%
                }
            }
        %>
    </table>
    <br/>
    <table style="border-collapse: collapse; border: none; width: 21cm;font-size: 14px;" align="center">
        <%-- <tr>
            <td style="text-align:center;">单位：<%=enterpriseName %></td>
            <td style="text-align:center;">制表：<%=userName %></td>
            <td style="text-align:center;">打印日期：<%=date %></td>
        </tr> --%>
    </table>
</div>
<div style="font-size: 14px;margin-bottom:0px;text-align: center;">第<%=index++ %>页</div>
<%
        startIndex += pageSize;
    }
%>
</body>
</html>

