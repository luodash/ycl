<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"+ request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
	   var context_path = '<%=path%>';
</script>
<style type="text/css">
.query_box .field-button.two {
	padding: 0px;
	left: 650px;
}
</style>
<div id="salemanage_list_grid-div">
	<!-- 隐藏区域：存放查询条件 -->
	<form id="salemanage_list_hiddenQueryForm" action="<%=path%>/salemanage/exportExcel" style="display:none;">
		<!-- 销售订单编号 -->
		<input id="salemanage_list_saleNo" name="saleNo" value="" />
		<!-- 销售订单名称-->
		<input id="salemanage_list_saleName" name="saleName" value="" />
		<!-- 客户 -->
		<input id="salemanage_list_customer" name="customer" value="" />
		<!-- 地址 -->
		<input id="salemanage_list_address" name="address" value="" />
		<!-- 销售日期 -->
		<input id="salemanage_list_saleDate" name="saleDate" value="" />
		<!-- 订单类型 -->
		<input id="salemanage_list_order_type" name="order_type" value="" />
		<!-- 订单状态 -->
		<input id="salemanage_list_order_status" name="order_status" value="" />
		<!-- 库区 -->
		<input id="salemanage_list_warehouse_area" name="warehouse_area" value="" />
		<!-- 订单数量 -->
		<input id="salemanage_list_order_num" name="order_num" value="" />
		<!-- 计划发货时间(开始) -->
		<input id="salemanage_list_order_p_stime" name="order_p_stime" value="" />
		<!-- 计划发货时间(结束) -->
		<input id="salemanage_list_order_p_etime" name="order_p_etime" value="" />
	</form>
	<div class="query_box" id="salemanage_list_yy" title="查询选项">
		<form id="salemanage_list_queryForm" style="max-width:100%;">
			<ul class="form-elements">
				<li class="field-group field-fluid3">
				    <label class="inline" for="salemanage_list_saleNo" style="margin-right:20px;width:100%;"> 
				          <span class="form_label" style="width:80px;">订单编号：</span> 
				          <input type="text" name="saleNo" id="salemanage_list_saleNo" value="" style="width: calc(100% - 85px);" placeholder="订单编号" />
				    </label>
				</li>
				<li class="field-group field-fluid3">
				    <label class="inline" for="salemanage_list_saleName" style="margin-right:20px;width:100%;"> 
					    <span class="form_label" style="width:80px;">订单名称：</span> 
					    <input type="text" name="saleName" id="salemanage_list_saleName" value="" style="width: calc(100% - 85px);" placeholder="订单名称" />
				    </label>
				</li>
				<li class="field-group field-fluid3">
				    <label class="inline" for="salemanage_list_customer" style="margin-right:20px;width:100%;"> 
					    <span class="form_label" style="width:80px;">客户：</span> 
					    <input type="text" name="customer" id="salemanage_list_customer" value="" style="width: calc(100% - 85px);" placeholder="客户" />
				    </label>
				</li>
				<li class="field-group-top field-group field-fluid3">
				    <label class="inline" for="salemanage_list_address" style="margin-right:20px;width:100%;"> 
				        <span class="form_label" style="width:80px;">收货地址：</span> 
				        <input type="text" name="address" id="salemanage_list_address" value="" style="width: calc(100% - 85px);" placeholder="地址" />
				    </label>
				</li>
				<li class="field-group field-fluid3">
				    <label class="inline" for="salemanage_list_saleDate" style="margin-right:20px;width:100%;"> 
				        <span class="form_label" style="width:80px;">日期：</span> 
				        <input type="text" class="form-control date-picker" id="salemanage_list_saleDate" name="saleDate" value="" style="width: calc(100% - 85px);" placeholder="日期" />
				    </label>
				</li>
				<li class="field-group field-fluid3">
				    <label class="inline" for="salemanage_list_order_type" style="margin-right:20px;width:100%;"> 
				        <span class="form_label" style="width:80px;">订单类型：</span> 
				        <select id="salemanage_list_order_type" name='order_type' style="width: calc(100% - 85px);">
				              <option value=''>--请选择--</option>
				              <option value='0'>生产订单生成</option>
				              <option value='1'>销售订单生成</option>
				              <option value='2'>手动添加</option>
				        </select>
				    </label>
				</li>
				<li class="field-group-top field-group field-fluid3">
				    <label class="inline" for="salemanage_list_order_status" style="margin-right:20px;width:100%;"> 
				        <span class="form_label" style="width:80px;">订单状态：</span> 
				         <select id="salemanage_list_order_status" name='order_status' style="width: calc(100% - 85px);">
				              <option value=''>--请选择--</option>
				              <option value='0'>未审核</option>
				              <option value='1'>已审核</option>
				              <option value='2'>计划中</option>
				              <option value='3'>出库完成</option>
				              <option value='4'>已锁定</option>
				        </select>
				    </label>
				</li>
				<li class="field-group field-fluid3">
				    <label class="inline" for="salemanage_list_warehouse_area" style="margin-right:20px;width:100%;"> 
				        <span class="form_label" style="width:80px;">库区：</span> 
				        <input id="salemanage_list_warehouse_area" name="warehouse_area" style="width: calc(100% - 85px);" type="text" placeholder="请选择库区"/>
				    </label>
				</li>
				<li class="field-group field-fluid3">
				    <label class="inline" for="salemanage_list_order_num" style="margin-right:20px;width:100%;"> 
				        <span class="form_label" style="width:80px;">订单数量：</span> 
				        <input type="text"  id="salemanage_list_order_num" name="order_num" value="" style="width: calc(100% - 85px);" placeholder="订单数量" />
				    </label>
				</li>
				<li class="field-group-top field-group field-fluid4">    
				     <label class="inline" for="salemanage_list_order_p_stime" style="margin-right:20px;width:100%;"> 
				     	 <span class="form_label" style="width:120px;">计划发货时间：</span>
				         <input type="text" class="form-control date-picker" id="salemanage_list_order_p_stime" 
				         name="order_p_stime" value="" style="width:150px;" placeholder="计划发货开始时间" />
				         &nbsp;&nbsp;~&nbsp;&nbsp;
				         <input type="text" class="form-control date-picker" id="salemanage_list_order_p_etime" 
				         name="order_p_etime" value="" style="width:150px;" placeholder="计划发货结束时间" />
				     </label>
				</li>
			</ul>
			<div class="field-button">
				<div class="btn btn-info" onclick="queryOk();">
					<i class="ace-icon fa fa-check bigger-110"></i>查询
				</div>
				<div class="btn" onclick="reset();">
					<i class="ace-icon icon-remove"></i>重置
				</div>
				<a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起
					<i class="fa fa-angle-up"></i>
				</a>
			</div>
		</form>
	</div>
	<div id="salemanage_list_fixed_tool_div" class="fixed_tool_div">
		<div id="salemanage_list___toolbar__" style="float:left;overflow:hidden;"></div>
	</div>
	<table id="grid-table" style="width:100%;height:100%;"></table>
	<div id="grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript" src="<%=path%>/static/js/techbloom/wms/ordermanage/salemanage/saleOrder.js"></script>
<script type="text/javascript">
	var context_path = '<%=path%>';
    var oriData; 
    var _grid;
    var dynamicDefalutValue="0faac5b4a5174cd5b1ae999da88fd1d1";
    $(function(){
       $(".toggle_tools").click();
    });
    $("#salemanage_list___toolbar__").iToolBar({
   	  	 id:"salemanage_list___tb__01",
   	  	 items:[
   	  	    {label:"添加",disabled:(${sessionUser.addQx}==1?false:true),onclick:addAllocateOrder,iconClass:'glyphicon glyphicon-plus'},
   	  	    {label:"编辑",disabled:(${sessionUser.editQx}==1?false:true),onclick:editAllocateOrder,iconClass:'glyphicon glyphicon-pencil'},
	   	  	{label:"删除",disabled:(${sessionUser.deleteQx}==1?false:true),onclick:delOutStorageOrder,iconClass:'glyphicon glyphicon-trash'},
	   	  	{label:"查看", disabled:(${sessionUser.queryQx} == 1 ? false : true),onclick:viewDetail, iconClass:'icon-zoom-in'},
	   	  	{label:"审核",disabled:(${sessionUser.queryQx}==1?false:true),onclick:auditOrder,iconClass:'icon-ok-circle'},
	   	  	{label:"出库计划手动生成",disabled:(${sessionUser.deleteQx}==1?false:true),onclick:outPlan,iconClass:'glyphicon glyphicon-pencil'},
	   	  	{label:"出库计划自动生成",disabled:(${sessionUser.deleteQx}==1?false:true),onclick:outPlanAuto,iconClass:'glyphicon glyphicon-pencil'},
	   	  	{label:"导出", disabled:(${sessionUser.queryQx} == 1 ? false : true),onclick:function () {$("#salemanage_list_hiddenQueryForm").submit();},iconClass:' icon-share'},
			{label:"打印", disabled:(${sessionUser.queryQx} == 1 ? false : true),onclick:function(){	
		  	  	var queryBean = iTsai.form.serialize($('#salemanage_list_hiddenQueryForm'));   //获取form中的值：json对象
	            var queryJsonString = JSON.stringify(queryBean); 
	            var url = context_path + "/salemanage/exportPrint?"+"id=" + 1;
	            window.open(url);},iconClass:' icon-print'},
			{label:"锁定",disabled:(${sessionUser.queryQx}==1?false:true),onclick:checkLock,iconClass:'icon-ok-circle'}	   	  	
			   ]
			 });   
   		$(function(){
		_grid = jQuery("#salemanage_list_grid-table").jqGrid({
	       url : context_path + "/salemanage/list.do",
	       datatype : "json",
	       colNames : [ "销售单主键","状态1","销售订单编号","销售订单名称","销售日期", "客户(收货人)","地址","联系方式","备注","状态"],
	       colModel : [ 
	                    {name : "id",index : "id",width : 20,hidden:true}, 
	                    {name : "state1",index : "state1",width : 20,hidden:true}, 
	                    {name : "saleNo",index : "saleNo",width : 40}, 
	                    {name : "saleName",index : "saleName",width : 40}, 
	                    {name : "saleDate",index : "saleDate",width : 40}, 
	                    {name : "customerName",index : "customerName",width : 40}, 
	                    {name : "address",index : "address",width : 40}, 
	                    {name : "contact",index : "contact",width : 40}, 
	                    {name : "remark",index : "REMARK",width : 40}, 
	                    {name : "state",index : "state",width : 40,
	                       formatter:function(cellValue,option,rowObject){
	                    		if(typeof cellValue == "number"){
	                    			if(cellValue==0){
	                    				return "<span style='color:#d15b47;font-weight:bold;'>未审核</span>";
	                    			}else if(cellValue==1){
	                    				return "<span style='color:#76b86b;font-weight:bold;'>已审核</span>";
	                    			}else if(cellValue==2){
	                    				return "<span style='color:#76b86b;font-weight:bold;'>计划中</span>";
	                    			}else if(cellValue==3){
	                    				return "<span style='color:#76b86b;font-weight:bold;'>出库完成</span>";
	                    			}else if(cellValue==4){
	                    				return "<span style='color:#76b86b;font-weight:bold;'>已锁定</span>";
	                    			}
	                    		}else{
	                    			return "异常";
	                    		}
	                    	}
	                    } 
	                  ],
	       rowNum : 20,
	       rowList : [ 10, 20, 30,40,50,70,100 ],
	       pager : "#salemanage_list_grid-pager",
	       sortname : "id",
	       sortorder : "desc",
	       altRows: false, 
	       viewrecords : true,
	       autowidth:true,
	       multiselect:true,
		   multiboxonly: true,
           beforeRequest:function (){
                dynamicGetColumns(dynamicDefalutValue,"salemanage_list_grid-table",$(window).width()-$("#sidebar").width() -7);
                //重新加载列属性
           },
	       loadComplete : function(data){
	           var table = this;
	           setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
	           oriData = data;
	       },
	       emptyrecords: "没有相关记录",
	       loadtext: "加载中...",
	       pgtext : "页码 {0} / {1}页",
	       recordtext: "显示 {0} - {1}共{2}条数据"
	  });
	 //在分页工具栏中添加按钮
	  jQuery("#salemanage_list_grid-table").navGrid("#salemanage_list_grid-pager",{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#salemanage_list_grid-pager",{  
		   caption:"",   
		   buttonicon:"ace-icon fa fa-refresh green",   
		   onClickButton: function(){
			   $("#salemanage_list_grid-table").jqGrid("setGridParam", 
						{
							postData: {queryJsonString:""} //发送数据 
						}
				  ).trigger("reloadGrid");
		   }
		}).navButtonAdd("#salemanage_list_grid-pager",{
              caption: "",
              buttonicon:"fa icon-cogs",
              onClickButton : function (){
                  jQuery("#salemanage_list_grid-table").jqGrid("columnChooser",{
                      done: function(perm, cols){
                          dynamicColumns(cols,dynamicDefalutValue);
                          $("#salemanage_list_grid-table").jqGrid("setGridWidth", $("#salemanage_list_grid-div").width()-3);
                      }
                  });
              }
          });
	  $(window).on("resize.jqGrid", function () {
		  $("#salemanage_list_grid-table").jqGrid("setGridWidth", $("#salemanage_list_grid-div").width()-3);
		  var height = $("#breadcrumb").outerHeight(true)+$("#salemanage_list_yy").outerHeight(true)+
                       $("#salemanage_list_fixed_tool_div").outerHeight(true)+
                       $("#gview_salemanage_list_grid-table .ui-jqgrid-hbox").outerHeight(true)+
                       $("#salemanage_list_grid-pager").outerHeight(true)+$("#header").outerHeight(true);
                       $("#salemanage_list_grid-table").jqGrid("setGridHeight", (document.documentElement.clientHeight)-height );
	  });	
	  $(window).triggerHandler("resize.jqGrid");
    }); 
   	var _queryForm_data = iTsai.form.serialize($("#salemanage_list_queryForm"));
	function queryOk(){
		var queryParam = iTsai.form.serialize($("#salemanage_list_queryForm"));
		queryInstoreListByParam(queryParam);		
	}	
	function reset(){
		iTsai.form.deserialize($("#salemanage_list_queryForm"),_queryForm_data); 
		$("#salemanage_list_allocateTypeSelect").val("").trigger("change");
		$("#salemanage_list_inWarehouseSelect").val("").trigger("change");
		$("#salemanage_list_outWarehouseSelect").val("").trigger("change");
		$("#salemanage_list_queryForm #salemanage_list_saleNo").val("").trigger("change");
		$("#salemanage_list_queryForm #salemanage_list_saleName").val("").trigger("change");
		$("#salemanage_list_queryForm #salemanage_list_customer").select2("val","");
		$("#salemanage_list_queryForm #salemanage_list_address").val("");
		$("#salemanage_list_queryForm #salemanage_list_saleDate").val("");
		
		$("#salemanage_list_queryForm #salemanage_list_order_type").val("").trigger("change");
		$("#salemanage_list_queryForm #salemanage_list_order_status").val("").trigger("change");
		$("#salemanage_list_queryForm #salemanage_list_warehouse_area").val("").trigger("change");
		
		$("#salemanage_list_queryForm #salemanage_list_order_num").val("");
		$("#salemanage_list_queryForm #salemanage_list_order_p_stime").val("");
		$("#salemanage_list_queryForm #salemanage_list_order_p_etime").val("");
		
		
		queryInstoreListByParam(_queryForm_data);
		$("#salemanage_list_allocateTypeSelect").find("option[text='所有类型']").attr("selected",true);
		$("#salemanage_list_outWarehouseSelect").find("option[text='所有库区']").attr("selected",true);
		$("#salemanage_list_inWarehouseSelect").find("option[text='所有库区']").attr("selected",true);
	}
 $("#salemanage_list_queryForm .mySelect2").select2();
 $(".date-picker").datetimepicker({format:"YYYY-MM-DD",useMinutes:true,useSeconds:true});
 function reloadGrid(){
     _grid.trigger("reloadGrid");
 }
 $("#salemanage_list_queryForm #salemanage_list_customer").select2({
        placeholder: "选择客户",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/salemanage/getSelectCusom",
            type: "POST",
            dataType: "json",
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });
    
    $("#salemanage_list_queryForm #salemanage_list_warehouse_area").select2({
        placeholder: "请选择库区",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/outStorage/getSelectArea",
            type: "POST",
            dataType: "json",
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });
    $("#salemanage_list_queryForm #salemanage_list_customerId").select2({
        placeholder: "请选择库区",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/outStorage/getSelectCusom",
            type: "POST",
            dataType: "json",
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });	
    function viewDetail() {
    var checkedNum = getGridCheckedNum("#salemanage_list_grid-table", "id");
	if (checkedNum == 0) {
		layer.alert("请选择一条要查看的成品订单！");
		return false;
	} else if (checkedNum > 1) {
		layer.alert("只能选择一条成品订单进行查看！");
		return false;
	} else {
	     var id = jQuery("#salemanage_list_grid-table").jqGrid("getGridParam","selrow");
	     $.get(context_path + "/salemanage/viewDetail?saleId=" + id).done(
			function(data) {
				layer.open({
					title : "质检单单查看",
					type : 1,
					skin : "layui-layer-molv",
					area : [ "750px", "600px" ],
					shade : 0.6, // 遮罩透明度
					moveType : 1, // 拖拽风格，0是默认，1是传统拖动
					anim : 2,
					content : data
				});
			});	
	    }	
     }
</script>