<%@ page language="java" import="java.lang.*"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
%>
<div id = "produce_edit_page" class="row-fluid" style="border:0px;">
    <form id="produce_order_split_baseInfor" class="form-horizontal" method="post" target="_ifr" style="border-bottom: solid 2px #3b73af;">
        <!-- 隐藏的入库单主键 -->
        <input type="hidden" id="produce_order_split_id" name="id" value="${pId }" />
        <div class="row" style="margin:0;padding:0;">
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="produce_order_split_orderCode" >订单编号：</label>
                <div class="controls">
                    <div class="" >
                        <input type="text" id="produce_order_split_orderCode" class="span10" name="orderCode" readonly value="${poo.orderCode }"  placeholder="后台自动生成"  />
                    </div>
                </div>
            </div>
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="produce_order_split_orderPlanTime">计划时间：</label>
                <div class="controls">
                    <div class="spa10">
                        <input class="form-control date-picker" id="produce_order_split_orderPlanTime" name="orderPlanTime" type="text" value="${fn:substring(poo.orderPlanTime,0,19)}" placeholder="计划时间" class="span8"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin:0;padding:0;">
            <div class="control-group span6">
                <label class="control-label" for="produce_order_split_saleId">销售单号：</label>
                <div class="controls">
                    <div class="" style=" float: none !important;">
                        <input type="hidden" id="produce_order_split_saleId" name="saleId" value="${poo.saleId }">
                        <select class="span10 mySelect2" id="produce_order_split_instorageTypeSelect" name="instorageTypeSelect" data-placeholder="请销售单号" style="margin-left:0px;">
                            <option value=""></option>
                            <c:forEach items="${goodsList}" var="in">
                                <option value="${in.id}" <c:if test="${poo.saleId==in.id }">selected="selected"</c:if>>
                                        ${in.saleNo}
                                </option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- 表格div -->
    <div id="produce_order_split_grid-div-d" style="width:100%;margin:0px auto;">
        <!-- 	表格工具栏 -->
        <div id="produce_order_split_fixed_tool_div" class="fixed_tool_div detailToolBar">
            <div id="produce_order_split___toolbar__-d" style="float:left;overflow:hidden;"></div>
        </div>
        <!-- 入库单信息表格 -->
        <table id="produce_order_split_grid-table-d" style="width:100%;height:100%;"></table>
        <!-- 表格分页栏 -->
        <div id="produce_order_split_grid-pager-d"></div>
    </div>
</div>
<iframe src="about:blank" name="_ifr" height="0" class="hidden"></iframe>
<script type="text/javascript" src="<%=path%>/static/js/techbloom/wms/ordermanage/produceorder/produceorder_detail.js"></script>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriData;      //表格数据
    var _grid;        //表格对象

    $(".date-picker").datetimepicker({format: 'YYYY-MM-DD HH:mm:ss',useMinutes:true,useSeconds:true});
    $('.mySelect2').select2();
    $('#produce_order_split_baseInfor #produce_order_split_instorageTypeSelect').change(function(){
        $('#produce_order_split_baseInfor #produce_order_split_saleId').val($('#produce_order_split_baseInfor #produce_order_split_instorageTypeSelect').val());
    });

    //工具栏
    $("#produce_order_split___toolbar__-d").iToolBar({
        id:"produce_order_split___tb__01",
        items:[
            {label:"确认生成", onclick:queryOk},
        ]
    });
    //初始化表格
    _grid = $("#produce_order_split_grid-table-d").jqGrid({
        url : context_path + '/produceOrder/getSplitDetail?pId='+$("#produce_order_split_baseInfor #produce_order_split_id").val(),
        datatype : "json",
        colNames : [ '物料主键','产品编号','产品名称','物料数量','产品剩余数量','出库计划数量'],
        colModel : [
            {name : 'id',index : 'id',width : 55,hidden:true},
            {name : 'materialNo',width : 30},
            {name : 'materialName',width : 30},
            {name : 'amount',width : 30},
            {name : 'restAmount',width : 20},
            {name : 'lastAmount', width: 20, editable: true, editrules: {custom: true, custom_func: myamountcheck},
                editoptions: {
                    size: 25, dataEvents: [{
                        type: 'blur',
                        fn: function (e) {
                            var $element = e.currentTarget;
                            var $elementId = $element.id;
                            var rowid = $elementId.split("_")[0];
                            var reg = new RegExp("^[0-9]+(.[0-9]{1,2})?$");
                            if (!reg.test($("#" + $elementId).val())) {
                                layer.alert("非法的数量！(注：可以有两位小数的正实数)");
                                return;
                            }
                            var id = $("#produce_order_split_grid-table-d").jqGrid('getGridParam', 'selrow');
                            var rowData = $("#produce_order_split_grid-table-d").jqGrid('getRowData', id).restAmount;
                            $.ajax({
                                type: "POST",
                                dataType: "json",
                                url: context_path + '/produceOrder/updateAmount',
                                data: {lastAmount: $("#" + rowid + "_lastAmount").val(), amount: rowData, id: id},
                                success: function (data) {
                                    if (!data.result) layer.alert(data.msg);
                                }
                            })
                        }
                    }]
                }
            }
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : '#produce_order_split_grid-pager-d',
        sortname : 'pod.id',
        sortorder : "asc",
        altRows: true,
        viewrecords : true,
        autowidth:true,
        multiselect:true,
        multiboxonly: true,
        loadComplete : function(data){
            var table = this;
            setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            oriData = data;
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据",
        cellEdit: true,
        cellsubmit : "clientArray",
    });
    //在分页工具栏中添加按钮
    $("#produce_order_split_grid-table-c").navGrid("#produce_order_split_grid-pager-d",{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd('#produce_order_split_grid-pager-d',{
            caption:"",
            buttonicon:"ace-icon fa fa-refresh green",
            onClickButton: function(){
                $("#produce_order_split_grid-table-d").jqGrid("setGridParam",
                    {
                        postData: {pId:$("#produce_order_split_baseInfor #produce_order_split_id").val()} //发送数据  :选中的节点
                    }
                ).trigger("reloadGrid");
            }
        });
    $(window).on("resize.jqGrid", function () {
        $("#produce_order_split_grid-table-d").jqGrid("setGridWidth", $("#produce_order_split_grid-div-d").width() - 3 );
        var height = $("#produce_order_split_grid-pager-d").outerHeight(true)+
                     $(".layui-layer-title",_grid.parents(".layui-layer")).height()+
                     $("#produce_order_split_baseInfor").outerHeight(true)+
                     $("#produce_order_split_fixed_tool_div.fixed_tool_div.detailToolBar").outerHeight(true)+
                     $("#gview_produce_order_split_grid-table-d .ui-jqgrid-hbox").outerHeight(true);
                     $("#produce_order_split_grid-table-d").jqGrid("setGridHeight", _grid.parents(".layui-layer").height()-height);
    });
    $(window).triggerHandler("resize.jqGrid");
    if($("#produce_order_split_id").val()!=""){
        reloadDetailTableList();   //重新加载详情列表
    }
    function myamountcheck(value, colname) {
        var reg = new RegExp("^[0-9]+(.[0-9]{1,2})?$");
        if (!reg.test(value)) {
            var a = layer.alert("非法的数量！(注：可以有两位小数的正实数)");
            return [ false,  ""];
        }else {
            return [ true, "" ];
        }
    }
    //关闭当前窗口
    function closeWindowBtn(){
        layer.closeAll();
    }
</script>