<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tbl.modules.wms.entity.ordermanage.ProduceOrder"%>
<%@page import="com.tbl.modules.wms.entity.ordermanage.ProduceOrderDetail"%>
<%@page import="com.mysql.jdbc.StringUtils" %>
<%@ page import="com.tbl.modules.wms.entity.ordermanage.ProduceOrder" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
    List<ProduceOrder> instorageList =(List<ProduceOrder>)request.getAttribute("list");
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">

    <title>生产订单打印</title>

    <script type="text/javascript">
        function aa(){
            window.print();
        }
    </script>
</head>
<body onload="aa();">
<%
    double allAmount = 0;
    if(instorageList != null && instorageList.size()>0){
        for(int b = 0;b <instorageList.size(); b++){
%>
<br/>
<h3 align="center" style="font-weight: bolder;padding-top: 5px;">生产订单</h3>
<table style="width: 20.2cm;font-size: 12px;" align="center">
    <tr>
        <td>生产编号：<%=instorageList.get(b).getOrderCode()==null?"":instorageList.get(b).getOrderCode()%></td>
        <td>创建日期：<%=instorageList.get(b).getOrderCreatTime1()==null?"":instorageList.get(b).getOrderCreatTime1() %></td>
    </tr>
</table>
<!--table的最大高度为height:21cm;  -->
<table style="border-collapse: collapse; border: none;width: 20cm;font-size: 12px;" align="center">
    <tr>
        <td style="border: solid #000 1px;width:2cm;">序号</td>
        <td style="border: solid #000 1px;width:2cm;">物料编号</td>
        <td style="border: solid #000 1px;width:50cm;">物料名称</td>
        <td style="border: solid #000 1px;width:2cm;">数量</td>
    </tr>
    <%
        int len = instorageList.get(b).getDetailList().size();
        int s = 4 - len;
        double yhj = 0;
        double jeyhj = 0;
        for(int i= 0;i< len ;i++){
            List<ProduceOrderDetail> dail = instorageList.get(b).getDetailList();
            //页合计数量
            yhj =  yhj + Double.parseDouble(dail.get(i).getDetailAmount()==null?"0":dail.get(i).getDetailAmount());

    %>
    <tr >
        <td style="border: solid #000 1px;"><%=i+1 %></td>
        <td style="border: solid #000 1px;"><%=dail.get(i).getMaterialNo()==null?"":dail.get(i).getMaterialNo() %></td>
        <td style="border: solid #000 1px;"><%=dail.get(i).getMaterialName()==null?"":dail.get(i).getMaterialName() %></td>
        <td style="border: solid #000 1px;"><%=dail.get(i).getDetailAmount()==null?"":dail.get(i).getDetailAmount() %></td>
    </tr>
    <%
        }
        for(int i= 0;i< s ;i++){
    %>
    <tr >
        <td style="border: solid #000 1px;">&nbsp;</td>
        <td style="border: solid #000 1px;">&nbsp;</td>
        <td style="border: solid #000 1px;">&nbsp;</td>
        <td style="border: solid #000 1px;">&nbsp;</td>

    </tr>
    <%
        }
        yhj = Math.round(yhj * 10000) / 10000.0;
        jeyhj = Math.round(jeyhj * 10000) / 10000.0;

        double allamount = Double.parseDouble(instorageList.get(b).getAllAmount()==null?"0":instorageList.get(b).getAllAmount());
        allamount = Math.round(allamount * 10000) / 10000.0;
    %>
    <tr >
        <td style="border: solid #000 1px;white-space:nowrap;">页合计：</td>
        <td style="border: solid #000 1px;white-space:nowrap;"></td>
        <td style="border: solid #000 1px;white-space:nowrap;"></td>
        <td style="border: solid #000 1px;white-space:nowrap;"><%=yhj %></td>

    </tr>
    <tr >
        <td style="border: solid #000 1px;white-space:nowrap;">合计：</td>
        <td style="border: solid #000 1px;white-space:nowrap;"></td>
        <td style="border: solid #000 1px;white-space:nowrap;"></td>
        <td style="border: solid #000 1px;white-space:nowrap;"><%=allamount %></td>
    </tr>
</table>
<table style="border-collapse: collapse; border: none;width: 20.2cm;font-size: 12px;margin-bottom: 10px;" align="center">
</table>
<%

%>
<%
    if (1 <instorageList.size()&&b!=instorageList.size()-1){
%>
<div id="xx" style="height:80px;"></div>
<%
            }
        }
    }
%>
</body>
</html>

