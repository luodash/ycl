<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<div id="generate_task_page" class="row-fluid" style="height: inherit;">
	<form id="exceptioncase_check_task_taskForm" class="form-horizontal" style="overflow: auto; height: calc(100% - 70px);">
		<input type="hidden" id="exceptioncase_check_task_ids" name="ids" value="${ids }">
		<div class="control-group">
			<label class="control-label" for="bomDetailNo">库位数：</label>
			<div class="controls">
				<input type="text" id="exceptioncase_check_task_locationNum" name="locationNum" style="width: 300px;" placeholder="请输入单个盘点任务需要盘点的库位数量" />
			</div>
		</div>
	</form>
	<div class="field-button" style="text-align: center;border-top: 0px;margin: 15px auto;">
		<span class="btn btn-info" onclick="taskOk();">
		   <i class="ace-icon fa fa-check bigger-110"></i>确定
		</span>
		<span class="btn btn-danger" onclick="layer.close($queryWindow);return false;">
		   <i class="icon-remove"></i>&nbsp;取消
		</span>
	</div>
</div>
<script type="text/javascript">
var context_path= '<%=path%>';
$("#exceptioncase_check_task_taskForm").validate({
   rules:{
      "exceptioncase_check_task_locationNum":{
         required:true,
         digits:true
      }
   },
   messages: {
      "exceptioncase_check_task_locationNum":{
         required:"请输入库位数！",
         digits:"只能输入正整数！"
      }
   },
   errorClass: "help-inline",
   errorElement: "span",
   highlight:function(element, errorClass, validClass) {
          $(element).parents('.control-group').addClass('error');
	 },
  unhighlight: function(element, errorClass, validClass) {
		  $(element).parents('.control-group').removeClass('error');
   }
});
function taskOk(){
   if($("#exceptioncase_check_task_taskForm").valid()) createTask();
}
function createTask(){
$.ajax({
	 type:"POST",
	 url:context_path + "/exceptioncase/generateTask.do",
	 data:{
		 ids:$("#exceptioncase_check_task_taskForm #exceptioncase_check_task_ids").val(),
		 locationNum:$("#exceptioncase_check_task_taskForm #exceptioncase_check_task_locationNum").val()
	 },
	 dataType:"json",
	 success:function(data){
	     if(Boolean(data.result)){
	         layer.msg("生成盘点任务成功！",{icon:1});
	         $("#exceptioncase_check_task_grid-table").jqGrid("setGridParam", {
                postData: {queryJsonString:""}
             }).trigger("reloadGrid");
             layer.close($queryWindow);
	     }else{
	         layer.msg(data.msg,{icon:2});
	     }			  
	 }
  });
}
</script>