<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<script type="text/javascript" src="<%=path%>/static/js/techbloom/wms/baseinfo/exceptioncase_list.js"></script>
<div id="exceptioncase_list_grid-div">
     <form id="exceptioncase_list_hiddenForm" action="<%=path%>/exceptioncase/toExcel" method="POST" style="display: none;">
           <input id="exceptioncase_list_ids" name="ids" value=""/>
     </form>
     <form id="exceptioncase_list_hiddenQueryForm" style="display:none;">
           <input id="exceptioncase_list_occurtime1" name="occurtime1" value="" />
           <input id="exceptioncase_list_endTime" name="endTime" value="" />
           <input id="exceptioncase_list_type" name="type" value="" />
           <input id="exceptioncase_list_materialId" name="materialId" value="" />
           <input id="exceptioncase_list_goodsLocation" name="goodsLocation" value="" />
           <input id="exceptioncase_list_orderNo" name="orderNo" value="" />
     </form>
     <div class="query_box" id="exceptioncase_list_yy" title="查询选项">
          <form id="exceptioncase_list_queryForm" style="max-width:100%;">
			    <ul class="form-elements">
			        <li class="field-group field-fluid3">
					    <label class="inline" for="exceptioncase_list_occurtime1" style="margin-right:20px;width:100%;">
						       <span class="form_label" style="width:65px;">开始时间：</span>
						       <input class="form-control date-picker" id="exceptioncase_list_occurtime1" name="occurtime1" type="text" placeholder="开始时间" style="width: calc(100% - 70px);"/> 
					    </label>			
				    </li>
				    <li class="field-group field-fluid3">
					    <label class="inline" for="exceptioncase_list_endTime" style="margin-right:20px;width:100%;">
						       <span class="form_label" style="width:65px;">结束时间：</span>
						       <input class="form-control date-picker" id="exceptioncase_list_endTime" name="endTime" type="text" placeholder="结束时间" style="width: calc(100% - 70px);"/> 
					    </label>			
				    </li>				    
				    <li class="field-group field-fluid3">
					    <label class="inline" for="exceptioncase_list_type" style="margin-right:20px;width:100%;">
						       <span class="form_label" style="width:65px;">单据类型：</span>
						       <select id="exceptioncase_list_type" name="type" data-placeholder="请选择单据类型" style="width: calc(100% - 70px);">
						           <option value="">所有类型</option>
						           <option value="0">补货</option>
						           <option value="1">拣货</option>
				               </select>
					    </label>					
				    </li>
				    <li class="field-group-top field-group field-fluid3">
					    <label class="inline" for="exceptioncase_list_materialId" style="margin-right:20px;width:100%;">
						       <span class="form_label" style="width:65px;">物料：</span>
						       <input id="exceptioncase_list_materialId" name="materialId" type="text" style="width:  calc(100% - 70px);" />
					    </label>			
				    </li>				    
				    <li class="field-group field-fluid3">
					    <label class="inline" for="exceptioncase_list_goodsLocation" style="margin-right:20px;width:100%;">
						       <span class="form_label" style="width:65px;">库位：</span>
						       <input id="exceptioncase_list_goodsLocation" name="goodsLocation" type="text" style="width:  calc(100% - 70px);" />
					    </label>					
				    </li>
				    <li class="field-group field-fluid3">
					    <label class="inline" for="exceptioncase_list_orderNo" style="margin-right:20px;width:100%;">
						       <span class="form_label" style="width:65px;">单据编号：</span>
						       <input id="exceptioncase_list_orderNo" name="orderNo" type="text" style="width: calc(100% - 70px);" placeholder="单据编号" />
					    </label>			
				    </li>
				   
			    </ul>
			     <div class="field-button">
					     <div class="btn btn-info" onclick="queryOk();">
				              <i class="ace-icon fa fa-check bigger-110"></i>查询
			             </div>
				         <div class="btn" onclick="reset();"><i class="ace-icon icon-remove"></i>重置</div>
				         <a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>
		            </div>
		  </form>		 
    </div>
    <div id="exceptioncase_list_fixed_tool_div" class="fixed_tool_div">
        <div id="exceptioncase_list___toolbar__" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="exceptioncase_list_grid-table" style="width:100%;margin: 0px !important;boder:0px !important"></table>
    <div id="exceptioncase_list_grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
var context_path = '<%=path%>';
var oriData;
var _grid;
var dynamicDefalutValue="f1136d9e8dd542dfbafe42819fdb5e08";
$(".date-picker").datetimepicker({format: "YYYY-MM-DD"});
$(function(){
    $(".toggle_tools").click();
});
$("#exceptioncase_list___toolbar__").iToolBar({
    id: "exceptioncase_list___tb__01",
    items: [
            {label: "生成盘点任务", disabled: ( ${sessionUser.addQx } == 1 ? false : true), onclick:generateTask, iconClass:'glyphicon glyphicon-plus'},
            {label: "重新生成下架任务", disabled: ( ${sessionUser.addQx } == 1 ? false : true), onclick:Restructure, iconClass:'glyphicon glyphicon-plus'}
           ]
});
$("#exceptioncase_list_queryForm #exceptioncase_list_type").select2({
    minimumInputLength:0,
    allowClear:true,
    delay:250,
    formatNoMatches:"没有结果",
	formatSearching:"搜索中...",
	formatAjaxError:"加载出错啦！"
    });
    $("#exceptioncase_list_queryForm #exceptioncase_list_type").on("change.select2",function(){
       $("#exceptioncase_list_queryForm #exceptioncase_list_type").trigger("keyup")}
    );
$(function(){
    _grid = jQuery("#exceptioncase_list_grid-table").jqGrid({
            url: context_path + "/exceptioncase/list.do",
            datatype: "json",

            colNames: ["主键","异常编号","发生时间", "单据类型", "物料编号", "物料名称", "异常库位", "来源单据编号","盘点状态", "状态"],
            colModel: [
                {name: "id", index: "id",hidden: true},
                {name: "exceptionNo", index: "exceptionNo", width: 50},
                {name: "occurtime1", index: "occurtime1", width: 60},
                {name: "typeName", index: "typeName", width: 40},
                {name: "materialNo",index: "materialNo",width:50},
                {name: "materialName",index: "materialName",width:50},
                {name: "locationName", index: "locationName", width: 50},
                {name: "orderNo", index: "orderNo", width: 50}, 
	            {name: "checkStatus",index : "checkStatus",width : 40,formatter:function(cellValue,option,rowObject){
                        if(typeof cellValue == "number"){
                   			if(cellValue==0){
                   				return "<span style='color:grey;font-weight:bold;'>未盘点</span>";
                   			}else if(cellValue==1){
                   				return "<span style='color:blue;font-weight:bold;'>待盘点</span>";
                   			}else if(cellValue==2){
                   				return "<span style='color:green;font-weight:bold;'>已盘点</span>";
                   			}
                   		}else{
                   			return "异常";
                   		}
                   	}
	              },
	              {name: "status",index : "status",width : 40,formatter:function(cellValue,option,rowObject){
                         if(typeof cellValue == "number"){
                  			if(cellValue==0){
                  				return "<span style='color:grey;font-weight:bold;'>未处理</span>";
                  			}else if(cellValue==1){
                  				return "<span style='color:blue;font-weight:bold;'>待下架</span>";
                  			}else if(cellValue==2){
                  				return "<span style='color:green;font-weight:bold;'>处理完成</span>";
                  			}
                  		}else{
                  			return "异常";
                  		}
                  	}
                  }
            ],
            rowNum: 20,
            rowList: [10, 20, 30],
            pager: "#exceptioncase_list_grid-pager",
            sortname: "id",
            sortorder: "desc",
            altRows: true,
            viewrecords: true,
            autowidth: true,
            multiselect: true,
            multiboxonly: true,
			beforeRequest:function (){
				dynamicGetColumns(dynamicDefalutValue,"exceptioncase_list_grid-table",$(window).width()-$("#sidebar").width() -7);
				//重新加载列属性
			},
            loadComplete: function (data) {
                var table = this;
                setTimeout(function () {
                    updatePagerIcons(table);
                    enableTooltips(table);
                }, 0);
                oriData = data;
            },
            emptyrecords: "没有相关记录",
            loadtext: "加载中...",
            pgtext: "页码 {0} / {1}页",
            recordtext: "显示 {0} - {1}共{2}条数据"
        });
        //在分页工具栏中添加按钮
        jQuery("#exceptioncase_list_grid-table").navGrid("#exceptioncase_list_grid-pager", {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false
        }).navButtonAdd("#exceptioncase_list_grid-pager", {
                    caption: "",
                    buttonicon: "ace-icon fa fa-refresh green",
                    onClickButton: function () {
                        $("#exceptioncase_list_grid-table").jqGrid("setGridParam",
                                {
                                    postData: {queryJsonString: ""} //发送数据
                                }
                        ).trigger("reloadGrid");
                    }
                }).navButtonAdd("#exceptioncase_list_grid-pager",{
                caption: "",
                buttonicon:"fa icon-cogs",
                onClickButton : function (){
                    jQuery("#exceptioncase_list_grid-table").jqGrid("columnChooser",{
                        done: function(perm, cols){
                            dynamicColumns(cols,dynamicDefalutValue);
                            $("#exceptioncase_list_grid-table").jqGrid("setGridWidth", $("#exceptioncase_list_grid-div").width());
                        }
                    });
                }
            });
        $(window).on("resize.jqGrid", function(){
            $("#exceptioncase_list_grid-table").jqGrid("setGridWidth", $("#exceptioncase_list_grid-div").width() );
            $("#exceptioncase_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-
            $("#exceptioncase_list_yy").outerHeight(true)-$("#exceptioncase_list_fixed_tool_div").outerHeight(true)-
            $("#exceptioncase_list_grid-pager").outerHeight(true)-$("#gview_exceptioncase_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
        });
        $(window).triggerHandler("resize.jqGrid");
    });
    
    var _queryForm_data = iTsai.form.serialize($("#exceptioncase_list_queryForm"));
    
	function queryOk(){
		var queryParam = iTsai.form.serialize($("#exceptioncase_list_queryForm"));
		queryByParam(queryParam);		
	}
	
	function queryByParam(jsonParam) {
        iTsai.form.deserialize($("#exceptioncase_list_hiddenQueryForm"), jsonParam);   //将json对象反序列化到列表页面中隐藏的form中
        var queryParam = iTsai.form.serialize($("#exceptioncase_list_hiddenQueryForm"));
        var queryJsonString = JSON.stringify(queryParam); 
        $("#exceptioncase_list_grid-table").jqGrid("setGridParam",
            {
                postData: {queryJsonString: queryJsonString} //发送数据
            }).trigger("reloadGrid");
    }
	function reset(){
	    $("#exceptioncase_list_queryForm #exceptioncase_list_type").select2("val","");
	    $("#exceptioncase_list_queryForm #exceptioncase_list_materialId").select2("val","");
	    $("#exceptioncase_list_queryForm #exceptioncase_list_goodsLocation").select2("val","");
		iTsai.form.deserialize($('#exceptioncase_list_queryForm'),_queryForm_data); 
		queryByParam(_queryForm_data);		
	} 
    $("#exceptioncase_list_queryForm #exceptioncase_list_materialId").select2({
			placeholder : "请选择物料",
			minimumInputLength : 0,
			allowClear : true, 
			ajax : {
				url : context_path + "/material/getMaterialSelectList",
				dataType : "json",
				delay : 250,
				data : function(term, pageNo) {
					term = $.trim(term);
					selectParam = term;
					return {
						queryString : term,
						pageSize : 15,
						pageNo : pageNo,
						time : new Date()
					//测试
					}
				},
				results : function(data, pageNo) {
					var res = data.result;
					if (res.length > 0) { //如果没有查询到数据，将会返回空串
						var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
						return {
							results : res,
							more : more
						};
					} else {
						return {
							results : {
								"id" : "0",
								"text" : "没有更多结果"
							}
						};
					}

				},
				cache : true
			}
		});
    $("#exceptioncase_list_queryForm #exceptioncase_list_goodsLocation").select2({
        placeholder: "选择库位",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/exceptioncase/getDeviceStuff",
            type: "POST",
            dataType: "json",
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    shelveId: $("#shelveId").val()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });
    function generateTask(){
	    var checkedNum = getGridCheckedNum("#exceptioncase_list_grid-table", "id");
        if (checkedNum == 0) {
           layer.alert("请至少选择一条记录生成盘点任务！");
           return false;
        } else {
           var ids = jQuery("#grid-table").jqGrid("getGridParam", "selarrrow");
           $.post(context_path + "/exceptioncase/toGenerateTaskView.do?ids="+ids, {}, function (str){
    		   $queryWindow=layer.open({
    		       title : "生成盘点任务", 
    	    	   type:1,
    	    	   skin : "layui-layer-molv",
    	    	   area : "490px",
    	    	   shade : 0.6, //遮罩透明度
    		       moveType : 1, //拖拽风格，0是默认，1是传统拖动
    		       anim : 2,
    		       content : str,
    		       success: function (layero, index) {
                   layer.closeAll("loading");
                }
    		});
    	});
      }
	}

function toExcel(){
    var ids = jQuery("#exceptioncase_list_grid-table").jqGrid("getGridParam", "selarrrow");
    $("#exceptioncase_list_hiddenForm #exceptioncase_list_ids").val(ids);
    $("#exceptioncase_list_hiddenForm").submit();	
}

 	function Restructure(){
	    var checkedNum = getGridCheckedNum("#exceptioncase_list_grid-table", "id");
        if (checkedNum == 0) {
           layer.alert("请至少选择一条记录重新下架！");
           return false;
        } else {
           var ids = jQuery("#exceptioncase_list_grid-table").jqGrid("getGridParam", "selarrrow");
           layer.confirm("确认生成下架单?", function () {
                $.ajax({
                    type: "POST",
                    url: context_path + "/exceptioncase/restructure?ids=" + ids,
                    dataType: "json",
                    success: function (data) {
                        if (data.result) {
                            //弹出提示信息
                            layer.msg(data.msg);
                            $("#exceptioncase_list_grid-table").jqGrid('setGridParam',
                                {
                                    postData: {queryJsonString: ""} //发送数据
                                }
                            ).trigger("reloadGrid");
                        } else {
                            layer.msg(data.msg);
                        }
                    }
                })
            });
        }
	}
</script>