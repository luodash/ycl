<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="wave_list_grid-div">
    <form id="wave_list_hiddenForm" action="<%=path%>/car/toExcel.do" method="POST" style="display: none;">
        <input id="wave_list_ids" name="ids" value=""/>
    </form>
    <form id="wave_list_hiddenQueryForm" style="display:none;">
        <input id="wave_list_materialNo" name="materialNo" value=""/>
        <input id="wave_list_materialName" name="carName" value="">
        <input id="wave_list_rfid" name="rfid" value=""/>
        <input id="wave_list_shelveId" name="shelveId" value="">
        <input id="wave_list_type" name="type" value="">
    </form>
    <div class="query_box" id="wave_list_yy" title="查询选项">
         <form id="wave_list_queryForm" style="max-width:100%;">
			 <ul class="form-elements">
				<li class="field-group field-fluid3">
					<label class="inline" for="wave_list_materialNo" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:80px;">波次单号：</span>
						<input id="wave_list_materialNo" name="materialNo" type="text" style="width: calc(100% - 85px);" placeholder="波次单号">
					</label>			
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="wave_list_materialName" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:80px;">波次规则：</span>
						<input id="wave_list_materialName" name="materialName" type="text" style="width: calc(100% - 85px);" placeholder="波次规则">
					</label>				
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="wave_list_outName" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:80px;">状态：</span>
						<input id="wave_list_outName" name="outName" type="text" style="width: calc(100% - 85px);" placeholder="状态">
					</label>			
				</li>
				
			</ul>
			<div class="field-button" style="">
					<div class="btn btn-info" onclick="queryOk();">
				        <i class="ace-icon fa fa-check bigger-110"></i>查询
			        </div>
					<div class="btn" onclick="reset();"><i class="ace-icon icon-remove"></i>重置</div>
		        </div>
		 </form>		 
    </div>
    <div id="wave_list_fixed_tool_div" class="fixed_tool_div">
        <div id="wave_list___toolbar__" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="wave_list_grid-table" style="width:100%;height:100%;"></table>
    <div id="wave_list_grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
var oriData; 
var _grid;
var openwindowtype = 0; //打开窗口类型：0新增，1修改
var selectid;
$(function  (){
    $(".toggle_tools").click();
});
$("#wave_list___toolbar__").iToolBar({
    id: "wave_list___tb__01",
    items: [
        {label: "导出",onclick:function(){toExcel();},iconClass:'icon-share'},
        {label: "打印",onclick:toPrint,iconClass:'icon-print'}
   ]
});
var _queryForm_data = iTsai.form.serialize($("#queryForm"));
_grid = jQuery("#wave_list_grid-table").jqGrid({
		url : context_path + "/wave/list.do",
	    datatype : "json",
	    colNames : [ "主键","波次单号", "波次规则","订单数量","总数量","状态"],
	    colModel : [ 
	                 {name : "id",index : "id",hidden:true},
	                 {name : "waveNo",index : "waveNo",width : 60}, 
	                 {name : "rule",index : "rule",width : 60},
	                 {name : "orderAmount",index : "orderAmount",width : 60},
	                 {name : "totalAmount",index : "totalAmount",width :70}, 
	                 {name : "status",index : "status",width : 60} 
	               ],
	    rowNum : 20,
	    rowList : [ 10, 20, 30 ],
	    pager : "#wave_list_grid-pager",
	    sortname : "waveNo",
	    sortorder : "asc",
           altRows: true,
           viewrecords : true,
           hidegrid:false,
    	    autowidth:true, 
           multiselect:true,
           loadComplete : function(data) {
	           var table = this;
	           setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
	           oriData = data;
           },
           emptyrecords: "没有相关记录",
           loadtext: "加载中...",
           pgtext : "页码 {0} / {1}页",
           recordtext: "显示 {0} - {1}共{2}条数据"
});

jQuery("#wave_list_grid-table").navGrid("#wave_list_grid-pager",{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd('#wave_list_grid-pager',{  
	caption:"",   
	buttonicon:"fa fa-refresh green",   
	onClickButton: function(){   
		$("#wave_list_grid-table").jqGrid("setGridParam", 
				{
			      postData: {queryJsonString:""} //发送数据 
				}
		).trigger("reloadGrid");
	}
});

$(window).on("resize.jqGrid", function () {
	$("#wave_list_grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);
	$("#wave_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-$("#wave_list_yy").outerHeight(true)-
	$("#wave_list_fixed_tool_div").outerHeight(true)-$("#wave_list_grid-pager").outerHeight(true)-
	$("#gview_wave_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
});

$(window).triggerHandler("resize.jqGrid");
/**
 * 查询按钮点击事件
 */
 function queryOk(){
	 var queryParam = iTsai.form.serialize($("#wave_list_queryForm"));
	 //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
	 queryByParam(queryParam);
}
 function queryByParam(jsonParam) {
	    iTsai.form.deserialize($("#wave_list_hiddenQueryForm"), jsonParam);
	    var queryParam = iTsai.form.serialize($("#wave_list_hiddenQueryForm"));
	    var queryJsonString = JSON.stringify(queryParam); 
	    $("#wave_list_grid-table").jqGrid("setGridParam",
	        {
	            postData: {queryJsonString: queryJsonString}
	        }
	    ).trigger("reloadGrid");
}
function reset(){
     $("#wave_list_queryForm #wave_list_shelveId").select2("val","");
     $("#wave_list_queryForm #wave_list_type").select2("val","");
	 iTsai.form.deserialize($("#wave_list_queryForm"),_queryForm_data); 
	 queryByParam(_queryForm_data);
}
function toExcel(){
    var ids = jQuery("#wave_list_grid-table").jqGrid("getGridParam", "selarrrow");
    $("#wave_list_hiddenForm #wave_list_ids").val(ids);
    $("#wave_list_hiddenForm").submit();	
}
$("#wave_list_queryForm #wave_list_shelveId").select2({
        placeholder: "选择货架",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/car/selectShelf",
            type: "POST",
            dataType: "json" ,
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });   
    $("#wave_list_queryForm .mySelect2").select2();   
    $("#wave_list_type").change(function(){
        $("#wave_list_queryForm #wave_list_type").val($("#wave_list_type").val());
    });
    function toPrint(){}   
</script>