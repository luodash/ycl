<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<div id="package_list_grid-div">
    <!-- 隐藏区域：存放查询条件 -->
    <form id="package_list_hiddenQueryForm" action="<%=path%>/packageManage/exportExcel" method="POST" style="display:none;">
        <input id="package_list_ids" name="ids" value="" />
        <input id="package_list_packageNo" name="packageNo" value="" >
        <input id="package_list_packageName" name="packageName" value="" >
        <input id="package_list_material" name="materialId" value="" >
        <input id="package_list_allocation" name="goodsId" value="" >
    </form>
    <div class="query_box" id="package_list_yy" title="查询选项">
            <form id="package_list_queryForm" style="max-width:100%;">
			 <ul class="form-elements">
				<li class="field-group field-fluid3">
					<label class="inline" for="package_list_packageNo" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:65px;">翻包单号：</span>
						<input type="text" name="packageNo" id="package_list_packageNo" value="" style="width: calc(100% -70px);" placeholder="翻包单号">
					</label>			
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="package_list_packageName" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:65px;">翻包名称：</span>
						<input type="text" name="packageName" id="package_list_packageName" value="" style="width: calc(100% -70px);" placeholder="翻包名称">
					</label>					
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="package_list_materialId" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:65px;">物料：</span>
						<input type="text" name="materialId" id="package_list_material" value="" style="width: calc(100% -70px);" placeholder="选择物料">
					</label>			
				</li>
				<li class="field-group-top field-group field-fluid3">
					<label class="inline" for="package_list_goodsId" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:65px;">库位：</span>
						<input type="text" name="goodsId" id="package_list_allocation" value="" style="width: calc(100% -70px);" placeholder="选择库位">
					</label>					
				</li>
				
			</ul>
			<div class="field-button" style="">
					<div class="btn btn-info" onclick="queryOk();">
				        <i class="ace-icon fa fa-check bigger-110"></i>查询
			        </div>
				    <div class="btn" onclick="reset();"><i class="ace-icon icon-remove"></i>重置</div>
				    <a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>
		        </div>
		  </form>		 
    </div>
    <div id="package_list_grid-div" style="width:100%;margin:0px auto;">
        <div id="package_list_fixed_tool_div" class="fixed_tool_div">
            <div id="package_list___toolbar__" style="float:left;overflow:hidden;"></div>
        </div>
        <table id="package_list_grid-table_package" style="width:100%;height:100%;overflow:auto;"></table>
        <div id="package_list_grid-pager_package"></div>
    </div>
</div>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriData;
    var _grid;
    var dynamicDefalutValue="f15a40678b304cfe94dd9f63683c717b";
    $(function  (){
        $(".toggle_tools").click();
    });
    
    $("#package_list___toolbar__").iToolBar({
        id:"package_list___tb__01",
        items:[
            {label: "添加",disabled:(${sessionUser.addQx}==1?false:true),onclick:addPackage,iconClass:'icon-plus'},
            {label: "编辑",disabled:(${sessionUser.editQx}==1?false:true),onclick:editPackage,iconClass:'icon-pencil'},
            {label: "提交",onclick:setPackageState,iconClass:'icon-pencil'},
            {label: "删除",disabled:(${sessionUser.deleteQx}==1?false:true),onclick:delPackage,iconClass:'icon-trash'},
            {label: "导出",disabled:(${sessionUser.deleteQx}==1?false:true),onclick:exportPackage,iconClass:'icon-share'},
            {label: "手动结束",disabled:(${sessionUser.addQx}==1?false:true),onclick:complete,iconClass:'icon-pencil'}
        ]
    });
    
    _grid = jQuery("#package_list_grid-table_package").jqGrid({
        url : context_path + '/packageManage/toList',
        datatype : "json",
        colNames : [ '翻包主键','翻包单号','翻包名称','翻包创建时间','翻包完成时间','物料编号','物料名称','物料单位','库位信息','数量','翻包入库数量','翻包出库数量','备注','状态'],
        colModel : [
            {name : 'id',index : 'id',width : 20,hidden:true},
            {name : 'packageNo',index : 'packageNo',width : 40},
            {name : 'packageName',index : 'packageName',width : 40},
            {name : 'createTime',index : 'createTime',width :40},
            {name : 'finishedTime',index : 'finishedTime',width :40,formatter:function(cellValu,option,rowObject){
                    if(cellValu){
                        return cellValu.substring(0,19);
                    }else{
                        return "";
                    }
                }},
            {name : 'materialNo',index : 'materialNo',width : 40},
            {name : 'materialName',index : 'materialName',width : 40},
            {name : 'unit',index : 'unit',width : 40},
            {name : 'goodsLocation',index : 'goodsLocation',width : 40},
            {name : 'amount',index : 'amount',width : 40},
            {name : 'packageIn',index : 'packageIn',width : 40},
            {name : 'packageOut',index : 'packageOut',width : 40},
            {name : 'remark',index : 'remark',width : 50},
            {name : 'state',index:'state',width:50,
                formatter:function(cellValu,option,rowObject){
                    if(cellValu==0){
                        return "<span style='color:red;font-weight:bold;'>未提交</span>" ;
                    }
                    if(cellValu==1){
                        return "<span style='color:green;font-weight:bold;'>已提交</span>" ;
                    }
                    if(cellValu==2){
                        return "<span style='color:orange;font-weight:bold;'>翻包出库中</span>" ;
                    }
                    if(cellValu==3){
                        return "<span style='color:green;font-weight:bold;'>翻包出库完成</span>" ;
                    }
                    if(cellValu==4){
                        return "<span style='color:green;font-weight:bold;'>翻包入库中</span>" ;
                    }
                    if(cellValu==5){
                        return "<span style='color:green;font-weight:bold;'>翻包完成</span>" ;
                    }
                }
            },
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : '#package_list_grid-pager_package',
        sortname : 'p.id',
        sortorder : "desc",
        altRows: false,
        viewrecords : true,
        autowidth:true,
        multiselect:true,
        multiboxonly: true,
        beforeRequest:function (){
            dynamicGetColumns(dynamicDefalutValue,'package_list_grid-table_package', $(window).width()-$("#sidebar").width() -7);
            //重新加载列属性
        },
        loadComplete : function(data){
            var table = this;
            setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            oriData = data;
            $(window).triggerHandler('resize.jqGrid');
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
    });
    //在分页工具栏中添加按钮
    jQuery("#package_list_grid-table_package").navGrid('#package_list_grid-pager_package',{edit:false,add:false,del:false,search:false,refresh:false})
        .navButtonAdd('#package_list_grid-pager_package',{
            caption:"",
            buttonicon:"ace-icon fa fa-refresh green",
            onClickButton: function(){
                $("#package_list_grid-table_package").jqGrid('setGridParam',
                    {
                        postData: {queryJsonString:""} //发送数据
                    }
                ).trigger("reloadGrid");
            }
        }).navButtonAdd('#package_list_grid-pager_package',{
            caption: "",
            buttonicon:"ace-icon fa icon-cogs",
            onClickButton : function (){
                jQuery("#package_list_grid-table_package").jqGrid('columnChooser',{
                    done: function(perm, cols){
                        dynamicColumns(cols,dynamicDefalutValue);
                        //cols页面获取隐藏的列,页面表格的值
                        $("#package_list_grid-table_package").jqGrid( 'setGridWidth', $("#package_list_grid-div").width() - 3);
                    }
                });
            }
        });
    $(window).on("resize.jqGrid", function () {
        $("#package_list_grid-table_package").jqGrid("setGridWidth", $("#package_list_grid-div").width() - 3 );
        var height = $("#breadcrumb").outerHeight(true)+$(".query_box").outerHeight(true)+
                     $("#package_list_fixed_tool_div").outerHeight(true)+
                     $("#gview_package_list_grid-table_package .ui-jqgrid-hbox").outerHeight(true)+
                     $("#package_list_grid-pager_package").outerHeight(true)+$("#header").outerHeight(true);
                     $("#package_list_grid-table_package").jqGrid("setGridHeight", (document.documentElement.clientHeight)-height );
    });
    var _queryForm_data = iTsai.form.serialize($('#package_list_packagemanage'));
    function queryOk(){
        var queryParam = iTsai.form.serialize($('#package_list_packagemanage'));
        //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
        queryInstoreListByParam(queryParam);
    }
    function queryInstoreListByParam(jsonParam){
        iTsai.form.deserialize($('#package_list_hiddenQueryForm'),jsonParam);   //将json对象反序列化到列表页面中隐藏的form中
        var queryParam = iTsai.form.serialize($('#package_list_hiddenQueryForm'));
        var queryJsonString = JSON.stringify(queryParam);
        $("#package_list_grid-table_package").jqGrid('setGridParam',
            {
                postData: {queryJsonString:queryJsonString} //发送数据
            }
        ).trigger("reloadGrid");
    }

    function reset(){
        iTsai.form.deserialize($('#package_list_packagemanage'),_queryForm_data);
        queryInstoreListByParam(_queryForm_data);
    }
    //添加
    function addPackage(){
        $.get( context_path + "/packageManage/toAddPackage?id=-1").done(function(data){
            layer.open({
                title : "翻包管理添加",
                type:1,
                skin : "layui-layer-molv",
                area : "600px",
                shade : 0.6, //遮罩透明度
                moveType : 1, //拖拽风格，0是默认，1是传统拖动
                anim : 2,
                content : data
            });
        });
    }
    function editPackage(){
        var checkedNum = getGridCheckedNum("#package_list_grid-table_package", "id");
        if (checkedNum == 0) {
            layer.alert("请选择一个要编辑的翻包单据！");
            return false;
        }else if (checkedNum > 1) {
            layer.alert("只能选择一个翻包单据进行编辑操作！");
            return false;
        }else{
            var id = jQuery("#package_list_grid-table_package").jqGrid('getGridParam', 'selrow');
            var rowData = jQuery("#package_list_grid-table_package").jqGrid('getRowData', id).state;
            if (rowData != '<span style="color:red;font-weight:bold;">未提交</span>') {
                layer.alert("只能选择状态为未提交的数据进行编辑操作");
                return false;
            }else{
                $.get(context_path + "/packageManage/toAddPackage?id=" + id).done(function (data) {
                    layer.open({
                        title: "翻包单据编辑",
                        type: 1,
                        skin: "layui-layer-molv",
                        area : "600px",
                        shade: 0.6, //遮罩透明度
                        moveType: 1, //拖拽风格，0是默认，1是传统拖动
                        anim: 2,
                        content: data
                    });
                });
            }
        }
    }
    function setPackageState(){
        var checkedNum = getGridCheckedNum("#package_list_grid-table_package", "id");
        if (checkedNum == 0) {
            layer.alert("请选择一个要提交的翻包单据！");
            return false;
        }else {
            var ids = jQuery("#package_list_grid-table_package").jqGrid('getGridParam', 'selarrrow');
            $.ajax({
                type: "POST",
                url: context_path + "/packageManage/setPackageState?ids=" + ids,
                dataType: "json",
                success: function (data) {
                    layer.alert(data.msg,{icon:1,time:1000});
                    gridReload();
                }
            })
        }
    }
    function delPackage(){
        var checkedNum = getGridCheckedNum("#package_list_grid-table_package", "id");
        if (checkedNum == 0) {
            layer.alert("请选择一个要删除的翻包单据！");
            return false;
        }else {
            var ids = jQuery("#package_list_grid-table_package").jqGrid('getGridParam', 'selarrrow');
            layer.confirm("确定删除选中的翻包单据?", function () {
                $.ajax({
                    type: "POST",
                    url: context_path + "/packageManage/delPackage?ids=" + ids,
                    dataType: "json",
                    success: function (data) {
                        if (data.result) {
                            layer.closeAll();
                            //弹出提示信息
                            layer.msg(data.msg);
                            $("#package_list_grid-table_package").jqGrid('setGridParam',
                                {
                                    postData: {queryJsonString: ""} //发送数据
                                }
                            ).trigger("reloadGrid");
                        } else {
                            layer.msg(data.msg);
                        }
                    }
                })
            });
        }
    }
    function exportPackage(){
        var ids = jQuery("#package_list_grid-table_package").jqGrid("getGridParam", "selarrrow");
        $("#package_list_hiddenQueryForm #package_list_ids").val(ids);
        $("#package_list_hiddenQueryForm").submit();
    }
    //手动结束按钮
    function complete(){
        var checkedNum = getGridCheckedNum("#package_list_grid-table_package", "id");
        if (checkedNum == 0) {
            layer.alert("请选择一个要结束的翻包单据！");
            return false;
        }else if (checkedNum > 1) {
            layer.alert("只能选择一个翻包单据进行编辑操作！");
            return false;
        }else {
            var id = jQuery("#package_list_grid-table_package").jqGrid('getGridParam', 'selrow');
            $.ajax({
                type: "POST",
                url: context_path + "/packageManage/complete?ids=" + id,
                dataType: "json",
                success: function (data) {
                    if(data.result){
                        layer.alert(data.msg,{icon:1,time:1000});
                        $("#package_list_grid-table_package").jqGrid('setGridParam',
                            {
                                postData: {queryJsonString: ""} //发送数据
                            }
                        ).trigger("reloadGrid");
                    }else{
                        layer.alert(data.msg,{icon:1,time:1000})
                    }
                }
            })
        }
    }

    function gridReload(){
        $("#package_list_grid-table_package").jqGrid('setGridParam',
            {
                url:context_path + '/packageManage/toList',
                postData: {queryJsonString:""}
            }
        ).trigger("reloadGrid");
    }
    $("#package_list_queryForm #package_list_material").select2({
        placeholder : "请选择物料",//文本框的提示信息
        minimumInputLength : 0, //至少输入n个字符，才去加载数据
        allowClear : true, //是否允许用户清除文本信息
        multiple: false,
        closeOnSelect:false,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax : {
            url : context_path + "/packageManage/getmaterialList",
            dataType : "json",
            delay : 250,
            data : function(term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                selectParam = term;
                return {
                    queryString : term, //联动查询的字符
                    pageSize : 15, //一次性加载的数据条数
                    pageNo : pageNo, //页码
                    time : new Date()
                }
            },
            results : function(data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results : res,
                        more : more
                    };
                } else {
                    return {
                        results : {
                            "id" : "0",
                            "text" : "没有更多结果"
                        }
                    };
                }
            },
            cache : true
        }
    });

    $("#package_list_queryForm #package_list_allocation").select2({
        placeholder: "请选择库位",//文本框的提示信息
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        multiple: false,
        closeOnSelect: false,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/packageManage/getGoodsList",
            dataType: "json",
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                selectParam = term;
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {
                            "id": "0",
                            "text": "没有更多结果"
                        }
                    };
                }
            },
            cache: true
        }
    });
</script>