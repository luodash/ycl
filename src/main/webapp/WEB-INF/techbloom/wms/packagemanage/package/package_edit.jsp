<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
    String path = request.getContextPath();
%>
<div id="supplier_edit_page" class="row-fluid" style="height: inherit;">
    <form id="baseInfor" class="form-horizontal" style="overflow: auto; height: calc(100% - 70px);">
        <input type="hidden" id="id" name="id" value="${packages.id }">
        <div class="control-group">
            <label class="control-label" for="packageNo">翻包编号：</label>
            <div class="controls">
                <div class="input-append span12 required">
                    <input type="text" class="span11" id="packageNo" name="packageNo" value="${packages.packageNo }" placeholder="翻包编号" readonly>
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="packageName">翻包名称：</label>
            <div class="controls">
                <div class="input-append span12 required">
                    <input type="text" class="span11" id="packageName" name="packageName" value="${packages.packageName }" placeholder="翻包名称">
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="materialId">物料：</label>
            <div class="controls">
                <div class="required" style="float: none !important;">
                    <input type="text" class="span11" id="materialId" name="materialId">
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="goodsId">库位：</label>
            <div class="controls">
                <div class="required" style="float: none !important;">
                    <input type="text" class="span11" id="goodsId" name="goodsId" >
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="amount">数量：</label>
            <div class="controls required">
                <input type="text" class="span11" name="amount" id="amount" placeholder="翻包数量" value="${packages.amount}" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="remark">备注：</label>
            <div class="controls">
                <input type = "text" class="span11" name="remark" id="remark"  value="${packages.remark }"></input>
            </div>
        </div>
    </form>
    <div style="margin-bottom:5px; text-align: center;">
            <span class="btn btn-info" id="formSave">
		       <i class="ace-icon fa fa-check bigger-110"></i>保存
            </span>
            <span class="btn btn-danger" onclick="layer.closeAll();">
               <i class="icon-remove"></i>&nbsp;取消
            </span>
    </div>
</div>


<iframe src="about:blank" name="_ifr" height="0" class="hidden"></iframe>
<script type="text/javascript">

    //单据保存按钮点击事件
    $("#formSave").click(function(){
        if($('#baseInfor').valid()){
            //通过验证：获取表单数据，保存表单信息
            var formdata = $('#baseInfor').serialize();
            saveFormInfo(formdata);
        }
    });

    $("#baseInfor").validate({
        ignore:"",
        rules: {
            "packageName": {
                required: true,
                remote:"<%=path%>/packageManage/existsName?id="+$("#id").val()
            },
            "materialId":{
                required: true,
            },
            "goodsId":{
                required: true,
            },
            "amount":{
                required: true,
                digits:true
            }
        },
        messages: {
            "packageName": {
                required: "请输入单据名称!",
                remote:"单据名称已经存在"
            },
            "materialId":{
                required: "请选择物料",
            },
            "goodsId":{
                required: "请选择库位",
            },
            "amount":{
                required: "请输入数量",
                digits:"必输输入整数"
            }

        },
        errorClass: "help-inline",
        errorElement: "span",
        highlight:function(element, errorClass, validClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        }
    })

    function saveFormInfo(formdata){
        $("#formSave").attr("disabled","disabled");
        $(window).triggerHandler('resize.jqGrid');
        $.ajax({
            type:"POST",
            url:context_path + "/packageManage/savePackage",
            data:formdata,
            dataType:"json",
            success:function(data){
                if(data.result){
                    layer.closeAll();
                    layer.msg("单据保存成功", {icon: 1,time:1200});
                    if($('#baseInfor #id').val()==-1){
                        $('#baseInfor #packageNo').val(data.packageNo);
                        $('#baseInfor #id').val(data.id);
                    }
                    gridReload();
                }else{
                    layer.alert("单据保存失败");
                }
            }
        })
    }

    $('#materialId').select2({
        placeholder : "请选择物料",//文本框的提示信息
        minimumInputLength : 0, //至少输入n个字符，才去加载数据
        allowClear : true, //是否允许用户清除文本信息
        multiple: false,
        closeOnSelect:false,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax : {
            url : context_path + '/packageManage/getmaterialList',
            dataType : 'json',
            delay : 250,
            data : function(term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                selectParam = term;
                return {
                    queryString : term, //联动查询的字符
                    pageSize : 15, //一次性加载的数据条数
                    pageNo : pageNo, //页码
                    time : new Date()
                }
            },
            results : function(data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results : res,
                        more : more
                    };
                } else {
                    return {
                        results : {
                            "id" : "0",
                            "text" : "没有更多结果"
                        }
                    };
                }
            },
            cache : true
        }
    });

    $('#goodsId').select2({
        placeholder: "请选择物料",//文本框的提示信息
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        multiple: false,
        closeOnSelect: false,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + '/packageManage/getGoodsList',
            dataType: 'json',
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                selectParam = term;
                return {
                    materialId:$("#materialId").val(),
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {
                            "id": "0",
                            "text": "没有更多结果"
                        }
                    };
                }
            },
            cache: true
        }
    });
    //如果id的值不是-1的话,则进行库位以及物料的赋值
    $(document).ready(function () {
        if($("#id").val()!=-1){
            $.ajax({
                url: context_path + "/packageManage/getMaterialAndGoods?tm=" + new Date(),
                type: "POST",
                data: {id: $("#id").val()},
                dataType: "JSON",
                success: function (data) {
                    $("#goodsId").select2("data", {
                        id: data.goodsId,
                        text: data.goodsLocation
                    });
                    $("#materialId").select2("data", {
                        id: data.materialId,
                        text: data.materialName
                    });
                }
            })
        }
    })
</script>
