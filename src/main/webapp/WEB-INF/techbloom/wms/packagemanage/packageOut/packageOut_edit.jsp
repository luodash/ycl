<%@ page language="java" import="java.lang.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<div class="row-fluid" style="height: inherit;margin:0px;border: 0px">
	<form id="outForm" class="form-horizontal" target="_ifr" style="margin-right:10px;">
		<input type="hidden" id="id" name="id" value="${out.id}"> 		
		<input type="hidden" id="hasDetail" name="hasDetail" value="${hasDetail}">
		<input type="hidden" id="state" name="state" value="${out.state}">
		<div class="row" style="margin:0;padding:0;">
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="outNo">翻出编号：</label>
				<div class="controls">
					<div class="input-append  span12">
						<c:choose>
							<c:when test="${edit==1}">
								<input type="text" class="span11" id="outNo" name="outNo" value="${out.outNo }" placeholder="翻出编号" readonly="readonly">
							</c:when>
							<c:otherwise>
								<input type="text" class="span11" id="outNo" name="outNo" placeholder="后台自动生成" readonly="readonly">
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			</div>
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="outName">翻出名称：</label>
				<div class="controls">
					<div class="input-append  span12 required">
						<input type="text" class="span11" id="outName" name="outName" value="${out.outName}" placeholder="翻出名称">
					</div>
				</div>
			</div>
		</div>
		<div class="row" style="margin:0;padding:0;">
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="packageId">翻包单号：</label>
				<div class="controls">
					<div class="span12 required" style=" float: none !important;">
						<input type="text" class="span11" name="packageId" id="packageId" placeholder="翻包单号" value="${out.packageId}" />
					</div>
				</div>
			</div>
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="operateTime">操作时间：</label>
				<div class="controls">
					<div class="input-append  span12 required">
						<input type="text" class="form-control date-picker" id="operateTime" name="operateTime" style="width:220px;" value="${out.operateTime }" placeholder="操作时间">
					</div>
				</div>
			</div>
		</div>
		<div class="row" style="margin:0;padding:0;">
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="userId">操作人：</label>
				<div class="controls">
					<div class="span12 required" style=" float: none !important;">
						<input type="text" class="span11" name="userId" id="userId" placeholder="操作人" value="${out.userId}">
					</div>
				</div>
			</div>
			<div class="control-group span6" style="display: inline">
				<label class="control-label" for="remark">备注：</label>
				<div class="controls">
					<div class="input-append  span12">
						<input type="text" class="span11" name="remark" id="remark" placeholder="备注" value="${out.remark}">
					</div>
				</div>
			</div>
		</div>
		<div style="margin-left:10px;">
			<span class="btn btn-info" id="formSave" onclick="saveForm();">
				<i class="ace-icon fa fa-check bigger-110"></i>保存
			</span>
			<span class="btn btn-info" id="publish" onclick="publish();">
				<i class="ace-icon fa fa-check bigger-110"></i>提交
			</span>
		</div>
	</form>
	<div id="materialDiv" style="margin:10px;">
		<!-- 下拉框 -->
		<label class="inline" for="materialInfor">物料：</label> 
		    <input type="text" id="materialInfor" name="materialInfor" style="width:350px;margin-right:10px;" />
		<button id="addMaterialBtn" class="btn btn-xs btn-primary" onclick="addDetail();">
			<i class="icon-plus" style="margin-right:6px;"></i>添加
		</button>
	</div>
	<!-- 表格div -->
	<div id="grid-div-c" style="width:100%;margin:0px auto;">
		<!-- 	表格工具栏 -->
		<div id="fixed_tool_div" class="fixed_tool_div detailToolBar">
			<div id="__toolbar__-c" style="float:left;overflow:hidden;"></div>
		</div>
		<!-- 物料详情信息表格 -->
		<table id="grid-table-c" style="width:100%;height:100%;"></table>
		<!-- 表格分页栏 -->
		<div id="grid-pager-c"></div>
	</div>
</div>
<script type="text/javascript">
var context_path = '<%=path%>';
var oriDataDetail;
var _grid_detail;
var lastsel2;
var dynamicDefalutValue="3163dc31b46f456d8f17d811d7f7f972";
var pdd=$("#outForm #packageId").val();
$(".date-picker").datetimepicker({format: "YYYY-MM-DD",useMinutes:true,useSeconds:true});
$("#outForm").validate({
   ignore: "", 
   rules:{
      "outName":{
         required:true,
         remote:{
            cache:false,
            async:false,
            url: "<%=path%>/packageOut/isHaveName?outId="+$("#outForm #id").val()
         }
      },
      "packageId":{
         required:true
      },
      "operateTime":{
         required:true
      },
      "userId":{
         required:true
      }
   },
   messages: {
     "outName":{
         required:"请输入翻出名称！",
         remote:"已存在相同的翻出名称！"
      },
      "packageId":{
         required:"请选择翻包单号！"
      },
      "operateTime":{
         required:"请输入操作时间！"
      },
      "userId":{
         required:"请选择操作人！"
      }
      
   },
    errorClass: "help-inline",
    errorElement: "span",
    highlight:function(element, errorClass, validClass) {
        $(element).parents('.control-group').addClass('error');
    },
    unhighlight: function(element, errorClass, validClass) {
        $(element).parents('.control-group').removeClass('error');
    }
});

function saveForm() {
    var bean = $("#outForm").serialize();
    if ($("#outForm").valid()) {
        saveOut(bean);
    }     
}
function saveOut(bean) {
        $.ajax({
			url : context_path + "/packageOut/saveOut.do",
			type : "POST",
			data : bean,
			dataType : "JSON",
			success : function(data) {
				if (Boolean(data.result)) {
					layer.msg("表头保存成功！", {icon : 1});
					$("#outForm #outNo").val(data.outNo);
					$("#outForm #id").val(data.outId);
					$("#outForm #state").val(data.state);
					$("#grid-table").jqGrid("setGridParam", {
                       postData: {queryJsonString:""}
                    }).trigger("reloadGrid");
				} else {
					layer.alert(data.msg, {icon : 2});
				}
			},
			error : function(XMLHttpRequest) {
				alert(XMLHttpRequest.readyState);
				alert("出错啦！！！");
			}
		});
    }
/* 保存、修改 */
function saveOrUpdate(bean){
   $.ajax({
   url:context_path+"/salemanage/save",
   type:"post",
   data:bean,
   dataType:"JSON",
   success:function (data){
      if(data.result){
      $("#saleId").val(data.saleId);
      $("#outForm #state").val("0");
      saleId=data.saleId;
      gridReload();
      layer.msg("操作成功！",{icon:1,time:1200});
      }else{
      layer.msg("操作失败！",{icon:2,time:1200});
      }
   }
   });

}
//单元格编辑成功后，回调的函数
	var editFunction = function eidtSuccess(XHR){
		 var data = eval("("+XHR.responseText+")"); 
		if(data["msg"]!=""){
			layer.alert(data["msg"]);
		}
		jQuery("#grid-table-c").jqGrid('setGridParam', 
				{
					postData: {
						id:$('#id').val(),
						queryJsonString:""
					} 
				}
		  ).trigger("reloadGrid");
	};
	
  
  	_grid_detail=jQuery("#grid-table-c").jqGrid({
         url : context_path + "/packageOut/detailList.do?pId="+$("#outForm #id").val(),
         datatype : "json",
         colNames : [ "详情主键","物料编号","物料名称","库位","数量","状态","操作"],
         colModel : [ 
  					  {name : "id",index : "id",width : 20,hidden:true}, 
  					  {name : "materialNo",index:"materialNo",width :20}, 
                      {name : "materialName",index:"materialName",width : 20},
                      {name : "locationName",index:"locationName",width : 60}, 
                      {name : "amount", index: "amount", width: 20,editable : true,editrules: {custom: true, custom_func: numberRegex},
                        editoptions: {
	                          size: 25,
	                          dataEvents: [
	                              {
	                                  type: "blur",     //blur,focus,change.............
	                                  fn: function (e) {
	                                	  var $element = e.currentTarget;
	                                	  var $elementId = $element.id;
	                                	  var rowid = $elementId.split("_")[0];
	                                	  var id=$element.parentElement.parentElement.children[1].textContent;
	                                	  var indocType = 1;
	                                	  var reg = new RegExp("^[0-9]+(.[0-9]{1,2})?$");
	                                	  if (!reg.test($("#"+$elementId).val())) {
	                                		  layer.alert("请输入正实数！(注：小数点后将舍去)");
	                                		  return;
	                                	   }
	                                	  $.ajax({
	                                		  url:context_path + "/packageOut/setAmount",
	                                		  type:"POST",
	                                		  data:{id:id,amount:$("#"+rowid+"_amount").val(),packageId:$("#outForm #packageId").val(),outId:$("#outForm #id").val()},
	                                		  dataType:"json",
	                                		  success:function(data){
	                                		        if(Boolean(data.result)){
	                                		           $("#grid-table-c").jqGrid("setGridParam", 
                                							{
                                					    		url : context_path + "/packageOut/detailList.do?pId="+$("#outForm #id").val(),
                                								postData: {queryJsonString:""} //发送数据  :选中的节点
                                							}
                                					  ).trigger("reloadGrid");
                                					  layer.msg("保存数量成功！");
	                                		        }else{
	                                		        layer.alert(data.msg);
	                                		        }	                                		                     		      
	                                		  }
	                                	  }); 
	                                  }
	                              }
	                          ]
	                      }
                      },
                      {name: "state",index:"state",width:50,formatter:function(cellvalue,option,rowObject){
                      if(cellvalue==0){
                          return "<span style='color:red;font-weight:bold;'>未完成</span>";
                      }
                      if(cellvalue==1){
                          return "<span style='color:green;font-weight:bold;'>已完成</span>";
                      }
                    }
                 },
                 {name: "operation", index:"operation", width:30, formatter: function (cellValu, option, rowObject) {
                       if(rowObject.state==0){
                           return "<div style='margin-bottom:5px' class='btn btn-xs btn-success' onclick='finishOut(" + rowObject.id + ")'>完成翻出</div>"
                       }else if(rowObject.state==1){
                           return "<div style='margin-bottom:5px' class='btn btn-xs btn-success' disabled='false' onclick='finishOut(" + rowObject.id + ")'>完成翻出</div>"
                       }
	                        
	        			}
	        	    }
                ],
         rowNum : 20,
         rowList : [ 10, 20, 30 ],
         pager : "#grid-pager-c",
         sortname : "d.id",
         sortorder : "asc",
         altRows: true,
         viewrecords : true,
         autowidth:true,
         multiselect:true,
		 multiboxonly: true,
         beforeRequest:function (){
            dynamicGetColumns(dynamicDefalutValue,'grid-table-c', $(window).width()-$("#sidebar").width() -7);
            //重新加载列属性
         },
         loadComplete : function(data) {
             var table = this;
             setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
             oriDataDetail = data;
             $(window).triggerHandler('resize.jqGrid');
         },
	   cellEdit: true,
	   cellsubmit : "clientArray",
  	   emptyrecords: "没有相关记录",
  	   loadtext: "加载中...",
  	   pgtext : "页码 {0} / {1}页",
  	   recordtext: "显示 {0} - {1}共{2}条数据",
    });
    //在分页工具栏中添加按钮
    $("#grid-table-c").navGrid('#grid-pager-c',{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#grid-pager-c",{  
  	   caption:"",   
  	   buttonicon:"ace-icon fa fa-refresh green",   
  	   onClickButton: function(){   
  	    $("#grid-table-c").jqGrid('setGridParam', 
				{
  	    			url:context_path + '/salemanage/detailList?saleId='+saleId,
					postData: {queryJsonString:""} //发送数据  :选中的节点
				}
		  ).trigger("reloadGrid");
  	   }
  	}).navButtonAdd("#grid-pager-c",{
            caption: "",
            buttonicon:"ace-icon fa icon-cogs",
            onClickButton : function (){
                jQuery("#grid-table-c").jqGrid('columnChooser',{
                    done: function(perm, cols){
                        dynamicColumns(cols,dynamicDefalutValue);
                        //cols页面获取隐藏的列,页面表格的值
                        $("#grid-table-c").jqGrid( 'setGridWidth', $("#grid-div-c").width()-3 );
                    }
                });
            }
        });
  	
  	$(window).on("resize.jqGrid", function () {
  		$("#grid-table-c").jqGrid( "setGridWidth", $("#grid-div-c").width() - 3 );
  		var height = $(".layui-layer-title",_grid_detail.parents(".layui-layer")).height()+
                     $("#outForm").outerHeight(true)+$("#materialDiv").outerHeight(true)+
                     $("#grid-pager-c").outerHeight(true)+$("#fixed_tool_div.fixed_tool_div.detailToolBar").outerHeight(true)+
                     $("#gview_grid-table-c .ui-jqgrid-hbox").outerHeight(true);
                     $("#grid-table-c").jqGrid('setGridHeight',_grid_detail.parents(".layui-layer").height()-height);
  	});
  	$(window).triggerHandler("resize.jqGrid"); 
//将数据格式化成两位小数：四舍五入
 function formatterNumToFixed(value,options,rowObj){
	if(value!=null){
		var floatNum = parseFloat(value);
		return floatNum.toFixed(2);
	}else{
		return "0.00";
	}
 } 
 //数量输入验证
function numberRegex(value, colname) {
    var regex = /^\d+\.?\d{0,2}$/;
    reloadDetailTableList();
    if (!regex.test(value)) {
        return [false, ""];
    }
    else  return [true, ""];
}
$("#materialInfor").on("change",function(e){
	var datas=$("#materialInfor").select2("val");
	selectData = datas;
	$("#materialInfor").select2("search",selectParam);
});
$("#outForm #packageId").on("change",function(e){
    if ($("#outForm #hasDetail").val()=="yes"){
            layer.alert("单据已存在详情，不可修改翻包单号！");
            $.ajax({
	 type:"POST",
	 url:context_path + "/packageOut/findOutById",
	 data:{id:$("#outForm #id").val()},
	 dataType:"json",
	 success:function(data){ 
             $("#outForm #packageId").select2("data", {
 			  id: data.packageId,
 			  text:data.packageName
             });
		}
	});
     return;         
   }
	pdd=$("#packageId").select2("val").toString();
	$("#packageId").select2("search",selectParam);
});
 //清空物料多选框中的值
 function removeChoice(){
	  $("#s2id_materialInfor .select2-choices").children(".select2-search-choice").remove();
	  $("#materialInfor").select2("val","");
	  selectData = 0;
	  
 }
//添加物料详情
 function addDetail(){
	  if($("#outForm #id").val()==""){
		  layer.alert("请先保存表单信息！");
		  return;
	  }
	  if($("#outForm #state").val()=="0"||$("#outForm #state").val()==""){
	     if(selectData!=""){
		  //将选中的物料添加到数据库中
		  $.ajax({
			  type:"POST",
			  url:context_path + "/packageOut/saveDetail.do",
			  data:{outId:$("#outForm #id").val(),packageId:$("#outForm #packageId").val(),materialId:selectData.toString()},
			  dataType:"json",
			  success:function(data){
				  removeChoice();   //清空下拉框中的值
				  if(Boolean(data.result)){
					 layer.msg("添加成功",{icon:1,time:1200});
					  //重新加载详情表格
					 reloadDetailTableList();
					 $("#outForm #hasDetail").val("yes");
				  }else{
					  layer.msg(data.msg,{icon:2,time:1200});
				  }
			  }
		  });
	  }else{
		  layer.alert("请选择物料！");
	  }
	 }else{
	    layer.alert("单据已提交，不可再添加详情！");
	    return; 
	 }	  
  }
  //工具栏
	  $("#__toolbar__-c").iToolBar({
	   	 id:"__tb__01",
	   	 items:[
	   	  	{label:"删除", onclick:delDetail},
	    ]
	  });
	//删除物料详情
	function delDetail(){
	   var checkedNum = getGridCheckedNum("#grid-table-c", "id");
	   if(checkedNum==0){
	      layer.alert("请先选择要删除的详情！");
	      return;
	   }
	   if($("#outForm #state").val()=="0"||$("#outForm #state").val()==""){
	       var ids = jQuery("#grid-table-c").jqGrid("getGridParam", "selarrrow");
	   $.ajax({
	      url:context_path + "/packageOut/deleteDetail.do?ids="+ids+"&outId="+$("#outForm #id").val(),
	      type:"POST",
	      dataType:"JSON",
	      success:function(data){
	         if(Boolean(data.result)){
	           layer.msg("操作成功！");
			   reloadDetailTableList();
			   $("#outForm #hasDetail").val(data.hasDetail);
	         }else{
	           layer.msg(data.msg);
	         }
	      }
	   });	   
	   }else{
	     layer.alert("单据已提交，不可删除详情！");
	     return;	   
	   }   
	}
	 function reloadDetailTableList(){
          $("#grid-table-c").jqGrid("setGridParam", 
			    {
  	    			url:context_path + "/packageOut/detailList.do?pId="+$("#outForm #id").val(),
					postData: {queryJsonString:""} //发送数据  :选中的节点
				}).trigger("reloadGrid");
     }
   $("#materialInfor").select2({
        placeholder: "选择物料",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        width: 300,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/packageOut/getMListByInId",
            type: "POST",
            dataType: "json",
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    packageId:$("#outForm #packageId").val(),
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });
   $("#outForm #userId").select2({
        placeholder: "选择操作人",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        width: 200,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/packageOut/getUserSelect",
            type: "POST",
            dataType: "json",
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });
    $("#outForm #packageId").select2({
        placeholder: "选择翻包单号",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        width: 200,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/packageOut/getPackageSelect",
            type: "POST",
            dataType: "json",
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });
    $.ajax({
	 type:"POST",
	 url:context_path + "/packageOut/findOutById",
	 data:{id:$("#outForm #id").val()},
	 dataType:"json",
	 success:function(data){
			  $("#outForm #userId").select2("data", {
 			  id: data.userId,
 			  text: data.userName
             }); 
             $("#outForm #packageId").select2("data", {
 			  id: data.packageId,
 			  text:data.packageName
             });
		}
	});
	function finishOut(id){
	    $.ajax({
	       type:"post",
	       url:context_path+"/packageOut/finishOut.do?id="+id+"&packageId="+$("#outForm #packageId").val()+"&outId="+$("#outForm #id").val(),
	       dataType:"json",
	       success:function(data){
	            if(Boolean(data.result)){
	                layer.msg(data.msg,{icon:1});
	                reloadDetailTableList();
	                $("#grid-table").jqGrid("setGridParam", {
                    postData: {queryJsonString:""}
                   }).trigger("reloadGrid");
	            }else{
	                layer.msg(data.msg,{icon:2,time:2000});
	            }
	       }	    
	    });
	}
	function publish(){
	if($("#outForm #id").val()==""){
	   layer.alert("请先保存表头信息！");
	   return;
	}
	if($("#outForm #state").val()=="0"||$("#outForm #state").val()==""){
	   layer.confirm("确定提交,提交之后数据不能修改", function () {
	       $.ajax({
	   type:"post",
	   url:context_path+"/packageOut/publish.do?outId="+$("#outForm #id").val()+"&packageId="+$("#outForm #packageId").val(),
	   dataType:"json",
	   success:function(data){
	       if(Boolean(data.result)){
	            layer.msg(data.msg,{icon:1});
	            $("#grid-table").jqGrid("setGridParam", {
                    postData: {queryJsonString:""}
                }).trigger("reloadGrid");
                $("#outForm #state").val("1");	       
	       }else{
	            layer.msg(data.msg,{icon:2});
	       }	   
	      }	
	    });   
	 });	   
	}else{
	   layer.alert("单据已提交，请勿重复提交！");
            return;
	}
}
</script>
