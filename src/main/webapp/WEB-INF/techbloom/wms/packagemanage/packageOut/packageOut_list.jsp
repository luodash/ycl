<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="packageOut_list_grid-div">
    <form id="packageOut_list_hiddenForm" action="<%=path%>/packageOut/toExcel.do" method="POST" style="display: none;">
        <input id="packageOut_list_ids" name="ids" value=""/>
    </form>
    <!-- 隐藏区域：存放查询条件 -->
    <form id="packageOut_list_hiddenQueryForm" style="display:none;">
        <input id="packageOut_list_outNo" name="outNo" value=""/>
        <input id="packageOut_list_outName" name="outName" value="">
        <input id="packageOut_list_userId" name="userId" value=""/>
        <input id="packageOut_list_packageId" name="packageId" value="">
    </form>
     <div class="query_box" id="packageOut_list_yy" title="查询选项">
            <form id="packageOut_list_queryForm" style="max-width:100%;">
			 <ul class="form-elements">
				<li class="field-group field-fluid3">
					<label class="inline" for="packageOut_list_outNo" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:80px;">翻出编号：</span>
						<input id="packageOut_list_outNo" name="outNo" type="text" style="width: calc(100% - 85px);" placeholder="翻出编号">
					</label>			
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="packageOut_list_outName" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:80px;">翻出名称：</span>
						<input id="packageOut_list_outName" name="outName" type="text" style="width: calc(100% - 85px);" placeholder="翻出名称">
					</label>				
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="packageOut_list_packageId" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:80px;">翻包单号：</span>
						<input id="packageOut_list_packageId" name="packageId" type="text" style="width: calc(100% - 85px);" placeholder="翻包单号">
					</label>			
				</li>
				<li class="field-group-top field-group field-fluid3">
					<label class="inline" for="packageOut_list_userId" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:80px;">操作人：</span>
						<input id="packageOut_list_userId" name="userId" type="text" style="width: calc(100% - 85px);" placeholder="操作人">
					</label>				
				</li>
				
			</ul>
			<div class="field-button" style="">
					<div class="btn btn-info" onclick="queryOk();">
				        <i class="ace-icon fa fa-check bigger-110"></i>查询
			        </div>
					<div class="btn" onclick="reset();"><i class="ace-icon icon-remove"></i>重置</div>
					<a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>
		        </div>
		  </form>		 
    </div>
    <div id="packageOut_list_fixed_tool_div" class="fixed_tool_div">
        <div id="packageOut_list___toolbar__" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="packageOut_list_grid-table" style="width:100%;margin: 0px !important;boder:0px !important"></table>
    <div id="packageOut_list_grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriData;
    var _grid;
    var openwindowtype;
    var dynamicDefalutValue="f1840fe8727340368e22b37e4311409f";
    $(function  (){
        $(".toggle_tools").click();
    });
    $("#packageOut_list___toolbar__").iToolBar({
        id: "packageOut_list___tb__01",
        items: [
            {label: "添加", disabled: ( ${sessionUser.addQx } == 1 ? false : true), onclick:addOut, iconClass:'glyphicon glyphicon-plus'},
            {label: "编辑", disabled: ( ${sessionUser.editQx} == 1 ? false : true),onclick: editOut, iconClass:'glyphicon glyphicon-pencil'},
            {label: "删除", disabled: ( ${sessionUser.deleteQx} == 1 ? false : true),onclick: deleteOut, iconClass:'glyphicon glyphicon-trash'},
            {label: "导出", disabled: ( ${sessionUser.queryQx}==1?false:true),onclick:function(){toExcel();},iconClass:' icon-share'}
           ]
    });

    $(function () {
        _grid = jQuery("#packageOut_list_grid-table").jqGrid({
            url: context_path + "/packageOut/list.do",
            datatype: "json",
            colNames: ["主键", "翻出编号","翻出名称", "翻包单号", "操作时间", "操作人", "备注","状态"],
            colModel: [
                {name: "id", index: "id", width: 20, hidden: true},
                {name: "outNo", index: "outNo", width: 60},
                {name: "outName", index: "outName", width: 60},
                {name: "packageNo",index: "packageNo",width:60},
                {name: "operateTime",index: "operateTime",width:70},
                {name: "userName", index: "userName", width: 70},
                {name: "remark", index: "remark", width: 60},
                {name: "state", index: "state" ,width : 30,
                formatter:function(cellValue,option,rowObject){
                    if(typeof cellValue == "number"){
                        if(cellValue==0){
                            return "<span style='color:grey;font-weight:bold;'>未提交</span>";
                        }else if(cellValue==1){
                            return "<span style='color:orange;font-weight:bold;'>已提交</span>";
                        }else if(cellValue==2){
                            return "<span style='color:blue;font-weight:bold;'>翻包出库</span>";
                        }else if(cellValue==3){
                            return "<span style='color:green;font-weight:bold;'>出库完成</span>";
                        }
                    }else{
                        return "未知";
                    }
                }
            }
            ],
            rowNum: 20,
            rowList: [10, 20, 30],
            pager: "#packageOut_list_grid-pager",
            sortname: "o.packageOutNo",
            sortorder: "asc",
            altRows: true,
            viewrecords: true,
            autowidth: true,
            multiselect: true,
            multiboxonly: true,
            beforeRequest:function (){
                dynamicGetColumns(dynamicDefalutValue,'packageOut_list_grid-table', $(window).width()-$("#sidebar").width() -7);
                //重新加载列属性
            },
            loadComplete: function (data) {
                var table = this;
                setTimeout(function () {
                    updatePagerIcons(table);
                    enableTooltips(table);
                }, 0);
                oriData = data;
            },
            emptyrecords: "没有相关记录",
            loadtext: "加载中...",
            pgtext: "页码 {0} / {1}页",
            recordtext: "显示 {0} - {1}共{2}条数据"
        });
        //在分页工具栏中添加按钮
        jQuery("#packageOut_list_grid-table").navGrid("#packageOut_list_grid-pager", {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false
        }).navButtonAdd("#packageOut_list_grid-pager", {
                    caption: "",
                    buttonicon: "ace-icon fa fa-refresh green",
                    onClickButton: function () {
                        $("#packageOut_list_grid-table").jqGrid("setGridParam",
                                {
                                    postData: {queryJsonString: ""} //发送数据
                                }
                        ).trigger("reloadGrid");
                    }
                })
            .navButtonAdd('#packageOut_list_grid-pager',{
                caption: "",
                buttonicon:"ace-icon fa icon-cogs",
                onClickButton : function (){
                    jQuery("#packageOut_list_grid-table").jqGrid('columnChooser',{
                        done: function(perm, cols){
                            dynamicColumns(cols,dynamicDefalutValue);
                            //cols页面获取隐藏的列,页面表格的值
                            $("#packageOut_list_grid-table").jqGrid( 'setGridWidth', $("#packageOut_list_grid-div").width() );
                        }
                    });
                }
            });
        $(window).on("resize.jqGrid", function () {
            $("#packageOut_list_grid-table").jqGrid("setGridWidth", $("#packageOut_list_grid-div").width() );
            $("#packageOut_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-
            $("#packageOut_list_yy").outerHeight(true)-$("#packageOut_list_fixed_tool_div").outerHeight(true)-
            $("#packageOut_list_grid-pager").outerHeight(true)-$("#gview_packageOut_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
        });

        $(window).triggerHandler("resize.jqGrid");
    });
    var _queryForm_data = iTsai.form.serialize($('#packageOut_list_queryForm'));
	function queryOk(){
		var queryParam = iTsai.form.serialize($("#packageOut_list_queryForm"));
		queryLineByParam(queryParam);		
	}
	function queryLineByParam(jsonParam) {
	    iTsai.form.deserialize($("#packageOut_list_hiddenQueryForm"), jsonParam);
	    var queryParam = iTsai.form.serialize($("#packageOut_list_hiddenQueryForm"));
	    var queryJsonString = JSON.stringify(queryParam); 
	    $("#packageOut_list_grid-table").jqGrid("setGridParam",
	        {
            postData: {queryJsonString: queryJsonString}
        }).trigger("reloadGrid");
    }
	function reset(){
		$("#packageOut_list_queryForm #packageOut_list_outNo").val("").trigger("change");
        $("#packageOut_list_queryForm #packageOut_list_outName").val("").trigger("change");
        $("#packageOut_list_queryForm #packageOut_list_packageId").select2("val","");
        $("#packageOut_list_queryForm #packageOut_list_userId").select2("val","");
	    $("#packageOut_list_grid-table").jqGrid("setGridParam", {
            postData: {queryJsonString:""}
        }).trigger("reloadGrid");		
	}
	function addOut(){
	    openwindowtype=0;
		$.post(context_path + "/packageOut/toEdit.do", {}, function (str){
		$queryWindow=layer.open({
		    title : "翻包出添加", 
	    	type:1,
	    	skin : "layui-layer-molv",
	    	area : ["750px","600px"],
	    	shade : 0.6, //遮罩透明度
		    moveType : 1, //拖拽风格，0是默认，1是传统拖动
		    anim : 2,
		    content : str,
		    success: function (layero, index) {
                layer.closeAll("loading");
            }
		});
	 });
	}
	function editOut(){
	var checkedNum = getGridCheckedNum("#packageOut_list_grid-table", "id");
    if (checkedNum == 0) {
       layer.alert("请选择一个要编辑的翻包出！");
        return false;
    } else if (checkedNum > 1) {
    	layer.alert("只能选择一个翻包出进行编辑操作！");
        return false;
    } else {
        openwindowtype=1;
        var id = jQuery("#packageOut_list_grid-table").jqGrid("getGridParam","selrow");
        $.post(context_path + "/packageOut/toEdit.do?id="+id, {}, function (str){
    		$queryWindow=layer.open({
    		    title : "翻包出编辑", 
    	    	type:1,
    	    	skin : "layui-layer-molv",
    	    	area : ["750px","600px"],
    	    	shade : 0.6, //遮罩透明度
    		    moveType : 1, //拖拽风格，0是默认，1是传统拖动
    		    anim : 2,
    		    content : str,
    		    success: function (layero, index) {
                    layer.closeAll("loading");
                }
    		});
    	});
      }
	}
	function deleteOut(){
	   var checkedNum = getGridCheckedNum("#packageOut_list_grid-table", "id");  //选中的数量
    if (checkedNum == 0) {
        layer.alert("请选择一个要删除的翻包出！");
    } else {
        var ids = jQuery("#packageOut_list_grid-table").jqGrid("getGridParam", "selarrrow");
        layer.confirm("确定删除选中的翻包出？", function() {
    		$.ajax({
    			type : "post",
    			url : context_path + "/packageOut/deleteOut.do?ids=" + ids,
    			dataType : "json",
    			cache : false,
    			success : function(data) {
    				layer.closeAll();
    				if (Boolean(data.result)) {
    					layer.msg(data.msg, {icon: 1,time:1000});
    				}else{
    					layer.msg(data.msg, {icon: 7,time:2000});
    				}
    				_grid.trigger("reloadGrid");  //重新加载表格
    			}
    		});
    	});
     }
	}
	$("#packageOut_list_queryForm #packageOut_list_userId").select2({
        placeholder: "选择操作人",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
   
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/packageOut/getUserSelect",
            type: "POST",
            dataType: "json",
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });
	$("#packageOut_list_queryForm #packageOut_list_packageId").select2({
        placeholder: "选择翻包单号",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/packageOut/getPackageSelect",
            type: "POST",
            dataType: "json",
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });
    function toExcel(){
        var ids = jQuery("#packageOut_list_grid-table").jqGrid("getGridParam", "selarrrow");
        $("#packageOut_list_hiddenForm #packageOut_list_ids").val(ids);
        $("#packageOut_list_hiddenForm").submit();	
    }
</script>
</html>