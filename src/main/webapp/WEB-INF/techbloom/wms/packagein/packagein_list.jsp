<%--
  Created by IntelliJ IDEA.
  User: HQKS-004-05
  Date: 2017/12/13
  Time: 13:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<div id="packagein_list_grid-div">
    <!-- 隐藏区域：存放查询条件 -->
    <form id="packagein_list_hiddenForm" action="<%=path%>/packageIn/toExcel.do" method="POST" style="display: none;">
        <input id="packagein_list_ids" name="ids" value=""/>
    </form>
    <form id="packagein_list_hiddenQueryForm" method="POST" style="display:none;">
        <input id="packagein_list_packageInNo" name="packageInNo" value="" />
        <input id="packagein_list_packageInName" name="packageInName" value="" >
        <input id="packagein_list_packageId" name="packageId" value="" />
        <input id="packagein_list_userId" name="userId" value="" >
    </form>
    <div class="query_box" id="packagein_list_yy" title="查询选项">
            <form id="packagein_list_queryForm" style="max-width:100%;">
			 <ul class="form-elements">
				<li class="field-group field-fluid3">
					<label class="inline" for="packagein_list_packageInNo" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:80px;">翻入编号：</span>
						<input type="text" name="packageInNo" id="packagein_list_packageInNo" value="" style="width: calc(100% - 85px);" placeholder="翻入编号">
					</label>			
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="packagein_list_packageInName" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:80px;">翻入名称：</span>
						<input type="text" name="packageInName" id="packagein_list_packageInName" value="" style="width: calc(100% - 85px);" placeholder="翻入名称">
					</label>				
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="packagein_list_packageId" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:80px;">翻包单号：</span>
						<input type="text" name="packageId" id="packagein_list_packageId" value="" style="width: calc(100% - 85px);" placeholder="翻包单号">
					</label>			
				</li>
				<li class="field-group-top  field-group field-fluid3">
					<label class="inline" for="packagein_list_userId" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:80px;">操作人：</span>
						<input type="text" name="userId" id="packagein_list_userId" value="" style="width: calc(100% - 85px);" placeholder="操作人">
					</label>				
				</li>
				
			</ul>
			<div class="field-button" style="">
					<div class="btn btn-info" onclick="queryOk();">
				        <i class="ace-icon fa fa-check bigger-110"></i>查询
			        </div>
					<div class="btn" onclick="reset();"><i class="ace-icon icon-remove"></i>重置</div>
					<a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>
		        </div>
		  </form>		 
    </div>
    <div id="packagein_list_grid-div" style="width:100%;margin:0px auto;">
        <div id="packagein_list_fixed_tool_div" class="fixed_tool_div">
            <div id="packagein_list___toolbar__" style="float:left;overflow:hidden;"></div>
        </div>
        <table id="packagein_list_grid-table_package" style="width:100%;height:100%;overflow:auto;"></table>
        <div id="packagein_list_grid-pager_package"></div>
    </div>
</div>

<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriData;
    var _grid;
    var openwindowtype;
    var dynamicDefalutValue="8e9109ed7c1b49d7811fbc6b309591bc";
    $(function  (){
        $(".toggle_tools").click();
    });
    $("#packagein_list___toolbar__").iToolBar({
        id:"packagein_list___tb__01",
        items:[
            {label: "添加", disabled: ( ${sessionUser.addQx } == 1 ? false : true), onclick:addIn, iconClass:'glyphicon glyphicon-plus'},
            {label: "编辑", disabled: ( ${sessionUser.editQx} == 1 ? false : true),onclick: editIn, iconClass:'glyphicon glyphicon-pencil'},
            {label: "删除", disabled: ( ${sessionUser.deleteQx} == 1 ? false : true),onclick: deleteIn, iconClass:'glyphicon glyphicon-trash'},
            {label: "导出", disabled: ( ${sessionUser.queryQx}==1?false:true),onclick:function(){toExcel();},iconClass:' icon-share'}
        ]
    });
    $(function () {
        _grid = jQuery("#packagein_list_grid-table_package").jqGrid({
            url: context_path + '/packageIn/toList',
            datatype: "json",
            colNames: ['翻包入库主键', '翻包入库编号', '翻包入库名称', '翻包单号', '操作人', '翻包入库时间', '备注', '状态'],
            colModel: [
                {name: 'id', index: 'id', width: 20, hidden: true},
                {name: 'packageInNo', index: 'packageInNo', width: 40},
                {name: 'packageInName', index: 'packageInName', width: 40},
                {name: 'packageNo', index: 'packageNo', width: 40},
                {name: 'userName', index: 'userName', width: 40},
                {
                    name: 'oprateTime',
                    index: 'oprateTime',
                    width: 40,
                    formatter: function (cellValu, option, rowObject) {
                        if (cellValu) {
                            return cellValu.substring(0, 19);
                        } else {
                            return "";
                        }
                    }
                },
                {name: 'remark', index: 'remark', width: 50},
                {
                    name: 'state', index: 'state', width: 50,
                    formatter: function (cellValu, option, rowObject) {
                        if (cellValu == 0) {
                            return "<span style='color:red;font-weight:bold;'>未提交</span>";
                        }
                        if (cellValu == 1) {
                            return "<span style='color:green;font-weight:bold;'>已提交</span>";
                        }
                    }
                },
            ],
            rowNum: 20,
            rowList: [10, 20, 30],
            pager: '#packagein_list_grid-pager_package',
            sortname: 'p.id',
            sortorder: "asc",
            altRows: false,
            viewrecords: true,
            autowidth: true,
            multiselect: true,
            multiboxonly: true,
            beforeRequest:function (){
                dynamicGetColumns(dynamicDefalutValue,'packagein_list_grid-table_package', $(window).width()-$("#sidebar").width() -7);
                //重新加载列属性
            },
            loadComplete: function (data) {
                var table = this;
                setTimeout(function () {
                    updatePagerIcons(table);
                    enableTooltips(table);
                }, 0);
                oriData = data;
                $(window).triggerHandler('resize.jqGrid');
            },
            emptyrecords: "没有相关记录",
            loadtext: "加载中...",
            pgtext: "页码 {0} / {1}页",
            recordtext: "显示 {0} - {1}共{2}条数据"
        });
        //在分页工具栏中添加按钮
        jQuery("#packagein_list_grid-table_package").navGrid('#packagein_list_grid-pager_package', {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false
        }).navButtonAdd('#packagein_list_grid-pager_package', {
                caption: "",
                buttonicon: "ace-icon fa fa-refresh green",
                onClickButton: function () {
                    $("#packagein_list_grid-table_package").jqGrid('setGridParam',
                        {
                            postData: {queryJsonString: ""} //发送数据
                        }
                    ).trigger("reloadGrid");
                }
            }).navButtonAdd('#packagein_list_grid-pager_package',{
                caption: "",
                buttonicon:"ace-icon fa icon-cogs",
                onClickButton : function (){
                    jQuery("#packagein_list_grid-table_package").jqGrid('columnChooser',{
                        done: function(perm, cols){
                            dynamicColumns(cols,dynamicDefalutValue);
                            //cols页面获取隐藏的列,页面表格的值
                            $("#packagein_list_grid-table_package").jqGrid( 'setGridWidth', $("#packagein_list_grid-div").width() - 3);
                        }
                    });
                }
            });
        $(window).on("resize.jqGrid", function () {
            $("#packagein_list_grid-table_package").jqGrid("setGridWidth", $("#packagein_list_grid-div").width() - 3);
            var height = $("#breadcrumb").outerHeight(true) + $(".query_box").outerHeight(true) +
                $("#packagein_list_fixed_tool_div").outerHeight(true) +
                $("#gview_packagein_list_grid-table_package .ui-jqgrid-hbox").outerHeight(true) +
                $("#packagein_list_grid-pager_package").outerHeight(true) + $("#header").outerHeight(true);
                $("#packagein_list_grid-table_package").jqGrid("setGridHeight", (document.documentElement.clientHeight) - height);
        });
        $(window).triggerHandler("resize.jqGrid");
    });

    //查询和重置
    var _queryForm_data = iTsai.form.serialize($('#packagein_list_queryForm'));
    function queryOk(){
        var queryParam = iTsai.form.serialize($('#packagein_list_queryForm'));
        //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
        queryInstoreListByParam(queryParam);
    }
    function queryInstoreListByParam(jsonParam){
        iTsai.form.deserialize($('#packagein_list_hiddenQueryForm'),jsonParam);   //将json对象反序列化到列表页面中隐藏的form中
        var queryParam = iTsai.form.serialize($('#packagein_list_hiddenQueryForm'));
        var queryJsonString = JSON.stringify(queryParam);
        $("#packagein_list_queryForm #packagein_list_packageId").select2("val","");
        $("#packagein_list_queryForm #packagein_list_userId").select2("val","");
        $("#packagein_list_grid-table_package").jqGrid('setGridParam',
            {
                postData: {queryJsonString:queryJsonString} //发送数据
            }
        ).trigger("reloadGrid");
    }
    function reset(){
        iTsai.form.deserialize($('#packagein_list_queryForm'),_queryForm_data);
        queryInstoreListByParam(_queryForm_data);
    }
    //添加
    function addIn(){
        $.get( context_path + "/packageIn/toAddPackage?id=-1").done(function(data){
            layer.open({
                title : "翻包入库添加",
                type:1,
                skin : "layui-layer-molv",
                area : ['780px', '620px'],
                shade : 0.6, //遮罩透明度
                moveType : 1, //拖拽风格，0是默认，1是传统拖动
                anim : 2,
                content : data
            });
        });
    }
    function editIn(){
        var checkedNum = getGridCheckedNum("#packagein_list_grid-table_package", "id");
        if (checkedNum == 0) {
            layer.alert("请选择一个要编辑的翻包入！");
            return false;
        }else if (checkedNum > 1) {
            layer.alert("只能选择一个翻包入库进行编辑操作！");
            return false;
        }else{
            openwindowtype=1;
            var id = jQuery("#packagein_list_grid-table_package").jqGrid("getGridParam","selrow");
            $.post(context_path + "/packageIn/toAddPackage?id="+id, {}, function (str){
                $queryWindow=layer.open({
                    title : "翻包入库编辑",
                    type:1,
                    skin : "layui-layer-molv",
                    area : ['780px', '620px'],
                    shade : 0.6, //遮罩透明度
                    moveType : 1, //拖拽风格，0是默认，1是传统拖动
                    anim : 2,
                    content : str,
                    success: function (layero, index) {
                        layer.closeAll("loading");
                    }
                });
            });
        }

    }
    function deleteIn(){
        var checkedNum = getGridCheckedNum("#packagein_list_grid-table_package", "id");
        if (checkedNum == 0) {
            layer.alert("请选择一个要删除的入库单据！");
            return false;
        }else {
            var ids = jQuery("#packagein_list_grid-table_package").jqGrid('getGridParam', 'selarrrow');
            layer.confirm("确定删除选中的入库单据?", function () {
                $.ajax({
                    type: "POST",
                    url: context_path + "/packageIn/delPackage?ids=" + ids,
                    dataType: "json",
                    success: function (data) {
                        if (data.result) {
                            layer.closeAll();
                            //弹出提示信息
                            layer.msg(data.msg);
                            $("#packagein_list_grid-table_package").jqGrid('setGridParam',
                                {
                                    postData: {queryJsonString: ""} //发送数据
                                }
                            ).trigger("reloadGrid");
                        } else {
                            layer.msg(data.msg);
                        }
                    }
                })
            });
        }
    }
    $("#packagein_list_queryForm #packagein_list_userId").select2({
        placeholder: "选择操作人",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
       
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/packageIn/getUserSelect",
            type: "POST",
            dataType: "json",
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });
    $("#packagein_list_queryForm #packagein_list_packageId").select2({
        placeholder: "选择翻包编号",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/packageIn/getPackageList",
            type: "POST",
            dataType: 'json',
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                    //测试
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });
    function toExcel(){
        var ids = jQuery("#packagein_list_grid-table_package").jqGrid("getGridParam", "selarrrow");
        $("#packagein_list_hiddenForm #packagein_list_ids").val(ids);
        $("#packagein_list_hiddenForm").submit();
    }
</script>
