<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<div id="rfIdBind_edit_page" class="row-fluid" style="height: inherit;">
	<form id="rfIdBind_edit_carForm" class="form-horizontal" style="overflow: auto; height: calc(100% - 70px);">
		<input type="hidden" id="rfIdBind_edit_id" name="id" value="${car.id}">
		<div class="control-group">
			<label class="control-label" for="rfIdBind_edit_carNo">rfid：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="rfIdBind_edit_carNo" name="carNo" value="${car.carNo }" placeholder="rfid">
				</div>
			</div>
		</div>
	</form>
	<div class="field-button" style="text-align: center;border-top: 0px;margin: 15px auto;">
		<span class="btn btn-info" onclick="saveForm();">
		   <i class="ace-icon fa fa-check bigger-110"></i>保存
		</span>
		<span class="btn btn-danger" onclick="layer.closeAll();">
		   <i class="icon-remove"></i>&nbsp;取消
		</span>
	</div>
</div>
<script type="text/javascript">

	//确定按钮点击事件
    function saveForm() {
        if ($("#rfIdBind_edit_carForm").valid()) 
            saveCarInfo($("#rfIdBind_edit_carForm").serialize());
    }
  	//保存/修改用户信息
    function saveCarInfo(bean) {
        $.ajax({
           url: context_path + "/rfIdBind/saveCar",
           type: "POST",
           data: bean,
           dataType: "JSON",
           success: function (data) {
               if (Boolean(data.result)) {
                   layer.msg("保存成功！", {icon: 1});
                   //关闭当前窗口
                   layer.close($queryWindow);
                   //刷新列表
                   $("#rfIdBind_edit_grid-table").jqGrid('setGridParam',{
                    	postData: {queryJsonString: ""}
                	}).trigger("reloadGrid");
               } else {
                   layer.alert("保存失败，请稍后重试！", {icon: 2});
               }
           },
           error:function(XMLHttpRequest){
       		alert(XMLHttpRequest.readyState);
       		alert("出错啦！！！");
       	   }
       });
    }
    $("#rfIdBind_edit_carForm #rfIdBind_edit_type").select2({
	    minimumInputLength:0,
	    allowClear:true,
	    delay:250,
	    width:435,
	    formatNoMatches:"没有结果",
		formatSearching:"搜索中...",
		formatAjaxError:"加载出错啦！"
    });
    $("#rfIdBind_edit_carForm #rfIdBind_edit_type").on("change.select2",function(){
       $("#rfIdBind_edit_carForm #rfIdBind_edit_type").trigger("keyup")}
    );
    $("#rfIdBind_edit_carForm #rfIdBind_edit_state").select2({
	    minimumInputLength:0,
	    allowClear:true,
	    delay:250,
	    width:435,
	    formatNoMatches:"没有结果",
		formatSearching:"搜索中...",
		formatAjaxError:"加载出错啦！"
    });
    $("#rfIdBind_edit_carForm #rfIdBind_edit_state").on("change.select2",function(){
       $("#rfIdBind_edit_carForm #rfIdBind_edit_state").trigger("keyup")}
    );
    
    $.ajax({
	 type:"POST",
	 url:context_path + "/rfIdBind/getCarById",
	 data:{id:$("#rfIdBind_edit_carForm #rfIdBind_edit_id").val()},
	 dataType:"json",
	 success:function(data){
	  	$("#rfIdBind_edit_carForm #rfIdBind_edit_shelveId").select2("data", {
		  id: data.shelveId,
		  text: data.shelveName
           });
		}
	});
</script>