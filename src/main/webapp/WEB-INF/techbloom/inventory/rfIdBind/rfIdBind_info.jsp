<%@ page language="java" import="java.lang.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<div class="row-fluid" style="height: inherit;margin:0px;border: 0px">
	<input type="hidden" value="${id}" id="rfIdBind_info_hid01">
	<!-- 表格div -->
	<div id="rfIdBind_info_grid-div-c" style="width:100%;margin:10px auto;">
		<!-- 	表格工具栏 -->
		<div id="rfIdBind_info_fixed_tool_div" class="fixed_tool_div detailToolBar">
			<div id="rfIdBind_info___toolbar__-c" style="float:left;overflow:hidden;"></div>
		</div>
		<!-- 物料详情信息表格 -->
		<table id="rfIdBind_info_grid-table-c" style="width:100%;height:100%;"></table>
		<!-- 表格分页栏 -->
		<div id="rfIdBind_info_grid-pager-c"></div>
	</div>
</div>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var p2p;
    var oriDataDetail;
    var _grid_detail;        //表格对象
    var lastsel2;
    var outLocationId=0;

    _grid_detail=jQuery("#rfIdBind_info_grid-table-c").jqGrid({
        url : context_path + "/rfIdBind/detailList.do?id="+$("#rfIdBind_info_hid01").val(),
        datatype : "json",
        colNames : [ "详情主键","物料名称","物料编码","RFID编码"],
        colModel : [
            {name : "id",index : "id",width : 20,hidden:true},
            {name : "carNo",index:"carNo",width :20},
            {name : "carName",index:"mi",width : 20},
            {name : "shelveName",index:"type",width : 20},
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : "#rfIdBind_info_grid-pager-c",
        sortname : "id",
        sortorder : "asc",
        altRows: true,
        viewrecords : true,
        caption : "详情列表",
        autowidth:true,
        multiselect:true,
        multiboxonly: true,
        loadComplete : function(data){
            var table = this;
            setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            oriDataDetail = data;
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据",
    });
    //在分页工具栏中添加按钮
    $("#rfIdBind_info_grid-table-c").navGrid("#rfIdBind_info_grid-pager-c",{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#rfIdBind_info_grid-pager-c",{
        caption:"",
        buttonicon:"ace-icon fa fa-refresh green",
        onClickButton: function(){
            $("#rfIdBind_info_grid-table-c").jqGrid('setGridParam',
                {
                    url:context_path + '/transfer/detailList?transferId='+transferId,
                    postData: {queryJsonString:""} //发送数据  :选中的节点
                }
            ).trigger("reloadGrid");
        }
    });

    function bindRfid() {
        $.post(context_path + '/orderDisplay/toBindRfid.do', {}, function (str) {
            $queryWindow = layer.open({
                title: "rfid绑定",
                type: 1,
                skin: "layui-layer-molv",
                area: "600px",
                shade: 0.6, //遮罩透明度
                moveType: 1, //拖拽风格，0是默认，1是传统拖动
                content: str,//注意，如果str是object，那么需要字符拼接。
                success: function (layero, index) {
                    layer.closeAll("loading");
                }
            });
        }).error(function () {
            layer.closeAll();
            layer.msg("加载失败！", {icon: 2});
        })
    }

    $(window).on("resize.jqGrid", function () {
        $("#rfIdBind_info_grid-table-c").jqGrid("setGridWidth", $("#rfIdBind_info_grid-div-c").width() - 3 );
        $("#rfIdBind_info_grid-table-c").jqGrid("setGridHeight", (document.documentElement.clientHeight - $("#rfIdBind_info_grid-pager-c").height() - 380) );
    });
    $(window).triggerHandler("resize.jqGrid");

    $("#rfIdBind_info_productId").change(function () {
        p2p=$("#rfIdBind_info_productId").val();
    })
    $("#rfIdBind_info_artBomId").change(function () {
        if ($("#rfIdBind_info_productId").val() == "") {
            layer.alert("请先选择产品！");
            $("#rfIdBind_info_artBomId").select2("val", "");
        }
    })

    $("#rfIdBind_info_outLocationId").change(function(){
        outLocationId=$("#rfIdBind_info_outLocationId").val();
    });


</script>