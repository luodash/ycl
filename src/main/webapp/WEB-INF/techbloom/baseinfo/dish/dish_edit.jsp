<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<div id="dish_edit_page" class="row-fluid" style="height: inherit;">
	<form id="dishEditForm" class="form-horizontal" style="overflow: auto; height: calc(100% - 70px);">
		<input type="hidden" id="id" name="id" value="${dish.id}">	
			
		<div class="control-group">
			<label class="control-label" for="code">盘具编码：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="code" name="code" value="${dish.code}" placeholder="盘具编码" readonly="readonly">
				</div>
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label" for="model">规格型号：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="model" name="model" value="${dish.model }" placeholder="规格型号" readonly="readonly">
				</div>
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label" for="drumType">盘具类型：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="drumType" name="drumType" value="${dish.drumType }" placeholder="盘具类型" readonly="readonly">
				</div>
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label" for="innerDiameter">内直径：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="innerDiameter" name="innerDiameter" value="${dish.innerDiameter }" placeholder="内径">
				</div>
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label" for="outerDiameter">外直径：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="outerDiameter" name="outerDiameter" value="${dish.outerDiameter }" placeholder="盘径">
				</div>
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label" for="oneMax">L1MAX：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="oneMax" name="oneMax" value="${dish.oneMax }" placeholder="L1MAX" readonly="readonly">
				</div>
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label" for="two">L2：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="two" name="two" value="${dish.two }" placeholder="L2" readonly="readonly">
				</div>
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label" for="maxCarry">最大载重：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="maxCarry" name="maxCarry" value="${dish.maxCarry }" placeholder="最大载重" readonly="readonly">
				</div>
			</div>
		</div>
	</form>
	<div class="field-button" style="text-align: center;border-top: 0px;margin: 15px auto;">
		<span class="btn btn-info" onclick="saveForm();">
		   <i class="ace-icon fa fa-check bigger-110"></i>保存
		</span>
		<span class="btn btn-danger" onclick="layer.closeAll();">
		   <i class="icon-remove"></i>&nbsp;取消
		</span>
	</div>
</div>
<script type="text/javascript">
	$("#dishEditForm").validate({
		rules:{
            "code":{
                required:true,
            },
            "model":{
                required:true,
            },
            "drumType":{
                required:true,
            },
            "innerDiameter":{
                required:true,
            },
            "outerDiameter":{
                required:true,
            },
            "oneMax":{
                required:true,
            },
            "two":{
                required:true,
            },
            "maxCarry":{
                required:true,
            },
  		},
  		messages:{
            "code":{
                required:"请输入盘具编码！",
            },
            "model":{
                required:"请输入规格型号！",
            },
            "drumType":{
                required:"请输入盘具类型！",
            },
            "innerDiameter":{
                required:"请输入内直径！",
            },
            "outerDiameter":{
                required:"请输入外直径！",
            },
            "oneMax":{
                required:"请输入L1MAX！",
            },
            "two":{
                required:"请输入L2！",
            },
            "maxCarry":{
                required:"请输入最大载重！",
            },
  		},
  		errorClass: "help-inline",
		errorElement: "div",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
		}
  	});
	//确定按钮点击事件
    function saveForm() {
        // if ($("#dishEditForm").valid()) {
            saveDishInfo($("#dishEditForm").serialize());
        // }
    }
  	//保存/修改用户信息
    function saveDishInfo(bean) {
        $.ajax({
            url: context_path + "/dish/saveDish",
            type: "POST",
            data: bean,
            dataType: "JSON",
            success: function (data) {
                if (Boolean(data.result)) {
                    layer.msg("保存成功！", {icon: 1});
                    //关闭当前窗口
                    layer.close($queryWindow);
                    //刷新列表
					iTsai.form.deserialize($("#dish_list_hiddenQueryForm"), iTsai.form.serialize($("#dish_list_queryForm")));
					var queryParam = iTsai.form.serialize($("#dish_list_hiddenQueryForm"));
					var queryJsonString = JSON.stringify(queryParam);
					$("#dish_list_grid-table").jqGrid('setGridParam', {
						 postData: {queryJsonString: queryJsonString}
					 }).trigger("reloadGrid");
                } else {
                    layer.alert("保存失败，请稍后重试！", {icon: 2});
                }
            },
            error:function(XMLHttpRequest){
        		alert(XMLHttpRequest.readyState);
        		alert("出错啦！！！");
        	}
        });
    }

</script>