<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="dish_list_grid-div">
    <form id="dish_list_hiddenForm" action="<%=path%>/dish/materialExcel" method="POST" style="display: none;">
        <input id="dish_list_ids" name="ids" value=""/>
		<input id="dish_list_queryCode" name="queryCode" value="">
        <input id="dish_list_queryExportExcelIndex" name="queryExportExcelIndex" value=""/>
    </form>
    <form id="dish_list_hiddenQueryForm" style="display:none;">
        <input name="code" value=""/>
    </form>
    <c:if test="${operationCode.webSearch==1}">
    <div class="query_box" id="dish_list_yy" title="查询选项">
         <form id="dish_list_queryForm" style="max-width:100%;">
			 <ul class="form-elements">
				 <li class="field-group field-fluid3">
					 <label class="inline" for="dish_list_code" style="margin-right:20px;width: 100%;">
						 <span class="form_label" style="width:80px;">盘具编码：</span>
						 <input id="dish_list_code" name="code" type="text" style="width: calc(100% - 85px);" placeholder="盘具编码">
					 </label>
				 </li>
			 </ul>
			<div class="field-button" style="">
				<div class="btn btn-info" onclick="dish_list_queryOk();">
			        <i class="ace-icon fa fa-check bigger-110"></i>查询
		        </div>
				<div class="btn" onclick="dish_list_reset();"><i class="ace-icon icon-remove"></i>重置</div>
	        </div>
		  </form>		 
    </div>
    </c:if>
    <div id="dish_list_fixed_tool_div" class="fixed_tool_div">
        <div id="dish_list_toolbar_" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="dish_list_grid-table" style="width:100%;height:100%;"></table>
    <div id="dish_list_grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
	var dish_list_oriData;
	var dish_list_grid;
    var dish_list_exportExcelIndex;

    $("input").keypress(function (e) {
        if (e.which == 13) {
            dish_list_queryOk();
        }
    });

	$(function  (){
	    $(".toggle_tools").click();
	});

	$("#dish_list_toolbar_").iToolBar({
	    id: "dish_list_tb_01",
	    items: [
	    	{label: "同步数据", hidden:"${operationCode.webTbsj}"=="1",onclick: dish_list_synchronousdata, iconClass:'icon-refresh'},
            {label: "编辑",hidden:"${operationCode.webEdit}"=="1", onclick: dish_list_openEditPage, iconClass:'glyphicon glyphicon-pencil'},
	        {label: "导出", hidden:"${operationCode.webExport}"=="1",onclick:function(){dish_list_toExcel();},iconClass:'icon-share'}
	   ]
	});

	var dish_list_queryForm_data = iTsai.form.serialize($("#dish_list_queryForm"));

    dish_list_grid = jQuery("#dish_list_grid-table").jqGrid({
			url : context_path + "/dish/list.do",
		    datatype : "json",
		    colNames : [ "主键","盘具编码", "规格型号","盘具类型","盘具子类型","盘具外径--mm","盘具内径--mm","L1MAX--mm","L2--mm","最大负重--kg","是否超宽"],
		    colModel : [ 
                 {name : "id",index : "id",hidden:true},
                 {name : "code",index : "code",width : 60},
                 {name : "model",index : "model",width : 160},
                 {name : "drumType",index : "drumType,id",width :40},
                 {name : "drumTypeDetail",index : "drumTypeDetail,id",width :60},
                 {name : "outerDiameter",index : "outerDiameter,id",width :40},
                 {name : "innerDiameter",index : "innerDiameter,id",width :40},
                 {name : "oneMax",index : "oneMax",width :40},
                 {name : "two",index : "two",width :40},
                 {name : "maxCarry",index : "maxCarry",width :40},
                 {name : "superWide",index : "SUPER_WIDE,id",width :40,formatter:function(cellvalue,option,rowObject){
                         if(cellvalue==0){
                             return "<span style='color:green;font-weight:bold;'>否</span>";
                         }else if(cellvalue==1){
                             return "<span style='color:red;font-weight:bold;'>是</span>";
                         }
                     }
                 }
            ],
		    rowNum : 20,
		    rowList : [ 10, 20, 30 ],
		    pager : "#dish_list_grid-pager",
		    sortname : "id",
		    sortorder : "asc",
            altRows: true,
            viewrecords : true,
            hidegrid:false,
     	    autowidth:true, 
            multiselect:true,
            multiboxonly: true,
            loadComplete : function(data) {
            	var table = this;
            	setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
                dish_list_oriData = data;
            },
            emptyrecords: "没有相关记录",
            loadtext: "加载中...",
            pgtext : "页码 {0} / {1}页",
            recordtext: "显示 {0} - {1}共{2}条数据"
	});

	jQuery("#dish_list_grid-table").navGrid("#dish_list_grid-pager",
    {edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#dish_list_grid-pager",{
		caption:"",   
		buttonicon:"fa fa-refresh green",   
		onClickButton: function(){   
			$("#dish_list_grid-table").jqGrid("setGridParam", {
                postData: {queryJsonString:""} //发送数据
            }).trigger("reloadGrid");
		}
	}).navButtonAdd("#dish_list_grid-pager",{
        caption: "",
        buttonicon:"fa icon-cogs",
        onClickButton : function (){
            jQuery("#dish_list_grid-table").jqGrid("columnChooser",{
                done: function(perm, cols){
                    $("#dish_list_grid-table").jqGrid("setGridWidth", $("#dish_list_grid-div").width());
                    dish_list_exportExcelIndex = perm;
                }
            });
        }
    });

	$(window).on("resize.jqGrid", function () {
		$("#dish_list_grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);
		$("#dish_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-10-
		$("#dish_list_yy").outerHeight(true)-$("#dish_list_fixed_tool_div").outerHeight(true)-
		$("#dish_list_grid-pager").outerHeight(true)-
		$("#gview_dish_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
	});
	$(window).triggerHandler("resize.jqGrid");
	
	/**打开添加页面*/
	function dish_list_openAddPage(){
		$.post(context_path + "/dish/toAdd.do", {}, function (str){
			$queryWindow=layer.open({
			    title : "盘具添加", 
		    	type:1,
		    	skin : "layui-layer-molv",
		    	area : "600px",
		    	shade : 0.6, //遮罩透明度
			    moveType : 1, //拖拽风格，0是默认，1是传统拖动
			    anim : 2,
			    content : str,
			    success: function (layero, index) {
	                layer.closeAll('loading');
	            }
			});
		});
	}

	/**打开编辑页面*/
	function dish_list_openEditPage(){
		var selectAmount = getGridCheckedNum("#dish_list_grid-table");
		if(selectAmount==0){
			layer.msg("请选择一条记录！",{icon:2});
			return;
		}else if(selectAmount>1){
			layer.msg("只能选择一条记录！",{icon:8});
			return;
		}
		layer.load(2);
		$.post(context_path+'/dish/toAdd.do', {
		    id:jQuery("#dish_list_grid-table").jqGrid("getGridParam", "selrow")
        }, function(str){
			$queryWindow = layer.open({
				title : "盘具编辑",
				type: 1,
			    skin : "layui-layer-molv",
			    area : "600px",
				shade: 0.6, //遮罩透明度
				moveType: 1, //拖拽风格，0是默认，1是传统拖动
				content: str,//注意，如果str是object，那么需要字符拼接。
				success:function(layero, index){
					layer.closeAll("loading");
				}
			});
		}).error(function() {
			layer.closeAll();
	    	layer.msg("加载失败！",{icon:2});
		});
	}
	
	/**删除功能*/
	function dish_list_deleteDish(){
		var checkedNum = getGridCheckedNum("#dish_list_grid-table", "id");  //选中的数量
	    if (checkedNum == 0) {
	    	layer.alert("请选择一个要删除的盘具！");
	    } else {
	        var ids = jQuery("#dish_list_grid-table").jqGrid("getGridParam", "selarrrow");
	        layer.confirm("确定删除选中的盘具？", function() {
	    		$.ajax({
	    			type : "POST",
	    			url : context_path + "/dish/deleteDish.do?ids="+ids ,
	    			dataType : "json",
	    			cache : false,
	    			success : function(data) {
	    				layer.closeAll();
	    				if (Boolean(data.result)) {
	    					layer.msg(data.msg, {icon: 1,time:1000});
                        }else{
	    					layer.msg(data.msg, {icon: 7,time:1000});
                        }
                        dish_list_grid.trigger("reloadGrid");  //重新加载表格
	    			}
	    		});
	    	});
	    }  
	}
	
	/**
	 * 查询按钮点击事件
	 */
	function dish_list_queryOk(){
		 var queryParam = iTsai.form.serialize($("#dish_list_queryForm"));
		 //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
		 dish_list_queryByParam(queryParam);
	}
	
	function dish_list_queryByParam(jsonParam) {
	    iTsai.form.deserialize($("#dish_list_hiddenQueryForm"), jsonParam);
	    var queryParam = iTsai.form.serialize($("#dish_list_hiddenQueryForm"));
	    var queryJsonString = JSON.stringify(queryParam); 
	    $("#dish_list_grid-table").jqGrid("setGridParam", {
            postData: {queryJsonString: queryJsonString}
        }).trigger("reloadGrid");
	 }
	
	/**重置清空查询条件*/
	function dish_list_reset(){
		 iTsai.form.deserialize($("#dish_list_queryForm"),dish_list_queryForm_data); 
		 dish_list_queryByParam(dish_list_queryForm_data);
	}

	/**导出excel*/
	function dish_list_toExcel(){
	    $("#dish_list_hiddenForm #dish_list_queryCode").val($("#dish_list_queryForm #dish_list_code").val());
	    $("#dish_list_hiddenForm #dish_list_ids").val(jQuery("#dish_list_grid-table").jqGrid("getGridParam", "selarrrow"));
        $("#dish_list_hiddenForm #dish_list_queryExportExcelIndex").val(dish_list_exportExcelIndex);
	    $("#dish_list_hiddenForm").submit();	
	}

	/**同步数据*/
	function dish_list_synchronousdata(){
		$.ajax({
            type: "GET",
            url: context_path+"/dish/synchronousdata.do",
            dataType: "json",
            success: function(data){
            	if(data.result){
					layer.msg(data.msg,{icon:1,time:1200});
                    dish_list_grid.trigger("reloadGrid");
				}else{
					layer.msg(data.msg,{icon:2,time:1200});
				}	
	        },
            error:function(XMLHttpRequest){
                alert(XMLHttpRequest.readyState);
                alert("出错啦！！！");
            }
        });
	}

</script>