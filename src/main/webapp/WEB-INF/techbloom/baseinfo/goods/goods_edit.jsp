<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<div id="goods_edit_page" class="row-fluid" style="height: inherit;">
	<form id="goods_edit_goodsForm" class="form-horizontal" style="overflow: auto; height: calc(100% - 70px);">
		<input type="hidden" id="goods_edit_id" name="id" value="${shelf.id}">
		<div class="control-group">
			<label class="control-label" for="goods_edit_factoryCode">厂区：</label>
			<div class="controls required">
				<input class="span11 select2_input " type="text" name="factoryCode" id="goods_edit_factoryCode" value="${shelf.factoryCode}"
					   placeholder="厂区"/>
				<input type="hidden" id="goods_edit_factoryid" value="${shelf.factoryCode}">
                <input type="hidden" id="goods_edit_factoryno" value="${shelf.factoryName}">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="goods_edit_warehouse" >仓库：</label>
			<div class="controls required">
				<input class="span11 select2_input" type = "text" id="goods_edit_warehouse" name="warehouseCode" value="${shelf.warehouseCode}"
					   placeholder="仓库"/>
				<input type="hidden" id="goods_edit_wid" value="${shelf.warehouseCode}">
				<input type="hidden" id="goods_edit_wname" value="${shelf.warehouseName}">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="goods_edit_area" >库区：</label>
			<div class="controls required">
				<input class="span11 select2_input" type = "text" id="goods_edit_area" name="area" value="${shelf.area}"
					   placeholder="库区"/>
				<input type="hidden" id="goods_edit_aid" value="${shelf.area}">
				<input type="hidden" id="goods_edit_aname" value="${shelf.areaName}">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="goods_edit_name">库位编码：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="goods_edit_name" name="code" value="${shelf.code}" placeholder="编码名称">
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="goods_edit_locationdesc">库位描述：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="goods_edit_locationdesc" name="locationDesc" value="${shelf.locationDesc}"
						   placeholder="编码名称">
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="goods_edit_mincapacity">最小库存量：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="goods_edit_mincapacity" name="minCapacity" value="${shelf.minCapacity}"
						   placeholder="最小库存数量">
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="goods_edit_maxcapacity">最大库存量：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="goods_edit_maxcapacity" name="maxCapacity" value="${shelf.maxCapacity}"
						   placeholder="最大库存数量">
				</div>
			</div>
		</div>
	</form>
	<div class="field-button" style="text-align: center;border-top: 0px;margin: 15px auto;">
		<span class="btn btn-info" onclick="saveForm();">
		   <i class="ace-icon fa fa-check bigger-110"></i>保存
		</span>
		<span class="btn btn-danger" onclick="layer.closeAll();">
		   <i class="icon-remove"></i>&nbsp;取消
		</span>
	</div>
</div>

<script type="text/javascript" src="<%=path%>/static/js/techbloom/selectMultip.js"></script>
<script type="text/javascript">
	let factoryCodeId = $("#goods_edit_goodsForm #goods_edit_factoryCode").val();
	//渲染多选框
	selectMultip.register();

	$("#goods_edit_goodsForm").validate({
		rules:{
            "factoryCode":{
                required:true
            },
            "warehouseCode":{
                required:true
            },
			"code":{
				required:true
			},
            "area":{
                required:true
            },
			"minCapacity":{
				required:true
			},
			"maxCapacity":{
				required:true
			}
  		},
  		messages:{
			"factoryCode":{
				required:"请选择厂区"
			},
            "warehouseCode":{
                required:"请选择仓库！"
            },
            "code":{
                required:"请输入名称！"
            },
            "area":{
                required:"请选择库区！"
            },
			"minCapacity":{
				required:"请输入最小库存量！"
			},
			"maxCapacity":{
				required:"请输入最大库存量！"
			}
  		},
  		errorClass: "help-inline",
		errorElement: "div",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
		}
  	});

	//确定按钮点击事件
    function saveForm() {
        if ($("#goods_edit_goodsForm").valid()) {
            saveGoodsInfo($("#goods_edit_goodsForm").serialize());
		}
    }

  	//保存/修改用户信息
    function saveGoodsInfo(bean) {
        $.ajax({
           url: context_path + "/goods/saveGoods",
           type: "POST",
           data: bean,
           dataType: "JSON",
           success: function (data) {
			   iTsai.form.deserialize($("#goods_list_hiddenForm"), iTsai.form.serialize($("#goods_list_hiddenQueryForm")));
			   if (Boolean(data.result)) {
                   layer.msg(data.msg, {icon: 1});
                   //关闭当前窗口
                   layer.close($queryWindow);
                   //刷新列表
                   $("#goods_list_grid-table").jqGrid('setGridParam', {
						postData: {queryJsonString: JSON.stringify(iTsai.form.serialize($("#goods_list_hiddenQueryForm")))}
					}).trigger("reloadGrid");
               } else {
                   layer.alert(data.msg, {icon: 2});
               }
           },
           error:function(XMLHttpRequest){
				alert(XMLHttpRequest.readyState);
				alert("出错啦！！！");
			}
       });
    }

  	//厂区下拉框初始化
    $("#goods_edit_goodsForm #goods_edit_factoryCode").select2({
        placeholder: "选择部门",
        minimumInputLength:0,   //至少输入n个字符，才去加载数据
        allowClear: true,  //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches:"没有结果",
        formatSearching:"搜索中...",
        formatAjaxError:"加载出错啦！",
        ajax : {
            url: context_path+"/factoryArea/getFactoryList",
            type:"POST",
            dataType : 'json',
            delay : 250,
            data: function (term,pageNo) {     //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term,    //联动查询的字符
                    pageSize: 15,    //一次性加载的数据条数
                    pageNo:pageNo    //页码
                }
            },
            results: function (data,pageNo) {
				const res = data.result;
				if(res.length>0){   //如果没有查询到数据，将会返回空串
					const more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results:res,more:more
                    };
                }else{
                    return {
                        results:{}
                    };
                }
            },
            cache : true
        }

    });
  	
    
    $("#goods_edit_goodsForm #goods_edit_factoryCode").on("change",function(e){
    	factoryCodeId = $("#goods_edit_goodsForm #goods_edit_factoryCode").val();
    });

  	//点击编辑的默认数据
    if($("#goods_edit_goodsForm #goods_edit_factoryid").val()!=""){
        $("#goods_edit_goodsForm #goods_edit_factoryCode").select2("data", {
            id: $("#goods_edit_goodsForm #goods_edit_factoryid").val(),
            text: $("#goods_edit_goodsForm #goods_edit_factoryno").val()
        });
    }
  	
     //仓库(根据厂区限定范围)
    $("#goods_edit_goodsForm #goods_edit_warehouse").select2({
        placeholder: "选择仓库",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        //multiple: true,//多选
        ajax: {
            url: context_path + "/car/selectWarehouse",
            type: "POST",
            dataType: 'json',
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    factoryCodeId:factoryCodeId
                }
            },
            results: function (data, pageNo) {
				const res = data.result;
				if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = pageNo < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });

    //库区
    $("#goods_edit_goodsForm #goods_edit_area").select2({
        placeholder: "选择库区",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        //multiple: true,//多选
        ajax: {
            url: context_path + "/area/getAreaList",
            type: "POST",
            dataType: 'json',
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    factoryCodeId:factoryCodeId
                }
            },
            results: function (data, pageNo) {
                const res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = pageNo < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });

    if($("#goods_edit_goodsForm #goods_edit_wid").val()!==""){
        $("#goods_edit_goodsForm #goods_edit_warehouse").select2("data", {
            id: $("#goods_edit_goodsForm #goods_edit_wid").val(),
            text: $("#goods_edit_goodsForm #goods_edit_wname").val()
        });
    }

</script>