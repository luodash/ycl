<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    const context_path = '<%=path%>';
</script>
<!-- 库位管理页面 -->
<body style="overflow:hidden;">
<div id="goods_list_grid-div">
    <!-- 隐藏区域：存放查询条件 -->
    <form id="goods_list_hiddenForm" action="<%=path%>/goods/materialExcel" method="POST"  style="display:none;">
        <input name="ids" id="goods_list_ids" value="" />
        <input name="queryFactoryCode" id="goods_list_queryFactoryCode" value="">
        <input name="queryWarehouse" id="goods_list_queryWarehouse" value="">
        <input name="queryCode" id="goods_list_queryCode" value="">
        <input name="queryExportExcelIndex" id="goods_list_queryExportExcelIndex" value=""/>
    </form>
    <form id="goods_list_hiddenQueryForm" style="display:none;">
        <input name="factoryCode" value="">
        <input name="warehouse" value="">
        <input name="code" value="" />
    </form>
    <c:if test="${operationCode.webSearch==1}">
    <div class="query_box" id="goods_list_yy" title="查询选项">
        <form id="goods_list_queryForm" style="max-width:100%;">
            <ul class="form-elements">
            	<li class="field-group field-fluid3">
					<label class="inline" for="goods_list_factoryCode" style="margin-right:20px;width: 100%;">
						 <span class="form_label" style="width:80px;">厂区：</span>
						 <input id="goods_list_factoryCode" name="factoryCode" type="text" style="width: calc(100% - 85px);" placeholder="厂区">
					</label>
				</li>
				<li class="field-group field-fluid3">
					 <label class="inline" for="goods_list_warehouse" style="margin-right:20px;width: 100%;">
						 <span class="form_label" style="width:80px;">仓库：</span>
						 <input id="goods_list_warehouse" name="warehouse" type="text" style="width:  calc(100% - 85px);" placeholder="仓库">
					 </label>
				 </li>
                <li class="field-group field-fluid3">
                    <label class="inline" for="goods_list_code" style="margin-right:20px;width:100%;">
                        <span class='form_label' style="width:80px;">库位编号：</span>
                        <input type="text" name="code" id="goods_list_code" value="" style="width: calc(100% - 85px);" placeholder="库位编号">
                    </label>
                </li>
            </ul>
            <div class="field-button" style="">
                <div class="btn btn-info" onclick="goods_list_queryOk();">
                    <i class="ace-icon fa fa-check bigger-110"></i>查询
                </div>
                <div class="btn" onclick="goods_list_reset();"><i class="ace-icon icon-remove"></i>重置</div>
                <!-- <a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a> -->
            </div>
        </form>
    </div>
    </c:if>
    <div id="goods_list_fixed_tool_div" class="fixed_tool_div">
        <div id="goods_list_toolbar_" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="goods_list_grid-table" style="width:100%;height:100%;"></table>
    <div id="goods_list_grid-pager"></div>
</div>
</body>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript" src="<%=path%>/static/js/techbloom/wms/baseinfo/goods_list.js"></script>
<script type="text/javascript">
    const context_path = '<%=path%>';
    let goods_list_oriData;      //表格数据
    let goods_list_grid;        //表格对象
    const goods_list_dynamicDefalutValue = "cbe154ed57f14f8e803c4d0982a4d17c";
    let goods_list_factoryCodeId;
    let goods_list_exportExcelIndex;

    $("input").keypress(function (e) {
        if (e.which === 13) {
            goods_list_queryOk();
        }
    });

    $(function  (){
        $(".toggle_tools").click();
    });

    $("#goods_list_toolbar_").iToolBar({
        id:"goods_list_tb_01",
        items:[
            {label: "添加",hidden:"${operationCode.webAdd}"==="1", onclick:goods_list_openAddPage, iconClass:'glyphicon-plus'},
            {label: "编辑",hidden:"${operationCode.webEdit}"==="1", onclick: goods_list_openEditPage, iconClass:'glyphicon-pencil'},
            {label: "删除",hidden:"${operationCode.webDel}"==="1", onclick: goods_list_deletes, iconClass:'glyphicon-trash'},
            {label: "同步数据", hidden:"${operationCode.webTbsj}"==="1",onclick: goods_list_synchronousdata, iconClass:'icon-refresh'},
            {label: "不可复用",hidden:"${operationCode.webUnmultiplexing}"==="1", onclick:function(){goods_list_unmltiplexing();},iconClass:'icon-lock'},
            {label: "可复用", hidden:"${operationCode.webMultiplexing}"==="1",onclick:function(){goods_list_mltiplexing();},iconClass:'icon-flag'},
            {label: "全部释放",hidden:"${operationCode.webAllclear}"==="1", onclick: goods_list_allRelease, iconClass:'icon-ok-sign'},
            {label: "关闭库位", hidden:"${operationCode.webInvalid}"==="1",onclick:function(){goods_list_disMiss();},iconClass:'icon-remove'},
            {label: "释放库位",hidden:"${operationCode.webEffective}"==="1", onclick:function(){goods_list_isStart();},iconClass:'icon-ok'},
            {label: "废弃库位",hidden:"${operationCode.webDiscardShelf}"==="1", onclick:function(){goods_list_discard();},iconClass:'icon-ban-circle'},
            {label: "激活库位",hidden:"${operationCode.webActiveShelf}"==="1", onclick:function(){goods_list_active();},iconClass:'icon-ok-circle'},
            {label:"导出",hidden:"${operationCode.webExport}"==="1",onclick:function(){goods_list_toExcel();},iconClass:' icon-share'}
        ]
    });

    $(function(){
        //初始化表格
        goods_list_grid = jQuery("#goods_list_grid-table").jqGrid({
            url : context_path + "/goods/list.do",
            datatype : "json",
            colNames : [ "主键","库位编号","库位描述","厂区编码","厂区名称","仓库编码","仓库名称","库区名称","状态","是否复用","最小库存量","最大库存量"],
            colModel : [
                {name : "id",index : "id",hidden:true},
                {name : "code",index : "code",width : 23},
                {name : "locationDesc",index : "location_desc",width : 43},
                {name : "factoryCode",index : "factoryCode",width : 13},
                {name : "factoryName",index : "factoryName",width : 33},
                {name : "warehouseCode",index : "warehouseCode",width : 13},
                {name : "warehouseName",index : "warehouseName",width : 33},
                {name : "areaName",index : "areaName",width : 33},
                {name : "state",index : "state",width : 13,formatter:function(cellValu,option,rowObject){
		                if(cellValu===1){
		                    return '<span style="color:green;font-weight:bold;">可用</span>';
		                }else if(cellValu===0){
		                    return '<span style="color:red;font-weight:bold;">不可用</span>';
		                }else if(cellValu===2){
                            return '<span style="color:orange;font-weight:bold;">分支电缆专用</span>';
                        }else if(cellValu===199){
                            return '<span style="color:grey;font-weight:bold;">禁用（废弃）</span>';
                        }
	            	}
                },
                {name : "repeatOccupancy",index : "repeat_occupancy",width : 15,
                    formatter:function(cellValu,option,rowObject){
                        if(cellValu===1){
                            return '<span style="color:green;font-weight:bold;">是</span>';
                        } if(cellValu===0){
                            return '<span style="color:red;font-weight:bold;">否</span>';
                        }
                    }
                },
                {name : "minCapacity",index : "min_capacity",width : 23},
                {name : "maxCapacity",index : "min_capacity",width : 22},
            ],
            rowNum : 20,
            rowList : [ 10, 20, 30 ],
            pager : "#goods_list_grid-pager",
            sortname : "id",
            sortorder : "asc",
            altRows: true,
            viewrecords : true,
            autowidth:true,
            multiselect:true,
            multiboxonly: true,
            beforeRequest:function (){
                dynamicGetColumns(goods_list_dynamicDefalutValue,"goods_list_grid-table",$(window).width()-$("#sidebar").width() -7);
                //重新加载列属性
            },
            loadComplete : function(data){
                const table = this;
                setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
                goods_list_oriData = data;
            },
            emptyrecords: "没有相关记录",
            loadtext: "加载中...",
            pgtext : "页码 {0} / {1}页",
            recordtext: "显示 {0} - {1}共{2}条数据"
        });
        //在分页工具栏中添加按钮
        jQuery("#goods_list_grid-table").navGrid("#goods_list_grid-pager", {edit:false,add:false,del:false,search:false,refresh:false})
        .navButtonAdd('#goods_list_grid-pager',{
            caption:"",
            buttonicon:"ace-icon fa fa-refresh green",
            onClickButton: function(){
                $("#goods_list_grid-table").jqGrid("setGridParam", {
                    postData: {jsonString:""} //发送数据
                }).trigger("reloadGrid");
            }
        }).navButtonAdd("#goods_list_grid-pager",{
            caption: "",
            buttonicon:"fa icon-cogs",
            onClickButton : function (){
                jQuery("#goods_list_grid-table").jqGrid("columnChooser",{
                    done: function(perm, cols){
                        dynamicColumns(cols,goods_list_dynamicDefalutValue);
                        $("#goods_list_grid-table").jqGrid("setGridWidth", $("#goods_list_grid-div").width());
                        goods_list_exportExcelIndex = perm;
                    }
                });
            }
        });
        $(window).on("resize.jqGrid", function () {
            $("#goods_list_grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);

            $("#goods_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-10- $("#goods_list_yy").outerHeight(true)-
            $("#goods_list_fixed_tool_div").outerHeight(true)- $("#goods_list_grid-pager").outerHeight(true)-
            $("#gview_goods_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
        });
        $(window).triggerHandler("resize.jqGrid");
    });

    const goods_list_queryForm_data = iTsai.form.serialize($("#goods_list_queryForm"));

    function goods_list_queryOk(){
        //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
        goods_list_queryByParam(iTsai.form.serialize($("#goods_list_queryForm")));
    }

    //重置
    function goods_list_reset(){
    	goods_list_factoryCodeId='';
    	$("#goods_list_queryForm #goods_list_warehouse").select2("val","");
    	$("#goods_list_queryForm #goods_list_factoryCode").select2("val","");
        iTsai.form.deserialize($("#goods_list_queryForm"),goods_list_queryForm_data);
        goods_list_queryByParam(goods_list_queryForm_data);
    }

    function goods_list_queryByParam(jsonParam) {
        iTsai.form.deserialize($("#goods_list_hiddenQueryForm"), jsonParam);
        const queryParam = iTsai.form.serialize($("#goods_list_hiddenQueryForm"));
        const queryJsonString = JSON.stringify(queryParam);
        $("#goods_list_grid-table").jqGrid("setGridParam", {
            postData: {queryJsonString: queryJsonString}
        }).trigger("reloadGrid");
    }

    //导出功能
    function goods_list_toExcel(){
        $("#goods_list_hiddenForm #goods_list_ids").val(jQuery("#goods_list_grid-table").jqGrid("getGridParam", "selarrrow"));
        $("#goods_list_hiddenForm #goods_list_queryFactoryCode").val($("#goods_list_queryForm #goods_list_factoryCode").val());
        $("#goods_list_hiddenForm #goods_list_queryWarehouse").val($("#goods_list_queryForm #goods_list_warehouse").val());
        $("#goods_list_hiddenForm #goods_list_queryCode").val($("#goods_list_queryForm #goods_list_code").val());
        $("#goods_list_hiddenForm #goods_list_queryExportExcelIndex").val(goods_list_exportExcelIndex);
        $("#goods_list_hiddenForm").submit();
    }

    /**关闭库位*/
    function goods_list_disMiss() {
        const selectAmount = getGridCheckedNum("#goods_list_grid-table");
        if(selectAmount<1){
            layer.msg("请选择要关闭的库位！",{icon:2,time:1200});
        }else{
            $.post(context_path+'/goods/disMiss.do?ids='+jQuery("#goods_list_grid-table").jqGrid("getGridParam", "selarrrow"),{
            },function success(){
                layer.msg('更新成功',{icon:1,time:1200});
                jQuery("#goods_list_grid-table").trigger("reloadGrid");
            });
        }
    }

    /**废弃库位*/
    function goods_list_discard() {
        var selectAmount = getGridCheckedNum("#goods_list_grid-table");
        if(selectAmount<1){
            layer.msg("请选择要废弃的库位！",{icon:2,time:1200});
        }else{
            $.post(context_path+'/goods/discardShelf.do?ids='+jQuery("#goods_list_grid-table").jqGrid("getGridParam", "selarrrow"),{
            },function success(){
                layer.msg('更新成功',{icon:1,time:1200});
                jQuery("#goods_list_grid-table").trigger("reloadGrid");
            });
        }
    }

    /**激活库位*/
    function goods_list_active() {
        var selectAmount = getGridCheckedNum("#goods_list_grid-table");
        if(selectAmount<1){
            layer.msg("请选择要激活的库位！",{icon:2,time:1200});
        }else{
            $.post(context_path+'/goods/activeShelf.do?ids='+jQuery("#goods_list_grid-table").jqGrid("getGridParam", "selarrrow"),{
            },function success(){
                layer.msg('更新成功',{icon:1,time:1200});
                jQuery("#goods_list_grid-table").trigger("reloadGrid");
            });
        }
    }

    /**释放库位*/
    function goods_list_isStart() {
        var selectAmount = getGridCheckedNum("#goods_list_grid-table");
        if(selectAmount<1){
            layer.msg("请选择要释放的库位！",{icon:2,time:1200});
        }else{
            $.post(context_path+'/goods/isStart.do?ids='+jQuery("#goods_list_grid-table").jqGrid("getGridParam", "selarrrow"),{
            },function success(){
                layer.msg('更新成功',{icon:1,time:1200});
                jQuery("#goods_list_grid-table").trigger("reloadGrid");
            });
        }
    }

    /**库位一键释放*/
    function goods_list_allRelease() {
        layer.confirm("确定设置所有库位吗？", function() {
            $.post(context_path+'/goods/allRelease.do',{},function success(){
                layer.msg('更新成功',{icon:1,time:1200});
                jQuery("#goods_list_grid-table").trigger("reloadGrid");
            });
        });
    }
    
    /**同步数据*/
	function goods_list_synchronousdata(){
		$.ajax({
            type: "GET",
            url: context_path+"/goods/synchronousdata.do",
            dataType: "json",
            success: function(data){
            	if(data.result){
					layer.msg(data.msg,{icon:1,time:1200});
                    goods_list_grid.trigger("reloadGrid");
				}else{
					layer.msg(data.msg,{icon:2,time:1200});
				}	
	        },
            error:function(XMLHttpRequest){
                alert(XMLHttpRequest.readyState);
                alert("出错啦！！！");
            }
        });
	}
    
    $("#goods_list_queryForm #goods_list_warehouse").select2({
        placeholder: "选择仓库",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/car/selectWarehouse",
            type: "POST",
            dataType: 'json',
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    factoryCodeId:goods_list_factoryCodeId
                }
            },
            results: function (data, pageNo) {
                const res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    const more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });
    
  	//厂区
    $("#goods_list_queryForm #goods_list_factoryCode").select2({
        placeholder: "选择厂区",
        minimumInputLength:0,   //至少输入n个字符，才去加载数据
        allowClear: true,  //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches:"没有结果",
        formatSearching:"搜索中...",
        formatAjaxError:"加载出错啦！",
        ajax : {
            url: context_path+"/factoryArea/getFactoryList",
            type:"POST",
            dataType : 'json',
            delay : 250,
            data: function (term,pageNo) {     //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term,    //联动查询的字符
                    pageSize: 15,    //一次性加载的数据条数
                    pageNo:pageNo    //页码
                }
            },
            results: function (data,pageNo) {
                const res = data.result;
                if(res.length>0){   //如果没有查询到数据，将会返回空串
                    const more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results:res,more:more
                    };
                }else{
                    return {
                        results:{}
                    };
                }
            },
            cache : true
        }
    });
  	
    $("#goods_list_queryForm #goods_list_factoryCode").on("change",function(e){
    	goods_list_factoryCodeId = $("#goods_list_queryForm #goods_list_factoryCode").val();
    });
  	
    $("#goods_list_queryForm #goods_list_factoryCode").on("change.select2",function(){
        $("#goods_list_queryForm #goods_list_factoryCode").trigger("keyup")}
    );
    
</script>