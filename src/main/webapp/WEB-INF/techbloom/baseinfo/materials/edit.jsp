<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
%>
<div id="material_edit_page" class="row-fluid" style="height: inherit;">
    <form id="wms_material_editForm" class="form-horizontal" style="overflow: auto; height: calc(100% - 70px);">
        <input type="hidden" id="material_edit_id" name="id" value="${material.id}">
        <div class="row" style="margin:0;padding:0;">
            <div class="control-group span6">
                <label class="control-label" for="material_edit_code">物料编码：</label>
                <div class="controls">
                    <div class="span12">
                        <input class="span10" type="text" id="material_edit_code" name="code" value="${material.code}"
                               placeholder="物料编码" readonly unselectable="on"/>
                    </div>
                </div>
            </div>
            <div class="control-group span6">
                <label class="control-label" for="material_edit_code">主单位：</label>
                <div class="controls">
                    <div class="span12">
                        <input class="span10" type="text" id="material_edit_mainUnit" name="mainUnit" value="${material.mainUnit}"
                               placeholder="主单位" readonly/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin:0;padding:0;">
            <div class="control-group span6">
                <label class="control-label" for="material_edit_code">物料名称：</label>
                <div class="controls">
                    <div class="span12">
                        <input class="span10" type="text" id="material_edit_name" name="name" value="${material.name}"
                               placeholder="物料名称" readonly unselectable="on"/>
                    </div>
                </div>
            </div>
            <div class="control-group span6">
                <label class="control-label" for="material_edit_code">辅单位：</label>
                <div class="controls">
                    <div class="span12">
                        <input class="span10" type="text" id="material_edit_subUnit" name="subUnit" value="${material.subUnit}"
                               placeholder="辅单位" readonly/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin:0;padding:0;">
            <div class="control-group span6">
                <label class="control-label" for="material_edit_code">最大库存量：</label>
                <div class="controls">
                    <div class="span12">
                        <input class="span10" type="text" id="material_edit_max_stock" name="maxStock" value="${material.maxStock}"
                               placeholder="最大库存量"/>
                    </div>
                </div>
            </div>
            <div class="control-group span6">
                <label class="control-label" for="material_edit_code">最小库存量：</label>
                <div class="controls">
                    <div class="span12">
                        <input class="span10" type="text" id="material_edit_min_stock" name="minStock" value="${material.minStock}"
                               placeholder="最小库存量"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin:0;padding:0;">
            <div class="control-group span6">
                <label class="control-label" for="material_edit_code">呆滞时间：</label>
                <div class="controls">
                    <div class="span12">
                        <input class="span10" type="text" id="material_edit_dead_time" name="deadTime" value="${material.deadTime}"
                               placeholder="呆滞时间"/>
                    </div>
                </div>
            </div>
            <div class="control-group span6">
                <label class="control-label" for="material_edit_code">物料说明：</label>
                <div class="controls">
                    <div class="span12">
                        <input class="span10" type="text" id="material_edit_remark" name="remark" value="${material.remark}"
                               placeholder="物料说明" readonly/>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <div class="field-button" style="text-align: center;border-top: 0px;margin: 15px auto;">
		<span class="btn btn-info" onclick="submitForm();">
		   <i class="ace-icon fa fa-check bigger-110"></i>保存
		</span>
        <span class="btn btn-danger" onclick="layer.closeAll();">
		   <i class="icon-remove"></i>&nbsp;取消
		</span>
    </div>
</div>

<script type="text/javascript">

    debugger
    var sdf = "${material.id}";
    $("#wms_material_editForm").validate({
        rules: {
            "name": {
                required: true
            },
            "warehouse": {
                required: true
            }
        },
        messages: {
            "name": {
                required: "请输入名称！"
            },
            "warehouse": {
                required: "请选择仓库！"
            }
        },
        errorClass: "help-inline",
        errorElement: "div",
        highlight: function (element, errorClass, validClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        }
    });

    //确定按钮点击事件
    function submitForm() {
        if ($("#wms_material_editForm").valid()) {
            saveInfo($("#wms_material_editForm").serialize());
        }
    }

    //保存/修改用户信息
    function saveInfo(bean) {
        $.ajax({
            url: context_path + "/materials/save.do",
            type: "POST",
            data: bean,
            dataType: "JSON",
            success: function (data) {
                iTsai.form.deserialize($("#materials_list_queryForm"), iTsai.form.serialize($("#materials_list_hiddenQueryForm")));
                var queryParam = iTsai.form.serialize($("#materials_list_hiddenQueryForm"));
                var queryJsonString = JSON.stringify(queryParam);
                if (Boolean(data.result)) {
                    layer.msg("保存成功！", {icon: 1});
                    //关闭当前窗口
                    layer.close($queryWindow);
                    //刷新列表
                    $("#materials_list_grid-table").jqGrid('setGridParam', {
                        postData: {queryJsonString: queryJsonString}
                    }).trigger("reloadGrid");
                } else {
                    layer.alert("保存失败，请稍后重试！", {icon: 2});
                }
            },
            error: function (XMLHttpRequest) {
                alert(XMLHttpRequest.readyState);
                alert("出错啦！！！");
            }
        });
    }



</script>