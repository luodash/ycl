<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<!-- 物料管理页面 -->
<body style="overflow:hidden;">
<div id="materials_list_grid-div">
    <!-- 隐藏区域：存放查询条件 -->
    <form id="materials_list_hiddenForm" action="<%=path%>/materials/materialExcel" method="POST"  style="display:none;">
        <input name="ids" id="materials_list_ids" value="" />
        <input name="queryName" id="materials_list_queryName" value="">
        <input name="queryExportExcelIndex" id="materials_list_queryExportExcelIndex" value=""/>
    </form>
    <form id="materials_list_hiddenQueryForm" style="display:none;">
        <input name="code" value="" />
        <input name="name" value="" />
        <input name="industry" value="" />
        <input name="category" value="" />
        <input name="gradeTwo" value="" />
        <input name="gradeThree" value="" />
    </form>
    <c:if test="${operationCode.webSearch==1}">
    <div class="query_box" id="materials_list_yy" title="查询选项">
        <form id="materials_list_queryForm" style="max-width:100%;">
            <ul class="form-elements">
                <li class="field-group field-fluid3">
                    <label class="inline" style="margin-right:20px;width:100%;">
                        <span class='form_label' style="width:80px;">物料编码：</span>
                        <input type="text" name="code" id="materials_list_code" value="" style="width: calc(100% - 85px);" placeholder="物料编码">
                    </label>
                </li>
                <li class="field-group field-fluid3">
                    <label class="inline" style="margin-right:20px;width:100%;">
                        <span class='form_label' style="width:80px;">物料名称：</span>
                        <input type="text" name="name" id="materials_list_name" value="" style="width: calc(100% - 85px);" placeholder="物料名称">
                    </label>
                </li>
                <li class="field-group field-fluid3">
                    <label class="inline" for="materials_list_industry" style="margin-right:20px;width: 100%;">
                        <span class="form_label" style="width:80px;">行业区分：</span>
                        <input id="materials_list_industry" name="industry" type="text" style="width:  calc(100% - 85px);" placeholder="行业区分">
                    </label>
                </li>

                <li class="field-group-top field-group field-fluid3">
                    <label class="inline" for="materials_list_category" style="margin-right:20px;width:100%;">
                        <span class='form_label' style="width:80px;">类别区分：</span>
                        <input type="text" name="category" id="materials_list_category" value="" style="width: calc(100% - 85px);" placeholder="类别区分">
                    </label>
                </li>
                <li class="field-group field-fluid3">
                    <label class="inline" for="materials_list_gradeTwo" style="margin-right:20px;width:100%;">
                        <span class='form_label' style="width:80px;">材料二级：</span>
                        <input type="text" name="gradeTwo" id="materials_list_gradeTwo" value="" style="width: calc(100% - 85px);" placeholder="材料二级分类">
                    </label>
                </li>
                <li class="field-group field-fluid3">
                    <label class="inline" for="materials_list_gradeThree" style="margin-right:20px;width: 100%;">
                        <span class="form_label" style="width:80px;">材料三级：</span>
                        <input type="text" name="gradeThree" id="materials_list_gradeThree" style="width:  calc(100% - 85px);" placeholder="材料三级分类">
                    </label>
                </li>
            </ul>
            <div class="field-button" style="">
                <div class="btn btn-info" onclick="materials_list_queryOk();">
                    <i class="ace-icon fa fa-check bigger-110"></i>查询
                </div>
                <div class="btn" onclick="materials_list_reset();"><i class="ace-icon icon-remove"></i>重置</div>
                <a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>
            </div>
        </form>
    </div>
    </c:if>
    <div id="materials_list_fixed_tool_div" class="fixed_tool_div">
        <div id="materials_list_toolbar_" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="materials_list_grid-table" style="width:100%;height:100%;"></table>
    <div id="materials_list_grid-pager"></div>
</div>
</body>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript" src="<%=path%>/static/js/techbloom/wms/baseinfo/material.js"></script>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var materials_list_oriData;      //表格数据
    var materials_list_grid;        //表格对象
    var materials_list_dynamicDefalutValue="cbe154ed57f14f8e803c4d0982a4d17c";
    var materials_list_exportExcelIndex;
    var materials_list_industry;
    var materials_list_category;
    var materials_list_gradeTwo;
    var materials_list_gradeThree;

    $("input").keypress(function (e) {
        if (e.which === 13) {
        }
    });

    $(function  (){
        $(".toggle_tools").click();
    });

    $("#materials_list_toolbar_").iToolBar({
        id:"materials_list_tb_01",
        items:[
            {label: "编辑",hidden:true, onclick: function(){material_edit();}, iconClass:'icon-pencil'},
        	{label: "同步数据",hidden:"${operationCode.webTbsj}"==="1", onclick: materials_list_synchronousdata, iconClass:'icon-refresh'},
    		{label:"导出",hidden:"${operationCode.webExport}"==="1",onclick: function(){materials_list_toExcel();},iconClass:' icon-share'}
    	]
    });

    $(function(){
        //初始化表格
        materials_list_grid = jQuery("#materials_list_grid-table").jqGrid({
            url : context_path + "/materials/list.do",
            datatype : "json",
            colNames : [ "物料主键","物料编码","物料名称","主单位","呆滞时间","最小库存量","最大库存量","状态"],
            colModel : [
                {name : "id",index : "id",hidden:true},
                {name : "code",index : "code",width : 33},
                {name : "name",index : "name",width : 33},
                {name : "mainUnit",index : "MAIN_UNIT",width : 20},
                {name : "deadTime",index : "DEAD_TIME",width : 20},
                {name : "minStock",index : "MIN_STOCK",width : 20},
                {name : "maxStock",index : "MAX_STOCK",width : 20},
                {name : "status",index : "status", width : 20, formatter : function(cellvalue, options, rowObject){
                        if(cellvalue==="Active"){
                            return "<span style=\"color:green;font-weight:bold;\">生效</span>";
                        }else{
                            return "<span style=\"color:red;font-weight:bold;\">失效</span>";
                        }
                    }
                }

            ],
            rowNum : 20,
            rowList : [ 10, 20, 30 ],
            pager : "#materials_list_grid-pager",
            sortname : "id",
            sortorder : "asc",
            altRows: true,
            viewrecords : true,
            //caption : "物料列表",
            autowidth:true,
            multiselect:true,
            multiboxonly: true,
            beforeRequest:function (){
                dynamicGetColumns(materials_list_dynamicDefalutValue,"materials_list_grid-table",$(window).width()-$("#sidebar").width() -7);
                //重新加载列属性
            },
            loadComplete : function(data){
                var table = this;
                setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
                materials_list_oriData = data;
            },
            emptyrecords: "没有相关记录",
            loadtext: "加载中...",
            pgtext : "页码 {0} / {1}页",
            recordtext: "显示 {0} - {1}共{2}条数据"
        });
        //在分页工具栏中添加按钮
        jQuery("#materials_list_grid-table").navGrid("#materials_list_grid-pager",
        {edit:true,add:false,del:false,search:false,refresh:false}).navButtonAdd('#materials_list_grid-pager',{
            caption:"",
            buttonicon:"ace-icon fa fa-refresh green",
            onClickButton: function(){
                $("#materials_list_grid-table").jqGrid("setGridParam", {
                    postData: {jsonString:""} //发送数据
                }).trigger("reloadGrid");
            }
        }).navButtonAdd("#materials_list_grid-pager",{
            caption: "",
            buttonicon:"fa icon-cogs",
            onClickButton : function (){
                jQuery("#materials_list_grid-table").jqGrid("columnChooser",{
                    done: function(perm, cols){
                        dynamicColumns(cols,materials_list_dynamicDefalutValue);
                        $("#materials_list_grid-table").jqGrid("setGridWidth", $("#materials_list_grid-div").width());
                        materials_list_exportExcelIndex = perm;
                    }
                });
            }
        });
        $(window).on("resize.jqGrid", function () {
            $("#materials_list_grid-table").jqGrid("setGridWidth", $("#materials_list_grid-div").width() );
            $("#materials_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-10-
            $("#materials_list_yy").outerHeight(true)-
            $("#materials_list_fixed_tool_div").outerHeight(true)-
            $("#materials_list_grid-pager").outerHeight(true)-
            $("#gview_materials_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
        });
        $(window).triggerHandler("resize.jqGrid");
    });

    var materials_list_queryForm_data = iTsai.form.serialize($("#materials_list_queryForm"));
</script>