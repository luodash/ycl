<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="warehouse_list_grid-div">
    <form id="warehouse_list_hiddenForm" action="<%=path%>/warehouselist/materialExcel" method="POST" style="display: none;">
        <input name="ids" id="warehouse_list_ids" value=""/>
        <input name="queryFactoryCode" id="warehouse_list_queryFactoryCode" value="">
        <input name="queryCode" id="warehouse_list_queryCode" value="">
        <input name="queryName" id="warehouse_list_queryName" value="">
        <input name="queryExportExcelIndex" id="warehouse_list_queryExportExcelIndex" value=""/>
    </form>
    <form id="warehouse_list_hiddenQueryForm" style="display:none;">
        <input name="factoryCode" value=""/>
        <input name="code" value=""/>
        <input name="name" value=""/>
    </form>
    <c:if test="${operationCode.webSearch==1}">
    <div class="query_box" id="warehouse_list_yy" title="查询选项">
        <form id="warehouse_list_queryForm" style="max-width:100%;">
            <ul class="form-elements">
	            <li class="field-group field-fluid3">
					<label class="inline" for="warehouse_list_factoryCode" style="margin-right:20px;width: 100%;">
						 <span class="form_label" style="width:80px;">厂区：</span>
						 <input id="warehouse_list_factoryCode" name="factoryCode" type="text" style="width: calc(100% - 85px);" placeholder="厂区">
					</label>
				</li>
           		 <li class="field-group field-fluid3">
                    <label class="inline" for="warehouse_list_code" style="margin-right:20px;width: 100%;">
                        <span class="form_label" style="width:80px;">仓库编号：</span>
                        <input id="warehouse_list_code" name="code" type="text" style="width: calc(100% - 85px);" placeholder="仓库编号">
                    </label>
                </li>
                <li class="field-group field-fluid3">
                    <label class="inline" for="warehouse_list_name" style="margin-right:20px;width: 100%;">
                        <span class="form_label" style="width:80px;">仓库名称：</span>
                        <input id="warehouse_list_name" name="name" type="text" style="width: calc(100% - 85px);" placeholder="仓库名称">
                    </label>
                </li>
            </ul>
            <div class="field-button" style="">
                <div class="btn btn-info" onclick="warehouse_list_queryOk();">
                    <i class="ace-icon fa fa-check bigger-110"></i>查询
                </div>
                <div class="btn" onclick="warehouse_list_reset();"><i class="ace-icon icon-remove"></i>重置</div>
            </div>
        </form>
    </div>
    </c:if>
    <div id="warehouse_list_fixed_tool_div" class="fixed_tool_div">
        <div id="warehouse_list___toolbar__" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="warehouse_list_grid-table" style="width:100%;height:100%;"></table>
    <div id="warehouse_list_grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
    var warehouse_list_oriData;
    var warehouse_list_grid;
    var warehouse_list_dynamicDefalutValue = "cbe154ed57f14f8e803c4d0982a4d18c";
    var warehouse_list_exportExcelIndex;

    $("input").keypress(function (e) {
        if (e.which == 13) {
            warehouse_list_queryOk();
        }
    });

    $(function  (){
        $(".toggle_tools").click();
    });

    $("#warehouse_list___toolbar__").iToolBar({
        id: "warehouse_list___tb__01",
        items: [
		    {label: "同步数据",hidden:"${operationCode.webTbsj}"==="1", onclick: warehouse_list_synchronousdata, iconClass:'icon-refresh'},
            {label: "不可复用",hidden:"${operationCode.webUnmultiplexing}"==="1", onclick:function(){warehouse_list_unmltiplexing();},iconClass:'icon-lock'},
            {label: "可复用",hidden:"${operationCode.webMultiplexing}"==="1", onclick:function(){warehouse_list_mltiplexing();},iconClass:'icon-flag'},
		    {label: "禁用",hidden:"${operationCode.webDisable}"==="1", onclick:function(){warehouse_list_disMiss();},iconClass:'icon-remove'},
		    {label: "启用",hidden:"${operationCode.webEnable}"==="1", onclick:function(){warehouse_list_isStart();},iconClass:'icon-ok'},
		    {label: "导出",hidden:"${operationCode.webExport}"==="1", onclick:function(){warehouse_list_toExcel();},iconClass:'icon-share'}
	    ]
    });

    var warehouse_list_queryForm_data = iTsai.form.serialize($("#warehouse_list_queryForm"));

    warehouse_list_grid = jQuery("#warehouse_list_grid-table").jqGrid({
        url : context_path + "/warehouselist/list.do",
        datatype : "json",
        colNames : [ "主键","仓库编号", "仓库名称","厂区编码", "厂区名称","状态", "是否复用"],
        colModel : [
            {name : "id",index : "a.id",hidden:true},
            {name : "code",index : "a.code",width : 30},
            {name : "name",index : "a.name",width : 60},
            {name : "factoryCode",index : "a.factorycode",width : 30},
            {name : "factoryName",index : "a.factorycode",width : 60},
            {name : "state",index : "a.state",width : 30, formatter:function(cellValu,option,rowObject){
	                if(cellValu=='1'){
	                    return '<span style="color:green;font-weight:bold;">可用</span>';
	                } if(cellValu=='0'){
	                    return '<span style="color:red;font-weight:bold;">不可用</span>';
	                }
            	}
            },
            {name : "repeatOccupancy",index : "a.repeat_occupancy,a.id",width : 30, formatter:function(cellValu,option,rowObject){
                    if(cellValu===1){
                        return '<span style="color:green;font-weight:bold;">是</span>';
                    } if(cellValu===0){
                        return '<span style="color:red;font-weight:bold;">否</span>';
                    }
                }
            }
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : "#warehouse_list_grid-pager",
        sortname : "a.state",
        sortorder : "desc",
        altRows: true,
        viewrecords : true,
        autowidth:true,
        multiselect:true,
        multiboxonly: true,
        beforeRequest:function (){
            dynamicGetColumns(warehouse_list_dynamicDefalutValue,"goods_list_grid-table",$(window).width()-$("#sidebar").width() -7);
            //重新加载列属性
        },
        loadComplete : function(data){
            var table = this;
            setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            warehouse_list_oriData = data;
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
    });

    jQuery("#warehouse_list_grid-table").navGrid("#warehouse_list_grid-pager", {edit:false,add:false,del:false,search:false,refresh:false})
    .navButtonAdd("#warehouse_list_grid-pager",{
        caption:"",
        buttonicon:"fa fa-refresh green",
        onClickButton: function(){
            $("#warehouse_list_grid-table").jqGrid("setGridParam", {
                postData: {queryJsonString:""} //发送数据
            }).trigger("reloadGrid");
        }
    }).navButtonAdd("#warehouse_list_grid-pager",{
        caption: "",
        buttonicon:"fa icon-cogs",
        onClickButton : function (){
            jQuery("#warehouse_list_grid-table").jqGrid("columnChooser",{
                done: function(perm, cols){
                    dynamicColumns(cols,warehouse_list_dynamicDefalutValue);
                    $("#warehouse_list_grid-table").jqGrid("setGridWidth", $("#warehouse_list_grid-div").width());
                    warehouse_list_exportExcelIndex = perm;
                }
            });
        }
    });

    $(window).on("resize.jqGrid", function () {
        $("#warehouse_list_grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);
        $("#warehouse_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-10- $("#warehouse_list_yy").outerHeight(true)-
        $("#warehouse_list_fixed_tool_div").outerHeight(true)- $("#warehouse_list_grid-pager").outerHeight(true)-
        $("#gview_warehouse_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
    });
    $(window).triggerHandler("resize.jqGrid");

    /**
     * 查询按钮点击事件
     */
    function warehouse_list_queryOk(){
        var queryParam = iTsai.form.serialize($("#warehouse_list_queryForm"));
        //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
        warehouse_list_queryByParam(queryParam);
    }
    
    function warehouse_list_queryByParam(jsonParam) {
        iTsai.form.deserialize($("#warehouse_list_hiddenQueryForm"), jsonParam);
        var queryParam = iTsai.form.serialize($("#warehouse_list_hiddenQueryForm"));
        var queryJsonString = JSON.stringify(queryParam);
        $("#warehouse_list_grid-table").jqGrid("setGridParam", {
            postData: {queryJsonString: queryJsonString}
        }).trigger("reloadGrid");
    }

    /**重置*/
    function warehouse_list_reset(){
        $("#warehouse_list_queryForm #warehouse_list_factoryCode").select2("val","");
        iTsai.form.deserialize($("#warehouse_list_queryForm"),warehouse_list_queryForm_data);
        warehouse_list_queryByParam(warehouse_list_queryForm_data);
    }

    /**导出Excel*/
    function warehouse_list_toExcel(){
        $("#warehouse_list_hiddenForm #warehouse_list_ids").val(jQuery("#warehouse_list_grid-table").jqGrid("getGridParam", "selarrrow"));
        $("#warehouse_list_hiddenForm #warehouse_list_queryName").val($("#warehouse_list_queryForm #warehouse_list_name").val());
        $("#warehouse_list_hiddenForm #warehouse_list_queryCode").val($("#warehouse_list_queryForm #warehouse_list_code").val());
        $("#warehouse_list_hiddenForm #warehouse_list_queryFactoryCode").val($("#warehouse_list_queryForm #warehouse_list_factoryCode").val());
        $("#warehouse_list_hiddenForm #warehouse_list_queryExportExcelIndex").val(warehouse_list_exportExcelIndex);
        $("#warehouse_list_hiddenForm").submit();
    }

    /**置为不可用*/
    function warehouse_list_disMiss() {
        var selectAmount = getGridCheckedNum("#warehouse_list_grid-table");
        if(selectAmount<1){
            layer.confirm("确定设置所有仓库吗？", function() {
                $.post(context_path+'/warehouselist/disMiss.do?ids='+jQuery("#warehouse_list_grid-table").jqGrid("getGridParam","selarrrow"),{
                },function success(){
                    layer.msg('更新成功',{icon:1,time:1200});
                    jQuery("#warehouse_list_grid-table").trigger("reloadGrid");
                });
            });
        }else{
            $.post(context_path+'/warehouselist/disMiss.do?ids='+jQuery("#warehouse_list_grid-table").jqGrid("getGridParam","selarrrow"),{
            },function success(){
                layer.msg('更新成功',{icon:1,time:1200});
                jQuery("#warehouse_list_grid-table").trigger("reloadGrid");
            });
        }
    }

    /**置为可用*/
    function warehouse_list_isStart() {
        var selectAmount = getGridCheckedNum("#warehouse_list_grid-table");
        if(selectAmount<1){
            layer.confirm("确定设置所有仓库吗？", function() {
                $.post(context_path+'/warehouselist/isStart.do?ids='+jQuery("#warehouse_list_grid-table").jqGrid("getGridParam", "selarrrow"),{
                },function success(){
                    layer.msg('更新成功',{icon:1,time:1200});
                    jQuery("#warehouse_list_grid-table").trigger("reloadGrid");
                });
            });
        }else{
            $.post(context_path+'/warehouselist/isStart.do?ids='+jQuery("#warehouse_list_grid-table").jqGrid("getGridParam","selarrrow"),{
            },function success(){
                layer.msg('更新成功',{icon:1,time:1200});
                jQuery("#warehouse_list_grid-table").trigger("reloadGrid");
            });
        }
    }
    
    /**同步数据*/
	function warehouse_list_synchronousdata(){
		$.ajax({
            type: "GET",
            url: context_path+"/warehouselist/synchronousdata.do",
            dataType: "json",
            success: function(data){
            	if(data.result){
					layer.msg(data.msg,{icon:1,time:1200});
                    warehouse_list_grid.trigger("reloadGrid");
				}else{
					layer.msg(data.msg,{icon:2,time:1200});
				}	
	        },
            error:function(XMLHttpRequest){
                alert(XMLHttpRequest.readyState);
                alert("出错啦！！！");
            }
        });
	}

    /**置为不可复用*/
    function warehouse_list_unmltiplexing() {
        var selectAmount = getGridCheckedNum("#warehouse_list_grid-table");
        if (selectAmount<1){
            layer.msg("请至少选择一条记录！",{icon:2});
            return;
        }

        layer.confirm("确定设置所选仓库吗？", function() {
            $.post(context_path+'/warehouselist/unMltiplexing.do?ids='+jQuery("#warehouse_list_grid-table").jqGrid("getGridParam", "selarrrow"),{
            },function success(){
                layer.msg('更新成功',{icon:1,time:1200});
                jQuery("#warehouse_list_grid-table").trigger("reloadGrid");
            });
        });
    }

    /**置为可复用*/
    function warehouse_list_mltiplexing() {
        var selectAmount = getGridCheckedNum("#warehouse_list_grid-table");
        if (selectAmount<1){
            layer.msg("请至少选择一条记录！",{icon:2});
            return;
        }

        layer.confirm("确定设置所选仓库吗？", function() {
            $.post(context_path+'/warehouselist/mltiplexing.do?ids='+jQuery("#warehouse_list_grid-table").jqGrid("getGridParam", "selarrrow"),{
            },function success(){
                layer.msg('更新成功',{icon:1,time:1200});
                jQuery("#warehouse_list_grid-table").trigger("reloadGrid");
            });
        });
    }

  	/**厂区编号*/
    $("#warehouse_list_factoryCode").select2({
        placeholder: "选择厂区",
        minimumInputLength:0,   //至少输入n个字符，才去加载数据
        allowClear: true,  //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches:"没有结果",
        formatSearching:"搜索中...",
        formatAjaxError:"加载出错啦！",
        ajax : {
            url: context_path+"/factoryArea/getFactoryList",
            type:"POST",
            dataType : 'json',
            delay : 250,
            data: function (term,pageNo) {     //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term,    //联动查询的字符
                    pageSize: 15,    //一次性加载的数据条数
                    pageNo:pageNo    //页码
                }
            },
            results: function (data,pageNo) {
                var res = data.result;
                if(res.length>0){   //如果没有查询到数据，将会返回空串
                    var more = (pageNo*15)<data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results:res,more:more
                    };
                }else{
                    return {
                        results:{}
                    };
                }
            },
            cache : true
        }
    });
  	
    $("#warehouse_list_factoryCode").on("change.select2",function(){
        $("#warehouse_list_factoryCode").trigger("keyup")}
    );
    
</script>