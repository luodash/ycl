<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<!-- 库位管理页面 -->
<body style="overflow:hidden;">
<div id="factoryarea_list_grid-div">
    <!-- 隐藏区域：存放查询条件 -->
    <form id="factoryarea_list_hiddenForm" action="<%=path%>/factoryArea/materialExcel" method="POST"  style="display:none;">
        <input name="ids" id="factoryarea_list_ids" value="" />
        <input name="queryCode" id="factoryarea_list_queryCode" value="">
        <input name="queryName" id="factoryarea_list_queryName" value="">
        <input name="queryExportExcelIndex" id="factoryarea_list_queryExportExcelIndex" value=""/>
    </form>
    <form id="factoryarea_list_hiddenQueryForm" style="display:none;">
    	<input name="code" value="" />
        <!-- 厂区名称 -->
        <input name="name" value="" />
    </form>
    <c:if test="${operationCode.webSearch==1}">
        <div class="query_box" id="factoryarea_list_yy" title="查询选项">
            <form id="factoryarea_list_queryForm" style="max-width:100%;">
                <ul class="form-elements">
                	<li class="field-group field-fluid3">
                        <label class="inline" for="factoryarea_list_code" style="margin-right:20px;width:100%;">
                            <span class='form_label' style="width:80px;">厂区编码：</span>
                            <input type="text" name="code" id="factoryarea_list_code" value="" style="width: calc(100% - 85px);" placeholder="厂区编码">
                        </label>
                    </li>
                    <li class="field-group field-fluid3">
                        <label class="inline" for="factoryarea_list_name" style="margin-right:20px;width:100%;">
                            <span class='form_label' style="width:80px;">厂区名称：</span>
                            <input type="text" name="name" id="factoryarea_list_name" value="" style="width: calc(100% - 85px);" placeholder="厂区名称">
                        </label>
                    </li>
                </ul>
                <div class="field-button" style="">
                    <div class="btn btn-info" onclick="factoryarea_list_queryOk();">
                        <i class="ace-icon fa fa-check bigger-110"></i>查询
                    </div>
                    <div class="btn" onclick="reset();"><i class="ace-icon icon-remove"></i>重置</div>
                </div>
            </form>
        </div>
    </c:if>
    <div id="factoryarea_list_fixed_tool_div" class="fixed_tool_div">
        <div id="factoryarea_list_toolbar_" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="factoryarea_list_grid-table" style="width:100%;height:100%;"></table>
    <div id="factoryarea_list_grid-pager"></div>
</div>
</body>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var factoryarea_list_oriData;      //表格数据
    var factoryarea_list_grid;        //表格对象
    var factoryarea_list_dynamicDefalutValue="cbe154ed57f14f8e803c4d0982a4d17c";
    var factoryarea_list_exportExcelIndex;

    $("input").keypress(function (e) {
        if (e.which == 13) {
            factoryarea_list_queryOk();
        }
    });

    $(function  (){
        $(".toggle_tools").click();
    });

    $(function(){
        $("#factoryarea_list_toolbar_").iToolBar({
            id:"factoryarea_list_tb_01",
            items:[
            	{label: "编辑",hidden:"${operationCode.webEdit}"==="1", onclick: factoryarea_list_openEditPage, iconClass:'glyphicon glyphicon-pencil'},
            	{label: "同步数据",hidden:"${operationCode.webTbsj}"==="1", onclick: factoryarea_list_synchronousdata, iconClass:'icon-refresh'},
                {label:"导出",hidden:"${operationCode.webExport}"==="1",onclick:function(){factoryarea_list_toExcel();},iconClass:' icon-share'}
            ]
        });
        //初始化表格
        factoryarea_list_grid = jQuery("#factoryarea_list_grid-table").jqGrid({
            url : context_path + "/factoryArea/list.do",
            datatype : "json",
            colNames : [ "主键","厂区编码","厂区名称"],
            colModel : [
                {name : "id",index : "id",hidden:true},
                {name : "code",index : "code",width : 33},
                {name : "name",index : "name",width : 33}
            ],
            rowNum : 20,
            rowList : [ 10, 20, 30 ],
            pager : "#factoryarea_list_grid-pager",
            sortname : "id",
            sortorder : "asc",
            altRows: true,
            viewrecords : true,
            autowidth:true,
            multiselect:true,
            multiboxonly: true,
            beforeRequest:function (){
                dynamicGetColumns(factoryarea_list_dynamicDefalutValue,"factoryarea_list_grid-table",$(window).width()-$("#sidebar").width()-7);
                //重新加载列属性
            },
            loadComplete : function(data){
                var table = this;
                setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
                factoryarea_list_oriData = data;
            },
            emptyrecords: "没有相关记录",
            loadtext: "加载中...",
            pgtext : "页码 {0} / {1}页",
            recordtext: "显示 {0} - {1}共{2}条数据"
        });
        //在分页工具栏中添加按钮
        jQuery("#factoryarea_list_grid-table").navGrid("#factoryarea_list_grid-pager",
        {edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd('#factoryarea_list_grid-pager',{
            caption:"",
            buttonicon:"ace-icon fa fa-refresh green",
            onClickButton: function(){
                $("#factoryarea_list_grid-table").jqGrid("setGridParam",
                    {
                        postData: {jsonString:""} //发送数据
                    }
                ).trigger("reloadGrid");
            }
        }).navButtonAdd("#factoryarea_list_grid-pager",{
            caption: "",
            buttonicon:"fa icon-cogs",
            onClickButton : function (){
                jQuery("#factoryarea_list_grid-table").jqGrid("columnChooser",{
                    done: function(perm, cols){
                        dynamicColumns(cols, factoryarea_list_dynamicDefalutValue);
                        $("#factoryarea_list_grid-table").jqGrid("setGridWidth", $("#factoryarea_list_grid-div").width());
                        factoryarea_list_exportExcelIndex = perm;
                    }
                });
            }
        });
        $(window).on("resize.jqGrid", function () {
            $("#factoryarea_list_grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);
            $("#factoryarea_list_grid-table").jqGrid("setGridHeight", $(".container-fluid").height()-10-
            $("#factoryarea_list_yy").outerHeight(true)-$("#factoryarea_list_fixed_tool_div").outerHeight(true)-
            $("#factoryarea_list_grid-pager").outerHeight(true)-$("#gview_factoryarea_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
        });
        $(window).triggerHandler("resize.jqGrid");
    });

    var factoryarea_list_queryForm_data = iTsai.form.serialize($("#factoryarea_list_queryForm"));

    /**查询*/
    function factoryarea_list_queryOk(){
        var queryParam = iTsai.form.serialize($("#factoryarea_list_queryForm"));
        //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
        factoryarea_list_queryByParam(queryParam);

    }

    /**重置*/
    function reset(){
        iTsai.form.deserialize($("#factoryarea_list_queryForm"),factoryarea_list_queryForm_data);
        //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
        factoryarea_list_queryByParam(factoryarea_list_queryForm_data);
    }

    /**打开编辑页面*/
	function factoryarea_list_openEditPage(){
		var selectAmount = getGridCheckedNum("#factoryarea_list_grid-table");
		if(selectAmount==0){
			layer.msg("请选择一条记录！",{icon:2});
			return;
		}else if(selectAmount>1){
			layer.msg("只能选择一条记录！",{icon:8});
			return;
		}
		layer.load(2);
		$.post(context_path+'/factoryArea/toEdit.do', {
		    id:jQuery("#factoryarea_list_grid-table").jqGrid("getGridParam", "selrow")
        }, function(str){
			$queryWindow = layer.open({
				title : "厂区编辑",
				type: 1,
			    skin : "layui-layer-molv",
			    area : "600px",
				shade: 0.6, //遮罩透明度
				moveType: 1, //拖拽风格，0是默认，1是传统拖动
				content: str,//注意，如果str是object，那么需要字符拼接。
				success:function(layero, index){
			   		layer.closeAll("loading");
			    }
			});
		}).error(function() {
			layer.closeAll();
	    	layer.msg("加载失败！",{icon:2});
		});
	}

    /**同步数据*/
	function factoryarea_list_synchronousdata(){
		$.ajax({
            type: "GET",
            url: context_path+"/factoryArea/synchronousdata.do",
            dataType: "json",
            success: function(data){
            	if(data.result){
					layer.msg(data.msg,{icon:1,time:1200});
                    factoryarea_list_grid.trigger("reloadGrid");
				}else{
					layer.msg(data.msg,{icon:2,time:1200});
				}	
	        },
            error:function(XMLHttpRequest){
                alert(XMLHttpRequest.readyState);
                alert("出错啦！！！");
            }
        });
	}
	
    function factoryarea_list_queryByParam(jsonParam) {
        iTsai.form.deserialize($("#factoryarea_list_hiddenQueryForm"), jsonParam);
        var queryParam = iTsai.form.serialize($("#factoryarea_list_hiddenQueryForm"));
        var queryJsonString = JSON.stringify(queryParam);
        $("#factoryarea_list_grid-table").jqGrid("setGridParam",
            {
                postData: {queryJsonString: queryJsonString}
            }
        ).trigger("reloadGrid");
    }

    //导出功能
    function factoryarea_list_toExcel(){
        $("#factoryarea_list_hiddenForm #factoryarea_list_ids").val(jQuery("#factoryarea_list_grid-table").jqGrid("getGridParam", "selarrrow"));
        $("#factoryarea_list_hiddenForm #factoryarea_list_queryName").val($("#factoryarea_list_queryForm #factoryarea_list_name").val());
        $("#factoryarea_list_hiddenForm #factoryarea_list_queryCode").val($("#factoryarea_list_queryForm #factoryarea_list_code").val());
        $("#factoryarea_list_hiddenForm #factoryarea_list_queryExportExcelIndex").val(factoryarea_list_exportExcelIndex);
        $("#factoryarea_list_hiddenForm").submit();
    }
</script>