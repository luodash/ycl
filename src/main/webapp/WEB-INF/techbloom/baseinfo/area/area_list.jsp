<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="area_list_grid-div">
    <form id="area_list_hiddenForm" action="<%=path%>/area/materialExcel" method="POST"  style="display:none;">
        <input name="ids" id="area_list_ids" value="" />
        <input name="queryCode" id="area_list_queryCode" value="">
        <input name="queryName" id="area_list_queryName" value="">
        <input name="queryExportExcelIndex" id="area_list_queryExportExcelIndex" value=""/>
    </form>
    <form id="area_list_hiddenQueryForm" style="display:none;">
        <input name="code" value=""/>
        <input name="name" value=""/>
    </form>
    <c:if test="${operationCode.webSearch==1}">
    <div class="query_box" id="area_list_yy" title="查询选项">
         <form id="area_list_queryForm" style="max-width:100%;">
			 <ul class="form-elements">
				 <li class="field-group field-fluid3">
					 <label class="inline" for="area_list_code" style="margin-right:20px;width: 100%;">
						 <span class="form_label" style="width:80px;">库区编码：</span>
						 <input id="area_list_code" name="code" type="text" style="width: calc(100% - 85px);" placeholder="库区编码">
					 </label>
				 </li>
                 <li class="field-group field-fluid3">
                     <label class="inline" for="area_list_name" style="margin-right:20px;width: 100%;">
                         <span class="form_label" style="width:80px;">库区名称：</span>
                         <input id="area_list_name" name="name" type="text" style="width: calc(100% - 85px);" placeholder="库区名称">
                     </label>
                 </li>
			 </ul>
			<div class="field-button" style="">
				<div class="btn btn-info" onclick="area_list_queryOk();">
			        <i class="ace-icon fa fa-check bigger-110"></i>查询
		        </div>
				<div class="btn" onclick="area_list_reset();"><i class="ace-icon icon-remove"></i>重置</div>
	        </div>
		  </form>		 
    </div>
    </c:if>
    <div id="area_list_fixed_tool_div" class="fixed_tool_div">
        <div id="area_list_toolbar_" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="area_list_grid-table" style="width:100%;height:100%;"></table>
    <div id="area_list_grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
	var area_list_oriData;
	var area_list_grid;
    var area_list_exportExcelIndex;

    $("input").keypress(function (e) {
        if (e.which == 13) {
            area_list_queryOk();
        }
    });

	$(function  (){
	    $(".toggle_tools").click();
	});

	$("#area_list_toolbar_").iToolBar({
	    id: "area_list_tb_01",
	    items: [
            {label: "添加",hidden:"${operationCode.webAdd}"=="1", onclick: area_list_openAddPage, iconClass:'glyphicon glyphicon-plus'},
            {label: "编辑",hidden:"${operationCode.webEdit}"=="1", onclick: area_list_openEditPage, iconClass:'glyphicon glyphicon-pencil'},
            {label: "删除",hidden:"${operationCode.webDel}"=="1", onclick: area_list_deletearea, iconClass:'glyphicon glyphicon-trash'},
            {label: "导出",hidden:"${operationCode.webExport}"=="1", onclick:function(){area_list_toExcel();},iconClass:' icon-share'},
            {label: "打印", onclick: area_list_print, iconClass:'icon-print'}
	   ]
	});

	var area_list_queryForm_data = iTsai.form.serialize($("#area_list_queryForm"));

    area_list_grid = jQuery("#area_list_grid-table").jqGrid({
			url : context_path + "/area/list.do",
		    datatype : "json",
		    colNames : [ "主键","库区编码", "库区名称"],
		    colModel : [ 
                 {name : "id",index : "id",hidden:true},
                 {name : "code",index : "code",width : 60},
                 {name : "name",index : "area_name",width : 160}
            ],
		    rowNum : 20,
		    rowList : [ 10, 20, 30 ],
		    pager : "#area_list_grid-pager",
		    sortname : "id",
		    sortorder : "asc",
            altRows: true,
            viewrecords : true,
            hidegrid:false,
     	    autowidth:true, 
            multiselect:true,
            multiboxonly: true,
            loadComplete : function(data) {
            	var table = this;
            	setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
                area_list_oriData = data;
            },
            emptyrecords: "没有相关记录",
            loadtext: "加载中...",
            pgtext : "页码 {0} / {1}页",
            recordtext: "显示 {0} - {1}共{2}条数据"
	});

	jQuery("#area_list_grid-table").navGrid("#area_list_grid-pager",
    {edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#area_list_grid-pager",{
		caption:"",   
		buttonicon:"fa fa-refresh green",   
		onClickButton: function(){   
			$("#area_list_grid-table").jqGrid("setGridParam", {
                postData: {queryJsonString:""} //发送数据
            }).trigger("reloadGrid");
		}
	}).navButtonAdd("#area_list_grid-pager",{
        caption: "",
        buttonicon:"fa icon-cogs",
        onClickButton : function (){
            jQuery("#area_list_grid-table").jqGrid("columnChooser",{
                done: function(perm, cols){
                    $("#area_list_grid-table").jqGrid("setGridWidth", $("#area_list_grid-div").width());
                    area_list_exportExcelIndex = perm;
                }
            });
        }
    });

	$(window).on("resize.jqGrid", function () {
		$("#area_list_grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);
		$("#area_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-10-
		$("#area_list_yy").outerHeight(true)-$("#area_list_fixed_tool_div").outerHeight(true)-
		$("#area_list_grid-pager").outerHeight(true)-
		$("#gview_area_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
	});
	$(window).triggerHandler("resize.jqGrid");
	
	/**打开添加页面*/
	function area_list_openAddPage(){
		$.post(context_path + "/area/toAdd.do", {}, function (str){
			$queryWindow=layer.open({
			    title : "库区添加",
		    	type:1,
		    	skin : "layui-layer-molv",
		    	area : "600px",
		    	shade : 0.6, //遮罩透明度
			    moveType : 1, //拖拽风格，0是默认，1是传统拖动
			    anim : 2,
			    content : str,
			    success: function (layero, index) {
	                layer.closeAll('loading');
	            }
			});
		});
	}

	/**打开编辑页面*/
	function area_list_openEditPage(){
		var selectAmount = getGridCheckedNum("#area_list_grid-table");
		if(selectAmount==0){
			layer.msg("请选择一条记录！",{icon:2});
			return;
		}else if(selectAmount>1){
			layer.msg("只能选择一条记录！",{icon:8});
			return;
		}
		layer.load(2);
		$.post(context_path+'/area/toAdd.do', {
		    id:jQuery("#area_list_grid-table").jqGrid("getGridParam", "selrow")
        }, function(str){
			$queryWindow = layer.open({
				title : "库区编辑",
				type: 1,
			    skin : "layui-layer-molv",
			    area : "600px",
				shade: 0.6, //遮罩透明度
				moveType: 1, //拖拽风格，0是默认，1是传统拖动
				content: str,//注意，如果str是object，那么需要字符拼接。
				success:function(layero, index){
					layer.closeAll("loading");
				}
			});
		}).error(function() {
			layer.closeAll();
	    	layer.msg("加载失败！",{icon:2});
		});
	}
	
	/**删除功能*/
	function area_list_deletearea(){
		var checkedNum = getGridCheckedNum("#area_list_grid-table", "id");  //选中的数量
	    if (checkedNum == 0) {
	    	layer.alert("请选择一个要删除库区！");
	    } else {
	        var ids = jQuery("#area_list_grid-table").jqGrid("getGridParam", "selarrrow");
	        layer.confirm("确定删除选中的库区？", function() {
	    		$.ajax({
	    			type : "POST",
	    			url : context_path + "/area/deleteArea.do?ids="+ids ,
	    			dataType : "json",
	    			cache : false,
	    			success : function(data) {
	    				layer.closeAll();
	    				if (Boolean(data.result)) {
	    					layer.msg(data.msg, {icon: 1,time:1000});
                        }else{
	    					layer.msg(data.msg, {icon: 7,time:1000});
                        }
                        area_list_grid.trigger("reloadGrid");  //重新加载表格
	    			}
	    		});
	    	});
	    }
	}

    //导出功能
    function area_list_toExcel(){
        $("#area_list_hiddenForm #area_list_ids").val(jQuery("#area_list_grid-table").jqGrid("getGridParam", "selarrrow"));
        $("#area_list_hiddenForm #area_list_queryCode").val($("#area_list_queryForm #area_list_code").val());
        $("#area_list_hiddenForm #area_list_queryName").val($("#area_list_queryForm #area_list_name").val());
        $("#area_list_hiddenForm #area_list_queryExportExcelIndex").val(area_list_exportExcelIndex);
        $("#area_list_hiddenForm").submit();
    }

    /**打印功能*/
    function area_list_print(){
        var checkedNum = getGridCheckedNum("#area_list_grid-table", "id");  //选中的数量
        if (checkedNum == 0) {
            layer.alert("请选择一个要打印的库区！");
        } else {
            layer.load(2);
            $.post(context_path+'/area/toPrint.do', {
                ids:jQuery("#area_list_grid-table").jqGrid("getGridParam", "selarrrow").toString()
        }, function(str){
                $queryWindow = layer.open({
                    title : "库区打印",
                    type: 1,
                    skin : "layui-layer-molv",
                    area : "800px",
                    shade: 0.6, //遮罩透明度
                    moveType: 1, //拖拽风格，0是默认，1是传统拖动
                    content: str,//注意，如果str是object，那么需要字符拼接。
                    success:function(layero, index){
                        layer.closeAll("loading");
                    }
                });
            }).error(function() {
                layer.closeAll();
                layer.msg("加载失败！",{icon:2});
            });
        }
    }
	
	/**
	 * 查询按钮点击事件
	 */
	function area_list_queryOk(){
		 var queryParam = iTsai.form.serialize($("#area_list_queryForm"));
		 //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
		 area_list_queryByParam(queryParam);
	}
	
	function area_list_queryByParam(jsonParam) {
	    iTsai.form.deserialize($("#area_list_hiddenQueryForm"), jsonParam);
	    var queryParam = iTsai.form.serialize($("#area_list_hiddenQueryForm"));
	    var queryJsonString = JSON.stringify(queryParam);
	    console.log(queryJsonString)
	    $("#area_list_grid-table").jqGrid("setGridParam", {
            postData: {queryJsonString: queryJsonString}
        }).trigger("reloadGrid");
	 }
	
	/**重置清空查询条件*/
	function area_list_reset(){
		 iTsai.form.deserialize($("#area_list_queryForm"),area_list_queryForm_data); 
		 area_list_queryByParam(area_list_queryForm_data);
	}


	/**同步数据*/
	function area_list_synchronousdata(){
		$.ajax({
            type: "GET",
            url: context_path+"/area/synchronousdata.do",
            dataType: "json",
            success: function(data){
            	if(data.result){
					layer.msg(data.msg,{icon:1,time:1200});
                    area_list_grid.trigger("reloadGrid");
				}else{
					layer.msg(data.msg,{icon:2,time:1200});
				}	
	        },
            error:function(XMLHttpRequest){
                alert(XMLHttpRequest.readyState);
                alert("出错啦！！！");
            }
        });
	}

</script>