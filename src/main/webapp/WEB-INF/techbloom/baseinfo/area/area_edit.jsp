<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<div id="area_edit_page" class="row-fluid" style="height: inherit;">
	<form id="areaEditForm" class="form-horizontal" style="overflow: auto; height: calc(100% - 70px);">
		<input type="hidden" id="id" name="id" value="${area.id}">
			
		<div class="control-group">
			<label class="control-label" for="code">库区编码：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="code" name="code" value="${area.code}" placeholder="库区编码" >
				</div>
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label" for="name">库区名称：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="name" name="name" value="${area.name}" placeholder="库区名称" >
				</div>
			</div>
		</div>
	</form>
	<div class="field-button" style="text-align: center;border-top: 0px;margin: 15px auto;">
		<span class="btn btn-info" onclick="saveForm();">
		   <i class="ace-icon fa fa-check bigger-110"></i>保存
		</span>
		<span class="btn btn-danger" onclick="layer.closeAll();">
		   <i class="icon-remove"></i>&nbsp;取消
		</span>
	</div>
</div>
<script type="text/javascript">
	$("#areaEditForm").validate({
		rules:{
            "code":{
                required:true,
            },
            "name":{
                required:true,
            },
  		},
  		messages:{
            "code":{
                required:"请输入库区编码！",
            },
            "name":{
                required:"请输入库区名称！",
            }
  		},
  		errorClass: "help-inline",
		errorElement: "div",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
		}
  	});
	//确定按钮点击事件
    function saveForm() {
        if ($("#areaEditForm").valid()) {
            saveAreaInfo($("#areaEditForm").serialize());
         }
    }
  	//保存/修改信息
    function saveAreaInfo(bean) {
        $.ajax({
            url: context_path + "/area/saveArea",
            type: "POST",
            data: bean,
            dataType: "JSON",
            success: function (data) {
                if (Boolean(data.result)) {
                    layer.msg("保存成功！", {icon: 1});
                    //关闭当前窗口
                    layer.close($queryWindow);
                    //刷新列表
					iTsai.form.deserialize($("#area_list_hiddenQueryForm"), iTsai.form.serialize($("#area_list_queryForm")));
					var queryParam = iTsai.form.serialize($("#area_list_hiddenQueryForm"));
					var queryJsonString = JSON.stringify(queryParam);
					$("#area_list_grid-table").jqGrid('setGridParam', {
						 postData: {queryJsonString: queryJsonString}
					 }).trigger("reloadGrid");
                } else {
                    layer.alert("保存失败，请稍后重试！", {icon: 2});
                }
            },
            error:function(XMLHttpRequest){
        		alert(XMLHttpRequest.readyState);
        		alert("出错啦！！！");
        	}
        });
    }

</script>