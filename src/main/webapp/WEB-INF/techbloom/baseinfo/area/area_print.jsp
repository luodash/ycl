<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<style>
	.t-body{
		overflow-y:scroll;
		display: block;
		height: 420px;
	}
	.box{
		text-align:center;
		margin-top: 20px;

	}
	.box ul{
		width: 100%;

	}

	.box ul li{
		list-style: none;
		margin: 0 auto;
		margin-top: 20px;
	}
</style>
<div class="t-body">
<!--startprint-->
<div id = "imagediv">
	<div class="box">
		<ul>
			<c:forEach items="${imageList}" var="image">
				<li style="list-style-type:none;margin: 0 auto;margin-top: 20px;" >
					<img src="data:image/png;base64,${image}"/>
				</li>
			</c:forEach>
		</ul>
	</div>
</div>
<!--endprint-->
</div>

<div class="field-button" style="text-align: center;border-top: 0px;margin: 15px auto;">
		<span class="btn btn-info" onclick="printPage();">
		   <i class="ace-icon fa fa-check bigger-110"></i>打印
		</span>
	<span class="btn btn-danger" onclick="layer.closeAll();">
		   <i class="icon-remove"></i>&nbsp;取消
		</span>
</div>
<script type="text/javascript">
function printPage() {
    var oldStr = window.document.body.innerHTML; // 获取body的内容
    var start = "<!--startprint-->"; // 开始打印标识, 17个字符
    var end = "<!--endprint-->"; // 结束打印标识
    var newStr = oldStr.substr(oldStr.indexOf(start) + 17); // 截取开始打印标识之后的内容
    newStr = newStr.substring(0, newStr.indexOf(end)); // 截取开始打印标识和结束打印标识之间的内容
    //判断iframe是否存在，不存在则创建iframe
    var iframe=document.getElementById("print-iframe");
    var doc = null;
    if(!iframe){
        iframe = document.createElement('IFRAME');
        iframe.setAttribute("id", "print-iframe");
        iframe.setAttribute('style', 'position:absolute;width:0px;height:0px;left:-500px;top:-500px;');
        document.body.appendChild(iframe);
        //这里可以自定义样式
    }
    doc = iframe.contentWindow.document;
    doc.write('<div>' + newStr + '</div>');
    doc.close();
    iframe.contentWindow.focus();
    iframe.contentWindow.print();
    if (navigator.userAgent.indexOf("MSIE") > 0){
        document.body.removeChild(iframe);
    }
}
</script>