<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<div id="workshop_edit_page" class="row-fluid" style="height: inherit;">
	<form id="workshop_edit_workshopForm" class="form-horizontal" style="overflow: auto; height: calc(100% - 70px);">
		<input type="hidden" id="workshop_edit_id" name="id" value="${workshop.id}">	
		<div class="control-group">
			<label class="control-label" for="workshop_edit_org">厂区：</label>
			<div class="controls required">
				<input class="span11 select2_input " type="text" name="org" id="workshop_edit_org" value="${workshop.org}" placeholder="厂区"></input>
				<input type="hidden" id="workshop_edit_factoryid" value="${workshop.org}">
                <input type="hidden" id="workshop_edit_factoryno" value="${workshop.factoryCodeName}">
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label" for="workshop_edit_workshopnumber">车间编码：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="workshop_edit_workshopnumber" name="workshopnumber" value="${workshop.workshopnumber}" placeholder="车间编码">
				</div>
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label" for="workshop_edit_workshopname">车间名称：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<input type="text" class="span11" id="workshop_edit_workshopname" name="workshopname" value="${workshop.workshopname}" placeholder="车间名称">
				</div>
			</div>
		</div>
	</form>
	<div class="field-button" style="text-align: center;border-top: 0px;margin: 15px auto;">
		<span class="btn btn-info" onclick="saveForm();">
		   <i class="ace-icon fa fa-check bigger-110"></i>保存
		</span>
		<span class="btn btn-danger" onclick="layer.closeAll();">
		   <i class="icon-remove"></i>&nbsp;取消
		</span>
	</div>
</div>
<script type="text/javascript">

	$("#workshop_edit_workshopForm").validate({
		rules:{
			"org":{
				required:true
			},
            "workshopnumber":{
                required:true
            },
            "workshopname":{
                required:true
            }
  		},
  		messages:{
  			"org":{
  				required:"请选择厂区！"
  			},
            "workshopnumber":{
                required:"请输入编码！"
            },
            "workshopname":{
                required:"请输入名称！"
            }
  		},
  		errorClass: "help-inline",
		errorElement: "div",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
		}
  	});
	//确定按钮点击事件
    function saveForm() {
        if ($("#workshop_edit_workshopForm").valid()) {
            saveworkshopInfo($("#workshop_edit_workshopForm").serialize());
        }
    }
  	//保存/修改用户信息
    function saveworkshopInfo(bean) {
    	console.log(bean)
        $.ajax({
           url: context_path + "/workshop/saveworkshop",
           type: "POST",
           data: bean,
           dataType: "JSON",
           success: function (data) {
			   iTsai.form.deserialize($("#workshop_list_hiddenQueryForm"), iTsai.form.serialize($("#workshop_list_queryForm")));
			   var queryParam = iTsai.form.serialize($("#workshop_list_hiddenQueryForm"));
			   var queryJsonString = JSON.stringify(queryParam);
               if (Boolean(data.result)) {
                   layer.msg("保存成功！", {icon: 1});
                   //关闭当前窗口
                   layer.close($queryWindow);
                   //刷新列表
                   $("#workshop_list_grid-table").jqGrid('setGridParam',
                {
                    postData: {queryJsonString: queryJsonString}
                }).trigger("reloadGrid");
               } else {
                   layer.alert("保存失败，请稍后重试！", {icon: 2});
               }
           },
           error:function(XMLHttpRequest){
				alert(XMLHttpRequest.readyState);
				alert("出错啦！！！");
			}
       });
    }

  	//厂区下拉框初始化
    $("#workshop_edit_workshopForm #workshop_edit_org").select2({
        placeholder: "选择部门",
        minimumInputLength:0,   //至少输入n个字符，才去加载数据
        allowClear: true,  //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches:"没有结果",
        formatSearching:"搜索中...",
        formatAjaxError:"加载出错啦！",
        ajax : {
            url: context_path+"/factoryArea/getFactoryList",
            type:"POST",
            dataType : 'json',
            delay : 250,
            data: function (term,pageNo) {     //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term,    //联动查询的字符
                    pageSize: 15,    //一次性加载的数据条数
                    pageNo:pageNo    //页码
                }
            },
            results: function (data,pageNo) {
                var res = data.result;
                if(res.length>0){   //如果没有查询到数据，将会返回空串
                    var more = (pageNo*15)<data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results:res,more:more
                    };
                }else{
                    return {
                        results:{}
                    };
                }
            },
            cache : true
        }

    });
  	
  	//点击编辑的默认数据
    if($("#workshop_edit_workshopForm #workshop_edit_factoryid").val()!=""){
        $("#workshop_edit_workshopForm #workshop_edit_org").select2("data", {
            id: $("#workshop_edit_workshopForm #workshop_edit_factoryid").val(),
            text: $("#workshop_edit_workshopForm #workshop_edit_factoryno").val()
        });
    }

</script>