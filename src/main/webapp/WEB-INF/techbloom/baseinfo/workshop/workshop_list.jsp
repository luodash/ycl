<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="workshop_list_grid-div">
    <form id="workshop_list_hiddenForm" action="<%=path%>/car/materialExcel" method="POST" style="display: none;">
        <input name="ids" id="workshop_list_ids" value=""/>
		<input name="queryOrg"  value="">
        <input name="queryCode" value="">
    </form>
    <form id="workshop_list_hiddenQueryForm" style="display:none;">
        <input  name="code" value=""/>
        <input  name="factoryCode" value="">
    </form>
	<c:if test="${operationCode.webSearch==1}">
    <div class="query_box" id="workshop_list_yy" title="查询选项">
         <form id="workshop_list_queryForm" style="max-width:100%;">
			 <ul class="form-elements">
			 	 <li class="field-group field-fluid3">
					<label class="inline" for="workshop_list_factoryCode" style="margin-right:20px;width: 100%;">
						 <span class="form_label" style="width:80px;">厂区：</span>
						 <input id="workshop_list_factoryCode" name="factoryCode" type="text" style="width: calc(100% - 85px);" placeholder="厂区">
					</label>
				 </li>
				 <li class="field-group field-fluid3">
					 <label class="inline" for="workshop_list_code" style="margin-right:20px;width: 100%;">
						 <span class="form_label" style="width:80px;">车间编号：</span>
						 <input id="workshop_list_code" name="code" type="text" style="width: calc(100% - 85px);" placeholder="编号">
					 </label>
				 </li>
			 </ul>
			<div class="field-button" style="">
				<div class="btn btn-info" onclick="workshop_list_queryOk();">
			        <i class="ace-icon fa fa-check bigger-110"></i>查询
		        </div>
				<div class="btn" onclick="workshop_list_reset();"><i class="ace-icon icon-remove"></i>重置</div>
	        </div>
		  </form>		 
    </div>
	</c:if>
    <div id="workshop_list_fixed_tool_div" class="fixed_tool_div">
        <div id="workshop_list_toolbar_" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="workshop_list_grid-table" style="width:100%;height:100%;"></table>
    <div id="workshop_list_grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
	var workshop_list_oriData; 
	var workshop_list_grid;

	$("input").keypress(function (e) {
		if (e.which == 13) {
			workshop_list_queryOk();
		}
	});

	$(function  (){
	    $(".toggle_tools").click();
	});
	
	$("#workshop_list_toolbar_").iToolBar({
	    id: "workshop_list_tb_01",
	    items: [
	    	{label: "添加", hidden:"${operationCode.webAdd}"=="1",onclick:workshop_list_openAddPage, iconClass:'glyphicon glyphicon-plus'},
	        {label: "编辑",hidden:"${operationCode.webEdit}"=="1", onclick: workshop_list_openEditPage, iconClass:'glyphicon glyphicon-pencil'},
	        {label: "启用",hidden:"${operationCode.webEnable}"=="1", onclick: workshop_list_useful, iconClass:'icon-ok'},
	        {label: "禁用",hidden:"${operationCode.webDisable}"=="1", onclick: workshop_list_useless, iconClass:'icon-remove'},
	        {label: "同步数据", hidden:"${operationCode.webTbsj}"=="1",onclick: workshop_list_synchronousdata, iconClass:'icon-refresh'},
	   ]
	});
	
	var workshop_list_queryForm_data = iTsai.form.serialize($("#workshop_list_queryForm"));

	workshop_list_grid = jQuery("#workshop_list_grid-table").jqGrid({
			url : context_path + "/workshop/list.do",
		    datatype : "json",
		    colNames : [ "主键","厂区","车间编码", "车间名称","状态"],
		    colModel : [ 
                 {name : "id",index : "id",hidden:true},
                 {name : "factoryName",index : "factoryName",width : 60},
                 {name : "workshopnumber",index : "workshopnumber",width : 60},
                 {name : "workshopname",index : "workshopname",width : 60},
                 {name : "state",index : "state",width : 60,formatter:function(cellvalue,option,rowObject){
	                	if(cellvalue==0) {
	                		return "<span style='color:red;font-weight:bold;'>不可用</span>";
						}else if(cellvalue==1) {
	                		return "<span style='color:green;font-weight:bold;'>可用</span>";
						}
	                }
                }
		    ],
		    rowNum : 20,
		    rowList : [ 10, 20, 30 ],
		    pager : "#workshop_list_grid-pager",
		    sortname : "id",
		    sortorder : "asc",
            altRows: true,
            viewrecords : true,
            hidegrid:false,
     	    autowidth:true, 
            multiselect:true,
            multiboxonly: true,
            loadComplete : function(data) {
            	var table = this;
            	setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
				workshop_list_oriData = data;
            },
            emptyrecords: "没有相关记录",
            loadtext: "加载中...",
            pgtext : "页码 {0} / {1}页",
            recordtext: "显示 {0} - {1}共{2}条数据"
	});

	jQuery("#workshop_list_grid-table").navGrid("#workshop_list_grid-pager",
	{edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#workshop_list_grid-pager",{
		caption:"",   
		buttonicon:"fa fa-refresh green",   
		onClickButton: function(){   
			$("#workshop_list_grid-table").jqGrid("setGridParam", {
			  	postData: {queryJsonString:""} //发送数据
			}).trigger("reloadGrid");
		}
	}).navButtonAdd("#workshop_list_grid-pager",{
        caption: "",
        buttonicon:"fa icon-cogs",
        onClickButton : function (){
            jQuery("#workshop_list_grid-table").jqGrid("columnChooser",{
                done: function(perm, cols){
                    dynamicColumns(cols,dynamicDefalutValue);
                    $("#workshop_list_grid-table").jqGrid("setGridWidth", $("#workshop_list_grid-div").width());
                }
            });
        }
    });

	$(window).on("resize.jqGrid", function () {
		$("#workshop_list_grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);
		$("#workshop_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-10-
		$("#workshop_list_yy").outerHeight(true)-$("#workshop_list_fixed_tool_div").outerHeight(true)-
		$("#workshop_list_grid-pager").outerHeight(true)-
		$("#gview_workshop_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
	});
	$(window).triggerHandler("resize.jqGrid");
	
	//可用
	function workshop_list_useful(){
		var checkedNum = getGridCheckedNum("#workshop_list_grid-table", "id");  //选中的数量
	    if (checkedNum == 0) {
	    	layer.alert("请至少选择一条记录！");
	    } else {
    		$.ajax({
    			type : "POST",
    			url : context_path + "/workshop/useful.do?ids="+jQuery("#workshop_list_grid-table").jqGrid("getGridParam", "selarrrow") ,
    			dataType : "json",
    			cache : false,
    			success : function(data) {
    				layer.closeAll();
    				if (Boolean(data.result)) {
    					layer.msg(data.msg, {icon: 1,time:1000});
    				}else{
    					layer.msg(data.msg, {icon: 7,time:1000});
    				}
					workshop_list_grid.trigger("reloadGrid");  //重新加载表格
    			}
    		});
	    }  
	}
	
	//不可用
	function workshop_list_useless(){
		var checkedNum = getGridCheckedNum("#workshop_list_grid-table", "id");  //选中的数量
	    if (checkedNum == 0) {
	    	layer.alert("请至少选择一条记录！");
	    } else {
    		$.ajax({
    			type : "POST",
    			url : context_path + "/workshop/useless.do?ids="+jQuery("#workshop_list_grid-table").jqGrid("getGridParam", "selarrrow") ,
    			dataType : "json",
    			cache : false,
    			success : function(data) {
    				layer.closeAll();
    				if (Boolean(data.result)) {
    					layer.msg(data.msg, {icon: 1,time:1000});
    				}else{
    					layer.msg(data.msg, {icon: 7,time:1000});
    				}
					workshop_list_grid.trigger("reloadGrid");  //重新加载表格
    			}
    		});
	    }  
	}
	
	/**
	 * 查询按钮点击事件
	 */
	 function workshop_list_queryOk(){
		 var queryParam = iTsai.form.serialize($("#workshop_list_queryForm"));
		 //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
		 workshop_list_queryByParam(queryParam);
	}
	
	 function workshop_list_queryByParam(jsonParam) {
	    iTsai.form.deserialize($("#workshop_list_hiddenQueryForm"), jsonParam);
	    var queryParam = iTsai.form.serialize($("#workshop_list_hiddenQueryForm"));
	    var queryJsonString = JSON.stringify(queryParam); 
	    $("#workshop_list_grid-table").jqGrid("setGridParam",
	        {
	            postData: {queryJsonString: queryJsonString}
	        }
	    ).trigger("reloadGrid");
	}
	 
	function workshop_list_reset(){
		 $("#workshop_list_queryForm #workshop_list_factoryCode").select2("val","");
		 iTsai.form.deserialize($("#workshop_list_queryForm"),workshop_list_queryForm_data); 
		 workshop_list_queryByParam(workshop_list_queryForm_data);
	}
	
	//厂区
	$("#workshop_list_queryForm #workshop_list_factoryCode").select2({
	    placeholder: "选择厂区",
	    minimumInputLength:0,   //至少输入n个字符，才去加载数据
	    allowClear: true,  //是否允许用户清除文本信息
	    delay: 250,
	    formatNoMatches:"没有结果",
	    formatSearching:"搜索中...",
	    formatAjaxError:"加载出错啦！",
	    ajax : {
	        url: context_path+"/factoryArea/getFactoryList",
	        type:"POST",
	        dataType : 'json',
	        delay : 250,
	        data: function (term,pageNo) {     //在查询时向服务器端传输的数据
	            term = $.trim(term);
	            return {
	                queryString: term,    //联动查询的字符
	                pageSize: 15,    //一次性加载的数据条数
	                pageNo:pageNo    //页码
	            }
	        },
	        results: function (data,pageNo) {
	            var res = data.result;
	            if(res.length>0){   //如果没有查询到数据，将会返回空串
	                var more = (pageNo*15)<data.total; //用来判断是否还有更多数据可以加载
	                return {
	                    results:res,more:more
	                };
	            }else{
	                return {
	                    results:{}
	                };
	            }
	        },
	        cache : true
	    }
	});
	
	
	$("#workshop_list_queryForm #workshop_list_factoryCode").on("change.select2",function(){
	    $("#workshop_list_queryForm #workshop_list_factoryCode").trigger("keyup")
	});
	
	function workshop_list_toExcel(){
	    $("#workshop_list_hiddenForm #workshop_list_ids").val(jQuery("#workshop_list_grid-table").jqGrid("getGridParam", "selarrrow"));
	    $("#workshop_list_hiddenForm #workshop_list_org").val($("#workshop_list_queryForm #workshop_list_factoryCode").val());
	    $("#workshop_list_hiddenForm #workshop_list_code").val($("#workshop_list_queryForm #workshop_list_code").val());
	    $("#workshop_list_hiddenForm").submit();	
	}
	
	//打开添加页面
	function workshop_list_openAddPage(){
		$.post(context_path + "/workshop/toAdd.do", {}, function (str){
			$queryWindow=layer.open({
			    title : "车间添加",
		    	type:1,
		    	skin : "layui-layer-molv",
		    	area : "600px",
		    	shade : 0.6, //遮罩透明度
			    moveType : 1, //拖拽风格，0是默认，1是传统拖动
			    anim : 2,
			    content : str,
			    success: function (layero, index) {
	                layer.closeAll('loading');
	            }
			});
		});
	}

	/*打开编辑页面*/
	function workshop_list_openEditPage(){
		var selectAmount = getGridCheckedNum("#workshop_list_grid-table");
		if(selectAmount==0){
			layer.msg("请选择一条记录！",{icon:2});
			return;
		}else if(selectAmount>1){
			layer.msg("只能选择一条记录！",{icon:8});
			return;
		}
		layer.load(2);

		$.post(context_path+'/workshop/toAdd.do', {
			id : jQuery("#workshop_list_grid-table").jqGrid("getGridParam", "selrow")
		}, function(str){
			$queryWindow = layer.open({
				title : "车间编辑",
				type: 1,
			    skin : "layui-layer-molv",
			    area : "600px",
				shade: 0.6, //遮罩透明度
				moveType: 1, //拖拽风格，0是默认，1是传统拖动
				content: str,//注意，如果str是object，那么需要字符拼接。
				success:function(layero, index){
			   		layer.closeAll("loading");
			    }
			});
		}).error(function() {
			layer.closeAll();
	    	layer.msg("加载失败！",{icon:2});
		});
	}
	
	//同步数据
	function workshop_list_synchronousdata(){
		$.ajax({
            type: "GET",
            url: context_path+"/workshop/synchronousdata.do",
            dataType: "json",
            success: function(data){
            	if(data.result){
					layer.msg(data.msg,{icon:1,time:1200});
					workshop_list_grid.trigger("reloadGrid");
				}else{
					layer.msg(data.msg,{icon:2,time:1200});
				}	
	        },
            error:function(XMLHttpRequest){
                alert(XMLHttpRequest.readyState);
                alert("出错啦！！！");
            }
        });
	}

</script>