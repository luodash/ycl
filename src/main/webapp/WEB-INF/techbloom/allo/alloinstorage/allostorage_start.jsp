<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<div id="allostorage_start_page" class="row-fluid" style="height: inherit;">
	<form id="allostorage_start_carForm" class="form-horizontal" style="overflow: auto; height: calc(100% - 70px);">
        <input type="hidden" id="allostorage_start_id" name="id" value="${info.id}">
        <input type="hidden" id="allostorage_start_hCarId" name="hCarId" value="${info.carId}">
        <input type="hidden" id="allostorage_start_allocationDetailId" name="allocationDetailId" value="${instorageDetailId}">
		<div class="control-group">
			<label class="control-label" >仓库：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<select id="allostorage_start_warehouseId" name="warehouseId" onchange="warehouseChange()" class="span9" disabled>
						<c:forEach items="${warehouseList}" var="tp">
							<option value="${tp.id}" <c:if test="${info.warehouseCode==tp.code }">selected="selected"</c:if>>
									${tp.warehouseCodeName}</option>
						</c:forEach>
					</select>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" >推荐库位：</label>
			<div class="controls">
				<div class="input-append span12">
					<textarea class="span11" id="allostorage_start_recommendcode" readonly>${recommendCode}</textarea>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" >行车：</label>
			<div class="controls">
				<div class="input-append span12 required">
					<select id="allostorage_start_carId" name="carId" class="span9">
						<c:forEach items="${carList}" var="tp">
							<option value="${tp.id}" <c:if test="${info.carId==tp.id }">selected="selected"</c:if>>
									${tp.name}</option>
						</c:forEach>
					</select>
				</div>
			</div>
		</div>
	</form>
	<div class="field-button" style="text-align: center;border-top: 0px;margin: 15px auto;">
		<span class="btn btn-info" onclick="saveForm();">
		   <i class="ace-icon fa fa-check bigger-110"></i>保存
		</span>
		<span class="btn btn-danger" onclick="layer.closeAll();">
		   <i class="icon-remove"></i>&nbsp;取消
		</span>
	</div>
</div>
<script type="text/javascript">
$("#allostorage_start_carForm").validate({
		rules:{
			"warehouseId":{
  				required:true,
  			},
            "carId":{//供应商名称
                required:true,
            },
  		},
  		messages:{
  			"warehouseId":{
  				required:"请选择仓库！",
  			},
            "carId":{
                required:"请选择行车！",
            }
  		},
  		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
		}
  	});
	//确定按钮点击事件
    function saveForm() {
        if ($("#allostorage_start_carForm").valid()) 
            updateWarehouseAndCar($("#allostorage_start_carForm").serialize());
    }
  	//保存/修改用户信息
    function updateWarehouseAndCar(bean) {
        $.ajax({
                url: context_path + "/allostoragein/updateWarehouseAndCar",
                type: "POST",
                data: bean,
                dataType: "JSON",
                success: function (data) {
                    if (Boolean(data.result)) {
                        layer.msg(data.msg, {icon: 1});
                        //关闭当前窗口
                        layer.close($queryWindow);
                        //刷新列表
                        $("#allostoragein_list_grid-table").jqGrid('setGridParam',
                     {
                         postData: {queryJsonString: ""}
                     }).trigger("reloadGrid");
                    } else {
                        layer.alert(data.msg, {icon: 2});
                    }
                },
                error:function(XMLHttpRequest){
            		alert(XMLHttpRequest.readyState);
            		alert("出错啦！！！");
            	}
            });
    }
    $("#allostorage_start_carForm #allostorage_start_type").select2({
	    minimumInputLength:0,
	    allowClear:true,
	    delay:250,
	    width:435,
	    formatNoMatches:"没有结果",
		formatSearching:"搜索中...",
		formatAjaxError:"加载出错啦！"
    });
    $("#allostorage_start_carForm #allostorage_start_type").on("change.select2",function(){
       $("#allostorage_start_carForm #allostorage_start_type").trigger("keyup")}
    );
    $("#allostorage_start_carForm #allostorage_start_state").select2({
	    minimumInputLength:0,
	    allowClear:true,
	    delay:250,
	    width:435,
	    formatNoMatches:"没有结果",
		formatSearching:"搜索中...",
		formatAjaxError:"加载出错啦！"
    });
    $("#allostorage_start_carForm #allostorage_start_state").on("change.select2",function(){
       $("#allostorage_start_carForm #allostorage_start_state").trigger("keyup")}
    );

    function warehouseChange(){
        var id = $("#allostorage_start_warehouseId").val();
        $.ajax({
            type:"POST",
            url:context_path + "/allostoragein/warehouseChangeList.do",
            data:{id:id},
            dataType:"json",
            success:function(data){
                var list = data.list;
                $("#allostorage_start_carId").empty();
                for(var i=0;i<list.length;i++){
                    $("#allostorage_start_carId").append("<option value='"+list[i].id+"'>"+list[i].name+"</option>");
                }
            }
        });
	}

</script>