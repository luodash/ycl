<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="alloallostoragein_list_grid-div">
    <form id="alloallostoragein_list_hiddenForm" action="<%=path%>/allostoragein/materialExcel" method="POST" style="display: none;">
        <input id="alloallostoragein_list_ids" name="ids" value=""/>
        <input id="alloallostoragein_list_queryQacode" name="queryQacode" value=""/>
        <input id="alloallostoragein_list_queryOutShelfCode" name="queryOutShelfCode" value=""/>
        <input id="alloallostoragein_list_queryInShelfCode" name="queryInShelfCode" value=""/>
        <input id="alloallostoragein_list_queryStartTime" name="queryStartTime" value=""/>
        <input id="alloallostoragein_list_queryEndTime" name="queryEndTime" value=""/>
        <input id="alloallostoragein_list_queryExportExcelIndex" name="queryExportExcelIndex" value=""/>
    </form>
    <form id="alloallostoragein_list_hiddenQueryForm" style="display:none;">
        <input name="qaCode" value=""/>
        <input name="outShelfCode" value=""/>
        <input name="inShelfCode" value=""/>
        <input name="createTime" value=""/>
        <input name="endTime" value=""/>
    </form>
    <c:if test="${operationCode.webSearch==1}">
    <div class="query_box" id="alloallostoragein_list_yy" title="查询选项">
        <form id="alloallostoragein_list_queryForm" style="max-width:100%;">
            <ul class="form-elements">
                <li class="field-group field-fluid3">
                    <label class="inline" for="alloallostoragein_list_qaCode" style="margin-right:20px;width: 100%;">
                        <span class="form_label" style="width:80px;">质保单号：</span>
                        <input id="alloallostoragein_list_qaCode" name="qaCode" type="text" style="width: calc(100% - 85px);" placeholder="质保单号">
                    </label>
                </li>
                <li class="field-group field-fluid3">
                    <label class="inline" style="margin-right:20px;width: 100%;">
                        <span class="form_label" style="width:80px;">移出库位：</span>
                        <input id="alloallostoragein_list_outShelfCode" name="outShelfCode" type="text" style="width: calc(100% - 85px);" placeholder="移（调）出库位">
                    </label>
                </li>
                <li class="field-group field-fluid3">
                    <label class="inline" style="margin-right:20px;width: 100%;">
                        <span class="form_label" style="width:80px;">移入库位：</span>
                        <input id="alloallostoragein_list_inShelfCode" name="inShelfCode" type="text" style="width: calc(100% - 85px);" placeholder="移（调）入库位">
                    </label>
                </li>

                <li class="field-group-top field-group field-fluid3">
                    <label class="inline" for="alloallostoragein_list_createTime" style="margin-right:20px;width:100%;">
                        <span class="form_label" style="width:80px;">开始时间：</span>
                        <input type="text" class="form-control date-picker" id="alloallostoragein_list_createTime" name="createTime" value=""
                               style="width: calc(100% - 85px);" placeholder="开始时间" />
                    </label>
                </li>
                <li class="field-group field-fluid3">
                    <label class="inline" for="alloallostoragein_list_endTime" style="margin-right:20px;width:100%;">
                        <span class="form_label" style="width:80px;">结束时间：</span>
                        <input type="text" class="form-control date-picker" id="alloallostoragein_list_endTime" name="endTime" value=""
                               style="width: calc(100% - 85px);" placeholder="结束时间" />
                    </label>
                </li>
            </ul>
            <div class="field-button" style="">
                <div class="btn btn-info" onclick="alloallostoragein_list_queryOk();">
                    <i class="ace-icon fa fa-check bigger-110"></i>查询
                </div>
                <div class="btn" onclick="alloallostoragein_list_reset();"><i class="ace-icon icon-remove"></i>重置</div>
                <a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>
            </div>
        </form>
    </div>
    </c:if>
    <div id="alloallostoragein_list_fixed_tool_div" class="fixed_tool_div">
        <div id="alloallostoragein_list_toolbar_" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="alloallostoragein_list_grid-table" style="width:100%;height:100%;"></table>
    <div id="alloallostoragein_list_grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
	var allostoragein_list_oriData;
	var allostoragein_list_grid;
    var alloallostoragein_list_exportExcelIndex;

    $("input").keypress(function (e) {
        if (e.which == 13) {
            alloallostoragein_list_queryOk();
        }
    });

	//时间控件
    $(".date-picker").datetimepicker({
        format: "YYYY-MM-DD HH:mm:ss" ,
        autoclose : true,
        todayHighlight : true
    });

	$(function  (){
	    $(".toggle_tools").click();
	});

	$("#alloallostoragein_list_toolbar_").iToolBar({
	    id: "alloallostoragein_list_tb_01",
	    items: [
            {label: "手动入库",hidden:"${operationCode.webManulWarehouse}"=="1",onclick:function(){allostoragein_list_manual()},iconClass:'icon-download-alt'},
	        {label: "导出",hidden:"${operationCode.webExport}"=="1", onclick:function(){alloallostoragein_list_exportExecl()},iconClass:'icon-share'}
	   ]
	});

	var allostoragein_list_queryForm_Data = iTsai.form.serialize($("#alloallostoragein_list_queryForm"));

    allostoragein_list_grid = jQuery("#alloallostoragein_list_grid-table").jqGrid({
		url : context_path + "/allostoragein/list.do",
	    datatype : "json",
	    colNames : [ "主键","质保单号","批次号","单据号","入库时间","调出厂区","调出仓库","调出库位","调入厂区","调入仓库","调入库位","出库人","入库人","状态","目标库位"],
	    colModel : [
                {name : "id",index : "id",hidden:true},
                {name : "qaCode",index : "qaCode",width : 160},
                {name : "batchNo",index : "batch_No",width :170},
                {name : "allCode",index : "allCode",width :160},
                {name : "inStorageTime",index : "inStorageTime",width : 160},
                {name : "factoryName",index : "factoryName",width : 120},
                {name : "outWarehouseName",index : "outWarehouseName",width : 160},
                {name : "outShelfCode",index : "OUTSHELFCODE",width : 130},
                {name : "inFactoryName",index : "inFactoryName",width : 120},
                {name : "inWarehouseName",index : "inWarehouseName",width : 160},
                {name : "inShelfCode",index : "INSHELFCODE",width : 130},
                {name : "outStorageName",index : "outStorageName",width : 100},
                {name : "pickManName",index : "pickManName",width : 100},
                {name : "state",index : "state",width :100,formatter:function(cellValue,option,rowObject){
                        if(cellValue=='1'){
                            return "<span style=\"color:red;font-weight:bold;\">生效</span>";
                        }else if(cellValue=='2'){
                            return "<span style=\"color:blue;font-weight:bold;\">开始调拨出库</span>";
                        }else if(cellValue=='3'){
                            return "<span style=\"color:orange;font-weight:bold;\">完成调拨出库</span>";
                        }else if(cellValue=='4'){
                            return "<span style=\"color:blue;font-weight:bold;\">开始调拨入库</span>";
                        }else if(cellValue=='5'){
                            return "<span style=\"color:green;font-weight:bold;\">完成调拨入库</span>";
                        }
                     }
                },
                {name : "targetShelf",index : "target_shelf",width : 130}
              ],
	    rowNum : 20,
	    rowList : [ 10, 20, 30 ],
	    pager : "#alloallostoragein_list_grid-pager",
	    sortname : "t1.state",
	    sortorder : "asc",
        altRows: true,
        viewrecords : true,
        hidegrid:false,
 	    autowidth:false,
        multiselect:true,
        multiboxonly: true,
        shrinkToFit:false,
        autoScroll: true,
        loadComplete : function(data){
        	var table = this;
        	setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            $("#alloallostoragein_list_grid-table").closest(".ui-jqgrid-bdiv").css({ 'overflow-x': 'auto' });
            allostoragein_list_oriData = data;
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
	});

	jQuery("#alloallostoragein_list_grid-table").navGrid("#alloallostoragein_list_grid-pager",
    {edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#alloallostoragein_list_grid-pager",{
		caption:"",   
		buttonicon:"fa fa-refresh green",   
		onClickButton: function(){   
			$("#alloallostoragein_list_grid-table").jqGrid("setGridParam", {
                url : context_path + "/allostoragein/list.do",
            }).trigger("reloadGrid");
		}
	}).navButtonAdd("#alloallostoragein_list_grid-pager",{
        caption: "",
        buttonicon:"fa icon-cogs",
        onClickButton : function (){
            jQuery("#alloallostoragein_list_grid-table").jqGrid("columnChooser",{
                done: function(perm, cols){
                    // dynamicColumns(cols,materials_list_dynamicDefalutValue);
                    $("#alloallostoragein_list_grid-table").jqGrid("setGridWidth", $("#alloallostoragein_list_grid-div").width());
                    alloallostoragein_list_exportExcelIndex = perm;
                }
            });
        }
    });

	$(window).on("resize.jqGrid", function () {
		$("#alloallostoragein_list_grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);
		$("#alloallostoragein_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-10-
		$("#alloallostoragein_list_yy").outerHeight(true)-$("#alloallostoragein_list_fixed_tool_div").outerHeight(true)-
		$("#alloallostoragein_list_grid-pager").outerHeight(true)-$("#gview_alloallostoragein_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
	});
	$(window).triggerHandler("resize.jqGrid");

	//导出Excel
	function alloallostoragein_list_exportExecl(){
	    $("#alloallostoragein_list_hiddenForm #alloallostoragein_list_ids")
            .val(jQuery("#alloallostoragein_list_grid-table").jqGrid("getGridParam", "selarrrow"));
	    $("#alloallostoragein_list_hiddenForm #alloallostoragein_list_queryQacode")
            .val($("#alloallostoragein_list_queryForm #alloallostoragein_list_qaCode").val());
        $("#alloallostoragein_list_hiddenForm #alloallostoragein_list_queryOutShelfCode")
            .val($("#alloallostoragein_list_queryForm #alloallostoragein_list_outShelfCode").val());
        $("#alloallostoragein_list_hiddenForm #alloallostoragein_list_queryInShelfCode")
            .val($("#alloallostoragein_list_queryForm #alloallostoragein_list_inShelfCode").val());
        $("#alloallostoragein_list_hiddenForm #alloallostoragein_list_queryStartTime")
            .val($("#alloallostoragein_list_queryForm #alloallostoragein_list_createTime").val());
        $("#alloallostoragein_list_hiddenForm #alloallostoragein_list_queryEndTime")
            .val($("#alloallostoragein_list_queryForm #alloallostoragein_list_endTime").val());
        $("#alloallostoragein_list_hiddenForm #alloallostoragein_list_queryExportExcelIndex").val(alloallostoragein_list_exportExcelIndex);
	    $("#alloallostoragein_list_hiddenForm").submit();
	}

	//开始入库
	function alloallostoragein_list_startin(){
	    var selectAmount = getGridCheckedNum("#alloallostoragein_list_grid-table");
	    if(selectAmount==0){
	        layer.msg("请选择一条记录！",{icon:2});
	        return;
	    }else if(selectAmount>1){
	        layer.msg("只能选择一条记录！",{icon:8});
	        return;
	    }
	    var rowData = jQuery("#alloallostoragein_list_grid-table").jqGrid('getRowData',
            $("#alloallostoragein_list_grid-table").jqGrid('getGridParam','selrow')).state;
	    if (rowData.indexOf("开始调拨入库")>-1) {
	        layer.alert("入库中,请勿重复提交");
	        return;
	    }
	    if (rowData.indexOf("完成调拨入库")>-1) {
	        layer.alert("入库完成,请勿重复提交");
	        return;
	    }
	    $.post(context_path + "/allostoragein/storagestart.do", {
	        id:jQuery("#alloallostoragein_list_grid-table").jqGrid("getGridParam", "selrow")
        }, function (str){
	        $queryWindow=layer.open({
	            title : "仓库与行车确认",
	            type:1,
	            skin : "layui-layer-molv",
	            area : "600px",
	            shade : 0.6, //遮罩透明度
	            moveType : 1, //拖拽风格，0是默认，1是传统拖动
	            anim : 2,
	            content : str,
	            success: function (layero, index) {
	                layer.closeAll('loading');
	            }
	        });
	    });
	}

	//入库确认
	function alloallostoragein_list_confirmCars(){
	    var selectAmount = getGridCheckedNum("#alloallostoragein_list_grid-table");
	    if(selectAmount==0){
	        layer.msg("请选择一条记录！",{icon:2});
	        return;
	    }else if(selectAmount>1){
	        layer.msg("只能选择一条记录！",{icon:8});
	        return;
	    }
	    var rowData = jQuery("#alloallostoragein_list_grid-table").jqGrid('getRowData',
            $("#alloallostoragein_list_grid-table").jqGrid('getGridParam','selrow')).state;
	    if (rowData.indexOf("完成调拨出库")>-1) {
	        layer.alert("请先开始执行入库操作！");
	        return;
	    }
	    if (rowData.indexOf("完成调拨入库")>-1) {
	        layer.alert("入库完成,请勿重复提交！");
	        return;
	    }
	    $.post(context_path + "/allostoragein/storageconfirm.do", {
	        id:jQuery("#alloallostoragein_list_grid-table").jqGrid("getGridParam", "selrow")
        }, function (str){
	        $queryWindow=layer.open({
	            title : "入库确认",
	            type:1,
	            skin : "layui-layer-molv",
	            area : "600px",
	            shade : 0.6, //遮罩透明度
	            moveType : 1, //拖拽风格，0是默认，1是传统拖动
	            anim : 2,
	            content : str,
	            success: function (layero, index) {
	                layer.closeAll('loading');
	            }
	        });
	    });
	}

    //手动入库
    function allostoragein_list_manual(){
        var selectAmount = getGridCheckedNum("#alloallostoragein_list_grid-table");
        if(selectAmount==0){
            layer.msg("请选择一条记录！",{icon:2});
            return;
        }else if(selectAmount>1){
            layer.msg("只能选择一条记录！",{icon:8});
            return;
        }
        var rowData=jQuery("#alloallostoragein_list_grid-table").jqGrid('getRowData', $("#alloallostoragein_list_grid-table").jqGrid('getGridParam','selrow')).state;
        if (rowData.indexOf('完成调拨入库')>-1) {
            layer.alert("入库完成,请勿重复提交！");
            return;
        }
        $.post(context_path + "/allostoragein/storageManual.do", {
            id:jQuery("#alloallostoragein_list_grid-table").jqGrid("getGridParam", "selrow")
        }, function (str){
            $queryWindow=layer.open({
                title : "手动入库",
                type:1,
                skin : "layui-layer-molv",
                area : "600px",
                shade : 0.6, //遮罩透明度
                moveType : 1, //拖拽风格，0是默认，1是传统拖动
                anim : 2,
                content : str,
                success: function (layero, index) {
                    layer.closeAll('loading');
                }
            });
        });
    }

	 //查询按钮点击事件
	function alloallostoragein_list_queryOk(){
		 var queryParam = iTsai.form.serialize($("#alloallostoragein_list_queryForm"));
		 //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
		 alloallostoragein_list_queryByParam(queryParam);
	}
	
	function alloallostoragein_list_queryByParam(jsonParam) {
	    iTsai.form.deserialize($("#alloallostoragein_list_hiddenQueryForm"), jsonParam);
	    var queryParam = iTsai.form.serialize($("#alloallostoragein_list_hiddenQueryForm"));
	    var queryJsonString = JSON.stringify(queryParam); 
	    $("#alloallostoragein_list_grid-table").jqGrid("setGridParam",
	        {
	            postData: {queryJsonString: queryJsonString}
	        }
	    ).trigger("reloadGrid");
	}

	//重置
	function alloallostoragein_list_reset(){
		 iTsai.form.deserialize($("#alloallostoragein_list_queryForm"),allostoragein_list_queryForm_Data);
		 alloallostoragein_list_queryByParam(allostoragein_list_queryForm_Data);
	}

</script>