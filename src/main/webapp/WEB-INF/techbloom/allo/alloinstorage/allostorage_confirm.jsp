<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<div id="allostorage_confirm_page" class="row-fluid" style="height: inherit;">
	<form id="allostorage_confirm_carForm" class="form-horizontal" style="overflow: auto; height: calc(100% - 70px);">
        <input type="hidden" id="allostorage_confirm_id" name="id" value="${info.id}">
		<input type="hidden" id="allostorage_confirm_warehouseId" name="warehouseId" value="${info.warehouseId}">
		<input type="hidden" id="allostorage_confirm_carId" value="${info.carId}">
		<div class="control-group">
			<label class="control-label" >推荐库位：</label>
			<div class="controls">
				<div class="input-append span12">
					<textarea class="span11" readonly>${recommendCode}</textarea>
				</div>
			</div>
		</div>
        <div class="control-group">
            <label class="control-label" for="allostorage_confirm_shelfId" >库位：</label>
            <div class="controls">
                <div class="required span12" >
                    <input class="span11 select2_input" type = "text" id="allostorage_confirm_shelfId" name="shelfId" value="${info.shelfId}" placeholder="库位"/>
                    <input type="hidden" id="allostorage_confirm_wid" value="${info.shelfId}">
                    <input type="hidden" id="allostorage_confirm_wname" value="${info.shelfCode}">
                </div>
            </div>
        </div>
	</form>
	<div class="field-button" style="text-align: center;border-top: 0px;margin: 15px auto;">
		<span class="btn btn-info" onclick="saveForm();">
		   <i class="ace-icon fa fa-check bigger-110"></i>保存
		</span>
		<span class="btn btn-danger" onclick="layer.closeAll();">
		   <i class="icon-remove"></i>&nbsp;取消
		</span>
	</div>
</div>
<div id="allostorage_confirm_doing"></div> 
<script type="text/javascript">
    $("#allostorage_confirm_carForm").validate({
        rules:{
            "shelfId":{
                required:true,
            },
        },
        messages:{
            "shelfId":{
                required:"请选择库位！",
            },
        },
        errorClass: "help-inline",
        errorElement: "span",
        highlight:function(element, errorClass, validClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        }
    });
	//确定按钮点击事件
    function saveForm() {
        if ($("#allostorage_confirm_carForm").valid()) 
            confirmInstorage($("#allostorage_confirm_carForm").serialize());
    }
  
	
    function confirmInstorage(bean) {
    	saveForm = function(){
    		return;
    	}
        $.ajax({
            url: context_path + "/allostoragein/confirmInstorage",
            type: "POST",
            data: bean,
            dataType: "JSON",
            beforeSend:function(XMLHttpRequest){
                $("#allostorage_confirm_doing").html("<span style='color:#FF0000;font-weight:bold;'>请勿进行其他操作，正在处理，请稍后······</span>"); 
            },
            success: function (data) {
                if (Boolean(data.result)) {
                    layer.msg(data.msg, {icon: 1});
                    //关闭当前窗口
                    layer.close($queryWindow);
                    //刷新列表
                    $("#allostoragein_list_grid-table").jqGrid('setGridParam',
                 {
                     postData: {queryJsonString: ""}
                 }).trigger("reloadGrid");
                } else {
                    layer.alert(data.msg, {icon: 2});
                }
            },
            complete:function(XMLHttpRequest,textStatus){
                $("#allostorage_confirm_doing").empty(); 
            },
            error:function(XMLHttpRequest){
            	$("#allostorage_confirm_doing").empty(); 
                alert(XMLHttpRequest.readyState);
                alert("出错啦！！！");
            }
        });
    }

    var warehouseId = $("#allostorage_confirm_warehouseId").val();
    var carId = $("#allostorage_confirm_carId").val();
    
    $("#allostorage_confirm_carForm #allostorage_confirm_shelfId").select2({
        placeholder: "选择仓库",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/allostoragein/selectUnbindShelf.do?warehouseId="+warehouseId,
            type: "POST",
            dataType: 'json',
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                	carId:carId,
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    time: new Date()
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });

    if($("#allostorage_confirm_carForm #allostorage_confirm_wid").val()!=""){
        $("#allostorage_confirm_carForm #allostorage_confirm_shelfId").select2("data", {
            id: $("#allostorage_confirm_carForm #allostorage_confirm_wid").val(),
            text: $("#allostorage_confirm_carForm #allostorage_confirm_wname").val()
        });
    }

</script>