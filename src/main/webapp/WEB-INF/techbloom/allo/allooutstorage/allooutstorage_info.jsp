<%@ page language="java" import="java.lang.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
    String path = request.getContextPath();
%>
<div class="row-fluid" style="height: inherit;margin:0px;border: 0px">
    <input type="hidden" id="allooutstorage_info_infoId" value="${infoId}">
    <!-- 表格div -->
    <div id="allooutstorage_info_grid-div-c" style="width:100%;margin:10px auto;">
        <!-- 	表格工具栏 -->
        <div id="allooutstorage_info_fixed_tool_div" class="fixed_tool_div detailToolBar">
            <div id="allooutstorage_info___toolbar__-c" style="float:left;overflow:hidden;"></div>
        </div>

        <form id="hiddenForm" action="<%=path%>/allostorageout/detailExcel.do" method="POST" style="display: none;">
            <input id="hiddenForm_ids" name="ids" value=""/>
            <input id="info_id" name="infoid" value="">
        </form>
        <div style="margin-bottom:5px;margin-left: 25px;">
            <span class="btn btn-info" id="export">
               <i class="ace-icon fa fa-check bigger-110"></i>导出
            </span>
        </div>

        <!-- 物料详情信息表格 -->
        <table id="allooutstorage_info_grid-table-c" style="width:100%;height:100%;"></table>
        <!-- 表格分页栏 -->
        <div id="allooutstorage_info_grid-pager-c"></div>
    </div>
</div>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriDataDetail;
    var _grid_detail;        //表格对象

    _grid_detail=jQuery("#allooutstorage_info_grid-table-c").jqGrid({
        url : context_path + "/allostorageout/detailList.do?id="+$("#allooutstorage_info_infoId").val(),
        datatype : "json",
        colNames : [ "详情主键","质保单号","批次号","物料编码","物料名称","米数","完成出库时间","拣货人","状态"],
        colModel : [
            {name : "id",index : "id",width : 20,hidden:true},
            {name : "qaCode",index:"qaCode",width : 20},
            {name : "batchNo",index:"batchNo",width : 40},
            {name : "materialCode",index:"materialCode",width : 20},
            {name : "materialName",index:"materialName",width :30},
            {name : "meter",index:"meter",width : 10},
            {name : 'outStorageTime',index : 'outStorageTime',width : 20},
            {name : 'pickManName',index : 'pickManName',width : 20},
            {name : 'state',index : 'state',width : 20,
                formatter:function(cellValue,option,rowObject){
                    if(cellValue=='1'){
                        return "<span style=\"color:red;font-weight:bold;\">未开始调拨出库</span>";
                    }else if(cellValue=='2'){
                        return "<span style=\"color:blue;font-weight:bold;\">开始调拨出库</span>";
                    }else if(cellValue=='3'){
                        return "<span style=\"color:brown;font-weight:bold;\">完成调拨出库</span>";
                    }else if(cellValue=='4'){
                        return "<span style=\"color:deepskyblue;font-weight:bold;\">开始调拨入库</span>";
                    }else if(cellValue=='5'){
                        return "<span style=\"color:green;font-weight:bold;\">完成调拨入库</span>";
                    }
                }
            }
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : "#allooutstorage_info_grid-pager-c",
        sortname : "t1.id",
        sortorder : "asc",
        altRows: true,
        viewrecords : true,
        caption : "详情列表",
        autowidth:true,
        multiselect:true,
        multiboxonly: true,
        loadComplete : function(data)
        {
            var table = this;
            setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            oriDataDetail = data;
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据",
    });
    //在分页工具栏中添加按钮
    $("#allooutstorage_info_grid-table-c").navGrid("#allooutstorage_info_grid-pager-c",
    {edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#allooutstorage_info_grid-pager-c",{
        caption:"",
        buttonicon:"ace-icon fa fa-refresh green",
        onClickButton: function(){
            $("#allooutstorage_info_grid-table-c").jqGrid('setGridParam',
                {
                    url:context_path + "/allostorageout/detailList.do?id="+$("#allooutstorage_info_infoId").val()
                }
            ).trigger("reloadGrid");
        }
    });

    $(window).on("resize.jqGrid", function () {
        $("#allooutstorage_info_grid-table-c").jqGrid("setGridWidth", $("#allooutstorage_info_grid-div-c").width() - 3 );
        $("#allooutstorage_info_grid-table-c").jqGrid("setGridHeight", (document.documentElement.clientHeight -
        $("#allooutstorage_info_grid-pager-c").height() - 580) );
    });
    $(window).triggerHandler("resize.jqGrid");

    $("#export").click(function(){
        $("#hiddenForm_ids").val(jQuery("#allooutstorage_info_grid-table-c").jqGrid("getGridParam", "selarrrow"));
        $("#info_id").val($("#allooutstorage_info_infoId").val());

        $("#hiddenForm").submit();
    });
</script>