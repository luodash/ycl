<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<div id="allooutstorage_list_grid-div">
    <form id="allooutstorage_list_hiddenForm" action="<%=path%>/allostorageout/toExcel.do" method="POST" style="display:none;">
        <input id="allooutstorage_list_ids" name="ids" value=""/>
        <input id="allooutstorage_list_queryAllCode" name="queryAllCode" value=""/>
        <input id="allooutstorage_list_queryStartTime" name="queryStartTime" value=""/>
        <input id="allooutstorage_list_queryEndTime" name="queryEndTime" value=""/>
        <input id="allooutstorage_list_queryExportExcelIndex" name="queryExportExcelIndex" value=""/>
    </form>
    <!-- 隐藏区域：存放查询条件 -->
    <form id="allooutstorage_list_hiddenQueryForm" style="display:none;">
        <input name="allCode" value=""/>
        <input name="startTime" value=""/>
        <input name="endTime" value=""/>
    </form>
    <c:if test="${operationCode.webSearch==1}">
    <div class="query_box" id="allooutstorage_list_yy" title="查询选项">
        <form id="allooutstorage_list_queryForm" style="max-width:100%;">
            <ul class="form-elements">
                <li class="field-group field-fluid3">
                    <label class="inline" for="allooutstorage_list_allCode" style="margin-right:20px;width: 100%;">
                        <span class="form_label" style="width:80px;">调拨单号：</span>
                        <input id="allooutstorage_list_allCode" name="allCode" type="text" style="width: calc(100% - 85px);" placeholder="调拨单号">
                    </label>
                </li>
                <li class="field-group field-fluid3">
					<label class="inline" for="allooutstorage_list_startTime" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:78px;">起始时间：</span>
						<input id="allooutstorage_list_startTime" name="startTime" class="form-control date-picker" type="text" style="width: calc(100% - 84px );" placeholder="起始时间" />
					</label>			
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="allooutstorage_list_endTime" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:78px;">截止时间：</span>
						<input id="allooutstorage_list_endTime" name="endTime" class="form-control date-picker" type="text" style="width: calc(100% - 84px );" placeholder="截止时间" />
					</label>			
				</li>
            </ul>
            <div class="field-button" style="">
                <div class="btn btn-info" onclick="allooutstorage_list_queryOk();">
                    <i class="ace-icon fa fa-check bigger-110"></i>查询
                </div>
                <div class="btn" onclick="allooutstorage_list_reset();"><i class="ace-icon icon-remove"></i>重置</div>
            </div>
        </form>
    </div>
    </c:if>
    <div id="allooutstorage_list_fixed_tool_div" class="fixed_tool_div">
        <div id="allooutstorage_list___toolbar__" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="allooutstorage_list_grid-table" style="width:100%;height:100%;"></table>
    <div id="allooutstorage_list_grid-pager"></div>
</div>

<script type="text/javascript" src="<%=path%>/static/js/techbloom/allo/allooutstorage/allooutstorage_list.js"></script>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var allooutstorage_list_oriData;
    var _grid;
    var allooutstorage_list_dynamicDefalutValue="17e870237a2d470fb170daeafe6292e1";//列表码
    var allooutstorage_list_exportExcelIndex;

  	//时间控件
    $(".date-picker").datetimepicker({format: "YYYY-MM-DD"});

    $(function  (){
        $(".toggle_tools").click();
    });

    $("#allooutstorage_list___toolbar__").iToolBar({
        id: "allooutstorage_list___tb__01",
        items: [
		    {label: "执行", onclick:allooutstorage_list_execute, iconClass:'icon-tag'},
		    {label: "查看", onclick:allooutstorage_list_openInfoPage, iconClass:'icon-search'},
		    {label: "导出", onclick:function(){allooutstorage_list_toExcel();},iconClass:'icon-share'}
    	]
    });

    var allooutstorage_list_queryForm_Data = iTsai.form.serialize($('#allooutstorage_list_queryForm'));

    _grid = jQuery("#allooutstorage_list_grid-table").jqGrid({
        url : context_path + '/allostorageout/storageListData.do',
        datatype : "json",
        colNames : ["主键","调拨单号","创建时间","调出厂区","调入厂区","创建人","单据状态"],
        colModel : [
            {name : "id",index : "id",hidden:true},
            {name : "allCode",index : "allCode",width : 60},
            {name : "moveTime",index : "moveTime",width :70},
            {name : "factoryName",index : "factoryName",width : 60},
            {name : "inFactoryName",index : "inFactoryName",width : 60},
            {name : "userName",index : "userName",width : 60},
            {name : "alloState",index : "alloState",width :70,formatter:function(cellvalue,option,rowObject){
                    if (cellvalue!=null) {
                        if (cellvalue.indexOf('2')>-1 || cellvalue.indexOf('3')>-1 || cellvalue.indexOf('4')>-1){
                            return  "<span style=\"color:blue;font-weight:bold;\">调拨中</span>" ;
                        }else if (cellvalue.indexOf('1')>-1){
                            if (cellvalue.indexOf('5')>-1){
                                return "<span style=\"color:blue;font-weight:bold;\">调拨中</span>";
                            }else{
                                return "<span style=\"color:red;font-weight:bold;\">新建</span>";
                            }
                        }else if (cellvalue.indexOf('5')>-1) {
                            return "<span style=\"color:green;font-weight:bold;\">完成</span>";
                        }else {
                            return "<span style=\"color:orange;font-weight:bold;\">其他</span>";
                        }
                    }else {
                        return "<span style=\"color:black;font-weight:bold;\">无数据</span>";
                    }
                }
            }
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : '#allooutstorage_list_grid-pager',
        sortname : 't1.id',
        sortorder : "desc",
        altRows: false,
        viewrecords : true,
        autowidth:true,
        multiselect:true,
        multiboxonly: true,
        beforeRequest:function (){
            dynamicGetColumns(allooutstorage_list_dynamicDefalutValue,"allooutstorage_list_grid-table",$(window).width()-$("#sidebar").width() -7);
            //重新加载列属性
        },
        loadComplete : function(data){
            var table = this;
            setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            allooutstorage_list_oriData = data;
            $(window).triggerHandler('resize.jqGrid');
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
    });

    //在分页工具栏中添加按钮
    jQuery("#allooutstorage_list_grid-table").navGrid("#allooutstorage_list_grid-pager",
    {edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd('#allooutstorage_list_grid-pager',{
        caption:"",
        buttonicon:"ace-icon fa fa-refresh green",
        onClickButton: function(){
            //jQuery("#grid-table").trigger("reloadGrid");  //重新加载表格
            $("#allooutstorage_list_grid-table").jqGrid("setGridParam",
                {
                    postData: {queryJsonString:""} //发送数据
                }
            ).trigger("reloadGrid");
        }
    }).navButtonAdd("#allooutstorage_list_grid-pager",{
        caption: "",
        buttonicon:"fa icon-cogs",
        onClickButton : function (){
            jQuery("#allooutstorage_list_grid-table").jqGrid("columnChooser",{
                done: function(perm, cols){
                    dynamicColumns(cols,allooutstorage_list_dynamicDefalutValue);
                    $("#allooutstorage_list_grid-table").jqGrid("setGridWidth", $("#allooutstorage_list_grid-div").width()-3);
                    allooutstorage_list_exportExcelIndex = perm;
                }
            });
        }
    });

    $(window).on("resize.jqGrid", function () {
		$("#allooutstorage_list_grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);
		$("#allooutstorage_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-10-
		$("#allooutstorage_list_yy").outerHeight(true)-$("#allooutstorage_list_fixed_tool_div").outerHeight(true)-
		$("#allooutstorage_list_grid-pager").outerHeight(true)-$("#gview_allooutstorage_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
	});
    
    //查询
    function allooutstorage_list_queryOk(){
        var queryParam = iTsai.form.serialize($('#allooutstorage_list_queryForm'));
        //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
        allooutstorage_list_queryInstoreListByParam(queryParam);
    }
    
    function allooutstorage_list_queryInstoreListByParam(jsonParam) {
	    iTsai.form.deserialize($("#allooutstorage_list_hiddenQueryForm"), jsonParam);
	    var queryParam = iTsai.form.serialize($("#allooutstorage_list_hiddenQueryForm"));
	    var queryJsonString = JSON.stringify(queryParam); 
	    $("#allooutstorage_list_grid-table").jqGrid("setGridParam",
	        {
	            postData: {queryJsonString: queryJsonString}
	        }
	    ).trigger("reloadGrid");
	}

	//重置
    function allooutstorage_list_reset(){
        iTsai.form.deserialize($("#allooutstorage_list_queryForm"),allooutstorage_list_queryForm_Data);
        allooutstorage_list_queryInstoreListByParam(allooutstorage_list_queryForm_Data);
    }
    
</script>