<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
%>
<div id="movealloinfo_add_page" class="row-fluid" style="height: inherit;margin:0px">
    <form action="" class="form-horizontal" id="movealloinfo_add_baseInfor" name="baseInfor" method="post" target="_ifr" style="border-bottom: solid 2px #3b73af;">
        <input type="hidden" id="movealloinfo_add_id" name="id" value="-1">
        <div class="row" style="margin:0;padding:0;">
            <div class="control-groaup span6">
                <label class="control-label" for="movealloinfo_add_factoryCode" >调出厂区：</label>
                <div class="controls">
                    <div class="required span12" >
                        <input class="span10 select2_input" type = "text" id="movealloinfo_add_factoryCode" name="factoryCode" value="" placeholder="选择厂区"/>
                        <input type="hidden" id="movealloinfo_add_factoryid">
                        <input type="hidden" id="movealloinfo_add_factoryname">
                    </div>
                </div>
            </div>
         	<div class="control-groaup span6">
                <label class="control-label" for="movealloinfo_add_factoryCodeIn" >调入厂区：</label>
                <div class="controls">
                    <div class="required span12" >
                        <input class="span10 select2_input" type = "text" id="movealloinfo_add_factoryCodeIn" name="inFactoryCode" value="" placeholder="选择厂区"/>
                        <input type="hidden" id="movealloinfo_add_factoryidIn">
                        <input type="hidden" id="movealloinfo_add_factorynameIn">
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin:0;padding:0;">
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="movealloinfo_add_outWarehouseId" >调出仓库：</label>
                <div class="controls">
                    <div class="required" >
                        <input class="span10 select2_input" type = "text" id="movealloinfo_add_outWarehouseId" name="outWarehouseId" value="" placeholder="调出仓库"/>
                        <input type="hidden" id="movealloinfo_add_outId">
                        <input type="hidden" id="movealloinfo_add_outName">
                    </div>
                </div>
            </div>
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="movealloinfo_add_inWarehouseId" >调入仓库：</label>
                <div class="controls">
                    <div class="required" >
                        <input class="span10 select2_input" type = "text" id="movealloinfo_add_inWarehouseId" name="inWarehouseId" value="" placeholder="调入仓库"/>
                        <input type="hidden" id="movealloinfo_add_inId">
                        <input type="hidden" id="movealloinfo_add_inName">
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div id="movealloinfo_add_materialDiv" style="margin:10px;">
        <label class="inline" for="movealloinfo_add_qaCode">质保单号：</label>
        <input type="text" id = "movealloinfo_add_qaCode" name="qaCode" style="width:350px;margin-right:10px;" />
        <button id="movealloinfo_add_addMaterialBtn" class="btn btn-xs btn-primary" onclick="addDetail();">
            <i class="icon-plus" style="margin-right:6px;"></i>添加
        </button>
    </div>
    <div id="movealloinfo_add_grid-div-c" style="width:100%;margin:0px auto;">
        <div id="movealloinfo_add_fixed_tool_div" class="fixed_tool_div detailToolBar">
            <div id="movealloinfo_add___toolbar__-c" style="float:left;overflow:hidden;"></div>
        </div>
        <table id="movealloinfo_add_grid-table-c" style="width:100%;height:100%;"></table>
        <div id="movealloinfo_add_grid-pager-c"></div>
    </div>
</div>
<iframe src="about:blank" name="_ifr" height="0" class="hidden"></iframe>
<script type="text/javascript" src="<%=path%>/static/js/techbloom/allo/alloinfo/movealloinfo_add.js"></script>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriData;      //表格数据
    var _grid;        //表格对象
    var selectData = 0;   //存放物料选择框中的值
    var selectParam = "";  //存放之前的查询条件
	var factorySelect; //调出厂区
	var factorySelectIn	//调入厂区
    var allCode = "";   //调拨单号

    //单据保存按钮点击事件
    $("#movealloinfo_add_formSave").click(function(){
        if($('#movealloinfo_add_baseInfor').valid()){
            if($("#movealloinfo_add_inWarehouseId").val()==$("#movealloinfo_add_outWarehouseId").val()){
                layer.alert("请选择不同的调入仓库与调出仓库！")
                return
            }
            //通过验证：获取表单数据，保存表单信息
            saveFormInfo($('#movealloinfo_add_baseInfor').serialize());
        }
    });

    $("#movealloinfo_add_baseInfor").validate({
        rules: {
            "inWarehouseId":{
                required: true,
            },
            "outWarehouseId":{
                required: true,
            },
            "factoryCode":{
                required: true,
            },
            "inFactoryCode":{
                required: true,
            }
        },
        messages: {
            "inWarehouseId": {
                required: "请选择调入仓库!",
            },
            "outWarehouseId": {
                required: "请选择调出仓库!",
            },
            "factoryCode": {
                required: "请选择调出厂区!",
            },
            "inFactoryCode": {
                required: "请选择调入厂区!",
            }
        },
        errorClass: "help-inline",
        errorElement: "span",
        highlight:function(element, errorClass, validClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        }
    })

    //清空物料多选框中的值
    function removeChoice(){
        $("#s2id_qaCode .select2-choices").children(".select2-search-choice").remove();
        $("#movealloinfo_add_qaCode").select2("val","");
        selectData = 0;
    }

    $('[data-rel=tooltip]').tooltip();

    //工具栏
    $("#movealloinfo_add___toolbar__-c").iToolBar({
        id:"movealloinfo_add___tb__01",
        items:[
            {label:"删除", onclick:delDetail}
        ]
    });

    //初始化表格
    _grid =  $("#movealloinfo_add_grid-table-c").jqGrid({
        url : context_path + "/moveAllo/detailList.do?id="+$("#movealloinfo_add_id").val(),
        datatype : "json",
        colNames : [ "详情主键","质保号","批次号","调出库位","调入库位","物料编码","数量","盘类型","盘规格","盘外径","盘内径"],
        colModel : [
        	{name : "id",index : "id",width : 20,hidden:true},
            {name : "qaCode",index:"qaCode",width : 15},
            {name : "batchNo",index:"batchNo",width : 20},
            {name : "outShelfCode",index:"outShelfCode",width : 25},
            {name : "inShelfCode",index:"inShelfCode",width : 25},
            {name : "materialCode",index:"materialCode",width : 20},
            {name : "meter",index:"meter",width : 10},
            {name : "drumType",index:"drumType",width : 10},
            {name : "model",index:"model",width : 35},
            {name : "outerDiameter",index:"outerDiameter",width : 10},
            {name : "innerDiameter",index:"innerDiameter",width : 10}
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : "#movealloinfo_add_grid-pager-c",
        sortname : "t1.id",
        sortorder : "asc",
        altRows: true,
        viewrecords : true,
        autowidth:true,
        multiselect:true,
        multiboxonly: true,
        loadComplete : function(data){
            var table = this;
            setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            oriData = data;
            $(window).triggerHandler("resize.jqGrid");
        },
        cellEdit: true,
        cellsubmit : "clientArray",
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据",
    });
    //在分页工具栏中添加按钮
    $("#movealloinfo_add_grid-table-c").navGrid("#movealloinfo_add_grid-pager-c",
        {edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#movealloinfo_add_grid-pager-c",{
            caption:"",
            buttonicon:"ace-icon fa fa-refresh green",
            onClickButton: function(){
                $("#movealloinfo_add_grid-table-c").jqGrid("setGridParam",
                    {
                        url:context_path + "/moveAllo/detailList.do?id="+$("#movealloinfo_add_id").val(),
                        postData: {
                            id:$("#movealloinfo_add_baseInfor #movealloinfo_add_id").val(),
                            queryJsonString:""
                        } //发送数据  :选中的节点
                    }
                ).trigger("reloadGrid");
            }
        });

    $(window).on("resize.jqGrid", function () {
        $("#movealloinfo_add_grid-table-c").jqGrid("setGridWidth", $("#movealloinfo_add_grid-div-c").width() - 3 );

        var height = $(".layui-layer-title",_grid.parents(".layui-layer")).height() + $("#movealloinfo_add_baseInfor").outerHeight(true)+
        $("#movealloinfo_add_materialDiv").outerHeight(true) + $("#movealloinfo_add_grid-pager-c").outerHeight(true)+
        $("#movealloinfo_add_fixed_tool_div.fixed_tool_div.detailToolBar").outerHeight(true) +
        $("#gview_movealloinfo_add_grid-table-c .ui-jqgrid-hbox").outerHeight(true);

        $("#movealloinfo_add_grid-table-c").jqGrid("setGridHeight",_grid.parents(".layui-layer").height()-height);
    });
    $(window).triggerHandler("resize.jqGrid");

</script>
