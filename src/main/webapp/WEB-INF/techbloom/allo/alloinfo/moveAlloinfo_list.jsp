<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<div id="moveAlloinfo_list_grid-div">
    <!-- 隐藏区域：存放查询条件 -->	
    <form id="moveAlloinfo_list_hiddenForm" action="<%=path%>/allotCon/toExcel.do" method="POST" style="display:none;">
        <input id="moveAlloinfo_list_ids" name="ids" value=""/>
        <input id="moveAlloinfo_list_queryAllCode" name="queryAllCode" value=""/>
        <input id="moveAlloinfo_list_queryFactoryCodeOut" name="queryFactoryCodeOut" value=""/>
        <input id="moveAlloinfo_list_queryWarehouseOut" name="queryWarehouseOut" value=""/>
        <input id="moveAlloinfo_list_queryFactoryCodeIn" name="queryFactoryCodeIn" value=""/>
        <input id="moveAlloinfo_list_queryWarehouseIn" name="queryWarehouseIn" value=""/>
        <input id="moveAlloinfo_list_queryStartTime" name="queryStartTime" value=""/>
        <input id="moveAlloinfo_list_queryEndTime" name="queryEndTime" value=""/>
        <input id="moveAlloinfo_list_queryCreater" name="queryCreater" value=""/>
        <input id="moveAlloinfo_list_queryExportExcelIndex" name="queryExportExcelIndex" value=""/>
    </form>
    <form id="moveAlloinfo_list_hiddenQueryForm" style="display:none;">
        <input name="allCode" value=""/>
        <input name="factoryCodeOut" value=""/>
        <input name="warehouseOut" value=""/>
        <input name="factoryCodeIn" value=""/>
        <input name="warehouseIn" value=""/>
        <input name="startTime" value=""/>
        <input name="endTime" value=""/>
        <input name="creater" value=""/>
    </form>

    <c:if test="${operationCode.webSearch==1}">
    <div class="query_box" id="moveAlloinfo_list_yy" title="查询选项">
        <form id="moveAlloinfo_list_queryForm" style="max-width:100%;">
            <ul class="form-elements">
                <li class="field-group field-fluid3">
                    <label class="inline" for="moveAlloinfo_list_allCode" style="margin-right:20px;width: 100%;">
                        <span class="form_label" style="width:80px;">单据号：</span>
                        <input id="moveAlloinfo_list_allCode" name="allCode" type="text" style="width: calc(100% - 85px);" placeholder="调拨（移库）单号">
                    </label>
                </li>
                <li class="field-group field-fluid3">
                    <label class="inline" for="moveAlloinfo_list_startTime" style="margin-right:20px;width:100%;">
                        <span class="form_label" style="width:80px;">创建时间起：</span>
                        <input type="text" class="form-control date-picker" id="moveAlloinfo_list_startTime" name="startTime" value="" style="width: calc(100% - 85px);" placeholder="创建时间起" />
                    </label>
                </li>
                <li class="field-group field-fluid3">
                    <label class="inline" for="moveAlloinfo_list_endTime" style="margin-right:20px;width:100%;">
                        <span class="form_label" style="width:80px;">创建时间止：</span>
                        <input type="text" class="form-control date-picker" id="moveAlloinfo_list_endTime" name="endTime" value="" style="width: calc(100% - 85px);" placeholder="创建时间止" />
                    </label>
                </li>
                
                <li class="field-group-top field-group field-fluid3">
					<label class="inline" for="moveAlloinfo_list_factoryCodeOut" style="margin-right:20px;width: 100%;">
						 <span class="form_label" style="width:80px;">调出厂区：</span>
						 <input id="moveAlloinfo_list_factoryCodeOut" name="factoryCodeOut" type="text" style="width: calc(100% - 85px);" placeholder="调出厂区">
					</label>
				 </li>
				 <li class="field-group field-fluid3">
					 <label class="inline" for="moveAlloinfo_list_warehouseOut" style="margin-right:20px;width: 100%;">
						 <span class="form_label" style="width:80px;">调出仓库：</span>
						 <input id="moveAlloinfo_list_warehouseOut" name="warehouseOut" type="text" style="width:  calc(100% - 85px);" placeholder="调出仓库">
					 </label>
				 </li>
                 <li class="field-group field-fluid3">
					<label class="inline" for="moveAlloinfo_list_factoryCodeIn" style="margin-right:20px;width: 100%;">
						 <span class="form_label" style="width:80px;">调入厂区：</span>
						 <input id="moveAlloinfo_list_factoryCodeIn" name="factoryCodeIn" type="text" style="width: calc(100% - 85px);" placeholder="调入厂区">
					</label>
				 </li>
				 
				 <li class="field-group-top field-group field-fluid3">
					 <label class="inline" for="moveAlloinfo_list_warehouseIn" style="margin-right:20px;width: 100%;">
						 <span class="form_label" style="width:80px;">调入仓库：</span>
						 <input id="moveAlloinfo_list_warehouseIn" name="warehouseIn" type="text" style="width:  calc(100% - 85px);" placeholder="调入仓库">
					 </label>
				 </li>
				 <li class="field-group field-fluid3">
                    <label class="inline" for="moveAlloinfo_list_creater" style="margin-right:20px;width:100%;">
                        <span class="form_label" style="width:80px;">创建人：</span>
                        <input id="moveAlloinfo_list_creater" name="creater" type="text" style="width: calc(100% - 85px);" placeholder="工号或姓名">
                    </label>
                </li>
            </ul>
            <div class="field-button" style="">
                <div class="btn btn-info" onclick="moveAlloinfo_list_queryOk();">
                    <i class="ace-icon fa fa-check bigger-110"></i>查询
                </div>
                <div class="btn" onclick="moveAlloinfo_list_reset();"><i class="ace-icon icon-remove"></i>重置</div>
                <a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>
            </div>
        </form>
    </div>
    </c:if>

    <div id="moveAlloinfo_list_fixed_tool_div" class="fixed_tool_div">
        <div id="moveAlloinfo_list_toolbar_" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="moveAlloinfo_list_grid-table" style="width:100%;height:100%;"></table>
    <div id="moveAlloinfo_list_grid-pager"></div>
</div>

<script type="text/javascript" src="<%=path%>/static/js/techbloom/allo/alloinfo/movealloinfo_list.js"></script>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var moveAlloinfo_list_grid;
    var moveAlloinfo_list_dynamicDefalutValue="17e870237a2d470fb170daeafe6292e1";//列表码
    var moveAlloinfo_list_factoryout_selectid;
    var moveAlloinfo_list_factoryin_selectid;
    var moveAlloinfo_list_exportExcelIndex;

  	//时间控件
    $(".date-picker").datetimepicker({
        format: "YYYY-MM-DD HH:mm:ss" ,
        autoclose : true,
        todayHighlight : true
    });

    $("input").keypress(function (e) {
        if (e.which == 13) {
            moveAlloinfo_list_queryOk();
        }
    });

    $(function  (){
        $(".toggle_tools").click();
    });

    $("#moveAlloinfo_list_toolbar_").iToolBar({
        id: "moveAlloinfo_list_tb_01",
        items: [
            {label: "新增",hidden:"${operationCode.webAdd}"=="1", onclick: moveAlloinfo_list_openAddPage, iconClass:'glyphicon glyphicon-plus'},
            {label: "编辑",hidden:"${operationCode.webEdit}"=="1", onclick: moveAlloinfo_list_openEditPage, iconClass:'glyphicon glyphicon-pencil'},
            {label: "查看",hidden:"${operationCode.webInfo}"=="1", onclick:moveAlloinfo_list_openInfoPage, iconClass:'icon-search'},
            {label: "删除",hidden:"${operationCode.webDel}"=="1", onclick:function(){moveAlloinfo_list_deleteByIds();}, iconClass:'glyphicon glyphicon-trash'},
            {label: "导出",hidden:"${operationCode.webExport}"=="1", onclick:function(){moveAlloinfo_list_toExcel();},iconClass:'icon-share'}
        ]
    });

    moveAlloinfo_list_grid = jQuery("#moveAlloinfo_list_grid-table").jqGrid({
        url : context_path + '/moveAllo/alloctionPageList.do',
        datatype : "json",
        colNames : ["主键","单据号" ,"单据创建时间","调出厂区","调出仓库","调入厂区","调入仓库","变更开始时间","变更结束时间","创建人","单据状态"],
        colModel : [
            {name : "id",index : "id",hidden:true},
            {name : "allCode",index : "allCode",width : 160},
            {name : "moveTime",index : "moveTime",width :160},
            {name : "factoryName",index : "factoryName",width : 200},
            {name : "outWarehouseName",index : "outWarehouseName",width : 160},
            {name : "inFactoryName",index : "inFactoryName",width : 200},
            {name : "inWarehouseName",index : "inWarehouseName",width : 160},
            {name : "startOutTimeStr",index : "startOutTimeStr",width : 140},
            {name : "endInTimeStr",index : "endInTimeStr",width : 140},
            {name : "userName",index : "userName",width : 80},
            {name : "alloState",index : "alloState",width : 60,formatter:function(cellvalue,option,rowObject){
                    if (cellvalue) {
                        if (cellvalue.indexOf('2')>-1 || cellvalue.indexOf('3')>-1 || cellvalue.indexOf('4')>-1){
                            return  "<span style=\"color:blue;font-weight:bold;\">变更中</span>" ;
                        }else if (cellvalue.indexOf('1')>-1){
                            if (cellvalue.indexOf('5')>-1){
                                return "<span style=\"color:blue;font-weight:bold;\">变更中</span>";
                            }else{
                                return "<span style=\"color:red;font-weight:bold;\">新建</span>";
                            }
                        }else if (cellvalue.indexOf('5')>-1) {
                            return "<span style=\"color:green;font-weight:bold;\">变更完成</span>";
                        }else {
                            return "<span style=\"color:orange;font-weight:bold;\">其他</span>";
                        }
                    }else {
                        return "<span style=\"color:red;font-weight:bold;\">无数据</span>";
                    }
                }
            }
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : '#moveAlloinfo_list_grid-pager',
        sortname : 't1.id',
        sortorder : "desc",
        altRows: false,
        viewrecords : true,
        hidegrid:false,
        autowidth:false,
        multiselect:true,
        multiboxonly: true,
        shrinkToFit:false,
        autoScroll: true,
        beforeRequest:function (){
            dynamicGetColumns(moveAlloinfo_list_dynamicDefalutValue,"moveAlloinfo_list_grid-table",$(window).width()-$("#sidebar").width() -7);
            //重新加载列属性
        },
        loadComplete : function(data){
            var table = this;
            setTimeout(
                function(){
                    updatePagerIcons(table);
                    enableTooltips(table);
                }, 1000);
            $(window).triggerHandler('resize.jqGrid');
            $("#moveAlloinfo_list_grid-table").closest(".ui-jqgrid-bdiv").css({ 'overflow-x': 'auto' });
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
    });
    //在分页工具栏中添加按钮
    jQuery("#moveAlloinfo_list_grid-table").navGrid("#moveAlloinfo_list_grid-pager",
    {edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd('#moveAlloinfo_list_grid-pager',{
        caption:"",
        buttonicon:"ace-icon fa fa-refresh green",
        onClickButton: function(){
            $("#moveAlloinfo_list_grid-table").jqGrid("setGridParam",
                {
                    postData: {queryJsonString:""} //发送数据
                }
            ).trigger("reloadGrid");
        }
    }).navButtonAdd("#moveAlloinfo_list_grid-pager",{
        caption: "",
        buttonicon:"fa icon-cogs",
        onClickButton : function (){
            jQuery("#moveAlloinfo_list_grid-table").jqGrid("columnChooser",{
                done: function(perm, cols){
                    dynamicColumns(cols,moveAlloinfo_list_dynamicDefalutValue);
                    $("#moveAlloinfo_list_grid-table").jqGrid("setGridWidth", $("#moveAlloinfo_list_grid-div").width()-3);
                    moveAlloinfo_list_exportExcelIndex = perm;
                }
            });
        }
    });
    
    $(window).on("resize.jqGrid", function () {
		$("#moveAlloinfo_list_grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);
		$("#moveAlloinfo_list_grid-table").jqGrid("setGridHeight", $(".container-fluid").height()-10- $("#moveAlloinfo_list_yy").outerHeight(true)-
        $("#moveAlloinfo_list_fixed_tool_div").outerHeight(true)- $("#moveAlloinfo_list_grid-pager").outerHeight(true)-
        $("#gview_moveAlloinfo_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
	});
    
    var moveAlloinfo_list_queryForm_data = iTsai.form.serialize($('#moveAlloinfo_list_queryForm'));

    //查询按钮
    function moveAlloinfo_list_queryOk(){
        //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
        moveAlloinfo_list_queryInstoreListByParam(iTsai.form.serialize($('#moveAlloinfo_list_queryForm')));
    }

    //重置
    function moveAlloinfo_list_reset(){
    	moveAlloinfo_list_factoryout_selectid = '';
    	moveAlloinfo_list_factoryin_selectid = '';
        iTsai.form.deserialize($("#moveAlloinfo_list_queryForm"),moveAlloinfo_list_queryForm_data);
        $("#moveAlloinfo_list_queryForm #moveAlloinfo_list_factoryCodeOut").select2("val","");
        $("#moveAlloinfo_list_queryForm #moveAlloinfo_list_factoryCodeIn").select2("val","");
        $("#moveAlloinfo_list_queryForm #moveAlloinfo_list_warehouseOut").select2("val","");
        $("#moveAlloinfo_list_queryForm #moveAlloinfo_list_warehouseIn").select2("val","");
        moveAlloinfo_list_queryInstoreListByParam(moveAlloinfo_list_queryForm_data);
    }
    
  	//调出厂区
    $("#moveAlloinfo_list_queryForm #moveAlloinfo_list_factoryCodeOut").select2({
        placeholder: "选择厂区",
        minimumInputLength:0,   //至少输入n个字符，才去加载数据
        allowClear: true,  //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches:"没有结果",
        formatSearching:"搜索中...",
        formatAjaxError:"加载出错啦！",
        ajax : {
            url: context_path+"/factoryArea/getFactoryList",
            type:"POST",
            dataType : 'json',
            delay : 250,
            data: function (term,pageNo) {     //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term,    //联动查询的字符
                    pageSize: 15,    //一次性加载的数据条数
                    pageNo:pageNo    //页码
                }
            },
            results: function (data,pageNo) {
                var res = data.result;
                if(res.length>0){   //如果没有查询到数据，将会返回空串
                    var more = (pageNo*15)<data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results:res,
                        more:more
                    };
                }else{
                    return {
                        results:{}
                    };
                }
            },
            cache : true
        }
    });
  	
  	//调入厂区
    $("#moveAlloinfo_list_queryForm #moveAlloinfo_list_factoryCodeIn").select2({
        placeholder: "选择厂区",
        minimumInputLength:0,   //至少输入n个字符，才去加载数据
        allowClear: true,  //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches:"没有结果",
        formatSearching:"搜索中...",
        formatAjaxError:"加载出错啦！",
        ajax : {
            url: context_path+"/factoryArea/getFactoryList",
            type:"POST",
            dataType : 'json',
            delay : 250,
            data: function (term,pageNo) {     //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term,    //联动查询的字符
                    pageSize: 15,    //一次性加载的数据条数
                    pageNo:pageNo    //页码
                }
            },
            results: function (data,pageNo) {
                var res = data.result;
                if(res.length>0){   //如果没有查询到数据，将会返回空串
                    var more = (pageNo*15)<data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results:res,more:more
                    };
                }else{
                    return {
                        results:{}
                    };
                }
            },
            cache : true
        }
    });
  	
  	//调出仓库
    $("#moveAlloinfo_list_queryForm #moveAlloinfo_list_warehouseOut").select2({
        placeholder: "选择仓库",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/car/selectWarehouse",
            type: "POST",
            dataType: 'json',
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    factoryCodeId: moveAlloinfo_list_factoryout_selectid
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });
  	
  	//调入仓库
    $("#moveAlloinfo_list_queryForm #moveAlloinfo_list_warehouseIn").select2({
        placeholder: "选择仓库",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/car/selectWarehouse",
            type: "POST",
            dataType: 'json',
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    factoryCodeId:moveAlloinfo_list_factoryin_selectid
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });
  	
    $("#moveAlloinfo_list_queryForm #moveAlloinfo_list_factoryCodeOut").on("change.select2",function(){
        $("#moveAlloinfo_list_queryForm #moveAlloinfo_list_factoryCodeOut").trigger("keyup")
    	moveAlloinfo_list_factoryout_selectid = $("#moveAlloinfo_list_queryForm #moveAlloinfo_list_factoryCodeOut").val();
    });
    
    $("#moveAlloinfo_list_queryForm #moveAlloinfo_list_factoryCodeIn").on("change.select2",function(){
        $("#moveAlloinfo_list_queryForm #moveAlloinfo_list_factoryCodeIn").trigger("keyup")
    	moveAlloinfo_list_factoryin_selectid = $("#moveAlloinfo_list_queryForm #moveAlloinfo_list_factoryCodeIn").val();
    });
</script>