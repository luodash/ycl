<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="instorage_list_grid-div">
    <form id="instorage_list_hiddenForm" action="<%=path%>/AccountHoist/instoragelExcel" method="POST" style="display: none;">
        <input id="instorage_list_ids" name="ids" value=""/>
        <input id="instorage_list_queryStartTime" name="queryStartTime" value=""/>
        <input id="instorage_list_queryEndTime" name="queryEndTime" value=""/>
        <input id="instorage_list_queryInstorageName" name="instorageName" value=""/>
        <input id="instorage_list_queryDdh" name="queryDdh" value=""/>
        <input id="instorage_list_queryCq" name="queryCq" value=""/>
        <input id="instorage_list_queryWlbh" name="queryWlbh" value=""/>
        <input id="instorage_list_queryQaCode" name="queryQaCode" value=""/>
        <input id="instorage_list_queryBatchno" name="queryBatchno" value=""/>
        <input id="instorage_list_queryEntityNo" name="queryEntityNo" value=""/>
        <input id="instorage_list_queryExportExcelIndex" name="queryExportExcelIndex" value=""/>
    </form>
    <form id="instorage_list_hiddenQueryForm" style="display:none;">
        <input name="startTime" value=""/>
        <input name="endTime" value=""/>
        <input name="instorageName" value=""/>
        <input name="orderno" value=""/>
        <input name="factoryarea" value=""/>
        <input name="mcode" value=""/>
        <input name="qaCode" value=""/>
        <input name="batchno" value=""/>
        <input name="entityNo" value=""/>
    </form>
    <c:if test="${operationCode.webSearch==1}">
    <div class="query_box" id="instorage_list_yy" title="查询选项">
        <form id="instorage_list_queryForm" style="max-width:100%;">
            <ul class="form-elements">
                <li class="field-group field-fluid3">
                    <label class="inline" for="instorage_list_startTime" style="margin-right:20px;width: 100%;">
                        <span class="form_label" style="width:80px;">时间起：</span>
                        <input type="text" class="form-control date-picker" id="instorage_list_startTime" name="startTime" value="" style="width: calc(100% - 85px);" placeholder="开始时间" />
                    </label>
                </li>
                <li class="field-group field-fluid3">
                    <label class="inline" for="instorage_list_endTime" style="margin-right:20px;width: 100%;">
                        <span class="form_label" style="width:80px;">时间止：</span>
                        <input type="text" class="form-control date-picker" id="instorage_list_endTime" name="endTime" value="" style="width: calc(100% - 85px);" placeholder="结束时间" />
                    </label>
                </li>
                <li class="field-group field-fluid3">
                    <label class="inline"  style="margin-right:20px;width: 100%;">
                        <span class="form_label" style="width:80px;">入库人：</span>
                        <input id="instorage_list_instorageName" name="instorageName" type="text" style="width: calc(100% - 85px);" placeholder="入库人姓名">
                    </label>
                </li>

                <li class="field-group-top field-group field-fluid3">
                    <label class="inline" for="instorage_list_ddh" style="margin-right:20px;width:100%;">
                        <span class="form_label" style="width:80px;">订单号：</span>
                        <input type="text" id="instorage_list_ddh"  name="orderno" value="" style="width: calc(100% - 85px);" placeholder="订单号" />
                    </label>
                </li>
                <li class=" field-group field-fluid3">
                    <label class="inline" for="instorage_list_cq" style="margin-right:20px;width: 100%;">
                        <span class="form_label" style="width:80px;">厂区：</span>
                        <input id="instorage_list_cq" name="factoryarea" type="text" style="width: calc(100% - 85px);" placeholder="厂区">
                    </label>
                </li>
                <li class="field-group field-fluid3">
                    <label class="inline" for="instorage_list_wlbh" style="margin-right:20px;width:100%;">
                        <span class="form_label" style="width:80px;">物料编号：</span>
                        <input type="text" id="instorage_list_wlbh"   name="mcode" value="" style="width: calc(100% - 85px);" placeholder="物料编号" />
                    </label>
                </li>

                <li class="field-group-top field-group field-fluid3">
                    <label class="inline" for="instorage_list_qaCode" style="margin-right:20px;width: 100%;">
                        <span class="form_label" style="width:80px;">质保单号：</span>
                        <input id="instorage_list_qaCode" name="qaCode" type="text" style="width: calc(100% - 85px);" placeholder="质保单号">
                    </label>
                </li>
                <li class="field-group field-fluid3">
                    <label class="inline" for="instorage_list_batchno" style="margin-right:20px;width:100%;">
                        <span class="form_label" style="width:80px;">批次号：</span>
                        <input type="text" id="instorage_list_batchno"  name="batchno" style="width: calc(100% - 85px);" placeholder="批次号" />
                    </label>
                </li>

                <li class=" field-group field-fluid3">
                    <label class="inline" for="instorage_list_entityNo" style="margin-right:20px;width: 100%;">
                        <span class="form_label" style="width:80px;">工单号：</span>
                        <input id="instorage_list_entityNo" name="entityNo" type="text" style="width: calc(100% - 85px);" placeholder="工单号">
                    </label>
                </li>
            </ul>
            <div class="field-button" style="">
                <div class="btn btn-info" onclick="instorage_list_queryOk();">
                    <i class="ace-icon fa fa-check bigger-110"></i>查询
                </div>
                <div class="btn" onclick="instorage_list_reset();"><i class="ace-icon icon-remove"></i>重置</div>
                <a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>
            </div>
        </form>
    </div>
    </c:if>
    <div id="instorage_list_fixed_tool_div" class="fixed_tool_div">
        <div id="instorage_list_toolbar_" style="float:left;overflow:hidden;"></div>
    </div>
    <div  style="overflow-x:auto">
        <table id="instorage_list_grid-table" style="width:100%;height:100%;"></table>
    </div>
    <div id="instorage_list_grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
	var instorage_list_grid;
	var instorage_list_exportExcelIndex;

    $("input").keypress(function (e) {
        if (e.which == 13) {
            instorage_list_queryOk();
        }
    });

    $(".date-picker").datetimepicker({
        format: "YYYY-MM-DD HH:mm:ss" ,
        autoclose : true,
        todayHighlight : true
    });

	$(function  (){
	    $(".toggle_tools").click();
	});

	$("#instorage_list_toolbar_").iToolBar({
	    id: "instorage_list_tb_01",
	    items: [
	        {label: "导出",hidden:"${operationCode.webExport}"=="1", onclick:function(){instorage_list_exportExecl()},iconClass:'icon-share'}
	   ]
	});

	var instorage_list_queryForm_Data = iTsai.form.serialize($("#instorage_list_queryForm"));

    instorage_list_grid = jQuery("#instorage_list_grid-table").jqGrid({
		url : context_path + "/AccountHoist/instorageList.do",
	    datatype : "json",
        colNames : [ "主键","状态","质保单号","批次号","订单号","订单行号","工单号", "组织机构","厂区","仓库","库位","物料编码","物料名称","初始化时间","包装时间",
             "包装人姓名","扫码时间","扫码人","入库时间","入库人姓名","数量","单位","重量","颜色","段号","报验单号","盘号","盘具编码","盘规格","盘外径","货物类型"],
        colModel : [
            {name : "id",index : "id",hidden:true},
            {name : "state",index : "state,id",width : 70,
                formatter:function(cellValue,option,rowObject){
                    if(cellValue==1){
                        return "<span style='color:red;font-weight:bold;'>未装包</span>";
                    }else if(cellValue==2){
                        return "<span style='color:orange;font-weight:bold;'>已包装</span>";
                    }else if(cellValue==3){
                        return "<span style='color:blue;font-weight:bold;'>入库中</span>";
                    }else if(cellValue==4){
                        return "<span style='color:green;font-weight:bold;'>入库完成</span>";
                    }else if(cellValue==5){
                        return "<span style='color:grey;font-weight:bold;'>已退库</span>";
                    }
                }
        	},
            {name : "qaCode",index : "qaCode",width : 150},
            {name : "batchNo",index : "batchNo",width : 200},
            {name : "ordernum",index : "ordernum",width : 130},
            {name : "orderline",index : "orderline",width : 70},
            {name : "entityNo",index : "entityNo",width :110},
            {name : "name",index : "name",width : 180},
            {name : "factoryName",index : "factoryName",width : 180},
            {name : "warehouseName",index : "warehouseName",width : 180},
            {name : "shelfCode",index : "shelfCode",width : 140},
            {name : "materialCode",index : "materialCode",width : 110},
            {name : "materialName",index : "materialName",width : 190},
            {name : "createtime",index : "createtime",width : 130},
            {name : "confirmTime",index : "confirmTime",width : 130},
            {name : "confirmName",index : "confirmName",width : 80},
            {name : "startStorageTime",index : "startStorageTime",width : 130},
            {name : "startstorageName",index : "startstorageName",width : 80},
            {name : "instorageTime",index : "instorageTime",width : 130},
            {name : "instorageName",index : "instorageName",width : 80},
            {name : "meter",index : "meter",width : 40},
            {name : "unit",index : "unit",width : 40},
            {name : "weight",index : "weight",width : 60},
            {name : "colour",index : "colour",width : 60},
            {name : "segmentno",index : "segmentno",width : 90},
            {name : "inspectno",index : "inspectno",width : 90},
            {name : "dishcode",index : "dishcode",width : 150},
            {name : "dishnumber",index : "dishnumber",width :90},
            {name : "model",index : "model",width : 160},
            {name : "outerDiameter",index : "outerDiameter",width : 60},
            {name : "storagetype",index : "storagetype",width : 60,
                 formatter:function(cellValue,option,rowObject){
                     if(cellValue==0){
                         return "正常生产";
                     }else if(cellValue==1){
                         return "销售退货";
                     }else if(cellValue==2){
                         return "外协采购";
                     }
                 }
            }
        ],
	    rowNum : 20,
	    rowList : [ 10, 20, 30 ],
	    pager : "#instorage_list_grid-pager",
	    sortname : "a.confirmtime",
	    sortorder : "desc",
        altRows: true,
        viewrecords : true,
        hidegrid:false,
        autowidth:false,
        multiselect:true,
        multiboxonly: true,
        shrinkToFit:false,
        autoScroll: true,
        loadComplete : function(data){
           	var table = this;
           	setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            $("#instorage_list_grid-table").closest(".ui-jqgrid-bdiv").css({ 'overflow-x': 'auto' });
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
	});

	jQuery("#instorage_list_grid-table").navGrid("#instorage_list_grid-pager",
    {edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#instorage_list_grid-pager",{
		caption:"",
		buttonicon:"fa fa-refresh green",
		onClickButton: function(){
			$("#instorage_list_grid-table").jqGrid("setGridParam", {
                url : context_path + "/storagein/list.do",
            }).trigger("reloadGrid");
		}
	}).navButtonAdd("#instorage_list_grid-pager",{
        caption: "",
        buttonicon:"fa icon-cogs",
        onClickButton : function (){
            jQuery("#instorage_list_grid-table").jqGrid("columnChooser",{
                done: function(perm, cols){
                    instorage_list_exportExcelIndex = perm;
                    $("#instorage_list_grid-table").jqGrid("setGridWidth", $("#instorage_list_grid-div").width());
                }
            });
        }
    });

	$(window).on("resize.jqGrid", function () {
		$("#instorage_list_grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);
		$("#instorage_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()- $("#instorage_list_yy").outerHeight(true)- 10-
        $("#instorage_list_fixed_tool_div").outerHeight(true)- $("#instorage_list_grid-pager").outerHeight(true)-
		$("#gview_instorage_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
	});
	$(window).triggerHandler("resize.jqGrid");

	function instorage_list_exportExecl(){
	    $("#instorage_list_hiddenForm #instorage_list_ids").val(jQuery("#instorage_list_grid-table").jqGrid("getGridParam", "selarrrow"));

	    $("#instorage_list_hiddenForm #instorage_list_queryQaCode").val($("#instorage_list_queryForm #instorage_list_qaCode").val());
	    $("#instorage_list_hiddenForm #instorage_list_queryBatchno").val($("#instorage_list_queryForm #instorage_list_batchno").val());
	    $("#instorage_list_hiddenForm #instorage_list_queryDdh").val($("#instorage_list_queryForm #instorage_list_ddh").val());
	    $("#instorage_list_hiddenForm #instorage_list_queryCq").val($("#instorage_list_queryForm #instorage_list_cq").val());
        $("#instorage_list_hiddenForm #instorage_list_queryWlbh").val($("#instorage_list_queryForm #instorage_list_wlbh").val());
        $("#instorage_list_hiddenForm #instorage_list_queryStartTime").val($("#instorage_list_queryForm #instorage_list_startTime").val());
        $("#instorage_list_hiddenForm #instorage_list_queryEndTime").val($("#instorage_list_queryForm #instorage_list_endTime").val());
        $("#instorage_list_hiddenForm #instorage_list_queryEntityNo").val($("#instorage_list_queryForm #instorage_list_entityNo").val());
        $("#instorage_list_hiddenForm #instorage_list_queryInstorageName").val($("#instorage_list_queryForm #instorage_list_instorageName").val());
        $("#instorage_list_hiddenForm #instorage_list_queryExportExcelIndex").val(instorage_list_exportExcelIndex);

	    $("#instorage_list_hiddenForm").submit();
	}

	/**
	 * 查询按钮点击事件
	 */
	function instorage_list_queryOk(){
		 var queryParam = iTsai.form.serialize($("#instorage_list_queryForm"));
		 //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
		 instorage_list_queryByParam(queryParam);
	}
	
 	function instorage_list_queryByParam(jsonParam) {
	    iTsai.form.deserialize($("#instorage_list_hiddenQueryForm"), jsonParam);
	    var queryParam = iTsai.form.serialize($("#instorage_list_hiddenQueryForm"));
	    var queryJsonString = JSON.stringify(queryParam);
	    $("#instorage_list_grid-table").jqGrid("setGridParam",
	        {
	            postData: {queryJsonString: queryJsonString}
	        }
	    ).trigger("reloadGrid");
	}

	//重置
	function instorage_list_reset(){
         $("#instorage_list_queryForm #instorage_list_cq").select2("val","");
		 iTsai.form.deserialize($("#instorage_list_queryForm"),instorage_list_queryForm_Data);
		 instorage_list_queryByParam(instorage_list_queryForm_Data);
	}

    //厂区
    $("#instorage_list_queryForm #instorage_list_cq").select2({
        placeholder: "选择厂区",
        minimumInputLength:0,   //至少输入n个字符，才去加载数据
        allowClear: true,  //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches:"没有结果",
        formatSearching:"搜索中...",
        formatAjaxError:"加载出错啦！",
        ajax : {
            url: context_path+"/factoryArea/getFactoryList",
            type:"POST",
            dataType : 'json',
            delay : 250,
            data: function (term,pageNo) {     //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term,    //联动查询的字符
                    pageSize: 15,    //一次性加载的数据条数
                    pageNo:pageNo    //页码
                }
            },
            results: function (data,pageNo) {
                var res = data.result;
                if(res.length>0){   //如果没有查询到数据，将会返回空串
                    var more = (pageNo*15)<data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results:res,more:more
                    };
                }else{
                    return {
                        results:{}
                    };
                }
            },
            cache : true
        }
    });

</script>