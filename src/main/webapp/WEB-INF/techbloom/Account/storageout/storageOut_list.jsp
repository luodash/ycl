<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="storageOut_list_grid-div">
    <form id="storageOut_list_hiddenForm" action="<%=path%>/AccountHoist/toStorageOutExcel.do" method="POST" style="display: none;">
        <input id="storageOut_list_ids" name="ids" value=""/>
        <input id="storageOut_list_queryExportExcelIndex" name="queryExportExcelIndex" value=""/>
        <input id="storageOut_list_queryOutstorageName" name="outstorageName" value=""/>
        <input id="storageOut_list_queryQacode" name="qaCode" value=""/>
        <input id="storageOut_list_queryStartTime" name="startTime" value=""/>
        <input id="storageOut_list_queryEndTime" name="endTime" value=""/>
        <input id="storageOut_list_queryShipNo" name="shipNo" value=""/>
    </form>
    <form id="storageOut_list_hiddenQueryForm" style="display:none;">
        <input name="outstorageName" value=""/>
        <input name="qaCode" value=""/>
        <input name="startTime" value="">
        <input name="endTime" value=""/>
        <input name="shipNo" value=""/>
    </form>

    <c:if test="${operationCode.webSearch==1}">
    <div class="query_box" id="storageOut_list_yy" title="查询选项">
        <form id="storageOut_list_queryForm" style="max-width:100%;">
            <ul class="form-elements">
                <li class="field-group field-fluid3">
                    <label class="inline" style="margin-right:20px;width: 100%;">
                        <span class="form_label" style="width:80px;">发货人：</span>
                        <input id="storageOut_list_outstorageName" name="outstorageName" type="text" style="width: calc(100% - 125px);" placeholder="发货人">
                    </label>
                </li>
                <li class="field-group field-fluid3">
					<label class="inline" for="storageOut_list_startTime" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:80px;">发货时间起：</span>
						<input id="storageOut_list_startTime" name="startTime" class="form-control date-picker" type="text" style="width: calc(100% - 125px );" placeholder="（下架）起始时间" />
					</label>			
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="storageOut_list_endTime" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:80px;">发货时间止：</span>
						<input id="storageOut_list_endTime" name="endTime" class="form-control date-picker" type="text" style="width: calc(100% - 125px );" placeholder="（下架）截止时间" />
					</label>			
				</li>

                <li class="field-group-top field-group field-fluid3">
                    <label class="inline" for="storageOut_list_qaCode" style="margin-right:20px;width: 100%;">
                        <span class="form_label" style="width:80px;">质保单号：</span>
                        <input id="storageOut_list_qaCode" name="qaCode" type="text" style="width: calc(100% - 125px);" placeholder="质保单号">
                    </label>
                </li>
                <li class="field-group field-fluid3">
                    <label class="inline" for="storageOut_list_shipNo" style="margin-right:20px;width:100%;">
                        <span class="form_label" style="width:78px;">发货单号：</span>
                        <input id="storageOut_list_shipNo" name="shipNo" type="text" style="width: calc(100% - 125px);" placeholder="提货单号">
                    </label>
                </li>
            </ul>
            <div class="field-button" style="">
                <div class="btn btn-info" onclick="storageOut_list_queryOk();">
                    <i class="ace-icon fa fa-check bigger-110"></i>查询
                </div>
                <div class="btn" onclick="storageOut_list_reset();"><i class="ace-icon icon-remove"></i>重置</div>
                <a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>
            </div>
        </form>
    </div>
    </c:if>

    <div id="storageOut_list_fixed_tool_div" class="fixed_tool_div">
        <div id="storageOut_list_toolbar_" style="float:left;overflow:hidden;"></div>
    </div>
    <div style="overflow-x:auto"><table id="storageOut_list_grid-table" style="width:100%;height:100%;"></table></div>
    <div id="storageOut_list_grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
	var storageOut_list_grid;
    var storageOut_list_exportExcelIndex;

	//时间控件
    $(".date-picker").datetimepicker({
        format: "YYYY-MM-DD HH:mm:ss" ,
        autoclose : true,
        todayHighlight : true
    });

    $("input").keypress(function (e) {
        if (e.which == 13) {
            storageOut_list_queryOk();
        }
    });

	$(function  (){
	    $(".toggle_tools").click();
	});

	$("#storageOut_list_toolbar_").iToolBar({
	    id: "storageOut_list_tb_01",
	    items: [
	        {label: "导出",hidden:"${operationCode.webExport}"=="1",onclick:function(){storageOut_list_toExecl();},iconClass:'icon-share'}
	   ]
	});
	
	var storageOut_list_queryForm_data = iTsai.form.serialize($("#storageOut_list_queryForm"));

    storageOut_list_grid = jQuery("#storageOut_list_grid-table").jqGrid({
	    url : context_path + "/AccountHoist/outstoragelist.do",
	    datatype : "json",
	    colNames : ["id","质保单号","批次号","单据号","物料编号","物料名称","盘号","销售经理","盘类型","盘规格","盘外径","盘内径","米数","仓库编码",
            "仓库","库位","发货人","发货时间","出库人","出库时间"],
	    colModel : [
	    	{name : "id",index : "id",hidden : true},
	        {name : "qaCode",index : "qaCode",width:180},
	        {name : "batchNo",index : "BATCH_NO",width:180},
            {name : "shipNo",index : "shipNo",width:180},
	        {name : "materialCode",index : "materialCode",width : 160},
	        {name : "materialName",index : "materialName",width :170},
            {name : "dishcode",index : "dishcode",width :170},
	        {name : "salesname",index : "salesname",width : 170},
	        {name : "drumType",index : "drumType",width : 50},
	        {name : "model",index : "model",width : 190},
            {name : "outerDiameter",index : "outerDiameter",width : 50},
            {name : "innerDiameter",index : "innerDiameter",width :50},
	        {name : "meter",index : "meter",width : 50},
	        {name : "warehouseCode",index : "warehouseCode",hidden : true},
	        {name : "warehouseName",index : "warehouseName",width : 100},
	        {name : "shelfCode",index : "shelfCode",width : 140},
            {name : "startstorageName",index : "startstorageName",width : 90},
            {name : "startStorageTime",index : "startStorageTime",width : 140},
            {name : "pickManName",index : "pickManName",width : 90},
            {name : "outStorageTime",index : "outStorageTime",width : 140}

	    ],
	    rowNum : 20,
	    rowList : [ 10, 20, 30 ],
	    pager : "#storageOut_list_grid-pager",
	    sortname : "v.id",
	    sortorder : "asc",
        altRows: true,
        viewrecords : true,
        hidegrid:false,
    	autowidth:false,
        multiselect:true,
        multiboxonly:true,
        shrinkToFit:false,
        autoScroll: true,
        loadComplete : function(data){
           	var table = this;
           	setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
           	$("#storageOut_list_grid-table").closest(".ui-jqgrid-bdiv").css({ 'overflow-x': 'auto' });
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
	});

	jQuery("#storageOut_list_grid-table").navGrid("#storageOut_list_grid-pager",
    {edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#storageOut_list_grid-pager",{
		caption:"",   
		buttonicon:"fa fa-refresh green",   
		onClickButton: function(){   
			$("#storageOut_list_grid-table").jqGrid("setGridParam",{
                postData: {queryJsonString:""} //发送数据
			}).trigger("reloadGrid");
		}
	}).navButtonAdd("#storageOut_list_grid-pager",{
        caption: "",
        buttonicon:"fa icon-cogs",
        onClickButton : function (){
            jQuery("#storageOut_list_grid-table").jqGrid("columnChooser",{
                done: function(perm, cols){
                    $("#storageOut_list_grid-table").jqGrid("setGridWidth", $("#storageOut_list_grid-div").width());
                    storageOut_list_exportExcelIndex = perm;
                }
            });
        }
    });

	$(window).on("resize.jqGrid", function () {
		$("#storageOut_list_grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);
		$("#storageOut_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-10-
		$("#storageOut_list_yy").outerHeight(true)-$("#storageOut_list_fixed_tool_div").outerHeight(true)-
		$("#storageOut_list_grid-pager").outerHeight(true)-$("#gview_storageOut_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
	});
	
	$(window).triggerHandler("resize.jqGrid");

	/**
	 * 查询按钮点击事件
	 */
	 function storageOut_list_queryOk(){
		 var queryParam = iTsai.form.serialize($("#storageOut_list_queryForm"));
		 //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
		 storageOut_list_queryByParam(queryParam);
	 }

	 function storageOut_list_queryByParam(jsonParam) {
	    iTsai.form.deserialize($("#storageOut_list_hiddenQueryForm"), jsonParam);
	    var queryParam = iTsai.form.serialize($("#storageOut_list_hiddenQueryForm"));
	    var queryJsonString = JSON.stringify(queryParam); 
	    $("#storageOut_list_grid-table").jqGrid("setGridParam",
	        {
	            postData: {queryJsonString: queryJsonString}
	        }
	    ).trigger("reloadGrid");
	 }
 
	 //清空重置
	function storageOut_list_reset(){	
		 iTsai.form.deserialize($("#storageOut_list_queryForm"),storageOut_list_queryForm_data);
		 storageOut_list_queryByParam(storageOut_list_queryForm_data);
	}

	 //导出Excel报表
	function storageOut_list_toExecl(){
	    $("#storageOut_list_hiddenForm #storageOut_list_ids").val(jQuery("#storageOut_list_grid-table").jqGrid("getGridParam", "selarrrow"));
        $("#storageOut_list_hiddenForm #storageOut_list_queryExportExcelIndex").val(storageOut_list_exportExcelIndex);
        $("#storageOut_list_hiddenForm #storageOut_list_queryStartTime").val($("#storageOut_list_queryForm #storageOut_list_startTime").val());
        $("#storageOut_list_hiddenForm #storageOut_list_queryEndTime").val($("#storageOut_list_queryForm #storageOut_list_endTime").val());
        $("#storageOut_list_hiddenForm #storageOut_list_queryQacode").val($("#storageOut_list_queryForm #storageOut_list_qaCode").val());
        $("#storageOut_list_hiddenForm #storageOut_list_queryOutstorageName").val($("#storageOut_list_queryForm #storageOut_list_outstorageName").val());
        $("#storageOut_list_hiddenForm #storageOut_list_queryShipNo").val($("#storageOut_list_queryForm #storageOut_list_shipNo").val());
	    $("#storageOut_list_hiddenForm").submit();
	}

</script>