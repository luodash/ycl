<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="hangcar_list_grid-div">
    <form id="hangcar_list_hiddenForm" action="<%=path%>/AccountHoist/toHangCarExcel.do" method="POST" style="display: none;">
        <input id="hangcar_list_ids" name="ids" value=""/>
        <input id="hangcar_list_queryExportExcelIndex" name="queryExportExcelIndex" value=""/>
        <input id="hangcar_list_queryCarerName" name="carerName" value=""/>
        <input id="hangcar_list_queryStartTime" name="startTime" value=""/>
        <input id="hangcar_list_queryEndTime" name="endTime" value=""/>
    </form>
    <form id="hangcar_list_hiddenQueryForm" style="display:none;">
        <input name="carerName" value=""/>
        <input name="startTime" value="">
        <input name="endTime" value=""/>
    </form>

    <c:if test="${operationCode.webSearch==1}">
    <div class="query_box" id="hangcar_list_yy" title="查询选项">
        <form id="hangcar_list_queryForm" style="max-width:100%;">
            <ul class="form-elements">
                <li class="field-group field-fluid3">
                    <label class="inline" style="margin-right:20px;width: 100%;">
                        <span class="form_label" style="width:80px;">行车工：</span>
                        <input id="hangcar_list_carerName" name="carerName" type="text" style="width: calc(100% - 125px);" placeholder="行车工姓名">
                    </label>
                </li>
                <li class="field-group field-fluid3">
					<label class="inline" for="hangcar_list_startTime" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:80px;">时间起：</span>
						<input id="hangcar_list_startTime" name="startTime" class="form-control date-picker" type="text" style="width: calc(100% - 125px );"
                               placeholder="完成吊装起始时间" />
					</label>			
				</li>
				<li class="field-group field-fluid3">
					<label class="inline" for="hangcar_list_endTime" style="margin-right:20px;width:100%;">
						<span class="form_label" style="width:80px;">时间止：</span>
						<input id="hangcar_list_endTime" name="endTime" class="form-control date-picker" type="text" style="width: calc(100% - 125px );"
                               placeholder="完成吊装截止时间" />
					</label>			
				</li>

            </ul>
            <div class="field-button" style="">
                <div class="btn btn-info" onclick="hangcar_list_queryOk();">
                    <i class="ace-icon fa fa-check bigger-110"></i>查询
                </div>
                <div class="btn" onclick="hangcar_list_reset();"><i class="ace-icon icon-remove"></i>重置</div>
                <%--<a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>--%>
            </div>
        </form>
    </div>
    </c:if>

    <div id="hangcar_list_fixed_tool_div" class="fixed_tool_div">
        <div id="hangcar_list_toolbar_" style="float:left;overflow:hidden;"></div>
    </div>
    <div style="overflow-x:auto"><table id="hangcar_list_grid-table" style="width:100%;height:100%;"></table></div>
    <div id="hangcar_list_grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
	var hangcar_list_grid;
    var hangcar_list_exportExcelIndex;

	//时间控件
    $(".date-picker").datetimepicker({
        format: "YYYY-MM-DD HH:mm:ss" ,
        autoclose : true,
        todayHighlight : true
    });

    $("input").keypress(function (e) {
        if (e.which == 13) {
            hangcar_list_queryOk();
        }
    });

	$(function  (){
	    $(".toggle_tools").click();
	});

	$("#hangcar_list_toolbar_").iToolBar({
	    id: "hangcar_list_tb_01",
	    items: [
	        {label: "导出",hidden:"${operationCode.webExport}"=="1",onclick:function(){hangcar_list_toExecl();},iconClass:'icon-share'}
	   ]
	});
	
	var hangcar_list_queryForm_data = iTsai.form.serialize($("#hangcar_list_queryForm"));

    hangcar_list_grid = jQuery("#hangcar_list_grid-table").jqGrid({
	    url : context_path + "/AccountHoist/hangtCarlist.do",
	    datatype : "json",
	    colNames : ["id","质保单号","批次号","物料编号","物料名称","厂区","仓库","库位","吊装时间","行车工"],
	    colModel : [
	    	{name : "id",index : "id",hidden : true},
	        {name : "QACODE",index : "qacode",width:180},
	        {name : "BATCH_NO",index : "batch_no",width:180},
	        {name : "MATERIALCODE",index : "materialcode",width : 160},
            {name : "MATERIALNAME",index : "materialname",width :270},
	        {name : "FACTORYNAME",index : "factoryname",width : 170},
	        {name : "WAREHOUSENAME",index : "warehousename",width : 150},
	        {name : "SHELFCODE",index : "shelfcode",width:100},
	        {name : "HANGTIME",index : "hangtime",width : 140},
            {name : "NAME",index : "name",width : 140}
	    ],
	    rowNum : 20,
	    rowList : [ 10, 20, 30 ],
	    pager : "#hangcar_list_grid-pager",
	    sortname : "id",
	    sortorder : "asc",
        altRows: true,
        viewrecords : true,
        hidegrid:false,
    	autowidth:false,
        multiselect:true,
        multiboxonly:true,
        shrinkToFit:false,
        autoScroll: true,
        loadComplete : function(data){
           	var table = this;
           	setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
           	$("#hangcar_list_grid-table").closest(".ui-jqgrid-bdiv").css({ 'overflow-x': 'auto' });
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
	});

	jQuery("#hangcar_list_grid-table").navGrid("#hangcar_list_grid-pager",
    {edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#hangcar_list_grid-pager",{
		caption:"",   
		buttonicon:"fa fa-refresh green",   
		onClickButton: function(){   
			$("#hangcar_list_grid-table").jqGrid("setGridParam",{
                postData: {queryJsonString:""} //发送数据
			}).trigger("reloadGrid");
		}
	}).navButtonAdd("#hangcar_list_grid-pager",{
        caption: "",
        buttonicon:"fa icon-cogs",
        onClickButton : function (){
            jQuery("#hangcar_list_grid-table").jqGrid("columnChooser",{
                done: function(perm, cols){
                    $("#hangcar_list_grid-table").jqGrid("setGridWidth", $("#hangcar_list_grid-div").width());
                    hangcar_list_exportExcelIndex = perm;
                }
            });
        }
    });

	$(window).on("resize.jqGrid", function () {
		$("#hangcar_list_grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);
		$("#hangcar_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-10-
		$("#hangcar_list_yy").outerHeight(true)-$("#hangcar_list_fixed_tool_div").outerHeight(true)-
		$("#hangcar_list_grid-pager").outerHeight(true)-$("#gview_hangcar_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
	});
	
	$(window).triggerHandler("resize.jqGrid");

	/**
	 * 查询按钮点击事件
	 */
	 function hangcar_list_queryOk(){
		 var queryParam = iTsai.form.serialize($("#hangcar_list_queryForm"));
		 //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
		 hangcar_list_queryByParam(queryParam);
	 }

	 function hangcar_list_queryByParam(jsonParam) {
	    iTsai.form.deserialize($("#hangcar_list_hiddenQueryForm"), jsonParam);
	    var queryParam = iTsai.form.serialize($("#hangcar_list_hiddenQueryForm"));
	    var queryJsonString = JSON.stringify(queryParam); 
	    $("#hangcar_list_grid-table").jqGrid("setGridParam", {
            postData: {queryJsonString: queryJsonString}
        }).trigger("reloadGrid");
	 }
 
	 //清空重置
	function hangcar_list_reset(){	
		 iTsai.form.deserialize($("#hangcar_list_queryForm"),hangcar_list_queryForm_data);
		 hangcar_list_queryByParam(hangcar_list_queryForm_data);
	}

	 //导出Excel报表
	function hangcar_list_toExecl(){
	    $("#hangcar_list_hiddenForm #hangcar_list_ids").val(jQuery("#hangcar_list_grid-table").jqGrid("getGridParam", "selarrrow"));
        $("#hangcar_list_hiddenForm #hangcar_list_queryExportExcelIndex").val(hangcar_list_exportExcelIndex);
        $("#hangcar_list_hiddenForm #hangcar_list_queryStartTime").val($("#hangcar_list_queryForm #hangcar_list_startTime").val());
        $("#hangcar_list_hiddenForm #hangcar_list_queryEndTime").val($("#hangcar_list_queryForm #hangcar_list_endTime").val());
        $("#hangcar_list_hiddenForm #hangcar_list_queryCarerName").val($("#hangcar_list_queryForm #hangcar_list_carerName").val());
	    $("#hangcar_list_hiddenForm").submit();
	}

</script>