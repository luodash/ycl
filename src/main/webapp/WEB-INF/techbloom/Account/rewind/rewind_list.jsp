<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="rewind_list_grid-div" >
    <form id="rewind_list_hiddenForm" action="<%=path%>/AccountHoist/rewindExcel" method="POST" style="display: none;">
        <input id="rewind_list_ids" name="ids" value=""/>
        <input id="rewind_list_queryQaCode" name="queryQaCode" value=""/>
        <input id="rewind_list_queryEntityNo" name="queryEntityNo" value=""/>
        <input id="rewind_list_queryStartTime" name="queryRewindStartTime" value="">
        <input id="rewind_list_queryEndTime" name="queryRewindEndTime" value=""/>
        <input id="rewind_list_cq2" name="factoryarea" value=""/>
        <input id="rewind_list_wlbh2" name="mcode" value=""/>
        <input id="rewind_list_ddh2" name="orderno" value=""/>
        <input id="rewind_list_batchno2" name="batchno" value=""/>
        <input id="rewind_list_queryExportExcelIndex" name="queryExportExcelIndex" value=""/>
        <input id="rewind_list_rewindname" name="queryRewindname" value=""/>
    </form>
    <form id="rewind_list_hiddenQueryForm" style="display:none;">
        <input  name="qaCode" value=""/>
        <input  name="entityNo" value=""/>
        <input  name="rewindStartTime" value="">
        <input  name="rewindEndTime" value=""/>
        <input  name="factoryarea" value=""/>
        <input  name="mcode" value=""/>
        <input  name="orderno" value=""/>
        <input  name="batchno" value=""/>
        <input  name="rewindname" value=""/>
    </form>
    <c:if test="${operationCode.webSearch==1}">
        <div class="query_box" id="rewind_list_yy" title="查询选项">
            <form id="rewind_list_queryForm" style="max-width:100%;">
                <ul class="form-elements">
                    <li class="field-group field-fluid3">
                        <label class="inline" style="margin-right:20px;width:100%;">
                            <span class="form_label" style="width:80px;">时间起：</span>
                            <input type="text" class="form-control date-picker" id="rewind_list_startTime" name="rewindStartTime" value="" style="width: calc(100% - 85px);" placeholder="复绕时间起" />
                        </label>
                    </li>
                    <li class="field-group field-fluid3">
                        <label class="inline" style="margin-right:20px;width:100%;">
                            <span class="form_label" style="width:80px;">时间止：</span>
                            <input type="text" class="form-control date-picker" id="rewind_list_endTime" name="rewindEndTime" value="" style="width: calc(100% - 85px);" placeholder="复绕时间止" />
                        </label>
                    </li>
                    <li class="field-group field-fluid3">
                        <label class="inline" style="margin-right:20px;width:100%;">
                            <span class="form_label" style="width:80px;">复绕人：</span>
                            <input type="text" id="rewind_list_rewind"  name="rewindname" value="" style="width: calc(100% - 85px);" placeholder="复绕人姓名" />
                        </label>
                    </li>

                    <li class="field-group-top field-group field-fluid3">
                        <label class="inline" for="rewind_list_entityNo" style="margin-right:20px;width: 100%;">
                            <span class="form_label" style="width:80px;">工单号：</span>
                            <input id="rewind_list_entityNo" name="entityNo" type="text" style="width: calc(100% - 85px);" placeholder="工单号">
                        </label>
                    </li>
                    <li class=" field-group field-fluid3">
                        <label class="inline" for="rewind_list_factory" style="margin-right:20px;width: 100%;">
                            <span class="form_label" style="width:80px;">厂区：</span>
                            <input id="rewind_list_factory" name="factoryarea" type="text" style="width: calc(100% - 85px);" placeholder="厂区">
                        </label>
                    </li>
                    <li class="field-group field-fluid3">
                        <label class="inline" for="rewind_list_wlbh" style="margin-right:20px;width:100%;">
                            <span class="form_label" style="width:80px;">物料编号：</span>
                            <input type="text" id="rewind_list_wlbh"   name="mcode" value="" style="width: calc(100% - 85px);" placeholder="物料编号" />
                        </label>
                    </li>

                    <li class="field-group-top field-group field-fluid3">
                        <label class="inline" for="rewind_list_qaCode" style="margin-right:20px;width: 100%;">
                            <span class="form_label" style="width:80px;">质保单号：</span>
                            <input id="rewind_list_qaCode" name="qaCode" type="text" style="width: calc(100% - 85px);" placeholder="质保单号">
                        </label>
                    </li>
                    <li class="field-group field-fluid3">
                        <label class="inline" for="rewind_list_batchno" style="margin-right:20px;width:100%;">
                            <span class="form_label" style="width:80px;">批次号：</span>
                            <input type="text" id="rewind_list_batchno"  name="batchno" value="" style="width: calc(100% - 85px);" placeholder="批次号" />
                        </label>
                    </li>
                    <li class="field-group field-fluid3">
                        <label class="inline" for="rewind_list_ddh" style="margin-right:20px;width:100%;">
                            <span class="form_label" style="width:80px;">订单号：</span>
                            <input type="text" id="rewind_list_ddh"  name="orderno" value="" style="width: calc(100% - 85px);" placeholder="订单号" />
                        </label>
                    </li>
                </ul>

                <div class="field-button" style="">
                    <div class="btn btn-info" onclick="rewind_list_queryOk();">
                        <i class="ace-icon fa fa-check bigger-110"></i>查询
                    </div>
                    <div class="btn" onclick="rewind_list_reset();"><i class="ace-icon icon-remove"></i>重置</div>
                    <a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>
                </div>
            </form>
        </div>
    </c:if>
    <div id="rewind_list_fixed_tool_div" class="fixed_tool_div">
        <div id="rewind_list_toolbar_" style="float:left;overflow:hidden;"></div>
    </div>
    <div  style="overflow-x:auto">
        <table id="rewind_list_grid-table" style="width:100%;height:100%;"></table>
    </div>
    <div id="rewind_list_grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
    var rewind_list_grid;
    var rewind_list_exportExcelIndex;

    $("input").keypress(function (e) {
        if (e.which == 13) {
            rewind_list_queryOk();
        }
    });

    $(".date-picker").datetimepicker({
        format: "YYYY-MM-DD HH:mm:ss" ,
        autoclose : true,
        todayHighlight : true
    });

    $(function  (){
        $(".toggle_tools").click();
    });

    $("#rewind_list_toolbar_").iToolBar({
        id: "rewind_list_tb_01",
        items: [
            {label: "导出",hidden:"${operationCode.webExport}"=="1", onclick:rewind_list_exportExecl,iconClass:'icon-share'}
    	]
    });

    var rewind_list_queryForm_Data = iTsai.form.serialize($("#rewind_list_queryForm"));

    rewind_list_grid = jQuery("#rewind_list_grid-table").jqGrid({
        url : context_path + "/AccountHoist/rewindList.do",
        datatype : "json",
        colNames : [ "主键","状态","质保单号","批次号","订单号","订单行号","工单号", "厂区","物料编码","物料名称", "复绕时间","复绕工","数量","单位","重量",
            "颜色","段号", "盘具编码","盘规格","盘外径","货物类型"],
        colModel : [
            {name : "id",index : "id",hidden:true},
            {name : "rewindState",index : "rewindstate,id",width : 60,
                formatter:function(cellValue,option,rowObject){
                	if(cellValue==1){
                        return "<span style='color:green;font-weight:bold;'>已复绕</span>";
                    }else{
                        return "<span style='color:red;font-weight:bold;'>未复绕</span>";
                    }
                }
            },
            {name : "qaCode",index : "qaCode",width : 180},
            {name : "batchNo",index : "batchNo",width : 180},
            {name : "ordernum",index : "ordernum",width : 140},
            {name : "orderline",index : "orderline",width : 60},
            {name : "entityNo",index : "entityNo",width :110},
            {name : "name",index : "name",width : 220},
            {name : "materialCode",index : "materialCode",width : 140},
            {name : "materialName",index : "materialName",width : 220},
            {name : "rewindingTime",index : "rewindingTime",width : 130},
            {name : "rewindName",index : "rewindName",width : 80},
            {name : "meter",index : "meter",width : 60},
            {name : "unit",index : "unit",width : 30},
            {name : "weight",index : "weight",width : 90},
            {name : "colour",index : "colour",width : 60},
            {name : "segmentno",index : "segmentno",width : 90},
            {name : "dishnumber",index : "dishnumber",width :90},
            {name : "model",index : "model",width : 190},
            {name : "outerDiameter",index : "outerDiameter",width : 60},
            {name : "storagetype",index : "storagetype",width : 60,
                formatter:function(cellValue,option,rowObject){
                	if(cellValue==0){
                        return "正常生产";
                    }else if(cellValue==1){
                        return "销售退货";
                    }else if(cellValue==2){
                        return "外协采购";
                    }
                }
            }
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : "#rewind_list_grid-pager",
        sortname : "a.id",
        sortorder : "desc",
        altRows: true,
        viewrecords : true,
        hidegrid:false,
        autowidth:false,
        multiselect:true,
        multiboxonly: true,
        shrinkToFit:false,
        autoScroll: true,
        loadComplete : function(data) {
            var table = this;
            setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            $("#rewind_list_grid-table").closest(".ui-jqgrid-bdiv").css({ 'overflow-x': 'auto' });
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
    });

    jQuery("#rewind_list_grid-table").navGrid("#rewind_list_grid-pager",{
        edit:false,
        add:false,
        del:false,
        search:false,
        refresh:false}).navButtonAdd("#rewind_list_grid-pager",{
        caption:"",
        buttonicon:"fa fa-refresh green",
        onClickButton: function(){
            $("#rewind_list_grid-table").jqGrid("setGridParam", {
                postData: {queryJsonString:""} //发送数据
            }).trigger("reloadGrid");
        }
    }).navButtonAdd("#rewind_list_grid-pager",{
        caption: "",
        buttonicon:"fa icon-cogs",
        onClickButton : function (){
            jQuery("#rewind_list_grid-table").jqGrid("columnChooser",{
                done: function(perm, cols){
                    $("#rewind_list_grid-table").jqGrid("setGridWidth", $("#rewind_list_grid-div").width());
                    rewind_list_exportExcelIndex = perm;
                }
            });
        }
    });

    $(window).on("resize.jqGrid", function () {
        $("#rewind_list_grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);
        $("#rewind_list_grid-table").jqGrid("setGridHeight", $(".container-fluid").height()-10- $("#rewind_list_yy").outerHeight(true)-
        $("#rewind_list_fixed_tool_div").outerHeight(true)- $("#rewind_list_grid-pager").outerHeight(true)-
        $("#gview_rewind_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
    });
    $(window).triggerHandler("resize.jqGrid");


    /**导出Excel*/
    function rewind_list_exportExecl(){
        $("#rewind_list_hiddenForm #rewind_list_ids").val(jQuery("#rewind_list_grid-table").jqGrid("getGridParam", "selarrrow"));
        $("#rewind_list_hiddenForm #rewind_list_queryEntityNo").val($("#rewind_list_queryForm #rewind_list_entityNo").val());
        $("#rewind_list_hiddenForm #rewind_list_queryStartTime").val($("#rewind_list_queryForm #rewind_list_startTime").val());
        $("#rewind_list_hiddenForm #rewind_list_queryEndTime").val($("#rewind_list_queryForm #rewind_list_endTime").val());
        $("#rewind_list_hiddenForm #rewind_list_queryQaCode").val($("#rewind_list_queryForm #rewind_list_qaCode").val());

        $("#rewind_list_hiddenForm #rewind_list_cq2").val($("#rewind_list_queryForm #rewind_list_factory").val());
        $("#rewind_list_hiddenForm #rewind_list_wlbh2").val($("#rewind_list_queryForm #rewind_list_wlbh").val());
        $("#rewind_list_hiddenForm #rewind_list_ddh2").val($("#rewind_list_queryForm #rewind_list_ddh").val());
        $("#rewind_list_hiddenForm #rewind_list_batchno2").val($("#rewind_list_queryForm #rewind_list_batchno").val());
        $("#rewind_list_hiddenForm #rewind_list_queryExportExcelIndex").val(rewind_list_exportExcelIndex);
        $("#rewind_list_hiddenForm #rewind_list_rewindname").val($("#rewind_list_queryForm #rewind_list_rewind").val());
        $("#rewind_list_hiddenForm").submit();
    }

    /**
     * 查询按钮点击事件
     */
    function rewind_list_queryOk(){
        var queryParam = iTsai.form.serialize($("#rewind_list_queryForm"));
        //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
        rewind_list_queryByParam(queryParam);
    }

    function rewind_list_queryByParam(jsonParam) {
        iTsai.form.deserialize($("#rewind_list_hiddenQueryForm"), jsonParam);
        var queryParam = iTsai.form.serialize($("#rewind_list_hiddenQueryForm"));
        var queryJsonString = JSON.stringify(queryParam);
        $("#rewind_list_grid-table").jqGrid("setGridParam", {
            postData: {queryJsonString: queryJsonString}
        }).trigger("reloadGrid");
    }

    /**重置*/
    function rewind_list_reset(){
        $("#rewind_list_queryForm #rewind_list_factory").select2("val","");
        iTsai.form.deserialize($("#rewind_list_queryForm"),rewind_list_queryForm_Data);
        rewind_list_queryByParam(rewind_list_queryForm_Data);
    }

    /**厂区选择*/
    $("#rewind_list_queryForm #rewind_list_factory").select2({
        placeholder: "选择厂区",
        minimumInputLength:0,   //至少输入n个字符，才去加载数据
        allowClear: true,  //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches:"没有结果",
        formatSearching:"搜索中...",
        formatAjaxError:"加载出错啦！",
        ajax : {
            url: context_path+"/factoryArea/getFactoryList",
            type:"POST",
            dataType : 'json',
            delay : 250,
            data: function (term,pageNo) {     //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term,    //联动查询的字符
                    pageSize: 15,    //一次性加载的数据条数
                    pageNo:pageNo    //页码
                }
            },
            results: function (data,pageNo) {
                var res = data.result;
                if(res.length>0){   //如果没有查询到数据，将会返回空串
                    var more = (pageNo*15)<data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results:res,more:more
                    };
                }else{
                    return {
                        results:{}
                    };
                }
            },
            cache : true
        }
    });
</script>