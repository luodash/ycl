<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%--
<style type="text/css">

    /*设置 layui 表格中单元格内容溢出可见样式*/
    #divtable .layui-table-view,
    #divtable .layui-table-box,
    #divtable .layui-table-body {
        overflow: visible;
    }

    #divtable .layui-table-cell {
        height: auto;
        overflow: visible;
    }

    /*文本对齐方式*/
    .text-center {
        text-align: center;
    }

    /* 使得下拉框与单元格刚好合适 */
    td .layui-form-select {
        margin-top: -5px;
        margin-left: -15px;
        margin-right: -15px;
    }

    #divtable .layui-anim-upbit {
        margin-top: -10px;
    }
</style>
--%>


<div id="yclOutsourcingOrderPage" style="margin: 15px">
    <%--query tools--%>
    <blockquote class="layui-elem-quote">
        <div class="layui-fluid">
            <div class="layui-row">
                <div class="layui-col-sm3">
                    <div class="layui-inline">
                        <label style="width: 80px">订单号</label>
                        <div class="layui-input-inline">
                            <input type="text" class="layui-input" v-model="queryParams.materialCode"/>
                        </div>
                    </div>
                </div>
                <div class="layui-col-sm3">
                    <div class="layui-inline">
                        <label style="width: 80px">供应商</label>
                        <div class="layui-input-inline">
                            <input type="text" class="layui-input" v-model="queryParams.purchaseUser"/>
                        </div>
                    </div>
                </div>
                <div class="layui-col-sm3">
                    <div class="layui-inline">
                        <label style="width: 80px">物料编码</label>
                        <div class="layui-input-inline">
                            <input type="text" class="layui-input" id="orderDate">
                        </div>
                    </div>
                </div>
                <div class="layui-col-sm3">
                    <div class="layui-inline">
                        <button type="button" class="layui-btn layui-btn-sm layui-btn-normal" @click="queryPageView">
                            <i class="layui-icon">&#xe615;</i>查询
                        </button>
                    </div>
                    <div class="layui-inline">
                        <button type="button" class="layui-btn layui-btn-primary layui-btn-sm" @click="restQueryParams">
                            <i class="layui-icon">&#xe669;</i>重置
                        </button>
                    </div>
                </div>
            </div>
            <div class="layui-row">
                <div class="layui-col-sm3">
                    <div class="layui-inline">
                        <label style="width: 80px">收货方</label>
                        <div class="layui-input-inline">
                            <input type="text" class="layui-input" v-model="queryParams.poNum"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </blockquote>

    <div id="toolbarOSOrder">
        <label class="layui-word-aux">订单列表</label>
        <%--        <button type="button" @click="addDeliveryOrder" class="layui-btn layui-btn-sm layui-btn-normal">--%>
        <%--            <i class="layui-icon">&#xe654;</i>创建送货单--%>
        <%--        </button>--%>
    </div>
    <table id="tableGridOutsourcing" lay-filter="tableGridFilterOutsourcing"></table>

    <div id="toolbarOSOrderSend">
        <label class="layui-word-aux">接收单列表</label>
        <button type="button" @click="orderJS" class="layui-btn layui-btn-sm layui-btn-normal">
            <i class="layui-icon">&#xe624;</i>订单接受
        </button>
        <button type="button" @click="receiveWorkOrderSave" class="layui-btn layui-btn-sm layui-btn-normal">
            <i class="layui-icon">&#xe605;</i>保存
        </button>
        <button type="button" @click="receiveWorkOrderCommit" class="layui-btn layui-btn-sm layui-btn-normal">
            <i class="layui-icon">&#x1005;</i>提交
        </button>
        <button type="button" @click="getReceiveOrderState" class="layui-btn layui-btn-sm layui-btn-normal">
            <i class="layui-icon">&#x1007;</i>取消收货
        </button>
        <%--     <button type="button" class="layui-btn layui-btn-sm layui-btn-normal">
                 <i class="layui-icon">&#xe66d;</i>打印完工物料码
             </button>--%>

    </div>
    <div id="divtable">
        <table id="tableGridOutsourcingSend" lay-filter="tableGridFilterOutsourcingSend"></table>
    </div>

    <div id="toolbarOSOrderSend">
        <label class="layui-word-aux">工单列表</label>
        <button type="button" @click="addOrder" class="layui-btn layui-btn-sm layui-btn-normal" lay-event="addIn">
            <i class="layui-icon">&#x1005;</i>增行
        </button>
        <button type="button" @click="saveOrder" class="layui-btn layui-btn-sm layui-btn-normal" lay-event="addIn">
            <i class="layui-icon">&#xe605;</i>工单保存
        </button>
        <button type="button" @click="commitOrder" class="layui-btn layui-btn-sm layui-btn-normal" lay-event="addIn">
            <i class="layui-icon">&#x1005;</i>工单提交
        </button>
        <%--   <button type="button" @click="delOrder" class="layui-btn layui-btn-sm layui-btn-danger">
               <i class="layui-icon">&#xe640;</i>删除
           </button>--%>
        <button type="button" @click="updateUp" class="layui-btn layui-btn-sm layui-btn-normal" lay-event="save">
            <i class="layui-icon">&#xe605;</i>更新上架
        </button>
        <button type="button" @click="reBack" class="layui-btn layui-btn-sm layui-btn-normal" lay-event="delete">
            <i class="layui-icon">&#xe666;</i>完工退回
        </button>
        <%--<div class="layui-inline">--%>
        <%--<label style="width: 65px">厂区</label>--%>
        <%--<div class="layui-input-inline">--%>
        <%--<select id="ftInfo" lay-filter="ftInfoFilter">--%>
        <%--</select>--%>
        <%--</div>--%>
        <%--</div>--%>
        <div class="layui-inline" style="margin-left: 10px;">
            <label style="width: 65px">物料编码：</label>
            <div class="layui-form layui-input-inline">
                <%--<input type="text" id="materialCodeSelect" autocomplete="off">--%>
                <input type="text" id="materialCodeSelect" name="materialCode"
                       style="width:350px;margin-right:10px;"/>
            </div>
        </div>
    </div>
    <table id="tableGridOutsourcingInFlow" lay-filter="tableGridOutsourcingInFlow"></table>
</div>


<script>

    const context_path = '${APP_PATH}';
    const table = layui.table,
        layer = layui.layer,
        form = layui.form,
        laydate = layui.laydate;

    let vm = new Vue({
        el: '#yclOutsourcingOrderPage',
        data: {
            queryParams: {
                poCode: "",
                supplierName: "",
                materialCode: "",
                purchaseUser: "",
                orderTimeStart: "",
                orderTimeEnd: "",
                poNum: ""
            },
            // 送退货
            businessCode: "",
            materialCode: "",
            lotsNum: "",
            oneData: {},
            clickCount: 0,
            locatorList: [],
            tbData: [],
            ftItemNum: "",
            ftItemId: "",
            ftItemDesc: ""

        },
        methods: {
            initTableIns() {
                // 订单物料
                table.render({
                    elem: '#tableGridOutsourcing'
                    , url: context_path + '/ycl/purchaseIn/PurchaseOrder/getPoListForOutSourcing'
                    , page: true
                    , id: "tableGridSetOutsourcing"
                    , size: 'sm'
                    , cols: [[
                        {type: 'radio'}
                        , {field: 'ouName', title: '业务实体', width: 150}
                        , {field: 'organizationName', title: '库存组织', width: 200}
                        , {field: 'supplierName', title: '供应商', width: 160}
                        // , {field: '', title: '供应商地点', width: 160}
                        , {field: 'materialName', title: '物料类型', width: 90}
                        , {field: '', title: '委外加工费', width: 100}
                        , {field: 'materialName', title: '委外加工物', width: 100}
                        , {field: 'wxItemNum', title: '完工物料编码', width: 110}
                        , {field: 'wxItemDesc', title: '完工物料名', width: 160}
                        , {field: 'primaryUnit', title: '单位', width: 70}
                        , {field: 'quantity', title: '订单数量', width: 145}
                        , {field: '', title: '加工费单值', width: 145}
                        , {field: 'purchaseUser', title: '采购员', width: 70}
                        , {field: '', title: '需求日期', width: 145}
                        , {field: '', title: '合同号', width: 145}
                        , {field: 'poId', title: '订单号', width: 70}
                        , {field: 'poLineId', title: '订单行号', width: 80}
                        , {field: 'rcvQuantity', title: '接收数量', width: 80}
                        , {field: 'purchaseDept', title: '收货方', width: 130}
                    ]]
                });
            },
            initTableIns2() {
                table.render({
                    elem: '#tableGridOutsourcingSend'
                    , id: "tableGridSetOutsourcingSend"
                    , page: true
                    , size: 'sm'
                    , data: []
                    , cols: [[
                        // {
                        //     field: 'state', title: '单号类型', width: 80, templet: function (d) {
                        //         if (d.asnType == 1) {
                        //             return '<span>送货单</span>'
                        //         } else if (d.asnType == 3) {
                        //             return '<span>退货单</span>'
                        //         }
                        //     }
                        // }
                        {type: 'radio'}
                        , {field: 'tempId', hide: true}
                        // , {field: 'orgnizationName', title: '库存组织', width: 200, hide: true}
                        // , {field: 'wxItemNum', title: '物料编码', width: 100, hide: true}
                        // , {field: 'materialName', title: '物料名称', width: 95, hide: true}
                        // , {field: 'vendorName', title: '供应商批次号', width: 120, edit: 'text'}
                        // // , {field: 'vendorName', title: '远东批次号', width: 200}
                        // , {field: 'rcvQuantity', title: '接收数量', width: 80, edit: 'text'}
                        // , {field: 'storeCode', title: '退货数量', width: 80, edit: 'text'}
                        // , {field: 'orderTime', title: '接收时间', width: 110}
                        // , {field: 'subinventory', title: '费用接收子库存', width: 115}
                        // , {field: 'subinventoryDesc', title: '费用接收子库描述', width: 130}
                        // , {field: 'locatorId', title: '费用接收货位', width: 120, templet: (d) => vm.renderSelectOptions(d)}
                        // , {field: 'locatorDesc', title: '费用接收货位描述', width: 130}
                        // // , {field: 'wxItemNum', title: '完工物料编码', width: 110}
                        // , {field: '', title: '接收单号', width: 80, hide: true}
                        // , {field: '', title: '接收行号', width: 80, hide: true}
                        // , {field: 'completedQuantity', title: '工单完工数', width: 90}
                        // , {field: 'ftcompletedQuantity', title: '消耗物数', width: 80}
                        // , {field: '', title: '加工费发货', width: 90}
                        // // , {field: 'expressNumber', title: '错误信息', width: 80}
                        // , {field: 'creationDate', title: '工单号', width: 75}
                        , {field: 'poId', title: '订单编号-外键ID', width: 120, hide: true}
                        , {field: 'rcvId', title: '接收单号', width: 65, hide: true}
                        , {field: 'organizationId', title: '库存组织ID', width: 120, hide: true}
                        , {field: 'organizationCode', title: '库存组织编码', width: 120, hide: true}
                        , {field: 'organizationName', title: '库存组织', width: 100, hide: true}
                        , {field: 'materialId', title: '委外加工费物料ID', width: 120, hide: true}
                        , {field: 'materialCode', title: '委外加工费物料编码', width: 140}
                        , {field: 'materialName', title: '委外加工费物料名称', width: 140}
                        , {field: 'subinventory', title: '费用接收子库存', width: 125}
                        , {field: 'subinventoryDesc', title: '费用接收子库存描述', width: 140}
                        , {field: 'locatorId', title: '费用接收货位ID', width: 120, hide: true}
                        // , {field: 'locatorCode', title: '费用接收货位', width: 110, templet: (d) => vm.renderSelectOptions(d)}
                        , {field: 'locatorCode', title: '费用接收货位', width: 110}
                        , {field: 'locatorDesc', title: '费用接收货位描述', width: 130}
                        , {field: 'wxItemId', title: '完工物料ID', width: 120, hide: true}
                        , {field: 'wxItemNum', title: '完工物料编码', width: 120, hide: true}
                        , {field: 'wxItemDesc', title: '完工物料名称', width: 120, hide: true}
                        , {field: 'orderTime', title: '订单创建时间', width: 120, hide: true}
                        , {field: 'wipEntityId', title: '工单ID', width: 120, hide: true}
                        , {field: 'wipEntityName', title: '工单名称', width: 120, hide: true}
                        , {field: 'rcvQuantity', title: '接收数量', width: 80, edit: 'text'}
                        , {field: 'completedQuantity', title: '完工数量', width: 80}
                        , {field: 'fycompletedQuantity', title: '加工费发料数量', width: 120}
                        , {field: 'ftcompletedQuantity', title: '消耗物料发料数量', width: 130}
                        , {field: 'receiptDate', title: '收据日期', width: 140}
                    ]]
                });
            },
            initTableIns3() {
                table.render({
                    elem: '#tableGridOutsourcingInFlow'
                    , id: "tableGridSetOutsourcingInFlow"
                    , data: []
                    , size: 'sm'
                    , cols: [[
                        {type: 'radio'}
                        , {field: 'tempId', hide: true}
                        // , {field: 'rcvId', title: '接收单号-主键ID', width: 120, hide: true}
                        // , {field: 'organizationId', title: '库存组织ID', width: 120, hide: true}
                        // , {field: 'organizationCode', title: '库存组织编码', width: 120, hide: true}
                        // , {field: 'organizationName', title: '库存组织', width: 120}
                        // , {field: 'organizationName', title: '完工物编码', width: 120}
                        // , {field: 'organizationName', title: '完工物名称', width: 120}
                        // , {field: 'organizationName', title: '完工子库存', width: 120}
                        // , {field: 'completedSub', title: '完工子库存描述', width: 120}
                        // , {field: 'completedLocatorId', title: '完工货位', width: 120}
                        // , {field: 'organizationName', title: '完工货位描述', width: 120}
                        // , {field: 'vendorLot', title: '供应商批次', width: 90, edit: 'text'}
                        // , {field: 'completedQuantity', title: '完工数量', width: 80, edit: 'text'}
                        // , {field: 'completedQuantity', title: '完工时间', width: 80, edit: 'text'}
                        // , {field: 'ftComponentSubinventory', title: '发料子库存', width: 120}
                        // , {field: 'ftSubinventoryDesc', title: '发料子库存描述', width: 128}
                        // , {field: 'ftComponentLocatorId', title: '发料货位ID', width: 120, hide: true}
                        // , {field: 'ftLocatorCode', title: '发料货位编码', width: 105}
                        // , {field: 'ftLocatorDesc', title: '发料货位描述', width: 120}
                        // , {field: 'ftItemNum', title: '消耗物物料编码', width: 85, edit: 'text'}
                        // , {field: 'vendorFtLot', title: '消耗物料供应商批次', width: 120, edit: 'text'}
                        // , {field: 'ftItemId', title: '消耗物料ID', width: 70, edit: 'text'}
                        // , {field: 'ftItemDesc', title: '消耗物料名称', width: 115, edit: 'text'}
                        // , {field: 'ftcompletedQuantity', title: '发料数量', width: 110, edit: 'text'}
                        // , {field: 'fyItemId', title: '加工费物料ID', width: 110, edit: 'text', hide: true}
                        // , {field: 'fyItemId', title: '加工费物料编码', width: 110, edit: 'text'}
                        // , {field: 'fyItemId', title: '委外加工费物料名称', width: 110, edit: 'text'}
                        // , {field: 'fyCompletedQuantity', title: '加工费发料数量', width: 115, edit: 'text'}
                        // , {field: 'fySub', title: '加工费发料子库存', width: 115}
                        // , {field: 'fySub', title: '加工费发料子库存描述', width: 115}
                        // , {field: 'fyLocatorId', title: '加工费发料货位ID', width: 140}
                        // , {field: 'fyLocatorCode', title: '加工费发料货位编码', width: 140}
                        // , {field: 'fyLocatorDesc', title: '加工费发料货位', width: 127}
                        // , {field: 'processFlag', title: '状态', width: 80, hide: true}
                        , {field: 'rcvId', title: '接收单号', width: 120, hide:true}
                        , {field: 'poId', title: '订单号', width: 120, hide:true}
                        , {field: 'wipComId', title: '工单号', width: 120, hide:true}
                        , {field: 'organizationId', title: '库存组织ID', width: 120, hide:true}
                        , {field: 'organizationCode', title: '库存组织编码', width: 120, hide:true}
                        , {field: 'organizationName', title: '库存组织', width: 120}
                        , {field: 'completeItemId', title: '完工物料id', width: 120, hide:true}
                        , {field: 'completeItemNum', title: '完工物料编码', width: 120}
                        , {field: 'completeItemDesc', title: '完工物料名称', width: 120}
                        , {field: 'completeSub', title: '完工子库存', width: 120, edit: 'text'}
                        , {field: 'completeSubDesc', title: '完工子库存描述', width: 120, edit: 'text'}
                        , {field: 'completeLocatorId', title: '完工货位Id', width: 120, edit: 'text'}
                        , {field: 'completeLocatorCode', title: '完工货位', width: 120, edit: 'text'}
                        // , {field: 'completeLocatorDesc', title: '完工货位描述', width: 120, edit: 'text'}
                        , {field: 'vendorLot', title: '供应商批次', width: 120, edit: 'text', edit: 'text'}
                        , {field: 'completeQty', title: '完工数量', width: 120, edit: 'text'}
                        , {field: 'transactionDate', title: '完工时间', width: 120}
                        , {field: 'ftComponentSub', title: '发料子库存', width: 120}
                        , {field: 'ftSubinventoryDesc', title: '发料子库存描述', width: 120}
                        , {field: 'ftComponentLocatorId', title: '发料货位', width: 120}
                        , {field: 'ftLocatorCode', title: '发料货位编码', width: 120, hide:true}
                        , {field: 'ftLocatorDesc', title: '发料货位描述', width: 120}
                        , {field: 'ftItemNum', title: '消耗物料编码', width: 120, edit: 'text'}
                        , {field: 'vendorFtLot', title: '消耗物料供应商批次', width: 120, edit: 'text'}
                        , {field: 'ftItemDesc', title: '消耗物料名称', width: 120, edit: 'text'}
                        , {field: 'ftcompletedQty', title: '发料数量', width: 120, edit: 'text'}
                        , {field: 'fyItemId', title: '加工费物料ID', width: 120, hide:true}
                        , {field: 'fyItemNum', title: '加工费物料编码', width: 120}
                        , {field: 'fyItemDesc', title: '委外加工费物料名称', width: 120}
                        , {field: 'fyComponentQty', title: '加工费发料数量', width: 120, edit: 'text'}
                        , {field: 'fySub', title: '加工费发料子库存', width: 120}
                        , {field: 'fySubDesc', title: '加工费发料子库存描述', width: 120}
                        , {field: 'fyComponentLocatorId', title: '加工费发料货位ID', width: 120}
                        , {field: 'fyLocatorCode', title: '加工费发料货位编码', width: 120, hide:true}
                        , {field: 'fyLocatorDesc', title: '加工费发料货位', width: 120}
                        , {field: 'attribute1', title: '操作状态', width: 120, hide:true}
                        , {field: 'processFlag', title: '状态', width: 120}
                    ]]
                });
            },
            queryPageView() {
                console.log(vm.queryParams)
                table.reload('tableGridSetOutsourcing', {
                    where: vm.queryParams
                    , page: {
                        curr: 1
                    }
                });
            },
            restQueryParams() {
                vm.queryParams = {
                    poCode: "",
                    supplierName: "",
                    materialCode: "",
                    purchaseUser: "",
                    orderDate: ""
                }
                table.reload('tableGridSetOutsourcing', {
                    where: vm.queryParams
                });
            },
            // 2新增接收单
            orderJS() {
                if (vm.clickCount === 1) {
                    layer.msg('请完成当前行', {icon: 2});
                    return
                }
                let oldData = table.cache['tableGridSetOutsourcingSend'];


                console.log(vm.oneData.organizationId)
                // 获取 费用接收子库存描述
                $.get(context_path + '/ycl/workorder/WorkOrder/getDescriptionByOrgId', {
                    orgId: vm.oneData.organizationId,
                }, function (res) {
                    console.log(res[0].DESCRIPTION)

                    // 货位描述
                    $.get(context_path + '/ycl/workorder/WorkOrder/getLocatorListByDesc', {
                        description: res[0].DESCRIPTION,
                        description2: vm.oneData.supplierName
                    }, function (res) {
                        console.log(res)
                        if (res.length > 0) {
                            // vm.$nextTick(() => {
                            vm.oneData.locatorId = res[0].INVENTORY_LOCATION_ID
                            vm.oneData.locatorCode = res[0].SEGMENT1
                            vm.oneData.locatorDesc = res[0].DESCRIPTION2
                            vm.oneData.subinventoryDesc = res[0].DESCRIPTION
                            vm.oneData.rcvQuantity = ""
                            // })
                        }
                        oldData.push(vm.oneData);
                        vm.clickCount += 1
                        table.reload('tableGridSetOutsourcingSend', {
                            data: oldData
                        })
                        // oldData[oldData.length - 1].LAY_CHECKED = false;
                    })

                })


                // $.get(context_path + '/ycl/workorder/WorkOrder/getDescriptionByOrgId', {
                //     orgId: vm.oneData.orgnizationId,
                // }, function (res) {
                //
                //     vm.oneData.subinventoryDesc = res[0].DESCRIPTION
                //     vm.oneData.tempId = new Date().valueOf()
                //     oldData.push(vm.oneData);
                //
                //     vm.$nextTick(() => {
                //         $.get(context_path + '/ycl/workorder/WorkOrder/getLocatorListByDesc', {
                //             description: res[0].DESCRIPTION
                //         }, function (res) {
                //             vm.locatorList = res
                //
                //             vm.clickCount += 1
                //             table.reload('tableGridSetOutsourcingSend', {
                //                 data: oldData
                //             })
                //         })
                //     })
                // })
            },
            // 2保存
            receiveWorkOrderSave() {

                const checkStatusData = table.checkStatus('tableGridSetOutsourcingSend').data

                if (checkStatusData.length === 0) {
                    layer.msg('请选择一条数据', {icon: 2});
                    return
                }
                let subData = checkStatusData[0];

                if (typeof subData.index != 'undefined') {
                    layer.msg('当前选择行已处理，不能继续操作');
                    return
                }

                if (subData.rcvQuantity == null || subData.rcvQuantity == '') {
                    layer.msg('接收数量不能为空', {icon: 2});
                    return
                }

                subData.type = 'save'
                subData.locatorId = vm.oneData.locatorId
                console.log(subData)

                layer.load(2, {time: 3 * 1000});

                $.ajax({
                    type: 'POST',
                    url: context_path + "/ycl/workorder/WorkOrder/receiveWorkOrder",
                    contentType: "application/json",
                    async: false,
                    dataType: "json",
                    data: JSON.stringify(subData),
                    success: function (res) {
                        if (res.retCode === 'success') {
                            layer.msg(res.retMess, {icon: 1});
                        } else {
                            layer.msg(res.retMess, {icon: 2});
                        }
                        return;
                    }
                })

            },
            // 2提交
            receiveWorkOrderCommit() {

                const checkStatusData = table.checkStatus('tableGridSetOutsourcingSend').data

                if (checkStatusData.length === 0) {
                    layer.msg('请选择一条数据', {icon: 2});
                    return
                }
                let subData = checkStatusData[0];

                if (typeof subData.index != 'undefined') {
                    layer.msg('当前选择行已处理，不能继续操作');
                    return
                }

                if (subData.rcvQuantity == null || subData.rcvQuantity == '') {
                    layer.msg('接收数量不能为空', {icon: 2});
                    return
                }

                subData.type = 'commit'
                subData.locatorId = vm.oneData.locatorId
                console.log(subData)

                layer.load(2, {time: 3 * 1000});

                $.ajax({
                    type: 'POST',
                    url: context_path + "/ycl/workorder/WorkOrder/receiveWorkOrder",
                    contentType: "application/json",
                    async: false,
                    dataType: "json",
                    data: JSON.stringify(subData),
                    success: function (res) {
                        if (res.retCode === 'success') {
                            layer.msg(res.retMess, {icon: 1});
                        } else {
                            layer.msg(res.retMess, {icon: 2});
                        }
                        return;
                    }
                })
            },
            // 2取消收货
            getReceiveOrderState() {

                const checkStatusData = table.checkStatus('tableGridSetOutsourcingSend').data

                console.log(checkStatusData)

                if (typeof checkStatusData[0].index == 'undefined') {
                    layer.msg('请选择有效数据', {icon: 2});
                    return
                }

                layer.load(2, {time: 3 * 1000});

                $.get(context_path + '/ycl/workorder/WorkOrder/returnProcess', {
                    rcvId: checkStatusData[0].rcvId,
                }, function (res) {
                    console.log(res)
                    if (res.retCode == 'error') {
                        layer.msg(res.retMess, {icon: 2});
                    } else {
                        layer.msg(res.retMess, {icon: 1});
                    }
                    return;
                })
            },
            renderSelectOptions(e) {
                let html = [];
                html.push('<div class="layui-form" lay-filter="shelfCode">');
                html.push('<select name="shelfCode" id="shelfCode" data-value="' + e.tempId + '" lay-filter="shelfCode" lay-search="">');
                html.push('<option value="">请选择</option>');
                this.locatorList.forEach(m => {
                    html.push('<option value="');
                    html.push(m.INVENTORY_LOCATION_ID);
                    html.push('"');
                    if (m.INVENTORY_LOCATION_ID == e.locatorId) {
                        html.push(' selected="selected"');
                    }
                    html.push('>');
                    html.push(m.SEGMENT1);
                    html.push('</option>');
                })
                html.push('</select></div>');
                return html.join('');
            },

            //3新增工单
            addOrder() {
                // 第二栏数据
                const checkStatusData = table.checkStatus('tableGridSetOutsourcingSend').data

                if (checkStatusData.length === 0) {
                    layer.msg('请选择一条数据', {icon: 2});
                    return
                }

                let checkData = checkStatusData[0]

                if (typeof checkData.index == 'undefined') {
                    layer.msg('当前选中的接收单未处理，不能继续操作');
                    return
                }

                if (parseFloat(checkData.rcvQuantity) < parseFloat(checkData.completedQuantity)) {
                    layer.msg('请选择接收单中接受数量大于完工数量的记录');
                    return
                }
                    let newData = {};
                    newData.organizationId = checkData.organizationId;
                    newData.organizationCode = checkData.organizationCode;
                    newData.organizationName = checkData.organizationName;
                    newData.poId = checkData.poId;
                    newData.rcvId = checkData.rcvId;
                    newData.completeItemId = checkData.wxItemId;
                    newData.completeItemNum = checkData.wxItemNum;
                    newData.completeItemDesc = checkData.wxItemDesc;
                    newData.ftComponentSub = checkData.subinventory;
                    newData.ftSubinventoryDesc = checkData.subinventoryDesc;
                    newData.ftComponentLocatorId = checkData.locatorId;
                    newData.ftLocatorCode = checkData.locatorCode;
                    newData.ftLocatorDesc = checkData.locatorDesc;
                    newData.fyItemNum = checkData.materialCode;
                    newData.fyItemId = checkData.materialId;
                    newData.fyItemDesc = checkData.materialName;
                    newData.fySub = checkData.subinventory;
                    newData.fySubDesc = checkData.subinventoryDesc;
                    newData.fyComponentLocatorId = checkData.locatorId;
                    newData.fyLocatorCode = checkData.locatorCode;
                    newData.fyLocatorDesc = checkData.locatorDesc;
                    let jsonStr = JSON.stringify(newData);
                layer.open({
                    title: '新增'
                    , type: 2
                    , area: ['1200px', '600px'] //宽高
                    , maxmin: true
                    , content: context_path + '/ycl/workorder/WorkOrder/addWorkorder'   //?jsonStr='+jsonStr
                });

            },
            // addOrder() {
            //     // 第二栏数据
            //     const checkStatusData = table.checkStatus('tableGridSetOutsourcingSend').data
            //
            //     if (checkStatusData.length === 0) {
            //         layer.msg('请选择一条数据', {icon: 2});
            //         return
            //     }
            //
            //     let checkData = checkStatusData[0]
            //
            //     if (typeof checkData.index == 'undefined') {
            //         layer.msg('当前选中的接收单未处理，不能继续操作');
            //         return
            //     }
            //
            //     if (parseFloat(checkData.rcvQuantity) < parseFloat(checkData.completedQuantity)) {
            //         layer.msg('请选择接收单中接受数量大于完工数量的记录');
            //         return
            //     }
            //
            //     // 第二栏数据
            //     let oldData = table.cache['tableGridSetOutsourcingInFlow'];
            //     console.log(checkData);
            //
            //     let newData = {};
            //     newData.organizationId = checkData.organizationId;
            //     newData.organizationCode = checkData.organizationCode;
            //     newData.organizationName = checkData.organizationName;
            //     newData.poId = checkData.poId;
            //     newData.rcvId = checkData.rcvId;
            //     newData.completeItemId = checkData.wxItemId;
            //     newData.completeItemNum = checkData.wxItemNum;
            //     newData.completeItemDesc = checkData.wxItemDesc;
            //     newData.ftComponentSub = checkData.subinventory;
            //     newData.ftSubinventoryDesc = checkData.subinventoryDesc;
            //     newData.ftComponentLocatorId = checkData.locatorId;
            //     newData.ftLocatorCode = checkData.locatorCode;
            //     newData.ftLocatorDesc = checkData.locatorDesc;
            //     newData.fyItemNum = checkData.materialCode;
            //     newData.fyItemId = checkData.materialId;
            //     newData.fyItemDesc = checkData.materialName;
            //     newData.fySub = checkData.subinventory;
            //     newData.fySubDesc = checkData.subinventoryDesc;
            //     newData.fyComponentLocatorId = checkData.locatorId;
            //     newData.fyLocatorCode = checkData.locatorCode;
            //     newData.fyLocatorDesc = checkData.locatorDesc;
            //
            //     oldData.push(newData);
            //     table.reload('tableGridSetOutsourcingInFlow', {
            //         data: oldData
            //     })
            // },
            // 3 保存
            saveOrder() {

                const checkStatusData = table.checkStatus('tableGridSetOutsourcingInFlow').data

                if (checkStatusData.length === 0) {
                    layer.msg('请选择一条数据', {icon: 2});
                    return
                }

                // let subData = checkStatusData[0];
                // subData.type = 'save'
                // subData.ftSubCode = subData.ftComponentSubinventory

                for (let i = 0; i < checkStatusData.length; i++) {
                    let item = checkStatusData[i]
                    item.type = 'save'
                    item.ftSubCode = item.ftComponentSubinventory
                    item.completedQuantity = parseFloat(item.completedQuantity)
                    item.ftcompletedQuantity = parseFloat(item.ftcompletedQuantity)
                    item.fyCompletedQuantity = parseFloat(item.fyCompletedQuantity)
                    item.completedQuantity = parseFloat(item.completedQuantity)
                }

                console.log(checkStatusData)

                $.ajax({
                    type: 'POST',
                    url: context_path + "/ycl/workorder/WorkOrder/createComplete",
                    contentType: "application/json",
                    async: false,
                    dataType: "json",
                    data: JSON.stringify(checkStatusData),
                    success: function (res) {
                        if (res.retCode === 'S') {
                            layer.msg(res.retMess, {icon: 1});
                        } else {
                            layer.msg(res.retMess, {icon: 2});
                        }
                        return;
                    }
                })

            },
            // 3 提交
            commitOrder() {
                const checkStatusData = table.checkStatus('tableGridSetOutsourcingInFlow').data

                if (checkStatusData.length === 0) {
                    layer.msg('请选择一条数据', {icon: 2});
                    return
                }

                // let subData = checkStatusData[0];
                // checkStatusData[0].type = 'commit'
                // // subData.type = 'commit'
                // console.log(subData)

                for (let i = 0; i < checkStatusData.length; i++) {
                    let item = checkStatusData[i]
                    item.type = 'commit'
                    item.ftSubCode = item.ftComponentSubinventory
                    item.completedQuantity = parseFloat(item.completedQuantity)
                    item.ftcompletedQuantity = parseFloat(item.ftcompletedQuantity)
                    item.fyCompletedQuantity = parseFloat(item.fyCompletedQuantity)
                    item.completedQuantity = parseFloat(item.completedQuantity)
                }

                $.ajax({
                    type: 'POST',
                    url: context_path + "/ycl/workorder/WorkOrder/createComplete",
                    contentType: "application/json",
                    async: false,
                    dataType: "json",
                    data: JSON.stringify(checkStatusData),
                    success: function (res) {
                        if (res.retCode === 'S') {
                            layer.msg(res.retMess, {icon: 1});
                        } else {
                            layer.msg(res.retMess, {icon: 2});
                        }
                        return;
                    }
                })

            },
            // 3 删除
            delOrder() {

                const checkStatusData = table.checkStatus('tableGridSetOutsourcingInFlow').data

                if (checkStatusData.length === 0) {
                    layer.msg('请选择一条数据', {icon: 2});
                    return
                }

            },
            // 3 更新上架
            updateUp() {

                const checkStatusData = table.checkStatus('tableGridSetOutsourcingInFlow').data

                if (checkStatusData.length === 0) {
                    layer.msg('请选择一条数据', {icon: 2});
                    return
                }

                for (let i = 0; i < checkStatusData.length; i++) {
                    let item = checkStatusData[i]

                    item.updownQuantity = parseFloat(item.completedQuantity)
                    item.areaCode = item.organizationId
                    item.areaName = item.organizationName
                    item.storeCode = 'V001'
                    item.storeName = item.subinventoryDesc
                    item.locatorCode = item.locatorCode
                    item.materialId = item.materialId
                    item.materialCode = item.materialCode
                    item.materialName = item.materialName
                }


                $.ajax({
                    type: 'POST',
                    url: context_path + "/ycl/ordermanage/asn/editAsnLineBatch",
                    contentType: "application/json",
                    async: false,
                    dataType: "json",
                    data: JSON.stringify(checkStatusData),
                    success: function (res) {
                        layer.msg(res.msg);
                        table.reload('tableGridSetInOutFlow');
                    }
                })

            },
            // 3 完工退回
            reBack() {

                const checkStatusData = table.checkStatus('tableGridSetOutsourcingInFlow').data

                console.log(checkStatusData)

                if (typeof checkStatusData[0].index == 'undefined') {
                    layer.msg('请选择有效数据', {icon: 2});
                    return
                }

                layer.load(2, {time: 3 * 1000});

                $.get(context_path + '/ycl/workorder/WorkOrder/returnProcess', {
                    rcvId: checkStatusData[0].rcvId,
                }, function (res) {
                    console.log(res)
                    if (res.retCode == 'error') {
                        layer.msg(res.retMess, {icon: 2});
                    } else {
                        layer.msg(res.retMess, {icon: 1});
                    }
                    return;
                })

            },
            initGetFtInfo() {
                $("#ftInfo").empty();
                $("#ftInfo").append('<option value="">请选择</option>');
                $.get(context_path + '/ycl/workorder/WorkOrder/getFtInfo'
                    , function (res) {
                        console.log(res)
                        let html = '';
                        let list = res.data;
                        for (let i = 0; i < list.length; i++) {
                            html += '<option value=' + list[i].id + '>' + list[i].text + '</option>';
                        }
                        $("#areaCodeSelectPICOEdit").append(html)
                        form.render('select');
                    })
            },
            addDeliveryOrder() {
                const checkStatusData = table.checkStatus('tableGridSetOutsourcing').data
                let purchaseOrderIds = checkStatusData.map(item => item.poLineId).toString()
                if (checkStatusData.length === 0) {
                    layer.msg('请选择订单', {icon: 2});
                    return
                }
                if (checkStatusData.some(item => item.supplierCode != checkStatusData[0].supplierCode)) {
                    layer.msg('请选择相同供应商', {icon: 2});
                    return
                }
                layer.open({
                    title: '新增'
                    , type: 2
                    , area: ['1200px', '600px'] //宽高
                    , maxmin: true,
                    content: context_path + '/ycl/purchaseIn/PurchaseOrder/toDeliveryOrderEdit?purchaseOrderIds=' + purchaseOrderIds
                });
            },
            initMaterialCodeSelect() {
                /**选择质保号*/
                $("#materialCodeSelect").select2({
                    placeholder: "请选择物料编码",//文本框的提示信息
                    minimumInputLength: 0, //至少输入n个字符，才去加载数据
                    allowClear: true, //是否允许用户清除文本信息
                    multiple: false,
                    closeOnSelect: false,
                    formatNoMatches: "没有结果",
                    formatSearching: "搜索中...",
                    formatAjaxError: "加载出错啦！",
                    ajax: {
                        url: context_path + '/ycl/workorder/WorkOrder/getFtInfo',
                        // url: context_path + '/materials/getMaterialPageByCode',
                        dataType: 'json',
                        delay: 250,
                        data: function (term, pageNo) { //在查询时向服务器端传输的数据
                            term = $.trim(term);
                            // selectParam = term;
                            return {
                                code: term, //联动查询的字符
                                limit: 15, //一次性加载的数据条数
                                page: pageNo //页码
                            }
                        },
                        results: function (data, pageNo) {
                            var res = data.data;
                            //如果没有查询到数据，将会返回空串
                            if (res.length > 0) {
                                var more = (pageNo * 15) < data.count; //用来判断是否还有更多数据可以加载
                                return {
                                    results: res,
                                    more: more
                                };
                            } else {
                                return {
                                    results: {
                                        "id": "0",
                                        "text": "没有更多结果"
                                    }
                                };
                            }
                        },
                        cache: true
                    }
                });
            }

        },
        created() {

        },
        mounted() {

        }

    })


    // 点击one
    table.on('row(tableGridFilterOutsourcing)', function (obj) {
        obj.tr.find('i[class="layui-anim layui-icon"]').trigger("click");
        vm.clickCount = 0
        const data = obj.data;
        data.subinventory = 'V001委外'
        vm.oneData = data

        $.get(context_path + '/ycl/workorder/WorkOrder/getRcvListByPoId', {
            poId: data.poId,
        }, function (res) {

            for (let i = 0; i < res.length; i++) {
                res[i].tempId = new Date().valueOf() + i
                res[i].index = i
            }
            table.reload('tableGridSetOutsourcingSend', {
                data: res
            });
        })
        table.reload('tableGridSetOutsourcingInFlow', {
            data: []
        });
        // let twoData = []
        // twoData.push(data)
        // table.reload('tableGridSetOutsourcingSend', {
        //     data: twoData
        // });
        obj.tr.addClass('layui-table-click').siblings().removeClass('layui-table-click');
    });

    // 点击two
    table.on('row(tableGridFilterOutsourcingSend)', function (obj) {

        const data = obj.data;


        // if (typeof data.index != 'undefined') {
        //     layer.msg('当前选择行已处理，不能继续操作');
        //     return
        // } else {
        //     obj.tr.find('i[class="layui-anim layui-icon"]').trigger("click");
        // }

        obj.tr.find('i[class="layui-anim layui-icon"]').trigger("click");

        $.get(context_path + '/ycl/workorder/WorkOrder/getWorkOrderListByRcvId', {
            rcvId: data.rcvId,
        }, function (res) {
            for (let i = 0; i < res.length; i++) {
                res[i].tempId = new Date().valueOf() + i
            }
            console.log(res);
            table.reload('tableGridSetOutsourcingInFlow', {
                data: res
            });
        })
        obj.tr.addClass('layui-table-click').siblings().removeClass('layui-table-click');
    });

    // table.on('radio(tableGridFilterOutsourcingSend)', function (obj, index) {
    //     console.log(obj.data); //选中行的相关数据
    //     let oldData = table.cache['tableGridSetOutsourcingSend'];
    //     if (typeof obj.data.index != 'undefined') {
    //         layer.msg('当前选择行已处理，不能继续操作');
    //         return
    //     }
    // });

    // 点击three
    table.on('row(tableGridOutsourcingInFlow)', function (obj) {
        obj.tr.find('i[class="layui-anim layui-icon"]').trigger("click");
        obj.tr.addClass('layui-table-click').siblings().removeClass('layui-table-click');
    });

    $("#materialCodeSelect").on("change", function (e) {
        var datas = $("#materialCodeSelect").select2("val");
        console.log(datas);
        $.get(context_path + '/ycl/workorder/WorkOrder/getFtInfo', {
            code: datas,
        }, function (res) {

            let oldData = table.cache['tableGridSetOutsourcingInFlow'];
            const checkStatusData = table.checkStatus('tableGridSetOutsourcingInFlow').data;
            oldData[oldData.length - 1].ftItemNum = res.data[0].id;
            oldData[oldData.length - 1].ftItemId = res.data[0].ftId;
            oldData[oldData.length - 1].ftItemDesc = res.data[0].name;

            console.log(checkStatusData);
            table.reload('tableGridSetOutsourcingInFlow', {
                data: oldData
            })
        })
        // var datas = $("#materialCodeSelect").select2("val");
        /*selectData = datas;
				var selectSize = datas.length;
				if (selectSize > 1) {
						var $tags = $("#materialCodeSelect .select2-choices");
						//$("#s2id_materialInfor").html(selectSize+"个被选中");
						var $choicelist = $tags.find(".select2-search-choice");
						var $clonedChoice = $choicelist[0];
						$tags.children(".select2-search-choice").remove();
						$tags.prepend($clonedChoice);
						$tags.find(".select2-search-choice").find("div").html(selectSize + "个被选中");
						$tags.find(".select2-search-choice").find("a").removeAttr("tabindex");
						$tags.find(".select2-search-choice").find("a").attr("href", "#");
						$tags.find(".select2-search-choice").find("a").attr("onclick", "removeChoice();");
				}*/
        //执行select的查询方法
        // $("#materialCodeSelect").select2("search", selectParam);
    });

    //监听select
    form.on('select(shelfCode)', function (data) {
        var elem = data.othis.parents('tr');
        var id = elem.first().find('td').eq(1).text();
        console.log(id)
        let val = data.value
        this.tbData = table.cache['tableGridSetOutsourcingSend'];

        this.tbData.forEach(n => {
            if (n.tempId == id) {
                console.log('data.value', data.value)
                n.locatorDesc = data.value;
            }
        })
        console.log(vm.tbData)
        table.reload({
            data: this.tbData
        });
    });

    table.on('edit(tableGridOutsourcingInFlow)', function (obj) { //注：edit是固定事件名，test是table原始容器的属性 lay-filter="对应的值"
        console.log(obj.value); //得到修改后的值
        console.log(obj.field); //当前编辑的字段名
        console.log(obj.data); //所在行的所有相关数据

        if (obj.field == "completedQuantity" || obj.field == "fyCompletedQuantity" || obj.field == "ftcompletedQuantity") {
            let oldData = table.cache['tableGridSetOutsourcingInFlow'];

            for (let i = 0; i < oldData.length; i++) {
                if (obj.data.tempId == oldData[i].tempId) {
                    oldData[i].completedQuantity = obj.value
                    oldData[i].fyCompletedQuantity = obj.value
                    oldData[i].ftcompletedQuantity = obj.value
                }
            }
            console.log(oldData)
            table.reload('tableGridSetOutsourcingInFlow', {
                data: oldData
            });
        }

    });

    // init
    ;!function () {
        vm.initTableIns()
        vm.initTableIns2()
        vm.initTableIns3()
        vm.initGetFtInfo()
        vm.initMaterialCodeSelect()
    }();
</script>