<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="organize_list_grid-div">
    <form id="organize_list_hiddenForm" action="<%=path%>/organize/toExcel.do" method="POST" style="display: none;">
        <input id="organize_list_ids" name="ids" value=""/>
    </form>
    <form id="organize_list_hiddenQueryForm" style="display:none;">
        <input name="entityCode" value="">
        <input name="entityName" value="">
        <input name="organizationCode" value="">
        <input name="organizationName" value="">
    </form>
    <c:if test="${operationCode.webSearch==1}">
        <div class="query_box" id="organize_list_yy" title="查询选项">
            <form id="organize_list_queryForm" style="max-width:100%;">
                <ul class="form-elements">
                    <li class="field-group field-fluid3">
                        <label class="inline"  style="margin-right:20px;width:100%;">
                            <span class="form_label" style="width:80px;">实体编码：</span>
                            <input type="text" id="entityCode"   name="entityCode" value="" style="width: calc(100% - 85px);" placeholder="实体编码" />
                        </label>
                    </li>
                    <li class="field-group field-fluid3">
                        <label class="inline" style="margin-right:20px;width:100%;">
                            <span class="form_label" style="width:80px;">实体名称：</span>
                            <input type="text" id="entityName"  name="entityName" value="" style="width: calc(100% - 85px);" placeholder="实体名称" />
                        </label>
                    </li>
                    <li class="field-group field-fluid3">
                        <label class="inline"  style="margin-right:20px;width:100%;">
                            <span class="form_label" style="width:80px;">组织编码：</span>
                            <input type="text" id="organizationCode"   name="organizationCode" value="" style="width: calc(100% - 85px);" placeholder="组织编码" />
                        </label>
                    </li>
                    <li class="field-group-top field-group field-fluid3">
                        <label class="inline" style="margin-right:20px;width:100%;">
                            <span class="form_label" style="width:80px;">组织名称：</span>
                            <input type="text" id="organizationName"  name="organizationName" value="" style="width: calc(100% - 85px);" placeholder="组织名称" />
                        </label>
                    </li>
                </ul>
                <div class="field-button" style="">
                    <div class="btn btn-info" onclick="organize_list_queryOk();">
                        <i class="ace-icon fa fa-check bigger-110"></i>查询
                    </div>
                    <div class="btn" onclick="organize_list_reset();"><i class="ace-icon icon-remove"></i>重置</div>
                    <a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>
                </div>
            </form>
        </div>
    </c:if>
    <div id="organize_list_fixed_tool_div" class="fixed_tool_div">
        <div id="organize_list_toolbar_" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="organize_list_grid-table" style="width:100%;height:100%;"></table><%--更改id--%>
    <div id="organize_list_grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
    var organize_list_oriData;
    var organize_list_grid;

    $("input").keypress(function (e) {
        if (e.which == 13) {
            organize_list_queryOk();
        }
    });

    $(function  (){
        $(".toggle_tools").click();
    });

    $("#organize_list_toolbar_").iToolBar({
        id: "organize_list_tb_01",
        items: [
            //{label: "查看",hidden:"${operationCode.webInfo}"=="1", onclick: organize_list_openInfoPage, iconClass:'icon-search'},
        ]
    });

    var organize_list_queryForm_data = iTsai.form.serialize($("#organize_list_queryForm"));

    organize_list_grid = jQuery("#organize_list_grid-table").jqGrid({
        url : context_path + "/wms2/baseinfo/organize/list.do",
        datatype : "json",
        colNames : [ "实体ID","实体编码","实体名称","组织编码","组织名称"],
        colModel : [
            {name : "entityId",index : "entityId",hidden:true},
            {name : "entityCode",index : "entityCode",width : 60 },
            {name : "entityName",index : "entityName",width : 60 },
            {name : "organizationCode",index : "organizationCode",width : 60 },
            {name : "organizationName",index : "organizationName",width : 60 }
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : "#organize_list_grid-pager",
        sortname : "",
        sortorder : "asc",
        altRows: true,
        viewrecords : true,
        hidegrid : false,
        autowidth : false,
        multiselect : false,
        multiboxonly : true,
        loadComplete: function (data) {
            var table = this;
            setTimeout(function () {
                updatePagerIcons(table);
                enableTooltips(table);
            }, 0);
            organize_list_oriData = data;
            $("#organize_list_grid-table").closest(".ui-jqgrid-bdiv").css({ 'overflow-x': 'auto' });
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
    });

    jQuery("#organize_list_grid-table").navGrid("#organize_list_grid-pager", {
        edit: false,
        add: false,
        del: false,
        search: false,
        refresh: false
    })
        .navButtonAdd("#organize_list_grid-pager", {
            caption: "",
            buttonicon: "fa fa-refresh green",
            onClickButton: function () {
                $("#organize_list_grid-table").jqGrid("setGridParam", {
                    postData: {queryJsonString: ""} //发送数据
                }).trigger("reloadGrid");
            }
        }).navButtonAdd("#organize_list_grid-pager", {
        caption: "",
        buttonicon: "fa icon-cogs",
        onClickButton: function () {
            jQuery("#organize_list_grid-table").jqGrid("columnChooser",{
                done: function(perm, cols){
                    // dynamicColumns(cols,materials_list_dynamicDefalutValue);
                    $("#organize_list_grid-table").jqGrid("setGridWidth", $("#organize_list_grid-div").width());
                }
            });
        }
    });

    $(window).on("resize.jqGrid", function () {
        $("#organize_list_grid-table").jqGrid("setGridWidth", $(window).width() - $("#sidebar").width() - 7);
        $("#organize_list_grid-table").jqGrid("setGridHeight", $(".container-fluid").height() - 10 - $("#organize_list_yy").outerHeight(true) -
            $("#organize_list_fixed_tool_div").outerHeight(true) - $("#organize_list_grid-pager").outerHeight(true) -
            $("#gview_organize_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
    });
    $(window).triggerHandler("resize.jqGrid");


    /**
     * 查询按钮点击事件
     */
    function organize_list_queryOk(){
        var queryParam = iTsai.form.serialize($("#organize_list_queryForm"));
        //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
        organize_list_queryByParam(queryParam);
    }

    //查询
    function organize_list_queryByParam(jsonParam) {
        iTsai.form.deserialize($("#organize_list_hiddenQueryForm"), jsonParam);
        var queryParam = iTsai.form.serialize($("#organize_list_hiddenQueryForm"));
        var queryJsonString = JSON.stringify(queryParam);
        $("#organize_list_grid-table").jqGrid("setGridParam",
            {
                postData: {queryJsonString: queryJsonString}
            }
        ).trigger("reloadGrid");
    }

    //重置
    function organize_list_reset(){
        $("#organize_list_queryForm #organize_list_cq").select2("val","");
        iTsai.form.deserialize($("#organize_list_queryForm"),organize_list_queryForm_Date);
        organize_list_queryByParam(organize_list_queryForm_Date);
    }

    $(".date-picker").datetimepicker({
        format: "YYYY-MM-DD HH:mm:ss" ,
        autoclose : true,
        todayHighlight : true
    });
</script>