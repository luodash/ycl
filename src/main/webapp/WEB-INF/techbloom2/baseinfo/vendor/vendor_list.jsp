<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="vendor_list_grid-div">
    <form id="vendor_list_hiddenForm" action="<%=path%>/vendor/toExcel.do" method="POST" style="display: none;">
        <input id="vendor_list_ids" name="ids" value=""/>
    </form>
    <form id="vendor_list_hiddenQueryForm" style="display:none;">
        <input name="code" value="">
        <input name="name" value="">
    </form>
    <c:if test="${operationCode.webSearch==1}">
        <div class="query_box" id="vendor_list_yy" title="查询选项">
            <form id="vendor_list_queryForm" style="max-width:100%;">
                <ul class="form-elements">
                    <li class="field-group field-fluid3">
                        <label class="inline"  style="margin-right:20px;width:100%;">
                            <span class="form_label" style="width:80px;">供应商编号：</span>
                            <input type="text" id="code"   name="code" value="" style="width: calc(100% - 85px);" placeholder="供应商编号" />
                        </label>
                    </li>
                    <li class="field-group field-fluid3">
                        <label class="inline" style="margin-right:20px;width:100%;">
                            <span class="form_label" style="width:80px;">供应商名称：</span>
                            <input type="text" id="name"  name="name" value="" style="width: calc(100% - 85px);" placeholder="供应商名称" />
                        </label>
                    </li>
                </ul>
                <div class="field-button" style="">
                    <div class="btn btn-info" onclick="vendor_list_queryOk();">
                        <i class="ace-icon fa fa-check bigger-110"></i>查询
                    </div>
                    <div class="btn" onclick="vendor_list_reset();"><i class="ace-icon icon-remove"></i>重置</div>
                </div>
            </form>
        </div>
    </c:if>
    <div id="vendor_list_fixed_tool_div" class="fixed_tool_div">
        <div id="vendor_list_toolbar_" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="vendor_list_grid-table" style="width:100%;height:100%;"></table>
    <div id="vendor_list_grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
    var vendor_list_oriData;
    var vendor_list_grid;

    $("input").keypress(function (e) {
        if (e.which === 13) {
            vendor_list_queryOk();
        }
    });

    $(function  (){
        $(".toggle_tools").click();
    });

    $("#vendor_list_toolbar_").iToolBar({
        id: "vendor_list_tb_01",
        items: [
            /*{label: "同步数据",hidden:"${operationCode.webTbsj}"==="1", onclick: vendor_list_synchronousdata, iconClass:'icon-refresh'},*/
        ]
    });

    var vendor_list_queryForm_data = iTsai.form.serialize($("#vendor_list_queryForm"));

    vendor_list_grid = jQuery("#vendor_list_grid-table").jqGrid({
        url : context_path + "/wms2/baseinfo/vendor/list.do",
        datatype : "json",
        colNames : [ "主键","供应商代码","供应商名称","状态"],
        colModel : [
            {name : "id",index : "id",hidden:true},
            {name : "code",index : "code",width : 60 },
            {name : "name",index : "name",width : 60 },
            {name : "status",index : "status",width : 60,formatter:function(cellvalue,option,rowObject){
                    if(cellvalue==='Y'){
                        return "<span style='color:green;font-weight:bold;'>生效</span>";
                    }else if(cellvalue==='N'){
                        return "<span style='color:red;font-weight:bold;'>失效</span>";
                    }else {
                        return "<span style='color:grey;font-weight:bold;'>已提交</span>";
                    }
                }
            }
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : "#vendor_list_grid-pager",
        sortname : "id",
        sortorder : "asc",
        altRows: true,
        viewrecords : true,
        hidegrid : false,
        autowidth : true,
        multiselect : true,
        multiboxonly : true,
        loadComplete: function (data) {
            var table = this;
            setTimeout(function () {
                updatePagerIcons(table);
                enableTooltips(table);
            }, 0);
            vendor_list_oriData = data;
            $("#vendor_list_grid-table").closest(".ui-jqgrid-bdiv").css({ 'overflow-x': 'auto' });
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
    });

    jQuery("#vendor_list_grid-table").navGrid("#vendor_list_grid-pager", {
        edit: false,
        add: false,
        del: false,
        search: false,
        refresh: false
    }).navButtonAdd("#vendor_list_grid-pager", {
            caption: "",
            buttonicon: "fa fa-refresh green",
            onClickButton: function () {
                $("#vendor_list_grid-table").jqGrid("setGridParam", {
                    postData: {queryJsonString: ""} //发送数据
                }).trigger("reloadGrid");
            }
        }).navButtonAdd("#vendor_list_grid-pager", {
        caption: "",
        buttonicon: "fa icon-cogs",
        onClickButton: function () {
            jQuery("#vendor_list_grid-table").jqGrid("columnChooser",{
                done: function(perm, cols){
                    // dynamicColumns(cols,materials_list_dynamicDefalutValue);
                    $("#vendor_list_grid-table").jqGrid("setGridWidth", $("#vendor_list_grid-div").width());
                }
            });
        }
    });

    $(window).on("resize.jqGrid", function () {
        $("#vendor_list_grid-table").jqGrid("setGridWidth", $(window).width() - $("#sidebar").width() - 7);
        $("#vendor_list_grid-table").jqGrid("setGridHeight", $(".container-fluid").height() - 10 -
        $("#vendor_list_yy").outerHeight(true) - $("#vendor_list_fixed_tool_div").outerHeight(true) -
        $("#vendor_list_grid-pager").outerHeight(true) - $("#gview_vendor_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
    });
    $(window).triggerHandler("resize.jqGrid");

    /**
     * 查询按钮点击事件
     */
    function vendor_list_queryOk(){
        var queryParam = iTsai.form.serialize($("#vendor_list_queryForm"));
        //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
        vendor_list_queryByParam(queryParam);
    }

    //查询
    function vendor_list_queryByParam(jsonParam) {
        iTsai.form.deserialize($("#vendor_list_hiddenQueryForm"), jsonParam);
        var queryParam = iTsai.form.serialize($("#vendor_list_hiddenQueryForm"));
        var queryJsonString = JSON.stringify(queryParam);
        $("#vendor_list_grid-table").jqGrid("setGridParam",
            {
                postData: {queryJsonString: queryJsonString}
            }
        ).trigger("reloadGrid");
    }

    //重置
    function vendor_list_reset(){
        $("#vendor_list_queryForm #vendor_list_cq").select2("val","");
        iTsai.form.deserialize($("#vendor_list_queryForm"),vendor_list_queryForm_data);
        vendor_list_queryByParam(vendor_list_queryForm_data);
    }

    $(".date-picker").datetimepicker({
        format: "YYYY-MM-DD HH:mm:ss" ,
        autoclose : true,
        todayHighlight : true
    });

    //同步数据
    function vendor_list_synchronousdata(){
        $.ajax({
            type: "GET",
            url: context_path+"/wms2/baseinfo/vendor/synchronousdata.do",
            dataType: "json",
            success: function(data){
                if(data.result){
                    layer.msg(data.msg,{icon:1,time:1200});
                    vendor_list_grid.trigger("reloadGrid");
                }else{
                    layer.msg(data.msg,{icon:2,time:1200});
                }
            },
            error:function(XMLHttpRequest){
                alert(XMLHttpRequest.readyState);
                alert("出错啦！！！");
            }
        });
    }
</script>