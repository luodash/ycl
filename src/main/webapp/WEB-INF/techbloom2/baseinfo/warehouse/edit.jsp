<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
%>
<div id="car_edit_page" class="row-fluid" style="height: inherit;">
    <form id="wms2_warehouse_editForm" class="form-horizontal" style="overflow: auto; height: calc(100% - 70px);">
        <input type="hidden" id="car_edit_id" name="id" value="${warehouse.id}">
        <div class="control-group">
            <label class="control-label" for="car_edit_factoryCode">厂区：</label>
            <div class="controls required">
                <input class="span11 select2_input " type="text" name="factoryCode" id="car_edit_factoryCode" value="${car.factoryCode}" placeholder="厂区"/>
                <input type="hidden" id="car_edit_factoryid" value="${car.factoryCode}">
                <input type="hidden" id="car_edit_factoryno" value="${car.factoryCodeName}">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="warehouse_info">仓库编号：</label>
            <div class="controls">
                <div class="input-append span12 required">
                    <input class="span11" type="text" id="warehouse_info" name="storeareaCode"
                           value="${warehouse.storeareaCode}"
                           placeholder="标签号"/>
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="car_edit_warehouse">仓库名称：</label>
            <div class="controls">
                <div class="input-append span12 required">
                    <input class="span11" type="text" id="warehouse_info" name="storeareaName"
                           value="${warehouse.storeareaName}"
                           placeholder="标签号"/>
                </div>
            </div>
        </div>
    </form>
    <div class="field-button" style="text-align: center;border-top: 0px;margin: 15px auto;">
		<span class="btn btn-info" onclick="submitForm();">
		   <i class="ace-icon fa fa-check bigger-110"></i>保存
		</span>
        <span class="btn btn-danger" onclick="layer.closeAll();">
		   <i class="icon-remove"></i>&nbsp;取消
		</span>
    </div>
</div>

<script type="text/javascript" src="<%=path%>/static/js/techbloom/selectMultip.js"></script>
<script type="text/javascript">
    let factoryCodeId = $("#wms2_warehouse_editForm #car_edit_factoryCode").val();
    //渲染多选框
    selectMultip.register();

    $("#wms2_warehouse_editForm").validate({
        rules: {
            "name": {
                required: true
            },
            "warehouse": {
                required: true
            }
        },
        messages: {
            "name": {
                required: "请输入名称！"
            },
            "warehouse": {
                required: "请选择仓库！"
            }
        },
        errorClass: "help-inline",
        errorElement: "div",
        highlight: function (element, errorClass, validClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        }
    });

    //确定按钮点击事件
    function submitForm() {
        if ($("#wms2_warehouse_editForm").valid()) {
            saveInfo($("#wms2_warehouse_editForm").serialize());
        }
    }

    //保存/修改用户信息
    function saveInfo(bean) {
        console.log(bean)
        $.ajax({
            url: context_path + "/wms2/baseinfo/warehouse/save",
            type: "POST",
            data: bean,
            dataType: "JSON",
            success: function (data) {
                iTsai.form.deserialize($("#car_list_hiddenForm"), iTsai.form.serialize($("#car_list_hiddenQueryForm")));
                const queryParam = iTsai.form.serialize($("#car_list_hiddenQueryForm"));
                const queryJsonString = JSON.stringify(queryParam);
                if (Boolean(data.result)) {
                    layer.msg("保存成功！", {icon: 1});
                    //关闭当前窗口
                    layer.close($queryWindow);
                    //刷新列表
                    $("#car_list_grid-table").jqGrid('setGridParam', {
                        postData: {queryJsonString: queryJsonString}
                    }).trigger("reloadGrid");
                } else {
                    layer.alert("保存失败，请稍后重试！", {icon: 2});
                }
            },
            error: function (XMLHttpRequest) {
                alert(XMLHttpRequest.readyState);
                alert("出错啦！！！");
            }
        });
    }

    //厂区下拉框初始化
    $("#wms2_warehouse_editForm #car_edit_factoryCode").select2({
        placeholder: "选择部门",
        minimumInputLength: 0,   //至少输入n个字符，才去加载数据
        allowClear: true,  //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/factoryArea/getFactoryList",
            type: "POST",
            dataType: 'json',
            delay: 250,
            data: function (term, pageNo) {     //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term,    //联动查询的字符
                    pageSize: 15,    //一次性加载的数据条数
                    pageNo: pageNo    //页码
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) {   //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res, more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }

    });


    $("#wms2_warehouse_editForm #car_edit_factoryCode").on("change", function (e) {
        factoryCodeId = $("#wms2_warehouse_editForm #car_edit_factoryCode").val();
    });

    //点击编辑的默认数据
    if ($("#wms2_warehouse_editForm #car_edit_factoryid").val() != "") {
        $("#wms2_warehouse_editForm #car_edit_factoryCode").select2("data", {
            id: $("#wms2_warehouse_editForm #car_edit_factoryid").val(),
            text: $("#wms2_warehouse_editForm #car_edit_factoryno").val()
        });
    }

    //仓库(根据厂区限定范围)
    $("#wms2_warehouse_editForm #car_edit_warehouse").select2({
        placeholder: "选择仓库",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        //multiple: true,//多选
        ajax: {
            url: context_path + "/car/selectWarehouse",
            type: "POST",
            dataType: 'json',
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    factoryCodeId: factoryCodeId
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = pageNo < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });

    if ($("#wms2_warehouse_editForm #car_edit_wid").val() != "") {
        $("#wms2_warehouse_editForm #car_edit_warehouse").select2("data", {
            id: $("#wms2_warehouse_editForm #car_edit_wid").val(),
            text: $("#wms2_warehouse_editForm #car_edit_wname").val()
        });
    }

</script>