<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="storehouse_list_grid-div">
    <form id="storehouse_list_hiddenForm" action="<%=path%>/storehouselist/materialExcel" method="POST"
          style="display: none;">
        <input name="ids" id="storehouse_list_ids" value=""/>
        <input name="queryFactoryCode" id="storehouse_list_queryFactoryCode" value="">
        <input name="queryCode" id="storehouse_list_queryCode" value="">
        <input name="queryName" id="storehouse_list_queryName" value="">
        <input name="queryExportExcelIndex" id="storehouse_list_queryExportExcelIndex" value=""/>
    </form>
    <form id="storehouse_list_hiddenQueryForm" style="display:none;">
        <input name="factoryCode" value=""/>
        <input name="code" value=""/>
        <input name="name" value=""/>
    </form>
    <c:if test="${operationCode.webSearch==1}">
        <div class="query_box" id="storehouse_list_yy" title="查询选项">
            <form id="storehouse_list_queryForm" style="max-width:100%;">
                <ul class="form-elements">
                    <li class="field-group field-fluid3">
                        <label class="inline" for="storehouse_list_factoryCode" style="margin-right:20px;width: 100%;">
                            <span class="form_label" style="width:80px;">厂区：</span>
                            <input id="storehouse_list_factoryCode" name="factoryCode" type="text"
                                   style="width: calc(100% - 85px);" placeholder="厂区">
                        </label>
                    </li>
                    <li class="field-group field-fluid3">
                        <label class="inline" for="storehouse_list_code" style="margin-right:20px;width: 100%;">
                            <span class="form_label" style="width:80px;">仓库编号：</span>
                            <input id="storehouse_list_code" name="code" type="text" style="width: calc(100% - 85px);"
                                   placeholder="仓库编号">
                        </label>
                    </li>
                    <li class="field-group field-fluid3">
                        <label class="inline" for="storehouse_list_name" style="margin-right:20px;width: 100%;">
                            <span class="form_label" style="width:80px;">仓库名称：</span>
                            <input id="storehouse_list_name" name="name" type="text" style="width: calc(100% - 85px);"
                                   placeholder="仓库名称">
                        </label>
                    </li>
                </ul>
                <div class="field-button" style="">
                    <div class="btn btn-info" onclick="storehouse_list_queryOk();">
                        <i class="ace-icon fa fa-check bigger-110"></i>查询
                    </div>
                    <div class="btn" onclick="storehouse_list_reset();"><i class="ace-icon icon-remove"></i>重置</div>
                </div>
            </form>
        </div>
    </c:if>
    <div id="storehouse_list_fixed_tool_div" class="fixed_tool_div">
        <div id="storehouse_list___toolbar__" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="storehouse_list_grid-table" style="width:100%;height:100%;"></table><%--更改--%>
    <div id="storehouse_list_grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
    var storehouse_list_oriData;
    var storehouse_list_grid;
    var storehouse_list_dynamicDefalutValue = "cbe154ed57f14f8e803c4d0982a4d18c";
    var storehouse_list_exportExcelIndex;

    $("input").keypress(function (e) {
        if (e.which == 13) {
            storehouse_list_queryOk();
        }
    });

    $(function () {
        $(".toggle_tools").click();
    });

    <%-- 按钮 --%>
     $("#storehouse_list___toolbar__").iToolBar({
        id: "storehouse_list___tb__01",
        items: [
            {
                label: "添加",
                hidden: "${operationCode.webTbsj}" == "1",
                onclick: storehouse_list_openAddPage,
                iconClass: 'glyphicon glyphicon-plus'
            },
            {
                label: "编辑",
                hidden: "${operationCode.webUnmultiplexing}" == "1",
                onclick: storehouse_list_openEditPage,
                iconClass: 'glyphicon glyphicon-pencil'
            },
            {
                label: "批量导入",
                hidden: "${operationCode.webTbsj}" == "1",
                onclick: storehouse_list_openAddPage,
                iconClass: 'glyphicon glyphicon-plus'
            },
            {
                label: "批量导出",
                hidden: "${operationCode.webTbsj}" == "1",
                onclick: storehouse_list_openAddPage,
                iconClass: 'glyphicon glyphicon-plus'
            }
        ]
    });

     var storehouse_list_queryForm_data = iTsai.form.serialize($("#storehouse_list_queryForm"));

     /**
      * 数据
      * */
    storehouse_list_grid = jQuery("#storehouse_list_grid-table").jqGrid({
        url: context_path + "/wms2/baseinfo/shelf/list.do",
        datatype: "json",
        colNames: ["主键", "厂区编号","厂区名称","仓库编号", "仓库名称", "库位编码", "库位名称", "是否允许混放","状态","推荐物料大类","备注","创建人","创建时间"],
        colModel: [
            {name: "id", index: "a.id", hidden: true},
            {name: "factoryCode", index: "a.factorycode", width: 30},
            {name: "factoryName", index: "a.factorycode", width: 60},
            {name: "code", index: "a.code", width: 30},
            {name: "name", index: "a.name", width: 60},
            {name: "number", index: "a.number", width: 30},
            {name: "ckname", index: "a.ckname", width: 60},
            {name: "yunxu", index: "a.yunxu", width: 60},
            {
                name: "state", index: "a.state", width: 30, formatter: function (cellValu, option, rowObject) {
                    if (cellValu == '1') {
                        return '<span style="color:green;font-weight:bold;">可用</span>';
                    }
                    if (cellValu == '0') {
                        return '<span style="color:red;font-weight:bold;">不可用</span>';
                    }
                }
            },
            {name: "recommend", index: "a.recommend", width: 60},
            {name: "remark", index: "a.remark", width: 60},
            {name: "creator", index: "a.creator", width: 60},
            {name: "time", index: "a.time", width: 60},
        ],
        rowNum: 20,
        rowList: [10, 20, 30],
        pager: "#storehouse_list_grid-pager",
        sortname: "a.id",
        sortorder: "asc",
        altRows: true,
        viewrecords: true,
        autowidth: true,
        multiselect: true,
        multiboxonly: true,
        beforeRequest: function () {
            dynamicGetColumns(storehouse_list_dynamicDefalutValue, "storehouse_list_grid-table", $(window).width() - $("#sidebar").width() - 7);
            //重新加载列属性
        },
        loadComplete: function (data) {
            var table = this;
            setTimeout(function () {
                updatePagerIcons(table);
                enableTooltips(table);
            }, 0);
            storehouse_list_oriData = data;
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext: "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
    });

     jQuery("#storehouse_list_grid-table").navGrid("#storehouse_list_grid-pager", {
        edit: false,
        add: false,
        del: false,
        search: false,
        refresh: false
    })
     .navButtonAdd("#storehouse_list_grid-pager", {
            caption: "",
            buttonicon: "fa fa-refresh green",
            onClickButton: function () {
                $("#storehouse_list_grid-table").jqGrid("setGridParam", {
                    postData: {queryJsonString: ""} //发送数据
                }).trigger("reloadGrid");
            }
        }).navButtonAdd("#storehouse_list_grid-pager", {
        caption: "",
        buttonicon: "fa icon-cogs",
        onClickButton: function () {
            jQuery("#storehouse_list_grid-table").jqGrid("columnChooser", {
                done: function (perm, cols) {
                    dynamicColumns(cols, storehouse_list_dynamicDefalutValue);
                    $("#storehouse_list_grid-table").jqGrid("setGridWidth", $("#storehouse_list_grid-div").width());
                    storehouse_list_exportExcelIndex = perm;
                }
            });
        }
    });

     $(window).on("resize.jqGrid", function ()  {
        $("#storehouse_list_grid-table").jqGrid("setGridWidth", $(window).width() - $("#sidebar").width() - 7);
        $("storehouse_list_grid-table").jqGrid("setGridHeight", $(".container-fluid").height() - 10 - $("#storehouse_list_yy").outerHeight(true) -
            $("#storehouse_list_fixed_tool_div").outerHeight(true) - $("#storehouse_list_grid-pager").outerHeight(true) -
            $("#gview_storehouse_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
    });
     $(window).triggerHandler("resize.jqGrid");

     /**打开添加页面*/
    function storehouse_list_openAddPage() {
        $.post(context_path + "/wms2/baseinfo/shelf/toAdd.do", {}, function (str) {
            $queryWindow = layer.open({
                title: "库位添加",
                type: 1,
                skin: "layui-layer-molv",
                area: "600px",
                shade: 0.6, //遮罩透明度
                moveType: 1, //拖拽风格，0是默认，1是传统拖动
                anim: 2,
                content: str,
                success: function (layero, index) {
                    layer.closeAll('loading');
                }
            });
        });
    }

    /**打开编辑页面*/
    function storehouse_list_openEditPage(){
        var selectAmount = getGridCheckedNum("#storehouse_list_grid-table");
        if(selectAmount==0){
            layer.msg("请选择一条记录！",{icon:2});
            return;
        }else if(selectAmount>1){
            layer.msg("只能选择一条记录！",{icon:8});
            return;
        }
        layer.load(2);
        $.post(context_path+'/wms2/baseinfo/shelf/toAdd.do', {
            id:jQuery("#car_list_grid-table").jqGrid("getGridParam", "selrow")
        }, function(str){
            $queryWindow = layer.open({
                title : "库位编辑",
                type: 1,
                skin : "layui-layer-molv",
                area : "600px",
                shade: 0.6, //遮罩透明度
                moveType: 1, //拖拽风格，0是默认，1是传统拖动
                content: str,//注意，如果str是object，那么需要字符拼接。
                success:function(layero, index){
                    layer.closeAll("loading");
                }
            });
        }).error(function() {
            layer.closeAll();
            layer.msg("加载失败！",{icon:2});
        });
    }

    /**查询按钮点击事件*/
    function storehouse_list_queryOk() {
        var queryParam = iTsai.form.serialize($("#storehouse_list_queryForm"));
        //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
        storehouse_list_queryByParam(queryParam);
    }

    function storehouse_list_queryByParam(jsonParam) {
        iTsai.form.deserialize($("#storehouse_list_hiddenQueryForm"), jsonParam);
        var queryParam = iTsai.form.serialize($("#storehouse_list_hiddenQueryForm"));
        var queryJsonString = JSON.stringify(queryParam);
        $("#storehouse_list_grid-table").jqGrid("setGridParam",
            {
                postData: {queryJsonString: queryJsonString}
            }
        ).trigger("reloadGrid");
    }

    /**厂区编号*/
    $("#storehouse_list_factoryCode").select2({
        placeholder: "选择厂区",
        minimumInputLength: 0,   //至少输入n个字符，才去加载数据
        allowClear: true,  //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/factoryArea/getFactoryList",
            type: "POST",
            dataType: 'json',
            delay: 250,
            data: function (term, pageNo) {     //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term,    //联动查询的字符
                    pageSize: 15,    //一次性加载的数据条数
                    pageNo: pageNo    //页码
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) {   //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res, more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });

    $("#storehouse_list_factoryCode").on("change.select2", function () {
            $("#storehouse_list_factoryCode").trigger("keyup")
        }
    );

</script>
