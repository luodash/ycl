<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
%>
<div id="add_page" class="row-fluid" style="height: inherit;margin:0px">
    <form action="" class="form-horizontal" id="add_baseInfor" name="baseInfor" method="post" target="_ifr" style="border-bottom: solid 2px #3b73af;">
        <input type="hidden" id="add_id" name="id" value="${moveStorageId}">
        <div class="row" style="margin:0;padding:0;">
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="add_userName" >送货单号：</label>
                <div class="controls">
                    <div class="" >
                        <input type="text" id="add_userNa" class="span10" name="userName" value="${userName}"/>
                    </div>
                </div>
            </div>
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="add_info" >送货方：</label>
                <div class="controls">
                    <div class="" >
                        <input type="text" id="add_inf" class="span10" name="info" value=""  placeholder="" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin:0;padding:0;">
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="add_userName" >物流单号/车牌号：</label>
                <div class="controls">
                    <div class="" >
                        <input type="text" id="add_userName1" class="span10" name="userName" value="${userName}"/>
                    </div>
                </div>
            </div>
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="add_info" >驾驶员/tel：</label>
                <div class="controls">
                    <div class="" >
                        <input type="text" id="add_info1" class="span10" name="info" value=""  placeholder="" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin:0;padding:0;">
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="add_userName" >送货方备注：</label>
                <div class="controls">
                    <div class="" >
                        <input type="text" id="add_userName" class="span10" name="userName" value="${userName}"/>
                    </div>
                </div>
            </div>
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="add_info" >送货日期：</label>
                <div class="controls">
                    <div class="" >
                        <input type="text" id="add_info" class="span10" name="info" value=""  placeholder="" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin:0;padding:0;">
            <div class="control-group span6" style="display: inline">
                <label class="control-label" >收货方：</label>
                <div class="controls">
                    <div class="" >
                        <input type="text" id="createdBy1" class="span10" name="userName" value="${userName}"/>
                    </div>
                </div>
            </div>
            <div class="control-group span6" style="display: inline">
                <label class="control-label" >收货组织/使用部门：</label>
                <div class="controls">
                    <div class="" >
                        <input type="text" id="createdOn1" class="span10" name="info" value=""  placeholder="" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin:0;padding:0;">
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="createdOn1" >采购员：</label>
                <div class="controls">
                    <div class="" >
                        <input type="text" id="createdBy" class="span10" name="userName" value="${userName}"/>
                    </div>
                </div>
            </div>
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="createdOn1" >联系电话：</label>
                <div class="controls">
                    <div class="" >
                        <input type="text" id="a" class="span10" name="userName" value="${userName}"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin:0;padding:0;">
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="createdOn1" >发货地址：</label>
                <div class="controls">
                    <div class="" >
                        <input type="text" id="b" class="span10" name="userName" value="${userName}"/>
                    </div>
                </div>
            </div>
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="createdOn1" >收货地址：</label>
                <div class="controls">
                    <div class="" >
                        <input type="text" id="c" class="span10" name="userName" value="${userName}"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin:0;padding:0;">
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="createdOn1" >制单人：</label>
                <div class="controls">
                    <div class="" >
                        <input type="text" id="d" class="span10" name="userName" value="${userName}"/>
                    </div>
                </div>
            </div>
            <div class="control-group span6" style="display: inline">
                <label class="control-label" for="createdOn1" >制单日期：</label>
                <div class="controls">
                    <div class="" >
                        <input type="text" id="f" class="span10" name="userName" value="${userName}"/>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div id="add_grid-div-c" style="width:100%;margin:0px auto;">
        <div id="add_fixed_tool_div" class="fixed_tool_div detailToolBar">
            <div id="add___toolbar__-c" style="float:left;overflow:hidden;"></div>
        </div>
        <table id="add_grid-table-c" style="width:100%;height:100%;"></table>
        <div id="add_grid-pager-c"></div>
    </div>
</div>
<iframe src="about:blank" name="_ifr" height="0" class="hidden"></iframe>
<script type="text/javascript" src="<%=path%>/static/js/techbloom/move/moveinfo/add.js"></script>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriData;      //表格数据
    var _grid;        //表格对象
    var selectData = 0;   //存放物料选择框中的值
    var selectParam = "";  //存放之前的查询条件
    var factorySelect;
    var moveCode = "";   //移库单号

    $("#add_baseInfor").validate({
        rules: {
            "warehouseId": {
                required: true,
            },
        },
        messages: {
            "warehouseId": {
                required: "请选择仓库!",
            },
        },
        errorClass: "help-inline",
        errorElement: "span",
        highlight:function(element, errorClass, validClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        }
    })

    $("#add_formSave").click(function(){
        if($('#add_baseInfor').valid()){
            //通过验证：获取表单数据，保存表单信息
            saveFormInfo($('#add_baseInfor').serialize());
        }
    });

    //清空物料多选框中的值
    function removeChoice(){
        $("#s2id_qaCode .select2-choices").children(".select2-search-choice").remove();
        $("#add_qaCode").select2("val","");
        selectData = 0;
    }

    $('[data-rel=tooltip]').tooltip();

    //工具栏
    $("#add___toolbar__-c").iToolBar({
        id:"add___tb__01",
        items:[
            {label:"删除", onclick:addDetail},
            {label:"新增", onclick:addDetail}
        ]
    });


    //初始化表格
    _grid =  $("#add_grid-table-c").jqGrid({
        url : context_path + "/move/detailList.do",
        datatype : "json",
        colNames : ["详情主键","序号","系统订单号","订单行号","批次号","物料编码","物料名称",
            "主单位","送货件数", "送货数量", "实收数量", "上架数量","质检状态","备注"],
        colModel : [
            {name : "id",index : "id",width : 20,hidden:true},
            {name : "id",index : "id",width : 20},
            {name : "n1",index:"n1",width : 20},
            {name : "n2",index:"n2",width : 30},
            {name : "n3",index:"n3",width : 20},
            {name : "n4",index:"n4",width : 30},
            {name : "n5",index:"n5",width : 30},
            {name : "n6",index:"n6",width : 20},
            {name : "n7",index:"n7",width : 20},
            {name : "n8",index:"n5",width : 30},
            {name : "n9",index:"n6",width : 20},
            {name : "n10",index:"n7",width : 20},
            {name : "n11",index:"n7",width : 20},
            {name : "n12",index:"n7",width : 20},
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : "#add_grid-pager-c",
        sortname : "t1.id",
        sortorder : "asc",
        altRows: true,
        viewrecords : true,
        autowidth:true,
        footerrow: true,
        gridComplete: function() {
        // var rows = $("#orders").jqGrid("getRowData"), total_count = 0;
        // for(var i = 0, l = rows.length; i<l; i++) {
        //     total_count += (rows[i].goods_count - 0);
        // }
        $("#add_grid-table-c").jqGrid("footerData", "set", {unit:"-合计-",quantity:1});
    },
    multiselect:true,
        multiboxonly: true,
        loadComplete : function(data){
            var table = this;
            setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            oriData = data;
            $(window).triggerHandler("resize.jqGrid");
        },
        cellEdit: true,
        cellsubmit : "clientArray",
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据",
    });
    //在分页工具栏中添加按钮
    $("#add_grid-table-c").navGrid("#add_grid-pager-c",
        {edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#add_grid-pager-c",{
        caption:"",
        buttonicon:"ace-icon fa fa-refresh green",
        onClickButton: function(){
            $("#add_grid-table-c").jqGrid("setGridParam",
                {
                    url:context_path + "/move/detailList.do?id="+$("#add_id").val(),
                    postData: {id:$("#add_baseInfor #add_id").val(),queryJsonString:""} //发送数据  :选中的节点
                }
            ).trigger("reloadGrid");
        }
    });

    $(window).on("resize.jqGrid", function () {
        $("#add_grid-table-c").jqGrid("setGridWidth", $("#add_grid-div-c").width() - 3 );
        var height = $(".layui-layer-title",_grid.parents(".layui-layer")).height()+
            $("#add_baseInfor").outerHeight(true)+
            $("#add_materialDiv").outerHeight(true)+
            $("#add_grid-pager-c").outerHeight(true)+
            $("#add_fixed_tool_div.fixed_tool_div.detailToolBar").outerHeight(true)+
            $("#gview_add_grid-table-c .ui-jqgrid-hbox").outerHeight(true);
        $("#add_grid-table-c").jqGrid("setGridHeight",_grid.parents(".layui-layer").height()-height);
    });
    $(window).triggerHandler("resize.jqGrid");


    //添加质保单号
    function addDetail(){
    }
</script>
