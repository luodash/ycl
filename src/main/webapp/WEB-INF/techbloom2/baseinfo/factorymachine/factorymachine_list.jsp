<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="factoryMachine_list_grid-div">
    <form id="factoryMachine_list_hiddenForm" action="<%=path%>/factoryMachine/toExcel.do" method="POST" style="display: none;">
        <input id="factoryMachine_list_ids" name="ids" value=""/>
    </form>
    <form id="factoryMachine_list_hiddenQueryForm" style="display:none;">
        <input name="departCode" value="">
        <input name="departDesc" value="">
        <input name="opCode" value="">
        <input name="opDesc" value="">
        <input name="machineCode" value="">
        <input name="machineDesc" value="">
    </form>
    <c:if test="${operationCode.webSearch==1}">
        <div class="query_box" id="factoryMachine_list_yy" title="查询选项">
            <form id="factoryMachine_list_queryForm" style="max-width:100%;">
                <ul class="form-elements">
                    <li class="field-group field-fluid3">
                        <label class="inline"  style="margin-right:20px;width:100%;">
                            <span class="form_label" style="width:80px;">工厂编码：</span>
                            <input type="text" id="departCode"   name="departCode" value="" style="width: calc(100% - 85px);" placeholder="工厂编码" />
                        </label>
                    </li>
                    <li class="field-group field-fluid3">
                        <label class="inline" style="margin-right:20px;width:100%;">
                            <span class="form_label" style="width:80px;">工厂名称：</span>
                            <input type="text" id="departDesc"  name="departDesc" value="" style="width: calc(100% - 85px);" placeholder="工厂名称" />
                        </label>
                    </li>
                    <li class="field-group field-fluid3">
                        <label class="inline"  style="margin-right:20px;width:100%;">
                            <span class="form_label" style="width:80px;">工序编码：</span>
                            <input type="text" id="opCode"   name="opCode" value="" style="width: calc(100% - 85px);" placeholder="工序编码" />
                        </label>
                    </li>
                    <li class="field-group-top field-group field-fluid3">
                        <label class="inline" style="margin-right:20px;width:100%;">
                            <span class="form_label" style="width:80px;">工序名称：</span>
                            <input type="text" id="opDesc"  name="opDesc" value="" style="width: calc(100% - 85px);" placeholder="工序名称" />
                        </label>
                    </li>
                    <li class="field-group field-fluid3">
                        <label class="inline"  style="margin-right:20px;width:100%;">
                            <span class="form_label" style="width:80px;">机台编码：</span>
                            <input type="text" id="machineCode"   name="machineCode" value="" style="width: calc(100% - 85px);" placeholder="机台编码" />
                        </label>
                    </li>
                    <li class="field-group field-fluid3">
                        <label class="inline" style="margin-right:20px;width:100%;">
                            <span class="form_label" style="width:80px;">机台名称：</span>
                            <input type="text" id="machineDesc"  name="machineDesc" value="" style="width: calc(100% - 85px);" placeholder="机台名称" />
                        </label>
                    </li>
                </ul>
                <div class="field-button" style="">
                    <div class="btn btn-info" onclick="factoryMachine_list_queryOk();">
                        <i class="ace-icon fa fa-check bigger-110"></i>查询
                    </div>
                    <div class="btn" onclick="factoryMachine_list_reset();"><i class="ace-icon icon-remove"></i>重置</div>
                    <a style="margin-left: 8px;color: #40a9ff;" class="toggle_tools">收起 <i class="fa fa-angle-up"></i></a>
                </div>
            </form>
        </div>
    </c:if>
    <div id="factoryMachine_list_fixed_tool_div" class="fixed_tool_div">
        <div id="factoryMachine_list_toolbar_" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="factoryMachine_list_grid-table" style="width:100%;height:100%;"></table><%--更改id--%>
    <div id="factoryMachine_list_grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
    var factoryMachine_list_oriData;
    var factoryMachine_list_grid;

    $("input").keypress(function (e) {
        if (e.which === 13) {
            factoryMachine_list_queryOk();
        }
    });

    $(function  (){
        $(".toggle_tools").click();
    });

    $("#factoryMachine_list_toolbar_").iToolBar({
        id: "factoryMachine_list_tb_01",
        items: [
            {label: "同步数据", hidden:"${operationCode.webTbsj}"==="1",onclick: factorymachine_list_synchronousdata, iconClass:'icon-refresh'}
        ]
    });

    var factoryMachine_list_queryForm_data = iTsai.form.serialize($("#factoryMachine_list_queryForm"));

    factoryMachine_list_grid = jQuery("#factoryMachine_list_grid-table").jqGrid({
        url : context_path + "/ycl/baseinfo/factorymachine/list.do",
        datatype : "json",
        colNames : [ "工厂代码","工厂名称","工序编码","工序名称","机台编码","机台名称"],
        colModel : [
            {name : "departCode",index : "departCode",width : 60 },
            {name : "departDesc",index : "departDesc",width : 60 },
            {name : "opCode",index : "opCode",width : 60 },
            {name : "opDesc",index : "opDesc",width : 60 },
            {name : "machineCode",index : "machineCode",width : 60 },
            {name : "machineDesc",index : "machineDesc",width : 60 }
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : "#factoryMachine_list_grid-pager",
        sortname : "departCode",
        sortorder : "asc",
        altRows: true,
        viewrecords : true,
        hidegrid : false,
        autowidth : false,
        multiselect : true,
        multiboxonly : true,
        loadComplete: function (data) {
            var table = this;
            setTimeout(function () {
                updatePagerIcons(table);
                enableTooltips(table);
            }, 0);
            factoryMachine_list_oriData = data;
            $("#factoryMachine_list_grid-table").closest(".ui-jqgrid-bdiv").css({ 'overflow-x': 'auto' });
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
    });

    jQuery("#factoryMachine_list_grid-table").navGrid("#factoryMachine_list_grid-pager", {
        edit: false,
        add: false,
        del: false,
        search: false,
        refresh: false
    }).navButtonAdd("#factoryMachine_list_grid-pager", {
            caption: "",
            buttonicon: "fa fa-refresh green",
            onClickButton: function () {
                $("#factoryMachine_list_grid-table").jqGrid("setGridParam", {
                    postData: {queryJsonString: ""} //发送数据
                }).trigger("reloadGrid");
            }
        }).navButtonAdd("#factoryMachine_list_grid-pager", {
        caption: "",
        buttonicon: "fa icon-cogs",
        onClickButton: function () {
            jQuery("#factoryMachine_list_grid-table").jqGrid("columnChooser",{
                done: function(perm, cols){
                    // dynamicColumns(cols,materials_list_dynamicDefalutValue);
                    $("#factoryMachine_list_grid-table").jqGrid("setGridWidth", $("#factoryMachine_list_grid-div").width());
                }
            });
        }
    });

    $(window).on("resize.jqGrid", function () {
        $("#factoryMachine_list_grid-table").jqGrid("setGridWidth", $(window).width() - $("#sidebar").width() - 7);
        $("#factoryMachine_list_grid-table").jqGrid("setGridHeight", $(".container-fluid").height() - 10 -
        $("#factoryMachine_list_yy").outerHeight(true) - $("#factoryMachine_list_fixed_tool_div").outerHeight(true) -
        $("#factoryMachine_list_grid-pager").outerHeight(true) - $("#gview_factoryMachine_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
    });
    $(window).triggerHandler("resize.jqGrid");


    /**
     * 查询按钮点击事件
     */
    function factoryMachine_list_queryOk(){
        var queryParam = iTsai.form.serialize($("#factoryMachine_list_queryForm"));
        //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
        factoryMachine_list_queryByParam(queryParam);
    }

    //查询
    function factoryMachine_list_queryByParam(jsonParam) {
        iTsai.form.deserialize($("#factoryMachine_list_hiddenQueryForm"), jsonParam);
        var queryParam = iTsai.form.serialize($("#factoryMachine_list_hiddenQueryForm"));
        var queryJsonString = JSON.stringify(queryParam);
        $("#factoryMachine_list_grid-table").jqGrid("setGridParam",
            {
                postData: {queryJsonString: queryJsonString}
            }
        ).trigger("reloadGrid");
    }

    //重置
    function factoryMachine_list_reset(){
        $("#factoryMachine_list_queryForm #factoryMachine_list_cq").select2("val","");
        iTsai.form.deserialize($("#factoryMachine_list_queryForm"),factoryMachine_list_queryForm_data);
        factoryMachine_list_queryByParam(factoryMachine_list_queryForm_data);
    }

    $(".date-picker").datetimepicker({
        format: "YYYY-MM-DD HH:mm:ss" ,
        autoclose : true,
        todayHighlight : true
    });

    //同步数据
    function factorymachine_list_synchronousdata(){
        $.ajax({
            type: "GET",
            url: context_path+"/ycl/baseinfo/factorymachine/synchronousdata.do",
            dataType: "json",
            success: function(data){
                if(data.result){
                    layer.msg(data.msg,{icon:1,time:1200});
                    factoryMachine_list_grid.trigger("reloadGrid");
                }else{
                    layer.msg(data.msg,{icon:2,time:1200});
                }
            },
            error:function(XMLHttpRequest){
                alert(XMLHttpRequest.readyState);
                alert("出错啦！！！");
            }
        });
    }
</script>