<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<div id="lableManage" style="margin: 30px">
    <%--query tools--%>
    <blockquote class="layui-elem-quote">
        <form class="layui-form">
            <div class="layui-fluid">
                <div class="layui-row layui-col-space10">
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">采购订单号</label>
                            <div class="layui-input-inline">
                                <input type="text" id="poCode" class="layui-input"
                                       v-model="queryParams.poCode"/>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">物料编码</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" v-model="queryParams.materialCode"/>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <button type="button" class="layui-btn layui-btn-sm layui-btn-normal"
                                    @click="queryPageView">
                                <i class="layui-icon">&#xe615;</i>查询
                            </button>
                        </div>
                        <div class="layui-inline">
                            <button type="button" class="layui-btn layui-btn-primary layui-btn-sm"
                                    @click="restQueryParams">
                                <i class="layui-icon">&#xe669;</i>重置
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </blockquote>

    <%-- table grid --%>
    <table id="tableGridLableManage" lay-filter="tableGridFilterCheck"></table>
    <%-- table grid toolbar --%>
    <iframe style="visibility:hidden;" id="myiframe_lableManage" name="myiframe_lableManage" src="" width=0
            height=0></iframe>
    <%-- table grid toolbar --%>
    <%-- table grid toolbar --%>
    <script type="text/html" id="tableGridToolbarLableManage">
        <div class="layui-btn-container">
            <button type="button" class="layui-btn layui-btn-sm layui-btn-normal" lay-event="addLableManage">
                <i class="layui-icon">&#xe654;</i>新增
            </button>
            <button type="button" lay-event="printLable" id="lablePrint" class="layui-btn layui-btn-sm layui-btn-normal">
                <i class="layui-icon">&#xe66d;</i>打印标签
            </button>
        </div>
    </script>
</div>
<script>

    const context_path = '${APP_PATH}';
    const table = layui.table,
        form = layui.form,
        layer = layui.layer,
        laydate = layui.laydate;

    let tableGridIns3;

    //获取table的list集合
    let vm = new Vue({
        el: '#lableManage',
        data: {
            queryParams: {
                poCode: "",
                materialCode: ""
            }
        },
        methods: {
            initTableIns() {
                table.render({
                    elem: '#tableGridLableManage'
                    , url: context_path + '/ycl/lableManage/list'
                    , page: true
                    , toolbar: '#tableGridToolbarLableManage'
                    , id: 'tableGridLableManage'
                    , size: 'sm'
                    , defaultToolbar: []
                    , cols: [[
                        { type: 'radio' }
                        , {field: 'id', hide: true}
                        , {field: 'poCode', title: '采购订单号'}
                        , {field: 'materialCode', title: '物料编码'}
                        , {field: 'vendorLotsNum', title: '供应商批次'}
                        , {field: 'lotsNum', title: '远东批次'}
                        , {field: 'productionDate', title: '生产日期'}
                        , {field: 'validity', title: '有效期'}
                        , {field: 'vendorName', title: '供应商名称'}
                    ]]
                });
            },

            queryPageView() {
                table.reload('tableGridLableManage', {
                    where: vm.queryParams
                });
            },
            restQueryParams() {
                vm.queryParams = {
                    poCode: "",
                    materialCode: ""
                }
                table.reload('tableGridLableManage', {
                    where: vm.queryParams
                });
            }
        },
        created() {
        },
        mounted() {
            this.initTableIns();
        }
    })

    table.on('toolbar(tableGridFilterCheck)', function (obj) {
        const checkStatusData = table.checkStatus(obj.config.id).data

        switch (obj.event) {
            case 'addLableManage':
                layer.open({
                    title: '新增'
                    , type: 2
                    , area: ['1000px', '600px'] //宽高
                    , maxmin: true
                    , content: context_path + '/ycl/lableManage/toAdd'
                });

                break;
        }
    });

    //监听select
    form.on('select(fromStoreCodeList)', function (data) {
        console.log(data.elem); //得到select原始DOM对象
        console.log(data.value); //得到被选中的值
        console.log(data.othis); //得到美化后的DOM对象
    });
    $("#lablePrint").click(function(){
        var id = table.checkStatus('tableGridLableManage').data[0].id;

        $.ajax({
            type: 'POST',
            url: context_path + "/ycl/lableManage/toPrint",
            async: false,
            dataType: "json",
            data: {
                id: id
            },
            success: function (res) {
                console.log(res)
            }
        })


    });

    function tableRender(){
        table.reload('tableGridLableManage', {
            where: vm.queryParams
        });
    }
</script>