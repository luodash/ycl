<%--其他出入库增加--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<head>
    <link rel="stylesheet" href="${APP_PATH}/static/layui/css/layui.css" type="text/css"/>
    <%--行编辑样式--%>
    <%--<link rel="stylesheet" href="layui/css/layui.css?v=201805080202" />--%>
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
        /*您可以将下列样式写入自己的样式表中*/
        .childBody {
            padding: 15px;
        }

        /*layui 元素样式改写*/
        .layui-btn-sm {
            line-height: normal;
            font-size: 12.5px;
        }

        .layui-table-view .layui-table-body {
            min-height: 256px;
        }

        .layui-table-cell .layui-input.layui-unselect {
            height: 30px;
            line-height: 30px;
        }

        /*设置 layui 表格中单元格内容溢出可见样式*/
        .table-overlay .layui-table-view,
        .table-overlay .layui-table-box,
        .table-overlay .layui-table-body {
            overflow: visible;
        }

        .table-overlay .layui-table-cell {
            height: auto;
            overflow: visible;
        }

        /*文本对齐方式*/
        .text-center {
            text-align: center;
        }

    </style>
</head>

<script src="${APP_PATH}/static/layui/layui.all.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

<div id="lableManage_add">
    <blockquote class="layui-elem-quote">
        <form class="layui-form">
            <div class="layui-row layui-col-space10">
                <div class="layui-col-sm6">
                    <div class="layui-inline">
                        <label style="width: 65px">采购订单号</label>
                        <div class="layui-input-inline">
                            <input id="poCode" type="text" autocomplete="off"  class="layui-input" value="${data.poCode}">
                        </div>
                    </div>
                </div>
                <div class="layui-col-sm6">
                    <div class="layui-inline">
                        <label style="width: 65px">物料编码</label>
                        <div class="layui-input-inline">
                            <input id="materialCode" type="text" autocomplete="off"  class="layui-input" value="${data.materialCode}">
                        </div>
                    </div>
                </div>
                <div class="layui-col-sm6">
                    <div class="layui-inline">
                        <label style="width: 65px">供应商批次</label>
                        <div class="layui-input-inline">
                            <input id="vendorLotsNum" type="text" autocomplete="off"  class="layui-input" value="${data.vendorLotsNum}">
                        </div>
                    </div>
                </div>
                <div class="layui-col-sm6">
                    <div class="layui-inline">
                        <label style="width: 65px">生产日期</label>
                        <div class="layui-input-inline">
                            <input id="productionDate" type="text" autocomplete="off"  class="layui-input" value="${data.productionDate}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-row layui-col-space10">
                <div class="layui-col-sm6">
                    <div class="layui-inline">
                        <label style="width: 65px">供应商名称</label>
                        <div class="layui-input-inline">
                            <input id="vendorName" type="text" autocomplete="off"  class="layui-input" value="${data.vendorName}">
                        </div>
                    </div>
                </div>
                <div class="layui-col-sm6">
                    <div class="layui-inline">
                        <label style="width: 65px">标签类型</label>
                        <div class="layui-input-inline" style="width: 187px">
                            <select name="type" lay-filter="type" lay-search>
                                <option value="L">批次码</option>
                                <option value="S">流水码</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="layui-col-sm6">
                    <div class="layui-inline">
                        <label style="width: 65px">物料有效期</label>
                        <div class="layui-input-inline">
                            <input id="validity" type="text" autocomplete="off"  class="layui-input" value="${data.validity}">
                        </div>
                    </div>
                </div>
                <div class="layui-col-sm6">
                    <div class="layui-inline layui-hide" id="div_serialNumber">
                        <label style="width: 65px;display: inline-block">流水号</label>
                        <div class="layui-input-inline" style="width: 83px">
                            <input id="serialNumberStart" type="text" autocomplete="off" class="layui-input" value="${data.serialNumberStart}"
                                   onKeyUp="this.value=this.value.replace(/[^\.\d]/g,'');this.value=this.value.replace('.','');">
                        </div>至
                        <div class="layui-input-inline" style="width: 83px">
                            <input id="serialNumberEnd" type="text" autocomplete="off" class="layui-input" value="${data.serialNumberEnd}"
                                   onKeyUp="this.value=this.value.replace(/[^\.\d]/g,'');this.value=this.value.replace('.','');">
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-row layui-col-space10">
                <div class="layui-col-sm6">
                </div>
                <div class="layui-col-sm6">
                    <%--查询按钮--%>
                    <div class="layui-input-block">
                        <button type="submit" class="layui-btn" lay-submit lay-filter="submitBtn">提交</button>
                        <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        <button type="button" lay-event="printLable" class="layui-btn layui-btn-sm layui-btn-normal">
                            <i class="layui-icon">&#xe66d;</i>打印标签
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </blockquote>
</div>




<%--引入表格样式--%>
<%--<script src="layui/layui.js?v=201805080202" charset="utf-8"></script>--%>

<script>
    const context_path = '${APP_PATH}';
    let entity={"id":'${data.id}',"poCode":'${data.id}',"materialCode":'${data.materialCode}',
        "vendorLotsNum":'${data.vendorLotsNum}',"productionDate":'${data.productionDate}',"validity":'${data.validity}',
        "vendorName":'${data.vendorName}',"type":'${data.type}',"serialNumber":'${data.serialNumber}'};

    const table = layui.table,
        form = layui.form,
        laydate = layui.laydate,
        $ = layui.jquery;
</script>

<script type="text/javascript">

        //监听select下拉选中事件
        form.on('select(type)', function (data) {
            var elem = data.elem; //得到select原始DOM对象
            $(elem).prev("a[lay-event='type']").trigger("click");
            if(elem.value=='L'){//批次号
                $('#div_serialNumber').addClass('layui-hide');
            }
            if(elem.value=='S'){
                $('#div_serialNumber').removeClass('layui-hide');
            }
        });


        //监听提交
        form.on('submit(submitBtn)', function (data) {
            /*增加*/
            $.ajax({
                type: 'POST',
                url: context_path + "/ycl/lableManage/save",
                async: false,
                dataType: "json",
                data: {
                    entity: entity
                },
                success: function (res) {
                    console.log(res)
                }
            })
            var index=parent.layer.getFrameIndex(window.name);
            parent.layer.close(index);
        });

        let vm = new Vue({
            el: '#lableManage_add',
            data: {
                poCode: '',
                materialCode: '',
                vendorLotsNum: '',
                productionDate: '',
                validity: '',
                vendorName: '',
                type: '',
                serialNumber: ''
            },
            methods: {
                printLable(){

                }
            },
            mounted() {
                form.render();
                form.render('select');
            }
        });

        // init
        ;!function () {
            //日期区间选择器
            laydate.render({
                elem: '#productionDate'
            });
        }();

</script>
