<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<head><%--销售出库--%>
    <link rel="stylesheet" href="${APP_PATH}/static/layui/css/layui.css" type="text/css"/>
    <style type="text/css">
        td .layui-form-select{
            margin-top: -5px;
        }
        /*设置 layui 表格中单元格内容溢出可见样式*/
        #divtable .layui-table-view,
        #divtable .layui-table-box,
        #divtable .layui-table-body{
            overflow: visible;
            padding: 0 0 0 0;
            margin: 0 0 0 0; /* 根据实际情况调整 */
        }
        #divtable .layui-table-cell{height: auto; overflow: visible;}
    </style>
</head>
<div id="yclMaterialSaleOut" style="margin: 30px">
    <%--query tools--%>
        <blockquote class="layui-elem-quote">
            <form class="layui-form" id="materialBackForm">
                <div class="layui-fluid">
                    <div class="layui-row">
                        <div class="layui-col-sm3">
                            <div class="layui-inline">
                                <label style="width: 65px">公司</label>
                                <div class="layui-input-inline">
                                    <select id="materialAreaCodeSelect" lay-filter="materialAreaCodeSelect">
                                        <option value="">请选择</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="layui-col-sm3">
                            <div class="layui-inline">
                                <label style="width: 65px">订单编号</label>
                                <div class="layui-input-inline">
                                    <input type="text" class="layui-input" v-model="queryParams.ordernumber" />
                                </div>
                            </div>
                        </div>
                        <div class="layui-col-sm3">
                            <div class="layui-inline">
                                <label style="width: 65px">物料编码</label>
                                <div class="layui-input-inline">
                                    <input type="text" class="layui-input" v-model="queryParams.itemcode" />
                                </div>
                            </div>
                        </div>
                        <div class="layui-col-sm3">
                            <div class="layui-inline">
                                <button type="button" class="layui-btn layui-btn-sm layui-btn-normal"
                                        @click="queryPageView">
                                    <i class="layui-icon">&#xe615;</i>查询
                                </button>
                            </div>
                            <div class="layui-inline">
                                <button type="button" class="layui-btn layui-btn-primary layui-btn-sm"
                                        @click="restQueryParams">
                                    <i class="layui-icon">&#xe669;</i>重置
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="layui-row">
                        <div class="layui-col-sm3">
                            <div class="layui-inline">
                                <label style="width: 65px">日期</label>
                                <div class="layui-input-inline">
                                    <input type="text" class="layui-input" id="materialApplyTime">
                                </div>
                            </div>
                        </div>
                        <div class="layui-col-sm3">
                            <div class="layui-inline">
                                <label style="width: 65px">出库状态</label>
                                <div class="layui-input-inline">
                                    <select id="outstats" lay-filter="outstats" v-model="queryParams.outstats">
                                        <option value="0" selected="selected">未出库</option>
                                        <option value="1">已出库</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </blockquote>
        <table id="tableGridOutsourcing" lay-filter="tableGridFilterOutsourcing"></table>
        <blockquote class="layui-elem-quote" id="searchForm2">
            <form class="layui-form" id="storeForm">
                <div  style="width: 100%">
                    <form class="layui-form" id="form2">
                        <label style="width: 30px">仓库</label>
                        <div class="layui-input-inline">
                            <select id="materialStoreCodeSelect" lay-filter="materialStoreCodeSelect">
                                <option value="" >请选择</option>
                            </select>
                        </div>
                    </form>
                </div>
            </form>
        </blockquote>
        
    <%-- table grid --%>
    <div id="detail" style="display: none">
        <div class="layui-btn-container" id="pick_btn" style="width: 100%">
            <button type="button" class="layui-btn layui-btn-sm layui-btn-normal" @click="addIn">
                <i class="layui-icon">&#xe654;</i>增行
            </button>
            <button type="button" class="layui-btn layui-btn-sm layui-btn-normal" @click="save">
                <i class="layui-icon">&#xe605;</i>出库
            </button>
            <button type="button" class="layui-btn layui-btn-sm layui-btn-danger" @click="deleteLine">
                <i class="layui-icon">&#xe640;</i>清除
            </button>
        </div>
        <div id="divtable" style="margin-top: 10px;">
            <table id="tableGridOutsourcing3" lay-filter="tableGridFilterOutsourcing3"></table>
        </div>
    </div>
    <%-- table grid toolbar --%>
    <script type="text/html" id="tableGridToolbarOutsourcing">
        <div class="layui-btn-container">
            <label class="layui-word-aux">原材料销售订单</label>
        </div>
    </script>
    <script type="text/html" id="tableGridToolbarOutsourcing3">
        <div class="layui-btn-container">
            <label class="layui-word-aux">出库单</label>
        </div>
    </script>
</div>


<script>

    const context_path = '${APP_PATH}';
    const table = layui.table,
        form = layui.form,
        layer = layui.layer,
        laydate = layui.laydate;
    let tableGridIns;
    let tableGridIns3;

    //获取table的list集合
    let vm = new Vue({
        el: '#yclMaterialSaleOut',
        data: {
            tableList: [],
            areaList: [], // 厂区
            warhouseList: [],
            locatorList: [],
            currentOrderNumber:"",
            currentStore:"",
            currentStoreName:"",
            currentOrderEntityId:"",
            currentMateriCode:"",
            total_quantity:0,
            queryParams: { //查询参数
                areaCode: "",
                itemcode: "",
                ordernumber: "",
                applyTimeStart: "",
                applyTimeEnd: "",
                outstats: "0"
            }
        },
        methods: {
            getDateStr(dayCount) {
                let dd = new Date();
                dd.setDate(dd.getDate() + dayCount); // 获取dayCount天后的日期
                let y = dd.getFullYear();
                let m = dd.getMonth() + 1; // 获取当前月份的日期
                if (m < 10) {
                    m = '0' + m;
                }
                let d = dd.getDate();
                if (d < 10) {
                    d = '0' + d;
                }
                return y + "-" + m + "-" + d;
            },
            initTableIns() {
                // 订单物料
                tableGridIns = table.render({
                    elem: '#tableGridOutsourcing'
                    , url: context_path + '/ycl/sale/list'
                    , page: true
                    , toolbar: '#tableGridToolbarOutsourcing'
                    , id: "tableGridSetOutsourcing"
                    , size: 'sm'
                    , where: vm.queryParams
                    , defaultToolbar: []
                    , cols: [[
                        {type: 'radio'}
                        , {field: 'orderNumber', title: '订单编号', width: 120}
                        , {field: 'orderType', title: '订单类型', width: 90}
                        , {field: 'name', title: '客户', width: 180}
                        , {field: 'customerName', title: '收货地点', width: 200}
                        , {field: 'itemCode', title: '物料编号', width: 100}
                        , {field: 'itemDesc', title: '物料名称', width: 150}
                        , {field: 'orderedQuantity', title: '订单数量', width: 90}
                        , {field: 'uomCode', title: '单位', width: 65}
                        , {field: 'stage', title: '临时提货区', width: 125}
                        , {field: 'creationDate', title: '创建日期', width: 150}
                        , {field: 'organizationName', title: '发货方', width: 225}
                        , {field: 'orgId', hide: true}
                        , {field: 'factorycode', hide: true}
                        , {field: 'headerId', hide: true}
                    ]]
                });
            },
            //第三栏
            initTableIns3() {
                tableGridIns3 = table.render({
                    elem: '#tableGridOutsourcing3'
                    //, url: context_path + '/ycl/production/picking/outlist'
                    , toolbar: '#tableGridToolbarOutsourcing3'
                    , id: "tableGridSetOutsourcing3"
                    , defaultToolbar: []
                    , limit: 99999
                    , data: []
                    , cols: [[
                        {type: 'radio'}
                        , {field: 'tempId', hide: true}
                        , {
                                field: 'itemCode', title: '物料编号', width: 145, templet: function (d) {
                                    if (d.lotsNumList.length === 0) {
                                        return '<span style="color:white;background-color:red;padding:5px;">' + d.itemCode + '</span>'
                                    } else {
                                        return '<span >' + d.itemCode + '</span>'
                                    }
                                }
                        }
                        ,{
                            field: 'lotsNum',
                            title: '批次号',
                            width: 200,
                            templet: (d) => vm.renderLotsNumSelectOptions(d)
                        }
                        , { field: 'locatorCode', title: '库位', width: 120 }
                        , { field: 'storeQuantity', title: '库存数量', width: 100 }
                        , {field: 'orderedQuantity', title: '订单数量', width: 80}
                        // , {field: 'storeQuantity', title: '库存数量', width: 80}
                        , {field: 'outQuantity', title: '出库数量', width: 80, edit: 'text', templet: (d) => d.outQuantity || ''}
                        , {field: 'stage', title: '临时提货区', width: 165}
                        , {field: 'organizationName', title: '发货方'}
                    ]]
                });
            },
            resetSelect(data, settings) {
                let html = '';
                let list = data;
                for (let i = 0; i < list.length; i++) {
                    html += '<option value=' + list[i].id + '>' + list[i].text + '</option>';
                }
                let $id = $('#' + settings);
                $id.empty();
                $id.append('<option value="">请选择</option>');
                $id.append(html);
                form.render('select');
            },
            clearSelect(data, settings) {
                let html = '';
                let list = data;
                let $id = $('#' + settings);
                $id.empty();
                $id.append('<option value="">请选择</option>');
                form.render('select');
            },
            renderSelectOptions: function(e){
                var html = [];
                html.push('<div class="layui-form" lay-filter="storeCode">');
                html.push('<select name="storeCode" id="storeCode" data-value="'+e.tempId+'" lay-filter="storeCode" lay-search="">');
                html.push('<option value="">请选择</option>');
                this.locatorList.forEach(m=>{
                    html.push('<option value="');
                    html.push(m.id);
                    html.push('"');
                    if(m.id == e.storeCode ){
                        html.push(' selected="selected"');
                    }
                    html.push('>');
                    html.push(m.text);
                    html.push('</option>');
                })
                html.push('</select></div>');
                return html.join('');
            },
            renderLotsNumSelectOptions: function (e) {
                    let list = e.lotsNumList;
                    var html = [];
                    html.push('<div class="layui-form" lay-filter="lotsNum">');
                    html.push('<select name="lotsNum" id="lotsNum" data-value="' + e.tempId + '" lay-filter="lotsNum" lay-search>');
                    html.push('<option value="">请选择</option>');
                    console.info(e)
                    if (list) {
                        list.forEach(m => {
                            html.push('<option value="');
                        html.push(m.CODE);
                        html.push('"');
                        html.push(' quantity="');
                        html.push(m.QUALITY);
                        html.push('"');
                        html.push(' locatorCode="');
                        html.push(m.LOCATOR_CODE);
                        html.push('"');
                        if (m.CODE == e.lotsNum) {
                            html.push(' selected="selected"');
                        }
                        html.push('>');
                        html.push(m.TEXT);
                        html.push('</option>');
                    })
                    }
                    html.push('</select></div>');
                    return html.join('');
            },
            initApplyTime() {
                //日期时间选择器
                laydate.render({
                    elem: '#materialApplyTime'
                    , type: 'date'
                    , range: '~'
                    , btns: ['confirm']
                    , value: vm.queryParams.applyTimeStart + ' ~ ' + vm.queryParams.applyTimeEnd
                    , done: function (value, date, endDate) {
                        let dateTime = value.split(' ~ ');
                        vm.queryParams.applyTimeStart = dateTime[0];
                        vm.queryParams.applyTimeEnd = dateTime[1];
                    }
                });
            },
            initSelect(async, url, data, domId, callback) {
                let dataJson = { queryString: "", pageSize: 15, pageNo: 1 };
                let json = $.extend({}, dataJson, data);
                $.ajax({
                    type: 'GET',
                    url: context_path + url,
                    data: json,
                    async: async,
                    dataType: "json",
                    success: function (res) {
                        let html = '';
                        let list = res.data;
                        callback(list);
                        for (let i = 0; i < list.length; i++) {
                            html += '<option value=' + list[i].id + '>' + list[i].text + '</option>';
                        }
                        $("#" + domId).append(html);
                        form.render('select');
                    }
                });
            },
            initSelect1(async, url, data, callback) {
                let dataJson = { queryString: "", pageSize: 15, pageNo: 1 };
                let json = $.extend({}, dataJson, data);
                $.ajax({
                    type: 'POST',
                    url: context_path + url,
                    data: json,
                    async: async,
                    dataType: "json",
                    success: function (res) {
                        let html = '';
                        let list = res.data;
                        callback(list);
                    }
                });
            },
            initSelect2(async, url, data, domId, callback) {
                let dataJson = { queryString: "", pageSize: 15, pageNo: 1 };
                let json = $.extend({}, dataJson, data);
                $.ajax({
                    type: 'GET',
                    url: context_path + url,
                    data: json,
                    async: async,
                    dataType: "json",
                    success: function (res) {
                        let html = '';
                        let list = res.data;
                        console.log("下拉框：" + list.length);
                        callback(list);
                        for (let i = 0; i < list.length; i++) {
                            html += '<option value=' + list[i].id  +'>' + list[i].text +
                                '</option>';
                        }
                        $("#" + domId).append(html);
                        form.render('select');
                    }
                });
            },
            initAreaList() {
                this.initSelect(true, '/ycl/baseinfo/basedata/area/0', {}, 'materialAreaCodeSelect', (list) => vm.areaList = list);
            },
            initStoreList(id) {
                this.initSelect2(false, '/warehouselist/getYclWarehouseByEntityId/?entityId='+id, {},
                    'materialStoreCodeSelect', (list) => vm.warhouseList = list);
            },
            // initStoreCodeList() {
            //     this.initSelect1(false, '/warehouselist/getYclWarehouseByFactory/?factoryCode=92', {}, (list) => vm.locatorList = list);
            // },
            queryPageView() {
                console.log(vm.queryParams)
                table.reload('tableGridSetOutsourcing', {
                    where: vm.queryParams
                });
            },
            addIn(){
                const checkStatusData = layui.table.checkStatus('tableGridSetOutsourcing3').data;
                let oldData = layui.table.cache['tableGridSetOutsourcing3'];
                console.log(oldData);
                if(checkStatusData.length === 0) {
                    layer.msg('请先选择一条出库单', {icon: 2});
                    return
                }

                let newData = [];
                oldData.forEach(m=>{
                    newData.push(m);
                    console.log(m);
                if(m.tempId === checkStatusData[0].tempId){
                    let newRow = {}
                    // var newRow = m;
                    newRow.creationDate = m.creationDate;
                    newRow.customerName = m.customerName;
                    newRow.factoryCode = m.factoryCode;
                    newRow.headerId = m.headerId;
                    newRow.itemCode = m.itemCode;
                    newRow.itemDesc = m.itemDesc;
                    newRow.lineId = m.lineId;
                    newRow.name = m.name;
                    newRow.orderNumber = m.orderNumber;
                    newRow.lotsNumList = m.lotsNumList;
                    newRow.orderType = m.orderType;
                    newRow.orderedQuantity = m.orderedQuantity;
                    newRow.orgId = m.orgId;
                    newRow.organizationName = m.organizationName;
                    newRow.stage = m.stage;
                    newRow.tempId = new Date().valueOf();
                    newData.push(newRow);
                }
            })
                tableGridIns3.reload({
                    data : newData
                });
                
            },
            save(){
                const checkStatusData = layui.table.checkStatus('tableGridSetOutsourcing3').data;
                let oldData = layui.table.cache['tableGridSetOutsourcing3'];
                console.log(oldData);
                if(vm.currentStore === ""  && oldData.length !== 0){
                    layer.msg('请选择要出库的仓库', {icon: 2});
                    return;
                }
                if(oldData.length === 0 && vm.currentStore !== ""){
                    layer.msg('请先添加出库单信息', {icon: 2});
                    return;
                }
                if(oldData.length === 0 && vm.currentStore === ""){
                    layer.msg('请选择仓库，并添加出库单信息', {icon: 2});
                    return;
                }
                let total_quantity = vm.total_quantity;
                console.log(vm.total_quantity);

                var add_quantity = 0;

                for(var i = 0; i < oldData.length; i++){
                    if(!oldData[i].outQuantity){
                        layer.msg("请填写第" + (i + 1) + "行的出库数量", {icon: 2});
                        return;
                    }
                    if(!oldData[i].lotsNum){
                        layer.msg("请选择第" + (i + 1) + "行的批次号", {icon: 2});
                        return;
                    }
                    if(oldData[i].storeQuantity < oldData[i].outQuantity){
                        layer.msg("第" + (i + 1) + "行的出库数量不能大于库存数量", {icon: 2});
                        return;
                    }
                    oldData[i].storeCode = vm.currentStore;
                    var outQuantity = oldData[i].outQuantity == ''?0:oldData[i].outQuantity;
                    add_quantity += outQuantity*1;
                }
                //出库数量等于订单数量
                let tempData = [];
                oldData.forEach(m=>{
                    console.log(m)
                    let tmp = tempData.find(x=>x.itemCode==m.itemCode);
                if (tmp!=undefined){
                    tmp.outQuantity=Number(tmp.outQuantity)+Number(m.outQuantity)
                }else{
                    let newRow = {}
                    newRow.itemCode=m.itemCode
                    newRow.orderedQuantity=m.orderedQuantity
                    newRow.outQuantity=m.outQuantity
                    tempData.push(newRow)
                }
            })
                console.log(tempData)
                if (tempData.some(m=> m.orderedQuantity!=m.outQuantity))
                {
                    layer.alert('出库数量必须等于订单数量!');
                    return;
                }
                
                
                var loading = layer.load(2,{shade: 0.2});
                $.ajax({
                    type: 'POST',
                    // url: context_path + "/ycl/production/picking/yclOrderOut",
                    url: context_path + "/ycl/sale/saveSale",
                    contentType: "application/json",
                    async: false,
                    dataType: "json",
                    data: JSON.stringify(oldData),
                    success: function (res) {
                        layer.close(loading);
                        if (res.code == 0) {
                            layer.msg(res.msg, { icon: 1 });
                            window.parent.tableRender();
                            let index = parent.layer.getFrameIndex(window.name);
                            parent.layer.close(index);
                            let data= [];
                            tableGridIns3.reload({
                                // console.log(oldData);
                                data: data
                            });
                            tableGridIns.reload();
                        } else {
                            layer.msg(res.msg, { icon: 2 });
                        }
                    },
                    error:function(res){
                        layer.close(loading);
                    }
                })

            },
            deleteLine(){
                const checkStatusData = layui.table.checkStatus('tableGridSetOutsourcing3').data;
                if(checkStatusData.length === 0) {
                    layer.msg('请先选择一条出库单', {icon: 2});
                    return
                }
                let oldData = layui.table.cache['tableGridSetOutsourcing3'];
                console.log(checkStatusData);
                console.log(oldData);
                for (let data of checkStatusData) {
                    for (let i = 0; i < oldData.length; i++) {
                        if (oldData[i].tempId === data.tempId) {
                            oldData.splice(i, 1);
                            break;
                        }
                    }
                }
                tableGridIns3.reload({
                    url: '',
                    data: oldData
                })
            },
            restQueryParams() {
                vm.queryParams = {
                    areaCode: "",
                    itemcode: "",
                    ordernumber: "",
                    outstats:"0",
                    applyTimeStart: vm.getDateStr(-7),
                    applyTimeEnd: vm.getDateStr(0),
                };
                // $("#outstats").get(0).selectedIndex;
                // $('#outstats').val("0");
                form.render('select','outstats');
                this.resetSelect(vm.areaList, 'materialAreaCodeSelect');
                vm.warhouseList = [];
                this.resetSelect(vm.warhouseList, 'materialStoreCodeSelect');
                this.initApplyTime();
                this.queryPageView();
                this.initTableIns();
                $("#searchForm2").show();
                $("#detail").hide();
            }
        },
        created() {

        },
        mounted() {

        }

    })

    table.on('toolbar(tableGridFilterOutsourcing3)', function (obj) {
        const checkStatusData = layui.table.checkStatus(obj.config.id).data
        let oldData =  layui.table.cache['tableGridSetOutsourcing3'];
        switch (obj.event) {
            case "storeCode":
                console.log(1)
                let select = tr.find("select[name='storeCode']");
                if (select) {
                    let selectedVal = select.val();
                    if (!selectedVal) {
                        layer.tips("请选择一个库位", select.next('.layui-form-select'), { tips: [3, '#FF5722'] }); //吸附提示
                    }
                    obj.update({ 'storeCode': selectedVal });
                }
                break;
        }
    });

    // 监听编辑单元格
    table.on('edit(tableGridFilterOutsourcing3)', function (obj) { //注：edit是固定事件名，test是table原始容器的属性 lay-filter="对应的值"
        let value = obj.value;
        let field1 = obj.field;
        if (field1 == "outQuantity") {
            value = parseFloat(value);
            if (isNaN(value)) {
                value = 0;
            }
        }
        obj.update({ [field1]: value });
    });

    // 点击订单物料
    table.on('row(tableGridFilterOutsourcing)', function (obj) {
        obj.tr.find('i[class="layui-anim layui-icon"]').trigger("click");
        vm.currentStore = "";
        vm.currentStoreName = "";
        const data = obj.data;
        vm.currentOrderEntityId = data.orgId;
        vm.total_quantity = data.orderedQuantity;
        vm.currentMateriCode = data.itemCode;
        vm.currentOrderNumber = data.ordernumber;
        $("#pick_btn").hide();
        tableGridIns3.reload({
                        data: []
                    })
        vm.clearSelect(vm.warhouseList, 'materialStoreCodeSelect');
        vm.initStoreList(data.orgId);
        obj.tr.addClass('layui-table-click').siblings().removeClass('layui-table-click');
        //如果outStatus是已出库，查询yck_sales_line表
        if(vm.queryParams.outstats === 1){
            $.ajax({
                type: 'GET',
                url: context_path + '/ycl/sale/getSaleLineByOutCode',
                data: "XSCK"+obj.orderNumber,
                dataType: "json",
                async: false,
                success: function (res) {
                    console.log(res);
                }
                    // tableGridIns3.reload({
                    //     // console.log(oldData);
                    //     data: resultList
                    // })
        }
       
        //标注选中样式
    });

    table.on('row(tableGridFilterOutsourcing3)', function (obj) {
        // obj.tr.find('i[class="layui-anim layui-icon"]').trigger("click");
    });

    //监听select
    form.on('select(materialAreaCodeSelect)', function (data) {
        vm.queryParams.areaCode = data.value;
    });
    //监听select仓库
    form.on('select(materialStoreCodeSelect)', function (data) {
        vm.currentStore = data.value;
        vm.currentStoreName = $("#materialStoreCodeSelect").find("option:selected").text();
        console.log($("#materialStoreCodeSelect").find("option:selected").text());
        const checkedOrderData = layui.table.checkStatus('tableGridSetOutsourcing').data;
        if(vm.currentStore !== ""){
            $.ajax({
                type: 'GET',
                url: context_path + '/ycl/sale/list',
                data: {"ordernumber":checkedOrderData[0].orderNumber,"page":"1","limit":"999"},
                dataType: "json",
                async: false,
                success: function (res) {
                    const resultList = []
                    const data = res.data;
                    for (let i = 0; i < data.length; i++) {
                        data[i].tempId = new Date().valueOf() + i;
                        let mtrlcode = data[i].itemCode;
                        //获取每一行的批次号
                        $.ajax({
                            type: 'GET',
                            url: context_path + '/wms2/baseinfo/inventory/lotsnum/' + vm.currentOrderEntityId + '/' +
                                vm.currentStoreName.split("_")[0] +
                                '/' + mtrlcode,
                            async: false,
                            dataType: "json",
                            success: function (re) {
                                data[i].lotsNumList = re.data;
                                console.log(data[i].lotsNumList);
                                }
                        });
                        resultList.push(data[i]);
                    }
                    tableGridIns3.reload({
                        // console.log(oldData);
                        data: resultList
                    })
                    $("#detail").show();
                    $("#pick_btn").show();
                }
            });
        }
        
    });
    
    //监听select
    form.on('select(outstats)', function (data) {
        vm.queryParams.outstats = data.value;
        table.reload('tableGridSetOutsourcing', {
            where: vm.queryParams
        });
        tableGridIns3.reload({
            url: '',
            data: []
        })
        vm.warhouseList = [];
        vm.resetSelect(vm.warhouseList, 'materialStoreCodeSelect');
        if(data.value == 0){
            $("#pick_btn").show();
            $("#searchForm2").show();
        } else {
            $("#pick_btn").hide();
            $("#searchForm2").hide();
        }
    });
    
    //监听select
    form.on('select(lotsNum)', function (data) {
        var elem = data.othis.parents('tr');
        var id = elem.first().find('td').eq(1).text();
        console.log(id)
        let val = data.value
        const  outData = layui.table.cache['tableGridSetOutsourcing3'];

        outData.forEach(n => {
            if (n.tempId == id) {
             const lotsInfo = data.value.split(",");
            n.lotsNum = data.value;
            n.locatorCode = lotsInfo[1];
            n.storeQuantity = lotsInfo[2];
        }
     })
        tableGridIns3.reload({
            data: this.outData
        });
    });
    // init
    ;!function () {
        vm.queryParams.applyTimeStart = vm.getDateStr(-7);
        vm.queryParams.applyTimeEnd = vm.getDateStr(0);
        vm.initAreaList();
        // vm.initStoreCodeList();
        vm.initApplyTime();
        vm.initTableIns();
        vm.initTableIns3();
    }();
</script>
