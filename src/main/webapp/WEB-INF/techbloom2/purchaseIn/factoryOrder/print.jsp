<%--
  Created by IntelliJ IDEA.
  User: huzg
  Date: 2020/6/25
  Time: 17:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <style>
        th, td
        {
            padding: 6px;
        }
        tr
        {
            height: 30px;
        }
        .info td{
            border-right: 1px solid #000000;
            border-bottom: 1px solid #000000;
        }
        .head td{
            border-top: 1px solid #000000;
            border-bottom: 1px solid #000000;
        }
    </style>
    <script src="/wms/plugins/public_components/js/jquery-2.1.4.js"></script>
    <script src="https://cdn.bootcss.com/jsbarcode/3.8.0/JsBarcode.all.min.js"></script>
</head>
<body>

<div style="padding: 10px;width: 98%">
<div id="yclFactoryPrint">
<div id="div1">
    <div style="width: 100%;text-align: center;font-size: 20px;font-weight: bold;padding-top: 15px;">
        采供中心物资出厂流转单
    </div>
    <br>
    <table style="width: 100%;"><tr><td style="width: 45%;">制单人：{{obj.creator}}</td><td>打印时间：{{ymd}}</td></tr></table>
    <table class="info" style="width: 100%; border-collapse:collapse;border-left: 1px solid #000000;border-spacing:0px;font-size: 14px;">
        <tbody>
        <tr class="head">
            <td>货物类型</td>
            <td>{{obj.goodType}}</td>
            <td>单据类型</td>
            <td width="100">{{obj.billType}}</td>
            <td rowspan="2">申请条码</td>
            <td rowspan="2" style="width: 300px;"><svg id="svgcode"></svg> </td>
        </tr>
        <tr>
            <td>申请人</td>
            <td>{{obj.applyUser}}</td>
            <td>申请人号码</td>
            <td>{{obj.applyTel}}</td>
        </tr>
        <tr>
            <td>公司</td>
            <td colspan="3">{{obj.company}}</td>
            <td>申请单号</td>
            <td>{{obj.outCode}}</td>
        </tr>
        <tr>
            <td>驾驶员号码</td>
            <td>{{obj.driver}}</td>
            <td>派车单号</td>
            <td>{{obj.sendNum}}</td>
            <td>申请日期</td>
            <td>{{obj.applyDate}}</td>
        </tr>
        <tr>
            <td>发出单位</td>
            <td>{{obj.sendout}}</td>
            <td>接收单位</td>
            <td>{{obj.receiver}}</td>
            <td>运输方</td>
            <td>{{obj.transport}}</td>
        </tr>
        <tr>
            <td>车牌号</td>
            <td>{{obj.autoNum}}</td>
            <td>头备注</td>
            <td colspan="3">{{obj.remark}}</td>
        </tr>
        </tbody>
    </table>
</div>
<div id="div2">
    <br>
    <table class="info" style="width:100%;border-collapse:collapse;border-left: 1px solid #000000;border-spacing:0px;font-size: 14px;">
    <tbody>
    <tr class="head">
        <td width="30">序号</td>
        <td width="80">物料编码</td>
        <td width="150">物料描述</td>
        <td width="50">数量</td>
        <td width="40">单位</td>
        <td width="100">导入来源</td>
        <td>行备注</td>
    </tr>
    <tr v-for="(en,index) in obj.line">
        <td>{{index+1}}</td>
        <td>{{en.materialCode}}</td>
        <td>{{en.materialName}}</td>
        <td>{{en.quality}}</td>
        <td>{{en.unit}}</td>
        <td>{{en.orderCode}}</td>
        <td>{{en.comments}}</td>
    </tr>
    </tbody>
</table>
    <table style="width: 100%;"><tr><td>仓库保管员：</td><td>提货人：</td><td>门卫：</td></tr></table>
</div>
</div>

</div>
<script src="/wms/static/layui/vue.js"></script>
<script>
    let options = {
        format: "CODE128",
        displayValue: false,
        height: 60
    };
    Date.prototype.Format = function (fmt) { //author: meizz
        var o = {
            "M+": this.getMonth() + 1, //月份
            "d+": this.getDate(), //日
            "H+": this.getHours(), //小时
            "m+": this.getMinutes(), //分
            "s+": this.getSeconds(), //秒
            "q+": Math.floor((this.getMonth() + 3) / 3), //季度
            "S": this.getMilliseconds() //毫秒
        };
        if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (var k in o)
            if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        return fmt;
    };
    let vm = new Vue({
        el: '#yclFactoryPrint',
        data: {
            obj:{},
            tranCode:'',
            purpose:'公司间调拨出库',
            ymd:'',
            tableList:[{}]
        },
        methods:{
            createPerson: function(){
                console.log(this.tableList)
                this.tableList.push({});
            }
        },
        created() {
            <%--this.obj = ${obj };--%>
            <%--console.log(this.obj)--%>
            this.ymd = new Date().Format("yyyy-MM-dd");
        },
        mounted(){
            var url = window.location.href;
            var dz_url = url.split('objStr=')
            if (dz_url.length>1)
            {
                this.obj = JSON.parse(decodeURI(dz_url[1]));
            }
        }
    });
    $("#svgcode").JsBarcode(vm.obj.outCode,options);
</script>
</body>
</html>
