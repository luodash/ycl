<%--
  Created by IntelliJ IDEA.
  User: 86176
  Date: 2020/6/7
  Time: 16:36
  To change this template use File | Settings | File Templates.
--%>
<%--出厂单--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<div id="factoryOrderListPage" style="margin: 15px">
    <%--query tools查询栏--%>
    <blockquote class="layui-elem-quote">
        <form class="layui-form">
            <div class="layui-fluid">
                <div class="layui-row layui-col-space10">
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 80px" class="layui-form-label">出厂单号</label>
                            <div class="layui-input-block">
                                <input type="text" class="layui-input" name="outCode" v-model="queryParams.outCode"
                                       lay-filter="outCode"/>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">车牌号码</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" name="autoNum" v-model="queryParams.autoNum"
                                       lay-filter="autoNum"/>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">接收单位</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" name="receiver"
                                       v-model="queryParams.receiver" lay-filter="receiver"/>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <button type="button" class="layui-btn layui-btn-sm layui-btn-normal"
                                    @click="queryPageView">
                                <i class="layui-icon">&#xe615;</i>查询
                            </button>
                        </div>
                        <div class="layui-inline">
                            <button type="button" class="layui-btn layui-btn-primary layui-btn-sm"
                                    @click="restQueryParams">
                                <i class="layui-icon">&#xe669;</i>重置
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </blockquote>
<%--按钮+数据集合列表--%>
    <div id="toolbarFactoryOrder">
        <label class="layui-word-aux">详情列表</label>
        <button type="button" @click="addFactoryOrder" class="layui-btn layui-btn-sm layui-btn-normal">
            <i class="layui-icon">&#xe654;</i>新增
        </button>
        <button type="button" @click="printOutFactory" class="layui-btn layui-btn-sm layui-btn-normal">
            <i class="layui-icon">&#xe66d;</i>打印
        </button>
    </div>
    <table id="tableGridFactoryOrder" lay-filter="tableGridFactoryOrder"></table>

    <iframe style="visibility:hidden;" id="myiframeFactory" name="myiframeFactory" src="/wms/ycl/purchasein/factory/toPrint" width=0 height=0> </iframe>
</div>

<script src="/wms/plugins/public_components/js/jquery-2.1.4.js"></script>
<script src="/wms/static/js/techbloom/LodopFuncs.js" charset="utf-8"></script>
<script>

    const context_path = '${APP_PATH}';
    const factoryOrderIds = '${factoryOrderIds}';
    const tableFactory = layui.table,
        layer = layui.layer,
        laydate = layui.laydate,
        form = layui.form;

    var LODOP; //声明为全局变量
    let vm = new Vue({
        el: '#factoryOrderListPage',
        data: {
            queryParams: {
                businessPurpose: "",
                businessType: "",
                inCode: "",
                materialCode: "",
                createUser: "",
                createTime: "",
                purpose: ""
            }
        },
        /*按钮点击事件*/
        methods: {
            /*查询*/
            queryPageView() {
                tableFactory.reload('tableGridSetFactoryOrder', {
                    where: vm.queryParams
                });
            },
            printOutFactory(){
                let selectData = layui.table.checkStatus('tableGridFactoryOrder').data;
                if (selectData.length != 1)
                {
                    layer.alert('请选中一条记录操作!');
                    return;
                }
                $.ajax({
                    type: 'GET',
                    url: "/wms/wms2/otherStorage/leave/selectLeaveEntity",
                    contentType: "application/json",
                    async: false,
                    data: {
                        id:selectData[0].id
                    },
                    success: res=> {
                        console.log(res)
                        let str = encodeURI(JSON.stringify(res))
                        let frm =document.getElementById('myiframeFactory');
                        frm.src='/wms/ycl/purchasein/factory/toPrint?objStr='+str;
                        $(frm).load(function(){
                            LODOP=getLodop();
                            let strHtml=frm.contentWindow.document.documentElement.innerHTML;
                            LODOP.ADD_PRINT_HTM(0,0,"100%","100%",strHtml);
                            LODOP.PREVIEW();
                        });
                    }
                })
            },
            /*重置*/
            restQueryParams() {
                vm.queryParams = {
                    businessPurpose: "",
                    businessType: "",
                    inCode: "",
                    materialCode: "",
                    createUser: "",
                    createTime: "",
                    purpose: ""
                }
                tableFactory.reload('tableGridSetFactoryOrder', {
                    where: vm.queryParams
                });
            },
            /*新增*/
            addFactoryOrder() {
                const checkStatusData = tableFactory.checkStatus('tableGridSetFactoryOrder').data
                let factoryOrderIds = checkStatusData.map(item => item.id).toString()
                layer.open({
                    title: '新增'
                    , type: 2
                    , area: ['1200px', '600px'] //宽高
                    , maxmin: true
                    ,
                    content: context_path + '/ycl/purchasein/factory/toEdit?otherStorageIds=' + factoryOrderIds
                });
            },
        },
        created() {

        },
        mounted() {
            //日期时间选择器
            laydate.render({
                elem: '#orderDate'
                , type: 'datetime'
                , done: function (value, date, endDate) {
                    vm.queryParams.orderDate = value
                }
            });

            // 列表展示的字段设置
            tableFactory.render({
                elem: '#tableGridFactoryOrder'
                , url: context_path + '/wms2/otherStorage/leave/list'
                , page: true
                , id: "tableGridFactoryOrder"
                , size: 'sm'
                , cols: [[
                    {type: 'radio'}
                    , {field: 'id', hide: true}
                    , {field: 'outCode', title: '出厂单号'}
                    , {field: 'goodType', title: '货物类型'}
                    , {field: 'billType', title: '单据类型'}
                    , {field: 'driver', title: '驾驶员'}
                    , {field: 'autoNum', title: '车牌号码'}
                    , {field: 'company', title: '公司'}
                    , {field: 'transport', title: '运输方'}
                    , {field: 'receiver', title: '接收单位'}
                    , {field: 'remark', title: '备注'}
                ]]
            });
        }
    })

    function tableRenderFactory(){
        tableFactory.reload('tableGridFactoryOrder', {
            where: vm.queryParams
        });
    }
    tableFactory.on('toolbar(tableGridFilter_factoryOrder)', function (obj) {
        const checkStatusData = tableFactory.checkStatus(obj.config.id).data
        let factoryOrderIds = checkStatusData.map(item => item.id).toString()

        //新增时无需勾选，订单信息也都是自己填写添加。当点击增加是不强制。
        switch (obj.event) {
            case 'addFactoryOrder':
                layer.open({
                    title: '新增'
                    , type: 2
                    , area: ['1200px', '600px'] //宽高
                    , maxmin: true
                    ,
                    content: context_path + '/ycl/purchasein/factory/toEdit?factoryOrderIds=' + factoryOrderIds
                });
                break;
            case 'addOther':
                layer.msg('sss');
                break;
            case 'delete':
                layer.msg('删除');
                break;
            case 'update':
                layer.msg('编辑');
                break;
        };
    });

    //监听select下拉选中事件
    form.on('select(businessPurpose)', function (data) {
        vm.queryParams.businessPurpose = data.value
    });

    // init
    ;!function () {
        //日期时间选择器
        laydate.render({
            elem: '#orderDate'
            , type: 'datetime'
            , done: function (value, date, endDate) {
                vm.queryParams.orderDate = value
            }
        });
    }();
</script>