<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<head>
    <link rel="stylesheet" href="${APP_PATH}/static/layui/css/layui.css" type="text/css"/>
    <link rel="stylesheet" href="${APP_PATH}/plugins/public_components/css/select2.css" />
    <style type="text/css">
        .layui-input {
            height: 30px;
        }
        /*layui 元素样式改写*/
        .layui-btn-sm{line-height: normal; font-size: 12.5px;}
        .layui-table-view .layui-table-body{min-height: 256px;}
        .layui-table-cell .layui-input.layui-unselect{height: 30px; line-height: 30px;}

        /*设置 layui 表格中单元格内容溢出可见样式*/
        .layui-table-view,
        .layui-table-box,
        .layui-table-body{overflow: visible;}
        .layui-table-cell{height: auto; overflow: visible;}

        /*文本对齐方式*/
        .text-center{text-align: center;}
        /* 使得下拉框与单元格刚好合适 */
        td .layui-form-select{
            margin-top: -5px;
            margin-left: -15px;
            margin-right: -15px;
        }
        .layui-anim-upbit{
            margin-top: -10px;
        }
    </style>
</head>
<script src="${APP_PATH}/static/layui/layui.all.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="${APP_PATH}/plugins/public_components/js/jquery-1.10.2.min.js"></script>
<script src="${APP_PATH}/plugins/public_components/js/select2.js"></script>
<div id="yclTransferOrderEdit">
    <form class="layui-form" lay-filter="transferorderForm">
        <blockquote class="layui-elem-quote">
            <form class="layui-form">
                <div class="layui-row layui-col-space10">
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">物料类型</label>
                            <div class="layui-input-inline">
                                <input type="text" autocomplete="off" class="layui-input"
                                       v-model="params.goodType">
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">单据类型</label>
                            <div class="layui-input-inline">
                                <input type="text" autocomplete="off" class="layui-input"
                                       v-model="params.billType">
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">驾驶员号码</label>
                            <div class="layui-input-inline">
                                <input type="text" autocomplete="off" class="layui-input" v-model="params.driver">
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">车牌号</label>
                            <div class="layui-input-inline">
                                <input type="text" autocomplete="off" class="layui-input" v-model="params.autoNum">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="layui-row layui-col-space10">
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">公司</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" v-model="params.company"/>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">运输方</label>
                            <div class="layui-input-inline">
                                <input type="text" autocomplete="off" class="layui-input"
                                       v-model="params.transport">
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">发出单位</label>
                            <div class="layui-input-inline">
                                <input type="text" autocomplete="off" class="layui-input"
                                       v-model="params.sendout">
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">接收单位</label>
                            <div class="layui-input-inline">
                                <input type="text" autocomplete="off" class="layui-input"
                                       v-model="params.receiver">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="layui-row layui-col-space10">
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">派车单号</label>
                            <div class="layui-input-inline">
                                <input autocomplete="off" class="layui-input" v-model="params.sendNum">
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">申请人</label>
                            <div class="layui-input-inline">
                                <input autocomplete="off" class="layui-input" v-model="params.applyUser">
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">申请人号码</label>
                            <div class="layui-input-inline">
                                <input autocomplete="off" class="layui-input" v-model="params.applyTel">
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">申请日期</label>
                            <div class="layui-input-inline">
                                <input autocomplete="off" id="applyDate" class="layui-input" v-model="params.applyDate">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="layui-row layui-col-space10">
                    <div class="layui-col-sm4">
                        <div class="layui-inline">
                            <label style="width: 65px">备注</label>
                            <div class="layui-input-inline">
                                <input autocomplete="off" style="width: 320px;" class="layui-input" v-model="params.remark">
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm4">
                        <%--查询按钮--%>
                        <div class="layui-input-block">
                            <button id="submitSave" type="button" @click="submitTransfer" class="layui-btn layui-btn-sm">
                                <i class="layui-icon">&#xe605;</i>打印
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </blockquote>
        <table id="tableGridCheckEdit"></table>
    </form>
    <iframe style="visibility:hidden;" id="myiframeFactory" name="myiframeFactory" src="/wms/ycl/purchasein/factory/toPrint" width=0 height=0> </iframe>
</div>
<script src="/wms/plugins/public_components/js/jquery-2.1.4.js"></script>
<script src="/wms/static/js/techbloom/LodopFuncs.js" charset="utf-8"></script>
<script>
    const context_path = '${APP_PATH}';
    const form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        laydate = layui.laydate,
        table = layui.table;
    var tableIns;
    var LODOP; //声明为全局变量
    let vm = new Vue({
        el: '#yclTransferOrderEdit',
        data: {
            tbData: [],
            selectHtml:[],
            arealist:[],
            fromStorelist:[],
            toStorelist:[],
            materialCodelist:[],
            params: {
                goodType: "自有",
                line: []
            }
        },
        methods: {
            delLine(){
                var selectData = layui.table.checkStatus('tableGridCheckEdit').data;
                this.tbData = table.cache['tableGridCheckEdit'];
                for(var i = 0; i < this.tbData.length; i++){
                    if(this.tbData[i].tempId === selectData[0].tempId){
                        this.tbData.splice(i,1);
                    }
                }
                tableIns.reload({
                    data : this.tbData
                });
            },
            addLine(){
                let materialCode = $("#materialCodeSelect").val().trim();
                if (materialCode == "") {
                    layer.msg('请输入物料编码', { icon: 2 });
                    return;
                }
                $.get(context_path + '/materials/code/' + materialCode, {}, function (res) {
                    let data = res.data;
                    if (data == "") {
                        layer.msg('查询不到数据', { icon: 2 });
                        return;
                    }
                    let newRow = {
                        tempId: new Date().valueOf()
                        , materialCode: data.CODE
                        , materialName: data.NAME
                        , quality: ''
                        , unit: data.PRIMARYUNIT
                        , remark: ""
                    };
                    this.tbData = table.cache['tableGridCheckEdit'];
                    this.tbData.push(newRow);
                    tableIns.reload({
                        data : this.tbData
                    });
                    // $("#materialCodeSelect").val(materialCode);
                    //initMaterialCodeSelect();
                })
            },
            submitTransfer() {
                this.params.line = table.cache['tableGridCheckEdit']
                if (this.params.line.length==0)
                {
                    layer.alert('请添加物料信息!');
                    return false;
                }
                if (this.params.line.some(m=> m.quality==null||m.quality==''||isNaN(m.quality)))
                {
                    layer.alert('出厂数量非数字!');
                    return;
                }
                $('#submitSave').addClass('layui-btn-disabled');
                $('#submitSave').attr('disabled', 'disabled');
                $.ajax({
                    type: 'POST',
                    url: context_path + "/wms2/otherStorage/leave/save",
                    contentType: "application/json",
                    async: false,
                    dataType: "json",
                    data: JSON.stringify(this.params),
                    success: (res)=> {
                        console.log(res)
                        this.params.outCode = res.outCode;
                        const str = encodeURI(JSON.stringify(this.params));
                        let frm =document.getElementById('myiframeFactory');
                        frm.src='/wms/ycl/purchasein/factory/toPrint?objStr='+str;
                        $(frm).load(function(){
                            LODOP=getLodop();
                            let strHtml=frm.contentWindow.document.documentElement.innerHTML;
                            LODOP.ADD_PRINT_HTM(0,0,"100%","100%",strHtml);
                            LODOP.PREVIEW();
                            let index = parent.layer.getFrameIndex(window.name);
                            parent.layer.close(index);
                        });
                    }
                })
            },
            /*校验*/
            formVerify(){
                this.params.fromAreaCode = $('#fromAreaCode').val();
                this.params.fromStoreCode = $('#fromStoreCode').val();
                this.params.toStoreCode = $('#toStoreCode').val();
                if (this.params.fromAreaCode=='')
                {
                    layer.alert('请选择调出厂区!');
                    return false;
                }
                if (this.params.fromStoreCode=='')
                {
                    layer.alert('请选择调出仓库!');
                    return false;
                }
                if (this.params.toStoreCode=='')
                {
                    layer.alert('请选择调入仓库!');
                    return false;
                }
                return true;
            }
        },
        created() {
        },
        /*列表数据源*/
        mounted() {
            //日期时间选择器
            laydate.render({
                elem: '#applyDate'
                , type: 'date'
                , done: function (value, date, endDate) {
                    vm.params.applyDate = value
                }
            });
            form.render();
            tableIns = table.render({
                elem: '#tableGridCheckEdit'
                ,data: this.tbData
                ,size: 'sm'
                ,cols: [[
                    {type: 'checkbox'}
                    ,{field: 'tempId', hide: true}
                    ,{field: 'materialCode', width:'150', title: '物料编码'}
                    ,{field: 'materialName', width:'300', title: '物料名称'}
                    ,{field: 'unit', width:'80', title: '单位'}
                    ,{field: 'quality', width:'100', style:'background-color: #5fb878;color:#fff', title: '数量', edit: 'text'}
                    ,{field: 'orderCode', width:'150', style:'background-color: #5fb878;color:#fff', title: '导入来源', edit: 'text'}
                    ,{field: 'comments', width:'300', style:'background-color: #5fb878;color:#fff', title: '备注', edit: 'text'}
                ]],
                done: function(res, curr, count){
                    this.tbData = res.data;
                }
            });
            var url = window.location.href;
            var dz_url = url.split('obj=')
            if (dz_url.length>1)
            {
                this.params = JSON.parse(decodeURI(dz_url[1]));
            }
            tableIns.reload({
                data : this.params.line
            });
        }
    })

    //监听select
    form.on('select(fromAreaCode)', function (data) {
        $.get(context_path + '/ycl/baseinfo/basedata/store/'+data.value
            , res=> {
                vm.fromStorelist = res.data;
                vm.toStorelist = res.data;
                vm.$nextTick(() => {
                    form.render('select','fromStoreCode');
                    form.render('select','toStoreCode');
                })
            })
    });
    //监听select
    form.on('select(fromStoreCode)', function (data) {
        $.get(context_path + '/ycl/report/inventory/listView', {
            //areaCode : $('#fromAreaCode').val(),
            storeCode: data.value
        }, function (res) {
            vm.materialCodelist = res.data
        })
    });
    //监听select
    form.on('select(materialCode)', function (data) {
        var elem = data.othis.parents('tr');
        var id = elem.first().find('td').eq(1).text();
        let val = data.value.split('_')
        $.get(context_path + '/ycl/report/inventory/listView', {
            storeCode:$('#fromStoreCode').val(),
            materialCode: val[0],
            locatorCode:val[1]
        }, function (res) {
            this.tbData = table.cache['tableGridCheckEdit'];
            res.data.forEach(m=>{
                this.tbData.forEach(n=>{
                    if (n.tempId==id)
                    {
                        n.materialCode = m.materialCode;
                        n.materialName = m.materialName;
                        n.fromLocator = m.locatorCode;
                        n.primaryUnit = m.primaryUnit;
                        n.lotsNum = m.lotsNum;
                        n.quality = m.quality;
                    }
                })
            })
            tableIns.reload({
                data : this.tbData
            });
        })
    });
</script>