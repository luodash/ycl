<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<head>
    <link rel="stylesheet" href="${APP_PATH}/static/layui/css/layui.css" type="text/css"/>
    <style type="text/css">
        .layui-input {
            height: 30px;
        }
    </style>
</head>
<script src="${APP_PATH}/static/layui/layui.all.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>


<div id="yclPurchaseCorrectEdit" style="margin: 15px">

    <div id="toolbarPICorrectOut">
        <label class="layui-word-aux">更正出库</label>
        <div class="layui-input-inline">
            <input id="outPoCode" type="text" required placeholder="请输入单号" class="layui-input">
        </div>
        <div class="layui-inline">
            <button type="button" @click="queryOut" class="layui-btn layui-btn-sm layui-btn-normal">
                <i class="layui-icon">&#xe615;</i>查询
            </button>
            <button type="button" @click="subOut" class="layui-btn layui-btn-sm">
                <i class="layui-icon">&#xe605;</i>确认
            </button>
        </div>
    </div>
    <table id="tableGridPICorrectOutEdit" lay-filter="tableGridFilterPICorrectOutEdit"></table>

    <div id="toolbarPICorrectIn">
        <label class="layui-word-aux">更正入库</label>
        <div class="layui-input-inline">
            <input id="inPoCode" type="text" placeholder="请输入单号" class="layui-input">
        </div>
        <div class="layui-inline">
            <button type="button" @click="queryIn" class="layui-btn layui-btn-sm layui-btn-normal">
                <i class="layui-icon">&#xe615;</i>查询
            </button>
            <button type="button" @click="subIn" class="layui-btn layui-btn-sm">
                <i class="layui-icon">&#xe605;</i>确认
            </button>
        </div>
        <div class="layui-inline">
            <label style="width: 65px">厂区</label>
            <div class="layui-form layui-input-inline">
                <select id="areaCodeSelectPICOEdit" lay-filter="areaCodeSelectFilterPICOEditFilter">
                </select>
            </div>
        </div>
        <div class="layui-inline">
            <label style="width: 65px">仓库</label>
            <div class="layui-form layui-input-inline">
                <select id="storeCodeSelectPICOEdit" lay-filter="storeCodeSelectPICOEditFilter">
                </select>
            </div>
        </div>
    </div>
    <table id="tableGridPICorrectInEdit" lay-filter="tableGridFilterPICorrectInEdit"></table>
    <%--    <button @click="addCorrect" type="button" class="layui-btn">确认更正</button>--%>
</div>

<script>

    const context_path = '${APP_PATH}';
    const form = layui.form,
        table = layui.table,
        $ = layui.jquery;

    let vm = new Vue({
        el: '#yclPurchaseCorrectEdit',
        data: {
            inData: {
                areaCode: '',
                areaName: '',
                storeCode: '',
                storeName: ''
            }
        },
        methods: {
            initAreaSelectPICOEdit() {
                $("#areaCodeSelectPICOEdit").empty();
                $("#areaCodeSelectPICOEdit").append('<option value="">请选择</option>');
                $.get(context_path + '/ycl/baseinfo/basedata/area/0'
                    , function (res) {
                        let html = '';
                        let list = res.data;
                        for (let i = 0; i < list.length; i++) {
                            html += '<option value=' + list[i].id + '>' + list[i].text + '</option>';
                        }
                        $("#areaCodeSelectPICOEdit").append(html)
                        form.render('select');
                    })
            },
            initStoreSelectPICOEdit() {
                $("#storeCodeSelectPICOEdit").empty();
                $("#storeCodeSelectPICOEdit").append('<option value="">请选择</option>');
                $.get(context_path + '/ycl/baseinfo/basedata/store/' + vm.inData.areaCode
                    , function (res) {
                        if (res.result) {
                            let html = '';
                            let list = res.data;
                            for (let i = 0; i < list.length; i++) {
                                html += '<option value=' + list[i].code + '>' + list[i].text + '</option>';
                            }
                            $("#storeCodeSelectPICOEdit").append(html);
                        }
                        form.render('select');
                    })
            },
            initTableInsOut() {
                table.render({
                    elem: '#tableGridPICorrectOutEdit'
                    , id: "tableGridSetPICorrectOutEdit"
                    , size: 'sm'
                    , data: []
                    , cols: [[
                        {type: 'radio'}
                        , {field: 'poCode', title: '订单号'}
                        , {field: 'poLineNum', title: '订单行号'}
                        , {field: 'materialCode', title: '物料编码'}
                        , {field: 'materialName', title: '物料名称'}
                        , {field: 'lotsNum', title: '批次号'}
                        , {field: 'storeName', title: '仓库名称'}
                        // , {field: 'storeCode', title: '库位编码'}
                        , {field: 'poQuantity', title: '订单数量'}
                        , {field: 'receiveQuantity', title: '已收数量'}
                        // , {field: '', title: '库存数量'}
                        , {field: 'correctQuantity', title: '更正数量', edit: 'text'}
                    ]]
                });
            },
            initTableInsIn() {
                table.render({
                    elem: '#tableGridPICorrectInEdit'
                    , id: "tableGridSetPICorrectInEdit"
                    // , size: 'sm'
                    , data: []
                    , cols: [[
                        {type: 'radio'}
                        , {field: 'poCode', title: '订单号'}
                        , {field: 'poLineNum', title: '订单行号'}
                        , {field: 'materialCode', title: '物料编码'}
                        , {field: 'materialName', title: '物料名称'}
                        // , {field: 'areaName', title: '仓库名称'}
                        // , {field: 'supplierCode', title: '库位编码'}
                        , {field: 'quantity', title: '订单数量'}
                        , {field: 'lotsNum', title: '更正批次号', edit: 'text'}
                        , {field: 'correctQuantity', title: '更正数量', edit: 'text'}
                        , {field: 'locatorCode', title: '库位', edit: 'text'}
                    ]]
                });
            },
            queryOut() {
                if ($('#outPoCode').val() == '' || $('#outPoCode').val() == null) {
                    layer.msg('请输入单号', {icon: 2});
                    return
                }

                $.get(context_path + '/ycl/purchaseIn/PurchaseCorrect/getOut', {
                    poCode: $('#outPoCode').val()
                }, function (res) {
                    let outOrder = res.data
                    table.reload('tableGridSetPICorrectOutEdit', {
                        data: outOrder
                    });
                })
            },
            queryIn() {

                if ($('#inPoCode').val() == '' || $('#inPoCode').val() == null) {
                    layer.msg('请输入单号', {icon: 2});
                    return
                }

                $.get(context_path + '/ycl/purchaseIn/PurchaseOrder/pageView', {
                    poCode: $('#inPoCode').val(),
                }, function (res) {
                    console.log(res)
                    let inOrder = res.data
                    table.reload('tableGridSetPICorrectInEdit', {
                        data: inOrder
                    });
                })
            },
            subOut() {
                let outCheck = table.checkStatus('tableGridSetPICorrectOutEdit').data

                if (outCheck.length === 0) {
                    layer.msg('至少选择一条数据', {icon: 2});
                    return
                }

                for (let i = 0; i < outCheck.length; i++) {

                    if (outCheck[i].correctQuantity == null || outCheck[i].correctQuantity == '') {
                        layer.msg('更正数量不能为空', {icon: 2});
                        return
                    }

                    if (typeof outCheck[i].correctQuantity != 'number' && isNaN(outCheck[i].correctQuantity)) {
                        layer.msg('请输入有效数字', {icon: 2});
                        return
                    }

                    if (parseFloat(outCheck[i].correctQuantity) <= 0) {
                        layer.msg('更正数量必要大于0', {icon: 2});
                        return
                    }

                    // if (outCheck[i].receiveQuantity * 1 < outCheck[i].correctQuantity * 1) {
                    //     layer.msg('更正数量不能大于已收数量', {icon: 2});
                    //     return
                    // }

                    outCheck[i].inoutType = '0'
                    outCheck[i].orderTime = outCheck[i].createTime
                    outCheck[i].id = null
                    outCheck[i].updownQuantity = parseFloat(outCheck[i].correctQuantity)
                }
                console.log(outCheck)
                $.ajax({
                    type: 'POST',
                    url: context_path + "/ycl/purchaseIn/PurchaseCorrect/subOut",
                    contentType: "application/json",
                    async: true,
                    dataType: "json",
                    data: JSON.stringify(outCheck),
                    success: function (res) {
                        if (res.result === false) {
                            layer.msg(res.msg, {icon: 2});
                            return;
                        } else {
                            layer.msg(res.msg, {icon: 1});
                            let index = parent.layer.getFrameIndex(window.name);
                            parent.layer.close(index);
                            window.parent.tableRender()
                        }
                    }
                })
            },
            subIn() {
                let inCheck = table.checkStatus('tableGridSetPICorrectInEdit').data

                if (inCheck.length === 0) {
                    layer.msg('请选择一条数据', {icon: 2});
                    return
                }

                if (vm.inData.areaCode == '' || vm.inData.areaCode == null) {
                    layer.msg('请选择厂区', {icon: 2});
                    return
                }

                if (vm.inData.storeCode == '' || vm.inData.storeCode == null) {
                    layer.msg('请选择仓库', {icon: 2});
                    return
                }

                for (let i = 0; i < inCheck.length; i++) {

                    if (inCheck[i].correctQuantity == null || inCheck[i].correctQuantity == '') {
                        layer.msg('更正数量不能为空', {icon: 2});
                        return
                    }

                    if (typeof inCheck[i].correctQuantity != 'number' && isNaN(inCheck[i].correctQuantity)) {
                        layer.msg('请输入有效数字', {icon: 2});
                        return
                    }

                    if (parseFloat(inCheck[i].correctQuantity) <= 0) {
                        layer.msg('更正数量必要大于0', {icon: 2});
                        return
                    }

                    if (inCheck[i].lotsNum == null || inCheck[i].lotsNum == '') {
                        layer.msg('更正批次不能为空', {icon: 2});
                        return
                    }

                    if (inCheck[i].locatorCode == null || inCheck[i].locatorCode == '') {
                        layer.msg('库位不能为空', {icon: 2});
                        return
                    }

                    inCheck[i].inout = '1'
                    inCheck[i].id = null
                    inCheck[i].amount = inCheck[i].correctQuantity * 1
                    inCheck[i].areaCode = vm.inData.areaCode
                    inCheck[i].areaName = vm.inData.areaName
                    inCheck[i].storeCode = vm.inData.storeCode
                    inCheck[i].storeName = vm.inData.storeName
                }

                console.log(inCheck[0])

                $.ajax({
                    type: 'POST',
                    url: context_path + "/ycl/purchaseIn/PurchaseCorrect/subIn",
                    contentType: "application/json",
                    async: true,
                    dataType: "json",
                    data: JSON.stringify(inCheck),
                    success: function (res) {
                        console.log(res)
                        if (res.result === false) {
                            layer.msg(res.msg, {icon: 5});
                            return;
                        } else {
                            layer.msg(res.msg, {icon: 5});
                            let index = parent.layer.getFrameIndex(window.name);
                            parent.layer.close(index);
                            window.parent.tableRender()
                        }
                    }
                })

            },
            addCorrect() {
                const outCheck = table.checkStatus('tableGridSetPICorrectOutEdit').data
                if (outCheck.length === 0) {
                    layer.msg('至少选择一条原订单', {icon: 2});
                    return
                }
                const inCheck = table.checkStatus('tableGridSetPICorrectInEdit').data
                if (inCheck.length === 0) {
                    layer.msg('至少选择一条更正订单', {icon: 2});
                    return
                }

                let sub = outCheck.concat(inCheck)
                let correct = inCheck[0]
                console.log(sub)
                console.log(correct)

                $.ajax({
                    type: 'POST',
                    url: context_path + "/ycl/purchaseIn/PurchaseCorrect/saveByMap",
                    contentType: "application/json",
                    async: true,
                    dataType: "json",
                    data: JSON.stringify({
                        sub,
                        correct
                    }),
                    success: function (res) {
                        if (res.result === false) {
                            layer.msg(res.msg, {icon: 5});
                            return;
                        } else {
                            layer.msg(res.msg, {icon: 5});
                            let index = parent.layer.getFrameIndex(window.name);
                            parent.layer.close(index);
                            window.parent.tableRender()
                        }
                    }
                })

            }
        },
        created() {
        }
        ,
        mounted() {
            form.render();
        }

    })


    form.on('select(areaCodeSelectFilterPICOEditFilter)', function (data) {
        vm.inData.areaCode = data.value
        vm.inData.areaName = data.elem[data.elem.selectedIndex].text
        vm.initStoreSelectPICOEdit()
    });

    form.on('select(storeCodeSelectPICOEditFilter)', function (data) {
        vm.inData.storeCode = data.value
        vm.inData.storeName = data.elem[data.elem.selectedIndex].text
    });

    // init
    ;!function () {
        vm.initAreaSelectPICOEdit()
        vm.initTableInsOut()
        vm.initTableInsIn()
    }();


</script>
