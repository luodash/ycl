<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<div id="yclPICorrectPage" style="margin: 15px">

    <blockquote class="layui-elem-quote">
        <form class="layui-form">
            <div class="layui-fluid">
                <div class="layui-row layui-col-space10">
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">更正单号</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" v-model="queryParams.correctCode"/>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">订单编号</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" v-model="queryParams.poCode"/>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">供应商</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" v-model="queryParams.supplierName"/>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <button type="button" class="layui-btn layui-btn-sm layui-btn-normal"
                                    @click="queryPageView">
                                <i class="layui-icon">&#xe615;</i>查询
                            </button>
                        </div>
                        <div class="layui-inline">
                            <button type="button" class="layui-btn layui-btn-primary layui-btn-sm"
                                    @click="restQueryParams">
                                <i class="layui-icon">&#xe669;</i>重置
                            </button>
                        </div>
                    </div>
                </div>
                <div class="layui-row layui-col-space10">
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">订单日期</label>
                            <div class="layui-input-inline">
                                <input type="text" id="orderDatePICorrectList" class="layui-input"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </blockquote>

    <div id="toolbarPICorrect">
        <button type="button" @click="addPICorrect" class="layui-btn layui-btn-sm layui-btn-normal">
            <i class="layui-icon">&#xe654;</i>新建
        </button>
        <button type="button" @click="printCertificate" class="layui-btn layui-btn-sm layui-btn-normal">
            <i class="layui-icon">&#xe66d;</i>打印
        </button>
    </div>
    <table id="tableGridPICorrect" lay-filter="tableGridFilterPICorrect"></table>
    <iframe style="visibility:hidden;" id="myIframeCorrect" name="myIframeCorrect"
            src="${APP_PATH}/ycl/purchaseIn/PurchaseCorrect/toCorrectPrint" width=0 height=0></iframe>

</div>

<script src="/wms/static/js/techbloom/LodopFuncs.js" charset="utf-8"></script>
<script>

    const context_path = '${APP_PATH}';
    const table = layui.table,
        form = layui.form,
        layer = layui.layer,
        laydate = layui.laydate;

    let vm = new Vue({
        el: '#yclPICorrectPage',
        data: {
            queryParams: {
                correctCode: "",
                poCode: "",
                supplierName: "",
                orderDate: ''
            }
        },
        watch: {},
        methods: {
            /*加载数据*/
            initTableIns() {
                table.render({
                    elem: '#tableGridPICorrect'
                    , url: context_path + '/ycl/purchaseIn/PurchaseCorrect/pageView'
                    , page: true
                    , id: "tableGridSetPICorrect"
                    , size: 'sm'
                    , cols: [[
                        {type: 'radio'}
                        , {field: 'correctCode', title: '更正单号', width: 160}
                        , {field: 'poCode', title: '订单号'}
                        , {field: 'poLineNum', title: '订单行号'}
                        , {field: 'materialCode', title: '物料编码'}
                        , {field: 'materialName', title: '物料名称'}
                        , {field: 'lotsNum', title: '批次号'}
                        , {field: 'orderTime', title: '订单日期'}
                        // , {field: 'supplierId', title: '供应商编号'}
                        , {field: 'supplierName', title: '供应商名称'}
                        , {field: 'purchaseOrgan', title: '采购组织'}
                        , {field: 'purchaseDept', title: '采购部门', width: 160}
                        // , {field: 'purchaseUser', title: '采购员'}
                    ]]
                });
            },
            /*查询*/
            queryPageView() {
                table.reload('tableGridSetPICorrect', {
                    where: vm.queryParams
                });
            },
            /*重置*/
            restQueryParams() {
                vm.queryParams = {
                    correctCode: "",
                    poCode: "",
                    supplierName: "",
                }
                $("#orderDatePICorrectList").val('');
                form.render('select');
                table.reload('tableGridSetPICorrect', {
                    where: vm.queryParams
                    , page: {
                        curr: 1
                    }
                });
            },
            /*新增*/
            addPICorrect() {
                layer.open({
                    title: '新增'
                    , type: 2
                    , area: ['1200px', '600px'] //宽高
                    , maxmin: true
                    , content: context_path + '/ycl/purchaseIn/PurchaseCorrect/toEdit'
                });
            },
            printCertificate() {

                let rowData = table.checkStatus('tableGridSetPICorrect').data

                if (rowData.length === 0) {
                    layer.msg('至少选择一条数据', {icon: 2});
                    return
                }

                let frm = document.getElementById('myIframeCorrect');
                frm.src = context_path + '/ycl/purchaseIn/PurchaseCorrect/toCorrectPrint?id=' + rowData[0].id;
                $(frm).load(function () {
                    LODOP = getLodop();
                    let strHtml = frm.contentWindow.document.documentElement.innerHTML;
                    LODOP.ADD_PRINT_HTM(0, 0, "100%", "100%", strHtml);
                    LODOP.PREVIEW();
                });

            }
        },
        created() {
        },
        mounted() {
        }

    })

    table.on('row(tableGridFilterPICorrect)', function (obj) {
        obj.tr.find('i[class="layui-anim layui-icon"]').trigger("click");
    })

    //监听select
    form.on('select(areaCodeSelect)', function (data) {
        vm.queryParams.areaCode = data.value
    });
    //监听select
    form.on('select(storeCodeSelect)', function (data) {
        vm.queryParams.storeCode = data.value
    });

    function tableRender() {
        table.reload('tableGridSetPICorrect');
    }

    // init
    ;!function () {
        // laydate.render({
        //     elem: '#orderDatePICorrectList'
        //     , done: function (value, date, endDate) {
        //         vm.queryParams.orderDate = value
        //     }
        // });


        laydate.render({
            elem: '#orderDatePICorrectList'
            , type: 'date'
            , range: '~'
            , done: function (value, date, endDate) {
                console.log(value)
                if (typeof value != 'undefined' && value != '' && value != null) {
                    let dateTime = value.split('~')
                    vm.queryParams.orderTimeStart = dateTime[0] + ' 00:00:00'
                    vm.queryParams.orderTimeEnd = dateTime[1] + ' 23:59:59'
                } else {
                    vm.queryParams.orderTimeStart = null
                    vm.queryParams.orderTimeEnd = null
                }
            }
        });

        vm.initTableIns()
    }();
</script>