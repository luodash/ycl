<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<div id="yclQualityControl" style="margin: 30px">
    <%--query tools--%>
    <blockquote class="layui-elem-quote">
        <form class="layui-form">
            <div class="layui-fluid">
                <div class="layui-row layui-col-space10">
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">送货单号</label>
                            <div class="layui-input-inline">
                                <input type="text" id="asnNumber" class="layui-input"
                                       v-model="queryParams.asnNumber"/>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">收货日期</label>
                            <div class="layui-input-inline">
                                <input type="text" id="createTime" class="layui-input"
                                       v-model="queryParams.createTime"/>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">物料编码</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" v-model="queryParams.materialCode"/>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">质检人</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" v-model="queryParams.createUser"/>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <button type="button" class="layui-btn layui-btn-sm layui-btn-normal"
                                    @click="queryPageView">
                                <i class="layui-icon">&#xe615;</i>查询
                            </button>
                        </div>
                        <div class="layui-inline">
                            <button type="button" class="layui-btn layui-btn-primary layui-btn-sm"
                                    @click="restQueryParams">
                                <i class="layui-icon">&#xe669;</i>重置
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </blockquote>

    <%-- table grid --%>
    <table id="tableGridQualityControl" lay-filter="tableGridFilterCheck"></table>
    <%-- table grid toolbar --%>
    <script type="text/html" id="tableGridToolbarQualityControl">
        <div class="layui-btn-container">
            <button type="button" class="layui-btn layui-btn-sm layui-btn-normal" lay-event="addQualityControl">
                <i class="layui-icon">&#xe654;</i>同意入库
            </button>
        </div>
    </script>
</div>
<script>

    const context_path = '${APP_PATH}';
    const table = layui.table,
        form = layui.form,
        layer = layui.layer,
        laydate = layui.laydate;

    let tableGridIns3;

    //获取table的list集合
    let vm = new Vue({
        el: '#yclQualityControl',
        data: {
            queryParams: {
                asnNumber: "",
                poCode: "",
                createTime: "",
                materialName: "",
                createUser: ""
            }
        },
        methods: {
            initTableIns() {
                table.render({
                    elem: '#tableGridQualityControl'
                    , url: context_path + '/ycl/purchaseIn/qualityControl/list'
                    , page: true
                    , toolbar: '#tableGridToolbarQualityControl'
                    , id: "tableGridQualityControl"
                    , size: 'sm'
                    , defaultToolbar: []
                    , cols: [[
                        {type: 'radio'}
                        , {field: 'id', hide: true}
                        , {field: 'asnNumber', title: '送货单号'}
                        , {field: 'storeName', title: '接收仓库'}
                        , {field: 'locator', title: '库位编码'}
                        , {field: 'vendorName', title: '供应商'}
                        , {field: 'lotsNum', title: '批次号'}
                        , {field: 'materialCode', title: '物料编码'}
                        , {field: 'materialName', title: '物料名称'}
                        , {field: 'boxAmount', title: '件数'}
                        , {field: 'receiveQuantity', title: '数量'}
                        , {field: 'primaryUnit', title: '单位'}
                        , {field: 'createTime', title: '接收日期'}
                        , {field: 'name', title: '质检员'}
                        , {
                            field: 'passWms', title: 'WMS质检', width: 80, templet: function (d) {
                                if (d.passWms == null || d.passWms == '') {
                                    return ''
                                } else if (d.passWms == 0) {
                                    return '<span>待质检</span>'
                                } else if (d.passWms == 1) {
                                    return '<span>放行</span>'
                                } else if (d.passWms == 2) {
                                    return '<span>合格</span>'
                                } else if (d.passWms == 3) {
                                    return '<span>不合格</span>'
                                }
                            }
                        }
                        , {
                            field: 'passOa', title: 'OA放行', width: 80, templet: function (d) {
                                if (d.passOa == null || d.passOa == '') {
                                    return ''
                                } else if (d.passOa == '1') {
                                    return '<span>通过</span>'
                                } else if (d.passOa == '2') {
                                    return '<span>未通过</span>'
                                }
                            }
                        }
                        , {
                            field: 'sample', title: '试样状态', width: 80, templet: function (d) {
                                if (d.sample == null || d.sample == '') {
                                    return ''
                                } else if (d.sample == 'Y') {
                                    return '<span>合格</span>'
                                } else if (d.sample == 'N') {
                                    return '<span>不合格</span>'
                                }
                            }
                        }
                        , {
                            field: 'consult', title: '最终处理结果', width: 80, templet: function (d) {
                                if (d.consult == null || d.consult == '') {
                                    return ''
                                } else if (d.consult == '10') {
                                    return '<span>让步接收</span>'
                                } else if (d.consult == '20') {
                                    return '<span>扣款接收</span>'
                                }
                            }
                        }

                    ]]
                });
            },

            queryPageView() {
                table.reload('tableGridQualityControl', {
                    where: vm.queryParams
                    , page: {
                        curr: 1
                    }
                });
            },
            restQueryParams() {
                vm.queryParams = {
                    asnNumber: "",
                    poCode: "",
                    createTime: "",
                    materialCode: "",
                    createUser: ""
                }
                table.reload('tableGridQualityControl', {
                    where: vm.queryParams
                    , page: {
                        curr: 1
                    }
                });
            }
        },
        created() {
        },
        mounted() {
            this.initTableIns();
        }
    })

    table.on('toolbar(tableGridFilterCheck)', function (obj) {
        const checkStatusData = table.checkStatus(obj.config.id).data

        if (checkStatusData.length === 0) {
            layer.msg('请选择一条记录', {icon: 2});
            return
        }

        let subDate = checkStatusData[0]

        if (subDate.passWms != null && subDate.passWms != '') {
            layer.msg('已质检无法重复操作', {icon: 2});
            return
        }

        if (subDate.passOa != null && subDate.passOa != '') {
            layer.msg('已质检无法重复操作', {icon: 2});
            return
        }

        switch (obj.event) {
            case 'addQualityControl':
                layer.open({
                    title: '质检审核'
                    , type: 2
                    , area: ['1000px', '400px'] //宽高
                    , maxmin: true
                    , content: context_path + '/ycl/purchaseIn/qualityControl/toEdit?id=' + checkStatusData[0].id
                });
                break;
        }
    });

    //监听select
    form.on('select(fromStoreCodeList)', function (data) {
        console.log(data.elem); //得到select原始DOM对象
        console.log(data.value); //得到被选中的值
        console.log(data.othis); //得到美化后的DOM对象
    });

    function tableRender() {
        table.reload('tableGridQualityControl');
    }

    // init
    ;!function () {
        //日期区间选择器
        laydate.render({
            elem: '#createTime'
            , type: 'date'
            , range: '~'
            , done: function (value, date, endDate) {
                console.log(value)
                console.log(date)
                console.log(endDate)
                let dateTime = value.split('~')
                console.log(dateTime[0])
                console.log(dateTime[1])
                vm.queryParams.createTime = value
            }
        });
    }();
</script>