<%--其他出入库增加--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<head>
    <link rel="stylesheet" href="${APP_PATH}/static/layui/css/layui.css" type="text/css"/>
    <%--行编辑样式--%>
    <%--<link rel="stylesheet" href="layui/css/layui.css?v=201805080202" />--%>
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
        /*您可以将下列样式写入自己的样式表中*/
        .childBody {
            padding: 15px;
        }

        /*layui 元素样式改写*/
        .layui-btn-sm {
            line-height: normal;
            font-size: 12.5px;
        }

        .layui-table-view .layui-table-body {
            min-height: 256px;
        }

        .layui-table-cell .layui-input.layui-unselect {
            height: 30px;
            line-height: 30px;
        }

        /*设置 layui 表格中单元格内容溢出可见样式*/
        .table-overlay .layui-table-view,
        .table-overlay .layui-table-box,
        .table-overlay .layui-table-body {
            overflow: visible;
        }

        .table-overlay .layui-table-cell {
            height: auto;
            overflow: visible;
        }

        /*文本对齐方式*/
        .text-center {
            text-align: center;
        }
    </style>
</head>

<script src="${APP_PATH}/static/layui/layui.all.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

<div id="quantityControl_edit">
    <blockquote class="layui-elem-quote">
        <form class="layui-form">
            <div class="layui-row layui-col-space10">
                <div class="layui-col-sm4">
                    <div class="layui-inline">
                        <label style="width: 65px">单据编号</label>
                        <div class="layui-input-inline">
                            <input id="businessCode" type="text" autocomplete="off" disabled class="layui-input"
                                   value="${data.asnNumber}">
                        </div>
                    </div>
                </div>
                <div class="layui-col-sm4">
                    <div class="layui-inline">
                        <label style="width: 65px">物料编码</label>
                        <div class="layui-input-inline">
                            <input id="materialCode" type="text" autocomplete="off" disabled class="layui-input"
                                   value="${data.materialCode}">
                        </div>
                    </div>
                </div>
                <div class="layui-col-sm4">
                    <div class="layui-inline">
                        <label style="width: 65px">批次号</label>
                        <div class="layui-input-inline">
                            <input id="lotsNum" type="text" autocomplete="off" disabled class="layui-input"
                                   value="${data.lotsNum}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-row layui-col-space10">
                <div class="layui-col-sm4">
                    <div class="layui-inline">
                        <label style="width: 65px">物料名称</label>
                        <div class="layui-input-inline">
                            <input id="materialName" type="text" autocomplete="off" disabled class="layui-input"
                                   value="${data.materialName}">
                        </div>
                    </div>
                </div>
                <div class="layui-col-sm4">
                    <div class="layui-inline">
                        <label style="width: 65px">入库数量</label>
                        <div class="layui-input-inline">
                            <input id="quantity" type="text" autocomplete="off" disabled class="layui-input"
                                   value="${data.receiveQuantity}">
                        </div>
                    </div>
                </div>
                <div class="layui-col-sm4">
                    <div class="layui-inline">
                        <label style="width: 65px">质检结果</label>
                        <div class="layui-input-inline" lay-filter="passWmsSelect">
                            <select id="passWms" lay-filter="passWmsSelect">
                                <option value="">-下拉选择-</option>
                                <option value="1">放行</option>
                                <option value="2">合格</option>
                                <option value="3">不合格</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-row layui-col-space10">
                <div class="layui-col-sm6">
                </div>
                <div class="layui-col-sm6">
                    <%--查询按钮--%>
                    <div class="layui-input-block">
                        <button type="submit" class="layui-btn" lay-submit lay-filter="submitBtn">提交</button>
                        <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                    </div>
                </div>
            </div>
        </form>
    </blockquote>
</div>


<%--引入表格样式--%>
<%--<script src="layui/layui.js?v=201805080202" charset="utf-8"></script>--%>

<script>
    const context_path = '${APP_PATH}';
    const otherStorageIds = '${otherStorageIds}'; //勾选的订单物料
    let entity = {"id": '${data.id}', "passWms": ''};

    const table = layui.table,
        form = layui.form,
        laydate = layui.laydate,
        $ = layui.jquery;
</script>

<script type="text/javascript">

    //监听select下拉选中事件
    form.on('select(type)', function (data) {
        var elem = data.elem; //得到select原始DOM对象
        $(elem).prev("a[lay-event='type']").trigger("click");
    });
    // form.on('select(passWmsSelect)', function(data){
    //     console.log(data.value); //得到被选中的值
    //
    // });


    //监听提交
    form.on('submit(submitBtn)', function (data) {

        if ($('#passWms').val() == null || $('#passWms').val() == '') {
            layer.msg('请选择质检结果', {icon: 2});
            return
        }

        $.ajax({
            type: 'POST',
            url: context_path + "/ycl/purchaseIn/qualityControl/updatePassWms",
            async: false,
            dataType: "json",
            data: {
                id: '${data.id}'
                , passWms: $('#passWms').val()
            },
            success: function (res) {
                if (res.result === false) {
                    layer.msg(res.msg, {icon: 2});
                    return;
                } else {
                    layer.msg(res.msg, {icon: 1});
                    let index = parent.layer.getFrameIndex(window.name);
                    parent.layer.close(index);
                    window.parent.tableRender()
                }
            }
        })

    });

    let vm = new Vue({
        el: '#quantityControl_edit',
        data: {
            businessCode: '',
            materialCode: '',
            materialName: '',
            lotsNum: '',
            passWms: ''
        },
        methods: {},
        created() {
        },
        mounted() {
            form.render();
            form.render('select');
        }
    });

</script>
