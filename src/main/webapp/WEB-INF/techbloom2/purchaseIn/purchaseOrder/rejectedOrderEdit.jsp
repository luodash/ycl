<%--
退货单
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<head>
    <link rel="stylesheet" href="${APP_PATH}/static/layui/css/layui.css" type="text/css"/>
    <style type="text/css">
        .layui-input {
            height: 30px;
        }
    </style>
</head>
<script src="${APP_PATH}/static/layui/layui.all.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

<div id="yclRejectedOrderEdit" style="margin: 15px">
    <blockquote class="layui-elem-quote">
        <form class="layui-form">
            <div class="layui-fluid">
                <div class="layui-row layui-col-space10">
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">供应商名</label>
                            <div class="layui-input-inline">
                                <input type="tel" autocomplete="off" class="layui-input" disabled
                                       v-model="rejectedOrderEditParams.asnHeader.vendorName">
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">收货公司</label>
                            <div class="layui-input-inline">
                                <input type="text" autocomplete="off" class="layui-input" disabled
                                       v-model="rejectedOrderEditParams.asnHeader.purchaseOrgan">
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">采购员</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" disabled
                                       v-model="rejectedOrderEditParams.asnHeader.purchaseUser">
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <button type="button" class="layui-btn layui-btn-sm layui-btn-normal"
                                    @click="submitRejectedOrder">
                                <i class="layui-icon">&#xe605;</i>确定
                            </button>
                        </div>
                        <div class="layui-inline">
                            <button type="button" class="layui-btn layui-btn-sm" @click="cancelDeliveryOrder">
                                <i class="layui-icon">&#x1006;</i>取消
                            </button>
                        </div>
                    </div>
                </div>
                <div class="layui-row">
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">备注说明</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input"
                                       v-model="rejectedOrderEditParams.asnHeader.remark">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </blockquote>

    <table id="tableGridPIROEdit" lay-filter="tableGridFilterPIROEdit"></table>

</div>

<script>

    const context_path = '${APP_PATH}';
    const purchaseOrderIds = '${purchaseOrderIds}'; //勾选的订单物料
    const table = layui.table,
        form = layui.form,
        laydate = layui.laydate,
        $ = layui.jquery;

    let vm = new Vue({
            el: '#yclRejectedOrderEdit',
            data: {
                rejectedOrderEditParams: {
                    asnHeader: {
                        vendorId: '',
                        vendorName: '${supplierName}',
                        purchaseOrgan: '${purchaseOrgan}',
                        purchaseUser: '${purchaseUser}',
                        remark: '${remark}',
                        areaCode: '',
                        areaName: '',
                        storeCode: '',
                        storeName: '',
                        asnType: "3"
                    },
                    asnLines: []
                }
            },
            methods: {
                initTableData() {
                    // if (purchaseOrderIds != '' || purchaseOrderIds != null) {
                    //     let str = purchaseOrderIds.split(",")
                    //     for (let i = 0; i < str.length; i++) {
                    //         let s = str[i].split("_")
                    //         listMap.push({
                    //             poCode: s[0],
                    //             poLineNum: s[1],
                    //         })
                    //     }
                    // }           // let listMap = []

                    $.ajax({
                        type: 'POST',
                        url: context_path + "/ycl/ordermanage/asn/listViewRejectAsnLine",
                        contentType: "application/json",
                        async: true,
                        dataType: "json",
                        data: JSON.stringify({
                            poLineIds: purchaseOrderIds
                        }),
                        success: function (res) {
                            if (res.code == 0) {
                                vm.$nextTick(() => {
                                    table.reload('tableGridSetPIRO', {
                                        data: res.data
                                    });
                                    vm.rejectedOrderEditParams.asnHeader.areaCode = res.data[0].areaCode
                                    vm.rejectedOrderEditParams.asnHeader.areaName = res.data[0].areaName
                                    vm.rejectedOrderEditParams.asnHeader.storeCode = res.data[0].storeCode
                                    vm.rejectedOrderEditParams.asnHeader.storeName = res.data[0].storeName
                                })
                            } else {
                                layer.msg(res.msg);
                            }
                        }
                    })
                },
                initTableIns() {
                    table.render({
                        elem: '#tableGridPIROEdit'
                        , id: "tableGridSetPIRO"
                        , size: 'sm'
                        , data: []
                        , cols: [[
                            {type: 'checkbox'}
                            , {field: 'poCode', title: '订单号'}
                            , {field: 'poLineNum', title: '订单行号', width: 80}
                            , {field: 'poLineId', title: '订单行ID', hide: true}
                            , {field: 'lotsNum', title: '批次号'}
                            , {field: 'materialCode', title: '物料编码'}
                            , {field: 'materialName', title: '物料名称'}
                            , {field: 'shipQuantity', title: '收货数量', width: 85}
                            , {field: 'returnReceiveQuantity', title: '退货数量', edit: 'text'}
                        ]]
                    });
                },
                submitRejectedOrder() {

                    let lineData = table.checkStatus('tableGridSetPIRO').data

                    if (lineData.length === 0) {
                        layer.msg('请选择一条订单', {icon: 2});
                        return
                    }

                    for (let i = 0; i < lineData.length; i++) {
                        let item = lineData[i]

                        if (item.returnReceiveQuantity == '' || item.returnReceiveQuantity == null) {
                            layer.msg('退货数量不能为空！', {icon: 2});
                            return
                        }

                        if (parseFloat(item.returnReceiveQuantity) == 0) {
                            layer.msg('退货数量不能为0！', {icon: 2});
                            return
                        }

                        if (parseFloat(item.shipQuantity) < parseFloat(item.returnReceiveQuantity)) {
                            layer.msg('退货数量不能大于收货数量！', {icon: 2});
                            return
                        }

                    }

                    vm.rejectedOrderEditParams.asnLines = lineData;

                    layer.load(2, {time: 3*1000});

                    $.ajax({
                        type: 'POST',
                        url: context_path + "/ycl/ordermanage/asn/save",
                        contentType: "application/json",
                        async: false,
                        dataType: "json",
                        data: JSON.stringify(vm.rejectedOrderEditParams),
                        success: function (res) {
                            if (res.result === false) {
                                layer.msg(res.msg, {icon: 5});
                                return;
                            } else {
                                layer.msg(res.msg, {icon: 5});
                                let index = parent.layer.getFrameIndex(window.name);
                                parent.layer.close(index);
                                window.parent.tableRender()
                            }
                        }
                    })
                },
                cancelDeliveryOrder() {
                    let index = parent.layer.getFrameIndex(window.name);
                    parent.layer.close(index);
                }
            },
            created() {
            },
            mounted() {
                form.render();
            }

        })

        // init
    ;!function () {
        vm.initTableData()
        vm.initTableIns()
    }();
</script>
