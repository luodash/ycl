<%--
  Created by IntelliJ IDEA.
  User: huzg
  Date: 2020/6/25
  Time: 17:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <style>
        th, td {
            padding: 6px;
        }

        tr {
            height: 30px;
        }

        .info td {
            border-right: 1px solid #000000;
            border-bottom: 1px solid #000000;
        }

        .head td {
            border-top: 1px solid #000000;
            border-bottom: 1px solid #000000;
        }
    </style>
</head>
<body>

<div style="padding: 10px;width: 98%">
    <div id="yclTransferOrderPrint">
        <div id="div1">
            <div style="width: 100%;text-align: center;font-size: 20px;font-weight: bold;padding-top: 15px;">
                新远东电缆有限公司<br/>
                退货/更正单
            </div>
            <table style="width: 100%;font-size: 14px;">
                <tbody>
                <tr>
                    <td>供应商：{{vendorName}}</td>
                    <td></td>
                    <td>仓库名称：</td>
                    <td width="100"></td>
                </tr>
                <tr>
                    <td>供货方：</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>采购订单号：{{asnNumber}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                </tbody>
            </table>
        </div>
        <div id="div2">
            <table class="info"
                   style="border-collapse:collapse;border-left: 1px solid #000000;border-spacing:0px;font-size: 14px;">
                <tbody>
                <tr class="head">
                    <td width="60">物料编码</td>
                    <td width="80">名称及规格</td>
                    <td width="50">类型</td>
                    <td width="50">单位</td>
                    <td width="80">数量</td>
                    <td width="80">供应商批次</td>
                    <td width="80">退货/更正时间</td>
                    <td width="100">备注</td>
                </tr>
                <tr v-for="en in tableList">
                    <td>{{en.materialCode}}</td>
                    <td>{{en.materialName}}</td>
                    <td>退货</td>
                    <td>{{en.primaryUnit}}</td>
                    <td>{{en.fromStore}}</td>
                    <td>{{en.lotsNum}}</td>
                    <td>{{en.creationDate}}</td>
                    <td>{{en.remark}}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

</div>
<script src="/wms/plugins/public_components/js/jquery-2.1.4.js"></script>
<script src="/wms/static/layui/vue.js"></script>
<script>
    const context_path = '${APP_PATH}';
    Date.prototype.Format = function (fmt) { //author: meizz
        var o = {
            "M+": this.getMonth() + 1, //月份
            "d+": this.getDate(), //日
            "H+": this.getHours(), //小时
            "m+": this.getMinutes(), //分
            "s+": this.getSeconds(), //秒
            "q+": Math.floor((this.getMonth() + 3) / 3), //季度
            "S": this.getMilliseconds() //毫秒
        };
        if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (var k in o)
            if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        return fmt;
    };
    let vm = new Vue({
        el: '#yclTransferOrderPrint',
        data: {
            asnHeaderId: '',
            asnNumber: '',
            vendorName: '',
            ymd: '',
            tableList: []
        },
        methods: {},
        created() {
            this.ymd = new Date().Format("yyyy-MM-dd");
            var url = window.location.href;
            var dz_url = url.split('asnHeaderId=')
            if (dz_url.length > 1) {
                this.asnHeaderId = dz_url[1];
                $.ajax({
                    type: 'GET',
                    url: context_path + "/ycl/ordermanage/asn/listViewAsn",
                    contentType: "application/json",
                    async: false,
                    data: {
                        asnHeaderId: this.asnHeaderId
                    },
                    success: res => {
                        console.log(res)
                        this.vendorName = res.data[0].vendorName
                        this.tableList = res.data
                    }
                })
            }
        },
    });
</script>
</body>
</html>
