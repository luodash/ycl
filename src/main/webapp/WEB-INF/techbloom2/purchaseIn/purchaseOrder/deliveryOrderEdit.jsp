<%--
送货单
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<head>
    <link rel="stylesheet" href="${APP_PATH}/static/layui/css/layui.css" type="text/css"/>
    <style type="text/css">
        .layui-input {
            height: 30px;
        }
    </style>
</head>
<script src="${APP_PATH}/static/layui/layui.all.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

<div id="yclDeliveryOrderEdit" style="margin: 15px">
    <blockquote class="layui-elem-quote">
        <form class="layui-form">
            <div class="layui-fluid">
                <div class="layui-row layui-col-space10">
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">供应商名</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" disabled
                                       v-model="deliveryOrderEditParams.asnHeader.vendorName">
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">收货公司</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" disabled
                                       v-model="deliveryOrderEditParams.asnHeader.purchaseOrgan">
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">车牌号码</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input"
                                       v-model="deliveryOrderEditParams.asnHeader.expressNumber">
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <button type="button" class="layui-btn layui-btn-sm layui-btn-normal"
                                    @click="submitDeliveryOrder">
                                <i class="layui-icon">&#xe605;</i>确定
                            </button>
                        </div>
                        <div class="layui-inline">
                            <button type="button" class="layui-btn layui-btn-sm" @click="cancelDeliveryOrder">
                                <i class="layui-icon">&#xe640;</i>取消
                            </button>
                        </div>
                    </div>
                </div>
                <div class="layui-row">
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">厂区</label>
                            <div class="layui-input-inline">
                                <select id="areaCodeSelectPIDOEdit" lay-filter="areaCodeSelectFilterPIDOEditFilter">
                                </select>
                                <%-- <input type="text" class="layui-input" disabled
                                        v-model="deliveryOrderEditParams.asnHeader.purchaseOrgan">--%>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">仓库</label>
                            <div class="layui-input-inline">
                                <select id="storeCodeSelectPIDOEdit" lay-filter="storeCodeSelectPIDOEditFilter">
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">备注说明</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input"
                                       v-model="deliveryOrderEditParams.asnHeader.remark">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </blockquote>

    <table id="tableGridDoEdit" lay-filter="tableGridFilterEdit"></table>

</div>

<script>

    const context_path = '${APP_PATH}';
    const purchaseOrderIds = '${purchaseOrderIds}'; //勾选的订单物料
    const table = layui.table,
        form = layui.form,
        laydate = layui.laydate,
        $ = layui.jquery;

    let vm = new Vue({
        el: '#yclDeliveryOrderEdit',
        data: {
            areaCodeSelectPIDOEdit: [],
            deliveryOrderEditParams: {
                asnHeader: {
                    vendorId: '',
                    vendorName: '',
                    purchaseOrgan: '',
                    purchaseOrganId: '',
                    purchaseDept: '',
                    purchaseDeptId: '',
                    shipDate: '',
                    expressNumber: '',
                    asnType: "1",
                    areaCode: '',
                    areaName: '',
                    storeCode: '',
                    storeName: ''
                },
                asnLines: []
            }
        },
        methods: {
            initAreaSelectPIDOEdit() {
                $("#areaCodeSelectPIDOEdit").empty();
                $("#areaCodeSelectPIDOEdit").append('<option value="">请选择</option>');
                $.get(context_path + '/ycl/baseinfo/basedata/area/0' // + vm.deliveryOrderEditParams.asnHeader.purchaseOrganId
                    , function (res) {
                        let html = '';
                        let list = res.data;
                        for (let i = 0; i < list.length; i++) {
                            html += '<option value=' + list[i].id + '>' + list[i].text + '</option>';
                        }
                        $("#areaCodeSelectPIDOEdit").append(html)
                        form.render('select');
                    })
            },
            initStoreSelectPIDOEdit() {
                $("#storeCodeSelectPIDOEdit").empty();
                $("#storeCodeSelectPIDOEdit").append('<option value="">请选择</option>');
                $.get(context_path + '/ycl/baseinfo/basedata/store/' + vm.deliveryOrderEditParams.asnHeader.areaCode
                    , function (res) {
                        if (res.result) {
                            let html = '';
                            let list = res.data;
                            for (let i = 0; i < list.length; i++) {
                                html += '<option value=' + list[i].code + '>' + list[i].text + '</option>';
                            }
                            $("#storeCodeSelectPIDOEdit").append(html);
                        }
                        form.render('select');
                    })
            },
            initTableIns() {
                table.render({
                    elem: '#tableGridDoEdit'
                    , url: context_path + '/ycl/purchaseIn/PurchaseOrder/pageView'
                    , where: {poLineIds: purchaseOrderIds}
                    , id: "tableGridDoEditSet"
                    , size: 'sm'
                    , cols: [[
                        {field: 'poCode', title: '订单号', width: 90}
                        , {field: 'poLineId', title: '订单行ID', width: 80}
                        , {field: 'lotsNum', title: '供应商批次号', width: 110, edit: 'text'}
                        , {field: 'materialCode', title: '物料编码', width: 140}
                        , {field: 'materialName', title: '物料名称'}
                        , {field: 'quantity', title: '订单数量', width: 80}
                        , {field: 'shipQuantity', title: '送货数量', width: 80, edit: 'text'}
                        , {field: 'receiveQuantity', title: '实收数量', width: 80, edit: 'text'}
                        , {field: 'boxAmount', title: '件数', width: 80, edit: 'text'}
                    ]]
                    , done: function (res) {
                        let poData = res.data[0]
                        console.log('poData', poData)
                        vm.deliveryOrderEditParams.asnHeader.vendorId = poData.supplierId
                        vm.deliveryOrderEditParams.asnHeader.vendorName = poData.supplierName
                        vm.deliveryOrderEditParams.asnHeader.purchaseOrgan = poData.purchaseOrgan
                        vm.deliveryOrderEditParams.asnHeader.purchaseOrganId = poData.purchaseOrganId
                        vm.deliveryOrderEditParams.asnHeader.purchaseDept = poData.purchaseDept
                        vm.deliveryOrderEditParams.asnHeader.purchaseDeptId = poData.purchaseDeptId
                        // vm.deliveryOrderEditParams.asnHeader.areaCode = poData.purchaseDeptId
                        // this.$nextTick(() => {
                        //     vm.initStoreSelectPIDOEdit()
                        // })
                    }
                });
            },
            submitDeliveryOrder() {

                if (vm.deliveryOrderEditParams.asnHeader.areaCode == '') {
                    layer.msg('厂区不能为空！', {icon: 2});
                    return
                }
                if (vm.deliveryOrderEditParams.asnHeader.storeCode == '') {
                    layer.msg('仓库不能为空！', {icon: 2});
                    return
                }
                let lines = table.cache['tableGridDoEditSet'];

                for (let i = 0; i < lines.length; i++) {

                    if (lines[i].lotsNum == '' || lines[i].lotsNum == null) {
                        layer.msg('供应商批次号不能为空！', {icon: 2});
                        return
                    }
                    if (lines[i].shipQuantity == '' || lines[i].shipQuantity == null) {
                        layer.msg('送货数量不能为空！', {icon: 2});
                        return
                    }

                    if (typeof lines[i].shipQuantity != 'number' && isNaN(lines[i].shipQuantity) & typeof lines[i].receiveQuantity != 'number' && isNaN(lines[i].receiveQuantity)) {
                        layer.msg('请输入有效数字', {icon: 2});
                        return
                    }

                    if (parseFloat(lines[i].shipQuantity) <= 0) {
                        layer.msg('送货数量不能小于等于0！', {icon: 2});
                        return
                    }
                    if (lines[i].receiveQuantity == '' || lines[i].receiveQuantity == null) {
                        layer.msg('实收数量不能为空！', {icon: 2});
                        return
                    }

                    if (parseFloat(lines[i].receiveQuantity) <= 0) {
                        layer.msg('实收数量不能小于等于0！', {icon: 2});
                        return
                    }

                    if (lines[i].boxAmount == '' || lines[i].boxAmount == null) {
                        layer.msg('件数不能为空！', {icon: 2});
                        return
                    }
                    if (parseFloat(lines[i].quantity) < parseFloat(lines[i].shipQuantity)) {
                        layer.msg('送货数量不能大于订单数量！', {icon: 2});
                        return
                    }
                    if (parseFloat(lines[i].shipQuantity) < parseFloat(lines[i].receiveQuantity)) {
                        layer.msg('实收数量不能大于送货数量！', {icon: 2});
                        return
                    }
                    lines[i].poQuantity = lines[i].quantity
                }


                vm.deliveryOrderEditParams.asnLines = lines
                console.log(vm.deliveryOrderEditParams)

                layer.load(2, {time: 3 * 1000});

                $.ajax({
                    type: 'POST',
                    url: context_path + "/ycl/ordermanage/asn/save",
                    contentType: "application/json",
                    async: false,
                    dataType: "json",
                    data: JSON.stringify(vm.deliveryOrderEditParams),
                    success: function (res) {
                        if (res.result === false) {
                            layer.msg(res.msg, {icon: 2});
                            return;
                        } else {
                            layer.msg(res.msg, {icon: 1});
                            let index = parent.layer.getFrameIndex(window.name)
                            parent.layer.close(index)
                            window.parent.tableRender()

                        }
                    }
                })
            },
            cancelDeliveryOrder() {
                let index = parent.layer.getFrameIndex(window.name)
                parent.layer.close(index)
                window.parent.tableRender()
            }
        },
        created() {
        },
        mounted() {
            form.render();
        }

    })

    form.on('select(areaCodeSelectFilterPIDOEditFilter)', function (data) {
        vm.deliveryOrderEditParams.asnHeader.areaCode = data.value
        vm.deliveryOrderEditParams.asnHeader.areaName = data.elem[data.elem.selectedIndex].text
        vm.initStoreSelectPIDOEdit()
    });
    form.on('select(storeCodeSelectPIDOEditFilter)', function (data) {
        vm.deliveryOrderEditParams.asnHeader.storeCode = data.value
        vm.deliveryOrderEditParams.asnHeader.storeName = data.elem[data.elem.selectedIndex].text
    });

    // init
    ;!function () {
        vm.initAreaSelectPIDOEdit()
        vm.initTableIns()
        // this.$nextTick(() => {
        //     vm.initStoreSelectPIDOEdit()
        // })

    }();
</script>
