<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%--销售订单查询--%>
<div id="saleOrderListPage">
    <%--query tools--%>
        <blockquote class="layui-elem-quote">
            <div class="layui-fluid">
                <div class="layui-row">
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 80px">订单编号</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" name="poCode" v-model="queryParams.orderNumber"/>

                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 80px">物料编码</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" name="meterialCode" v-model="queryParams.itemCode"/>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <button type="button" class="layui-btn layui-btn-sm layui-btn-normal" @click="queryPageView1">
                                <i class="layui-icon">&#xe615;</i>查询
                            </button>
                        </div>
                        <div class="layui-inline">
                            <button type="button" class="layui-btn layui-btn-primary layui-btn-sm" @click="restQueryParams1">
                                <i class="layui-icon">&#xe669;</i>重置
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </blockquote>
    <%-- table grid --%>
    <table id="tableGrid_saleOrderList" lay-filter="tableGridFilter"></table>
    <%-- table grid toolbar --%>
</div>
<script>
    const context_path = '${APP_PATH}';
    const table = layui.table,//集合数据
        layer = layui.layer,
        laydate = layui.laydate,//日期控件
        form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功

    let vm = new Vue({
        el: '#saleOrderListPage',
        data: {
            queryParams:{
                orderNumber: "",
                itemCode: "",
                itemDesc: ""
            }
        },
        methods: {
            initLayUiTable() {},
            queryPageView1() {
                console.log(vm.queryParams)
                table.reload('tableGridSaleOrderList', {
                    where: vm.queryParams
                });
            },
            //重置
            restQueryParams1(){
                vm.queryParams = {
                    orderNumber: "",
                    itemCode: "",
                    itemDesc: ""
                }
                form.render('select');
                table.reload('tableGridSaleOrderList', {
                    where: vm.queryParams
                });
            },
            addOrder() {
                console.log("ADD")
            }
        },
        created() {},
        mounted() {
            //日期时间选择器
            laydate.render({
                elem: '#orderDate'
                , type: 'datetime'
                , done: function (value, date, endDate) {
                    vm.queryParams.orderDate = value
                }
            });
            // 订单物料
            table.render({
                elem: '#tableGrid_saleOrderList'
                , url: context_path + '/ycl/sale/list2'
                , page: true
                , id: "tableGridSaleOrderList"
                , size: 'sm'
                , cols: [[
                    , {field: 'orderNumber', title: '订单编号'}
                    , {field: 'customerName', title: '客户'}
                    , {field: 'address', title: '收货地点'}
                    , {field: 'itemCode', title: '物料编码'}
                    , {field: 'itemDesc', title: '物料名称'}
                    , {field: 'stage', title: '临时提货区'}
                    , {field: 'consigner', title: '发货方'}
                    , {field: 'orderedQuantity', title: '订单数量'}
                    , {field: 'quality', title: '出库数量'}
                    , {field: 'storeCode', title: '子库'}
                    , {field: 'localCode', title: '库位'}
                    , {field: 'batch', title: '批次号'}
                    , {field: 'orderType', title: '订单类型'}
                ]]
            });
        }
    })
    function search(){}
</script>