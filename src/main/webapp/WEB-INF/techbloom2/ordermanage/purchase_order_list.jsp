<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<div id="purchaseOrderListPage">
    <%--query tools--%>
        <blockquote class="layui-elem-quote">
            <div class="layui-fluid">
                <div class="layui-row">
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 80px">订单编号</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" name="poCode" v-model="queryParams.poCode"/>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 80px">供应商名称</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" name="supplierName" v-model="queryParams.supplierName"/>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 80px">物料编码</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" name="materialCode" v-model="queryParams.materialCode"/>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <button type="button" class="layui-btn layui-btn-sm layui-btn-normal" @click="queryPageView1">
                                <i class="layui-icon">&#xe615;</i>查询
                            </button>
                        </div>
                        <div class="layui-inline">
                            <button type="button" class="layui-btn layui-btn-primary layui-btn-sm" @click="restQueryParams1">
                                <i class="layui-icon">&#xe669;</i>重置
                            </button>
                        </div>
                    </div>
                </div>
                <div class="layui-row">
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 80px">采购员</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" name="purchaseUser" v-model="queryParams.purchaseUser"/>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 80px">订单日期</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" id="orderTime" name="orderTime" v-model="queryParams.orderTime"/>
                            </div>
                        </div>
                    </div>
                    <%--<div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 80px" class="layui-form-label">订单类型</label>
                            <div class="layui-input-block">
                                <select name="poType"  v-model="queryParams.poType" lay-filter="poType">
                                    <option value="">全部</option>
                                    <option value="1">原材料采购</option>
                                    <option value="2">委外加工</option>
                                </select>
                            </div>
                        </div>
                    </div>--%>
                </div>
            </div>
        </blockquote>

    <%-- table grid --%>
    <table id="tableGrid_purchase" lay-filter="tableGridFilter"></table>
    <%-- table grid toolbar --%>

</div>


<script>

    const context_path = '${APP_PATH}';
    const table = layui.table,//集合数据
        layer = layui.layer,
        laydate = layui.laydate,//日期控件
        form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功

    let vm = new Vue({
        el: '#purchaseOrderListPage',
        data: {
            queryParams:{
                poCode: "",
                supplierName: "",
                materialCode: "",
                purchaseUser: "",
                orderTime: "",
                poType:""
            }
        },
        methods: {
            initLayUiTable() {},
            queryPageView1() {
                console.log(vm.queryParams)
                table.reload('tableGridSet', {
                    where: vm.queryParams
                });
            },
            //重置
            restQueryParams1(){
                vm.queryParams = {
                    poCode: "",
                    supplierName: "",
                    materialCode: "",
                    purchaseUser: "",
                    orderTime: "",
                    poType:""
                }
                $("#orderDateTime").val('');
                table.reload('tableGridSet', {
                    where: vm.queryParams
                });
            },
            addOrder() {
                console.log("ADD")
            }
        },
        created() {},
        mounted() {
            // 订单物料
            table.render({
                elem: '#tableGrid_purchase'
                , url: context_path + '/ycl/ordermanage/list'
                , page: true
                , id: "tableGridSet"
                , size: 'sm'
                , cols: [[
                    {type: 'checkbox'}
                     ,{field: 'poHeaderId', hide: true}
                    , {field: 'supplierName', title: '供应商'}
                    , {field: 'purchaseDept', title: '收货组织'}
                    , {field: 'poCode', title: '订单号'}
                    , {field: 'poLineNum', title: '行号', width: 55}
                    , {field: 'materialCode', title: '物料编号'}
                    , {field: 'materialName', title: '物料名称'}
                    , {field: 'primaryUnit', title: '单位', width: 55}
                    , {field: 'quantity', title: '订单数量', width: 85}
                    // , {field: 'wealth', title: '已收数量', width: 85}
                    // , {field: 'wealth', title: '紧急状态', width: 85}
                    // , {field: 'wealth', title: '接受时间'}
                    , {field: 'purchaseUser', title: '采购员'}
                    , {field: 'orderTime', title: '创建时间'}
                ]]
            });
        }
    })

    table.on('toolbar(tableGridFilter)', function (obj) {
        const checkStatusData = table.checkStatus(obj.config.id).data
        let purchaseOrderIds = checkStatusData.map(item => item.id).toString()

        if (checkStatusData.length === 0) {
            layer.msg('请选择订单', {icon: 2});
            return
        }

        if (checkStatusData.some(item => item.supplierName != checkStatusData[0].supplierName)) {
            layer.msg('请选择相同供应商', {icon: 2});
            return
        }

        switch (obj.event) {
            case 'addDeliveryOrder':
                layer.open({
                    title: '新增'
                    ,
                    type: 2
                    ,
                    area: ['1000px', '500px'] //宽高
                    ,
                    maxmin: true
                    ,
                    content: context_path + '/ycl/purchaseIn/PurchaseOrder/toDeliveryOrderEdit?purchaseOrderIds=' + purchaseOrderIds
                });
                break;
            case 'addRejectedOrder':
                layer.open({
                    title: '在线调试'
                    , type: 2
                    , area: ['1000px', '500px'] //宽高
                    , maxmin: true
                    , content: context_path + '/ycl/purchaseIn/PurchaseOrder/toEdit'
                });
                break;
            case 'delete':
                layer.msg('删除');
                break;
            case 'update':
                layer.msg('编辑');
                break;
        };
    });

    // 点击订单物料
    table.on('row(tableGridFilter)', function (obj) {
        const data = obj.data;
        console.log(data)
        table.reload('tableGridSet2', {
            where: {
                "poNum": data.poCode,
                "poLineNum": data.lineNum
            }
        });
        //标注选中样式
        obj.tr.addClass('layui-table-click').siblings().removeClass('layui-table-click');
    });

    function search(){}

    ;!function () {
        //日期区间选择器
        laydate.render({
            elem: '#orderTime'
            , type: 'date'
            , range: '~'
            , done: function (value, date, endDate) {
                console.log(value)
                console.log(date)
                console.log(endDate)
                let dateTime = value.split('~')
                console.log(dateTime[0])
                console.log(dateTime[1])
                vm.queryParams.orderTime = value
            }
        });
    }();

</script>