<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<div id="asnOrderListPage">
    <%--query tools--%>
        <blockquote class="layui-elem-quote">
            <div class="layui-fluid">
                <div class="layui-row">
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 80px">送货方名称</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" v-model="queryParams.vendorName"/>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 80px">送货单号</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" v-model="queryParams.asnNumber"/>
                            </div>
                        </div>
                    </div>
<%--                    <div class="layui-col-sm3">--%>
<%--                        <div class="layui-inline">--%>
<%--                            <label style="width: 80px">收货方组织</label>--%>
<%--                            <div class="layui-form layui-input-inline" lay-filter="purchaseDept">--%>
<%--                                <select id="purchaseDept" lay-filter="purchaseDept" lay-search="">--%>
<%--                                    <option value="">请选择</option>--%>
<%--                                    <option  v-for="(item,index) in organizationlist" :value="item.organizationId" :key="index">{{item.organizationName}}</option>--%>
<%--                                </select>--%>
<%--                            </div>--%>
<%--                        </div>--%>
<%--                    </div>--%>
                    <%--按钮--%>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 80px">物料编码</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" v-model="queryParams.materialCode"/>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <button type="button" class="layui-btn layui-btn-sm layui-btn-normal" @click="queryPageView">
                                <i class="layui-icon">&#xe615;</i>查询
                            </button>
                        </div>
                        <div class="layui-inline">
                            <button type="button" class="layui-btn layui-btn-primary layui-btn-sm" @click="restQueryParams">
                                <i class="layui-icon">&#xe669;</i>重置
                            </button>
                        </div>
                    </div>
                </div>
                <div class="layui-row">

                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 80px">送货日期</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" id="shipDate" v-model="queryParams.shipDate"/>
                            </div>
                        </div>
                    </div>
                    <%--送货单状态下拉--%>
                    <div class="layui-col-sm3">
                            <div class="layui-inline">
                                <label style="width: 80px">送货单状态</label>
                                <div class="layui-input-inline">
                                    <select id="status" style="width: 168px" v-model="queryParams.status">
                                        <option value="">全部</option>
                                        <option value="ALL_CLOSE">全部关闭</option>
                                        <option value="NEW">新建</option>
                                        <option value="PART_CANCEL">部分取消</option>
                                        <option value="PART_RECEIVE">部分接收</option>
                                        <option value="RECEIVE_CLOSE">接收关闭</option>
                                        <option value="SEND_OUT">已发货</option>
                                        <option value="APPROVING">已批准</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                </div>

            </div>
        </blockquote>

    <%-- table grid --%>
    <table id="asn_tableGrid" lay-filter="tableGridFilter"></table>
    <%-- table grid toolbar --%>
    <script type="text/html" id="tableGridToolbar">
        <div class="layui-btn-container">
            <label class="layui-word-aux">所有订单列表</label>
        </div>
    </script>
</div>


<script>

    const {} = '${APP_PATH}';
    const table = layui.table,
        layer = layui.layer,
        form = layui.form,
        laydate = layui.laydate;

    let vm = new Vue({
        el: '#asnOrderListPage',
        data: {
            organizationlist:[],
            queryParams: {
                vendorName: "",
                asnNumber: "",
                purchaseDept: [],
                materialCode: "",
                shipDate: "",
                status: ""
            }
        },
        methods: {
            initLayUiTable() {},
            queryPageView() {
                console.log(vm.queryParams)
                vm.queryParams.purchaseDept = $('#purchaseDept').val();
                table.reload('tableGridSet', {
                    where: vm.queryParams
                });
            },
            restQueryParams() {
                vm.queryParams = {
                    vendorName:"",
                    asnNumber:"",
                    purchaseDept:"",
                    materialCode:"",
                    shipDate:"",
                    status:""
                }
                $('#purchaseDept').val("");
                form.render('select','purchaseDept');
                table.reload('tableGridSet', {
                    where: vm.queryParams
                });
            },
            addOrder() {
                console.log("ADD")
            }
        },
            created() {
                $.get(context_path + '/wms2/baseinfo/organize/organization/0/0'
                    , res=> {
                        this.organizationlist = res.data;
                        this.$nextTick(() => {
                            form.render('select','purchaseDept');
                        });
                    })
            },
        mounted() {


            // 送/退货单
            table.render({
                elem: '#asn_tableGrid'
                , url: context_path + '/ycl/ordermanage/asnList'
                , page: true
                , toolbar: '#tableGridToolbar'
                , id: "tableGridSet"
                , size: 'sm'
                , defaultToolbar: ['filter']
                , cols: [[
                    {field : "asnHeaderId",title : "asnHeaderId",hide:true},
                    {field : "poCode",title : "采购订单号",width : 100 },
                    {field : "vendorName",title : "送货方",width : 200 },
                    {field : "poLineNum",title : "订单行号",width : 80 },
                    {field : "asnNumber",title : "送货单号",width : 150 },
                    {field : "purchaseDept",title : "收货方组织及使用部门",width : 200 },
                    {field : "materialCode",title : "物料编码",width : 100 },
                    {field : "materialName",title : "物料名称",width : 200 },
                    {field : "primaryUnit",title : "单位",width : 60},
                    {field : "shipQuantity",title : "数量",width : 60},
                    {field : "boxAmount",title : "件数",width : 60},
                    {field : "transactionQuantity",title : "在途",width : 60},
                    {field : "receiveQuantity",title : "已收",width : 60},
                    {field : "shipDate",title : "送货日期",width : 100},
                    {field : "asnType",title : "单据类型",width : 100 ,templet: function (d) {
                            if(d.asnType=='1'){
                                return '送货单';
                            }else if(d.asnType=='2'){
                                return '退货单'
                            }
                        }
                    },
                    {field : "status",title : "送货单状态",width : 90,
                        templet:function(d){
                            if(d.status=='ALL_CLOSE'){
                                return "全部关闭";
                            }else if(d.status=='NEW'){
                                return "新建";
                            }else if (d.status=='PART_CANCEL'){
                                return "部分取消";
                            }else if (d.status=='PART_RECEIVE'){
                                return "部分接收";
                            }else if (d.status=='RECEIVE_CLOSE'){
                                return "接收关闭";
                            }else if (d.status=='SEND_OUT'){
                                return "已发货";
                            }else if (d.status=='APPROVING'){
                                return "审批中";
                            }
                        }
                    },
                    {field : "dataSource",title : "数据源  ",width : 80}
                ]]
            });

        }

    })

    ;!function () {

        //日期区间选择器
        laydate.render({
            elem: '#shipDate'
            , type: 'date'
            , range: '~'
            , done: function (value, date, endDate) {
                console.log(value)
                console.log(date)
                console.log(endDate)
                let dateTime = value.split('~')
                console.log(dateTime[0])
                console.log(dateTime[1])
                vm.queryParams.shipDate = value
            }
        });
    }();


</script>