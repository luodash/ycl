<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<div id="yclCheck">
    <blockquote class="layui-elem-quote">
        <form class="layui-form">
            <div class="layui-inline">
                <div class="layui-input-inline">
                    <input type="text" class="layui-input" id="userName" placeholder="请输入用户名"/>
                </div>
                <%--  <a class="layui-btn search_btn" data-type="reload">搜索</a>--%>
            </div>
            <div class="layui-inline">
                <button type="button" class="layui-btn layui-btn-sm layui-btn-normal">
                    <i class="layui-icon">&#xe615;</i>查询
                </button>
            </div>
            <div class="layui-inline">
                <button type="button" class="layui-btn layui-btn-primary layui-btn-sm">
                    <i class="layui-icon">&#xe669;</i>重置
                </button>
            </div>
        </form>
    </blockquote>
    <div class="layui-btn-group">
        <button type="button" class="layui-btn layui-btn-sm" @click="add">增加</button>
        <button type="button" class="layui-btn layui-btn-sm">编辑</button>
        <button type="button" class="layui-btn layui-btn-sm layui-btn-danger">
            <i class="layui-icon">&#xe640;</i>删除
        </button>
    </div>
    <table id="tableGrid" lay-filter="usersList"></table>
    <script type="text/html" id="usersListBar">
        <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
        <a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="del">删除</a>
    </script>
</div>

<script>

    const context_path = '${APP_PATH}';

    let vm = new Vue({
            el: '#yclCheck',
            data: {
                aa: '测试'
            },
            methods: {
                add() {
                    console.log("ADD")
                }
            },
            created() {

            },
            mounted() {
            }

        })

    ;!function () {
        const table = layui.table;
        table.render({
            elem: '#tableGrid'
            , height: 312
            , url: context_path + '/ycl/operation/check/list'
            , page: true
            , cols: [[
                {field: 'id', title: 'ID', width: 80, sort: true, fixed: 'left'}
                , {field: 'username', title: '用户名', width: 80}
                , {field: 'sex', title: '性别', width: 80, sort: true}
                , {field: 'city', title: '城市', width: 80}
                , {field: 'sign', title: '签名', width: 177}
                , {field: 'experience', title: '积分', width: 80, sort: true}
                , {field: 'score', title: '评分', width: 80, sort: true}
                , {field: 'classify', title: '职业', width: 80}
                , {field: 'wealth', title: '财富', width: 135, sort: true}
            ]]
        });

    }();
</script>