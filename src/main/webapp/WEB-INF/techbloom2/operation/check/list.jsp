<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="inventory_list_grid-div">
    <form id="inventory_list_hiddenForm" action="<%=path%>/warehouselist/materialExcel" method="POST"
          style="display: none;">
        <input name="ids" id="list_ids" value=""/>
        <input name="queryFactoryCode" id="list_queryFactoryCode" value="">
        <input name="queryCode" id="list_queryCode" value="">
        <input name="queryName" id="list_queryName" value="">
        <input name="queryExportExcelIndex" id="list_queryExportExcelIndex" value=""/>
    </form>
    <form id="inventory_list_hiddenQueryForm" style="display:none;">
        <input name="tranCode" value=""/>
        <input name="startTime" value=""/>
        <input name="endTime" value=""/>
    </form>
        <div class="query_box" id="list_yy" title="查询选项">
            <form id="inventory_list_queryForm" style="max-width:100%;">
                <ul class="form-elements">
                    <li class="field-group field-fluid3">
                        <label class="inline" for="inventory_list_tranCode" style="margin-right:20px;width: 100%;">
                            <span class="form_label" style="width:80px;">调拨单号：</span>
                            <input id="inventory_list_tranCode" name="tranCode" type="text"
                                   style="width: calc(100% - 85px);" placeholder="">
                        </label>
                    </li>
                    <li class="field-group field-fluid3">
                        <label class="inline" for="inventory_list_startTime" style="margin-right:20px;width:100%;">
                            <span class="form_label" style="width:80px;">创建时间起：</span>
                            <input type="text" class="form-control date-picker" id="inventory_list_startTime" name="startTime" value="" style="width: calc(100% - 85px);" placeholder="创建时间起" />
                        </label>
                    </li>
                    <li class="field-group field-fluid3">
                        <label class="inline" for="inventory_list_endTime" style="margin-right:20px;width:100%;">
                            <span class="form_label" style="width:80px;">创建时间止：</span>
                            <input type="text" class="form-control date-picker" id="inventory_list_endTime" name="endTime" value="" style="width: calc(100% - 85px);" placeholder="创建时间止" />
                        </label>
                    </li>
                </ul>
                <div class="field-button" style="">
                    <div class="btn btn-info" onclick="inventory_list_queryOk();">
                        <i class="ace-icon fa fa-check bigger-110"></i>查询
                    </div>
                    <div class="btn" onclick="inventory_list_reset();"><i class="ace-icon icon-remove"></i>重置</div>
                </div>
            </form>
        </div>
    <div id="list_fixed_tool_div" class="fixed_tool_div">
    <div id="storage_inventory_list___toolbar__" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="storage_inventory_list_grid-table" style="width:100%;height:100%;"></table>
    <div id="list_grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
    var list_oriData;
    var list_grid;
    var list_dynamicDefalutValue = "cbe154ed57f14f8e803c4d0982a4d18c";
    var list_exportExcelIndex;
    var inventory_list_factoryCodeId;

    $("input").keypress(function (e) {
        if (e.which == 13) {
            inventory_list_queryOk();
        }
    });

    //时间控件
    $(".date-picker").datetimepicker({
        format: "YYYY-MM-DD HH:mm:ss" ,
        autoclose : true,
        todayHighlight : true
    });
    $(function () {
        $(".toggle_tools").click();
    });

    $("#storage_inventory_list___toolbar__").iToolBar({
        id: "list___tb__01",
        items: [
            {
                label: "添加",
                onclick: list_openAddPage,
                iconClass: 'glyphicon glyphicon-plus'
            },
            {
                label: "编辑",
                onclick: list_openEditPage,
                iconClass: 'glyphicon glyphicon-pencil'
            },
            {
                label: "详情",
                onclick: list_openEditPage,
                iconClass: 'glyphicon glyphicon-pencil'
            }
        ]
    });

    var list_queryForm_data = iTsai.form.serialize($("#list_queryForm"));

    list_grid = jQuery("#storage_inventory_list_grid-table").jqGrid({
        url: context_path + "/wms2/operation/inventory/list.do",
        datatype: "json",
        colNames: ["主键", "调拨单号", "调出厂区", "调出仓库", "调入厂区", "调入仓库", "状态", "创建日期", "创建人"],
        colModel: [
            {name: "id", index: "a.id", hidden: true},
            {name: "tranCode", index: "a.tranCode", width: 30},
            {name: "formArea", index: "a.formArea", width: 60},
            {name: "formHouse", index: "a.formHouse", width: 30},
            {name: "toArea", index: "a.toArea", width: 60},
            {name: "toHouse", index: "a.toHouse", width: 60},
            {name: "state", index: "a.state", width: 30},
            {name: "createTime", index: "a.createTime", width: 60},
            {name: "createUser", index: "a.createUser", width: 60}
        ],
        rowNum: 20,
        rowList: [10, 20, 30],
        pager: "#list_grid-pager",
        sortname: "a.id",
        sortorder: "asc",
        altRows: true,
        viewrecords: true,
        autowidth: true,
        multiselect: true,
        multiboxonly: true,
        beforeRequest: function () {
            dynamicGetColumns(list_dynamicDefalutValue, "goods_list_grid-table", $(window).width() - $("#sidebar").width() - 7);
            //重新加载列属性
        },
        loadComplete: function (data) {
            var table = this;
            setTimeout(function () {
                updatePagerIcons(table);
                enableTooltips(table);
            }, 0);
            list_oriData = data;
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext: "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
    });

    jQuery("#storage_inventory_list_grid-table").navGrid("#list_grid-pager", {
        edit: false,
        add: false,
        del: false,
        search: false,
        refresh: false
    })
        .navButtonAdd("#list_grid-pager", {
            caption: "",
            buttonicon: "fa fa-refresh green",
            onClickButton: function () {
                $("#storage_inventory_list_grid-table").jqGrid("setGridParam", {
                    postData: {queryJsonString: ""} //发送数据
                }).trigger("reloadGrid");
            }
        }).navButtonAdd("#list_grid-pager", {
        caption: "",
        buttonicon: "fa icon-cogs",
        onClickButton: function () {
            jQuery("#storage_inventory_list_grid-table").jqGrid("columnChooser", {
                done: function (perm, cols) {
                    dynamicColumns(cols, list_dynamicDefalutValue);
                    $("#storage_inventory_list_grid-table").jqGrid("setGridWidth", $("#list_grid-div").width());
                    list_exportExcelIndex = perm;
                }
            });
        }
    });

    $(window).on("resize.jqGrid", function () {
        $("#storage_inventory_list_grid-table").jqGrid("setGridWidth", $(window).width() - $("#sidebar").width() - 7);
        $("#storage_inventory_list_grid-table").jqGrid("setGridHeight", $(".container-fluid").height() - 10 - $("#list_yy").outerHeight(true) -
            $("#list_fixed_tool_div").outerHeight(true) - $("#list_grid-pager").outerHeight(true) -
            $("#gview_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
    });
    $(window).triggerHandler("resize.jqGrid");

    var inventory_list_queryForm_data = iTsai.form.serialize($("#inventory_list_queryForm"));
    //重置
    function inventory_list_reset(){
        inventory_list_factoryCodeId='';
        $("#inventory_list_queryForm #inventory_list_warehouse").select2("val","");
        $("#inventory_list_queryForm #inventory_list_factoryCode").select2("val","");
        iTsai.form.deserialize($("#inventory_list_queryForm"),inventory_list_queryForm_data);
        inventory_list_queryByParam(inventory_list_queryForm_data);
    }
    /**打开添加页面*/
    function list_openAddPage(){
        $.post(context_path + "/wms2/operation/inventory/toAdd.do", {}, function (str){
            $queryWindow=layer.open({
                title : "废品出库单",
                type:1,
                skin : "layui-layer-molv",
                area : "1000px",
                shade : 0.6, //遮罩透明度
                moveType : 1, //拖拽风格，0是默认，1是传统拖动
                anim : 2,
                content : str,
                success: function (layero, index) {
                    layer.closeAll('loading');
                }
            });
        });
    }

    /**打开编辑页面*/
    function list_openEditPage(){
        var selectAmount = getGridCheckedNum("#storage_inventory_list_grid-table");
        if(selectAmount==0){
            layer.msg("请选择一条记录！",{icon:2});
            return;
        }else if(selectAmount>1){
            layer.msg("只能选择一条记录！",{icon:8});
            return;
        }
        layer.load(2);
        $.post(context_path+'/wms2/operation/inventory/toView.do', {
            id:jQuery("#storage_inventory_list_grid-table").jqGrid("getGridParam", "selrow")
        }, function(str){
            $queryWindow = layer.open({
                title : "仓库编辑",
                type: 1,
                skin : "layui-layer-molv",
                area : "1000px",
                shade: 0.6, //遮罩透明度
                moveType: 1, //拖拽风格，0是默认，1是传统拖动
                content: str,//注意，如果str是object，那么需要字符拼接。
                success:function(layero, index){
                    layer.closeAll("loading");
                }
            });
        }).error(function() {
            layer.closeAll();
            layer.msg("加载失败！",{icon:2});
        });
    }

    /**
     * 查询按钮点击事件
     */
    function inventory_list_queryOk() {
        var queryParam = iTsai.form.serialize($("#inventory_list_queryForm"));
        //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
        inventory_list_queryByParam(queryParam);
    }

    function inventory_list_queryByParam(jsonParam) {
        iTsai.form.deserialize($("#inventory_list_hiddenQueryForm"), jsonParam);
        var queryParam = iTsai.form.serialize($("#inventory_list_hiddenQueryForm"));
        var queryJsonString = JSON.stringify(queryParam);
        $("#storage_inventory_list_grid-table").jqGrid("setGridParam",
            {
                postData: {queryJsonString: queryJsonString}
            }
        ).trigger("reloadGrid");
    }

    $("#inventory_list_queryForm #inventory_list_warehouse").select2({
        placeholder: "选择仓库",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/car/selectWarehouse",
            type: "POST",
            dataType: 'json',
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    factoryCodeId:inventory_list_factoryCodeId
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });

    //厂区
    $("#inventory_list_queryForm #inventory_list_factoryCode").select2({
        placeholder: "选择厂区",
        minimumInputLength:0,   //至少输入n个字符，才去加载数据
        allowClear: true,  //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches:"没有结果",
        formatSearching:"搜索中...",
        formatAjaxError:"加载出错啦！",
        ajax : {
            url: context_path+"/factoryArea/getFactoryList",
            type:"POST",
            dataType : 'json',
            delay : 250,
            data: function (term,pageNo) {     //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term,    //联动查询的字符
                    pageSize: 15,    //一次性加载的数据条数
                    pageNo:pageNo    //页码
                }
            },
            results: function (data,pageNo) {
                var res = data.result;
                if(res.length>0){   //如果没有查询到数据，将会返回空串
                    var more = (pageNo*15)<data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results:res,more:more
                    };
                }else{
                    return {
                        results:{}
                    };
                }
            },
            cache : true
        }
    });

    $("#inventory_list_queryForm #inventory_list_factoryCode").on("change",function(e){
        inventory_list_factoryCodeId = $("#inventory_list_queryForm #inventory_list_factoryCode").val();
    });

    $("#inventory_list_queryForm #goods_list_factoryCode").on("change.select2",function(){
        $("#inventory_list_queryForm #inventory_list_factoryCode").trigger("keyup")}
    );
</script>
