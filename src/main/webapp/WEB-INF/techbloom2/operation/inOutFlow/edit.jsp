<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<head>
    <link rel="stylesheet" href="${APP_PATH}/static/layui/css/layui.css" type="text/css"/>
</head>
<script src="${APP_PATH}/static/layui/layui.all.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>


<div id="yclPurchaseOrderEdit">

    <form class="layui-form">

        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">上下架单号</label>
                <div class="layui-input-inline">
                    <input type="tel" name="inCode" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">批次号</label>
                <div class="layui-input-inline">
                    <input type="text" name="batchCode" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">物料编号</label>
                <div class="layui-input-inline">
                    <input type="text" name="materialCode" autocomplete="off" class="layui-input">
                </div>
            </div>
        </div>

        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">物料名称</label>
                <div class="layui-input-inline">
                    <input type="tel" name="materialName" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">仓库</label>
                <div class="layui-input-inline">
                    <input type="text" name="houseCode" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">库位编码</label>
                <div class="layui-input-inline">
                    <input type="text" name="locationCode" autocomplete="off" class="layui-input">
                </div>
            </div>
        </div>

        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">收退货数量</label>
                <div class="layui-input-inline">
                    <input type="tel" name="orderQuality" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">出入库数量</label>
                <div class="layui-input-inline">
                    <input type="text" name="actualQuality" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">单位</label>
                <div class="layui-input-inline">
                    <input type="text" name="unit" autocomplete="off" class="layui-input">
                </div>
            </div>
        </div>

        <div class="layui-form-item">
            <div class="layui-input-block">
                <button type="submit" class="layui-btn" lay-submit lay-filter="submitBtn">提交</button>
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
        </div>
    </form>

</div>

<script>

    const context_path = '${APP_PATH}';

    const form = layui.form,
        layer = layui.layer,
        $ = layui.jquery;

    let vm = new Vue({
        el: '#yclPurchaseOrderEdit',
        data: {},
        methods: {},
        created() {
        },
        mounted() {
            form.render();
        }

    })

    form.on('submit(submitBtn)', function (data) {
        let params = data.field;
        console.log(params)
        console.log(layer)
        layer.msg(params);
        // let res = $.ajax({
        //     type: 'POST',
        //     url: context_path + "/ycl/operation/yclInOutFlow/save",
        //     contentType: "application/json",
        //     async: false,
        //     dataType: "json",
        //     data: JSON.stringify(params)
        // success: function (res) {
        //     layer.msg(res);
        //     console.log(res)
        // if (res.result === false) {
        //     layer.msg(res.msg, {icon: 5});
        //     return false;
        // } else {
        //     layer.msg(res.msg, {icon: 5});
        //     layer.closeAll();
        //     //刷新父页面
        //     // parent.location.reload();
        // }
        // }
        // })
        // layer.msg(res);
        // console.log(res)
    });

</script>
