<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <style>
        th, td {
            padding: 6px;
        }

        tr {
            height: 30px;
        }

        .info td {
            border-right: 1px solid #000000;
            border-bottom: 1px solid #000000;
        }

        .head td {
            border-top: 1px solid #000000;
            border-bottom: 1px solid #000000;
        }
    </style>
</head>
<body>

<div style="padding: 10px;width: 98%">
    <div id="yclTransferOrderPrint">
        <div id="div1">
            <div style="width: 100%;text-align: center;font-size: 20px;font-weight: bold;padding-top: 15px;">
                远东电缆有限公司({{titleText}})
            </div>
            <table style="width: 100%;font-size: 14px;">
                <tbody>
                <tr>
                    <td>退料单号：{{billno}}</td>
                </tr>
                </tbody>
            </table>
        </div>
        <div id="div2">
            <table class="info"
                   style="border-collapse:collapse;border-left: 1px solid #000000;border-spacing:0px;font-size: 14px;">
                <tbody>
                <tr class="head">
                    <td width="60">物料编码</td>
                    <td width="100">物料描述</td>
                    <td width="50">单位</td>
                    <td width="50">数量</td>
                    <td width="80">生产厂</td>
                    <td width="80">库位</td>
                    <td width="80">批次</td>
                    <td width="80">处理日期</td>
                    <td width="30">备注</td>
                </tr>
                <tr v-for="en in tableList">
                    <td>{{en.itemname}}</td>
                    <td>{{en.itemdesc}}</td>
                    <td>{{en.uom}}</td>
                    <td>{{en.inQuantity}}</td>
                    <td>{{en.shopDesc}}</td>
                    <td>{{en.locatorCode}}</td>
                    <td>{{en.lotno}}</td>
                    <td>{{en.createTime}}</td>
                    <td>{{en.remark}}</td>
                </tr>
                </tbody>
            </table>
        </div>
        <div id="div3">
            <table style="width: 100%;font-size: 14px;">
                <tbody>
                <tr>
                    <td width="25%">经办人：</td>
                    <td width="25%">复核人：</td>
                    <td width="25%">制单人：${operator}</td>
                    <td width="25%">制单日期：{{ymd}}</td>
                </tr>
                <tr>
                    <td width="25%"></td>
                    <td width="25%"></td>
                    <td width="25%"></td>
                    <td width="25%" style="text-align: right">第1页/共1页</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

</div>
<script src="/wms/plugins/public_components/js/jquery-2.1.4.js"></script>
<script src="/wms/static/layui/vue.js"></script>
<script>
    const context_path = '${APP_PATH}';
    Date.prototype.Format = function (fmt) { //author: meizz
        var o = {
            "M+": this.getMonth() + 1, //月份
            "d+": this.getDate(), //日
            "H+": this.getHours(), //小时
            "m+": this.getMinutes(), //分
            "s+": this.getSeconds(), //秒
            "q+": Math.floor((this.getMonth() + 3) / 3), //季度
            "S": this.getMilliseconds() //毫秒
        };
        if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (var k in o)
            if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        return fmt;
    };
    let vm = new Vue({
        el: '#yclTransferOrderPrint',
        data: {
            lineId: '${lineId}',
            billno: '${billno}',
            titleText: '生产退料单',
            ymd: new Date().Format("yyyy-MM-dd"),
            tableList: []
        },
        methods: {
            getPrintData(lineId) {
                $.ajax({
                    type: 'GET',
                    url: context_path + "/ycl/material/back/printData",
                    data: { lineId: lineId },
                    async: false,
                    dataType: "json",
                    success: res => {
                        this.tableList = res.data;
                        console.info(this.tableList)
                    }
                });
            }
        },
        created() {
            this.getPrintData(this.lineId);
        },
    });
</script>
</body>
</html>
