<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
%>
<div id="car_edit_page" class="row-fluid" style="height: inherit;">
    <form id="car_edit_carForm" class="form-horizontal" style="overflow: auto; height: calc(100% - 70px);">
        <div class="control-group">
            <div class="controls">
                <label for="productionDate">按生产日期先进先出：</label>
                <input class="span11" style="width: 50px;" type="radio" name="inAndOut" id="productionDate"
                       value="productionDate"/>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <label for="inDate">按入库日期先进先出：</label>
                <input class="span11" style="width: 50px;" type="radio" name="inAndOut" id="inDate" value="inDate"/>
            </div>
        </div>
        。
        <div class="control-group">
            <div class="controls">
                <label for="car_edit_name">允许多个批次入库：</label>
                <input type="checkbox" style="width: 50px;" class="span11" id="car_edit_name" name="isBatch" value="1"/>
            </div>
        </div>
    </form>
    <div class="field-button" style="text-align: center;border-top: 0px;margin: 15px auto;">
		<span class="btn btn-info" onclick="saveForm();">
		   <i class="ace-icon fa fa-check bigger-110"></i>保存
		</span>
        <span class="btn btn-danger" onclick="layer.closeAll();">
		   <i class="icon-remove"></i>&nbsp;取消
		</span>
    </div>
</div>

<script type="text/javascript" src="<%=path%>/static/js/techbloom/selectMultip.js"></script>
<script type="text/javascript">
    let factoryCodeId = $("#car_edit_carForm #car_edit_factoryCode").val();
    //渲染多选框
    selectMultip.register();

    //确定按钮点击事件
    function saveForm() {
        if ($("#car_edit_carForm").valid()) {
            saveInfo($("#car_edit_carForm").serialize());
        }
    }

    //保存/修改用户信息
    function saveInfo(bean) {
        console.log(bean)
        $.ajax({
            url: context_path + "/car/save",
            type: "POST",
            data: bean,
            dataType: "JSON",
            success: function (data) {
                iTsai.form.deserialize($("#car_list_hiddenForm"), iTsai.form.serialize($("#car_list_hiddenQueryForm")));
                var queryParam = iTsai.form.serialize($("#car_list_hiddenQueryForm"));
                var queryJsonString = JSON.stringify(queryParam);
                if (Boolean(data.result)) {
                    layer.msg("保存成功！", {icon: 1});
                    //关闭当前窗口
                    layer.close($queryWindow);
                    //刷新列表
                    $("#car_list_grid-table").jqGrid('setGridParam', {
                        postData: {queryJsonString: queryJsonString}
                    }).trigger("reloadGrid");
                } else {
                    layer.alert("保存失败，请稍后重试！", {icon: 2});
                }
            },
            error: function (XMLHttpRequest) {
                alert(XMLHttpRequest.readyState);
                alert("出错啦！！！");
            }
        });
    }

    //厂区下拉框初始化
    $("#car_edit_carForm #car_edit_factoryCode").select2({
        placeholder: "选择部门",
        minimumInputLength: 0,   //至少输入n个字符，才去加载数据
        allowClear: true,  //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/factoryArea/getFactoryList",
            type: "POST",
            dataType: 'json',
            delay: 250,
            data: function (term, pageNo) {     //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term,    //联动查询的字符
                    pageSize: 15,    //一次性加载的数据条数
                    pageNo: pageNo    //页码
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) {   //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res, more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }

    });


    $("#car_edit_carForm #car_edit_factoryCode").on("change", function (e) {
        factoryCodeId = $("#car_edit_carForm #car_edit_factoryCode").val();
    });

    //点击编辑的默认数据
    if ($("#car_edit_carForm #car_edit_factoryid").val() != "") {
        $("#car_edit_carForm #car_edit_factoryCode").select2("data", {
            id: $("#car_edit_carForm #car_edit_factoryid").val(),
            text: $("#car_edit_carForm #car_edit_factoryno").val()
        });
    }

</script>