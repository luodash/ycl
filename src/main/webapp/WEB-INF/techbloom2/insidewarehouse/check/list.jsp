<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<div id="yclCheckPage" style="margin: 15px">
    <%--query tools--%>
    <blockquote class="layui-elem-quote">
        <form class="layui-form">
            <div class="layui-fluid">
                <div class="layui-row layui-col-space10">
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">厂区</label>
                            <div class="layui-input-inline">
                                <select id="areaCodeSelectCheck" name="areaCode" lay-filter="areaCodeSelectCheckFilter">
                                    <option value="">请选择</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">仓库</label>
                            <div class="layui-input-inline">
                                <select id="storeCodeSelectCheck" name="storeCode" lay-filter="storeCodeSelect">
                                    <option value="">请选择</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">创建日期</label>
                            <div class="layui-input-inline">
                                <input type="text" id="createrTimeCheckList" class="layui-input"
                                       v-model="queryParams.createrTime"/>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <button type="button" class="layui-btn layui-btn-sm layui-btn-normal"
                                    @click="queryPageView">
                                <i class="layui-icon">&#xe615;</i>查询
                            </button>
                        </div>
                        <div class="layui-inline">
                            <button type="button" class="layui-btn layui-btn-primary layui-btn-sm"
                                    @click="restQueryParams">
                                <i class="layui-icon">&#xe669;</i>重置
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </blockquote>

    <div id="toolbarCheckPage">
        <button type="button" @click="addInventoryOrder" class="layui-btn layui-btn-sm layui-btn-normal">
            <i class="layui-icon">&#xe654;</i>新建
        </button>
        <button type="button" @click="deleteInventoryOrder" class="layui-btn layui-btn-sm layui-btn-danger">
            <i class="layui-icon">&#xe640;</i>删除
        </button>
        <button type="button" @click="detailInventoryOrder" class="layui-btn layui-btn-sm layui-btn-normal">
            <i class="layui-icon">&#xe702;</i>详情
        </button>
    </div>

    <%-- table grid --%>
    <table id="tableGridCheck" lay-filter="tableGridFilterCheck"></table>

</div>

<script>

    const context_path = '${APP_PATH}';
    const table = layui.table,
        form = layui.form,
        layer = layui.layer,
        laydate = layui.laydate;

    let vm = new Vue({
        el: '#yclCheckPage',
        data: {
            queryParams: {
                areaCode: "",
                storeCode: "",
                kssj: "",
                jssj: ""
            },
            areaList: [],
            storeList: []
        },
        methods: {
            initAreaSelectCheck() {
                $("#areaCodeSelectCheck").empty();
                $("#areaCodeSelectCheck").append('<option value="">请选择</option>');
                $.get(context_path + '/ycl/baseinfo/basedata/area/0'
                    , function (res) {
                        let html = '';
                        let list = res.data;
                        for (let i = 0; i < list.length; i++) {
                            html += '<option value=' + list[i].id + '>' + list[i].text + '</option>';
                        }
                        $("#areaCodeSelectCheck").append(html)
                        form.render('select');
                    })
            },
            initStoreSelectCheck() {
                $("#storeCodeSelectCheck").empty();
                $("#storeCodeSelectCheck").append('<option value="">请选择</option>');
                $.get(context_path + '/ycl/baseinfo/basedata/store/' + vm.queryParams.areaCode
                    , function (res) {
                        if (res.result) {
                            let html = '';
                            let list = res.data;
                            for (let i = 0; i < list.length; i++) {
                                html += '<option value=' + list[i].code + '>' + list[i].text + '</option>';
                            }
                            $("#storeCodeSelectCheck").append(html);
                        }
                        form.render('select');
                    })
            },
            initTableIns() {
                table.render({
                    elem: '#tableGridCheck'
                    , url: context_path + '/ycl/insidewarehouse/check/list'
                    , page: true
                    , id: "tableGridSetCheck"
                    , size: 'sm'
                    , cols: [[
                        {type: 'checkbox'}
                        , {
                            field: 'state', title: '状态', width: 70, templet: function (d) {
                                if (d.state == 1) {
                                    return '<span style="color: #c00;">进行中</span>'
                                } else if (d.state == 2) {
                                    return '<span style="color: #c00;">完成</span>'
                                }
                            }
                        }
                        , {field: 'checkCode', title: '盘库单号', width: 160}
                        , {field: 'storeName', title: '仓库'}
                        , {field: 'createrTime', title: '创建日期'}
                        , {field: 'updateTime', title: '盘库日期'}
                        , {field: 'createrUser', title: '责任人'}
                        , {field: 'operator', title: '盘点人员'}
                        , {field: 'remark', title: '盘点说明'}
                    ]]
                });
            },
            queryPageView() {
                console.log(vm.queryParams)
                table.reload('tableGridSetCheck', {
                    where: vm.queryParams
                });
            },
            restQueryParams() {
                vm.queryParams = {
                    createrUser: "",
                    storeCode: "",
                    areaCode: "",
                    kssj: "",
                    jssj: ""
                }
                table.reload('tableGridSetCheck', {
                    where: vm.queryParams
                });

                $("#areaCodeSelectCheck").val("");
                $("#storeCodeSelectCheck").val("");
                form.render();
            },

            addInventoryOrder() {
                layer.open({
                    title: '新增'
                    , type: 2
                    , area: ['1200px', '600px'] //宽高
                    , maxmin: true
                    , content: context_path + '/ycl/insidewarehouse/check/toEdit'
                });
            },
            deleteInventoryOrder() {
                const checkStatusData = table.checkStatus('tableGridSetCheck').data
                console.log(checkStatusData)
                let checkCodes = checkStatusData.map(item => item.checkCode).toString()
                if (checkStatusData.length === 0) {
                    layer.msg('至少选择一条数据', {icon: 2});
                    return
                }
                $.get(context_path + '/ycl/insidewarehouse/check/deleteByCheckCodes', {checkCodes}, function (res) {
                    layer.msg(res.msg);
                    table.reload('tableGridSetCheck');
                })
            },
            detailInventoryOrder() {
                const checkStatusData = table.checkStatus('tableGridSetCheck').data
                console.log(checkStatusData)
                // let checkCodes = checkStatusData.map(item => item.checkCode).toString()
                if (checkStatusData.length === 0) {
                    layer.msg('请选择一条数据', {icon: 2});
                    return
                }
                if (checkStatusData.length > 1) {
                    layer.msg('只能选择一条数据', {icon: 2});
                    return
                }
                let checkCode = checkStatusData[0].checkCode;
                console.log(checkCode)
                layer.open({
                    title: '详情'
                    , type: 2
                    , area: ['1200px', '600px'] //宽高
                    , maxmin: true
                    , content: context_path + '/ycl/insidewarehouse/check/toDetail?checkCode=' + checkCode
                });
            }
        },
        created() {
        },
        mounted() {
        }

    })

    //监听select
    form.on('select(areaCodeSelectCheckFilter)', function (data) {
        vm.queryParams.areaCode = data.value
        vm.initStoreSelectCheck()
    });
    //监听select
    form.on('select(storeCodeSelect)', function (data) {
        vm.queryParams.storeCode = data.value
    });

    function tableRender() {
        table.reload('tableGridSetCheck', {
            where: vm.queryParams
        });
    }

    // init
    ;!function () {

        laydate.render({
            elem: '#createrTimeCheckList'
            , type: 'date'
            , range: '~'
            , done: function (value, date, endDate) {
                let dateTime = value.split('~')
                vm.queryParams.kssj = dateTime[0]
                vm.queryParams.jssj = dateTime[1]
            }
        });

        vm.initAreaSelectCheck()
        vm.initTableIns()

    }();
</script>