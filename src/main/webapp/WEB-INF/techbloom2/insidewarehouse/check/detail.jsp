<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<head>
    <link rel="stylesheet" href="${APP_PATH}/static/layui/css/layui.css" type="text/css"/>
</head>
<script src="${APP_PATH}/static/layui/layui.all.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>


<div id="yclCheckOrderDetail" style="margin: 15px">

    <blockquote class="layui-elem-quote">
        <div class="layui-fluid">
            <div class="layui-row">
                <div class="layui-col-sm3">
                    <div class="layui-inline">
                        <label style="width: 65px">盘库单号</label>
                        <div class="layui-input-inline">
                            <input type="text" class="layui-input" disabled value="${yclCheckEntity.checkCode}"/>
                        </div>
                    </div>
                </div>
                <div class="layui-col-sm3">
                    <div class="layui-inline">
                        <label style="width: 65px">盘点仓库</label>
                        <div class="layui-input-inline">
                            <input type="text" class="layui-input" disabled value="${yclCheckEntity.storeCode}"/>
                        </div>
                    </div>
                </div>
                <div class="layui-col-sm3">
                    <div class="layui-inline">
                        <label style="width: 65px">盘库日期</label>
                        <div class="layui-input-inline">
                            <input type="text" class="layui-input" disabled
                                   value="<fmt:formatDate value="${yclCheckEntity.createrTime}" pattern="yyyy-MM-dd HH:mm:ss"/>"/>
                        </div>
                    </div>
                </div>
                <div class="layui-col-sm3">
                    <div class="layui-inline">
                        <label style="width: 65px">审核日期</label>
                        <div class="layui-input-inline">
                            <input type="text" class="layui-input" disabled
                                   value="<fmt:formatDate value="${yclCheckEntity.updateTime}" pattern="yyyy-MM-dd HH:mm:ss"/>"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-row">
                <div class="layui-col-sm3">
                    <div class="layui-inline">
                        <label style="width: 65px">责任人员</label>
                        <div class="layui-input-inline">
                            <input type="text" class="layui-input" disabled value="${yclCheckEntity.createrUser}"/>
                        </div>
                    </div>
                </div>
                <div class="layui-col-sm3">
                    <div class="layui-inline">
                        <label style="width: 65px">盘库人员</label>
                        <div class="layui-input-inline">
                            <input type="text" class="layui-input" disabled value="${yclCheckEntity.operator}"/>
                        </div>
                    </div>
                </div>
                <div class="layui-col-sm3">
                    <div class="layui-inline">
                        <label style="width: 65px">盘库说明</label>
                        <div class="layui-input-inline">
                            <input type="text" class="layui-input" disabled value="${yclCheckEntity.remark}"/>
                        </div>
                    </div>
                </div>
                <div class="layui-col-sm3">
                    <div class="layui-inline">
                        <label style="width: 65px">审核人员</label>
                        <div class="layui-input-inline">
                            <input type="text" class="layui-input" disabled value="${yclCheckEntity.updateUser}"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </blockquote>

    <div id="toolbarCheckDetail">
        <button type="button" @click="printBill" class="layui-btn layui-btn-sm layui-btn-normal">
            <i class="layui-icon">&#xe654;</i>打印单据
        </button>
        <button type="button" @click="audit" class="layui-btn layui-btn-sm layui-btn-danger">
            <i class="layui-icon">&#xe605;</i>审核
        </button>
    </div>

    <table id="tableGridCheckDetail" lay-filter="tableGridFilterCheckDetail"></table>

</div>

<script>

    const context_path = '${APP_PATH}';
    let checkCode = '${checkCode}';
    let yclCheckEntity = '${yclCheckEntity}';
    let yclCheckState = '${yclCheckEntity.state}';
    let checkId = '${yclCheckEntity.id}';
    const form = layui.form,
        table = layui.table,
        $ = layui.jquery;

    let vm = new Vue({
        el: '#yclCheckOrderDetail',
        data: {
            checkItem: yclCheckEntity
        },
        methods: {
            printBill() {
            },
            audit() {
                if (yclCheckState == 2) {
                    layer.msg("审核已完成，无需重复审核！", {icon: 5});
                    return
                }
                layer.confirm('是否通过盘库审核？', {
                    btn: ['通过', '重盘']
                }, function () {
                    $.ajax({
                        type: 'GET',
                        url: context_path + "/ycl/insidewarehouse/check/audit",
                        contentType: "application/json",
                        async: false,
                        dataType: "json",
                        data: {id: checkId},
                        success: function (res) {
                            if (res.result === false) {
                                layer.msg(res.msg, {icon: 5});
                                return false;
                            } else {
                                layer.msg(res.msg, {icon: 1});
                                window.parent.tableRender()
                                let index = parent.layer.getFrameIndex(window.name);
                                parent.layer.close(index);
                            }
                        }
                    })
                });
            }
        },
        created() {
        },
        mounted() {
            form.render();
            table.render({
                elem: '#tableGridCheckDetail'
                , url: context_path + '/ycl/insidewarehouse/check/detailList?checkCode=' + checkCode
                , id: "tableGridSetCheck"
                , size: 'sm'
                , cols: [[
                    // {type: 'checkbox'}
                    {
                        field: 'isWeigh', title: '是否称重', width: 85, templet: function (d) {
                            if (d.isWeigh == 1) {
                                return '<span style="color: #c00;">需要称重</span>'
                            } else {
                                return ''
                            }
                        }
                    }
                    , {field: 'locatorCode', title: '库位编码'}
                    , {field: 'materialCode', title: '物料编码'}
                    , {field: 'materialName', title: '物料名称'}
                    , {field: 'lotsNum', title: '批次号'}
                    , {field: 'primaryUnit', title: '单位'}
                    , {field: 'checkQuality', title: '实盘数'}
                    , {field: 'quality', title: '系统数量'}
                ]]
            });
        }

    })

</script>
