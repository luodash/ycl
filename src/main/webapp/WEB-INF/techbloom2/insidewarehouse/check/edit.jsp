<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<head>
    <link rel="stylesheet" href="${APP_PATH}/static/layui/css/layui.css" type="text/css"/>
    <style type="text/css">
        .layui-input {
            height: 30px;
        }
    </style>
</head>
<script src="${APP_PATH}/static/layui/layui.all.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>


<div id="yclCheckOrderEdit" style="margin: 15px">

    <blockquote class="layui-elem-quote">
        <form class="layui-form">
            <div class="layui-fluid">
                <div class="layui-row layui-col-space10">
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">厂区</label>
                            <div class="layui-input-inline">
                                <select id="areaCodeSelectCheckEdit" lay-filter="areaCodeSelectCheckEditFilter">
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">仓库</label>
                            <div class="layui-input-inline">
                                <select id="storeCodeSelectCheckEdit" lay-filter="storeCodeSelectCheckEditFilter">
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">盘库人员</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" v-model="params.checkEntity.operator"/>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <button type="button" class="layui-btn layui-btn-sm" @click="submitCheck">
                                <i class="layui-icon">&#xe605;</i>提交
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </blockquote>

    <div id="toolbar">
        <button type="button" @click="del()" class="layui-btn layui-btn-sm layui-btn-danger">
            <i class="layui-icon">&#xe640;</i>删除
        </button>
        <div class="layui-inline" style="margin-left: 10px;">
            <label style="width: 65px">物料编码：</label>
            <div class="layui-form layui-input-inline">
                <select id="inventorySelect" lay-filter="inventorySelectFilter" lay-search="">
                    <option value="">请选择</option>
                </select>
            </div>
        </div>
        <button type="button" @click="add" class="layui-btn layui-btn-sm layui-btn-normal">
            <i class="layui-icon">&#xe654;</i>添加
        </button>
        <div class="layui-inline" style="margin-left: 10px;">
            <label style="width: 65px">库位编码：</label>
            <div class="layui-form layui-input-inline">
                <input id="locatorCodeId" type="text" class="layui-input"/>
            </div>
        </div>
        <button type="button" @click="addFromLocator" class="layui-btn layui-btn-sm layui-btn-normal">
            <i class="layui-icon">&#xe654;</i>添加
        </button>
        <button type="button" @click="addFromAllStore" class="layui-btn layui-btn-sm layui-btn-normal">
            <i class="layui-icon">&#xe654;</i>全仓库盘点
        </button>
    </div>

    <table id="tableGridCheckEdit" lay-filter="tableGridFilterCheckEdit"></table>

</div>

<script>

    const context_path = '${APP_PATH}';
    const form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        table = layui.table,
        $ = layui.jquery;

    let vm = new Vue({
        el: '#yclCheckOrderEdit',
        data: {
            params: {
                checkEntity: {
                    areaCode: '',
                    areaName: '',
                    storeCode: '',
                    storeName: '',
                    operator: '',
                },
                lines: []
            },
            inventoryList: []
        },
        methods: {
            initAreaSelectCheckEdit() {
                $("#areaCodeSelectCheckEdit").empty();
                $("#areaCodeSelectCheckEdit").append('<option value="">请选择</option>');
                $.get(context_path + '/ycl/baseinfo/basedata/area/0'
                    , function (res) {
                        let html = '';
                        let list = res.data;
                        for (let i = 0; i < list.length; i++) {
                            html += '<option value=' + list[i].id + '>' + list[i].text + '</option>';
                        }
                        $("#areaCodeSelectCheckEdit").append(html)
                        form.render('select');
                    })
            },
            initStoreSelectCheckEdit() {
                $("#storeCodeSelectCheckEdit").empty();
                $("#storeCodeSelectCheckEdit").append('<option value="">请选择</option>');
                $.get(context_path + '/ycl/baseinfo/basedata/store/' + vm.params.checkEntity.areaCode
                    , function (res) {
                        if (res.result) {
                            let html = '';
                            let list = res.data;
                            for (let i = 0; i < list.length; i++) {
                                html += '<option value=' + list[i].code + '>' + list[i].text + '</option>';
                            }
                            $("#storeCodeSelectCheckEdit").append(html);
                        }
                        form.render('select');
                    })
            },
            initInventorySelect() {
                $("#inventorySelect").empty();
                $("#inventorySelect").append('<option value="">请选择</option>');
                $.get(context_path + '/ycl/report/inventory/listView', {
                    storeCode: vm.params.checkEntity.storeCode
                }, function (res) {
                    let html = '';
                    let list = res.data;
                    for (let i = 0; i < list.length; i++) {
                        html += '<option value=' + list[i].materialCode + '>' + list[i].materialCode + '</option>';
                    }
                    $("#inventorySelect").append(html);
                    form.render('select');
                })
            },
            initTableIns() {
                table.render({
                    elem: '#tableGridCheckEdit'
                    , id: "tableGridSetCheckEdit"
                    , data: this.inventoryList
                    , size: 'sm'
                    , cols: [[
                        {type: 'checkbox'}
                        , {field: 'materialCode', title: '物料编码'}
                        , {field: 'materialName', title: '物料名称'}
                        , {field: 'lotsNum', title: '批次号'}
                        , {field: 'locatorCode', title: '库位'}
                        , {field: 'primaryUnit', title: '单位'}
                        , {field: 'quality', title: '系统库存数'}
                    ]],
                    done: function (res, curr, count) {
                        this.inventoryList = res.data;
                    }
                });
            },
            formVerify() {
                if (vm.params.checkEntity.areaCode == '') {
                    layer.msg('请选择厂区!', {icon: 2});
                    return false;
                }
                if (this.params.checkEntity.storeCode == '') {
                    layer.msg('请选择调出仓库!', {icon: 2});
                    return false;
                }
                if (this.params.checkEntity.operator == '') {
                    layer.msg('请输入盘库人员!', {icon: 2});
                    return false;
                }
                return true;
            },
            del() {
                var selectData = layui.table.checkStatus('tableGridSetCheckEdit').data;
                console.log(selectData)
                var oldData = table.cache['tableGridSetCheckEdit'];
                console.log(oldData)
                for (var i = 0; i < oldData.length; i++) {
                    if (oldData[i].tempId === selectData[0].tempId) {
                        oldData.splice(i, 1);
                    }
                }
                table.reload('tableGridSetCheckEdit', {
                    data: oldData
                });
            },
            add() {
                if ($('#inventorySelect').val() == '') {
                    layer.msg('请选择库存物料', {icon: 2});
                    return;
                }
                $.get(context_path + '/ycl/report/inventory/listView', {
                    materialCode: $('#inventorySelect').val()
                }, function (res) {
                    let oldData = table.cache['tableGridSetCheckEdit'];
                    console.log(oldData)
                    let newRow = res.data[0];
                    newRow.tempId = new Date().valueOf();
                    oldData.push(newRow);
                    vm.params.lines = oldData
                    table.reload('tableGridSetCheckEdit', {
                        data: oldData
                    });
                })
            },
            addFromLocator() {
                if (!vm.formVerify()) {
                    return;
                }
                $.get(context_path + '/ycl/report/inventory/listView', {
                    areaCode: vm.params.checkEntity.areaCode,
                    storeCode: vm.params.checkEntity.storeCode,
                    locatorCode: $('#locatorCodeId').val(),
                }, function (res) {
                    let oldData = table.cache['tableGridSetCheckEdit'];
                    console.log(oldData)
                    for (let i = 0; i < res.data.length; i++) {
                        let newRow = res.data[i];
                        newRow.tempId = new Date().valueOf() + i;
                        oldData.push(newRow);
                    }
                    vm.params.lines = oldData
                    table.reload('tableGridSetCheckEdit', {
                        data: oldData
                    });
                })
            },
            addFromAllStore() {
                if (!vm.formVerify()) {
                    return;
                }
                $.get(context_path + '/ycl/report/inventory/listView', {
                    areaCode: vm.params.checkEntity.areaCode,
                    storeCode: vm.params.checkEntity.storeCode
                }, function (res) {
                    let oldData = table.cache['tableGridSetCheckEdit'];
                    console.log(oldData)
                    for (let i = 0; i < res.data.length; i++) {
                        let newRow = res.data[i];
                        newRow.tempId = new Date().valueOf() + i;
                        oldData.push(newRow);
                    }
                    vm.params.lines = oldData
                    table.reload('tableGridSetCheckEdit', {
                        data: oldData
                    });
                })
            },
            submitCheck() {
                if (!vm.formVerify()) {
                    return;
                }
                const oldData = table.cache['tableGridSetCheckEdit'];
                if (oldData.length == 0) {
                    layer.msg('盘点库存物料能为空！', {icon: 2});
                    return;
                }
                let b = ''
                if (oldData.length > 3) {
                    for (let i = 0; i < oldData.length; i++) {
                        let a = oldData.length
                        if (i == 0) {
                            let item = oldData[i]
                            item.isWeigh = 1
                            b.concat('【' + item.materialName + '】')
                        }
                        // if (i == a - 1) {
                        if (i == 1) {
                            let item = oldData[i]
                            item.isWeigh = 1
                            b.concat('【' + item.materialName + '】')
                        }
                        // if (i == parseInt(a / 2)) {
                        if (i == oldData.length - 1) {
                            let item = oldData[i]
                            item.isWeigh = 1
                            b.concat('【' + item.materialName + '】等三种物料需要实际称重')
                        }
                    }
                }
                vm.params.lines = oldData
                console.log(b)
                $.ajax({
                    type: 'POST',
                    url: context_path + "/ycl/insidewarehouse/check/save",
                    contentType: "application/json",
                    async: false,
                    dataType: "json",
                    data: JSON.stringify(vm.params),
                    success: function (res) {
                        if (res.result === false) {
                            layer.msg(res.msg + b, {icon: 5});
                            return false;
                        } else {
                            layer.msg(res.msg, {icon: 1});
                            let index = parent.layer.getFrameIndex(window.name);
                            parent.layer.close(index);
                            window.parent.tableRender()
                        }
                    }
                })
            }
        },
        created() {
        },
        mounted() {
            form.render();
            this.$nextTick(() => {
                vm.initTableIns()
            })
        }

    })

    //监听 厂区 select
    form.on('select(areaCodeSelectCheckEditFilter)', function (data) {
        vm.params.checkEntity.areaCode = data.value
        vm.params.checkEntity.areaName = data.elem[data.elem.selectedIndex].text
        vm.initStoreSelectCheckEdit()
    });
    //监听 仓库 select
    form.on('select(storeCodeSelectCheckEditFilter)', function (data) {
        vm.params.checkEntity.storeCode = data.value
        vm.params.checkEntity.storeName = data.elem[data.elem.selectedIndex].text
        vm.initInventorySelect()
    });
    //监听select
    form.on('select(inventorySelectFilter)', function (data) {
        if (!vm.formVerify()) {
            return;
        }
    });

    // init
    ;!function () {
        vm.initAreaSelectCheckEdit()
    }();

</script>
