<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<style type="text/css">

    /*设置 layui 表格中单元格内容溢出可见样式*/
    #divtable .layui-table-view,
    #divtable .layui-table-box,
    #divtable .layui-table-body{overflow: visible;}
    #divtable .layui-table-cell{height: auto; overflow: visible;}

    /*文本对齐方式*/
    .text-center{text-align: center;}
    /* 使得下拉框与单元格刚好合适 */
    td .layui-form-select{
        margin-top: -5px;
        margin-left: -15px;
        margin-right: -15px;
    }
    #divtable .layui-anim-upbit{
        margin-top: -10px;
    }
</style>
<div id="yclMoveOrder" style="margin: 30px">
    <%--query tools--%>
    <blockquote class="layui-elem-quote">
        <form class="layui-form">
            <div class="layui-fluid">
                <div class="layui-row layui-col-space10">
                    <div class="layui-col-sm4">
                        <div class="layui-inline">
                            <label style="width: 65px">移位单号</label>
                            <div class="layui-input-inline">
                                <input type="text" id="tranCode" class="layui-input"
                                       v-model="queryParams.tranCode"/>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm4">
                        <div class="layui-inline">
                            <label style="width: 65px">物料名称</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" v-model="queryParams.materialName"/>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm4">
                        <div class="layui-inline">
                            <label style="width: 65px">创建日期</label>
                            <div class="layui-input-inline">
                                <input type="text" id="createTimeMove" class="layui-input"
                                       v-model="queryParams.createTime"/>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm4">
                        <div class="layui-inline">
                            <label style="width: 65px">移出厂区</label>
                            <div class="layui-form layui-input-inline" lay-filter="fromAreaCodeMove">
                                <select id="fromAreaCodeMove" lay-filter="fromAreaCodeMove" lay-search="">
                                    <option value="">请选择</option>
                                    <option v-for="(item,index) in arealist" :value="item.id" :key="index">{{item.text}}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm4">
                        <div class="layui-inline">
                            <label style="width: 65px">移出仓库</label>
                            <div class="layui-form layui-input-inline" lay-filter="fromStoreCodeMove">
                                <select id="fromStoreCodeMove" lay-filter="fromStoreCodeMove" lay-search="">
                                    <option value="">请选择</option>
                                    <option v-for="(item,index) in storelist" :value="item.code" :key="index">{{item.text}}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm4">
                        <div class="layui-inline">
                            <button type="button" class="layui-btn layui-btn-sm layui-btn-normal"
                                    @click="queryPageView">
                                <i class="layui-icon">&#xe615;</i>查询
                            </button>
                        </div>
                        <div class="layui-inline">
                            <button type="button" class="layui-btn layui-btn-primary layui-btn-sm"
                                    @click="restQueryParams">
                                <i class="layui-icon">&#xe669;</i>重置
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </blockquote>
        <%--按钮--%>
    <div class="layui-btn-container" style="margin-bottom: 0px;padding-bottom: 0px;">
        <button type="button" @click="addMoveOrder" class="layui-btn layui-btn-sm layui-btn-normal">
            <i class="layui-icon">&#xe654;</i>新建
        </button>
<%--        <button type="button" @click="edtMoveOrder" class="layui-btn layui-btn-sm layui-btn-normal">--%>
<%--            <i class="layui-icon">&#xe642;</i>编辑--%>
<%--        </button>--%>
        <button type="button" @click="delMoveOrder" class="layui-btn layui-btn-sm layui-btn-danger">
            <i class="layui-icon">&#xe640;</i>删除
        </button>
        <button type="button" @click="printMoveOrder" class="layui-btn layui-btn-sm layui-btn-normal">
            <i class="layui-icon">&#xe66d;</i>打印
        </button>
    </div>
    <%-- table grid --%>
    <table id="tableGridMoveOrder" lay-filter="tableGridMoveOrder"></table>
    <%-- table grid toolbar --%>
        <%--按钮--%>
        <div class="layui-btn-container" style="margin-bottom: 0px;padding-bottom: 0px;">
            <label style="width: 65px">上架详情</label>
            <button id="btnMoveAdd" type="button" @click="addMoveOrderUp" class="layui-btn layui-btn-sm layui-btn-normal">
                <i class="layui-icon">&#xe654;</i>增行
            </button>
            <button id="btnMoveDel" type="button" @click="delMoveOrderUp" class="layui-btn layui-btn-sm layui-btn-danger">
                <i class="layui-icon">&#xe640;</i>删除
            </button>
            <button id="btnMoveSave" type="button" @click="saveMoveOrderUp" class="layui-btn layui-btn-sm layui-btn-normal">
                <i class="layui-icon">&#xe605;</i>提交
            </button>
        </div>
        <%-- table grid --%>
        <div id="divtable">
            <table id="tableGridMoveOrderUp"></table>
        </div>
        <%-- table grid toolbar --%>
        <iframe style="visibility:hidden;" id="iframeMove" src="/wms/ycl/insidewarehouse/transferOrder/toPrint" width=0 height=0> </iframe>
</div>
<script src="/wms/plugins/public_components/js/jquery-2.1.4.js"></script>
<script src="/wms/static/js/techbloom/LodopFuncs.js" charset="utf-8"></script>
<script>
    const context_path = '${APP_PATH}';
    const tableMoveOrder = layui.table,
        form = layui.form,
        layer = layui.layer,
        laydate = layui.laydate;

    var tableMoveIns;
    //获取table的list集合
    let vmMove = new Vue({
        el: '#yclMoveOrder',
        data: {
            selectedRow:null,
            arealist:[],
            storelist:[],
            shelfCodelist:[],
            upDownList:[],
            queryParams: {
                tranCode: "",
                createTime: "",
                materialName: "",
                fromAreaCode: "",
                fromStoreCode: ""
            }
        },
        methods: {
            addMoveOrder(){
                layer.open({
                    title: '新增'
                    , type: 2
                    , area: ['1000px', '600px'] //宽高
                    , maxmin: true
                    , content: context_path + '/ycl/insidewarehouse/transferOrder/moveEdit'
                });
            },
            edtMoveOrder(){
                var selectData = layui.table.checkStatus('tableGridMoveOrder').data;
                if (selectData.length!=1)
                {
                    layer.alert('请选中一条记录操作!');
                    return false;
                }
                if (selectData[0].status!=null&&selectData[0].status!='0')
                {
                    layer.alert('该记录已经开始，不能编辑!');
                    return false;
                }
                layer.open({
                    title: '新增'
                    , type: 2
                    , area: ['1000px', '400px'] //宽高
                    , maxmin: true
                    , content: context_path + '/ycl/insidewarehouse/TransferOrder/moveEdit?id='+selectData[0].id
                });
            },
            printMoveOrder(){
                var selectData = layui.table.checkStatus('tableGridMoveOrder').data;
                if (selectData.length != 1)
                {
                    layer.alert('请选中一条记录操作!');
                    return;
                }
                var frm =document.getElementById('iframeMove');
                frm.src='/wms/ycl/insidewarehouse/transferOrder/toPrint?tcode='+selectData[0].tranCode;
                $(frm).load(function(){
                    LODOP=getLodop();
                    var strHtml=frm.contentWindow.document.documentElement.innerHTML;
                    LODOP.ADD_PRINT_HTM(0,0,"100%","100%",strHtml);
                    LODOP.PREVIEW();
                });
            },
            delMoveOrder(){
                var selectData = layui.table.checkStatus('tableGridMoveOrder').data;
                if (selectData.length != 1)
                {
                    layer.alert('请选中一条记录操作!');
                    return;
                }
                if (selectData[0].lineState == '1')
                {
                    layer.alert('完成状态无法删除!');
                    return;
                }
                layer.confirm('确认删除？', function(){
                    $.get(context_path + '/ycl/insidewarehouse/transferOrder/deleteByLineIds', {
                        lineIds: selectData[0].lineId
                    }, function (res) {
                        tableMoveOrder.reload('tableGridMoveOrder', {
                            where: vmMove.queryParams
                        });
                        layer.msg('操作成功！');
                    })
                });
            },
            addMoveOrderUp(){
                var selectData = tableMoveOrder.checkStatus('tableGridMoveOrderUp').data;
                if (selectData.length == 0)
                {
                    layer.alert('请先选中一条记录!');
                    return;
                }
                var oldData = tableMoveOrder.cache['tableGridMoveOrderUp'];
                let newData = [];
                oldData.forEach(m=>{
                    newData.push(m);
                    if(m.tempId === selectData[0].tempId){
                        var newRow = {};
                        newRow.tempId = new Date().valueOf();
                        newRow.lineId = m.lineId;
                        newRow.businessCode = m.businessCode;
                        newRow.areaCode = m.areaCode;
                        newRow.storeCode = m.storeCode;
                        newRow.fromLocator = m.fromLocator;
                        newRow.quantity = m.quantity;
                        newRow.primaryUnit = m.primaryUnit;
                        newRow.materialCode = m.materialCode;
                        newRow.materialName = m.materialName;
                        newRow.lotsNum = m.lotsNum;
                        newRow.state = '1';
                        newRow.inoutType = '1';
                        newData.push(newRow);
                    }
                })
                tableMoveIns.reload({
                    data : newData
                });
            },
            delMoveOrderUp(){
                var selectData = tableMoveOrder.checkStatus('tableGridMoveOrderUp').data;
                if (selectData.length == 0)
                {
                    layer.alert('请先选中一条记录!');
                    return;
                }
                layer.confirm('确认删除？', function(){
                    if (selectData[0].id != undefined && selectData[0].id != '') {
                        $.get(context_path + '/ycl/operation/yclInOutFlow/delete', {
                            ids: selectData[0].id
                        }, function (res) {
                        })
                    }
                    var oldData = tableMoveOrder.cache['tableGridMoveOrderUp'];
                    for(var i = 0; i < oldData.length; i++){
                        if(oldData[i].tempId === selectData[0].tempId){
                            oldData.splice(i,1);
                        }
                    }
                    tableMoveIns.reload({
                        data: oldData
                    });
                    layer.msg('操作成功！');
                });
            },
            saveMoveOrderUp(){
                this.upDownList = tableMoveOrder.cache['tableGridMoveOrderUp']
                if (this.upDownList.length == 0)
                {
                    layer.alert('暂无上架数据!');
                    return;
                }
                let updownQuantityError = false;
                let locatorError = false;
                this.upDownList.forEach(m=>{
                    if (m.updownQuantity==null||m.updownQuantity==''||isNaN(m.updownQuantity))
                    {
                        updownQuantityError=true;
                    }
                    if (m.locatorCode==null ||m.locatorCode=='')
                    {
                        locatorError=true;
                    }
                })
                if (locatorError)
                {
                    layer.alert('上架库位不能为空!');
                    return;
                }
                if (updownQuantityError)
                {
                    layer.alert('移出数量为空或非数字!');
                    return;
                }
                let tempData = [];
                this.upDownList.forEach(m=>{
                    let tmp = tempData.find(x=>x.lineId==m.lineId);
                    if (tmp!=undefined){
                        tmp.updownQuantity=Number(tmp.updownQuantity)+Number(m.updownQuantity)
                    }else{
                        let newRow = {}
                        newRow.lineId=m.lineId
                        newRow.quantity=m.quantity
                        newRow.updownQuantity=m.updownQuantity
                        tempData.push(newRow)
                    }
                })
                if (tempData.some(m=> m.quantity!=m.updownQuantity))
                {
                    layer.alert('上架数量须等于移位数量!');
                    return;
                }
                // let inout = {};
                // inout.areaCode = this.selectedRow.fromAreaCode
                // inout.storeCode = this.selectedRow.fromStoreCode
                // inout.locator = this.selectedRow.fromLocator
                // inout.materialCode = this.selectedRow.materialCode
                // inout.materialName = this.selectedRow.materialName
                // inout.lotsNum = this.selectedRow.lotsNum
                // inout.updownQuantity = this.selectedRow.outQuantity
                // inout.inoutType = '0'
                // console.log(JSON.stringify(this.upDownList));
                $.ajax({
                    type: 'POST',
                    url: context_path + "/ycl/insidewarehouse/transferOrder/saveBatch",
                    contentType: "application/json",
                    async: false,
                    dataType: "json",
                    data: JSON.stringify(this.upDownList),
                    success: res=> {
                        if (res.result) {
                            this.reloadUp();
                            this.queryPageView();
                        }
                        layer.msg(res.msg);
                    }
                })
            },
            reloadUp(){
                $.get(context_path + '/ycl/operation/yclInOutFlow/listView', {
                    businessCode:vmMove.selectedRow.tranCode
                }, function (res) {
                    let dbRes = res.data;
                    if (dbRes.length >0)
                    {
                        $('#btnMoveAdd').addClass('layui-btn-disabled');
                        $('#btnMoveAdd').attr('disabled', 'disabled');
                        $('#btnMoveDel').addClass('layui-btn-disabled');
                        $('#btnMoveDel').attr('disabled', 'disabled');
                        $('#btnMoveSave').addClass('layui-btn-disabled');
                        $('#btnMoveSave').attr('disabled', 'disabled');
                        tableMoveIns.reload({
                            data : dbRes
                        });
                    }else{
                        $('#btnMoveAdd').removeClass('layui-btn-disabled');
                        $('#btnMoveAdd').removeAttr('disabled', 'disabled');
                        $('#btnMoveDel').removeClass('layui-btn-disabled');
                        $('#btnMoveDel').removeAttr('disabled', 'disabled');
                        $('#btnMoveSave').removeClass('layui-btn-disabled');
                        $('#btnMoveSave').removeAttr('disabled', 'disabled');
                        //请求
                        $.get(context_path + '/ycl/insidewarehouse/transferOrder/list?tranType=1&tranCode='+vmMove.selectedRow.tranCode, {
                        }, function (re) {
                            var tid = new Date().valueOf();
                            re.data.forEach(r=>{
                                while(tid === new Date().valueOf())
                                {
                                    continue;
                                }
                                tid = new Date().valueOf();
                                var newRow = {};
                                newRow.tempId = tid;
                                newRow.lineId = r.lineId;
                                newRow.businessCode = r.tranCode;
                                newRow.areaCode = r.fromAreaCode;
                                newRow.storeCode = r.toStoreCode;
                                newRow.fromLocator = r.fromLocator;
                                newRow.quantity = r.outQuantity;
                                newRow.primaryUnit = r.primaryUnit;
                                newRow.materialCode = r.materialCode;
                                newRow.materialName = r.materialName;
                                newRow.lotsNum = r.lotsNum;
                                newRow.state = '1';
                                newRow.inoutType = '1';
                                dbRes.push(newRow);
                            })
                            tableMoveIns.reload({
                                data : dbRes
                            });
                        })
                    }
                })
            },
            inittableMoveIns() {
                tableMoveOrder.render({
                    elem: '#tableGridMoveOrder'
                    , url: context_path + '/ycl/insidewarehouse/transferOrder/list?tranType=1'
                    , page: true
                    , limit:5
                    , id: "tableGridMoveOrder"
                    , size: 'sm'
                    , defaultToolbar: []
                    , cols: [[
                        {type: 'radio'}
                        , {field: 'id', hide: true}
                        , {field: 'lineId', hide: true}
                        , {field: 'tranCode', title: '移位单号',width:'160'}
                        , {field: 'materialCode', title: '物料编码',width:'150'}
                        , {field: 'materialName', title: '物料名称',width:'200'}
                        , {field: 'outQuantity', title: '数量',width:'80'}
                        , {field: 'lotsNum', title: '批次号',width:'100'}
                        , {field: 'fromArea', title: '移出厂区',width:'120'}
                        , {field: 'fromStore', title: '移出仓库',width:'150'}
                        , {field: 'fromLocator', title: '移出库位',width:'100'}
                        , {field: 'toStore', title: '移入仓库',width:'150'}
                        , {field: 'createTime', title: '创建时间',width:'100'}
                        // , {field: 'status', title: '状态',width:'80', templet: (d)=>d.lineState == '1'?'完成':''}
                    ]]
                });

                tableMoveIns = tableMoveOrder.render({
                    elem: '#tableGridMoveOrderUp'
                    ,data:[]
                    ,limit:999
                    ,size: 'sm'
                    ,cols: [[
                        {type: 'radio'}
                        ,{field: 'tempId', hide: true}
                        ,{field: 'materialCode', title: '物料编码'}
                        ,{field: 'lotsNum', title: '批次号'}
                        ,{field: 'quantity', title: '移位数量'}
                        ,{field: 'primaryUnit', title: '单位'}
                        ,{field: 'locatorCode', title: '上架库位', templet: (d)=>vmMove.renderSelectOptions(d)}
                        ,{field: 'updownQuantity', title: '上架数量', style:'background-color: #5fb878;color:#fff', edit: 'text'}
                        ,{field: 'materialName', title: '物料名称'}
                    ]]
                });
            },
            queryPageView() {
                vmMove.queryParams.fromAreaCode = $('#fromAreaCodeMove').val();
                vmMove.queryParams.fromStoreCode = $('#fromStoreCodeMove').val();
                tableMoveOrder.reload('tableGridMoveOrder', {
                    where: vmMove.queryParams
                });
            },
            restQueryParams() {
                vmMove.queryParams = {
                    tranCode: "",
                    createTime: "",
                    materialName: "",
                    fromAreaCode: "",
                    fromStoreCode: ""
                }
                $('#fromAreaCodeMove').val("");
                $('#fromStoreCodeMove').val("");
                form.render('select','fromAreaCodeMove');
                form.render('select','fromStoreCodeMove');
                tableMoveOrder.reload('tableGridMoveOrder', {
                    where: vmMove.queryParams
                });
            },
            renderSelectOptions(e){
                let html = [];
                html.push('<div class="layui-form" lay-filter="shelfCode">');
                html.push('<select name="shelfCode" id="shelfCode" data-value="'+e.tempId+'" lay-filter="shelfCode" lay-search="">');
                html.push('<option value="">请选择</option>');
                this.shelfCodelist.forEach(m=>{
                    html.push('<option value="');
                    html.push(m.code);
                    html.push('"');
                    if(m.code == e.locatorCode ){
                        html.push(' selected="selected"');
                    }
                    html.push('>');
                    html.push(m.codename);
                    html.push('</option>');
                })
                html.push('</select></div>');
                return html.join('');
            }
        },
        created() {
            $.get(context_path + '/ycl/baseinfo/basedata/area/0'
                , res=> {
                    this.arealist = res.data;
                    this.$nextTick(() => {
                        form.render('select','fromAreaCodeMove');
                    });
                })
        },
        mounted() {
            form.render();
            laydate.render({
                elem: '#createTimeMove'
                ,range: true
                ,done: function (value, date, endDate) {
                    vmMove.queryParams.createTime = value
                }
            });

            this.inittableMoveIns()
        }
    })

    tableMoveOrder.on('row(tableGridMoveOrder)', function(obj){
        vmMove.selectedRow = obj.data;
        //标注选中样式
        obj.tr.addClass('layui-table-click').siblings().removeClass('layui-table-click');
        $.get(context_path + '/goods/getShelfByFactWareCode.do', {
            warehouseCode : vmMove.selectedRow.toStoreCode,
            entityId: vmMove.selectedRow.toAreaCode,
            queryString: '',
            pageSize: 999,
            pageNo: 1
        }, function (res) {
            vmMove.shelfCodelist = res.result
            vmMove.reloadUp();
        })
    });

    //监听select
    form.on('select(fromAreaCodeMove)', function (data) {
        $.get(context_path + '/ycl/baseinfo/basedata/store/'+data.value
            , res=> {
                vmMove.storelist = res.data;
                vmMove.$nextTick(() => {
                    form.render('select','fromStoreCodeMove');
                })
            })
    });

    function tableRender(){
        tableMoveOrder.reload('tableGridMoveOrder', {
            where: vmMove.queryParams
        });
    }

    //监听select
    form.on('select(shelfCode)', function (data) {
        var elem = data.othis.parents('tr');
        var id = elem.first().find('td').eq(1).text();
        let val = data.value
        console.log(val)
        this.tbData = tableMoveOrder.cache['tableGridMoveOrderUp'];
        this.tbData.forEach(n=>{
            if (n.tempId==id)
            {
                n.locatorCode = data.value;
            }
        })
        tableMoveIns.reload({
            data : this.tbData
        });
    });
</script>