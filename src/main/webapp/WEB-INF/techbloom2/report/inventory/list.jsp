<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%--实时库存报表--%>
<div id="yclInventoryPage" style="margin: 15px">
    <%--query tools--%>
    <blockquote class="layui-elem-quote">
        <form class="layui-form">
            <div class="layui-fluid">
                <div class="layui-row layui-col-space10">
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">厂区</label>
                            <div class="layui-input-inline">
                                <select id="areaCodeSelectInventory" lay-filter="areaCodeSelectInventoryFilter"
                                        v-model="queryParams.areaCode">
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">仓库</label>
                            <div class="layui-input-inline">
                                <select id="storeCodeSelectInventory" lay-filter="storeCodeSelectInventoryFilter"
                                        v-model="queryParams.storeCode">
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">库位</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input"
                                       v-model="queryParams.locatorCode"/>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <button type="button" class="layui-btn layui-btn-sm layui-btn-normal"
                                    @click="queryPageView">
                                <i class="layui-icon">&#xe615;</i>查询
                            </button>
                        </div>
                        <div class="layui-inline">
                            <button type="button" class="layui-btn layui-btn-primary layui-btn-sm"
                                    @click="restQueryParams">
                                <i class="layui-icon">&#xe669;</i>重置
                            </button>
                        </div>
                    </div>
                </div>
                <div class="layui-row layui-col-space10">
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">物料号</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input"
                                       v-model="queryParams.materialCode"/>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">批次号</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input"
                                       v-model="queryParams.lotsNum"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </blockquote>

    <%-- table grid --%>
    <table id="tableGridInventory" lay-filter="tableGridFilterInventory"></table>
    <table id="tableGridInventory2" lay-filter="tableGridFilterInventory2"></table>

        <%-- table grid toolbar --%>
        <script type="text/html" id="tableGridToolbarInventory">
            <div class="layui-btn-container">
                <label class="layui-word-aux">物料库存信息</label>
            </div>
        </script>
        <script type="text/html" id="tableGridToolbarInventory2">
            <div class="layui-btn-container">
                <label class="layui-word-aux">库存详情信息</label>
            </div>
        </script>
</div>


<script>

    const context_path = '${APP_PATH}';
    const table = layui.table,
        form = layui.form,
        layer = layui.layer,
        laydate = layui.laydate;
    let tableGridIns;
    let tableGridIns2;

    let vm = new Vue({
        el: '#yclInventoryPage',
        data: {
            queryParams: {
                areaCode: "",
                storeCode: "",
                locatorCode: "",
                materialCode: "",
                lotsNum: ""
            }
        },
        methods: {
            /*初始下拉选*/
            initAreaSelectInventory() {
                $("#areaCodeSelectInventory").empty();
                $("#areaCodeSelectInventory").append('<option value="">请选择</option>');
                $.get(context_path + '/factoryArea/getFactoryList', {
                        pageNo: 1,
                        pageSize: 999,
                        queryString: ''
                    },
                    function (res) {
                        let html = '';
                        let list = res.data;
                        for (let i = 0; i < list.length; i++) {
                            html += '<option value=' + list[i].id + '>' + list[i].text + '</option>';
                        }
                        $("#areaCodeSelectInventory").append(html);
                        form.render('select');
                    })
            },
            /*初始下拉选*/
            initStoreSelectInventory() {
                $("#storeCodeSelectInventory").empty();
                $("#storeCodeSelectInventory").append('<option value="">请选择</option>');
                $.get(context_path + '/warehouselist/getYclWarehouseByFactory', {
                    factoryCode: vm.queryParams.areaCode
                }, function (res) {
                    if (res.result) {
                        let html = '';
                        let list = res.data;
                        for (let i = 0; i < list.length; i++) {
                            html += '<option value=' + list[i].id + '>' + list[i].text + '</option>';
                        }
                        $("#storeCodeSelectInventory").append(html);
                    }
                    form.render('select');
                })
            },
            /*初始化列表*/
            initTableIns() {
                tableGridIns = table.render({
                    elem: '#tableGridInventory'
                    , url: context_path + '/ycl/report/inventory/pageView'
                    , page: true
                    , toolbar: '#tableGridToolbarInventory'
                    , id: "tableGridSetInventory"
                    , size: 'sm'
                    , defaultToolbar: []
                    , cols: [[
                        {type: 'checkbox'}
                        , {field: 'areaCode', title: '厂区'}
                        , {field: 'storeCode', title: '仓库'}
                        , {field: 'materialCode', title: '物料号'}
                        , {field: 'materialName', title: '物料名称'}
                        , {field: 'lotsNum', title: '批次号'}
                        , {field: 'quality', title: '总数量'}
                        , {field: 'primaryUnit', title: '单位'}
                        , {field: 'updateTime', title: '更新时间'}
                    ]]
                });
            },
            /*第二栏数据列表*/
            initTableIns2() {
                tableGridIns2 = table.render({
                    elem: '#tableGridInventory2'
                    //, url: context_path + '/ycl/report/inventory/pageView'
                    , toolbar: '#tableGridToolbarInventory2'
                    , id: "tableGridSetInventory2"
                    , defaultToolbar: []
                    , limit: 9999
                    , data: []
                    , cols: [[
                        {type: 'checkbox'}
                        , {field: 'locatorCode', title: '库位'}
                        , {field: 'materialCode', title: '物料号'}
                        , {field: 'materialName', title: '物料名称'}
                        , {field: 'lotsNum', title: '批次号'}
                        , {field: 'primaryUnit', title: '单位'}
                        , {field: 'quality', title: '数量'}
                        , {field: 'updateTime', title: '更新时间'}
                    ]]
                });
            },
            /*查询*/
            queryPageView() {
                table.reload('tableGridSetInventory', {
                    where: vm.queryParams
                });
            },
            /*重置*/
            restQueryParams() {
                vm.queryParams = {
                    areaCode: "",
                    storeCode: "",
                    locatorCode: "",
                    materialCode: "",
                    lotsNum: ""
                }
                form.render('select');
                table.reload('tableGridSetInventory', {
                    where: vm.queryParams
                });
            }
        },
        created() {
        },
        mounted() {
        }

    })

    /*针对第二栏的按钮请求*/
    table.on('toolbar(tableGridFilterInventory2)', function (obj) {
        const checkStatusData = table.checkStatus(obj.config.id).data
        let oldData = table.cache['tableGridSetInventory2'];
        switch (obj.event) {
            case 'addIn':
                console.log(oldData);
                let newRow = oldData[0];
                oldData.push(newRow);
                console.log(oldData);
                console.log(tableGridIns3);
                tableGridIns3.reload({
                    // console.log(oldData);
                    data: oldData
                })
                break;
            case 'save' :

                if(checkStatusData.length == 0){
                    Dialog.alert("请选择要出库的数据");
                    return;
                }

                let total_quantity = checkStatusData[0].quantity;

                var add_quantity = 0;

                for(var i = 0; i < checkStatusData.length; i++){
                    if(!checkStatusData[i].outQuantity){
                        Dialog.alert("请填写第" + (i + 1) + "行的配送数量");
                        return;
                    }
                    if(!checkStatusData[i].package){
                        Dialog.alert("请填写第" + (i + 1) + "行的件数");
                        return;
                    }
                    if(!checkStatusData[i].lotsNum){
                        Dialog.alert("请填写第" + (i + 1) + "行的批次号");
                        return;
                    }
                    if(!checkStatusData[i].locatorCode){
                        Dialog.alert("请填写第" + (i + 1) + "行的库位编码");
                        return;
                    }
                    var outQuantity = checkStatusData[i].outQuantity == ''?0:checkStatusData[i].outQuantity;
                    add_quantity += outQuantity*1;
                }

                console.log(add_quantity);

                if(add_quantity > total_quantity){
                    Dialog.alert("配送数量的和不能大于领料数量");
                    return;
                }

                $.ajax({
                    type: 'POST',
                    url: context_path + "/ycl/production/picking/save",
                    contentType: "application/json",
                    async: false,
                    dataType: "json",
                    data: JSON.stringify(checkStatusData),
                    success: function (res) {
                        console.log(res)
                        if (res.result === false) {
                            layer.msg(res.msg, {icon: 5});
                            return false;
                        } else {
                            layer.msg(res.msg, {icon: 1});
                            let data= [];
                            tableGridIns3.reload({
                                // console.log(oldData);
                                data: data
                            });
                            tableGridIns.reload();
                            //刷新父页面
                            // parent.location.reload();
                        }
                    }
                })

                break;
            case 'delete' :
                if (checkStatusData.length === 0) {
                    layer.msg('至少选择一条数据', {icon: 2});
                    return
                }
                console.log(obj.tr)
                console.log(checkStatusData)

                oldData.splice(obj.config.data('index'),1);

                tableGridIns3.reload({
                    data: oldData
                })

                // let ids = checkStatusData.map(item => item.id).toString()
                // console.log(ids)
                // $.get(context_path + '/ycl/operation/yclInOutFlow/delete', {ids}, function (res) {
                //     layer.msg(res.msg);
                //     table.reload('tableGridSet3');
                // })
                break;
        }
    });

    // 点击订单物料
    table.on('row(tableGridFilterInventory)', function (obj) {
        const data = obj.data;
        console.log("物料名称", data)
        vm.materialCode = data.materialCode
        vm.lotsNum = data.lotsNum
        $.get(context_path + '/yclreport/inventory/listView', {
            materialCode: data.materialCode,
            lotsNum: data.lotsNum
        }, function (res) {
            table.reload('tableGridSetDROrder', {
                data: res.data
            });
        })
        obj.tr.addClass('layui-table-click').siblings().removeClass('layui-table-click');
    });

    //监听select
    form.on('select(areaCodeSelectInventoryFilter)', function (data) {
        vm.queryParams.areaCode = data.value
        vm.initStoreSelectInventory()
    });
    //监听select
    form.on('select(storeCodeSelectInventoryFilter)', function (data) {
        vm.queryParams.storeCode = data.value
    });

    // init
    ;!function () {
        laydate.render({
            elem: '#createrTime'
            , done: function (value, date, endDate) {
                vm.queryParams.createrTime = value
            }
        });

        vm.initTableIns()
        vm.initTableIns2()
        vm.initAreaSelectInventory()
    }();
</script>