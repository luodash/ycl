<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<head>
    <link rel="stylesheet" href="${APP_PATH}/static/layui/css/layui.css" type="text/css"/>
</head>
<script src="${APP_PATH}/static/layui/layui.all.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>


<div id="yclCheckOrderEdit" style="margin: 15px">

    <form class="layui-form" action="">
        <input type="hidden" name="id" value="${yclInventoryEntity.id}" class="layui-input">
        <div class="layui-form-item">
            <label class="layui-form-label">所属厂区</label>
            <div class="layui-input-block">
                <select id="areaCodeSelect" name="areaCode" lay-filter="areaCodeSelect">
                    <option value="">请选择</option>
                </select>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">所属仓库</label>
            <div class="layui-input-block">
                <select id="storeCodeSelect" name="storeCode" lay-filter="storeCodeSelect">
                    <option value="">请选择</option>
                </select>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">所属库位</label>
            <div class="layui-input-block">
                <input type="text" name="locatorCode" value="${yclInventoryEntity.locatorCode}" class="layui-input"
                       placeholder="请输入"/>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">物料号</label>
            <div class="layui-input-block">
                <input type="text" name="materialCode" value="${yclInventoryEntity.materialCode}" placeholder="请输入"
                       class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">物料名称</label>
            <div class="layui-input-block">
                <input type="text" name="materialName" value="${yclInventoryEntity.materialName}" placeholder="请输入"
                       class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">批次号</label>
            <div class="layui-input-block">
                <input type="text" name="lotsNum" value="${yclInventoryEntity.lotsNum}" placeholder="请输入"
                       class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">单位</label>
            <div class="layui-input-block">
                <input type="text" name="primaryUnit" value="${yclInventoryEntity.primaryUnit}" placeholder="请输入"
                       class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">数量</label>
            <div class="layui-input-block">
                <input type="tel" name="quality" value="${yclInventoryEntity.quality}" placeholder="请输入"
                       class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <button type="submit" class="layui-btn" lay-submit="" lay-filter="submitInventory">立即提交</button>
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
        </div>

    </form>

</div>

<script>

    const context_path = '${APP_PATH}';
    const form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        table = layui.table,
        $ = layui.jquery;

    let areaCode = '${yclInventoryEntity.areaCode}'
    let storeCode = '${yclInventoryEntity.storeCode}'
    let locatorCode = '${yclInventoryEntity.locatorCode}'

    let vm = new Vue({
        el: '#yclCheckOrderEdit',
        data: {
            areaCode: areaCode,
            storeCode: storeCode
        },
        watch: {
            areaCode: {
                handler(newName, oldName) {
                    this.$nextTick(() => {
                        vm.initStoreList()
                    })
                },
                immediate: true
            }
        },
        methods: {
            initAreaList() {
                $.post(context_path + '/factoryArea/getFactoryList', {
                    queryString: "",
                    pageSize: 15,
                    pageNo: 1
                }, function (res) {
                    let html = '';
                    let list = res.result;
                    for (let i = 0; i < list.length; i++) {
                        if ((areaCode != null || areaCode != '' || typeof areaCode != 'undefined') && (list[i].id == areaCode)) {
                            html += '<option selected value=' + list[i].id + '>' + list[i].text + '</option>';
                        } else {
                            html += '<option value=' + list[i].id + '>' + list[i].text + '</option>';
                        }
                    }
                    $("#areaCodeSelect").append(html);
                    form.render('select');
                })
            },
            initStoreList() {
                $.post(context_path + '/car/selectWarehouse', {
                    queryString: "",
                    pageSize: 9999,
                    pageNo: 1,
                    factoryCodeId: vm.areaCode
                }, function (res) {
                    let html = '';
                    let list = res.result;
                    for (let i = 0; i < list.length; i++) {
                        if ((storeCode != null || storeCode != '' || typeof storeCode != 'undefined') && (list[i].id == storeCode)) {
                            html += '<option selected value=' + list[i].id + '>' + list[i].text + '</option>';
                        } else {
                            html += '<option value=' + list[i].id + '>' + list[i].text + '</option>';
                        }
                    }
                    $("#storeCodeSelect").empty();
                    $("#storeCodeSelect").append('<option value="">请选择</option>');
                    $("#storeCodeSelect").append(html);
                    form.render('select');
                })
            },
            initLocatorList() {
                $.get(context_path + '/warehouselist/yclList.do', {
                    rows: '9999',
                    queryJsonString: JSON.stringify({
                        factoryCode: vm.storeCode
                    })
                }, function (res) {
                    let html = '';
                    let list = res.rows;
                    for (let i = 0; i < list.length; i++) {
                        html += '<option value=' + list[i].code + '>' + list[i].name + '</option>';
                    }
                    $("#locatorCodeSelect").append(html);
                    form.render('select');
                })
            },
            initMaterialList() {
                $.get(context_path + '/warehouselist/yclList.do', {rows: '9999'}, function (res) {
                    let html = '';
                    let list = res.rows;
                    for (let i = 0; i < list.length; i++) {
                        html += '<option value=' + list[i].code + '>' + list[i].name + '</option>';
                    }
                    $("#materialCode").append(html);
                    form.render('select');
                })
            },
            submitInventory(params) {
                console.log(params)
                $.ajax({
                    type: 'POST',
                    url: context_path + "/ycl/report/inventory/save",
                    contentType: "application/json",
                    async: false,
                    dataType: "json",
                    data: JSON.stringify(params),
                    success: function (res) {
                        if (res.result === false) {
                            layer.msg(res.msg, {icon: 5});
                            return false;
                        } else {
                            layer.msg(res.msg, {icon: 1});
                            // layer.closeAll();
                            let index = parent.layer.getFrameIndex(window.name);
                            parent.layer.close(index);
                        }
                    }
                })
            }
        },
        created() {
        },
        mounted() {
            form.render();
        }

    });

    //监听select
    form.on('select(areaCodeSelect)', function (data) {
        vm.areaCode = data.value
    });
    //监听select
    form.on('select(storeCodeSelect)', function (data) {
        vm.storeCode = data.value
    });

    //监听提交
    form.on('submit(submitInventory)', function (data) {
        vm.submitInventory(data.field)
    });

    // init
    ;!function () {
        vm.initAreaList()
    }();

</script>
