<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<div id="outflowReportPage" style="margin: 30px">
    <%--query tools--%>
    <blockquote class="layui-elem-quote">
        <form class="layui-form">
            <div class="layui-fluid">
                <div class="layui-row layui-col-space10">
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">出库类型</label>
                            <div class="layui-input-inline">
                                <select id="areaCodeSelect" lay-filter="areaCodeSelect"
                                        v-model="queryParams.businessType">
                                    <option value=""></option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">仓库</label>
                            <div class="layui-input-inline">
                                <select id="storeCodeSelect" lay-filter="storeCodeSelect"
                                        v-model="queryParams.storeCode">
                                    <option value="">请选择</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">物料编码</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input"
                                       v-model="queryParams.materialCode"/>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="layui-row layui-col-space10">
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">出库时间</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" id="orderDateTime"
                                       v-model="queryParams.createTime"/>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">物料类别</label>
                            <div class="layui-input-inline">
                                <%--<input type="text" class="layui-input" v-model="queryParams.lotsNum"/>--%>
                                <select id="wllb" lay-filter="storeCodeSelect"
                                        v-model="queryParams.lotsNum">
                                    <option value="">请选择</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <button type="button" class="layui-btn layui-btn-sm layui-btn-normal"
                                    @click="queryPageView">
                                <i class="layui-icon">&#xe615;</i>查询
                            </button>
                        </div>
                        <div class="layui-inline">
                            <button type="button" class="layui-btn layui-btn-primary layui-btn-sm"
                                    @click="restQueryParams">
                                <i class="layui-icon">&#xe669;</i>重置
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </blockquote>

    <%-- table grid --%>
    <table id="tableGridoutflowReport" lay-filter="tableGridFilteroutflowReport"></table>
    <%-- table grid toolbar --%>
    <script type="text/html" id="tableGridToolbaroutflowReport">
        <div class="layui-btn-container">
            <button type="button" class="layui-btn layui-btn-sm layui-btn-danger" lay-event="editoutflowReport">
                <i class="layui-icon">&#xe702;</i>查看详情
            </button>
            <button type="button" class="layui-btn layui-btn-sm layui-btn-normal" lay-event="addoutflowReport">
                <i class="layui-icon">&#xe66d;</i>打印单据
            </button>
        </div>
    </script>

    <iframe style="visibility:hidden;" id="myiframe" name="myiframe" src="<%=path%>/ycl/report/outflow/toPrint" width=0 height=0> </iframe>
</div>


<script>

    const context_path = '${APP_PATH}';
    const table = layui.table,
        form = layui.form,
        layer = layui.layer,
        laydate = layui.laydate;

    let vm = new Vue({
        el: '#outflowReportPage',
        data: {
            queryParams: {
                businessPurpose: "",
                materialCode: "",
                createTime: "",
                materialCode: "",
                houseCode: ""
            }
        },
        watch: {
            'queryParams.areaCode': {
                handler(newName, oldName) {
                    this.$nextTick(() => {
                        vm.initStoreList()
                    })
                },
                deep: true,
                immediate: true
            }
        },
        methods: {
            /*厂区*/
            initAreaList() {
                $.post(context_path + '/factoryArea/getFactoryList', {
                    queryString: "",
                    pageSize: 15,
                    pageNo: 1
                }, function (res) {
                    let html = '';
                    let list = res.result;
                    for (let i = 0; i < list.length; i++) {
                        html += '<option value=' + list[i].id + '>' + list[i].text + '</option>';
                    }
                    $("#areaCodeSelect").append(html);
                    form.render('select');
                })
            },
            /*仓库*/
            initStoreList() {
                $.post(context_path + '/car/selectWarehouse', {
                    queryString: "",
                    pageSize: 9999,
                    pageNo: 1,
                    factoryCodeId: vm.queryParams.areaCode
                }, function (res) {
                    let html = '';
                    let list = res.result;
                    for (let i = 0; i < list.length; i++) {
                        html += '<option value=' + list[i].id + '>' + list[i].text + '</option>';
                    }
                    $("#storeCodeSelect").empty();
                    $("#storeCodeSelect").append('<option value="">请选择</option>');
                    $("#storeCodeSelect").append(html);
                    form.render('select');
                })
            },
            /* 库位*/
            initLocatorList() {
                $.get(context_path + '/goods/list.do', {
                    rows: '9999',
                    queryJsonString: JSON.stringify({
                        factoryCode: vm.areaCode,
                        warehouse: vm.storeId
                    })
                }, function (res) {
                    let html = '';
                    let list = res.rows;
                    for (let i = 0; i < list.length; i++) {
                        html += '<option value=' + list[i].code + '>' + list[i].name + '</option>';
                    }
                    $("#locatorCodeSelect").empty();
                    $("#locatorCodeSelect").append('<option value="">请选择</option>');
                    $("#locatorCodeSelect").append(html);
                    form.render('select');
                })
            },
            /*列表数据*/
            initTableIns() {
                table.render({
                    elem: '#tableGridoutflowReport'
                    , url: context_path + '/ycl/report/outflow/list'
                    , page: true
                    , toolbar: '#tableGridToolbaroutflowReport'
                    , id: "tableGridSetoutflowReport"
                    , size: 'sm'
                    , defaultToolbar: []
                    , cols: [[
                        {type: 'checkbox'}
                        , {field: 'areaCode', title: '出库类型'}
                        , {field: 'storeCode', title: '出库单号'}
                        , {field: 'locatorCode', title: '物料编码'}
                        , {field: 'materialCode', title: '物料名称'}
                        , {field: 'materialName', title: '批次号'}
                        , {field: 'lotsNum', title: '仓库编码'}
                        , {field: 'primaryUnit', title: '仓库名称'}
                        , {field: 'quality', title: '库位编码'}
                        , {field: 'quality', title: '库存数量'}
                        , {field: 'quality', title: '出库数量'}
                        , {field: 'quality', title: '需求部门'}
                        , {field: 'updateTime', title: '更新时间'}
                        , {field: 'quality', title: '出库人'}
                    ]]
                });
            },
            /*查询*/
            queryPageView() {
                console.log(vm.queryParams)
                table.reload('tableGridSetoutflowReport', {
                    where: vm.queryParams
                });
            },
            /*重置*/
            restQueryParams() {
                vm.queryParams = {
                    areaCode: "",
                    storeCode: "",
                    locatorCode: "",
                    materialCode: "",
                    lotsNum: ""
                }
                form.render('select');
                table.reload('tableGridSetoutflowReport', {
                    where: vm.queryParams
                });
            },
            addoutflowReport(){
                var selectData = layui.table.checkStatus('tableGridoutflowReport').data;
                if (selectData.length != 1)
                {
                    layer.alert('请选中一条记录操作!');
                    return;
                }
                var frm =document.getElementById('myiframe');
                frm.src='/wms/ycl/report/outflow/toPrint?tcode='+selectData[0].tranCode;
                $(frm).load(function(){
                    LODOP=getLodop();
                    var strHtml=frm.contentWindow.document.documentElement.innerHTML;
                    LODOP.ADD_PRINT_HTM(0,0,"100%","100%",strHtml);
                    LODOP.PREVIEW();
                });
            }
        },
        created() {
        },
        mounted() {
        }

    })

    table.on('toolbar(tableGridFilteroutflowReport)', function (obj) {
        const checkStatusData = table.checkStatus(obj.config.id).data
        console.log(checkStatusData)
        let checkCodes = checkStatusData.map(item => item.checkCode).toString()

        switch (obj.event) {
            /*case 'addoutflowReport':
                layer.open({
                    title: '新增'
                    , type: 2
                    , area: ['1200px', '600px'] //宽高
                    , maxmin: true
                    , content: context_path + '/ycl/report/outflow/toPrint'
                });
                break;*/
            case 'editoutflowReport':
                if (checkStatusData.length === 0) {
                    layer.msg('至少选择一条数据', {icon: 2});
                    return
                }
                if (checkStatusData.length > 1) {
                    layer.msg('只能选择一条数据', {icon: 2});
                    return
                }
                let id = checkStatusData[0].id
                $.get(context_path + '/ycl/report/outflow/toEdit', {id}, function (res) {
                    layer.open({
                        title: '编辑'
                        , type: 2
                        , area: ['1200px', '600px'] //宽高
                        , maxmin: true
                        , content: context_path + '/ycl/report/outflow/toEdit?id=' + id
                    });
                })
                break;
            case 'deleteoutflowReport':
                if (checkStatusData.length === 0) {
                    layer.msg('至少选择一条数据', {icon: 2});
                    return
                }
                $.get(context_path + '/ycl/insidewarehouse/check/deleteByCheckCodes', {checkCodes}, function (res) {
                    layer.msg(res.msg);
                    table.reload('tableGridSetoutflowReport');
                })
                break;
        }
    });

    //监听select
    form.on('select(areaCodeSelect)', function (data) {
        vm.queryParams.areaCode = data.value
    });
    //监听select
    form.on('select(storeCodeSelect)', function (data) {
        vm.queryParams.storeCode = data.value
    });

    // init
    ;!function () {

        //日期区间选择器
        laydate.render({
            elem: '#orderDateTime'
            , type: 'datetime'
            , range: '~'
            , done: function (value, date, endDate) {
                console.log(value)
                console.log(date)
                console.log(endDate)
                let dateTime = value.split('~')
                console.log(dateTime[0])
                console.log(dateTime[1])
                vm.queryParams.orderDate = value
            }
        });

        laydate.render({
            elem: '#createrTime'
            , done: function (value, date, endDate) {
                vm.queryParams.createrTime = value
            }
        });

        vm.initAreaList()
        vm.initTableIns()

    }();
</script>