<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%--不合格退料出库--%>
<div id="ReturnReportPage" style="margin: 30px">
    <%--query tools--%>
    <blockquote class="layui-elem-quote">
        <form class="layui-form">
            <div class="layui-fluid">
                <div class="layui-row layui-col-space10">
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">退货单号</label>
                            <div class="layui-input-inline">
                                <select id="areaCodeSelect" lay-filter="areaCodeSelect">
                                    <option value="">请选择</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">物料编码</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" v-model="queryParams.locatorCode"/>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">来源厂区</label>
                            <div class="layui-input-inline">
                                <select id="storeCodeSelect" lay-filter="storeCodeSelect">
                                    <option value="">请选择</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="layui-row layui-col-space10">
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">机台</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" id="orderDateTime" v-model="queryParams.materialCode"/>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">销售员</label>
                            <div class="layui-input-inline">
                                <%--<input type="text" class="layui-input" v-model="queryParams.lotsNum"/>--%>
                                <select id="wllb" lay-filter="storeCodeSelect">
                                    <option value="">请选择</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">退料日期</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input"  v-model="queryParams.materialCode"/>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <button type="button" class="layui-btn layui-btn-sm layui-btn-normal"
                                    @click="queryPageView">
                                <i class="layui-icon">&#xe615;</i>查询
                            </button>
                        </div>
                        <div class="layui-inline">
                            <button type="button" class="layui-btn layui-btn-primary layui-btn-sm"
                                    @click="restQueryParams">
                                <i class="layui-icon">&#xe669;</i>重置
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </blockquote>

    <%-- table grid --%>
    <table id="tableGridReturnReport" lay-filter="tableGridFilterWReturnReport"></table>
    <%-- table grid toolbar --%>
    <script type="text/html" id="tableGridToolbarReturnReport">
        <div class="layui-btn-container">
            <button type="button" class="layui-btn layui-btn-sm layui-btn-normal" lay-event="addReturnReport">
                <i class="layui-icon">&#xe66d;</i>打印单据
            </button>
        </div>
    </script>

</div>


<script>

    const context_path = '${APP_PATH}';
    const table = layui.table,
        form = layui.form,
        layer = layui.layer,
        laydate = layui.laydate;

    let vm = new Vue({
        el: '#ReturnReportPage',
        data: {
            queryParams: {
                applyCode: "",
                materialCode: "",
                areaCode: "",
                device: "",
                createUser: "",/*销售员*/
                applyTime:""
            }
        },
        watch: {
            'queryParams.areaCode': {
                handler(newName, oldName) {
                    this.$nextTick(() => {
                        vm.initStoreList()
                    })
                },
                deep: true,
                immediate: true
            }
        },
        /*厂区*/
        methods: {
            initAreaList() {
                $.post(context_path + '/factoryArea/getFactoryList', {
                    queryString: "",
                    pageSize: 15,
                    pageNo: 1
                }, function (res) {
                    let html = '';
                    let list = res.result;
                    for (let i = 0; i < list.length; i++) {
                        html += '<option value=' + list[i].id + '>' + list[i].text + '</option>';
                    }
                    $("#areaCodeSelect").append(html);
                    form.render('select');
                })
            },
            /*仓库*/
            initStoreList() {
                $.post(context_path + '/car/selectWarehouse', {
                    queryString: "",
                    pageSize: 9999,
                    pageNo: 1,
                    factoryCodeId: vm.queryParams.areaCode
                }, function (res) {
                    let html = '';
                    let list = res.result;
                    for (let i = 0; i < list.length; i++) {
                        html += '<option value=' + list[i].id + '>' + list[i].text + '</option>';
                    }
                    $("#storeCodeSelect").empty();
                    $("#storeCodeSelect").append('<option value="">请选择</option>');
                    $("#storeCodeSelect").append(html);
                    form.render('select');
                })
            },
            /*库位*/
            initLocatorList() {
                $.get(context_path + '/goods/list.do', {
                    rows: '9999',
                    queryJsonString: JSON.stringify({
                        factoryCode: vm.areaCode,
                        warehouse: vm.storeId
                    })
                }, function (res) {
                    let html = '';
                    let list = res.rows;
                    for (let i = 0; i < list.length; i++) {
                        html += '<option value=' + list[i].code + '>' + list[i].name + '</option>';
                    }
                    $("#locatorCodeSelect").empty();
                    $("#locatorCodeSelect").append('<option value="">请选择</option>');
                    $("#locatorCodeSelect").append(html);
                    form.render('select');
                })
            },
            initTableIns() {
                table.render({
                    elem: '#tableGridReturnReport'
                    , url: context_path + '/ycl/report/return/list'
                    , page: true
                    , toolbar: '#tableGridToolbarReturnReport'
                    , id: "tableGridSetReturnReport"
                    , size: 'sm'
                    , defaultToolbar: []
                    , cols: [[
                        {type: 'checkbox'}
                        , {field: 'applyCode', title: '退料单号'}
                        , {field: 'IN_CODE', title: '订单号'}
                        , {field: 'lineCode', title: '订单行号'}
                        , {field: 'SUPPLIER_CODE', title: '供应商'}
                        , {field: 'BATCH_CODE', title: '批次号'}
                        , {field: 'materialCode', title: '物料编码'}
                        , {field: 'materialName', title: '物料名称'}
                        , {field: 'backQuantity', title: '出库数量'}
                        , {field: 'primaryUnit', title: '单位'}
                        , {field: 'areaCode', title: '来源厂区'}
                        , {field: 'device', title: '机台'}
                        , {field: 'storeCode', title: '来源仓库'}
                        , {field: 'locatorCode', title: '来源库位'}
                        , {field: 'createUser', title: '销售员'}
                        , {field: 'createTime', title: '退料日期'}

                    ]]
                });
            },
            queryPageView() {
                console.log(vm.queryParams)
                table.reload('tableGridSetReturnReport', {
                    where: vm.queryParams
                });
            },
            restQueryParams() {
                vm.queryParams = {
                    applyCode: "",
                    materialCode: "",
                    areaCode: "",
                    device: "",
                    createUser: "",
                    applyTime:""
                }
                form.render('select');
                table.reload('tableGridSetReturnReport', {
                    where: vm.queryParams
                });
            }
        },
        created() {},
        mounted() {}

    })

    table.on('toolbar(tableGridFilterReturnReport)', function (obj) {
        const ReturnStatusData = table.checkStatus(obj.config.id).data
        console.log(ReturnStatusData)
        let checkCodes = ReturnStatusData.map(item => item.checkCode).toString()

        switch (obj.event) {
            case 'addReturnReport':
                layer.open({
                    title: '新增'
                    , type: 2
                    , area: ['1200px', '600px'] //宽高
                    , maxmin: true
                    , content: context_path + '/ycl/report/Return/toEdit'
                });
                break;
            case 'editReturnReport':
                if (ReturnStatusData.length === 0) {
                    layer.msg('至少选择一条数据', {icon: 2});
                    return
                }
                if (ReturnStatusData.length > 1) {
                    layer.msg('只能选择一条数据', {icon: 2});
                    return
                }
                let id = ReturnStatusData[0].id
                $.get(context_path + '/ycl/report/Return/toEdit', {id}, function (res) {
                    layer.open({
                        title: '编辑'
                        , type: 2
                        , area: ['1200px', '600px'] //宽高
                        , maxmin: true
                        , content: context_path + '/ycl/report/Return/toEdit?id=' + id
                    });
                })
                break;
            case 'deleteReturnReport':
                if (ReturnStatusData.length === 0) {
                    layer.msg('至少选择一条数据', {icon: 2});
                    return
                }
                $.get(context_path + '/ycl/insidewarehouse/check/deleteByCheckCodes', {checkCodes}, function (res) {
                    layer.msg(res.msg);
                    table.reload('tableGridSetReturnReport');
                })
                break;
        }
    });

    //监听select
    form.on('select(areaCodeSelect)', function (data) {
        vm.queryParams.areaCode = data.value
    });
    //监听select
    form.on('select(storeCodeSelect)', function (data) {
        vm.queryParams.storeCode = data.value
    });

    // init
    ;!function () {

        //日期区间选择器
        laydate.render({
            elem: '#orderDateTime'
            , type: 'datetime'
            , range: '~'
            , done: function (value, date, endDate) {
                console.log(value)
                console.log(date)
                console.log(endDate)
                let dateTime = value.split('~')
                console.log(dateTime[0])
                console.log(dateTime[1])
                vm.queryParams.orderDate = value
            }
        });

        laydate.render({
            elem: '#createrTime'
            , done: function (value, date, endDate) {
                vm.queryParams.createrTime = value
            }
        });

        vm.initAreaList()
        vm.initTableIns()

    }();
</script>