<%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <div id="yclReportLayout" style="margin: 30px">
        <svg id="svgcanvas" width="100%" height="700" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg">
    <g>
        <rect id="svg_2369" height="427" width="787" y="49.75" x="35.150024" stroke="#000000" fill="#fcfcfc"/>
        <text xml:space="preserve" text-anchor="middle" font-family="serif" font-size="24" id="txt_2370" y="85.75" x="407.150024" stroke-width="0" stroke="#000000" fill="#000000">二楼</text>
        <rect stroke="#000000" id="loc_2375" height="41.999999" width="757.000074" y="178.75" x="48.149982" stroke-width="null" fill="#fcfcfc"/>
        <rect id="loc_2377" height="70.666667" width="41.333333" y="108" x="48.483297" stroke="#000000" fill="#fcfcfc"/>
        <text xml:space="preserve" text-anchor="middle" font-family="serif" font-size="18" id="txt_2379" y="146.416667" x="69.149963" stroke-width="0" stroke="#000000" fill="#000000" transform="rotate(-90 69.14843750000004,140.41406250000003) ">胶带</text>
        <rect id="loc_1" height="70.666667" width="41.333333" y="108" x="94.483328" stroke="#000000" fill="#bf0000"/>
        <text id="txt_2" xml:space="preserve" text-anchor="middle" font-family="serif" font-size="18" y="150.252604" x="113.151519" stroke-width="0" stroke="#000000" fill="#000000" transform="rotate(-90 113.14843750000001,144.2578125) ">无纺布</text>
        <rect id="loc_3" height="70.666667" width="41.333333" y="108" x="141.483328" stroke="#000000" fill="#fcfcfc"/>
        <text id="txt_4" xml:space="preserve" text-anchor="middle" font-family="serif" font-size="18" y="148.244792" x="163.153076" stroke-width="0" stroke="#000000" fill="#000000" transform="rotate(-90 163.14843750000003,142.2421875) ">扎花带</text>
        </g>
        </svg>
    </div>

    <style type="text/css">
    .tooltip{
        font-family:simsun;
        font-size:14px;
        width:150px;
        height:auto;
        position:absolute;
        border-style:solid;
        border-width:1px;
        background-color:white;
        border-radius:5px;
    }
    </style>
    <script src="/wms/static/js/techbloom/d3.min.js" charset="utf-8"></script>

    <script>
    var margin = {
        left: 0,
        top: 0,
        right: 10,
        bottom: 10
    },
    width = $("#yclReportLayout").parent().width(),
    height = $("#yclReportLayout").parent().height(),

    bg_width = 2105,
    bg_height = 1005,
    ratio_init = width / bg_width;

    var zoom = d3.behavior.zoom()
    .scaleExtent([ratio_init, 3])
    .on("zoom", zoomed);

    var drag = d3.behavior.drag()
    .origin(function(d) {
        return d;
    })
    .on("dragstart", dragstarted)
    .on("drag", dragged)
    .on("dragend", dragended);

    var _svg = d3.select("#svgcanvas")
    .attr("width", width)
    .attr("height", height)
    .call(zoom)

    var tooltip = d3.select("body").append("div")
    .attr("class","tooltip") //用于css设置类样式
    .attr("opacity",0.0);

    d3.selectAll("[id^='loc']").on("mouseover",function()
    {
        //uptInfo();
        //设置tooltip文字
        tooltip.html("物料名称："+this.id+"<br/>"+"库存数量："+(new Date().valueOf()).toString().substring(10))
        //设置tooltip的位置(left,top 相对于页面的距离)
        .style("left",(d3.event.pageX)+"px")
        .style("top",(d3.event.pageY+20)+"px")
        .style("opacity",1.0);
    })
    //--鼠标移出事件
    .on("mouseout",function()
    {
        tooltip.style("opacity",0.0);
    });

    // 将画布调整到起始比率
    //zoom.scale(ratio_init);
    //zoom.event(_svg.transition().duration(200));
    function uptInfo() {
    d3.select('#txt_4').text(new Date().valueOf());
    }

    function dottype(d) {
        d.x = +d.x;
        d.y = +d.y;
        d.id = +d.id;
        return d;
    }

    function zoomed() {
        _svg.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
    }

    function dragstarted(d) {
        d3.event.sourceEvent.stopPropagation();
        d3.select(this).classed("dragging", true);
    }

    function dragged(d) {
        d3.select(this).attr("cx", d.x = d3.event.x).attr("cy", d.y = d3.event.y);
    }

    function dragended(d) {
        d3.select(this).classed("dragging", false);
    }

    </script>
