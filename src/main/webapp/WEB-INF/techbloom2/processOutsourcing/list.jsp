<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<div id="ProcessOutsourcingPage" style="margin: 30px">
    <%--query tools--%>
    <blockquote class="layui-elem-quote">
        <form class="layui-form">
            <div class="layui-fluid">
                <div class="layui-row layui-col-space10">
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">单据编号</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" v-model="queryParams.businessCode"/>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">定制单位</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" v-model="queryParams.supplierCode"/>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">处理状态</label>
                            <div class="layui-input-inline">
                               <select id="stateCodeSelect_processOutsourcing"
                                       lay-filter="stateCodeSelect_processOutsourcing"
                                       v-model="queryParams.stateCode">
                                   <option value=""></option>
                               </select>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">发出组织</label>
                            <div class="layui-input-inline">
                                <select id="storeCodeSelect" lay-filter="storeCodeSelect"
                                        v-model="queryParams.storeCode" lay-search="">
                                    <option value=""></option>
                                    <option v-for="(item,index) in arealist" :value="item.ORGANIZATIONID" :key="index">{{item.ORGANIZATIONNAME}}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">发出单位</label>
                            <div class="layui-input-inline">
                                <select id="locatorCodeSelect" lay-filter="locatorCodeSelect"
                                        v-model="queryParams.locatorCode" lay-search="">
                                    <option value=""></option>
                                    <option v-for="(item,index) in storelist" :value="item.id" :key="index">{{item.text}}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">申请日期</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" id="createTime" v-model="queryParams.createTime"/>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">物料编码</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" v-model="queryParams.materialCode"/>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">工单号</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" v-model="queryParams.woCode"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="layui-row layui-col-space10">
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">接收组织</label>
                            <div class="layui-input-inline">
                                <select id="areaCodeSelect" lay-filter="areaCodeSelect"
                                        v-model="queryParams.areaCode">
                                    <option value=""></option>
                                    <option v-for="(item,index) in arealist" :value="item.ORGANIZATIONID" :key="index">{{item.ORGANIZATIONNAME}}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">接收单位</label>
                            <div class="layui-input-inline">
                                <select id="locatorCodeSelect1" lay-filter="locatorCodeSelect1"
                                        v-model="queryParams.locatorCode1">
                                    <option value="">请选择</option>
                                    <option v-for="(item,index) in arealist" :value="item.ORGANIZATIONID" :key="index">{{item.ORGANIZATIONNAME}}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <button type="button" class="layui-btn layui-btn-sm layui-btn-normal"
                                    @click="queryPageView">
                                <i class="layui-icon">&#xe615;</i>查询
                            </button>
                        </div>
                        <div class="layui-inline">
                            <button type="button" class="layui-btn layui-btn-primary layui-btn-sm"
                                    @click="restQueryParams">
                                <i class="layui-icon">&#xe669;</i>重置
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </blockquote>

    <%-- table grid --%>
    <table id="tableGridProcessOutsourcing" lay-filter="tableGridFilterProcessOutsourcing"></table>
    <%-- table grid toolbar --%>
    <script type="text/html" id="tableGridToolbarProcessOutsourcing">
        <div class="layui-btn-container">
            <button type="button" class="layui-btn layui-btn-sm layui-btn-normal" lay-event="addProcessOutsourcing">
                <i class="layui-icon">&#xe66d;</i>打印单据
            </button>
            <button type="button" class="layui-btn layui-btn-sm layui-btn-danger" lay-event="editProcessOutsourcing">
                <i class="layui-icon">&#xe656;</i>预览
            </button>
            <button type="button" class="layui-btn layui-btn-sm layui-btn-danger" lay-event="deleteProcessOutsourcing">
                <i class="layui-icon">&#xe67d;</i>导出
            </button>
            <button type="button" class="layui-btn layui-btn-sm layui-btn-danger" lay-event="deleteProcessOutsourcing">
                <i class="layui-icon">&#xe605;</i>出库确认
            </button>
            <button type="button" class="layui-btn layui-btn-sm layui-btn-danger" lay-event="deleteProcessOutsourcing">
                <i class="layui-icon">&#xe605;</i>入库确认
            </button>
        </div>
    </script>

</div>


<script>
    /*加载地址、数据、layui配置等*/
    const context_path = '${APP_PATH}';
    const table = layui.table,
        form = layui.form,
        layer = layui.layer,
        laydate = layui.laydate;
    /*查询参数获取*/
    let vm = new Vue({
        el: '#ProcessOutsourcingPage',
        data: {
            queryParams: {
                businessCode: "",
                supplierCode: "",
                createTime: "",
                materialCode: "",
                woCode:"",
                stateCode:"",
                storeCode:"",
                locatorCode:"",
                locatorCode1:"",
                areaCode:""
            }
        },
        watch: {},
        methods: {
            /*状态*/
            initStateList(){
                $.get(context_path + '/BaseDicType/qc', /*{}放参数*/{},
                    function (res) {
                    let html = '';
                    let list = res.data;
                    for (let i = 0; i < list.length; i++) {
                        html += '<option value=' + list[i].id + '>' + list[i].text + '</option>';
                    }
                    $("#stateCodeSelect_processOutsourcing").append(html);
                    form.render('select','stateCodeSelect_processOutsourcing');
                })
            },
            /*接收组织下拉获取生产*/
            initAreaList() {
                $.get(context_path + '/wms2/baseinfo/organize/organization/0/0',{},
                    function (res) {
                    let html = '';
                    let list = res.data;
                    for (let i = 0; i < list.length; i++) {
                        html += '<option value=' + list[i].ORGANIZATIONID + '>' + list[i].ORGANIZATIONNAME + '</option>';
                    }
                    console.log(html)/*刷新*/
                    $("#areaCodeSelect").empty();
                    $("#areaCodeSelect").append('<option value="">请选择</option>');
                    $("#areaCodeSelect").append(html);
                    form.render('select');
                })
            },
            /*发出组织下拉框*/
            initStoreList() {
                $.get(context_path + '/wms2/baseinfo/organize/organization/0/0',{},
                    function (res) {
                    let html = '';
                    let list = res.data;
                    for (let i = 0; i < list.length; i++) {
                        html += '<option value=' + list[i].ORGANIZATIONID + '>' + list[i].ORGANIZATIONNAME + '</option>';
                    }
                    $("#storeCodeSelect").empty();
                    $("#storeCodeSelect").append('<option value="">请选择</option>');
                    $("#storeCodeSelect").append(html);
                    form.render('select');
                })
            },
            /*发出单位下拉框*/
            initLocatorList() {
                $.get(context_path + '/ycl/baseinfo/factorymachine/organize/0',
                    {
                        departCode:0
                    },
                    function (res) {
                    let html = '';
                    let list = res.data;
                    for (let i = 0; i < list.length; i++) {
                        html += '<option value=' + list[i].departCode + '>' + list[i].departDesc + '</option>';
                    }
                    console.log(html)
                    $("#locatorCodeSelect").empty();
                    $("#locatorCodeSelect").append('<option value="">请选择</option>');
                    $("#locatorCodeSelect").append(html);
                    form.render('select');
                })
            },
            /*接收单位*/
            initUnitList(){
                $.get(context_path + '/ycl/baseinfo/factorymachine/organize/0',
                    {
                        departCode:0
                    },
                    function (res) {
                        let html = '';
                        let list = res.data;
                        for (let i = 0; i < list.length; i++) {
                            html += '<option value=' + list[i].departCode + '>' + list[i].departDesc + '</option>';
                        }
                        console.log(html)
                        $("#locatorCodeSelect1").empty();
                        $("#locatorCodeSelect1").append('<option value="">请选择</option>');
                        $("#locatorCodeSelect1").append(html);
                        form.render('select');
                    })
            },
            /*list数据源*/
            initTableIns() {
                table.render({
                    elem: '#tableGridProcessOutsourcing'
                    , url: context_path + '/ycl/processOutsourcing/processOutsourcing/list'
                    , page: true
                    , toolbar: '#tableGridToolbarProcessOutsourcing'
                    , id: "tableGridSetProcessOutsourcing"
                    , size: 'sm'
                    , defaultToolbar: []
                    , cols: [[
                        {type: 'checkbox'}
                        , {field: 'receivestate', title: '状态'}
                        , {field: 'ticketno', title: '单据编号'}
                        , {field: 'moName', title: '工单号'}
                        , {field: 'goodsName', title: '物料编码'}
                        , {field: 'goodsType', title: '物料类型'}
                        , {field: 'qty', title: '数量'}
                        , {field: 'unit', title: '定制单位'}
                        , {field: 'receiveFacCode', title: '接收组织',value:95}/*默认95*/
                        , {field: 'receiveUnit', title: '接收单位'}
                        , {field: 'orgid', title: '发出组织'}
                        , {field: 'sourceFacDesc', title: '发出单位'}/*厂*/
                        , {field: 'opDesc', title: '发出厂工序'}
                        , {field: 'invoutuser', title: '出库确认人'}
                        , {field: 'invouttime', title: '出库确认时间'}
                        , {field: 'invinuser', title: '入库确认人'}
                        , {field: 'invintime', title: '入库确认时间'}
                        , {field: 'createUser', title: '申请人'}
                        , {field: 'createDate', title: '申请日期'}
                    ]]
                });
            },
            /*查询*/
            queryPageView() {
                console.log(vm.queryParams)
                table.reload('tableGridSetProcessOutsourcing', {
                    where: vm.queryParams
                });
            },
            /*重置*/
            restQueryParams() {
                vm.queryParams = {
                    businessCode: "",
                    supplierCode: "",
                    createTime: "",
                    materialCode: "",
                    woCode:"",
                    stateCode:"",
                    storeCode:"",
                    locatorCode:"",
                    locatorCode1:"",
                    areaCode:""
                }
                form.render('select');
                table.reload('tableGridSetProcessOutsourcing', {
                    where: vm.queryParams
                });
            }
        },
        created() {},
        mounted() {}
    })

    /*校验——新增弹窗样式*/
    table.on('toolbar(tableGridFilterProcessOutsourcing)', function (obj) {
        const ReturnStatusData = table.checkStatus(obj.config.id).data
        console.log(ReturnStatusData)
        let checkCodes = ReturnStatusData.map(item => item.checkCode).toString()

        switch (obj.event) {
            case 'addProcessOutsourcing':
                layer.open({
                    title: '出库确认'
                    , type: 2
                    , area: ['1200px', '600px'] //宽高
                    , maxmin: true
                    , content: context_path + '/ycl/report/Return/toEdit'
                });
                break;
            case 'editProcessOutsourcing':
                layer.open({
                    title: '入库确认'
                    , type: 2
                    , area: ['1200px', '600px'] //宽高
                    , maxmin: true
                    , content: context_path + '/ycl/report/Return/toEdit'
                });
                break;
        }
    });

    //监听select
    form.on('select(stateCodeSelect_processOutsourcing)', function (data) {
        vm.queryParams.areaCode = data.value
    });
    //监听select
    form.on('select(areaCodeSelect)', function (data) {
        vm.queryParams.areaCode = data.value
    });
    //监听select
    form.on('select(storeCodeSelect)', function (data) {
        $.ajax({
            url:context_path + '/ycl/baseinfo/factorymachine/organize/0/0',
            type:'get',
            data:{storeCode:data.value},
            success:function(res){
                vm.storelist = res.data;
                vm.$nextTick(() => {
                    form.render('select','storeCode');
                })
            }
        });
    });

    // init
    ;!function () {

        //日期区间选择器
        laydate.render({
            elem: '#createTime'
            , type: 'datetime'
            , range: '~'
            , done: function (value, date, endDate) {
                console.log(value)
                console.log(date)
                console.log(endDate)
                let dateTime = value.split('~')
                console.log(dateTime[0])
                console.log(dateTime[1])
                vm.queryParams.createTime = value
            }
        });
        /*时间*/
        laydate.render({
            elem: '#createrTime'
            , done: function (value, date, endDate) {
                vm.queryParams.createrTime = value
            }
        });

        vm.initStateList()
        vm.initAreaList()
        vm.initUnitList()
        vm.initTableIns()
        vm.initStoreList()
        vm.initLocatorList()

    }();
</script>