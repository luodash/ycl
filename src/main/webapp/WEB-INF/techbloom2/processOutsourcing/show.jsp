<%--
  Created by IntelliJ IDEA.
  User: 86176
  Date: 2020/6/25
  Time: 19:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<head>
    <link rel="stylesheet" href="${APP_PATH}/static/layui/css/layui.css" type="text/css"/>
    <style type="text/css">
        .layui-input {
            height: 30px;
        }
    </style>
</head>
<script src="${APP_PATH}/static/layui/layui.all.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<div id="yclTransferOrderEdit">
    <form class="layui-form" lay-filter="transferorderForm">
        <blockquote class="layui-elem-quote">
            <div class="layui-fluid">
                <div class="layui-row">
                    <div class="layui-col-sm4">
                        <div class="layui-inline">
                            <label style="width: 65px">调出厂区</label>
                            <div class="layui-form layui-input-inline" lay-filter="fromAreaCode">
                                <select id="fromAreaCode" lay-filter="fromAreaCode" lay-search="">
                                    <option value="">请选择</option>
                                    <option v-for="(item,index) in arealist" :value="item.id" :key="index">{{item.text}}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm4">
                        <div class="layui-inline">
                            <label style="width: 65px">调出仓库</label>
                            <div class="layui-form layui-input-inline" lay-filter="fromStoreCode">
                                <select id="fromStoreCode" lay-filter="fromStoreCode" lay-search="">
                                    <option value="">请选择</option>
                                    <option v-for="(item,index) in fromStorelist" :value="item.id" :key="index">{{item.text}}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="layui-row" style="margin-top: 10px;">
                    <div class="layui-col-sm4">
                        <div class="layui-inline">
                            <label style="width: 65px">调入厂区</label>
                            <div class="layui-form layui-input-inline" lay-filter="toAreaCode">
                                <select id="toAreaCode" lay-filter="toAreaCode" lay-search="">
                                    <option value="">请选择</option>
                                    <option v-for="(item,index) in arealist" :value="item.id" :key="index">{{item.text}}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm4">
                        <div class="layui-inline">
                            <label style="width: 65px">调入仓库</label>
                            <div class="layui-form layui-input-inline" lay-filter="toStoreCode" lay-search="">
                                <select id="toStoreCode">
                                    <option value="">请选择</option>
                                    <option v-for="(item,index) in toStorelist" :value="item.id" :key="index">{{item.text}}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm4">
                        <div class="layui-inline">
                            <button type="button" @click="submitTransfer" class="layui-btn layui-btn-sm">
                                <i class="layui-icon">&#xe605;</i>提交
                            </button>
                        </div>
                        <div class="layui-inline">
                            <button type="button" class="layui-btn layui-btn-primary layui-btn-sm">
                                <i class="layui-icon">&#xe669;</i>重置
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </blockquote>
        <div id="toolbar">
            <div style="margin-left: 15px;">
                <button type="button" @click="delLine" class="layui-btn layui-btn-sm layui-btn-danger">
                    <i class="layui-icon">&#xe640;</i>删除
                </button>
                <div class="layui-inline" style="margin-left: 10px;">
                    <label style="width: 65px">物料编码：</label>
                    <div class="layui-form layui-input-inline" lay-filter="materialCode">
                        <select id="materialCode" lay-filter="materialCode" lay-search="">
                            <option value="">请选择</option>
                            <option v-for="(item,index) in materialCodelist" :value="item.materialCode" :key="index">{{item.materialCode}}</option>
                        </select>
                    </div>
                </div>
                <button type="button" @click="addLine" class="layui-btn layui-btn-sm layui-btn-normal">
                    <i class="layui-icon">&#xe654;</i>添加
                </button>
            </div>
        </div>

        <table id="tableGridCheckEdit"></table>

    </form>
</div>

<script>
    const context_path = '${APP_PATH}';
    const form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        table = layui.table,
        $ = layui.jquery;
    var tableIns;
    let vm = new Vue({
        el: '#yclTransferOrderEdit',
        data: {
            tbData: [],
            arealist:[],
            fromStorelist:[],
            toStorelist:[],
            materialCodelist:[],
            params: {
                fromAreaCode: "",
                fromStoreCode: "",
                toAreaCode:'',
                toStoreCode:'',
                line: []
            }
        },
        methods: {
            delLine(){
                var selectData = layui.table.checkStatus('tableGridCheckEdit').data;
                var oldData = table.cache['tableGridCheckEdit'];
                for(var i = 0; i < oldData.length; i++){
                    if(oldData[i].tempId === selectData[0].tempId){
                        oldData.splice(i,1);
                    }
                }
                tableIns.reload({
                    data : oldData
                });
            },
            addLine(){
                if ($('#materialCode').val()=='')
                {
                    layer.alert('请选择物料编码');
                    return;
                }
                $.get(context_path + '/ycl/report/inventory/listView', {
                    materialCode: $('#materialCode').val()
                }, function (res) {
                    var oldData = table.cache['tableGridCheckEdit'];
                    var newRow = res.data[0];
                    newRow.tempId = new Date().valueOf();
                    oldData.push(newRow);
                    tableIns.reload({
                        data : oldData
                    });
                })
            },
            submitTransfer() {
                if (!this.formVerify())
                {
                    return;
                }
                this.params.line = table.cache['tableGridCheckEdit']
                var isError = false;
                this.params.line.forEach(m=>{
                    if (m.outQuantity==''||isNaN(m.outQuantity))
                    {
                        isError=true;
                    }
                })
                if (isError)
                {
                    layer.alert('移出数量为空或非数字!');
                    return;
                }
                $.ajax({
                    type: 'POST',
                    url: context_path + "/ycl/insidewarehouse/transferOrder/save",
                    contentType: "application/json",
                    async: false,
                    dataType: "json",
                    data: JSON.stringify(this.params),
                    success: function (res) {
                        console.log(res)
                        // if (res.result === false) {
                        //     layer.msg(res.msg, {icon: 5});
                        //     return false;
                        // } else {
                        //     layer.msg(res.msg, {icon: 1});
                        // }
                        window.parent.tableRender()
                        console.log(1,this.params);
                        let index = parent.layer.getFrameIndex(window.name);
                        parent.layer.close(index);
                    }
                })

            },
            formVerify(){
                this.params.fromAreaCode = $('#fromAreaCode').val();
                this.params.fromStoreCode = $('#fromStoreCode').val();
                this.params.toAreaCode = $('#toAreaCode').val();
                this.params.toStoreCode = $('#toStoreCode').val();
                if (this.params.fromAreaCode=='')
                {
                    layer.alert('请选择调出厂区!');
                    return false;
                }
                if (this.params.fromStoreCode=='')
                {
                    layer.alert('请选择调出仓库!');
                    return false;
                }
                if (this.params.toAreaCode=='')
                {
                    layer.alert('请选择调入厂区!');
                    return false;
                }
                if (this.params.toStoreCode=='')
                {
                    layer.alert('请选择调入仓库!');
                    return false;
                }
                return true;
            },
            initDate(){
                var url = window.location.href;
                var dz_url = url.split('id=')
                if (dz_url.length>1)
                {
                    $("#fromAreaCode").each(function() {
                        $(this).children("option").each(function() {
                            // 判断需要对那个选项进行回显
                            if (this.value == '92') {
                                $(this).attr("selected","selected");
                            }
                        });
                        form.render('select','fromAreaCode');
                        $.ajax({
                            url:context_path + '/warehouselist/getYclWarehouseByFactory',
                            type:'post',
                            data:{factoryCode:92},
                            success:function(res){
                                vm.fromStorelist = res.wareHouseList;
                                vm.$nextTick(() => {
                                    form.render('select','fromStoreCode');
                                    $("#fromStoreCode").each(function() {
                                        $(this).children("option").each(function() {
                                            // 判断需要对那个选项进行回显
                                            if (this.value == '981') {
                                                $(this).attr("selected","selected");
                                            }
                                        });
                                        form.render('select','fromStoreCode');
                                    })
                                })
                            }
                        });
                    })
                }
            }
        },
        created() {
            $.post(context_path + '/factoryArea/getFactoryList', {
                queryString: "",
                pageSize: 15,
                pageNo: 1
            }, res=> {
                this.arealist = res.result;
                this.$nextTick(() => {
                    form.render('select','fromAreaCode');
                    form.render('select','toAreaCode');

                    this.initDate();
                });
            })
        },
        mounted() {
            form.render();
            tableIns = table.render({
                elem: '#tableGridCheckEdit'
                ,data: this.tbData
                ,size: 'sm'
                ,cols: [[
                    {type: 'checkbox'}
                    ,{field: 'materialCode', title: '物料编码'}
                    ,{field: 'materialName', title: '物料名称'}
                    ,{field: 'lotsNum', title: '批次号', edit: 'text'}
                    ,{field: 'quality', title: '移出数量'}
                    ,{field: 'locatorCode', title: '上架库位', edit: 'text'}
                    ,{field: 'outQuantity', title: '上架数量', edit: 'text'}
                ]],
                done: function(res, curr, count){
                    this.tbData = res.data;
                }
            });
        }
    })

    //监听select
    form.on('select(fromAreaCode)', function (data) {
        // $.post(context_path + '/car/selectWarehouse', {
        //     queryString: "",
        //     pageSize: 9999,
        //     pageNo: 1,
        //     factoryCodeId: data.value
        // }, function (res) {
        //     vm.fromStorelist = res.result;
        //     vm.$nextTick(() => {
        //         form.render('select','fromStoreCode');
        //     })
        // })
        $.ajax({
            url:context_path + '/warehouselist/getYclWarehouseByFactory',
            type:'post',
            data:{factoryCode:data.value},
            success:function(res){
                vm.fromStorelist = res.data;
                vm.$nextTick(() => {
                    form.render('select','fromStoreCode');
                })
            }
        });
    });
    //监听select
    form.on('select(toAreaCode)', function (data) {
        $.post(context_path + '/car/selectWarehouse', {
            queryString: "",
            pageSize: 9999,
            pageNo: 1,
            factoryCodeId: data.value
        }, function (res) {
            vm.toStorelist = res.result;
            vm.$nextTick(() => {
                form.render('select','toStoreCode');
            })
        })
    });
    //监听select
    form.on('select(fromStoreCode)', function (data) {
        $.get(context_path + '/ycl/report/inventory/listView', {
            storeCode: data.value
        }, function (res) {
            vm.materialCodelist = res.data;
            vm.$nextTick(() => {
                form.render('select','materialCode');
            })
        })
    });
    //监听select
    form.on('select(materialCode)', function (data) {
        if (!vm.formVerify())
        {
            return;
        }
    });
</script>

