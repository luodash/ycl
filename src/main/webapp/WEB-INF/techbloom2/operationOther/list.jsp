<%--其他出入库--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<div id="yclOperationOtherListPage" style="margin: 15px">
    <%--query tools查询栏--%>
    <blockquote class="layui-elem-quote">
        <form class="layui-form" id="operationOtherForm">
            <div class="layui-fluid">
                <div class="layui-row layui-col-space10">
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">业务类型</label>
                            <div class="layui-input-inline">
                                <select name="businessType" v-model="queryParams.businessType" lay-filter="businessType"
                                        lay-search>
                                    <option value="">全部</option>
                                    <option value="1">其他出库</option>
                                    <option value="2">其他入库</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">业务目的</label>
                            <div class="layui-input-inline">
                                <select id="businessPurpose" lay-filter="businessPurpose" lay-search>
                                    <option value="">请选择</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">日期</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" autocomplete="off" id="createTime" />
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <button type="button" class="layui-btn layui-btn-sm layui-btn-normal"
                                    @click="queryPageView">
                                <i class="layui-icon">&#xe615;</i>查询
                            </button>
                        </div>
                        <div class="layui-inline">
                            <button type="button" class="layui-btn layui-btn-primary layui-btn-sm"
                                    @click="resetQueryParams">
                                <i class="layui-icon">&#xe669;</i>重置
                            </button>
                        </div>
                    </div>
                </div>

                <div class="layui-row layui-col-space10">
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">单据编号</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" name="otherCode" v-model="queryParams.otherCode"
                                       lay-filter="otherCode" />
                            </div>
                        </div>
                    </div>

                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">物料编码</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" name="materialCode"
                                       v-model="queryParams.materialCode" lay-filter="materialCode" />
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">领用用途</label>
                            <div class="layui-input-inline">
                                <select id="effect" lay-filter="effect" lay-search>
                                    <option value="">请选择</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </blockquote>

    <table id="tableGrid_operationOther" lay-filter="tableGridFilter_operationOther"></table>
    <%-- table grid toolbar --%>
    <iframe style="visibility:hidden;" id="myiframe_operationOther" name="myiframe_operationOther" src="" width=0
            height=0></iframe>
    <%-- table grid toolbar --%>
    <script type="text/html" id="tableGridToolbar_operationOther">
        <div class="layui-btn-container">
            <button type="button" class="layui-btn layui-btn-sm layui-btn-normal" lay-event="add">
                <i class="layui-icon">&#xe654;</i>新增
            </button>
            <button type="button" lay-event="printOther" class="layui-btn layui-btn-sm layui-btn-normal">
                <i class="layui-icon">&#xe66d;</i>打印单据
            </button>
            <button type="button" lay-event="printOtherOut" class="layui-btn layui-btn-sm layui-btn-normal">
                <i class="layui-icon">&#xe66d;</i>打印出厂单
            </button>
        </div>
    </script>

</div>
<script src="/wms/plugins/public_components/js/jquery-2.1.4.js"></script>
<script src="/wms/static/js/techbloom/LodopFuncs.js" charset="utf-8"></script>
<script>
    const context_path = '${APP_PATH}';
    const table = layui.table,
        layer = layui.layer,
        laydate = layui.laydate,
        form = layui.form;

    let vm = new Vue({
        el: '#yclOperationOtherListPage',
        data: {
            businessTypeData: [{ id: "1", text: "其他出库" }, { id: "2", text: "其他入库" }],
            stateData: [{ id: "1", text: "已处理" }, { id: "0", text: "未处理" }],
            queryParams: {
                businessType: "",
                otherCode: "",
                materialCode: "",
                createUser: "",
                createTimeStart: "",
                createTimeEnd: "",
                businessPurpose: "",
                effect: ""
            }
        },
        methods: {
            initCreateTime() {
                //日期时间选择器
                laydate.render({
                    elem: '#createTime'
                    , type: 'date'
                    , range: '~'
                    // , btns: ['confirm']
                    // , value: vm.queryParams.applyTimeStart + ' ~ ' + vm.queryParams.applyTimeEnd
                    , done: function (value, date, endDate) {
                        let dateTime = value.split(' ~ ');
                        vm.queryParams.createTimeStart = dateTime[0];
                        vm.queryParams.createTimeEnd = dateTime[1];
                    }
                });
            },
            initSelect(async, url, data, domId, callback) {
                let dataJson = { queryString: "", limit: 100, page: 1 };
                let json = $.extend({}, dataJson, data);
                let valueField = data.valueField || 'id',
                    textField = data.textField || 'text';
                $.ajax({
                    type: 'GET',
                    url: context_path + url,
                    data: json,
                    async: async,
                    dataType: "json",
                    success: function (res) {
                        let html = '';
                        let list = res.data;
                        if (!!callback) {
                            callback(list);
                        }
                        for (let i = 0; i < list.length; i++) {
                            html += '<option value=' + list[i][valueField] + '>' + list[i][textField] + '</option>';
                        }
                        let $id = $('#' + domId);
                        $id.empty();
                        $id.append('<option value="">请选择</option>');
                        $id.append(html);
                        form.render('select');
                    }
                });
            },
            resetSelectOptions(data, domId, settings) {
                settings = settings || {};
                let valueField = settings.valueField || 'id',
                    textField = settings.textField || 'text';
                let html = '';
                for (let i = 0; i < data.length; i++) {
                    html += '<option value=' + data[i][valueField] + '>' + data[i][textField] + '</option>';
                }
                let $id = $('#' + domId);
                $id.empty();
                $id.append('<option value="">请选择</option>');
                $id.append(html);
                form.render('select');
            },
            initBusinessPurposeSelect(type) {
                this.initSelect(true, '/BaseDicType/otherTransaction/' + type, {}, 'businessPurpose');
            },
            // 领用用途
            initEffectSelect() {
                this.initSelect(true, '/BaseDicType/lyyt/0', {}, 'effect');
            },
            initTable() {
                table.render({
                    elem: '#tableGrid_operationOther'
                    , url: context_path + '/ycl/operation/other/list'
                    , page: true
                    , toolbar: '#tableGridToolbar_operationOther'
                    , id: "tableGridSet_operationOther"
                    , size: 'sm'
                    , defaultToolbar: []
                    , limit: 15
                    , limits: [15, 20, 30, 40, 50, 60, 70, 80, 90]
                    , cols: [[
                        { type: 'radio' }
                        , {
                            field: 'businessType', title: '业务类型', width: 80, templet: function (d) {
                                for (let data of vm.businessTypeData) {
                                    if (data.id == d.businessType) {
                                        return data.text;
                                    }
                                }
                            }
                        }
                        , { field: 'businessPurposeName', title: '业务目的', width: 170 }
                        , {
                            field: 'state', title: '状态', width: 70, templet: function (d) {
                                for (let data of vm.stateData) {
                                    if (data.id == d.state) {
                                        return data.text;
                                    }
                                }
                            }
                        }
                        , { field: 'otherCode', title: '单据编号', width: 155 }
                        , { field: 'lineCode', title: '行号', width: 55 }
                        , { field: 'materialCode', title: '物料编码', width: 140 }
                        , { field: 'materialName', title: '物料名称', width: 200 }
                        , { field: 'orderQuantity', title: '数量', width: 80 }
                        , { field: 'primaryUnit', title: '单位', width: 60 }
                        , { field: 'lotsNum', title: '批次号', width: 120 }
                        , { field: 'storeCodeName', title: '仓库', width: 120 }
                        , { field: 'locatorCode', title: '库位', width: 120 }
                        , { field: 'locatorCodeName', title: '库位名称', width: 160 }
                        , { field: 'quantity', title: '出入数量', width: 120 }
                        , { field: 'price', title: '单价', width: 80 }
                        , { field: 'supplierCode', title: '供应商编码', width: 100 }
                        , { field: 'supplierName', title: '供应商名称', width: 220 }
                        , { field: 'effect', title: '领用用途', width: 120 }
                        , { field: 'creator', title: '创建人', width: 80 }
                        , { field: 'createTime', title: '创建时间', width: 145 }
                    ]]
                });
            },
            queryPageView() {
                table.reload('tableGridSet_operationOther', {
                    where: vm.queryParams,
                    page: { curr: 1 }
                });
            },
            resetQueryParams() {
                vm.queryParams = {
                    businessType: "",
                    otherCode: "",
                    materialCode: "",
                    createUser: "",
                    createTimeStart: "",
                    createTimeEnd: "",
                    businessPurpose: "",
                    effect: ""
                }
                // this.initCreateTime();
                $("#operationOtherForm")[0].reset();
                this.queryPageView();
            },
            add() {
                let index = layer.open({
                    title: '新增'
                    , type: 2
                    , area: ['1200px', '600px'] //宽高
                    , maxmin: true
                    , content: context_path + '/ycl/operation/other/toEdit'
                });
                layer.full(index);
            },
            printOtherOut() {
                let selectData = table.checkStatus('tableGridSet_operationOther').data;
                if (selectData.length == 0) {
                    layer.msg('请选中一条记录操作', { icon: 2 });
                    return;
                }
                if (selectData[0].businessType != 1) {
                    layer.msg('请选择 其他出库 数据', { icon: 2 });
                    return;
                }
                $.ajax({
                    type: 'GET',
                    url: context_path + '/ycl/operation/other/toPrintOut?otherCode=' + selectData[0].otherCode,
                    async: false,
                    dataType: "json",
                    success: res => {
                        console.log(res)
                        if (res.code == 0) {
                            let obj = {
                                line: []
                            };
                            let objData = res.data;
                            obj.goodType = '自有' //物料类型
                            obj.billType = objData.businessPurposeName   //单据类型
                            obj.driver = ''   //驾驶员号码
                            obj.autoNum = ''   //车牌号
                            obj.company = ''   //公司
                            obj.transport = ''   //运输方
                            obj.sendout = ''   //发出单位
                            obj.receiver = ''   //接收单位
                            obj.sendNum = ''   //派车单号
                            obj.applyUser = objData.workNo   //申请人
                            obj.applyTel = ''   //申请人号码
                            obj.applyDate = objData.createTimeString   //申请日期
                            obj.remark = objData.remark   //备注
                            obj.orderCode = objData.otherCode  //导入来源
                            objData.lineList.forEach(m => {
                                const line = {};
                                line.materialCode = m.materialCode  //物料编码
                                line.materialName = m.materialName  //物料名称
                                line.unit = m.primaryUnit  //单位
                                line.quality = m.quantity  //数量
                                line.orderCode = m.otherCode  //导入来源
                                line.comments = m.remark  //备注
                                obj.line.push(line)
                            })
                            layer.open({
                                title: '打印',
                                type: 2,
                                area: ['1200px', '600px'], //宽高
                                maxmin: true,
                                content: context_path + '/ycl/purchasein/factory/printView?obj=' + encodeURI(JSON.stringify(obj))
                            });
                        }
                    }
                })
            },
            printOther(otherCode) {
                var frm = document.getElementById('myiframe_operationOther');
                frm.src = '/wms/ycl/operation/other/toPrint?otherCode=' + otherCode;
                $(frm).load(function () {
                    LODOP = getLodop();
                    var strHtml = frm.contentWindow.document.documentElement.innerHTML;
                    LODOP.ADD_PRINT_HTM(0, 0, "100%", "100%", strHtml);
                    LODOP.PREVIEW();
                });
            }
        },
        created() {
        },
        mounted() {
            this.initTable();
            this.initCreateTime();
            this.initEffectSelect();
            form.render('select');
        }

    })

    table.on('row(tableGridFilter_operationOther)', function (obj) {
        //选中行样式
        obj.tr.addClass('layui-table-click').siblings().removeClass('layui-table-click');
        //选中radio样式
        obj.tr.find('i[class="layui-anim layui-icon"]').trigger("click");
    });

    table.on('toolbar(tableGridFilter_operationOther)', function (obj) {
        let length;
        let selectData;
        switch (obj.event) {
            case 'add':
                vm.add();
                break;
            case 'printOtherOut':
                vm.printOtherOut();
                break;
            case 'printOther':
                selectData = table.checkStatus(obj.config.id).data;
                length = selectData.length;
                if (length == 0) {
                    layer.msg('请选择一行数据', { icon: 2 });
                    return;
                }
                vm.printOther(selectData[0].otherCode);
                break;
        }
    });

    //监听select
    form.on('select(businessType)', function (data) {
        vm.queryParams.businessType = data.value;
        vm.queryParams.businessPurpose = "";
        if (!!data.value) {
            vm.initBusinessPurposeSelect(data.value);
        } else {
            vm.resetSelectOptions([], 'businessPurpose', {});
        }
    });
    form.on('select(businessPurpose)', function (data) {
        vm.queryParams.businessPurpose = data.value;
    });
    form.on('select(effect)', function (data) {
        vm.queryParams.effect = data.value;
    });

    function tableRender() {
        vm.queryPageView();
    }

</script>