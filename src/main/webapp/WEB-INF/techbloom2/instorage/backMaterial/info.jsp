<%@ page language="java" import="java.lang.*" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
%>
<div class="row-fluid" style="height: inherit;margin:0px;border: 0px">
    <div style="margin-left: 30px">
        <ul class="form-elements">
            <li class="field-group field-fluid3">
                <label class="inline" for="warehouse_list_factoryCode" style="margin-right:20px;width: 100%;">
                    <span class="form_label" style="width:80px;">退货单号：</span>
                    <input id="warehouse_list_factoryCode" name="factoryCode" type="text"
                           style="width: calc(100% - 85px);">
                </label>
            </li>
            <li class="field-group field-fluid3">
                <label class="inline" for="warehouse_list_code" style="margin-right:20px;width: 100%;">
                    <span class="form_label" style="width:80px;">申请日期：</span>
                    <input id="warehouse_list_code" name="code" type="text" style="width: calc(100% - 85px);">
                </label>
            </li>
            <li class="field-group field-fluid3">
                <label class="inline" for="warehouse_list_name" style="margin-right:20px;width: 100%;">
                    <span class="form_label" style="width:80px;">申请部门/机台：</span>
                    <input id="warehouse_list_name" name="name" type="text" style="width: calc(100% - 85px);">
                </label>
            </li>
            <li class="field-group-top field-group field-fluid3">
                <label class="inline" for="warehouse_list_name" style="margin-right:20px;width: 100%;">
                    <span class="form_label" style="width:80px;">申请人：</span>
                    <input id="warehouse_list_name2" name="name" type="text" style="width: calc(100% - 85px);">
                </label>
            </li>
            <li class="field-group field-fluid3">
                <label class="inline" for="warehouse_list_code" style="margin-right:20px;width: 100%;">
                    <span class="form_label" style="width:95px;">联系电话：</span>
                    <input id="warehouse_list_code2" name="code" type="text" style="width: calc(100% - 100px);">
                </label>
            </li>
        </ul>
    </div>
    <input type="hidden" id="outstorage_info_infoId" value="${infoId}">
    <!-- 表格div -->
    <div id="outstorage_info_grid-div-c" style="width:100%;margin:10px auto;">
        <!-- 	表格工具栏 -->
        <div id="outstorage_info_fixed_tool_div" class="fixed_tool_div detailToolBar">
            <div id="outstorage_info___toolbar__-c" style="float:left;overflow:hidden;"></div>
        </div>

        <form id="hiddenForm" action="<%=path%>/outsrorageCon/detailExcel.do" method="POST" style="display: none;">
            <input id="hiddenForm_ids" name="ids" value=""/>
            <input id="info_id" name="infoid" value="">
        </form>
        <div style="margin-bottom:5px;margin-left: 25px;">
            <span class="btn btn-info" id="export">
               <i class="ace-icon fa fa-check bigger-110"></i>打印单据
            </span>
        </div>

        <!-- 物料详情信息表格 -->
        <table id="outstorage_info_grid-table-c" style="width:100%;height:100%;"></table>
        <!-- 表格分页栏 -->
        <div id="outstorage_info_grid-pager-c"></div>
    </div>
</div>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriDataDetail;
    var _grid_detail;        //表格对象

    _grid_detail = jQuery("#outstorage_info_grid-table-c").jqGrid({
        url: context_path + "/outsrorageCon/detailList.do?id=" + $("#outstorage_info_infoId").val(),
        datatype: "json",
        colNames: ["详情主键", "物料编号", "物料名称", "单位", "数量", "物料二维码", "备注"],
        colModel: [
            {name: "id", index: "id", hidden: true},
            {name: "qaCode", index: "qaCode", width: 40},
            {name: "qaCode", index: "qaCode", width: 40},
            {name: "batchNo", index: "batch_No", width: 30},
            {name: "dishcode", index: "dishcode", width: 40},
            {name: "meter", index: "meter", width: 10},
            {name: "unit", index: "unit", width: 10}
        ],
        rowNum: 20,
        rowList: [10, 20, 30],
        pager: "#outstorage_info_grid-pager-c",
        sortname: "id",
        sortorder: "asc",
        altRows: true,
        viewrecords: true,
        caption: "详情列表",
        autowidth: true,
        multiselect: true,
        multiboxonly: true,
        loadComplete: function (data) {
            var table = this;
            setTimeout(function () {
                updatePagerIcons(table);
                enableTooltips(table);
            }, 0);
            oriDataDetail = data;
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext: "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据",
    });
    //在分页工具栏中添加按钮
    $("#outstorage_info_grid-table-c").navGrid("#outstorage_info_grid-pager-c",
        {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false
        }).navButtonAdd("#outstorage_info_grid-pager-c", {
        caption: "",
        buttonicon: "ace-icon fa fa-refresh green",
        onClickButton: function () {
            $("#outstorage_info_grid-table-c").jqGrid('setGridParam',
                {
                    url: context_path + "/outsrorageCon/detailList.do?id=" + $("#outstorage_info_infoId").val()
                }
            ).trigger("reloadGrid");
        }
    });

    $(window).on("resize.jqGrid", function () {
        $("#outstorage_info_grid-table-c").jqGrid("setGridWidth", $("#outstorage_info_grid-div-c").width() - 3);
        $("#outstorage_info_grid-table-c").jqGrid("setGridHeight", (document.documentElement.clientHeight - $("#outstorage_info_grid-pager-c").height() - 380));
    });
    $(window).triggerHandler("resize.jqGrid");

    $("#export").click(function () {
        $("#hiddenForm_ids").val(jQuery("#outstorage_info_grid-table-c").jqGrid("getGridParam", "selarrrow"));
        $("#info_id").val($("#outstorage_info_infoId").val());

        $("#hiddenForm").submit();
    });

</script>