<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="list_grid-div">
    <form id="list_hiddenForm" action="<%=path%>/warehouselist/materialExcel" method="POST"
          style="display: none;">
        <input name="ids" id="list_ids" value=""/>
        <input name="queryFactoryCode" id="list_queryFactoryCode" value="">
        <input name="queryCode" id="list_queryCode" value="">
        <input name="queryName" id="list_queryName" value="">
        <input name="queryExportExcelIndex" id="list_queryExportExcelIndex" value=""/>
    </form>
    <form id="list_hiddenQueryForm" style="display:none;">
        <input name="factoryCode" value=""/>
        <input name="code" value=""/>
        <input name="name" value=""/>
    </form>
    <c:if test="${operationCode.webSearch==1}">
        <div class="query_box" id="list_yy" title="查询选项">
            <form id="list_queryForm" style="max-width:100%;">
                <ul class="form-elements">
                    <li class="field-group field-fluid3">
                        <label class="inline" for="list_supplierName" style="margin-right:20px;width: 100%;">
                            <span class="form_label" style="width:80px;">供应商名称：</span>
                            <input id="list_supplierName" name="supplierName" type="text"
                                   style="width: calc(100% - 85px);" placeholder="">
                        </label>
                    </li>
                    <li class="field-group field-fluid3">
                        <label class="inline" for="list_code" style="margin-right:20px;width: 100%;">
                            <span class="form_label" style="width:80px;">入库日期：</span>
                            <input id="list_code" name="code" type="text" style="width: calc(100% - 85px);"
                                   placeholder="">
                        </label>
                    </li>
                    <li class="field-group field-fluid3">
                        <label class="inline" for="list_name" style="margin-right:20px;width: 100%;">
                            <span class="form_label" style="width:80px;">送货单号：</span>
                            <input id="list_name" name="name" type="text" style="width: calc(100% - 85px);"
                                   placeholder="">
                        </label>
                    </li>
                </ul>
                <div class="field-button" style="">
                    <div class="btn btn-info" onclick="list_queryOk();">
                        <i class="ace-icon fa fa-check bigger-110"></i>查询
                    </div>
                    <div class="btn" onclick="list_reset();"><i class="ace-icon icon-remove"></i>重置</div>
                </div>
            </form>
        </div>
    </c:if>
    <div id="list_fixed_tool_div" class="fixed_tool_div">
        <div id="list_warehouse_entry_toolbar" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="instorage_outsideProcess_warehouseEntry_list_grid-table" style="width:100%;height:100%;"></table>
    <div id="list_grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
    var list_oriData;
    var list_grid;
    var list_dynamicDefalutValue = "cbe154ed57f14f8e803c4d0982a4d18c";
    var list_exportExcelIndex;

    $("input").keypress(function (e) {
        if (e.which == 13) {
            list_queryOk();
        }
    });

    $(function () {
        $(".toggle_tools").click();
    });

    $("#list_warehouse_entry_toolbar").iToolBar({
        id: "list___tb__01",
        items: [
            {
                label: "导出",
                hidden: "${operationCode.webUnmultiplexing}" == "1",
                onclick: list_export,
                iconClass: 'glyphicon glyphicon-pencil'
            }
        ]
    });

    var list_queryForm_data = iTsai.form.serialize($("#list_queryForm"));

    list_grid = jQuery("#instorage_outsideProcess_warehouseEntry_list_grid-table").jqGrid({
        url: context_path + "/wms2/instorage/outsideProcessWarehouseEntry/list.do",
        datatype: "json",
        colNames: ["主键", "序号","入库单号", "送货单号", "供应商名称", "供货方(供货人)", "供应商批次",
            "采购员", "仓库名称", "入库货架号", "物料编码", "物料名称", "主单位", "入库数量", "入库日期", "经办人"],
        colModel: [
            {name: "id", index: "a.id", hidden: true},
            {name: "id", index: "a.id", width: 20 },
            {name: "n1", index: "a.n1", width: 40},
            {name: "n2", index: "a.n2", width: 40},
            {name: "n3", index: "a.n3", width: 40},
            {name: "n4", index: "a.n4", width: 40},
            {name: "n5", index: "a.n5", width: 40},
            {name: "n6", index: "a.n6", width: 40},
            {name: "n7", index: "a.n7", width: 40},
            {name: "n8", index: "a.n8", width: 40},
            {name: "n9", index: "a.n9", width: 40},
            {name: "n10", index: "a.n10", width: 40},
            {name: "n11", index: "a.n11", width: 20},
            {name: "n12", index: "a.n12", width: 20},
            {name: "n13", index: "a.n13", width: 40},
            {name: "n14", index: "a.n14", width: 40}
        ],
        rowNum: 20,
        rowList: [10, 20, 30],
        pager: "#list_grid-pager",
        sortname: "a.id",
        sortorder: "asc",
        altRows: true,
        viewrecords: true,
        autowidth: true,
        multiselect: true,
        multiboxonly: true,
        beforeRequest: function () {
            dynamicGetColumns(list_dynamicDefalutValue, "goods_list_grid-table", $(window).width() - $("#sidebar").width() - 7);
            //重新加载列属性
        },
        loadComplete: function (data) {
            var table = this;
            setTimeout(function () {
                updatePagerIcons(table);
                enableTooltips(table);
            }, 0);
            list_oriData = data;
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext: "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
    });

    jQuery("#instorage_outsideProcess_warehouseEntry_list_grid-table").navGrid("#list_grid-pager", {
        edit: false,
        add: false,
        del: false,
        search: false,
        refresh: false
    })
        .navButtonAdd("#list_grid-pager", {
            caption: "",
            buttonicon: "fa fa-refresh green",
            onClickButton: function () {
                $("#instorage_outsideProcess_warehouseEntry_list_grid-table").jqGrid("setGridParam", {
                    postData: {queryJsonString: ""} //发送数据
                }).trigger("reloadGrid");
            }
        }).navButtonAdd("#list_grid-pager", {
        caption: "",
        buttonicon: "fa icon-cogs",
        onClickButton: function () {
            jQuery("#instorage_outsideProcess_warehouseEntry_list_grid-table").jqGrid("columnChooser", {
                done: function (perm, cols) {
                    dynamicColumns(cols, list_dynamicDefalutValue);
                    $("#instorage_outsideProcess_warehouseEntry_list_grid-table").jqGrid("setGridWidth", $("#list_grid-div").width());
                    list_exportExcelIndex = perm;
                }
            });
        }
    });

    $(window).on("resize.jqGrid", function () {
        $("#instorage_outsideProcess_warehouseEntry_list_grid-table").jqGrid("setGridWidth", $(window).width() - $("#sidebar").width() - 7);
        $("#instorage_outsideProcess_warehouseEntry_list_grid-table").jqGrid("setGridHeight", $(".container-fluid").height() - 10 - $("#list_yy").outerHeight(true) -
            $("#list_fixed_tool_div").outerHeight(true) - $("#list_grid-pager").outerHeight(true) -
            $("#gview_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
    });
    $(window).triggerHandler("resize.jqGrid");


    function list_export(){

    }

    /**
     * 查询按钮点击事件
     */
    function list_queryOk() {
        var queryParam = iTsai.form.serialize($("#list_queryForm"));
        //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
        list_queryByParam(queryParam);
    }

    function list_queryByParam(jsonParam) {
        iTsai.form.deserialize($("#list_hiddenQueryForm"), jsonParam);
        var queryParam = iTsai.form.serialize($("#list_hiddenQueryForm"));
        var queryJsonString = JSON.stringify(queryParam);
        $("#instorage_outsideProcess_warehouseEntry_list_grid-table").jqGrid("setGridParam",
            {
                postData: {queryJsonString: queryJsonString}
            }
        ).trigger("reloadGrid");
    }
</script>
