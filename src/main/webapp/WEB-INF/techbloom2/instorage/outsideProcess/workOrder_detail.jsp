<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
%>
<div id="add_page" class="row-fluid" style="height: inherit;margin:0px">
    <form action="" class="form-horizontal" id="add_baseInfor" name="baseInfor" method="post" target="_ifr" style="border-bottom: solid 2px #3b73af;">
        <input type="hidden" id="add_id" name="id" value="${moveStorageId}">
        <div class="row" style="margin:0;padding:0;">
            <div class="control-groaup span6">
                <label class="control-label" >工单单据号：</label>
                <div class="controls">
                    <div class="span12" >
                        <input class="span10" type = "text" name="" value="" placeholder=""/>
                    </div>
                </div>
            </div>
            <div class="control-groaup span6">
                <label class="control-label" >工单日期：</label>
                <div class="controls">
                    <div class="span12" >
                        <input class="span10" type = "text" name="" value="" placeholder=""/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin:0;padding:0;">
            <div class="control-groaup span6">
                <label class="control-label" >供应商名称：</label>
                <div class="controls">
                    <div class="span12" >
                        <input class="span10" type = "text" name="" value="" placeholder=""/>
                    </div>
                </div>
            </div>
            <div class="control-groaup span6">
                <label class="control-label" >采购组织：</label>
                <div class="controls">
                    <div class="span12" >
                        <input class="span10" type = "text" name="" value="" placeholder=""/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin:0;padding:0;">
            <div class="control-groaup span6">
                <label class="control-label" >采购部门：</label>
                <div class="controls">
                    <div class="span12" >
                        <input class="span10" type = "text" name="" value="" placeholder=""/>
                    </div>
                </div>
            </div>
            <div class="control-groaup span6">
                <label class="control-label" >采购员：</label>
                <div class="controls">
                    <div class="span12" >
                        <input class="span10" type = "text" name="" value="" placeholder=""/>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div id="add_grid-div-c" style="width:100%;margin:0px auto;">
        <div id="add_fixed_tool_div" class="fixed_tool_div detailToolBar">
            <div id="add___toolbar__-c" style="float:left;overflow:hidden;"></div>
        </div>
        <table id="add_grid-table-c" style="width:100%;height:100%;"></table>
        <div id="add_grid-pager-c"></div>
    </div>
</div>
<iframe src="about:blank" name="_ifr" height="0" class="hidden"></iframe>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriData;      //表格数据
    var _grid;        //表格对象
    var selectData = 0;   //存放物料选择框中的值
    var selectParam = "";  //存放之前的查询条件
    var factorySelect;
    var moveCode = "";   //移库单号

    $("#add_baseInfor").validate({
        rules: {
            "warehouseId": {
                required: true,
            },
        },
        messages: {
            "warehouseId": {
                required: "请选择仓库!",
            },
        },
        errorClass: "help-inline",
        errorElement: "span",
        highlight:function(element, errorClass, validClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        }
    })

    $("#add_formSave").click(function(){
        if($('#add_baseInfor').valid()){
            //通过验证：获取表单数据，保存表单信息
            saveFormInfo($('#add_baseInfor').serialize());
        }
    });

    $('[data-rel=tooltip]').tooltip();



    //初始化表格
    _grid =  $("#add_grid-table-c").jqGrid({
        url : context_path + "/wms2/instorage/outsideProcessWorkOrder/list.do",
        datatype : "json",
        colNames : ["详情主键","行号","采购订单号","采购订单行号","物料编号","物料名称","单位", "工单数量"],
        colModel : [
            {name : "id",index : "id",width : 20,hidden:true},
            {name : "id",index:"id",width : 20},
            {name : "",index:"",width : 30},
            {name : "",index:"",width : 20},
            {name : "",index:"",width : 30},
            {name : "",index:"",width : 30},
            {name : "",index:"",width : 20},
            {name : "",index:"",width : 20}
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : "#add_grid-pager-c",
        sortname : "t1.id",
        sortorder : "asc",
        altRows: true,
        viewrecords : true,
        autowidth:true,
        footerrow: true,
        gridComplete: function() {
            // var rows = $("#orders").jqGrid("getRowData"), total_count = 0;
            // for(var i = 0, l = rows.length; i<l; i++) {
            //     total_count += (rows[i].goods_count - 0);
            // }
            $("#add_grid-table-c").jqGrid("footerData", "set", {unit:"-合计-",quantity:1});
        },
        multiselect:true,
        multiboxonly: true,
        loadComplete : function(data){
            var table = this;
            setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            oriData = data;
            $(window).triggerHandler("resize.jqGrid");
        },
        cellEdit: true,
        cellsubmit : "clientArray",
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据",
    });
    //在分页工具栏中添加按钮
    $("#add_grid-table-c").navGrid("#add_grid-pager-c",
        {edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#add_grid-pager-c",{
        caption:"",
        buttonicon:"ace-icon fa fa-refresh green",
        onClickButton: function(){
            $("#add_grid-table-c").jqGrid("setGridParam",
                {
                    url:context_path + "/move/detailList.do?id="+$("#add_id").val(),
                    postData: {id:$("#add_baseInfor #add_id").val(),queryJsonString:""} //发送数据  :选中的节点
                }
            ).trigger("reloadGrid");
        }
    });

    $(window).on("resize.jqGrid", function () {
        $("#add_grid-table-c").jqGrid("setGridWidth", $("#add_grid-div-c").width() - 3 );
        var height = $(".layui-layer-title",_grid.parents(".layui-layer")).height()+
            $("#add_baseInfor").outerHeight(true)+
            $("#add_materialDiv").outerHeight(true)+
            $("#add_grid-pager-c").outerHeight(true)+
            $("#add_fixed_tool_div.fixed_tool_div.detailToolBar").outerHeight(true)+
            $("#gview_add_grid-table-c .ui-jqgrid-hbox").outerHeight(true);
        $("#add_grid-table-c").jqGrid("setGridHeight",_grid.parents(".layui-layer").height()-height);
    });
    $(window).triggerHandler("resize.jqGrid");


    //添加质保单号
    function addDetail(){
    }
</script>
