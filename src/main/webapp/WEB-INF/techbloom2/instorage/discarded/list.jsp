<%--
  Created by IntelliJ IDEA.
  User: DASH
  Date: 2020/4/24
  Time: 22:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="list_grid-div">
    <form id="list_hiddenForm" action="<%=path%>/warehouselist/materialExcel" method="POST"
          style="display: none;">
        <input name="ids" id="list_ids" value=""/>
        <input name="queryFactoryCode" id="list_queryFactoryCode" value="">
        <input name="queryCode" id="list_queryCode" value="">
        <input name="queryName" id="list_queryName" value="">
        <input name="queryExportExcelIndex" id="list_queryExportExcelIndex" value=""/>
    </form>
    <form id="list_hiddenQueryForm" style="display:none;">
        <input name="factoryCode" value=""/>
        <input name="code" value=""/>
        <input name="name" value=""/>
    </form>
    <c:if test="${operationCode.webSearch==1}">
        <div class="query_box" id="list_yy" title="查询选项">
            <form id="list_queryForm" style="max-width:100%;">
                <ul class="form-elements">
                    <li class="field-group field-fluid3">
                        <label class="inline" for="list_factoryCode" style="margin-right:20px;width: 100%;">
                            <span class="form_label" style="width:80px;">部门/台机：</span>
                            <input id="list_factoryCode" name="factoryCode" type="text"
                                   style="width: calc(100% - 85px);" placeholder="">
                        </label>
                    </li>
                    <li class="field-group field-fluid3">
                        <label class="inline" for="list_code" style="margin-right:20px;width: 100%;">
                            <span class="form_label" style="width:80px;">入库日期：</span>
                            <input id="list_code" name="code" type="text" style="width: calc(100% - 85px);"
                                   placeholder="">
                        </label>
                    </li>
                    <li class="field-group field-fluid3">
                        <label class="inline" for="list_name" style="margin-right:20px;width: 100%;">
                            <span class="form_label" style="width:80px;">物料名称：</span>
                            <input id="list_name" name="name" type="text" style="width: calc(100% - 85px);"
                                   placeholder="">
                        </label>
                    </li>
                </ul>
                <div class="field-button" style="">
                    <div class="btn btn-info" onclick="list_queryOk();">
                        <i class="ace-icon fa fa-check bigger-110"></i>查询
                    </div>
                    <div class="btn" onclick="list_reset();"><i class="ace-icon icon-remove"></i>重置</div>
                </div>
            </form>
        </div>
    </c:if>
    <div id="list_fixed_tool_div" class="fixed_tool_div">
        <div id="list___toolbar__" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="instorage_discarded_list_grid-table" style="width:100%;height:100%;"></table>
    <div id="list_grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
    var list_oriData;
    var list_grid;
    var list_dynamicDefalutValue = "cbe154ed57f14f8e803c4d0982a4d18c";
    var list_exportExcelIndex;

    $("input").keypress(function (e) {
        if (e.which == 13) {
            list_queryOk();
        }
    });

    $(function () {
        $(".toggle_tools").click();
    });

    $("#list___toolbar__").iToolBar({
        id: "list___tb__01",
        items: [
            {
                label: "添加",
                hidden: "${operationCode.webTbsj}" == "1",
                onclick: list_openAddPage,
                iconClass: 'glyphicon glyphicon-plus'
            },
            {
                label: "编辑",
                hidden: "${operationCode.webUnmultiplexing}" == "1",
                onclick: list_openEditPage,
                iconClass: 'glyphicon glyphicon-pencil'
            },
            {
                label: "详情",
                hidden: "${operationCode.webUnmultiplexing}" == "1",
                onclick: list_openEditPage,
                iconClass: 'glyphicon glyphicon-pencil'
            }
        ]
    });

    var list_queryForm_data = iTsai.form.serialize($("#list_queryForm"));

    list_grid = jQuery("#instorage_discarded_list_grid-table").jqGrid({
        url: context_path + "/wms2/instorage/discarded/list.do",
        datatype: "json",
        colNames: ["主键", "废品入库单号", "入库日期", "申请部门/台机", "物料名称", "申请人", "联系电话", "入库人"],
        colModel: [
            {name: "id", index: "a.id", hidden: true},
            {name: "code", index: "a.code", width: 30},
            {name: "date", index: "a.date", width: 60},
            {name: "dept", index: "a.dept", width: 30},
            {name: "material", index: "a.material", width: 60},
            {name: "applicat", index: "a.applicat", width: 60},
            {name: "phone", index: "a.phone", width: 60},
            {name: "createdBy", index: "a.createdBy", width: 60}
        ],
        rowNum: 20,
        rowList: [10, 20, 30],
        pager: "#list_grid-pager",
        sortname: "a.id",
        sortorder: "asc",
        altRows: true,
        viewrecords: true,
        autowidth: true,
        multiselect: true,
        multiboxonly: true,
        beforeRequest: function () {
            dynamicGetColumns(list_dynamicDefalutValue, "goods_list_grid-table", $(window).width() - $("#sidebar").width() - 7);
            //重新加载列属性
        },
        loadComplete: function (data) {
            var table = this;
            setTimeout(function () {
                updatePagerIcons(table);
                enableTooltips(table);
            }, 0);
            list_oriData = data;
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext: "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
    });

    jQuery("#instorage_discarded_list_grid-table").navGrid("#list_grid-pager", {
        edit: false,
        add: false,
        del: false,
        search: false,
        refresh: false
    })
        .navButtonAdd("#list_grid-pager", {
            caption: "",
            buttonicon: "fa fa-refresh green",
            onClickButton: function () {
                $("#instorage_discarded_list_grid-table").jqGrid("setGridParam", {
                    postData: {queryJsonString: ""} //发送数据
                }).trigger("reloadGrid");
            }
        }).navButtonAdd("#list_grid-pager", {
        caption: "",
        buttonicon: "fa icon-cogs",
        onClickButton: function () {
            jQuery("#instorage_discarded_list_grid-table").jqGrid("columnChooser", {
                done: function (perm, cols) {
                    dynamicColumns(cols, list_dynamicDefalutValue);
                    $("#instorage_discarded_list_grid-table").jqGrid("setGridWidth", $("#list_grid-div").width());
                    list_exportExcelIndex = perm;
                }
            });
        }
    });

    $(window).on("resize.jqGrid", function () {
        $("#instorage_discarded_list_grid-table").jqGrid("setGridWidth", $(window).width() - $("#sidebar").width() - 7);
        $("#instorage_discarded_list_grid-table").jqGrid("setGridHeight", $(".container-fluid").height() - 10 - $("#list_yy").outerHeight(true) -
            $("#list_fixed_tool_div").outerHeight(true) - $("#list_grid-pager").outerHeight(true) -
            $("#gview_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
    });
    $(window).triggerHandler("resize.jqGrid");

    /**打开添加页面*/
    function list_openAddPage(){
        $.post(context_path + "/wms2/instorage/discarded/toAdd.do", {}, function (str){
            $queryWindow=layer.open({
                title : "废品入库单",
                type:1,
                skin : "layui-layer-molv",
                area : "1000px",
                shade : 0.6, //遮罩透明度
                moveType : 1, //拖拽风格，0是默认，1是传统拖动
                anim : 2,
                content : str,
                success: function (layero, index) {
                    layer.closeAll('loading');
                }
            });
        });
    }

    /**打开编辑页面*/
    function list_openEditPage(){
        var selectAmount = getGridCheckedNum("#instorage_discarded_list_grid-table");
        if(selectAmount==0){
            layer.msg("请选择一条记录！",{icon:2});
            return;
        }else if(selectAmount>1){
            layer.msg("只能选择一条记录！",{icon:8});
            return;
        }
        layer.load(2);
        $.post(context_path+'/wms2/instorage/discarded/toAdd.do', {
            id:jQuery("#instorage_discarded_list_grid-table").jqGrid("getGridParam", "selrow")
        }, function(str){
            $queryWindow = layer.open({
                title : "仓库编辑",
                type: 1,
                skin : "layui-layer-molv",
                area : "1000px",
                shade: 0.6, //遮罩透明度
                moveType: 1, //拖拽风格，0是默认，1是传统拖动
                content: str,//注意，如果str是object，那么需要字符拼接。
                success:function(layero, index){
                    layer.closeAll("loading");
                }
            });
        }).error(function() {
            layer.closeAll();
            layer.msg("加载失败！",{icon:2});
        });
    }

    /**
     * 查询按钮点击事件
     */
    function list_queryOk() {
        var queryParam = iTsai.form.serialize($("#list_queryForm"));
        //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
        list_queryByParam(queryParam);
    }

    function list_queryByParam(jsonParam) {
        iTsai.form.deserialize($("#list_hiddenQueryForm"), jsonParam);
        var queryParam = iTsai.form.serialize($("#list_hiddenQueryForm"));
        var queryJsonString = JSON.stringify(queryParam);
        $("#instorage_discarded_list_grid-table").jqGrid("setGridParam",
            {
                postData: {queryJsonString: queryJsonString}
            }
        ).trigger("reloadGrid");
    }
</script>
