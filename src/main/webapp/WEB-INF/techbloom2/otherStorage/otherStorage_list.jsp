<%--
  Created by IntelliJ IDEA.
  User: 86176
  Date: 2020/6/7
  Time: 16:36
  To change this template use File | Settings | File Templates.
--%>
<%--其他出入库--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<div id="yclOtherStorageListPage" style="margin: 15px">
    <%--query tools查询栏--%>
    <blockquote class="layui-elem-quote">
        <form class="layui-form">
            <div class="layui-fluid">
                <div class="layui-row layui-col-space10">
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 80px" class="layui-form-label">业务类型</label>
                            <div class="layui-input-block">
                                <select name="businessPurpose" v-model="queryParams.businessPurpose" lay-filter="businessPurpose">
                                    <option value="">全部</option>
                                    <option value="1">其他出库</option>
                                    <option value="2">其他入库</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">单据编号</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" name="inCode" v-model="queryParams.inCode"
                                       lay-filter="inCode"/>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">物料编码</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" name="materialCode"
                                       v-model="queryParams.materialCode" lay-filter="materialCode"/>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <button type="button" class="layui-btn layui-btn-sm layui-btn-normal"
                                    @click="queryPageView">
                                <i class="layui-icon">&#xe615;</i>查询
                            </button>
                        </div>
                        <div class="layui-inline">
                            <button type="button" class="layui-btn layui-btn-primary layui-btn-sm"
                                    @click="restQueryParams">
                                <i class="layui-icon">&#xe669;</i>重置
                            </button>
                        </div>
                    </div>
                </div>

                <div class="layui-row layui-col-space10">
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">日期</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" name="createTime"
                                       v-model="queryParams.createTime" lay-filter="createTime"/>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">业务目的</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" name="businessType"
                                       v-model="queryParams.businessType" lay-filter="businessType"/>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm3">
                        <div class="layui-inline">
                            <label style="width: 65px">领用用途</label>
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input" name="purpose" v-model="queryParams.purpose"
                                       lay-filter="purpose"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </blockquote>
<%--按钮+数据集合列表--%>
    <div id="toolbarOSOrder">
        <label class="layui-word-aux">订单列表</label>
        <button type="button" @click="addOtherStorage" class="layui-btn layui-btn-sm layui-btn-normal">
            <i class="layui-icon">&#xe654;</i>新增
        </button>
        <button type="button" @click="update" class="layui-btn layui-btn-sm layui-btn-normal">
            <i class="layui-icon">&#xe642;</i>库存处理
        </button>
        <button type="button" @click="save" class="layui-btn layui-btn-sm layui-btn-normal">
            <i class="layui-icon">&#xe605;</i>提交
        </button>
        <button type="button" @click="printOutOrder" class="layui-btn layui-btn-sm layui-btn-normal">
            <i class="layui-icon">&#xe66d;</i>打印出库单
        </button>
        <button type="button" @click="exportReport" class="layui-btn layui-btn-sm layui-btn-normal">
            <i class="layui-icon">&#xe655;</i>导出报表
        </button>
    </div>
    <table id="tableGridOtherInOutStore" lay-filter="tableGridFilter_otherStorage"></table>
</div>

<script>

    const context_path = '${APP_PATH}';
    const otherStorageIds = '${otherStorageIds}';
    const table = layui.table,
        layer = layui.layer,
        laydate = layui.laydate,
        form = layui.form;

    let vm = new Vue({
        el: '#yclOtherStorageListPage',
        data: {
            queryParams: {
                businessPurpose1: "",
                businessType: "",
                inCode: "",
                materialCode: "",
                createUser: "",
                createTime: "",
                purpose: ""
            }
        },
        /*按钮点击事件*/
        methods: {
            /*查询*/
            queryPageView() {
                console.log(vm.queryParams)
                table.reload('tableGridSetOtherStorage', {
                    where: vm.queryParams
                });
            },
            /*重置*/
            restQueryParams() {
                vm.queryParams = {
                    businessPurpose1: "",
                    businessType: "",
                    inCode: "",
                    materialCode: "",
                    createUser: "",
                    createTime: "",
                    purpose: ""
                }
                table.reload('tableGridSetOtherStorage', {
                    where: vm.queryParams
                });
            },
            /*新增*/
            addOtherStorage() {
                const checkStatusData = table.checkStatus('tableGridSetOtherStorage').data
                let otherStorageIds = checkStatusData.map(item => item.id).toString()
                layer.open({
                    title: '新增'
                    , type: 2
                    , area: ['1200px', '600px'] //宽高
                    , maxmin: true
                    ,
                    content: context_path + '/wms2/otherStorage/otherStorage/toEdit?otherStorageIds=' + otherStorageIds
                });
            },
            /*提交*/
            save(){
                this.upDownList = tableMoveOrder.cache['tableGridMoveOrderUp']
                var upDownQuantityError = false;
                var locatorError = false;
                var quantityError = false;
                this.upDownList.forEach(m=>{
                    if (m.updownQuantity==''||isNaN(m.updownQuantity))
                    {
                        upDownQuantityError=true;
                    }
                    if (m.locator=='')
                    {
                        locatorError=true;
                    }
                })
                if (locatorError)
                {
                    layer.alert('上架库位不能为空!');
                    return;
                }
                if (upDownQuantityError)
                {
                    layer.alert('移出数量为空或非数字!');
                    return;
                }
                $.ajax({
                    type: 'POST',
                    url: context_path + "/ycl/operation/yclInOutFlow/saveBatch",
                    contentType: "application/json",
                    async: false,
                    dataType: "json",
                    data: JSON.stringify(this.upDownList),
                    success: function (res) {
                        console.log(res)
                    }
                })
                },
            /*打印出库单*/
            printOutOrder() {
            },
            /*导出报表*/
            exportReport(){}
        },
        created() {

        },
        mounted() {
            //日期时间选择器
            laydate.render({
                elem: '#orderDate'
                , type: 'datetime'
                , done: function (value, date, endDate) {
                    vm.queryParams.orderDate = value
                }
            });

            // 列表展示的字段设置
            table.render({
                elem: '#tableGridOtherInOutStore'
                , url: context_path + '/wms2/otherStorage/otherStorage/list'
                , page: true
                , id: "tableGridSetOtherStorage"
                , size: 'sm'
                , cols: [[
                    {type: 'checkbox'}
                    , {field: 'businessType', title: '业务类型'}
                    , {field: 'businessPurpose', title: '业务目的'}
                    , {field: 'STATE', title: '订单状态'}
                    , {field: 'businessCode', title: '单据编号'}
                    , {field: 'businessCode', title: '行号'}
                    , {field: 'lotsNum', title: '批次号'}
                    , {field: 'materialCode', title: '物料编号'}
                    , {field: 'materialName', title: '物料名称'}
                    , {field: 'locationCode', title: '库位编码', edit: 'text'}
                    , {field: 'quantity', title: '收/退货数量', edit: 'text'}
                    , {field: 'actualQuality', title: '出/入库数量', edit: 'text'}
                    , {field: 'primaryUnit', title: '单位'}
                ]]
            });
        }
    })

    table.on('toolbar(tableGridFilter_otherStorage)', function (obj) {
        const checkStatusData = table.checkStatus(obj.config.id).data
        let otherStorageIds = checkStatusData.map(item => item.id).toString()

        //新增时无需勾选，订单信息也都是自己填写添加。当点击增加是不强制。
        switch (obj.event) {
            case 'addOtherStorage':
                layer.open({
                    title: '新增'
                    , type: 2
                    , area: ['1200px', '600px'] //宽高
                    , maxmin: true
                    ,
                    content: context_path + '/wms2/otherStorage/otherStorage/toOtherStorageEdit?otherStorageIds=' + otherStorageIds
                });
                break;
            case 'addOther':
                layer.open({
                    title: '库存处理'
                    , type: 2
                    , area: ['1000px', '500px'] //宽高
                    , maxmin: true
                    , content: context_path + '/wms2/otherStorage/otherStorage/toOtherStorageEdit'
                });
                break;
            case 'delete':
                layer.msg('删除');
                break;
            case 'update':
                layer.msg('编辑');
                break;
        }
        ;
    });

    // init
    ;!function () {

        //日期时间选择器
        laydate.render({
            elem: '#orderDate'
            , type: 'datetime'
            , done: function (value, date, endDate) {
                vm.queryParams.orderDate = value
            }
        });

    }();
</script>