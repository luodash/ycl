<%--其他出入库增加--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<head>
    <link rel="stylesheet" href="${APP_PATH}/static/layui/css/layui.css" type="text/css"/>
    <%--行编辑样式--%>
    <%--<link rel="stylesheet" href="layui/css/layui.css?v=201805080202" />--%>
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
        /*您可以将下列样式写入自己的样式表中*/
        .childBody {
            padding: 15px;
        }

        /*layui 元素样式改写*/
        .layui-btn-sm {
            line-height: normal;
            font-size: 12.5px;
        }

        .layui-table-view .layui-table-body {
            min-height: 256px;
        }

        .layui-table-cell .layui-input.layui-unselect {
            height: 30px;
            line-height: 30px;
        }

        /*设置 layui 表格中单元格内容溢出可见样式*/
        .table-overlay .layui-table-view,
        .table-overlay .layui-table-box,
        .table-overlay .layui-table-body {
            overflow: visible;
        }

        .table-overlay .layui-table-cell {
            height: auto;
            overflow: visible;
        }

        /*文本对齐方式*/
        .text-center {
            text-align: center;
        }
    </style>
</head>

<script src="${APP_PATH}/static/layui/layui.all.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

<div id="otherStorage_edit">
    <blockquote class="layui-elem-quote">
        <form class="layui-form">
            <div class="layui-row layui-col-space10">
                <div class="layui-col-sm3">
                    <div class="layui-inline">
                        <label style="width: 65px">业务类型</label>
                        <div class="layui-input-inline">
                            <select id="businessTypeSelect" lay-filter="businessTypeSelect">
                                <option value="">请选择...</option>
                                <option value="1">其他出库</option>
                                <option value="2">其他入库</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="layui-col-sm3">
                    <div class="layui-inline">
                        <label style="width: 65px">业务目的</label>
                        <div class="layui-input-inline">
                            <select id="businessPurposeSelect" lay-filter="businessPurposeSelect">
                                <option value="">请选择...</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="layui-col-sm3">
                    <div class="layui-inline">
                        <label style="width: 65px">事物处理日期</label>
                        <div class="layui-input-inline">
                            <input id="createTime" type="text" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-row layui-col-space10">
                <div class="layui-col-sm3">
                    <div class="layui-inline">
                        <label style="width: 65px">单据编号(自动)</label>
                        <div class="layui-input-inline">
                            <input type="text" autocomplete="off" class="layui-input"
                                   v-model="deliveryOrderEditParams.inCode">
                        </div>
                    </div>
                </div>
                <div class="layui-col-sm3">
                    <div class="layui-inline">
                        <label style="width: 65px">生产厂/部门</label>
                        <div class="layui-input-inline">
                            <select id="factoryAreaListSelect" lay-filter="factoryAreaListSelect">
                                <option value="">请选择厂区</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="layui-col-sm3">
                    <div class="layui-inline">
                        <label style="width: 65px">工序/二级部门</label>
                        <div class="layui-input-block">
                            <input type="tel" name="houseCode" autocomplete="off" class="layui-input"
                                   v-model="deliveryOrderEditParams.houseCode">
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-row layui-col-space10">
                <div class="layui-inline">
                    <label style="width: 65px">机台</label>
                    <div class="layui-input-block">
                        <input type="tel" name="locationCode" autocomplete="off" class="layui-input"
                               v-model="deliveryOrderEditParams.locationCode">
                    </div>
                </div>
                <div class="layui-inline">
                    <label style="width: 65px">维修/加工单号</label>
                    <div class="layui-input-block">
                        <input type="tel" name="poCode" autocomplete="off" class="layui-input"
                               v-model="deliveryOrderEditParams.poCode">
                    </div>
                </div>
                <div class="layui-inline">
                    <label style="width: 65px">申请人</label>
                    <div class="layui-input-block">
                        <input type="tel" name="createUser" autocomplete="off" class="layui-input"
                               v-model="deliveryOrderEditParams.createUser">
                    </div>
                </div>
            </div>
            <div class="layui-row layui-col-space10">
                <div class="layui-inline">
                    <label style="width: 65px">总金额</label>
                    <div class="layui-input-block">
                        <input type="tel" name="UNIT_PRICE" autocomplete="off" class="layui-input"
                               v-model="deliveryOrderEditParams.UNIT_PRICE">
                    </div>
                </div>
                <div class="layui-inline">
                    <label style="width: 65px">领用用途</label>
                    <div class="layui-input-block">
                        <input type="tel" name="PURPOSE" autocomplete="off" class="layui-input"
                               v-model="deliveryOrderEditParams.PURPOSE">
                    </div>
                </div>
                <div class="layui-inline">
                    <label style="width: 65px">备注</label>
                    <div class="layui-input-block">
                        <input type="tel" name="SUPPLIER_CODE" autocomplete="off" class="layui-input"
                               v-model="deliveryOrderEditParams.SUPPLIER_CODE">
                    </div>
                </div>
            </div>
            <div class="layui-row layui-col-space10">
                <div class="layui-inline">
                    <label style="width: 65px">装车通知单</label>
                    <div class="layui-input-block">
                        <%-- <input type="tel" name="SUPPLIER_NAME" autocomplete="off" class="layui-input" v-model="deliveryOrderEditParams.SUPPLIER_NAME">--%>
                        <select id="supplierNameSelect" lay-filter="supplierNameSelect">
                            <option value="">请选择...</option>
                        </select>
                    </div>
                </div>
                <%--查询按钮--%>
                <div class="layui-input-block">
                    <button type="submit" class="layui-btn" lay-submit lay-filter="submitBtn">提交</button>
                    <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                </div>
            </div>
        </form>
    </blockquote>
</div>

<%--测试增行--%>
<%--<div class="layui-card">
    <div class="layui-card-body layui-text">
        <div id="toolbar">
            <div>
                <button type="button" class="layui-btn layui-btn-sm" data-type="addRow" title="添加一行">
                    <i class="layui-icon layui-icon-add-1"></i> 添加一行
                </button>
            </div>
        </div>
        <div id="tableRes" class="table-overlay">
            <table id="tableGridDoEdit" lay-filter="tableGridFilterEdit" class="layui-hide"></table>
        </div>
        &lt;%&ndash;<div id="action" class="text-center">
            <button type="button" name="btnSave" class="layui-btn" data-type="save"><i class="layui-icon layui-icon-ok-circle"></i>保存</button>
            <button type="reset" name="btnReset" class="layui-btn layui-btn-primary">取消</button>
        </div>&ndash;%&gt;
    </div>
</div>--%>

<%--原table--%>
<table id="tableGridDoEdit" lay-filter="tableGridFilterEdit"></table>

<%--引入表格样式--%>
<%--<script src="layui/layui.js?v=201805080202" charset="utf-8"></script>--%>
<script type="text/javascript">
    //准备视图对象
    window.viewObj = {
        //默认第一行数据
        tbData: [{
            tempId: new Date().valueOf(),
            type: 0,
            name: '测试项名称',
            state: 1
        }],
        typeData: [
            {id: 1, name: '分类一'},
            {id: 2, name: '分类二'},
            {id: 3, name: '分类三'},
            {id: 4, name: '分类四'}
        ],
        renderSelectOptions: function (data, settings) {
            settings = settings || {};
            var valueField = settings.valueField || 'value',
                textField = settings.textField || 'text',
                selectedValue = settings.selectedValue || "";
            var html = [];
            for (var i = 0, item; i < data.length; i++) {
                item = data[i];
                html.push('<option value="');
                html.push(item[valueField]);
                html.push('"');
                if (selectedValue && item[valueField] == selectedValue) {
                    html.push(' selected="selected"');
                }
                html.push('>');
                html.push(item[textField]);
                html.push('</option>');
            }
            return html.join('');
        }
    };

    //layui 模块化引用
    layui.use(['jquery', 'table', 'layer'], function () {
        var $ = layui.$, table = layui.table, form = layui.form, layer = layui.layer;

        //数据表格实例化
        var tbWidth = $("#tableGridDoEdit").width();
        var layTableId = "layTable";
        var tableIns = table.render({
            elem: '#tableGridDoEdit',
            id: layTableId,
            data: viewObj.tbData,
            width: tbWidth,
            page: true,
            loading: true,
            even: false, //不开启隔行背景
            cols: [[
                {title: '序号', type: 'numbers'},
                {field: 'materialCode', title: '物料编码', edit: 'text'},
                {field: 'materialName', title: '物料名称', edit: 'text'},
                {
                    field: 'houseCode', title: '来源子库', templet: function (d) {
                        var options = viewObj.renderSelectOptions(viewObj.typeData, {
                            valueField: "id",
                            textField: "name",
                            selectedValue: d.type
                        });
                        return '<a lay-event="type"></a><select name="type" lay-filter="type"><option value="">请选择分类</option>' + options + '</select>';
                    }
                },
                {
                    field: 'locationCode', title: '来源库位', templet: function (d) {
                        var options = viewObj.renderSelectOptions(viewObj.typeData, {
                            valueField: "id",
                            textField: "name",
                            selectedValue: d.type
                        });
                        return '<a lay-event="type"></a><select name="type" lay-filter="type"><option value="">请选择分类</option>' + options + '</select>';
                    }
                },
                {
                    field: 'tragetWarehouse', title: '目标子库', templet: function (d) {
                        var options = viewObj.renderSelectOptions(viewObj.typeData, {
                            valueField: "id",
                            textField: "name",
                            selectedValue: d.type
                        });
                        return '<a lay-event="type"></a><select name="type" lay-filter="type"><option value="">请选择分类</option>' + options + '</select>';
                    }
                },
                {
                    field: 'tragetLocation', title: '目标库位', templet: function (d) {
                        var options = viewObj.renderSelectOptions(viewObj.typeData, {
                            valueField: "id",
                            textField: "name",
                            selectedValue: d.type
                        });
                        return '<a lay-event="type"></a><select name="type" lay-filter="type"><option value="">请选择分类</option>' + options + '</select>';
                    }
                },
                {field: 'batchCode', title: '批次号', edit: 'text'},
                {field: 'quantity', title: '数量', edit: 'text'},
                {field: 'unit', title: '单位', edit: 'text'},
                {field: 'unitPrice', title: '单价', edit: 'text'},
                {field: 'purpose', title: '备注', edit: 'text'},
                {
                    field: 'tempId', title: '操作', templet: function (d) {
                        return '<a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="del" lay-id="' + d.tempId + '"><i class="layui-icon layui-icon-delete"></i>移除</a>';
                    }
                }
            ]],
            done: function (res, curr, count) {
                viewObj.tbData = res.data;
            }
        });

        //定义事件集合
        var active = {
            addRow: function () {	//添加一行
                var oldData = table.cache[layTableId];
                console.log(oldData);
                var newRow = {tempId: new Date().valueOf(), type: null, name: '请填写名称', state: 0};
                oldData.push(newRow);
                tableIns.reload({
                    data: oldData
                });
            },
            updateRow: function (obj) {
                var oldData = table.cache[layTableId];
                console.log(oldData);
                for (var i = 0, row; i < oldData.length; i++) {
                    row = oldData[i];
                    if (row.tempId == obj.tempId) {
                        $.extend(oldData[i], obj);
                        return;
                    }
                }
                tableIns.reload({
                    data: oldData
                });
            },
            removeEmptyTableCache: function () {
                var oldData = table.cache[layTableId];
                for (var i = 0, row; i < oldData.length; i++) {
                    row = oldData[i];
                    if (!row || !row.tempId) {
                        oldData.splice(i, 1);    //删除一项
                    }
                    continue;
                }
                tableIns.reload({
                    data: oldData
                });
            },
            save: function () {
                var oldData = table.cache[layTableId];
                console.log(oldData);
                for (var i = 0, row; i < oldData.length; i++) {
                    row = oldData[i];
                    if (!row.type) {
                        layer.msg("检查每一行，请选择分类！", {icon: 5}); //提示
                        return;
                    }
                }
                document.getElementById("jsonResult").innerHTML = JSON.stringify(table.cache[layTableId], null, 2);	//使用JSON.stringify() 格式化输出JSON字符串
            }
        }

        //激活事件
        var activeByType = function (type, arg) {
            if (arguments.length === 2) {
                active[type] ? active[type].call(this, arg) : '';
            } else {
                active[type] ? active[type].call(this) : '';
            }
        }

        //增行按钮
        $('.layui-btn[data-type]').on('click', function () {
            var type = $(this).data('type');
            activeByType(type);
        });

        // 厂区选择
        form.on('select(factoryAreaListSelect)', function (data) {
            vm.deliveryOrderEditParams.shipToOrganizationName = data.value
        });

        table.on('edit(tableGridFilterEdit)', function (obj) {
            console.log(obj.value);
            console.log(obj.field);
            console.log(obj.data);
        });

        //监听select下拉选中事件
        form.on('select(type)', function (data) {
            var elem = data.elem; //得到select原始DOM对象
            $(elem).prev("a[lay-event='type']").trigger("click");
        });

        //监听工具条
        table.on('tool(tableGridDoEdit)', function (obj) {
            var data = obj.data, event = obj.event, tr = obj.tr; //获得当前行 tr 的DOM对象;
            console.log(data);
            switch (event) {
                case "type":
                    //console.log(data);
                    var select = tr.find("select[name='type']");
                    if (select) {
                        var selectedVal = select.val();
                        if (!selectedVal) {
                            layer.tips("请选择一个分类", select.next('.layui-form-select'), {tips: [3, '#FF5722']}); //吸附提示
                        }
                        console.log(selectedVal);
                        $.extend(obj.data, {'type': selectedVal});
                        activeByType('updateRow', obj.data);	//更新行记录对象
                    }
                    break;
                case "state":
                    var stateVal = tr.find("input[name='state']").prop('checked') ? 1 : 0;
                    $.extend(obj.data, {'state': stateVal})
                    activeByType('updateRow', obj.data);	//更新行记录对象
                    break;
                case "del":
                    layer.confirm('真的删除行么？', function (index) {
                        obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
                        layer.close(index);
                        activeByType('removeEmptyTableCache');
                    });
                    break;
            }
        });
    });
</script>

<%--<script>

    const context_path = '${APP_PATH}';

    const otherStorageIds = '${otherStorageIds}'; //勾选的订单物料

    const table = layui.table,
        form = layui.form,
        laydate = layui.laydate,
        $ = layui.jquery;

    let vm = new Vue({
        el: '#otherStorage_edit',
        data: {
            factoryAreaList: [],
            deliveryOrderEditParams: {
                businessType: '',
                businessPurpose: '',
                createTime: '',
                inCode: '',
                shipToOrganizationName: '',
                asnLines: []
            }
        },
        methods: {},
        created() {
        },
        mounted() {

            form.render();

            //日期时间选择器
             laydate.render({
                 elem: '#createTime'
                 , type: 'date'
                 , done: function (value, date, endDate) {
                     vm.deliveryOrderEditParams.createTime = value
                 }
             });

             //原列表集合数据
            table.render({
                elem: '#tableGridDoEdit'
                , url: context_path + '/wms2/otherStorage/otherStorage/list'
                , where: {poLineIds: otherStorageIds}
                , id: "tableGridDoEditSet"
                , size: 'sm'
                , cols: [[
                    , {field: 'poId', hide: true}
                    , {field: 'businessType', title: '业务类型'}
                    , {field: 'businessPurpose', title: '业务目的'}
                    , {field: 'inCode', title: '单据编号'}
                    , {field: 'poCode', title: '订单号', width: 95}
                    , {field: 'lineNumber', title: '行号', width: 55}
                    , {field: 'materialCode', title: '物料编号', width: 100}
                    , {field: 'materialName', title: '物料名称'}
                    , {field: 'batchCode', title: '批次号'}
                    , {field: 'unit', title: '单位', width: 55}
                    , {field: 'quantity', title: '订单数量', width: 85}
                    , {field: 'houseCode', title: '来源子库', width: 85}
                    , {field: 'locationCode', title: '来源库位', width: 85}
                    , {field: 'tragetWarehouse', title: '目标子库', width: 85 ,edit:'text'}
                    , {field: 'tragetLocation', title: '目标库位', width: 85,edit:'text'}
                    , {field: 'ORDER_QUALITY', title: '收/退货数量', width: 85}
                    , {field: 'ACTUAL_QUALITY', title: '出/入库数量', width: 85}
                    , {field: 'UNIT_PRICE', title: '单价', width: 85}
                    , {field: 'SUPPLIER_CODE', title: '供应商编码', width: 85}
                    , {field: 'SUPPLIER_NAME', title: '供应商名称', width: 85}
                    , {field: 'PURPOSE', title: '领用用途', width: 85}
                    , {field: 'wealth', title: '紧急状态', width: 85}
                    , {field: 'wealth', title: '接收时间'}
                    , {field: 'createUser', title: '创建人'}
                    , {field: 'createTime', title: '创建时间'}
                ]]
                , done: function (res) {
                    console.log('tableGridDoEditSet', res)
                    let poData = res.data[0]
                    console.log(poData, poData)
                    vm.deliveryOrderEditParams.vendorName = poData.supplierName
                    vm.deliveryOrderEditParams.companyName = poData.purchaseOrgan
                    //vm.deliveryOrderEditParams.createTime = poData.purchaseOrgan
                }
            });

        }

    })

    // 厂区选择
    form.on('select(factoryAreaListSelect)', function (data) {
        vm.deliveryOrderEditParams.shipToOrganizationName = data.value
    });

    table.on('edit(tableGridFilterEdit)', function (obj) {
        console.log(obj.value);
        console.log(obj.field);
        console.log(obj.data);
    });


    //监听提交
    form.on('submit(submitBtn)', function (data) {
        vm.deliveryOrderEditParams.asnLines = table.cache['tableGridDoEditSet'];

        console.log(vm.deliveryOrderEditParams)

        $.ajax({
            type: 'POST',
            url: context_path + "/wms2/otherStorage/otherStorage/save",
            contentType: "application/json",
            async: false,
            dataType: "json",
            data: JSON.stringify(vm.deliveryOrderEditParams),
            success: function (res) {
                console.log(res)
            }
        })
        var index=parent.layer.getFrameIndex(window.name);
        parent.layer.close(index);

        // layer.closeAll();
        /*setTimeout(function () {
            window.parent.location.reload();//让打开这个窗口的父窗口刷新，然后本子窗口关闭！
        });*/

    });


    //获取厂区信息
    $.get(context_path + '/factoryArea/list.do', {}, function (res) {
        console.log(res.rows)
        let html = '';
        let list = res.rows;
        for (let i = 0; i < list.length; i++) {
            html += '<option value=' + list[i].code + '>' + list[i].name + '</option>';
        }
        $("#factoryAreaListSelect").append(html);
        //刷新select选择框渲染
        form.render('select');		//放在异步里面才会起作用
    })

</script>--%>
