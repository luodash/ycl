<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
%>
<div id="car_edit_page" class="row-fluid" style="height: inherit;">
    <form id="car_edit_carForm" class="form-horizontal" style="overflow: auto; height: calc(100% - 70px);">
        <input type="hidden" id="car_edit_id" name="id" value="${car.id}">
        <div class="control-group">
            <label class="control-label" for="car_edit_factoryCode">委外加工出库单号：</label>
            <div class="controls required">
                <input class="span11 select2_input " type="text" name="factoryCode" id="car_edit_factoryCode"
                       value="${car.factoryCode}" placeholder="厂区"/>
                <input type="hidden" id="car_edit_factoryid" value="${car.factoryCode}">
                <input type="hidden" id="car_edit_factoryno" value="${car.factoryCodeName}">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="car_edit_warehouse">客户名称：</label>
            <div class="controls required">
                <input class="span11 select2_input" type="text" id="car_edit_warehouse" name="warehouse"
                       value="${car.warehouse}" placeholder="仓库"/>
                <input type="hidden" id="car_edit_wid" value="${car.warehouse}">
                <input type="hidden" id="car_edit_wname" value="${car.warehouseName}">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">订单日期：</label>
            <div class="controls">
                <div class="input-append span12 required">
                    <select multip id="car_edit_warehouseArea_select" name="warehouseArea"
                            style="width: calc(100% - 85px);" placeholder="仓库区域">
                        <option value="">--请选择--</option>
                        <c:forEach items="${warehouseAreaList}" var="wareArea">
                            <option value='${wareArea.WAREHOUSEAREACODE}' <%--<c:if test='${wareArea.WAREHOUSEAREANAME == car.warehouseArea}'>
									selected='selected' </c:if>--%>>${wareArea.WAREHOUSEAREANAME}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="car_edit_tag">申请组织：</label>
            <div class="controls">
                <div class="input-append span12 required">
                    <input class="span11" type="text" id="car_edit_tag" name="tag" value="${car.tag}"
                           placeholder="标签号"/>
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="car_edit_name">申请部门：</label>
            <div class="controls">
                <div class="input-append span12 required">
                    <input type="text" class="span11" id="car_edit_name" name="name" value="${car.name}"
                           placeholder="名称">
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="car_edit_name">申请人：</label>
            <div class="controls">
                <div class="input-append span12 required">
                    <input type="text" class="span11" id="car_edit_name" name="name" value="${car.name}"
                           placeholder="名称">
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="car_edit_name">仓库名称：</label>
            <div class="controls">
                <div class="input-append span12 required">
                    <input type="text" class="span11" id="car_edit_name" name="name" value="${car.name}"
                           placeholder="名称">
                </div>
            </div>
        </div>
        <table id="outstorage_info_grid-table-c" style="width:100%;height:100%;"></table>
    </form>
    <div class="field-button" style="text-align: center;border-top: 0px;margin: 15px auto;">
		<span class="btn btn-info" onclick="saveForm();">
		   <i class="ace-icon fa fa-check bigger-110"></i>保存
		</span>
        <span class="btn btn-danger" onclick="layer.closeAll();">
		   <i class="icon-remove"></i>&nbsp;取消
		</span>
    </div>
</div>

<script type="text/javascript" src="<%=path%>/static/js/techbloom/selectMultip.js"></script>
<script type="text/javascript">
    var factoryCodeId = $("#car_edit_carForm #car_edit_factoryCode").val();

    var _grid_detail;        //表格对象

    _grid_detail = jQuery("#outstorage_info_grid-table-c").jqGrid({
        url: context_path + "/outsrorageCon/detailList.do?id=" + $("#outstorage_info_infoId").val(),
        datatype: "json",
        colNames: ["详情主键", "物料编号", "物料名称", "物料类型", "批次号", "单价", "金额", "单位", "销售数量"],
        colModel: [
            {name: "id", index: "id", hidden: true},
            {
                name: 'state', index: 'state', width: 10,
                formatter: function (cellValu, option, rowObject) {
                    if (cellValu == '1') {
                        return "<span style=\"color:black;font-weight:bold;\">未出库</span>";
                    } else if (cellValu == '2') {
                        return "<span style=\"color:blue;font-weight:bold;\">拣货中</span>";
                    } else if (cellValu == '3') {
                        return "<span style=\"color:green;font-weight:bold;\">出库完成</span>";
                    } else if (cellValu == '4') {
                        return "<span style=\"color:orange;font-weight:bold;\">预出库</span>";
                    } else {
                        return "";
                    }
                }
            },
            {name: "qaCode", index: "qaCode", width: 40},
            {name: "qaCode", index: "qaCode", width: 40},
            {name: "batchNo", index: "batch_No", width: 30},
            {name: "dishcode", index: "dishcode", width: 40},
            {name: "meter", index: "meter", width: 10},
            {name: "unit", index: "unit", width: 10},
            {name: 'model', index: 'model', width: 40}
        ],
        rowNum: 20,
        rowList: [10, 20, 30],
        pager: "#outstorage_info_grid-pager-c",
        sortname: "id",
        sortorder: "asc",
        altRows: true,
        viewrecords: true,
        caption: "详情列表",
        autowidth: true,
        multiselect: true,
        multiboxonly: true,
        loadComplete: function (data) {
            var table = this;
            setTimeout(function () {
                updatePagerIcons(table);
                enableTooltips(table);
            }, 0);
            oriDataDetail = data;
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext: "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据",
    });

    //渲染多选框
    selectMultip.register();

    $("#car_edit_carForm").validate({
        rules: {
            "name": {
                required: true
            },
            "warehouse": {
                required: true
            }
        },
        messages: {
            "name": {
                required: "请输入名称！"
            },
            "warehouse": {
                required: "请选择仓库！"
            }
        },
        errorClass: "help-inline",
        errorElement: "div",
        highlight: function (element, errorClass, validClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        }
    });

    //确定按钮点击事件
    function saveForm() {
        if ($("#car_edit_carForm").valid()) {
            saveCarInfo($("#car_edit_carForm").serialize());
        }
    }

    //保存/修改用户信息
    function saveCarInfo(bean) {
        $.ajax({
            url: context_path + "/car/saveCar",
            type: "POST",
            data: bean,
            dataType: "JSON",
            success: function (data) {
                iTsai.form.deserialize($("#car_list_hiddenForm"), iTsai.form.serialize($("#car_list_hiddenQueryForm")));
                var queryParam = iTsai.form.serialize($("#car_list_hiddenQueryForm"));
                var queryJsonString = JSON.stringify(queryParam);
                if (Boolean(data.result)) {
                    layer.msg("保存成功！", {icon: 1});
                    //关闭当前窗口
                    layer.close($queryWindow);
                    //刷新列表
                    $("#car_list_grid-table").jqGrid('setGridParam', {
                        postData: {queryJsonString: queryJsonString}
                    }).trigger("reloadGrid");
                } else {
                    layer.alert("保存失败，请稍后重试！", {icon: 2});
                }
            },
            error: function (XMLHttpRequest) {
                alert(XMLHttpRequest.readyState);
                alert("出错啦！！！");
            }
        });
    }

    //厂区下拉框初始化
    $("#car_edit_carForm #car_edit_factoryCode").select2({
        placeholder: "选择部门",
        minimumInputLength: 0,   //至少输入n个字符，才去加载数据
        allowClear: true,  //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        ajax: {
            url: context_path + "/factoryArea/getFactoryList",
            type: "POST",
            dataType: 'json',
            delay: 250,
            data: function (term, pageNo) {     //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term,    //联动查询的字符
                    pageSize: 15,    //一次性加载的数据条数
                    pageNo: pageNo    //页码
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) {   //如果没有查询到数据，将会返回空串
                    var more = (pageNo * 15) < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res, more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }

    });


    $("#car_edit_carForm #car_edit_factoryCode").on("change", function (e) {
        factoryCodeId = $("#car_edit_carForm #car_edit_factoryCode").val();
    });

    //点击编辑的默认数据
    if ($("#car_edit_carForm #car_edit_factoryid").val() != "") {
        $("#car_edit_carForm #car_edit_factoryCode").select2("data", {
            id: $("#car_edit_carForm #car_edit_factoryid").val(),
            text: $("#car_edit_carForm #car_edit_factoryno").val()
        });
    }

    //仓库(根据厂区限定范围)
    $("#car_edit_carForm #car_edit_warehouse").select2({
        placeholder: "选择仓库",
        minimumInputLength: 0, //至少输入n个字符，才去加载数据
        allowClear: true, //是否允许用户清除文本信息
        delay: 250,
        formatNoMatches: "没有结果",
        formatSearching: "搜索中...",
        formatAjaxError: "加载出错啦！",
        //multiple: true,//多选
        ajax: {
            url: context_path + "/car/selectWarehouse",
            type: "POST",
            dataType: 'json',
            delay: 250,
            data: function (term, pageNo) { //在查询时向服务器端传输的数据
                term = $.trim(term);
                return {
                    queryString: term, //联动查询的字符
                    pageSize: 15, //一次性加载的数据条数
                    pageNo: pageNo, //页码
                    factoryCodeId: factoryCodeId
                }
            },
            results: function (data, pageNo) {
                var res = data.result;
                if (res.length > 0) { //如果没有查询到数据，将会返回空串
                    var more = pageNo < data.total; //用来判断是否还有更多数据可以加载
                    return {
                        results: res,
                        more: more
                    };
                } else {
                    return {
                        results: {}
                    };
                }
            },
            cache: true
        }
    });

    if ($("#car_edit_carForm #car_edit_wid").val() != "") {
        $("#car_edit_carForm #car_edit_warehouse").select2("data", {
            id: $("#car_edit_carForm #car_edit_wid").val(),
            text: $("#car_edit_carForm #car_edit_wname").val()
        });
    }

</script>