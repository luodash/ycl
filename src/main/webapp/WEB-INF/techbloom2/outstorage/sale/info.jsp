<%@ page language="java" import="java.lang.*" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
%>
<div class="row-fluid" style="height: inherit;margin:0px;border: 0px">
    <div style="margin-left: 30px">
        <ul class="form-elements">
            <li class="field-group field-fluid3">
                <label class="inline" for="warehouse_list_factoryCode" style="margin-right:20px;width: 100%;">
                    <span class="form_label" style="width:80px;">销售单号：</span>
                    <input id="warehouse_list_factoryCode" name="factoryCode" type="text"
                           style="width: calc(100% - 85px);">
                </label>
            </li>
            <li class="field-group field-fluid3">
                <label class="inline" for="warehouse_list_code" style="margin-right:20px;width: 100%;">
                    <span class="form_label" style="width:80px;">客户名称：</span>
                    <input id="warehouse_list_code" name="code" type="text" style="width: calc(100% - 85px);">
                </label>
            </li>
            <li class="field-group field-fluid3">
                <label class="inline" for="warehouse_list_name" style="margin-right:20px;width: 100%;">
                    <span class="form_label" style="width:80px;">订单日期：</span>
                    <input id="warehouse_list_name" name="name" type="text" style="width: calc(100% - 85px);">
                </label>
            </li>
            <li class="field-group-top field-group field-fluid3">
                <label class="inline" for="warehouse_list_name" style="margin-right:20px;width: 100%;">
                    <span class="form_label" style="width:80px;">销售组织：</span>
                    <input id="warehouse_list_name2" name="name" type="text" style="width: calc(100% - 85px);">
                </label>
            </li>
            <li class="field-group field-fluid3">
                <label class="inline" for="warehouse_list_code" style="margin-right:20px;width: 100%;">
                    <span class="form_label" style="width:80px;">销售部门：</span>
                    <input id="warehouse_list_code2" name="code" type="text" style="width: calc(100% - 85px);">
                </label>
            </li>
            <li class="field-group field-fluid3">
                <label class="inline" for="warehouse_list_name" style="margin-right:20px;width: 100%;">
                    <span class="form_label" style="width:80px;">销售员：</span>
                    <input id="warehouse_list_name2" name="name" type="text" style="width: calc(100% - 85px);">
                </label>
            </li>
            <li class="field-group-top field-group field-fluid3">
                <label class="inline" for="warehouse_list_name" style="margin-right:20px;width: 100%;">
                    <span class="form_label" style="width:80px;">仓库名称：</span>
                    <input id="warehouse_list_name3" name="name" type="text" style="width: calc(100% - 85px);">
                </label>
            </li>
        </ul>
    </div>
    <input type="hidden" id="outstorage_info_infoId" value="${infoId}">
    <!-- 表格div -->
    <div id="outstorage_info_grid-div-c" style="width:100%;margin:10px auto;">
        <!-- 	表格工具栏 -->
        <%--        <div id="outstorage_info_fixed_tool_div" class="fixed_tool_div detailToolBar">--%>
        <%--            <div id="outstorage_info___toolbar__-c" style="float:left;overflow:hidden;"></div>--%>
        <%--        </div>--%>

        <%--        <form id="hiddenForm" action="<%=path%>/outsrorageCon/detailExcel.do" method="POST" style="display: none;">--%>
        <%--            <input id="hiddenForm_ids" name="ids" value=""/>--%>
        <%--            <input id="info_id" name="infoid" value="">--%>
        <%--        </form>--%>
        <%--        <div style="margin-bottom:5px;margin-left: 25px;">--%>
        <%--            <span class="btn btn-info" id="export">--%>
        <%--		       <i class="ace-icon fa fa-check bigger-110"></i>导出--%>
        <%--            </span>--%>
        <%--        </div>--%>

        <!-- 物料详情信息表格 -->
        <table id="outstorage_info_grid-table-c" style="width:100%;height:100%;"></table>
        <!-- 表格分页栏 -->
        <div id="outstorage_info_grid-pager-c"></div>
    </div>
</div>
<script type="text/javascript">
    var context_path = '<%=path%>';
    var oriDataDetail;
    var _grid_detail;        //表格对象

    _grid_detail = jQuery("#outstorage_info_grid-table-c").jqGrid({
        url: context_path + "/outsrorageCon/detailList.do?id=" + $("#outstorage_info_infoId").val(),
        datatype: "json",
        colNames: ["详情主键", "物料编号", "物料名称", "物料类型", "批次号", "单价", "金额", "单位", "销售数量", "出库日期", "出库库位", "出库数量", "出库人"],
        colModel: [
            {name: "id", index: "id", hidden: true},
            {
                name: 'state', index: 'state', width: 10,
                formatter: function (cellValu, option, rowObject) {
                    if (cellValu == '1') {
                        return "<span style=\"color:black;font-weight:bold;\">未出库</span>";
                    } else if (cellValu == '2') {
                        return "<span style=\"color:blue;font-weight:bold;\">拣货中</span>";
                    } else if (cellValu == '3') {
                        return "<span style=\"color:green;font-weight:bold;\">出库完成</span>";
                    } else if (cellValu == '4') {
                        return "<span style=\"color:orange;font-weight:bold;\">预出库</span>";
                    } else {
                        return "";
                    }
                }
            },
            {name: "qaCode", index: "qaCode", width: 40},
            {name: "qaCode", index: "qaCode", width: 40},
            {name: "batchNo", index: "batch_No", width: 30},
            {name: "dishcode", index: "dishcode", width: 40},
            {name: "meter", index: "meter", width: 10},
            {name: "unit", index: "unit", width: 10},
            {name: 'model', index: 'model', width: 40},
            {name: 'colour', index: 'colour', width: 20},
            {name: 'weight', index: 'weight', width: 20},
            {name: 'segmentno', index: 'segmentno', width: 20},
            {name: 'segmentno', index: 'segmentno', width: 20}

        ],
        rowNum: 20,
        rowList: [10, 20, 30],
        pager: "#outstorage_info_grid-pager-c",
        sortname: "id",
        sortorder: "asc",
        altRows: true,
        viewrecords: true,
        caption: "详情列表",
        autowidth: true,
        multiselect: true,
        multiboxonly: true,
        loadComplete: function (data) {
            var table = this;
            setTimeout(function () {
                updatePagerIcons(table);
                enableTooltips(table);
            }, 0);
            oriDataDetail = data;
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext: "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据",
    });
    //在分页工具栏中添加按钮
    $("#outstorage_info_grid-table-c").navGrid("#outstorage_info_grid-pager-c",
        {
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: false
        }).navButtonAdd("#outstorage_info_grid-pager-c", {
        caption: "",
        buttonicon: "ace-icon fa fa-refresh green",
        onClickButton: function () {
            $("#outstorage_info_grid-table-c").jqGrid('setGridParam',
                {
                    url: context_path + "/outsrorageCon/detailList.do?id=" + $("#outstorage_info_infoId").val()
                }
            ).trigger("reloadGrid");
        }
    });

    $(window).on("resize.jqGrid", function () {
        $("#outstorage_info_grid-table-c").jqGrid("setGridWidth", $("#outstorage_info_grid-div-c").width() - 3);
        $("#outstorage_info_grid-table-c").jqGrid("setGridHeight", (document.documentElement.clientHeight - $("#outstorage_info_grid-pager-c").height() - 380));
    });
    $(window).triggerHandler("resize.jqGrid");

    $("#export").click(function () {
        $("#hiddenForm_ids").val(jQuery("#outstorage_info_grid-table-c").jqGrid("getGridParam", "selarrrow"));
        $("#info_id").val($("#outstorage_info_infoId").val());

        $("#hiddenForm").submit();
    });

</script>