<%--
  Created by IntelliJ IDEA.
  User: huzg
  Date: 2020/6/25
  Time: 17:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <style>
        th, td
        {
            padding: 6px;
        }
        tr
        {
            height: 30px;
        }
        .info td{
            border-right: 1px solid #000000;
            border-bottom: 1px solid #000000;
        }
        .head td{
            border-top: 1px solid #000000;
            border-bottom: 1px solid #000000;
        }
    </style>
</head>
<body>

<div style="padding: 10px;width: 98%">
<div id="yclProdPickingPrintDv">
<div id="div1">
    <div style="width: 100%;text-align: center;font-size: 20px;font-weight: bold;padding-top: 15px;">
        远东电缆有限公司<br/>
        配送计划单
    </div>
    <table style="width: 100%;font-size: 14px;">
        <tbody>
        <tr>
            <td>配送单号：{{psdh}}</td>
            <td>库存组织：{{orgName}}</td>
        </tr>
        <tr>
            <td>配送申请员：{{applyUser}}</td>
            <td>事务处理日期：{{ymd}}</td>
        </tr>
        </tbody>
    </table>
</div>
<div id="div2">
    <table class="info" style="border-collapse:collapse;border-left: 1px solid #000000;border-spacing:0px;font-size: 14px;">
    <tbody>
    <tr class="head">
        <td width="60">物料编码</td>
        <td width="80">物料描述</td>
        <td width="50">单位</td>
        <td width="50">配送数量</td>
        <td width="80">来源子库</td>
        <td width="80">生产厂/部门</td>
        <td width="50">工序</td>
        <td width="80">机台</td>
        <td width="80">工厂班次</td>
        <td width="80">配送地点</td>
        <td width="60">领料人</td>
        <td width="100">备注</td>
    </tr>
    <tr v-for="en in tableList">
        <td>{{en.itemCode}}</td>
        <td>{{en.itemDesc}}</td>
        <td>{{en.primaryUomCode}}</td>
        <td>{{en.quantity}}</td>
        <td>{{en.fromSubinvDesc}}</td>
        <td>{{en.facName}}</td>
        <td>{{en.opName}}</td>
        <td>{{en.deviceName}}</td>
        <td>{{en.shiftCode}}</td>
        <td>{{en.address}}</td>
        <td>{{en.userName}}</td>
        <td>{{en.comments}}</td>
    </tr>
    </tbody>
</table>
</div>
</div>

</div>
<script src="/wms/plugins/public_components/js/jquery-2.1.4.js"></script>
<script src="/wms/static/layui/vue.js"></script>
<script>
    Date.prototype.Format = function (fmt) { //author: meizz
        var o = {
            "M+": this.getMonth() + 1, //月份
            "d+": this.getDate(), //日
            "H+": this.getHours(), //小时
            "m+": this.getMinutes(), //分
            "s+": this.getSeconds(), //秒
            "q+": Math.floor((this.getMonth() + 3) / 3), //季度
            "S": this.getMilliseconds() //毫秒
        };
        if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (var k in o)
            if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        return fmt;
    };
    let vm = new Vue({
        el: '#yclProdPickingPrintDv',
        data: {
            psdh:'',
            orgName:'',
            applyUser: '',
            ymd:'',
            tableList:[]
        },
        methods:{
            createPerson: function(){
                console.log(this.tableList)
                this.tableList.push({});
            }
        },
        created() {
            this.ymd = new Date().Format("yyyy-MM-dd");
            var url = window.location.href;
            var dz_url = url.split('moCode=');

            if (dz_url.length > 1)
            {
                this.moCode = dz_url[1];
                $.ajax({
                    type: 'GET',
                    url: "/wms/ycl/production/picking/printListDv",
                    contentType: "application/json",
                    async: false,
                    data: {
                        moCode: this.moCode
                    },
                    success: res=> {

                        this.tableList = res.data;
                        this.psdh = res.psdh;
                        this.orgName = res.orgName;
                        this.applyUser = res.applyUser;
                    }
                })
            } else {
                dz_url = url.split('deliveryId=');
                this.deliveryId = dz_url[1];
                $.ajax({
                    type: 'GET',
                    url: "/wms/ycl/production/picking/printListDv",
                    contentType: "application/json",
                    async: false,
                    data: {
                        deliveryId: this.deliveryId
                    },
                    success: res=> {

                        this.tableList = res.data;
                        this.psdh = res.psdh;
                        this.orgName = res.orgName;
                        this.applyUser = res.applyUser;
                    }
                })
            }
        },
    });
</script>
</body>
</html>
