<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="agv_list_grid-div">
    <form id="agv_list_hiddenForm" action="<%=path%>/agvTask/materialExcel" method="POST"  style="display:none;">
        <input name="ids" id="agv_list_ids" value="" />
        <input name="queryWhNoFrom" id="agv_list_queryWhNoFrom" value="">
        <input name="queryWhNoTo" id="agv_list_queryWhNoTo" value="">
        <input name="queryExportExcelIndex" id="agv_list_queryExportExcelIndex" value=""/>
    </form>
    <form id="agv_list_hiddenQueryForm" style="display:none;">
        <input name="whNoFrom" value=""/>
        <input name="whNoTo" value=""/>
    </form>
    <c:if test="${operationCode.webSearch==1}">
        <div class="query_box" id="agv_list_yy" title="查询选项">
            <form id="agv_list_queryForm" style="max-width:100%;">
                <ul class="form-elements">
                    <li class="field-group field-fluid3">
                        <label class="inline" for="whNoFrom" style="margin-right:20px;width: 100%;">
                            <span class="form_label" style="width:80px;">来源仓库号：</span>
                            <input id="whNoFrom" name="whNoFrom" type="text" style="width: calc(100% - 85px);" placeholder="来源仓库号">
                        </label>
                    </li>
                    <li class="field-group field-fluid3">
                        <label class="inline" for="whNoTo" style="margin-right:20px;width: 100%;">
                            <span class="form_label" style="width:80px;">目的仓库号：</span>
                            <input id="whNoTo" name="whNoTo" type="text" style="width: calc(100% - 85px);" placeholder="目的仓库号">
                        </label>
                    </li>
                </ul>
                <div class="field-button" style="">
                    <div class="btn btn-info" onclick="agv_list_queryOk();">
                        <i class="ace-icon fa fa-check bigger-110"></i>查询
                    </div>
                    <div class="btn" onclick="agv_list_reset();"><i class="ace-icon icon-remove"></i>重置</div>
                </div>
            </form>
        </div>
    </c:if>
    <div id="agv_list_fixed_tool_div" class="fixed_tool_div">
        <div id="agv_list_toolbar_" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="agv_list_grid-table" style="width:100%;height:100%;"></table>
    <div id="agv_list_grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
    var agv_list_oriData;
    var agv_list_grid;
    var agv_list_exportExcelIndex;

    $("input").keypress(function (e) {
        if (e.which == 13) {
            agv_list_queryOk();
        }
    });

    $(function  (){
        $(".toggle_tools").click();
    });

    $("#agv_list_toolbar_").iToolBar({
        id: "agv_list_tb_01",
        items: [
            {label: "导出",hidden:"${operationCode.webExport}"=="1", onclick:function(){agv_list_toExcel();},iconClass:' icon-share'}
        ]
    });

    var agv_list_queryForm_data = iTsai.form.serialize($("#agv_list_queryForm"));

    agv_list_grid = jQuery("#agv_list_grid-table").jqGrid({
        url : context_path + "/agvTask/list.do",
        datatype : "json",
        colNames : [ "主键","任务类型", "来源仓库号", "目的仓库号", "系统名称", "设备名称", "生成的AGV任务号", "生成的任务对应的批次号", "任务序号", "来源暂存位", "目的暂存位", "优先级"],
        colModel : [
            {name : "id",index : "id",hidden:true},
            {name : "bussinessType",index : "bussinessType",width : 60},
            {name : "whNoFrom",index : "whNoFrom",width : 60},
            {name : "whNoTo",index : "whNoTo",width : 60},
            {name : "sysName",index : "sysName",width : 60},
            {name : "deviceName",index : "deviceName",width : 60},
            {name : "taskNo",index : "taskNo",width : 60},
            {name : "batchNo",index : "batchNo",width : 80},
            {name : "orderNo",index : "orderNo",width : 60},
            {name : "locationFrom",index : "locationFrom",width : 60},
            {name : "locationTo",index : "locationTo",width : 60},
            {name : "priority",index : "priority",width : 60}
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : "#agv_list_grid-pager",
        sortname : "id",
        sortorder : "asc",
        altRows: true,
        viewrecords : true,
        hidegrid:false,
        autowidth:true,
        multiselect:true,
        multiboxonly: true,
        loadComplete : function(data) {
            var table = this;
            setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            agv_list_oriData = data;
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
    });

    jQuery("#agv_list_grid-table").navGrid("#agv_list_grid-pager",
        {edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#agv_list_grid-pager",{
        caption:"",
        buttonicon:"fa fa-refresh green",
        onClickButton: function(){
            $("#agv_list_grid-table").jqGrid("setGridParam", {
                postData: {queryJsonString:""} //发送数据
            }).trigger("reloadGrid");
        }
    }).navButtonAdd("#agv_list_grid-pager",{
        caption: "",
        buttonicon:"fa icon-cogs",
        onClickButton : function (){
            jQuery("#agv_list_grid-table").jqGrid("columnChooser",{
                done: function(perm, cols){
                    $("#agv_list_grid-table").jqGrid("setGridWidth", $("#agv_list_grid-div").width());
                    agv_list_exportExcelIndex = perm;
                }
            });
        }
    });

    $(window).on("resize.jqGrid", function () {
        $("#agv_list_grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);
        $("#agv_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-10-
            $("#agv_list_yy").outerHeight(true)-$("#agv_list_fixed_tool_div").outerHeight(true)-
            $("#agv_list_grid-pager").outerHeight(true)-
            $("#gview_agv_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
    });
    $(window).triggerHandler("resize.jqGrid");


    //导出功能
    function agv_list_toExcel(){
        $("#agv_list_hiddenForm #agv_list_ids").val(jQuery("#agv_list_grid-table").jqGrid("getGridParam", "selarrrow"));
        $("#agv_list_hiddenForm #agv_list_queryWhNoFrom").val($("#agv_list_queryForm #whNoFrom").val());
        $("#agv_list_hiddenForm #agv_list_queryWhNoTo").val($("#agv_list_queryForm #whNoTo").val());
        $("#agv_list_hiddenForm #agv_list_queryExportExcelIndex").val(agv_list_exportExcelIndex);
        $("#agv_list_hiddenForm").submit();
    }


    /**
     * 查询按钮点击事件
     */
    function agv_list_queryOk(){
        var queryParam = iTsai.form.serialize($("#agv_list_queryForm"));
        //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
        agv_list_queryByParam(queryParam);
    }

    function agv_list_queryByParam(jsonParam) {
        iTsai.form.deserialize($("#agv_list_hiddenQueryForm"), jsonParam);
        var queryParam = iTsai.form.serialize($("#agv_list_hiddenQueryForm"));
        var queryJsonString = JSON.stringify(queryParam);
        console.log(queryJsonString)
        $("#agv_list_grid-table").jqGrid("setGridParam", {
            postData: {queryJsonString: queryJsonString}
        }).trigger("reloadGrid");
    }

    /**重置清空查询条件*/
    function agv_list_reset(){
        iTsai.form.deserialize($("#agv_list_queryForm"),agv_list_queryForm_data);
        agv_list_queryByParam(agv_list_queryForm_data);
    }


    /**同步数据*/
    function agv_list_synchronousdata(){
        $.ajax({
            type: "GET",
            url: context_path+"/agv/synchronousdata.do",
            dataType: "json",
            success: function(data){
                if(data.result){
                    layer.msg(data.msg,{icon:1,time:1200});
                    agv_list_grid.trigger("reloadGrid");
                }else{
                    layer.msg(data.msg,{icon:2,time:1200});
                }
            },
            error:function(XMLHttpRequest){
                alert(XMLHttpRequest.readyState);
                alert("出错啦！！！");
            }
        });
    }

</script>