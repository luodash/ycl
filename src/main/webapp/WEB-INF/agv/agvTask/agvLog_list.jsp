<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<script type="text/javascript">
    var context_path = '<%=path%>';
</script>
<div id="agvLog_list_grid-div">
    <form id="agvLog_list_hiddenForm" action="<%=path%>/agvTaskLog/materialExcel" method="POST"  style="display:none;">
        <input name="ids" id="agvLog_list_ids" value="" />
        <input name="queryTaskNo" id="agvLog_list_queryTaskNo" value="">
        <input name="queryExportExcelIndex" id="agvLog_list_queryExportExcelIndex" value=""/>
    </form>
    <form id="agvLog_list_hiddenQueryForm" style="display:none;">
        <input name="taskNo" value=""/>
    </form>
    <c:if test="${operationCode.webSearch==1}">
        <div class="query_box" id="agvLog_list_yy" title="查询选项">
            <form id="agvLog_list_queryForm" style="max-width:100%;">
                <ul class="form-elements">
                    <li class="field-group field-fluid3">
                        <label class="inline" for="taskNo" style="margin-right:20px;width: 100%;">
                            <span class="form_label" style="width:80px;">AGV任务号：</span>
                            <input id="taskNo" name="taskNo" type="text" style="width: calc(100% - 85px);" placeholder="来源仓库号">
                        </label>
                    </li>
                </ul>
                <div class="field-button" style="">
                    <div class="btn btn-info" onclick="agvLog_list_queryOk();">
                        <i class="ace-icon fa fa-check bigger-110"></i>查询
                    </div>
                    <div class="btn" onclick="agvLog_list_reset();"><i class="ace-icon icon-remove"></i>重置</div>
                </div>
            </form>
        </div>
    </c:if>
    <div id="agvLog_list_fixed_tool_div" class="fixed_tool_div">
        <div id="agvLog_list_toolbar_" style="float:left;overflow:hidden;"></div>
    </div>
    <table id="agvLog_list_grid-table" style="width:100%;height:100%;"></table>
    <div id="agvLog_list_grid-pager"></div>
</div>
<script type="text/javascript" src="<%=path%>/plugins/public_components/js/iTsai-webtools.form.js"></script>
<script type="text/javascript">
    var agvLog_list_oriData;
    var agvLog_list_grid;
    var agvLog_list_exportExcelIndex;

    $("input").keypress(function (e) {
        if (e.which == 13) {
            agvLog_list_queryOk();
        }
    });

    $(function  (){
        $(".toggle_tools").click();
    });

    $("#agvLog_list_toolbar_").iToolBar({
        id: "agvLog_list_tb_01",
        items: [
            {label: "导出",hidden:"${operationCode.webExport}"=="1", onclick:function(){agvLog_list_toExcel();},iconClass:' icon-share'}
        ]
    });

    var agvLog_list_queryForm_data = iTsai.form.serialize($("#agvLog_list_queryForm"));

    agvLog_list_grid = jQuery("#agvLog_list_grid-table").jqGrid({
        url : context_path + "/agvTaskLog/list.do",
        datatype : "json",
        colNames : [ "主键","AGV任务号", "状态", "调用时间"],
        colModel : [
            {name : "id",index : "id",hidden:true},
            {name : "taskNo",index : "taskNo",width : 60},
            {name : "agvState",index : "agvState",width : 60},
            {name : "agvTime",index : "agvTime",width : 60}
        ],
        rowNum : 20,
        rowList : [ 10, 20, 30 ],
        pager : "#agvLog_list_grid-pager",
        sortname : "id",
        sortorder : "asc",
        altRows: true,
        viewrecords : true,
        hidegrid:false,
        autowidth:true,
        multiselect:true,
        multiboxonly: true,
        loadComplete : function(data) {
            var table = this;
            setTimeout(function(){updatePagerIcons(table);enableTooltips(table);}, 0);
            agvLog_list_oriData = data;
        },
        emptyrecords: "没有相关记录",
        loadtext: "加载中...",
        pgtext : "页码 {0} / {1}页",
        recordtext: "显示 {0} - {1}共{2}条数据"
    });

    jQuery("#agvLog_list_grid-table").navGrid("#agvLog_list_grid-pager",
        {edit:false,add:false,del:false,search:false,refresh:false}).navButtonAdd("#agvLog_list_grid-pager",{
        caption:"",
        buttonicon:"fa fa-refresh green",
        onClickButton: function(){
            $("#agvLog_list_grid-table").jqGrid("setGridParam", {
                postData: {queryJsonString:""} //发送数据
            }).trigger("reloadGrid");
        }
    }).navButtonAdd("#agvLog_list_grid-pager",{
        caption: "",
        buttonicon:"fa icon-cogs",
        onClickButton : function (){
            jQuery("#agvLog_list_grid-table").jqGrid("columnChooser",{
                done: function(perm, cols){
                    $("#agvLog_list_grid-table").jqGrid("setGridWidth", $("#agvLog_list_grid-div").width());
                    agvLog_list_exportExcelIndex = perm;
                }
            });
        }
    });

    $(window).on("resize.jqGrid", function () {
        $("#agvLog_list_grid-table").jqGrid("setGridWidth", $(window).width()-$("#sidebar").width() -7);
        $("#agvLog_list_grid-table").jqGrid("setGridHeight",  $(".container-fluid").height()-10-
            $("#agvLog_list_yy").outerHeight(true)-$("#agvLog_list_fixed_tool_div").outerHeight(true)-
            $("#agvLog_list_grid-pager").outerHeight(true)-
            $("#gview_agvLog_list_grid-table .ui-jqgrid-hdiv").outerHeight(true));
    });
    $(window).triggerHandler("resize.jqGrid");


    //导出功能
    function agvLog_list_toExcel(){
        $("#agvLog_list_hiddenForm #agvLog_list_ids").val(jQuery("#agvLog_list_grid-table").jqGrid("getGridParam", "selarrrow"));
        $("#agvLog_list_hiddenForm #agvLog_list_queryTaskNo").val($("#agvLog_list_queryForm #taskNo").val());
        $("#agvLog_list_hiddenForm #agvLog_list_queryExportExcelIndex").val(agvLog_list_exportExcelIndex);
        $("#agvLog_list_hiddenForm").submit();
    }


    /**
     * 查询按钮点击事件
     */
    function agvLog_list_queryOk(){
        var queryParam = iTsai.form.serialize($("#agvLog_list_queryForm"));
        //执行父窗口中的js方法：将当前窗口中的form的值传递到父窗口，并放到父窗口中隐藏的form中，接着执行刷新父窗口列表的操作
        agvLog_list_queryByParam(queryParam);
    }

    function agvLog_list_queryByParam(jsonParam) {
        iTsai.form.deserialize($("#agvLog_list_hiddenQueryForm"), jsonParam);
        var queryParam = iTsai.form.serialize($("#agvLog_list_hiddenQueryForm"));
        var queryJsonString = JSON.stringify(queryParam);
        $("#agvLog_list_grid-table").jqGrid("setGridParam", {
            postData: {queryJsonString: queryJsonString}
        }).trigger("reloadGrid");
    }

    /**重置清空查询条件*/
    function agvLog_list_reset(){
        iTsai.form.deserialize($("#agvLog_list_queryForm"),agvLog_list_queryForm_data);
        agvLog_list_queryByParam(agvLog_list_queryForm_data);
    }


    /**同步数据*/
    function agvLog_list_synchronousdata(){
        $.ajax({
            type: "GET",
            url: context_path+"/agvLog/synchronousdata.do",
            dataType: "json",
            success: function(data){
                if(data.result){
                    layer.msg(data.msg,{icon:1,time:1200});
                    agvLog_list_grid.trigger("reloadGrid");
                }else{
                    layer.msg(data.msg,{icon:2,time:1200});
                }
            },
            error:function(XMLHttpRequest){
                alert(XMLHttpRequest.readyState);
                alert("出错啦！！！");
            }
        });
    }

</script>