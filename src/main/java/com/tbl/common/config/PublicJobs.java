package com.tbl.common.config;

import com.tbl.common.utils.DateUtils;
import com.tbl.modules.platform.controller.AbstractController;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 定时任务
 * @author 70486
 */
@Component
@Configuration
public class PublicJobs extends AbstractController {

    /**
     * 定时清除执行任务缓存号
     */
    @Scheduled(cron="0 1 0 * * ?")
    public void clearCacheJob(){
        Map<String,String> stringObjectMap = ConRunningMap.getRunningMap();
        logger.info(DateUtils.getTime()+" >>开始日常清除执行任务缓存号....");
        if (stringObjectMap!=null){
            for (Map.Entry<String,String> entry : stringObjectMap.entrySet()) {
                logger.info(entry.getKey());
                ConRunningMap.removeItem(entry.getKey());
            }
        }
    }

}
