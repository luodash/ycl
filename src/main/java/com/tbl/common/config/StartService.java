package com.tbl.common.config;

import com.tbl.common.utils.YamlConfigurerUtil;
import com.tbl.modules.platform.constant.MenuConstant;
import com.tbl.modules.platform.entity.system.Menu;
import com.tbl.modules.platform.service.system.MenuService;
import com.tbl.modules.platform.service.system.RoleService;
import com.tbl.modules.wms.constant.Constant;
import com.tbl.modules.wms.constant.EmumConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * 继承 ApplicationRunner 接口后项目启动时会按照执行顺序执行 run 方法
 * @author cxf
 * @date 2020/5/31
 */
@Component
@Order(value = 1)
public class StartService implements ApplicationRunner {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * 菜单
     */
    @Autowired
    private MenuService menuService;

    /**
     * 角色权限
     */
    @Autowired
    private RoleService roleService;

    @Override
    public void run(ApplicationArguments args) {
        logger.info("=========== 项目启动后，初始化  =============");
        Menu menu = menuService.selectById(MenuConstant.print);
        //根据出库模式隐藏或显示拣货下架菜单，并注释掉或释放菜单下的按钮权限
        if (Objects.equals(EmumConstant.deliveryMode.ACTUAL.getCode(), YamlConfigurerUtil.getIntegerYmlVal("yddl.outstorageModel"))){
            logger.info("=========== 根据出库模式：实时出库，隐藏拣货下架菜单以及按钮权限  =============");
            menu.setMenuType(Constant.MASK_MENU);

            roleService.updateByMenuId(MenuConstant.print);
        }else{
            logger.info("=========== 根据出库模式：预出库，显示拣货下架菜单以及按钮权限  =============");
            menu.setMenuType(Constant.WEB_SHWO_MENU);

            roleService.setBakByMenuId(MenuConstant.print);
        }
        menuService.updateById(menu);
    }

}
