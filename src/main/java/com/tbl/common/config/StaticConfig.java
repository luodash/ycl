package com.tbl.common.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

/**
 * 静态方法获取配置文件信息
 * @author 70486
 */
@Configuration
public class StaticConfig {

    private static String connectTimeout1;

    private static String readTimeout1;

    private static String static_ip_1;

    private static String static_port_1;

    private static String static_name_1;

    private static String getRfid1;

    private static String static_adapteRfid;

    private static String static_putBillUrl_1;

    private static String static_downBillUrl_1;

    private static String static_putBillOkLocation_1;

    private static String static_lightOn_1;

    private static String static_lightOnBj_1;

    private static String static_lcdNum_1;

    private static String static_lcdOff_1;

    private static String static_lightOff_1;

    private static String static_carOff_1;

    private static String static_lightOnWholeOnly_1;

    private static String static_lightNumber_1;

    private static String static_errorLightNumber_1;

    private static String static_adapteCz_1;

    private static String static_time_1;

    private static String static_resetCount_1;
    
    private static String static_database_url;
    
    private static String static_database_username;
    
    private static String static_database_password;
    
    private static String static_database_drivename;

    /******************************************/
    @Value("${read.connectTimeout}")
    private  String connectTimeout;


    @Value("${read.readTimeout}")
    private  String readTimeout;
/******************************************/
    @Value("${read.ip}")
    private String ip;

    @Value("${read.port}")
    private String port;

    @Value("${read.name}")
    private String name;

    @Value("${read.getRfid}")
    private String getRfid;
    /*************************************/
    @Value("${read.adapteRfid}")
    private String getAdapteRfid;

    @Value("${read.putBillUrl}")
    private String putBillUrl;

    @Value("${read.downBillUrl}")
    private String downBillUrl;

    @Value("${read.putBillOkLocation}")
    private String putBillOkLocation;

    @Value("${read.lightOn}")
    private String lightOn;

    @Value("${read.lightOnBj}")
    private String lightOnBj;

    @Value("${read.lcdNum}")
    private String lcdNum;

    @Value("${read.lcdOff}")
    private String lcdOff;

    @Value("${read.lightOff}")
    private String lightOff;

    @Value("${read.carOff}")
    private String carOff;

    @Value("${read.lightOnWholeOnly}")
    private String lightOnWholeOnly;

    @Value("${read.lightNumber}")
    private String lightNumber;

    @Value("${read.errorLightNumber}")
    private String errorLightNumber;

    @Value("${read.adapteCz}")
    private String adapteCz;

    @Value("${read.time}")
    private String time;

    @Value("${read.resetCount}")
    private String resetCount;
    
    @Value("${read.database_url}")
    private String databaseUrl;
    
    @Value("${read.database_username}")
    private String databaseUsername;
    
    @Value("${read.database_password}")
    private String databasePassword;
    
    @Value("${read.database_drivename}")
    private String databaseDrivename;

    @PostConstruct
    public void getApiToken() {

        connectTimeout1=this.connectTimeout;

        readTimeout1=this.readTimeout;

        static_ip_1=this.ip;

        static_port_1=this.port;

        static_name_1=this.name;

        getRfid1=this.getRfid;

        static_adapteRfid = this.getAdapteRfid;

        static_putBillUrl_1 = this.putBillUrl;

        static_downBillUrl_1 = this.downBillUrl;

        static_putBillOkLocation_1 = this.putBillOkLocation;

        static_lightOn_1 = this.lightOn;

        static_lightOnBj_1 = this.lightOnBj;

        static_lcdNum_1 = this.lcdNum;

        static_lcdOff_1 = this.lcdOff;

        static_lightOff_1 = this.lightOff;

        static_carOff_1 = this.carOff;

        static_lightOnWholeOnly_1 = this.lightOnWholeOnly;

        static_lightNumber_1 = this.lightNumber;

        static_errorLightNumber_1 = this.errorLightNumber;

        static_adapteCz_1 = this.adapteCz;

        static_time_1 = this.time;

        static_resetCount_1 = this.resetCount;
        
        static_database_url = this.databaseUrl;
        
        static_database_username = this.databaseUsername;
        
        static_database_password = this.databasePassword;
        
        static_database_drivename = this.databaseDrivename;

    }

    public static String getErrorLightNumber() {
        return static_errorLightNumber_1;
    }

    public static String getLightNumber() {
        return static_lightNumber_1;
    }

    public static String getAdapteCz() {
        return static_adapteCz_1;
    }

    public static String getConnectTimeout() {
        return connectTimeout1;
    }

    public static String getReadTimeout() {
        return readTimeout1;
    }

    public static String getIp() {
        return static_ip_1;
    }

    public static String getPort() {
        return static_port_1;
    }

    public static String getName() {
        return static_name_1;
    }

    public static String getRfidinfo(){
        return getRfid1;
    }

    public static String getGetAdapteRfid() {
        return static_adapteRfid;
    }
    public static String getPutBillUrl() {
        return static_putBillUrl_1;
    }

    public static String getDownBillUrl() {
        return static_downBillUrl_1;
    }

    public static String getPutBillOkLocation() {
        return static_putBillOkLocation_1;
    }

    public static String getLightOn() {
        return static_lightOn_1;
    }

    public static String getLightOnBj() {
        return static_lightOnBj_1;
    }

    public static String getLcdNum() {
        return static_lcdNum_1;
    }

    public static String getLcdOff() {
        return static_lcdOff_1;
    }

    public static String getLightOff() {
        return static_lightOff_1;
    }

    public static String getCarOff() {
        return static_carOff_1;
    }

    public static String getLightOnWholeOnly() {
        return static_lightOnWholeOnly_1;
    }

    public static String getTime() {
        return static_time_1;
    }

    public static String getStaticResetCount1() {
        return static_resetCount_1;
    }
    
    public static String getStaticDatabaseUrl() {
    	return static_database_url;
    }
    
    public static String getStaticDatabaseUsername() {
    	return static_database_username;
    }
    
    public static String getStaticDatabasePassword() {
    	return static_database_password;
    }
    
    public static String getStaticDatabaseDrivename() {
    	return static_database_drivename;
    }
}
