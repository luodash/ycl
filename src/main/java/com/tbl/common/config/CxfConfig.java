package com.tbl.common.config;

import com.tbl.modules.wms.service.webservice.CxfService;
import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.xml.ws.Endpoint;

/**
 * Created by nbfujx on 2017/11/22.
 * @author 70486
 */
@Configuration
public class CxfConfig {

    @Autowired
    private Bus bus;

    @Autowired
    private CxfService busService;

    @Bean
    public Endpoint endpoint() {
        EndpointImpl endpoint = new EndpointImpl(bus, busService);
        //接口发布在 /NetbarServices 目录下
        endpoint.publish("/cxfService");
        return endpoint;
    }


}
