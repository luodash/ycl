package com.tbl.common.config;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 内存常量map
 * @author 70486
 * @date 2020/3/22
 */
public class ConRunningMap {

    private static Map<String, String> runningMap = new ConcurrentHashMap<>();

    public static Map<String, String> getRunningMap() {
        return runningMap;
    }
    public static void setRunningMap(Map<String, String> runningMap) {
        ConRunningMap.runningMap = runningMap;
    }
    public static synchronized void addItem(String item) {
        if(!"".equals(item)){
            runningMap.put(item,"");
        }

    }
    public static synchronized void removeItem(String item) {
        if(!"".equals(item)){
            runningMap.remove(item);
        }

    }
    public static synchronized void addItemValue(String item,String value) {
        runningMap.put(item,value);
    }
    public static synchronized boolean containsItem(String item) {
        return !"".equals(item) && runningMap.containsKey(item);
    }

}
