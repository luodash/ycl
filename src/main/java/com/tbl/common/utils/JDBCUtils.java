package com.tbl.common.utils;

import java.sql.*;

/**
 * JDBC工具类
 * @author cxf
 */
public class JDBCUtils {
    /**
     * oracle驱动
     */
    public static final String DRIVER = "oracle.jdbc.driver.OracleDriver";
    /**
     * url
     */
    public static String URL = YamlConfigurerUtil.getStrYmlVal("spring.datasource.druid.first.url");
    /**
     * username
     */
    public static String USERNAME = YamlConfigurerUtil.getStrYmlVal("spring.datasource.druid.first.username");
    /**
     * password
     */
    public static String PASSWORD = YamlConfigurerUtil.getStrYmlVal("spring.datasource.druid.first.password");

    //通过静态代码块 注册数据库驱动
    static {
        try {
            Class.forName(DRIVER);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获得Connection
     * @return Connection
     */
    public static Connection getConnection() {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(URL, USERNAME, PASSWORD);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conn;
    }

    /**
     * 获得Statement
     * @return Statement
     */
    public static Statement getStatement() {
        Statement st = null;
        try {
            st = getConnection().createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return st;
    }

    /**
     * 关闭ResultSet
     * @param rs ResultSet
     */
    public static void closeResultSet(ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 关闭Statement
     * @param st Statement
     */
    public static void closeStatement(Statement st) {
        if (st != null) {
            try {
                st.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 关闭Connection
     * @param conn Connection
     */
    public static void closeConnection(Connection conn) {
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 关闭全部
     * @param rs ResultSet
     * @param sta Statement
     * @param conn Connection
     */
    public static void closeAll(ResultSet rs, Statement sta, Connection conn) {
        closeResultSet(rs);
        closeStatement(sta);
        closeConnection(conn);
    }

}


