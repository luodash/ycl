package com.tbl.common.utils;


import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * 页面page
 * @author 70486
 */
public class PageData extends HashMap<Object, Object> implements Map<Object, Object>{
	
	private static final long serialVersionUID = 1L;
	
	Map<String,Object> map;
	HttpServletRequest request;
	
	public PageData() 
	{
		map = new HashMap<>();
	}
	
	public PageData(HttpServletRequest request)
	{
		this.request = request;
		Map<String, String[]> properties = request.getParameterMap();
		Map<String,Object> returnMap = new HashMap<String,Object>(); 
		Iterator<Entry<String, String[]>> entries = properties.entrySet().iterator();
		Entry<String, String[]> entry;
		String name;
		while (entries.hasNext())
		{
			String value = "";
			entry = entries.next();
			name = entry.getKey();
			Object valueObj = entry.getValue(); 
			if(null == valueObj)
			{ 
				value = ""; 
			}
			else {
				String[] values = (String[])valueObj;
				for (String s : values) {
					value += (StringUtils.isEmptyString(value) ? "" : ",") + s;
				}
			}
			returnMap.put(name, value); 
		}
		map = returnMap;
	}
	
	@Override
	public Object get(Object key) 
	{
		Object obj;
		if(map.get(key) instanceof Object[]) 
		{
			Object[] arr = (Object[])map.get(key);
			obj = request == null ? arr:(request.getParameter((String)key) == null ? arr:arr[0]);
		} 
		else 
		{
			obj = map.get(key);
		}
		return obj;
	}
	
	public String getString(Object key) 
	{
		return (String)get(key);
	}
	
	public long getLong(Object key){
		return (long)get(key);
	}
	
	@Override
	public Object put(Object key, Object value) {
		return map.put((String)key, value);
	}
	
	@Override
	public Object remove(Object key) {
		return map.remove(key);
	}

	@Override
	public void clear() {
		map.clear();
	}

	@Override
	public boolean containsKey(Object key) {
		return map.containsKey(key);
	}

	@Override
	public boolean containsValue(Object value) {
		return map.containsValue(value);
	}

	@Override
	public Set entrySet() {
		return map.entrySet();
	}

	@Override
	public boolean isEmpty() {
		return map.isEmpty();
	}

	@Override
	public Set keySet() {
		return map.keySet();
	}

	@Override
	public void putAll(Map t) {
		map.putAll(t);
	}

	@Override
	public int size() {
		return map.size();
	}

	@Override
	public Collection values() {
		return map.values();
	}
	
}
