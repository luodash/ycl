package com.tbl.common.utils;


import org.apache.commons.beanutils.ConvertUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * 字符串工具类
 * @author 70486
 */
public class StringUtils extends org.apache.commons.lang3.StringUtils {
    private static final String EMPTY = "";

    public StringUtils() {
    }

    public static boolean isEmptyString(String str) {
        return str == null || "".equals(str.trim()) || str.length() <= 0 || "NULL".equals(str.toUpperCase());
    }

    public static String join(Object array, String separator, boolean asString) {
        if (array == null) {
            return "";
        }

        if (array.getClass().isArray()) {
            int arraylength = Array.getLength(array);
            return join(array, separator, asString, 0, arraylength - 1);
        }

        throw new IllegalArgumentException("StringUtils.join():The param [Object array] must be a array!");
    }


    public static String join(Object array, String separator, boolean asString, int startIndex, int endIndex) {
        if (array == null) {
            return "";
        }

        if (array.getClass().isArray()) {
            StringBuilder buffer = new StringBuilder();

            separator = (separator == null) ? "" : separator;

            for (int i = startIndex; i <= endIndex; ++i) {
                Object dataEle = Array.get(array, i);

                if (dataEle != null) {
                    dataEle = (asString) ? "" + dataEle + "" : dataEle.toString();
                    buffer.append(dataEle);
                }

                if (i < endIndex){
                    buffer.append(separator);
                }

            }

            return buffer.toString();
        }

        throw new IllegalArgumentException("StringUtils.join():The param [Object array] must be a array!");
    }

    /**
     * 判断字符串中是否含有英文，包含返回true
     * @param string 字符串
     * @return boolean
     */
    public static boolean isENChar(String string) {
        boolean flag = false;
        Pattern p = Pattern.compile("[a-zA-z]");
        if(p.matcher(string).find()) {
            flag = true;
        }
        return flag;
    }

    /**
     * 将带逗号得字符串转成List<Integer>
     * @param temp 字符串
     * @return List<Integer>
     */
    public static List<Integer> stringtoList(String temp) {
        if (null == temp || temp.trim().length() == 0) {
            return null;
        }
        return Arrays.stream(temp.split(",")).map(s -> Integer.parseInt(s.trim())).collect(Collectors.toList());
    }

    /**
     * 将带逗号得字符串转成Int数组
     * @param params 字符串
     * @return Integer[]
     */
    public static Integer[] strArrtoIntArr(String params) {
        if (params != null && !"".equals(params)) {
            return (Integer[]) ConvertUtils.convert(params.split(","), Integer.class);
        } else {
            return new Integer[]{};
        }
    }

    /**
     * 大写转小写
     * @param params 字符串
     * @return String
     */
    public static String toLowerCase(String params) {
        if (params != null) {
            return params.toLowerCase();
        } else {
            return "";
        }
    }

    /**
     * 将string转换成int数组
     * @param strs string字符串
     * @return int[]
     */
    public static int[] stringToInt(String strs) {
        if (null == strs || strs.trim().length() == 0) {
            return null;
        }
        String[] str = strs.split(",");
        int[] ints = new int[str.length];
        for (int i = 0; i < str.length; i++) {
            ints[i] = Integer.parseInt(str[i]);
        }
        return ints;
    }

    /**
     * 将string转换成string数组
     * @param strs string字符串
     * @return String[]
     */
    public static String[] stringToString(String strs) {
        if (null == strs || strs.trim().length() == 0) {
            return null;
        }
        String[] str = strs.split(",");
        String[] str1 = new String[str.length];
        System.arraycopy(str, 0, str1, 0, str.length);
        return str1;
    }

    /**
     * 将string转换成list数组
     * @param strs string字符串
     * @return List<Long>
     */
    public static List<Long> stringToList(String strs) {
        if (null == strs || strs.trim().length() == 0) {
            return null;
        }
        return Arrays.stream(strs.split(",")).map(Long::parseLong).collect(Collectors.toList());
    }

    public static List<String> stringToListString(String strs) {
        if (null == strs || strs.trim().length() == 0) {
            return null;
        }
        return Arrays.stream(strs.split(",")).map(String::toString).collect(Collectors.toList());
    }


    /**
     * Object转BigDecimal类型-王雷-2018年5月14日09:56:26
     * @param value 要转的object类型
     * @return BigDecimal 转成的BigDecimal类型数据
     */
    public static BigDecimal getBigDecimal(Object value) {
        BigDecimal ret = null;
        if (value != null) {
            if (value instanceof BigDecimal) {
                ret = (BigDecimal) value;
            } else if (value instanceof String) {
                ret = new BigDecimal((String) value);
            } else if (value instanceof BigInteger) {
                ret = new BigDecimal((BigInteger) value);
            } else if (value instanceof Number) {
                ret = BigDecimal.valueOf(((Number) value).doubleValue());
            } else {
                throw new ClassCastException("Not possible to coerce [" + value + "] from class " + value.getClass() + " into a BigDecimal.");
            }
        }
        return ret;
    }

    /**
     * string数组转化成 list<Long>
     * @param strs string数组
     * @return List<Long>
     */
    public static List<Long> toLongList(String[] strs) {
        if (strs.length < 1) {
            return null;
        }
        return Arrays.stream(strs).map(s -> Long.parseLong(s.trim())).collect(Collectors.toList());
    }


    public static String getNoByDecimalFormat(String nullReturnValue, String no, int length, String chars, String prex) {
        if (StringUtils.isEmptyString(no)) {
            return nullReturnValue;
        }
        no = no.substring(no.length() - length);
        int nos = Integer.parseInt(no) + 1;
        StringBuilder strs = new StringBuilder();
        for (int i = 0; i < length; i++) {
            strs.append(chars);
        }
        DecimalFormat df3 = new DecimalFormat(strs.toString());
        return prex + df3.format(nos);
    }

    /**
     * 对象转数组
     * @param obj 对象
     * @return byte[]
     */
    public static byte[] toByteArray(Object obj) {
        byte[] bytes = null;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            ObjectOutputStream oos = new ObjectOutputStream(bos);
            oos.writeObject(obj);
            oos.flush();
            bytes = bos.toByteArray();
            oos.close();
            bos.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return bytes;
    }

    public static String listToString(List<String> mList) {
        StringBuilder convertedListStr = new StringBuilder();
        if (null != mList && mList.size() > 0) {
            String[] mListArray = mList.toArray(new String[0]);
            for (int i = 0; i < mListArray.length; i++) {
                if (i < mListArray.length - 1) {
                    convertedListStr.append(mListArray[i]).append(",");
                } else {
                    convertedListStr.append(mListArray[i]);
                }
            }
            return convertedListStr.toString();
        } else {
            return "List is null!!!";
        }
    }

}
