package com.tbl.common.utils;

import com.google.common.collect.Maps;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import java.util.*;

/**
 * json格式转换为map格式工具
 * @author 70486
 */
public  class JsontoMap {

    /**
     * JSON转为HashMap
     * @param json JSONObject
     * @return HashMap
     */
    public static Map<String, Object> parseJSON2Map(JSONObject json) {
        Map<String, Object> map = new HashMap<>(1);
        if(json!=null && json.keySet()!=null) {
            // 最外层解析
            for (Object k : json.keySet()) {
                Object v = json.get(k);
                // 如果内层还是json数组的话，继续解析
                if (v instanceof JSONArray) {
                    List<Map<String, Object>> list = new ArrayList<>();
                    Iterator<JSONObject> it = ((JSONArray) v).iterator();
                    while (it.hasNext()) {
                        JSONObject json2 = it.next();
                        list.add(parseJSON2Map(json2));
                    }
                    map.put(k.toString(), list);
                } else if (v instanceof JSONObject) {
                    // 如果内层是json对象的话，继续解析
                    map.put(k.toString(), parseJSON2Map((JSONObject) v));
                } else {
                    // 如果内层是普通对象的话，直接放入map中
                    map.put(k.toString(), v);
                }
            }
        }
        return map;
    }

    /**
     * JSON转为IdentityHashMap
     * @param json
     * @return IdentityHashMap
     */
    public static IdentityHashMap parseJSON2IdentityHashMap(JSONObject json) {
        IdentityHashMap identityHashMap = Maps.newIdentityHashMap();
        if(json!=null && json.keySet()!=null) {
            // 最外层解析
            for (Object k : json.keySet()) {
                Object v = json.get(k);
                // 如果内层还是json数组的话，继续解析
                if (v instanceof JSONArray) {
                    List<Map<String, Object>> list = new ArrayList<>();
                    Iterator<JSONObject> it = ((JSONArray) v).iterator();
                    while (it.hasNext()) {
                        JSONObject json2 = it.next();
                        list.add(parseJSON2IdentityHashMap(json2));
                    }
                    identityHashMap.put(new String(k.toString()), list);
                } else if (v instanceof JSONObject) {
                    // 如果内层是json对象的话，继续解析
                    identityHashMap.put(new String(k.toString()), parseJSON2IdentityHashMap((JSONObject) v));
                } else {
                    // 如果内层是普通对象的话，直接放入map中
                    identityHashMap.put(new String(k.toString()), v);
                }
            }
        }
        return identityHashMap;
    }
}
