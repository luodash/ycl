package com.tbl.common.utils;


import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.Code128Writer;
import sun.misc.BASE64Encoder;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 生成条形码工具类
 * @Author: zhouxf
 * @Date: 2020/7/8 9:34
 */
public class BarCodeUtil {
    /** 条形码宽度 */
    private static final int WIDTH = 700;

    /** 条形码高度 */
    private static final int HEIGHT = 100;

    /** 加文字 条形码 */
    private static final int WORDHEIGHT = 200;
    /**
     * 设置 条形码参数
     */
    private static Map<EncodeHintType, Object> hints = new HashMap<EncodeHintType, Object>() {
        private static final long serialVersionUID = 1L;
        {
            // 设置编码方式
            put(EncodeHintType.CHARACTER_SET, "utf-8");
        }
    };
    /**
     * 生成 图片缓冲
     * @author fxbin
     * @param vaNumber  VA 码
     * @return 返回BufferedImage
     */
    public static BufferedImage getBarCode(String vaNumber){
        try {
            Code128Writer writer = new Code128Writer();
            // 编码内容, 编码类型, 宽度, 高度, 设置参数
            BitMatrix bitMatrix = writer.encode(vaNumber, BarcodeFormat.CODE_128, WIDTH, HEIGHT, hints);
            return MatrixToImageWriter.toBufferedImage(bitMatrix);
        } catch (WriterException e) {
            e.printStackTrace();
        }
        return null;
    }
    /**
     * 把带logo的二维码下面加上文字
     * @author fxbin
     * @param image  条形码图片
     * @param words  文字
     * @return 返回BufferedImage
     */
    public static BufferedImage insertWords(BufferedImage image, String words){
        // 新的图片，把带logo的二维码下面加上文字
        if (StringUtils.isNotEmpty(words)) {

            BufferedImage outImage = new BufferedImage(WIDTH, WORDHEIGHT, BufferedImage.TYPE_INT_RGB);

            Graphics2D g2d = outImage.createGraphics();

            // 抗锯齿
            setGraphics2D(g2d);
            // 设置白色
            setColorWhite(g2d);

            // 画条形码到新的面板
            g2d.drawImage(image, 0, 0, image.getWidth(), image.getHeight(), null);
            // 画文字到新的面板
            Color color=new Color(0, 0, 0);
            g2d.setColor(color);
            // 字体、字型、字号
            g2d.setFont(new Font("微软雅黑", Font.PLAIN, 27));
            //文字长度
            int firstStrWidth = g2d.getFontMetrics().stringWidth(words);
            //int secondStrWidth = g2d.getFontMetrics().stringWidth(words2);
            //int thirdStrWidth = g2d.getFontMetrics().stringWidth(words3);
            //总长度减去文字长度的一半  （居中显示）
            int firstWordStartX=(WIDTH - firstStrWidth) / 2;
            //int secondWordStartX=(WIDTH - secondStrWidth) / 2;
            //int thirdWordStartX=(WIDTH - thirdStrWidth) / 2;
            //height + (outImage.getHeight() - height) / 2 + 12
            int wordStartY=HEIGHT+30;

            // 画文字
            g2d.drawString(words, firstWordStartX, wordStartY);
            //g2d.drawString(words2, secondWordStartX, wordStartY+25);
            //g2d.drawString(words3, thirdWordStartX, wordStartY+50);
            g2d.dispose();
            outImage.flush();
            return outImage;
        }
        return null;
    }

    /**
     * 设置 Graphics2D 属性  （抗锯齿）
     * @param g2d  Graphics2D提供对几何形状、坐标转换、颜色管理和文本布局更为复杂的控制
     */
    private static void setGraphics2D(Graphics2D g2d){
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_DEFAULT);
        Stroke s = new BasicStroke(1, BasicStroke.CAP_ROUND, BasicStroke.JOIN_MITER);
        g2d.setStroke(s);
    }

    /**
     * 设置背景为白色
     * @param g2d Graphics2D提供对几何形状、坐标转换、颜色管理和文本布局更为复杂的控制
     */
    private static void setColorWhite(Graphics2D g2d){
        g2d.setColor(Color.WHITE);
        //填充整个屏幕
        g2d.fillRect(0,0,800,800);
        //设置笔刷
        g2d.setColor(Color.BLACK);
    }

    /**
     * 生成条形码 并base64转为字符
     * @param barCode firstWord secondWord thirdWord
     * @return {@link String}
     * @throws
     * @author jk
     * @date 2019/9/26 13:16
     **/
    public static String getBarCodeImage(String barCode,String firstWord){
        //生成条形码
        BufferedImage image = insertWords(getBarCode(barCode), firstWord);
        String BarCodeString="";
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            //转换成png格式的IO流
            ImageIO.write(image, "png", out);
            byte[] bytes = out.toByteArray();
            // 2、将字节数组转为二进制
            BASE64Encoder encoder = new BASE64Encoder();
            BarCodeString = encoder.encodeBuffer(bytes).trim();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return BarCodeString;
    }

    public static void main(String[] args) throws IOException {
//        BufferedImage image = insertWords(getBarCode("123456789"), "123456789");
//        A80/90R8A(8A侧通孔)
        BufferedImage image = insertWords(getBarCode("allocode=DG124243/lineNum=12"), "文字描述");
        ImageIO.write(image, "jpg", new File("F:/barcode.png"));
    }


}

