package com.tbl.common.utils;

import java.io.*;
import java.net.URL;
import java.util.Properties;

/**
 * 配置工具
 * @author 70486
 */
public class ConfigUtil {

    private static final URL FILE_PATH =Thread.currentThread().getContextClassLoader().getResource("");
    private static final String FILE_NAME = "resources.properties";
    public ConfigUtil(){}
    private static final Properties PROPS = new Properties();
    static{
        try {
            PROPS.load(new FileInputStream((FILE_PATH != null ? FILE_PATH.getPath() : null) + File.separator+ FILE_NAME));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static String getValue(String key){
        return PROPS.getProperty(key);
    }

    public static void updateProperties(String key,String value) {
        PROPS.setProperty(key, value);
    }

    public static void writeProperties(String key,String value) {
        try {
            OutputStream fos = new FileOutputStream((FILE_PATH != null ? FILE_PATH.getPath() : null) +File.separator+ FILE_NAME);
            PROPS.setProperty(key, value);
            //以适合使用 load 方法加载到 Properties 表中的格式，
            //将此 Properties 表中的属性列表（键和元素对）写入输出流
            PROPS.store(fos, "Update '" + key + "' value");
            fos.close();// 关闭流
        } catch (IOException e) {
            System.err.println("Visit "+ FILE_PATH +" for updating "+key+" value error");
        }
    }

    public static void main(String[] args) {
        System.out.println(getValue("1"));
    }
}
