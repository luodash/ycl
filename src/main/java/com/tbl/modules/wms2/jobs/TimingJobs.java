//package com.tbl.modules.wms2.jobs;
//
//import com.tbl.modules.platform.controller.AbstractController;
//import com.tbl.modules.wms2.entity.ordermanage.deliverynote.DeliveryNoteEntity;
//import com.tbl.modules.wms2.entity.remotedata.CronJobEntity;
//import com.tbl.modules.wms2.service.remotedata.CronJobService;
//import com.tbl.modules.wms2.service.interfacerecord.InterfaceRunService;
//import com.tbl.modules.wms2.service.ordermanage.deliverynote.DeliveryNoteService;
//import com.tbl.modules.wms2.service.remotedata.RemoteDataService;
//import com.tbl.modules.wms2.service.webservice.client.Wms2Service;
//import org.apache.commons.lang3.StringUtils;
//import org.apache.commons.lang3.concurrent.BasicThreadFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Component;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Map;
//import java.util.concurrent.ScheduledExecutorService;
//import java.util.concurrent.ScheduledThreadPoolExecutor;
//import java.util.stream.Collectors;
//
//
///**
// * 定时任务
// * @author 70486
// */
//@Component
//@Configuration
//public class TimingJobs extends AbstractController {
//
//    /**
//     * 3分钟
//     */
//    public final static long THREE_Minutes =  60 * 1000 * 3;
//
//    /**
//     * 定时任务行-送货单
//     */
//    public final static Integer CRON_JOB_01 = 1;
//    /**
//     * 接口执行
//     */
//    @Autowired
//    private InterfaceRunService interfaceRunService;
//    /**
//     * wms接口调用入口
//     */
//    @Autowired
//    private Wms2Service wms2Service;
//    @Autowired
//    private CronJobService cronJobService;
//    @Autowired
//    private DeliveryNoteService deliveryNoteServicel;
//    @Autowired
//    private RemoteDataService remoteDataService;
//
//    /**
//     * 离线接口多线程执行，2分钟一次
//     */
//    @Scheduled(fixedDelay = THREE_Minutes)
//    public void delayJob(){
//
//        ScheduledExecutorService executorService = new ScheduledThreadPoolExecutor(10,new BasicThreadFactory.Builder().namingPattern("example-schedule-pool-%d").daemon(true).build());
//        executorService.execute(()->{
//            CronJobEntity cronJob = new CronJobEntity();
//            cronJob = cronJobService.findById(CRON_JOB_01);
//            Long minId = cronJob.getMaxId();
//            Integer count = 0;
//            count = remoteDataService.getCountFromAsnHeader(minId);
//            if(count>0){
//                List<Map<String,Object>> lists = remoteDataService.getAsnHeaders(minId);
//                int listSize = lists.size();
//
//                List<DeliveryNoteEntity> deliveryNoteEntityList = new ArrayList<DeliveryNoteEntity>(listSize);
//                for(int i=0;i<listSize;i++){
//                    DeliveryNoteEntity deliveryNoteEntity = new DeliveryNoteEntity();
//                    //asn_header_id id,asn_number asnNumber,vendor_name vendorName,vendor_site_name shipAddress,
//                    // express_number expressNumber, concat(dispatching_staff,contact_information) driver,comments,
//                    // ship_date_fmt shipDate,company_name receiveOrgan,ship_to_organization_name receiveDept,status
//
//
//                }
//            }
//
//
//
//        });
//        executorService.shutdown();
//        try {
//            // 设置休眠，等待响应完成
//            Thread.sleep(2000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//
//    }
//
//
//}
