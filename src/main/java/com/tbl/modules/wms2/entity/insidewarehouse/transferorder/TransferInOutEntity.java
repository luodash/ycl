package com.tbl.modules.wms2.entity.insidewarehouse.transferorder;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 移位调拨
 * @author 70486
 */
@TableName("YCL_TRANSFER_ORDER")
@Getter
@Setter
@ToString
public class TransferInOutEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId(value = "ID",type = IdType.ID_WORKER_STR)
    private String id;

    /**
     * 移位调拨单编号
     */
    @TableField(value = "TRAN_CODE")
    private String tranCode;

    /**
     * 来源厂区编码
     */
    @TableField(value = "FROM_AREA_CODE")
    private String fromAreaCode;

    @TableField(value = "FROM_AREA")
    private String fromArea;

    /**
     * 来源仓库编码
     */
    @TableField(value = "FROM_STORE_CODE")
    private String fromStoreCode;

    @TableField(value = "FROM_STORE")
    private String fromStore;

    /**
     * 目标厂区编码
     */
    @TableField(value = "TO_AREA_CODE")
    private String toAreaCode;

    @TableField(value = "TO_AREA")
    private String toArea;

    /**
     * 目标仓库ID
     */
    @TableField(value = "TO_STORE_CODE")
    private String toStoreCode;

    @TableField(value = "TO_STORE")
    private String toStore;

    /**
     * 状态（1:操作中，2:完成）
     */
    @TableField(value = "STATUS")
    private String status;

    /**
     * 创建人ID
     */
    @TableField(value = "CREATOR_ID")
    private String creatorId;

    /**
     * 创建人
     */
    @TableField(value = "CREATOR")
    private String creator;

    /**
     * 头备注
     */
    @TableField(value = "REMARK")
    private String remark;

    /**
     * 类型  1:移库(包含同库移位和同厂区异库移位) 3:调拨（不同厂区走调拨）
     */
    @TableField(value = "TRAN_TYPE")
    private String tranType;

    /**
     * 创建时间
     */
    @TableField(value = "CREATE_TIME")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;


    /**
     * 行表字段展示
     */
    @TableField(exist = false)
    private String lineId;
    @TableField(exist = false)
    private String lineNum;

    @TableField(exist = false)
    private String materialCode;

    @TableField(exist = false)
    private String materialName;

    @TableField(exist = false)
    private String quantity;

    @TableField(exist = false)
    private String outQuantity;

    @TableField(exist = false)
    private String primaryUnit;

    @TableField(exist = false)
    private String lotsNum;

    @TableField(exist = false)
    private String fromLocator;
    @TableField(exist = false)
    private String lineState;
    @TableField(exist = false)
    private String lineRemark;
    @TableField(exist = false)
    private String toLocator;



    @TableField(exist = false)
    List<TransferOrderLineEntity> line;

}
