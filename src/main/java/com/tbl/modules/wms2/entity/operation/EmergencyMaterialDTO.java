package com.tbl.modules.wms2.entity.operation;

import com.tbl.common.utils.StringUtils;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 紧急物料信息
 *
 * @author Sweven
 * @date 2020-06-25
 */
@Data
@ToString
public class EmergencyMaterialDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 接收单号
     */
    public String deliveryNo;
    /**
     * 订单行号
     */
    public String deliveryLineNo;

    /**
     * 采购订单号
     */
    private String poNumber;

    /**
     * 采购订单行号
     */
    private String lineNum;


    /**
     * 物料编码
     */
    private String itemCode;

    /**
     * 物料描述
     */
    private String itemDesc;
    /**
     * 供应商编码
     */
    private String vendorCode;
    /**
     * 供应商名称
     */
    private String vendorName;
    /**
     * 销售订单号
     */
    private String orderNumber;
    /**
     * 销售订单行号
     */
    private String orderLineNumber;
    /**
     * 承办单位
     */
    private String salesrepName;
    /**
     * 需求日期
     */
    private String needByDate;
    /**
     * 生产厂
     */
    private String facName;
    /**
     * 采购订单头ID
     */
    private String poHeaderId;
    /**
     * 采购订单行ID
     */
    private String poLineId;

    /**
     * 生产日期
     */
    private String productionDate;

    /**
     * 批次号
     */
    private String lotsNum;

    /**
     * 数量
     */
    private String quantity;

    public String getOrderNumber() {
        return StringUtils.isNotBlank(orderNumber)?orderNumber:"";
    }

    public String getOrderLineNumber() {
        return StringUtils.isNotBlank(orderLineNumber)?orderLineNumber:"";
    }

    public String getSalesrepName() {
        return StringUtils.isNotBlank(salesrepName)?salesrepName:"";
    }

    public String getNeedByDate() {
        return StringUtils.isNotBlank(needByDate)?needByDate:"";
    }

    public String getFacName() {
        return StringUtils.isNotBlank(facName)?facName:"";
    }
}