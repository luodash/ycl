package com.tbl.modules.wms2.entity.baseinfo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Setter
@Getter
@ToString
public class PickingOutDealEntity implements Serializable {
    private String outcode;
    private List<PickingOutItemEntity> item;
}
