package com.tbl.modules.wms2.entity.insidewarehouse.check;

import lombok.Data;

import java.util.List;


/**
 * @author DASH
 */
@Data
public class YclCheckCreateDTO {

    YclCheckEntity checkEntity;
    List<YclCheckLineEntity> lines;
}
