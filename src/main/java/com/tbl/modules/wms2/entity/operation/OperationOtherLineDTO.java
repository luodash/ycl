package com.tbl.modules.wms2.entity.operation;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 其他出入库-存储过程参数
 * 创建行信息入参
 *
 * @author Sweven
 * @date 2020-07-13
 */
@Data
public class OperationOtherLineDTO implements Serializable {

    private static final long serialVersionUID = -6699324447470149311L;

    /** 返回代码(S:正确  E:错误) */
    private String retCode;
    /** 如果retCode为E,返回报错信息 */
    private String retMess;
    /** 行ID （当更新行数据时传入原EBS行ID，新增行时传空） */
    private Long lineId;
    /** 其他出入库头ID */
    private Long headId;
    /** 库存组织ID */
    private Long organizationId;
    /** 行号 */
    private Long lineNum;
    /** 物料编码 */
    private String materialCode;
    /** 物料单位 */
    private String primaryUnit;
    /** 事务处理数量 */
    private BigDecimal transactionQuantity;
    /** 子库 */
    private String subCode;
    /** 货位 */
    private Long locator;
    /** 备注 非必填 */
    private String remark;
    /** 目标子库 当事务处理类型为'13-子库存转移'时必填 */
    private String transferSubCode;
    /** 目标货位 非必填 */
    private Long transferLocator;
    /** 当前操作人工号 */
    private String user;

    public String getRemark() {
        return StringUtils.defaultIfBlank(remark, "");
    }

    public String getTransferSubCode() {
        return StringUtils.defaultIfBlank(transferSubCode, "");
    }

}