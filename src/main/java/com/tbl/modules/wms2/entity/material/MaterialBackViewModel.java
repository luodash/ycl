package com.tbl.modules.wms2.entity.material;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 退料入库单视图详情 Model
 *
 * @author Sweven
 * @date 2020-06-19
 */
@Getter
@Setter
@ToString
public class MaterialBackViewModel implements Serializable {

    private static final long serialVersionUID = 7225584896089095575L;

    /** 行ID **/
    private String lineId;
    /** 组织ID **/
    private String orgid;
    /** 组织名称 **/
    private String orgname;
    /** 退料申请单 **/
    private String billno;
    /** 退料申请单行号 **/
    private String seq;
    /** 来源子库编码 **/
    private String fromInvCode;
    /** 来源子库 **/
    private String fromInvDesc;
    /** 目标子库编码 **/
    private String rcvInvCode;
    /** 目标子库 **/
    private String rcvInvDesc;
    private String shopName;
    /** 生产厂区 **/
    private String shopDesc;
    /** 机台编码 **/
    private String deviceCode;
    /** 机台名称 **/
    private String deviceDesc;
    /** 工序编码 **/
    private String opcode;
    /** 工序 **/
    private String opdesc;
    /** 物料编码 **/
    private String itemname;
    /** 物料名称 **/
    private String itemdesc;
    /** 批次号 **/
    private String lotno;
    /** 数量 **/
    private String qty;
    /** 单位 **/
    private String uom;
    /** 关联的领料申请单 **/
    private String prevBillNo;
    /** 关联的领料申请单行号 **/
    private String prevBillSeq;
    /** 关联的领料申请单头ID **/
    private String prevBillHeaderId;
    /** 关联的领料申请单行ID **/
    private String prevBillLineId;
    /** 申请时间 **/
    private String createdate;
    /** 领料出库数量 */
    private String outBackQuantity;
    /** 业务类型，‘1’：机台任务单，‘2’：领料申请（来自EBS视图） */
    private String moType;
    /** 领料单号 */
    private String moCode;
    /** 事务处理类型名称 */
    private String transactionTypeName;

}