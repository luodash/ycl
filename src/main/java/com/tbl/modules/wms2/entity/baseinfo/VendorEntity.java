package com.tbl.modules.wms2.entity.baseinfo;

import lombok.Data;

/**
 * 供应商档案
 *
 * @author DASH
 */

@Data
public class VendorEntity {

    /**
     * 主键
     */
    private Integer id;

    /**
     * 供应商代码
     */
    private String code;

    /**
     * 供应商名称
     */
    private String name;

    /**
     * 状态
     */
    private String status;
}
