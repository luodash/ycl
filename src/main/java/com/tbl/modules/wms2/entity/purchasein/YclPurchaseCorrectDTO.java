package com.tbl.modules.wms2.entity.purchasein;

import com.tbl.modules.wms2.entity.operation.YclInOutFlowEntity;
import lombok.Data;

import java.util.List;

/**
 * @author DASH
 */
@Data
public class YclPurchaseCorrectDTO {

    YclPurchaseCorrectEntity correct;

    List<YclInOutFlowEntity> sub;

}
