package com.tbl.modules.wms2.entity.purchasein;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Setter
@Getter
@ToString
public class MeterialBackWmsEntity implements Serializable {

    private static final long serialVersionUID = 6844141778918963029L;

    /** 返回代码(S:正确  E:错误) */
    private String retCode;
    /** 如果retCode为E,返回报错信息 */
    private String retMess;
    /** 组织ID */
    private Long organizationId;
    /** 单据号 */
    private String billNo;
    /** 退料单行ID */
    private Long lineId;
    /** 料号 */
    private String materialCode;
    /** 批次 */
    private String lotsNum;
    /** 数量 */
    private BigDecimal quantity;
    /** 操作人 */
    private String operator;
    /** 发料时间 */
    private Date transDate;
    /** 备注 */
    private String comments;

}
