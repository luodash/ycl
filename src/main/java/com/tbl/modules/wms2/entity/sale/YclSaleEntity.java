package com.tbl.modules.wms2.entity.sale;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * 销售出库
 * @author
 */
@TableName("YCL_SALES")
@Getter
@Setter
@ToString
public class YclSaleEntity implements Serializable {

    /**
     * 主键id
     */
    @TableId(value = "ID", type = IdType.ID_WORKER_STR)
    private String ID;

    /**
     * 出库单号
     */
    @TableField(value = "OUT_CODE")
    private String outCode;

    /**
     * 销售订单号
     */
    @TableField(value = "ORDER_CODE")
    private String orderCode;

    /**
     * 订单类型
     */
    @TableField(value = "ORDER_TYPE")
    private String orderType;

    /**
     * 客户
     */
    @TableField(value = "CLIENT")
    private String client;

    /**
     * 收货地点
     */
    @TableField(value = "ADDRESS")
    private String address;

    /**
     * 厂区编码
     */
    @TableField(value = "AREA_CODE")
    private String areaCode;

    /**
     * 仓库（子库）编码
     */
    @TableField(value = "STORE_CODE")
    private String storeCode;

    /**
     * 临时提货区
     */
    @TableField(value = "TEMPORARY")
    private String temporary;

    /**
     * 发运时间
     */
    @TableField(value = "SHIPMENT")
    private String shipment;

    /**
     * 请求时间
     */
    @TableField(value = "REQUEST")
    private String request;

    /**
     * 发货方
     */
    @TableField(value = "CONSIGNER")
    private String consigner;

    /**
     * 状态（预留）
     */
    @TableField(value = "STATE")
    private String state;

    @TableField(value = "LINE_ID")
    private String lineId;

}
