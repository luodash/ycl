package com.tbl.modules.wms2.entity.processOutsourcing;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * 工序外协
 */

@Setter
@Getter
@Data
public class ProcessOutsourcingEntity {
    /**
     * 订单编码/TICKETNO
     */
    private String ticketno;

    /**
     * 工单号/MONAME
     */
    private String moName;

    /**
     * 物料名称/GOODSNAME
     */
    private String goodsName;

    /**
     * 物料类别/GOODSTYPE
     */
    private String goodsType;

    /**
     * 数量/QTY
     */
    private String qty;

    /**
     * 单位/UNIT
     */
    private String unit;

    /**
     * 发出组织
     */
    private String orgid;

    /**
     * 发出单位（厂）编码/SOURCEFACCODE
     */
    private String sourceFacCode;

    /**
     * 发出单位（厂）名称SOURCEFACDESC
     */
    private String sourceFacDesc;

    /**
     * 发出厂工序代码OPCODE
     */
    private String opCode;

    /**
     * 发出厂工序代码名称OPDESC
     */
    private String opDesc;

    /**
     * 接收组织（厂）编码
     * 默认95
     * RECEIVEFACCODE
     */
    private String receiveFacCode;

    /**
     * 接收单位编码
     */
    private String receivefaccode;

    /**
     * 接收单位名称
     *
     * RECEIVEUNIT
     */
    private String receiveUnit;

    /**
     * 出库确认人
     */
    private String invoutuser;

    /**
     * 出库确认时间
     */
    private String invouttime;

    /**
     * 入库确认人
     */
    private String invinuser;

    /**
     * 入库确认时间
     */
    private String invintime;

    /**
     *接收状态
     */
    private String decode;

    /**
     * 订单状态
     * receivestate,'未接收',receivestate,decode(sign(nvl(nattribute6,qty) - nattribute12),1,'部分接收','已接收')) receivestate,--接收状态
     */
    private String receivestate;

    /**
     *申请人工号
     * APPLYUSERCODE
     */
    private String applyUserCode;

    /**
     * 创建人（申请人姓名）
     * CREATEUSER
     */
    private String createUser;

    /**
     * 申请日期
     * CREATEDATE
     */
    private String createDate;

    /*
    applyusercode 申请人工号
    createdate  申请日期
    createuser  申请人名称
    goodsname 物料名称
    goodstype  物料类型
    moname  工单号
    opcode  发出厂工序代码
    opdesc  发出厂工序名称
    qty  交易数量
    receivefaccode 接收厂编码
    receiveunit  接收单位
    sourcefaccode  来源厂编码
    sourcefacdesc  来源厂名称
    ticketno  订单编号
    unit  单位
    */
}
