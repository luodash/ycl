package com.tbl.modules.wms2.entity.ordermanage.delivery;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 收货
 *
 * @author DASH
 */
@TableName("YCL_DELIVERY")
@Data
public class DeliveryEntity implements Serializable {

    private static final long serialVersionUID = -8347621534496598794L;
    /**
     * 主键ID
     */
    @TableId(value = "ID", type = IdType.ID_WORKER_STR)
    private String ID;

    /**
     * 订单行ID
     */
    @TableField(value = "PO_LINE_ID")
    private String poLineId;

    /**
     * 采购订单编号
     */
    @TableField(value = "PO_CODE")
    private String poCode;

    /**
     * 采购订单行号
     */
    @TableField(value = "PO_LINE_NUM")
    private String poLineNum;

    /**
     * 订单数量
     */
    @TableField(value = "PO_QUANTITY")
    private BigDecimal poQuantity;


    /**
     * 批次号
     */
    @TableField(value = "LOTS_NUM")
    private String lotsNum;

    /**
     * 物料ID
     */
    @TableField(value = "MATERIAL_Id")
    private String materialId;

    /**
     * 物料编码
     */
    @TableField(value = "MATERIAL_CODE")
    private String materialCode;

    /**
     * 物料名称
     */
    @TableField(value = "MATERIAL_NAME")
    private String materialName;

    /**
     * 单位
     */
    @TableField(value = "PRIMARY_UNIT")
    private String primaryUnit;

    /**
     * 件数
     */
    @TableField(value = "BOX_AMOUNT")
    private BigDecimal boxAmount;

    /**
     * 发运数量（送货数量）
     */
    @TableField(value = "SHIP_QUANTITY")
    private BigDecimal shipQuantity;

    /**
     * 收货数量
     */
    @TableField(value = "RECEIVE_QUANTITY")
    private BigDecimal receiveQuantity;

    /**
     * 入库数量
     */
    @TableField(value = "DELIVER_QUANTITY")
    private BigDecimal deliverQuantity;

    /**
     * 行备注
     */
    @TableField(value = "COMMENTS")
    private String comments;
    /**
     * 订单创建日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @TableField(value = "CREATE_TIME")
    private Date createTime;

    /**
     * 送货单号
     */
    @TableField(value = "ASN_NUMBER")
    private String asnNumber;

    /**
     * 供应商名称
     */
    @TableField(value = "VENDOR_NAME")
    private String vendorName;

    /**
     * 供应商编码
     */
    @TableField(value = "VENDOR_CODE")
    private String vendorCode;

    /**
     * 接收单号
     */
    @TableField(value = "DELIVERY_NO")
    private String deliveryNo;

    /**
     * 接收单行号
     */
    @TableField(value = "DELIVERY_LINE_NO")
    private String deliveryLineNo;
    /**
     * 收货公司ID
     */
    @TableField(value = "ENTITY_ID")
    private String entityId;

    /**
     * 是否允许入库((0:否 1:是))
     */
    @TableField(value = "IS_PASS")
    private String isPass;

    /**
     * 采购接收时调用存储过程返回的EBS行ID
     */
    @TableField(value = "EBS_LINE_ID")
    private String ebsLineId;

    /**
     * 类别: 1-送货单 2-委外 4-退料
     */
    @TableField(value = "TYPE")
    private String type;

}
