package com.tbl.modules.wms2.entity.requestXML;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 *  FEWMS201  WMS接口-采购订单-订单行接口
 * @author 70486
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "orderLine"
})
@XmlRootElement(name = "orderLines")
@Getter
@Setter
@ToString
public class FEWMS201_orderlines {
	/**
	 * 订单行号
	 */
	public List<FEWMS201_orderline> orderLine;

}
