package com.tbl.modules.wms2.entity.requestXML;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *  FEWMS202  WMS接口-送货单-送货单行数据
 * @author 70486
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
		"poCode",
		"lineNo",
		"batch",
		"materialCode",
		"materialName",
		"unit",
		"packing",
		"quantity",
		"receiveQuality",
		"upQuantity",
		"remark"
})
@XmlRootElement(name = "deliveryline")
@Getter
@Setter
@ToString
public class FEWMS202_deliveryline {

	/**
	 * 订单行号
	 */
	public String lineNo;
	/**
	 * 采购订单编号
	 */
	public String poCode;
	/**
	 * 批次号
	 */
	public String batch;
	/**
	 * 物料编码
	 */
	public String materialCode;
	/**
	 * 物料名称
	 */
	public String materialName;
	/**
	 * 单位
	 */
	public String unit;
	/**
	 * 送货件数
	 */
	public String packing;
	/**
	 * 送货数量
	 */
	public String quantity;
	/**
	 * 收货数量
	 */
	public String receiveQuality;

	/**
	 * 上架数量
	 */
	public String upQuantity;

	/**
	 * 备注
	 */
	public String remark;

}
