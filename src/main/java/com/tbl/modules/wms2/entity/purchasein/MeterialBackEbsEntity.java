package com.tbl.modules.wms2.entity.purchasein;

import com.tbl.common.utils.StringUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

@Getter
@Setter
@ToString
public class MeterialBackEbsEntity implements Serializable {

    private static final long serialVersionUID = -1581819855928352851L;

    /** 返回代码(S:正确  E:错误) */
    private String retCode;
    /** 如果retCode为E,返回报错信息 */
    private String retMess;
    /** 库存组织Id */
    private Long orgId;
    /** 业务类型，‘1’：机台任务单，‘2’：领料申请（来自EBS视图） */
    private String moType;
    /** 领料单号 */
    private String lyNumber;
    /** 出库单号 */
    private String ckNumber;
    /** 物料编码 */
    private String itemNumber;
    /** 退库数量 */
    private BigDecimal qty;
    /** 事务处理类型名称 */
    private String transactionTypeName;
    /** 事务处理日期 YYYY-MM-DD HH24:MI:SS */
    private String transactionDate;
    /** 返修标识  目前没有，传空 */
    private String fxFlag;
    /** 折扣   目前没有，传空 */
    private BigDecimal discount;

    public String getFxFlag() {
        return StringUtils.isNotBlank(fxFlag) ? fxFlag : "";
    }

}
