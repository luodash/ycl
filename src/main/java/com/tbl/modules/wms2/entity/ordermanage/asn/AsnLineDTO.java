package com.tbl.modules.wms2.entity.ordermanage.asn;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author DASH
 */
@Data
public class AsnLineDTO implements Serializable {

    private static final long serialVersionUID = 4347241086433883865L;
    /**
     * 主键ID
     */
    @TableId(value = "ASN_LINE_ID", type = IdType.ID_WORKER_STR)
    private String asnLineId;

    /**
     * 送货单主键ID
     */
    @TableField(value = "ASN_HEADER_ID")
    private String asnHeaderId;

    /**
     * 送货单编号
     */
    @TableField(value = "ASN_NUMBER")
    private String asnNumber;

    private String areaCode;

    private String areaName;

    private String storeCode;

    private String storeName;

    /**
     * 采购订单编号
     */
    @TableField(value = "PO_CODE")
    private String poCode;

    /**
     * 采购订单行号
     */
    @TableField(value = "PO_LINE_NUM")
    private String poLineNum;

    /**
     * 采购订单号
     */
    @TableField(value = "PO_LINE_ID")
    private String poLineId;

    /**
     * 批次号
     */
    @TableField(value = "LOTS_NUM")
    private String lotsNum;

    /**
     * 物料编码
     */
    @TableField(value = "MATERIAL_CODE")
    private String materialCode;

    /**
     * 物料名称
     */
    @TableField(value = "MATERIAL_NAME")
    private String materialName;

    /**
     * 单位
     */
    @TableField(value = "PRIMARY_UNIT")
    private String primaryUnit;

    /**
     * 件数
     */
    @TableField(value = "BOX_AMOUNT")
    private String boxAmount;

    /**
     * 发运数量（送货数量）
     */
    @TableField(value = "SHIP_QUANTITY")
    private String shipQuantity;

    /**
     * (事务数量)在途
     */
    @TableField(value = "TRANSACTION_QUANTITY")
    private String transactionQuantity;

    /**
     * 收货数量
     */
    @TableField(value = "RECEIVE_QUANTITY")
    private String receiveQuantity;

    /**
     * 入库数量
     */
    @TableField(value = "DELIVER_QUANTITY")
    private String deliverQuantity;

    /**
     * 退回至接收数量
     */
    @TableField(value = "RETURN_RECEIVE_QUANTITY")
    private String returnReceiveQuantity;

    /**
     * 退回至供应商数量
     */
    @TableField(value = "RETURN_VENDOR_QUANTITY")
    private String returnVendorQuantity;

    /**
     * 取消数量
     */
    @TableField(value = "CANCEL_QUANTITY")
    private String cancelQuantity;


    /**
     * 关闭数量
     */
    @TableField(value = "CLOSED_QUANTITY")
    private String closedQuantity;

    /**
     * 行备注
     */
    @TableField(value = "COMMENTS")
    private String comments;

    /**
     * 需求日期
     */
    @TableField(value = "NEED_BY_DATE")
    private Date needByDate;


    /**
     * 接收日期
     */
    @TableField(value = "RECEIVE_DATE")
    private Date receiveDate;

    /**
     * 订单创建日期
     */
    @TableField(value = "ORDER_TIME")
    private Date orderTime;

}
