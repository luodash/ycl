package com.tbl.modules.wms2.entity.operation;

import lombok.Data;

import java.io.Serializable;

/**
 * 其他出入库-存储过程参数
 * 数据校验信息入参
 *
 * @author Sweven
 * @date 2020-07-13
 */
@Data
public class OperationOtherValidDTO implements Serializable {

    private static final long serialVersionUID = 4038541948137937487L;

    /** 返回代码(S:正确  E:错误) */
    private String retCode;
    /** 如果retCode为E,返回报错信息 */
    private String retMess;
    /** 其他出入库头ID */
    private Long headId;

}