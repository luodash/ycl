package com.tbl.modules.wms2.entity.production;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * 领料配送对应表
 * @author
 */
@TableName("YCL_PICK_DELIVERY_REL")
@Getter
@Setter
@ToString
public class YclPickDeliveryRelEntity implements Serializable {

    /**
     * 领料单号
     */
    @TableField(value = "MO_CODE")
    private String moCode;

    /**
     * 配送单号
     */
    @TableField(value = "DELIVERY_ID")
    private String deliveryId;

    /**
     * 配送单号
     */
    @TableField(value = "DELIVERY_DATE")
    private String deliveryDate;

}
