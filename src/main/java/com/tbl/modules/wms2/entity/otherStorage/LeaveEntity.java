package com.tbl.modules.wms2.entity.otherStorage;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 出厂单
 *
 * @author DASH
 */
@TableName("YCL_LEAVE")
@Data
public class LeaveEntity {

    @TableId(value = "ID", type = IdType.ID_WORKER_STR)
    private String id;
    /**
     * 出厂单号
     */
    @TableField(value = "OUT_CODE")
    private String outCode;
    /**
     * 相关单号(导入来源)
     */
    @TableField(value = "ORDER_CODE")
    private String orderCode;

    /**
     * 货物类型
     */
    @TableField(value = "GOOD_TYPE")
    private String goodType;

    /**
     * 单据类型
     */
    @TableField(value = "BILL_TYPE")
    private String billType;

    /**
     * 驾驶员号码
     */
    @TableField(value = "DRIVER")
    private String driver;

    /**
     * 车牌号码
     */
    @TableField(value = "AUTO_NUM")
    private String autoNum;

    /**
     * 公司
     */
    @TableField(value = "COMPANY")
    private String company;

    /**
     * 付款状态
     */
    @TableField(value = "PAY_STATUS")
    private String payStatus;

    /**
     * 导入来源
     */
    @TableField(value = "IN_SOURCE")
    private String inSource;

    /**
     * 运输方
     */
    @TableField(value = "TRANSPORT")
    private String transport;

    /**
     * 发出单位
     */
    @TableField(value = "SENDOUT")
    private String sendout;

    /**
     * 接收单位
     */
    @TableField(value = "RECEIVER")
    private String receiver;

    /**
     * 价格导入来源
     */
    @TableField(value = "SOURCE")
    private String source;

    @TableField(value = "APPLY_USER")
    private String applyUser;

    @TableField(value = "APPLY_TEL")
    private String applyTel;

    @TableField(value = "APPLY_DATE")
    private String applyDate;

    @TableField(value = "REMARK")
    private String remark;

    @TableField(value = "CREATOR")
    private String creator;

    @TableField(value = "SEND_NUM")
    private String sendNum;

    @TableField(value = "CREATE_TIME")
    private Date createTime;

    /**
     * 行数据
     */
    @TableField(exist = false)
    List<LeaveLineEntity> line;
}
