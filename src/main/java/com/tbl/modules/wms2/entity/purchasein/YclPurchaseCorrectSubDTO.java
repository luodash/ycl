package com.tbl.modules.wms2.entity.purchasein;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 采购更正
 *
 * @author DASH
 */
@Data
public class YclPurchaseCorrectSubDTO {

    /**
     * 主键id
     */
    @TableId(value = "ID", type = IdType.ID_WORKER_STR)
    private String id;

    /**
     * 更正单号
     */
    @TableField(value = "CORRECT_CODE")
    private String correctCode;

    /**
     * 订单号
     */
    @TableField(value = "PO_CODE")
    private String poCode;

    /**
     * 订单行号
     */
    @TableField(value = "PO_LINE_NUM")
    private String poLineNum;

    /**
     * 物料编号
     */
    @TableField(value = "MATERIAL_CODE")
    private String materialCode;


    /**
     * 物料编号
     */
    @TableField(value = "PO_LINE_ID")
    private String poLineId;

    /**
     * 物料名称
     */
    @TableField(value = "MATERIAL_NAME")
    private String materialName;

    /**
     * 批次号
     */
    @TableField(value = "LOTS_NUM")
    private String lotsNum;

    /**
     * 订单日期
     */
    @TableField(value = "ORDER_TIME")
    @DateTimeFormat(pattern = "yyyy-MM-hh")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    private Date orderTime;

    /**
     * 订单日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-hh")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    private Date createTime;

    /**
     * 供应商ID
     */
    @TableField(value = "SUPPLIER_ID")
    private String supplierId;

    /**
     * 供应商编号
     */
    @TableField(value = "SUPPLIER_CODE")
    private String supplierCode;

    /**
     * 供应商名称
     */
    @TableField(value = "SUPPLIER_NAME")
    private String supplierName;

    /**
     * 采购组织
     */
    @TableField(value = "PURCHASE_ORGAN")
    private String purchaseOrgan;

    /**
     * 采购部门
     */
    @TableField(value = "PURCHASE_DEPT")
    private String purchaseDept;

    /**
     * 采购人
     */
    @TableField(value = "PURCHASE_USER")
    private String purchaseUser;

    /**
     * 厂区
     */
    private String areaCode;
    private String areaName;

    /**
     * 所在仓库
     */
    private String storeCode;
    private String storeName;

    /**
     * 库位
     */
    private String locatorCode;


    /**
     * 单位
     */
    @TableField(value = "PRIMARY_UNIT")
    private String primaryUnit;



    /**
     * 出入数量
     */
    private BigDecimal amount;

    /**
     * 出：0 入：1
     */
    private String inout;

    private BigDecimal correctQuantity;

    private String vendorId;

    private String vendorName;

}
