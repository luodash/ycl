package com.tbl.modules.wms2.entity.purchasein;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

@Setter
@Getter
@ToString
public class MeteriaRcvEbsEntity implements Serializable {
    /**
     *头ID（EBS的）
     */
    private Long headerId;
    /**
     *行ID
     */
    private Long lineId;
    /**
     *库存组织ID
     */
    private Long orgId;
    /**
     *供应商批次
     */
    private String vendorLot;
    /**
     *事务处理日期
     */
    private String transactionDate;
    /**
     *出库数量
     */
    private BigDecimal transactionQuantity;
    /**
     *备注
     */
    private String comments;
    /**
     *供应商名称
     */
    private String vendorName;
    /**
     *来源货位ID
     */
    private Long fromLocatorId;
    /**
     *目标货位ID
     */
    private Long toLocatorId;

}
