package com.tbl.modules.wms2.entity.operation;

import com.tbl.common.utils.StringUtils;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 其他出入库-存储过程参数
 * 创建头信息入参
 *
 * @author Sweven
 * @date 2020-06-25
 */
@Data
public class OperationOtherHeadDTO implements Serializable {

    private static final long serialVersionUID = 1733906970928844353L;

    /** 返回代码(S:正确  E:错误) */
    private String retCode;
    /** 如果retCode为E,返回报错信息 */
    private String retMess;
    /** 头ID （当更新头数据时传入原EBS头ID,新增头信息时传空） */
    private Long headId;
    /** 库存组织ID */
    private Long organizationId;
    /** 单据类型(INBOND/OUTBOND) */
    private String businessType;
    /** 事务处理类型 */
    private String businessPurpose;
    /** 事务处理日期 */
    private Date transactionDate;
    /** 部门号 */
    private String departCode;
    /** 工序(非必须) */
    private String operationSeqNum;
    /** 机台(非必须) */
    private String operationMachine;
    /** 申请人工号 */
    private String preparer;
    /** 备注，(非必须) */
    private String remark;
    /** 当前操作人工号 */
    private String user;
    /** 领用用途(非必须) */
    private String lyytCode;
    /** 装车通知单(当事务处理类型为'1-3废品销售出库'时必填) */
    private String load;
    /** 供应商(非必须) */
    private String vendorName;

//    public String getOperationSeqNum() {
//        return StringUtils.isNotBlank(operationSeqNum) ? operationSeqNum : "";
//    }
//
//    public String getOperationMachine() {
//        return StringUtils.isNotBlank(operationMachine) ? operationMachine : "";
//    }

//    public String getRemark() {
//        return StringUtils.isNotBlank(remark) ? remark : "";
//    }
//
//    public String getLyytCode() {
//        return StringUtils.isNotBlank(lyytCode) ? lyytCode : "";
//    }
//
//    public String getVendorName() {
//        return StringUtils.isNotBlank(vendorName) ? vendorName : "";
//    }
//
//    public String getLoad() {
//        return StringUtils.isNotBlank(load) ? load : "";
//    }

}