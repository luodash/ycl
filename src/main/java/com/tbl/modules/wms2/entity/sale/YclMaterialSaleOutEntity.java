package com.tbl.modules.wms2.entity.sale;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 领料出库
 * @author
 */
@TableName("apps.cux_wms_ycl_order_v@wms_to_ebs.fegroup.com.cn")
@Getter
@Setter
@ToString
public class YclMaterialSaleOutEntity implements Serializable {

    @TableField(value = "NAME")
    private String name;

    @TableField(value = "ORDER_NUMBER")
    private String orderNumber;

    @TableField(value = "ORDER_TYPE")
    private String orderType;

    @TableField(value = "ITEM_CODE")
    private String itemCode;

    @TableField(value = "ITEM_DESC")
    private String itemDesc;

    @TableField(value = "CUSTOMER_NAME")
    private String customerName;

    @TableField(value = "STAGE")
    private String stage;

    @TableField(value = "ORGANIZATION_NAME")
    private String organizationName;

    @TableField(value = "ORG_ID")
    private String orgId;

    @TableField(value = "ORDERED_QUANTITY")
    private String orderedQuantity;

    @TableField(value = "CREATION_DATE")
    private String creationDate;

    @TableField(value = "UOM_CODE")
    private String uomCode;

    @TableField(exist = false)
    private String lineId;

    @TableField(exist = false)
    private String outstats;
    @TableField(exist = false)
    private String address;
    @TableField(exist = false)
    private String consigner;
    @TableField(exist = false)
    private String quality;
    @TableField(exist = false)
    private String storeCode;
    @TableField(exist = false)
    private String localCode;
    @TableField(exist = false)
    private String batch;
    @TableField(exist = false)
    private String factoryCode;
    @TableField(exist = false)
    private String headerId;
}
