package com.tbl.modules.wms2.entity.purchasein;

import lombok.Data;

import java.io.Serializable;
@Data
public class MaterialQcEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    private String materialCode;
    private String materialName;
    private String lotsNum;
    private String businessCode;
    private String quantity;
    private String primaryUnit;
    private String qcWms;

}
