package com.tbl.modules.wms2.entity.insidewarehouse.check;


import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.io.Serializable;

/**
 * 盘库子表
 *
 * @author DASH
 */
@TableName("YCL_CHECK_LINE")
@Data
public class YclCheckLineEntity implements Serializable {

    private static final long serialVersionUID = -5475405854623750422L;
    /**
     * 主键id
     */
    @TableId(value = "ID", type = IdType.ID_WORKER_STR)
    private String id;

    /**
     * 盘点订单编号
     */
    @TableField(value = "CHECK_CODE")
    private String checkCode;

    /**
     * 盘点库位
     */
    @TableField(value = "LOCATOR_CODE")
    private String locatorCode;

    /**
     * 批次码（每个成品唯一，贴在外包装上）
     */
    @TableField(value = "LOTS_NUM")
    private String lotsNum;

    /**
     * 物料编码
     */
    @TableField(value = "MATERIAL_CODE")
    private String materialCode;

    /**
     * 物料名称
     */
    @TableField(value = "MATERIAL_NAME")
    private String materialName;

    /**
     * 单位
     */
    @TableField(value = "PRIMARY_UNIT")
    private String primaryUnit;

    /**
     * 系统数量
     */
    @TableField(value = "QUALITY")
    private Integer quality;

    /**
     * 移出数量
     */
    @TableField(value = "CHECK_QUALITY")
    private Integer checkQuality;

    /**
     * 检验状态
     */
    @TableField(value = "STATE")
    private String state;

    /**
     * 备注
     */
    @TableField(value = "REMARK")
    private String remark;

    /**
     * 是否需要称重：0-不需要，1-需要
     */
    @TableField(value = "IS_WEIGH")
    private String isWeigh;

}
