package com.tbl.modules.wms2.entity.workorder;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
@Setter
@Getter
@ToString
public class SaveWorkOrderEntity implements Serializable {
    /*
    *类型  保存："save"      提交："commit"
     */
    private String type;
    /**委外加工头ID*/
    private Long poId;
    /**库存组织ID*/
    private Long organizationId;
    /**接收数量*/
    private BigDecimal rcvQuantity;
    /**委外加工物料(废品)*/
    private Long itemId;
    /**费用接收子库*/
    private String subinventory;
    /**费用接收货位*/
    private Long locatorId;
    /**完工物料ID*/
    private Long wxItemId;
    /**接收行ID*/
    private Long rcvId;
    private Long materialId;
    private Long completeItemId;/**完工物料ID*/
    private String completedSub;/**完工子库*/
    private Long completedLocatorId;
    private String completeLocator;/**完工货位*/
    private BigDecimal completeQty;/**完工数量*/
    private Long fyItemId;/**加工费物料ID*/
    private String fySub;/**加工费发料子库*/
    private Long fyLocatorId;/**加工费发料货位id*/
    private String fyLocator;/**加工费发料货位*/
    private BigDecimal fyQty;/**加工费发料数量*/
    private Long ftItemId;/**消耗物料ID*/
    private String ftSub;/**消耗物料发料子库*/
    private String ftSubCode;/**消耗物料发料子库编码*/
    private String ftLocator;/**消耗物料发料货位*/
    private String ftLocatorId;/**消耗物料发料货位ID*/
    private BigDecimal ftQty;/**消耗物料发料数量*/
    private Long wientityId;/**工单ID*/
    private String vendorLot;/**供应商批次*/
    private String vendorFtLot;/**消耗物料供应商批次*/
    private Long wicomId;/**完工行ID*/
    private BigDecimal FtcompletedQuantity;
    private Long FtComponentLocatorId;
    private String FtComponentSubinventory;
    private BigDecimal FyCompletedQuantity;
    private BigDecimal CompletedQuantity;

}
