package com.tbl.modules.wms2.entity.ordermanage.asn;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@TableName("YCL_ASN_LINE")
@Data
public class AsnLineEntity implements Serializable {

    /**
     * 主键ID
     */
    @TableId(value = "ASN_LINE_ID", type = IdType.ID_WORKER_STR)
    private String asnLineId;

    /**
     * 送货单主键ID
     */
    @TableField(value = "ASN_HEADER_ID")
    private String asnHeaderId;


    /**
     * 采购订单编号
     */
    @TableField(value = "PO_CODE")
    private String poCode;

    /**
     * 采购订单行号
     */
    @TableField(value = "PO_LINE_NUM")
    private String poLineNum;

    /**
     * 采购订单号
     */
    @TableField(value = "PO_LINE_ID")
    private String poLineId;

    /**
     * 批次号
     */
    @TableField(value = "LOTS_NUM")
    private String lotsNum;

    /**
     * 物料编码
     */
    @TableField(value = "MATERIAL_CODE")
    private String materialCode;

    /**
     * 物料名称
     */
    @TableField(value = "MATERIAL_NAME")
    private String materialName;

    /**
     * 单位
     */
    @TableField(value = "PRIMARY_UNIT")
    private String primaryUnit;

    /**
     * 件数
     */
    @TableField(value = "BOX_AMOUNT")
    private BigDecimal boxAmount;

    /**
     * 订单数量
     */
    @TableField(value = "PO_QUANTITY")
    private BigDecimal poQuantity;

    /**
     * 发运数量（送货数量）
     */
    @TableField(value = "SHIP_QUANTITY")
    private BigDecimal shipQuantity;

    /**
     * (事务数量)在途
     */
    @TableField(value = "TRANSACTION_QUANTITY")
    private BigDecimal transactionQuantity;

    /**
     * 收货数量
     */
    @TableField(value = "RECEIVE_QUANTITY")
    private BigDecimal receiveQuantity;

    /**
     * 入库数量
     */
    @TableField(value = "DELIVER_QUANTITY")
    private BigDecimal deliverQuantity;

    /**
     * 退回至接收数量
     */
    @TableField(value = "RETURN_RECEIVE_QUANTITY")
    private BigDecimal returnReceiveQuantity;

    /**
     * 退回至供应商数量
     */
    @TableField(value = "RETURN_VENDOR_QUANTITY")
    private BigDecimal returnVendorQuantity;

    /**
     * 取消数量
     */
    @TableField(value = "CANCEL_QUANTITY")
    private BigDecimal cancelQuantity;


    /**
     * 关闭数量
     */
    @TableField(value = "CLOSED_QUANTITY")
    private BigDecimal closedQuantity;

    /**
     * 行备注
     */
    @TableField(value = "COMMENTS")
    private String comments;

    /**
     * 需求日期
     */
    @TableField(value = "NEED_BY_DATE")
    private Date needByDate;


    /**
     * 接收日期
     */
    @TableField(value = "RECEIVE_DATE")
    private Date receiveDate;

    /**
     * 订单创建日期
     */
    @TableField(value = "ORDER_TIME")
    private Date orderTime;

    /**
     * 库位
     */
//    @TableField(value = "LOCATOR_CODE")
//    private String locatorCode;

    /**
     * 状态（1：已上架,0:未上架）
     */
    @TableField(value = "STATUS")
    private String status;

    /**
     * 标签号
     */
    @TableField(value = "LOTS_NUM_YD")
    private String lotsNumYd;

}
