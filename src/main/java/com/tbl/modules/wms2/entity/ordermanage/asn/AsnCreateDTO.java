package com.tbl.modules.wms2.entity.ordermanage.asn;

import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author DASH
 */
@TableName("YCL_ASN_HEADER")
@Data
public class AsnCreateDTO implements Serializable {
    private static final long serialVersionUID = -3063074562001454834L;

    private AsnHeaderEntity asnHeader;

    private List<AsnLineEntity> asnLines;

}
