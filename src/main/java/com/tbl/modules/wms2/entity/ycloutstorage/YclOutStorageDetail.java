package com.tbl.modules.wms2.entity.ycloutstorage;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 出库详情
 * @author 70486
 */
@TableName("YDDL_OUTSTORAGE_DETAIL")
@Getter
@Setter
@ToString
public class YclOutStorageDetail implements Serializable {

	/**
	 * 主键
	 */
    @TableId(value = "ID")
    private Long id;

    /**
     * 入库单号
     */
    @TableField(value = "IN_STORAGE_CODE")
    private String inStorageCode;

    /**
     * 物料编码
     */
    @TableField(value = "MATERIALCODE")
    private String materialCode;

    /**
     * 物料名称
     */
    @TableField(value = "MATERIALNAME")
    private String materialName;

    /**
     * 批次码
     */
    @TableField(value = "BATCH_CODE")
    private String batchCode;

    /**
     * 出库数量
     */
    @TableField(value = "QUANTITY")
    private String quantity;


    /**
     * 单位
     */
    @TableField(value = "UNIT")
    private String unit;
    
    /**
     * 操作时间
     */
    @TableField(value = "UPDATE_TIME")
    private Date updateTime;

}
