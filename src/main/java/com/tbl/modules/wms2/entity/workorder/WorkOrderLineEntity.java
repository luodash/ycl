package com.tbl.modules.wms2.entity.workorder;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 采购订单-行表(原材料、委外加工)
 * @author 70486
 */
@TableName("YCL_WORK_ORDER_LINE")
@Getter
@Setter
@ToString
public class WorkOrderLineEntity implements Serializable {

    /**
     * 主键
     */
    @TableId(value = "WO_LINE_ID")
    private Long poLineId;

    /**
     * 采购订单编号
     */
    @TableField(value = "PO_CODE")
    private String poCode;

    /**
     * 物料ID
     */
    @TableField(value = "MATERIAL_ID")
    private Long materialId;

    /**
     * 物料编码
     */
    @TableField(value = "MATERIAL_CODE")
    private String materialCode;

    /**
     * 物料名称
     */
    @TableField(value = "MATERIAL_NAME")
    private String materialName;

    /**
     * 主单位
     */
    @TableField(value = "PRIMARY_UNIT")
    private String primaryUnit;


    /**
     * 订单数量
     */
    @TableField(value = "QUANTITY")
    private BigDecimal quantity;

    /**
     * 订单行号
     */
    @TableField(value = "LINE_NUM")
    private String lineNum;

    /**
     * 采购订单主键ID
     */
    @TableField(value = "WO_ID")
    private Long woId;

    /**
     * 完工物料ID
     */
    @TableField(value = "WXITEM_ID")
    private Long wxitemId;

    /**
     * 完工物料编码
     */
    @TableField(value = "WXITEM_CODE")
    private  String wxitemCode;

    /**
     * 完工物料名称
     */
    @TableField(value = "WXITEM_NAME")
    private String wxitemName;


    /**
     * 是否失效（'0':否  '1':是）
     */
    private String expired;


}
