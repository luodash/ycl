package com.tbl.modules.wms2.entity.workorder;


import com.mchange.lang.DoubleUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

@Getter
@Setter
@ToString

public class ReceiveOrderEntity implements Serializable {
    private Long poId ;/**订单编号-外键ID**/
    private Long rcvId;/**接收单号-主键ID**/
    private Long organizationId;/**库存组织ID**/
    private String organizationCode;/**库存组织编码**/
    private String organizationName;/**库存组织**/
    private BigDecimal rcvQuantity;/**接受数量**/
    private String receiptDate;/**收据日期**/
    private Long materialId ;/**委外加工费物料ID**/
    private String materialCode ;/**委外加工费物料编码**/
    private String materialName;/**委外加工费物料名称**/
    private String subinventory;/**费用接受子库存**/
    private String subinventoryDesc;/**费用接受子库存描述**/
    private Long locatorId;/**费用接受货位ID**/
    private String locatorCode;/**费用接受货位**/
    private String locatorDesc;/**费用接受货位描述**/
    private Long wxItemId ;/**完工物料ID**/
    private String wxItemNum ;/**完工物料编码**/
    private String wxItemDesc;/**完工物料名称**/
    private String attribute1;/**操作状态**/
    private String orderTime ;/**订单创建时间**/
    private Long wipEntityId;/**工单ID**/
    private String wipEntityName;/**工单名称**/
    private BigDecimal completedQuantity;/**完工数量**/
    private BigDecimal fycompletedQuantity;/**加工费发料数量**/
    private BigDecimal ftcompletedQuantity;/**消耗物料发料数量**/

}
