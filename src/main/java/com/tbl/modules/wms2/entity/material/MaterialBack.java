package com.tbl.modules.wms2.entity.material;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 退料入库单详情
 *
 * @author Sweven
 * @date 2020-06-19
 */
@Getter
@Setter
@ToString
@TableName(value = "YCL_MATERIAL_BACK")
public class MaterialBack implements Serializable {

    private static final long serialVersionUID = -6926865358231198682L;

    /** 主键id */
    @TableId(value = "ID", type = IdType.INPUT)
    private Long id;
    /** 入库单号 */
    @TableField(value = "OUT_CODE")
    private String outCode;
    /** 厂区编码 */
    @JsonProperty("shopName")
    @TableField(value = "AREA_CODE")
    private String areaCode;
    /** 厂区名称 */
    @JsonProperty("shopDesc")
    @TableField(value = "AREA_NAME")
    private String areaName;
    /** 仓库（库区）编码 */
    @JsonProperty("fromInvCode")
    @TableField(value = "STORE_CODE")
    private String storeCode;
    /** 库位编码 */
    @NotNull(message = "请填写库位编码")
    @TableField(value = "LOCATOR_CODE")
    private String locatorCode;
    /** 退料申请单号 */
    @JsonProperty("billno")
    @TableField(value = "APPLY_CODE")
    private String applyCode;
    /** 退料单行号 */
    @JsonProperty("seq")
    @TableField(value = "LINE_CODE")
    private String lineCode;
    /** 物料编码 */
    @JsonProperty("itemname")
    @TableField(value = "MATERIAL_CODE")
    private String materialCode;
    /** 物料名称 */
    @JsonProperty("itemdesc")
    @TableField(value = "MATERIAL_NAME")
    private String materialName;
    /** 单位 */
    @JsonProperty("uom")
    @TableField(value = "PRIMARY_UNIT")
    private String primaryUnit;
    /** 批次号 */
    @JsonProperty("lotno")
    @TableField(value = "LOTS_NUM")
    private String lotsNum;
    /** 生产班次 */
    @TableField(value = "SHIFT")
    private String shift;
    /** 机台 */
    @JsonProperty("deviceCode")
    @TableField(value = "DEVICE")
    private String device;
    /** 退料数量 */
    @JsonProperty("qty")
    @TableField(value = "BACK_QUANTITY")
    private BigDecimal backQuantity;
    /** 入库数量 */
    @TableField(value = "IN_QUANTITY")
    private BigDecimal inQuantity;
    /** 领料出库数量 */
    @TableField(value = "OUT_BACK_QUANTITY")
    private BigDecimal outBackQuantity;
    /** 质检状态 */
    @TableField(value = "CHECK_STATE")
    private String checkState;
    /** 状态 */
    @TableField(value = "STATE")
    private String state;
    /** 申请时间 */
    @JsonProperty("createdate")
    @TableField(value = "APPLY_TIME")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date applyTime;
    /** 创建人 */
    @TableField(value = "CREATE_USER")
    private String createUser;
    /** 创建时间(系统时间) */
    @TableField(value = "CREATE_TIME")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /** 行ID */
    @TableField(value = "LINE_ID")
    private String lineId;
    /** 件数 */
    @TableField(value = "BOX_AMOUNT")
    private BigDecimal boxAmount;
    /** 组织ID */
    @JsonProperty("orgid")
    @TableField(value = "ORGANIZATION_ID")
    private Long organizationId;
    /** 领料申请单header_id */
    @TableField(value = "PREV_BILL_HEADER_ID")
    private String prevBillHeaderId;
    /** 领料申请单line_id */
    @TableField(value = "PREV_BILL_LINE_ID")
    private String prevBillLineId;

    private transient String applyTimeStart;
    private transient String applyTimeEnd;

    /** 业务类型，‘1’：机台任务单，‘2’：领料申请（来自EBS视图） */
    private String moType;
    /** 领料单号 */
    private String moCode;
    /** 事务处理类型名称 */
    private String transactionTypeName;

}