package com.tbl.modules.wms2.entity.purchasein;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 采购订单主表(原材料、委外加工)
 * @author 70486
 */

@Getter
@Setter
@ToString
public class PurchaseOrderEntity implements Serializable {

    @TableId(value = "PO_HEADER_ID")
    private Long poHeaderId;

    /**
     * 采购订单编号
     */
    @TableField(value = "PO_CODE")
    private String poCode;

    /**
     * 订单日期
     */
    @TableField(value = "ORDER_TIME")
    @DateTimeFormat(pattern = "yyyy-MM-hh HH:mm:ss")
    private String orderTime;

    /**
     * 查询条件-开始时间
     */
    @TableField(exist = false)
    private String orderTimeStart;

    /**
     * 查询条件-结束时间
     */
    @TableField(exist = false)
    private String orderTimeEnd;

    /**
     * 供应商ID
     */
    @TableField(value = "SUPPLIER_ID")
    private String supplierId;

    /**
     * 供应商名称
     */
    @TableField(value = "SUPPLIER_NAME")
    private String supplierName;

    /**
     * 采购组织(公司或实体)
     */
    @TableField(value = "PURCHASE_ORGAN")
    private String purchaseOrgan;
    @TableField(value = "PURCHASE_ORGAN_ID")
    private String purchaseOrganId;

    /**
     * 收货方
     */
    @TableField(value = "PURCHASE_DEPT")
    private String purchaseDept;

    private String purchaseDeptId;


    /**
     * 采购员
     */
    @TableField(value = "PURCHASE_USER")
    private String purchaseUser;

    /**
     * 采购员ID
     */
    @TableField(value = "PURCHASE_USER_ID")
    private String purchaseUserId;

    /**
     * 订单头备注
     */
    @TableField(value = "REMARK")
    private String remark;


    /**
     * 订单行ID
     */

    private String lineId;



    /**
     * 行表字段展示
     */
    @TableField(exist = false)
    private String materialCode;
    @TableField(exist = false)
    private String materialName;
    @TableField(exist = false)
    private String primaryUnit;
    @TableField(exist = false)
    private String subUnit;
    @TableField(exist = false)
    private String ratio;
    @TableField(exist = false)
    private Double quantity;
    @TableField(exist = false)
    private String poLineNum;
    @TableField(exist = false)
    private Long wxitemId;
    @TableField(exist = false)
    private String wxitemCode;
    @TableField(exist = false)
    private String wxitemName;
    @TableField(exist = false)
    private String price;
    @TableField(exist = false)
    private String poLineId;
    @TableField(exist = false)
    private String poLineIds;

    //List<PurchaseOrderLineEntity> purchaseOrderLines;

}
