package com.tbl.modules.wms2.entity.requestXML;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

/**
 *  FEWMS202  WMS接口-送货单-送货单详情
 * @author 70486
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "deliveryline"
})
@XmlRootElement(name = "deliverylines")
@Getter
@Setter
@ToString
public class FEWMS202_deliverylines {
	/**
	 * 订单行号
	 */
	public List<FEWMS202_deliveryline> deliveryline;

}
