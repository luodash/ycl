package com.tbl.modules.wms2.entity.requestXML;

import com.tbl.common.utils.StringUtils;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * FEWMS203  OA查询紧急物料订单信息
 * @author 70486
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "deliveryNo",//接收单号
        "deliveryLineNo",//接收单行号
        "poNumber",
        "lineNum",
        "itemCode",
        "itemDesc",
        "vendorCode",
        "orderNumber",
        "orderLineNumber",
        "lotsNum",
        "vendorName",
        "salesrepName",
        "needByDate",
        "facName",
        "productionDate",
        "quantity"
})
@XmlRootElement(name = "data")
@Data
public class FEWMS203_data {

    /**
     * 接收单号
     */
    public String deliveryNo;
    /**
     * 接收单行号
     */
    public String deliveryLineNo;
    /**
     * 采购订单号
     */
    private String poNumber;

    /**
     * 采购订单行号
     */
    private String lineNum;


    /**
     * 物料编码
     */
    private String itemCode;

    /**
     * 物料描述
     */
    private String itemDesc;
    /**
     * 供应商编码
     */
    private String vendorCode;
    /**
     * 供应商名称
     */
    private String vendorName;
    /**
     * 销售订单号
     */
    private String orderNumber;

    /**
     * 销售订单行号
     */
    private String orderLineNumber;
    /**
     * 承办单位
     */
    private String salesrepName;
    /**
     * 需求日期
     */
    private String needByDate;
    /**
     * 生产厂
     */
    private String facName;

    /**
     * 生产日期
     */
    private String productionDate;

    /**
     * 批次号
     */
    private String lotsNum;

    /**
     * 数量
     */
    private BigDecimal quantity;

    public String getOrderNumber() {
        return StringUtils.isNotBlank(orderNumber)?orderNumber:"";
    }

    public String getOrderLineNumber() {
        return StringUtils.isNotBlank(orderLineNumber)?orderLineNumber:"";
    }

    public String getFacName() {
        return StringUtils.isNotBlank(facName)?facName:"";
    }

    public String getSalesrepName() {
        return StringUtils.isNotBlank(salesrepName)?salesrepName:"";
    }
}
