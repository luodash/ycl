package com.tbl.modules.wms2.entity.ordermanage.asn;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author DASH
 */
@Data
public class AsnVO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 送货单ID
     */
    private String asnHeaderId;


    private String asnLineId;

    /**
     * 送退货单编号
     */
    private String asnNumber;

    /**
     * 供应商ID
     */
    private Long vendorId;

    /**
     * 供应商名称
     */
    @TableField(value = "VENDOR_NAME")
    private String vendorName;

    /**
     * 供应商编码
     */
    @TableField(value = "VENDOR_CODE")
    private String vendorCode;


    /**
     * 实际送货日期
     */
    @TableField(value = "SHIP_DATE")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date shipDate;

    /**
     * 收货方公司ID
     */
    @TableField(value = "PURCHASE_ORGAN_ID")
    private Long purchaseOrganId;

    @TableField(value = "PURCHASE_ORGAN")
    private String purchaseOrgan;

    @TableField(value = "PURCHASE_DEPT_ID")
    private Long purchaseDeptId;

    /**
     * 收货方组织及使用部门
     */
    @TableField(value = "PURCHASE_DEPT")
    private String purchaseDept;

    /**
     * 状态（ALL_CLOSE:全部关闭, NEW:新建, PART_CANCEL:部分取消, PART_RECEIVE:部分接收, RECEIVE_CLOSE:接收关闭,
     * SEND_OUT:已发货, APPROVING:审批中）
     */
    @TableField(value = "STATUS")
    private String status;

    private String lineStatus;

    /**
     * 创建日期
     */
    @TableField(value = "CREATION_DATE")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date creationDate;

    /**
     * 预计到货日期
     */
    @TableField(value = "EXPECTED_DATE")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date expectedDate;

    @TableField(value = "DATA_SOURCE")
    private String dataSource;

    /**
     * 单据类型(1:送货单 3:退货单)
     */
    @TableField(value = "ASN_TYPE")
    private String asnType;

    /**
     * 快递单号或车牌号
     */
    @TableField(value = "EXPRESS_NUMBER")
    private String expressNumber;

    /**
     * 送货单头行备注
     */
    @TableField(value = "remark")
    private String remark;

    @TableField(value = "STORE_CODE")
    private String storeCode;

    @TableField(value = "STORE_NAME")
    private String storeName;

    @TableField(value = "AREA_CODE")
    private String areaCode;

    @TableField(value = "AREA_NAME")
    private String areaName;

    private String areaId;

    private String poCode;

    private String poLineNum;

    private String materialId;

    private String materialCode;

    private String materialName;

    private String primaryUnit;
    @TableField(exist = false)
    private String boxAmount;

    private String poQuantity;
    @TableField(exist = false)
    private String shipQuantity;
    @TableField(exist = false)
    private String transactionQuantity;
    @TableField(exist = false)
    private String receiveQuantity;
    @TableField(exist = false)
    private String deliverQuantity;
    @TableField(exist = false)
    private String returnReceiveQuantity;
    @TableField(exist = false)
    private String returnVendorQuantity;
    @TableField(exist = false)
    private String cancelQuantity;
    @TableField(exist = false)
    private String closeQuantity;

    private String lotsNum;

    private String lotsNumYd;

    private String receiveDate;

    private String comments;

    /**
     * 库位
     */
    private String locatorCode;

    private String poLineId;


}
