package com.tbl.modules.wms2.entity.operation;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 其他出入库操作
 *
 * @author Sweven
 * @date 2020-06-25
 */
@Getter
@Setter
@ToString
public class OperationOtherPrint implements Serializable {

    private static final long serialVersionUID = -7871827568537789497L;

    private BigDecimal id;
    /** 操作单号 */
    private String otherCode;
    /** 业务类型 */
    private String businessType;
    /** 业务目的 */
    private String businessPurpose;
    private String businessPurposeName;
    /** 厂区编码 */
    private String areaCode;
    /** 仓库（库区）编码 */
    private String storeCode;
    private String storeCodeName;
    /** 生产厂 */
    private String manufacturer;
    private String manufacturerText;
    /** 工序 */
    private String process;
    private String processText;
    /** 机台 */
    private String machine;
    /** 维修加工单 */
    private String maintainCode;
    /** 总金额 */
    private BigDecimal amount;
    /** 领用用途 */
    private String effect;
    private String effectText;
    /** 装车通知单 */
    private String carInfo;
    /** 供应商代码 */
    private String supplierCode;
    /** 供应商名称 */
    private String supplierName;
    /** 备注 */
    private String remark;
    /** 状态 */
    private String state;
    /** 创建人ID */
    private String creatorId;
    /** 创建人 */
    private String creator;
    /** 创建时间(事件处理日) */
    private String createTimeStr;
    private String createTimeString;
    /** 工号 */
    private String workNo;

    /** 其他出入库详情 */
    private transient List<OperationOtherLine> lineList;

}