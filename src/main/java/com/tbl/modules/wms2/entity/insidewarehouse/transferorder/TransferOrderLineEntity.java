package com.tbl.modules.wms2.entity.insidewarehouse.transferorder;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 移位调拨行表
 * @author 70486
 */
@TableName("YCL_TRANSFER_ORDER_LINE")
@Getter
@Setter
@ToString
public class TransferOrderLineEntity implements Serializable {

    /**
     * 主键
     */
    @TableId(value = "LINE_ID",type = IdType.ID_WORKER_STR)
    private String lineId;

    /**
     * 移位调拨主表ID
     */
    @TableField(value = "TRAN_ID")
    private String tranId;

    /**
     * 移出库位
     */
    @TableField(value = "FROM_LOCATOR")
    private String fromLocator;

    /**
     * 批次号
     */
    @TableField(value = "LOTS_NUM")
    private String lotsNum;

    /**
     * 物料编码
     */
    @TableField(value = "MATERIAL_CODE")
    private String materialCode;

    /**
     * 物料名称
     */
    @TableField(value = "MATERIAL_NAME")
    private String materialName;

    /**
     * 主单位
     */
    @TableField(value = "PRIMARY_UNIT")
    private String primaryUnit;


    /**
     * 库位现有数量
     */
    @TableField(value = "QUANTITY")
    private String quantity;

    /**
     * 移出数量
     */
    @TableField(value = "OUT_QUANTITY")
    private String outQuantity;

    /**
     * 状态(0:待处理 1:已上架)
     */
    private String lineState;

    /**
     * 行备注
     */
    private String lineRemark;

    /**
     * 订单行号
     */
    @TableField(value = "LINE_NUM")
    private String lineNum;

    @TableField(value = "OPERATOR")
    private String operator;

    /**
     * 目标库位
     */
    @TableField(value = "TO_LOCATOR")
    private String toLocator;
    /**
     * 更新时间
     */
    @TableField(value = "UPDATE_TIME")
    private Date updateTime;

}
