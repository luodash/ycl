package com.tbl.modules.wms2.entity.requestXML;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

/**
 * FEWMS202  送货单
 * @author 70486
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "deliveryCode",
        "deliveryOrgan",
        "trackingNo",
        "driver",
        "deliveryRemark",
        "deliveryDate",
        "receiveOrgan",
        "receiveDept",
        "receiveUser",
        "phone",
        "purchaseDept",
        "purchaseUser",
        "shipAddress",
        "address",
        "remark",
        "deliverylines"

})
@XmlRootElement(name = "data")
@Getter
@Setter
@ToString
public class FEWMS202_data {

    /**
     * 送货单编号
     */
    public String deliveryCode;
    /**
     * 送货方
     */
    public String deliveryOrgan;
    /**
     * 物流单号或车牌号
     */
    public String trackingNo;
    /**
     * 驾驶员及联系方式
     */
    public String driver;
    /**
     * 送货方备注
     */
    public String deliveryRemark;
    /**
     * 送货日期
     */
    public String deliveryDate;
    /**
     * 收货方实体
     */
    public String receiveOrgan;
    /**
     * 收货方组织及使用部门
     */
    public String receiveDept;
    /**
     * 收货人
     */
    public String receiveUser;

    /**
     * 采购员电话
     */
    private String phone;

    /**
     * 采购部门
     */
    private String purchaseDept;

    /**
     * 采购员
     */
    private String purchaseUser;

    /**
     * 发货地址
     */
    private String shipAddress;

    /**
     * 收货地址
     */
    private String address;

    /**
     * 备注
     */
    private String remark;

    /**
     * 订单行数据
     */
    public List<FEWMS202_deliverylines> deliverylines;

}
