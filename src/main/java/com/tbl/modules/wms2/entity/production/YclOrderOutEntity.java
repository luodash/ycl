package com.tbl.modules.wms2.entity.production;

import io.swagger.models.auth.In;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
@Getter
@Setter
@ToString
public class YclOrderOutEntity implements Serializable {
    /**
     * EBS订单头ID
     */
    private Long orderNumber;
    /**
     * 发运操作人工号
     */
    private String requestCode;
    /**
     * 发运规则
     */
    private String ruleName;
    /**
     * 组织id
     */
    private Long factoryCode;
}
