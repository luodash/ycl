package com.tbl.modules.wms2.entity.baseinfo;

import lombok.Data;

/**
 * 工厂机台
 *
 * @author DASH
 */

@Data
public class CompanyStoreEntity {


    /**
     * 工厂编码
     */
    private String departCode;

    /**
     * 工厂名称
     */
    private String departDesc;

    /**
     * 工序编码
     */
    private String opCode;

    /**
     * 工序名称
     */
    private String opDesc;

    /**
     * 机台编码
     */
    private String machineCode;

    /**
     * 机台名称
     */
    private String machineDesc;

    /**
     * 组织ID
     */
    private String organizationId;


}
