package com.tbl.modules.wms2.entity.baseinfo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

/**
 * 仓库
 *
 * @author DASH
 */
@TableName("YCL_STOREAREA")
@Data
public class WarehouseEntity {

    @TableId(value = "ID",type = IdType.ID_WORKER)
    private Long id;

    /**
     *
     */
    @TableField(value = "AREA_CODE")
    private String areaCode;

    @TableField(value = "AREA_NAME")
    private String areaName;

    @TableField(value = "STOREAREA_CODE")
    private String storeareaCode;

    @TableField(value = "STOREAREA_NAME")
    private String storeareaName;

    @TableField(value = "CREATE_TIME")
    private String createTime;

    @TableField(value = "CREATE_USER")
    private String createUser;

    @TableField(value = "REMARK")
    private String remark;


}
