package com.tbl.modules.wms2.entity.report;


import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 库存
 *
 * @author DASH
 */
@TableName("YCL_INVENTORY")
@Data
public class YclInventoryEntity implements Serializable {

    private static final long serialVersionUID = -1534333999576108561L;

    /**
     * 主键id
     */
    @TableId(value = "ID", type = IdType.ID_WORKER_STR)
    private String id;

    /**
     * 厂区
     */
    @TableField(value = "AREA_CODE")
    private String areaCode;

    /**
     * 厂区
     */
    @TableField(value = "AREA_NAME")
    private String areaName;

    /**
     * 所在仓库
     */
    @TableField(value = "STORE_CODE")
    private String storeCode;

    /**
     * 所在仓库
     */
    @TableField(value = "STORE_NAME")
    private String storeName;

    /**
     * 库位
     */
    @TableField(value = "LOCATOR_CODE")
    private String locatorCode;

    /**
     * 物料编码
     */
    @TableField(value = "MATERIAL_CODE")
    private String materialCode;

    /**
     * 物料名称
     */
    @TableField(value = "MATERIAL_NAME")
    private String materialName;

    /**
     * 批次码（每个成品唯一，贴在外包装上）
     */
    @TableField(value = "LOTS_NUM")
    private String lotsNum;

    /**
     * 单位
     */
    @TableField(value = "PRIMARY_UNIT")
    private String primaryUnit;

    /**
     * 数量
     */
    @TableField(value = "QUALITY")
    private BigDecimal quality;

    /**
     * 创建时间
     */
    @TableField(value = "UPDATE_TIME")
    private Date updateTime;

}
