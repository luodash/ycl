package com.tbl.modules.wms2.entity.requestXML;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *  FEWMS201  WMS接口-采购订单-订单行接口
 * @author 70486
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
		"poLineNum",
		"materialCode",
		"materialName",
		"primaryUnit",
		"ratio",
		"quantity",
		"wxitemCode",
		"wxitemDesc",
		"price"
})
@XmlRootElement(name = "orderLine")
@Getter
@Setter
@ToString
public class FEWMS201_orderline {
	/**
	 * 订单行号
	 */
	public String poLineNum;
	/**
	 * 物料编码
	 */
	public String materialCode;
	/**
	 * 物料名称
	 */
	public String materialName;
	/**
	 * 主单位
	 */
	public String primaryUnit;
	/**
	 * 辅单位
	 */
	public String ratio;
	/**
	 * 订单数量
	 */
	public String quantity;
	/**
	 * 完工物料编码(仅委外加工订单有此数据)
	 */
	public String wxitemCode;
	/**
	 * 完工物料名称(仅委外加工订单有此数据)
	 */
	public String wxitemDesc;
	/**
	 * 物料价格
	 */
	public String price;

}
