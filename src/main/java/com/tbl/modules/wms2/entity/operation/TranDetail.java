package com.tbl.modules.wms2.entity.operation;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 调拨单主表
 */
@TableName("YCL_TRAN_DETAIL")
@Getter
@Setter
@ToString
public class TranDetail implements Serializable {

    @TableId(value = "ID")
    private Long id;

    /**
     * 调拨单编号
     */
    @TableField(value = "TRAN_CODE")
    private String tranCode;

    /**
     * 来源厂区
     */
    @TableField(value = "FORM_AREA")
    private String formArea;

    /**
     * 来源仓库
     */
    @TableField(value = "FORM_HOUSE")
    private String formHouse;

    /**
     * 目的厂区
     */
    @TableField(value = "TO_AREA")
    private String toArea;

    /**
     * 目的仓库
     */
    @TableField(value = "TO_HOUSE")
    private String toHouse;

    /**
     * 备注
     */
    @TableField(value = "REMARK")
    private String remark;

    /**
     * 订单状态(批准, 已取消)
     */
    @TableField(value = "STATE")
    private String state;

    /**
     * 创建人
     */
    @TableField(value = "CREATE_USER")
    private String createUser;

    /**
     * 创建时间
     */
    @TableField(value = "CREATE_TIME")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
}