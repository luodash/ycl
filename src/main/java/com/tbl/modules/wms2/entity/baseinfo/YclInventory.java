package com.tbl.modules.wms2.entity.baseinfo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 库存信息表
 */
@TableName("YCL_INVENTORY")
@Getter
@Setter
@ToString
public class YclInventory implements Serializable {

    @TableId(value = "ID")
    private Long id;

    /**
     * 厂区
     */
    @TableField(value = "AREA_CODE")
    private String areaCode;

    /**
     * 仓库
     */
    @TableField(value = "WAREHOUSE_CODE")
    private String warehouseCode;

    /**
     * 库位
     */
    @TableField(value = "LOCATION_CODE")
    private String locationCode;

    /**
     * 物料号
     */
    @TableField(value = "MATERIAL_CODE")
    private String materialCode;

    /**
     * 批次号
     */
    @TableField(value = "BATCH_CODE")
    private String batchCode;

    /**
     * 单位
     */
    @TableField(value = "UNIT")
    private String unit;

    /**
     * 数量
     */
    @TableField(value = "QUALITY")
    private double quality;

    /**
     * 更新时间
     */
    @TableField(value = "UPDATE_TIME")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    private String storeCode;
    private String locatorCode;
    private String lotsNum;
    private String primaryUnit;
    private String materialName;
    private String areaName;
    private String storeName;

}