package com.tbl.modules.wms2.entity.ordermanage.delivery;

import com.baomidou.mybatisplus.annotations.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @author DASH
 */
@Data
public class DeliveryAddSCDTO {

    /**
     * 供应商名称
     */
    @TableField(value = "VENDOR_NAME")
    private String vendorName;

    /**
     * 供应商编码
     */
    @TableField(value = "VENDOR_CODE")
    private String vendorCode;

    /**
     * 送货单号
     */
    @TableField(value = "ASN_NUMBER")
    private String asnNumber;

    /**
     * 订单创建日期
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    @TableField(value = "CREATE_TIME")
    private Date createTime;

    List<DeliveryEntity> data;


}
