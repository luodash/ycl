package com.tbl.modules.wms2.entity.remotedata;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@TableName("YCL_CRON_JOB")
public class CronJobEntity implements Serializable {
    @TableId(value = "ID")
    private Integer id;

    /**
     * 远端表名
     */
    @TableField(value = "TABLE_NAME")
    private String tableName;

    /**
     * 表同步-基准字段
     */
    @TableField(value = "FIELD_NAME")
    private String fieldName;

    /**
     * 已同步到的最大ID
     */
    @TableField(value = "MAX_ID")
    private Long maxId;

    /**
     * 最后同步时间
     */
    @TableField(value = "LAST_TIME")
    private LocalDateTime lastTime;

    @TableField(value = "REMARK")
    private String remark;

}
