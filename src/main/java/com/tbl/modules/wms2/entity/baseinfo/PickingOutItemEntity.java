package com.tbl.modules.wms2.entity.baseinfo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.beans.BeanInfo;
import java.io.Serializable;
import java.math.BigDecimal;

@Setter
@Getter
@ToString
public class PickingOutItemEntity implements Serializable {
    private String locatorCode;
    private String materialCode;
    private String materialName;
    private String machineCode;
    private String lotsNum;
    private BigDecimal quantity;
}
