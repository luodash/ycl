package com.tbl.modules.wms2.entity.operation;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 其他出入库子表
 *
 * @author Sweven
 * @date 2020-06-25
 */
@Getter
@Setter
@ToString
@TableName(value = "YCL_OPERATION_OTHER_LINE")
public class OperationOtherLine implements Serializable {

    private static final long serialVersionUID = -1662877610976330607L;

    @TableId(value = "ID", type = IdType.INPUT)
    private BigDecimal id;
    /** 操作单号 */
    @TableField(value = "OTHER_CODE")
    private String otherCode;
    /** 行号 */
    @TableField(value = "LINE_CODE")
    private String lineCode;
    /** 厂区编码 */
    @TableField(value = "AREA_CODE")
    private String areaCode;
    /** 仓库（子库）编码 */
    @TableField(value = "STORE_CODE")
    private String storeCode;
    /** 库位 */
    @TableField(value = "LOCATOR_CODE")
    private String locatorCode;
    /** 物料编码 */
    @TableField(value = "MATERIAL_CODE")
    private String materialCode;
    /** 物料名称 */
    @TableField(value = "MATERIAL_NAME")
    private String materialName;
    /** 批次号 */
    @TableField(value = "LOTS_NUM")
    private String lotsNum;
    /** 单位 */
    @TableField(value = "PRIMARY_UNIT")
    private String primaryUnit;
    /** 单价 */
    @TableField(value = "PRICE")
    private BigDecimal price;
    /** 备注 */
    @TableField(value = "REMARK")
    private String remark;
    /** 状态（预留） */
    @TableField(value = "STATE")
    private String state;
    /** 数量 */
    @TableField(value = "QUANTITY")
    private BigDecimal quantity;
    /** 订单数量 */
    @TableField(value = "ORDER_QUANTITY")
    private BigDecimal orderQuantity;

}