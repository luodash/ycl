package com.tbl.modules.wms2.entity.purchasein;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

@Setter
@Getter
@ToString
public class PurchaseOrderSaveEntity implements Serializable {
    /**
     * 采购订单行ID
     */
    private Long poLineId;
    /**
     * 入库数量
     */
    private Double quantity;
    /**
     * 子库
     */
    private String subCode;
    /**
     * 货位ID
     */
    private Long loctorId;
    /**
     * 入库日期
     */
    //@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Timestamp transDate;
    /**
     * 供应商批次
     */
    private String vendorLot;

    /**
     * 唯一主键ID
     */
    private String sourceId;

    private String retCode;

    private String retMess;

    private Long rcvLineId;
}
