package com.tbl.modules.wms2.entity.workorder;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.tbl.modules.wms2.entity.purchasein.PurchaseOrderLineEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * 委外加工主表
 * @author 70486
 */
//@TableName("YCL_WORK_ORDER")
@Getter
@Setter
@ToString
public class WorkOrderEntity implements Serializable {

//    @TableId(value = "WO_ID")
//    private Long woId;
//
//    /**
//     * 工单编号
//     */
//    @TableField(value = "WO_CODE")
//    private String woCode;
//
//    /**
//     * 工单日期
//     */
//    @TableField(value = "WO_DATE")
//    @DateTimeFormat(pattern = "yyyy-MM-dd")
//    private Date woDate;
//
//    /**
//     * 查询条件-开始时间
//     */
//    @TableField(exist = false)
//    private String woDateStart;
//
//    /**
//     * 查询条件-结束时间
//     */
//    @TableField(exist = false)
//    private String woDateEnd;
//
//    /**
//     * 供应商编码
//     */
//    @TableField(value = "SUPPLIER_CODE")
//    private String supplierCode;
//
//    /**
//     * 供应商名称
//     */
//    @TableField(value = "SUPPLIER_NAME")
//    private String supplierName;
//
//    /**
//     * 采购组织
//     */
//    @TableField(value = "PURCHASE_ORGAN")
//    private String purchaseOrgan;
//
//    /**
//     * 采购部门
//     */
//    @TableField(value = "PURCHASE_DEPT")
//    private String purchaseDept;
//
//    /**
//     * 采购员
//     */
//    @TableField(value = "PURCHASE_USER")
//    private String purchaseUser;
//
//    /**
//     * 备注
//     */
//    @TableField(value = "REMARK")
//    private String remark;
//
//    /**
//     * 订单状态(批准, 已取消)
//     */
//    @TableField(value = "STATE")
//    private String state;
//
//    /**
//     * 创建人
//     */
//    @TableField(value = "CREATE_USER")
//    private String createUser;
//
//    /**
//     * 创建时间
//     */
//    @TableField(value = "CREATE_TIME")
//    @DateTimeFormat(pattern = "yyyy-MM-dd HH24:mm:ss")
//    private LocalDateTime createTime;
//
//    /**
//     * 委外加工头ID(视图cux_wx_po_v主键ID)
//     */
//    @TableField(value = "PO_ID")
//    private String poId;
//
//
//
//    /**
//     * 行表字段展示
//     */
//    @TableField(exist = false)
//    private String woLineId;
//    @TableField(exist = false)
//    private String poCode;
//    @TableField(exist = false)
//    private String lineNum;
//    @TableField(exist = false)
//    private Long materialId;
//    @TableField(exist = false)
//    private String materialCode;
//    @TableField(exist = false)
//    private String materialName;
//    @TableField(exist = false)
//    private String primaryUnit;
//    @TableField(exist = false)
//    private String quantity;
//    @TableField(exist = false)
//    private Long wxitemId;
//    @TableField(exist = false)
//    private String wxitemCode;
//    @TableField(exist = false)
//    private String wxitemName;
//
//    @TableField(exist = false)
//    private String woLineIds;
//
//    List<WorkOrderLineEntity> workOrderLines;


    /*
     *类型  保存："save"      提交："commit"
     */
    private String type;
    /**委外加工头ID*/
    private Long poId;
    private String rcvId;/**接收单号*/
    private String wipComId;/**工单号*/
    private String organizationId;/**库存组织ID*/
    private String organizationCode;/**库存组织编码*/
    private String organizationName;/**库存组织*/
    private String completeItemId;/**完工物料id*/
    private String completeItemNum;/**完工物料编码*/
    private String completeItemDesc;/**完工物料名称*/
    private String completeSub;/**完工子库存*/
    private String completeSubDesc;/**完工子库存描述*/
    private String completeLocatorId;/**完工货位Id*/
    private String completeLocatorCode;/**完工货位*/
    private String completeLocatorDesc;/**完工货位描述*/
    private String completeQty;/**完工数量*/
    private String transactionDate;/**完工时间*/
    private String ftComponentSub;/**发料子库存*/
    private String ftSubinventoryDesc;/**料子库存描述*/
    private String ftComponentLocatorId;/**发料货位ID*/
    private String ftLocatorCode;/**发料货位编码*/
    private String ftLocatorDesc;/**发料货位描述*/
    private String ftItemNum;/**消耗物料编码*/
    private String ftItemDesc;/**消耗物料名称*/
    private String ftcompletedQty;/**发料数量*/
    private String fyItemId;/**加工费物料ID*/
    private String fyItemNum;/**加工费物料编码*/
    private String fyComponentItemId;/**委外加工费物ID*/
    private String fyItemDesc;/**委外加工费物名称*/
    private String fyComponentQty;/**加工费发料数量*/
    private String fySub;/**加工费发料子库存*/
    private String fySubDesc;/**加工费发料子库存描述*/
    private String fyComponentLocatorId;/**加工费发料货位ID*/
    private String fyLocatorCode;/**加工费发料编码*/
    private String fyLocatorDesc;/**加工费发料货位*/
    private String attribute1;/**操作状态*/
    private String processFlag;/**状态*/

    private Long wipEntityId;/**工单ID*/
    private String vendorLot;/**供应商批次*/
    private String vendorFtLot;/**消耗物料供应商批次*/



}
