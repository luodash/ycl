package com.tbl.modules.wms2.entity.ordermanage.asn;

import lombok.Data;

import java.io.Serializable;

/**
 * @author DASH
 */
@Data
public class AsnCorrectDTO implements Serializable {

    private static final long serialVersionUID = -2268538800498160125L;

    /**
     * 送货单ID
     */
    private String asnHeaderId;

    /**
     * 送货单编号
     */
    private String asnNumber;

    /**
     * 供应商ID
     */
    private Long vendorId;

    /**
     * 供应商名称
     */
    private String vendorName;

    /**
     * 供应商编码
     */
    private String vendorCode;

    /**
     * 收货方公司ID
     */
    private Long purchaseOrganId;

    private String purchaseOrgan;

    private Long purchaseDeptId;

    /**
     * 收货方组织及使用部门
     */
    private String purchaseDept;

    private String storeCode;

    private String storeName;

    private String areaCode;

    private String areaName;

    private String poCode;

    private String poLineNum;

    private String materialCode;

    private String materialName;

    private String boxAmount;

    private String poQuantity;

    private String shipQuantity;

    private String transactionQuantity;

    private String receiveQuantity;

    private String deliverQuantity;

    private String returnReceiveQuantity;

    private String returnVendorQuantity;

    private String cancelQuantity;

    private String closeQuantity;

    private String lotsNum;


}
