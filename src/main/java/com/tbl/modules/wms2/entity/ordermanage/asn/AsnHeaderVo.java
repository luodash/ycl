package com.tbl.modules.wms2.entity.ordermanage.asn;

import com.baomidou.mybatisplus.annotations.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 采购订单VO
 *
 * @author DASH
 */
@Data
public class AsnHeaderVo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 送货单ID
     */
    private String asnHeaderId;

    /**
     * 送退货单编号
     */
    @TableField(value = "ASN_NUMBER")
    private String asnNumber;

    /**
     * 单据类型(1:送货单 3:退货单)
     */
    @TableField(value = "ASN_TYPE")
    private String asnType;

    private String asnTypeName;

    /**
     * 供应商ID
     */
    private Long vendorId;

    /**
     * 供应商名称
     */
    @TableField(value = "VENDOR_NAME")
    private String vendorName;

    /**
     * 供应商编码
     */
    @TableField(value = "VENDOR_CODE")
    private String vendorCode;


    /**
     * 收货方公司ID
     */
    @TableField(value = "PURCHASE_ORGAN_ID")
    private Long purchaseOrganId;

    @TableField(value = "PURCHASE_ORGAN")
    private String purchaseOrgan;

    @TableField(value = "PURCHASE_DEPT_ID")
    private Long purchaseDeptId;

    /**
     * 收货方组织及使用部门
     */
    @TableField(value = "PURCHASE_DEPT")
    private String purchaseDept;


    /**
     * 状态（ALL_CLOSE:全部关闭, NEW:新建, PART_CANCEL:部分取消, PART_RECEIVE:部分接收, RECEIVE_CLOSE:接收关闭,
     * SEND_OUT:已发货, APPROVING:审批中）
     */
    @TableField(value = "STATUS")
    private String status;

    /**
     * 创建日期
     */
    @TableField(value = "CREATION_DATE")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date creationDate;

    /**
     * 送货单头行备注
     */
    @TableField(value = "remark")
    private String remark;

    @TableField(value = "STORE_CODE")
    private String storeCode;

    @TableField(value = "STORE_NAME")
    private String storeName;

    @TableField(value = "AREA_CODE")
    private String areaCode;

    @TableField(value = "AREA_NAME")
    private String areaName;
    @TableField(value = "AREA_ID")
    private String areaId;



}
