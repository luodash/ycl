package com.tbl.modules.wms2.entity.requestXML;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * FEWMS204  OA反馈质检信息行
 * @author 70486
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "deliveryNo",//接收单号
        "deliveryLineNo",//接收单行号
        "isPass",
        "oaState"//向OA发起审批阶段状态
})
@XmlRootElement(name = "qcline")
@Getter
@Setter
@ToString
public class FEWMS204_oa_qcline {

    /**
     * 接收单号
     */
    public String deliveryNo;
    /**
     * 订单行号
     */
    public String deliveryLineNo;

    /**
     * 审批结果
     */
    public String isPass;
    /**
     * 向OA发起审批的阶段(1:已发起 2:已撤回 3:已结束)
     */
    public String oaState;


}
