package com.tbl.modules.wms2.entity.ycloutstorage;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 原材料出库信息主表
 * @author 70486
 */
@TableName("YCL_OUT_STORAGE")
@Getter
@Setter
@ToString
public class YclOutStorage implements Serializable {

    /**
     * 主键
     */
    @TableId(value = "ID")
    private Long id;

    /**
     * 出库单号
     */
    @TableField(value = "OUT_STORAGE_CODE")
    private String outStorageCode;

    /**
     * 出库类别
     */
    @TableField(value = "OUT_TYPE")
    private String outType;

    /**
     * 关联代码
     */
    @TableField(value = "RELATED_CODE")
    private String relatedCode;

    /**
     * 申请部门
     */
    @TableField(value = "APPLY_DEPT")
    private String applyDept;

    /**
     * 申请人
     */
    @TableField(value = "APPLY_USER")
    private String applyUser;

    /**
     * 电话
     */
    @TableField(value = "PHONE")
    private String phone;

    /**
     * 备注
     */
    @TableField(value = "REMARK")
    private String remark;

    /**
     * 状态（1，新增，2，出库中，3，完成）
     */
    @TableField(value = "STATE")
    private String state;

    /**
     * 操作人
     */
    @TableField(value = "CREATE_USER")
    private String createUser;

    /**
     * 操作时间
     */
    @TableField(value = "CREATE_TIME")
    private String createTime;

}
