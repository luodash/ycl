package com.tbl.modules.wms2.entity.report;


import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 更新库存
 *
 * @author DASH
 */
@Data
public class YclInventoryQueryDTO implements Serializable {


    private static final long serialVersionUID = -8035119403569434580L;

    /**
     * 厂区
     */
    private String areaCode;

    /**
     * 所在仓库
     */
    private String storeCode;

    /**
     * 库位
     */
    private String locatorCode;

    /**
     * 物料编码
     */
    private String materialCode;


    /**
     * 批次码（每个成品唯一，贴在外包装上）
     */
    private String lotsNum;


    /**
     * 出入数量
     */
    private BigDecimal amount;

    /**
     * 出：0 入：1
     */
    private String inout;


}
