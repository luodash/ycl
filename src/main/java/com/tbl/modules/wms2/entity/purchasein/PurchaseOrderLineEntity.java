package com.tbl.modules.wms2.entity.purchasein;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 采购订单-行表(原材料、委外加工)
 * @author 70486
 */
@TableName("YCL_PURCHASE_ORDER_LINE")
@Getter
@Setter
@ToString
public class PurchaseOrderLineEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "PO_LINE_ID",type = IdType.ID_WORKER_STR)
    private String poLineId;

    /**
     * 物料编码
     */
    @TableField(value = "MATERIAL_CODE")
    private String materialCode;

    /**
     * 物料名称
     */
    @TableField(value = "MATERIAL_NAME")
    private String materialName;

    /**
     * 主单位
     */
    @TableField(value = "PRIMARY_UNIT")
    private String primaryUnit;

    /**
     * 辅单位
     */
    @TableField(value = "SUB_UNIT")
    private String subUnit;

    /**
     * 主辅比例
     */
    @TableField(value = "RATIO")
    private String ratio;

    /**
     * 订单数量
     */
    @TableField(value = "QUANTITY")
    private BigDecimal quantity;

    /**
     * 订单行号
     */
    @TableField(value = "PO_LINE_NUM")
    private String poLineNum;

    /**
     * 采购订单主键ID
     */
    @TableField(value = "PO_HEADER_ID")
    private String poHeaderId;

    /**
     * 完工物料ID
     */
    @TableField(value = "WXITEM_ID")
    private String wxitemId;

    /**
     * 完工物料编码
     */
    @TableField(value = "WXITEM_CODE")
    private  String wxitemCode;

    /**
     * 完工物料名称
     */
    @TableField(value = "WXITEM_NAME")
    private String wxitemName;

    /**
     * 采购价格
     */
    @TableField(value = "PRICE")
    private String price;

    /**
     * 是否过期（'0':否  '1':是）
     */
    private String expired;


}
