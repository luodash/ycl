package com.tbl.modules.wms2.entity.operation;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 其他出入库操作
 *
 * @author Sweven
 * @date 2020-06-25
 */
@Getter
@Setter
@ToString
@TableName(value = "YCL_OPERATION_OTHER")
public class OperationOtherVO implements Serializable {

    private static final long serialVersionUID = 2853458986339980388L;

    /** 操作单号 */
    @TableField(value = "OTHER_CODE")
    private String otherCode;
    /** 业务类型 */
    @TableField(value = "BUSINESS_TYPE")
    private String businessType;
    /** 业务目的 */
    @TableField(value = "BUSINESS_PURPOSE")
    private String businessPurpose;
    private String businessPurposeName;
    /** 厂区编码 */
    @TableField(value = "AREA_CODE")
    private String areaCode;
    /** 仓库（库区）编码 */
    @TableField(value = "STORE_CODE")
    private String storeCode;
    /** 生产厂 */
    @TableField(value = "MANUFACTURER")
    private String manufacturer;
    /** 工序 */
    @TableField(value = "PROCESS")
    private String process;
    /** 机台 */
    @TableField(value = "MACHINE")
    private String machine;
    /** 维修加工单 */
    @TableField(value = "MAINTAIN_CODE")
    private String maintainCode;
    /** 总金额 */
    @TableField(value = "AMOUNT")
    private BigDecimal amount;
    /** 领用用途 */
    @TableField(value = "EFFECT")
    private String effect;
    /** 装车通知单 */
    @TableField(value = "CAR_INFO")
    private String carInfo;
    /** 供应商代码 */
    @TableField(value = "SUPPLIER_CODE")
    private String supplierCode;
    /** 供应商名称 */
    @TableField(value = "SUPPLIER_NAME")
    private String supplierName;
    /** 备注 */
    @TableField(value = "REMARK")
    private String remark;
    /** 状态 */
    @TableField(value = "STATE")
    private String state;
    /** 创建人ID */
    @TableField(value = "CREATOR_ID")
    private String creatorId;
    /** 创建人 */
    @TableField(value = "CREATOR")
    private String creator;
    /** 创建时间(事件处理日) */
    @TableField(value = "CREATE_TIME")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /** 物料编码 */
    @TableField(value = "MATERIAL_CODE")
    private String materialCode;
    /** 物料名称 */
    @TableField(value = "MATERIAL_NAME")
    private String materialName;

    /** 行号 */
    @TableField(value = "LINE_CODE")
    private String lineCode;
    /** 库位 */
    @TableField(value = "LOCATOR_CODE")
    private String locatorCode;
    /** 批次号 */
    @TableField(value = "LOTS_NUM")
    private String lotsNum;
    /** 单位 */
    @TableField(value = "PRIMARY_UNIT")
    private String primaryUnit;
    /** 单价 */
    @TableField(value = "PRICE")
    private BigDecimal price;
    /** 数量 */
    @TableField(value = "QUANTITY")
    private BigDecimal quantity;
    /** 订单数量 */
    @TableField(value = "ORDER_QUANTITY")
    private BigDecimal orderQuantity;

    /** 仓库名称 */
    private String storeCodeName;
    /** 仓库名称 */
    private String locatorCodeName;

}