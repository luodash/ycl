package com.tbl.modules.wms2.entity.insidewarehouse.check;

import lombok.Data;

import java.io.Serializable;


/**
 * @author DASH
 */
@Data
public class YclCheckQueryDTO implements Serializable {

    private static final long serialVersionUID = 776997324606738577L;

    private String areaCode;
    private String storeCode;
    private String kssj;
    private String jssj;


}
