package com.tbl.modules.wms2.entity.operation;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * 出入库流水
 *
 * @author DASH
 */
@TableName("YCL_IN_OUT_FLOW")
@Data
public class YclInOutFlowEntity implements Serializable {

    private static final long serialVersionUID = 6882477889634359911L;

    /**
     * 主键id
     */
    @TableId(value = "ID", type = IdType.ID_WORKER_STR)
    private String id;

    /**
     * 单据编号
     */
    @TableField(value = "BUSINESS_CODE")
    private String businessCode;

    /**
     * 厂区
     */
    @TableField(value = "AREA_CODE")
    private String areaCode;

    @TableField(value = "AREA_NAME")
    private String areaName;

    @TableField(value = "AREA_ID")
    private String areaId;

    /**
     * 仓库
     */
    @TableField(value = "STORE_CODE")
    private String storeCode;
    @TableField(value = "STORE_NAME")
    private String storeName;

    /**
     * 库位
     */
    @TableField(value = "LOCATOR_CODE")
    private String locatorCode;

    /**
     * 物料编码
     */
    @TableField(value = "MATERIAL_CODE")
    private String materialCode;

    /**
     * 物料名称
     */
    @TableField(value = "MATERIAL_NAME")
    private String materialName;

    /**
     * 批次号
     */
    @TableField(value = "LOTS_NUM")
    private String lotsNum;

    /**
     * 单位
     */
    @TableField(value = "PRIMARY_UNIT")
    private String primaryUnit;

    /**
     * 实际收退货数量
     */
    @TableField(value = "QUANTITY")
    private BigDecimal quantity;

    /**
     * 上下架数量
     */
    @TableField(value = "UPDOWN_QUANTITY")
    private BigDecimal updownQuantity;

    /**
     * 状态：0未上架，1审核通过，2未通过，9已上架
     */
    @TableField(value = "STATE")
    private String state;

    /**
     * 创建人
     */
    @TableField(value = "CREATE_USER")
    private String createUser;

    /**
     * 创建时间
     */
    @TableField(value = "CREATE_TIME")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 单据类型(1:其他出库、2：其他入库)
     */
    @TableField(value = "BUSINESS_TYPE")
    private String businessType;

    /**
     * 出入库类型(1:入库, 0:出库)
     */
    @TableField(value = "INOUT_TYPE")
    private String inoutType;

    /**
     * 业务目的
     */
    @TableField(value = "BUSINESS_PURPOSE")
    private String businessPurpose;

    /**
     * 用途原因
     */
    @TableField(value = "CAUSE")
    private String cause;

    /**
     * 供应商编码
     */
    @TableField(value = "SUPPLIER_CODE")
    private String supplierCode;

    /**
     * 供应商名称
     */
    @TableField(value = "SUPPLIER_NAME")
    private String supplierName;

    /**
     * 件数
     */
    @TableField(value = "BOX_AMOUNT")
    private String boxAmount;

    /**
     * 质检状态(0:待发送质检 1:待质检 2:已质检)
     * 质检判定结果-三个来源最终得出的(0待确认 1通过 2未通过)
     * 判定逻辑：三种情况，一是OA走紧急审批流程，OA返回审批同意，二是质检员人工确认后点同意入库，
     * 三是取EBS检测结果（试样状态合格直接入库，否则还需要判断最终处理结果是否让步接收或扣款接收）
     */
    @TableField(value = "QC_STATE")
    private String qcState;

    /**
     * 判断是否已质检: 0-未质检 1-已质检
     */
    @TableField(value = "IS_QC")
    private String isQc;

    @TableField(value = "INOUT_CODE")
    private String inoutCode;

    @TableField(value = "INOUT_LINE_NUM")
    private Integer inoutLineNum;

    /**
     * 是否入库存，默认入库存： 0-否 1-是
     */
    @TableField(value = "IS_IN")
    private String isIn;

    //试样结果
    @TableField(exist = false)
    private String qcSample;
    //协商放行
    @TableField(exist = false)
    private String qcConsult;
    //wms人工确认
    @TableField(exist = false)
    private String qcWms;
    //OA紧急流程
    @TableField(exist = false)
    private String qcEmergency;

    /**
     * 采购订单号
     */
    @TableField(exist = false)
    private String poCode;
    /**
     * 采购订单行号
     */
    @TableField(exist = false)
    private String poLineNum;

    @TableField(exist = false)
    private String asnLineId;

    /**
     * 远东批次号
     */
    @TableField(value = "LOTS_NUM_YD")
    private String lotsNumYd;

    /**
     * 更新之前库存数
     */
    @TableField(value = "BEFORE_IN")
    private BigDecimal beforeIn;

}