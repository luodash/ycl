package com.tbl.modules.wms2.entity.production;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * 领料出库
 * @author
 */
@TableName("YCL_MATERIAL_OUT")
@Getter
@Setter
@ToString
public class YclMaterialOutEntity implements Serializable {

    /**
     * 主键id
     */
    @TableId(value = "ID", type = IdType.ID_WORKER_STR)
    private String ID;

    /**
     * 领料申请单行号id
     */
    @TableField(value = "LINE_ID")
    private String lineId;

    /**
     * 出库单号
     */
    @TableField(value = "OUT_CODE")
    private String outCode;

    /**
     * 厂区编码
     */
    @TableField(value = "AREA_CODE")
    private String areaCode;

    /**
     * 仓库（库区）编码
     */
    @TableField(value = "STORE_CODE")
    private String storeCode;

    /**
     * 库位编码
     */
    @TableField(value = "LOCATOR_CODE")
    private String locatorCode;

    /**
     * 领料申请单号
     */
    @TableField(value = "APPLY_CODE")
    private String applyCode;

    /**
     * 申请单行号
     */
    @TableField(value = "LINE_CODE")
    private String lineCode;

    /**
     * 物料编码
     */
    @TableField(value = "MATERIAL_CODE")
    private String materialCode;

    /**
     * 物料名称
     */
    @TableField(value = "MATERIAL_NAME")
    private String materialName;

    /**
     * 主单位
     */
    @TableField(value = "PRIMARY_UNIT")
    private String primaryUnit;

    /**
     * 批次号
     */
    @TableField(value = "LOTS_NUM")
    private String lotsNum;

    /**
     * 班次
     */
    @TableField(value = "SHIFT")
    private String shift;

    /**
     * 机台
     */
    @TableField(value = "DEVICE")
    private String device;

    /**
     * 领料数量
     */
    @TableField(value = "QUANTITY")
    private String quantity;

    /**
     * 配送数量
     */
    @TableField(value = "OUT_QUANTITY")
    private String outQuantity;

    /**
     * 件数
     */
    @TableField(value = "BOX_AMOUNT")
    private String boxAmount;

    /**
     * 状态
     */
    @TableField(value = "STATE")
    private String state;

    /**
     * 创建人
     */
    @TableField(value = "CREATE_USER")
    private String createUser;

    /**
     * 创建时间(系统时间)
     */
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "CREATE_TIME")
    private Date createTime;

    @TableField(exist = false)
    private String typeName;

    @TableField(exist = false)
    private String storeName;

}
