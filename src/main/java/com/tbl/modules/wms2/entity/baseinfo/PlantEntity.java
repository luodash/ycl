package com.tbl.modules.wms2.entity.baseinfo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;
import org.springframework.data.annotation.Id;

/**
 * 工厂机台
 *
 * @author DASH
 */
@TableName("apps.wip_schedule_groups@wms_to_ebs.fegroup.com.cn")
@Data
public class PlantEntity {
    /**
     * 生产厂ID
     */
    @TableId(value = "SCHEDULE_GROUP_ID")
    private String id;

    /**
     * 生产厂编码
     */
    @TableField(value = "SCHEDULE_GROUP_NAME")
    private String code;

    /**
     * 生产厂名称
     */
    @TableField(value = "DESCRIPTION")
    private String description;

    /**
     * 组织ID
     */
    @TableField(value = "ORGANIZATION_ID")
    private String organizeId;

}
