package com.tbl.modules.wms2.entity.otherStorage;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.util.Date;

/**
 * 出厂单
 *
 * @author DASH
 */
@TableName("YCL_LEAVE_LINE")
@Data
public class LeaveLineEntity {

    @TableId(value = "LINE_ID", type = IdType.ID_WORKER_STR)
    private String lineId;
    /**
     * 出厂单号
     */
    @TableField(value = "OUT_CODE")
    private String outCode;
    /**
     * 仓库编码
     */
    @TableField(value = "STORE_CODE")
    private String storeCode;

    /**
     * 批次号
     */
    @TableField(value = "BATCH")
    private String batch;

    /**
     * 物料编码
     */
    @TableField(value = "MATERIAL_CODE")
    private String materialCode;

    /**
     * 物料名称
     */
    @TableField(value = "MATERIAL_NAME")
    private String materialName;

    /**
     * 单位
     */
    @TableField(value = "UNIT")
    private String unit;

    /**
     * 数量
     */
    @TableField(value = "QUALITY")
    private String quality;

    /**
     * 单价
     */
    @TableField(value = "PRICE")
    private String price;

    /**
     * 导入来源
     */
    @TableField(value = "ORDER_CODE")
    private String orderCode;


    @TableField(value = "COMMENTS")
    private String comments;


}
