package com.tbl.modules.wms2.entity.ordermanage.asn;

import com.baomidou.mybatisplus.annotations.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 采购订单VO
 *
 * @author DASH
 */
@Data
public class AsnHeaderQueryDTO implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 送退货单编号
     */
    private String asnNumber;

    /**
     * 送退货单编号
     */
    private String asnHeaderId;

    /**
     * 单据类型(1:送货单 3:退货单)
     */
    private String asnType;

    private String asnTypeName;

    /**
     * 供应商ID
     */
    private Long vendorId;

    /**
     * 供应商名称
     */
    private String vendorName;

    /**
     * 供应商编码
     */
    private String vendorCode;


}
