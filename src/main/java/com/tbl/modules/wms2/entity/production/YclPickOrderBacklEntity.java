package com.tbl.modules.wms2.entity.production;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 领料申请单回退关联表
 * @author
 */
@TableName("YCL_PICK_ORDER_BACK")
@Getter
@Setter
@ToString
public class YclPickOrderBacklEntity implements Serializable {

    /**
     * 领料单号
     */
    @TableField(value = "LINE_ID")
    private String lineId;

    /**
     * 订单日期
     */
    @TableField(value = "INSERT_DATE")
    @DateTimeFormat(pattern = "yyyy-MM-hh HH:mm:ss")
    private LocalDateTime insertDate;
}
