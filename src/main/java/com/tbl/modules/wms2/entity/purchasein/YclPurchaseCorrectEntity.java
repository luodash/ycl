package com.tbl.modules.wms2.entity.purchasein;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 采购更正
 *
 * @author DASH
 */
@TableName("YCL_PURCHASE_CORRECT")
@Data
public class YclPurchaseCorrectEntity {

    /**
     * 主键id
     */
    @TableId(value = "ID", type = IdType.ID_WORKER_STR)
    private String id;

    /**
     * 更正单号
     */
    @TableField(value = "CORRECT_CODE")
    private String correctCode;

    /**
     * 订单号
     */
    @TableField(value = "PO_CODE")
    private String poCode;

    /**
     * 订单行号
     */
    @TableField(value = "PO_LINE_NUM")
    private String poLineNum;

    /**
     * 物料编号
     */
    @TableField(value = "MATERIAL_CODE")
    private String materialCode;

    /**
     * 物料名称
     */
    @TableField(value = "MATERIAL_NAME")
    private String materialName;

    /**
     * 批次号
     */
    @TableField(value = "LOTS_NUM")
    private String lotsNum;

    /**
     * 订单日期
     */
    @TableField(value = "ORDER_TIME")
    @DateTimeFormat(pattern = "yyyy-MM-hh")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    private Date orderTime;

    /**
     * 供应商ID
     */
    @TableField(value = "SUPPLIER_ID")
    private String supplierId;

    /**
     * 供应商编号
     */
    @TableField(value = "SUPPLIER_CODE")
    private String supplierCode;

    /**
     * 供应商名称
     */
    @TableField(value = "SUPPLIER_NAME")
    private String supplierName;

    /**
     * 采购组织
     */
    @TableField(value = "PURCHASE_ORGAN")
    private String purchaseOrgan;

    /**
     * 采购部门
     */
    @TableField(value = "PURCHASE_DEPT")
    private String purchaseDept;

    /**
     * 采购人
     */
    @TableField(value = "PURCHASE_USER")
    private String purchaseUser;


    @TableField(value = "CREATE_TIME")
    @DateTimeFormat(pattern = "yyyy-MM-hh")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    private Date createTime;

}
