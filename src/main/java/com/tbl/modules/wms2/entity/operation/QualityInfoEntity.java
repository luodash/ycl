package com.tbl.modules.wms2.entity.operation;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 质检信息
 * @author 70486
 */
@TableName("YCL_QUALITY_INFO")
@Getter
@Setter
@ToString
public class QualityInfoEntity implements Serializable {

    @TableId(value = "ID",type = IdType.ID_WORKER_STR)
    private String id;

    /**
     * 采购单号
     */
    @TableField(value = "PO_CODE")
    private String poCode;

    /**
     * 采购订单行号
     */
    @TableField(value = "PO_LINE_NUM")
    private String poLineNum;


    @TableField(value = "IN_OUT_FLOW_ID")
    private String inOutFlowId;

    /**
     * 最终处理结果(10:让步接收 20:扣款接收 30:退货 40:重抽 50:返修)
     */
    @TableField(value = "CONSULT")
    private String consult;

    /**
     * 质检结果  ID
     */
    @TableField(value = "SAMPLE")
    private String sample;

    /**
     * 报验单号(EBS生成的,对wms无用)
     */
    @TableField(value = "SAMPLE_NO")
    private String sampleNo;

    /**
     * 质检员通过WMS确认通过(0:质检中 1:放行 2:合格 3:不合格)
     */
    @TableField(value = "PASS_WMS")
    private String passWms;

    /**
     * OA紧急放行(1:通过 2:未通过)
     */
    @TableField(value = "PASS_OA")
    private String passOa;

    /**
     * 最后更新时间
     */
    @TableField(value = "UPDATE_TIME")
    private String updateTime;

    /**
     * 质检人员工号
     */
    @TableField(value = "USERNAME")
    private String username;
    /**
     * 质检人员工号
     */
    @TableField(value = "NAME")
    private String name;

    /**
     * 是否入库(0否  1是)
     */
    @TableField(value = "INTO_INVENTORY")
    private String intoInventory;

    /**
     * 接收表ID
     */
    @TableField(value = "DELIVERY_ID")
    private String deliveryId;

    /**
     * 接收单号
     */
    @TableField(value = "DELIVERY_NO")
    private String deliveryNo;
    /**
     * 接收行号
     */
    @TableField(value = "DELIVERY_LINE_NO")
    private String deliveryLineNo;

    /**
     * 发送待质检(0:未发送  1:已发送)
     */
    @TableField(value = "TOBE_QC")
    private String tobeQc;

    /**
     * 允许入库来源(1:EBS 2:OA 3:WMS)
     */
    @TableField(value = "INTO_INVENTORY_BY")
    private String intoInventoryBy;

    @TableField(value = "MATERIAL_CODE")
    private String materialCode;

    @TableField(value = "LOTS_NUM")
    private String lotsNum;

    /**
     * 向OA发起审批的阶段((0:缺省 1:已发起 2:已撤回 3:已结束))
     */
    @TableField(value = "OA_STATE")
    private String oaState;

    @TableField(exist = false)
    private String oaStates;


    @TableField(exist = false)
    private String asnNumber;
    @TableField(exist = false)
    private String vendorName;
    @TableField(exist = false)
    private String materialName;
    @TableField(exist = false)
    private String receiveQuantity;
    @TableField(exist = false)
    private String primaryUnit;
    @TableField(exist = false)
    private String createTime;
    @TableField(exist = false)
    private String createUser;
    @TableField(exist = false)
    private String storeName;
    @TableField(exist = false)
    private String boxAmount;
    @TableField(exist = false)
    private String locator;
    @TableField(exist = false)
    private String createTimeStart;
    @TableField(exist = false)
    private String createTimeEnd;


    @TableField(exist = false)
    private String isPass;


}
