package com.tbl.modules.wms2.entity.requestXML;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

/**
 * FEWMS204  OA反馈质检信息行
 * @author 70486
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "qcLine"
})
@XmlRootElement(name = "qclines")
@Getter
@Setter
@ToString
public class FEWMS204_oa_qclines {

    List<FEWMS204_oa_qcline> qcLine;

}
