package com.tbl.modules.wms2.entity.production;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.tbl.modules.wms2.entity.purchasein.PurchaseOrderLineEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 领料申请
 * @author 70486
 */
@TableName("V_WMS_SHOP_ITEM_REQ")
@Getter
@Setter
@ToString
public class VWmsShopItemReqEntity implements Serializable {

    @TableId(value = "HEADER_ID")
    private Long headerId;

    /**
     * 行表ID
     */
    @TableField(value = "LINE_ID")
    private Long lineId;

    /**
     * 组织ID
     */
    @TableField(value = "ORGANIZATION_ID")
    private Long organizationId;

    /**
     * 组织CODE
     */
    @TableField(value = "ORG_CODE")
    private String orgCode;

    /**
     * 组织名称
     */
    @TableField(value = "ORGANIZATION_NAME")
    private String organizationName;

    /**
     * 领料申请单号
     */
    @TableField(value = "MO_CODE")
    private String moCode;

    /**
     * 申请单行号
     */
    @TableField(value = "LINE_NUM")
    private Long lineNum;

    /**
     * 状态
     */
    @TableField(value = "STATUS")
    private String status;

    /**
     * 物料编码
     */
    @TableField(value = "ITEM_CODE")
    private String itemCode;

    /**
     * 物料名称
     */
    @TableField(value = "ITEM_DESC")
    private String itemDesc;

    /**
     * 班次
     */
    @TableField(value = "SHIFT_CODE")
    private String shiftCode;

    /**
     * 机台编码
     */
    @TableField(value = "DEVICE_CODE")
    private String deviceCode;

    /**
     * 机台名称
     */
    @TableField(value = "DEVICE_NAME")
    private String deviceName;

    /**
     * 申请数量
     */
    @TableField(value = "QUANTITY")
    private Long quantity;

    /**
     * 主单位
     */
    @TableField(value = "PRIMARY_UOM_CODE")
    private String primaryUomCode;

    /**
     * 生产厂编码
     */
    @TableField(value = "FAC_CODE")
    private String facCode;

    /**
     * 生产厂名称
     */
    @TableField(value = "FAC_NAME")
    private String facName;

    /**
     * 工序编码
     */
    @TableField(value = "OP_CODE")
    private String opCode;

    /**
     * 工序名称
     */
    @TableField(value = "OP_NAME")
    private String opName;

    /**
     * 来源子库编码
     */
    @TableField(value = "FROM_SUBINV_CODE")
    private String fromSubinvCode;

    /**
     * 来源子库名称
     */
    @TableField(value = "FROM_SUBINV_DESC")
    private String fromSubinvDesc;

    /**
     * 申请人编码
     */
    @TableField(value = "USER_CODE")
    private String userCode;

    /**
     * 申请人名称
     */
    @TableField(value = "USER_NAME")
    private String userName;

    /**
     * 配送地点
     */
    @TableField(value = "ADDRESS")
    private String address;

    /**
     * 备注
     */
    @TableField(value = "COMMENTS")
    private String comments;

    /**
     * 订单日期
     */
    @TableField(value = "NEED_BY_DATE")
    @DateTimeFormat(pattern = "yyyy-MM-hh HH:mm:ss")
    private LocalDateTime needByDate;

    @TableField(exist = false)
    private String warehouse;

    @TableField(exist = false)
    private String typeName;

    @TableField(exist = false)
    private String storeName;

    @TableField(exist = false)
    private String outstats;

    @TableField(exist = false)
    private String materialCode;

    @TableField(exist = false)
    private String materialName;

    @TableField(exist = false)
    private String primaryUnit;

    @TableField(exist = false)
    private String outQuantity;

    @TableField(exist = false)
    private String fromStore;

    @TableField(exist = false)
    private String toStore;

    @TableField(exist = false)
    private String lotsNum;

    @TableField(exist = false)
    private String device;

    @TableField(exist = false)
    private String remark;

    @TableField(exist = false)
    private String boxAmount;

    @TableField(exist = false)
    private String locatorCode;

    @TableField(exist = false)
    private String tempId;

    @TableField(exist = false)
    private String storeQuantity;

    @TableField(exist = false)
    private String deliveryId;
}
