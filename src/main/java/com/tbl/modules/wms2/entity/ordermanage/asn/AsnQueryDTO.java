package com.tbl.modules.wms2.entity.ordermanage.asn;

import lombok.Data;

import java.io.Serializable;

/**
 * 采购订单VO
 *
 * @author DASH
 */
@Data
public class AsnQueryDTO implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 送退货单编号
     */
    private String asnNumber;

    private String asnHeaderId;

    private String asnLineId;

    /**
     * 单据类型(1:送货单 3:退货单)
     */
    private String asnType;

    private String asnTypeName;

    /**
     * 供应商ID
     */
    private Long vendorId;

    /**
     * 供应商名称
     */
    private String vendorName;

    /**
     * 供应商编码
     */
    private String vendorCode;

    private String poCode;

    private String poLineNum;

    private String materialCode;

    private String materialName;

    private String lotsNum;

    private String status;

    private String areaCode;

    private String storeCode;

    private String creationDateStart;

    private String creationDateEnd;

    private String purchaseDept;

    private String purchaseOrgan;


}
