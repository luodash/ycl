package com.tbl.modules.wms2.entity.insidewarehouse.check;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 盘库主表
 *
 * @author DASH
 */
@TableName("YCL_CHECK")
@Data
public class YclCheckEntity implements Serializable {

    private static final long serialVersionUID = 6421071986884481125L;

    /**
     * 主键id
     */
    @TableId(value = "ID", type = IdType.ID_WORKER_STR)
    private String id;

    /**
     * 盘点单号
     */
    @TableField(value = "CHECK_CODE")
    private String checkCode;


    /**
     * 厂区
     */
    @TableField(value = "AREA_CODE")
    private String areaCode;

    /**
     * 厂区
     */
    @TableField(value = "AREA_NAME")
    private String areaName;

    /**
     * 仓库
     */
    @TableField(value = "STORE_CODE")
    private String storeCode;

    /**
     * 仓库
     */
    @TableField(value = "STORE_NAME")
    private String storeName;

    /**
     * 指定操作人
     */
    @TableField(value = "OPERATOR")
    private String operator;

    /**
     * 状态（1，操作中，2，完成）
     */
    @TableField(value = "STATE")
    private String state;

    /**
     * 创建人
     */
    @TableField(value = "CREATER_USER")
    private String createrUser;

    /**
     * 创建时间(系统时间)
     */
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @JSONField(format = "yyyy-MM-dd")
    @TableField(value = "CREATER_TIME")
    private Date createrTime;

    @TableField(value = "UPDATE_USER")
    private String updateUser;

    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @JSONField(format = "yyyy-MM-dd")
    @TableField(value = "UPDATE_TIME")
    private Date updateTime;

    /**
     * 备注
     */
    @TableField(value = "REMARK")
    private String remark;


}
