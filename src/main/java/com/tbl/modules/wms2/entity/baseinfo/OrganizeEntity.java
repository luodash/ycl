package com.tbl.modules.wms2.entity.baseinfo;

import lombok.Data;

/**
 * 工厂机台
 *
 * @author DASH
 */

@Data
public class OrganizeEntity {


    /**
     * 公司(实体)ID
     */
    private String entityId;

    /**
     * 公司（实体）编码
     */
    private String entityCode;

    /**
     * 公司（实体）名称
     */
    private String entityName;

    /**
     * 生产库存组织ID
     */
    private String organizationId;

    /**
     * 生产库存组织编码
     */
    private String organizationCode;

    /**
     * 组织名称
     */
    private String organizationName;

//    private String departCode;
//
//    private String opCode;

    /**
     * 仓库（子库）编码
     */
    private String storeCode;
    /**
     * 仓库(子库) 名称
     */
    private String storeName;


}
