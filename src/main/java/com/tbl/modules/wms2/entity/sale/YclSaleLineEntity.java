package com.tbl.modules.wms2.entity.sale;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.tbl.common.utils.StringUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 销售出库详情
 * @author
 */
@TableName("YCL_SALES_LINE")
@Getter
@Setter
@ToString
public class YclSaleLineEntity implements Serializable {

    /**
     * 主键id
     */
    @TableId(value = "ID", type = IdType.ID_WORKER_STR)
    private String ID;

    /**
     * 出库单号
     */
    @TableField(value = "OUT_CODE")
    private String outCode;

    /**
     * 库位
     */
    @TableField(value = "LOCAL_CODE")
    private String localCode;

    /**
     * 批次号
     */
    @TableField(value = "BATCH")
    private String batch;

    /**
     * 物料编码
     */
    @TableField(value = "MATERIAL_CODE")
    private String materialCode;

    /**
     * 物料名称
     */
    @TableField(value = "MATERIAL_NAME")
    private String materialName;

    /**
     * 单位
     */
    @TableField(value = "UNIT")
    private String unit;

    /**
     * 出库数量
     */
    @TableField(value = "QUALITY")
    private String quality;

    /**
     * 状态（预留）
     */
    @TableField(value = "STATE")
    private String state;

    @TableField(exist = false)
    private String lineId;

    @TableField(exist = false)
    private String orderNumber;
    @TableField(exist = false)
    private String name;
    @TableField(exist = false)
    private String customerName;
    @TableField(exist = false)
    private String storeCode;
    @TableField(exist = false)
    private String stage;
    @TableField(exist = false)
    private String shipment;
    @TableField(exist = false)
    private String request;
    @TableField(exist = false)
    private String organizationName;
    @TableField(exist = false)
    private String itemDesc;
    @TableField(exist = false)
    private String outQuantity;
    @TableField(exist = false)
    private String orgId;
    @TableField(exist = false)
    private String itemCode;
    @TableField(exist = false)
    private String lotsNum;
    @TableField(exist = false)
    private String locatorCode;
    @TableField(exist = false)
    private String factoryCode;
    @TableField(exist = false)
    private String headerId;





    public String getStoreCode(){
        return StringUtils.isNotBlank(storeCode) ? storeCode : "";
    }
    public String getShipment(){
        return StringUtils.isNotBlank(shipment) ? shipment : "";
    }
    public String getRequest(){
        return StringUtils.isNotBlank(request) ? request : "";
    }
    public String getOrganizationName(){
        return StringUtils.isNotBlank(organizationName) ? organizationName : "";
    }
    public String getItemDesc(){
        return StringUtils.isNotBlank(itemDesc) ? itemDesc : "";
    }




}
