package com.tbl.modules.wms2.entity.production;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 退料入库单详情
 */
@Getter
@Setter
@TableName("YCL_MATERIAL_BACK")
@ToString
public class YclMaterialBackEntity implements Serializable {
    /**
     * 主键id
     */
    @TableId(value = "ID", type = IdType.ID_WORKER_STR)
    private String id;

    /**
     * 入库单号
     */
    @TableField("OUT_CODE")
    private String outCode;

    /**
     * 厂区编码
     */
    @TableField("AREA_CODE")
    private String areaCode;

    /**
     * 仓库（库区）编码
     */
    @TableField("STORE_CODE")
    private String storeCode;

    /**
     * 库位编码
     */
    @TableField("LOCATOR_CODE")
    private String locatorCode;

    /**
     * 退料申请单号
     */
    @TableField("APPLY_CODE")
    private String applyCode;

    /**
     * 退料单行号
     */
    @TableField("LINE_CODE")
    private String lineCode;

    /**
     * 物料编码
     */
    @TableField("MATERIAL_CODE")
    private String materialCode;

    /**
     * 物料名称
     */
    @TableField("MATERIAL_NAME")
    private String materialName;

    /**
     * 单位
     */
    @TableField("PRIMARY_UNIT")
    private String primaryUnit;

    /**
     * 批次号
     */
    @TableField("LOTS_NUM")
    private String lotsNum;

    /**
     * 生产班次
     */
    @TableField("SHIFT")
    private String shift;

    /**
     * 机台
     */
    @TableField("DEVICE")
    private String device;

    /**
     * 退料数量
     */
    @TableField("BACK_QUANTITY")
    private String backQuantity;

    /**
     * 入库数量
     */
    @TableField("IN_QUANTITY")
    private String inQuantity;

    /**
     * 质检状态
     */
    @TableField("CHECK_STATE")
    private String checkState;

    /**
     * 状态
     */
    @TableField("STATE")
    private String state;

    /**
     * 申请时间
     */
    @TableField("APPLY_TIME")
    private Date applyTime;

    /**
     * 创建人
     */
    @TableField("CREATE_USER")
    private String createUser;

    /**
     * 创建时间(系统时间)
     */
    @TableField("CREATE_TIME")
    private Date createTime;

    /**
     * 领料申请单行号id
     */
    @TableField("LINE_ID")
    private String lineId;

    /**
     * 件数
     */
    @TableField("BOX_AMOUNT")
    private String boxAmount;
}
