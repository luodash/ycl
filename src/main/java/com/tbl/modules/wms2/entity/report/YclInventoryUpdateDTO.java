package com.tbl.modules.wms2.entity.report;


import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 更新库存
 *
 * @author DASH
 */
@Data
public class YclInventoryUpdateDTO implements Serializable {


    private static final long serialVersionUID = -8035119403569434580L;

    /**
     * 厂区
     */
    private String areaCode;
    private String areaName;

    /**
     * 所在仓库
     */
    private String storeCode;
    private String storeName;

    /**
     * 库位
     */
    private String locatorCode;

    /**
     * 物料编码
     */
    private String materialCode;

    /**
     * 物料名称
     */
    @TableField(value = "MATERIAL_NAME")
    private String materialName;

    /**
     * 单位
     */
    @TableField(value = "PRIMARY_UNIT")
    private String primaryUnit;


    /**
     * 批次码（每个成品唯一，贴在外包装上）
     */
    private String lotsNum;


    /**
     * 出入数量
     */
    private BigDecimal amount;

    /**
     * 出：0 入：1
     */
    private String inout;

//    /**
//     * 订单行
//     */
//    private String poLineId;
//
//
//   /**
//     * 源ID
//     */
//    private String sourceId;


}
