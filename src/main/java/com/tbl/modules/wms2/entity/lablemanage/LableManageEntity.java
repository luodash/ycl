package com.tbl.modules.wms2.entity.lablemanage;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.util.Date;

/**
 * 标签管理
 *
 * @author DASH
 */
@TableName("YCL_LABLE_MANAGE")
@Data
public class LableManageEntity {

    @TableId(value = "ID", type = IdType.ID_WORKER_STR)
    private String id;

    /**
     * 采购订单号
     */
    @TableField(value = "PO_CODE")
    private String poCode;

    /**
     * 物料编码
     */
    @TableField(value = "MATERIAL_CODE")
    private String materialCode;

    @TableField(value = "MATERIAL_NAME")
    private String materialName;

    /**
     * 供应商批次号
     */
    @TableField(value = "VENDOR_LOTS_NUM")
    private String vendorLotsNum;

    /**
     * 供应商编码
     */
    @TableField(value = "VENDOR_CODE")
    private String vendorCode;

    /**
     * 生产日期
     */
    @TableField(value = "PRODUCTION_DATE")
    private Date productionDate;

    /**
     * 有效期
     */
    @TableField(value = "VALIDITY")
    private String validity;

    /**
     * 供应商名称
     */
    @TableField(value = "VENDOR_NAME")
    private String vendorName;

    /**
     * 流水号
     */
    @TableField(value = "SERIAL_NUMBER")
    private String serialNumber;


}
