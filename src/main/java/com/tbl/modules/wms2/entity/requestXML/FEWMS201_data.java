package com.tbl.modules.wms2.entity.requestXML;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 * FEWMS201  采购订单
 * @author 70486
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "revision",
        "poType",//
        "poCode",//
        "orderTime",//
        "supplierCode",//
        "supplierName",//
        "purchaseOrgan",
        "purchaseOrganId",
        "purchaseUser",//
        "purchaseUserId",//
        "remark",//
        "state",//
        "createUser",//
        "place",//
        "contact",//
        "currency",//
        "shipToLocationId",
        "locationCode",
        "orderLines"

})
@XmlRootElement(name = "data")
@Getter
@Setter
@ToString
public class FEWMS201_data {

    /**
     * 订单版本
     */
    public String revision;
    /**
     * 订单类型（标准采购订单） 不使用源数据
     */
    public String poType;
    /**
     * 采购单编号
     */
    public String poCode;
    public String orderTime;
    /**
     * 供应商代码
     */
    public String supplierCode;
    public String supplierName;
    /**
     * 业务实体
     */
    public String purchaseOrgan;
    public String purchaseOrganId;
    /**
     * 采购员
     */
    public String purchaseUser;
    public String purchaseUserId;
    /**
     * 摘要
     */
    public String remark;
    /**
     * 状态（批准、已取消）
     */
    public String state;
    /**
     * 创建人
     */
    public String createUser;
    /**
     * 地点
     */
    public String place;
    public String contact;
    /**
     * 币种
     */
    public String currency;

    /**
     * 收货方ID
     */
    public String shipToLocationId;

    /**
     * 收货方
     */
    public String locationCode;

    /**
     * 订单行数据
     */
    public List<FEWMS201_orderlines> orderLines;

}
