package com.tbl.modules.wms2.entity.purchasein;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;

@Getter
@Setter
@ToString
public class OutSourcingPOEntity implements Serializable {
    private Long poId ;/**订单编号-主键ID**/
    private String ouName;/**业务实体**/
    private Long supplierId ;/**供应商ID**/
    private String supplierCode ;/**供应商编码**/
    private String supplierName ;/**供应商**/
    private Long purchaseDeptId ;/**收货方ID**/
    private String purchaseDept ;/**收货方**/
    private Long purchaseUserId ;/**采购员ID**/
    private String purchaseUser ;/**采购员**/
    private String materialName ;/**委外加工费物料名称**/
    private Long materialId ;/**委外加工费物料ID**/
    private String materialCode ;/**委外加工费物料编码**/
    private String primaryUnit ;/**单位**/
    private BigDecimal unitPrice;/**单价**/
    private BigDecimal quantity;/**订单数量**/
    private Long organizationId;/**库存组织ID**/
    private String organizationName;/**库存组织**/
    private Long poLineId ;/**订单行ID**/
    private String orderTime ;/**订单创建时间**/
    private Long poHeaderId ;/**订单头ID**/
    private Long wxItemId ;/**完工物料ID**/
    private String wxItemNum ;/**完工物料编码**/
    private String wxItemDesc;/**完工物料名称**/
    private Long ftItemId ;/**消耗物料发料ID**/
    private String ftItemNum ;/**消耗物料发料编码**/
    private String ftItemDesc ;/**消耗物料发料名称**/
    private BigDecimal rcvQuantity;/**接受数量**/
}
