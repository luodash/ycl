package com.tbl.modules.wms2.entity.baseinfo;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

/**
 * 库位
 *
 * @author DASH
 */
@TableName("YCL_STOREHOUSE")
@Data
public class StorehouseEntity {

    @TableId(value = "ID")
    private String id;
    @TableId(value = "storearea_code")
    private String storeareaCode;
    @TableId(value = "storearea_name")
    private String storeareaName;
    @TableId(value = "storehouse_code")
    private String storehouseCode;
    @TableId(value = "storehouse_name")
    private String storehouseName;
    @TableId(value = "storehouse_type")
    private String storehouseType;
    @TableId(value = "related_material")
    private String relatedMaterial;
    @TableId(value = "state")
    private String state;
    @TableId(value = "upper_limit")
    private String upperLimit;
    @TableId(value = "lower_limit")
    private String lowerLimit;
    @TableId(value = "create_time")
    private String createTime;
    @TableId(value = "create_user")
    private String createUser;
    @TableId(value = "remark")
    private String remark;


}
