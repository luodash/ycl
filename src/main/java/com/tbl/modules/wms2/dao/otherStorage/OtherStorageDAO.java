package com.tbl.modules.wms2.dao.otherStorage;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.tbl.modules.wms2.entity.operation.YclInOutFlowEntity;

import java.util.List;
import java.util.Map;

/**
 * 出入库
 */
public interface OtherStorageDAO extends BaseMapper<YclInOutFlowEntity> {
    /**
     * 获取分页列表集合数据
     * @param page
     * @param map
     * @return
     */
    List<YclInOutFlowEntity> getPageList(Pagination page, Map<String,Object> map);

    /**
     * 获得自增id
     * @return Long
     */
    Long getSequence();


}
