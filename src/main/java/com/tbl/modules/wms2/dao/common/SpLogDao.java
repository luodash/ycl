package com.tbl.modules.wms2.dao.common;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tbl.modules.wms2.common.support.SysLogEntity;


/**
 * 日志
 *
 * @author DASH
 */
public interface SpLogDao extends BaseMapper<SysLogEntity> {


}
