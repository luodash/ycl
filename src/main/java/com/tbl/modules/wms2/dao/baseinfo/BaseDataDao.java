package com.tbl.modules.wms2.dao.baseinfo;

import java.util.List;
import java.util.Map;

/**
 * @author DASH
 */
public interface BaseDataDao {

    /**
     * 获取公司/厂区-下拉框
     *
     * @param entityId
     * @return
     */
    List<Map<String, Object>> getAreaList(Integer entityId);

    /**
     * 获取仓库-下拉框
     *
     * @param entityId
     * @return
     */
    List<Map<String, Object>> getStoreByEntityId(Integer entityId);

    /**
     * 获取仓库-下拉框
     *
     * @param organizationId 组织ID
     * @return
     */
    List<Map<String, Object>> getStoreByOrganizationId(Integer organizationId);

    List<Map<String, Object>> getStores(Map map);

    /**
     * 获取生产厂-下拉框
     *
     * @param entityId
     * @return
     */
    List<Map<String, Object>> getPlantByEntityId(Integer entityId);

}
