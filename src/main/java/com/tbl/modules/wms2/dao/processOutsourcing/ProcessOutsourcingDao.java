package com.tbl.modules.wms2.dao.processOutsourcing;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.modules.wms2.entity.operation.YclInOutFlowEntity;
import com.tbl.modules.wms2.entity.processOutsourcing.ProcessOutsourcingEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 工序外协
 */
public interface ProcessOutsourcingDao extends BaseMapper<ProcessOutsourcingEntity> {

    /**
     * 获取列表数据
     *
     * @param page
     * @param map
     * @return List<WarehouseEntity>
     */
    List<ProcessOutsourcingEntity> getPageList(Page page, Map<String, Object> map);

    /**
     *功能描述:列表页 主表
     * @param page
     * @param processOutsourcingEntity
     * @return
     */
    Page<ProcessOutsourcingEntity> getPageList(Page page,ProcessOutsourcingEntity processOutsourcingEntity);

    /**
     * 功能描述:根据ID查询
     *
     * @param id
     * @return
     */
    ProcessOutsourcingEntity findById(Long id);

    /**
     * 根据条件查询数据总数
     * @param map
     * @return
     */
    Integer count(Map<String,Object> map);
}
