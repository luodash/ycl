package com.tbl.modules.wms2.dao.sale;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.tbl.modules.wms2.entity.sale.YclMaterialSaleOutEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 采购订单
 *
 * @author 70486
 */
@Mapper
public interface YclMeterSaleOutDAO extends BaseMapper<YclMaterialSaleOutEntity> {

    /**
     * 查询采购订单数据列表
     *
     * @param page
     * @param map
     * @return List<PurchaseOrderEntity>
     */
    List<YclMaterialSaleOutEntity> getPageList(Pagination page, Map<String, Object> map);

    /**
     * 销售订单查询
     *
     * @param page
     * @param map
     * @return
     */
    List<YclMaterialSaleOutEntity> getPageList2(Pagination page, Map<String, Object> map);

    Integer count(Map<String, Object> paramsMap);

    /**
     * 获取Grid列表数据——手机端
     *
     * @param page
     * @param itemCode
     * @return List<PurchaseOrderEntity>
     */
    List<YclMaterialSaleOutEntity> getPageListApi(Pagination page, String itemCode);

    Integer countApi(String itemCode);

    List<Map<String,Object>> getSaleLineByOutCode(String outCode);

}
