package com.tbl.modules.wms2.dao.report;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.wms2.entity.operation.YclInOutFlowEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 入库流水报表
 */
@Mapper
public interface WarehousingReportDao extends BaseMapper<YclInOutFlowEntity> {
    /**
     * 获取集合
     * @param page
     * @param map
     * @return
     */
    List<YclInOutFlowEntity> getPageList(Pagination page, Map<String,Object> map);
}
