package com.tbl.modules.wms2.dao.insidewarehouse.transferorder;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.tbl.modules.wms2.entity.insidewarehouse.transferorder.TransferOrderEntity;


import java.util.List;
import java.util.Map;

/**
 * 移位调拨单
 * @author 70486
 */
public interface TransferOrderDAO extends BaseMapper<TransferOrderEntity> {


	/**
	 * 查询移位调拨单数据列表
	 * @param map
	 * @return
	 */
	List<TransferOrderEntity> getPageList(Map map);


	/**
	 * 查询移位调拨单头表数据列表
	 * @param map
	 * @return
	 */
	List<TransferOrderEntity> getHeadPageList(Map map);

	/**
	 * 获得自增id
	 * @return Long
	 */
	Long getSequence();

	/**
	 * 获取查询的数据总数
	 * @param map
	 * @return
	 */
	int getCount(Map map);

	/**
	 * 查询头表的数据总数
	 * @param map
	 * @return
	 */
	int getHeadCount(Map map);

	List<TransferOrderEntity> getListById(String id);

	Map<String,String> insertHeader(Map<String,Object> map);

	Map<String,String> insertLine(Map<String,Object> map);

	Map<String,String> processOrder(Map<String,Object> map);

	Map<String, Object> getEBSFromLocator(String lineId);

    Map<String, Object> getEBSToLocator(String lineId);

    Map<String,Object> getHeaderInfoById (Long lineId);
}
