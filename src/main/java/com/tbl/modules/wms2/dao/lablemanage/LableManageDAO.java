package com.tbl.modules.wms2.dao.lablemanage;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tbl.modules.wms2.entity.lablemanage.LableManageEntity;

import java.util.List;
import java.util.Map;

/**
 * @author DASH
 */
public interface LableManageDAO extends BaseMapper<LableManageEntity> {

    /**
     * 获取标签管理列表
     * @param map
     * @return
     */
    List<LableManageEntity> getPageList(Map<String,Object> map);

    int count(Map<String,Object> map);


    LableManageEntity selectOneById(String id);





}
