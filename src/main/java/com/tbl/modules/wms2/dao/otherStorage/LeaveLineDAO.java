package com.tbl.modules.wms2.dao.otherStorage;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tbl.modules.wms2.entity.otherStorage.LeaveEntity;
import com.tbl.modules.wms2.entity.otherStorage.LeaveLineEntity;

import java.util.List;
import java.util.Map;

/**
 * 出入库
 */
public interface LeaveLineDAO extends BaseMapper<LeaveLineEntity> {
    /**
     * 获取分页列表集合数据
     * @param map
     * @return
     */
    List<LeaveLineEntity> getPageList(Map<String,Object> map);


    int count(Map<String,Object> map);


}
