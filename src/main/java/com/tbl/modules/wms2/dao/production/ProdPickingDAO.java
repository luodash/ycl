package com.tbl.modules.wms2.dao.production;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.tbl.modules.wms2.entity.production.VWmsShopItemReqEntity;
import com.tbl.modules.wms2.entity.production.YclPickDeliveryRelEntity;
import com.tbl.modules.wms2.entity.production.YclPickOrderBacklEntity;
import com.tbl.modules.wms2.entity.purchasein.PurchaseOrderEntity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 采购订单
 * @author 70486
 */
public interface ProdPickingDAO extends BaseMapper<VWmsShopItemReqEntity> {

	/**
	 * 查询采购订单数据列表
	 * @param page
	 * @param map
	 * @return List<PurchaseOrderEntity>
	 */
	List<VWmsShopItemReqEntity> getPageList(Pagination page, Map<String, Object> map);


	List<VWmsShopItemReqEntity> getApiPageList(Pagination page, Map<String, Object> map);

	/**
	 * 查询采购订单数据列表
	 * @param map
	 * @return List<PurchaseOrderEntity>
	 */
	List<VWmsShopItemReqEntity> getOutPageList(Map<String, Object> map);

	/**
	 *
	 * @param map
	 * @return List<PurchaseOrderEntity>
	 */
	List<VWmsShopItemReqEntity> getPrintPageList(Map<String, Object> map);

	/**
	 * 打印配送单
	 * @param moCodes
	 * @return List<PurchaseOrderEntity>
	 */
	List<VWmsShopItemReqEntity> getPrintDvPageList(@Param("moCodes") List<String> moCodes);

	List<VWmsShopItemReqEntity> getPrintDvPageListById(@Param("deliveryId") String deliveryId);

	List<Map<String,Object>> getLotsNumByEntityId(@Param("entityId")String entityId,
												  @Param("warehouseId")String warehouseId,
												  @Param("itemCode")String itemCode);
	/**
	 * 获得自增id
	 * @return Long
	 */
	Long getSequence();

	/**
	 * 采购订单   插入
	 * @param purchaseOrderEntity
	 * @return int
	 */
	void insertPurchaseOrder(@Param("po") PurchaseOrderEntity purchaseOrderEntity);

	/**
	 * 插入
	 * @param yclPickDeliveryRelEntity
	 * @return int
	 */
	void insertPickDeliveryRel(@Param("pdr") YclPickDeliveryRelEntity yclPickDeliveryRelEntity);

	void insertPickOrderBack(@Param("pob") YclPickOrderBacklEntity yclPickOrderBacklEntity);

	/**
	 * 查询订单号是否存在
	 * @param poCode
	 * @return
	 */
	int selectCountByPoCode(@Param("poCode") String poCode);

	int getCountByLineId(@Param("lineId") String lineId);

	int getDeliveryCountByMoCode(@Param("moCode") String moCode);

	int checkItemCodeIfAutoMove(@Param("itemCode") String itemCode);

	Map<String, Object> checkLocatorIsSelf(@Param("code") String code, @Param("facCode") String facCode, @Param("locatorCode") String locatorCode);

	Map<String, Object> getDeliveryCodeByDate(@Param("toDay") String toDay);

	Integer count(Map<String, Object> paramsMap);

	Integer countOfApi(Map<String, Object> paramsMap);

	int update(HashMap<String, Object> map);

	PurchaseOrderEntity getPurchaseOrder(@Param("po") PurchaseOrderEntity purchaseOrderEntity);

	List<PurchaseOrderEntity> SelectPurchaseOrders(@Param("po") PurchaseOrderEntity purchaseOrderEntity);

	@Select("select * from apps.cux_wx_po_v@wms_to_ebs.fegroup.com.cn t where t.po_number=#{poCode}")
	Map<String,Object> getWxPoView(String poCode);

	@Override
	Integer insert(VWmsShopItemReqEntity vWmsShopItemReqEntity);

	List<Map<String, Object>> getPickDetailInfo(@Param("moCodes") List<String> moCodes, @Param("facCode") String facCode);

	List<Map<String, Object>> getPickDeviceInfo(@Param("moCodes") List<String> moCodes, @Param("facCode") String facCode, @Param("itemCode") String itemCode);

	List<Map<String, Object>> queryFactFromMoCode(@Param("moCodes") List<String> moCodes);

	List<Map<String,Object>> getPickingOutFactory(String outCode);

	List<Map<String,Object>> getPickingOutMaterial(Map paramMap);

	List<Map<String,Object>> getPickingOutMachine(Map paramMap);

	List<Map<String,Object>> getPickingFactoryName(String areaCode);

	List<Map<String,Object>> getMaterialSite(String outCode);

	void updateInvetoryQty(Map paramsMap);

	void updateMaterialQty(Map paramsMap);

	void updateMaterialLiziQty(Map paramsMap);

	List<Map<String,Object>>  getLiziInfo(String materialscode);

}
