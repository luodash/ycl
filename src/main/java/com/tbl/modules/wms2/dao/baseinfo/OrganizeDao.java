package com.tbl.modules.wms2.dao.baseinfo;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.modules.wms2.entity.baseinfo.OrganizeEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author DASH
 */
public interface OrganizeDao extends BaseMapper<OrganizeEntity> {

    /**
     * 获取列表数据
     *
     * @param page
     * @param map
     * @return List<VendorEntity>
     */
    List<OrganizeEntity> getPageList(Page page, Map<String, Object> map);


    /**
     * 获取仓库的下拉列表（公司-组织-仓库）
     * @param map
     * @return
     */
    List<Map<String,Object>> getCompanyAndOrganizationAndStoreList(Map<String, Object> map);


    /**
     * 获取查询数据的总数
     * @param map
     * @return
     */
    int getCount(Map map);

    List<Map<String,Object>> getVendorList(Map map);

    /**
     * 获取公司下拉列表
     * @param map
     * @return
     */
    List<Map<String,Object>> getCompanyList(Map map);

    /**
     * 获取仓库下拉列表
     * @param map
     * @return
     */
    List<Map<String,Object>> getStoreListByEntityId(Map map);

    /**
     * 获取组织下拉列表
     * @param map
     * @return
     */
    List<Map<String,Object>> getOrganizationListByEntityId(Map map);

    /**
     * 根据仓库代码获取组织
     * @param map
     * @return
     */
    List<Map<String,Object>> selectOrganization(Map map);


    /**
     * 获取供应商总数
     * @param map
     * @return
     */
    int getVendorCountByvendorId(Map map);

    /**
     * 获取仓库下拉列表
     * @param map
     * @return
     */
    int getStoreCountByEntityId(Map map);


    List<Map<String,Object>> selectVendor(String vendorName);




}
