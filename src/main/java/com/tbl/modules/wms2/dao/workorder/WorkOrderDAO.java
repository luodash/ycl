package com.tbl.modules.wms2.dao.workorder;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tbl.modules.wms2.entity.purchasein.OutSourcingPOEntity;
import com.tbl.modules.wms2.entity.purchasein.PurchaseOrderEntity;
import com.tbl.modules.wms2.entity.workorder.ReceiveOrderEntity;
import com.tbl.modules.wms2.entity.workorder.WorkOrderEntity;

import java.util.List;
import java.util.Map;

/**
 * 委外加工
 * @author 70486
 */
public interface WorkOrderDAO extends BaseMapper<WorkOrderEntity> {


	/**
	 * 订单接收-仅创建或编辑费用接收的数据，不做实际接收
	 * @param map
	 * @return
	 */
	Map<String,Object> createReceive(Map<String,Object> map);

	/**
	 * 订单接收-实际接收处理
	 * @param map
	 * @return
	 */
	Map<String,Object> receiveProcess(Map<String,Object> map);

	/**
	 * 完工数据校验
	 * @param map
	 * @return
	 */
	Map<String,Object> validateQuantity(Map<String,Object> map);

	/**
	 * 创建或保存完工参数
	 * @param map
	 * @return
	 */
	Map<String,Object> createComplete(Map<String,Object> map);

	/**
	 * 完工实际处理
	 * @param map
	 * @return
	 */
	Map<String,Object> completeProcess(Map<String,Object> map);

	/**
	 * 完工退回
	 * @param map
	 * @return
	 */
	Map<String,String> returnProcess(Map<String,Object> map);

	OutSourcingPOEntity getOutSourcingPOByPoId(Long poId);

	List<ReceiveOrderEntity>getRcvListByPoId(Long poId);

	List<WorkOrderEntity> getWorkOrderListByRcvId(Long rcvId);

	ReceiveOrderEntity getRcvInfoByRcvId(Long rcvId);

	List<Map<String,Object>> getReceiveOrderState(Long rcvId);

	List<Map<String,Object>> getDescriptionByOrgId(Long orgId);

	List<Map<String,Object>> getLocatorListByDesc(Map paramsMap);

	List<Map<String,Object>> getFtInfo(Map map);

	int getFtCount(Map map);


}
