package com.tbl.modules.wms2.dao.baseinfo;

import com.tbl.modules.wms2.entity.purchasein.MeterialBackWmsEntity;

import java.util.Map;

/**
 * @author DASH
 */
public interface MesDao {


    /**
     * MES-仓库发料，更新单据明细
     * @param map
     * @return
     */
    Map<String,Object> issueBillLot(Map<String,Object> map);

    Map<String,Object> returnBillLot(MeterialBackWmsEntity meterialBackWmsEntity);


}
