package com.tbl.modules.wms2.dao.ordermanage.delivery;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.tbl.modules.wms2.entity.ordermanage.delivery.DeliveryEntity;
import com.tbl.modules.wms2.entity.report.YclInventoryEntity;

import java.util.List;
import java.util.Map;


/**
 * 收货
 *
 * @author DASH
 */
public interface DeliveryDao extends BaseMapper<DeliveryEntity> {

    /**
     * 查询列表
     *
     * @param map
     * @return List<DeliveryEntity>
     */
    DeliveryEntity selectDeliveryBydeliveryLineNo(Map<String, Object> map);






}
