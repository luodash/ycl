package com.tbl.modules.wms2.dao.baseinfo;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.modules.wms2.entity.baseinfo.VendorEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author DASH
 */
public interface VendorDao extends BaseMapper<VendorEntity> {

    /**
     * 获取列表数据
     *
     * @param page
     * @param map
     * @return List<VendorEntity>
     */
    List<VendorEntity> getPageList(Page page, Map<String, Object> map);

    /**
     * 功能描述:根据code查询
     *
     * @param id
     * @return
     */
    VendorEntity findById(Long id);
}
