package com.tbl.modules.wms2.dao.purchasein;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.tbl.modules.wms2.entity.operation.YclInOutFlowEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.poi.ss.formula.functions.T;

import java.util.List;
import java.util.Map;

/**
 * 物资出厂单
 */
@Mapper
public interface FactoryOrderDao extends BaseMapper<YclInOutFlowEntity> {
    /**
     * 获取集合列表
     * @return
     */
    List<T> getPageList(Pagination page, Map<String,Object> map);
}
