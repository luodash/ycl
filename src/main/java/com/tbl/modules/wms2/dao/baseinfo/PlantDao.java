package com.tbl.modules.wms2.dao.baseinfo;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.modules.wms2.entity.baseinfo.PlantEntity;

import java.util.List;
import java.util.Map;

/**
 * @author DASH
 */
public interface PlantDao extends BaseMapper<PlantEntity> {

    /**
     * 获取列表数据
     *
     * @param page
     * @param map
     * @return List<VendorEntity>
     */
    List<PlantEntity> getPageList(Page page, Map<String, Object> map);

    /**
     * 根据公司/实体ID 获取生产厂-下拉框列表数据
     * @param entityId
     * @return
     */
    List<Map<String,Object>> getPlantsByEntityId(Integer entityId);


    /**
     * 根据生产厂编码 获取工序-下拉框列表数据
     * @param plantCode
     * @return
     */
    List<Map<String,Object>> getOPByPlantCode (String plantCode);


    /**
     * 生产厂/部门的模糊查询
     * @param departCode
     * @return
     */
    List<Map<String,Object>> selectPlant (String departCode);

}
