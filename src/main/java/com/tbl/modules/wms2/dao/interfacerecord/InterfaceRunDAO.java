package com.tbl.modules.wms2.dao.interfacerecord;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tbl.modules.wms2.entity.ycloutstorage.YclOutStorageDetail;
import com.tbl.modules.wms2.interfacerecord.InterfaceRun;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 接口执行持久层
 * @author 70486
 */
public interface InterfaceRunDAO extends BaseMapper<InterfaceRun> {

    /**
     * 批量更新接口离线执行表状态
     * @param lstOutStorageDetail 发货单明细
     * @param state 状态
     */
    void updateStateBatches(@Param("lstOutStorageDetail") List<YclOutStorageDetail> lstOutStorageDetail, @Param("state") String state);

    /**
     * 更新接口执行表
     * @param interfaceRun 接口数据
     */
    void updateInterfaceRunByid(@Param("interfaceRun") InterfaceRun interfaceRun);

    /**
     * 获取定时任务需要执行的离线接口
     * @return InterfaceRun
     */
    List<InterfaceRun> findJobsInterfaceRuns();
}
