package com.tbl.modules.wms2.dao.insidewarehouse.check;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.tbl.modules.wms2.entity.insidewarehouse.check.YclCheckEntity;
import com.tbl.modules.wms2.entity.purchasein.PurchaseOrderEntity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 采购订单
 * @author 70486
 */
public interface CheckDao extends BaseMapper<YclCheckEntity> {
	
	/**
	 * 查询采购订单数据列表
	 * @param page
	 * @param map
	 * @return List<PurchaseOrderEntity>
	 */
	List<YclCheckEntity> getPageList(Pagination page, Map<String, Object> map);

	/**
	 * 获得自增id
	 * @return Long
	 */
	Long getSequence();

	/**
	 * 采购订单   插入
	 * @param yclCheckEntity
	 * @return int
	 */
	void insertPurchaseOrder(@Param("po") YclCheckEntity yclCheckEntity);

	/**
	 * 查询订单号是否存在
	 * @param poCode
	 * @return
	 */
	int selectCountByPoCode(@Param("poCode") String poCode);


	int update(HashMap<String, Object> map);

	@Select("")
	boolean deleteByCheckCodes(String codes);








}
