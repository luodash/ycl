package com.tbl.modules.wms2.dao.operation;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.modules.wms2.entity.operation.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author Sweven
 * @date 2020-06-25
 */
@Mapper
public interface OperationOtherMapper extends BaseMapper<OperationOther> {

    /**
     * 获取列表数据
     *
     * @param page
     * @param paramsMap
     * @return java.util.List<com.tbl.modules.wms2.entity.operation.OperationOtherVO>
     */
    List<OperationOtherVO> getPageList(Page page, Map<String, Object> paramsMap);

    Integer count(Map<String, Object> paramsMap);

    /**
     * 获取列表数据（主）
     *
     * @param page
     * @param paramsMap
     * @return java.util.List<com.tbl.modules.wms2.entity.operation.OperationOtherVO>
     */
    List<OperationOtherVO> getPageListApi(Page page, Map<String, Object> paramsMap);

    Integer countApi(Map<String, Object> paramsMap);

    OperationOtherPrint findByOtherCode(@Param("otherCode") String otherCode);

    Map<String, Object> createHead(OperationOtherHeadDTO headDto);

    Map<String, Object> createLine(OperationOtherLineDTO lineDTO);

    Map<String, Object> deleteLine(Map<String, Object> map);

    Map<String, Object> validateData(OperationOtherValidDTO validDTO);

    Map<String, Object> process(OperationOtherProcessDTO processDTO);

}