package com.tbl.modules.wms2.dao.baseinfo;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.modules.wms2.entity.baseinfo.WarehouseEntity;

import java.util.List;
import java.util.Map;

/**
 * @author DASH
 */
public interface WarehouseDao extends BaseMapper<WarehouseEntity> {

    /**
     * 获取列表数据
     *
     * @param page
     * @param map
     * @return List<WarehouseEntity>
     */
    List<WarehouseEntity> getPageList(Page page, Map<String, Object> map);

    /**
     * 功能描述:根据ID查询
     *
     * @param id
     * @return
     */
    WarehouseEntity findById(Long id);


    List<Map<String,Object>> entruckNotice(String num);

    List<Map<String,Object>> getLocationfromEBS(Map map);

}
