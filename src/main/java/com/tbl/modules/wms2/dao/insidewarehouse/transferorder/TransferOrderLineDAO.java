package com.tbl.modules.wms2.dao.insidewarehouse.transferorder;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tbl.modules.wms2.entity.insidewarehouse.transferorder.TransferOrderLineEntity;


/**
 * 移位调拨单
 * @author 70486
 */
public interface TransferOrderLineDAO extends BaseMapper<TransferOrderLineEntity> {


	/**
	 * 获得自增id
	 * @return Long
	 */
	Long getSequence();


}
