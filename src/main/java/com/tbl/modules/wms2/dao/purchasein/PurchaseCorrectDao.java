package com.tbl.modules.wms2.dao.purchasein;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.tbl.modules.wms2.entity.purchasein.YclPurchaseCorrectEntity;
import com.tbl.modules.wms2.entity.report.YclInventoryEntity;

import java.util.List;
import java.util.Map;


/**
 * 采购更正
 *
 * @author DASH
 */
public interface PurchaseCorrectDao extends BaseMapper<YclPurchaseCorrectEntity> {

    /**
     * 查询列表
     *
     * @param page
     * @param map
     * @return List<PurchaseOrderEntity>
     */
    List<YclInventoryEntity> getPageList(Pagination page, Map<String, Object> map);


}
