package com.tbl.modules.wms2.dao.operation;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.modules.wms2.entity.baseinfo.WarehouseEntity;
import com.tbl.modules.wms2.entity.operation.YclInOutFlowEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 其他出入库
 */
@Mapper
public interface YclInOutFlowDao extends BaseMapper<YclInOutFlowEntity> {

    /**
     * 获取列表数据
     *
     * @param page
     * @param map
     * @return List<YclInOutFlowEntity>
     */
    List<YclInOutFlowEntity> getPageList(Page page,Map<String, Object> map);


    List<YclInOutFlowEntity> getYclInOutFlowList(Map<String, Object> map);

}
