package com.tbl.modules.wms2.dao.common;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.tbl.modules.wms2.common.support.YclCountEntity;
import com.tbl.modules.wms2.entity.report.YclInventoryEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


/**
 * 库存
 *
 * @author DASH
 */
public interface CountDao extends BaseMapper<YclCountEntity> {

}
