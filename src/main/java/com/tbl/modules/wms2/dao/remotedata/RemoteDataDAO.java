package com.tbl.modules.wms2.dao.remotedata;


import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface RemoteDataDAO {
    /**
     * 查询EBS数据库，获取送货单头数据
     * @param id
     * @return
     */
    List<Map<String,Object>> queryAsnHeaders(@Param("minId") Long id);

    Integer countAsnHeaders(@Param("minId") Long id);

    /**
     * 查询EBS数据库,获取送货单对应的行数据
     * @param id
     * @return
     */
    List<Map<String,Object>> queryAsnLines(@Param("asnHeaderId") Long id);
}
