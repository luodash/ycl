package com.tbl.modules.wms2.dao.sale;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.tbl.modules.wms2.entity.production.YclMaterialOutEntity;
import com.tbl.modules.wms2.entity.sale.YclSaleEntity;

import java.util.List;
import java.util.Map;

/**
 * 采购订单
 * @author 70486
 */
public interface YclSaleOutDAO extends BaseMapper<YclSaleEntity> {

	/**
	 * 查询采购订单数据列表
	 * @param page
	 * @param map
	 * @return List<PurchaseOrderEntity>
	 */
	List<YclSaleEntity> getPageList(Pagination page, Map<String, Object> map);

    Integer count(Map<String, Object> paramsMap);

	/**
	 * 获得自增id
	 * @return Long
	 */
	Long getSequence();

}
