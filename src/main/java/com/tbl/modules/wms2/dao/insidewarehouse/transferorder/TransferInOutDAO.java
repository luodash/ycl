package com.tbl.modules.wms2.dao.insidewarehouse.transferorder;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tbl.modules.wms2.entity.insidewarehouse.transferorder.TransferOrderEntity;

import java.util.List;
import java.util.Map;

/**
 * 移位调拨单
 * @author 70486
 */
public interface TransferInOutDAO extends BaseMapper<TransferOrderEntity> {


	/**
	 * 查询移位调拨单数据列表
	 * @param map
	 * @return
	 */
	List<TransferOrderEntity> getPageList(Map map);

	/**
	 * 获得自增id
	 * @return Long
	 */
	Long getSequence();

	List<TransferOrderEntity> getListById(String id);


}
