package com.tbl.modules.wms2.dao.purchasein;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.tbl.modules.wms2.entity.purchasein.PurchaseOrderLineEntity;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 采购订单
 * @author 70486
 */
public interface PurchaseOrderLineDAO extends BaseMapper<PurchaseOrderLineEntity> {

	/**
	 * 查询采购订单数据列表
	 * @param page
	 * @param map
	 * @return List<Matepo>
	 */
	List<PurchaseOrderLineEntity> getPageList(Pagination page, Map<String,Object> map);

	/**
	 * 获得自增id
	 * @return Long
	 */
	Long getSequence();
	
	/**
	 * 采购订单-行表   插入
	 * @param
	 * @return int
	 */
	int insertPurchaseOrderLine(@Param("pol") PurchaseOrderLineEntity purchaseOrderLineEntity);

	int updateInvalidStatus(HashMap<String,Object> map);

	List<PurchaseOrderLineEntity> getPurchaseOrderLines(@Param("pol") PurchaseOrderLineEntity purchaseOrderLineEntity);


}
