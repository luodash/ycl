package com.tbl.modules.wms2.dao.operation;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tbl.modules.wms2.entity.operation.OperationOtherLine;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Sweven
 * @date 2020-06-25
 */
@Mapper
public interface OperationOtherLineMapper extends BaseMapper<OperationOtherLine> {

    List<OperationOtherLine> findByOtherCode(@Param("otherCode") String otherCode);

}