package com.tbl.modules.wms2.dao.operation;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.tbl.modules.wms2.entity.operation.TranDetail;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 调拨订单
 * @author 70486
 */
public interface TranDetailDAO extends BaseMapper<TranDetail> {
	
	/**
	 * 查询调拨订单数据列表
	 * @param page
	 * @param map
	 * @return List<TranDetail>
	 */
	List<TranDetail> getPageList(Pagination page, Map<String,Object> map);

	/**
	 * 获得自增id
	 * @return Long
	 */
	Long getSequence();


	TranDetail selectTranDetailById(@Param("id") Long id);
}
