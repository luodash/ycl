package com.tbl.modules.wms2.dao.ordermanage.asn;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.tbl.modules.wms2.entity.ordermanage.asn.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 送货单
 *
 * @author 70486
 */
public interface AsnHeaderDAO extends BaseMapper<AsnHeaderEntity> {

    /**
     * 查询送退货详情数据列表
     *
     * @param asnQueryDTO
     * @return List<AsnHeaderEntity>
     */
    List<AsnVO> listViewAsn(@Param("asnQueryDTO") AsnQueryDTO asnQueryDTO);

    List<AsnVO> getListViewAsnForPda(@Param("asnQueryDTO") AsnQueryDTO asnQueryDTO);

    List<AsnHeaderEntity> getAsnList(Map<String, Object> map);

    int getCount(Map<String, Object> map);

    /**
     * 获得自增id
     *
     * @return Long
     */
    Long getSequence();


    /**
     * 更新本地新建送货单的状态
     *
     * @param asnHeader
     */
    void updateAsnHeader(@Param("asnHeader") AsnHeaderEntity asnHeader);

    /**
     * 根据订单获取头id
     *
     * @param poCode    订单号
     * @param poLineNum 订单行号
     * @return
     */
    List<String> getAsnHeaderIds(@Param("poCode") String poCode, @Param("poLineNum") String poLineNum);

    /**
     * 根据收货头id获取列表
     *
     * @param asnHeaderIds
     * @return
     */
    List<AsnHeaderPoVo> getAsnHeaders(@Param("asnHeaderIds") String asnHeaderIds);

    List<AsnHeaderVo> getAsnHeader(@Param("asnHeaderQueryDTO") AsnHeaderQueryDTO asnHeaderQueryDTO);

    List<String> listViewAsnNumberSC(@Param("asnNumber") String asnNumber);

    String vendorCodeById(@Param("vendorId") String vendorId);
}
