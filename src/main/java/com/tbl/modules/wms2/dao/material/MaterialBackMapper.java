package com.tbl.modules.wms2.dao.material;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.modules.wms2.entity.material.MaterialBack;
import com.tbl.modules.wms2.entity.material.MaterialBackPrint;
import com.tbl.modules.wms2.entity.material.MaterialBackViewModel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 退料入库单详情 Mapper
 *
 * @author Sweven
 * @date 2020-06-19
 */
@Mapper
public interface MaterialBackMapper extends BaseMapper<MaterialBack> {

    /**
     * 获取列表数据
     *
     * @param page
     * @param materialBack
     * @return java.util.List<com.tbl.modules.wms2.entity.material.MaterialBackViewModel>
     */
    List<MaterialBackViewModel> getPageList(Page page, MaterialBack materialBack);

    int insertList(@Param("list") List<MaterialBack> list);

    Integer count(MaterialBack materialBack);

    List<MaterialBack> getList(MaterialBack materialBack);

    MaterialBack sumInQuantity(MaterialBack materialBack);

    List<MaterialBackPrint> findByLineId(String lineId);

}