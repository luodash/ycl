package com.tbl.modules.wms2.dao.report;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.tbl.modules.wms2.entity.production.YclMaterialBackEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 不合格退料报表
 */
@Mapper
public interface ReturnReportDao extends BaseMapper<YclMaterialBackEntity> {
    /**
     * 获取列表
     * @param page
     * @param map
     * @param yclMaterialBackEntity
     * @return
     */
    List<YclMaterialBackEntity> getPageList(Pagination page, Map<String,Object> map,YclMaterialBackEntity yclMaterialBackEntity);
}
