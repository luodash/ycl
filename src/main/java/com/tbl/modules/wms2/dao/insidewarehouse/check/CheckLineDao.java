package com.tbl.modules.wms2.dao.insidewarehouse.check;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.tbl.modules.wms2.entity.insidewarehouse.check.YclCheckEntity;
import com.tbl.modules.wms2.entity.insidewarehouse.check.YclCheckLineEntity;
import com.tbl.modules.wms2.entity.purchasein.PurchaseOrderEntity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 采购订单
 * @author 70486
 */
public interface CheckLineDao extends BaseMapper<YclCheckLineEntity> {



}
