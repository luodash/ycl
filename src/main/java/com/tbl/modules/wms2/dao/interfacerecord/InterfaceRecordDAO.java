package com.tbl.modules.wms2.dao.interfacerecord;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tbl.modules.wms2.interfacerecord.InterfaceRecord;
import org.apache.ibatis.annotations.Param;

/**
 * 接口日志记录
 * @author 70486
 */
public interface InterfaceRecordDAO extends BaseMapper<InterfaceRecord> {

    /**
     * 插入日志
     * @param code 接口编码
     * @param name 接口名称
     * @param requestString 请求报文
     * @param responseString 返回报文
     * @param itemCode 物料编码
     * @param batchNo 批次号
     */
    void insertLog(@Param("code") String code, @Param("name") String name, @Param("requestString") String requestString, @Param("responseString") String responseString,
                   @Param("itemCode") String itemCode, @Param("batchNo") String batchNo);
	
}
