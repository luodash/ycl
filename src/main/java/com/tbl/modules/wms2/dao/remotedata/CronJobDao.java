package com.tbl.modules.wms2.dao.remotedata;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tbl.modules.wms2.entity.remotedata.CronJobEntity;

import java.util.List;
import java.util.Map;

/**
 * @author DASH
 */
public interface CronJobDao extends BaseMapper<CronJobEntity> {

    /**
     *
     * @return
     */
    List<CronJobEntity> getList(Map map);

    /**
     * 功能描述:根据ID查询
     *
     * @param id
     * @return
     */
    CronJobEntity findById(Integer id);
}
