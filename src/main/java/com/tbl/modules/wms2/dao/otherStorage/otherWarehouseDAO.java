package com.tbl.modules.wms2.dao.otherStorage;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tbl.modules.wms2.entity.operation.YclInOutFlowEntity;

/**
 * 入库
 */
public interface otherWarehouseDAO extends BaseMapper<YclInOutFlowEntity> {

}
