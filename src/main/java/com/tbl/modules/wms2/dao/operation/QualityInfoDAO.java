package com.tbl.modules.wms2.dao.operation;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tbl.modules.wms2.entity.operation.EmergencyMaterialDTO;
import com.tbl.modules.wms2.entity.operation.QualityInfoEntity;
import com.tbl.modules.wms2.entity.requestXML.FEWMS203_data;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.mapping.StatementType;

import java.util.List;
import java.util.Map;

/**
 * EBS和WMS数据交互 待质检状态信息
 * @author 70486
 */
public interface QualityInfoDAO extends BaseMapper<QualityInfoEntity> {
	
	/**
	 * 查询数据列表
	 * @return List<QualityInfoEntity>
	 */
	List<QualityInfoEntity> getQualityInfoList(Map<String,Object> map);

	QualityInfoEntity selectQualityInfoById(String id);

	/**
	 * 获得自增id
	 * @return Long
	 */
	Long getSequence();

	Map<String,Object> qcInfoToEBSProcessing(Map<String,Object> map);

	Integer count(Map<String,Object> map);

//	EmergencyMaterialDTO getEmergencyMaterialInfo(Map<String,Object> map);

	FEWMS203_data selectEmergencyByPoNumberAndLineNumber(Map<String,Object> map);


}
