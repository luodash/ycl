package com.tbl.modules.wms2.dao.baseinfo;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.modules.wms2.entity.baseinfo.FactoryMachineEntity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * @author DASH
 */
public interface FactoryMachineDao extends BaseMapper<FactoryMachineEntity> {

    /**
     * 获取列表数据
     *
     * @param page
     * @param map
     * @return List<FactoryMachineEntity>
     */
    List<FactoryMachineEntity> getPageList(Page page, Map<String, Object> map);


    /**
     * 获取所有的班次
     * @return
     */
    @Select("select distinct shiftcode from mes.TBLSHIFT@WMS_TO_MES.FEGROUP.COM.CN")
    List<String> getShiftCode();

    int count(@Param("entity") FactoryMachineEntity factoryMachineEntity);


    /**
     * 获取生产厂-工序-机台的下拉列表
     * @param factoryMachineEntity
     * @return
     */
    List<FactoryMachineEntity> getPageList(@Param("entity") FactoryMachineEntity factoryMachineEntity);

    /**
     * 获取生产厂的下拉列表
     * @param factoryMachineEntity
     * @return
     */
    List<FactoryMachineEntity> getFactoryList(@Param("entity") FactoryMachineEntity factoryMachineEntity);

    List<Map<String,Object>> getOpByDepartCode(Map<String, Object> map);


}
