package com.tbl.modules.wms2.dao.report;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.tbl.modules.wms2.entity.insidewarehouse.check.YclCheckEntity;
import com.tbl.modules.wms2.entity.report.YclInventoryEntity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 库存
 *
 * @author DASH
 */
public interface InventoryDao extends BaseMapper<YclInventoryEntity> {

    /**
     * 查询列表
     *
     * @param page
     * @param map
     * @return List<PurchaseOrderEntity>
     */
    List<YclInventoryEntity> getPageList(Pagination page, Map<String, Object> map);

    /**
     * 获取库位包含几种物料
     * @param map
     * @return
     */
    List<Map<String, Object>> getCountByShelf(Map<String, Object> map);
}
