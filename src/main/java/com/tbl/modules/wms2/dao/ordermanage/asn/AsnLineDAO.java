package com.tbl.modules.wms2.dao.ordermanage.asn;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.tbl.modules.wms2.entity.ordermanage.asn.AsnCorrectDTO;
import com.tbl.modules.wms2.entity.ordermanage.asn.AsnLineDTO;
import com.tbl.modules.wms2.entity.ordermanage.asn.AsnLineEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 送货单
 *
 * @author 70486
 */
public interface AsnLineDAO extends BaseMapper<AsnLineEntity> {

    /**
     * 查询采购订单数据列表
     *
     * @param page
     * @param map
     * @return List<AsnLineEntity>
     */
    List<AsnLineEntity> getPageList(Pagination page, Map<String, Object> map);

    List<AsnCorrectDTO> listViewAsnByPoCode(@Param("poCodes") String poCodes);

    /**
     * 获取送退货列表
     *
     * @param map
     * @return
     */
    List<AsnLineDTO> listViewAsnLine(Map<String, Object> map);

    List<AsnLineEntity> listViewLine(@Param("asnHeaderIds")String asnHeaderIds);
}
