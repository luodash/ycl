package com.tbl.modules.wms2.dao.report;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.tbl.modules.wms2.entity.operation.YclInOutFlowEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 出库报表
 */
@Mapper
public interface OutflowReportDao extends BaseMapper<YclInOutFlowEntity> {

    /**
     * 获取集合
     * @param page
     * @param map
     * @return
     */
    List<YclInOutFlowEntity> getPageList(Pagination page, Map<String, Object> map,YclInOutFlowEntity yclInOutFlowEntity);
}
