package com.tbl.modules.wms2.dao.baseinfo;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.modules.wms2.entity.baseinfo.StorehouseEntity;
import com.tbl.modules.wms2.entity.baseinfo.WarehouseEntity;

import java.util.List;
import java.util.Map;

/**
 * 库位
 * @author DASH
 */
public interface StorehouseDao extends BaseMapper<StorehouseEntity> {

    /**
     * 获取列表数据
     * 分页
     * @param page
     * @param map
     * @return List<WarehouseEntity>
     */
    List<WarehouseEntity> getPageList(Page page, Map<String, Object> map);

    /**
     * 根据id，获取库位信息
     * @param id
     * @return
     */
    StorehouseEntity findById(Long id);

}
