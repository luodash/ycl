package com.tbl.modules.wms2.dao.purchasein;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.tbl.modules.wms2.entity.purchasein.OutSourcingPOEntity;
import com.tbl.modules.wms2.entity.purchasein.PurchaseOrderEntity;
import com.tbl.modules.wms2.entity.purchasein.PurchaseOrderSaveEntity;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * 采购订单
 *
 * @author 70486
 */
public interface PurchaseOrderDAO extends BaseMapper<PurchaseOrderEntity> {

    /**
     * 查询采购订单数据列表
     *
     * @param map
     * @param map
     * @return List<PurchaseOrderEntity>
     */
    List<PurchaseOrderEntity> getListView(Map<String, Object> map);


    /**
     * 分页查询采购订单数据列表
     *
     * @param map
     * @return
     */
    List<PurchaseOrderEntity> getPurchaseOrderList(Map<String, Object> map);

    /**
     * 根据条件查询数据总数
     *
     * @param map
     * @return
     */
    Integer count(Map<String, Object> map);

    @Select("select * from apps.cux_wx_po_v@wms_to_ebs.fegroup.com.cn t where t.po_number=#{poCode}")
    Map<String, Object> getWxPoView(String poCode);

    /**
     * 从本地视图查询采购订单 todo 后续看使用这个是否更快速
     *
     * @param map
     * @return
     */
    List<PurchaseOrderEntity> getPageView(Map<String, Object> map);

    Integer countOutSourcing(Map<String, Object> map);

    List<OutSourcingPOEntity> getPoListForOutSourcing(Map<String, Object> map);

    PurchaseOrderSaveEntity savePurchaseOrder(PurchaseOrderSaveEntity poSaveEntity);

    Map<String,Object> returnPurchaseOrder(Map<String,Object> map);

}
