package com.tbl.modules.wms2.dao.baseinfo;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.tbl.modules.wms2.entity.baseinfo.YclInventory;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 调拨订单
 *
 * @author 70486
 */
public interface YclInventoryDAO extends BaseMapper<YclInventory> {

	/**
	 * 查询调拨订单数据列表
	 *
	 * @param page
	 * @param map
	 * @return List<YclInventory>
	 */
	List<YclInventory> getPageList(Pagination page, Map<String, Object> map);

	List<Map<String, Object>> getSelectCode(Map<String, Object> map);

	/**
	 * 获得自增id
	 *
	 * @return Long
	 */
	Long getSequence();

	YclInventory selectTransferById(@Param("id") Long id);

	/**
	 * 根据仓库、厂区、批次号获取物料信息
	 *
	 * @param yclInventory
	 * @return java.util.List<com.tbl.modules.wms2.entity.baseinfo.YclInventory>
	 */
	List<YclInventory> findByLotsNum(YclInventory yclInventory);

	/**
	 * 获取批次号
	 *
	 * @param areaCode
	 * @param storeCode
	 * @param materialCode
	 * @return java.util.List<java.util.Map < java.lang.String, java.lang.Object>>
	 */
	List<Map<String, Object>> getLotsnumList(@Param("areaCode") String areaCode, @Param("storeCode") String storeCode,
									  @Param("materialCode") String materialCode);

	/**
	 * 获取库存数据
	 *
	 * @param areaCode
	 * @param storeCode
	 * @param locatorCode
	 * @return java.util.List<java.util.Map < java.lang.String, java.lang.Object>>
	 */
	List<Map<String, Object>> getInventoryList(@Param("areaCode") String areaCode, @Param("storeCode") String storeCode,
											 @Param("locatorCode") String locatorCode);

}
