package com.tbl.modules.wms2.controller.purchasein;

import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.platform.constant.MenuConstant;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.service.system.RoleService;
import com.tbl.modules.wms2.dao.purchasein.PurchaseOrderDAO;
import com.tbl.modules.wms2.entity.purchasein.OutSourcingPOEntity;
import com.tbl.modules.wms2.entity.purchasein.PurchaseOrderEntity;
import com.tbl.modules.wms2.entity.purchasein.PurchaseOrderSaveEntity;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.*;

/**
 * 采购订单
 * 入库管理
 *
 * @author DASH
 */
@Controller
@RequestMapping(value = "/ycl/purchaseIn/PurchaseOrder")
public class PurchaseOrderController extends AbstractController {


    @Resource
    private RoleService roleService;

    @Resource
    private PurchaseOrderDAO purchaseOrderDAO;


    /**
     * 送货单页面
     *
     * @param id
     * @return ModelAndView
     */
    @RequestMapping(value = "/toDeliveryOrderEdit")
    @ResponseBody
    public ModelAndView toDeliveryOrderEdit(Long id, WebRequest request) {
        String purchaseOrderIds = request.getParameter("purchaseOrderIds");
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("techbloom2/purchaseIn/purchaseOrder/deliveryOrderEdit");
        mv.addObject("purchaseOrderIds", purchaseOrderIds);

//        Map<String, Object> paramsMap = new HashMap<>(1);
//        paramsMap.put("poLineIds", purchaseOrderIds);
//        List<PurchaseOrderEntity> pageList = purchaseOrderDAO.getPurchaseOrderList(paramsMap);
//        mv.addObject("vendorName", pageList.get(0).getSupplierName());
        return mv;
    }

    /**
     * 退货单页面
     *
     * @param id
     * @return ModelAndView
     */
    @RequestMapping(value = "/toRejectedOrderEdit")
    @ResponseBody
    public ModelAndView toRejectedOrderEdit(Long id, WebRequest request) {
        String purchaseOrderIds = request.getParameter("purchaseOrderIds");

        ModelAndView mv = this.getModelAndView();
        mv.setViewName("techbloom2/purchaseIn/purchaseOrder/rejectedOrderEdit");
        mv.addObject("purchaseOrderIds", purchaseOrderIds);
        mv.addObject("supplierName", request.getParameter("supplierName"));
        mv.addObject("purchaseOrgan", request.getParameter("purchaseOrgan"));
        mv.addObject("purchaseUser", request.getParameter("purchaseUser"));

//        Map<String, Object> paramsMap = new HashMap<>(1);
//        paramsMap.put("poLineIds", purchaseOrderIds);
//        List<PurchaseOrderEntity> pageList = purchaseOrderDAO.getPurchaseOrderList(paramsMap);
//        mv.addObject("vendorName", pageList.get(0).getSupplierName());
        return mv;
    }

    /**
     * 跳转Grid列表页
     *
     * @return
     */
    @RequestMapping(value = "/toList")
    public ModelAndView toRewindList() {
        ModelAndView mv = this.getModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(2L, MenuConstant.warehouselist));
        mv.setViewName("techbloom2/purchaseIn/purchaseOrder/list");
        return mv;
    }

    /**
     * 获取Grid列表数据
     *
     * @return Map<String, Object>
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Map<String, Object> list() {

        Map<String, Object> paramsMap = getAllParameter();
        List<PurchaseOrderEntity> pageList = purchaseOrderDAO.getListView(paramsMap);
        Map<String, Object> map = new HashMap<>(4);
        map.put("code", 0);
        map.put("msg", null);
        map.put("count", pageList.size());
        map.put("data", pageList);
        return map;
    }

    @RequestMapping(value = "/pageView")
    @ResponseBody
    public Map<String, Object> pageView() {

        Map<String, Object> paramsMap = getAllParameter();
        List<PurchaseOrderEntity> pageList = purchaseOrderDAO.getPurchaseOrderList(paramsMap);
        Integer count = purchaseOrderDAO.count(paramsMap);
        Map<String, Object> map = new HashMap<>(4);
        map.put("code", 0);
        map.put("msg", null);
        map.put("count", count);
        map.put("data", pageList);
        return map;
    }

    @RequestMapping(value = "/getPoListForOutSourcing")
    @ResponseBody
    public Map<String, Object> getPoListForOutSourcing() {
        Map<String, Object> paramsMap = getAllParameter();
        List<OutSourcingPOEntity> pageList = purchaseOrderDAO.getPoListForOutSourcing(paramsMap);
        Integer count = purchaseOrderDAO.countOutSourcing(paramsMap);
        Map<String, Object> map = new HashMap<>(4);
        map.put("code", 0);
        map.put("msg", null);
        map.put("count", count);
        map.put("data", pageList);
        return map;
    }

    @RequestMapping(value = "/listByIds")
    @ResponseBody
    public Map<String, Object> listByIds() {

        Map<String, Object> paramsMap = getAllParameter();
        List<PurchaseOrderEntity> pageList = purchaseOrderDAO.selectBatchIds(StringUtils.stringToList((String) paramsMap.get("purchaseOrderIds")));
        Map<String, Object> map = new HashMap<>(4);
        map.put("code", 0);
        map.put("msg", null);
        map.put("count", pageList.size());
        map.put("data", pageList);
        return map;
    }

    /**
     * 跳转编辑页
     *
     * @return
     */
    @RequestMapping(value = "/toEdit")
    public ModelAndView toDetail(Long id) {
        String ids = request.getParameter("ids");
        ModelAndView mv = this.getModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(2L, MenuConstant.warehouselist));
        mv.setViewName("techbloom2/purchaseIn/purchaseOrder/edit");
        return mv;
    }


    /**
     * 流水打印页面
     *
     * @return
     */
    @RequestMapping("/toInoutPrint")
    @ResponseBody
    public ModelAndView toInoutPrint() {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("techbloom2/purchaseIn/purchaseOrder/printInout");
        return mv;
    }

    /**
     * 退货打印页面
     *
     * @return
     */
    @RequestMapping("/toRejectPrint")
    @ResponseBody
    public ModelAndView toRejectPrint() {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("techbloom2/purchaseIn/purchaseOrder/printReject");
        return mv;
    }

    /**
     * 采购接收
     *
     * @return
     */
    @RequestMapping(value = "/savePurchaseOrder", method = {RequestMethod.POST})
    @ResponseBody
    public Map<String, Object> savePurchaseOrder(PurchaseOrderSaveEntity poSaveEntity) {
        Map<String, Object> resultaMap = new HashMap<>();
        purchaseOrderDAO.savePurchaseOrder(poSaveEntity);
        return resultaMap;
    }

    /**
     * 采购退回
     *
     * @return
     */
    @RequestMapping(value = "/returnPurchaseOrder", method = {RequestMethod.POST})
    @ResponseBody
    public Map<String, Object> returnPurchaseOrder(@RequestBody JSONObject purchaseOrderJson) {
        Map<String, Object> purchaseOrderMap = (Map<String, Object>) JSONObject.toBean(purchaseOrderJson, Map.class);
        Map<String, Object> resultaMap = new HashMap<>();
        purchaseOrderDAO.returnPurchaseOrder(purchaseOrderMap);
        return resultaMap;
    }


    /***************************************************************************************/



    }
