package com.tbl.modules.wms2.controller.baseinfo;

import com.alibaba.fastjson.JSON;
import com.tbl.common.utils.JDBCUtils;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.platform.constant.MenuConstant;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.service.system.RoleService;
import com.tbl.modules.wms2.entity.baseinfo.FactoryMachineEntity;
import com.tbl.modules.wms2.service.baseinfo.FactoryMachineService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 供应商档案
 * @author DASH
 */
@Controller
@RequestMapping(value = "/ycl/baseinfo/factorymachine")
public class FactoryMachineController extends AbstractController {

    @Autowired
    private RoleService roleService;
    @Autowired
    private FactoryMachineService factoryMachineService;

    /**
     * 跳转Grid列表页
     * @return ModelAndView
     */
    @RequestMapping(value = "/toList")
    public ModelAndView toRewindList() {
        ModelAndView mv = this.getModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(this.getSessionUser().getRoleId(), MenuConstant.factoryMachine));
        mv.setViewName("techbloom2/baseinfo/factorymachine/factorymachine_list");

        return mv;
    }

    /**
     * 获取Grid列表数据
     * @param queryJsonString 查询条件
     * @return Map<String, Object>
     */
    @RequestMapping(value = "/list.do")
    @ResponseBody
    public Map<String, Object> list(String queryJsonString) {
        Map<String, Object> map = new HashMap<>(4);
        if (!StringUtils.isEmptyString(queryJsonString)) {
            map = JSON.parseObject(queryJsonString);
        }
        PageTbl page = this.getPage();
        PageUtils utils = factoryMachineService.getPageList(page, map);
        page.setTotalRows(utils.getTotalCount() == 0 ? 1 : utils.getTotalCount());
        //初始化分页对象
        executePageMap(map, page);
        map.put("rows", utils.getList());
        map.put("total", utils.getTotalPage() == 0 ? 1 : utils.getTotalPage());

        return map;
    }

    /**
     * 获取仓库的下拉选信息
     * @return Map<String,Object>
     */
    @RequestMapping(value = "/selectShiftCode")
    @ResponseBody
    public Map<String, Object> selectWarehouse() {
        Map<String, Object> map = new HashMap<>();
        List<?> list = factoryMachineService.getShiftCode();
        map.put("result", list);
        return map;
    }


    /**
     * 获取生产厂-工序-机台下拉框列表数据
     * @param departCode
     * @param opCode
     * @return
     */
    @RequestMapping(value = {"/depart/{departCode}","/depart/{departCode}/{opCode}","/depart/{departCode}/{opCode}/{machineCode}"},
            method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getFactoryMachineList(@PathVariable("departCode") String departCode,
                                                    @PathVariable("opCode") String opCode,
                                                    @PathVariable(required = false) String machineCode){
        Map<String,Object> map = new HashMap<>();
        if(StringUtils.isNotEmpty(departCode)){
            map.put("departCode",departCode);
        }
        if(StringUtils.isNotEmpty(opCode)){
            map.put("opCode",opCode);
        }
        if(StringUtils.isNotEmpty(machineCode)){
            map.put("machineCode",machineCode);
        }

        int count = factoryMachineService.getCount(map);
        Map<String, Object> resultMap = new HashMap<>();
        if(count>0){
            List<FactoryMachineEntity> list = factoryMachineService.getFactoryMachineList(map);
            resultMap.put("data",list);
            resultMap.put("msg","success");
            resultMap.put("result",true);
            resultMap.put("code",0);
        }else{
            resultMap.put("data","");
            resultMap.put("msg","no data");
            resultMap.put("result",false);
            resultMap.put("code",1);
        }
        return resultMap;
    }


    /**
     *
     * @param organizationId
     * @param departCode
     * @return
     */
    @RequestMapping(value = {"/organize/{organizationId}","/organize/{organizationId}/{departCode}"},method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getFactoryList(@PathVariable("organizationId") String organizationId,
                                             @PathVariable(required = false) String departCode) {
        Map<String,Object> map = new HashMap<>();
        if(StringUtils.isNotEmpty(organizationId)){
            map.put("organizationId",organizationId);
        }
        if(StringUtils.isNotEmpty(departCode)){
            map.put("departCode",departCode);
        }
        List<FactoryMachineEntity> list = factoryMachineService.getFactoryList(map);
        Map<String,Object> resultMap = new HashMap<>();
        if(list.size()>0){
            resultMap.put("code", 0);
            resultMap.put("msg", "success");
            resultMap.put("result", true);
            resultMap.put("data", list);
        }else{
            resultMap.put("code", 1);
            resultMap.put("msg", "Can't found data!");
            resultMap.put("result", false);
            resultMap.put("data", null);
        }
        return resultMap;
    }


    /**
     *
     * @param departCode
     * @return
     */
    @RequestMapping(value = "/op/{departCode}",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getOPList(@PathVariable("departCode") String departCode){
        Map<String,Object> map = getAllParameter();
        map.put("departCode",departCode);

        List<Map<String,Object>> list = factoryMachineService.getOpByDepartCode(map);
        Map<String,Object> resultMap = new HashMap<>();
        if(list.size()>0){
            resultMap.put("code", 0);
            resultMap.put("msg", "success");
            resultMap.put("result", true);
            resultMap.put("data", list);
        }else{
            resultMap.put("code", 1);
            resultMap.put("msg", "Can't found data!");
            resultMap.put("result", false);
            resultMap.put("data", "");
        }
        return resultMap;
    }

    /**
     * 同步生产厂/机台数据
     * @return Map<String, Object>
     */
    @RequestMapping(value = "/synchronousdata.do")
    @ResponseBody
    public Map<String, Object> synchronousdata() {
        Connection conn = JDBCUtils.getConnection();
        PreparedStatement pst = null;
        // 创建执行存储过程的对象
        CallableStatement proc = null;
        try {
            proc = conn.prepareCall("{ call EBS2WMS_WIP_SCHEDULE_GROUPS() }");
            // 执行
            proc.execute();

            proc = conn.prepareCall("{ call EBS2WMS_CUX_INV_OP() }");
            // 执行
            proc.execute();

            proc = conn.prepareCall("{ call EBS2WMS_CUX_INV_MACHINE() }");
            // 执行
            proc.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                // 关闭IO流
                proc.close();
                JDBCUtils.closeAll(null, pst, conn);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return new HashMap<String, Object>(2) {{
            put("result", true);
            put("msg", "更新成功！");
        }};
    }

}


