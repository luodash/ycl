package com.tbl.modules.wms2.controller.report;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.service.system.RoleService;
import com.tbl.modules.wms2.entity.report.YclInventoryEntity;
import com.tbl.modules.wms2.entity.report.YclInventoryUpdateDTO;
import com.tbl.modules.wms2.service.report.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 库存管理
 *
 * @author DASH
 */
@Controller
@RequestMapping(value = "/ycl/report/inventory")
public class InventoryController extends AbstractController {

    @Autowired
    private RoleService roleService;

    @Resource(name = "yclInventoryService")
    private InventoryService inventoryService;

    /**
     * 删除
     *
     * @param
     * @return Map<String, Object>
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Map<String, Object> delete(String ids) {
        Map<String, Object> map = new HashMap<>(2);
        boolean result = inventoryService.delete(ids);
        map.put("result", result);
        map.put("msg", result ? "操作成功！" : "操作失败！");
        return map;
    }

    /**
     * 保存
     *
     * @param
     * @return Map<String, Object>
     */
    @PostMapping(value = "/save")
    @ResponseBody
    public Map<String, Object> save(@RequestBody YclInventoryEntity yclInventoryEntity) {
        yclInventoryEntity.setUpdateTime(DateUtil.date());
        boolean result = inventoryService.save(yclInventoryEntity);
        Map resultMap = new HashMap(2);
        resultMap.put("result", result);
        resultMap.put("msg", result ? "操作成功！" : "操作失败！");
        return resultMap;
    }

    /**
     * 弹出到新增/编辑页面
     *
     * @param id
     * @return ModelAndView
     */
    @RequestMapping(value = "/toEdit")
    @ResponseBody
    public ModelAndView toAdd(String id) {
        ModelAndView mv = this.getModelAndView();
        YclInventoryEntity yclInventoryEntity = null;
        if (StrUtil.isNotEmpty(id)) {
            yclInventoryEntity = inventoryService.findById(id);
        }
        mv.setViewName("techbloom2/report/inventory/edit");
        mv.addObject("yclInventoryEntity", yclInventoryEntity);
        return mv;
    }

    /**
     * 主表 跳转Grid列表页
     *
     * @return
     */
    @RequestMapping(value = "/toList")
    public ModelAndView toRewindList() {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("techbloom2/report/inventory/list");
        return mv;
    }

    /**
     * 主表-获取Grid列表数据
     *
     * @return Map<String, Object>
     */
    @RequestMapping(value = "/pageView")
    @ResponseBody
    public Map<String, Object> pageView(YclInventoryEntity yclInventoryEntity) {
        Map<String, Object> paramsMap = getAllParameter();
        Page<YclInventoryEntity> mapPage = inventoryService.getPageList(
                new Page(
                        Integer.parseInt((String) paramsMap.get("page")),
                        Integer.parseInt((String) paramsMap.get("limit"))),
                yclInventoryEntity);
        return returnLayResponse(mapPage);
    }

    /**
     * 主表-获取Grid列表数据
     *
     * @return Map<String, Object>
     */
    @RequestMapping(value = "/getEntity")
    @ResponseBody
    public Map<String, Object> getEntity(YclInventoryEntity yclInventoryEntity) {
        YclInventoryEntity entity = inventoryService.find(yclInventoryEntity.getAreaCode(), yclInventoryEntity.getStoreCode(),
                yclInventoryEntity.getMaterialCode());
        Map resultMap = new HashMap(2);
        resultMap.put("data", entity);
        resultMap.put("msg", "success！");
        return resultMap;
    }

    /**
     * 不分页
     *
     * @return Map<String, Object>
     */
    @RequestMapping(value = "/listView")
    @ResponseBody
    public Map<String, Object> listView(YclInventoryEntity yclInventoryEntity) {
        List<YclInventoryEntity> page = inventoryService.listView(yclInventoryEntity);
        Map<String, Object> map = new HashMap<>(4);
        map.put("code", 0);
        map.put("msg", null);
        map.put("count", 0);
        map.put("data", page);
        return map;
    }

    /**
     * 不分页 大于O库存
     *
     * @return Map<String, Object>
     */
    @RequestMapping(value = "/listViewWithoutNegative")
    @ResponseBody
    public Map<String, Object> listViewWithoutNegative(YclInventoryEntity yclInventoryEntity) {
        List<YclInventoryEntity> page = inventoryService.listViewWithoutNegative(yclInventoryEntity);
        Map<String, Object> map = new HashMap<>(4);
        map.put("code", 0);
        map.put("msg", null);
        map.put("count", 0);
        map.put("data", page);
        return map;
    }


    /**
     * 更新库存
     *
     * @return Map<String, Object>
     */
    @PostMapping(value = "/updateInventory")
    @ResponseBody
    public Map<String, Object> updateInventory(@RequestBody YclInventoryUpdateDTO inventoryUpdateDTO) {
        Boolean result = inventoryService.updateInventory(inventoryUpdateDTO);
        Map resultMap = new HashMap(2);
        resultMap.put("result", result);
        resultMap.put("msg", result ? "操作成功！" : "操作失败！");
        return resultMap;
    }


}


