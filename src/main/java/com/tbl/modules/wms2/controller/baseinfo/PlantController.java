package com.tbl.modules.wms2.controller.baseinfo;

import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.service.system.RoleService;
import com.tbl.modules.wms2.entity.baseinfo.PlantEntity;
import com.tbl.modules.wms2.service.baseinfo.PlantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 生产厂管理
 *
 * @author DASH
 */
@Controller
@RequestMapping(value = "/ycl/baseinfo/plant")
public class PlantController extends AbstractController {

    @Autowired
    private RoleService roleService;

    @Autowired
    private PlantService plantService;


    /**
     * 主表-获取Grid列表数据
     *
     * @return Map<String, Object>
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Map<String, Object> list(PlantEntity plantEntity) {
        Map<String, Object> paramsMap = getAllParameter();
        Page<PlantEntity> mapPage = plantService.getPageList(
                new Page(
                        paramsMap.get("page")==null?1:Integer.parseInt(paramsMap.get("page").toString()),
                        paramsMap.get("limit")==null?100:Integer.parseInt(paramsMap.get("limit").toString())),
                plantEntity);
        return returnLayResponse(mapPage);
    }

    /**
     * 接口描述：根据公司（实体）ID-获取生产厂-下拉列表
     * @param entityId
     * @return
     */
    @RequestMapping(value = "/plants/{entityId}",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getPlantList(@PathVariable("entityId") Integer entityId) {

        return plantService.getPlantsByEntityId(entityId);

    }


    /**
     * 接口描述: 根据生产厂编码-获取工序-下拉框列表
     * @param plantCode
     * @return
     */
    @RequestMapping(value = "/op/{plantCode}",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getPlantList(@PathVariable("plantCode") String plantCode) {

        return plantService.getOPByPlantCode(plantCode);

    }


    @RequestMapping(value = "/selectPlant/{departCode}")
    @ResponseBody
    public Map<String,Object> getPlantByDepartCode(@PathVariable("departCode") String departCode){
        return plantService.selectPlant(departCode);
    }

}


