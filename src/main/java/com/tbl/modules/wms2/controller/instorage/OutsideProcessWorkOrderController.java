package com.tbl.modules.wms2.controller.instorage;

import com.alibaba.fastjson.JSON;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.platform.constant.MenuConstant;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.service.system.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author DASH
 */
@Controller
@RequestMapping(value = "/wms2/instorage/outsideProcessWorkOrder")
public class OutsideProcessWorkOrderController extends AbstractController {

    @Autowired
    private RoleService roleService;

    /**
     * 弹出到详情页面
     *
     * @param id
     * @return ModelAndView
     */
    @RequestMapping(value = "/toDetail.do")
    @ResponseBody
    public ModelAndView toPurchaseOrderDetail(Long id) {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("techbloom2/instorage/outsideProcess/workOrder_detail");
        return mv;
    }

    /**
     * 跳转Grid列表页
     *
     * @return
     */
    @RequestMapping(value = "/toList")
    public ModelAndView toRewindList() {
        ModelAndView mv = this.getModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(2L, MenuConstant.warehouselist));
        mv.setViewName("techbloom2/instorage/outsideProcess/workOrder_list");
        return mv;
    }

    /**
     * 获取Grid列表数据
     *
     * @param queryJsonString 查询条件
     * @return Map<String, Object>
     */
    @RequestMapping(value = "/list.do")
    @ResponseBody
    public Map<String, Object> list(String queryJsonString) {
        Map<String, Object> map = new HashMap<>(4);
        if (!StringUtils.isEmptyString(queryJsonString)) {
            map = JSON.parseObject(queryJsonString);
        }
//        map.put("factorycode", Arrays.asList(getSessionUser().getFactoryCode().split(",")));
//        PageTbl page = this.getPage();
//        PageUtils utils = warehouseService.getPageList(page, map);
//        page.setTotalRows(utils.getTotalCount() == 0 ? 1 : utils.getTotalCount());
//        //初始化分页对象
//        executePageMap(map, page);
//        map.put("rows", utils.getList());
//        map.put("total", utils.getTotalPage() == 0 ? 1 : utils.getTotalPage());

        List<Map<String, Object>> list = new ArrayList<>();
        for (int i = 1; i < 11; i++) {
            Map map2 = new HashMap(10);
            map2.put("id", i);
            map2.put("processWorkOrderState","工单状态"+i);
            map2.put("processWorkOrderCode", "100000" + i);
            map2.put("processWorkOrderDate", "2010-04-01");
            map2.put("supplierCode", "供应商编号" + i);
            map2.put("supplierName", "供应商名称" + i);
            map2.put("purchaseOrg", "采购组织" + i);
            map2.put("purchaseDept", "采购部门" + i);
            map2.put("purchaser", "采购员" + i);

            list.add(map2);
        }

        //map.put("factorycode", Arrays.asList("92", "93", "94", "95"));
        map.put("rows", list);
        map.put("records", 10);
        map.put("page", 1);
        map.put("total", 10);
        return map;
    }

}


