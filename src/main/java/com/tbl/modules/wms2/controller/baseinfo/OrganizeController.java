package com.tbl.modules.wms2.controller.baseinfo;

import com.alibaba.fastjson.JSON;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.platform.constant.MenuConstant;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.service.system.RoleService;
import com.tbl.modules.wms2.service.baseinfo.OrganizeService;
import io.swagger.annotations.*;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

/**
 * 实体组织
 *
 * @author DASH
 */
@Api(value = "公司组织及供应商信息API",tags = {"公司组织及供应商信息API"})
@RestController
@RequestMapping(value = "/wms2/baseinfo/organize")
public class OrganizeController extends AbstractController {

    @Autowired
    private RoleService roleService;
    @Autowired
    private OrganizeService organizeService;

    /**
     * 跳转Grid列表页
     *
     * @return
     */
    @RequestMapping(value = "/toList")
    public ModelAndView toRewindList() {
        ModelAndView mv = this.getModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(this.getSessionUser().getRoleId(), MenuConstant.organize));
        mv.setViewName("techbloom2/baseinfo/organize/organize_list");

        return mv;
    }

    /**
     * 获取Grid列表数据
     *
     * @param queryJsonString 查询条件
     * @return Map<String, Object>
     */
    @RequestMapping(value = "/list.do")
    @ResponseBody
    public Map<String, Object> list(String queryJsonString) {
        Map<String, Object> map = new HashMap<>(4);
        if (!StringUtils.isEmptyString(queryJsonString)) {
            map = JSON.parseObject(queryJsonString);
        }
        PageTbl page = this.getPage();
        PageUtils utils = organizeService.getPageList(page, map);
        page.setTotalRows(utils.getTotalCount() == 0 ? 1 : utils.getTotalCount());
        //初始化分页对象
        executePageMap(map, page);
        map.put("rows", utils.getList());
        map.put("total", utils.getTotalPage() == 0 ? 1 : utils.getTotalPage());

        return map;
    }


    /**
     * 获取公司列表信息
     * @return
     */
    @ApiOperation(value = "根据公司ID获取公司列表信息,0为获取全部列表",notes = "http://localhost:8083/wms/wms2/baseinfo/organize/company/0")
    @ApiImplicitParam(name = "entityId",value = "公司ID",paramType = "query",required = true)
    @RequestMapping(value = "/company/{entityId}",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getCompanies(@PathVariable("entityId") Integer entityId){
        Map<String,Object> map = new HashMap<>();
        map.put("entityId",entityId);
        return organizeService.getCompanyList(map);
    }


    /**
     * 获取组织信息
     * @param entityId
     * @param organizationId
     * @return
     */
    @RequestMapping(value = {"/organization/{entityId}","/organization/{entityId}/{organizationId}"},method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getOrganizations(@PathVariable("entityId") Integer entityId,
                                               @PathVariable(required = false) Integer organizationId){
        Map<String,Object> map = new HashMap<>();
        map.put("entityId",entityId);
        if(organizationId!=null && organizationId>0){
            map.put("organizationId",organizationId);
        }
        return organizeService.getOrganizationList(map);
    }

    @RequestMapping(value = "/selectOrganization",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> getOrganizationByEntityIdAndStoreCode(@RequestParam("storeCode") String storeCode,
                                                         @RequestParam("entityId") String entityId){
        Map<String,Object> map = new HashMap<>();
        map.put("storeCode",storeCode);
        map.put("entityId",entityId);
        return organizeService.selectOrganization(map);
    }


    /**
     * 获取仓库的下拉列表框
     * @param entityId
     * @param storeId
     * @return
     */
    @RequestMapping(value = {"/store/{entityId}","/store/{entityId}/{storeId}"},method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getStoreList(@PathVariable("entityId") String entityId,@PathVariable(required = false) String storeId) {
        Map<String,Object> map = new HashMap<>();
        map.put("entityId",entityId);
        if(StringUtils.isNotEmpty(storeId)){
            map.put("storeId",storeId);
        }
        return organizeService.getStoreList(map);
    }

    /**
     * 获取供应商下拉列表(/wms2/baseinfo/organize/vendor/)
     * @param //vendorId
     * @return
     */
    @ApiOperation(value = "根据供应商ID或供应商名称获取供应商信息",notes = "http://localhost:8083/wms/wms2/baseinfo/organize/vendor?vendorName=金鹰")
    @ApiImplicitParams({@ApiImplicitParam(name = "vendorId",value = "供应商ID",paramType = "query",required = false),
                        @ApiImplicitParam(name = "vendorName",value = "供应商名称",paramType = "query",required = false)})
    @RequestMapping(value = {"/vendor"},method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getVendorList(){
        Map<String,Object> map = getAllParameter();
        return organizeService.getVendorList(map);
    }


    @RequestMapping(value = "/selectVendor",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> selectVendor(@Param("vendorName") String vendorName){
        return organizeService.selectVendor(vendorName);

    }



}


