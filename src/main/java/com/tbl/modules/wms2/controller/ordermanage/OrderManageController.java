package com.tbl.modules.wms2.controller.ordermanage;

import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.platform.constant.MenuConstant;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.service.system.RoleService;
import com.tbl.modules.wms2.dao.ordermanage.asn.AsnHeaderDAO;
import com.tbl.modules.wms2.dao.purchasein.PurchaseOrderDAO;
import com.tbl.modules.wms2.entity.insidewarehouse.check.YclCheckEntity;
import com.tbl.modules.wms2.entity.ordermanage.asn.AsnHeaderEntity;
import com.tbl.modules.wms2.entity.purchasein.PurchaseOrderEntity;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.sql.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 采购顶订单
 * 订单查询
 * @author DASH
 */
@Controller
@RequestMapping(value = "/ycl/ordermanage")
public class OrderManageController extends AbstractController {


    @Autowired
    private RoleService roleService;

    @Resource
    private PurchaseOrderDAO purchaseOrderDAO;

    @Resource
    private AsnHeaderDAO asnHeaderDAO;


    /**
     * 弹出到新增/编辑页面
     *
     * @param id
     * @return ModelAndView
     */
    @RequestMapping(value = "/toDeliveryOrderEdit")
    @ResponseBody
    public ModelAndView toAdd(Long id, WebRequest request) {
        String purchaseOrderIds = request.getParameter("purchaseOrderIds");
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("techbloom2/purchaseIn/purchaseOrder/deliveryOrderEdit");
        mv.addObject("purchaseOrderIds", purchaseOrderIds);
//        mv.addObject("edit", id != null && id != EmumConstant.carState.INSTORAGING.getCode().longValue() ? EmumConstant.carState.OUTSTORAGING
//                : EmumConstant.carState.INSTORAGING);
//        mv.addObject("warehouse", checkService.findById(id));
//        mv.addObject("warehouseAreaList" , carService.selectWarehouseArea());
        return mv;
    }

    /**
     * 保存
     *
     * @param
     * @return Map<String, Object>
     */
    @RequestMapping(value = "/save")
    @ResponseBody
    public Map<String, Object> save(YclCheckEntity checkEntity) {
        Map<String, Object> map = new HashMap<>(2);
//        car.setWarehouseArea(car.getWarehouseArea().replaceAll("\\d+",""));
//        boolean result = checkService.save(checkEntity);
//        map.put("result", result);
//        map.put("msg", result ? "保存成功！" : "保存失败！");
        return map;
    }

    /**
     * 跳转Grid列表页
     *
     * @return
     */
    @RequestMapping(value = "/toPurchaseOrderList")
    public ModelAndView toRewindList() {
        ModelAndView mv = this.getModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(2L, MenuConstant.warehouselist));
        mv.setViewName("techbloom2/ordermanage/purchase_order_list");
        return mv;
    }

    /**
     * 跳转Grid列表页
     *
     * @return
     */
    @RequestMapping(value = "/toSaleOrderList")
    public ModelAndView toSaleOrderList() {
        ModelAndView mv = this.getModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(2L, MenuConstant.warehouselist));
        mv.setViewName("techbloom2/ordermanage/sale_order_list");
        return mv;
    }

    /**
     * 获取Grid列表数据
     *
     * @return Map<String, Object>
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Map<String, Object> list() {
        Map<String, Object> paramsMap = getAllParameter();
        if(paramsMap.containsKey("orderTime") && StringUtils.isNotEmpty(String.valueOf(paramsMap.get("orderTime")))){//2020-07-01+~+2020-08-01
            String orderTime = String.valueOf(paramsMap.get("orderTime"));
            String orderTimeStart = orderTime.substring(0,10);
            String orderTimeEnd = orderTime.substring(orderTime.lastIndexOf("-")-7);
            paramsMap.put("orderTimeStart",orderTimeStart);
            paramsMap.put("orderTimeEnd",orderTimeEnd);

        }
        Map<String, Object> map = new HashMap<>(4);
        int count = purchaseOrderDAO.count(paramsMap);
        if(count>0){
            List<PurchaseOrderEntity> list = purchaseOrderDAO.getPurchaseOrderList(paramsMap);
            map.put("code", 0);
            map.put("msg", null);
            map.put("count", count);
            map.put("data", list);
        }else{
            map.put("code", 1);
            map.put("msg", "no data");
        }
        return map;
    }

    @RequestMapping(value = "/listByIds")
    @ResponseBody
    public Map<String, Object> listByIds() {

        Map<String, Object> paramsMap = getAllParameter();
        paramsMap.put("purchaseOrderIds","1131");
        List<PurchaseOrderEntity> pageList = purchaseOrderDAO.selectBatchIds(StringUtils.stringToList((String) paramsMap.get("purchaseOrderIds")));
        Map<String, Object> map = new HashMap<>(4);
        map.put("code", 0);
        map.put("msg", null);
        map.put("count", pageList.size());
        map.put("data", pageList);
        return map;
    }


    /**
     * 跳转编辑页
     *
     * @return
     */
    @RequestMapping(value = "/toEdit")
    public ModelAndView toDetail(Long id) {
        String ids = request.getParameter("ids");
        ModelAndView mv = this.getModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(2L, MenuConstant.warehouselist));
        mv.setViewName("techbloom2/purchaseIn/purchaseOrder/edit");
        return mv;
    }

    /**
     * 跳转Grid列表页
     *
     * @return
     */
    @RequestMapping(value = "/toAsnList")
    public ModelAndView toAsnList() {
        ModelAndView mv = this.getModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(this.getSessionUser().getRoleId(), MenuConstant.warehouselist));
        mv.setViewName("techbloom2/ordermanage/asn_list");
        return mv;
    }

    /**
     * 获取Grid列表数据
     *
     * @return Map<String, Object>
     */
    @RequestMapping(value = "/asnList")
    @ResponseBody
    public Map<String, Object> asnList() {
        Map<String, Object> paramsMap = getAllParameter();
        if(paramsMap.containsKey("shipDate") && !String.valueOf(paramsMap.get("shipDate")).isEmpty()){//2020-07-01+~+2020-08-01
            String shipDate = String.valueOf(paramsMap.get("shipDate"));
            String shipDateStart = shipDate.substring(0,10);
            String shipDateEnd = shipDate.substring(shipDate.lastIndexOf("-")-7);
            paramsMap.put("shipDateStart",shipDateStart);
            paramsMap.put("shipDateEnd",shipDateEnd);

        }
        int count = asnHeaderDAO.getCount(paramsMap);
        List<AsnHeaderEntity> list = asnHeaderDAO.getAsnList(paramsMap);
        Map<String, Object> map = new HashMap<>(4);
        map.put("code", 0);
        map.put("msg", null);
        map.put("count", count);
        map.put("data", list);
        return map;
    }


    public void doCall(){


        String driver = "oracle.jdbc.driver.OracleDriver";
        String strUrl = "jdbc:oracle:thin:@10.1.90.176:1521:wms";
        Statement stmt = null;
        ResultSet rs = null;
        Connection conn = null;
        Long poId=4261L;//委外加工头ID
        Long organizationId=92L;//库存组织ID   92:INV_A01_远东电缆生产库存组织
        Long rcvQuantity=200L;//接收数量
        Long itemId=1469980L;//委外加工物料
        String subInventory="Y001";//费用接收子库
        Long locatorId=302539L;//费用接收货位
        Long wxItemId=487502L;//完工物料ID
        String rcvId="";//接收行ID,如果是未保存过的数据，该值传空值即可
        try {
            Class.forName(driver);
            conn =  DriverManager.getConnection(strUrl, "db_wms", "db_wms");
            CallableStatement proc = null;
            proc = conn.prepareCall("{ call apps.cux_wms_outside_pkg.create_rcv@wms_to_ebs.fegroup.com.cn(?,?,?,?,?,?,?,?,?,?) }");
            proc.setLong(3, poId);
            proc.setLong(4,organizationId);
            proc.setLong(5,rcvQuantity);
            proc.setLong(6,itemId);
            proc.setString(7,subInventory);
            proc.setLong(8,locatorId);
            proc.setLong(9,wxItemId);
            proc.setString(10,rcvId);

            proc.registerOutParameter(1, Types.VARCHAR);
            proc.registerOutParameter(2, Types.VARCHAR);
            proc.execute();

            String retCode = proc.getString(1);
            String retMess = proc.getString(2);
            System.out.println("存储过程返回的状态是："+retCode+" ，返回消息是："+retMess);
        }
        catch (SQLException ex2) {
            ex2.printStackTrace();
        }
        catch (Exception ex2) {
            ex2.printStackTrace();
        }
        finally{
            try {
                if(rs != null){
                    rs.close();
                    if(stmt!=null){
                        stmt.close();
                    }
                    if(conn!=null){
                        conn.close();
                    }
                }
            }
            catch (SQLException ex1) {
            }
        }
    }

}
