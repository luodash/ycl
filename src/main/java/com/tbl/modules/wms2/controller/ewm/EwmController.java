package com.tbl.modules.wms2.controller.ewm;

import com.alibaba.fastjson.JSON;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.platform.constant.MenuConstant;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.service.system.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;

/**
 * 扫码入库单
 */
@Controller("/ewm")
@RequestMapping("wms2/ewm/ewm")
public class EwmController extends AbstractController {
    @Autowired
    private RoleService roleService;


    /**
     * 弹窗新增
     * @param id
     * @return
     */
    @RequestMapping("/toAdd.do")
    @ResponseBody
    public ModelAndView toAdd(Long id){
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("techbloom2/baseinfo/ewm/ewm_detail");
        return mv;
    }


    /**
     * 跳转Grid列表页
     * @return
     */
    @RequestMapping(value = "/toList")
    public ModelAndView toRewindList() {
        ModelAndView mv = this.getModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(2L, MenuConstant.warehouselist));//因为没有送货单，所以使用出货单代替
        mv.setViewName("techbloom2/baseinfo/ewm/ewm_list");
        return mv;
    }


    /**
     *
     * @param queryJsonString
     * @return
     */
    @RequestMapping(value = "/list.do")
    @ResponseBody
    public Map<String, Object> list(String queryJsonString) {
        Map<String, Object> map = new HashMap<>(4);
        if (!StringUtils.isEmptyString(queryJsonString)) {
            map = JSON.parseObject(queryJsonString);
        }

        List<Map<String, Object>> list = new ArrayList<>();
        for (int i = 0; i < 11; i++) {
            Map map2 = new HashMap(15);
            map2.put("id",i);
            map2.put("q", "1001"+i);
            map2.put("w","sh000"+i);
            map2.put("e", "供应商"+i);
            map2.put("r", "钉钉");
            map2.put("t", 10000 + i);
            map2.put("a", "采购员" + i);
            map2.put("b","仓库"+i);
            map2.put("c","库架"+i);
            map2.put("d", "130010"+i);
            map2.put("f", "物料"+i);
            map2.put("g", "千克"+i);
            map2.put("h", i);
            map2.put("l", "2020-4-28");
            map2.put("j", "经办人"+i);
            map2.put("k", "销售员"+i);
            list.add(map2);
        }

        map.put("factorycode", Arrays.asList("92", "93", "94", "95"));
        map.put("rows", list);
        map.put("records", 11);
        map.put("page", 1);
        map.put("total", 11);
        return map;
    }
}
