package com.tbl.modules.wms2.controller.report;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.service.system.RoleService;
import com.tbl.modules.wms2.entity.production.YclMaterialBackEntity;
import com.tbl.modules.wms2.service.report.ReturnReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/ycl/report/layout")
public class LayoutController extends AbstractController {

    @Autowired
    private RoleService roleService;

    @Autowired
    private ReturnReportService returnReportService;

    /**
     * 主表 跳转Grid列表页
     *
     * @return
     */
    @RequestMapping(value = "/toView")
    public ModelAndView toRewindList() {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("techbloom2/report/layout/view");
        return mv;
    }

    /**
     * 主表-获取Grid列表数据
     * @return Map<String, Object>
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Map<String, Object> list(YclMaterialBackEntity yclMaterialBackEntity) {
        Map<String, Object> paramsMap = getAllParameter();
        Page<YclMaterialBackEntity> mapPage = returnReportService.getPageList(
                new Page(
                        Integer.parseInt((String) paramsMap.get("page")),
                        Integer.parseInt((String) paramsMap.get("limit"))),
                paramsMap,yclMaterialBackEntity);
        return returnLayResponse(mapPage);
    }
}
