package com.tbl.modules.wms2.controller.baseinfo;

import com.alibaba.fastjson.JSON;
import com.tbl.common.utils.JDBCUtils;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.platform.constant.MenuConstant;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.service.system.RoleService;
import com.tbl.modules.wms2.service.baseinfo.VendorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * 供应商档案
 * @author DASH
 */
@Controller
@RequestMapping(value = "/wms2/baseinfo/vendor")
public class VendorController extends AbstractController {

    @Autowired
    private RoleService roleService;

    @Autowired
    private VendorService vendorService;

    /**
     * 跳转Grid列表页
     * @return ModelAndView
     */
    @RequestMapping(value = "/toList")
    public ModelAndView toRewindList() {
        ModelAndView mv = this.getModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(this.getSessionUser().getRoleId(), MenuConstant.vendorList));
        mv.setViewName("techbloom2/baseinfo/vendor/vendor_list");
        return mv;
    }

    /**
     * 获取Grid列表数据
     * @param queryJsonString 查询条件
     * @return Map<String, Object>
     */
    @RequestMapping(value = "/list.do")
    @ResponseBody
    public Map<String, Object> list(String queryJsonString) {
        Map<String, Object> map = new HashMap<>(4);
        if (!StringUtils.isEmptyString(queryJsonString)) {
            map = JSON.parseObject(queryJsonString);
        }
        PageTbl page = this.getPage();
        PageUtils utils = vendorService.getPageList(page, map);
        page.setTotalRows(utils.getTotalCount() == 0 ? 1 : utils.getTotalCount());
        //初始化分页对象
        executePageMap(map, page);
        map.put("rows", utils.getList());
        map.put("total", utils.getTotalPage() == 0 ? 1 : utils.getTotalPage());

        return map;
    }

    /**
     * 同步供应商档案基础数据
     * @return Map<String,Object>
     */
    @RequestMapping(value = "/synchronousdata.do")
    @ResponseBody
    public Map<String,Object> synchronousdata() {
        Connection conn = JDBCUtils.getConnection();
        PreparedStatement pst = null;
        // 创建执行存储过程的对象
        CallableStatement proc = null;
        try {
            proc = conn.prepareCall("{ call EBS2WMS_YDDL_WAREHOUSE () }");
            // 执行
            proc.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                // 关闭IO流
                proc.close();
                JDBCUtils.closeAll(null, pst, conn);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return new HashMap<String,Object>(2) {{
            put("result", true);
            put("msg", "更新成功！");
        }};
    }
}


