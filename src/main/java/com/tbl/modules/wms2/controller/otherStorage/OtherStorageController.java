package com.tbl.modules.wms2.controller.otherStorage;

import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.modules.platform.constant.MenuConstant;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.service.system.RoleService;
import com.tbl.modules.wms2.dao.operation.YclInOutFlowDao;
import com.tbl.modules.wms2.entity.operation.YclInOutFlowEntity;
import com.tbl.modules.wms2.entity.purchasein.PurchaseOrderEntity;
import com.tbl.modules.wms2.service.operation.YclInOutFlowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.swing.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 其他出入库
 */
@Controller
@RequestMapping("/wms2/otherStorage/otherStorage")
public class OtherStorageController extends AbstractController {
    @Autowired
    RoleService roleService;

    @Autowired
    private YclInOutFlowService yclInOutFlowService;

    @Autowired
    private YclInOutFlowDao yclInOutFlowDao;

    /**
     * 保存
     *
     * @param
     * @return Map<String, Object>
     */
    @PostMapping(value = "/save")
    @ResponseBody
    public Map<String, Object> save(@RequestBody YclInOutFlowEntity yclInOutFlowEntity) {
        Map<String, Object> map = new HashMap<>(2);
        boolean result = yclInOutFlowService.save(yclInOutFlowEntity);
        map.put("result", result);
        map.put("msg", result ? "操作成功！" : "操作失败！");
        return map;
    }

    /**
     * 删除
     *
     * @param
     * @return Map<String, Object>
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Map<String, Object> delete(String ids) {
        Map<String, Object> map = new HashMap<>(2);
        boolean result = yclInOutFlowService.delete(ids);
        map.put("result", result);
        map.put("msg", result ? "操作成功！" : "操作失败！");
        return map;
    }

    /**
     * 跳转编辑页
     *
     * @return
     */
    @RequestMapping(value = "/toEdit")
    public ModelAndView toDetail(String id) {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("techbloom2/otherStorage/otherStorage_edit");
        return mv;
    }

    /**
     * 跳转至列表页
     * @return
     */
    @RequestMapping("/toList")
    public ModelAndView toRewindList(){
        ModelAndView mv = this.getModelAndView();
       // mv.addObject("operationCode",roleService.selectOperationByRoleId(2L, MenuConstant.));
        mv.setViewName("techbloom2/otherStorage/otherStorage_list");
        return mv;
    }


    /**
     * 获取Grid列表数据
     *
     * @return Map<String, Object>
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Map<String, Object> list() {
        Map<String, Object> paramsMap = getAllParameter();
        List<YclInOutFlowEntity> pageList = yclInOutFlowDao.getPageList(
                new Page(
                Integer.parseInt((String)paramsMap.get("page")),
                        Integer.parseInt((String)paramsMap.get("limit"))),
                        paramsMap
        );
        Map<String, Object> map = new HashMap<>(4);
        map.put("code", 0);
        map.put("msg", null);
        map.put("count", pageList.size());
        map.put("data", pageList);
        return map;
    }

    /**
     * 弹出到新增/编辑页面
     * @param id
     * @param request
     * @return
     */
    @RequestMapping("/toOtherStorageEdit")
    @ResponseBody
    public ModelAndView toAdd(Long id, WebRequest request) {
        String otherStorageIds = request.getParameter("otherStorageIds");
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("techbloom2/otherStorage/otherStorage_edit");
        mv.addObject("otherStorageIds", otherStorageIds);

        Map<String, Object> paramsMap = new HashMap<>(1);
        paramsMap.put("poLineIds", otherStorageIds);
        List<YclInOutFlowEntity> pageList = yclInOutFlowDao.getPageList(
                new Page(1, 999),
                paramsMap);
        mv.addObject("vendorName", pageList.get(0).getSupplierName());
        return mv;
    }
}