package com.tbl.modules.wms2.controller.setting;

import com.alibaba.fastjson.JSON;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.platform.constant.MenuConstant;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.service.system.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author DASH
 */
@Controller("/wms2/setting/retention")
@RequestMapping(value = "/wms2/setting/retention")
public class RetentionController extends AbstractController {

    @Autowired
    private RoleService roleService;

    /**
     * 弹出到新增/编辑页面
     *
     * @param id
     * @return ModelAndView
     */
    @RequestMapping(value = "/toAdd.do")
    @ResponseBody
    public ModelAndView toAdd(Long id) {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("techbloom2/setting/retention/edit");
        return mv;
    }

    /**
     * 弹出到新增/编辑页面
     *
     * @param id
     * @return ModelAndView
     */
    @RequestMapping(value = "/toView.do")
    @ResponseBody
    public ModelAndView toView(Long id) {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("techbloom2/setting/retention/view");
        return mv;
    }

    /**
     * 跳转Grid列表页
     *
     * @return
     */
    @RequestMapping(value = "/toList")
    public ModelAndView toRewindList() {
        ModelAndView mv = this.getModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(2L, MenuConstant.warehouselist));
        mv.setViewName("techbloom2/setting/retention/list");
        return mv;
    }

    /**
     * 获取Grid列表数据
     *
     * @param queryJsonString 查询条件
     * @return Map<String, Object>
     */
    @RequestMapping(value = "/list.do")
    @ResponseBody
    public Map<String, Object> list(String queryJsonString) {
        Map<String, Object> map = new HashMap<>(4);
        if (!StringUtils.isEmptyString(queryJsonString)) {
            map = JSON.parseObject(queryJsonString);
        }
        List<Map<String, Object>> list = new ArrayList<>();
        for (int i = 1; i < 11; i++) {
            Map map2 = new HashMap(9);
            map2.put("id", i);
            map2.put("code", "100000" + i);
            map2.put("date", "");
            map2.put("dept", "");
            map2.put("material", "");
            map2.put("applicat", "");
            map2.put("phone", "");
            map2.put("createdBy", "");
            map2.put("status", "已出库");
            list.add(map2);
        }
        map.put("rows", list);
        map.put("records", 10);
        map.put("page", 1);
        map.put("total", 10);
        return map;
    }

}


