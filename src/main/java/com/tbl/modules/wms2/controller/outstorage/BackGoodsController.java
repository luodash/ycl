package com.tbl.modules.wms2.controller.outstorage;

import com.alibaba.fastjson.JSON;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.platform.constant.MenuConstant;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.service.system.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;

/**
 * 退货出库
 *
 * @author DASH
 */
@Controller
@RequestMapping(value = "/wms2/outstorage/backGoods")
public class BackGoodsController extends AbstractController {

    @Autowired
    private RoleService roleService;

    /**
     * 弹出到新增/编辑页面
     *
     * @param id
     * @return ModelAndView
     */
    @RequestMapping(value = "/toAdd.do")
    @ResponseBody
    public ModelAndView toAdd(Long id) {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("techbloom2/outstorage/backGoods/edit");
//        mv.addObject("edit", id != null && id != EmumConstant.carState.INSTORAGING.getCode().longValue() ? EmumConstant.carState.OUTSTORAGING
//                : EmumConstant.carState.INSTORAGING);
//        mv.addObject("car", carService.findCarById(id, getSessionUser().getFactoryCode()));
//        mv.addObject("warehouseAreaList" , carService.selectWarehouseArea());
        return mv;
    }

    /**
     * 跳转Grid列表页
     *
     * @return
     */
    @RequestMapping(value = "/toList")
    public ModelAndView toRewindList() {
        ModelAndView mv = this.getModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(2L, MenuConstant.warehouselist));
        mv.setViewName("techbloom2/outstorage/backGoods/list");
        return mv;
    }

    /**
     * 获取Grid列表数据
     *
     * @param queryJsonString 查询条件
     * @return Map<String, Object>
     */
    @RequestMapping(value = "/list.do")
    @ResponseBody
    public Map<String, Object> list(String queryJsonString) {
        Map<String, Object> map = new HashMap<>(4);
        if (!StringUtils.isEmptyString(queryJsonString)) {
            map = JSON.parseObject(queryJsonString);
        }
//        map.put("factorycode", Arrays.asList(getSessionUser().getFactoryCode().split(",")));
//        PageTbl page = this.getPage();
//        PageUtils utils = warehouseService.getPageList(page, map);
//        page.setTotalRows(utils.getTotalCount() == 0 ? 1 : utils.getTotalCount());
//        //初始化分页对象
//        executePageMap(map, page);
//        map.put("rows", utils.getList());
//        map.put("total", utils.getTotalPage() == 0 ? 1 : utils.getTotalPage());

        List<Map<String, Object>> list = new ArrayList<>();
        for (int i = 0; i < 11; i++) {
            Map map2 = new HashMap(5);
            map2.put("id", i);
            map2.put("q", 0000 + i);
            map2.put("w", "仓库" + i);
            map2.put("e", 0000 + i);
            map2.put("r", "厂区" + i);
            map2.put("t", "厂区" + i);
            list.add(map2);
        }

        map.put("factorycode", Arrays.asList("92", "93", "94", "95"));
        map.put("rows", list);
        map.put("records", 11);
        map.put("page", 1);
        map.put("total", 11);
        return map;
    }

    /**
     * 详情
     *
     * @param id 出库单主键
     * @return ModelAndView
     */
    @RequestMapping(value = "/toInfo.do")
    @ResponseBody
    public ModelAndView toInfo(Long id) {
        ModelAndView mv = this.getModelAndView();
        mv.addObject("infoId", id);
        mv.setViewName("techbloom2/outstorage/backGoods/info");
        return mv;
    }

}
