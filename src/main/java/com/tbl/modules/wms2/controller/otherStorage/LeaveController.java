package com.tbl.modules.wms2.controller.otherStorage;

import cn.hutool.core.date.DateUtil;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.service.system.RoleService;
import com.tbl.modules.wms2.dao.otherStorage.LeaveDAO;
import com.tbl.modules.wms2.entity.otherStorage.LeaveEntity;
import com.tbl.modules.wms2.entity.otherStorage.LeaveLineEntity;
import com.tbl.modules.wms2.service.common.CountService;
import com.tbl.modules.wms2.service.otherStorage.LeaveLineService;
import com.tbl.modules.wms2.service.otherStorage.LeaveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 出厂单
 */
@Controller
@RequestMapping("/wms2/otherStorage/leave")
public class LeaveController extends AbstractController {
    @Autowired
    RoleService roleService;

    @Autowired
    private LeaveLineService leaveLineService;

    @Autowired
    private LeaveService leaveService;
    @Resource
    private LeaveDAO leaveDAO;
    @Resource
    private CountService countService;

    /**
     * 保存
     *
     * @param
     * @return Map<String, Object>
     */
    @PostMapping(value = "/save")
    @ResponseBody
    public LeaveEntity save(@RequestBody LeaveEntity leaveEntity) {
        Map<String, Object> map = new HashMap<>(2);
        String outCode = "";
        if(StringUtils.isNotEmpty(leaveEntity.getOrderCode())){
            String orderCode = leaveEntity.getOrderCode();
            Map<String,Object> map1 = new HashMap<>();
            map1.put("order_code",orderCode);
            List<LeaveEntity> list = new ArrayList<>();
            list = leaveService.selectByMap(map1);
            for(int i=0;i<list.size();i++){
                outCode = list.get(i).getOutCode();
                Map<String,Object> map2 = new HashMap<>();
                map2.put("out_code",outCode);
                leaveLineService.deleteByMap(map2);
                leaveService.delete(list.get(i).getId());
            }
        }
        if ("".equals(outCode))
        {
            outCode = countService.getNextCount("CCD");
        }
        leaveEntity.setOutCode(outCode);
        leaveEntity.setCreator(getSessionUser().getName());
        leaveEntity.setCreateTime(DateUtil.date());
//        leaveEntity.setOrderCode("xxx");
//        leaveEntity.setPayStatus("1");
//        leaveEntity.setAutoNum("1");

        //为行数据设置outCode字段
        if(leaveEntity.getLine()!=null && leaveEntity.getLine().size()>0){
            List<LeaveLineEntity> lineList = new ArrayList<>();
            for(int i=0,j=leaveEntity.getLine().size();i<j;i++){
                LeaveLineEntity leaveLineEntity = leaveEntity.getLine().get(i);
                leaveLineEntity.setOutCode(outCode);
                lineList.add(leaveLineEntity);
            }
            boolean bLine = leaveLineService.insertBatch(lineList);
            if(bLine){
                leaveService.insert(leaveEntity);
            }
        }
        return leaveEntity;
    }

    /**
     * 删除
     *
     * @param
     * @return Map<String, Object>
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Map<String, Object> delete(String ids) {
        Map<String, Object> map = new HashMap<>(2);
        boolean result = leaveService.delete(ids);
        map.put("result", result);
        map.put("msg", result ? "操作成功！" : "操作失败！");
        return map;
    }

    /**
     * 跳转编辑页
     *
     * @return
     */
    @RequestMapping(value = "/toEdit")
    public ModelAndView toDetail(String id) {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("techbloom2/otherStorage/leave_edit");
        return mv;
    }

    /**
     * 跳转至列表页
     * @return
     */
    @RequestMapping("/toList")
    public ModelAndView toRewindList(){
        ModelAndView mv = this.getModelAndView();
       // mv.addObject("operationCode",roleService.selectOperationByRoleId(2L, MenuConstant.));
        mv.setViewName("techbloom2/otherStorage/leave_list");
        return mv;
    }


    /**
     * 获取Grid列表数据
     *
     * @return Map<String, Object>
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Map<String, Object> list() {
        Map<String, Object> paramsMap = getAllParameter();
        Map<String, Object> resultMap = new HashMap<>(4);
        int count = leaveDAO.count(paramsMap);
        if(count>0){
            List<LeaveEntity> list = leaveService.getPageList(paramsMap);
            resultMap.put("code", 0);
            resultMap.put("data", list);
            resultMap.put("msg", null);
            resultMap.put("count", count);
        }else{
            resultMap.put("code", 1);
            resultMap.put("data", "");
            resultMap.put("msg", "no data");
            resultMap.put("count", count);

        }
        return resultMap;
    }

    /**
     * 弹出到新增/编辑页面
     * @param lineId
     * @return
     */
    @RequestMapping("/toLeaveEdit")
    @ResponseBody
    public ModelAndView toAdd(String lineId) {
        LeaveEntity leaveEntity = leaveDAO.selectLeaveByLineId(lineId);
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("techbloom2/otherStorage/leave_edit");
        mv.addObject("leaveEntity", leaveEntity);
        return mv;
    }


    /**
     * 根据主表ID获取主表和行表信息
     * @param id
     * @return
     */
    @RequestMapping("/selectLeaveEntity")
    @ResponseBody
    public LeaveEntity selectLeaveEntity(@RequestParam String id) {
        LeaveEntity leaveEntity = leaveService.findById(id);
        String outCode = leaveEntity.getOutCode();
        Map<String,Object> map = new HashMap<>();
        map.put("out_code",outCode);
        List<LeaveLineEntity> list = leaveLineService.selectByMap(map);
        if(list.size()>0){
            leaveEntity.setLine(list);
        }
        return leaveEntity;

    }
}