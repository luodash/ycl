package com.tbl.modules.wms2.controller.ordermanage.asn;

import cn.hutool.core.date.DateUtil;
import com.tbl.modules.platform.constant.MenuConstant;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.service.system.RoleService;
import com.tbl.modules.wms2.dao.ordermanage.asn.AsnHeaderDAO;
import com.tbl.modules.wms2.dao.ordermanage.asn.AsnLineDAO;
import com.tbl.modules.wms2.entity.ordermanage.asn.*;
import com.tbl.modules.wms2.service.operation.QualityInfoService;
import com.tbl.modules.wms2.service.ordermanage.asn.AsnHeaderService;
import com.tbl.modules.wms2.service.ordermanage.asn.AsnLineService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 送退货单
 *
 * @author DASH
 */
@Controller()
@RequestMapping("/ycl/ordermanage/asn")
public class AsnHeaderController extends AbstractController {

    @Resource
    private RoleService roleService;

    @Resource
    private AsnHeaderService asnHeaderService;

    @Resource
    private AsnHeaderDAO asnHeaderDAO;

    @Resource
    private AsnLineDAO asnLineDAO;

    @Resource
    private AsnLineService asnLineService;

    @Resource
    private QualityInfoService qualityInfoService;


    /**
     * 保存主表 + 子表 +接受表
     *
     * @param
     * @return Map<String, Object>
     */
    @PostMapping(value = "/save")
    @ResponseBody
    public Map<String, Object> save(@RequestBody AsnCreateDTO asn) {
        Map<String, Object> map = new HashMap<>(2);
        boolean result = asnHeaderService.insertAsn(asn);
        map.put("result", result);
        map.put("msg", result ? "操作成功！" : "操作失败！");
        return map;
    }

    /**
     * 批量更新子表和出入库流水
     *
     * @param
     * @return Map<String, Object>
     */
    @PostMapping(value = "/editAsnLineBatch")
    @ResponseBody
    public Map<String, Object> editAsnLineBatch(@RequestBody List<AsnLineUpdateDTO> asnLineUpdateDTOS) {
        Map<String, Object> map = new HashMap<>(2);
        boolean result = asnLineService.editAsnLineBatch(asnLineUpdateDTOS);
        map.put("result", result);
        map.put("msg", result ? "操作成功！" : "操作失败！");
        return map;
    }


    /**
     * 跳转Grid列表页
     *
     * @return
     */
    @RequestMapping(value = "/toList")
    public ModelAndView toRewindList() {
        ModelAndView mv = this.getModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(this.getSessionUser().getRoleId(), MenuConstant.asn));
        mv.setViewName("techbloom2/ordermanage/asn_list");
        return mv;
    }

    /**
     * 弹出到明细页面
     *
     * @param id 入库主表主键
     * @return ModelAndView
     */
    @RequestMapping(value = "/toDetailList")
    @ResponseBody
    public ModelAndView toDetailList(Long id) {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("techbloom2/ordermanage/asn/asn_header_detail");
        mv.addObject("asnHeaderId", id);
        return mv;
    }

    /**
     * 根据订单查询送退货单
     *
     * @param poCode    订单号
     * @param poLineNum 订单行号
     * @return
     */
    @RequestMapping(value = "/listViewHeaderBy")
    @ResponseBody
    public Map<String, Object> listViewHeaderBy(String poCode, String poLineNum) {
        List<AsnHeaderPoVo> asnHeaderEntities = asnHeaderService.listViewHeader(poCode, poLineNum);
        Map<String, Object> map = new HashMap<>(4);
        map.put("code", 0);
        map.put("msg", null);
        map.put("count", asnHeaderEntities.size());
        map.put("data", asnHeaderEntities);
        return map;
    }

    /**
     * 根据订单查询送退货单
     *
     * @param asnHeaderQueryDTO
     * @return
     */
    @RequestMapping(value = "/listViewHeader")
    @ResponseBody
    public Map<String, Object> listViewHeader(AsnHeaderQueryDTO asnHeaderQueryDTO) {
        List<AsnHeaderVo> asnHeaderEntities = asnHeaderService.listViewHeader(asnHeaderQueryDTO);
        Map<String, Object> map = new HashMap<>(4);
        map.put("code", 0);
        map.put("msg", null);
        map.put("count", asnHeaderEntities.size());
        map.put("data", asnHeaderEntities);
        return map;
    }

    @RequestMapping(value = "/listViewLine")
    @ResponseBody
    public Map<String, Object> listViewLine(String asnHeaderIds) {
        List<AsnLineEntity> asnLineEntities = asnLineService.listViewLine(asnHeaderIds);
        Map<String, Object> map = new HashMap<>(4);
        map.put("code", 0);
        map.put("msg", null);
        map.put("count", asnLineEntities.size());
        map.put("data", asnLineEntities);
        return map;
    }

    @PostMapping(value = "/listViewLineListMap")
    @ResponseBody
    public Map<String, Object> listViewLineListMap(@RequestBody List<Map> list) {
        List<AsnLineEntity> asnLineEntities = asnLineService.listViewLineListMap(list);
        Map<String, Object> map = new HashMap<>(4);
        map.put("code", 0);
        map.put("msg", null);
        map.put("count", asnLineEntities.size());
        map.put("data", asnLineEntities);
        return map;
    }

    /**
     * 获取送退货列表
     *
     * @param maps
     * @return
     */
    @PostMapping(value = "/listViewAsnLine")
    @ResponseBody
    public Map<String, Object> listViewAsnLine(@RequestBody Map maps) {
        List<AsnLineDTO> list = asnLineDAO.listViewAsnLine(maps);
        Map<String, Object> map = new HashMap<>(4);
        if (list != null && list.size() > 0) {
            map.put("code", 0);
            map.put("msg", null);
            map.put("count", list.size());
            map.put("data", list);
        } else {
            map.put("code", -1);
            map.put("msg", "没有对应的数据");
            map.put("count", 0);
            map.put("data", null);
        }
        return map;
    }

    /**
     * 根据poLineIds获取送货单
     *
     * @param maps
     * @return
     */
    @PostMapping(value = "/listViewRejectAsnLine")
    @ResponseBody
    public Map<String, Object> listViewRejectAsnLine(@RequestBody Map maps) {
        List<AsnLineDTO> list = new ArrayList<>();
        String[] poLineIds = ((String) maps.get("poLineIds")).split(",");
        Map m = new HashMap();
        for (int i = 0; i < poLineIds.length; i++) {
            m.put("asnType", "1");
            m.put("poLineId", poLineIds[i]);
            list.addAll(asnLineDAO.listViewAsnLine(m));
        }
        Map<String, Object> map = new HashMap<>(4);
        if (list.size() > 0) {
            map.put("code", 0);
            map.put("msg", null);
            map.put("count", list.size());
            map.put("data", list);
        } else {
            map.put("code", -1);
            map.put("msg", "该订单没用对应的送货单");
            map.put("count", list.size());
            map.put("data", list);
        }

        return map;
    }

    @GetMapping(value = "/listViewAsnByPoCode")
    @ResponseBody
    public Map<String, Object> listViewAsnByPoCode(String poCodes) {
        List<AsnCorrectDTO> asns = asnLineService.listViewAsnByPoCode(poCodes);
        Map<String, Object> map = new HashMap<>(4);
        map.put("code", 0);
        map.put("msg", null);
        map.put("count", asns.size());
        map.put("data", asns);
        return map;
    }

    /**
     * 第三栏查询
     * 物料上架显示上架列
     * 未上架现在物料子表
     *
     * @return
     */
    @RequestMapping(value = "/listViewAsnSelect")
    @ResponseBody
    public Map<String, Object> listViewAsnSelect(AsnQueryDTO asnQueryDTO) {
        Map<String, Object> map = new HashMap<>(4);
        List<Map<String, Object>> asns = asnHeaderService.listViewAsnSelect(asnQueryDTO);
        map.put("code", 0);
        map.put("msg", null);
        map.put("count", null);
        map.put("data", asns);
        return map;
    }


    /**
     * 查询送退货详情数据列表
     *
     * @return
     */
    @RequestMapping(value = "/listViewAsn")
    @ResponseBody
    public Map<String, Object> listViewAsn(AsnQueryDTO asnQueryDTO) {
        Map<String, Object> map = new HashMap<>(4);
        List<AsnVO> asns = asnHeaderDAO.listViewAsn(asnQueryDTO);
        map.put("code", 0);
        map.put("msg", null);
        map.put("count", null);
        map.put("data", asns);
        return map;
    }

    /**************************************************************************************************/

    /**
     * SC-查询送退货详情数据列表
     *
     * @return
     */
    @RequestMapping(value = "/listViewAsnSC")
    @ResponseBody
    public Map<String, Object> listViewAsnSC(AsnQueryDTO asnQueryDTO) {
        Map<String, Object> map = new HashMap<>(4);
        List<AsnVO> asns = asnHeaderDAO.listViewAsn(asnQueryDTO);
        Map<String, List<AsnVO>> collect = asns.stream().collect(Collectors.groupingBy(AsnVO::getAsnNumber));
        List list = new ArrayList();
        Iterator it = collect.keySet().iterator();
        while (it.hasNext()) {
            String key = it.next().toString();
            Map map1 = new HashMap();
            map1.put("asnHeader", key);
            map1.put("asnLine", collect.get(key));
            list.add(map1);
        }
        map.put("code", 0);
        map.put("msg", null);
        map.put("count", null);
        map.put("data", list);
        return map;
    }

    /**
     * SC-模糊查询送货单
     *
     * @return
     */
    @RequestMapping(value = "/listViewAsnNumberSC")
    @ResponseBody
    public Map<String, Object> listViewAsnNumberSC(String asnNumber) {
        Map<String, Object> map = new HashMap<>(4);
        List<String> asnNumbers = asnHeaderDAO.listViewAsnNumberSC(asnNumber);
        map.put("code", 0);
        map.put("msg", null);
        map.put("count", null);
        map.put("data", asnNumbers);
        return map;
    }

    /**
     * 保存主表 + 子表
     *
     * @param
     * @return Map<String, Object>
     */
    @PostMapping(value = "/saveSC")
    @ResponseBody
    public Map<String, Object> saveSC(@RequestBody AsnCreateDTO asn) {
        Map<String, Object> map = new HashMap<>(2);
        Boolean result = asnHeaderService.saveSC(asn);
        map.put("result", result);
        map.put("msg", result ? "操作成功！" : "操作失败！");
        return map;
    }

    /**
     * 保存主表 + 子表
     *
     * @param
     * @return Map<String, Object>
     */
    @PostMapping(value = "/saveTHSC")
    @ResponseBody
    public Map<String, Object> saveTHSC(@RequestBody List<AsnCreateDTO> asnCreateDTOS) {
        Map<String, Object> map = new HashMap<>(2);
        Boolean result = asnHeaderService.saveTHSC(asnCreateDTOS);
        map.put("result", result);
        map.put("msg", result ? "操作成功！" : "操作失败！");
        return map;
    }

    /**
     * SC-拒收
     *
     * @return
     */
    @RequestMapping(value = "/rejectLine")
    @ResponseBody
    public Map<String, Object> rejectLine(String asnLineId) {
        Map<String, Object> map = new HashMap<>(4);
        Boolean result = asnHeaderService.rejectLine(asnLineId);
        map.put("result", result);
        map.put("msg", result ? "操作成功！" : "操作失败！");
        return map;
    }

    /**
     * 打印
     *
     * @param asnQueryDTO
     * @return
     */
    @RequestMapping(value = "/printByIds")
    @ResponseBody
    public Map<String, Object> printByIds(AsnQueryDTO asnQueryDTO) {

        List<AsnVO> asns = asnHeaderDAO.listViewAsn(asnQueryDTO);

        AsnVO asnVO = null;
        if (asns != null || asns.size() > 0) {
            asnVO = asns.get(0);
        }

        String s = "CT~~CD,~CC^~CT~\n" +
                "^XA\n" +
                "~TA008\n" +
                "~JSN\n" +
                "^LT0\n" +
                "^MNN\n" +
                "^MTT\n" +
                "^PON\n" +
                "^PMN\n" +
                "^LH0,0\n" +
                "^JMA\n" +
                "^PR2,2\n" +
                "~SD15\n" +
                "^JUS\n" +
                "^LRN\n" +
                "^CI27\n" +
                "^PA0,1,1,0\n" +
                "^XZ\n" +
                "^XA\n" +
                "^MMT\n" +
                "^PW575\n" +
                "^LL400\n" +
                "^LS0\n" +
                "^FO5,1^GB566,397,2^FS\n" +
                "^FO36,8^GB0,392,2^FS\n" +
                "^FT27,387^A0B,23,23^FH\\^CI28^FD采购单号^FS^CI27\n" +
                "^FO68,8^GB0,392,2^FS\n" +
                "^FO166,8^GB0,392,2^FS\n" +
                "^FT60,387^A0B,23,23^FH\\^CI28^FD物料编号^FS^CI27\n" +
                "^FT125,387^A0B,23,23^FH\\^CI28^FD物料名称^FS^CI27\n" +
                "^FO199,5^GB0,395,2^FS\n" +
                "^FT190,387^A0B,23,23^FH\\^CI28^FD供方批次^FS^CI27\n" +
                "^FO231,5^GB0,395,2^FS\n" +
                "^FT222,387^A0B,23,23^FH\\^CI28^FD远东批次^FS^CI27\n" +
                "^FO264,5^GB0,395,2^FS\n" +
                "^FT255,387^A0B,23,23^FH\\^CI28^FD生产日期^FS^CI27\n" +
                "^FT255,152^A0B,23,23^FH\\^CI28^FD有效期^FS^CI27\n" +
                "^FT95,276^A0B,23,23^FH\\^CI28^FD高强度镀锌钢绞线^FS^CI27\n" +
                "^FT124,276^A0B,23,23^FH\\^CI28^FD高强度镀锌钢绞线^FS^CI27\n" +
                "^FT153,276^A0B,23,23^FH\\^CI28^FD高强度镀锌钢绞线^FS^CI27\n" +
                "^FT255,279^A0B,23,23^FH\\^CI28^FD" + DateUtil.format(asnVO.getCreationDate(), "yyyy/MM/dd") + "^FS^CI27\n" +
                "^FT255,68^A0B,23,23^FH\\^CI28^FD6666^FS^CI27\n" +
                "^FO304,5^GB0,395,2^FS\n" +
                "^FT296,368^A0B,23,23^FH\\^CI28^FD毛重^FS^CI27\n" +
                "^FO345,5^GB0,395,2^FS\n" +
                "^FT336,368^A0B,23,23^FH\\^CI28^FD净重^FS^CI27\n" +
                "^FO232,159^GB114,0,2^FS\n" +
                "^FO232,71^GB114,0,2^FS\n" +
                "^FT295,143^A0B,23,23^FH\\^CI28^FD皮重^FS^CI27\n" +
                "^FT333,144^A0B,23,23^FH\\^CI28^FD长度^FS^CI27\n" +
                "^FO415,8^GB0,392,2^FS\n" +
                "^FT391,376^A0B,23,23^FH\\^CI28^FD供应商^FS^CI27\n" +
                "^FT424,405^BQN,2,5\n" +
                "^FH\\^FDLA,L99999999999999999999666\\0D\\0A^FS\n" +
                "^FT374,276^A0B,23,23^FH\\^CI28^FD" + asnVO.getVendorName() + "^FS^CI27\n" +
                "^FT555,376^A0B,20,20^FB352,1,5,C^FH\\^CI28^FDL99999999999999999999666^FS^CI27\n" +
                "^FO8,279^GB408,0,2^FS\n" +
                "^FT31,279^A0B,23,23^FH\\^CI28^FD " + asnVO.getPoCode() + "^FS^CI27\n" +
                "^FT63,279^A0B,23,23^FH\\^CI28^FD" + asnVO.getMaterialCode() + "^FS^CI27\n" +
                "^FT191,276^A0B,23,23^FH\\^CI28^FD" + asnVO.getLotsNum() + "^FS^CI27\n" +
                "^FT223,276^A0B,23,23^FH\\^CI28^FD" + asnVO.getVendorId() + asnVO.getLotsNum() + "^FS^CI27\n" +
                "^FT295,272^A0B,23,23^FH\\^CI28^FD" + asnVO.getShipQuantity() + "^FS^CI27\n" +
                "^FT295,64^A0B,23,23^FH\\^CI28^FD" + "" + "^FS^CI27\n" +
                "^FT335,272^A0B,23,23^FH\\^CI28^FD" + "" + "^FS^CI27\n" +
                "^FT335,64^A0B,23,23^FH\\^CI28^FD" + "" + "^FS^CI27\n" +
                "^PQ1,0,1,Y\n" +
                "^XZ\n";

        Map<String, Object> map = new HashMap<>(4);
        map.put("code", 0);
        map.put("msg", null);
        map.put("count", null);
        map.put("data", s);

        return map;
    }


}
