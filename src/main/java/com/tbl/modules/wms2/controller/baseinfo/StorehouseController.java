package com.tbl.modules.wms2.controller.baseinfo;

import com.alibaba.fastjson.JSON;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.platform.constant.MenuConstant;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.service.system.RoleService;
import com.tbl.modules.wms2.entity.baseinfo.StorehouseEntity;
import com.tbl.modules.wms2.entity.baseinfo.WarehouseEntity;
import com.tbl.modules.wms2.service.baseinfo.StorehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.*;

/**
 * 库位信息表
 */
@Controller("shelf")
@RequestMapping("wms2/baseinfo/shelf")
public class StorehouseController extends AbstractController {
    @Resource
    private RoleService roleService;

    @Resource
    private StorehouseService storehouseService;

    /**
     * 弹窗新增界面
     * @param id
     * @return
     */
    @RequestMapping("/toAdd.do")
    @ResponseBody
    public ModelAndView toAdd(Long id){
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("techbloom2/baseinfo/shelf/shelf_edit");
        mv.addObject("shelf",storehouseService.findById(id));
        return mv;
    }

    /**
     *  提交新增。
     * @param
     * @return Map<String, Object>
     */
    @RequestMapping(value = "/save")
    @ResponseBody
    public Map<String, Object> save(StorehouseEntity storehouseEntity) {
        Map<String, Object> map = new HashMap<>(2);
//        car.setWarehouseArea(car.getWarehouseArea().replaceAll("\\d+",""));
        boolean result = storehouseService.save(storehouseEntity);
        map.put("result", result);
        map.put("msg", result ? "保存成功！" : "保存失败！");
        return map;
    }


    /**
     * 跳转Grid列表页
     * @return
     */
    @RequestMapping(value = "/toList")
    public ModelAndView toRewindList() {
        ModelAndView mv = this.getModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(getSessionUser().getRoleId(), MenuConstant.goods));
        mv.setViewName("techbloom2/baseinfo/shelf/shelf_list");
        return mv;
    }


    /**
     * 获取Grid数据
     * @param queryJsonString
     * @return
     */
    @RequestMapping(value = "/list.do")
    @ResponseBody
    public Map<String, Object> list(String queryJsonString) {
        Map<String, Object> map = new HashMap<>(4);
        if (!StringUtils.isEmptyString(queryJsonString)) {
            map = JSON.parseObject(queryJsonString);
        }
        //okk
        map.put("storehouseCode",Arrays.asList(getSessionUser().getFactoryCode().split(",")));
        PageTbl page = this.getPage();
        PageUtils utils = storehouseService.getPageList(page,map);
        page.setTotalRows(utils.getTotalCount() == 0 ? 1 : utils.getTotalCount());
        //初始化分页对象
        executePageMap(map, page);
        map.put("rows", utils.getList());
        map.put("total", utils.getTotalPage() == 0 ? 1 : utils.getTotalPage());
        return map;
    }

    /**
     * 跳转编辑页
     *
     * @return
     */
    @RequestMapping(value = "/toEdit.do")
    public ModelAndView toDetail(Long id) {
        ModelAndView mv = this.getModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(2L,
                MenuConstant.goods));
        mv.setViewName("techbloom2/baseinfo/shelf/shelf_edit");
        return mv;
    }


}
