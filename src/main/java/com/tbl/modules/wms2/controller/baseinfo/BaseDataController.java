package com.tbl.modules.wms2.controller.baseinfo;

import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.service.system.RoleService;
import com.tbl.modules.wms2.dao.baseinfo.BaseDataDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 基础数据
 *
 * @author DASH
 */
@Controller
@RequestMapping(value = "/ycl/baseinfo/basedata")
public class BaseDataController extends AbstractController {

    @Autowired
    private RoleService roleService;

    @Resource
    private BaseDataDao baseDataDao;



    /**
     * 接口描述：获取公司（厂区）-下拉框列表
     * @param entityId
     * @return
     */
    @RequestMapping(value = "/area/{entityId}",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAreaList(@PathVariable("entityId") Integer entityId) {
        List<Map<String,Object>> list = baseDataDao.getAreaList(entityId);
        if(list.size()>0){
            return restfulResponse(list);
        }else{
            return restfulResponseError();
        }
    }


    /**
     * 接口描述: 获取仓库-下拉框
     * @param entityId
     * @return
     */
    @RequestMapping(value = "/store/{entityId}",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getStoreList(@PathVariable("entityId") Integer entityId) {
        List<Map<String,Object>> list = baseDataDao.getStoreByEntityId(entityId);
        if(list.size()>0){
            return restfulResponse(list);
        }else{
            return restfulResponseError();
        }
    }

    /**
     * 接口描述: 获取仓库-下拉框
     *
     * @param organizationId 组织ID
     * @return
     */
    @RequestMapping(value = "/store/organizationId/{organizationId}", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getStoreListByOrganizationId(@PathVariable("organizationId") Integer organizationId) {
        List<Map<String, Object>> list = baseDataDao.getStoreByOrganizationId(organizationId);
        return restfulResponse(list);
    }

    @RequestMapping(value = "/plant/{entityId}",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getPlantList(@PathVariable("entityId") Integer entityId) {
        List<Map<String,Object>> list = baseDataDao.getPlantByEntityId(entityId);
        if(list.size()>0){
            return restfulResponse(list);
        }else{
            return restfulResponseError();
        }
    }


}


