package com.tbl.modules.wms2.controller.report;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.service.system.RoleService;
import com.tbl.modules.wms2.entity.production.YclMaterialBackEntity;
import com.tbl.modules.wms2.service.report.ReturnReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/ycl/report/return")
public class ReturnReportController extends AbstractController {

    @Autowired
    private RoleService roleService;

    @Autowired
    private ReturnReportService returnReportService;

    /**
     * 保存
     * @param yclMaterialBackEntity
     * @return
     */
    @PostMapping(value = "/save")
    @ResponseBody
    public Map<String, Object> save(@RequestBody YclMaterialBackEntity yclMaterialBackEntity) {
        yclMaterialBackEntity.setCreateTime(new Date());
        boolean result = returnReportService.save(yclMaterialBackEntity);
        Map resultMap = new HashMap(2);
        resultMap.put("result", result);
        resultMap.put("msg", result ? "操作成功！" : "操作失败！");
        return resultMap;
    }


    /**
     * 删除
     *
     * @param
     * @return Map<String, Object>
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Map<String, Object> delete(String ids) {
        Map<String, Object> map = new HashMap<>(2);
        boolean result = returnReportService.delete(ids);
        map.put("result", result);
        map.put("msg", result ? "操作成功！" : "操作失败！");
        return map;
    }

/**
 * 弹出到新增/编辑页面
 *
 * @param id
 * @return ModelAndView
 */
    @RequestMapping(value = "/toEdit")
    @ResponseBody
    public ModelAndView toAdd(String id) {
        ModelAndView mv = this.getModelAndView();
        YclMaterialBackEntity yclMaterialBackEntity = null;
        if (StrUtil.isNotEmpty(id)) {
             yclMaterialBackEntity = returnReportService.findById(id);
        }
        mv.setViewName("techbloom2/report/return/edit");
        mv.addObject("yclMaterialBackEntity", yclMaterialBackEntity);
        return mv;
    }


    /**
     * 主表 跳转Grid列表页
     *
     * @return
     */
    @RequestMapping(value = "/toList")
    public ModelAndView toRewindList() {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("techbloom2/report/return/list");
        return mv;
    }

    /**
     * 主表-获取Grid列表数据
     * @return Map<String, Object>
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Map<String, Object> list(YclMaterialBackEntity yclMaterialBackEntity) {
        Map<String, Object> paramsMap = getAllParameter();
        Page<YclMaterialBackEntity> mapPage = returnReportService.getPageList(
                new Page(
                        Integer.parseInt((String) paramsMap.get("page")),
                        Integer.parseInt((String) paramsMap.get("limit"))),
                paramsMap,yclMaterialBackEntity);
        return returnLayResponse(mapPage);
    }
}
