package com.tbl.modules.wms2.controller.baseinfo;

import com.alibaba.fastjson.JSON;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.platform.constant.MenuConstant;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.service.system.RoleService;
import com.tbl.modules.wms2.entity.baseinfo.WarehouseEntity;
import com.tbl.modules.wms2.service.baseinfo.WarehouseService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;

/**
 * 仓库
 *
 * @author DASH
 */
@Controller
@RequestMapping(value = "/wms2/baseinfo/warehouse")
public class WarehouseController extends AbstractController {

    @Autowired
    private RoleService roleService;

    @Autowired
    private WarehouseService warehouseService;

    /**
     * 弹出到新增/编辑页面
     *
     * @param id
     * @return ModelAndView
     */
    @RequestMapping(value = "/toAdd.do")
    @ResponseBody
    public ModelAndView toAdd(Long id) {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("techbloom2/baseinfo/warehouse/edit");
//        mv.addObject("edit", id != null && id != EmumConstant.carState.INSTORAGING.getCode().longValue() ? EmumConstant.carState.OUTSTORAGING
//                : EmumConstant.carState.INSTORAGING);
        mv.addObject("warehouse", warehouseService.findById(id));
//        mv.addObject("warehouseAreaList" , carService.selectWarehouseArea());
        return mv;
    }

    /**
     * 保存
     *
     * @param
     * @return Map<String, Object>
     */
    @RequestMapping(value = "/save")
    @ResponseBody
    public Map<String, Object> save(WarehouseEntity warehouseEntity) {
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>");
        System.out.println(warehouseEntity);
        Map<String, Object> map = new HashMap<>(2);
//        car.setWarehouseArea(car.getWarehouseArea().replaceAll("\\d+",""));
        boolean result = warehouseService.save(warehouseEntity);
        map.put("result", result);
        map.put("msg", result ? "保存成功！" : "保存失败！");
        return map;
    }

    /**
     * 跳转Grid列表页
     *
     * @return
     */
    @RequestMapping(value = "/toList")
    public ModelAndView toRewindList() {
        ModelAndView mv = this.getModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(2L, MenuConstant.warehouselist));
        mv.setViewName("techbloom2/baseinfo/warehouse/list");
        return mv;
    }

    /**
     * 获取Grid列表数据
     *
     * @param queryJsonString 查询条件
     * @return Map<String, Object>
     */
    @RequestMapping(value = "/list.do")
    @ResponseBody
    public Map<String, Object> list(String queryJsonString) {
        Map<String, Object> map = new HashMap<>(4);
        if (!StringUtils.isEmptyString(queryJsonString)) {
            map = JSON.parseObject(queryJsonString);
        }
        //okk
        map.put("areaCode", Arrays.asList(getSessionUser().getFactoryCode().split(",")));
        PageTbl page = this.getPage();
        PageUtils utils = warehouseService.getPageList(page, map);
        page.setTotalRows(utils.getTotalCount() == 0 ? 1 : utils.getTotalCount());
        //初始化分页对象
        executePageMap(map, page);
        map.put("rows", utils.getList());
        map.put("total", utils.getTotalPage() == 0 ? 1 : utils.getTotalPage());
        return map;
    }

    /**
     * 跳转编辑页
     *
     * @return
     */
    @RequestMapping(value = "/toEdit.do")
    public ModelAndView toDetail(Long id) {
        ModelAndView mv = this.getModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(2L, MenuConstant.warehouselist));
        mv.setViewName("techbloom2/baseinfo/warehouse/edit");
        return mv;
    }


    /**
     * 获取装车通知单-下拉框
     * @param num
     * @return
     */
    @RequestMapping(value = "/entruckNotice/{num}")
    @ResponseBody
    public Map<String,Object> entruckNotice(@PathVariable("num")String num){
        return warehouseService.entruckNotice(num);
    }


    /**
     * 获取EBS中的库位信息
     * @param organizationId
     * @param storeCode
     * @return
     */
    @RequestMapping(value = "/ebsLocation")
    @ResponseBody
    public Map<String,Object> getLocationfromEBS(Integer organizationId,String storeCode,Integer id,String name){
        Map<String,Object> map = getAllParameter();
        return warehouseService.getLocationfromEBS(map);
    }


}


