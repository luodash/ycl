package com.tbl.modules.wms2.controller.purchasein;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.service.system.RoleService;
import com.tbl.modules.wms2.entity.operation.YclInOutFlowEntity;
import com.tbl.modules.wms2.entity.otherStorage.LeaveEntity;
import com.tbl.modules.wms2.service.purchasein.FactoryOrderService;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 物资出厂单
 */

@Controller
@RequestMapping("/ycl/purchasein/factory")
public class FactoryOrderController extends AbstractController {

    @Autowired
    private RoleService roleService;

    @Autowired
    private FactoryOrderService factoryOrderService;

    /**
     * 删除
     *
     * @param
     * @return Map<String, Object>
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Map<String, Object> delete(String ids) {
        Map<String, Object> map = new HashMap<>(2);
        boolean result = factoryOrderService.delete(ids);
        map.put("result", result);
        map.put("msg", result ? "操作成功！" : "操作失败！");
        return map;
    }

    /**
     * 保存
     *
     * @param
     * @return Map<String, Object>
     */
    @PostMapping(value = "/save")
    @ResponseBody
    public Map<String, Object> save(@RequestBody YclInOutFlowEntity yclInOutFlowEntity) {
        yclInOutFlowEntity.setCreateTime(DateUtil.date());
        boolean result = factoryOrderService.save(yclInOutFlowEntity);
        Map resultMap = new HashMap(2);
        resultMap.put("result", result);
        resultMap.put("msg", result ? "操作成功！" : "操作失败！");
        return resultMap;
    }

    /**
     * 弹出到新增/编辑页面
     *
     * @param id
     * @return ModelAndView
     */
    @RequestMapping(value = "/toEdit")
    @ResponseBody
    public ModelAndView toAdd(String id) {
        ModelAndView mv = this.getModelAndView();
        T t = null;
        if (StrUtil.isNotEmpty(id)) {
            // yclInOutFlowEntity = yclInOutFlowService.findById(id);
        }
        mv.setViewName("techbloom2/purchaseIn/factoryOrder/edit");
        mv.addObject("t", t);
        return mv;
    }

    /**
     * 主表 跳转Grid列表页
     */
    @RequestMapping(value = "/toList")
    public ModelAndView toRewindList() {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("techbloom2/purchaseIn/factoryOrder/list");
        return mv;
    }
    /**
     * 主表 跳转Grid列表页
     */
    @RequestMapping(value = "/toPrint")
    public ModelAndView toPrint(String objStr) {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("techbloom2/purchaseIn/factoryOrder/print");
        return mv;
    }
    /**
     * 主表 跳转Grid列表页
     */
    @RequestMapping(value = "/printView")
    public ModelAndView printView(String objStr) {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("techbloom2/purchaseIn/factoryOrder/printView");
        return mv;
    }

    /**
     * 主表-获取Grid列表数据
     * @return Map<String, Object>
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Map<String, Object> list(YclInOutFlowEntity yclInOutFlowEntity) {
        Map<String, Object> paramsMap = getAllParameter();
        Page<YclInOutFlowEntity> mapPage = factoryOrderService.getPageList(
                new Page(
                        Integer.parseInt((String) paramsMap.get("page")),
                        Integer.parseInt((String) paramsMap.get("limit"))),
                paramsMap,yclInOutFlowEntity);
        System.out.println(mapPage);
        return returnLayResponse(mapPage);
    }
}
