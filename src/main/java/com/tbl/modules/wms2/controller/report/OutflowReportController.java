package com.tbl.modules.wms2.controller.report;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.service.system.RoleService;
import com.tbl.modules.wms2.dao.operation.YclInOutFlowDao;
import com.tbl.modules.wms2.dao.report.OutflowReportDao;
import com.tbl.modules.wms2.entity.operation.YclInOutFlowEntity;
import com.tbl.modules.wms2.service.operation.YclInOutFlowService;
import com.tbl.modules.wms2.service.report.OutflowReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 出库流水统计报表
 */
@Controller
@RequestMapping("/ycl/report/outflow")
public class OutflowReportController extends AbstractController {

    @Autowired
    private RoleService roleService;

    @Autowired
    private YclInOutFlowService yclInOutFlowService;

    @Autowired
    private YclInOutFlowDao yclInOutFlowDao;

    @Autowired
    private OutflowReportDao outflowReportDao;

    @Autowired
    private OutflowReportService outflowReportService;


    /**
     * 删除
     *
     * @param
     * @return Map<String, Object>
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Map<String, Object> delete(String ids) {
        Map<String, Object> map = new HashMap<>(2);
        boolean result = yclInOutFlowService.delete(ids);
        map.put("result", result);
        map.put("msg", result ? "操作成功！" : "操作失败！");
        return map;
    }

    /**
     * 保存
     *
     * @param
     * @return Map<String, Object>
     */
    @PostMapping(value = "/save")
    @ResponseBody
    public Map<String, Object> save(@RequestBody YclInOutFlowEntity yclInOutFlowEntity) {
        yclInOutFlowEntity.setCreateTime(DateUtil.date());
        boolean result = yclInOutFlowService.save(yclInOutFlowEntity);
        Map resultMap = new HashMap(2);
        resultMap.put("result", result);
        resultMap.put("msg", result ? "操作成功！" : "操作失败！");
        return resultMap;
    }

    /**
     * 弹出到新增/编辑页面
     *
     * @param id
     * @return ModelAndView
     */
    @RequestMapping(value = "/toEdit")
    @ResponseBody
    public ModelAndView toAdd(String id) {
        ModelAndView mv = this.getModelAndView();
        YclInOutFlowEntity yclInOutFlowEntity = null;
        if (StrUtil.isNotEmpty(id)) {
           // yclInOutFlowEntity = yclInOutFlowService.findById(id);
        }
        mv.setViewName("techbloom2/report/outflow/edit");
        mv.addObject("yclInventoryEntity", yclInOutFlowEntity);
        return mv;
    }

    /**
     * 主表 跳转Grid列表页
     *
     * @return
     */
    @RequestMapping(value = "/toList")
    public ModelAndView toRewindList() {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("techbloom2/report/outflow/list");
        return mv;
    }

    /**
     * 主表-获取Grid列表数据
     * @return Map<String, Object>
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Map<String, Object> list(YclInOutFlowEntity yclInOutFlowEntity) {
        Map<String, Object> paramsMap = getAllParameter();
        Page<YclInOutFlowEntity> mapPage = outflowReportService.getPageList(
                new Page(
                        Integer.parseInt((String) paramsMap.get("page")),
                        Integer.parseInt((String) paramsMap.get("limit"))),
                paramsMap,yclInOutFlowEntity);
        System.out.println(mapPage);
        return returnLayResponse(mapPage);
    }

    @RequestMapping("/toPrint")
    @ResponseBody
    public ModelAndView toPrint(){
        ModelAndView mv = new ModelAndView();
        mv.setViewName("techbloom2/report/outflow/print");
        return mv;
    }
}
