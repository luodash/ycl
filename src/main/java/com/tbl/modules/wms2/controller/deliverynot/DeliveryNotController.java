package com.tbl.modules.wms2.controller.deliverynot;

import com.alibaba.fastjson.JSON;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.platform.constant.MenuConstant;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.service.system.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;

/**
 * 送货单
 */
@Controller("/deliveryNot")
@RequestMapping("wms2/baseinfo/deliverynot")
public class DeliveryNotController extends AbstractController {
    @Autowired
    private RoleService roleService;


    /**
     * 弹出到详情页面
     *
     * @param id
     * @return ModelAndView
     */
    @RequestMapping(value = "/toDetail.do")
    @ResponseBody
    public ModelAndView toDetail(Long id) {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("techbloom2/baseinfo/deliverynot/detail");
        return mv;
    }

    /**
     * 弹窗新增
     * @param id
     * @return
     */
    @RequestMapping("/toAdd.do")
    @ResponseBody
    public ModelAndView toAdd(Long id){
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("techbloom2/baseinfo/deliverynot/deliveryNot_detail");
        return mv;
    }


    /**
     * 跳转Grid列表页
     * @return
     */
    @RequestMapping(value = "/toList")
    public ModelAndView toRewindList() {
        ModelAndView mv = this.getModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(2L, MenuConstant.warehouselist));//因为没有送货单，所以使用出货单代替
        mv.setViewName("techbloom2/baseinfo/deliverynot/deliveryNot_list");
        return mv;
    }


    /**
     *
     * @param queryJsonString
     * @return
     */
    @RequestMapping(value = "/list.do")
    @ResponseBody
    public Map<String, Object> list(String queryJsonString) {
        Map<String, Object> map = new HashMap<>(4);
        if (!StringUtils.isEmptyString(queryJsonString)) {
            map = JSON.parseObject(queryJsonString);
        }

        List<Map<String, Object>> list = new ArrayList<>();
        for (int i = 0; i < 11; i++) {
            Map map2 = new HashMap(14);
            map2.put("id",i);
            map2.put("q", "已签收");
            map2.put("w","上上");
            map2.put("e", "ss100"+i);
            map2.put("r", "2010-04-01");
            map2.put("t", 0000 + i);
            map2.put("a", "订单" + i);
            map2.put("b",i);
            map2.put("c","顺丰");
            map2.put("d", "老远东");
            map2.put("h", "销售员"+i);
            map2.put("f", i);
            map2.put("g", i);
            list.add(map2);
        }

        map.put("factorycode", Arrays.asList("92", "93", "94", "95"));
        map.put("rows", list);
        map.put("records", 11);
        map.put("page", 1);
        map.put("total", 11);
        return map;
    }
}
