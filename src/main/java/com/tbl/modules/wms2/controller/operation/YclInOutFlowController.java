package com.tbl.modules.wms2.controller.operation;

import cn.hutool.Hutool;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.tbl.common.utils.DateUtils;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.wms2.dao.operation.QualityInfoDAO;
import com.tbl.modules.wms2.dao.operation.YclInOutFlowDao;
import com.tbl.modules.wms2.entity.operation.QualityInfoEntity;
import com.tbl.modules.wms2.entity.operation.YclInOutFlowEntity;
import com.tbl.modules.wms2.entity.ordermanage.asn.AsnLineEntity;
import com.tbl.modules.wms2.entity.ordermanage.delivery.DeliveryEntity;
import com.tbl.modules.wms2.service.operation.QualityInfoService;
import com.tbl.modules.wms2.service.operation.YclInOutFlowService;
import com.tbl.modules.wms2.service.ordermanage.asn.AsnLineService;
import com.tbl.modules.wms2.service.ordermanage.delivery.DeliveryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 出入库流水
 *
 * @author DASH
 */
@Controller
@RequestMapping(value = "/ycl/operation/yclInOutFlow")
public class YclInOutFlowController extends AbstractController {

    @Resource
    YclInOutFlowService yclInOutFlowService;

    @Resource
    YclInOutFlowDao yclInOutFlowDao;

    @Resource
    AsnLineService asnLineService;

    @Resource
    QualityInfoDAO qualityInfoDAO;

    @Autowired
    QualityInfoService qualityInfoService;

    @Autowired
    DeliveryService deliveryService;

    /**
     * 保存
     *
     * @param
     * @return Map<String, Object>
     */
    @PostMapping(value = "/save")
    @ResponseBody
    public Map<String, Object> save(@RequestBody YclInOutFlowEntity yclInOutFlowEntity) {

        Map<String, Object> map = new HashMap<>(2);
        boolean result = yclInOutFlowService.save(yclInOutFlowEntity);
        map.put("result", result);
        map.put("msg", result ? "操作成功！" : "操作失败！");
        return map;
    }

    /**
     * 批量保存
     *
     * @param
     * @return Map<String, Object>
     */
    @PostMapping(value = "/saveBatch")
    @ResponseBody
    public Map<String, Object> saveBatch(@RequestBody List<YclInOutFlowEntity> yclInOutFlowEntities) {
        Map<String, Object> map = new HashMap<>(2);
        Boolean b = yclInOutFlowService.saveBatch(yclInOutFlowEntities);
        map.put("result", b);
        map.put("msg", b ? "操作成功！" : "操作失败！");
        return map;
    }

    /**
     * 删除
     *
     * @param
     * @return Map<String, Object>
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Map<String, Object> delete(String ids) {
        Map<String, Object> map = new HashMap<>(2);
        boolean result = yclInOutFlowService.delete(ids);
        map.put("result", result);
        map.put("msg", result ? "操作成功！" : "操作失败！");
        return map;
    }

    /**
     * 跳转编辑页
     *
     * @return
     */
    @RequestMapping(value = "/toEdit")
    public ModelAndView toDetail(String id) {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("techbloom2/operation/inOutFlow/edit");
        return mv;
    }

    /**
     * 获取Grid列表数据 不分页
     *
     * @return Map<String, Object>
     */
    @RequestMapping(value = "/listView")
    @ResponseBody
    public Map<String, Object> listView(YclInOutFlowEntity yclInOutFlowEntity) {
        List<YclInOutFlowEntity> list = yclInOutFlowService.listView(yclInOutFlowEntity);
        Map<String, Object> map = new HashMap<>(4);
        map.put("code", 0);
        map.put("msg", null);
        map.put("count", 0);
        map.put("data", list);
        return map;
    }

    /**
     * 分页
     *
     * @return Map<String, Object>
     */
    @RequestMapping(value = "/pageView")
    @ResponseBody
    public Map<String, Object> pageView() {

        Map<String, Object> paramsMap = getAllParameter();
        List<YclInOutFlowEntity> pageList = yclInOutFlowDao.selectList(null);
        Map<String, Object> map = new HashMap<>(4);
        map.put("code", 0);
        map.put("msg", null);
        map.put("count", pageList.size());
        map.put("data", pageList);
        return map;
    }


}
