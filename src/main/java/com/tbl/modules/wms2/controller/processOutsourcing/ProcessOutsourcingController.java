package com.tbl.modules.wms2.controller.processOutsourcing;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.service.system.RoleService;
import com.tbl.modules.wms2.dao.processOutsourcing.ProcessOutsourcingDao;
import com.tbl.modules.wms2.entity.operation.YclInOutFlowEntity;
import com.tbl.modules.wms2.entity.processOutsourcing.ProcessOutsourcingEntity;
import com.tbl.modules.wms2.entity.report.YclInventoryEntity;
import com.tbl.modules.wms2.service.operation.YclInOutFlowService;
import com.tbl.modules.wms2.service.processOutsourcing.ProcessOutsourcingService;
import com.tbl.modules.wms2.service.report.WarehousingReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 工序外协
 */
@Controller
@RequestMapping("/ycl/processOutsourcing/processOutsourcing")
public class ProcessOutsourcingController extends AbstractController {
    @Autowired
    private RoleService roleService;

    @Autowired
    private ProcessOutsourcingService processOutsourcingService;

    @Autowired
    private ProcessOutsourcingDao processOutsourcingDao;

    @Autowired
    private WarehousingReportService warehousingReportService;


    /**
     * 删除
     *
     * @param
     * @return Map<String, Object>
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Map<String, Object> delete(String ids) {
        Map<String, Object> map = new HashMap<>(2);
        boolean result = processOutsourcingService.delete(ids);
        map.put("result", result);
        map.put("msg", result ? "操作成功！" : "操作失败！");
        return map;
    }

    /**
     * 保存
     *
     * @param
     * @return Map<String, Object>
     */
    @PostMapping(value = "/save")
    @ResponseBody
    public Map<String, Object> save(@RequestBody ProcessOutsourcingEntity processOutsourcingEntity) {
      //  processOutsourcingEntity.setCreateTime(DateUtil.toLocalDateTime(new Date()));
        boolean result = processOutsourcingService.save(processOutsourcingEntity);
        Map resultMap = new HashMap(2);
        resultMap.put("result", result);
        resultMap.put("msg", result ? "操作成功！" : "操作失败！");
        return resultMap;
    }

    /**
     * 弹出到新增/编辑页面
     *
     * @param id
     * @return ModelAndView
     */
    @RequestMapping(value = "/toEdit")
    @ResponseBody
    public ModelAndView toAdd(String id) {
        ModelAndView mv = this.getModelAndView();
        YclInOutFlowEntity yclInOutFlowEntity = null;
        if (StrUtil.isNotEmpty(id)) {
           //  yclInOutFlowEntity = processOutsourcingService.findById(Long.parseLong(id));
        }
        mv.setViewName("techbloom2/processOutsourcing/show");
        mv.addObject("yclInventoryEntity", yclInOutFlowEntity);
        return mv;
    }

    /**
     * 主表 跳转Grid列表页
     *
     * @return
     */
    @RequestMapping(value = "/toList")
    public ModelAndView toRewindList() {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("techbloom2/processOutsourcing/list");
        return mv;
    }

    /**
     * 主表-获取Grid列表数据
     * @return Map<String, Object>
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Map<String, Object> list() {
        Map<String, Object> paramsMap = getAllParameter();
        List<ProcessOutsourcingEntity> pageList = processOutsourcingDao.getPageList(
                new Page(
                        Integer.parseInt((String) paramsMap.get("page")),
                        Integer.parseInt((String) paramsMap.get("limit"))),
                paramsMap);
        Map<String,Object> map = new HashMap<>(4);
        Integer count = processOutsourcingDao.count(map);
        map.put("code", 0);
        map.put("msg", null);
        map.put("count", count);
        map.put("data", pageList);
        return map;
    }
}
