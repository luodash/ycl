package com.tbl.modules.wms2.controller.outsourcing;

import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.platform.constant.MenuConstant;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.service.system.RoleService;
import com.tbl.modules.wms2.dao.purchasein.PurchaseOrderDAO;
import com.tbl.modules.wms2.entity.insidewarehouse.check.YclCheckEntity;
import com.tbl.modules.wms2.entity.purchasein.PurchaseOrderEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 委外加工
 * 工单处理
 *
 * @author DASH
 */
@Controller
@RequestMapping(value = "/ycl/outsourcing/order")
public class OrderController extends AbstractController {


    @Autowired
    private RoleService roleService;

    @Autowired
    private PurchaseOrderDAO purchaseOrderDAO;


    /**
     * 弹出到新增/编辑页面
     *
     * @param id
     * @return ModelAndView
     */
    @RequestMapping(value = "/toDeliveryOrderEdit")
    @ResponseBody
    public ModelAndView toAdd(Long id, WebRequest request) {
        String purchaseOrderIds = request.getParameter("purchaseOrderIds");
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("techbloom2/purchaseIn/purchaseOrder/deliveryOrderEdit");
        mv.addObject("purchaseOrderIds", purchaseOrderIds);

        Map<String, Object> paramsMap = new HashMap<>(1);
        paramsMap.put("poLineIds", purchaseOrderIds);
        List<PurchaseOrderEntity> pageList = purchaseOrderDAO.getPurchaseOrderList(paramsMap);
        mv.addObject("vendorName", pageList.get(0).getSupplierName());
//        mv.addObject("edit", id != null && id != EmumConstant.carState.INSTORAGING.getCode().longValue() ? EmumConstant.carState.OUTSTORAGING
//                : EmumConstant.carState.INSTORAGING);
//        mv.addObject("warehouse", checkService.findById(id));
//        mv.addObject("warehouseAreaList" , carService.selectWarehouseArea());
        return mv;
    }

    /**
     * 保存
     *
     * @param
     * @return Map<String, Object>
     */
    @RequestMapping(value = "/save")
    @ResponseBody
    public Map<String, Object> save(YclCheckEntity checkEntity) {
        Map<String, Object> map = new HashMap<>(2);
//        car.setWarehouseArea(car.getWarehouseArea().replaceAll("\\d+",""));
//        boolean result = checkService.save(checkEntity);
//        map.put("result", result);
//        map.put("msg", result ? "保存成功！" : "保存失败！");
        return map;
    }

    /**
     * 跳转Grid列表页
     *
     * @return
     */
    @RequestMapping(value = "/toList")
    public ModelAndView toRewindList() {
        ModelAndView mv = this.getModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(2L, MenuConstant.warehouselist));
        mv.setViewName("techbloom2/outsourcing/order/list");
        return mv;
    }

    /**
     * 获取Grid列表数据
     *
     * @return Map<String, Object>
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Map<String, Object> list() {
        Map<String, Object> paramsMap = getAllParameter();
        List<PurchaseOrderEntity> pageList = purchaseOrderDAO.getPurchaseOrderList(paramsMap);
        Map<String, Object> map = new HashMap<>(4);
        map.put("code", 0);
        map.put("msg", null);
        map.put("count", pageList.size());
        map.put("data", pageList);
        return map;
    }

    @RequestMapping(value = "/listByIds")
    @ResponseBody
    public Map<String, Object> listByIds() {

        Map<String, Object> paramsMap = getAllParameter();
        List<PurchaseOrderEntity> pageList = purchaseOrderDAO.selectBatchIds(StringUtils.stringToList((String) paramsMap.get("purchaseOrderIds")));
        Map<String, Object> map = new HashMap<>(4);
        map.put("code", 0);
        map.put("msg", null);
        map.put("count", pageList.size());
        map.put("data", pageList);
        return map;
    }


    /**
     * 跳转编辑页
     *
     * @return
     */
    @RequestMapping(value = "/toEdit")
    public ModelAndView toDetail(Long id) {
        String ids = request.getParameter("ids");
        ModelAndView mv = this.getModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(2L, MenuConstant.warehouselist));
        mv.setViewName("techbloom2/purchaseIn/purchaseOrder/edit");
        return mv;
    }

}
