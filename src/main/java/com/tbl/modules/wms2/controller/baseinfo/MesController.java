package com.tbl.modules.wms2.controller.baseinfo;

import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.service.system.RoleService;
import com.tbl.modules.wms.dao.interfacelog.InterfaceLogDAO;
import com.tbl.modules.wms2.constant.Constant;
import com.tbl.modules.wms2.service.baseinfo.MesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * MES接口：领料、退料
 *
 * @author DASH
 */
@Controller
@RequestMapping(value = "/ycl/baseinfo/mes")
public class MesController extends AbstractController {

    @Autowired
    private RoleService roleService;

    @Resource
    private MesService mesService;
    @Resource
    private InterfaceLogDAO interfaceLogDAO;


    @RequestMapping(value = "/requestItemFromWms",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> requestItemFromWms(){
        Map<String,Object> paramMap = getAllParameter();
        Map<String,Object> map = new HashMap<>();
        Long organizationId = Long.parseLong(String.valueOf(paramMap.get("organizationId")));
        Long headerId = Long.parseLong(String.valueOf(paramMap.get("headerId")));
        Long lineId = Long.parseLong(String.valueOf(paramMap.get("lineId")));
        String materialCode = String.valueOf(paramMap.get("materialCode"));
        String lotsNum = String.valueOf(paramMap.get("lotsNum"));
        Long quantity = Long.parseLong(String.valueOf(paramMap.get("quantity")));
        String operator = String.valueOf(paramMap.get("operator"));//员工工号
        Date transDate = new Date();
        String comments = String.valueOf(paramMap.get("comments"));
        map.put("organizationId",organizationId);
        map.put("headerId",headerId);
        map.put("lineId",lineId);
        map.put("materialCode",materialCode);
        map.put("lotsNum",lotsNum);
        map.put("quantity",quantity);
        map.put("operator",operator);
        map.put("transDate",transDate);
        map.put("comments",comments);
        mesService.issueBillLot(map);

        Map<String,Object> resultMap = new HashMap<>();
        String retCode = String.valueOf(map.get("retCode"));
        if(retCode.equals(Constant.SUCCESS)){
            resultMap.put("code", 0);
            resultMap.put("msg", "success");
            interfaceLogDAO.insertLog(Constant.MES_PROCEDURE_1,"MES接口-存储过程-仓库发料",map.toString(),retCode,"","");
        }
        if(retCode.equals(Constant.ERROR)){
            resultMap.put("code", 1);
            resultMap.put("msg", String.valueOf(map.get("retMess")));
        }
        return resultMap;
    }



}


