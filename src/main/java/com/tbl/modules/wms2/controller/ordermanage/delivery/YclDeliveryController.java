package com.tbl.modules.wms2.controller.ordermanage.delivery;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.service.system.RoleService;
import com.tbl.modules.wms2.entity.ordermanage.delivery.DeliveryAddSCDTO;
import com.tbl.modules.wms2.entity.ordermanage.delivery.DeliveryEntity;
import com.tbl.modules.wms2.service.ordermanage.delivery.DeliveryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 收货
 *
 * @author DASH
 */
@Controller
@RequestMapping(value = "/ycl/ordermanage/delivery")
public class YclDeliveryController extends AbstractController {

    @Autowired
    private RoleService roleService;

    @Autowired
    private DeliveryService deliveryService;

    /**
     * 删除
     *
     * @param
     * @return Map<String, Object>
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Map<String, Object> delete(String ids) {
        Map<String, Object> map = new HashMap<>(2);
        boolean result = deliveryService.delete(ids);
        map.put("result", result);
        map.put("msg", result ? "操作成功！" : "操作失败！");
        return map;
    }

    /**
     * 保存
     *
     * @param
     * @return Map<String, Object>
     */
    @PostMapping(value = "/add")
    @ResponseBody
    public Map<String, Object> add(@RequestBody DeliveryEntity deliveryEntity) {
        deliveryEntity.setCreateTime(DateUtil.date());
        boolean result = deliveryService.add(deliveryEntity, true);
        Map resultMap = new HashMap(2);
        resultMap.put("result", result);
        resultMap.put("msg", result ? "操作成功！" : "操作失败！");
        return resultMap;
    }

    /**
     * 弹出到新增/编辑页面
     *
     * @param id
     * @return ModelAndView
     */
    @RequestMapping(value = "/toEdit")
    @ResponseBody
    public ModelAndView toAdd(String id) {
        ModelAndView mv = this.getModelAndView();
        DeliveryEntity deliveryEntity = null;
        if (StrUtil.isNotEmpty(id)) {
            deliveryEntity = deliveryService.findById(id);
        }
        mv.setViewName("techbloom2/report/inventory/edit");
        mv.addObject("deliveryEntity", deliveryEntity);
        return mv;
    }

    /**
     * 主表 跳转Grid列表页
     *
     * @return
     */
    @RequestMapping(value = "/toList")
    public ModelAndView toRewindList() {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("techbloom2/report/inventory/list");
        return mv;
    }

    /**
     * 主表-获取Grid列表数据
     *
     * @return Map<String, Object>
     */
//    @RequestMapping(value = "/pageView")
//    @ResponseBody
//    public Map<String, Object> pageView(YclInventoryEntity yclInventoryEntity) {
//        Map<String, Object> paramsMap = getAllParameter();
//        Page<YclInventoryEntity> mapPage = deliveryService.getPageList(
//                new Page(
//                        Integer.parseInt((String) paramsMap.get("page")),
//                        Integer.parseInt((String) paramsMap.get("limit"))),
//                yclInventoryEntity);
//        return returnLayResponse(mapPage);
//    }

    /**
     * 不分页
     *
     * @return Map<String, Object>
     */
    @RequestMapping(value = "/listView")
    @ResponseBody
    public Map<String, Object> listView(DeliveryEntity deliveryEntity) {
        List<DeliveryEntity> page = deliveryService.listView(deliveryEntity);
        Map<String, Object> map = new HashMap<>(4);
        map.put("code", 0);
        map.put("msg", null);
        map.put("count", 0);
        map.put("data", page);
        return map;
    }

    /*********************************************************************************/

    /**
     * 手持保存
     *
     * @return Map<String, Object>
     */
    @PostMapping(value = "/addSC")
    @ResponseBody
    public Map<String, Object> addSC(@RequestBody DeliveryAddSCDTO deliveryAddSCDTO) {

        Boolean result = deliveryService.addSC(deliveryAddSCDTO);
        Map resultMap = new HashMap(2);
        resultMap.put("result", result);
        resultMap.put("msg", result ? "操作成功！" : "操作失败！");
        return resultMap;
    }

}


