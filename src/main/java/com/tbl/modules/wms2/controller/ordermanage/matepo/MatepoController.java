//package com.tbl.modules.wms2.controller.ordermanage.matepo;
//
//import com.baomidou.mybatisplus.plugins.Page;
//import com.tbl.common.utils.PageTbl;
//import com.tbl.common.utils.PageUtils;
//import com.tbl.modules.platform.constant.MenuConstant;
//import com.tbl.modules.platform.controller.AbstractController;
//import com.tbl.modules.platform.service.system.RoleService;
//import com.tbl.modules.wms2.dao.ordermanage.matepo.MatepoDAO;
//import com.tbl.modules.wms2.entity.ordermanage.matepo.Matepo;
//import com.tbl.modules.wms2.service.ordermanage.matepo.MatepoService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.servlet.ModelAndView;
//
//import java.util.*;
//
///**
// * 原材料采购订单
// */
//@Controller()
//@RequestMapping("/ycl/ordermanage/matepo")
//public class MatepoController extends AbstractController {
//    @Autowired
//    private RoleService roleService;
//
//    @Autowired
//    private MatepoService matepoService;
//    @Autowired
//    private MatepoDAO matepoDAO;
//
//
//
//
//    /**
//     * 跳转Grid列表页
//     * @return
//     */
//    @RequestMapping(value = "/toList")
//    public ModelAndView toRewindList() {
//        ModelAndView mv = this.getModelAndView();
//        mv.addObject("operationCode", roleService.selectOperationByRoleId(this.getSessionUser().getRoleId(), MenuConstant.matepo));
//        mv.setViewName("po_list");
//        return mv;
//    }
//
//    /**
//     *
//     * @return
//     */
//    @RequestMapping(value = "/list")
//    @ResponseBody
//    public Map<String, Object> list() {
//        Map<String, Object> paramsMap = getAllParameter();
//        List<Matepo> pageList = matepoDAO.getPageList(
//                new Page(
//                        Integer.parseInt((String) paramsMap.get("page")),
//                        Integer.parseInt((String) paramsMap.get("limit"))),
//                paramsMap);
//        Map<String, Object> map = new HashMap<>(4);
//        map.put("code", 0);
//        map.put("msg", null);
//        map.put("count", pageList.size());
//        map.put("data", pageList);
//        return map;
//    }
//
//    /**
//     * 弹出到明细页面
//     * @param id 入库主表主键
//     * @return ModelAndView
//     */
//    @RequestMapping(value = "/toDetailList.do")
//    @ResponseBody
//    public ModelAndView toDetailList(Long id) {
//        ModelAndView mv = this.getModelAndView();
//        mv.setViewName("techbloom2/ordermanage/matepo/matepo_detail");
//        mv.addObject("poId", id);
//        return mv;
//    }
//
//    /**
//     * 订单展示明细信息
//     * @param id 入库主表主键
//     * @return Map<String,Object>
//     */
//    @RequestMapping(value = "/detailList.do")
//    @ResponseBody
//    public Map<String, Object> detailList(Long id) {
//        Map<String, Object> map = new HashMap<>();
//        map.put("poId", Long.valueOf(id));
//        PageTbl page = this.getPage();
//        PageUtils utils = matepoService.getPageList(page, map);
//        page.setTotalRows(utils.getTotalCount() == 0 ? 1 : utils.getTotalCount());
//        executePageMap(map, page);  //初始化分页对象
//        map.put("rows", utils.getList());
//        map.put("total", utils.getTotalPage() == 0 ? 1 : utils.getTotalPage());
//        return map;
//    }
//}
