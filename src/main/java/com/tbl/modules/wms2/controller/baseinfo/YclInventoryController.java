package com.tbl.modules.wms2.controller.baseinfo;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Maps;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.platform.constant.MenuConstant;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.service.system.RoleService;
import com.tbl.modules.wms2.dao.baseinfo.YclInventoryDAO;
import com.tbl.modules.wms2.service.baseinfo.YclInventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 实际库存
 */
@Controller
@RequestMapping(value = "/wms2/baseinfo/inventory")
public class YclInventoryController extends AbstractController {

    @Autowired
    private RoleService roleService;
    @Autowired
    private YclInventoryService _service;
    @Autowired
    private YclInventoryDAO inventoryDAO;
    /**
     * 弹出到新增/编辑页面
     *
     * @param id
     * @return ModelAndView
     */
    @RequestMapping(value = "/toAdd.do")
    @ResponseBody
    public ModelAndView toAdd(Long id) {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("techbloom2/operation/transfer/edit");
        return mv;
    }

    /**
     * 弹出到新增/编辑页面
     *
     * @param id
     * @return ModelAndView
     */
    @RequestMapping(value = "/toView.do")
    @ResponseBody
    public ModelAndView toView(Long id) {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("techbloom2/operation/transfer/view");
        return mv;
    }

    /**
     * 跳转Grid列表页
     *
     * @return
     */
    @RequestMapping(value = "/toList")
    public ModelAndView toRewindList() {
        ModelAndView mv = this.getModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(2L, MenuConstant.warehouselist));
        mv.setViewName("techbloom2/operation/transfer/list");
        return mv;
    }

    /**
     * 获取Grid列表数据
     *
     * @param queryJsonString 查询条件
     * @return Map<String, Object>
     */
    @RequestMapping(value = "/list.do")
    @ResponseBody
    public Map<String, Object> list(String queryJsonString) {
        Map<String, Object> map = new HashMap<>(4);
        if (!StringUtils.isEmptyString(queryJsonString)) {
            map = JSON.parseObject(queryJsonString);
        }
        PageTbl page = this.getPage();
        PageUtils utils = _service.getPageList(page,map);
        page.setTotalRows(utils.getTotalCount() == 0 ? 1 : utils.getTotalCount());
        //初始化分页对象
        executePageMap(map, page);
        map.put("rows", utils.getList());
        map.put("total", utils.getTotalPage() == 0 ? 1 : utils.getTotalPage());
        return map;
    }

    /**
     * 点击编辑，获取质保单号列表
     * @param  queryString 查询条件
     * @param pageSize 页码
     * @param pageNo 页面编号
     * @return Map<String,Object>
     */
    @RequestMapping(value = "/getSelectCode")
    @ResponseBody
    public Map<String, Object> getSelectCode(String queryString, int pageSize, int pageNo ) {
        Map<String, Object> map = Maps.newHashMap();
        if (StringUtils.isNotEmpty(queryString)) {
            map.put("queryString", queryString);
        }
        PageTbl page = this.getPage();
//        page.setPagesize(pageSize);
//        page.setPageno(pageNo);
//        PageUtils utils = _service.getSelectCode(page, map);
//        map.put("result", utils.getList() == null ? "" : utils.getList());
//        map.put("total", utils.getTotalPage() == 0 ? 1 : utils.getTotalPage());
//        map.put("msg", "success");
//        return map;
        map.put("page", pageNo);
        map.put("limit", pageSize);
        String sortOrder = page.getSortorder();
        if (StringUtils.isEmptyString(sortOrder)) {
            sortOrder = "desc";
            page.setSortorder(sortOrder);
        }
        map.put("sidx", page.getSortname());
        map.put("order", page.getSortorder());
        map.put("dname", queryString.toUpperCase());
        //获取厂区列表信息 ：下拉框
        map.put("result", _service.getSelectCode(map));
        return map;
    }

    /**
     * 接口描述: 获取批次号
     *
     * @param areaCode
     * @param storeCode
     * @param materialCode
     * @return
     */
    @GetMapping(value = "/lotsnum/{areaCode}/{storeCode}/{materialCode}")
    @ResponseBody
    public Map<String, Object> getLotsnumList(@PathVariable("areaCode") String areaCode,
                                              @PathVariable("storeCode") String storeCode,
                                              @PathVariable("materialCode") String materialCode) {
        return restfulResponse(inventoryDAO.getLotsnumList(areaCode, storeCode, materialCode));
    }

    /**
     * 接口描述: 获取库存数据
     *
     * @param areaCode
     * @param storeCode
     * @param locatorCode
     * @return
     */
    @GetMapping(value = "/inventoryList/{areaCode}/{storeCode}/{locatorCode}")
    @ResponseBody
    public Map<String, Object> getInventoryList(@PathVariable("areaCode") String areaCode,
                                              @PathVariable("storeCode") String storeCode,
                                              @PathVariable("locatorCode") String locatorCode) {
        return restfulResponse(inventoryDAO.getInventoryList(areaCode, storeCode, locatorCode));
    }

    /**
     * 接口描述: 传的打印字符串
     * 仓库厂区库位批次
     * @return Map<String,Object>
     */
    @RequestMapping(value = "/printInventory")
    @ResponseBody
    public Map<String, Object> printInventoryList() {
        Map<String, Object> map = getAllParameter();
        List printList = _service.printInventoryList(map);
        return restfulResponse(printList);
    }

}


