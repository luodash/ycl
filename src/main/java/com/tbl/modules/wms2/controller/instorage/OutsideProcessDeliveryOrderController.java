package com.tbl.modules.wms2.controller.instorage;

import com.alibaba.fastjson.JSON;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.platform.constant.MenuConstant;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.service.system.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Gary
 *
 */
@Controller
@RequestMapping(value = "/wms2/instorage/outsideProcessDeliveryOrder")
public class OutsideProcessDeliveryOrderController extends AbstractController {

    @Autowired
    private RoleService roleService;

    /**
     * 弹出到新增/编辑页面
     *
     * @param id
     * @return ModelAndView
     */
    @RequestMapping(value = "/toAdd.do")
    @ResponseBody
    public ModelAndView toAdd(Long id) {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("techbloom2/instorage/outsideProcess/deliveryOrder_edit");
        return mv;
    }

    /**
     * 弹出到详情页面
     *
     * @param id
     * @return ModelAndView
     */
    @RequestMapping(value = "/toDetail.do")
    @ResponseBody
    public ModelAndView toDetail(Long id) {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("techbloom2/instorage/outsideProcess/deliveryOrder_detail");
        return mv;
    }

    /**
     * 跳转Grid列表页
     *
     * @return
     */
    @RequestMapping(value = "/toList")
    public ModelAndView toRewindList() {
        ModelAndView mv = this.getModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(2L, MenuConstant.warehouselist));
        mv.setViewName("techbloom2/instorage/outsideProcess/deliveryOrder_list");
        return mv;
    }

    /**
     * 获取Grid列表数据
     *
     * @param queryJsonString 查询条件
     * @return Map<String, Object>
     */
    @RequestMapping(value = "/list.do")
    @ResponseBody
    public Map<String, Object> list(String queryJsonString) {
        Map<String, Object> map = new HashMap<>(4);
        if (!StringUtils.isEmptyString(queryJsonString)) {
            map = JSON.parseObject(queryJsonString);
        }
//        map.put("factorycode", Arrays.asList(getSessionUser().getFactoryCode().split(",")));
//        PageTbl page = this.getPage();
//        PageUtils utils = warehouseService.getPageList(page, map);
//        page.setTotalRows(utils.getTotalCount() == 0 ? 1 : utils.getTotalCount());
//        //初始化分页对象
//        executePageMap(map, page);
//        map.put("rows", utils.getList());
//        map.put("total", utils.getTotalPage() == 0 ? 1 : utils.getTotalPage());

        List<Map<String, Object>> list = new ArrayList<>();
        for (int i = 1; i < 11; i++) {
            Map map2 = new HashMap(10);
            map2.put("id", i);
            map2.put("deliveryOrderCode", "100000" + i);
            map2.put("deliveryParty", "送货方" + i);
            map2.put("deliveryOrderDate", "2010-04-01");
            map2.put("logisticsCode", "物流编号"+i);
            map2.put("driver", "驾驶员"+i);
            map2.put("shipperAddress", "发货方地址"+i);
            map2.put("receiveAddress", "收货方地址"+i);
            map2.put("note", "送货方备注"+i);
            map2.put("supplierCode", "送货日期" + i);
            map2.put("supplierName", "制单人员" + i);
            map2.put("receiveParty", "收货方" + i);
            map2.put("receiveOrg", "收货方组织" + i);
            map2.put("purchaseOrg", "采购组织" + i);
            map2.put("purchaseDept", "采购部门" + i);
            map2.put("purchaser", "采购员" + i);
            map2.put("purchaseMobile", "采购员电话"+i);

            list.add(map2);
        }

        //map.put("factorycode", Arrays.asList("92", "93", "94", "95"));
        map.put("rows", list);
        map.put("records", 10);
        map.put("page", 1);
        map.put("total", 10);
        return map;
    }

}


