package com.tbl.modules.wms2.controller.insidewarehouse;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.service.system.RoleService;
import com.tbl.modules.wms2.common.RuleCode;
import com.tbl.modules.wms2.dao.insidewarehouse.check.CheckDao;
import com.tbl.modules.wms2.entity.insidewarehouse.check.YclCheckCreateDTO;
import com.tbl.modules.wms2.entity.insidewarehouse.check.YclCheckEntity;
import com.tbl.modules.wms2.entity.insidewarehouse.check.YclCheckLineEntity;
import com.tbl.modules.wms2.entity.insidewarehouse.check.YclCheckQueryDTO;
import com.tbl.modules.wms2.service.insidewarehouse.check.CheckLineService;
import com.tbl.modules.wms2.service.insidewarehouse.check.CheckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 盘库管理
 */
@Controller
@RequestMapping(value = "/ycl/insidewarehouse/check")
public class CheckController extends AbstractController {

    @Autowired
    private RoleService roleService;

    @Autowired
    CheckService checkService;

    @Autowired
    CheckLineService checkLineService;

    /**
     * 弹出到详情页面
     *
     * @param checkCode
     * @return ModelAndView
     */
    @RequestMapping(value = "/toDetail")
    @ResponseBody
    public ModelAndView toDetail(String checkCode) {
        ModelAndView mv = this.getModelAndView();
        YclCheckEntity yclCheckEntity = checkService.findByCheckCode(checkCode);
        mv.addObject("checkCode", checkCode);
        mv.addObject("yclCheckEntity", yclCheckEntity);
        mv.setViewName("techbloom2/insidewarehouse/check/detail");
        return mv;
    }

    /**
     * 详情页面子列表
     *
     * @param checkCode
     * @return List<YclCheckLineEntity>
     */
    @RequestMapping(value = "/detailList")
    @ResponseBody
    public Map<String, Object> detailList(String checkCode) {
        Map hashMap = new HashMap(1);
        hashMap.put("check_code", checkCode);
        List<YclCheckLineEntity> pageListByMap = checkLineService.getPageListByMap(hashMap);
        return returnLayResponse(pageListByMap);
    }

    /**
     * 删除
     *
     * @param
     * @return Map<String, Object>
     */
    @RequestMapping(value = "/deleteByCheckCodes")
    @ResponseBody
    public Map<String, Object> delete(String checkCodes) {
        Map<String, Object> map = new HashMap<>(2);
        boolean result = checkService.deleteByCheckCodes(checkCodes);
        map.put("result", result);
        map.put("msg", result ? "操作成功！" : "操作失败！");
        return map;
    }

    @PostMapping(value = "/updateCheck")
    @ResponseBody
    public Map<String, Object> updateCheck(YclCheckEntity yclCheckEntity) {
        Map<String, Object> map = new HashMap<>(2);
        boolean result = checkService.save(yclCheckEntity);
        map.put("result", result);
        map.put("msg", result ? "操作成功！" : "操作失败！");
        return map;

    }

    @GetMapping(value = "/audit")
    @ResponseBody
    public Map<String, Object> audit(String id) {
        YclCheckEntity yclCheckEntity = checkService.findById(id);
        yclCheckEntity.setState("2");
        yclCheckEntity.setUpdateTime(DateUtil.date());
        yclCheckEntity.setOperator(this.getSessionUser().getName());
        yclCheckEntity.setUpdateUser(this.getSessionUser().getName());
        Map<String, Object> map = new HashMap<>(2);
        boolean result = checkService.save(yclCheckEntity);
        map.put("result", result);
        map.put("msg", result ? "操作成功！" : "操作失败！");
        return map;

    }

    /**
     * 新增保存
     *
     * @param
     * @return Map<String, Object>
     */
    @PostMapping(value = "/save")
    @ResponseBody
    @Transactional
    public Map<String, Object> save(@RequestBody YclCheckCreateDTO yclCheckCreateDTO) {
        String checkCode = RuleCode.getCodeByPrefix("CHECK");
        YclCheckEntity checkEntity = yclCheckCreateDTO.getCheckEntity();
        checkEntity.setCheckCode(checkCode);
        checkEntity.setState("1");
        checkEntity.setCreaterUser(this.getSessionUser().getName());
        checkEntity.setCreaterTime(DateUtil.date());
        List<YclCheckLineEntity> lines = yclCheckCreateDTO.getLines();
        for (int i = 0; i < lines.size(); i++) {
            YclCheckLineEntity lineEntity = lines.get(i);
            lineEntity.setCheckCode(checkCode);
            lineEntity.setState("0");
        }

        boolean result;
        if (checkService.save(checkEntity) && checkLineService.saveBatch(lines)) {
            result = true;
        } else {
            result = false;
        }
        Map resultMap = new HashMap(2);
        resultMap.put("result", result);
        resultMap.put("msg", result ? "操作成功！" : "操作失败！");
        return resultMap;
    }

    /**
     * 弹出到新增/编辑页面
     *
     * @param id
     * @return ModelAndView
     */
    @RequestMapping(value = "/toEdit")
    @ResponseBody
    public ModelAndView toAdd(Long id) {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("techbloom2/insidewarehouse/check/edit");
        return mv;
    }

    /**
     * 主表 跳转Grid列表页
     *
     * @return
     */
    @RequestMapping(value = "/toList")
    public ModelAndView toRewindList() {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("techbloom2/insidewarehouse/check/list");
        return mv;
    }

    /**
     * 主表-获取Grid列表数据
     *
     * @return Map<String, Object>
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Map<String, Object> list(YclCheckQueryDTO checkQueryDTO) {
        Map<String, Object> paramsMap = getAllParameter();
        Page<YclCheckEntity> mapPage = checkService.getPageList(
                new Page(
                        Integer.parseInt((String) paramsMap.get("page")),
                        Integer.parseInt((String) paramsMap.get("limit"))),
                checkQueryDTO);
        return returnLayResponse(mapPage);
    }

    /**
     * @return Map<String, Object>
     */
    @RequestMapping(value = "/listView")
    @ResponseBody
    public Map<String, Object> listView(YclCheckQueryDTO checkQueryDTO) {
        List<YclCheckEntity> entityList = checkService.listView(checkQueryDTO);
        return returnLayResponse(entityList);
    }

}


