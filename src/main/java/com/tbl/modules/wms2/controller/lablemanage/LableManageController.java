package com.tbl.modules.wms2.controller.lablemanage;

import cn.hutool.core.map.MapUtil;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.service.system.RoleService;
import com.tbl.modules.wms2.dao.lablemanage.LableManageDAO;
import com.tbl.modules.wms2.entity.lablemanage.LableManageEntity;
import com.tbl.modules.wms2.entity.operation.OperationOtherPrint;
import com.tbl.modules.wms2.entity.operation.QualityInfoEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 标签管理
 *
 * @author DASH
 */
@Controller
@RequestMapping(value = "/ycl/lableManage")
public class LableManageController extends AbstractController {

    @Autowired
    private RoleService roleService;

    @Resource
    private LableManageDAO lableManageDAO;


    /**
     * 跳转Grid列表页
     * @return
     */
    @PostMapping(value = "/toList")
    public ModelAndView toList() {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("techbloom2/lablemanage/list");
        return mv;
    }


    /**
     * 获取Grid列表数据
     * @param
     * @return
     */
    @GetMapping(value = "/list")
    @ResponseBody
    public Map<String, Object> list() {
        Map<String, Object> paramsMap = getAllParameter();
        Map<String, Object> map = MapUtil.newHashMap();
        int count = lableManageDAO.count(paramsMap);
        if (count > 0) {
            List<LableManageEntity> list = lableManageDAO.getPageList(paramsMap);
            map.put("code", 0);
            map.put("msg", null);
            map.put("count", count);
            map.put("data", list);
        } else {
            map.put("code", 1);
            map.put("msg", "no data");
            map.put("count", count);
            map.put("data", "");
        }
        return map;
    }


    /**
     * 跳转编辑页
     *
     * @return
     */
    @RequestMapping(value = "/toAdd")
    public ModelAndView toDetail(String id) {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("techbloom2/lablemanage/add");
        return mv;
    }


    /**
     * 打印页面
     *
     * @return
     */
    @RequestMapping("/toPrint")
    @ResponseBody
    public void toPrint(String id) {
        LableManageEntity lableManageEntity = lableManageDAO.selectOneById(id);




    }




}


