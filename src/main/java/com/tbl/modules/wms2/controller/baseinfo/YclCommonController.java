package com.tbl.modules.wms2.controller.baseinfo;

import com.google.gson.JsonObject;
import com.tbl.modules.wms2.dao.common.CountDao;
import com.tbl.modules.wms2.service.common.CountService;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * @author DASH
 */
@Controller
@RequestMapping(value = "/wms2/baseinfo/common")
public class YclCommonController {

    @Resource
    CountService countService;
    @Resource
    CountDao countDao;

    @RequestMapping(value = "/getNextCount")
    @ResponseBody
    public Map<String, Object> getNextCount(String id) {
        String nextCount = countService.getNextCount(id);
        Map<String, Object> map = new HashMap<>(4);
        map.put("code", 0);
        map.put("msg", null);
        map.put("count", null);
        map.put("data", nextCount);
        return map;
    }

}
