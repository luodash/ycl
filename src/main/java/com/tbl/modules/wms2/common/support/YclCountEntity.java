package com.tbl.modules.wms2.common.support;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

/**
 * @author DASH
 */
@TableName("YCL_COUNT")
@Data
public class YclCountEntity {

    @TableId(value = "ID")
    String id;

    @TableField(value = "YEAR")
    Integer year;

    @TableField(value = "MONTH")
    Integer month;

    @TableField(value = "DAY")
    Integer day;

    @TableField(value = "COUNT")
    Integer count;

    @TableField(value = "REMARK")
    String remark;
}
