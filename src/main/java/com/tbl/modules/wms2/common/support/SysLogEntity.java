package com.tbl.modules.wms2.common.support;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;


/**
 * @author DASH
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName(value = "SP_SYS_LOG")
public class SysLogEntity {

    /**
     * 主键id
     */
    @TableId(value = "ID", type = IdType.ID_WORKER_STR)
    private String id;

    // 日志类型（1登录日志，2操作日志)
    private Integer type;
    // 日志内容
    private String content;
    // 操作用户账号
    private String userid;
    // 操作用户名称
    private String username;
    // IP
    private String ip;
    // 请求java类
    private String className;
    // 请求java方法
    private String classMethod;
    // 请求路径
    private String requestUrl;
    // 请求类型
    private String requestMethod;
    // 请求参数
    private String requestParam;
    // 浏览器类型
    private String userAgent;
    // 请求时间
    private Double requestTime;
    // 响应时间
    private Double responseTime;
    // 耗时
    private Long costTime;
    // 异常信息
    private String exception;
    // 异常堆栈
    private String exceptionStack;
    // 响应状态
    private Integer httpStatus;

    private Date createTime;
}
