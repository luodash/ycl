package com.tbl.modules.wms2.common;

import org.apache.commons.lang3.StringUtils;

import java.util.Random;
import java.util.UUID;

/**
 * 简单规则编码
 *
 * @author DASH
 */
public class RuleCode {


    /**
     * 前缀
     */
    private final static String prefix = "YD";

    /**
     * 后缀
     */
    private final static String suffix = "YD2";

    /**
     * 数字和随机数总长度
     */

    private static final int maxLength = 14;

    /**
     * 生成固定长度随机码
     *
     * @param n 长度
     */
    private static long getRandom(long n) {
        long min = 1, max = 9;
        for (int i = 1; i < n; i++) {
            min *= 10;
            max *= 10;
        }
        long rangeLong = (((long) (new Random().nextDouble() * (max - min)))) + min;
        return rangeLong;
    }

    /**
     * 根据数字进行加密+加随机数组成固定长度编码
     */
    private static String toCode(Integer num) {
        String idStr = num.toString();
        StringBuilder idsbs = new StringBuilder();
        for (int i = idStr.length() - 1; i >= 0; i--) {
            idsbs.append(idStr.charAt(i) - '0');
        }
        return idsbs.append(getRandom(maxLength - idStr.length())).toString();
    }

    /**
     * 生成时间戳
     */
    private static String getTimeStamp() {
        return String.valueOf(System.currentTimeMillis());
    }


    /**
     * 固定前缀 + 时间戳
     *
     * @return
     */
    public static String getCodeByPrefix() {
        return prefix + getTimeStamp();
    }

    /**
     * 自定义前缀 + 时间戳
     *
     * @return
     */
    public static String getCodeByPrefix(String prefixStr) {
        return prefixStr + getTimeStamp();
    }

    /**
     * 自定义前缀 + 自定义时间戳
     *
     * @return
     */
    public static String getCodeByPrefix(String prefixStr, String str) {
        return prefixStr + str;
    }

    /**
     * 时间戳 + 固定后缀
     *
     * @return
     */
    public static String getCodeBySuffix() {
        return getTimeStamp() + suffix;
    }

    /**
     * 时间戳 + 自定义后缀
     *
     * @return
     */
    public static String getCodeBySuffix(String suffixStr) {
        return getTimeStamp() + suffixStr;
    }

    /**
     * 自定义时间戳 + 自定义后缀
     *
     * @return
     */
    public static String getCodeBySuffix(String str, String suffixStr) {
        return str + suffixStr;
    }

    /**
     * 多个参数顺序依次拼接
     *
     * @param params
     * @return
     */
    public static String getCodeByParams(String... params) {
        return StringUtils.join(params);
    }


    /**
     * UUID
     *
     * @return
     */
    public static String getUUID() {
        return UUID.randomUUID().toString();
    }



}
