package com.tbl.modules.wms2.service.report;
import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.modules.wms2.entity.production.YclMaterialBackEntity;

import java.util.Map;

/**
 * 不合格退料报表
 */
public interface ReturnReportService {
    /**
     * 增加保存
     * @param entity
     * @return
     */
    boolean save(YclMaterialBackEntity entity);

    /**
     * 获取集合
     * @param page
     * @param map
     * @param yclMaterialBackEntity
     * @return
     */
    Page<YclMaterialBackEntity> getPageList(Page page, Map<String,Object> map,YclMaterialBackEntity yclMaterialBackEntity);

    /**
     * 删除
     * @param ids
     * @return
     */
    boolean delete(String ids);

    /**
     * 获取对象
     * @param id
     * @return
     */
    YclMaterialBackEntity findById(String id);
}
