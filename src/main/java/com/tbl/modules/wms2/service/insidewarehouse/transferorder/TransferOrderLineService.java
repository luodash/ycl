package com.tbl.modules.wms2.service.insidewarehouse.transferorder;

import com.tbl.modules.wms2.entity.insidewarehouse.transferorder.TransferOrderLineEntity;

import java.util.List;
import java.util.Map;

/**
 * 移位移库子表
 *
 * @author DASH
 */
public interface TransferOrderLineService {

    /**
     * 批量保存
     *
     * @param entitys
     * @return boolean
     */
    boolean saveBatch(List<TransferOrderLineEntity> entitys);

    /**
     * 查询列表
     *
     * @param map
     * @return
     */
    List<TransferOrderLineEntity> getPageListByMap(Map map);


    /**
     * 功能描述:删除
     *
     * @param ids
     * @return boolean
     */
    boolean delete(String ids);

    TransferOrderLineEntity findById(String id);

    List<TransferOrderLineEntity> getTransferOrderLines(Map<String,Object> map);


}
