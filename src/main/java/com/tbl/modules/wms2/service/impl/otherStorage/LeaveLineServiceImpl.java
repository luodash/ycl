package com.tbl.modules.wms2.service.impl.otherStorage;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.wms2.dao.otherStorage.LeaveDAO;
import com.tbl.modules.wms2.dao.otherStorage.LeaveLineDAO;
import com.tbl.modules.wms2.entity.otherStorage.LeaveEntity;
import com.tbl.modules.wms2.entity.otherStorage.LeaveLineEntity;
import com.tbl.modules.wms2.service.otherStorage.LeaveLineService;
import com.tbl.modules.wms2.service.otherStorage.LeaveService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author DASH
 */
@Service("yclLeaveLineService")
public class LeaveLineServiceImpl extends ServiceImpl<LeaveLineDAO, LeaveLineEntity> implements LeaveLineService {


}