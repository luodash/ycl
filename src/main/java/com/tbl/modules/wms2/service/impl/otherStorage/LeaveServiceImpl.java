package com.tbl.modules.wms2.service.impl.otherStorage;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.modules.wms2.dao.otherStorage.LeaveDAO;
import com.tbl.modules.wms2.dao.otherStorage.LeaveLineDAO;
import com.tbl.modules.wms2.entity.otherStorage.LeaveEntity;
import com.tbl.modules.wms2.entity.otherStorage.LeaveLineEntity;
import com.tbl.modules.wms2.service.otherStorage.LeaveLineService;
import com.tbl.modules.wms2.service.otherStorage.LeaveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author DASH
 */
@Service("yclLeaveService")
public class LeaveServiceImpl extends ServiceImpl<LeaveDAO, LeaveEntity> implements LeaveService {

    @Resource
    private LeaveDAO leaveDAO;
    @Resource
    private LeaveLineDAO leaveLineDAO;
    @Autowired
    private LeaveLineService leaveLineService;



    @Override
    public List<LeaveEntity> getPageList(Map<String, Object> map) {
        return leaveDAO.getPageList(map);
    }

    @Override
    public boolean delete(String ids) {
        String aId[] = ids.split(",");
        if(aId.length>0){
            boolean b = true;
            for(int i=0;i<aId.length;i++){
                int dNum = leaveDAO.deleteById(aId[i]);
                b = b&&(dNum==1);
            }
            return b;
        }
        return false;
    }

    @Override
    public LeaveEntity findById(String id) {
        return leaveDAO.selectById(id);
    }
}