package com.tbl.modules.wms2.service.impl.common;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.modules.wms2.common.support.SysLogEntity;
import com.tbl.modules.wms2.dao.common.SpLogDao;
import com.tbl.modules.wms2.service.common.SpLogService;
import org.springframework.stereotype.Service;

/**
 * LOG
 *
 * @author DASH
 */
@Service
public class SpLogServiceImpl extends ServiceImpl<SpLogDao, SysLogEntity> implements SpLogService {


    @Override
    public Boolean createLog(SysLogEntity sysLogEntity) {
        return this.insertOrUpdate(sysLogEntity);
    }
}
