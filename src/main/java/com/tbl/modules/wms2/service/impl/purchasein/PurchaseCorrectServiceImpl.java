package com.tbl.modules.wms2.service.impl.purchasein;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.modules.wms2.common.Utils;
import com.tbl.modules.wms2.dao.purchasein.PurchaseCorrectDao;
import com.tbl.modules.wms2.entity.operation.YclInOutFlowEntity;
import com.tbl.modules.wms2.entity.ordermanage.asn.AsnQueryDTO;
import com.tbl.modules.wms2.entity.ordermanage.asn.AsnVO;
import com.tbl.modules.wms2.entity.purchasein.YclPurchaseCorrectEntity;
import com.tbl.modules.wms2.entity.report.YclInventoryEntity;
import com.tbl.modules.wms2.service.operation.YclInOutFlowService;
import com.tbl.modules.wms2.service.ordermanage.asn.AsnHeaderService;
import com.tbl.modules.wms2.service.purchasein.PurchaseCorrectService;
import com.tbl.modules.wms2.service.report.InventoryService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 库存
 *
 * @author DASH
 */
@Service
public class PurchaseCorrectServiceImpl extends ServiceImpl<PurchaseCorrectDao, YclPurchaseCorrectEntity> implements PurchaseCorrectService {

    @Resource
    PurchaseCorrectDao purchaseCorrectDao;

    @Resource
    AsnHeaderService asnHeaderService;

    @Resource(name = "yclInventoryService")
    InventoryService inventoryService;

    @Resource
    YclInOutFlowService yclInOutFlowService;


    @Override
    public boolean save(YclPurchaseCorrectEntity entity) {
        return this.insertOrUpdate(entity);
    }

    @Override
    public Page<YclInventoryEntity> getPageList(Page page, Map<String, Object> map) {
        Wrapper wrapper = new EntityWrapper();
        wrapper
                .eq(StrUtil.isNotBlank((String) map.get("correctCode")), "CORRECT_CODE", map.get("correctCode"))
                .eq(StrUtil.isNotBlank((String) map.get("orderCode")), "CORRECT_CODE", map.get("correctCode"))
                .eq(StrUtil.isNotBlank((String) map.get("supplierName")), "CORRECT_CODE", map.get("correctCode"))
                .eq(StrUtil.isNotBlank((String) map.get("correctCode")), "CORRECT_CODE", map.get("correctCode"));
        Page<YclInventoryEntity> mapPage = this.selectPage(page, null);
        return mapPage;
    }

    @Override
    public Page<YclPurchaseCorrectEntity> getPageList(Page page, Map<String, Object> map, YclPurchaseCorrectEntity entity) {

        String kssj = (String) map.get("orderTimeStart");
        String jssj = (String) map.get("orderTimeEnd");
        Wrapper wrapper = new EntityWrapper();
        wrapper
                .like(StrUtil.isNotBlank(entity.getCorrectCode()), "CORRECT_CODE", entity.getCorrectCode())
                .like(StrUtil.isNotBlank(entity.getPoCode()), "PO_CODE", entity.getPoCode())
                .like(StrUtil.isNotBlank(entity.getSupplierName()), "SUPPLIER_NAME", entity.getSupplierName())
                .ge(StrUtil.isNotBlank(kssj), "ORDER_TIME", DateUtil.parse(kssj))
                .le(StrUtil.isNotBlank(jssj), "ORDER_TIME", DateUtil.parse(jssj))
                .orderBy("CORRECT_CODE", false);
        Page<YclPurchaseCorrectEntity> mapPage = this.selectPage(page, wrapper);
        return mapPage;
    }

    @Override
    public boolean delete(String ids) {
        return this.deleteBatchIds(Arrays.asList(ids.split(",")));
    }

    @Override
    public YclPurchaseCorrectEntity findById(String id) {
        return this.selectById(id);
    }

    @Override
    public List<YclPurchaseCorrectEntity> listView(YclPurchaseCorrectEntity entity) {
        return this.selectList(new EntityWrapper<>(entity));
    }

    @Override
    public boolean batchSave(List<YclPurchaseCorrectEntity> entities) {
        return this.insertOrUpdateBatch(entities);
    }

    @Override
    public List<Map<String, Object>> getOut(String poCode) {

        AsnQueryDTO asnQueryDTO = new AsnQueryDTO();
        asnQueryDTO.setPoCode(poCode);
        asnQueryDTO.setAsnType("1");
        // 送货单详情
        List<AsnVO> asnVOS = asnHeaderService.listViewAsn(asnQueryDTO);

        List<Map<String, Object>> list = new ArrayList<>();
        for (int i = 0; i < asnVOS.size(); i++) {
            AsnVO asnVO = asnVOS.get(i);
            // 流水表
            YclInOutFlowEntity yclInOutFlowEntity = new YclInOutFlowEntity();
            yclInOutFlowEntity.setBusinessCode(asnVO.getAsnNumber());
            yclInOutFlowEntity.setMaterialCode(asnVO.getMaterialCode());
            yclInOutFlowEntity.setLotsNum(asnVO.getLotsNum());
            yclInOutFlowEntity.setInoutType("1");
            List<YclInOutFlowEntity> yclInOutFlowEntities = yclInOutFlowService.listView(yclInOutFlowEntity);
            // 去重批次
//            ArrayList<YclInOutFlowEntity> arrayList = yclInOutFlowEntities.stream().collect(
//                    Collectors.collectingAndThen(
//                            Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(YclInOutFlowEntity::getLotsNum))),
//                            userOrders -> new ArrayList<>(userOrders)
//                    )
//            );

            // 有流水
            if (yclInOutFlowEntities != null || yclInOutFlowEntities.size() > 0) {


                for (YclInOutFlowEntity item : yclInOutFlowEntities) {
                    // 新订单 库存没有 怎么更正
//                    YclInventoryEntity yclInventoryEntity = inventoryService.find(item.getAreaCode(), item.getStoreCode(), item.getLocatorCode(), item.getMaterialCode(), item.getLotsNum());
                    Map<String, Object> stringObjectMap = Utils.objectToMap(item);

//                    if (yclInventoryEntity != null) {
                    stringObjectMap.put("poCode", asnVO.getPoCode());
                    stringObjectMap.put("poLineNum", asnVO.getPoLineNum());
                    stringObjectMap.put("poQuantity", asnVO.getPoQuantity());
                    stringObjectMap.put("receiveQuantity", asnVO.getReceiveQuantity());
//                    }
                    list.add(stringObjectMap);
                }
            } else {

                Map<String, Object> stringObjectMap = Utils.objectToMap(asnVO);
                list.add(stringObjectMap);
            }

        }
        return list;

    }

}
