package com.tbl.modules.wms2.service.impl.insidewarehouse.check;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.wms2.dao.insidewarehouse.check.CheckDao;
import com.tbl.modules.wms2.dao.insidewarehouse.check.CheckLineDao;
import com.tbl.modules.wms2.entity.insidewarehouse.check.YclCheckEntity;
import com.tbl.modules.wms2.entity.insidewarehouse.check.YclCheckLineEntity;
import com.tbl.modules.wms2.entity.insidewarehouse.check.YclCheckQueryDTO;
import com.tbl.modules.wms2.service.insidewarehouse.check.CheckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 盘库
 *
 * @author DASH
 */
@Service
public class CheckServiceImpl extends ServiceImpl<CheckDao, YclCheckEntity> implements CheckService {

    @Resource
    CheckDao checkDao;

    @Resource
    CheckLineDao checkLineDao;

    @Override
    public boolean save(YclCheckEntity entity) {
        return this.insertOrUpdateAllColumn(entity);
    }

    @Override
    public Page<YclCheckEntity> getPageList(Page page, YclCheckQueryDTO checkQueryDTO) {

        String kssj = checkQueryDTO.getKssj();
        String jssj = checkQueryDTO.getJssj();
        EntityWrapper<YclCheckEntity> ew = new EntityWrapper<YclCheckEntity>();
        ew.eq(StrUtil.isNotEmpty(checkQueryDTO.getAreaCode()), "AREA_CODE", checkQueryDTO.getAreaCode())
                .eq(StrUtil.isNotEmpty(checkQueryDTO.getStoreCode()), "STORE_CODE", checkQueryDTO.getStoreCode())
//                .eq(StrUtil.isNotEmpty(yclCheckEntity.getCreaterUser()), "CREATER_USER", yclCheckEntity.getCreaterUser())
                .ge(StrUtil.isNotBlank(kssj), "CREATER_TIME", DateUtil.parse(kssj))
                .le(StrUtil.isNotBlank(jssj), "CREATER_TIME", DateUtil.parse(jssj))
                .orderBy("CREATER_TIME", false);
        Page<YclCheckEntity> mapPage = this.selectPage(page, ew);
        return mapPage;
    }

    @Override
    public List<YclCheckLineEntity> getPageListByMap(Map map) {
        return this.selectByMap(map);
    }

    @Override
    public boolean delete(String ids) {
        return this.deleteBatchIds(StringUtils.stringToList(ids));
    }

    @Override
    public boolean deleteByCheckCodes(String checkCodes) {
        Wrapper<YclCheckEntity> wrapper = new EntityWrapper<YclCheckEntity>();
        wrapper.in("CHECK_CODE", checkCodes.split(","));
        Integer delCheck = checkDao.delete(wrapper);
        Wrapper<YclCheckLineEntity> wrapper2 = new EntityWrapper<YclCheckLineEntity>();
        wrapper.in("CHECK_CODE", checkCodes.split(","));
        Integer delCheckLine = checkLineDao.delete(wrapper2);
        if (delCheck > 0 && delCheckLine > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public YclCheckEntity findById(String id) {
        return this.selectById(id);
    }

    @Override
    public YclCheckEntity findByCheckCode(String checkCode) {
        YclCheckEntity yclCheckEntity = new YclCheckEntity();
        yclCheckEntity.setCheckCode(checkCode);
        YclCheckEntity entity = checkDao.selectOne(yclCheckEntity);
        return entity;
    }

    @Override
    public List<YclCheckEntity> listView(YclCheckQueryDTO checkQueryDTO) {
        String kssj = checkQueryDTO.getKssj();
        String jssj = checkQueryDTO.getJssj();
        EntityWrapper ew = new EntityWrapper<YclCheckEntity>();
        ew.eq(StrUtil.isNotBlank(checkQueryDTO.getAreaCode()), "AREA_CODE", checkQueryDTO.getAreaCode())
                .eq(StrUtil.isNotBlank(checkQueryDTO.getStoreCode()), "STORE_CODE", checkQueryDTO.getStoreCode())
                .ge(StrUtil.isNotBlank(kssj), "CREATER_TIME", DateUtil.parse(kssj))
                .le(StrUtil.isNotBlank(jssj), "CREATER_TIME", DateUtil.parse(jssj));
        return this.selectList(ew);
    }
}
