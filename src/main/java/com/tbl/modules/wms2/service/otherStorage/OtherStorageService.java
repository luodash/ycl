package com.tbl.modules.wms2.service.otherStorage;

import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.wms2.entity.operation.YclInOutFlowEntity;

import java.util.Map;

/**
 * 其他出入库
 */
public interface OtherStorageService {
    /**
     * 功能描述:保存
     * @param entity
     * @return boolean
     */
//    boolean save(YclInOutFlowEntity entity);

    /**
     * 功能描述:列表页
     * @param pageTbl
     * @param map
     * @return PageUtils
     */
    PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map);

    /**
     * 功能描述:删除
     * @param ids
     * @return boolean
     */
//    boolean delete(String ids);

    /**
     * 功能描述:根据ID查询
     * @param id
     * @return
     */
//    WarehouseEntity findById(Long id);
}
