package com.tbl.modules.wms2.service.impl.remotedata;

import com.tbl.modules.wms2.dao.remotedata.RemoteDataDAO;
import com.tbl.modules.wms2.service.remotedata.RemoteDateService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service("yclRemoteDataService")
public class RemoteDataServiceImpl implements RemoteDateService {

    @Resource
    private RemoteDataDAO remoteDataDAO;

    public List<Map<String,Object>> getAsnHeaders(Long id){
        return remoteDataDAO.queryAsnHeaders(id);
    }

    public Integer getAsnCountFromRemote(Long id){
        return remoteDataDAO.countAsnHeaders(id);
    }
}
