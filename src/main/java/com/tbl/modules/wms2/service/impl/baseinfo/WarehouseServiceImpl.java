package com.tbl.modules.wms2.service.impl.baseinfo;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.wms2.dao.baseinfo.WarehouseDao;
import com.tbl.modules.wms2.entity.baseinfo.WarehouseEntity;
import com.tbl.modules.wms2.service.baseinfo.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author DASH
 */
@Service("yddl2WarehouseService")
public class WarehouseServiceImpl extends ServiceImpl<WarehouseDao, WarehouseEntity> implements WarehouseService {

    @Resource
    WarehouseDao warehouseDao;

    @Override
    public boolean save(WarehouseEntity entity) {
        return this.insertOrUpdate(entity);
    }

    @Override
    public PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map) {
//        if (map.get("code") != null) {
//            map.put("code", map.get("code").toString().toUpperCase());
//        }
        String sortOrder = pageTbl.getSortorder();
        String sortName = pageTbl.getSortname();
        if (StringUtils.isEmptyString(sortName)) {
            sortName = "id";
        }
        if (StringUtils.isEmptyString(sortOrder)) {
            sortOrder = "desc";
        }
        Page page = new Page(pageTbl.getPageno(), pageTbl.getPagesize(), sortName, "asc".equalsIgnoreCase(sortOrder));
        return new PageUtils(page.setRecords(warehouseDao.getPageList(page, map)));
    }

    @Override
    public boolean delete(String ids) {
        return this.deleteBatchIds(StringUtils.stringToList(ids));
    }

    @Override
    public WarehouseEntity findById(Long id) {
        return id == null || id==0L ? new WarehouseEntity() : warehouseDao.findById(id);
    }


    public Map<String,Object> entruckNotice(String num) {
        Map<String,Object> map = new HashMap<>(4);
        List<Map<String,Object>> list = warehouseDao.entruckNotice(num);
        if(list.size()>0){
            map.put("code", 0);
            map.put("msg", "success");
            map.put("result", true);
            map.put("data", list);
        }else{
            map.put("code", 1);
            map.put("msg", "no data");
            map.put("result", false);
            map.put("data", "");
        }
        return map;
    }


    /**
     * 获取EBS中的库位信息
     * @param map
     * @return
     */
    public Map<String,Object> getLocationfromEBS(Map map){
        Map<String,Object> resultMap = new HashMap<>(4);
        List<Map<String,Object>> list = warehouseDao.getLocationfromEBS(map);
        if(list.size()>0){
            map.put("code", 0);
            map.put("msg", "success");
            map.put("result", true);
            map.put("data", list);
        }else{
            map.put("code", 1);
            map.put("msg", "no data");
            map.put("result", false);
            map.put("data", "");
        }
        return map;
    }


}
