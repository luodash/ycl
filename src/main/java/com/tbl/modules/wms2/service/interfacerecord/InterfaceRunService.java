package com.tbl.modules.wms2.service.interfacerecord;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.wms2.entity.ycloutstorage.YclOutStorageDetail;
import com.tbl.modules.wms2.interfacerecord.InterfaceRun;

import java.util.List;
import java.util.Map;

/**
 *  接口离线执行
 * @author 70486
 **/
public interface InterfaceRunService extends IService<InterfaceRun> {
	
	/**
	 * 插入接口执行表
	 * @param code 接口编码
	 * @param name 接口名称
	 * @param requestString 请求报文
	 * @param operateTime 操作时间
	 * @param itemCode 物料编码
	 * @return Integer
	 */
	Integer insertRun(String code,String name,String requestString,String operateTime,String itemCode);

	/**
	 * 获取离线接口维护列表的数据
	 * @param  map
	 * @return PageUtils
	 */
	PageUtils queryPage(Map<String,Object> map);

	/**
	 * 批量更新接口离线执行表状态
	 * @param lstYclOutStorageDetail 发货单明细
	 * @param state 状态
	 */
	void updateStateBatches(List<YclOutStorageDetail> lstYclOutStorageDetail, String state);

	/**
	 * 更新接口执行表
	 * @param interfaceRun 接口数据
	 */
	void updateInterfaceRunByid(InterfaceRun interfaceRun);

	/**
	 * 获取定时任务需要执行的离线接口
	 * @return InterfaceRun
	 */
	List<InterfaceRun> findJobsInterfaceRuns();
}
