package com.tbl.modules.wms2.service.workorder;



import com.sun.org.apache.xpath.internal.operations.Bool;
import com.tbl.modules.wms2.entity.purchasein.OutSourcingPOEntity;
import com.tbl.modules.wms2.entity.workorder.ReceiveOrderEntity;
import com.tbl.modules.wms2.entity.workorder.WorkOrderEntity;

import java.util.List;
import java.util.Map;

/**
 * 采购订单
 *
 * @author DASH
 */
public interface WorkOrderService {


    /**
     * 委外加工-送货订单创建或保存功能,不做实际接收处理
     * @param map
     * @return
     */
    Map<String,Object> createReceive(Map<String,Object> map);

    /**
     * 委外加工-送货订单接收处理
     * @param map
     * @return
     */
    Map<String,Object> receiveProcess(Map<String,Object> map);

    /**
     * 委外加工-回退
     * @param rcvId
     * @return
     */
    Map<String,String> returnProcess(Long rcvId);

    /**
     * 委外加工-工单完工提交时进行验证
     */
    Map<String,Object> validateQuantity(Map<String,Object> map);

    /**
     * 委外加工-工单完工保存按钮
     */
    Map<String,Object> createComplete(Map<String,Object> map);

    /**
     * 委外加工-工单完工提交按钮
     */
    Map<String,Object> completeProcess(Map<String,Object> map);
    /**
     * 委外加工-通过订单ID获取订单信息
     */
    OutSourcingPOEntity getOutSourcingPOByPoId(Long poId);
    /**
     * 委外加工-通过订单ID获取接收单list
     */
    List<ReceiveOrderEntity> getRcvListByPoId(Long poId);
    /**
     * 委外加工-通过接收单ID获取工单list
     */
    List<WorkOrderEntity> getWorkOrderListByRcvId(Long rcvId);
    /**
     * 委外加工-通过接收单D获取接收单信息
     */
    ReceiveOrderEntity getRcvInfoByRcvId(Long rcvId);
    /**
     * 委外加工-通过接受单ID查看是否可以退回
     */
    String getReceiveOrderState (Long rcvId);

    List<Map<String,Object>> getDescriptionByOrgId(Long orgId);

    List<Map<String,Object>> getLocatorListByDesc(Map paramsMap);

    Map<String,Object> getFtInfo(Map map);

}
