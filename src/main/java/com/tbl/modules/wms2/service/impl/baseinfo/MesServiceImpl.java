package com.tbl.modules.wms2.service.impl.baseinfo;

import com.alibaba.fastjson.JSONObject;
import com.tbl.modules.wms.dao.interfacelog.InterfaceLogDAO;
import com.tbl.modules.wms2.dao.baseinfo.MesDao;
import com.tbl.modules.wms2.entity.purchasein.MeterialBackWmsEntity;
import com.tbl.modules.wms2.service.baseinfo.MesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.Map;

/**
 * @author DASH
 */
@Service("yclMesService")
public class MesServiceImpl implements MesService {

    @Resource
    private MesDao mesDao;

    @Autowired
    private InterfaceLogDAO interfaceLogDAO;

    public Map<String, Object> issueBillLot(Map<String, Object> map) {

        String requestStr = JSONObject.toJSONString(map);

        mesDao.issueBillLot(map);

        interfaceLogDAO.insertLog("mes.wms_2_mes_issue_bill.issue_bill_lot", "领料出库", requestStr, JSONObject.toJSONString(map), "", "");

        return mesDao.issueBillLot(map);
    }

    @Override
    public MeterialBackWmsEntity returnBillLot(MeterialBackWmsEntity meterialBackWmsEntity) {
        meterialBackWmsEntity.setTransDate(new Date());
        mesDao.returnBillLot(meterialBackWmsEntity);
        return meterialBackWmsEntity;
    }

}
