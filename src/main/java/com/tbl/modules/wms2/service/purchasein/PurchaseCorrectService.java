package com.tbl.modules.wms2.service.purchasein;

import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.modules.wms2.entity.purchasein.YclPurchaseCorrectEntity;
import com.tbl.modules.wms2.entity.report.YclInventoryEntity;

import java.util.List;
import java.util.Map;

/**
 * 采购更正
 *
 * @author DASH
 */
public interface PurchaseCorrectService {

    /**
     * 功能描述:保存
     *
     * @param entity
     * @return boolean
     */
    boolean save(YclPurchaseCorrectEntity entity);

    /**
     * 功能描述:列表页 主表
     *
     * @param page
     * @param map
     * @return Page
     */
    Page<YclInventoryEntity> getPageList(Page page, Map<String, Object> map);

    /**
     * 功能描述:列表页 主表
     *
     * @param page
     * @param entity
     * @return Page
     */
    Page<YclPurchaseCorrectEntity> getPageList(Page page, Map<String, Object> map, YclPurchaseCorrectEntity entity);

    /**
     * 删除
     *
     * @param ids
     * @return boolean
     */
    boolean delete(String ids);


    YclPurchaseCorrectEntity findById(String id);

    List<YclPurchaseCorrectEntity> listView(YclPurchaseCorrectEntity entity);

    /**
     * 批量保存
     *
     * @param entities
     * @return
     */
    boolean batchSave(List<YclPurchaseCorrectEntity> entities);

    /**
     * 根据订单号查询所有送货单 对应库存
     *
     * @param poCode
     * @return
     */
    List<Map<String, Object>> getOut(String poCode);
}
