package com.tbl.modules.wms2.service.impl.remotedata;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.modules.wms2.dao.remotedata.CronJobDao;
import com.tbl.modules.wms2.entity.remotedata.CronJobEntity;
import com.tbl.modules.wms2.service.remotedata.CronJobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @author DASH
 */
@Service("yddl2CronJobService")
@Transactional(rollbackFor = Exception.class)
public class CronJobServiceImpl extends ServiceImpl<CronJobDao, CronJobEntity> implements CronJobService {

    @Autowired
    CronJobDao cronJobDao;


    @Override
    public CronJobEntity findById(Integer id) {
        return id == null || id==0L ? new CronJobEntity() : cronJobDao.findById(id);
    }

    public List<CronJobEntity> getList(Map map){
        return cronJobDao.getList(map);
    }
}
