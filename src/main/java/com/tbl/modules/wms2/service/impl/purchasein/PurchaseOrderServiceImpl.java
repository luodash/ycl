package com.tbl.modules.wms2.service.impl.purchasein;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.wms2.dao.purchasein.PurchaseOrderDAO;
import com.tbl.modules.wms2.entity.purchasein.PurchaseOrderEntity;
import com.tbl.modules.wms2.service.purchasein.PurchaseOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 采购订单
 *
 * @author DASH
 */
@Service("PurchaseOrderService")
public class PurchaseOrderServiceImpl extends ServiceImpl<PurchaseOrderDAO, PurchaseOrderEntity> implements PurchaseOrderService {

    @Autowired
    PurchaseOrderDAO purchaseOrderDAO;

    @Override
    public PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map) {
        return null;
    }

    /**
     * 通过委外加工视图查询数据
     * @param poCode
     * @return
     */
    public PurchaseOrderEntity getWxPoView(String poCode){
        Map<String,Object> resultmap = new HashMap<>();
        if(StringUtils.isEmpty(poCode)){
            return null;
        }
        //通过采购订单编码查委外加工订单视图CUX_WX_PO_V
        Map<String,Object> map = purchaseOrderDAO.getWxPoView(poCode);
        if(map!=null && map.size()>0){
            return null;
        }
        PurchaseOrderEntity purchaseOrderEntity = new PurchaseOrderEntity();
        //purchaseOrderEntity.setPoId(String.valueOf(map.get("po_id")));
        purchaseOrderEntity.setPurchaseDeptId(String.valueOf(map.get("ship_to_organization_id")));
        purchaseOrderEntity.setWxitemId(Long.parseLong(String.valueOf(map.get("wx_item_id"))));

        //TODO将视图中获取的数据写入到对象中返回,主要是后续运行存储过程所需要的参数
        return purchaseOrderEntity;
    }

}
