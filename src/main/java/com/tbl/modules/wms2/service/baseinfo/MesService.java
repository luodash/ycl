package com.tbl.modules.wms2.service.baseinfo;

import com.tbl.modules.wms2.entity.purchasein.MeterialBackWmsEntity;

import java.util.Map;

/**
 * MES系统接口服务
 *
 * @author DASH
 */
public interface MesService {

    /**
     * 调用MES存储过程实现：仓库发料，更新单据明细
     *
     * @param map
     * @return
     */
    Map<String, Object> issueBillLot(Map<String, Object> map);

    MeterialBackWmsEntity returnBillLot(MeterialBackWmsEntity meterialBackWmsEntity);

}
