package com.tbl.modules.wms2.service.baseinfo;

import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.wms2.entity.baseinfo.StorehouseEntity;

import java.util.Map;

/**
 * 库位
 *
 * @author DASH
 */
public interface StorehouseService {

    /**
     * 功能描述:保存
     *
     * @param storehouseEntity
     * @return boolean
     */
    boolean save(StorehouseEntity storehouseEntity);

    /**
     * 功能描述:列表页
     * 分页
     * @param pageTbl
     * @param map
     * @return PageUtils
     */
    PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map);

    /**
     * 功能描述:删除
     *
     * @param ids
     * @return boolean
     */
    boolean delete(String ids);

    /**
     * 根据id，获取库位信息
     * @param id
     * @return
     */
    StorehouseEntity findById(Long id);
}
