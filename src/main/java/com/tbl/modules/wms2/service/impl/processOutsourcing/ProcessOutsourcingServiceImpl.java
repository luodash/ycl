package com.tbl.modules.wms2.service.impl.processOutsourcing;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.wms2.dao.processOutsourcing.ProcessOutsourcingDao;
import com.tbl.modules.wms2.entity.processOutsourcing.ProcessOutsourcingEntity;
import com.tbl.modules.wms2.entity.report.YclInventoryEntity;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Map;

/**
 * 工序外协
 */
@Service
public class ProcessOutsourcingServiceImpl extends ServiceImpl<ProcessOutsourcingDao, ProcessOutsourcingEntity> implements com.tbl.modules.wms2.service.processOutsourcing.ProcessOutsourcingService {
    @Resource
    private ProcessOutsourcingDao processOutsourcingDao;

    @Override
    public boolean save(ProcessOutsourcingEntity entity) {
        return this.insertOrUpdate(entity);
    }

    @Override
    public Page<ProcessOutsourcingEntity> getPageList(Page page, Map<String, Object> map) {
        Page<ProcessOutsourcingEntity> mapPage = this.selectPage(page, null);
        return mapPage;
    }

    @Override
    public Page<ProcessOutsourcingEntity> getPageList(Page page, ProcessOutsourcingEntity processOutsourcingEntity) {
        Wrapper wrapper = new EntityWrapper();
        /*if (StrUtil.isNotEmpty(processOutsourcingEntity.getAreaCode())) {
            wrapper.eq("AREA_CODE", processOutsourcingEntity.getAreaCode());
        }
        if (StrUtil.isNotEmpty(processOutsourcingEntity.getStoreCode())) {
            wrapper.eq("STORE_CODE", processOutsourcingEntity.getStoreCode());
        }
        if (StrUtil.isNotEmpty(processOutsourcingEntity.getLocatorCode())) {
            wrapper.eq("LOCATOR_CODE", processOutsourcingEntity.getLocatorCode());
        }
        if (StrUtil.isNotEmpty(processOutsourcingEntity.getMaterialCode())) {
            wrapper.eq("MATERIAL_CODE", processOutsourcingEntity.getMaterialCode());
        }
        if (StrUtil.isNotEmpty(processOutsourcingEntity.getLotsNum())) {
            wrapper.eq("LOTS_NUM", processOutsourcingEntity.getLotsNum());
        }*/
        Page<ProcessOutsourcingEntity> mapPage = this.selectPage(page,wrapper);
        return mapPage;
    }


    @Override
    public Integer count(Map<String, Object> map) {
        return null;
    }

    @Override
    public boolean delete(String ids) {
        return this.deleteBatchIds(Arrays.asList(ids.split(",")));
    }

    @Override
    public ProcessOutsourcingEntity findById(String id) {
        return this.selectById(id);
    }
}
