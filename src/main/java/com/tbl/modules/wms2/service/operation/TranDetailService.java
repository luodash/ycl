package com.tbl.modules.wms2.service.operation;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.wms2.entity.operation.TranDetail;

import java.util.Map;

public interface TranDetailService extends IService<TranDetail> {
    /**
     * 列表页数据
     *
     * @param pageTbl
     * @param map
     * @return PageUtils
     */
    PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map);

    TranDetail selectTransferById(Long id);

    Long getSequence();
}
