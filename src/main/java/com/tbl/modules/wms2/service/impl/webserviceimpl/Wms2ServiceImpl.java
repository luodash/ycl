package com.tbl.modules.wms2.service.impl.webserviceimpl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.modules.wms2.dao.webserviceDAO.Wms2DAO;
import com.tbl.modules.wms2.service.webservice.client.Wms2Service;
import org.apache.poi.ss.formula.functions.T;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * WMS调外接口实现类
 * @author 70486
 */
@Service
public class Wms2ServiceImpl extends ServiceImpl<Wms2DAO, T> implements Wms2Service {

    /**
     * 日志打印
     */
    private Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * 调EBS接口-基础数据物料 FEWMS001
     * @param map
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public String FEWMS202(Map<String, Object> map,String itemCode) {
        logger.info("#####外部---》WMS####物料数据同步："+itemCode);
        return itemCode;
    }
}
