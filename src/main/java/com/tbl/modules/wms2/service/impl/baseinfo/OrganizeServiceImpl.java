package com.tbl.modules.wms2.service.impl.baseinfo;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.wms2.dao.baseinfo.OrganizeDao;
import com.tbl.modules.wms2.entity.baseinfo.OrganizeEntity;
import com.tbl.modules.wms2.service.baseinfo.OrganizeService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author DASH
 */
@Service("yddl2OrganizeService")
@Transactional(rollbackFor = Exception.class)
public class OrganizeServiceImpl extends ServiceImpl<OrganizeDao, OrganizeEntity> implements OrganizeService {


    @Resource
    OrganizeDao organizeDao;

    public PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map) {
//        if (map.get("code") != null) {
//            map.put("code", map.get("code").toString().toUpperCase());
//        }
        String sortOrder = pageTbl.getSortorder();
        String sortName = pageTbl.getSortname();
        if (StringUtils.isEmptyString(sortName)) {
            sortName = "entityId";
        }
        if (StringUtils.isEmptyString(sortOrder)) {
            sortOrder = "desc";
        }
        Page page = new Page(pageTbl.getPageno(), pageTbl.getPagesize(), sortName, "asc".equalsIgnoreCase(sortOrder));
        return new PageUtils(page.setRecords(organizeDao.getPageList(page, map)));
    }

    /**
     * 获取公司/组织的下拉框列表数据
     * @param map（entityId,organizeId）
     * @return
     */
    public Map<String,Object> getCompanyAndOrganizationAndStores(Map<String, Object> map) {
        int count = organizeDao.getCount(map);
        Map<String,Object> resultMap = new HashMap<>();
        if(count>0){
            List<Map<String,Object>> list = organizeDao.getCompanyAndOrganizationAndStoreList(map);
            resultMap.put("code", 0);
            resultMap.put("msg", "success");
            resultMap.put("result", true);
            resultMap.put("data", list);
        }else{
            resultMap.put("code", 1);
            resultMap.put("msg", "no data!");
            resultMap.put("result", false);
            resultMap.put("data", "");
        }

        return resultMap;
    }

//    /**
//     * 获取公司-组织-仓库的下拉框列表数据
//     * @param map (entityId,organizeId,storeCode)
//     * @return
//     */
//    public Map<String,Object> getStoreList(Map<String,Object> map) {
//        List<Map<String,Object>> list = organizeDao.getStoreList(map);
//        Map<String,Object> resultMap = new HashMap<>();
//        if(list.size()>0){
//            resultMap.put("code", 0);
//            resultMap.put("msg", "success");
//            resultMap.put("result", true);
//            resultMap.put("data", list);
//        }else{
//            resultMap.put("code", 1);
//            resultMap.put("msg", "Can't found data!");
//            resultMap.put("result", false);
//            resultMap.put("data", null);
//        }
//        return resultMap;
//    }



    public Map<String,Object> getVendorList(Map<String,Object> map){
        Map<String,Object> resultMap = new HashMap<>();
        if(map.containsKey("limit") && StringUtils.isNotEmpty(String.valueOf(map.get("limit")))){
            int count = organizeDao.getVendorCountByvendorId(map);
            resultMap.put("count",count);
        }
        List<Map<String,Object>> list = organizeDao.getVendorList(map);
        if(list.size()>0){
            resultMap.put("code", 0);
            resultMap.put("msg", "success");
            resultMap.put("result", true);
            resultMap.put("data",list);

        }else{
            resultMap.put("code", 1);
            resultMap.put("msg", "no data");
            resultMap.put("result", false);
            resultMap.put("data","");
        }
        return resultMap;

    }


    public Map<String,Object> selectVendor(String vendorName){
        Map<String,Object> resultMap = new HashMap<>();
        List<Map<String,Object>> list = organizeDao.selectVendor(vendorName);
        if(list.size()>0){
            resultMap.put("code", 0);
            resultMap.put("msg", "success");
            resultMap.put("result", true);
            resultMap.put("data",list);
        }else{
            resultMap.put("code", 1);
            resultMap.put("msg", "no data");
            resultMap.put("result", false);
            resultMap.put("data","");
        }
        return resultMap;
    }

    public Map<String,Object> getCompanyList(Map<String,Object> map){
        Map<String,Object> resultMap = new HashMap<>();
        List<Map<String,Object>> list = organizeDao.getCompanyList(map);
        if(list.size()>0){
            resultMap.put("code", 0);
            resultMap.put("msg", "success");
            resultMap.put("result", true);
            resultMap.put("data",list);

        }else{
            resultMap.put("code", 1);
            resultMap.put("msg", "no data");
            resultMap.put("result", false);
            resultMap.put("data","");
        }
        return resultMap;
    }



    public Map<String,Object> getStoreList(Map<String,Object> map){
        Map<String,Object> resultMap = new HashMap<>();
        int count = organizeDao.getStoreCountByEntityId(map);
        if(count>0){
            List<Map<String,Object>> list = organizeDao.getStoreListByEntityId(map);
            resultMap.put("code", 0);
            resultMap.put("msg", "success");
            resultMap.put("result", true);
            resultMap.put("count",count);
            resultMap.put("data",list);
        }else{
            resultMap.put("code", 1);
            resultMap.put("msg", "no data");
            resultMap.put("result", false);
            resultMap.put("count",0);
            resultMap.put("data","");
        }
        return resultMap;
    }



    public Map<String,Object> getOrganizationList(Map<String,Object> map){
        Map<String,Object> resultMap = new HashMap<>();
        List<Map<String,Object>> list = organizeDao.getOrganizationListByEntityId(map);
        if(list.size()>0){
            resultMap.put("code", 0);
            resultMap.put("msg", "success");
            resultMap.put("result", true);
            resultMap.put("count",list.size());
            resultMap.put("data",list);
        }else{
            resultMap.put("code", 1);
            resultMap.put("msg", "no data");
            resultMap.put("result", false);
            resultMap.put("count",0);
            resultMap.put("data","");
        }
        return resultMap;
    }



    public Map<String,Object> selectOrganization(Map<String,Object> map){
        Map<String,Object> resultMap = new HashMap<>();
        List<Map<String,Object>> list = organizeDao.selectOrganization(map);
        if(list.size()>0){
            resultMap.put("code", 0);
            resultMap.put("msg", "success");
            resultMap.put("result", true);
            resultMap.put("count",1);
            resultMap.put("data",list.get(0));
        }else{
            resultMap.put("code", 1);
            resultMap.put("msg", "no data");
            resultMap.put("result", false);
            resultMap.put("count",0);
            resultMap.put("data","");
        }
        return resultMap;
    }









}
