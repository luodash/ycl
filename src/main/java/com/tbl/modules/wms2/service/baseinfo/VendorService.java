package com.tbl.modules.wms2.service.baseinfo;

import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.wms2.entity.baseinfo.VendorEntity;

import java.util.Map;

/**
 * 供应商档案
 *
 * @author DASH
 */
public interface VendorService {


    /**
     * 功能描述:列表页
     * 分页
     * @param pageTbl
     * @param map
     * @return PageUtils
     */
    PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map);


    /**
     * 根据id，获取库位信息
     * @param id
     * @return
     */
    VendorEntity findById(Long id);
}
