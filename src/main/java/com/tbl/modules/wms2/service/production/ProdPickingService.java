package com.tbl.modules.wms2.service.production;

import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.modules.wms2.entity.insidewarehouse.check.YclCheckEntity;
import com.tbl.modules.wms2.entity.insidewarehouse.check.YclCheckLineEntity;
import com.tbl.modules.wms2.entity.production.YclMaterialOutEntity;

import java.util.List;
import java.util.Map;

/**
 * 盘库
 *
 * @author DASH
 */
public interface ProdPickingService {

    /**
     * 功能描述:保存
     *
     * @param entity
     * @return boolean
     */
    boolean save(YclMaterialOutEntity entity);

    /**
     * 批量保存
     *
     * @param entitys
     * @return boolean
     */
    boolean saveBatch(List<YclMaterialOutEntity> entitys);

    /**
     * 删除
     *
     * @param ids
     * @return boolean
     */
    boolean delete(String ids);


}
