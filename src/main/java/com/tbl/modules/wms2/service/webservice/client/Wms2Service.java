package com.tbl.modules.wms2.service.webservice.client;

import com.baomidou.mybatisplus.service.IService;
import org.apache.poi.ss.formula.functions.T;

import java.util.List;
import java.util.Map;

/**
 * wms调用接口入口（客户端）
 * @author 70486
 */

public interface Wms2Service extends IService<T> {


    /**
     * 待质检信息
     * @param params
     */
    String FEWMS202(Map<String, Object> params,String itemCode);


}
