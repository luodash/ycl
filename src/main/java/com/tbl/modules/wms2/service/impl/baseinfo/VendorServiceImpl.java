package com.tbl.modules.wms2.service.impl.baseinfo;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.wms2.dao.baseinfo.VendorDao;
import com.tbl.modules.wms2.dao.baseinfo.WarehouseDao;
import com.tbl.modules.wms2.entity.baseinfo.VendorEntity;
import com.tbl.modules.wms2.entity.baseinfo.WarehouseEntity;
import com.tbl.modules.wms2.service.baseinfo.VendorService;
import com.tbl.modules.wms2.service.baseinfo.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * @author DASH
 */
@Service("yddl2VendorService")
@Transactional(rollbackFor = Exception.class)
public class VendorServiceImpl extends ServiceImpl<VendorDao, VendorEntity> implements VendorService {


    @Autowired
    VendorDao vendorDao;

    @Override
    public PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map) {
        String sortOrder = pageTbl.getSortorder();
        String sortName = pageTbl.getSortname();
        if (StringUtils.isEmptyString(sortName)) {
            sortName = "id";
        }
        if (StringUtils.isEmptyString(sortOrder)) {
            sortOrder = "desc";
        }
        Page page = new Page(pageTbl.getPageno(), pageTbl.getPagesize(), sortName, "asc".equalsIgnoreCase(sortOrder));
        return new PageUtils(page.setRecords(vendorDao.getPageList(page, map)));
    }

    @Override
    public VendorEntity findById(Long id) {
        return id == null || id==0L ? new VendorEntity() : vendorDao.findById(id);
    }
}
