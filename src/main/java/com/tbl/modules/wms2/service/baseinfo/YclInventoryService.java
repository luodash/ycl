package com.tbl.modules.wms2.service.baseinfo;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.wms2.entity.baseinfo.YclInventory;

import java.util.List;
import java.util.Map;

public interface YclInventoryService extends IService<YclInventory> {
    /**
     * 列表页数据
     *
     * @param pageTbl
     * @param map
     * @return PageUtils
     */
    PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map);

    List<Map<String, Object>> getSelectCode(Map<String, Object> map);

    YclInventory selectTransferById(Long id);

    Long getSequence();

    /**
     * 手持机打印字符串
     *
     * @param map
     * @return PageUtils
     */
    List printInventoryList(Map<String, Object> map);
}
