package com.tbl.modules.wms2.service.baseinfo;

import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.modules.wms2.entity.baseinfo.PlantEntity;

import java.util.List;
import java.util.Map;

/**
 * 库存
 *
 * @author DASH
 */
public interface PlantService {




    /**
     * 功能描述:列表页 主表
     *
     * @param page
     * @param plantEntity
     * @return Page
     */
    Page<PlantEntity> getPageList(Page page, PlantEntity plantEntity);



    PlantEntity findById(String id);

    Map<String,Object> getPlantsByEntityId(Integer entityId);

    Map<String,Object> getOPByPlantCode (String plantCode);

    Map<String,Object> selectPlant(String departCode);

}
