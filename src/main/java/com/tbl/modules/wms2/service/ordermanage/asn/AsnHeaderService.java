package com.tbl.modules.wms2.service.ordermanage.asn;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.modules.wms2.entity.ordermanage.asn.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author DASH
 */
public interface AsnHeaderService extends IService<AsnHeaderEntity> {

    /**
     * 列表页数据
     *
     * @param asnQueryDTO
     * @return PageUtils
     */
    List<AsnVO> listViewAsn(AsnQueryDTO asnQueryDTO);

    Long getSequence();

    Boolean insertAsn(AsnCreateDTO asnHeaderEntity);

    void updateAsnHeader(AsnHeaderEntity asnHeaderEntity);


    /**
     * 根据订单查询送退货单
     *
     * @param poCode
     * @param poLineNum
     * @return
     */
    List<AsnHeaderPoVo> listViewHeader(String poCode, String poLineNum);

    /**
     * 根据订单查询送退货单
     *
     * @param asnHeaderQueryVo
     * @return
     */
    List<AsnHeaderVo> listViewHeader(AsnHeaderQueryDTO asnHeaderQueryVo);

    /**
     * 第三栏查询
     * 物料上架显示上架列
     * 未上架现在物料子表
     *
     * @param asnQueryDTO
     * @return
     */
    List<Map<String, Object>> listViewAsnSelect(AsnQueryDTO asnQueryDTO);


    /****************************************************************************************/


    Boolean saveSC(AsnCreateDTO asn);

    Boolean rejectLine(String asnLineId);

    Boolean saveTHSC(List<AsnCreateDTO> asnCreateDTOS);
}
