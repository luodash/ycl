package com.tbl.modules.wms2.service.remotedata;

import com.tbl.modules.wms2.entity.remotedata.CronJobEntity;

import java.util.List;
import java.util.Map;

/**
 * 定时任务
 *
 * @author DASH
 */

public interface CronJobService {

    CronJobEntity findById(Integer id);

    List<CronJobEntity> getList(Map map);




}
