package com.tbl.modules.wms2.service.impl.interfacerecord;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.Query;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.wms2.dao.interfacerecord.InterfaceRunDAO;
import com.tbl.modules.wms2.entity.ycloutstorage.YclOutStorageDetail;
import com.tbl.modules.wms2.interfacerecord.InterfaceRun;
import com.tbl.modules.wms2.service.interfacerecord.InterfaceRunService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 接口执行实现类
 * @author 70486
 **/
@Service
public class InterfaceRunServiceImp extends ServiceImpl<InterfaceRunDAO, InterfaceRun> implements InterfaceRunService {

	/**
	 * 离线接口执行表
	 */
	@Autowired
	private InterfaceRunDAO interfaceRunDAO;
	
	/**
	 * 插入离线执行接口表
	 * @param code 接口编码
	 * @param name 接口名称
	 * @param requestString 请求报文
	 * @param operateTime 操作时间
	 * @return Long 离线接口表主键
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public Integer insertRun(String code, String name, String requestString, String operateTime, String itemCode) {
		InterfaceRun interfaceRun = new InterfaceRun();
    	interfaceRun.setInterfacecode(code);
    	interfaceRun.setInterfacename(name);
//    	if (shipno.startsWith("PDD") && StringUtils.isBlank(qacode)){
//    		interfaceRun.setCblogParams(requestString);
//		}else {
//    		interfaceRun.setParamsinfo(requestString);
//		}
//    	interfaceRun.setQacode(qacode);
//    	interfaceRun.setBatchNo(batchNo);
//    	interfaceRun.setSpesign(spesign);
//    	interfaceRun.setShipno(shipno);

		return interfaceRunDAO.insert(interfaceRun);
	}

	/**
	 * 获取接口离线执行维护列表的数据
	 * @param  map
	 * @return PageUtils
	 */
	@Override
	public PageUtils queryPage(Map<String,Object> map){
		String interfacecode = (String)map.get("interfacecode");
		String batchNo = (String)map.get("batchNo");
		String shipNo = (String)map.get("shipNo");
		String qaCode = (String)map.get("qaCode");
		String success = (String)map.get("success");
		Page<InterfaceRun> page = this.selectPage(new Query<InterfaceRun>(map).getPage(), new EntityWrapper<InterfaceRun>()
				.like(StringUtils.isNotBlank(interfacecode),"INTERFACECODE", interfacecode != null ? interfacecode.trim() : null)
				.like(StringUtils.isNotBlank(batchNo),"BATCH_NO", batchNo != null ? batchNo.trim() : null)
				.like(StringUtils.isNotBlank(shipNo), "SHIPNO", shipNo != null ? shipNo.trim() : null)
				.like(StringUtils.isNotBlank(qaCode),"QACODE", qaCode != null ? qaCode.trim() : null)
				.like(StringUtils.isNotBlank(success),"ISSUCCESS",success != null ? success.trim() : null));
		return new PageUtils(page.setRecords(page.getRecords()));
	}

	/**
	 * 批量更新接口离线执行表状态
	 * @param lstOutStorageDetail 发货单明细
	 * @param state 状态
	 */
	@Override
	public void updateStateBatches(List<YclOutStorageDetail> lstOutStorageDetail, String state){
		interfaceRunDAO.updateStateBatches(lstOutStorageDetail, state);
	}

	/**
	 * 更新接口执行表
	 * @param interfaceRun 接口数据
	 */
	@Override
	public void updateInterfaceRunByid(InterfaceRun interfaceRun){
		interfaceRunDAO.updateInterfaceRunByid(interfaceRun);
	}

	/**
	 * 获取定时任务需要执行的离线接口
	 * @return InterfaceRun
	 */
	@Override
	public List<InterfaceRun> findJobsInterfaceRuns(){
		return interfaceRunDAO.findJobsInterfaceRuns();
	}
}
