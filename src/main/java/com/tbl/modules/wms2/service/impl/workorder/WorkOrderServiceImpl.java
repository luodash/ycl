package com.tbl.modules.wms2.service.impl.workorder;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.modules.wms2.dao.workorder.WorkOrderDAO;
import com.tbl.modules.wms2.entity.purchasein.OutSourcingPOEntity;
import com.tbl.modules.wms2.entity.workorder.ReceiveOrderEntity;
import com.tbl.modules.wms2.entity.workorder.WorkOrderEntity;
import com.tbl.modules.wms2.service.workorder.WorkOrderService;
import jxl.write.Blank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 委外加工
 *
 * @author DASH
 */
@Service("WorkOrderService")
public class WorkOrderServiceImpl extends ServiceImpl<WorkOrderDAO, WorkOrderEntity> implements WorkOrderService {

    @Resource
    WorkOrderDAO workOrderDAO;


    /**
     * 委外加工-创建或保存接收数据，不做实际接收
     * @param map
     * @return
     */
    @Override
    public Map<String,Object> createReceive(Map<String, Object> map){
        return workOrderDAO.createReceive(map);
    }

    /**
     * 委外加工-接收处理
     * @param map
     * @return
     */
    public Map<String,Object> receiveProcess(Map<String,Object> map){
        return workOrderDAO.receiveProcess(map);
    }

    /**
     * 委外加工-工单完工提交时进行验证
     * S 为数量检验通过、E 为数量校验失败
     * @param map
     * @return
     */
    public Map<String,Object> validateQuantity(Map<String,Object> map){
        return workOrderDAO.validateQuantity(map);
    }

    /**
     * 委外加工-工单完工保存按钮
     * @param map
     * @return
     */
    public Map<String,Object> createComplete(Map<String,Object> map){
        return workOrderDAO.createComplete(map);
    }

    /**
     * 委外加工-工单完工提交按钮
     * @param map
     * @return
     */
    public Map<String,Object> completeProcess(Map<String,Object> map){
        return workOrderDAO.completeProcess(map);
    }




    public Map<String,String> returnProcess(Long rcvId){
        /**
         *  TODO 使用该回退服务之前，先要查询CUX_WX_RCV_V视图attribute1字段，用于判断是否允许进行事务处理操作。
         * 状态值：‘NEW’，新建转态，允许进行采购接收操作，接收行数据允许修改
         *    ‘RECEIVE’，已接收状态，允许进行完工、采购退回操作，接收行数据不允许修改，完工行允许新增修改
         *    ‘COMPLETE’，已完工状态，允许进行采购退回操作，接收行数据不允许修改，完工行不允许新增修改
         *    ‘RETURN’，已退回状态，不允许进行任何操作，完工行接收行都不允许变更
         */
        Map<String,Object> paramsMap = new HashMap<String,Object>();
        Map<String,String> resultMap = new HashMap<>();
        paramsMap.put("rcvId",rcvId);
        workOrderDAO.returnProcess(paramsMap);
        if(paramsMap.containsKey("retCode")){
            if("E".equals(paramsMap.get("retCode"))){
                resultMap.put("retCode","error");
                resultMap.put("retMess","操作失败");
            }else if("S".equals(paramsMap.get("retCode"))){
                // TODO 到达这里说明EBS上已经回退成功，下面要在WMS上进行回退处理，后续代码需要补全
                resultMap.put("retCode","success");
                resultMap.put("retMess","回退成功");
            }
        }
        return resultMap;
    }

    public OutSourcingPOEntity getOutSourcingPOByPoId(Long poId) {
        return workOrderDAO.getOutSourcingPOByPoId(poId);
    }

    public List<ReceiveOrderEntity> getRcvListByPoId(Long poId) {
        return workOrderDAO.getRcvListByPoId(poId);
    }

    public List<WorkOrderEntity> getWorkOrderListByRcvId(Long poId) {
        return workOrderDAO.getWorkOrderListByRcvId(poId);
    }

    public ReceiveOrderEntity getRcvInfoByRcvId(Long poId) {
        return workOrderDAO.getRcvInfoByRcvId(poId);
    }

    public String getReceiveOrderState(Long rcvId) {
        List<Map<String,Object>> isPermissionMapList =  workOrderDAO.getReceiveOrderState(rcvId);
        Map<String,Object>  isPermissionMap = isPermissionMapList.get(0);
        String state = new String("");
        if(isPermissionMap != null) {
            state = isPermissionMap.get("attribute1") == null ? "" :  isPermissionMap.get("attribute1").toString();
        }
        return state;
    }

    public List<Map<String,Object>> getDescriptionByOrgId(Long orgId){
        return workOrderDAO.getDescriptionByOrgId(orgId);
    }

    public List<Map<String,Object>> getLocatorListByDesc(Map paramsMap){
        return workOrderDAO.getLocatorListByDesc(paramsMap);
    }

    public Map<String,Object> getFtInfo(Map map){
        Map<String,Object> resultMap = new HashMap<>();
        int count = workOrderDAO.getFtCount(map);
        if(count>0){
            List<Map<String,Object>> pageList = workOrderDAO.getFtInfo(map);
            resultMap.put("code",0);
            resultMap.put("msg","success");
            resultMap.put("result",true);
            resultMap.put("count",count);
            resultMap.put("data",pageList);
        }else{
            resultMap.put("code",1);
            resultMap.put("msg","no data");
            resultMap.put("result",false);
            resultMap.put("count",0);
            resultMap.put("data","");
        }
        return resultMap;
    }


}
