package com.tbl.modules.wms2.service.impl.material;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.modules.wms2.dao.material.MaterialBackMapper;
import com.tbl.modules.wms2.entity.material.MaterialBack;
import com.tbl.modules.wms2.entity.material.MaterialBackPrint;
import com.tbl.modules.wms2.entity.material.MaterialBackViewModel;
import com.tbl.modules.wms2.service.material.MaterialBackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 退料入库单详情 ServiceImpl
 *
 * @author Sweven
 * @date 2020-06-19
 */
@Service
public class MaterialBackServiceImpl extends ServiceImpl<MaterialBackMapper, MaterialBack> implements MaterialBackService {

    @Autowired
    private MaterialBackMapper materialBackMapper;

    @Override
    public List<MaterialBackViewModel> getPageList(Page page, MaterialBack materialBack) {
        return materialBackMapper.getPageList(page, materialBack);
    }

    @Override
    public void push(List<MaterialBack> backList) {
        materialBackMapper.insertList(backList);
    }

    @Override
    public Integer count(MaterialBack materialBack) {
        return materialBackMapper.count(materialBack);
    }

    @Override
    public List<MaterialBack> getList(MaterialBack materialBack) {
        return materialBackMapper.getList(materialBack);
    }

    @Override
    public List<MaterialBackPrint> findByLineId(String lineId) {
        return materialBackMapper.findByLineId(lineId);
    }

}
