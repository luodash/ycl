package com.tbl.modules.wms2.service.impl.baseinfo;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.wms2.dao.baseinfo.StorehouseDao;
import com.tbl.modules.wms2.entity.baseinfo.StorehouseEntity;
import com.tbl.modules.wms2.entity.baseinfo.WarehouseEntity;
import com.tbl.modules.wms2.service.baseinfo.StorehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 库位
 *
 * @author DASH
 */
@Service("yddl2StorehouseService")
public class StorehouseServiceImpl extends ServiceImpl<StorehouseDao, StorehouseEntity> implements StorehouseService {

    @Resource
    StorehouseDao storehouseDao;

    @Override
    public boolean save(StorehouseEntity entity) {
        return this.insertOrUpdate(entity);
    }

    @Override
    public PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map) {
        if (map.get("code") != null) {
            map.put("code", map.get("code").toString().toUpperCase());
        }
        String sortOrder = pageTbl.getSortorder();
        String sortName = pageTbl.getSortname();
        if (StringUtils.isEmptyString(sortName)) {
            sortName = "id";
        }
        if (StringUtils.isEmptyString(sortOrder)) {
            sortOrder = "desc";
        }
        Page page = new Page(pageTbl.getPageno(), pageTbl.getPagesize(), sortName, "asc".equalsIgnoreCase(sortOrder));
        return new PageUtils(page.setRecords(storehouseDao.getPageList(page, map)));
    }

    @Override
    public boolean delete(String ids) {
        return this.deleteBatchIds(StringUtils.stringToList(ids));
    }

    @Override
    public StorehouseEntity findById(Long id) {
        return id == null || id==0L ? new StorehouseEntity() : storehouseDao.findById(id);
    }
}
