package com.tbl.modules.wms2.service.impl.operation;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.platform.entity.system.User;
import com.tbl.modules.wms.dao.baseinfo.WarehouseDAO;
import com.tbl.modules.wms.entity.baseinfo.Warehouse;
import com.tbl.modules.wms2.dao.operation.YclInOutFlowDao;
import com.tbl.modules.wms2.dao.ordermanage.asn.AsnHeaderDAO;
import com.tbl.modules.wms2.dao.purchasein.PurchaseOrderDAO;
import com.tbl.modules.wms2.entity.baseinfo.WarehouseEntity;
import com.tbl.modules.wms2.entity.operation.YclInOutFlowEntity;
import com.tbl.modules.wms2.entity.ordermanage.asn.AsnQueryDTO;
import com.tbl.modules.wms2.entity.ordermanage.asn.AsnVO;
import com.tbl.modules.wms2.entity.purchasein.PurchaseOrderSaveEntity;
import com.tbl.modules.wms2.entity.report.YclInventoryEntity;
import com.tbl.modules.wms2.entity.report.YclInventoryUpdateDTO;
import com.tbl.modules.wms2.service.common.CountService;
import com.tbl.modules.wms2.service.operation.YclInOutFlowService;
import com.tbl.modules.wms2.service.report.InventoryService;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import javax.xml.crypto.Data;
import java.util.*;

/**
 * @author DASH
 */
@Service
public class YclInOutFlowServiceImpl extends ServiceImpl<YclInOutFlowDao, YclInOutFlowEntity> implements YclInOutFlowService {

    @Resource(name = "yclInventoryService")
    InventoryService inventoryService;

    @Resource
    private CountService countService;

    @Resource
    PurchaseOrderDAO purchaseOrderDAO;

    @Resource
    WarehouseDAO warehouseDAO;

    @Resource
    AsnHeaderDAO asnHeaderDAO;

    @Override
    public boolean save(YclInOutFlowEntity yclInOutFlowEntity) {

        if (StrUtil.isBlank(yclInOutFlowEntity.getInoutType())) {
            throw new RuntimeException("出入库类型不能空");
        }

        HttpSession session = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest().getSession();
        String username = ((User) session.getAttribute("sessionUser")).getUsername();
        yclInOutFlowEntity.setCreateUser(username);
        yclInOutFlowEntity.setCreateTime(DateUtil.date());

        // 是否入库存
        if ("1".equals(yclInOutFlowEntity.getIsIn()) || StrUtil.isBlank(yclInOutFlowEntity.getIsIn())) {

            YclInventoryEntity yclInventoryEntity = inventoryService.find(yclInOutFlowEntity.getAreaCode(), yclInOutFlowEntity.getStoreCode(), yclInOutFlowEntity.getLocatorCode(), yclInOutFlowEntity.getMaterialCode(), yclInOutFlowEntity.getLotsNum());
            yclInOutFlowEntity.setBeforeIn(yclInventoryEntity.getQuality());

            YclInventoryUpdateDTO yclInventoryUpdateDTO = new YclInventoryUpdateDTO();
            yclInventoryUpdateDTO.setStoreCode(yclInOutFlowEntity.getStoreCode());
            yclInventoryUpdateDTO.setStoreName(yclInOutFlowEntity.getStoreName());
            yclInventoryUpdateDTO.setAreaCode(yclInOutFlowEntity.getAreaCode());
            yclInventoryUpdateDTO.setAreaName(yclInOutFlowEntity.getAreaName());
            yclInventoryUpdateDTO.setLocatorCode(yclInOutFlowEntity.getLocatorCode());
            yclInventoryUpdateDTO.setMaterialCode(yclInOutFlowEntity.getMaterialCode());
            yclInventoryUpdateDTO.setMaterialName(yclInOutFlowEntity.getMaterialName());
            yclInventoryUpdateDTO.setLotsNum(yclInOutFlowEntity.getLotsNum());
            yclInventoryUpdateDTO.setAmount(yclInOutFlowEntity.getUpdownQuantity());
            yclInventoryUpdateDTO.setInout(yclInOutFlowEntity.getInoutType());
            inventoryService.updateInventory(yclInventoryUpdateDTO);
        }
        yclInOutFlowEntity.setInoutCode(countService.getNextCount("INOUT"));
        yclInOutFlowEntity.setInoutLineNum(1);
        return this.insertOrUpdate(yclInOutFlowEntity);
    }

    @Override
    public Boolean saveBatch(List<YclInOutFlowEntity> yclInOutFlowEntities) {


        String inout = countService.getNextCount("INOUT");
        YclInventoryUpdateDTO yclInventoryUpdateDTO = new YclInventoryUpdateDTO();

//        AsnQueryDTO asnQueryDTO = new AsnQueryDTO();

        for (int i = 0; i < yclInOutFlowEntities.size(); i++) {
            YclInOutFlowEntity yclInOutFlowEntity = yclInOutFlowEntities.get(i);
            // 是否入库存
            if ("1".equals(yclInOutFlowEntity.getIsIn()) || StrUtil.isBlank(yclInOutFlowEntity.getIsIn())) {

                YclInventoryEntity yclInventoryEntity = inventoryService.find(yclInOutFlowEntity.getAreaCode(), yclInOutFlowEntity.getStoreCode(), yclInOutFlowEntity.getLocatorCode(), yclInOutFlowEntity.getMaterialCode(), yclInOutFlowEntity.getLotsNum());
                yclInOutFlowEntity.setBeforeIn(yclInventoryEntity.getQuality());

//                // 送退货详情
//                asnQueryDTO.setAsnNumber(yclInOutFlowEntity.getBusinessCode());
//                asnQueryDTO.setLotsNum(yclInOutFlowEntity.getLotsNum());
//                List<AsnVO> asnVOS = asnHeaderDAO.listViewAsn(asnQueryDTO);

                // wms 库存
//                yclInventoryUpdateDTO.setPoLineId(asnVOS.get(0).getPoLineId());
//                yclInventoryUpdateDTO.setSourceId();

                yclInventoryUpdateDTO.setStoreCode(yclInOutFlowEntity.getStoreCode());
                yclInventoryUpdateDTO.setStoreName(yclInOutFlowEntity.getStoreName());
                yclInventoryUpdateDTO.setAreaCode(yclInOutFlowEntity.getAreaCode());
                yclInventoryUpdateDTO.setAreaName(yclInOutFlowEntity.getAreaName());
                yclInventoryUpdateDTO.setLocatorCode(yclInOutFlowEntity.getLocatorCode());
                yclInventoryUpdateDTO.setMaterialCode(yclInOutFlowEntity.getMaterialCode());
                yclInventoryUpdateDTO.setMaterialName(yclInOutFlowEntity.getMaterialName());
                yclInventoryUpdateDTO.setLotsNum(yclInOutFlowEntity.getLotsNum());
                yclInventoryUpdateDTO.setAmount(yclInOutFlowEntity.getUpdownQuantity());
                yclInventoryUpdateDTO.setInout(yclInOutFlowEntity.getInoutType());
                inventoryService.updateInventory(yclInventoryUpdateDTO);
            }

            HttpSession session = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest().getSession();
            String username = ((User) session.getAttribute("sessionUser")).getUsername();
            yclInOutFlowEntity.setCreateUser(username);
            yclInOutFlowEntity.setCreateTime(DateUtil.date());
            yclInOutFlowEntity.setInoutCode(inout);
            yclInOutFlowEntity.setInoutLineNum(i + 1);

            this.insertOrUpdate(yclInOutFlowEntity);
        }

        return true;
    }

    @Override
    public PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map) {
        return null;
    }

    @Override
    public boolean delete(String ids) {
        return this.deleteBatchIds(StringUtils.stringToList(ids));
    }

    @Override
    public YclInOutFlowEntity findById(Long id) {
        return this.selectById(id);
    }

    @Override
    public List<YclInOutFlowEntity> listView(YclInOutFlowEntity yclInOutFlowEntity) {
        return this.selectList(new EntityWrapper<>(yclInOutFlowEntity));
    }
}
