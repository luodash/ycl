package com.tbl.modules.wms2.service.report;

import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.modules.wms2.entity.insidewarehouse.check.YclCheckEntity;
import com.tbl.modules.wms2.entity.insidewarehouse.check.YclCheckLineEntity;
import com.tbl.modules.wms2.entity.report.YclInventoryEntity;
import com.tbl.modules.wms2.entity.report.YclInventoryQueryDTO;
import com.tbl.modules.wms2.entity.report.YclInventoryUpdateDTO;

import java.util.List;
import java.util.Map;

/**
 * 库存
 *
 * @author DASH
 */
public interface InventoryService {

    /**
     * 功能描述:保存
     *
     * @param entity
     * @return boolean
     */
    boolean save(YclInventoryEntity entity);

    /**
     * 功能描述:列表页 主表
     *
     * @param page
     * @param map
     * @return Page
     */
    Page<YclInventoryEntity> getPageList(Page page, Map<String, Object> map);

    /**
     * 功能描述:列表页 主表
     *
     * @param page
     * @param yclInventoryEntity
     * @return Page
     */
    Page<YclInventoryEntity> getPageList(Page page, YclInventoryEntity yclInventoryEntity);

    /**
     * 删除
     *
     * @param ids
     * @return boolean
     */
    boolean delete(String ids);

    YclInventoryEntity findById(String id);

    /**
     * 单条
     *
     * @param areaCode     厂区
     * @param storeCode    仓库
     * @param locatorCode  库位
     * @param materialCode 物料号
     * @param lotsNum      批次
     * @return
     */
    YclInventoryEntity find(String areaCode, String storeCode, String locatorCode, String materialCode, String lotsNum);
    /**
     * 单条
     *
     * @param areaCode     厂区
     * @param storeCode    仓库
     * @param materialCode 物料号
     * @return
     */
    YclInventoryEntity find(String areaCode, String storeCode, String materialCode);

    /**
     * 条件查询
     *
     * @param yclInventoryEntity
     * @return
     */
    List<YclInventoryEntity> listView(YclInventoryEntity yclInventoryEntity);

    /**
     * 条件查询不包含负库存
     *
     * @param yclInventoryEntity
     * @return
     */
    List<YclInventoryEntity> listViewWithoutNegative(YclInventoryEntity yclInventoryEntity);

    /**
     * 更新库存
     *
     * @param inventoryUpdateDTO
     * @return
     */
    boolean updateInventory(YclInventoryUpdateDTO inventoryUpdateDTO);

    /**
     * 更新库存
     * @param yclInventoryEntity
     * @return
     */
    boolean updateInventoryById(YclInventoryEntity yclInventoryEntity);
}
