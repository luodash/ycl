package com.tbl.modules.wms2.service.operation;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.modules.wms2.entity.operation.OperationOtherLine;

import java.util.List;

/**
 * @author Sweven
 * @date 2020-06-25
 */
public interface OperationOtherLineService extends IService<OperationOtherLine> {

    List<OperationOtherLine> findByOtherCode(String otherCode);

}
