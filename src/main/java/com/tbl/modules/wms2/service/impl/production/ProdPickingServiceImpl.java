package com.tbl.modules.wms2.service.impl.production;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.wms2.dao.insidewarehouse.check.CheckDao;
import com.tbl.modules.wms2.dao.insidewarehouse.check.CheckLineDao;
import com.tbl.modules.wms2.dao.production.ProdPickingDAO;
import com.tbl.modules.wms2.dao.production.YclMeterialOutDAO;
import com.tbl.modules.wms2.entity.insidewarehouse.check.YclCheckEntity;
import com.tbl.modules.wms2.entity.insidewarehouse.check.YclCheckLineEntity;
import com.tbl.modules.wms2.entity.production.YclMaterialOutEntity;
import com.tbl.modules.wms2.service.insidewarehouse.check.CheckService;
import com.tbl.modules.wms2.service.production.ProdPickingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 盘库
 *
 * @author DASH
 */
@Service
public class ProdPickingServiceImpl extends ServiceImpl<YclMeterialOutDAO, YclMaterialOutEntity> implements ProdPickingService {

    @Override
    public boolean save(YclMaterialOutEntity entity) {
        return this.insertOrUpdateAllColumn(entity);
    }

    @Override
    public boolean saveBatch(List<YclMaterialOutEntity> entitys) {
        return this.insertOrUpdateBatch(entitys);
    }

    @Override
    public boolean delete(String ids) {
        return this.deleteBatchIds(StringUtils.stringToList(ids));
    }



}
