package com.tbl.modules.wms2.service.insidewarehouse.transferorder;

import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.modules.wms2.entity.insidewarehouse.transferorder.TransferOrderEntity;
import com.tbl.modules.wms2.entity.insidewarehouse.transferorder.TransferOrderLineEntity;

import java.util.List;
import java.util.Map;

/**
 * 移位移库
 *
 * @author DASH
 */
public interface TransferOrderService {

    /**
     * 功能描述:保存
     *
     * @param entity
     * @return boolean
     */
    //boolean save(TransferOrderEntity entity);

    /**
     * 功能描述:列表页 主表
     *
     * @return PageUtils
     */
    Map<String, Object> getPageList(Map map);


    Map<String,Object> getHeadPageList(Map map);

    List<TransferOrderEntity> getPageListByMap(Map map);


    /**
     * 删除
     *
     * @param ids
     * @return boolean
     */
    boolean delete(String ids);

    /**
     * 删除
     *
     * @param tranCodes
     * @return boolean
     */
    boolean deleteByTranCodes(String tranCodes);

    /**
     * 根据ID获取对象
     * @param id
     * @return
     */
    TransferOrderEntity findById(String id);

    /**
     * 根据行表ID获取对象
     * @param id
     * @return
     */
    List<TransferOrderEntity> getListById(String id);

    Map<String,String> insertHeader(Map<String,Object> map);

    Map<String,String> insertLine(Map<String,Object> map);

    Map<String,String> processOrder(Map<String,Object> map);

    Map<String, Object> getEBSFromLocator(String lineId);

    Map<String, Object> getEBSToLocator(String lineId);

    Map<String,Object> saveTransOrderMap(Map<String, Object> headerMap ,TransferOrderEntity line, String type);

}
