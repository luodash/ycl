package com.tbl.modules.wms2.service.impl.baseinfo;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.wms2.dao.baseinfo.PlantDao;
import com.tbl.modules.wms2.entity.baseinfo.PlantEntity;
import com.tbl.modules.wms2.service.baseinfo.PlantService;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 生产厂
 *
 * @author DASH
 */
@Service
public class PlantServiceImpl extends ServiceImpl<PlantDao, PlantEntity> implements PlantService {

    @Resource
    PlantDao plantDao;



    @Override
    public Page<PlantEntity> getPageList(Page page, PlantEntity plantEntity) {
        Wrapper wrapper = new EntityWrapper();
        if (StrUtil.isNotEmpty(plantEntity.getId())) {
            wrapper.eq("SCHEDULE_GROUP_ID", plantEntity.getId());
        }
        if (StrUtil.isNotEmpty(plantEntity.getCode())) {
            wrapper.eq("SCHEDULE_GROUP_NAME", plantEntity.getCode());
        }
        if (StrUtil.isNotEmpty(plantEntity.getDescription())) {
            wrapper.eq("DESCRIPTION", plantEntity.getDescription());
        }
        if (StrUtil.isNotEmpty(plantEntity.getOrganizeId())) {
            wrapper.eq("ORGANIZATION_ID", plantEntity.getOrganizeId());
        }
        Page<PlantEntity> mapPage = this.selectPage(page, wrapper);
        return mapPage;
    }


    @Override
    public PlantEntity findById(String id) {
        return this.selectById(id);
    }

    /**
     * 根据公司/实体ID获取生产厂下拉列表
     * @param entityId
     * @return
     */
    public Map<String, Object> getPlantsByEntityId(Integer entityId) {
        Map<String, Object> map = new HashMap<>(4);
        List<Map<String,Object>> list = plantDao.getPlantsByEntityId(entityId);
        if(list.size()>0){
            map.put("code", 0);
            map.put("msg", "success");
            map.put("result", true);
            map.put("data", list);
        }else {
            map.put("code", 1);
            map.put("msg", "no data");
            map.put("result", false);
            map.put("data", "");
        }
        return map;
    }


    /**
     * 根据生产厂编码-获取工序-下拉框列表
     * @param plantCode
     * @return
     */
    public Map<String,Object> getOPByPlantCode(String plantCode){
        Map<String, Object> map = new HashMap<>(4);
        List<Map<String,Object>> list = plantDao.getOPByPlantCode(plantCode);
        if(list.size()>0){
            map.put("code", 0);
            map.put("msg", "success");
            map.put("result", true);
            map.put("data", list);
        }else {
            map.put("code", 1);
            map.put("msg", "no data");
            map.put("result", false);
            map.put("data", "");
        }
        return map;
    }


    /**
     * 生产厂/部门的模糊查询
     * @param departCode
     * @return
     */
    public Map<String,Object> selectPlant(String departCode){
        Map<String,Object> map = new HashMap<>(4);
        List<Map<String,Object>> list = plantDao.selectPlant(departCode);
        if(list.size()>0){
            map.put("code", 0);
            map.put("msg", "success");
            map.put("result", true);
            map.put("data", list);
        }else{
            map.put("code", 1);
            map.put("msg", "no data");
            map.put("result", false);
            map.put("data", "");
        }
        return map;
    }


}
