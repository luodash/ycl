package com.tbl.modules.wms2.service.baseinfo;

import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.wms2.entity.baseinfo.FactoryMachineEntity;

import java.util.List;
import java.util.Map;

/**
 * 工厂机台
 *
 * @author DASH
 */
public interface FactoryMachineService {


    /**
     * 功能描述:列表页
     * 分页
     * @param pageTbl
     * @param map
     * @return PageUtils
     */
    PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map);

    /**
     * 获取所有的班次
     * @return
     */
    List<String> getShiftCode();

    int getCount(Map<String, Object> map);


    /**
     * 功能描述：获取生产厂-工序-机台的下拉列表
     * @param map
     * @return
     */
    List<FactoryMachineEntity> getFactoryMachineList(Map<String, Object> map);

    /**
     * 功能描述：获取生产厂的列表信息
     * @param map
     * @return
     */
    List<FactoryMachineEntity> getFactoryList(Map<String,Object> map);

    /**
     * 功能描述：通过生产厂获取工序列表信息
     * @param map
     * @return
     */
    List<Map<String,Object>> getOpByDepartCode(Map<String, Object> map);


}
