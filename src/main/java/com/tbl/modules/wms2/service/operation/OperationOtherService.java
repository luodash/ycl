package com.tbl.modules.wms2.service.operation;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.tbl.modules.wms2.entity.operation.*;
import org.apache.ibatis.annotations.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @author Sweven
 * @date 2020-06-25
 */
public interface OperationOtherService extends IService<OperationOther> {

    /**
     * 获取列表数据
     *
     * @param page
     * @param paramsMap
     * @return java.util.List<com.tbl.modules.wms2.entity.operation.OperationOtherVO>
     */
    List<OperationOtherVO> getPageList(Page page, Map<String, Object> paramsMap);

    /**
     * 获取列表数据（主）
     *
     * @param page
     * @param paramsMap
     * @return java.util.List<com.tbl.modules.wms2.entity.operation.OperationOtherVO>
     */
    List<OperationOtherVO> getPageListApi(Page page, Map<String, Object> paramsMap);

    /**
     * 查询打印数据
     *
     * @param otherCode
     * @return com.tbl.modules.wms2.entity.operation.OperationOtherPrint
     */
    OperationOtherPrint findByOtherCode(@Param("otherCode") String otherCode);

    /**
     * 创建或更新头
     *
     * @param operationOtherHeadDto
     * @return com.tbl.modules.wms2.entity.operation.OperationOtherHeadDTO
     */
    @Transactional
    OperationOtherHeadDTO createHead(OperationOtherHeadDTO operationOtherHeadDto);

    /**
     * 创建或更新行
     *
     * @param lineDTO
     * @return com.tbl.modules.wms2.entity.operation.OperationOtherLineDTO
     */
    OperationOtherLineDTO createLine(OperationOtherLineDTO lineDTO);

    /**
     * 删除行
     *
     * @param map
     * @return
     */
    Map<String, Object> deleteLine(@Param("map") Map<String, Object> map);

    /**
     * 数据校验
     *
     * @param validDTO
     * @return com.tbl.modules.wms2.entity.operation.OperationOtherValidDTO
     */
    OperationOtherValidDTO validateData(OperationOtherValidDTO validDTO);

    /**
     * 事务处理
     *
     * @param processDTO
     * @return com.tbl.modules.wms2.entity.operation.OperationOtherProcessDTO
     */
    OperationOtherProcessDTO process(OperationOtherProcessDTO processDTO);

}

