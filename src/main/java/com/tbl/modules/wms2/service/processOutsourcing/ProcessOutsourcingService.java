package com.tbl.modules.wms2.service.processOutsourcing;

import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.wms2.entity.operation.YclInOutFlowEntity;
import com.tbl.modules.wms2.entity.processOutsourcing.ProcessOutsourcingEntity;
import com.tbl.modules.wms2.entity.report.YclInventoryEntity;

import java.util.Map;

/**
 * 工序外协
 */
public interface ProcessOutsourcingService {

    /**
     * 功能描述:保存
     *
     * @param entity
     * @return boolean
     */
    boolean save(ProcessOutsourcingEntity entity);

    /**
     *
     * @param page
     * @param map
     * @return
     */
    Page<ProcessOutsourcingEntity> getPageList(Page page, Map<String, Object> map);

     /**
     *功能描述:列表页 主表
     * @param page
     * @param processOutsourcingEntity
     * @return
     */
    Page<ProcessOutsourcingEntity> getPageList(Page page,ProcessOutsourcingEntity processOutsourcingEntity);

    /**
     * 查总条数
     * @param map
     * @return
     */
    Integer count(Map<String,Object> map);

    /**
     * 删除
     *
     * @param ids
     * @return boolean
     */
    boolean delete(String ids);

    /**
     * 获取对象
     * @param id
     * @return
     */
    ProcessOutsourcingEntity findById(String id);
}
