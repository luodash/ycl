package com.tbl.modules.wms2.service.ordermanage.delivery;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.modules.wms2.entity.ordermanage.delivery.DeliveryAddSCDTO;
import com.tbl.modules.wms2.entity.ordermanage.delivery.DeliveryEntity;

import java.util.List;

/**
 * 收货
 *
 * @author DASH
 */
public interface DeliveryService extends IService<DeliveryEntity> {

    /**
     * 功能描述:保存
     *
     * @param entity
     * @return boolean
     */
    boolean add(DeliveryEntity entity, Boolean isQC);

    /**
     * 功能描述: 编辑
     *
     * @param entity
     * @return boolean
     */
    boolean edit(DeliveryEntity entity);

    /**
     * 功能描述:列表页 主表
     *
     * @param page
     * @param map
     * @return Page
     */
//    Page<YclInventoryEntity> getPageList(Page page, Map<String, Object> map);

    /**
     * 功能描述:列表页 主表
     *
     * @param page
     * @param yclInventoryEntity
     * @return Page
     */
//    Page<YclInventoryEntity> getPageList(Page page, YclInventoryEntity yclInventoryEntity);

    /**
     * 删除
     *
     * @param ids
     * @return boolean
     */
    boolean delete(String ids);

    DeliveryEntity findById(String id);

    /**
     * 条件查询
     *
     * @param deliveryEntity
     * @return
     */
    List<DeliveryEntity> listView(DeliveryEntity deliveryEntity);

    DeliveryEntity getOne(DeliveryEntity deliveryEntity);

    Boolean addSC(DeliveryAddSCDTO deliveryAddSCDTO);
}
