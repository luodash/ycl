package com.tbl.modules.wms2.service.impl.purchasein;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.modules.wms2.dao.purchasein.FactoryOrderDao;
import com.tbl.modules.wms2.entity.operation.YclInOutFlowEntity;
import com.tbl.modules.wms2.service.purchasein.FactoryOrderService;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Map;

/**
 * 出厂单
 */
@Service
public class FactoryOrderServiceImpl extends ServiceImpl<FactoryOrderDao, YclInOutFlowEntity> implements FactoryOrderService {
    @Autowired
    private FactoryOrderDao factoryOrderDao;

    @Override
    public boolean save(YclInOutFlowEntity entity) {
        return this.insertOrUpdate(entity);
    }

    @Override
    public Page<YclInOutFlowEntity> getPageList(Page page, Map<String, Object> map, YclInOutFlowEntity yclInOutFlowEntity) {
        EntityWrapper<YclInOutFlowEntity> ew = new EntityWrapper<YclInOutFlowEntity>();
        ew.eq("BUSINESS_TYPE",1)
                .eq(StrUtil.isNotEmpty(yclInOutFlowEntity.toString()),"",yclInOutFlowEntity.toString())
                .eq(StrUtil.isNotEmpty(yclInOutFlowEntity.toString()),"",yclInOutFlowEntity.toString());
        Page<YclInOutFlowEntity> mapPage = this.selectPage(page,ew);
        return mapPage;
    }

    @Override
    public boolean delete(String ids) {
        return this.deleteBatchIds(Arrays.asList(ids.split(",")));
    }

    @Override
    public YclInOutFlowEntity findById(String id) {
        return this.selectById(id);
    }
}
