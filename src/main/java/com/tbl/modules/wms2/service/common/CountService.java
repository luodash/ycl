package com.tbl.modules.wms2.service.common;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.modules.wms2.common.support.YclCountEntity;
import com.tbl.modules.wms2.dao.common.CountDao;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 库存
 *
 * @author DASH
 */
public interface CountService extends IService<YclCountEntity> {


    YclCountEntity get(Integer year, Integer month, Integer dayOfMonth,String id);

    YclCountEntity getById(String id);

    List<YclCountEntity> getALL();

    Boolean update(YclCountEntity entity);



    String getNextCount(String var);
}
