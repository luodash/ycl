package com.tbl.modules.wms2.service.purchasein;

import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.wms2.entity.purchasein.PurchaseOrderEntity;

import java.util.List;
import java.util.Map;

/**
 * 采购订单
 *
 * @author DASH
 */
public interface PurchaseOrderService {

    /**
     * 功能描述:保存
     *
     * @param entity
     * @return boolean
     */
//    boolean save(WarehouseEntity entity);

    /**
     * 功能描述:列表页
     *
     * @param pageTbl
     * @param map
     * @return PageUtils
     */
    PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map);

    /**
     * 功能描述:删除
     *
     * @param ids
     * @return boolean
     */
//    boolean delete(String ids);

//    WarehouseEntity findById(Long id);

    /**
     * 通过委外采购订单编码获取委外视图数据
     * @param poCode
     * @return
     */
    PurchaseOrderEntity getWxPoView(String poCode);


}
