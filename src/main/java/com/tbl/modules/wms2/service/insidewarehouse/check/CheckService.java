package com.tbl.modules.wms2.service.insidewarehouse.check;

import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.modules.wms2.entity.insidewarehouse.check.YclCheckEntity;
import com.tbl.modules.wms2.entity.insidewarehouse.check.YclCheckLineEntity;
import com.tbl.modules.wms2.entity.insidewarehouse.check.YclCheckQueryDTO;

import java.util.List;
import java.util.Map;

/**
 * 盘库
 *
 * @author DASH
 */
public interface CheckService {

    /**
     * 功能描述:保存
     *
     * @param entity
     * @return boolean
     */
    boolean save(YclCheckEntity entity);

    /**
     * 功能描述:列表页 主表
     *
     * @param page
     * @param checkQueryDTO
     * @return PageUtils
     */
    Page<YclCheckEntity> getPageList(Page page, YclCheckQueryDTO checkQueryDTO);

    List<YclCheckLineEntity> getPageListByMap(Map map);


    /**
     * 删除
     *
     * @param ids
     * @return boolean
     */
    boolean delete(String ids);

    /**
     * 删除
     *
     * @param checkCodes
     * @return boolean
     */
    boolean deleteByCheckCodes(String checkCodes);

    YclCheckEntity findById(String id);

    YclCheckEntity findByCheckCode(String checkCode);


    List<YclCheckEntity> listView(YclCheckQueryDTO checkQueryDTO);
}
