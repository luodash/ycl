package com.tbl.modules.wms2.service.impl.sale;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.wms2.dao.sale.YclSaleOutDAO;
import com.tbl.modules.wms2.dao.sale.YclSaleOutLineDAO;
import com.tbl.modules.wms2.entity.sale.YclSaleEntity;
import com.tbl.modules.wms2.entity.sale.YclSaleLineEntity;
import com.tbl.modules.wms2.service.sale.YclSaleOutLineService;
import com.tbl.modules.wms2.service.sale.YclSaleOutService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 盘库
 *
 * @author DASH
 */
@Service
public class YclSaleOutLineServiceImpl extends ServiceImpl<YclSaleOutLineDAO, YclSaleLineEntity> implements YclSaleOutLineService {

    @Override
    public boolean save(YclSaleLineEntity entity) {
        return this.insertOrUpdateAllColumn(entity);
    }

    @Override
    public boolean saveBatch(List<YclSaleLineEntity> entitys) {
        return this.insertOrUpdateBatch(entitys);
    }

    @Override
    public boolean delete(String ids) {
        return this.deleteBatchIds(StringUtils.stringToList(ids));
    }

}
