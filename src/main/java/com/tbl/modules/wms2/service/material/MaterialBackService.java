package com.tbl.modules.wms2.service.material;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.tbl.modules.wms2.entity.material.MaterialBack;
import com.tbl.modules.wms2.entity.material.MaterialBackPrint;
import com.tbl.modules.wms2.entity.material.MaterialBackViewModel;

import java.util.List;
import java.util.Map;

/**
 * 退料入库单详情 Service
 *
 * @author Sweven
 * @date 2020-06-19
 */
public interface MaterialBackService extends IService<MaterialBack> {

    /**
     * 获取列表数据
     *
     * @param page
     * @param materialBack
     * @return java.util.List<com.tbl.modules.wms2.entity.material.MaterialBackViewModel>
     */
    List<MaterialBackViewModel> getPageList(Page page, MaterialBack materialBack);

    /**
     * 提交Grid列表数据
     *
     * @param backList
     */
    void push(List<MaterialBack> backList);

    Integer count(MaterialBack materialBack);

    List<MaterialBack> getList(MaterialBack materialBack);

    List<MaterialBackPrint> findByLineId(String lineId);


}
