package com.tbl.modules.wms2.service.impl.insidewarehouse.check;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.modules.wms2.dao.insidewarehouse.check.CheckLineDao;
import com.tbl.modules.wms2.entity.insidewarehouse.check.YclCheckLineEntity;
import com.tbl.modules.wms2.service.insidewarehouse.check.CheckLineService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 盘库
 *
 * @author DASH
 */
@Service
public class CheckLineServiceImpl extends ServiceImpl<CheckLineDao, YclCheckLineEntity> implements CheckLineService {

    @Override
    public boolean saveBatch(List<YclCheckLineEntity> entitys) {
        return this.insertOrUpdateBatch(entitys);
    }

    @Override
    public List<YclCheckLineEntity> getPageListByMap(Map map) {
        return this.selectByMap(map);
    }

}
