package com.tbl.modules.wms2.service.impl.baseinfo;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.wms2.dao.baseinfo.FactoryMachineDao;
import com.tbl.modules.wms2.entity.baseinfo.FactoryMachineEntity;
import com.tbl.modules.wms2.service.baseinfo.FactoryMachineService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author DASH
 */
@Service("yddl2FactoryMachineService")
@Transactional(rollbackFor = Exception.class)
public class FactoryMachineServiceImpl extends ServiceImpl<FactoryMachineDao, FactoryMachineEntity> implements FactoryMachineService {


    @Resource
    FactoryMachineDao factoryMachineDao;

    @Override
    public PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map) {
        String sortOrder = pageTbl.getSortorder();
        String sortName = pageTbl.getSortname();
        if (StringUtils.isEmptyString(sortName)) {
            sortName = "departCode";
        }
        if (StringUtils.isEmptyString(sortOrder)) {
            sortOrder = "desc";
        }
        Page page = new Page(pageTbl.getPageno(), pageTbl.getPagesize(), sortName, "asc".equalsIgnoreCase(sortOrder));
        return new PageUtils(page.setRecords(factoryMachineDao.getPageList(page, map)));
    }

    @Override
    public List<String> getShiftCode(){
        return factoryMachineDao.getShiftCode();
    }

    /**
     * 根据条件查询生产厂-工序-机台的列表数据总数量
     * @param map
     * @return
     */
    @Override
    public int getCount(Map<String, Object> map){
        FactoryMachineEntity entity = new FactoryMachineEntity();
        if(map.containsKey("departCode") && !String.valueOf(map.get("departCode")).equals("0")){
            entity.setDepartCode(String.valueOf(map.get("departCode")));
        }
        if(map.containsKey("departDesc") && !String.valueOf(map.get("departDesc")).equals("0")){
            entity.setDepartDesc(String.valueOf(map.get("departDesc")));
        }
        if(map.containsKey("machineCode") && !String.valueOf(map.get("machineCode")).equals("0")){
            entity.setMachineCode(String.valueOf(map.get("machineCode")));
        }
        if(map.containsKey("machineDesc") && !String.valueOf(map.get("machineDesc")).equals("0")){
            entity.setMachineDesc(String.valueOf(map.get("machineDesc")));
        }
        if(map.containsKey("opCode") && !String.valueOf(map.get("opCode")).equals("0")){
            entity.setOpCode(String.valueOf(map.get("opCode")));
        }
        if(map.containsKey("opDesc") && !String.valueOf(map.get("opDesc")).equals("0")){
            entity.setOpCode(String.valueOf(map.get("opDesc")));
        }
        return factoryMachineDao.count(entity);
    }


    /**
     * 获取生产厂-工序-机台的下拉列表框（可联动）
     * @param map
     * @return
     */
    @Override
    public List<FactoryMachineEntity> getFactoryMachineList(Map<String, Object> map){
        FactoryMachineEntity entity = new FactoryMachineEntity();
        if(map.containsKey("departCode") && !String.valueOf(map.get("departCode")).equals("0")){
            entity.setDepartCode(String.valueOf(map.get("departCode")));
        }
        if(map.containsKey("departDesc") && !String.valueOf(map.get("departDesc")).equals("0")){
            entity.setDepartDesc(String.valueOf(map.get("departDesc")));
        }
        if(map.containsKey("opCode") && !String.valueOf(map.get("opCode")).equals("0")){
            entity.setOpCode(String.valueOf(map.get("opCode")));
        }
        if(map.containsKey("opDesc") && !String.valueOf(map.get("opDesc")).equals("0")){
            entity.setOpCode(String.valueOf(map.get("opDesc")));
        }

        return factoryMachineDao.getPageList(entity);
    }


    @Override
    public List<FactoryMachineEntity> getFactoryList(Map<String, Object> map){
        FactoryMachineEntity entity = new FactoryMachineEntity();
        if(map.containsKey("departCode") && !String.valueOf(map.get("departCode")).equals("0")){
            entity.setDepartCode(String.valueOf(map.get("departCode")));
        }
        if(map.containsKey("organizationId") && !String.valueOf(map.get("organizationId")).equals("0")){
            entity.setOrganizationId(String.valueOf(map.get("organizationId")));
        }

        return factoryMachineDao.getFactoryList(entity);
    }

    @Override
    public List<Map<String,Object>> getOpByDepartCode(Map<String, Object> map){
        return factoryMachineDao.getOpByDepartCode(map);
    }




}
