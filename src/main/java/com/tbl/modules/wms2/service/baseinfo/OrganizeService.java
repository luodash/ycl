package com.tbl.modules.wms2.service.baseinfo;

import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;

import java.util.Map;

/**
 * 实体组织
 *
 * @author DASH
 */
public interface OrganizeService {


    /**
     * 功能描述:列表页
     * 分页
     * @param pageTbl
     * @param map
     * @return PageUtils
     */
    PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map);



    /**
     * 功能描述：获取公司-组织-仓库的列表信息
     * @param map
     * @return
     */
    Map<String,Object> getCompanyAndOrganizationAndStores(Map<String,Object> map);


    Map<String,Object> getVendorList(Map<String,Object> map);


    /**
     * 供应商-模糊查询
     * @param vendorName
     * @return
     */
    Map<String,Object> selectVendor(String vendorName);


    /**
     * 功能描述：获取公司列表信息
     * @param map
     * @return
     */
    Map<String,Object> getCompanyList(Map<String,Object> map);

    /**
     * 功能描述：获取仓库列表信息
     * @param map
     * @return
     */
    Map<String,Object> getStoreList(Map<String,Object> map);


    /**
     * 功能描述：获取组织（生产库存组织等）信息
     * @param map
     * @return
     */
    Map<String,Object> getOrganizationList(Map<String,Object> map);

    /**
     * 功能描述：通过仓库编码获取组织信息
     * @param map
     * @return
     */
    Map<String,Object> selectOrganization(Map<String,Object> map);




}
