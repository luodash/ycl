package com.tbl.modules.wms2.service.purchasein;

import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.modules.wms2.entity.operation.YclInOutFlowEntity;
import org.apache.poi.ss.formula.functions.T;

import java.util.Map;

/**
 * 物资出厂单
 */

public interface FactoryOrderService {

    /**
     * 功能描述:保存
     *
     * @param entity
     * @return boolean
     */
    boolean save(YclInOutFlowEntity entity);

    /**
     * 功能描述:列表页 主表
     *
     * @param page
     * @param map
     * @return Page
     */
    Page<YclInOutFlowEntity> getPageList(Page page, Map<String, Object> map, YclInOutFlowEntity yclInOutFlowEntity);

    /**
     * 删除
     *
     * @param ids
     * @return boolean
     */
    boolean delete(String ids);

    /**
     * 获取对象
     * @param id
     * @return
     */
    YclInOutFlowEntity findById(String id);
}
