package com.tbl.modules.wms2.service.insidewarehouse.check;

import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.modules.wms2.entity.insidewarehouse.check.YclCheckEntity;
import com.tbl.modules.wms2.entity.insidewarehouse.check.YclCheckLineEntity;

import java.util.List;
import java.util.Map;

/**
 * 盘库子表
 *
 * @author DASH
 */
public interface CheckLineService {

    /**
     * 批量保存
     *
     * @param entitys
     * @return boolean
     */
    boolean saveBatch(List<YclCheckLineEntity> entitys);

    /**
     * 查询列表
     *
     * @param map
     * @return
     */
    List<YclCheckLineEntity> getPageListByMap(Map map);


    /**
     * 功能描述:删除
     *
     * @param ids
     * @return boolean
     */
//    boolean delete(String ids);

//    WarehouseEntity findById(Long id);


}
