package com.tbl.modules.wms2.service.operation;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.modules.wms2.entity.operation.EmergencyMaterialDTO;
import com.tbl.modules.wms2.entity.operation.QualityInfoEntity;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * 待质检信息
 *
 * @author DASH
 */
@Service
public interface QualityInfoService extends IService<QualityInfoEntity> {

    /**
     * 待质检信息List
     * @param map
     * @return
     */
    List<QualityInfoEntity> getQualityInfoList(Map<String, Object> map);

    QualityInfoEntity selectQualityInfoById(String id);



    Long getSequence();

    QualityInfoEntity insertQcInfo(String deliveryNo,String deliveryLineNo,String materialCode,String lotsNum,String asnNumber);


    /**
     * 发送待质检信息到EBS
     * @param type  待质检传Y,撤回待质检传N
     * @param qInfoId 质检信息表ycl_quality_info主键
     * @param entityId 公司ID
     * @param lotsNum  批次码
     * @param materialCode 物料编号
     * @param poNumber 采购单号
     * @param poLineNumber 采购行号
     * @param quantity 数量
     * @return 成功("result":true)  失败("result":false,"msg":"失败原因")
     */
    Map<String,Object> qcInfoToEBSProcessing(String type, String qInfoId, String entityId, String lotsNum, String materialCode,
                                             String poNumber, String poLineNumber, BigDecimal quantity);


    /**
     * 创建本地质检信息并发送给EBS待质检
     * @param
     * @return
     */
//    boolean toBeQualityControl(String businessCode);

//    /**
//     * 紧急物料信息
//     * @param map
//     * @return
//     */
//    EmergencyMaterialDTO getEmergencyMaterialInfo(Map<String,Object> map);


    boolean updateQcStateForDelivery(String deliveryNo,String deliveryLineNo);


}
