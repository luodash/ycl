package com.tbl.modules.wms2.service.interfacerecord;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.modules.wms2.interfacerecord.InterfaceRecord;

/**
 * 接口日志记录服务
 * @author 70486
 **/
public interface InterfaceRecordService extends IService<InterfaceRecord> {
	
	/**
	 * 插入日志
	 * @param code 接口编码
	 * @param name 接口名称
	 * @param requestString 请求报文
	 * @param responseString 返回报文
	 * @param poCode 采购订单号
	 * @param batchNo 批次号
	 */
	void insertLog(String code,String name,String requestString,String responseString,String poCode,String batchNo);

}
