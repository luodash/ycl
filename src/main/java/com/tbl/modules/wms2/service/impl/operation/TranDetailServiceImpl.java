package com.tbl.modules.wms2.service.impl.operation;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.wms2.dao.operation.TranDetailDAO;
import com.tbl.modules.wms2.entity.operation.TranDetail;
import com.tbl.modules.wms2.service.operation.TranDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service("yclTranDetailService")
public class TranDetailServiceImpl extends ServiceImpl<TranDetailDAO, TranDetail> implements TranDetailService{

    @Autowired
    private TranDetailDAO _dao;

    public PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map){
        String sortName = pageTbl.getSortname();
        String sortOrder = pageTbl.getSortorder();
        if (StringUtils.isEmptyString(sortName)) {
            sortName = "a.id";
        }
        if (StringUtils.isEmptyString(sortOrder)) {
            sortOrder = "desc";
        }
        Page page = new Page(pageTbl.getPageno(), pageTbl.getPagesize(), "a.state".equals(sortName)?"a.state,a.id":sortName, "asc".equalsIgnoreCase(sortOrder));
        return new PageUtils(page.setRecords(_dao.getPageList(page, map)));
    }

    public TranDetail selectTransferById(Long id){
        return _dao.selectTranDetailById(id);
    }
    public Long getSequence(){
        return _dao.getSequence();
    }
}