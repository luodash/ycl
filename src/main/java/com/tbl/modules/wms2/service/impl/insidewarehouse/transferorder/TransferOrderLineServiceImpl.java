package com.tbl.modules.wms2.service.impl.insidewarehouse.transferorder;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.modules.wms2.dao.insidewarehouse.transferorder.TransferOrderLineDAO;
import com.tbl.modules.wms2.entity.insidewarehouse.transferorder.TransferOrderLineEntity;
import com.tbl.modules.wms2.service.insidewarehouse.transferorder.TransferOrderLineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


@Service("yclTransferOrderLineService")
public class TransferOrderLineServiceImpl extends ServiceImpl<TransferOrderLineDAO, TransferOrderLineEntity> implements TransferOrderLineService {

    @Resource
    private TransferOrderLineDAO transferOrderLineDAO;


    public Long getSequence(){
        return transferOrderLineDAO.getSequence();
    }


    @Override
    public boolean saveBatch(List<TransferOrderLineEntity> entitys) {
        return this.insertOrUpdateBatch(entitys);
    }

    @Override
    public List<TransferOrderLineEntity> getPageListByMap(Map map) {
        return this.selectByMap(map);
    }

    @Override
    public boolean delete(String ids) {
        return this.deleteBatchIds(Arrays.asList(ids.split(",")));
    }

    @Override
    public TransferOrderLineEntity findById(String id) {
        return this.selectById(id);
    }

    public List<TransferOrderLineEntity> getTransferOrderLines(Map<String,Object> map) {
        return transferOrderLineDAO.selectByMap(map);
    }


}