package com.tbl.modules.wms2.service.impl.interfacerecord;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.modules.wms2.dao.interfacerecord.InterfaceRecordDAO;
import com.tbl.modules.wms2.interfacerecord.InterfaceRecord;
import com.tbl.modules.wms2.service.interfacerecord.InterfaceRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 接口日志记录实现类
 * @author 70486
 **/
@Service
public class InterfaceRecordServiceImp extends ServiceImpl<InterfaceRecordDAO, InterfaceRecord> implements InterfaceRecordService {

	/**
	 * 接口日志记录
	 */
	@Autowired
	private InterfaceRecordDAO interfaceRecordDAO;

	/**
	 * 插入日志
	 * @param code 接口编码
	 * @param name 接口名称
	 * @param requestString 请求报文
	 * @param responseString 返回报文
	 * @param itemCode 物料编码
	 * @param batchNo 批次号
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void insertLog(String code,String name,String requestString,String responseString,String itemCode,String batchNo) {
		interfaceRecordDAO.insertLog(code,name,requestString,responseString,itemCode,batchNo);
	}
	
}
