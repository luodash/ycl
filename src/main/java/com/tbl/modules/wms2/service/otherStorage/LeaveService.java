package com.tbl.modules.wms2.service.otherStorage;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.wms2.entity.material.MaterialBack;
import com.tbl.modules.wms2.entity.otherStorage.LeaveEntity;

import java.util.List;
import java.util.Map;

/**
 * 其他出入库
 */
public interface LeaveService extends IService<LeaveEntity> {

    /**
     * 功能描述:列表页
     * @param map
     * @return PageUtils
     */
    List<LeaveEntity> getPageList(Map<String, Object> map);

    /**
     * 功能描述:删除
     * @param ids
     * @return boolean
     */
    boolean delete(String ids);

    /**
     * 功能描述:根据ID查询
     * @param id
     * @return
     */
    LeaveEntity findById(String id);

}
