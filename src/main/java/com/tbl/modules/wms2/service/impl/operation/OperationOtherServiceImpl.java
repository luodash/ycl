package com.tbl.modules.wms2.service.impl.operation;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.modules.wms2.dao.operation.OperationOtherMapper;
import com.tbl.modules.wms2.entity.operation.*;
import com.tbl.modules.wms2.service.operation.OperationOtherService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Sweven
 * @date 2020-06-25
 */
@Service
public class OperationOtherServiceImpl extends ServiceImpl<OperationOtherMapper, OperationOther> implements OperationOtherService {

    @Resource
    private OperationOtherMapper otherMapper;
    @Resource
    private OperationOtherMapper operationOtherMapper;

    @Override
    public List<OperationOtherVO> getPageList(Page page, Map<String, Object> paramsMap) {
        return otherMapper.getPageList(page, paramsMap);
    }

    @Override
    public List<OperationOtherVO> getPageListApi(Page page, Map<String, Object> paramsMap) {
        return otherMapper.getPageListApi(page, paramsMap);
    }

    @Override
    public OperationOtherPrint findByOtherCode(String otherCode) {
        return operationOtherMapper.findByOtherCode(otherCode);
    }

    /**
     * 调用存储过程，创建/更新其他出入库头信息
     *
     * @param headDto
     * @return
     */
    @Override
    public OperationOtherHeadDTO createHead(OperationOtherHeadDTO headDto) {
        headDto.setTransactionDate(new Date());
        operationOtherMapper.createHead(headDto);
        return headDto;
    }

    @Override
    public OperationOtherLineDTO createLine(OperationOtherLineDTO lineDTO) {
        operationOtherMapper.createLine(lineDTO);
        return lineDTO;
    }

    @Override
    public Map<String, Object> deleteLine(Map<String, Object> map) {
        return null;
    }

    @Override
    public OperationOtherValidDTO validateData(OperationOtherValidDTO validDTO) {
        operationOtherMapper.validateData(validDTO);
        return validDTO;
    }

    @Override
    public OperationOtherProcessDTO process(OperationOtherProcessDTO processDTO) {
        operationOtherMapper.process(processDTO);
        return processDTO;
    }

}

