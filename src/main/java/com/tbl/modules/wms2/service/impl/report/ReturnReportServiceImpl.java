package com.tbl.modules.wms2.service.impl.report;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.modules.wms2.dao.report.ReturnReportDao;
import com.tbl.modules.wms2.entity.production.YclMaterialBackEntity;
import com.tbl.modules.wms2.service.report.ReturnReportService;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Map;

/**
 * 不合格退料报表
 */
@Service
public class ReturnReportServiceImpl extends ServiceImpl<ReturnReportDao, YclMaterialBackEntity> implements ReturnReportService {
    @Override
    public boolean save(YclMaterialBackEntity entity) {
        return this.insertOrUpdate(entity);
    }

    @Override
    public Page<YclMaterialBackEntity> getPageList(Page page, Map<String, Object> map, YclMaterialBackEntity yclMaterialBackEntity) {
        EntityWrapper<YclMaterialBackEntity> ew = new EntityWrapper<>();
        ew/*.eq("CHECK_STATE",2)*/
                .eq(StrUtil.isNotEmpty(yclMaterialBackEntity.getAreaCode()),"",yclMaterialBackEntity.getAreaCode())
                .eq(StrUtil.isNotEmpty(yclMaterialBackEntity.getApplyCode()),"",yclMaterialBackEntity.getApplyCode());
        Page<YclMaterialBackEntity> mapPage = this.selectPage(page,ew);
        return mapPage;
    }

    @Override
    public boolean delete(String ids) {
        return this.deleteBatchIds(Arrays.asList(ids.split(",")));
    }

    @Override
    public YclMaterialBackEntity findById(String id) {
        return this.selectById(id);
    }
}
