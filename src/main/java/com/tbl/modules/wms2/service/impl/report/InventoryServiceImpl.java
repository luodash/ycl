package com.tbl.modules.wms2.service.impl.report;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.modules.wms.dao.baseinfo.WarehouseDAO;
import com.tbl.modules.wms.entity.baseinfo.Warehouse;
import com.tbl.modules.wms2.dao.purchasein.PurchaseOrderDAO;
import com.tbl.modules.wms2.dao.report.InventoryDao;
import com.tbl.modules.wms2.entity.ordermanage.asn.AsnVO;
import com.tbl.modules.wms2.entity.purchasein.PurchaseOrderSaveEntity;
import com.tbl.modules.wms2.entity.report.YclInventoryEntity;
import com.tbl.modules.wms2.entity.report.YclInventoryQueryDTO;
import com.tbl.modules.wms2.entity.report.YclInventoryUpdateDTO;
import com.tbl.modules.wms2.service.report.InventoryService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

/**
 * 库存
 *
 * @author DASH
 */
@Service("yclInventoryService")
public class InventoryServiceImpl extends ServiceImpl<InventoryDao, YclInventoryEntity> implements InventoryService {

    @Resource
    InventoryDao inventoryDao;

    @Resource
    WarehouseDAO warehouseDAO;

    @Resource
    PurchaseOrderDAO purchaseOrderDAO;


    @Override
    public boolean save(YclInventoryEntity entity) {
        return this.insertOrUpdate(entity);
    }

    @Override
    public Page<YclInventoryEntity> getPageList(Page page, Map<String, Object> map) {
        Page<YclInventoryEntity> mapPage = this.selectPage(page, null);
        return mapPage;
    }

    @Override
    public Page<YclInventoryEntity> getPageList(Page page, YclInventoryEntity yclInventoryEntity) {
        Wrapper wrapper = new EntityWrapper();
        if (StrUtil.isNotEmpty(yclInventoryEntity.getAreaCode())) {
            wrapper.eq("AREA_CODE", yclInventoryEntity.getAreaCode());
        }
        if (StrUtil.isNotEmpty(yclInventoryEntity.getStoreCode())) {
            wrapper.eq("STORE_CODE", yclInventoryEntity.getStoreCode());
        }
        if (StrUtil.isNotEmpty(yclInventoryEntity.getLocatorCode())) {
            wrapper.eq("LOCATOR_CODE", yclInventoryEntity.getLocatorCode());
        }
        if (StrUtil.isNotEmpty(yclInventoryEntity.getMaterialCode())) {
            wrapper.eq("MATERIAL_CODE", yclInventoryEntity.getMaterialCode());
        }
        if (StrUtil.isNotEmpty(yclInventoryEntity.getLotsNum())) {
            wrapper.eq("LOTS_NUM", yclInventoryEntity.getLotsNum());
        }
        wrapper.orderBy("UPDATE_TIME", false);
        Page<YclInventoryEntity> mapPage = this.selectPage(page, wrapper);
        return mapPage;
    }

    @Override
    public boolean delete(String ids) {
        return this.deleteBatchIds(Arrays.asList(ids.split(",")));
    }

    @Override
    public YclInventoryEntity findById(String id) {
        return this.selectById(id);
    }

    @Override
    public YclInventoryEntity find(String areaCode, String storeCode, String locatorCode, String materialCode, String lotsNum) {
        Wrapper ew = new EntityWrapper<YclInventoryEntity>();
        ew.eq("AREA_CODE", areaCode)
                .eq("STORE_CODE", storeCode)
                .eq("LOCATOR_CODE", locatorCode)
                .eq("MATERIAL_CODE", materialCode)
                .eq("LOTS_NUM", lotsNum);
        return this.selectOne(ew);
    }

    @Override
    public YclInventoryEntity find(String areaCode, String storeCode, String materialCode) {
        Wrapper ew = new EntityWrapper<YclInventoryEntity>();
        ew.eq("AREA_CODE", areaCode)
                .eq("STORE_CODE", storeCode)
                .eq("MATERIAL_CODE", materialCode).orderBy(" update_time");
        return this.selectOne(ew);
    }

    @Override
    public List<YclInventoryEntity> listView(YclInventoryEntity yclInventoryEntity) {
        return this.selectList(new EntityWrapper<>(yclInventoryEntity));
    }

    @Override
    public List<YclInventoryEntity> listViewWithoutNegative(YclInventoryEntity yclInventoryEntity) {
        Wrapper wrapper = new EntityWrapper();
        wrapper.eq(StrUtil.isNotBlank(yclInventoryEntity.getAreaCode()), "AREA_CODE", yclInventoryEntity.getAreaCode())
                .eq(StrUtil.isNotBlank(yclInventoryEntity.getStoreCode()), "STORE_CODE", yclInventoryEntity.getStoreCode())
                .eq(StrUtil.isNotBlank(yclInventoryEntity.getLocatorCode()), "LOCATOR_CODE", yclInventoryEntity.getLocatorCode())
                .eq(StrUtil.isNotBlank(yclInventoryEntity.getLotsNum()), "LOTS_NUM", yclInventoryEntity.getLotsNum())
                .eq(StrUtil.isNotBlank(yclInventoryEntity.getMaterialCode()), "MATERIAL_CODE", yclInventoryEntity.getMaterialCode())
                .eq(StrUtil.isNotBlank(yclInventoryEntity.getId()), "ID", yclInventoryEntity.getId())
                .gt("QUALITY", 0);
        return this.selectList(wrapper);
    }

    @Override
    public boolean updateInventory(YclInventoryUpdateDTO inventoryUpdateDTO) {

        String areaCode = inventoryUpdateDTO.getAreaCode();
        String areaName = inventoryUpdateDTO.getAreaName();
        String storeCode = inventoryUpdateDTO.getStoreCode();
        String storeName = inventoryUpdateDTO.getStoreName();
        String locatorCode = inventoryUpdateDTO.getLocatorCode();
        String materialCode = inventoryUpdateDTO.getMaterialCode();
        String lotsNum = inventoryUpdateDTO.getLotsNum();
        String primaryUnit = inventoryUpdateDTO.getPrimaryUnit();
        String materialName = inventoryUpdateDTO.getMaterialName();
        BigDecimal amount = inventoryUpdateDTO.getAmount();
        String inout = inventoryUpdateDTO.getInout();

        if (StrUtil.isBlank(areaCode) || StrUtil.isBlank(storeCode) || StrUtil.isBlank(locatorCode) || StrUtil.isBlank(materialCode) || StrUtil.isBlank(lotsNum) || StrUtil.isBlank(inout) || amount == null) {
            return false;
        }
        YclInventoryEntity entity = find(areaCode, storeCode, locatorCode, materialCode, lotsNum);
        if (entity == null) {
            entity = new YclInventoryEntity();
            entity.setQuality(BigDecimal.ZERO);
            entity.setAreaCode(areaCode);
            entity.setAreaName(areaName);
            entity.setStoreCode(storeCode);
            entity.setStoreName(storeName);
            entity.setLocatorCode(locatorCode);
            entity.setMaterialCode(materialCode);
            entity.setMaterialName(materialName);
            entity.setLotsNum(lotsNum);
            entity.setPrimaryUnit(primaryUnit);
        }
        entity.setUpdateTime(DateUtil.date());
        BigDecimal quality = entity.getQuality();

        // 入库存
        if ("1".equals(inout)) {
            quality = quality.add(amount);


        } else {
            quality = quality.subtract(amount);


        }


        if (quality.equals(BigDecimal.ZERO)) {
            return this.delete(entity.getId());
        }
        entity.setQuality(quality);
        return this.insertOrUpdate(entity);
    }


    @Override
    public boolean updateInventoryById(YclInventoryEntity yclInventoryEntity) {
        return this.updateById(yclInventoryEntity);
    }

}
