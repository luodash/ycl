package com.tbl.modules.wms2.service.remotedata;

import java.util.List;
import java.util.Map;

public interface RemoteDateService {
    List<Map<String,Object>> getAsnHeaders(Long id);

    Integer getAsnCountFromRemote(Long id);

}
