package com.tbl.modules.wms2.service.ordermanage.asn;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.wms2.entity.ordermanage.asn.AsnCorrectDTO;
import com.tbl.modules.wms2.entity.ordermanage.asn.AsnLineEntity;
import com.tbl.modules.wms2.entity.ordermanage.asn.AsnLineUpdateDTO;

import java.util.List;
import java.util.Map;

/**
 * @author DASH
 */
public interface AsnLineService extends IService<AsnLineEntity> {
    /**
     * 列表页数据
     *
     * @param pageTbl
     * @param map
     * @return PageUtils
     */
    PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map);

    void insertAsnLine(AsnLineEntity asnLineEntity);

    /**
     * 编辑
     * @param asnLineEntity
     * @return
     */
    Boolean editAsnLine(AsnLineEntity asnLineEntity);

    /**
     * 批量保存子表和出入库流水
     * @param asnLineUpdateDTOS
     * @return
     */
    Boolean editAsnLineBatch(List<AsnLineUpdateDTO> asnLineUpdateDTOS);

    /**
     * 更新 非一定ID
     * @param asnLineEntity
     * @return
     */
    Boolean updateAsnLine(AsnLineEntity asnLineEntity);

    /**
     * @param asnHeaderId
     * @return
     */
    List<AsnLineEntity> listViewLine(String asnHeaderId);

    /**
     * @param list
     * @return
     */
    List<AsnLineEntity> listViewLineListMap(List<Map> list);

    List<AsnCorrectDTO> listViewAsnByPoCode(String poCodes);
}
