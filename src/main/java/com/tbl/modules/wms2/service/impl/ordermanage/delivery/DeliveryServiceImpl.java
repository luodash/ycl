package com.tbl.modules.wms2.service.impl.ordermanage.delivery;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.modules.wms2.common.RuleCode;
import com.tbl.modules.wms2.controller.operation.QualityControlController;
import com.tbl.modules.wms2.dao.ordermanage.delivery.DeliveryDao;
import com.tbl.modules.wms2.dao.report.InventoryDao;
import com.tbl.modules.wms2.entity.ordermanage.delivery.DeliveryAddSCDTO;
import com.tbl.modules.wms2.entity.ordermanage.delivery.DeliveryEntity;
import com.tbl.modules.wms2.entity.report.YclInventoryEntity;
import com.tbl.modules.wms2.entity.report.YclInventoryUpdateDTO;
import com.tbl.modules.wms2.service.common.CountService;
import com.tbl.modules.wms2.service.operation.QualityInfoService;
import com.tbl.modules.wms2.service.ordermanage.delivery.DeliveryService;
import com.tbl.modules.wms2.service.report.InventoryService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * 库存
 *
 * @author DASH
 */
@Service
public class DeliveryServiceImpl extends ServiceImpl<DeliveryDao, DeliveryEntity> implements DeliveryService {

    @Resource
    DeliveryDao deliveryDao;

    @Resource
    QualityInfoService qualityInfoService;

    @Resource
    QualityControlController qualityControlController;

    @Resource
    private CountService countService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean add(DeliveryEntity entity, Boolean isQC) {
        String id = RuleCode.getUUID();
        entity.setID(id);
        // 发送质检
        if (isQC) {
            Map<String, Object> qcInfo = qualityControlController.createQcInfo(entity);
            Boolean result = (Boolean) qcInfo.get("result");
            if (!result) {
                return false;
            }
        }
        return this.insert(entity);
    }

    @Override
    public boolean edit(DeliveryEntity entity) {
        return this.insertOrUpdate(entity);
    }

    //    @Override
    public Page<YclInventoryEntity> getPageList(Page page, YclInventoryEntity yclInventoryEntity) {
        Wrapper wrapper = new EntityWrapper();
        if (StrUtil.isNotEmpty(yclInventoryEntity.getAreaCode())) {
            wrapper.eq("AREA_CODE", yclInventoryEntity.getAreaCode());
        }
        if (StrUtil.isNotEmpty(yclInventoryEntity.getStoreCode())) {
            wrapper.eq("STORE_CODE", yclInventoryEntity.getStoreCode());
        }
        if (StrUtil.isNotEmpty(yclInventoryEntity.getLocatorCode())) {
            wrapper.eq("LOCATOR_CODE", yclInventoryEntity.getLocatorCode());
        }
        if (StrUtil.isNotEmpty(yclInventoryEntity.getMaterialCode())) {
            wrapper.eq("MATERIAL_CODE", yclInventoryEntity.getMaterialCode());
        }
        if (StrUtil.isNotEmpty(yclInventoryEntity.getLotsNum())) {
            wrapper.eq("LOTS_NUM", yclInventoryEntity.getLotsNum());
        }
        Page<YclInventoryEntity> mapPage = this.selectPage(page, wrapper);
        return mapPage;
    }

    @Override
    public boolean delete(String ids) {
        return this.deleteBatchIds(Arrays.asList(ids.split(",")));
    }

    @Override
    public DeliveryEntity findById(String id) {
        return this.selectById(id);
    }

    @Override
    public List<DeliveryEntity> listView(DeliveryEntity entity) {
        return this.selectList(new EntityWrapper<>(entity));
    }

    @Override
    public DeliveryEntity getOne(DeliveryEntity deliveryEntity) {
        Wrapper wrapper = new EntityWrapper();
        wrapper.eq(StrUtil.isNotBlank(deliveryEntity.getPoCode()), "PO_CODE", deliveryEntity.getPoCode())
                .eq(StrUtil.isNotBlank(deliveryEntity.getPoLineNum()), "PO_LINE_NUM", deliveryEntity.getPoLineNum())
                .eq(StrUtil.isNotBlank(deliveryEntity.getAsnNumber()), "ASN_NUMBER", deliveryEntity.getAsnNumber());
        return this.selectOne(wrapper);
    }

    @Override
    public Boolean addSC(DeliveryAddSCDTO deliveryAddSCDTO) {
        List<DeliveryEntity> data = deliveryAddSCDTO.getData();
        String jsb = countService.getNextCount("JSB");
        int i = 0;
        if (data.size() > 0) {
            for (DeliveryEntity item : data) {
                item.setAsnNumber(deliveryAddSCDTO.getAsnNumber());
                item.setVendorCode(deliveryAddSCDTO.getVendorCode());
                item.setVendorName(deliveryAddSCDTO.getVendorName());
                item.setCreateTime(deliveryAddSCDTO.getCreateTime());
                item.setDeliveryNo(jsb);
                item.setDeliveryLineNo(String.valueOf(i + 1));
                i++;
                if (!this.insertOrUpdate(item)) {
                    return false;
                }
            }
        }
        return true;
    }

}
