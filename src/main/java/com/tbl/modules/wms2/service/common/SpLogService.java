package com.tbl.modules.wms2.service.common;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.modules.wms2.common.support.SysLogEntity;
import com.tbl.modules.wms2.common.support.YclCountEntity;

import java.util.List;

/**
 * 库存
 *
 * @author DASH
 */
public interface SpLogService extends IService<SysLogEntity> {

    Boolean createLog(SysLogEntity sysLogEntity);


}
