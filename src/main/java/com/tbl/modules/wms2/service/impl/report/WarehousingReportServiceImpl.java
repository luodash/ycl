package com.tbl.modules.wms2.service.impl.report;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.modules.wms2.dao.report.WarehousingReportDao;
import com.tbl.modules.wms2.entity.operation.YclInOutFlowEntity;
import com.tbl.modules.wms2.service.report.WarehousingReportService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Map;

/**
 * 入库报表
 */
@Service
public class WarehousingReportServiceImpl extends ServiceImpl<WarehousingReportDao, YclInOutFlowEntity> implements WarehousingReportService {

    @Resource
    private WarehousingReportDao warehousingReportDao;

    @Override
    public boolean save(YclInOutFlowEntity entity) {
        return this.insertOrUpdate(entity);
    }

    @Override
    public Page<YclInOutFlowEntity> getPageList(Page page, Map<String, Object> map, YclInOutFlowEntity yclInOutFlowEntity) {
        EntityWrapper<YclInOutFlowEntity> ew = new EntityWrapper<YclInOutFlowEntity>();
        /*ew.eq("BUSINESS_TYPE",2)*/
                ew.eq(StrUtil.isNotEmpty(yclInOutFlowEntity.getAreaCode()),"",yclInOutFlowEntity.getAreaCode())
                .eq(StrUtil.isNotEmpty(yclInOutFlowEntity.getSupplierName()),"",yclInOutFlowEntity.getSupplierName());
        Page<YclInOutFlowEntity> mapPage = this.selectPage(page,ew);
        return mapPage;
    }

    /*@Override
    public Page<YclInOutFlowEntity> getPageList(Page page, YclInOutFlowEntity yclInOutFlowEntity) {
        Wrapper wrapper = new EntityWrapper();
        if (StrUtil.isNotEmpty(yclInOutFlowEntity.getAreaCode())) {
            wrapper.eq("AREA_CODE", yclInOutFlowEntity.getAreaCode());
        }
        if (StrUtil.isNotEmpty(yclInOutFlowEntity.getHouseCode())) {
            wrapper.eq("HOUSE_CODE", yclInOutFlowEntity.getHouseCode());
        }
        if (StrUtil.isNotEmpty(yclInOutFlowEntity.getLocationCode())) {
            wrapper.eq("LOCATION_CODE", yclInOutFlowEntity.getLocationCode());
        }
        if (StrUtil.isNotEmpty(yclInOutFlowEntity.getMaterialCode())) {
            wrapper.eq("MATERIAL_CODE", yclInOutFlowEntity.getMaterialCode());
        }
        if (StrUtil.isNotEmpty(yclInOutFlowEntity.getLineNumber())) {
            wrapper.eq("LINE_NUMBER", yclInOutFlowEntity.getLineNumber());
        }
        Page<YclInOutFlowEntity> mapPage = this.selectPage(page, wrapper);
        return mapPage;
    }
*/
    @Override
    public boolean delete(String ids) {
        return this.deleteBatchIds(Arrays.asList(ids.split(",")));
    }

    @Override
    public YclInOutFlowEntity findById(String id) {
        return this.selectById(id);
    }
}
