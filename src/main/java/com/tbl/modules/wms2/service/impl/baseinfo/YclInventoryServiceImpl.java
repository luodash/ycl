package com.tbl.modules.wms2.service.impl.baseinfo;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.wms2.dao.baseinfo.VendorDao;
import com.tbl.modules.wms2.dao.baseinfo.YclInventoryDAO;
import com.tbl.modules.wms2.dao.ordermanage.asn.AsnLineDAO;
import com.tbl.modules.wms2.entity.baseinfo.YclInventory;
import com.tbl.modules.wms2.entity.ordermanage.asn.AsnLineEntity;
import com.tbl.modules.wms2.service.baseinfo.YclInventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("yclYclInventoryService")
public class YclInventoryServiceImpl extends ServiceImpl<YclInventoryDAO, YclInventory> implements YclInventoryService{

    @Autowired
    private YclInventoryDAO _dao;

    @Autowired
    private AsnLineDAO asnLineDAO;

    @Autowired
    private VendorDao verdorDao;

    public PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map){
        String sortName = pageTbl.getSortname();
        String sortOrder = pageTbl.getSortorder();
        if (StringUtils.isEmptyString(sortName)) {
            sortName = "a.location_code";
        }
        if (StringUtils.isEmptyString(sortOrder)) {
            sortOrder = "asc";
        }
        Page page = new Page(pageTbl.getPageno(), pageTbl.getPagesize(), "a.state".equals(sortName)?"a.state,a.id":sortName, "asc".equalsIgnoreCase(sortOrder));
        return new PageUtils(page.setRecords(_dao.getPageList(page, map)));
    }

    public List<Map<String, Object>> getSelectCode(Map<String, Object> map){
        return _dao.getSelectCode(map);
    }

    public YclInventory selectTransferById(Long id){
        return _dao.selectById(id);
    }
    public Long getSequence(){
        return _dao.getSequence();
    }

    /**
     * 手持机打印字符串
     *
     * @param map
     * @return PageUtils
     */
    public List printInventoryList(Map<String, Object> map){
        //仓库厂区库位批次
        String lotsNumList =  map.get("lotsNumList").toString();
        List printList = new ArrayList();
        //根据远东批次查出送货单行数据
        List<AsnLineEntity> asnlist = asnLineDAO.selectList(new EntityWrapper<AsnLineEntity>()
                .in("LOTS_NUM_YD",lotsNumList));
        //循环所有批次 一条对应一个打印码
        for(AsnLineEntity asnLineEntity : asnlist){
            Map<String, Object> mapStr = new HashMap<>();
            mapStr.put("poCode",asnLineEntity.getPoCode());//采购订单编号
            mapStr.put("macterialCode",asnLineEntity.getMaterialCode());//物料编码
            mapStr.put("macterialName",asnLineEntity.getMaterialName());//物料名称
            mapStr.put("lotsNum",asnLineEntity.getLotsNum());//供方批次
            //远东批次为发过来的批次删去L、物料编码、采购订单
            //String replaceStr = "L|"+asnLineEntity.getMaterialCode()+"|"+asnLineEntity.getPoCode()+"";
            int replaceBegin = 1+asnLineEntity.getMaterialCode().length()+asnLineEntity.getPoCode().length();
            //远东批次
            mapStr.put("ydLotsNum",asnLineEntity.getLotsNumYd().substring(replaceBegin,asnLineEntity.getLotsNumYd().length()));
            //供应商编码为发过来的批次删去L、物料编码、采购订单、供应商批次
            //String replaceSupplyStr = "L|"+asnLineEntity.getMaterialCode()+"|"+asnLineEntity.getPoCode()+"|"+asnLineEntity.getLotsNum()+"";
            int endBegin = asnLineEntity.getLotsNumYd().length()-asnLineEntity.getLotsNum().length();
            Long vendorId = Long.parseLong(asnLineEntity.getLotsNumYd().substring(replaceBegin,endBegin));
            mapStr.put("supply",verdorDao.findById(vendorId).getName());//供应商名称
            mapStr.put("finalNum",asnLineEntity.getLotsNumYd());//总批次
            mapStr.put("manufactureDate","2020/07/30");//生产日期
            mapStr.put("expiryDate","");//有效期
            mapStr.put("roughWeight","");//毛重
            mapStr.put("tareWeight","");//皮重
            mapStr.put("netWeight","");//净重
            mapStr.put("length","");//长度
            String printStr =  printInventoryStr(mapStr);
            printList.add(printStr);
        }
        return printList;
    }

    /**
     * 手持机字符串拼接
     *
     * @param map
     * @return String
     */
    public String printInventoryStr(Map<String, Object> map) {
        //获取物料名称 分成两段显示
        String macterialName = map.get("macterialName").toString();
        String macterialName_part1="", macterialName_part2="";
        if (macterialName.length() <= 17){
            macterialName_part1 = macterialName;macterialName_part2 = "";
        }else{
            macterialName_part1 = macterialName.substring(0, 17); macterialName_part2 = macterialName.substring(17, macterialName.length());

        }
        //获取供应商名称 分成两段显示
        String supply = map.get("supply").toString();
        String supply_part1="", supply_part2="";
        if (supply.length() <= 17) {
            supply_part1 = supply; supply_part2 = "";
        }else{
            supply_part1 = supply.substring(0, 17); supply_part2 = supply.substring(17, supply.length());
        }

            String printStr = "CT~~CD,~CC^~CT~\n" +
                    "^XA\n" +
                    "^CW0,E:MSUNG24.FNT^FS ^CI28\n" +
                    "~TA016\n" +
                    "~JSN\n" +
                    "^LT0\n" +
                    "^MNN\n" +
                    "^MTT\n" +
                    "^PON\n" +
                    "^PMN\n" +
                    "^LH0,0\n" +
                    "^JMA\n" +
                    "^PR2,2\n" +
                    "~SD15\n" +
                    "^JUS\n" +
                    "^LRN\n" +
                    "^CI27\n" +
                    "^PA0,1,1,0\n" +
                    "^XZ\n" +
                    "^XA\n" +
                    "^MMT\n" +
                    "^PW559\n" +
                    "^LL480\n" +
                    "^LS0\n" +
                    "^FO36,0^GB0,480,2^FS\n" +
                    "^FT31,464^A0B,23,23^FH\\^CI28^FD采购单号^FS^CI27\n" +
                    "^FO68,0^GB0,480,2^FS\n" +
                    "^FO135,0^GB0,480,2^FS\n" +
                    "^FT63,464^A0B,23,23^FH\\^CI28^FD物料编号^FS^CI27\n" +
                    "^FT111,464^A0B,23,23^FH\\^CI28^FD物料名称^FS^CI27\n" +
                    "^FO175,0^GB0,480,2^FS\n" +
                    "^FT167,464^A0B,23,23^FH\\^CI28^FD供方批次^FS^CI27\n" +
                    "^FO215,0^GB0,480,2^FS\n" +
                    "^FT207,464^A0B,23,23^FH\\^CI28^FD远东批次^FS^CI27\n" +
                    "^FO253,0^GB0,480,2^FS\n" +
                    "^FT244,467^A0B,23,23^FH\\^CI28^FD生产日期^FS^CI27\n" +
                    "^FT244,232^A0B,23,23^FH\\^CI28^FD有效期^FS^CI27\n" +
                    "^FT93,352^A0B,20,20^FH\\^CI28^FD"+macterialName_part1+"^FS^CI27\n" +
                    "^FT118,352^A0B,20,20^FH\\^CI28^FD"+macterialName_part2+"^FS^CI27\n" +
                    "^FT244,352^A0B,23,23^FH\\^CI28^FD"+map.get("manufactureDate")+"^FS^CI27\n" +
                    "^FT244,144^A0B,23,23^FH\\^CI28^FD"+map.get("expiryDate")+"^FS^CI27\n" +
                    "^FO293,0^GB0,480,2^FS\n" +
                    "^FT285,448^A0B,23,23^FH\\^CI28^FD毛重^FS^CI27\n" +
                    "^FO334,0^GB0,480,2^FS\n" +
                    "^FT325,448^A0B,23,23^FH\\^CI28^FD净重^FS^CI27\n" +
                    "^FO215,239^GB120,0,2^FS\n" +
                    "^FO215,151^GB120,0,2^FS\n" +
                    "^FT283,223^A0B,23,23^FH\\^CI28^FD皮重^FS^CI27\n" +
                    "^FT322,224^A0B,23,23^FH\\^CI28^FD长度^FS^CI27\n" +
                    "^FO404,0^GB0,480,2^FS\n" +
                    "^FT379,456^A0B,23,23^FH\\^CI28^FD供应商^FS^CI27\n" +
                    "^FT408,482^BQN,2,6\n" +
                    "^FH\\^FDLA,"+map.get("finalNum")+"\\0D\\0A^FS\n" +
                    "^FT547,328^A0B,20,20^FB320,1,5,C^FH\\^CI28^FD"+map.get("finalNum")+"^FS^CI27\n" +
                    "^FO0,359^GB404,0,2^FS\n" +
                    "^FT31,352^A0B,23,23^FH\\^CI28^FD"+map.get("poCode")+"^FS^CI27\n" +
                    "^FT63,352^A0B,23,23^FH\\^CI28^FD"+map.get("macterialCode")+"^FS^CI27\n" +
                    "^FT167,352^A0B,23,23^FH\\^CI28^FD"+map.get("lotsNum")+"^FS^CI27\n" +
                    "^FT207,352^A0B,23,23^FH\\^CI28^FD"+map.get("ydLotsNum")+"^FS^CI27\n" +
                    "^FT284,352^A0B,23,23^FH\\^CI28^FD"+map.get("roughWeight")+"^FS^CI27\n" +
                    "^FT284,144^A0B,23,23^FH\\^CI28^FD"+map.get("tareWeight")+"^FS^CI27\n" +
                    "^FT324,352^A0B,23,23^FH\\^CI28^FD"+map.get("netWeight")+"^FS^CI27\n" +
                    "^FT324,144^A0B,23,23^FH\\^CI28^FD"+map.get("length")+"^FS^CI27\n" +
                    "^FT364,352^A0B,20,20^FH\\^CI28^FD"+supply_part1+"^FS^CI27\n" +
                    "^FT389,352^A0B,20,20^FH\\^CI28^FD"+supply_part2+"^FS^CI27\n" +
                    "^PQ1,0,1,Y\n" +
                    "^XZ";
            return printStr;
        }

}