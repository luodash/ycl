package com.tbl.modules.wms2.service.sale;

import com.tbl.modules.wms2.entity.production.YclMaterialOutEntity;
import com.tbl.modules.wms2.entity.sale.YclSaleEntity;
import com.tbl.modules.wms2.entity.sale.YclSaleLineEntity;

import java.util.List;

/**
 * 盘库
 *
 * @author DASH
 */
public interface YclSaleOutService {

    /**
     * 功能描述:保存
     *
     * @param entity
     * @return boolean
     */
    boolean save(YclSaleEntity entity);

    /**
     * 批量保存
     *
     * @param entitys
     * @return boolean
     */
    boolean saveBatch(List<YclSaleEntity> entitys);

    /**
     * 删除
     *
     * @param ids
     * @return boolean
     */
    boolean delete(String ids);

}
