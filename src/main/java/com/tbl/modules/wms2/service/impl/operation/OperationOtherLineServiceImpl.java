package com.tbl.modules.wms2.service.impl.operation;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.modules.wms2.dao.operation.OperationOtherLineMapper;
import com.tbl.modules.wms2.entity.operation.OperationOtherLine;
import com.tbl.modules.wms2.service.operation.OperationOtherLineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Sweven
 * @date 2020-06-25
 */
@Service
public class OperationOtherLineServiceImpl extends ServiceImpl<OperationOtherLineMapper, OperationOtherLine> implements OperationOtherLineService {

    @Autowired
    private OperationOtherLineMapper operationOtherLineMapper;

    @Override
    public List<OperationOtherLine> findByOtherCode(String otherCode) {
        return operationOtherLineMapper.findByOtherCode(otherCode);
    }

}
