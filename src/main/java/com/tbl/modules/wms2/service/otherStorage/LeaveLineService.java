package com.tbl.modules.wms2.service.otherStorage;


import com.baomidou.mybatisplus.service.IService;
import com.tbl.modules.wms2.entity.otherStorage.LeaveLineEntity;

/**
 * 出厂单
 */
public interface LeaveLineService extends IService<LeaveLineEntity> {



}
