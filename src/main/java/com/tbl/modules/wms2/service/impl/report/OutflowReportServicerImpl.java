package com.tbl.modules.wms2.service.impl.report;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.modules.wms2.dao.report.OutflowReportDao;
import com.tbl.modules.wms2.entity.insidewarehouse.check.YclCheckEntity;
import com.tbl.modules.wms2.entity.operation.YclInOutFlowEntity;
import com.tbl.modules.wms2.service.report.OutflowReportService;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Map;

/**
 * 出库报表
 */
@Service
public class OutflowReportServicerImpl extends ServiceImpl<OutflowReportDao, YclInOutFlowEntity> implements OutflowReportService {

    @Resource
    private OutflowReportDao outflowReportDao;

    @Override
    public boolean save(YclInOutFlowEntity entity) {
        return this.insertOrUpdate(entity);
    }

    @Override
    public Page<YclInOutFlowEntity> getPageList(Page page, Map<String, Object> map, YclInOutFlowEntity yclInOutFlowEntity) {
        EntityWrapper<YclInOutFlowEntity> ew = new EntityWrapper<YclInOutFlowEntity>();
        /*ew.eq("BUSINESS_TYPE",1)*/
                ew.eq(StrUtil.isNotEmpty(yclInOutFlowEntity.getAreaCode()),"",yclInOutFlowEntity.getAreaCode())
                .eq(StrUtil.isNotEmpty(yclInOutFlowEntity.getSupplierName()),"",yclInOutFlowEntity.getSupplierName());
        Page<YclInOutFlowEntity> mapPage = this.selectPage(page,ew);
        return mapPage;
    }

    @Override
    public boolean delete(String ids) {
        return this.deleteBatchIds(Arrays.asList(ids.split(",")));
    }

    @Override
    public YclInOutFlowEntity findById(String id) {
        return this.selectById(id);
    }
}
