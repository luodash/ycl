package com.tbl.modules.wms2.service.operation;

import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.wms2.entity.operation.YclInOutFlowEntity;

import java.util.List;
import java.util.Map;

/**
 * 出入库流水
 *
 * @author DASH
 */
public interface YclInOutFlowService {

    /**
     * 功能描述:保存并更新库存
     *
     * @param yclInOutFlowEntity
     * @return boolean
     */
    boolean save(YclInOutFlowEntity yclInOutFlowEntity);

    /**
     * 功能描述: 批量保存
     *
     * @param yclInOutFlowEntities
     * @return boolean
     */
    Boolean saveBatch(List<YclInOutFlowEntity> yclInOutFlowEntities);

    /**
     * 功能描述:列表页
     *
     * @param pageTbl
     * @param map
     * @return PageUtils
     */
    PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map);

    /**
     * 功能描述:删除
     *
     * @param ids
     * @return boolean
     */
    boolean delete(String ids);

    /**
     * 功能描述:根据ID查询
     *
     * @param id
     * @return
     */
    YclInOutFlowEntity findById(Long id);

    /**
     *
     *
     * @param yclInOutFlowEntity
     * @return
     */
    List<YclInOutFlowEntity> listView(YclInOutFlowEntity yclInOutFlowEntity);
}
