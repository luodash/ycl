package com.tbl.modules.wms2.service.impl.insidewarehouse.transferorder;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.wms2.dao.insidewarehouse.transferorder.TransferOrderDAO;
import com.tbl.modules.wms2.dao.insidewarehouse.transferorder.TransferOrderLineDAO;
import com.tbl.modules.wms2.entity.insidewarehouse.transferorder.TransferOrderEntity;
import com.tbl.modules.wms2.entity.insidewarehouse.transferorder.TransferOrderLineEntity;
import com.tbl.modules.wms2.service.insidewarehouse.transferorder.TransferOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.sound.sampled.Line;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("yclTransferOrderService")
public class TransferOrderServiceImpl extends ServiceImpl<TransferOrderDAO, TransferOrderEntity> implements TransferOrderService {

    @Resource
    private TransferOrderDAO transferOrderDAO;
    @Resource
    private TransferOrderLineDAO transferOrderLineDAO;

    public PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map){
        String sortName = pageTbl.getSortname();
        String sortOrder = pageTbl.getSortorder();
        if (StringUtils.isEmptyString(sortName)) {
            sortName = "";
        }
        if (StringUtils.isEmptyString(sortOrder)) {
            sortOrder = "desc";
        }
        Page page = new Page(pageTbl.getPageno(), pageTbl.getPagesize(), sortName, "asc".equalsIgnoreCase(sortOrder));
        return null;
    }

    public Long getSequence(){
        return transferOrderDAO.getSequence();
    }


    public Map<String, Object> getPageList(Map map) {
        if(map.containsKey("createTime") && StringUtils.isNotEmpty(String.valueOf(map.get("createTime")))){
            //2020-06-05+-+2020-07-02
            String createTime = String.valueOf(map.get("createTime"));
            String createTimeStart = createTime.substring(0,10);
            String createTimeEnd = createTime.substring(createTime.lastIndexOf("-")-7);
            map.put("createTimeStart",createTimeStart);
            map.put("createTimeEnd",createTimeEnd);

        }
        int count = transferOrderDAO.getCount(map);
        if(count>0){
            List<TransferOrderEntity> list = transferOrderDAO.getPageList(map);
            Map<String, Object> rerultMap = new HashMap<>(4);
            rerultMap.put("code", 0);
            rerultMap.put("msg", "success");
            rerultMap.put("count", count);
            rerultMap.put("data", list);
            return rerultMap;
        }else{
            Map<String, Object> rerultMap = new HashMap<>(4);
            rerultMap.put("code", 1);
            rerultMap.put("msg", "Can not found data");
            rerultMap.put("count", 0);
            rerultMap.put("data", null);
            return rerultMap;
        }

    }


    /**
     * 查询头表数据列表
     * @param map
     * @return
     */
    public Map<String, Object> getHeadPageList(Map map) {
        if(map.containsKey("createTime") && StringUtils.isNotEmpty(String.valueOf(map.get("createTime")))){
            //2020-06-05+-+2020-07-02
            String createTime = String.valueOf(map.get("createTime"));
            String createTimeStart = createTime.substring(0,10);
            String createTimeEnd = createTime.substring(createTime.lastIndexOf("-")-7);
            map.put("createTimeStart",createTimeStart);
            map.put("createTimeEnd",createTimeEnd);

        }
        int count = transferOrderDAO.getHeadCount(map);
        if(count>0){
            List<TransferOrderEntity> list = transferOrderDAO.getHeadPageList(map);
            Map<String, Object> rerultMap = new HashMap<>(4);
            rerultMap.put("code", 0);
            rerultMap.put("msg", "success");
            rerultMap.put("count", count);
            rerultMap.put("data", list);
            return rerultMap;
        }else{
            Map<String, Object> rerultMap = new HashMap<>(4);
            rerultMap.put("code", 1);
            rerultMap.put("msg", "Can not found data");
            rerultMap.put("count", 0);
            rerultMap.put("data", null);
            return rerultMap;
        }

    }


    @Override
    public List<TransferOrderEntity> getPageListByMap(Map map) {
        return this.selectByMap(map);
    }

    @Override
    public boolean delete(String ids) {
        return this.deleteBatchIds(StringUtils.stringToList(ids));
    }

    @Override
    public boolean deleteByTranCodes(String tranCodes) {
        Wrapper<TransferOrderEntity> wrapper = new EntityWrapper<TransferOrderEntity>();
        wrapper.in("TRAN_CODE", tranCodes.split(","));
        Integer deleteFlag = transferOrderDAO.delete(wrapper);
        //Wrapper<TransferOrderLineEntity> lineEntityWrapper = new EntityWrapper<TransferOrderLineEntity>();

        System.out.println(deleteFlag);
        return deleteFlag > 0 ? true : false;
    }


    public TransferOrderEntity findById(String id) {
        return transferOrderDAO.selectById(id);
    }

    public List<TransferOrderEntity> getListById(String id) {
        return transferOrderDAO.getListById(id);
    }

    public Map<String,String> insertHeader(Map<String,Object> map){
//        if(map==null || map.size()<17){//参数不允许小于17个
//            Map<String, String> resultMap = new HashMap<>(2);
//            resultMap.put("code","error");
//            resultMap.put("msg","need at least 17 parameters,missing input parameters!");
//            return resultMap;
//        }
//        transferOrderDAO.insertHeader(map);
//        Map<String,String> resultMap = transferOrderDAO.insertHeader(map);
//        if(!resultMap.containsKey("retCode")){
//            resultMap.put("code","error");
//            resultMap.put("msg","保存错误!");
//            return resultMap;
//        }
//        //执行有反馈结果
//        String retCode = String.valueOf(resultMap.get("retCode"));
//        if("E".equals(retCode)){//存储过程处理出错、退出并返回出错信息
//            resultMap.put("code","error");
//            resultMap.put("msg",String.valueOf(resultMap.get("retMess")));
//        }else if("S".equals(retCode)){//接收成功标志
//            //TODO 在WMS中保存输入的参数信息，为委外加工接收界面中的保存按钮的功能
//
//            resultMap.put("code","success");
//            resultMap.put("msg","保存成功！");
//        }
//        return resultMap;
        return transferOrderDAO.insertHeader(map);
    }

    public Map<String,String> insertLine(Map<String,Object> map){
        return transferOrderDAO.insertLine(map);
    }

    public Map<String,String> processOrder(Map<String,Object> map){
        return transferOrderDAO.processOrder(map);
    }

    public Map<String,Object> getEBSFromLocator(String lineId){
        return transferOrderDAO.getEBSFromLocator(lineId);
    }

    public Map<String,Object> getEBSToLocator(String lineId){
        return transferOrderDAO.getEBSToLocator(lineId);
    }


    public Map<String,Object>  saveTransOrderMap(Map<String, Object> headerMap ,TransferOrderEntity line,String type){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Map<String,Object> resultMap = new HashMap<>();
        Map<String,Object> LineMap = new HashMap();
        LineMap.put("headerId",headerMap.get("headerId"));//头ID
        LineMap.put("lineId",line.getLineId());//行ID
        LineMap.put("vendorLot",line.getLotsNum());//供应商批次
        LineMap.put("itemNumber",line.getMaterialCode());//物料编码
        LineMap.put("quantity",line.getQuantity());//数量
        LineMap.put("createdBy",headerMap.get("moUserId"));//创建人
        LineMap.put("creationDate",headerMap.get("creationDate"));//创建时间
        LineMap.put("transActionDate",formatter.format(new Date()));
        LineMap.put("lastUpdateBy",headerMap.get("moUserId"));
        LineMap.put("lastUpdateDate",formatter.format(new Date()));
        LineMap.put("transActionQuality",line.getOutQuantity());//上下架数量
        LineMap.put("needByDate",formatter.format(new Date()));//创建时间
        LineMap.put("fromOrganizationId",headerMap.get("fromOrganizationId"));//来源组织ID
        LineMap.put("toOrganizationId",headerMap.get("toOrganizationId"));//目标组织ID
        LineMap.put("fromSubinvCode",headerMap.get("fromSubinvCode"));//来源仓库编码
        LineMap.put("toSubinvCode",headerMap.get("toSubinvCode"));//目标仓库编码
        LineMap.put("fromLocatorId",headerMap.get("fromLocatorld"));//来源货位ID
        LineMap.put("toLocatorId",headerMap.get("toLocatorld"));//目标货位ID
        LineMap.put("vendorName","");//供应商名称  有些情况是必须，有些情况可为空(暂时先传空)
        LineMap.put("classesDate","");//班次日期
        LineMap.put("classes","");
        LineMap.put("lastUpdateLogin","");
        LineMap.put("comments","");
        if("commit".equals(type)){
            transferOrderDAO.processOrder(LineMap);
        }
        if("save".equals(type)){
            transferOrderDAO.insertLine(LineMap);
        }
        resultMap.put("retCode",LineMap.get("retCode"));
        resultMap.put("retMess",LineMap.get("retMess"));

//        transferOrderDAO.insertLine(LineMap);
//        if(LineMap.containsKey("retCode")) {
//            if(LineMap.get("retCode").toString().equals("S")) {
//                LineMap.remove("retCode");
//                LineMap.remove("retMess");
//
//                if(LineMap.containsKey("retCode")) {
//                    resultMap.put("retCode",LineMap.get("retCode"));
//                    resultMap.put("retMess",LineMap.get("retMess"));
//                }
//            }else{
//                resultMap.put("retCode","E");
//                resultMap.put("retMess","行信息保存失败");
//            }
//        }
        return resultMap;
    }
}