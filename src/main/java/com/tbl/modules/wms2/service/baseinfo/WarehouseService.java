package com.tbl.modules.wms2.service.baseinfo;

import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.wms2.entity.baseinfo.WarehouseEntity;

import java.util.List;
import java.util.Map;

/**
 * 仓库
 *
 * @author DASH
 */
public interface WarehouseService {

    /**
     * 功能描述:保存
     *
     * @param entity
     * @return boolean
     */
    boolean save(WarehouseEntity entity);

    /**
     * 功能描述:列表页
     *
     * @param pageTbl
     * @param map
     * @return PageUtils
     */
    PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map);

    /**
     * 功能描述:删除
     *
     * @param ids
     * @return boolean
     */
    boolean delete(String ids);

    /**
     * 功能描述:根据ID查询
     *
     * @param id
     * @return
     */
    WarehouseEntity findById(Long id);



    Map<String,Object> entruckNotice(String num);


    Map<String,Object> getLocationfromEBS(Map map);

}
