package com.tbl.modules.wms2.interfacerecord;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 接口执行表
 * @author 70486
 */
@TableName("YCL_INTERFACE_RUN")
@Getter
@Setter
@ToString
public class InterfaceRun implements Serializable {

	/**
	 * 主键
	 */
    @TableId(value = "ID")
    private Long id;

    /**
     * 接口编码
     */
    @TableField(value = "INTERFACECODE")
    private String interfacecode;

    /**
     * 接口名称
     */
    @TableField(value = "INTERFACENAME")
    private String interfacename;

    /**
     * 操作时间
     */
    @TableField(value = "OPERATETIME")
    private String operatetime;

    /**
     * 请求报文
     */
    @TableField(value = "PARAMSINFO")
    private String paramsinfo;
    
    /**
     * 返回报文
     */
    @TableField(value = "RETURNCODE")
    private String returncode;

    /**
     * 是否成功
     */
    @TableField(value = "ISSUCCESS")
    private String issuccess;
    
    /**
     * 第一次失败原因
     */
    @TableField(value = "FLASECONTENT")
    private String flasecontent;
    
    /**
     * 特殊标识：用于维护；
     */
    @TableField(value = "SPESIGN")
    private Long spesign;

    /**
     * 创建时间
     */
    @TableField(value = "CREATETIME")
    private Date createtime;

    /**
     * 执行次数
     */
    @TableField(value = "EXECUTENUM")
    private Long executenum;

    /**
     * 物料编码
     */
    @TableField(value = "ITEM_CODE")
    private String itemCode;

    /**
     * 送货单号
     */
    @TableField(value = "DELIVERY_NO")
    private String deliveryNo;

    /**
     * 采购订单编号
     */
    @TableField(value = "PURCHASE_NO")
    private String purchaseNo;

}
