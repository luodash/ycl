package com.tbl.modules.wms2.interfacerecord;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 接口日志记录
 * @author 70486
 */
@TableName("YCL_INTERFACE_RECORD")
@Getter
@Setter
@ToString
public class InterfaceRecord implements Serializable {

	/**
	 * 主键
	 */
    @TableId(value = "ID")
    private Long id;

    /**
     * 接口编码
     */
    @TableField(value = "INTERFACE_CODE")
    private String interfaceCode;

    /**
     * 接口名称
     */
    @TableField(value = "INTERFACE_NAME")
    private String interfaceName;

    /**
     * 创建时间
     */
    @TableField(value = "CREATE_TIME")
    private Date createTime;

    /**
     * 请求报文
     */
    @TableField(value = "REQUEST_MESSAGE")
    private String requestMessage;
    
    /**
     * 返回报文
     */
    @TableField(value = "RESPONSE_MESSAGE")
    private String responseMessage;

    /**
     * 物料编码
     */
    @TableField(value = "ITEM_CODE")
    private String itemCode;

    /**
     * 送货单号
     */
    @TableField(value = "DELIVERY_NO")
    private String deliveryNo;

    /**
     * 采购订单编号
     */
    @TableField(value = "PURCHASE_NO")
    private String purchaseNo;

}
