package com.tbl.modules.wms2.constant;

/**
 * 常量类
 * @author 70486
 **/
public class Constant {

    /**
     * 订单状态（已取消、批准）
     */
    public final static String ORDER_STATE_CANCEL = "已取消";//订单取消
    public final static String ORDER_STATE_APPROVE = "批准";//订单审批通过
    public final static String SUCCESS = "S";//存储过程调用成功
    public final static String ERROR = "E";//存储过程调用失败
    public final static String MES_PROCEDURE_1 = "wms_2_mes_issue_bill.issue_bill_lot";
}
