package com.tbl.modules.les.constant;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tbl.common.config.StaticConfig;
import net.sf.json.JSONArray;
import org.springframework.web.client.RestTemplate;

/**
 * 获取中间件设备工具类
 * @author OYZQ
 */
public class DeviceUtil {
    public static final String URI = "http://" + StaticConfig.getIp() + ":" + StaticConfig.getPort() + "/" + StaticConfig.getName() + "/" + StaticConfig.getRfidinfo();
    public static RestTemplate restTemplate = RestTemplateUtil.createRestTemplate();

    public static com.alibaba.fastjson.JSONArray getDevices() {
        String returnStr = restTemplate.postForObject(URI + "/{type}", null, String.class, (Object) new String[]{"020204"});
        JSONObject json = JSONObject.parseObject(returnStr);
        com.alibaba.fastjson.JSONArray array = JSON.parseArray(json.get("result").toString());
        return JSON.parseArray(json.get("result").toString());
    }
}
