package com.tbl.modules.les.constant;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.util.Properties;

/**
 * 配置工具
 * @author 70486
 */
public class ConfigUtil {
    private static final URL FILE_PATH =Thread.currentThread().getContextClassLoader().getResource("");

    public ConfigUtil(){}
    private static final Properties PROPS = new Properties();
    static{
        try {
            PROPS.load(ConfigUtil.class.getClassLoader().getResourceAsStream("resources.properties"));
          
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static String getValue(String key){
        return PROPS.getProperty(key);
    }

    public static void updateProperties(String key,String value) {    
            PROPS.setProperty(key, value);
    } 
    
    public static void writeProperties(String key,String value) {
        try {
            String fileName = "resources.properties";
            OutputStream fos = new FileOutputStream(FILE_PATH.getPath()+File.separator+ fileName);
               PROPS.setProperty(key, value);
               //以适合使用 load 方法加载到 Properties 表中的格式，
               //将此 Properties 表中的属性列表（键和元素对）写入输出流
               PROPS.store(fos, "Update '" + key + "' value");
               fos.close();// 关闭流
           } catch (IOException e) {
               System.err.println("Visit "+ FILE_PATH +" for updating "+key+" value error");
           }
       }
    
    public static void main(String[] args) {
		System.out.println(getValue("1"));
	}
}