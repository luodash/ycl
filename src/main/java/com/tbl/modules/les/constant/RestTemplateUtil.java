package com.tbl.modules.les.constant;

import com.tbl.common.config.StaticConfig;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

/**
 * Rest工具类
 * @author duckhyj
 * @create 2017-11-21 17:21
 */
public class RestTemplateUtil {

    public static RestTemplate restTemplate;

    static {
        SimpleClientHttpRequestFactory httpRequestFactory = new SimpleClientHttpRequestFactory();
        //设置连接超时
        httpRequestFactory.setConnectTimeout(Integer.parseInt(StaticConfig.getConnectTimeout()));
        //设置读等待
        httpRequestFactory.setReadTimeout(Integer.parseInt(StaticConfig.getReadTimeout()));
        restTemplate = new RestTemplate(httpRequestFactory);
    }

    public static RestTemplate createRestTemplate(){
        return restTemplate;
    }




}