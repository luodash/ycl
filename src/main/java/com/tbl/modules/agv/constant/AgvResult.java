package com.tbl.modules.agv.constant;

import com.tbl.common.utils.R;
import lombok.Getter;
import lombok.Setter;

/**
 *  接口返回结果封装
 * @author 70486
 */
@Getter
@Setter
public class AgvResult {
    public String success = "True";
    public String errCode = "";
    public String errMsg = "";


    public AgvResult() {
    }

    public AgvResult(String data) {
        this.errMsg = data;
    }

    public AgvResult(String success, String errCode, String errMsg) {
        this.success = success;
        this.errCode = errCode;
        this.errMsg = errMsg;
    }

}
