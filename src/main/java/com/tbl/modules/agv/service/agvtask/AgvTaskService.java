package com.tbl.modules.agv.service.agvtask;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.agv.entity.agvtask.AgvTask;
import com.tbl.modules.wms.entity.baseinfo.Car;
import com.tbl.modules.wms.entity.outstorage.OutStorageDetail;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * agv主任务服务类
 * @author cxf
 */
public interface AgvTaskService extends IService<AgvTask> {

    /**
     *:列表页
     * @param pageTbl
     * @param map
     * @return PageUtils
     */
    PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map);

    /**
     * 获取列表
     * @param ids 选中的主键
     * @param queryWhNoFrom
     * @param queryWhNoTo
     * @return List<AgvTask>
     */
    List<AgvTask> getExcelList(String ids, String queryWhNoFrom, String queryWhNoTo);
}
