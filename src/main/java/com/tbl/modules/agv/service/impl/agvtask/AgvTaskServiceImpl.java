package com.tbl.modules.agv.service.impl.agvtask;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.agv.dao.agv.AgvTaskDAO;
import com.tbl.modules.agv.entity.agvtask.AgvTask;
import com.tbl.modules.agv.service.agvtask.AgvTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * agv任务主表
 * @author cxf
 */
@Service
public class AgvTaskServiceImpl extends ServiceImpl<AgvTaskDAO, AgvTask> implements AgvTaskService {
    /**
     * agv任务
     */
    @Autowired
    private AgvTaskDAO agvTaskDAO;
    /**
     * 查询列表信息
     * @param pageTbl
     * @param map 条件
     * @return PageUtils
     */
    @Override
    public PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map) {
        String sortName = pageTbl.getSortname();
        String sortOrder = pageTbl.getSortorder();
        if (StringUtils.isEmptyString(sortName)) {
            sortName = "id";
        }
        if (StringUtils.isEmptyString(sortOrder)) {
            sortOrder = "desc";
        }
        Page page = new Page(pageTbl.getPageno(), pageTbl.getPagesize(), sortName, "asc".equalsIgnoreCase(sortOrder));
        return new PageUtils(page.setRecords(agvTaskDAO.getPageList(page, map)));
    }

    /**
     * 获取导出列表
     * @param ids 选中的主键
     * @param queryWhNoFrom
     * @param queryWhNoTo
     * @return List<AgvTask>
     */
    @Override
    public List<AgvTask> getExcelList(String ids, String queryWhNoFrom, String queryWhNoTo) {
        Map<String, Object> map = new HashMap<>(3);
        map.put("ids", StringUtils.stringToInt(ids));
        map.put("queryWhNoFrom", queryWhNoFrom);
        map.put("queryWhNoTo", queryWhNoTo);
        return agvTaskDAO.getExcelList(map);
    }

}
