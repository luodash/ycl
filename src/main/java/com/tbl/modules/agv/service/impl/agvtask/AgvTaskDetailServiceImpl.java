package com.tbl.modules.agv.service.impl.agvtask;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.modules.agv.dao.agv.AgvTaskDetailDAO;
import com.tbl.modules.agv.entity.agvtask.AgvTaskDetail;
import com.tbl.modules.agv.service.agvtask.AgvTaskDetailService;
import org.springframework.stereotype.Service;

/**
 * agv任务子表
 * @author cxf
 */
@Service
public class AgvTaskDetailServiceImpl extends ServiceImpl<AgvTaskDetailDAO, AgvTaskDetail> implements AgvTaskDetailService {


}
