package com.tbl.modules.agv.service.agvtask;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.agv.entity.agvtask.AgvTaskLog;

import java.util.List;
import java.util.Map;

/**
 * agv日志
 * @author zxf
 */
public interface AgvTaskLogService extends IService<AgvTaskLog> {
    /**
     *:列表页
     * @param pageTbl
     * @param map
     * @return PageUtils
     */
    PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map);

    /**
     * 获取列表
     * @param ids 选中的主键
     * @param taskNo
     * @return List<AgvTaskLog>
     */
    List<AgvTaskLog> getExcelList(String ids, String taskNo);
}
