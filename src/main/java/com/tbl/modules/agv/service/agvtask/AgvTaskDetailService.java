package com.tbl.modules.agv.service.agvtask;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.modules.agv.entity.agvtask.AgvTaskDetail;

/**
 * agv子任务服务类
 * @author cxf
 */
public interface AgvTaskDetailService extends IService<AgvTaskDetail> {


}
