package com.tbl.modules.agv.service.webapi.client;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.modules.agv.entity.agvtask.AgvTask;
import org.apache.poi.ss.formula.functions.T;

import java.util.Map;

/**
 * http接口客户端
 * @author cxf
 */
public interface WebApiClientService extends IService<T> {

    /**
     * 原材料入库：WMS生成AGV任务并发送给AMS
     * @param barCode 条形码
     * @param locationFrom 点位信息
     * @return String
     */
    String storageInAddTask(String barCode, String locationFrom);

    /**
     * 原材料配送：WMS生成AGV任务并发送给AMS
     * @return String
     */
    String deliveryAddTask();

    /**
     * 原材料配送：WMS生成AGV任务并发送给AMS
     * @return String
     */
    String returnAddTask();

    /**
     * 原材料退料：WMS生成AGV任务并发送给AMS
     * @return String
     */
    String returnBracketTask();

    /**
     *  退固废：WMS生成AGV任务并发送给AMS
     * @return String
     */
    String returnSolidWasteTask();

    /**
     *  退固废：WMS生成AGV任务并发送给AMS
     * @return String
     */
    String emptyDiskTask();

    /**
     * 查询光感，判断库位上是否有托盘
     * @param station 点位编号
     * @return Map<String,Object>
     */
    Map<String,Object> readStockStatus(Integer station);

}
