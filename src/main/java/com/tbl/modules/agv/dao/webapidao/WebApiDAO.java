package com.tbl.modules.agv.dao.webapidao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.poi.ss.formula.functions.T;

/**
 * http接口服务端持久层
 * @author cxf
 */
public interface WebApiDAO extends BaseMapper<T> {

    /**
     * 获取agv任务号的最新序列值
     * @return long
     */
    long selectSeqAgvTaskNo();

}
