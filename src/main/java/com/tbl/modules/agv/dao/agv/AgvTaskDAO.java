package com.tbl.modules.agv.dao.agv;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.modules.agv.entity.agvtask.AgvTask;

import java.util.List;
import java.util.Map;

/**
 * agv任务主表DAO
 * @author zxf
 */
public interface AgvTaskDAO extends BaseMapper<AgvTask> {
    /**
     * 查询库区列表信息
     * @param page
     * @param map
     * @return AgvTask
     */
    List<AgvTask> getPageList(Page page, Map<String, Object> map);
    /**
     * 获得Excel导出列表
     * @param map 条件参数
     * @return
     */
    List<AgvTask> getExcelList(Map<String, Object> map);
}
