package com.tbl.modules.agv.dao.agv;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.modules.agv.entity.agvtask.AgvTask;
import com.tbl.modules.agv.entity.agvtask.AgvTaskLog;

import java.util.List;
import java.util.Map;

/**
 * agv任务主表DAO
 * @author zxf
 */
public interface AgvTaskLogDAO extends BaseMapper<AgvTaskLog> {
    /**
     * 查询agv日志列表信息
     * @param page
     * @param map
     * @return AgvTaskLog
     */
    List<AgvTaskLog> getPageList(Page page, Map<String, Object> map);
    /**
     * 获得Excel导出列表
     * @param map 条件参数
     * @return
     */
    List<AgvTaskLog> getExcelList(Map<String, Object> map);
}
