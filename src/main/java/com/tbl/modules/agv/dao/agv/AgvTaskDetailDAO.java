package com.tbl.modules.agv.dao.agv;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tbl.modules.agv.entity.agvtask.AgvTaskDetail;

/**
 * agv任务子表DAO
 * @author cxf
 */
public interface AgvTaskDetailDAO extends BaseMapper<AgvTaskDetail> {
}
