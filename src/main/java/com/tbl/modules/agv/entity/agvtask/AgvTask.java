package com.tbl.modules.agv.entity.agvtask;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * wms生成的agv任务主表
 * @author cxf
 */
@TableName("ZNFH_AGV_TASK")
@Data
public class AgvTask implements Serializable {

    /**
     * 主键
     */
    @TableId(value = "ID")
    private Long id;

    /**
     * 任务类型
     */
    @TableField(value = "BUSSINESS_TYPE")
    private Integer bussinessType;

    /**
     * 来源仓库号
     */
    @TableField(value = "WH_NO_FROM")
    private String whNoFrom;

    /**
     * 目的仓库号
     */
    @TableField(value = "WH_NO_TO")
    private String whNoTo;

    /**
     * 系统名称
     */
    @TableField(value = "SYS_NAME")
    private String sysName;

    /**
     * 设备名称
     */
    @TableField(value = "DEVICE_NAME")
    private String deviceName;

    /**
     * 生成的AGV任务号(要求唯一性)
     */
    @TableField(value = "TASK_NO")
    private String taskNo;

    /**
     * 批次号
     */
    @TableField(value = "BATCH_NO")
    private String batchNo;

    /**
     * 任务序号
     */
    @TableField(value = "ORDER_NO")
    private Integer orderNo;

    /**
     * 来源暂存位
     */
    @TableField(value = "LOCATION_FROM")
    private String locationFrom;

    /**
     * 目的暂存位
     */
    @TableField(value = "LOCATION_TO")
    private String locationTo;

    /**
     * 优先级(数字越大优先级越高)
     */
    @TableField(value = "PRIORITY")
    private Integer priority;

    /**
     * 用以分区相同设备不同动作的流程
     */
    @TableField(value = "EXT_PARAM")
    private String extParam;

    /**
     * 备用参数1
     */
    @TableField(value = "EXT1")
    private String ext1;

    /**
     * 备用参数2
     */
    @TableField(value = "EXT2")
    private String ext2;

    /**
     * 备用参数3
     */
    @TableField(value = "EXT3")
    private String ext3;

    /**
     * 物料编码
     */
    @TableField(value = "MATERIAL_CODE")
    private String materialCode;

    /**
     * 叫料移库数量
     */
    @TableField(value = "DELIVERY_NUM")
    private BigDecimal deliveryNum;
}
