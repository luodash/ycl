package com.tbl.modules.agv.entity.agvtask;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * wms生成的agv任务子表
 * @author cxf
 */
@TableName("ZNFH_AGV_TASK_DETAIL")
@Data
public class AgvTaskDetail implements Serializable {

    /**
     * 主键
     */
    @TableId(value = "ID")
    private Long id;

    /**
     * 父ID
     */
    @TableField(value = "P_ID")
    private Long pId;

    /**
     * 来源库区,支持多个库区
     */
    @TableField(value = "AREA_FROM")
    private String areaFrom;

    /**
     * 目的库区,支持多个库区
     */
    @TableField(value = "AREA_TO")
    private String areaTo;

}
