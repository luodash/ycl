package com.tbl.modules.agv.entity.agvtask;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * agv接口日志表
 * @author zxf
 */
@TableName("ZNFH_AGV_TASK_LOG")
@Data
public class AgvTaskLog implements Serializable {

    /**
     * 主键
     */
    @TableId(value = "ID")
    private Long id;

    /**
     * 生成的AGV任务号(要求唯一性)
     */
    @TableField(value = "TASK_NO")
    private String taskNo;

    /**
     * AGV状态
     */
    @TableField(value = "AGV_STATE")
    private Integer agvState;

    /**
     * AGV调用时间
     */
    @TableField(value = "AGV_TIME")
    private Date agvTime;
    /**
     * AGV调用时间
     */
    @TableField(exist = false)
    private String  agvTimeStr;
}
