package com.tbl.modules.agv.schedule;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.tbl.common.utils.DateUtils;
import com.tbl.modules.agv.dao.webapidao.WebApiDAO;
import com.tbl.modules.agv.service.webapi.client.WebApiClientService;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.wms.entity.baseinfo.Shelf;
import com.tbl.modules.wms.service.baseinfo.ShelfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.DecimalFormat;
import java.util.Map;

/**
 * agv定时任务
 * @author cxf
 */
@Component
@Configuration
public class AgvJobs extends AbstractController {

    /**
     * 2分钟
     */
    public final static long TWO_Minute =  60 * 1000 * 2;
    /**
     * http客户端
     */
    @Autowired
    private WebApiClientService webApiClientService;
    /**
     * 库位
     */
    @Autowired
    private ShelfService shelfService;
    /**
     * webapi
     */
    @Autowired
    private WebApiDAO webApiDAO;

    /**
     * 定时查询光感，判断库位上是否有托盘
     */
    /*@Scheduled(fixedDelay = TWO_Minute)
    public void readStockStatus(){
        logger.info(DateUtils.getTime()+" >>查询光感，检测库位上是否有托盘....");
        shelfService.selectList(new EntityWrapper<Shelf>().isNotNull("AGV_POINT")).forEach(shelf -> {
            Map<String,Object> pointStatusMap = webApiClientService.readStockStatus(Integer.parseInt(shelf.getAgvPoint()));
            //1非空0空9点位不存在
            int occupy = (Integer) pointStatusMap.get("occupy");

            //检测到库位为空，那么根据mes叫料接口（dblink）的数据，匹配对应的物料，生成搬运任务，并存入agv任务表
            //agv任务号
            String agvTaskNo = new DecimalFormat("000000000000").format(webApiDAO.selectSeqAgvTaskNo());

            //下发任务给AMS
            webApiClientService.deliveryAddTask();

        });
    }*/

}
