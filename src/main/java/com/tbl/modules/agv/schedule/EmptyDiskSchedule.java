package com.tbl.modules.agv.schedule;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.time.LocalDateTime;


/**
 * 定时任务
 * @author zxf
 */
@Configuration      //1.主要用于标记配置类，兼备Component的效果。
@EnableScheduling   // 2.开启定时任务
public class EmptyDiskSchedule {
    //3.添加定时任务
    @Scheduled(cron = "0 0/2 * * * ?")
    //2分钟执行一次
    //@Scheduled(fixedRate=5000)
    private void configureTasks() {

    }
}
