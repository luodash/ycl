package com.tbl.modules.agv.util;

import com.alibaba.fastjson.JSONObject;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;

/**
 * Http请求客户端GET/POST请求方式
 * @author cxf
 */
public class Rbsc {

    /**
    * 发送GET请求
    * @param url 目的地址
    * @param parameters 请求参数，Map类型。
    * @return 远程响应结果
    **/
    public static String sendGet(String url, Map<String, String> parameters) {
        String result="";
        // 读取响应输入流
        BufferedReader in = null;
        // 存储参数
        StringBuffer sb = new StringBuffer();
        // 编码之后的参数
        String params = "";
        try {
            // 编码请求参数
            if(parameters.size()==1){
                for(String name:parameters.keySet()){
                    sb.append(name).append("=").append(java.net.URLEncoder.encode(parameters.get(name), "UTF-8"));
                }
                params=sb.toString();
            }else{
                for (String name : parameters.keySet()) {
                    sb.append(name).append("=").append(java.net.URLEncoder.encode(parameters.get(name), "UTF-8")).append("&");
                }
                String temp_params = sb.toString();
                params = temp_params.substring(0, temp_params.length() - 1);
            }
            String full_url = url + "?" + params;
            System.out.println(full_url);
            // 创建URL对象
            java.net.URL connURL = new java.net.URL(full_url);
            // 打开URL连接
            java.net.HttpURLConnection httpConn = (java.net.HttpURLConnection) connURL.openConnection();
            // 设置通用属性
            httpConn.setRequestProperty("Accept", "/*");
            httpConn.setRequestProperty("Connection", "Keep-Alive");
            httpConn.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
            // 建立实际的连接
            httpConn.connect();
            // 响应头部获取
            Map<String, List<String>> headers = httpConn.getHeaderFields();
            // 遍历所有的响应头字段
            for (String key : headers.keySet()) {
                System.out.println(key + "\t：\t" + headers.get(key));
            }
            // 定义BufferedReader输入流来读取URL的响应,并设置编码方式
            in = new BufferedReader(new InputStreamReader(httpConn.getInputStream(), "UTF-8"));
            String line;
            // 读取返回的内容
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return result ;
    }

    /**
     * 发送POST请求
     * @param url 目的地址
     * @param par 请求参数，jsonstring类型。
     * @return 远程响应结果
     */
    public static String httpPostWithJSON(String url,String par)  {
        HttpPost httpPost = new HttpPost(url);
        CloseableHttpClient client = HttpClients.createDefault();
        String respContent = null;
        //解决中文乱码问题
        StringEntity entity = new StringEntity(par,"utf-8");
        entity.setContentEncoding("UTF-8");
        entity.setContentType("application/json");
        httpPost.setEntity(entity);

        try{
            HttpResponse resp = client.execute(httpPost);
            System.out.println(resp.getStatusLine().getStatusCode());
            if(resp.getStatusLine().getStatusCode() == 200) {
                HttpEntity he = resp.getEntity();
                respContent = EntityUtils.toString(he,"UTF-8");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return respContent;
    }



    public static void main(String[] args) {

        JSONObject json = new JSONObject();
        json.put("ZSR",0);
        json.put("YLSR",0);
        json.put("MZSR",0);
        json.put("GHSR",0);
        json.put("ZCSR",0);
        json.put("JCSR",0);
        json.put("HYSR",0);
        json.put("ZLSR",0);
        json.put("SSSR",0);
        json.put("WSCLSR",0);
        json.put("YPSR",0);
        json.put("ZYYPSR",0);
        json.put("YSFWFSR",0);
        json.put("QTMZSR",0);
        json.put("ZYSR",0);
        json.put("CWSR",0);
        json.put("ZCSR_ZY",0);
        json.put("JCSR_ZY",0);
        json.put("HYSR_ZY",0);
        json.put("ZLSR_ZY",0);
        json.put("SSSR_ZY",0);
        json.put("HLSR_ZY",0);
        json.put("WSCLSR_ZY",0);
        json.put("YPSR_ZY",0);
        json.put("ZYYPSR_ZY",0);
        json.put("YSFWFSR_ZY",0);
        json.put("QTZYSR",0);
        json.put("JBYWSR",0);
        json.put("QZZYSR",0);
        json.put("ZQTSR",0);
        json.put("YBHZYLSR",0);
        json.put("YBMLWSR",0);
        json.put("YBMLNSR",0);
        json.put("MLNBXDYLFY",0);
        json.put("ZFY",0);
        json.put("YLYWCB",0);
        json.put("WSCLF",0);
        json.put("YPZC",0);
        json.put("ZYZC",0);
        json.put("GLFY",0);
        json.put("GGWSZC",0);
        json.put("ZLZRC",0);
        json.put("MZHJZRCS",0);
        json.put("YYZLRCS",0);
        json.put("PTMZRCS",0);
        json.put("CYRS",0);
        json.put("SWRS",0);
        json.put("RYRCS",0);
        json.put("RYRTS",0);
        json.put("ZYBRSSZH",0);
        json.put("YJSSCS",0);
        json.put("EJSSCS",0);
        json.put("SJSSCS",0);
        json.put("SIJSSCS",0);
        json.put("YWRQ","2017-01-21");


        String re= httpPostWithJSON("http://p.zjgws.com/QA/WN/RiBao/Add?YLDWCODE=467223515",json.toString());
        System.out.println(re);
    }

}
