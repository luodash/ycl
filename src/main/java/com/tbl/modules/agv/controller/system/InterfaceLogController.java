package com.tbl.modules.agv.controller.system;

import com.alibaba.fastjson.JSON;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.platform.constant.MenuConstant;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.service.system.RoleService;
import com.tbl.modules.wms.entity.interfacelog.InterfaceLog;
import com.tbl.modules.wms.service.interfacelog.InterfaceLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * 接口日志控制层
 * @author 70486
 */
@Controller
@RequestMapping(value = "/interfaceLog")
public class InterfaceLogController extends AbstractController {

    /**
     * 用户service
     */
    @Autowired
    private RoleService roleService;
    /**
     * 接口日志服务类
     */
    @Autowired
    private InterfaceLogService interfaceLogService;

    /**
     * 跳转到接口日志维护列表
     * @return ModelAndView
     */
    @RequestMapping(value = "/toList.do")
    @ResponseBody
    public ModelAndView toList() {
        ModelAndView mv = new ModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(getSessionUser().getRoleId(),
                MenuConstant.interfaceLog));
        mv.setViewName("techbloom/system/interfacelog/interfacelog_list");
        return mv;
    }

    /**
     * 获取接口日志列表数据
     * @param queryJsonString 查询条件
     * @return Object
     */
    @RequestMapping(value = "/list.do")
    @ResponseBody
    public Object list(String queryJsonString) {
    	Map<String, Object> map = new HashMap<>(5);
        if (!StringUtils.isEmpty(queryJsonString)) {
            map = JSON.parseObject(queryJsonString);
        }
        PageTbl page = this.getPage();
        map.put("page", page.getPageno());
        map.put("limit", page.getPagesize());
        String sortName = page.getSortname();
        if (StringUtils.isEmptyString(sortName)) {
            sortName = "id";
            page.setSortname(sortName);
        }
        String sortOrder = page.getSortorder();
        if (StringUtils.isEmptyString(sortOrder)) {
            sortOrder = "desc";
            page.setSortorder(sortOrder);
        }
        map.put("sidx", page.getSortname());
        map.put("order", page.getSortorder());
        PageUtils pageUser = interfaceLogService.queryPage(map);
        page.setTotalRows(pageUser.getTotalCount() == 0 ? 1 : pageUser.getTotalCount());
        map.put("rows", pageUser.getList());
        executePageMap(map, page);
        return map;
    }

    /**
     * 弹出到明细页面
     * @param id 接口日志表主键
     * @return ModelAndView
     */
    @RequestMapping(value = "/toDetailList.do")
    @ResponseBody
    public ModelAndView toDetailList(Long id) {
        InterfaceLog interfaceLog = interfaceLogService.selectById(id);
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("techbloom/system/interfacelog/interfacelog_detail");
        mv.addObject("INTERFACECODE",interfaceLog.getInterfaceCode());
        mv.addObject("QACODES",interfaceLog.getQacode());
        mv.addObject("CREATETIME", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(interfaceLog.getCreateTime()));
        mv.addObject("PARAMSINFO",interfaceLog.getResponseMessage());
        mv.addObject("RETURNCODE",interfaceLog.getResponseMessage());
        return mv;
    }

}
