package com.tbl.modules.agv.controller.system;

import com.alibaba.fastjson.JSON;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.platform.constant.MenuConstant;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.service.system.LogService;
import com.tbl.modules.platform.service.system.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * 系统操作日志控制层
 * @author 70486
 */
@Controller
@RequestMapping(value = "/systemLog")
public class systemLogController extends AbstractController {

    /**
     * 用户service
     */
    @Autowired
    private RoleService roleService;
    /**
     * 接口日志服务类
     */
    @Autowired
    private LogService logService;

    /**
     * 跳转到系统日志维护列表
     * @return ModelAndView
     */
    @RequestMapping(value = "/toList.do")
    @ResponseBody
    public ModelAndView toList() {
        ModelAndView mv = new ModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(getSessionUser().getRoleId(), MenuConstant.systemLog));
        mv.setViewName("techbloom/system/systemlog/systemlog_list");
        return mv;
    }

    /**
     * 获取系统日志列表
     * @param queryJsonString 查询条件
     * @return Object
     */
    @RequestMapping(value = "/list.do")
    @ResponseBody
    public Object list(String queryJsonString) {
        Map<String, Object> map = new HashMap<>(4);
        if (!StringUtils.isEmptyString(queryJsonString)) {
            map = JSON.parseObject(queryJsonString);
        }
        PageTbl page = this.getPage();
        PageUtils utils = logService.getPageList(page, map);
        page.setTotalRows(utils.getTotalCount() == 0 ? 1 : utils.getTotalCount());
        //初始化分页对象
        executePageMap(map, page);
        map.put("rows", utils.getList());
        map.put("total", utils.getTotalPage() == 0 ? 1 : utils.getTotalPage());
        return map;
    }

}
