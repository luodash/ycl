package com.tbl.modules.agv.controller.system;

import com.alibaba.fastjson.JSON;
import com.tbl.common.utils.DateUtils;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.agv.service.agvtask.AgvTaskLogService;
import com.tbl.modules.platform.constant.LogActionConstant;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.service.system.LogService;
import com.tbl.modules.platform.service.system.RoleService;
import com.tbl.modules.platform.util.DeriveExcel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.tbl.modules.platform.constant.MenuConstant.agvTaskLog;

/**
 * agv日志控制层
 * @author zxf
 */
@RestController
@RequestMapping(value = "/agvTaskLog")
public class AgvTaskLogController extends AbstractController {
    /**
     * 角色
     */
    @Autowired
    private RoleService roleService;
    /**
     * agv日志
     */
    @Autowired
    private AgvTaskLogService agvTaskLogService;
    /**
     * 日志信息
     */
    @Autowired
    private LogService logService;

    /**
     * 跳转agv日志列表
     * @return MpdelAndView
     */
    @RequestMapping(value = "/toList")
    @ResponseBody
     public ModelAndView toList() {
        ModelAndView mv = this.getModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(getSessionUser().getRoleId(), agvTaskLog));
        mv.setViewName("agv/agvTask/agvLog_list");
        return mv;
    }

    /**
     * 获取agv日志列表数据
     * @param queryJsonString 查询条件
     * @return Map<String,Object>
     */
    @RequestMapping(value = "/list.do")
    @ResponseBody
    public Map<String, Object> list(String queryJsonString) {
        Map<String, Object> map = new HashMap<>(3);
        if (!StringUtils.isEmptyString(queryJsonString)) {
            map = JSON.parseObject(queryJsonString);
        }
        PageTbl page = this.getPage();
        PageUtils utils = agvTaskLogService.getPageList(page, map);
        page.setTotalRows(utils.getTotalCount() == 0 ? 1 : utils.getTotalCount());
        //初始化分页对象
        executePageMap(map, page);
        map.put("rows", utils.getList());
        map.put("total", utils.getTotalPage() == 0 ? 1 : utils.getTotalPage());
        return map;
    }

    /**
     * agv日志导出Excel
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     */
    @RequestMapping(value = "/materialExcel", method = RequestMethod.POST)
    @ResponseBody
    public void materialExcel(HttpServletRequest request, HttpServletResponse response){
        try {
            String sheetName = "agv任务" + "(" + DateUtils.getDay() + ")";
            response.setHeader("Content-Type", "application/force-download");
            response.setHeader("Content-Type", "application/vnd.ms-excel");
            response.setCharacterEncoding("UTF-8");
            response.setHeader("Expires", "0");
            response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
            response.setHeader("Pragma", "public");
            response.setHeader("Content-disposition", "attachment;filename=" + new String(sheetName.getBytes("gbk"),
                    "ISO8859-1") + ".xls");

            Map<String, String> mapFields = new LinkedHashMap<>();
            String [] excelIndexArray = request.getParameter("queryExportExcelIndex").split(",");

            if (excelIndexArray.length==1&&"".equals(excelIndexArray[0])) {
                mapFields.put("taskNo", "AGV任务号");
                mapFields.put("agvState", "状态");
                mapFields.put("agvTimeStr", "调用时间");
            }else {
                for (String s : excelIndexArray) {
                    if ("2".equals(s)) {
                        mapFields.put("taskNo", "AGV任务号");
                    } else if ("3".equals(s)) {
                        mapFields.put("agvState", "状态");
                    } else if ("4".equals(s)) {
                        mapFields.put("agvTimeStr", "调用时间");
                    }
                }
            }

            DeriveExcel.exportExcel(sheetName, agvTaskLogService.getExcelList(request.getParameter("ids"), request.getParameter("queryTaskNo")), mapFields, response, "");
            logService.logInsert("AGV日志导出", LogActionConstant.USER_EXPORT, request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
