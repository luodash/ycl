package com.tbl.modules.agv.controller.system;

import com.alibaba.fastjson.JSON;
import com.tbl.common.utils.*;
import com.tbl.modules.agv.service.agvtask.AgvTaskService;
import com.tbl.modules.platform.constant.LogActionConstant;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.service.system.LogService;
import com.tbl.modules.platform.service.system.RoleService;
import com.tbl.modules.platform.util.DeriveExcel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

import static com.tbl.modules.platform.constant.MenuConstant.agvTask;

/**
 * agv任务表控制层
 * @author zxf
 */
@RestController
@RequestMapping(value = "/agvTask")
public class AgvTaskController extends AbstractController {
    /**
     * 角色
     */
    @Autowired
    private RoleService roleService;
    /**
     * agv任务
     */
    @Autowired
    private AgvTaskService agvTaskService;
    /**
     * 日志信息
     */
    @Autowired
    private LogService logService;

    /**
     * 跳转agv任务列表
     * @return MpdelAndView
     */
    @RequestMapping(value = "/toList")
    @ResponseBody
     public ModelAndView toList() {
        ModelAndView mv = this.getModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(getSessionUser().getRoleId(), agvTask));
        mv.setViewName("agv/agvTask/agv_list");
        return mv;
    }

    /**
     * 原材料配送任务列表
     * @return MpdelAndView
     */
    @RequestMapping(value = "/toDeliveryAddList")
    @ResponseBody
    public ModelAndView toDeliveryAddList() {
        ModelAndView mv = this.getModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(getSessionUser().getRoleId(), agvTask));
        mv.setViewName("agv/agvTask/deliveryAdd_list");
        return mv;
    }
    /**
     * 原材料退料
     * @return MpdelAndView
     */
    @RequestMapping(value = "/toReturnAddList")
    @ResponseBody
    public ModelAndView toReturnAddList() {
        ModelAndView mv = this.getModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(getSessionUser().getRoleId(), agvTask));
        mv.setViewName("agv/agvTask/returnAdd_list");
        return mv;
    }
    /**
     * 退空托架
     * @return MpdelAndView
     */
    @RequestMapping(value = "/toReturnBracketList")
    @ResponseBody
    public ModelAndView toReturnBracketList() {
        ModelAndView mv = this.getModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(getSessionUser().getRoleId(), agvTask));
        mv.setViewName("agv/agvTask/returnBracket_list");
        return mv;
    }

    /**
     * 退固废
     * @return MpdelAndView
     */
    @RequestMapping(value = "/toReturnSolidWasteList")
    @ResponseBody
    public ModelAndView toReturnSolidWasteList() {
        ModelAndView mv = this.getModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(getSessionUser().getRoleId(), agvTask));
        mv.setViewName("agv/agvTask/returnSolidWaste_list");
        return mv;
    }

    /**
     * 空具盘转移
     * @return MpdelAndView
     */
    @RequestMapping(value = "/toEmptyDiskList")
    @ResponseBody
    public ModelAndView toEmptyDiskList() {
        ModelAndView mv = this.getModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(getSessionUser().getRoleId(), agvTask));
        mv.setViewName("agv/agvTask/emptyDisk_list");
        return mv;
    }

    /**
     * 获取agv任务列表数据
     * @param queryJsonString 查询条件
     * @return Map<String,Object>
     */
    @RequestMapping(value = "/list.do")
    @ResponseBody
    public Map<String, Object> list(String queryJsonString) {
        Map<String, Object> map = new HashMap<>(3);
        if (!StringUtils.isEmptyString(queryJsonString)) {
            map = JSON.parseObject(queryJsonString);
        }
        PageTbl page = this.getPage();
        PageUtils utils = agvTaskService.getPageList(page, map);
        page.setTotalRows(utils.getTotalCount() == 0 ? 1 : utils.getTotalCount());
        //初始化分页对象
        executePageMap(map, page);
        map.put("rows", utils.getList());
        map.put("total", utils.getTotalPage() == 0 ? 1 : utils.getTotalPage());
        return map;
    }


    /**
     * 获取agv任务列表数据
     * @param queryJsonString 查询条件
     * @return Map<String,Object>
     */
    @RequestMapping(value = "/list2.do")
    @ResponseBody
    public Map<String, Object> list2(String queryJsonString) {
        Map<String, Object> map = new HashMap<>(3);
        if (!StringUtils.isEmptyString(queryJsonString)) {
            map = JSON.parseObject(queryJsonString);
        }
        PageTbl page = this.getPage();
        PageUtils utils = agvTaskService.getPageList(page, map);
        page.setTotalRows(utils.getTotalCount() == 0 ? 1 : utils.getTotalCount());
        //初始化分页对象
        executePageMap(map, page);
        map.put("rows", utils.getList());
        map.put("total", utils.getTotalPage() == 0 ? 1 : utils.getTotalPage());
        return map;
    }



    /**
     * agv任务导出Excel
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     */
    @RequestMapping(value = "/materialExcel", method = RequestMethod.POST)
    @ResponseBody
    public void materialExcel(HttpServletRequest request, HttpServletResponse response){
        try {
            String sheetName = "agv任务" + "(" + DateUtils.getDay() + ")";
            response.setHeader("Content-Type", "application/force-download");
            response.setHeader("Content-Type", "application/vnd.ms-excel");
            response.setCharacterEncoding("UTF-8");
            response.setHeader("Expires", "0");
            response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
            response.setHeader("Pragma", "public");
            response.setHeader("Content-disposition", "attachment;filename=" + new String(sheetName.getBytes("gbk"),
                    "ISO8859-1") + ".xls");

            Map<String, String> mapFields = new LinkedHashMap<>();
            String [] excelIndexArray = request.getParameter("queryExportExcelIndex").split(",");

            if (excelIndexArray.length==1&&"".equals(excelIndexArray[0])) {
                mapFields.put("bussinessType", "任务类型");
                mapFields.put("whNoFrom", "来源仓库号");
                mapFields.put("whNoTo", "目的仓库号");
                mapFields.put("sysName", "系统名称");
                mapFields.put("deviceName", "设备名称");
                mapFields.put("taskNo", "生成的AGV任务号");
                mapFields.put("batchNo", "批次号");
                mapFields.put("orderNo", "任务序号");
                mapFields.put("locationFrom", "来源暂存位");
                mapFields.put("locationTo", "目的暂存位");
                mapFields.put("priority", "优先级");
            }else {
                for (String s : excelIndexArray) {
                    if ("2".equals(s)) {
                        mapFields.put("bussinessType", "任务类型");
                    } else if ("3".equals(s)) {
                        mapFields.put("whNoFrom", "来源仓库号");
                    } else if ("4".equals(s)) {
                        mapFields.put("whNoTo", "目的仓库号");
                    } else if ("5".equals(s)) {
                        mapFields.put("sysName", "系统名称");
                    } else if ("6".equals(s)) {
                        mapFields.put("deviceName", "设备名称");
                    } else if ("7".equals(s)) {
                        mapFields.put("taskNo", "生成的AGV任务号");
                    } else if ("8".equals(s)) {
                        mapFields.put("batchNo", "批次号");
                    } else if ("9".equals(s)) {
                        mapFields.put("orderNo", "任务序号");
                    } else if ("10".equals(s)) {
                        mapFields.put("locationFrom", "来源暂存位");
                    } else if ("11".equals(s)) {
                        mapFields.put("locationTo", "目的暂存位");
                    } else if ("12".equals(s)) {
                        mapFields.put("priority", "优先级");
                    }
                }
            }

            DeriveExcel.exportExcel(sheetName, agvTaskService.getExcelList(request.getParameter("ids"), request.getParameter("queryWhNoFrom"),
                    request.getParameter("queryWhNoTo")), mapFields, response, "");
            logService.logInsert("库区导出", LogActionConstant.USER_EXPORT, request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
