package com.tbl.modules.wms.dao.move;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tbl.modules.wms.entity.move.MoveOutStorage;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 移库出库持久层
 * @author 70486
 */
public interface MoveOutStorageDAO extends BaseMapper<MoveOutStorage> {

    /**
     * 移库出库表批量插入
     * @param lstMoveOutStorage 出库单明细集合
     * @return int 批量插入条数
     */
    int insertMoveOutstorageBatch(@Param("lstMoveOutStorage") List<MoveOutStorage> lstMoveOutStorage);

}
