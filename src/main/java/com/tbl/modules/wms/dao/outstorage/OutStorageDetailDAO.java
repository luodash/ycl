package com.tbl.modules.wms.dao.outstorage;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.tbl.modules.wms.entity.outstorage.OutStorageDetail;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 出库详情持久层
 * @author 70486
 */
public interface OutStorageDetailDAO extends BaseMapper<OutStorageDetail> {

	/**
	 * 批量修改出库详情
	 * @param lstOutStorageDetail
	 */
	void updateOutstorageDetailBatches(@Param("lstOutStorageDetail") List<OutStorageDetail> lstOutStorageDetail);

	/**
	 * 获取详细数据
	 * @param page
	 * @param params 
	 * @return OutStorageDetail
	 */
	List<OutStorageDetail> selectMixedList(Pagination page,Map<String,Object> params);
	
	  /**
     * 获取导出列
     * @param map
     * @return List<OutStorageDetail>
     * */
    List<OutStorageDetail> getAllLists(Map<String,Object> map);
    
    /**
     * 查询人员正常出库量
     * @param date
     * @return List<OutStorageDetail>
     */
    List<OutStorageDetail> findList(@Param("date") Date date);

    /**
     * 查询出库明细
     * @param org
     * @param carCode
     * @return List<OutStorageDetail>
     */
    List<OutStorageDetail> selectByFactoryCode(@Param("org") List<String> org,@Param("carCode")String carCode);
    
    /**
     * 根据pid获取该出库单下的所有出库明细的出库状态
	 * @param pid
     * @return String
     */
    String getState(@Param("pid") Long pid);

	/**
	 * 出库单明细表批量插入
	 * @param lstOutstorageDetail 出库单明细集合
	 * @return int 批量插入条数
	 */
	int insertOutStorageDetailBatch(@Param("lstOutstorageDetail") List<OutStorageDetail> lstOutstorageDetail);

	/**
	 * 获取发货单主界面所有出库单中，处于选择的厂区、仓库、仓库区域下的出库详情数量
	 * @param outStorageIdList 发货单主键集合
	 * @param factoryCode 厂区
	 * @param warehouseCode 仓库
	 * @return int 总数数量
	 */
	int getOutstorageDetailTotalNum(@Param("outStorageIdList") List<Long> outStorageIdList,@Param("factoryCode") String factoryCode,
									@Param("warehouseCode") String warehouseCode);

	/**
	 * 获取发货单主界面所有出库单中，处于选择的厂区、仓库、仓库区域下的出库详情已完成数量
	 * @param outStorageIdList 发货单主键集合
	 * @param factoryCode 厂区
	 * @param warehouseCode 仓库
	 * @return int 总数数量
	 */
	int getOutstorageDetailCompleteNum(@Param("outStorageIdList") List<Long> outStorageIdList,@Param("factoryCode") String factoryCode,
									@Param("warehouseCode") String warehouseCode);

	/**
	 * 获取能够批量出库的出库详情
	 * @param lstIds 出库单详情主键
	 * @return OutStorageDetail 出库明细
	 */
	List<OutStorageDetail> findBatchOutstorageDetail(@Param("lstIds") List<Long> lstIds);

	/**
	 * 根据车牌号在EBS数据库查询发货单及明细数据
	 * @param page 分页
	 * @param map 查询体条件
	 * @return OutStorageDetail
	 */
	List<OutStorageDetail> findOutstorageDetailByCarNo(Page page, Map<String, Object> map);

	/**
	 * 根据发货明细主键查询明细信息以及对应发货单号
	 * @param ids 提货单明细主键
	 * @return List<OutStorageDetail>
	 */
	List<OutStorageDetail> getOutStorageDetailAndShipNoListByOutstorageDetailId(@Param("ids") List<Long> ids);

	/**
	 * 根据发货明细主键获取要删除的删除离线接口主键
	 * @param ids 提货单明细主键
	 * @return List<Long>
	 */
	List<Long> getCancelInterfaceDoIds(@Param("ids") List<Long> ids);

	/**
	 * 批量更新出库详情状态
	 * @param lstIds
	 * @param state
	 */
	void updateDetailBatches(@Param("lstIds") List<Long> lstIds, @Param("state") Long state);

	/**
	 * 根据质保号/批次号/rfid，提货单号，获取发货单明细
	 * @param shipNo
	 * @param rfid
	 * @param barcode
	 * @param barcodeType
	 * @return OutStorageDetail
	 */
	List<OutStorageDetail> selectOutStorageDetailByShipNoAndId(@Param("shipNo") String shipNo, @Param("rfid") String rfid, @Param("barcode") String barcode,
															   @Param("barcodeType") String barcodeType);

	/**
	 * 根据rfid集合，提货单号，获取发货单明细集合
	 * @param map
	 * @return OutStorageDetail
	 */
	List<OutStorageDetail> selectOutStorageDetailByShipNoAndRfids(Map<String,Object> map);

	/**
	 * 拣货下架（划单）获取详细数据
	 * @param page
	 * @param params
	 * @return OutStorageDetail
	 */
	List<OutStorageDetail> getDetailPageList(Pagination page,Map<String,Object> params);
}
