package com.tbl.modules.wms.dao.allo;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tbl.modules.wms.entity.allo.AlloInStorage;

/**
 * 调拨入库管理持久层
 * @author 70486
 */
public interface AlloInStorageDAO extends BaseMapper<AlloInStorage> {
}
