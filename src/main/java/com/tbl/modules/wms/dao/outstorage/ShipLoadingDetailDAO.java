package com.tbl.modules.wms.dao.outstorage;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tbl.modules.wms.entity.outstorage.ShipLoadingDetail;

/**
 * 出库管理-装车发运明细
 * @author 70486
 */
public interface ShipLoadingDetailDAO extends BaseMapper<ShipLoadingDetail> {
}
