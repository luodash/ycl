package com.tbl.modules.wms.dao.instorage;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.tbl.modules.wms.entity.instorage.InstorageDetail;
import com.tbl.modules.wms.entity.inventory.InventoryRegistration;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 入库管理--订单详情
 * @author 70486
 */
public interface InstorageDetailDAO extends BaseMapper<InstorageDetail> {

    /**
     * 获取入库汇总列表
     * @param page
     * @param map
     * @return List<InstorageDetail>
     */
    List<InstorageDetail> getPageList(Page page, Map<String, Object> map);

    /**
     * 入库汇总表导出excel获取列表数据
     * @param map
     * @return List<InstorageDetail>
     */
    List<InstorageDetail> getAllLists(Map<String,Object> map);

    /**
     * 查询人员正常入库量
     * @param date
     * @return List<InstorageDetail>
     */
    List<InstorageDetail> findList(@Param("date") Date date);

    /**
     * 根据质保单号获取RFID绑定信息
     * @param qaCode
     * @return List<InstorageDetail>
     */
    List<InstorageDetail> getRfidBind(@Param("qaCode") String qaCode);
    
    /**
     * 获取外采入库列表信息
     * @param page
     * @param map
     * @return List<InstorageDetail>
     */
    List<InstorageDetail> getPagePackageList(Page page, Map<String, Object> map);
    
    /**
	 * 桌面读卡器绑定列表查询
	 * @param page
	 * @param map
	 * @return List<InstorageDetail>
	 */
    List<InstorageDetail> getList(Pagination page, Map<String, Object> map);

    /**
     * 获得导出列
     * @param page
     * @param map
     * @return InstorageDetail
     */
    List<InstorageDetail> getAllList(Pagination page, Map<String, Object> map);

    /**
     * 装包扫描根据唯一码查询
     * @param map 条件
     * @return InstorageDetail
     */
    List<InstorageDetail> selectList(Map<String, Object> map);

    /**
     * 查询明细导出列（包含父表数据）
     * @param ids 标签初始化表明细主键
     * @param infoid 标签初始化父表主键
     * @return InstorageDetail
     */
    List<InstorageDetail> selectInstorageAndDetail(@Param("ids") List<Long> ids, @Param("infoid") String infoid);

    /**
     * 根据rfid批量更新
     * @param mapList
     */
    void updateBatches(@Param("mapList") List<Map<String, Object>> mapList);

    /**
     * 根据rfid批量更新
     * @param lstInventoryRegistration 所有的对应ebs盘点单号下的已经盘点的库存(要上传的部分)
     */
    void updateInstorageDetailByRfid(@Param("lstInventoryRegistration") List<InventoryRegistration> lstInventoryRegistration);

    /**
     * 批量更新标签初始化米数
     * @param lstInstorageDetail
     */
    void updateMeterBatches(@Param("lstInstorageDetail") List<InstorageDetail> lstInstorageDetail);
}
