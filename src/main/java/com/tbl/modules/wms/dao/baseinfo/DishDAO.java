package com.tbl.modules.wms.dao.baseinfo;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.modules.wms.entity.baseinfo.Dish;
import org.apache.ibatis.annotations.Param;

/**
 * 盘具信息
 * @author 70486
 */
public interface DishDAO extends BaseMapper<Dish> {
	/**
	 * 获得导出列
	 * @param map
	 * @return dish
	 */
	List<Dish> getExcelList(Map<String, Object> map);

	/**
	 * 查询盘具列表信息
	 * @param page
	 * @param map
	 * @return Dish
	 */
    List<Dish> getPageList(Page page, Map<String, Object> map);

	/**
	 * 根据盘具编码查询盘具信息
	 * @param code
	 * @return Dish
	 */
	Dish getDish(String code);

	/**
	 * 获取盘具列表全部信息
	 * @param page
	 * @param code
	 * @return List<Dish>
	 */
    List<Dish> selectAll(Page page,@Param("code") String code);
}
