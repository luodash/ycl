package com.tbl.modules.wms.dao.interfacelog;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tbl.modules.wms.entity.interfacelog.InterfaceLog;
import org.apache.ibatis.annotations.Param;

/**
 * 接口日志记录
 * @author 70486
 */
public interface InterfaceLogDAO extends BaseMapper<InterfaceLog> {

    /**
     * 插入日志
     * @param code 接口编码
     * @param name 接口名称
     * @param requestString 请求报文
     * @param responseString 返回报文
     * @param qacode 质保号
     * @param batchNo 批次号
     */
    void insertLog(@Param("code") String code, @Param("name") String name, @Param("requestString") String requestString, @Param("responseString") String responseString,
                   @Param("qacode") String qacode, @Param("batchNo") String batchNo);
	
}
