package com.tbl.modules.wms.dao.baseinfo;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.modules.wms.entity.baseinfo.Area;

import java.util.List;
import java.util.Map;

/**
 * 库区列表
 * @author zxf
 */
public interface AreaDAO extends BaseMapper<Area> {
	/**
	 * 查询库区列表信息
	 * @param page
	 * @param map
	 * @return Dish
	 */
    List<Area> getPageList(Page page, Map<String, Object> map);

	/**
	 * 获取库区全部下拉列表
	 * @param map
	 * @return List<Map<String,Object>
	 */
	List<Map<String, Object>> selectAreaList(Map<String, Object> map);
	/**
	 * 获得库区Excel导出列表
	 * @param map 条件参数
	 * @return Shelf
	 */
	List<Area> getExcelList(Map<String, Object> map);
}
