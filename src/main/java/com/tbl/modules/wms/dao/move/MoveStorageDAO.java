package com.tbl.modules.wms.dao.move;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.modules.wms.entity.baseinfo.Car;
import com.tbl.modules.wms.entity.baseinfo.Shelf;
import com.tbl.modules.wms.entity.baseinfo.Warehouse;
import com.tbl.modules.wms.entity.move.MoveStorage;
import com.tbl.modules.wms.entity.move.MoveStorageDetail;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 移库处理持久层
 * @author 70486
 */
public interface MoveStorageDAO extends BaseMapper<MoveStorage> {

    /**
     * 移库单列表页数据
     * @param page
     * @param map
     * @return MoveStorage
     */
    List<MoveStorage> getPageList(Page page, Map<String, Object> map);

    /**
     * 根据主键获取到详情的列表
     * @param page
     * @param id 移库明细主键
     * @return MoveStorageDetail
     */
    List<MoveStorageDetail> getDetailList(Page page, Integer id);

    /**
     * 点击查看，获取详情列表页数据
     * @param id 移库单主键
     * @return MoveStorage 移库单
     */
    MoveStorage selectInfoById(@Param("id") Long id);

    /**
     * 获得导出Excel列表数据
     * @param map
     * @return List<MoveStorage>
     */
    List<MoveStorage> getExcelList(Map<String, Object> map);

    /**
     * 获取移库单最新流水号
     * @return int
     */
    int selectLatestAllocation();

    /**
     * 获取质保单号列表
     * @param page
     * @param map
     * @return Map<String, Object>
     */
    List<Map<String, Object>> getQaCode(Page<Map<String, Object>> page, Map<String, Object> map);

    /**
     * 移库正在移库进行中的物料数量
     * @param ids
     * @return int
     */
    int selectDetailsState(@Param("ids") List<Long> ids);

    /**
     * 获取调入仓库数据
     * @param page
     * @param map
     * @return Map<String, Object>
     */
    List<Map<String, Object>> selectWarehouse(Page page, Map<String, Object> map);

    /**
     * 获取绑定行车列表
     * @param warehouseCode 仓库编码
     * @param factoryList 厂区编码
     * @return List<Car> 行车下拉框中的选择
     */
    List<Car> selectCarListByWarehouseCode(@Param("warehouseCode") String warehouseCode,@Param("factoryList") List<String> factoryList);

    /**
     * 移库入库获取列表数据
     * @param page
     * @param map
     * @return MoveStorageDetail
     */
    List<MoveStorageDetail> getInDetailList(Page page, Map<String, Object> map);

    /**
     * 根据主键查询移库入库明细数据
     * @param id 移库单明细主键
     * @return MoveStorageDetail 移库单明细
     */
    MoveStorageDetail getDetailById(@Param("id") Long id);

    /**
     * 获得仓库列表
     * @param factoryCode 厂区编码
     * @return List<Warehouse> 仓库列表
     */
    List<Warehouse> getWarehouseList(@Param("factoryCode") String factoryCode);

    /**
     * 获得行车列表
     * @param id 仓库主键
     * @return List<Car> 行车列表
     */
    List<Car> getCarList(@Param("id") Long id);

    /**
     * 获取仓库下未被占用的库位
     * @param code 仓库编码
     * @param factoryCode 厂区编码
     * @return String Shelf
     */
    List<Shelf> getRecommendShelf(@Param("code") String code,@Param("factoryCode") String factoryCode);

    /**
     * 下拉框选择库位
     * @param page
     * @param map
     * @return Map<String, Object>
     */
    List<Map<String, Object>> selectUnbindShelf(Page page, Map<String, Object> map);

    /**
     * 入库确认-保存
     * @param id 移库单明细主键
     * @param code 库位编码
     * @param userId 用户主键
     * @return boolean
     */
    int confirmInstorage(@Param("id") Long id, @Param("code") String code, @Param("userId") Long userId);

    /**
     * 获取excel导出列表
     * @param map
     * @return List<MoveStorageDetail>
     */
    List<MoveStorageDetail> getMoveInExcelList(Map<String, Object> map);

    /**
     * 更新移库单、调拨单流水号
     * @param serial 流水号
     * @param type 类型：移库，调拨
     */
    void updateSerial(@Param("serial") String serial, @Param("type") String type);
}
