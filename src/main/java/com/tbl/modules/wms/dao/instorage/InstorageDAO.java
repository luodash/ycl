package com.tbl.modules.wms.dao.instorage;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.modules.wms.entity.baseinfo.Car;
import com.tbl.modules.wms.entity.baseinfo.Shelf;
import com.tbl.modules.wms.entity.baseinfo.Warehouse;
import com.tbl.modules.wms.entity.instorage.Instorage;
import com.tbl.modules.wms.entity.instorage.InstorageDetail;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 入库管理--订单
 * @author 70486
 */
public interface InstorageDAO extends BaseMapper<Instorage> {

    /**
     * 订单展示----获取列表数据
     * @param page 分页工具类
     * @param map 参数条件
     * @return PageUtils
     */
    List<Instorage> getPageList(Page page, Map<String, Object> map);

    /**
     * 订单展示----获取详情列表数据
     * @param page 分页工具类
     * @param map 参数条件
     * @return PageUtils
     */
    List<InstorageDetail> getPageDetgailList(Page page, Map<String, Object> map);

    /**
     * 装包管理----获取详情列表数据
     * @param page 分页工具类
     * @param map 参数条件
     * @return PageUtils
     */
    List<InstorageDetail> getPackDetailPageList(Page page, Map<String, Object> map);

    /**
     * 装包管理----点击确认改变状态
     * @param lstInstorageDetail 入库明细
     * @return int
     */
    int packDetailConfirm(@Param("lstInstorageDetail") List<InstorageDetail> lstInstorageDetail);

    /**
     * 装包管理----导出
     * @param map 条件参数
     * @return List<InstorageDetail>
     */
    List<InstorageDetail> getDetailExcelList(Map<String, Object> map);

    /**
     * 吊装台账管理----导出
     * @param map 条件参数
     * @return List<InstorageDetail>
     */
    List<InstorageDetail> getAccountHoistExcelList(Map<String, Object> map);

    /**
     * 包装台账管理----导出
     * @param map 条件参数
     * @return List<InstorageDetail>
     */
    List<InstorageDetail> getAccountPackingExcelList(Map<String, Object> map);

    /**
     * 入库登记查询数据列表
     * @param page 实现分页辅助
     * @param map 参数条件
     * @return InstorageDetail
     */
    List<InstorageDetail> getPagePackageList(Page page, Map<String, Object> map);

    /**
     * 入库----获取仓库列表
     * @param codeList 仓库编码
     * @return List<Warehouse> 仓库列表
     */
    List<Warehouse> getWarehouseList(@Param("codeList") List<String> codeList);

    /**
     * 入库----获取到推荐库位
     * @param code 仓库编码
     * @param factoryCode 厂区编码
     * @return String 库位编码
     */
    List<Shelf> getRecommendShelf(@Param("code") String code,@Param("factoryCode") String factoryCode);

    /**
     * 入库----根据详情主键获取详情信息
     * @param id 标签初始化表主键
     * @return InstorageDetail
     */
    InstorageDetail getDetailById(@Param("id") Long id);

    /**
     * 入库----获取仓库下的行车列表
     * @param id 仓库主键
     * @param org 厂区编码
     * @return List<Car> 行车列表
     */
    List<Car> getCarList(@Param("id") Long id,@Param("org") String org);

    /**
     * 入库----获取仓库下的行车列表
     * @param factoryCode 厂区编码
     * @return List<Car> 行车列表
     */
    List<Car> findCarList(@Param("factoryCode") String factoryCode);

    /**
     * 入库----入库确认获取选择的库位列表
     * @param page 分页工具页面类
     * @param map 条件参数
     * @return PageUtils
     */
    List<Map<String, Object>> selectUnbindShelf(Page page, Map<String, Object> map);

    /**
     * 手动入库----获取选择的库位列表
     * @param page 分页工具页面类
     * @param map 参数条件
     * @return PageUtils
     */
    List<Map<String, Object>> selectManualShelf(Page page, Map<String, Object> map);

    /**
     * 入库----导出
     * @param map 条件参数
     * @return List<InstorageDetail>
     */
    List<InstorageDetail> getPackageInExcelList(Map<String, Object> map);

    /**
     * 根据批次号查询标签初始化数据
     * @param batchNo 批次号
     * @return InstorageDetail
     */
    InstorageDetail selectByBatch(@Param("batchNo") String batchNo);

}
