package com.tbl.modules.wms.dao.jobs;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.poi.ss.formula.functions.T;

/**
 * 定时任务持久层
 * @author 70486
 */
public interface JobsDAO extends BaseMapper<T> {

}
