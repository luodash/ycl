package com.tbl.modules.wms.dao.move;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tbl.modules.wms.entity.move.MoveStorageDetail;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 移库处理-详情
 * @author 70486
 */
public interface MoveStorageDetailDAO extends BaseMapper<MoveStorageDetail> {

    /**
     * 查询导出Excel列表
     * @param map
     * @return List<MoveStorageDetail>
     */
    List<MoveStorageDetail> getDetailExcelList(Map<String, Object> map);

    /**
     * 根据移库单号查询单号下所有详情的移库状态
     * @param idList
     * @return List<Long>
     */
    List<Long> getStatesByMoveStorageId(@Param("idList") List<Long> idList);

    /**
     * 根据移库单号查询单号下所有详情的质保单号
     * @param idList
     * @return List<Long>
     */
    List<String> getQacodesByMoveStorageId(@Param("idList") List<Long> idList);
}
