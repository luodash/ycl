package com.tbl.modules.wms.dao.storageinfo;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.tbl.modules.wms.entity.instorage.InstorageDetail;
import com.tbl.modules.wms.entity.storageinfo.StorageInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 库存持久层
 * @author 70486
 */
public interface StorageInfoDAO extends BaseMapper<StorageInfo> {

    /**
     * 获取导出列
     * @param map
     * @return List<StorageInfo>
     */
    List<StorageInfo> getAllLists(Map<String,Object> map);

    /**
     * 获取导出列
     * @param qaCode
     * @param lstIds
     * @return List<StorageInfo>
     */
    List<InstorageDetail> getAllLists2(@Param("qaCode") String qaCode, @Param("lstIds") List<Long> lstIds);

    /**
     * 查询库龄满多少年
     * @param qaCode
     * @return List<StorageInfo>
     */
    List<StorageInfo> getRfidBind(@Param("qaCode") String qaCode);

    /**
     * 根据rfid查询库存数据
     * @param map
     * @return List<StorageInfo>
     */
    List<StorageInfo> selectByMapInfo(Map<String,Object> map);
    
    /**
     * 退库回生产线 分页列表数据
     * @param page
     * @param map
     * @return List<StorageInfo>
     */
    List<StorageInfo> getReturnStockList(Pagination page, Map<String,Object> map);
    
    /**
     * 库存查询 分页列表数据
     * @param page
     * @param map
     * @return List<StorageInfo>
     */
    List<StorageInfo> getStorageInfoList(Pagination page, Map<String,Object> map);

    /**
     * 批量插入
     * @param lstStorageInfo
     * @return int
     */
    int insertScrapBatchesStorageInfo(@Param("lstStorageInfo") List<StorageInfo> lstStorageInfo);

    /**
     * 批量更新库存米数
     * @param lstStorageInfo
     */
    void updateScrapBatchesStorageInfo(@Param("lstStorageInfo") List<StorageInfo> lstStorageInfo);

    /**
     * 根据质保号获取库存主键字符拼接
     * @param qacodes 质保号数组
     * @return String 库存主键字符（1，2，3，4，5，6）
     * */
    String selectStorageInfoIdsByQacode(@Param("qacodes") String [] qacodes);

    /**
     * 根据rfid批量更新
     * @param mapList
     */
    void updateBatches(@Param("mapList") List<Map<String, Object>> mapList);

    /**
     * 根据库位编码查询库存信息
     * @param factoryCode 厂区
     * @param warehouseCode 仓库
     * @param shelfCode 库位
     * @return StorageInfo
     * */
    List<StorageInfo> getStorageInfoByShelf(@Param("factoryCode") String factoryCode, @Param("warehouseCode") String warehouseCode, @Param("shelfCode") String shelfCode);

    /**
     * 根据wms盘点单号主键修改库存米数，把库存米数更新为盘点米数
     * @param lstPlanId EBS盘点单号对应的多个wms盘点单主键集合
     * @param failQacodes EBS处理失败的质保号
     */
    void updateMeterByInventory(@Param("lstPlanId") List<String> lstPlanId, @Param("failQacodes") List<String> failQacodes);

    /**
     * 找出这个集结号下的所有上传米数盘点的数据中执行成功的,来对库存表进行更新
     * @param specialSign	集结号
     * @param failQacodes	失败的质保号
     */
    void updateStorageInfoMeterByInventoryRegistration(@Param("specialSign") Long specialSign, @Param("failQacodes") List<String> failQacodes);
}
