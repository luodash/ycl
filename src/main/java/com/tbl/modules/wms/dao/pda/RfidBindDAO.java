package com.tbl.modules.wms.dao.pda;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tbl.modules.wms.entity.split.RfidBind;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * RFID绑定
 * @author 70486
 */
public interface RfidBindDAO extends BaseMapper<RfidBind> {

    /**
     * 根据质保号查询rfid绑定表
     * @param qaCode
     * @return List<RfidBind>
     */
    List<RfidBind> getRfidBind(@Param("qaCode") String qaCode);

    /**
     * 根据批次获取物料数据
     * @param batchno
     * @return Map<String,Object>
     */
    Map<String,Object> getMaterialInfo(@Param("batchno") String batchno);
}
