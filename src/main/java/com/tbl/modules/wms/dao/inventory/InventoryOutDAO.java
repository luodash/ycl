package com.tbl.modules.wms.dao.inventory;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tbl.modules.wms.entity.inventory.InventoryOut;

/**
 * 盘点出库审核
 * @author 70486
 */
public interface InventoryOutDAO extends BaseMapper<InventoryOut> {
	
}
