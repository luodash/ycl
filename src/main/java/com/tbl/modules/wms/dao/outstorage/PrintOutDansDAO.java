package com.tbl.modules.wms.dao.outstorage;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.poi.ss.formula.functions.T;

import java.util.List;
import java.util.Map;

/**
 *发货单打印数据控制层
 * @author 70486
 */
public interface PrintOutDansDAO extends BaseMapper<T> {

    /**
     *根据发货单获取出库打印表中该发货单信息
     * @param outno
     * @return Map<String,Object>
     */
    Map<String,Object> getOutPrintInfo(String outno);

    /**
     * 获取打印信息
     * @param params
     * @return Map<String,Object>
     */
    List<Map<String,Object>> getOutPrintInfoByList(Map<String,Object> params);

    /**
     *根据批次号获取打印单列表
     * @param params
     * @return Map<String,Object>
     */
    List<Map<String,Object>> getPrintListByBatchno(Map<String,Object> params);

    /**
     * 获取打印出库单得详情列表
     * @param id
     * @return Map<String,Object>
     */
    List<Map<String,Object>> getPrintOutDetail(String id);

}
