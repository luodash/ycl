package com.tbl.modules.wms.dao.outstorage;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.modules.wms.entity.outstorage.ExchangePrint;
import com.tbl.modules.wms.entity.printinfo.PrintInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 打印流转单持久层
 * @author 70486
 */
public interface ExchangeDAO extends BaseMapper<PrintInfo> {

    /**
     * 根据出库单号获取车牌信息
     * @param outno 发货单号
     * @param sTime 起始时间
     * @param eTime 截止日期
     * @return Map<String,String >
     */
    List<Map<String,String >> getCarList(@Param("outno") String  outno, @Param("sTime") String sTime, @Param("eTime") String eTime);

    /**
     * 根据车牌和出库单信息判断是否已存在表中
     * @param exchangePrint 流转单
     * @return Integer
     */
    Integer getCountByMap(@Param("exchangePrint") ExchangePrint exchangePrint);

    /**
     * 物资流转单列表页数据查询
     * @param map
     * @return ExchangePrint
     */
    ExchangePrint getInfo(Map<String,Object> map);

    /**
     * 物资流转单打印列表数据
     * @param page
     * @param map
     * @return Map<String,Object>
     */
    List<Map<String,Object>> getDataList(Page page, Map<String,Object> map);

    /**
     * 根据车牌及发货单验证是否存在
     * @param outno 出库单
     * @param carno 车牌
     * @return Integer
     */
    Integer validateTrue(@Param("outno") String outno, @Param("carno")String carno);

    /**
     * 获取厂区信息
     * @return Map<String,Object>
     */
    List<Map<String,Object>> getFactoryArea();

    /**
     * 获取最大的流转单号
     * @return Integer
     */
    Integer getMaxOddNumber();

    /**
     * 更新物资流转单号
     * @param outno 提货单号
     * @param carno 车牌号
     * @param number 物资流转单号
     */
    void updateOddNumber(@Param("outno") String outno, @Param("carno")String carno, @Param("number")String number);

    /**
     * 获取打印流转单数据列表
     * @param map
     * @return Map<String,Object>
     */
    List<Map<String,Object>> getPrintInfo(Map<String,Object> map);

    /**
     * 获取物资流转单
     * @param outno 发货单号
     * @param carno 车牌号
     * @param oddnumber 物资流转单
     * @return Map<String,Object>
     */
    List<Map<String,Object>> getgetExchangePrint(@Param("outno")String outno, @Param("carno") String carno, @Param("oddnumber")String oddnumber);


    /**
     * 获取最大的物资流转单号
     * @return
     */
    String getMaxWzlzCode();
}
