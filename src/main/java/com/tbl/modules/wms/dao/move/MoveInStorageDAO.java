package com.tbl.modules.wms.dao.move;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tbl.modules.wms.entity.move.MoveInStorage;

/**
 * 移库入库持久层
 * @author 70486
 */
public interface MoveInStorageDAO extends BaseMapper<MoveInStorage> {
}
