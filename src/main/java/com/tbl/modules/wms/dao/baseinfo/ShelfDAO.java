package com.tbl.modules.wms.dao.baseinfo;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.tbl.modules.wms.entity.baseinfo.Shelf;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 库位信息
 * @author 70486
 */
public interface ShelfDAO extends BaseMapper<Shelf> {

    /**
     * 库位列表分页获取列表数据
     * @param page 分页模型
     * @param map 条件参数
     * @return PageUtils
     */
    List<Shelf> getPageList(Pagination page, Map<String, Object> map);

    /**
     * 获得库位Excel导出列表
     * @param map 条件参数
     * @return Shelf
     */
    List<Shelf> getExcelList(Map<String, Object> map);

    /**
     * 库位占用率列表
     * @param page 分页模型
     * @param params 条件参数
     * @return Shelf
     */
    List<Shelf> getList(Pagination page,Map<String,Object> params);

    /**
     * 库位占用率图形
     * @return Shelf
     */
    List<Shelf> getLine();

    /**
     * 获取顶配推荐库位的前几位标识
     * @param factoryCode 厂区编码
     * @param warehouseCode 仓库编码
     * @param type 配置类型
     * @param outerDiameter 盘外经
     * @param drumType 盘类型
     * @param warehouseArea 仓库区域
     * @param superWide 是否超宽
     * @param coloured 是否有颜色
     * @return List<String> 库位前几位标识集合
     */
    List<String> selectRecommentTwoCode(@Param("factoryCode") String factoryCode,@Param("warehouseCode") String warehouseCode,
                                        @Param("type") String type, @Param("outerDiameter") String outerDiameter,
                                        @Param("drumType") String drumType, @Param("warehouseArea") List<String> warehouseArea,
                                        @Param("superWide") Integer superWide, @Param("coloured") Integer coloured);

    /**
     * 获取符合的规则标志
     * @param shelfTopTwoFirst 库位标志前2位
     * @return List<String> 库位前几位标识集合
     */
    List<String> selectAllTwoCode(@Param("shelfTopTwoFirst") List<String> shelfTopTwoFirst);

    /**
     * 获取推荐库位
     * @param shelfTopTwo 库位前几位标识集合
     * @param factoryCode 厂区编码
     * @param warehouseCode 仓库编码
     * @return List<Shelf> 库位列表
     */
    List<Shelf> selRemList(@Param("shelfTopTwo") List<String> shelfTopTwo,@Param("factoryCode") String factoryCode,
                           @Param("warehouseCode") String warehouseCode);

    /**
     * 获取库位表中未被使用的库位
     * @param code 仓库编码
     * @param factoryCode 厂区编码
     * @param warehouseArea 仓库区域
     * @return List<Shelf>
     */
    List<Shelf> getRecommendShelf(@Param("code") String code,@Param("factoryCode") String factoryCode,
                                  @Param("warehouseArea") List<String> warehouseArea);

    /**
     * 根据厂区和仓库获取对应的库位
     * @param page 分页辅助类
     * @param map 条件参数
     * @return List<Map<String, Object>>
     */
    List<Map<String, Object>> getShelfByFactoryWarehouse(Page page, Map<String, Object> map);

    /**
     * 根据厂区编码和仓库编码获取对应的库位
     * @param page 分页辅助类
     * @param map 条件参数
     * @return List<Map<String, Object>>
     */
    List<Map<String, Object>> getShelfByFactWareCode(Page page, Map<String, Object> map);

    /**
     * 库位批量插入
     * @param addList 库位列表
     * @return int
     */
    int insertBatches(@Param("addList") List<Shelf> addList);

    /**
     * 库位批量更新
     * @param updateList 库位列表
     */
    void updateBatches(@Param("updateList") List<Shelf> updateList);

    /**
     * 设置所有的库位状态为可用
     */
    void setAllStatesUseful();

    /**
     * 获取最靠近过道的库位
     * @param salesCode 营销经理编号
     * @param warehouseArea 仓库区域
     * @return Shelf 库位集合
     */
    List<Shelf> selectMostClosedShelfList(@Param("salesCode") String salesCode,@Param("warehouseArea") List<String> warehouseArea);

    /**
     * 推荐库位规则一：获取符合盘大小，盘类型的可用库位
     * @param factoryCode 厂区编码
     * @param warehouseCode 仓库编码
     * @param shelfTopTwoFirst 推荐库位规则表的前几位的标识
     * @return Shelf 库位集合
     */
    List<Shelf> selectRoleOneShelfList(@Param("factoryCode") String factoryCode, @Param("warehouseCode") String warehouseCode,
                                       @Param("shelfTopTwoFirst") List<String> shelfTopTwoFirst);

    /**
     * 推荐库位规则一：获取符合盘大小，盘类型的可用库位（次顶配）
     * @param factoryCode 厂区编码
     * @param warehouseCode 仓库编码
     * @param shelfTopTwoSecond 推荐库位规则表的前几位的标识（次顶配）
     * @return Shelf 库位集合
     */
    List<Shelf> selectRoleTwoShelfList(@Param("factoryCode") String factoryCode, @Param("warehouseCode") String warehouseCode,
                                       @Param("shelfTopTwoSecond") String shelfTopTwoSecond);

    /**
     * 根据仓库区域获取可用库位
     * @param warehouseAreaList 仓库区域列表
     * @return Shelf 库位集合
     */
    List<Shelf> findShelfByArea(@Param("warehouseAreaList") List<String> warehouseAreaList);

    /**
     * 根据行车编码获取所有库位
     * @param warehouseAreaList 仓库区域列表
     * @return Shelf 库位集合
     */
    List<Shelf> findAllShelfByArea(@Param("warehouseAreaList") List<String> warehouseAreaList);

    /**
     * 根据id查询库位信息
     * @param id 库位主键
     * @param factoryCode 厂区编码
     * @return Shelf 库位
     */
    Shelf findShelfById(@Param("id") Long id, @Param("factoryCode") String factoryCode);
}
