package com.tbl.modules.wms.dao.outstorage;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.tbl.modules.wms.entity.baseinfo.Car;
import com.tbl.modules.wms.entity.outstorage.OutStorage;
import com.tbl.modules.wms.entity.outstorage.OutStorageDetail;
import com.tbl.modules.wms.entity.outstorage.OutStoragePrint;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 出库信息持久层
 * @author 70486
 */
public interface OutstorageDAO extends BaseMapper<OutStorage> {

    /**
     * 发货单列表
     * @param page
     * @param map
     * @return OutStorage
     */
    List<OutStorage> getPageList(Page page, Map<String, Object> map);

    /**
     * 获取打印发货单列表数据
     * @param page
     * @param map
     * @return OutStoragePrint
     */
    List<OutStoragePrint> getPrintPageList(Pagination page, Map<String, Object> map);

    /**
     * 发货单执行列表
     * @param page
     * @param id
     * @return OutStorageDetail
     */
    List<OutStorageDetail> getDetailList(Pagination page, @Param("id") Integer id);

    /**
     * 获得质保单号(物料明细)
     * @param page
     * @param map
     * @return List<Map<String,Object>>
     */
    List<Map<String, Object>> getQaCode(Page<Map<String, Object>> page, Map<String, Object> map);

    /**
     * 获取对应厂区下的仓库下的行车
     * @param warehouseCode 仓库编码
     * @param org 厂区编码
     * @return List<Car> 行车列表
     */
    List<Car> selectCarListByWarehouseCode(@Param("warehouseCode") String warehouseCode,@Param("org") List<String> org);

    /**
     * 点击打印按钮，根据车牌号获取列表数据
     * @param page 页面封装类
     * @param wayno 运单号
     * @return OutStoragePrint
     */
    List<OutStoragePrint> getPrintPageList1(Pagination page,@Param("wayno") String wayno);

    /**
     * 出库单表批量插入
     * @param lstOutstorage 出库单集合
     * @return int 批量插入条数
     */
    int insertOutstorageBatch(@Param("lstOutstorage") List<OutStorage> lstOutstorage);

    /**
     * 根据输入的车牌号获取发货单(手持机)
     * @param carNumber 车牌号
     * @return List<Map<String,Object>> 发货单号集合
     */
    List<Map<String,Object>> getOutstorageListByCarNumber(@Param("carNumber") String carNumber);

    /**
     * 根据输入的提货单号获取车辆及货物信息
     * @param shipNo
     * @return Map<String,Object>
     */
    List<Map<String,Object>> getOutstorageListByCarNo(@Param("shipNo") String shipNo);

    /**
     * 获得导出excel列表
     * @param map
     * @return List<OutStorage>
     */
    List<OutStorage> getExcelList(Map<String, Object> map);

    /**
     * 获得查看明细导出excel列表
     * @param map
     * @return List<OutStorage>
     */
    List<OutStorageDetail> getDetailExcelList(Map<String, Object> map);

    /**
     * 根据提货单号查询所有的提货单以及对应的每个提货单下的物料出库状态ping连接
     * @param lstShipno 发货单号集合
     * @param factoryCode 厂区编码
     * @param warehouseCode 仓库编码
     * @return List<OutStorage>
     */
    List<OutStorage> selectOutstorageListAndDetailStateByShipNos(@Param("lstShipno") List<String> lstShipno, @Param("factoryCode") String factoryCode,
                                                                 @Param("warehouseCode") String warehouseCode);
}
