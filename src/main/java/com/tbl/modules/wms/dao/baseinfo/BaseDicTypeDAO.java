package com.tbl.modules.wms.dao.baseinfo;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tbl.modules.wms.entity.baseinfo.BaseDicType;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Map;

/**
 * 下拉框选择项持久层
 * @author 70486
 */
public interface BaseDicTypeDAO extends BaseMapper<BaseDicType> {

    /**
     * 获取入库状态全部下拉列表
     * @param map
     * @return List<Map<String,Object>>
     */
    List<Map<String, Object>> getInstorageTypeList(Map<String, Object> map);

    /**
     * 获取时间类型全部下拉列表
     * @param map
     * @return List<Map<String,Object>>
     */
    List<Map<String, Object>> getTimeTypeList(Map<String, Object> map);

    /**
     * 获取出库类型全部下拉列表
     * @param map
     * @return List<Map<String,Object>>
     */
    List<Map<String, Object>> getOutStorageTypeList(Map<String, Object> map);

    /**
     * 获取打印列表信息
     * @param map
     * @return List<Map<String,Object>>
     */
    List<Map<String, Object>> getPrinterHierarchyList(Map<String, Object> map);


    /**
     * 获取质检状态下拉列表
     * @return
     */
    List<Map<String,Object>> getQcDict();

    /**
     * 获取其它出入库事务类型
     * @param type
     * @return Map<String,Object>
     */
    List<Map<String,Object>> getOtherTransactionDict(@Param("type") Integer type);


    /**
     * 获取领用用途-下拉列表
     * @param id
     * @return Map<String,Object>
     */
    List<Map<String,Object>> getLyytDict(@PathVariable("id") Integer id);
}
