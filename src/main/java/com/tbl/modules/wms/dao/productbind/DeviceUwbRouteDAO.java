package com.tbl.modules.wms.dao.productbind;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tbl.modules.wms.entity.productbind.DeviceUwbRoute;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 行车坐标信息持久层
 * @author 70486
 */
public interface DeviceUwbRouteDAO extends BaseMapper<DeviceUwbRoute> {
    /**
     * 获取导出列
     * @param ids 勾选的行主键
     * @return List<DeviceUwbRoute> 导出列表
     * */
    List<DeviceUwbRoute> getAllLists(@Param("ids") List<Long> ids);
}
