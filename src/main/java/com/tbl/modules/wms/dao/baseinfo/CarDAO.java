package com.tbl.modules.wms.dao.baseinfo;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.tbl.modules.wms.entity.baseinfo.Car;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 行车持久层
 * @author 70486
 */
public interface CarDAO extends BaseMapper<Car> {

	/**
	 * 获取行车列表数据
	 * @param page
	 * @param map
	 * @return List<Car>
	 */
    List<Car> getPageList(Page page, Map<String, Object> map);

    /**
     * 获取导出Excel列表
     * @param map
     * @return List<Car>
     */
    List<Car> getExcelList(Map<String, Object> map);

    /**
     * 根据id查询行车信息
     * @param id 行车主键
     * @param factoryCode 厂区编码
     * @return Car 行车
     */
    Car findCarById(@Param("id") Long id,@Param("factoryCode") String factoryCode);

    /**
     * 获取最大的行车主键
     * @return Long
     */
    Long selectMaxCarId();
    
    /**
     * 功能描述：获取所有的仓库区域
     * @return Map<String,Object>
     */
    List<Map<String,Object>> selectWarehouseArea();

    /**
     * 功能描述：更新行车状态
     * @param carCode 行车编码
     * @param missionShelf 目标库位
     * @param state 行车状态
     */
    void updateCarByCode(@Param("carCode") String carCode, @Param("missionShelf") String missionShelf, @Param("state") Long state );

    /**
     * 获取行吊台账数据
     * @param page
     * @param params
     * @return OutStorageDetail
     */
    List<Map<String,Object>> getHangCarList(Pagination page, Map<String,Object> params);

    /**
     * 获取行吊台账数据Excel
     * @param params
     * @return OutStorageDetail
     */
    List<Map<String,Object>> getHangCarExcelList(Map<String,Object> params);

}
