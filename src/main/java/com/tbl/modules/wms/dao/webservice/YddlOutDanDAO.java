package com.tbl.modules.wms.dao.webservice;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tbl.modules.wms.entity.printinfo.PrintInfo;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * 出库货单打印
 * @author 70486
 */
public interface YddlOutDanDAO extends BaseMapper<PrintInfo> {

    /**
     * 保存打印单据
     * @param map
     * @return Integer
     */
    Integer saveYddlPrintDan(Map<String,Object> map);

    /**
     * 保存打印单据明细信息
     * @param map
     */
    void savePrintDetail(Map<String,Object> map);

    /**
     * 获取数量
     * @param outno
     * @param car
     * @return Integer
     */
    Integer selectInfoById(@Param("outno") String outno, @Param("car")String car);
}
