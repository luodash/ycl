package com.tbl.modules.wms.dao.outstorage;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.modules.wms.entity.outstorage.ShipLoading;
import com.tbl.modules.wms.entity.outstorage.ShipLoadingDetail;

import java.util.List;
import java.util.Map;

/**
 * 出库管理-装车发运
 * @author 70486
 */
public interface ShipLoadingDAO extends BaseMapper<ShipLoading> {

    /**
     * 装车发运列表页数据
     * @param page
     * @param map
     * @return ShipLoading
     */
    List<ShipLoading> getPageList(Page page, Map<String, Object> map);

    /**
     * 详情的列表
     * @param page
     * @param id 装车发运表主键
     * @return ShipLoadingDetail
     */
    List<ShipLoadingDetail> getDetailList(Page page, Integer id);

    /**
     * 导出excel列表
     * @param map 查询条件
     * @return List<ShipLoading>
     */
    List<ShipLoading> getExcelList(Map<String, Object> map);

    /**
     * 编辑页面  获取质保单号列表
     * @param page
     * @param map
     * @return Map<String, Object>
     */
    List<Map<String, Object>> getQaCode(Page<Map<String, Object>> page, Map<String, Object> map);
}
