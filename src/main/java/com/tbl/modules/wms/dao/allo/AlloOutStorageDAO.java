package com.tbl.modules.wms.dao.allo;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tbl.modules.wms.entity.allo.AlloOutStorage;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 调拨出库持久层
 * @author 70486
 */
public interface AlloOutStorageDAO extends BaseMapper<AlloOutStorage> {

    /**
     * 调拨出库表批量插入
     * @param lstalloOutstorage 出库单明细集合
     * @return int 批量插入条数
     */
    int insertAlloOutstorageBatch(@Param("lstalloOutstorage") List<AlloOutStorage> lstalloOutstorage);

}
