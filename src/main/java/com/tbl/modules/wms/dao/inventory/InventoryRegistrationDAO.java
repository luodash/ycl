package com.tbl.modules.wms.dao.inventory;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.modules.wms.entity.instorage.InstorageDetail;
import com.tbl.modules.wms.entity.inventory.Inventory;
import com.tbl.modules.wms.entity.inventory.InventoryRegistration;
import com.tbl.modules.wms.entity.storageinfo.StorageInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 盘点登记
 * @author 70486
 */
public interface InventoryRegistrationDAO extends BaseMapper<InventoryRegistration> {
    /**
     * 详情页面
     * @param page
     * @param map
     * @return List<InventoryRegistration>
     */
    List<InventoryRegistration> getPageList(Page page, Map<String, Object> map);

    /**
     * 盘点审核页面
     * @param page
     * @param param
     * @return List<StorageInfo>
     */
    List<StorageInfo> getContrastList(Page page, Map<String, Object> param);

    /**
     * 获得自增id
     * @return Long
     */
    Long getSequence();

    /**
     * 获得最大盘点编号
     * @return String
     */
    String getMaxCode();

    /**
     * 更新盘点计划状态
     * @param planid
     * @param state
     */
    void updateExamStatus(@Param("planid") Integer planid,@Param("state") Long state);

    /**
     * 根据rfid 去库存表查询是否存在
     * @param rfid
     * @param org
     * @param warehouseArr
     * @return Integer 
     */
    Integer getStorageInfoByRfid(@Param("rfid") String rfid, @Param("org") String org, @Param("warehouseArr") List<String> warehouseArr);

    /**
     * 根据盘点传入得rfid列表信息与库存对比，查出库存中多出得rfid信息
     * @param listArr
     * @param org
     * @param warehouseArr
     * @return List<InventoryRegistration>
     */
    List<InventoryRegistration> getRegComperStockByArr(@Param("listArr") List<String> listArr, @Param("org")String org, @Param("warehouseArr") List<String> warehouseArr);

    /**
     * 根据rfid获取盘点表得明细信息
     * @param rfid
     * @param ebsCode ebs盘点单号
     * @return InventoryRegistration
     */
    InventoryRegistration getRegistrationInfoByRfid(@Param("rfid") String rfid, @Param("ebsCode") String ebsCode);
    
    /**
	 * 查询盘点单下的明细的所有状态（是否已经全部审核成功）
	 * @param planid
	 * @return String
	 */
	String selectExamineArray(@Param("planid") Integer planid);

    /**
     * 盘点登记批量更新
     * @param lstUpdate
     */
    void updateBatches(@Param("lstUpdate") List<InventoryRegistration> lstUpdate);

    /**
     * 盘点登记批量新增
     * @param lstAdd
     */
    void insertBatches(@Param("lstAdd") List<InventoryRegistration> lstAdd);

    /**
     * 根据提交上来的线盘的rfid，在库存表或标签表查询基础属性，以及盘点结果
     * @param mapList
     * @return InstorageDetail
     */
    List<InstorageDetail> selectInfoInventoryStateList(@Param("mapList") List<Map<String, Object>> mapList);

    /**
     * 查询明细导出列（包含父表数据）
     * @param ids 库存主键
     * @param factoryCodeList 盘点计划主键
     * @return StorageInfo
     */
    List<StorageInfo> selectInventoryDifference(@Param("ids") List<Long> ids, @Param("factoryCodeList") List<String> factoryCodeList);

    /**
     * 找出这个集结号下的所有上传米数盘点的数据中执行成功的,来对盘点登记表进行更新
     * @param specialSign	集结号
     * @param failQacodes	失败的质保号
     */
    void updateInventoryMeter(@Param("specialSign") Long specialSign, @Param("failQacodes") List<String> failQacodes);

    /**
     * 批量更新盘点表库位
     * @param lstStorageInfo
     */
    void updateShelfByInventory(@Param("lstStorageInfo") List<StorageInfo> lstStorageInfo);

    /**
     * 更新盘点库位审核状态
     * @param qaCode
     * @param shelfState
     * @param lstInventory
     */
    void updateShelfState(@Param("qaCode") String qaCode, @Param("shelfState") Integer shelfState, @Param("lstInventory") List<Inventory> lstInventory);
}
