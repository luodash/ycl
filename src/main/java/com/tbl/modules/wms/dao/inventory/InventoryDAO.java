package com.tbl.modules.wms.dao.inventory;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.tbl.modules.wms.entity.storageinfo.StorageInfo;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.tbl.modules.wms.entity.inventory.Inventory;

/**
 * 盘点计划
 * @author 70486
 */
public interface InventoryDAO extends BaseMapper<Inventory> {
	
	/**
	 * 查询盘点计划数据列表
	 * @param page
	 * @param params
	 * @return List<Inventory>
	 */
	List<Inventory> selectInventoryList(Pagination page, Map<String,Object> params);

	/**
	 * 查询盘点审核数据列表
	 * @param page
	 * @param params
	 * @return List<Inventory>
	 */
	List<Inventory> selectInventoryReviewList(Pagination page, Map<String,Object> params);

	/**
	 * 获得自增id
	 * @return Long
	 */
	Long getSequence();
	
	/**
	 * 获得最大盘点编号
	 * @return int
	 */
	int getMaxCode();
	
	/**
     * 获取导出列
	 * @param map
     * @return StorageInfo
     */
	List<StorageInfo> getAllLists(Map<String, Object> map);

	/**
	 * 获取厂区列表信息
	 * @param list
	 * @return Map<String,Object>
	 */
	List<Map<String,Object>> getFactoryList(@Param("list") List<String> list);

	/**
	 * 根据用户ID获取用户姓名
	 * @param list
	 * @return String
	 */
	List<String> getUserNameByUserId(@Param("list") List<String> list);

	/**
	 * 确认审核日期
	 * @param ebsCode
	 * @param date
	 */
	void confirmAuditTime(@Param("ebsCode") String ebsCode, @Param("date") Date date);
}
