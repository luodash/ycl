package com.tbl.modules.wms.dao.baseinfo;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tbl.modules.wms.entity.baseinfo.RealLocation;

/**
 * 真实库位坐标记录持久层
 * @author 70486
 */
public interface RealLocationDAO extends BaseMapper<RealLocation> {

}
