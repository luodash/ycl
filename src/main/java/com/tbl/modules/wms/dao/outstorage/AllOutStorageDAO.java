package com.tbl.modules.wms.dao.outstorage;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tbl.modules.wms.entity.outstorage.AllOutStorage;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 1:退库回生产线  2.采购退货  3.报废
 * @author 70486
 */
public interface AllOutStorageDAO extends BaseMapper<AllOutStorage> {

    /**
     * 批量插入
     * @param allOutStorages
     * @return int
     */
    int insertBatches(@Param("allOutStorages") List<AllOutStorage> allOutStorages);

    /**
     * 批量插入
     * @param allOutStorages
     * @return int
     */
    int insertScrapBatches(@Param("allOutStorages") List<AllOutStorage> allOutStorages);
}
