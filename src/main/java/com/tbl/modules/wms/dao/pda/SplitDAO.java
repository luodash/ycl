package com.tbl.modules.wms.dao.pda;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.tbl.modules.wms.entity.split.Split;
import com.tbl.modules.wms.entity.storageinfo.StorageInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 拆分持久层
 * @author 70486
 */
public interface SplitDAO extends BaseMapper<Split> {

    /**
     * 查询拆分表所有数据
     * @return Split
     */
    List<Split> getAllSplit();

    /**
     * 批量更新拆分表库位
     * @param lstStorageInfo
     */
    void updateBatch(@Param("lstStorageInfo") List<StorageInfo> lstStorageInfo);

    /**
     * 拆分表分页列表数据
     * @param page
     * @param map
     * @return List<split>
     */
    List<Split> getList(Pagination page, Map<String,Object> map);

    /**
     * 根据拆分表主键删除拆分库存
     * @param meterEqualList
     */
    void deleteBySplitListIds(@Param("meterEqualList") List<Split> meterEqualList);

    /**
     * 根据质保号检索拆分库存表
     * @param map 质保号、批次号、rfid，类型
     * @return Split
     */
    Split findSplitByqbr(Map<String, Object> map);
}
