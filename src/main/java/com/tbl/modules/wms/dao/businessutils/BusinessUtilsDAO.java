package com.tbl.modules.wms.dao.businessutils;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.poi.ss.formula.functions.T;

/**
 * 业务流程公用服务
 * @author cxf
 */
public interface BusinessUtilsDAO extends BaseMapper<T> {

}
