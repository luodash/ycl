package com.tbl.modules.wms.dao.pad;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.poi.ss.formula.functions.T;

/**
 * 平板持久层
 * @author 70486
 */
public interface PadDAO extends BaseMapper<T> {

}
