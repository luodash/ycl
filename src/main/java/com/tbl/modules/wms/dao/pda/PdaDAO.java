package com.tbl.modules.wms.dao.pda;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.modules.wms.entity.allo.Allocation;
import com.tbl.modules.wms.entity.allo.AllocationDetail;
import com.tbl.modules.wms.entity.baseinfo.Shelf;
import com.tbl.modules.wms.entity.baseinfo.Warehouse;
import com.tbl.modules.wms.entity.instorage.InstorageDetail;
import com.tbl.modules.wms.entity.inventory.Inventory;
import com.tbl.modules.wms.entity.move.MoveStorage;
import com.tbl.modules.wms.entity.move.MoveStorageDetail;
import com.tbl.modules.wms.entity.outstorage.OutStorage;
import com.tbl.modules.wms.entity.outstorage.OutStorageDetail;
import com.tbl.modules.wms.entity.storageinfo.StorageInfo;
import org.apache.ibatis.annotations.Param;
import org.apache.poi.ss.formula.functions.T;

import java.util.List;
import java.util.Map;

/**
 * 手持机持久层
 * @author 70486
 */
public interface PdaDAO extends BaseMapper<T> {

    /**
     * 查询菜单
     * @param roleId
     * @param type
     * @return List<String>
     */
    List<String> getMenuList(@Param("roleId") Long roleId, @Param("type") Integer type);

    /**
     * 根据质保号获取EBS对应的批次号
     * @param qaCode
     * @return Map<String,Object>
     */
    Map<String,Object> getebsbatchno(@Param("qaCode") String qaCode);

    /**
     * 获取标签表数据
     * @param dataList
     * @return List<InstorageDetail>
     */
    List<InstorageDetail> getPackingList(@Param("dataList") List<String> dataList);

    /**
     * 包装确认批量更新
     * @param lstInstorageDetail
     */
    void storageinPackingSubmit(@Param("lstInstorageDetail") List<InstorageDetail> lstInstorageDetail);

    /**
     * 入库获取仓库列表
     * @param page
     * @param factoryCode
     * @param code
     * @return List<Warehouse>
     */
    List<Warehouse> storageinStorageinStart(@Param("page") Page page, @Param("factoryCode") String factoryCode,@Param("code")String code);

    /**
     * 查询库存外的库位
     * @param code
     * @return List<Shelf>
     */
    List<Shelf> storageinStorageinShelf(@Param("code") String code);

    /**
     * 根据rfid查询标签初始数据
     * @param rfid
     * @return InstorageDetail
     */
    InstorageDetail selectInstorageDetailByRfid(@Param("rfid") String rfid);

    /**
     * 查询库位分页数据，可用库位
     * @param page
     * @param warehouseCode
     * @param factoryCode
     * @param code
     * @param warehouseArea
     * @return List<Shelf>
     */
    List<Shelf> storageinEmptyShelf(@Param("page") Page page, @Param("warehouseCode") String warehouseCode,
                                    @Param("factoryCode") String factoryCode,
                                    @Param("code") String code, @Param("warehouseArea") List<String> warehouseArea);

    /**
     * 查询所有库位
     * @param page
     * @param code
     * @return List<Shelf>
     */
    List<Shelf> getallemptyshelf(Page page, @Param("code") String code);

    /**
     * 获取所有可用库位
     * @param page
     * @param warehouseCode
     * @param factoryCode
     * @param code
     * @return List<Shelf>
     */
    List<Shelf> selectNoCarShelf(@Param("page") Page page, @Param("warehouseCode") String warehouseCode,
                                 @Param("factoryCode") String factoryCode, @Param("code") String code);

    /**
     * 获取室内库库位（虚拟行车）
     * @param page
     * @param warehouseCode
     * @param factoryCode
     * @param code
     * @return List<Shelf>
     */
    List<Shelf> selectVisualShelf(@Param("page") Page page, @Param("warehouseCode") String warehouseCode, @Param("factoryCode") String factoryCode,
                                 @Param("code") String code);

    /**
     * 周围一圈的库位
     * @param page
     * @param code
     * @param warehouseArea
     * @param factoryCode
     * @param warehouseCode
     * @return List<Shelf>
     */
    List<Shelf> getRealArroundShelf(@Param("page") Page page, @Param("code") String code, @Param("warehouseArea") List<String> warehouseArea,
                                    @Param("factoryCode") String factoryCode, @Param("warehouseCode") String warehouseCode);

    /**
     * 判断报验时间是否处于每月1号的00：00至08：30
     * @param rfid RFID
     * @param currentMounthFirstDay 本月的第一天
     * @return int
     */
    int findInspectNoTimeOrNot(@Param("rfid") String rfid, @Param("currentMounthFirstDay") String currentMounthFirstDay);

    /**
     * 判断报验时间是否处于每月1号的08：30之前
     * @param rfid RFID
     * @param currentMounthFirstDay 本月的第一天
     * @return int
     */
    int findInspectNoTimeOrNot2(@Param("rfid") String rfid, @Param("currentMounthFirstDay") String currentMounthFirstDay);

    /**
     * 根据发货单号查发货单
     * @param shipNo
     * @return OutStorage
     */
    OutStorage selectStorageByShipNo(@Param("shipNo")String shipNo);

    /**
     * 根据提货单号、厂区、仓库,查询该提货单下的明细
     * @param shipNo
     * @param factoryCode
     * @param warehouseCode
     * @return List<OutStorageDetail>
     */
    List<OutStorageDetail> selectOutStorageDetail(@Param("shipNo") String shipNo, @Param("factoryCode") String factoryCode,
                                                  @Param("warehouseCode") String warehouseCode);

    /**
     * 获取该发货单下的所有详情的所在库位以及出库情况
     * @param shipNo
     * @param org
     * @param warehouseCode
     * @return List<Map<String,Object>>
     */
    List<Map<String,Object>> findOutstorageShelfs(@Param("shipNo") String shipNo, @Param("org") String org,
                                                  @Param("warehouseCode") String warehouseCode);

    /**
     * 查询货位上的出库明细
     * @param shipNo
     * @param warehouseCode
     * @param org
     * @param warehouseArea
     * @return List<OutStorageDetail>
     */
    List<OutStorageDetail> selectReOutStorageDetail(@Param("shipNo") String shipNo, @Param("warehouseCode") String warehouseCode,
                                                    @Param("org") String org, @Param("warehouseArea") String warehouseArea);

    /**
     * 根据提货单号查询提货单明细数量
     * @param outstorageShipnoList
     * @return int
     */
    int selectCountOutstorageDetailByShipNo(@Param("outstorageShipnoList") List<String> outstorageShipnoList);

    /**
     * 根据发货单号查询出库主表信息
     * @param outstorageShipnoList
     * @return List<Long>
     */
    List<Long> selectOutstorageIdListByShipNo(@Param("outstorageShipnoList") List<String> outstorageShipnoList);

    /**
     * 发货主界面排序按钮
     * @param outStorageIdList
     * @param factoryCode
     * @param warehouseCode
     * @return List<Map<String, Object>>
     */
    List<Map<String, Object>> sortOutstorageDetail(@Param("outStorageIdList") List<Long> outStorageIdList,
                                                   @Param("factoryCode") String factoryCode,
                                                   @Param("warehouseCode") String warehouseCode);

    /**
     * 根据发货单号查询详情是否已经全部出库完成
     * @param shipNo
     * @param factoryCode
     * @param warehouseCode
     * @return int
     */
    int findNoFinishOutstorageDetail (@Param("shipNo") String shipNo,@Param("factoryCode") String factoryCode,
                                      @Param("warehouseCode") String warehouseCode);

    /**
     * 点击排序后的明细，跳转到单一明细界面
     * @param storageInfoId
     * @param shipNo
     * @return List<StorageInfo>
     */
    List<StorageInfo> findSortSoloByStorageInfoId(@Param("storageInfoId") Long storageInfoId, @Param("shipNo") String shipNo);

    /**
     * 查询指定车牌、厂区下的发货数据
     * @param carNo
     * @param factoryCode
     * @return OutStorageDetail
     */
    List<OutStorageDetail> selectWzlzData(@Param("carNo") String carNo, @Param("factoryCode") String factoryCode);

    /**
     * 返回所有未完成的主表信息
     * @param orgList
     * @return List<Allocation>
     */
    List<Allocation> selectAllAllocation(@Param("orgArr") List<String> orgList);

    /**
     * 返回所有未完成调拨出库的主表信息
     * @param orgList
     * @return List<Allocation>
     */
    List<Allocation> alloSelectPartUndo(@Param("orgArr") List<String> orgList);

    /**
     * 显示所有待入库的调拨单
     * @param orgList
     * @return List<Allocation>
     */
    List<Allocation> pendingAlloStorageList(@Param("orgArr") List<String> orgList);

    /**
     * 获取调拨任务
     * @param id
     * @return List<AllocationDetail>
     */
    List<AllocationDetail> alloSelectDetailById(@Param("id") Long id);

    /**
     * 获取指定调拨单的调拨出库任务
     * @param id
     * @return List<AllocationDetail>
     */
    List<AllocationDetail> alloOutSelectDetailById(@Param("id") Long id);

    /**
     * 获取调拨入库任务
     * @param id
     * @return List<AllocationDetail>
     */
    List<AllocationDetail> alloInSelectDetailById(@Param("id") Long id);

    /**
     * 判断该主表下有无已经开始的字表信息
     * @param id
     * @return int
     */
    int selectDetailsState(@Param("id") Long id);

    /**
     * 根据三属性板子查询调拨明细
     * @param barcodeType
     * @param rfid
     * @return Allocation
     */
    Allocation selectWarehouseCodeByRfid(@Param("barcodeType") String barcodeType, @Param("rfid") String rfid);

    /**
     * 获取指定仓库下不在调拨入库库位中的库位
     * @param warehouseId
     * @return List<Shelf>
     */
    List<Shelf> selectAlloShelf(@Param("warehouseId") Long warehouseId);

    /**
     * 返回所有未完成的主表信息
     * @param orglist
     * @return List<Allocation>
     */
    List<Allocation> selectAllMoveStorage(@Param("orglist") List<String> orglist);

    /**
     * 返回所有未完成移库出库的主表信息
     * @param orglist
     * @return List<Allocation>
     */
    List<Allocation> moveSelectPartUndo(@Param("orglist")List<String> orglist);

    /**
     * 显示所有待入库的移库单
     * @param orgList
     * @return List<Allocation>
     */
    List<Allocation> pendingMoveStorageList(@Param("orgArr") List<String> orgList);

    /**
     * 移库功能模块进入，展示移库单明细列表
     * @param id
     * @return List<AllocationDetail>
     */
    List<AllocationDetail> moveSelectDetailById(@Param("id") Long id);

    /**
     * 移库入库明细
     * @param id
     * @return List<AllocationDetail>
     */
    List<AllocationDetail> moveInSelectDetailById(@Param("id") Long id);

    /**
     * 移库出库明细
     * @param id
     * @return List<AllocationDetail>
     */
    List<AllocationDetail> moveOutSelectDetailById(@Param("id") Long id);

    /**
     * 根据调拨主表主键查询进行中的任务数量
     * @param id
     * @return int
     */
    int selectDetailsStateMove(@Param("id") Long id);

    /**
     *  根据三属性板子查询移库主表信息
     * @param barcodeType
     * @param rfid
     * @return MoveStorage
     */
    MoveStorage selectWarehouseCodeByRfidMove(@Param("barcodeType") String barcodeType, @Param("rfid") String rfid);

    /**
     * 查询库位
     * @param id
     * @return List<Shelf>
     */
    List<Shelf> selectMoveShelf(@Param("id") Long id);

    /**
     * 查下盘点主计划
     * @param userId
     * @return List<Inventory>
     */
    List<Inventory> inventoryPlanSelect(@Param("userId") Long userId);

    /**
     * 根据批次号查库存
     * @param rfid
     * @return StorageInfo
     */
    StorageInfo getAllInstorage(@Param("rfid") String rfid);

    /**
     * 根据主键更新米数
     * @param id
     * @param meter
     */
    void updateallinstoragebyid(@Param("id") Long id, @Param("meter") String meter);

    /**
     * 获取拆分入库表序列
     * @return String
     */
    String getseqsplitinstorage();

    /**
     * 盘点扫描时获取物料的详情
     * @param rfid
     * @param barcode
     * @param barcodeType
     * @return StorageInfo
     */
    StorageInfo getMaterialInfo(@Param("rfid") String rfid, @Param("barcode") String barcode, @Param("barcodeType") String barcodeType);

    /**
     * 扫描获取库存物料详情（分盘扫码也是用的这个）
     * @param rfid
     * @param barcode
     * @param barcodeType
     * @return StorageInfo
     */
    StorageInfo getStorageInfoDetail(@Param("rfid") String rfid, @Param("barcode") String barcode, @Param("barcodeType") String barcodeType);

    /**
     * 查下货物详情信息
     * @param code
     * @param barcodeType
     * @return InstorageDetail
     */
    InstorageDetail getMaterialInfo2(@Param("code") String code, @Param("barcodeType") Integer barcodeType);
    
    /**
     * 功能描述: 获取UWB所在库位(完成入库弹框里面的最终库位)
     * @param x
     * @param y
     * @param warehouseArea
     * @return List<Shelf>
     * 返回：定位库位
     */
    List<Shelf> getUwbShelf(@Param("x") Float x,@Param("y") Float y,@Param("warehouseArea") List<String> warehouseArea);

    /**
     * 根据质保号检索库存表
     * @param map 质保号、批次号、rfid，类型
     * @return StorageInfo
     */
    StorageInfo findStorageInfoByqbr(Map<String, Object> map);

    /**
     * 根据质保号检索调拨明细表
     * @param map 质保号、批次号、rfid，类型
     * @return AllocationDetail
     */
    List<AllocationDetail> findAllocationDetailByqbr(Map<String, Object> map);

    /**
     * 根据质保号检索移库明表
     * @param map 质保号、批次号、rfid，类型
     * @return MoveStorageDetail
     */
    MoveStorageDetail findMoveStorageDetailByqbr(Map<String, Object> map);

    /**
     * 根据质保号检索标签明表
     * @param map 质保号、批次号、rfid，类型
     * @return InstorageDetail
     */
    InstorageDetail findInstorageDetailByqbr(Map<String, Object> map);

    /**
     * 在物料视图中根据质保号查询物料相关信息
     * @param queryMap
     * @return Map<String,Object>
     */
    Map<String,Object> findOrgByVmaterial(Map<String,Object> queryMap);

    /**
     * 获取手持机最新版本号，url地址
     * @return Map<String,Object>
     */
    Map<String,Object> findPdaConfig();
}
