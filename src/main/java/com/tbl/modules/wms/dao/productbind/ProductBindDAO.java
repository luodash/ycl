package com.tbl.modules.wms.dao.productbind;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.modules.wms.entity.productbind.ProductBind;

/**
 * 产线ip持久层
 * @author 70486
 */
public interface ProductBindDAO extends BaseMapper<ProductBind> {

    /**
     * 获取产线IP列表
     * @param page
     * @param map
     * @return ProductBind
     */
    List<ProductBind> getPageList(Page page, Map<String, Object> map);
}
