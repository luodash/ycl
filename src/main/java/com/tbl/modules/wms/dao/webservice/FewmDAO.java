package com.tbl.modules.wms.dao.webservice;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.poi.ss.formula.functions.T;

/**
 * Webservice服务的接口方法持久层
 * @author 70486
 */
public interface FewmDAO extends BaseMapper<T> {

    /**
     * 获取YDDL_PRINT_INFO最新序列值
     * @return long
     */
    long selectSeqPrintInfo();

    /**
     * 获取rfid最新序列值
     * @return int
     */
    int selectSeqRfid();

    /**
     * 保留接口更新标签初始化表
     * @param batch_no
     * @param orderline
     * @param orderno
     */
    void updateOrderInstorageDetail(@Param("batch_no") String batch_no, @Param("orderline") String orderline, @Param("orderno") String orderno);

    /**
     * 保留接口更新rfid重新绑定表
     * @param batch_no
     * @param orderline
     * @param orderno
     */
    void updateOrderRfidBindDetail(@Param("batch_no") String batch_no, @Param("orderline") String orderline, @Param("orderno") String orderno);

}
