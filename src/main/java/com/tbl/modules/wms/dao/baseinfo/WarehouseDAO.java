package com.tbl.modules.wms.dao.baseinfo;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.tbl.modules.wms.entity.baseinfo.Warehouse;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 仓库管理持久层
 *
 * @author 70486
 */
public interface WarehouseDAO extends BaseMapper<Warehouse> {

    /**
     * 获取仓库数据
     *
     * @param page
     * @param map
     * @return Warehouse
     */
    List<Warehouse> getPageList(Pagination page, Map<String, Object> map);

    /**
     * 获取原材料仓库数据
     *
     * @param page
     * @param map
     * @return Warehouse
     */
    List<Warehouse> getYclPageList(Pagination page, Map<String, Object> map);

    /**
     * 获取原材料仓库下拉数据
     *
     * @param map 参数条件
     * @return Map<String, Object>
     */
    List<Map<String, Object>> getYclPageWarehouseList(Map<String, Object> map);

    List<Map<String, Object>> getYclPageWarehouseListByEntityID(Map<String, Object> map);


    /**
     * 仓库列表Excel导出
     *
     * @param map
     * @return warehouse
     */
    List<Warehouse> getExcelList(Map<String, Object> map);

    /**
     * 获取仓库下拉数据
     *
     * @param page 分页辅助类
     * @param map  参数条件
     * @return Map<String, Object>
     */
    List<Map<String, Object>> getPageWarehouseList(Page page, Map<String, Object> map);

    /**
     * 仓库批量插入
     *
     * @param addList 仓库集合
     * @return int
     */
    int insertBatches(List<Warehouse> addList);

    /**
     * 仓库批量更新
     *
     * @param updateList 仓库集合
     */
    void updateBatches(List<Warehouse> updateList);

    /**
     * 获取仓库全部下拉列表
     *
     * @param map
     * @return List<Map < String, Object>
     */
    List<Map<String, Object>> getWarehouseList(Map<String, Object> map);

    /**
     * 根据仓库主键查询仓库名称
     *
     * @param warehouseIds
     * @return String
     */
    List<String> findWarehouseNameByCode(@Param("warehouseIds") List<String> warehouseIds);

    /**
     * 查询
     *
     * @param code     仓库编号
     * @param entityId 厂区编码
     * @return com.tbl.modules.wms.entity.baseinfo.Warehouse
     */
    Warehouse findByCodeAndEntityId(@Param("code") String code, @Param("entityId") String entityId);

    Warehouse getWarehouseByCode(Map<String, Object> map);

}
