package com.tbl.modules.wms.dao.printer;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tbl.modules.wms.entity.system.Printer;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 打印机配置持久层
 * @author 70486
 */
public interface PrinterDAO extends BaseMapper<Printer> {

    /**
     * 获取打印配置列表的数据
     * @param paramsMap
     * @return Printer
     */
    List<Printer> selectPageList(Map<String,Object> paramsMap);

    /**
     * 根据id查询
     * @param  id
     * @return Printer
     */
    Printer findById(@Param("id") Long id);

    /**
     * 根据用户ID获取打印机配置
     * @param userId
     * @return Printer
     */
    Printer findPrinterCodeByUserId(@Param("userId") Long userId);
}
