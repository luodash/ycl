package com.tbl.modules.wms.dao.interfacelog;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tbl.modules.wms.entity.interfacelog.InterfaceDo;
import com.tbl.modules.wms.entity.outstorage.OutStorageDetail;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 接口执行持久层
 * @author 70486
 */
public interface InterfaceDoDAO extends BaseMapper<InterfaceDo> {

    /**
     * 批量更新接口离线执行表状态
     * @param lstOutStorageDetail 发货单明细
     * @param state 状态
     */
    void updateStateBatches(@Param("lstOutStorageDetail") List<OutStorageDetail> lstOutStorageDetail, @Param("state") String state);

    /**
     * 更新接口执行表
     * @param interfaceDo 接口数据
     */
    void updateInterfaceDoByid(@Param("interfaceDo") InterfaceDo interfaceDo);

    /**
     * 获取定时任务需要执行的离线接口
     * @return InterfaceDo
     */
    List<InterfaceDo> findJobsInterfaceDos();
}
