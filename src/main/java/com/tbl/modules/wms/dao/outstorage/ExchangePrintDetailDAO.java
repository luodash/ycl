package com.tbl.modules.wms.dao.outstorage;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tbl.modules.wms.entity.outstorage.ExchangePrintDetail;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 物资流转单明细
 * @author wc
 */
@Mapper
public interface ExchangePrintDetailDAO extends BaseMapper<ExchangePrintDetail> {

    /**
     * 根据车牌获取发货单号信息列表
     * @param carNo 车牌号
     * @return String
     */
    List<String> getShipNoList(@Param("carNo") String carNo);

    /**
     * 获取打印明细列表
     * @param shipNoList 提货单列表
     * @return Map<String,Object>
     */
    List<Map<String,Object>>  getPrintDetailList(@Param("shipNoList")  List<String> shipNoList);



}
