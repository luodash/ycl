package com.tbl.modules.wms.dao.allo;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tbl.modules.wms.entity.allo.AllocationDetail;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 调拨详情
 * @author 70486
 */
public interface AllocationDetailDAO extends BaseMapper<AllocationDetail> {

    /**
     * 调拨出库明细Excel导出获取列表
     * @param map
     * @return AllocationDetail
     */
    List<AllocationDetail> getAlloStorageOutDetailExcel(Map<String, Object> map);

    /**
     * 调拨单新增详情批量插入
     * @param allocationDetailBatches 出库单集合
     * @return int 批量插入条数
     */
    int insertBatches(@Param("allocationDetailBatches") List<AllocationDetail> allocationDetailBatches);

    /**
     * 获取调拨单详情状态
     * @param idList 调拨单明细主键集合
     * @return List<Long>
     */
    List<Long> getAllocationDetailStateList(@Param("idList") List<Long> idList);

    /**
     * 获取调拨单详情质保号
     * @param idList 调拨单明细主键集合
     * @return List<String>
     */
    List<String> getAllocationDetailQacodeList(@Param("idList") List<Long> idList);

}
