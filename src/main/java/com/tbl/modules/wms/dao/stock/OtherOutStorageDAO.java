package com.tbl.modules.wms.dao.stock;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.tbl.modules.wms.entity.outstorage.AllOutStorage;

import java.util.List;
import java.util.Map;

/**
 * 其他类出库持久层
 * @author 70486
 */
public interface OtherOutStorageDAO extends BaseMapper<AllOutStorage> {

    /**
     * 退回产线+采购退货+报废清单
     * @param page
     * @param params
     * @return List<InstorageDetail>
     */
    List<AllOutStorage> getPageList(Pagination page, Map<String,Object> params);

    /**
     * 导出excel
     * @param map
     * @return List<OutStorageDetail>
     */
    List<AllOutStorage> getExcelList(Map<String, Object> map);
}
