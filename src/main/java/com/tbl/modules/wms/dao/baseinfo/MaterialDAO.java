package com.tbl.modules.wms.dao.baseinfo;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.tbl.modules.wms.entity.baseinfo.Material;
import com.tbl.modules.wms2.entity.purchasein.MeterialBackEbsEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 物料持久层
 * @author 70486
 */
public interface MaterialDAO extends BaseMapper<Material> {

    /**
     * 货物物料查询列表
     * @param page
     * @param map
     * @return Material
     */
    List<Material> getPageList(Pagination page, Map<String, Object> map);

    /**
     * 获取导出Excel列表
     * @param map
     * @return Material
     */
    List<Material> getExcelList(Map<String, Object> map);

    /**
     * 物料批量插入
     * @param addList
     * @return int
     */
    int insertBatches(List<Material> addList);

    /**
     * 物料批量更新
     * @param updateList
     * @return int
     */
    void updateBatches(@Param("updateList") List<Material> updateList);

    /**
     * 根据主键查询物料信息
     * @param id
     * @return Material
     */
    Material selectInfoById(@Param("id") Long id);

    /**
     * 根据物料编码查询物料信息
     * @param map
     * @return Map<String,Object>
     */
    Map<String,Object> selectMaterialByCode(Map map);

    /**
     * 根据物料编码查询物料信息
     * @param map
     * @return Map<String,Object>
     */
    List<Map<String,Object>> getMaterialsByCode(Map map);

    /**
     * 根据物料编码模糊查询获取物料分页数据
     * @param map
     * @return
     */
    List<Map<String,Object>> getPageByCode(Map map);

    /**
     * 根据物料编码获取物料总数
     * @param map
     * @return
     */
    int getCountByCode(Map map);


    /**
     * 获取行业全部下拉列表
     * @return List<Map<String,Object>
     */
    List<Map<String, Object>> getIndustryList();

    /**
     * 获取类别全部下拉列表
     * @param industry 行业编码
     * @return List<Map<String,Object>
     */
    List<Map<String, Object>> getCategoryList(@Param("industry") String industry);

    /**
     * 功能描述：获取二级材料列表
     * @param industry 行业编码
     * @param category 物理类别
     * @return List<Map<String,Object>>
     */
    List<Map<String, Object>> getGradeTwoList(@Param("industry") String industry, @Param("category") String category);

    /**
     * 功能描述：获取三级材料列表
     * @param industry 行业编码
     * @param category 物理类别
     * @param gradeTwo 二级材料
     * @return List<Map<String,Object>>
     */
    List<Map<String, Object>> getGradeThreeList(@Param("industry") String industry, @Param("category") String category,
                                                @Param("gradeTwo") String gradeTwo);

    Map<String,Object>  materialBackEBS(MeterialBackEbsEntity meteriaBackEbsEntity);

    Map<String,Object>  materialReceiveEBS(Map<String,Object> map);


}
