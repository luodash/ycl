package com.tbl.modules.wms.dao.allo;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.tbl.modules.wms.entity.allo.Allocation;
import com.tbl.modules.wms.entity.allo.AllocationDetail;
import com.tbl.modules.wms.entity.baseinfo.Car;
import com.tbl.modules.wms.entity.baseinfo.Shelf;
import com.tbl.modules.wms.entity.baseinfo.Warehouse;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 调拨管理持久层
 * @author 70486
 */
public interface AllocationDAO extends BaseMapper<Allocation> {

    /**
     * 调拨单管理列表页数据
     * @param page
     * @param map
     * @return Allocation
     */
    List<Allocation> getPageList(Page page, Map<String, Object> map);

    /**
     * 移库调拨单管理列表页数据
     * @param page
     * @param map
     * @return Allocation
     */
    List<Allocation> getMoveAlloPageList(Page page, Map<String, Object> map);

    /**
     * 调拨单获取详情的列表
     * @param page
     * @param id 调拨单主键
     * @return PageUtils
     */
    List<AllocationDetail> getDetailList(Page page, Integer id);

    /**
     * 移库调拨单获取详情的列表
     * @param page
     * @param id 移库调拨单主键
     * @return PageUtils
     */
    List<AllocationDetail> getMoveAlloDetailList(Page page, Integer id);

    /**
     * 调拨出库导表和调拨单管理导表
     * @param map 模糊查询条件
     * @return List<Allocation>
     */
    List<Allocation> getAlloStorageOutExcel(Map<String, Object> map);

    /**
     * 编辑页面  获取质保单号列表
     * @param page
     * @param map
     * @return PageUtils
     */
    List<Map<String, Object>> getQaCode(Page<Map<String, Object>> page, Map<String, Object> map);

    /**
     * 获取调入仓库的数据
     * @param page
     * @param map
     * @return PageUtils
     */
    List<Map<String, Object>> selectWarehouseIn(Page page, Map<String, Object> map);

    /**
     *  获取调出仓库的数据
     *  @param page
     *  @param map
     * @return PageUtils
     */
    List<Map<String, Object>> selectWarehouseOut(Page page, Map<String, Object> map);

    /**
     * 根据主键获取主表信息
     * @param id 调拨单主键
     * @param factoryCode
     * @return Allocation 调拨单
     */
    Allocation selectInfoById(@Param("id") Long id,@Param("factoryCode") String factoryCode);

    /**
     * 查询以及开始的调拨明细数量
     * @param infoIdList
     * @return int
     */
    int selectDetailsState(@Param("infoIdList") List<Long> infoIdList);

    /**
     * 跳转行车绑定页面获取行车绑定列表
     * @param warehouseCode 仓库编码
     * @param org 厂区编码
     * @return List<Car> 行车列表
     */
    List<Car> selectCarListByWarehouseCode(@Param("warehouseCode") String warehouseCode, @Param("org") List<String> org);

    /**
     * 获取调拨入库列表数据
     * @param page
     * @param map
     * @return PageUtils
     */
    List<AllocationDetail> getInDetailList(Page page, Map<String, Object> map);

    /**
     * 获取调拨流水号
     * @return int
     */
    int selectLatestAllocation();

    /**
     * 查询调拨单详细信息
     * @param id 调拨明细表主键
     * @return AllocationDetail 调拨明细
     */
    AllocationDetail getDetailById(@Param("id") Long id);

    /**
     * 获得仓库列表
     * @param factoryCode 厂区编码
     * @return List<Warehouse> 仓库列表
     */
    List<Warehouse> getWarehouseList(@Param("factoryCode") String factoryCode);

    /**
     * 获取行车列表
     * @param id 仓库主键
     * @return List<Car> 行车列表
     */
    List<Car> getCarList(@Param("id") Long id);

    /**
     * 获取库位表中未被使用的库位
     * @param code 仓库编码
     * @param factoryCode 厂区编码
     * @return Shelf 库位编码
     */
    List<Shelf> getRecommendShelf(@Param("code") String code ,@Param("factoryCode") String factoryCode);

    /**
     * 选择库位
     * @param page
     * @param map
     * @return Map<String, Object>
     */
    List<Map<String, Object>> selectUnbindShelf(Page page, Map<String, Object> map);

    /**
     * 功能描述:入库确认
     * @param id 调拨/移库 详情主键
     * @param shelfCode 库位编码
     * @param userId 用户主键
     * @return Map<String, Object>
     */
    boolean confirmInstorage(@Param("id") Long id, @Param("shelfCode") String shelfCode, @Param("userId") Long userId);

    /**
     * 获得excel导出列表
     * @param map
     * @return List<AllocationDetail>
     */
    List<AllocationDetail> getAlloInExcelList(Map<String, Object> map);

    /**
     * 获取调拨入库列表数据
     * @param page
     * @param map
     * @return Allocation
     */
    List<Allocation> getPageAlloList(Pagination page, Map<String, Object> map);

    /**
     * 根据id查询明细
     * @param id
     * @return
     */
    List<Allocation> selectAllocationDetail(@Param("id") Long id);
}
