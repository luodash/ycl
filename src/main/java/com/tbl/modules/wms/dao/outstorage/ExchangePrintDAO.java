package com.tbl.modules.wms.dao.outstorage;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.modules.wms.entity.outstorage.ExchangePrint;
import io.lettuce.core.dynamic.annotation.Param;

import java.util.List;

/**
 * 流转单打印持久层
 * @author 70486
 */
public interface ExchangePrintDAO extends BaseMapper<ExchangePrint> {

    /**
     * 查询近五天的物资流转单打印清单
     * @param page 分页
     * @param circulationNo 流转单单号
     * @param carNo 车牌号
     * @param fromFactory 出发厂
     * @param toFactory 目的厂
     * @return List<ExchangePrint>
     */
    List<ExchangePrint> findPatchPrintList(@Param("page") Page page,@Param("circulationNo") String circulationNo, @Param("carNo") String carNo,
                                           @Param("fromFactory") String fromFactory,@Param("toFactory") String toFactory);

    /**
     * 查询近五天的物资流转单打印清单的总数量
     * @param circulationNo 流转单单号
     * @param carNo 车牌号
     * @param fromFactory 出发厂
     * @param toFactory 目的厂
     * @return List<ExchangePrint>
     */
    int findPatchPrintCount(@Param("circulationNo") String circulationNo, @Param("carNo") String carNo, @Param("fromFactory") String fromFactory,
                            @Param("toFactory") String toFactory);

}
