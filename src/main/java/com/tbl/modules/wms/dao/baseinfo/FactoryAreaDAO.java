package com.tbl.modules.wms.dao.baseinfo;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.tbl.modules.wms.entity.baseinfo.FactoryArea;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 厂区持久层
 * @author 70486
 */
public interface FactoryAreaDAO extends BaseMapper<FactoryArea> {

    /**
     * 查下厂区列表
     * @param page
     * @param map
     * @return FactoryArea
     */
    List<FactoryArea> getPageList(Pagination page, Map<String, Object> map);

    /**
     * 获取厂区列表导出列
     * @param map
     * @return FactoryArea
     */
    List<FactoryArea> getExcelList(Map<String, Object> map);

    /**
     * 获取厂区全部下拉列表
     * @param map
     * @return List<Map<String,Object>
     */
    List<Map<String, Object>> selectFactoryList(Map<String, Object> map);

    /**
     * 厂区批量插入
     * @param map
     * @return int
     */
    int insertBatches(List<FactoryArea> map);

    /**
     * 获取厂区编码，名称
     * @param listArr
     * @return Map<String,Object>
     */
    List<Map<String,Object>> getFactoryInfo(@Param("listArr") List<String> listArr);

    /**
     * 获取厂区名称
     * @param listArr
     * @return String
     */
    String getWarehouseNames(@Param("listArr") List<String> listArr);

    /**
     * 获取所有厂区信息
     * @return Map<String,Object>
     */
    List<Map<String,Object>> selectLists();

    /**
     * 功能描述：清空厂区流水号，每日清空
     */
    void updateSerialNum();
    
    /**
     * 功能描述：清空调拨、移库单流水号，每日清空
     */
    void updateAlloMoveSerialNum();
    
    /**
     * 功能描述：清空盘点单流水号，每月清空
     */
    void updateInventoryNum();

    /**
     * 根据厂区编码列表查询厂区名称
     * @param areaCodeList
     * @return String
     */
    List<String> findAreaNameByCode(@Param("areaCodeList") List<String> areaCodeList);
}
