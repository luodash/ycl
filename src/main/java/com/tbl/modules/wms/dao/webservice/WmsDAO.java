package com.tbl.modules.wms.dao.webservice;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.poi.ss.formula.functions.T;

/**
 * wms接口服务端
 * @author 70486
 */
public interface WmsDAO extends BaseMapper<T> {
}
