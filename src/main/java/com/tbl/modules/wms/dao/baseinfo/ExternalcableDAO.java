package com.tbl.modules.wms.dao.baseinfo;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.modules.wms.entity.baseinfo.Externalcable;

/**
 * 同步外协采购的电缆数据持久层
 * @author 70486
 */
public interface ExternalcableDAO extends BaseMapper<Externalcable> {

	/**
	 * 获取下拉菜单
	 * @param page
	 * @param map
	 * @return
	 */
	List<Map<String, Object>> getPageQacodeList(Page page, Map<String, Object> map);
}
