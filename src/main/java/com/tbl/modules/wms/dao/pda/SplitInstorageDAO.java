package com.tbl.modules.wms.dao.pda;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tbl.modules.wms.entity.split.Split;
import com.tbl.modules.wms.entity.split.SplitInstorage;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 拆分入库
 * @author 70486
 */
public interface SplitInstorageDAO extends BaseMapper<SplitInstorage> {

	/**
	 * 拆分入库
     * @param rfid
     * @param barcodeType
     * @return SplitInstorage
	 */
    SplitInstorage getInstorageSplit(@Param("rfid") String rfid,@Param("barcodeType") String barcodeType);

    /**
     * 获取拆分表中未入库的
     * @return Split
     */
    List<Split> getInstorageSplitType();

    /**
     * 获取拆分表入库数据
     * @param pid
     * @return SplitInstorage
     */
    List<SplitInstorage> getsplitBelong(@Param("pid") String pid);
}
