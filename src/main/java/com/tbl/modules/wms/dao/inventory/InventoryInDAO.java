package com.tbl.modules.wms.dao.inventory;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tbl.modules.wms.entity.inventory.InventoryIn;

/**
 * 入库日志记录表
 * @author 70486
 */
public interface InventoryInDAO extends BaseMapper<InventoryIn> {
	
}
