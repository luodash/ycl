package com.tbl.modules.wms.dao.baseinfo;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.modules.wms.entity.baseinfo.Workshop;

import java.util.List;
import java.util.Map;

/**
 * 车间持久层
 * @author 70486
 */
public interface WorkshopDAO extends BaseMapper<Workshop> {
	/**
	 * 获取车间列表数据
	 * @param page
	 * @param map
	 * @return List<Workshop>
	 */
    List<Workshop> getPageList(Page page, Map<String, Object> map);
    
    /**
     * 车间批量插入
     * @param addList
     * @return int
     */
    int insertBatches(List<Workshop> addList);

	/**
	 * 车间批量更新
	 * @param updateList
	 * @return int
	 */
	void updateBatches(List<Workshop> updateList);
}
