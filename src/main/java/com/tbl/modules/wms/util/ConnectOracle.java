package com.tbl.modules.wms.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import com.tbl.common.config.StaticConfig;

/**
 * 锁表
 * @author 70486
 */
public class ConnectOracle {
    /**
     * Oracle数据库连接URL
     */
    private final static String DB_URL = StaticConfig.getStaticDatabaseUrl();
    /**
     * 数据库用户名
     */
    private final static String DB_DRIVER = StaticConfig.getStaticDatabaseDrivename();
    /**
     * 数据库密码
     */
    private final static String DB_USERNAME = StaticConfig.getStaticDatabaseUsername();
    /**
     * 获取数据库连接
     */
    private final static String DB_PASSWORD = StaticConfig.getStaticDatabasePassword();

    public Connection getConnection() {
        /** 声明Connection连接对象 */
        Connection conn = null;
        try {
            /** 使用Class.forName()方法自动创建这个驱动程序的实例且自动调用DriverManager来注册它 */
            Class.forName(DB_DRIVER);
            /** 通过DriverManager的getConnection()方法获取数据库连接 */
            conn = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
        } catch (Exception ex) { ex.printStackTrace(); }
        return conn;
    }

    /** * 关闭数据库连接 * * @param connect */
    public void closeConnection(Connection conn) {
        try {
            if (conn != null) {
                /** 判断当前连接连接对象如果没有被关闭就调用关闭方法 */
                if (!conn.isClosed()) {
                    conn.close();
                }
            }
        } catch (Exception ex) { ex.printStackTrace(); }
    }
    
    

    public static void main(String[] args) throws SQLException {
        Connection connect = new ConnectOracle().getConnection();
        //设置手动提交事务
        connect.setAutoCommit(false);
        Statement stmt = connect.createStatement();
        //锁表
        stmt.addBatch("lock table t_symbol_code_fee in exclusive mode");
        //此处打上断点后，执行另一个类，你会发现，执行成功后并没有更改记录，因为表已经被锁定。只有提交事务后，TestOracle中执行的修改才能生效。
        stmt.executeBatch();
        //提交后自动解锁，回滚时也会自动解锁
        connect.commit();
        stmt.close();
        connect.close();
    }
}

