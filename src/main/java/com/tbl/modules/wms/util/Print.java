package com.tbl.modules.wms.util;

import lombok.Getter;
import lombok.Setter;

import java.awt.*;
import java.awt.print.*;

/**
 * java打印
 * @author 70486
 */
public class Print implements Printable {

    /**
     * 打印的总页数
     */
    @Getter
    @Setter
    private int pageSize;
    /**
     * 打印的纸张宽度
     */
    private double paperW=0;
    /**
     * 打印的纸张高度
     */
    private double paperH=0;

    /**
     * 实现java.awt.print.Printable接口的打印方法
     * pageIndex:打印的当前页，此参数是系统自动维护的，不需要手动维护，系统会自动递增
     * @param graphics
     * @param pageFormat
     * @param pageIndex
     * @return int
     */
    @Override
    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) {
        if (pageIndex >= pageSize){
            //退出打印
            return Printable.NO_SUCH_PAGE;
        }else {
            Graphics2D g2 = (Graphics2D) graphics;
            g2.setColor(Color.BLUE);
            Paper p = new Paper();
            //此处的paperW和paperH是从目标打印机的进纸规格中获取的，实际针式打印机的可打印区域是有限的，
            //距纸张的上下左右1inch(英寸)的中间的距形框为实际可打印区域，超出范围的内容将不会打印出来(没有设置偏移的情况)
            //如果设置偏移量，那么超出的范围也是可以打印的，这里的pageW和pageH我是直接获取打印机的进纸规格的宽和高
            //也可以手动指定，从是如果手动指定的宽高和目标打印机的进纸规格相差较大，将会默认以A4纸为打印模版
            // 设置可打印区域
            p.setImageableArea(0, 0, paperW, paperH);
            // 设置纸张的大小
            p.setSize(paperW,paperH);
            pageFormat.setPaper(p);
            //调用打印内容的方法
            drawCurrentPageText(g2, pageFormat);
            return PAGE_EXISTS;
        }

    }

    /**
     * 打印内容
     * @param g2
     * @param pf
     */
    private void drawCurrentPageText(Graphics2D g2, PageFormat pf) {
        //设置打印的字体
        Font font = new Font("新宋体", Font.BOLD, 11);
        // 设置字体
        g2.setFont(font);
        //此处打印一句话，打印开始位置是(200,200),表示从pf.getPaper()中座标为(200,200)开始打印
        //此处200的单位是1/72(inch)，inch:英寸，所以这里的长度，在测量后需要进行转换
        //获取打印数据
        /*List<OutStorageDetail> outStorageDetails = pdaDAO.selectWzlzData((String) ConRunningMap.wzlzMap.get("carNo"), (String) ConRunningMap.wzlzMap.get("factoryCode"));
        //页数
        int printPaperSize = outStorageDetails.size()/6;
        //余量
        int leftPaperSize = outStorageDetails.size()%6;

        //从厂
        String factoryCode = "";
        //流转厂
        String transferFactoryCode = "";
        //打印日期
        String printDate = DateUtils.getDay();
        //车辆
        String carNo = "";
        //物资流转单号
        String wzlzCode;
        DecimalFormat df = new DecimalFormat("0000");
        Integer oddnumber = exchangeDAO.getMaxOddNumber();
        if(oddnumber == null){
            wzlzCode = "WZLZ" + DateUtils.getDays() + "0001";
        }else{
            oddnumber += 1;
            wzlzCode = "WZLZ" + DateUtils.getDays() + df.format(oddnumber);
        }

        if (printPaperSize>0){
            for (int i = 0;i < printPaperSize;i++) {
                for (int j = 6*i+1;j <= 6*i+6;j++) {

                }
                System.out.println("print");
            }
        }
        if (leftPaperSize>0){
            for (int j = 1;j <= leftPaperSize;j++) {
            }
            System.out.println("print");
        }*/

        g2.drawString("远东电缆物资流转单",173,50);
        g2.drawString("从厂：",10,80);
        g2.drawString("打印日期：",80,100);
        g2.drawString("至厂：",250,80);
        g2.drawString("车辆：",150,80);
        g2.drawString("单号：",250,100);
        g2.drawRect(30,120,380,250);
        g2.drawString("发货员：",10,400);
        g2.drawString("提示：发货员、门卫、驾驶员三方必须清点进出货物",10,420);
        g2.drawString("接收人：",250,400);
        g2.drawString("班次：",300,400);
        g2.drawString("门卫：",350,400);

    }

    /**
     * 连接打印机，弹出打印对话框
     */
    public void starPrint() {
        try {
            PrinterJob prnJob = PrinterJob.getPrinterJob();
            PageFormat pageFormat = new PageFormat();
            pageFormat.setOrientation(PageFormat.PORTRAIT);
            prnJob.setPrintable(this);
            /*//弹出打印对话框，也可以选择不弹出打印提示框，直接打印
            if (!prnJob.printDialog()){
                return;
            }*/
            //获取所连接的目标打印机的进纸规格的宽度，单位：1/72(inch)
            paperW=prnJob.getPageFormat(null).getPaper().getWidth();
            //获取所连接的目标打印机的进纸规格的宽度，单位：1/72(inch)
            paperH=prnJob.getPageFormat(null).getPaper().getHeight();
            //System.out.println("paperW:"+paperW+";paperH:"+paperH);
            prnJob.print();//启动打印工作
        } catch (PrinterException ex) {
            ex.printStackTrace();
            System.err.println("打印错误：" + ex.toString());
        }
    }

    /**
     * 入口方法
     * @param args
     */

    public static void main(String[] args) {
        // 实例化打印类
        Print pm = new Print();
        //打印1页
        pm.pageSize = 1;
        pm.starPrint();


       /* //页数
        int printPaperSize = 13/6;
        //余量
        int leftPaperSize = 13%6;
        for (int i = 0;i < printPaperSize;i++) {
            for (int j = 6*i+1;j <= 6*i+6;j++) {

            }
            System.out.println("print");
        }

        for (int j = 1;j <= leftPaperSize;j++) {
        }
            System.out.println("print");*/
    }


}
