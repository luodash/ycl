package com.tbl.modules.wms.util;

import com.tbl.modules.wms.constant.JaxbUtil;
import com.tbl.modules.wms.entity.requestxml.PDA_UPDATE_DATA;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.PostConstruct;
import javax.xml.bind.JAXBException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

/**
 * 获取服务器上的文件并解析
 * @author 70486
 */
public class GetServerFile {

    private static String pdapath;

    @Value("${yddl.PDA_PATH}")
    private String path;

    @PostConstruct
    public void getApiToken(){
        pdapath = this.path;
    }

    /**
     * 请求服务器读取tomcat中配置好context的文件，解析获取相关信息
     * @return Map<String,Object>
     */
    public static Map<String,Object> initData() {

        Map<String, Object> map = new HashMap<>(4);
        //开启线程发起网络请求
        new Thread() {
            @Override
            public void run() {
                super.run();
                try {
                    //要访问的路径
                    URL url = new URL(pdapath);
                    //用于发送或接受数据
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    //发送get请求
                    conn.setRequestMethod("GET");
                    //请求超时时间
                    conn.setConnectTimeout(5000);
                    //服务器返回的状态码
                    int code = conn.getResponseCode();

                    //说明请求成功
                    if (code == 200) {
                        //获取服务器返回的数据
                        InputStream in = conn.getInputStream();

                        ByteArrayOutputStream bos = new ByteArrayOutputStream();

                        //读取缓存
                        byte[] buffer = new byte[2048];
                        int length = 0;
                        while((length = in.read(buffer)) != -1) {
                            bos.write(buffer, 0, length);//写入输出流
                        }
                        in.close();//读取完毕，关闭输入流

                        // 根据输出流创建字符串对象
                        String str = new String(bos.toByteArray(), StandardCharsets.UTF_8);
                        try {
                            PDA_UPDATE_DATA pda_update_data = JaxbUtil.xmlToBean(str, PDA_UPDATE_DATA.class);

                            map.put("version", pda_update_data.version);
                            map.put("name", pda_update_data.name);
                            map.put("url", pda_update_data.url);

                        } catch (JAXBException e) {
                            e.printStackTrace();
                        }

                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }.start();

        return map;
    }


}
