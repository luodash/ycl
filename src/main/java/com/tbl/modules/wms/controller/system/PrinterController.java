package com.tbl.modules.wms.controller.system;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Maps;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.platform.constant.MenuConstant;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.service.system.RoleService;
import com.tbl.modules.wms.entity.system.Printer;
import com.tbl.modules.wms.service.system.PrinterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

/**
 * 打印机设置控制层
 * @author 70486
 */
@Controller
@RequestMapping(value = "/printer")
public class PrinterController extends AbstractController {

    /**
     * 用户service
     */
    @Autowired
    private RoleService roleService;
    /**
     * 打印配置服务
     */
    @Autowired
    private PrinterService printerService;

    /**
     * 跳转到打印机配置维护界面
     * @return ModelAndView
     */
    @RequestMapping(value = "/toList.do")
    @ResponseBody
    public ModelAndView toList() {
        ModelAndView mv = new ModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(getSessionUser().getRoleId(), MenuConstant.printConfig));
        mv.setViewName("techbloom/system/printconfig/printer_list");
        return mv;
    }

    /**
     * 获取打印机配置列表数据
     * @param queryJsonString 查询条件参数
     * @return Object
     */
    @RequestMapping(value = "/list.do")
    @ResponseBody
    public Object list(String queryJsonString) {
    	Map<String, Object> map = new HashMap<>(6);
        if (!StringUtils.isEmpty(queryJsonString)) {
            map = JSON.parseObject(queryJsonString);
        }
        PageTbl page = this.getPage();
        map.put("page", page.getPageno());
        map.put("limit", page.getPagesize());
        String sortName = page.getSortname();
        if (StringUtils.isEmptyString(sortName)) {
            sortName = "id";
            page.setSortname(sortName);
        }
        String sortOrder = page.getSortorder();
        if (StringUtils.isEmptyString(sortOrder)) {
            sortOrder = "desc";
            page.setSortorder(sortOrder);
        }
        map.put("sidx", page.getSortname());
        map.put("order", page.getSortorder());
        PageUtils pageUser = printerService.queryPage(map);
        page.setTotalRows(pageUser.getTotalCount() == 0 ? 1 : pageUser.getTotalCount());
        map.put("rows", pageUser.getList());
        executePageMap(map, page);
        return map;
    }

    /**
     * 保存/修改
     * @param printer 打印配置信息
     * @return Map<String,Object>
     */
    @RequestMapping(value = "/save")
    @ResponseBody
    public Map<String, Object> save(Printer printer) {
        Map<String, Object> map = Maps.newHashMap();
        if (printer.getId() == -1){
            printer.setId(null);
        }
        printerService.insertOrUpdate(printer);
        map.put("result", true);
        return map;
    }

    /**
     * 返回到修改页面
     * @param id 打印配置表主键
     * @return ModelAndView
     */
    @RequestMapping(value = "/printer_edit")
    public ModelAndView printerEdit(Long id) {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("techbloom/system/printconfig/printer_edit");
        mv.addObject("id", id);
        mv.addObject("printer", id == -1 ? new Printer() : printerService.findById(id));
        return mv;
    }


}
