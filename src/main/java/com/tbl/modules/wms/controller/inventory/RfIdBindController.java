package com.tbl.modules.wms.controller.inventory;

import com.alibaba.fastjson.JSON;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.wms.service.instorage.InstorageDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * rfid 重新绑定
 * @author 70486
 */
@Controller
@RequestMapping(value = "/rfIdBind")
public class RfIdBindController extends AbstractController {

    /**
     * rfid绑定
     */
	@Autowired
	private InstorageDetailService instorageDetailService;

    /**
     * 跳转到车辆管理列表页面
     * @return ModelAndView
     */
    @RequestMapping(value = "/toList")
    public ModelAndView toList() {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("techbloom/demo/inventory/rfIdBind/rfIdBind_list");
        return mv;
    }

    /**
     * 获取rfid重新绑定列表数据
     * @param queryJsonString 查询条件
     * @return Map<String , Object>
     */
    @RequestMapping(value = "/rfidList")
    @ResponseBody
    public Map<String, Object> rfidList(String queryJsonString) {
        Map<String, Object> map = new HashMap<>(3);
        if (!StringUtils.isEmptyString(queryJsonString)) {
            map = JSON.parseObject(queryJsonString);
        }
        PageTbl page = this.getPage();
        PageUtils utils = instorageDetailService.getList(page, map);
        page.setTotalRows(utils.getTotalCount() == 0 ? 1 : utils.getTotalCount());
        //初始化分页对象
        executePageMap(map, page);
        map.put("rows", utils.getList());
        map.put("total", utils.getTotalPage() == 0 ? 1 : utils.getTotalPage());
        return map;
    }

    /**
     * 弹出到编辑页面
     * @param id
     * @return ModelAndView
     */
    @RequestMapping(value = "/toAdd.do")
    @ResponseBody
    public ModelAndView toAdd(Long id) {
        ModelAndView mv = this.getModelAndView();
        if (id != null && id != 0L) {
            mv.setViewName("techbloom/demo/inventory/rfIdBind/rfIdBind_info");
            List<Map<String,Object>> mapList = new ArrayList<>();
            for (int i = 0; i < 6; i++) {
                Map<String, Object> objMap = new HashMap<>(4);
                objMap.put("id", id);
                objMap.put("carNo", "物料" + id);
                objMap.put("carName", "BM0000" + id);
                objMap.put("shelveName", "RF0000" + id);
                mapList.add(objMap);
            }
            mv.addObject("car", mapList);
            mv.addObject("id", id);
        } else {
            mv.setViewName("techbloom/demo/inventory/rfIdBind/rfIdBind_edit");
            Map<String, Object> objMap = new HashMap<>(6);
            objMap.put("id", null);
            objMap.put("carNo", null);
            objMap.put("carName", null);
            objMap.put("shelveName", null);
            objMap.put("rfid", null);
            objMap.put("typeName", null);
            mv.addObject("car", objMap);
        }
        mv.addObject("edit", 1);
        return mv;
    }


}
