package com.tbl.modules.wms.controller.outstorage;

import com.alibaba.fastjson.JSON;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.platform.constant.MenuConstant;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.service.system.RoleService;
import com.tbl.modules.wms.entity.outstorage.ExchangePrint;
import com.tbl.modules.wms.service.outstorage.ExchangePrintService;
import com.tbl.modules.wms.service.outstorage.ExchangeService;
import com.tbl.modules.wms.service.outstorage.OutStorageService;
import com.tbl.modules.wms.service.webservice.client.WmsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

/**
 * 打印物资流转单
 * @author 70486
 */
@Controller
@RequestMapping("/exchange")
public class ExchangeController extends AbstractController {

    /**
     * 打印流转单服务
     */
    @Autowired
    private ExchangeService exchangeService;
    /**
     * 流转单打印服务
     */
    @Autowired
    private ExchangePrintService exchangePrintService;
    /**
     * 角色权限服务
     */
    @Autowired
    private RoleService roleService;
    /**
     * 出库单服务
     */
    @Autowired
    private OutStorageService outStorageService;
    /**
     * 接口client
     */
    @Autowired
    private WmsService wmsService;

    /**
     * 跳转到打印流转单页面
     * @return ModelAndView
     */
    @RequestMapping("/list")
    public ModelAndView list(){
        ModelAndView mv=this.getModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(getSessionUser().getRoleId(), MenuConstant.exchangePrint));
        mv.addObject("factoryList",exchangeService.getFactoryArea());
        mv.setViewName("techbloom/outstorage/outstorage/exchange_print");
        return mv;
    }

    /**
     * 流转单页面列表查询
     * @param queryJsonString
     * @return Map<String,Object>
     */
    @RequestMapping("/datalist")
    @ResponseBody
    public Map<String,Object> datalist(String queryJsonString){
        Map<String, Object> map = new HashMap<>(5);
        PageTbl page = this.getPage();
        if (!StringUtils.isEmptyString(queryJsonString)) {
            map = JSON.parseObject(queryJsonString);
        }
        PageUtils utils = exchangeService.getPageList(page, map);
        page.setTotalRows(utils.getTotalCount() == 0 ? 1 : utils.getTotalCount());
        //初始化分页对象
        executePageMap(map, page);
        map.put("rows", utils.getList());
        map.put("total", utils.getTotalPage() == 0 ? 1 : utils.getTotalPage());
        return map;
    }

    /**
     * 根据出库单获取车牌信息列表
     * @param outno 发货单
     * @param sTime 起始时间
     * @param eTime 截止时间
     * @return Map<String,Object>
     */
    @RequestMapping("/getCarlist")
    @ResponseBody
    public Map<String,Object> getCarList(String outno,String sTime,String eTime){
        Map<String,Object> resultMap = new HashMap<>(1);
        //根据提货单号获取提货单及明细
        try{
            //从远东获取打印出库单数据
            outStorageService.getOutstorageListByCarNo(outno).forEach( map -> {
                String shipno = (String)map.get("SHIPNO");
                if(StringUtils.isNotBlank(shipno)){
                    Map<String,Object> xmlMap = new HashMap<>(1);
                    xmlMap.put("line",new HashMap<String,Object>(1){{
                        put("outno", shipno);
                    }});
                    wmsService.FEWMS009(xmlMap);
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
        resultMap.put("carlist",exchangeService.getCarList(outno, sTime, eTime));
        return resultMap;
    }

    /**
     * 根据出库单获取车牌信息页面
     * @param outno
     * @param sTime
     * @param eTime
     * @return ModelAndView
     */
    @RequestMapping("/selectCarView")
    public ModelAndView selectCarView(String outno, String sTime, String eTime){
        ModelAndView mv = this.getModelAndView();
        mv.addObject("carlist",exchangeService.getCarList(outno,sTime,eTime));
        mv.setViewName("techbloom/outstorage/outstorage/selectCar");
        return mv;
    }

    /**
     * 保存物资流转单信息
     * @param outno 发货单号
     * @param carno 车牌号
     * @return Map<String,Object>
     */
    @RequestMapping("/savePrintInfo")
    @ResponseBody
    public Map<String, Object> savePrintInfo(String outno,String carno){
        Map<String, Object> returnMap = new HashMap<>(1);
        ExchangePrint exchangePrint = new ExchangePrint();
        exchangePrint.setCar(carno);
        if(!exchangeService.isExistInfo(exchangePrint)){
            exchangePrintService.insert(exchangePrint);
        }
        return returnMap;
    }

    /**
     * 根据车牌及发货单验证是否存在
     * @param outno 发货单号
     * @param carno 车牌号
     * @return Map<String,Object>
     */
    @RequestMapping("/validateTrue")
    @ResponseBody
    public Map<String,Object> validateTrue(String outno,String carno){
        Map<String,Object> map = new HashMap<>(1);
        map.put("issuccess", exchangeService.validateTrue(outno,carno));
        return map;
    }

    /**
     * 打印
     * @param outno 发货单号
     * @param carno 车牌号
     * @param e_date 截止时间
     * @param s_date 起始时间
     * @return Map<String,Object>
     */
    @RequestMapping("/print")
    @ResponseBody
    public Map<String,Object> print(String outno, String carno, String e_date, String s_date){
        Map<String,Object> map = new HashMap<>(1);
        //更新物资流转单号
        String oddnumber = exchangeService.updateOddNumber(outno, carno);
        map.put("outno", outno);
        map.put("carno", carno);
        map.put("e_date", e_date);
        map.put("s_date", s_date);
        map.put("oddnumber", oddnumber);
        map.put("exchangePrintInfo",exchangeService.getPrintInfo(map));
        return map;
    }


}
