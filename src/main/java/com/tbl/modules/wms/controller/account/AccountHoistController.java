package com.tbl.modules.wms.controller.account;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Maps;
import com.tbl.common.utils.DateUtils;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.platform.constant.LogActionConstant;
import com.tbl.modules.platform.constant.MenuConstant;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.service.system.LogService;
import com.tbl.modules.platform.service.system.RoleService;
import com.tbl.modules.platform.util.DeriveExcel;
import com.tbl.modules.wms.service.baseinfo.CarService;
import com.tbl.modules.wms.service.instorage.InstorageService;
import com.tbl.modules.wms.service.outstorage.OutStorageDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 台账管理
 * @author 70486
 */
@Controller
@RequestMapping(value = "/AccountHoist")
public class AccountHoistController extends AbstractController {

    /**
     * 入库管理-入库表
     */
    @Autowired
    private InstorageService instorageService;
    /**
     * 日志管理
     */
    @Autowired
    private LogService logService;
    /**
     * 出库单明细详情
     */
    @Autowired
    private OutStorageDetailService outStorageDetailService;
    /**
     * 用户权限管理
     */
    @Autowired
    private RoleService roleService;
    /**
     * 行车
     */
    @Autowired
    private CarService carService;

    /**
     * 跳转到台账复绕列表页
     * @return ModelAndView
     */
    @RequestMapping(value = "/toRewindList")
    public ModelAndView toRewindList() {
        ModelAndView mv = this.getModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(getSessionUser().getRoleId(), MenuConstant.rewind));
        mv.setViewName("techbloom/Account/rewind/rewind_list");
        return mv;
    }

    /**
     * 获取台账复绕列表数据
     * @param queryJsonString
     * @return Map<String,Object>
     */
    @RequestMapping(value = "/rewindList.do")
    @ResponseBody
    public Map<String, Object> rewindList(String queryJsonString) {
        Map<String, Object> map = new HashMap<>(5);
        if (!StringUtils.isEmptyString(queryJsonString)) {
            map = JSON.parseObject(queryJsonString);
        }
        //已复绕
        map.put("state", 9);
        PageTbl page = this.getPage();
        PageUtils utils = instorageService.getPackDetailPageList(page, map);
        page.setTotalRows(utils.getTotalCount() == 0 ? 1 : utils.getTotalCount());
        //初始化分页对象
        executePageMap(map, page);
        map.put("rows", utils.getList());
        map.put("total", utils.getTotalPage() == 0 ? 1 : utils.getTotalPage());
        return map;
    }

    /**
     * 导出复绕报表Excel
     * @param request
     * @param response
     */
    @RequestMapping(value = "/rewindExcel", method = RequestMethod.POST)
    @ResponseBody
    public void rewindExcel(HttpServletRequest request, HttpServletResponse response){
        Map<String, Object> map = new HashMap<>(12);
        map.put("ids", StringUtils.stringToInt(request.getParameter("ids")));
        map.put("qaCode", request.getParameter("queryQaCode"));
        map.put("entityNo", request.getParameter("queryEntityNo"));
        map.put("rewindStartTime", request.getParameter("queryRewindStartTime"));
        map.put("rewindEndTime", request.getParameter("queryRewindEndTime"));
        map.put("factoryarea", request.getParameter("factoryarea"));
        map.put("mcode", request.getParameter("mcode"));
        map.put("orderno", request.getParameter("orderno"));
        map.put("batchno", request.getParameter("batchno"));
        map.put("rewindname", request.getParameter("queryRewindname"));
        //已吊装
        map.put("state", 9);

        try {
            String sheetName = "复绕台账" + "(" + DateUtils.getDay() + ")";
            response.setHeader("Content-Type", "application/force-download");
            response.setHeader("Content-Type", "application/vnd.ms-excel");
            response.setCharacterEncoding("UTF-8");
            response.setHeader("Expires", "0");
            response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
            response.setHeader("Pragma", "public");
            response.setHeader("Content-disposition", "attachment;filename=" + new String(sheetName.getBytes("gbk"),
                    "ISO8859-1") + ".xls");

            Map<String, String> mapFields = new LinkedHashMap<>();
            String [] excelIndexArray = request.getParameter("queryExportExcelIndex").split(",");

            if (excelIndexArray.length==1&&"".equals(excelIndexArray[0])) {
                mapFields.put("rewindStateStr", "状态");
                mapFields.put("qaCode", "质保单号");
                mapFields.put("batchNo", "批次号");
                mapFields.put("ordernum", "订单号");
                mapFields.put("orderline", "订单行号");
                mapFields.put("entityNo", "工单号");
                mapFields.put("name", "厂区");
                mapFields.put("materialCode", "物料编码");
                mapFields.put("materialName", "物料名称");
                mapFields.put("rewindTimeStr","复绕时间");
                mapFields.put("rewindName","复绕工");
                mapFields.put("meter", "数量");
                mapFields.put("unit", "单位");
                mapFields.put("weight", "重量");
                mapFields.put("colour", "颜色");
                mapFields.put("segmentno", "段号");
                mapFields.put("dishnumber", "盘具编码");
                mapFields.put("model", "盘规格");
                mapFields.put("outerDiameter", "盘外径");
                mapFields.put("storagetypeStr", "货物类型");
            }else {
                for (String s : excelIndexArray) {
                    if ("2".equals(s)) {
                        mapFields.put("hoistStateStr", "状态");
                    } else if ("3".equals(s)) {
                        mapFields.put("qaCode", "质保单号");
                    } else if ("4".equals(s)) {
                        mapFields.put("batchNo", "批次号");
                    } else if ("5".equals(s)) {
                        mapFields.put("ordernum", "订单号");
                    } else if ("6".equals(s)) {
                        mapFields.put("orderline", "订单行号");
                    } else if ("7".equals(s)) {
                        mapFields.put("entityNo", "工单号");
                    } else if ("8".equals(s)) {
                        mapFields.put("name", "厂区");
                    } else if ("9".equals(s)) {
                        mapFields.put("materialCode", "物料编码");
                    } else if ("10".equals(s)) {
                        mapFields.put("materialName", "物料名称");
                    } else if ("11".equals(s)) {
                        mapFields.put("hoistingTimeStr", "吊装时间");
                    } else if ("12".equals(s)) {
                        mapFields.put("hoistingName", "吊装工");
                    } else if ("13".equals(s)) {
                        mapFields.put("meter", "数量");
                    } else if ("14".equals(s)) {
                        mapFields.put("unit", "单位");
                    } else if ("15".equals(s)) {
                        mapFields.put("weight", "重量");
                    } else if ("16".equals(s)) {
                        mapFields.put("colour", "颜色");
                    } else if ("17".equals(s)) {
                        mapFields.put("segmentno", "段号");
                    } else if ("18".equals(s)) {
                        mapFields.put("dishnumber", "盘具编码");
                    } else if ("19".equals(s)) {
                        mapFields.put("model", "盘规格");
                    } else if ("20".equals(s)) {
                        mapFields.put("outerDiameter", "盘外径");
                    } else if ("21".equals(s)) {
                        mapFields.put("storagetypeStr", "货物类型");
                    }
                }
            }

            DeriveExcel.exportExcel(sheetName, instorageService.getAccountHoistExcelList(map), mapFields, response, "");
            logService.logInsert("吊装台账导出", LogActionConstant.USER_EXPORT, request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 跳转到台账吊装列表页
     * @return ModelAndView
     */
    @RequestMapping(value = "/toList")
    public ModelAndView toList() {
        ModelAndView mv = this.getModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(getSessionUser().getRoleId(), MenuConstant.hoistList));
        mv.setViewName("techbloom/Account/hoist/hoist_list");
        return mv;
    }

    /**
     * 获取台账吊装列表数据
     * @param queryJsonString
     * @return Map<String,Object>
     */
    @RequestMapping(value = "/list.do")
    @ResponseBody
    public Map<String, Object> list(String queryJsonString) {
        Map<String, Object> map = new HashMap<>(5);
        if (!StringUtils.isEmptyString(queryJsonString)) {
            map = JSON.parseObject(queryJsonString);
        }
        //已吊装
        map.put("state", 10);
        PageTbl page = this.getPage();
        PageUtils utils = instorageService.getPackDetailPageList(page, map);
        page.setTotalRows(utils.getTotalCount() == 0 ? 1 : utils.getTotalCount());
        //初始化分页对象
        executePageMap(map, page);
        map.put("rows", utils.getList());
        map.put("total", utils.getTotalPage() == 0 ? 1 : utils.getTotalPage());
        return map;
    }

    /**
     * 导出吊装台账Excel
     * @param request
     * @param response
     */
    @RequestMapping(value = "/materialExcel", method = RequestMethod.POST)
    @ResponseBody
    public void materialExcel(HttpServletRequest request, HttpServletResponse response){
        Map<String, Object> map = new HashMap<>(12);
        map.put("ids", StringUtils.stringToInt(request.getParameter("ids")));
        map.put("qaCode", request.getParameter("queryQaCode"));
        map.put("entityNo", request.getParameter("queryEntityNo"));
        map.put("hoistStartTime", request.getParameter("queryHoistStartTime"));
        map.put("hoistEndTime", request.getParameter("queryHoistEndTime"));
        map.put("factoryarea", request.getParameter("factoryarea"));
        map.put("mcode", request.getParameter("mcode"));
        map.put("orderno", request.getParameter("orderno"));
        map.put("batchno", request.getParameter("batchno"));
        map.put("hoistname", request.getParameter("queryHoistname"));
        //已吊装
        map.put("state", 10);

        try {
            String sheetName = "吊装台账" + "(" + DateUtils.getDay() + ")";
            response.setHeader("Content-Type", "application/force-download");
            response.setHeader("Content-Type", "application/vnd.ms-excel");
            response.setCharacterEncoding("UTF-8");
            response.setHeader("Expires", "0");
            response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
            response.setHeader("Pragma", "public");
            response.setHeader("Content-disposition", "attachment;filename=" + new String(sheetName.getBytes("gbk"),
                    "ISO8859-1") + ".xls");

            Map<String, String> mapFields = new LinkedHashMap<>();
            String [] excelIndexArray = request.getParameter("queryExportExcelIndex").split(",");

            if (excelIndexArray.length==1&&"".equals(excelIndexArray[0])) {
                mapFields.put("hoistStateStr", "状态");
                mapFields.put("qaCode", "质保单号");
                mapFields.put("batchNo", "批次号");
                mapFields.put("ordernum", "订单号");
                mapFields.put("orderline", "订单行号");
                mapFields.put("entityNo", "工单号");
                mapFields.put("name", "厂区");
                mapFields.put("shelfCode", "库位");
                mapFields.put("materialCode", "物料编码");
                mapFields.put("materialName", "物料名称");
                mapFields.put("hoistingTimeStr","吊装时间");
                mapFields.put("hoistingName","吊装工");
                mapFields.put("meter", "数量");
                mapFields.put("unit", "单位");
                mapFields.put("weight", "重量");
                mapFields.put("colour", "颜色");
                mapFields.put("segmentno", "段号");
                mapFields.put("dishnumber", "盘具编码");
                mapFields.put("model", "盘规格");
                mapFields.put("outerDiameter", "盘外径");
                mapFields.put("storagetypeStr", "货物类型");
            }else {
                for (String s : excelIndexArray) {
                    if ("2".equals(s)) {
                        mapFields.put("hoistStateStr", "状态");
                    } else if ("3".equals(s)) {
                        mapFields.put("qaCode", "质保单号");
                    } else if ("4".equals(s)) {
                        mapFields.put("batchNo", "批次号");
                    } else if ("5".equals(s)) {
                        mapFields.put("ordernum", "订单号");
                    } else if ("6".equals(s)) {
                        mapFields.put("orderline", "订单行号");
                    } else if ("7".equals(s)) {
                        mapFields.put("entityNo", "工单号");
                    } else if ("8".equals(s)) {
                        mapFields.put("name", "厂区");
                    } else if ("9".equals(s)) {
                        mapFields.put("shelfCode", "库位");
                    } else if ("10".equals(s)) {
                        mapFields.put("materialCode", "物料编码");
                    } else if ("11".equals(s)) {
                        mapFields.put("materialName", "物料名称");
                    } else if ("12".equals(s)) {
                        mapFields.put("hoistingTimeStr", "吊装时间");
                    } else if ("13".equals(s)) {
                        mapFields.put("hoistingName", "吊装工");
                    } else if ("14".equals(s)) {
                        mapFields.put("meter", "数量");
                    } else if ("15".equals(s)) {
                        mapFields.put("unit", "单位");
                    } else if ("16".equals(s)) {
                        mapFields.put("weight", "重量");
                    } else if ("17".equals(s)) {
                        mapFields.put("colour", "颜色");
                    } else if ("18".equals(s)) {
                        mapFields.put("segmentno", "段号");
                    } else if ("19".equals(s)) {
                        mapFields.put("dishnumber", "盘具编码");
                    } else if ("20".equals(s)) {
                        mapFields.put("model", "盘规格");
                    } else if ("21".equals(s)) {
                        mapFields.put("outerDiameter", "盘外径");
                    } else if ("22".equals(s)) {
                        mapFields.put("storagetypeStr", "货物类型");
                    }
                }
            }

            DeriveExcel.exportExcel(sheetName, instorageService.getAccountHoistExcelList(map), mapFields, response, "");
            logService.logInsert("吊装台账导出", LogActionConstant.USER_EXPORT, request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 跳转到台账包装列表页
     * @return ModelAndView
     */
    @RequestMapping(value = "/toPackingList")
    public ModelAndView toPackingList() {
        ModelAndView mv = this.getModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(getSessionUser().getRoleId(), MenuConstant.packagIng));
        mv.setViewName("techbloom/Account/packing/packing_list");
        return mv;
    }

    /**
     * 获取台账包装列表数据
     * @param queryJsonString
     * @return Map<String,Object>
     */
    @RequestMapping(value = "/packingList.do")
    @ResponseBody
    public Map<String, Object> packingList(String queryJsonString) {
        Map<String, Object> map = new HashMap<>(5);
        if (!StringUtils.isEmptyString(queryJsonString)) {
            map = JSON.parseObject(queryJsonString);
        }
        map.put("state", 8);
        PageTbl page = this.getPage();
        PageUtils utils = instorageService.getPackDetailPageList(page, map);
        page.setTotalRows(utils.getTotalCount() == 0 ? 1 : utils.getTotalCount());
        //初始化分页对象
        executePageMap(map, page);
        map.put("rows", utils.getList());
        map.put("total", utils.getTotalPage() == 0 ? 1 : utils.getTotalPage());
        return map;
    }

    /**
     * 包装台账导出Excel
     * @param request
     * @param response
     */
    @RequestMapping(value = "/packingExcel", method = RequestMethod.POST)
    @ResponseBody
    public void packingExcel(HttpServletRequest request, HttpServletResponse response){
        Map<String, Object> map = new HashMap<>(11);
        map.put("ids", StringUtils.stringToInt(request.getParameter("ids")));
        map.put("qaCode", request.getParameter("queryQaCode"));
        map.put("entityNo", request.getParameter("queryEntityNo"));
        map.put("confirmStartTime", request.getParameter("queryConfirmStartTime"));
        map.put("confirmEndTime", request.getParameter("queryConfirmEndTime"));
        map.put("factoryarea", request.getParameter("factoryarea"));
        map.put("mcode", request.getParameter("mcode"));
        map.put("orderno", request.getParameter("orderno"));
        map.put("batchno", request.getParameter("batchno"));
        map.put("confirmname", request.getParameter("queryConfirmname"));
        map.put("state", 8);

        try {
            String sheetName = "包装台账" + "(" + DateUtils.getDay() + ")";
            response.setHeader("Content-Type", "application/force-download");
            response.setHeader("Content-Type", "application/vnd.ms-excel");
            response.setCharacterEncoding("UTF-8");
            response.setHeader("Expires", "0");
            response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
            response.setHeader("Pragma", "public");
            response.setHeader("Content-disposition", "attachment;filename=" + new String(sheetName.getBytes("gbk"),
                    "ISO8859-1") + ".xls");

            Map<String, String> mapFields = new LinkedHashMap<>();
            String [] excelIndexArray = request.getParameter("queryExportExcelIndex").split(",");

            if (excelIndexArray.length==1&&"".equals(excelIndexArray[0])) {
                mapFields.put("stateStr", "状态");
                mapFields.put("qaCode", "质保单号");
                mapFields.put("batchNo", "批次号");
                mapFields.put("ordernum", "订单号");
                mapFields.put("orderline", "订单行号");
                mapFields.put("entityNo", "工单号");
                mapFields.put("name", "厂区");
                mapFields.put("materialCode", "物料编码");
                mapFields.put("materialName", "物料名称");
                mapFields.put("confirmTimeStr","包装时间");
                mapFields.put("confirmName","包装工");
                mapFields.put("meter", "数量");
                mapFields.put("unit", "单位");
                mapFields.put("weight", "重量");
                mapFields.put("colour", "颜色");
                mapFields.put("segmentno", "段号");
                mapFields.put("dishnumber", "盘具编码");
                mapFields.put("model", "盘规格");
                mapFields.put("outerDiameter", "盘外径");
                mapFields.put("storagetypeStr", "货物类型");
            }else {
                for (String s : excelIndexArray) {
                    if ("2".equals(s)) {
                        mapFields.put("stateStr", "状态");
                    } else if ("3".equals(s)) {
                        mapFields.put("qaCode", "质保单号");
                    } else if ("4".equals(s)) {
                        mapFields.put("batchNo", "批次号");
                    } else if ("5".equals(s)) {
                        mapFields.put("ordernum", "订单号");
                    } else if ("6".equals(s)) {
                        mapFields.put("orderline", "订单行号");
                    } else if ("7".equals(s)) {
                        mapFields.put("entityNo", "工单号");
                    } else if ("8".equals(s)) {
                        mapFields.put("name", "厂区");
                    } else if ("9".equals(s)) {
                        mapFields.put("materialCode", "物料编码");
                    } else if ("10".equals(s)) {
                        mapFields.put("materialName", "物料名称");
                    } else if ("11".equals(s)) {
                        mapFields.put("confirmTimeStr", "吊装时间");
                    } else if ("12".equals(s)) {
                        mapFields.put("confirmName", "吊装工");
                    } else if ("13".equals(s)) {
                        mapFields.put("meter", "数量");
                    } else if ("14".equals(s)) {
                        mapFields.put("unit", "单位");
                    } else if ("15".equals(s)) {
                        mapFields.put("weight", "重量");
                    } else if ("16".equals(s)) {
                        mapFields.put("colour", "颜色");
                    } else if ("17".equals(s)) {
                        mapFields.put("segmentno", "段号");
                    } else if ("18".equals(s)) {
                        mapFields.put("dishnumber", "盘具编码");
                    } else if ("19".equals(s)) {
                        mapFields.put("model", "盘规格");
                    } else if ("20".equals(s)) {
                        mapFields.put("outerDiameter", "盘外径");
                    } else if ("21".equals(s)) {
                        mapFields.put("storagetypeStr", "货物类型");
                    }
                }
            }

            DeriveExcel.exportExcel(sheetName, instorageService.getAccountPackingExcelList(map), mapFields, response, "");
            logService.logInsert("装包台账导出", LogActionConstant.USER_EXPORT, request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 跳转到入库管理列表页面
     * @return ModelAndView
     */
    @RequestMapping(value = "/toStorageinList")
    public ModelAndView toStorageinList() {
        ModelAndView mv = this.getModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(getSessionUser().getRoleId(), MenuConstant.storagIng));
        mv.setViewName("techbloom/Account/storagein/instorage_list");
        return mv;
    }

    /**
     * 获取已完成入库列表数据
     * @param queryJsonString
     * @return Map<String,Object>
     */
    @RequestMapping(value = "/instorageList.do")
    @ResponseBody
    public Map<String, Object> instorageList(String queryJsonString) {
        Map<String, Object> map = new HashMap<>(4);
        if (!StringUtils.isEmptyString(queryJsonString)) {
            map = JSON.parseObject(queryJsonString);
        }
        map.put("timeType", "3");
        map.put("state", "4");
        PageTbl page = this.getPage();
        PageUtils utils = instorageService.getPagePackageList(page, map);
        page.setTotalRows(utils.getTotalCount() == 0 ? 1 : utils.getTotalCount());
        //初始化分页对象
        executePageMap(map, page);
        map.put("rows", utils.getList());
        map.put("total", utils.getTotalPage() == 0 ? 1 : utils.getTotalPage());
        return map;
    }

    /**
     * 入库台账导出Excel
     * @param request
     * @param response
     */
    @RequestMapping(value = "/instoragelExcel", method = RequestMethod.POST)
    @ResponseBody
    public void instoragelExcel(HttpServletRequest request, HttpServletResponse response)  {
        Map<String, Object> map = new HashMap<>(15);
        if (request.getParameter("ids")!=null && !"".equals(request.getParameter("ids"))){
            map.put("idList", Arrays.stream(request.getParameter("ids").split(","))
                    .map(Long::parseLong).collect(Collectors.toList()));
        }
        map.put("qaCode", request.getParameter("queryQaCode"));
        map.put("batchno", request.getParameter("queryBatchno"));
        map.put("orderno", request.getParameter("queryDdh"));
        map.put("factoryarea", request.getParameter("queryCq"));
        map.put("mcode", request.getParameter("queryWlbh"));
        map.put("startTime", request.getParameter("queryStartTime"));
        map.put("endTime", request.getParameter("queryEndTime"));
        map.put("entityNo", request.getParameter("queryEntityNo"));
        map.put("state", Arrays.stream("4".split(",")).map(Long::parseLong).collect(Collectors.toList()));
        map.put("timeType", "3");
        map.put("instorageName", request.getParameter("instorageName"));
        try {
            String sheetName = "入库台账" + "(" + DateUtils.getDay() + ")";
            response.setHeader("Content-Type", "application/force-download");
            response.setHeader("Content-Type", "application/vnd.ms-excel");
            response.setCharacterEncoding("UTF-8");
            response.setHeader("Expires", "0");
            response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
            response.setHeader("Pragma", "public");
            response.setHeader("Content-disposition", "attachment;filename=" + new String(sheetName.getBytes("gbk"),
                    "ISO8859-1") + ".xls");

            String [] excelIndexArray = request.getParameter("queryExportExcelIndex").split(",");
            Map<String, String> mapFields = new LinkedHashMap<>();
            if (excelIndexArray.length==1&&"".equals(excelIndexArray[0])){
                mapFields.put("stateCode", "状态");
                mapFields.put("qaCode", "质保单号");
                mapFields.put("batchNo", "批次号");
                mapFields.put("ordernum", "订单号");
                mapFields.put("orderline", "订单行号");
                mapFields.put("entityNo", "工单号");
                mapFields.put("name", "组织机构");
                mapFields.put("factoryName", "厂区");
                mapFields.put("warehouseName", "仓库");
                mapFields.put("shelfCode", "库位");
                mapFields.put("materialCode", "物料编码");
                mapFields.put("materialName", "物料名称");
                mapFields.put("createtimeStr", "初始化时间");
                mapFields.put("confirmTimeStr", "包装时间");
                mapFields.put("confirmName", "包装人姓名");
                mapFields.put("startStorageTimeStr", "扫码时间");
                mapFields.put("startstorageName", "扫码人");
                mapFields.put("instorageTimeStr", "入库时间");
                mapFields.put("instorageName", "入库人姓名");
                mapFields.put("meter", "数量");
                mapFields.put("unit", "单位");
                mapFields.put("weight", "重量");
                mapFields.put("colour", "颜色");
                mapFields.put("segmentno", "段号");
                mapFields.put("inspectno", "报验单号");
                mapFields.put("dishcode", "盘号");
                mapFields.put("dishnumber", "盘具编码");
                mapFields.put("model", "盘规格");
                mapFields.put("outerDiameter", "盘外径");
                mapFields.put("instorageType", "货物类型");
            }else {
                for (String s : excelIndexArray) {
                    if ("2".equals(s)) {
                        mapFields.put("stateCode", "状态");
                    } else if ("3".equals(s)) {
                        mapFields.put("qaCode", "质保单号");
                    } else if ("4".equals(s)) {
                        mapFields.put("batchNo", "批次号");
                    } else if ("5".equals(s)) {
                        mapFields.put("ordernum", "订单号");
                    } else if ("6".equals(s)) {
                        mapFields.put("orderline", "订单行号");
                    } else if ("7".equals(s)) {
                        mapFields.put("entityNo", "工单号");
                    } else if ("8".equals(s)) {
                        mapFields.put("name", "组织机构");
                    } else if ("9".equals(s)) {
                        mapFields.put("factoryName", "厂区");
                    } else if ("10".equals(s)) {
                        mapFields.put("warehouseName", "仓库");
                    } else if ("11".equals(s)) {
                        mapFields.put("shelfCode", "库位");
                    } else if ("12".equals(s)) {
                        mapFields.put("materialCode", "物料编码");
                    } else if ("13".equals(s)) {
                        mapFields.put("materialName", "物料名称");
                    } else if ("14".equals(s)) {
                        mapFields.put("createtimeStr", "初始化时间");
                    } else if ("15".equals(s)) {
                        mapFields.put("confirmTimeStr", "包装时间");
                    } else if ("16".equals(s)) {
                        mapFields.put("confirmName", "包装人姓名");
                    } else if ("17".equals(s)) {
                        mapFields.put("startStorageTimeStr", "扫码时间");
                    } else if ("18".equals(s)) {
                        mapFields.put("startstorageName", "扫码人");
                    } else if ("19".equals(s)) {
                        mapFields.put("instorageTimeStr", "入库时间");
                    } else if ("20".equals(s)) {
                        mapFields.put("instorageName", "入库人姓名");
                    } else if ("21".equals(s)) {
                        mapFields.put("meter", "数量");
                    } else if ("22".equals(s)) {
                        mapFields.put("unit", "单位");
                    } else if ("23".equals(s)) {
                        mapFields.put("weight", "重量");
                    } else if ("24".equals(s)) {
                        mapFields.put("colour", "颜色");
                    } else if ("25".equals(s)) {
                        mapFields.put("segmentno", "段号");
                    } else if ("26".equals(s)) {
                        mapFields.put("inspectno", "报验单号");
                    } else if ("27".equals(s)) {
                        mapFields.put("dishcode", "盘号");
                    } else if ("28".equals(s)) {
                        mapFields.put("dishnumber", "盘具编码");
                    } else if ("29".equals(s)) {
                        mapFields.put("model", "盘规格");
                    } else if ("30".equals(s)) {
                        mapFields.put("outerDiameter", "盘外径");
                    } else if ("31".equals(s)) {
                        mapFields.put("instorageType", "货物类型");
                    }
                }
            }

            DeriveExcel.exportExcel(sheetName, instorageService.getPackageInExcelList(map), mapFields, response, "");
            logService.logInsert("包装入库管理数据导出", LogActionConstant.USER_EXPORT, request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 跳转到出库台账页面
     * @return ModelAndView
     */
    @RequestMapping(value = "/toOutStorageList")
    public ModelAndView toOutStorageList() {
        ModelAndView mv = this.getModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(getSessionUser().getRoleId(),
                MenuConstant.outstoragIng));
        mv.setViewName("techbloom/Account/storageout/storageOut_list");
        return mv;
    }

    /**
     * 获取出库台账列表数据
     * @param queryJsonString
     * @return Map<String,Object>
     */
    @RequestMapping(value = "/outstoragelist.do")
    @ResponseBody
    public Map<String, Object> outstoragelist(String queryJsonString) {
        Map<String, Object> map = new HashMap<>(2);
        if (!StringUtils.isEmpty(queryJsonString)) {
            map = JSON.parseObject(queryJsonString);
        }

        PageTbl page = this.getPage();
        String sortName = page.getSortname();
        if (StringUtils.isEmptyString(sortName)) {
            sortName = "";
            page.setSortname(sortName);
        }
        String sortOrder = page.getSortorder();
        if (StringUtils.isEmptyString(sortOrder)) {
            sortOrder = "desc";
            page.setSortorder(sortOrder);
        }
        PageUtils pageList = outStorageDetailService.getList(page,map);
        page.setTotalRows(pageList.getTotalCount() == 0 ? 1 : pageList.getTotalCount());
        map.put("rows", pageList.getList());
        executePageMap(map, page);
        return map;
    }

    /**
     * 出库台账导出Excel
     */
    @RequestMapping(value = "/toStorageOutExcel.do")
    @ResponseBody
    public void toStorageOutExcel() {
        Map<String,Object> map = Maps.newHashMap();
        map.put("qaCode",request.getParameter("qaCode"));
        map.put("startTime",request.getParameter("startTime"));
        map.put("endTime",request.getParameter("endTime"));
        map.put("ids",StringUtils.stringToInt(request.getParameter("ids")));
        map.put("outstorageName", request.getParameter("outstorageName"));
        map.put("shipNo", request.getParameter("shipNo"));
        outStorageDetailService.toOutStorageExcel(response, "", outStorageDetailService.getAllLists(map),
                request.getParameter("queryExportExcelIndex").split(","));
    }

    /**
     * 跳转到行吊台账页面
     * @return ModelAndView
     */
    @RequestMapping(value = "/toHangCarList")
    public ModelAndView toHangCarList() {
        ModelAndView mv = this.getModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(getSessionUser().getRoleId(),
                MenuConstant.hangCarList));
        mv.setViewName("techbloom/Account/hangcar/hangcar_list");
        return mv;
    }

    /**
     * 获取行吊台账列表数据
     * @param queryJsonString
     * @return Map<String,Object>
     */
    @RequestMapping(value = "/hangtCarlist.do")
    @ResponseBody
    public Map<String, Object> hangtCarlist(String queryJsonString) {
        Map<String, Object> map = new HashMap<>(2);
        if (!StringUtils.isEmpty(queryJsonString)) {
            map = JSON.parseObject(queryJsonString);
        }

        PageTbl page = this.getPage();
        String sortName = page.getSortname();
        if (StringUtils.isEmptyString(sortName)) {
            sortName = "";
            page.setSortname(sortName);
        }
        String sortOrder = page.getSortorder();
        if (StringUtils.isEmptyString(sortOrder)) {
            sortOrder = "desc";
            page.setSortorder(sortOrder);
        }
        PageUtils pageList = carService.getHangCarList(page,map);
        page.setTotalRows(pageList.getTotalCount() == 0 ? 1 : pageList.getTotalCount());
        map.put("rows", pageList.getList());
        executePageMap(map, page);
        return map;
    }

    /**
     * 行吊台账导出Excel
     */
    @RequestMapping(value = "/toHangCarExcel.do")
    @ResponseBody
    public void toHangCarExcel() {
        Map<String,Object> map = Maps.newHashMap();
        map.put("startTime",request.getParameter("startTime"));
        map.put("endTime",request.getParameter("endTime"));
        map.put("ids",StringUtils.stringToInt(request.getParameter("ids")));
        map.put("carerName", request.getParameter("carerName"));
        carService.toHangCarerExcel(response, "", carService.getAllHangCarLists(map),
                request.getParameter("queryExportExcelIndex").split(","));
    }
}
