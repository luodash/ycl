package com.tbl.modules.wms.controller.system;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Maps;
import com.tbl.common.utils.DateUtils;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.platform.constant.MenuConstant;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.service.system.RoleService;
import com.tbl.modules.wms.entity.productbind.DeviceUwbRoute;
import com.tbl.modules.wms.service.baseinfo.FactoryAreaService;
import com.tbl.modules.wms.service.productbind.DeviceUwbRouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * UWB管理控制层
 * @author 70486
 */
@Controller
@RequestMapping(value = "/uwb")
public class UwbController extends AbstractController {

    /**
     * 角色服务类
     */
    @Autowired
    private RoleService roleService;
    /**
     * 厂区信息服务类
     */
    @Autowired
    private FactoryAreaService factoryAreaService;
    /**
     * uwb
     */
    @Autowired
    private DeviceUwbRouteService deviceUwbRouteService;

    /**
     * 跳转到UWB维护列表页面
     * @return ModelAndView
     */
    @RequestMapping(value = "/toList.do")
    @ResponseBody
    public ModelAndView list() {
        ModelAndView mv = new ModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(getSessionUser().getRoleId(), MenuConstant.uwbConstant));
        mv.setViewName("techbloom/system/uwb/uwb_list");
        return mv;
    }

    /**
     * 获取UWB维护列表数据
     * @param queryJsonString 查询条件
     * @return Object UWB维护列表数据
     */
    @RequestMapping(value = "/list.do")
    @ResponseBody
    public Object list(String queryJsonString) {
    	Map<String, Object> map = new HashMap<>(6);
        if (!StringUtils.isEmpty(queryJsonString)) {
            map = JSON.parseObject(queryJsonString);
        }
        PageTbl page = this.getPage();
        map.put("page", page.getPageno());
        map.put("limit", page.getPagesize());
        String sortName = page.getSortname();
        if (StringUtils.isEmptyString(sortName)) {
            sortName = "id";
            page.setSortname(sortName);
        }
        String sortOrder = page.getSortorder();
        if (StringUtils.isEmptyString(sortOrder)) {
            sortOrder = "desc";
            page.setSortorder(sortOrder);
        }
        map.put("sidx", page.getSortname());
        map.put("order", page.getSortorder());
        map.put("suserid", getSessionUser().getUserId());
        PageUtils pageUser = deviceUwbRouteService.queryPage(map);
        page.setTotalRows(pageUser.getTotalCount() == 0 ? 1 : pageUser.getTotalCount());
        map.put("rows", pageUser.getList());
        executePageMap(map, page);
        return map;
    }


    /**
     * 返回到UWB添加页面
     * @return ModelAndView
     */
    @RequestMapping(value = "/uwbAdd")
    public ModelAndView uwbAdd() {
        ModelAndView mv=this.getModelAndView();
        mv.addObject("factlist",factoryAreaService.getFactoryList());
        mv.setViewName("techbloom/system/uwb/uwb_add");
        return mv;
    }

    /**
     * 保存/修改uwb信息
     * @param deviceUwbRoute uwb标签信息
     * @return Map<String,Object>
     */
    @RequestMapping(value = "/save")
    @ResponseBody
    public Map<String, Object> save(DeviceUwbRoute deviceUwbRoute) {
        Map<String, Object> map = Maps.newHashMap();
        deviceUwbRoute.setCreatetime(DateUtils.getTime());
        map.put("result", deviceUwbRoute.getId() != null && deviceUwbRoute.getId() != -1 ? deviceUwbRouteService.updateById(deviceUwbRoute) :
                deviceUwbRouteService.insert(deviceUwbRoute));
        return map;
    }

    /**
     * 返回到uwb修改页面
     * @param id uwb主键
     * @return ModelAndView
     */
    @RequestMapping(value = "/uwb_edit")
    public ModelAndView uwbEdit(Long id) {
    	ModelAndView mv = new ModelAndView();
        mv.addObject("tag",deviceUwbRouteService.selectById(id).getTag());
        mv.addObject("id",id);
        mv.setViewName("techbloom/system/uwb/uwb_edit");
        return mv;
    }

    /**
     * uwb删除
     * @param ids uwb主键
     * @return Boolean 成功/失败
     */
    @RequestMapping(value = "/delUwb")
    @ResponseBody
    public Boolean delUwb(String[] ids) {
        return deviceUwbRouteService.deleteBatchIds(Arrays.stream(StringUtils.join(ids, ",", true).replaceAll("'", "")
                .split(",")).map(s -> Long.parseLong(s.trim())).collect(Collectors.toList()));
    }

    /**
     * 导出Excel
     */
    @RequestMapping(value = "/toExcel")
    @ResponseBody
    public void artBomExcel() {
        deviceUwbRouteService.toExcel(response, "", deviceUwbRouteService.getAllLists(request.getParameter("ids")));
    }

}
