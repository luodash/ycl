package com.tbl.modules.wms.controller.pda;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.tbl.common.utils.YamlConfigurerUtil;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.wms.constant.EmumConstant;
import com.tbl.modules.wms.constant.PdaResult;
import com.tbl.modules.wms.entity.baseinfo.BaseDicType;
import com.tbl.modules.wms.service.baseinfo.BaseDicTypeService;
import com.tbl.modules.wms.service.pda.PdaService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * 手持机API接口服务端
 * @author 70486
 */
@RestController
@RequestMapping(value = "/pda")
public class PdaController extends AbstractController {

    /**
     * 手持机
     */
    @Autowired
    private PdaService pdaService;

    /**
     * 下拉框选择项服务类
     */
    @Autowired
    private BaseDicTypeService baseDicTypeService;

    /**
     * 用户登陆
     * @param username 用户名
     * @param password 密码
     * @return PdaResult
     */
    @RequestMapping(value = "/login")
    @ResponseBody
    public PdaResult findBaseInfoByMaterialBarCode(String username, String password) {
        return pdaService.login(username, password, this.getRequest().getSession());
    }

    /**
     * 台账类型获取
     * @return PdaResult 台账类别列表
     */
    @RequestMapping(value = "/getLedgerType")
    @ResponseBody
    public PdaResult getLedgerType() {
        return new PdaResult(0,"",baseDicTypeService.selectList(new EntityWrapper<BaseDicType>()
                .eq("DESCRIPTION", "LEDGER")));
    }

    /**
     * 装包:获取物料详情列表
     * @param rfid rfid
     * @param userId 用户主键
     * @param barcode 二维码号
     * @param barcodeType 二维码类型
     * @param dictTypeNum 台账类型
     * @return PdaResult 状态为未确认的详情列表
     */
    @RequestMapping(value = "/storageinPackingDetailList")
    @ResponseBody
    public PdaResult storageinPackingDetailList(String rfid, Long userId, String barcode, String barcodeType,String dictTypeNum) {
        return pdaService.storageinPackingDetailList(rfid, userId, barcode, barcodeType, dictTypeNum);
    }

    /**
     * 装包:点击确认
     * @param queryMap rfid
     * @return PdaResult 详情表列表
     */
    @RequestMapping(value = "/storageinPackingSubmit")
    @ResponseBody
    public PdaResult storageinPackingSubmit(@RequestBody Map<String, Object> queryMap) {
        return pdaService.storageinPackingSubmit(queryMap);
    }

    /**
     * 入库:点击开始入库,返回仓库列表
     * @param factoryCode 厂区
     * @param page 分页
     * @param size 页码
     * @param code 仓库查询条件
     * @return PdaResult
     */
    @RequestMapping(value = "/storageinStorageinStart")
    @ResponseBody
    public PdaResult storageinStorageinStart(String factoryCode,String page,String size,String code) {
        return pdaService.storageinStorageinStart(page,size,factoryCode,code);
    }

    /**
     * 入库:选择闲置的行车
     * @param warehouseCode 仓库编码
     * @param factoryCode 厂区编码
     * @return PdaResult 行车列表
     */
    @RequestMapping(value = "/storageinStorageinCar")
    @ResponseBody
    public PdaResult storageinStorageinCar(String warehouseCode,String factoryCode) {
        return pdaService.storageinStorageinCar(warehouseCode,factoryCode);
    }
    
    /**
     * 入库:扫描
     * @param warehouseCode 仓库编码
     * @param rfid rfid
     * @param factoryCode 厂区编码
     * @param barcode 二维码信息
     * @param barcodeType 二维码类型
     * @param carCode 行车
     * @param isCertificate 是否是合格证扫描：0不是。1是
     * @param userId 操作人
     * @return PdaResult 推荐库位，物料详情
     */
    @RequestMapping(value = "/storageinStorageinScan")
    @ResponseBody
    public PdaResult storageinStorageinScan(String warehouseCode, String rfid, String factoryCode, String barcode, String barcodeType,
                                            String carCode, String isCertificate, Long userId) {
        return pdaService.storageinStorageinScan(warehouseCode, rfid, factoryCode, barcode, barcodeType, carCode, isCertificate, userId);
    }
    
    /**
     * 入库:再次扫描（点击开始入库后，操作员发现入错了，应该时入另一盘）
     * @param warehouseCode 仓库编码
     * @param rfid rfid
     * @param factoryCode 厂区编码
     * @param originalRfid 上一盘线rfid
     * @param barcode 二维码
     * @param barcodeType 二维码类型
     * @param carCode 行车编码
     * @param isCertificate 是否是合格证扫描：0不是。1是
     * @param userId 操作人
     * @return PdaResult 推荐库位，物料详情
     */
    @RequestMapping(value = "/againStorageinScan")
    @ResponseBody
    public PdaResult againStorageinScan(String warehouseCode, String rfid, String factoryCode, String originalRfid, String barcode,
                                        String barcodeType, String carCode, String isCertificate, Long userId) {
    	return pdaService.againStorageinScan(warehouseCode,rfid,factoryCode,originalRfid,barcode,barcodeType,carCode,isCertificate,userId);
    }

    /**
     * 入库:选择仓库，行车之后提交
     * @param rfid rfid
     * @param shelfCode 库位编码
     * @param warehouseCode 仓库编码
     * @param carCode 行车编码
     * @param factoryCode 厂区编码
     * @return PdaResult
     */
    @RequestMapping(value = "/storageinStorageinSubmit")
    @ResponseBody
    public PdaResult storageinStorageinSubmit(String rfid, String shelfCode, String warehouseCode, String carCode,String factoryCode) {
        return pdaService.storageinStorageinSubmit(rfid, shelfCode, warehouseCode, carCode, factoryCode);
    }
    
    /**
     * 点击开始入库后，完成入库之前，用户修改推荐库位，修改后同步到pad
     * @param shelfCode 库位编码
     * @param carCode 行车编码
     * @return PdaResult
     */
    @RequestMapping(value = "/recommendShelfSubmit")
    @ResponseBody
    public PdaResult recommendShelfSubmit(String shelfCode, String carCode) {
        return pdaService.recommendShelfSubmit(shelfCode,carCode);
    }

    /**
     * 完成入库时，根据入库事务日期，判断是否需要弹框提示确认
     * @param date 事务时间
     * @param rfid rfid
     * @return PdaResult
     */
    @RequestMapping(value = "/storageinTiemCheck")
    @ResponseBody
    public PdaResult storageinTiemCheck(String date, String rfid) {
        return pdaService.storageinTiemCheck(date, rfid);
    }

    /**
     * 入库完成
     * @param rfid rfid
     * @param shelfCode 库位编码
     * @param userId 用户id
     * @param date 日期
     * @param isCertificate 是否是合格证扫描：0不是。1是
     * @param factoryCode 厂区
     * @param warehouseCode 仓库
     * @return PdaResult
     */
    @RequestMapping(value = "/storageinStorageinSuccess")
    @ResponseBody
    public PdaResult storageinStorageinSuccess(String rfid, String shelfCode, Long userId, String date, String isCertificate,
                                               String factoryCode, String warehouseCode) {
        return pdaService.storageinStorageinSuccess(rfid, shelfCode, userId, date, isCertificate, factoryCode, warehouseCode);
    }

    /**
     * 获取指定厂区仓库下指定仓库区域内的可用库位
     * @param warehouseCode 仓库编码
     * @param factoryCode 厂区编码
     * @param page 分页
     * @param size 页码
     * @param code 查询条件
     * @param carCode 行车编码
     * @return PdaResult 库位列表
     */
    @RequestMapping(value = "/storageinEmptyShelf")
    @ResponseBody
    public PdaResult storageinEmptyShelf(String warehouseCode,String factoryCode,int page,int size,String code,String carCode) {
        return pdaService.storageinEmptyShelf(warehouseCode,factoryCode,page,size,code,carCode);
    }

    /**
     * 获取指定厂区仓库下的可用库位
     * @param warehouseCode 仓库编码
     * @param factoryCode 厂区编码
     * @param page 分页
     * @param size 页码
     * @param code 条件
     * @return PdaResult 库位列表
     */
    @RequestMapping(value = "/selectNoCarShelf")
    @ResponseBody
    public PdaResult selectNoCarShelf(String warehouseCode,String factoryCode,int page,int size,String code) {
        return pdaService.selectNoCarShelf(warehouseCode,factoryCode,page,size,code);
    }
    
    /**
     * 完成入库真实库位选择
     * @param pageNo 页码
     * @param size 每页数量
     * @param code 库位模糊查询输入
     * @param carCode 行车
     * @return PdaResult 行车所在仓库区域的库位列表
     */
    @RequestMapping(value = "/getRealArroundShelf")
    @ResponseBody
    public PdaResult getRealArroundShelf(int pageNo,int size,String code,String carCode) {
    	return pdaService.getRealArroundShelf(pageNo,size,code,carCode);
    }

    /**
     * 出库:选择闲置和出库中的行车
     * @param warehouseCode 仓库编码
     * @param factoryCode 厂区编码
     * @return PdaResult 行车列表
     */
    @RequestMapping(value = "/storageoutStorageoutCar")
    @ResponseBody
    public PdaResult storageoutStorageoutCar(String warehouseCode,String factoryCode) {
    	return pdaService.storageoutStorageoutCar(warehouseCode,factoryCode);
    }
    
    /**
     * 发货：扫描发货单
     * @param shipNo 发货单号
     * @param warehouseCode 仓库
     * @param userId 用户
     * @param factoryCode 厂区
     * @param carCode 行车编码
     * @return PdaResult 发货明细列表,发货单号,营销经理
     */
    @RequestMapping(value = "/storageoutDetailList")
    @ResponseBody
    public PdaResult storageoutDetailList(String shipNo, String warehouseCode, Long userId, String factoryCode, String carCode) {
        return pdaService.storageoutDetailList(shipNo, warehouseCode, userId, factoryCode, carCode);
    }

    /**
     * 扫描获取出库单
     * @param shipNo 发货单号
     * @param factoryCode 厂区
     * @param warehouseCode 仓库
     * @param carCode 航车
     * @return PdaResult 发货单信息
     */
    @RequestMapping(value = "/findOutstorageByScan")
    @ResponseBody
    public PdaResult findOutstorageByScan(String shipNo,String factoryCode,String warehouseCode,String carCode) {
        return pdaService.findOutstorageByScan(shipNo,factoryCode,warehouseCode,carCode);
    }

    /**
     * 发货：点击,更改状态拣选中  (1:生效  2:拣选中  3:拣选完成 4:发运完成)
     * @param rfid rfid
     * @param carCode 行车编码
     * @param user_id 用户主键
     * @param shipNo 提货单号
     * @param barcode 二维码信息
     * @param barcodeType 二维码类型
     * @return PdaResult
     */
    @RequestMapping(value = "/storageoutDetailClick1")
    @ResponseBody
    public PdaResult storageoutDetailClick1(String rfid, String carCode,Long user_id,String shipNo,String barcode,String barcodeType) {
        return pdaService.storageoutDetailClick1(rfid, carCode, user_id, shipNo, barcode, barcodeType);
    }

    /**
     * 排序界面进入单条明细界面扫描物料rfid=开始出库,更改状态拣选中  (1:生效  2:拣选中  3:拣选完成  4:预出库)
     * @param rfid rfid
     * @param carCode 行车编码
     * @param user_id 用户主键
     * @param shipNo 发货单号
     * @param barcode 二维码
     * @param barcodeType 二维码类型
     * @param instorageDetailId 库存表主键
     * @return PdaResult
     */
    @RequestMapping(value = "/storageoutSortScan")
    @ResponseBody
    @Transactional(rollbackFor = Exception.class)
    public PdaResult storageoutSortScan(String rfid,String carCode,Long user_id,String shipNo,String barcode,String barcodeType,
                                        Long instorageDetailId){
        return pdaService.storageoutSortScan(rfid, carCode, user_id, shipNo, barcode, barcodeType, instorageDetailId);
    }

    /**
     * 发货：点击,更改状态拣选完成  (1:生效  2:拣货中  3:发运完成)
     * @param qaCode 质保号
     * @param userId 用户
     * @param type 1全部，2按米
     * @param shipNo 发货单号
     * @return PdaResult
     */
    @RequestMapping(value = "/storageoutDetailClick2")
    @ResponseBody
    public PdaResult storageoutDetailClick2(String qaCode,Long userId,String type,String shipNo) {
        //预出库模式：扫码下架，释放库位
        if (Objects.equals(EmumConstant.deliveryMode.BEFOREHAND.getCode(), YamlConfigurerUtil.getIntegerYmlVal("yddl.outstorageModel"))){
            return pdaService.storageoutDetailClick2(qaCode, userId, type, shipNo);
        }else if(Objects.equals(EmumConstant.deliveryMode.ACTUAL.getCode(), YamlConfigurerUtil.getIntegerYmlVal("yddl.outstorageModel"))) {
            //直接出库模式：扫码直接出库，释放库位
            return pdaService.storageoutConfirm(qaCode, userId, type, shipNo);
        }else {
            return null;
        }
    }

    /**
     * 划单: 撤销出库扫描提货单
     * @param shipNo 提货单号
     * @return PdaResult
     */
    @RequestMapping(value = "/revokeOustroageScan")
    @ResponseBody
    public PdaResult revokeOustroageScan(String shipNo){
        return pdaService.revokeOustroageScan(shipNo);
    }

    /**
     * 划单: 撤销出库扫描物料
     * @param rfid rfid
     * @param barcode 二维码
     * @param barcodeType 二维码类型
     * @param shipNo 提货单号
     * @return PdaResult
     */
    @RequestMapping(value = "/revokeOustroageDetailScan")
    @ResponseBody
    public PdaResult revokeOustroageDetailScan(String rfid, String barcode, String barcodeType, String shipNo){
        return pdaService.revokeOustroageDetailScan(rfid,barcode,barcodeType,shipNo);
    }

    /**
     * 划单: 撤销出库确认
     * @param queryMap 参数
     * @return PdaResult
     */
    @RequestMapping(value = "/revokeOustroageDetailConfirm")
    @ResponseBody
    public PdaResult revokeOustroageDetailConfirm(@RequestBody Map<String, Object> queryMap){
        return pdaService.revokeOustroageDetailConfirm(queryMap);
    }

    /**
     * 出库-完成出库窗口中取消出库
     * @param qaCode 质保号
     * @param shipNo 发货单号
     * @return PdaResult
     */
    @RequestMapping(value = "/cancelOutStorage")
    @ResponseBody
    public PdaResult cancelOutStorage(String qaCode, String shipNo) {
        return pdaService.cancelOutStorage(qaCode, shipNo);
    }
    
    /**
     * 完成出库-判断发货单米数与库存不相符，选择了正常出库，提示“库存米数xx，发货米数xx，是否整盘出？” 点是就整盘出，点否就重新看一下。
     * @param qaCode 质保号
     * @param type 出库类型：1全部出库，2按米出库
     * @param shipNo 提货单号
     * @return PdaResult
     */
    @RequestMapping(value = "/judgeOutmeter")
    @ResponseBody
    public PdaResult judgeOutmeter(String qaCode,String type,String shipNo) {
    	return pdaService.judgeOutmeter(qaCode,type,shipNo);
    }

    /**
     * 根据输入的车牌号获取发货单
     * @param carNumber 车牌号
     * @param factoryCode 厂区
     * @param warehouseCode 仓库
     * @param carCode 航车
     * @return PdaResult
     */
    @RequestMapping(value = "/getOutstorageListByCarNumber")
    @ResponseBody
    public PdaResult getOutstorageListByCarNumber(String carNumber,String factoryCode,String warehouseCode,String carCode) {
        return pdaService.getOutstorageListByCarNumber(carNumber,factoryCode,warehouseCode,carCode);
    }

    /**
     * 根据发货单号获取已经完成的和所有的明细的数量
     * @param shipnoJsonString 发货单
     * @param factoryCode 厂区
     * @param warehouseCode 仓库
     * @param carCode 航车
     * @return Map<String,Object>
     */
    @RequestMapping(value = "/getNumberByShipNo")
    @ResponseBody
    public PdaResult getNumberByShipNo(@RequestParam("lstShipNo") String shipnoJsonString,@RequestParam("factoryCode") String factoryCode,
                                       @RequestParam("warehouseCode") String warehouseCode,@RequestParam("carCode") String carCode){
        return pdaService.getNumberByShipNo(shipnoJsonString,factoryCode,warehouseCode,carCode);
    }

    /**
     * 发货主界面排序按钮
     * @param shipnoJsonString 发货单
     * @param factoryCode 厂区编码
     * @param warehouseCode 仓库编码
     * @param carCode 航车编码
     * @param carNo 车牌号
     * @return Map<String,Object>
     */
    @RequestMapping(value = "/sortOutstorageDetail")
    @ResponseBody
    public PdaResult sortOutstorageDetail(@RequestParam("lstShipNo") String shipnoJsonString, @Param("factoryCode") String factoryCode,
                                          @Param("warehouseCode") String warehouseCode, @Param("carCode") String carCode,
                                          @Param("carNo") String carNo){
        return pdaService.sortOutstorageDetail(shipnoJsonString,factoryCode,warehouseCode,carCode,carNo);
    }

    /**
     * 发货主界面排序后点击单一详情
     * @param instorageDetailId 初始化标主键
     * @param shipNo 发货单号
     * @return Map<String,Object>
     */
    @RequestMapping(value = "/clickSoloOutStorageDetail")
    @ResponseBody
    public PdaResult clickSoloOutStorageDetail(Long instorageDetailId, String shipNo){
        return pdaService.clickSoloOutStorageDetail(instorageDetailId, shipNo);
    }

    /**
     * 模块：出库管理
     * 发货主界面刷新按钮
     * @param shipnoJsonString 发货单
     * @param carNo 车牌号
     * @param factoryCode 厂区
     * @param warehouseCode 仓库
     * @param carCode 行车
     * @return Map<String,Object>
     */
    @RequestMapping(value = "/refreshOutstorage")
    @ResponseBody
    public PdaResult refreshOutstorage(@RequestParam("lstShipNo") String shipnoJsonString, @Param("carNo") String carNo,
                                       @Param("factoryCode") String factoryCode, @Param("warehouseCode") String warehouseCode,
                                       @Param("carCode") String carCode){
        return pdaService.refreshOutstorage(shipnoJsonString, carNo, factoryCode, warehouseCode, carCode);
    }

    /**
     * 模块：出库管理
     * 物资流转单打印
     * @param carNo 车牌号
     * @param factoryCode 厂区
     * @param transferFactoryCode 流转厂
     * @return Map<String,Object>
     */
    @RequestMapping(value = "/wzlzPrint")
    @ResponseBody
    public PdaResult wzlzPrint(@Param("carNo") String carNo, @Param("factoryCode") String factoryCode,
                               @Param("transferFactoryCode") String transferFactoryCode){
        return pdaService.wzlzPrint(carNo, factoryCode, transferFactoryCode);
    }

    /**
     * 调拨列表
     * @param rfid rfid
     * @param userId 用户主键
     * @param barcode 二维码信息
     * @param barcodeType 二维码类型
     * @return PdaResult 库存列表
     */
    @RequestMapping(value = "/alloStorageInfoList")
    @ResponseBody
    public PdaResult alloStorageInfoList(String rfid, Long userId, String barcode, String barcodeType) {
        return pdaService.alloStorageInfoList(rfid, userId, barcode, barcodeType);
    }

    /**
     * 点击提交，生成调拨单
     * @param queryMap 调入仓库,调出仓库,库存列表id,用户id
     * @return PdaResult
     */
    @RequestMapping(value = "/alloAdd")
    @ResponseBody
    public PdaResult alloAdd(@RequestBody Map<String, Object> queryMap) {
        return pdaService.alloAdd(queryMap);
    }

    /**
     * 返回所有未完成的主表信息
     * @param userId 用户主键
     * @return PdaResult 调拨单主表列表
     */
    @RequestMapping(value = "/alloSelectAllUndo")
    @ResponseBody
    public PdaResult alloSelectAllUndo(Long userId) {
        return pdaService.alloSelectAllUndo(userId);
    }

    /**
     * 返回所有未完成调拨出库的主表信息
     * @param userId 用户主键
     * @return PdaResult 调拨单主表列表
     */
    @RequestMapping(value = "/alloSelectPartUndo")
    @ResponseBody
    public PdaResult alloSelectPartUndo(Long userId) {
        return pdaService.alloSelectPartUndo(userId);
    }

    /**
     * 调拨功能模块进入，展示调拨单列表
     * @param id 调拨主表主键
     * @return PdaResult 详情列表
     */
    @RequestMapping(value = "/alloSelectDetailById")
    @ResponseBody
    public PdaResult alloSelectDetailById(Long id) {
        return pdaService.alloSelectDetailById(id);
    }
    
    /**
     * 调拨功能模块进入，展示调拨出库单列表
     * @param id 主表id
     * @return PdaResult 详情列表
     */
    @RequestMapping(value = "/alloOutSelectDetailById")
    @ResponseBody
    public PdaResult alloOutSelectDetailById(Long id) {
        return pdaService.alloOutSelectDetailById(id);
    }
    
    /**
     * 调拨出库，进入调拨出库单展示列表后，选择行车，把在该调拨单下并且属于该行车场景范围内的任务信息同步给行车
     * @param id 主表id
     * @param carCode 行车编码
     * @return PdaResult 详情列表
     */
    @RequestMapping(value = "/alloMissionToCar")
    @ResponseBody
    public PdaResult alloMissionToCar(Long id,String carCode) {
        return pdaService.alloMissionToCar(id,carCode);
    }
    
    /**
     * 调拨功能模块进入，展示调拨入库单列表
     * @param id 主表主键
     * @return PdaResult
     */
    @RequestMapping(value = "/alloInSelectDetailById")
    @ResponseBody
    public PdaResult alloInSelectDetailById(Long id) {
        return pdaService.alloInSelectDetailById(id);
    }

    /**
     * 删除主表信息，子表信息,需要判断该主表下有无已经开始的字表信息，如有则提示  库存表更改状态
     * @param id 主表id
     * @return PdaResult
     */
    @RequestMapping(value = "/alloDelInfoAndDetail")
    @ResponseBody
    public PdaResult alloDelInfoAndDetail(Long id) {
        return pdaService.alloDelInfoAndDetail(id);
    }

    /**
     * 删除子表信息  判断该字表的状态  更改库存表的状态
     * @param id 主表id
     * @return PdaResult
     */
    @RequestMapping(value = "/alloDelDetail")
    @ResponseBody
    public PdaResult alloDelDetail(Long id) {
        return pdaService.alloDelDetail(id);
    }

    /**
     * 点击，开始出库  将状态改为开始出库 状态 1:未开始 2.开始调拨出库 3.完成调拨出库 4.开始调拨入库 5.完成调拨入库
     * @param rfid rfid
     * @param carCode 行车编码
     * @param barcode 二维码
     * @param barcodeType 二维码类型
     * @return PdaResult
     */
    @RequestMapping(value = "/alloOutStorageStart")
    @ResponseBody
    public PdaResult alloOutStorageStart(String rfid, String carCode, String barcode, String barcodeType) {
        return pdaService.alloOutStorageStart(rfid, carCode, barcode, barcodeType);
    }

    /**
     * 点击，完成出库 1.需要修改状态 2.需要增加调拨出库变动表信息  3.需要在库存里面将原有rfid的数据删除
     * @param rfid rfid
     * @param userId 用户主键
     * @return PdaResult
     */
    @RequestMapping(value = "/alloOutStorageSuccess")
    @ResponseBody
    public PdaResult alloOutStorageSuccess(String rfid, Long userId) {
        return pdaService.alloOutStorageSuccess(rfid, userId);
    }
    
    /**
     * 显示所有待入库的调拨单
     * @param userId 用户id
     * @return PdaResult
     */
    @RequestMapping(value = "/pendingAlloStorageList")
    @ResponseBody
    public PdaResult pendingAlloStorageList(Long userId) {
    	return pdaService.pendingAlloStorageList(userId);
    }

    /**
     * 扫描rfid，返回调拨出库完成的详情表信息
     * @param rfid rfid
     * @param barcode 二维码
     * @param barcodeType 二维码类型
     * @param carCode 行车编码
     * @return PdaResult
     */
    @RequestMapping(value = "/alloInStorageList")
    @ResponseBody
    public PdaResult alloInStorageList(String rfid, String barcode, String barcodeType, String carCode) {
        return pdaService.alloInStorageList(rfid,barcode,barcodeType,carCode);
    }

    /**
     * 点击开始，修改状态为开始调拨入库
     * @param rfid rfid
     * @param shelfCode 库位
     * @param carCode 行车
     * @param warehouseCode 仓库
     * @return PdaResult
     */
    @RequestMapping(value = "/alloInStorageStart")
    @ResponseBody
    public PdaResult alloInStorageStart(String rfid, String shelfCode, String carCode, String warehouseCode) {
        return pdaService.alloInStorageStart(rfid, shelfCode, carCode, warehouseCode);
    }

    /**
     * 结束,状态改为调拨入库
     * @param rfid rfid
     * @param userId 用户主键
     * @param shelfCode 弹框修改后的最终库位
     * @return PdaResult
     */
    @RequestMapping(value = "/alloInStorageSuccess")
    @ResponseBody
    public PdaResult alloInStorageSuccess(String rfid, Long userId, String shelfCode) {
        return pdaService.alloInStorageSuccess(rfid, userId, shelfCode);
    }

    /**
     * 获取移库单信息
     * @param rfid rfid
     * @param userId 用户id
     * @param barcode 二维码
     * @param barcodeType 二维码类型
     * @return PdaResult 库存列表
     */
    @RequestMapping(value = "/moveStorageInfoList")
    @ResponseBody
    public PdaResult moveStorageInfoList(String rfid, Long userId, String barcode, String barcodeType) {
        return pdaService.moveStorageInfoList(rfid, userId, barcode, barcodeType);
    }

    /**
     * 点击提交，生成移库单
     * @param queryMap storageInfoRfids（库存列表id）,userId(用户id)
     * @return PdaResult
     */
    @RequestMapping(value = "/moveAdd")
    @ResponseBody
    public PdaResult moveAdd(@RequestBody Map<String, Object> queryMap) {
        return pdaService.moveAdd(queryMap);
    }

    /**
     * 返回所有未完成的主表信息
     * @param userId 用户主键
     * @return PdaResult 移库单主表列表
     */
    @RequestMapping(value = "/moveSelectAllUndo")
    @ResponseBody
    public PdaResult moveSelectAllUndo(Long userId) {
        return pdaService.moveSelectAllUndo(userId);
    }

    /**
     * 返回所有未完成移库出库的主表信息
     * @param userId 用户主键
     * @return PdaResult 移库单主表列表
     */
    @RequestMapping(value = "/moveSelectPartUndo")
    @ResponseBody
    public PdaResult moveSelectPartUndo(Long userId) {
        return pdaService.moveSelectPartUndo(userId);
    }

    /**
     * 移库功能模块进入，展示移库单列表
     * @param id 主表id
     * @return PdaResult 详情列表
     */
    @RequestMapping(value = "/moveSelectDetailById")
    @ResponseBody
    public PdaResult moveSelectDetailById(Long id) {
        return pdaService.moveSelectDetailById(id);
    }
    
    /**
     * 移库入库展示明细
     * @param id 主表主键
     * @return PdaResult 详情列表
     */
    @RequestMapping(value = "/moveInSelectDetailById")
    @ResponseBody
    PdaResult moveInSelectDetailById(Long id){
        return pdaService.moveInSelectDetailById(id);
    }
    
    /**
     * 移库出库展示明细
     * @param id 主表id
     * @return PdaResult 详情列表
     */
    @RequestMapping(value = "/moveOutSelectDetailById")
    @ResponseBody
    PdaResult moveOutSelectDetailById(Long id){
        return pdaService.moveOutSelectDetailById(id);
    }
    
    /**
     * 移库出库，进入移库出库单展示列表后，选择行车，把在该移库单下并且属于该行车场景范围内的任务信息同步给行车
     * @param id 主表主键
     * @param carCode 行车编码
     * @return PdaResult 详情列表
     */
    @RequestMapping(value = "/moveMissionToCar")
    @ResponseBody
    public PdaResult moveMissionToCar(Long id,String carCode) {
        return pdaService.moveMissionToCar(id,carCode);
    }

    /**
     * 删除主表信息，子表信息  需要判断该主表下有无已经开始的字表信息，如有则提示  更新库存状态
     * @param id 主表id
     * @return PdaResult
     */
    @RequestMapping(value = "/moveDelInfoAndDetail")
    @ResponseBody
    public PdaResult moveDelInfoAndDetail(Long id) {
        return pdaService.moveDelInfoAndDetail(id);
    }

    /**
     * 删除子表信息  判断该字表的状态
     * @param id 主表id
     * @return PdaResult
     */
    @RequestMapping(value = "/moveDelDetail")
    @ResponseBody
    public PdaResult moveDelDetail(Long id) {
        return pdaService.moveDelDetail(id);
    }

    /**
     * 扫码，开始出库  将状态改为开始出库
     * 状态 1:未开始 2.开始移库出库 3.完成移库出库 4.开始移库入库 5.完成移库入库
     * @param rfid rfid
     * @param carCode 行车编码
     * @param barcode 二维码
     * @param barcodeType 二维码类型
     * @return PdaResult
     */
    @RequestMapping(value = "/moveOutStorageStart")
    @ResponseBody
    public PdaResult moveOutStorageStart(String rfid, String carCode, String barcode, String barcodeType) {
        return pdaService.moveOutStorageStart(rfid, carCode, barcode, barcodeType);
    }

    /**
     * 点击，完成出库
     * 1.需要修改状态 2.需要增加移库出库变动表信息  3.需要在库存里面将原有rfid的数据删除
     * @param rfid rfid
     * @param userId 用户id
     * @return PdaResult
     */
    @RequestMapping(value = "/moveOutStorageSuccess")
    @ResponseBody
    public PdaResult moveOutStorageSuccess(String rfid, Long userId) {
        return pdaService.moveOutStorageSuccess(rfid, userId);
    }

    /**
     * 显示所有待入库的移库单
     * @param userId 用户主键
     * @return PdaResult
     */
    @RequestMapping(value = "/pendingMoveStorageList")
    @ResponseBody
    public PdaResult pendingMoveStorageList(Long userId) {
    	return pdaService.pendingMoveStorageList(userId);
    }
    
    /**
     * 扫描rfid，返回移库出库完成的详情表信息
     * @param rfid rfid
     * @param barcode 二维码
     * @param barcodeType 二维码类型
     * @param carCode 行车编码
     * @return PdaResult
     */
    @RequestMapping(value = "/moveInStorageList")
    @ResponseBody
    public PdaResult moveInStorageList(String rfid,String barcode,String barcodeType,String carCode) {
        return pdaService.moveInStorageList(rfid,barcode,barcodeType,carCode);
    }

    /**
     * 点击开始，修改状态为开始移库入库
     * @param rfid rfid
     * @param shelfCode 库位编码
     * @param carCode 行车编码
     * @param warehouseCode 仓库编码
     * @return PdaResult
     */
    @RequestMapping(value = "/moveInStorageStart")
    @ResponseBody
    public PdaResult moveInStorageStart(String rfid, String shelfCode, String carCode, String warehouseCode) {
        return pdaService.moveInStorageStart(rfid, shelfCode, carCode, warehouseCode);
    }

    /**
     * 结束,状态改为移库入库
     * @param rfid rfid
     * @param userId 用户主键
     * @param shelfCode 最终库位
     * @return PdaResult
     */
    @RequestMapping(value = "/moveInStorageSuccess")
    @ResponseBody
    public PdaResult moveInStorageSuccess(String rfid, Long userId, String shelfCode) {
        return pdaService.moveInStorageSuccess(rfid, userId, shelfCode);
    }

    /**
     * 移库单主界面支持直接扫码，扫码分三种情况
     *   1.检索后没有一样的质保号，那么跳入新增的提交界面，要多一个行车的选择，可选可不选
     *   2.检索后有一样的质保号，并且该质保号已经完成了移库出库，那么就直接跳入入库界面，移库入库界面行车可选可不选
     *   3.检索后有一样的质保号，但是该质保号没有完成移库出库，那么提示“未出库，请先确认出库！”
     * @param rfid rfid
     * @param barcode 二维码
     * @param barcodeType 二维码类型
     * @param userId 用户主键
     * @return PdaResult
     */
    @RequestMapping(value = "/moveStorageScan")
    @ResponseBody
    public PdaResult moveStorageScan(String rfid,String barcode,String barcodeType,Long userId) {
        return pdaService.moveStorageScan(rfid, barcode, barcodeType, userId);
    }

    /**
     * 拆分提交接口
     * @param queryMap 参数
     * @return PdaResult
     */
    @RequestMapping(value = "/splitSubmit")
    @ResponseBody
    public PdaResult splitSubmit(@RequestBody Map<String, Object> queryMap) {
        return pdaService.splitSubmit(queryMap);
    }

    /**
     * 重新绑定RFID 绑定表
     * @param qaCode 质保单号
     * @return PdaResult
     */
    @RequestMapping(value = "/reSetrRfidBind")
    @ResponseBody
    public PdaResult reSetrRfidBind(String qaCode) {
        return pdaService.reSetrRfidBind(qaCode);
    }

    /**
     * 获取拆分主表信息
     * @return PdaResult
     */
    @RequestMapping(value = "/splitTypeIsZero")
    @ResponseBody
    public PdaResult splitTypeIsZero() {
        return pdaService.splitTypeIsZero();
    }

    /**
     * 根据id查询拆分入库表信息
     * @param id 主表id
     * @return PdaResult
     */
    @RequestMapping(value = "/splitBelong")
    @ResponseBody
    public PdaResult splitBelong(String id) {
        return pdaService.splitBelong(id);
    }

    /**
     * 根据rfid修改状态
     * @param rfid rfid
     * @return PdaResult
     */
    @RequestMapping(value = "/updateRfidType")
    @ResponseBody
    public PdaResult updateRfidType(String rfid) {
        return pdaService.updateRfidType(rfid);
    }
    
    /**
     * 获取UWB所在库位
     * @param carCode 行车编码
     * @return PdaResult 定位库位
     */
    @RequestMapping(value = "/getUwbShelf")
    @ResponseBody
    public PdaResult getUwbShelf(String carCode) {
        return pdaService.getUwbShelf(carCode);
    }

    /**
     * 返回所有未审核已提交的盘点任务信息
     * @param user_id 用户主键
     * @return PdaResult
     */
    @RequestMapping(value = "/inventoryPlanSelect")
    @ResponseBody
    public PdaResult inventoryPlanSelect(Long user_id) {
        return pdaService.inventoryPlanSelect(user_id);
    }

    /**
     * 解析提交的盘点信息，再存入明细表
     * @param queryMap 仓胡
     * @return PdaResult
     */
    @RequestMapping(value = "/inventoryPlanAdd")
    @ResponseBody
    public PdaResult inventoryPlanAdd(@RequestBody Map<String, Object> queryMap) {
        return pdaService.inventoryPlanAdd(queryMap);
    }
    
    /**
     * 获取盘信息
     * @param code 输入的盘具编码或者盘具规格
     * @param pageNo 页码
     * @param pageSize 每页数量
     * @return dish 盘具列表
     */
    @RequestMapping(value = "/getDishList")
    @ResponseBody
    public PdaResult getDishList(String code,int pageNo,int pageSize) {
        return pdaService.getDishList(code,pageNo,pageSize);
    }

    /**
     * 盘点扫描时获取物料得详情
     * @param factorycode 厂区
     * @param warehouse 仓库
     * @param rfid rfid
     * @param barcode 二维码
     * @param barcodeType 二维码类型
     * @param userId 用户主键
     * @param ebsCode ebs盘点单号
     * @return StorageInfo
     */
    @RequestMapping(value = "/getMaterialInfoBySalesAndWare")
    @ResponseBody
    public PdaResult getMaterialInfoBySalesAndWare(String factorycode, String warehouse, String rfid, String barcode, String barcodeType,
                                                   String userId, String ebsCode){
        return pdaService.getMaterialInfo(factorycode, warehouse, rfid, barcode, barcodeType, userId, ebsCode);
    }

    /**
     * 盘点修改库位界面中获取所有的库位
     * @param code 模糊查询条件
     * @param pageNo 页码
     * @param size 每页数量
     * @return Shelf 库位
     */
    @RequestMapping(value = "/getInventoryShelf")
    @ResponseBody
    public PdaResult getInventoryShelf(String code,int pageNo,int size) {
        return pdaService.getInventoryShelf(code,pageNo,size);
    }

    /**
     * 根据用户获取厂区
     * @param userId 用户主键
     * @return List<FactoryArea>
     */
    @RequestMapping(value = "/getFactorys")
    @ResponseBody
    public PdaResult getFactorys(String userId){
        return pdaService.getFactorys(userId);
    }
    
    /**
     * 显示物料详情
     * @param rfid rfid
     * @param barcode 二维码
     * @param barcodeType 二维码类型
     * @return PdaResult
     */
    @RequestMapping(value = "/findInfoByRfid")
    @ResponseBody
    public PdaResult findInfoByRfid(String rfid,String barcode,String barcodeType){
        return pdaService.findInfoByRfid(rfid,barcode,barcodeType);
    }

    /**
     * 扫描获取库存物料详情
     * @param rfid rfid
     * @param barcode 批次号/质保号
     * @param barcodeType 扫描类型
     * @return StorageInfo 库存物料
     */
    @RequestMapping(value = "/getStorageInfoDetail")
    @ResponseBody
    public PdaResult getStorageInfoDetail(String rfid,String barcode,String barcodeType){
        return pdaService.getStorageInfoDetail(rfid, barcode, barcodeType);
    }

    /**
     * 获取服务器手持机更新文件内容
     * @return Map<String, Object> 版本号、程序包名称、文件路劲（url）
     */
    @RequestMapping(value = "/autoUpdatePda")
    @ResponseBody
    public PdaResult autoUpdatePda(){
        Map<String, Object> map = new HashMap<>(3);
        /*try {
            //要访问的路径
            URL url = new URL(pdaPath);
            //用于发送或接受数据
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            //发送get请求
            conn.setRequestMethod("GET");
            //请求超时时间
            conn.setConnectTimeout(5000);
            //服务器返回的状态码
            int code = conn.getResponseCode();

            //说明请求成功
            if (code == 200) {
                //获取服务器返回的数据
                InputStream in = conn.getInputStream();
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                //读取缓存
                byte[] buffer = new byte[2048];
                int length;
                while((length = in.read(buffer)) != -1) {
                    //写入输出流
                    bos.write(buffer, 0, length);
                }
                in.close();//读取完毕，关闭输入流

                // 根据输出流创建字符串对象
                String str = new String(bos.toByteArray(), StandardCharsets.UTF_8);
                try {
                    PDA_UPDATE_DATA pda_update_data = JaxbUtil.xmlToBean(str, PDA_UPDATE_DATA.class);
                    map.put("version", pda_update_data.version);
                    map.put("name", pda_update_data.name);
                    map.put("url", pda_update_data.url);
                } catch (JAXBException e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        Map<String,Object> pdaMap = pdaService.findPdaConfig();
        map.put("version", pdaMap.get("VERSION"));
        map.put("name", pdaMap.get("NAME"));
        map.put("url", pdaMap.get("URL"));
        return new PdaResult(map);
    }

    /**
     * 物资流转单补打界面查询
     * @param circulationNo 流转单单号
     * @param carNo 车牌号
     * @param fromFactory 出发厂
     * @param toFactory 目的厂
     * @param page 页码
     * @param size 每页数量
     * @return List<ExchangePrint>
     */
    @RequestMapping(value = "/selectWzlzPrintPage")
    @ResponseBody
    public PdaResult selectWzlzPrintPage(String circulationNo,String carNo,String fromFactory,String toFactory,String page,String size) {
        return pdaService.selectWzlzPrintPage(circulationNo, carNo, fromFactory, toFactory, page, size);
    }

    /**
     * 物资流转单补打打印
     * @param circulationNo 流转单单号
     * @return PdaResult
     */
    @RequestMapping(value = "/patchPrint")
    @ResponseBody
    public PdaResult patchPrint(String circulationNo) {
        return pdaService.patchPrint(circulationNo);
    }
}
