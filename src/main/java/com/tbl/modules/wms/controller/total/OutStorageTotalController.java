package com.tbl.modules.wms.controller.total;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Maps;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.platform.constant.MenuConstant;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.service.system.RoleService;
import com.tbl.modules.wms.service.outstorage.OutStorageDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

/**
 * 出库汇总管理控制层
 * @author 70486
 */
@Controller
@RequestMapping("/outTotal")
public class OutStorageTotalController extends AbstractController {

    /**
     * 出库明细
     */
    @Autowired
    private OutStorageDetailService outStorageDetailService;
    /**
     * 角色
     */
    @Autowired
    private RoleService roleService;

    /**
     * 跳转到出库汇总表列表页面
     * @return ModelAndView
     */
    @RequestMapping(value = "/toList")
    public ModelAndView toList() {
        ModelAndView mv = this.getModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(getSessionUser().getRoleId(), MenuConstant.outTotal));
        mv.setViewName("techbloom/total/outstoragein_list");
        return mv;
    }

    /**
     * 获取出库汇总表列表数据
     * @param queryJsonString 查询条件
     * @return Map<String,Object>
     */
    @RequestMapping(value = "/list.do")
    @ResponseBody
    public Map<String, Object> list(String queryJsonString) {
        Map<String, Object> map = new HashMap<>(2);
        if (!StringUtils.isEmpty(queryJsonString)) {
            map = JSON.parseObject(queryJsonString);
        }

        PageTbl page = this.getPage();
        String sortName = page.getSortname();
        if (StringUtils.isEmptyString(sortName)) {
            sortName = "";
            page.setSortname(sortName);
        }
        String sortOrder = page.getSortorder();
        if (StringUtils.isEmptyString(sortOrder)) {
            sortOrder = "desc";
            page.setSortorder(sortOrder);
        }
        PageUtils pageList = outStorageDetailService.getList(page,map);
        page.setTotalRows(pageList.getTotalCount() == 0 ? 1 : pageList.getTotalCount());
        map.put("rows", pageList.getList());
        executePageMap(map, page);
        return map;
    }

    /**
     * 导出Excel
     */
    @RequestMapping(value = "/toExcel.do")
    @ResponseBody
    public void artBomExcel() {
        Map<String,Object> map = Maps.newHashMap();
        map.put("qaCode",request.getParameter("qaCode"));
        map.put("queryStartTime",request.getParameter("queryStartTime"));
        map.put("queryEndTime",request.getParameter("queryEndTime"));
        map.put("shipNo", request.getParameter("shipNo"));
        map.put("ids", StringUtils.stringToInt(request.getParameter("ids")));
        outStorageDetailService.toExcel(response, "", outStorageDetailService.getAllLists(map),
                request.getParameter("queryExportExcelIndex").split(","));
    }
}
