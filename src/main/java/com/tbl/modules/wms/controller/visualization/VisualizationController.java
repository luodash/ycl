package com.tbl.modules.wms.controller.visualization;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.wms.entity.baseinfo.Car;
import com.tbl.modules.wms.entity.baseinfo.FactoryArea;
import com.tbl.modules.wms.entity.baseinfo.Shelf;
import com.tbl.modules.wms.entity.baseinfo.Warehouse;
import com.tbl.modules.wms.entity.productbind.DeviceUwbRoute;
import com.tbl.modules.wms.entity.storageinfo.StorageInfo;
import com.tbl.modules.wms.service.baseinfo.CarService;
import com.tbl.modules.wms.service.baseinfo.FactoryAreaService;
import com.tbl.modules.wms.service.baseinfo.ShelfService;
import com.tbl.modules.wms.service.baseinfo.WarehouseService;
import com.tbl.modules.wms.service.productbind.DeviceUwbRouteService;
import com.tbl.modules.wms.service.storageinfo.StorageInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * 可视化控制层
 * @author 70486
 */
@Controller
@RequestMapping("/visualization")
public class VisualizationController extends AbstractController {

	/**
	 * 行车uwb位置信息
	 */
	@Autowired
	private DeviceUwbRouteService deviceUwbRouteService;
	/**
	 * 库位信息
	 */
	@Autowired
	private ShelfService shelfService;
	/**
	 * 仓库
	 */
	@Autowired
	private WarehouseService warehouseService;
	/**
	 * 行车
	 */
	@Autowired
	private CarService carService;
	/**
	 * 厂区信息
	 */
	@Autowired
	private FactoryAreaService factoryAreaService;
	/**
	 * 库存
	 */
	@Autowired
	private StorageInfoService storageInfoService;
	
	/**
	 * pad可视化页面的跳转
	 * @param userId 用户主键
	 * @param carCode 行车编码
	 * @return ModelAndView
	 */
    @RequestMapping(value="/visualPad.do")
    public ModelAndView visualPad(String userId,String carCode){
    	ModelAndView mv = new ModelAndView();
    	FactoryArea factoryArea = null;
    	Warehouse warehouse = null;
    	Car car = carService.selectOne(new EntityWrapper<Car>().eq("CODE", carCode));
    	if(car!=null) {
			factoryArea = factoryAreaService.selectOne(new EntityWrapper<FactoryArea>().eq("CODE", car.getFactoryCode()));
			warehouse = warehouseService.selectOne(new EntityWrapper<Warehouse>()
					.eq("CODE", car.getWarehouseCode())
					.eq("FACTORYCODE", car.getFactoryCode()));
			mv.addObject("wareArea",car.getWarehouseArea().replace(",QK",""));
			mv.addObject("mission",car.getMission());
    	}
    	if(factoryArea!=null && warehouse!=null){
    		mv.addObject("factoryWarehouseAreaName",warehouse.getName()+"-"+car.getWarehouseArea()+"区域");
		}
		mv.addObject("userId",userId);
		mv.addObject("carCode",carCode);
		mv.setViewName("techbloom/visualization/visualization_index");
		return mv;
    }

	/**
	 * 库位指引可视化web页面
	 * @param userId 用户主键
	 * @return ModelAndView
	 */
	@RequestMapping(value="/visualWms.do")
	public ModelAndView visualWms(String userId){
		ModelAndView mv = new ModelAndView();
		mv.addObject("userId",userId);
		mv.setViewName("techbloom/visualization/visualization_platform");
		return mv;
	}

	/**
	 * 库存可视化页面的跳转
	 * @return ModelAndView
	 */
	@RequestMapping(value="/storageVisual.do")
	public ModelAndView storageVisual(){
		ModelAndView mv = new ModelAndView();
		mv.setViewName("techbloom/visualization/wms_visualization");
		return mv;
	}

	/**
	 * 获得行车位置
	 * @param userId 用户编码
	 * @param carCode 行车编码
	 * @return List<DeviceUwbRoute>
	 */
	@RequestMapping(value = "/getCarLocation")
	@ResponseBody
	public DeviceUwbRoute getCarLocation(String userId,String carCode){
		//获取行车实时坐标位置
		Car car = carService.selectOne(new EntityWrapper<Car>().eq("CODE", carCode));
		DeviceUwbRoute deviceUwbRoute = deviceUwbRouteService.selectOne(new EntityWrapper<DeviceUwbRoute>().eq("TAG", car.getTag()));
		deviceUwbRoute.setCarMission(car.getMission());
		return deviceUwbRoute;
	}
	
	/**
	 * 获得一个仓库区域的可用库位
	 * @param carCode 行车编码
	 * @return List<shelf>
	 */
	@RequestMapping(value = "/getShelfTabs")
    @ResponseBody
    public List<Shelf> getShelfTabs(String carCode) {
		Car car = carService.selectOne(new EntityWrapper<Car>().eq("CODE", carCode));
		List<Shelf> shelfList = shelfService.findShelfByArea(Arrays.asList(car.getWarehouseArea().split(",")));
		shelfList.forEach(shelf -> {
			if (shelf.getCode().equals(car.getMission())) {
				shelf.setVisualTargetShelf("1");
			}
		});
		Shelf shelf = shelfList.get(0);
		String codes = shelf.getCode().substring(0,1);
		if ("F".equals(codes)){
			codes = shelf.getCode().substring(0,2);
		}else if ("N".equals(codes)){
			codes = shelf.getCode().substring(0,2);
		}
		shelf.setCodes(codes);
		return shelfList;
    }

	/**
	 * 获得一个仓库区域的所有库位
	 * @param carCode 行车编码
	 * @return List<shelf>
	 */
	@RequestMapping(value = "/getAllShelfTabs")
	@ResponseBody
	public List<Shelf> getAllShelfTabs(String carCode) {
		List<Shelf> shelfList = shelfService.findAllShelfByArea(Arrays.asList(carService.selectOne(new EntityWrapper<Car>()
				.eq("CODE", carCode)).getWarehouseArea().split(",")));
		Shelf shelf = shelfList.get(0);
		shelf.setTotalNum(Long.parseLong(String.valueOf(shelfList.size())));
		shelf.setUseNum(Long.valueOf(String.valueOf((int) shelfList.stream().filter(s -> s.getState() == 0).count())));
		shelf.setProhibitNum(Long.valueOf(String.valueOf((int) shelfList.stream().filter(s -> s.getState() == 199).count())));
		shelf.setUnuseNum(shelf.getTotalNum() - shelf.getUseNum() - shelf.getProhibitNum());
		shelf.setUsePro(String.valueOf(shelf.getUnuseNum()*100/shelf.getTotalNum()));

		String codes = shelf.getCode().substring(0,1);
		if ("F".equals(codes)){
            codes = shelf.getCode().substring(0,2);
		}else if ("N".equals(codes)){
			codes = shelf.getCode().substring(0,2);
		}
		shelf.setCodes(codes);
		return shelfList;
	}

	/**
	 * 获得目标库位
	 * @param carCode 行车编码
	 * @return shelf 推荐库位、出库库位
	 */
	@RequestMapping(value = "/getShelfTarget")
	@ResponseBody
	public Shelf getShelfTarget(String carCode) {
		Shelf shelf = null;
		Car car = carService.selectOne(new EntityWrapper<Car>().eq("CODE", carCode));
		if(car!=null&&car.getMission()!=null){
			shelf = shelfService.selectByMap(new HashMap<String,Object>(3){{
				put("CODE",car.getMission());
				put("FACTORYCODE", car.getFactoryCode());
				put("WAREHOUSECODE", car.getWarehouseCode());
			}}).get(0);
		}

		return shelf;
	}

	/**
	 * 点击库位跳转查看弹框
	 * @param id 库位主键
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/toDetail.do")
	@ResponseBody
	public ModelAndView toDetail(Long id) {
		ModelAndView mv = this.getModelAndView();
		Shelf shelf = shelfService.selectById(id);
		List<StorageInfo> lstStorageInfo = storageInfoService.getStorageInfoByShelf(shelf.getFactoryCode(), shelf.getWarehouseCode(),
				shelf.getCode());
		StorageInfo storageInfo = lstStorageInfo!=null && lstStorageInfo.size()>0 ? lstStorageInfo.get(0) : new StorageInfo();
		storageInfo.setShelfCode(shelf.getCode());
		mv.setViewName("techbloom/visualization/visualization_detail");
		mv.addObject("storageInfo", storageInfo);

		return mv;
	}

}
