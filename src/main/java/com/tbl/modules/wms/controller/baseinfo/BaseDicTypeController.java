package com.tbl.modules.wms.controller.baseinfo;

import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.wms.service.baseinfo.BaseDicTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * 下拉框固定选择项
 * @author 70486
 */
@Controller
@RequestMapping(value = "/BaseDicType")
public class BaseDicTypeController extends AbstractController {

    /**
     * 下拉框选择类服务
     */
    @Autowired
    private BaseDicTypeService baseDicTypeService;

    /**
     * 获取入库状态列表信息 ：下拉框
     * @param queryString 查询条件
     * @param pageSize 页大小
     * @param pageNo 页码
     * @return Map<String,Object>
     */
    @RequestMapping(value = "/getInstorageTypeList")
    @ResponseBody
    public Map<String, Object> getInstorageTypeList(String queryString, int pageSize, int pageNo) {
        Map<String, Object> map = new HashMap<>(7);
        PageTbl page = this.getPage();
        map.put("page", pageNo);
        map.put("limit", pageSize);
        String sortOrder = page.getSortorder();
        if (StringUtils.isEmptyString(sortOrder)) {
            sortOrder = "desc";
            page.setSortorder(sortOrder);
        }
        map.put("sidx", page.getSortname());
        map.put("order", page.getSortorder());
        map.put("dname", queryString.toUpperCase());
        map.put("result", baseDicTypeService.getInstorageTypeList(map));
        return map;
    }

    /**
     * 获取入库阶段性时间列表信息 ：下拉框
     * @param queryString 查询条件
     * @param pageSize 页大小
     * @param pageNo 页码
     * @return Map<String,Object>
     */
    @RequestMapping(value = "/getTimeTypeList")
    @ResponseBody
    public Map<String, Object> getTimeTypeList(String queryString, int pageSize, int pageNo) {
        Map<String, Object> map = new HashMap<>(7);
        PageTbl page = this.getPage();
        map.put("page", pageNo);
        map.put("limit", pageSize);
        String sortOrder = page.getSortorder();
        if (StringUtils.isEmptyString(sortOrder)) {
            sortOrder = "desc";
            page.setSortorder(sortOrder);
        }
        map.put("sidx", page.getSortname());
        map.put("order", page.getSortorder());
        map.put("dname", queryString.toUpperCase());
        map.put("result", baseDicTypeService.getTimeTypeList(map));
        return map;
    }

    /**
     * 获取出库类型列表信息 ：下拉框
     * @param queryString 查询条件
     * @param pageSize 页大小
     * @param pageNo 页码
     * @return Map<String,Object>
     */
    @RequestMapping(value = "/getOutStorageTypeList")
    @ResponseBody
    public Map<String, Object> getOutStorageTypeList(String queryString, int pageSize, int pageNo) {
        Map<String, Object> map = new HashMap<>(7);
        PageTbl page = this.getPage();
        map.put("page", pageNo);
        map.put("limit", pageSize);
        String sortOrder = page.getSortorder();
        if (StringUtils.isEmptyString(sortOrder)) {
            sortOrder = "desc";
            page.setSortorder(sortOrder);
        }
        map.put("sidx", page.getSortname());
        map.put("order", page.getSortorder());
        map.put("dname", queryString.toUpperCase());
        map.put("result", baseDicTypeService.getOutStorageTypeList(map));
        return map;
    }

    /**
     * 获取打印信息 ：下拉框
     * @param queryString 查询条件
     * @param pageSize 页大小
     * @param pageNo 页码
     * @param description
     * @return Map<String,Object>
     */
    @RequestMapping(value = "/getKindList")
    @ResponseBody
    public Map<String, Object> getKindList(String queryString, int pageSize, int pageNo, String description) {
        Map<String, Object> map = new HashMap<>(7);
        PageTbl page = this.getPage();
        map.put("page", pageNo);
        map.put("limit", pageSize);
        String sortOrder = page.getSortorder();
        if (StringUtils.isEmptyString(sortOrder)) {
            sortOrder = "desc";
            page.setSortorder(sortOrder);
        }
        map.put("sidx", page.getSortname());
        map.put("order", page.getSortorder());
        map.put("dname", queryString.toUpperCase());
        map.put("description", description);
        map.put("result", baseDicTypeService.getPrinterHierarchyList(map));
        return map;
    }


    /**
     * 获取质检状态下拉框
     * @return
     */
    @RequestMapping(value = "/qc",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getQcDict() {
        return baseDicTypeService.getQcDict();
    }

    /**
     * 获取其它出入库(事务列表)下拉框
     * @return
     */
    @RequestMapping(value = "/otherTransaction/{type}",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getOtherTransactionDict(@PathVariable ("type") Integer type) {
        return baseDicTypeService.getOtherTransactionDict(type);
    }


    @RequestMapping(value = "/lyyt/{id}",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getLyytDict(@PathVariable("id") Integer id){
        return baseDicTypeService.getLyytDict(id);
    }

}
