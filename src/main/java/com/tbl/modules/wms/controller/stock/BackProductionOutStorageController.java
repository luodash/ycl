package com.tbl.modules.wms.controller.stock;

import com.alibaba.fastjson.JSON;
import com.tbl.common.utils.DateUtils;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.platform.constant.LogActionConstant;
import com.tbl.modules.platform.constant.MenuConstant;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.service.system.LogService;
import com.tbl.modules.platform.service.system.RoleService;
import com.tbl.modules.platform.util.DeriveExcel;
import com.tbl.modules.wms.service.stock.OtherOutStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 退库清单（退库回生产线清单+采购退库清单）
 * @author 70486
 * @date 2020/3/17
 */
@Controller
@RequestMapping(value = "/backProductionOutStorage")
public class BackProductionOutStorageController extends AbstractController {

    /**
     * 其他出库
     */
    @Autowired
    OtherOutStorageService otherOutStorageService;
    /**
     * 日志信息
     */
    @Autowired
    LogService logService;
    /**
     * 角色
     */
    @Autowired
    private RoleService roleService;

    /**
     * 跳转到退回产线+采购退货列表页面
     * @return ModelAndView
     */
    @RequestMapping(value = "/toList")
    public ModelAndView toList() {
        ModelAndView mv = this.getModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(getSessionUser().getRoleId(), MenuConstant.backProductionOutStorage));
        mv.setViewName("techbloom/stock/backProductionOutStorage/back_production_outstorage_list");
        return mv;
    }

    /**
     * 获取退库回生产线清单+采购退货列表数据
     * @param queryJsonString 查询条件
     * @return Map<String,Object>
     */
    @RequestMapping(value = "/list.do")
    @ResponseBody
    public Map<String, Object> list(String queryJsonString) {
        Map<String, Object> map = new HashMap<>(4);
        if (!StringUtils.isEmptyString(queryJsonString)) {
            map = JSON.parseObject(queryJsonString);
        }
        map.put("type", Arrays.stream("1,2".split(",")).map(Long::parseLong).collect(Collectors.toList()));
        PageTbl page = this.getPage();
        PageUtils utils = otherOutStorageService.getList(page, map);
        page.setTotalRows(utils.getTotalCount() == 0 ? 1 : utils.getTotalCount());
        //初始化分页对象
        executePageMap(map, page);
        map.put("rows", utils.getList());
        map.put("total", utils.getTotalPage() == 0 ? 1 : utils.getTotalPage());
        return map;
    }

    /**
     * 导出Excel
     * @param request
     * @param response
     */
    @RequestMapping(value = "/toExcel.do")
    @ResponseBody
    public void toExcel(HttpServletRequest request, HttpServletResponse response) {
        try {
            String sheetName = "退回产线出库" + "(" + DateUtils.getDay() + ")";
            response.setHeader("Content-Type", "application/force-download");
            response.setHeader("Content-Type", "application/vnd.ms-excel");
            response.setCharacterEncoding("UTF-8");
            response.setHeader("Expires", "0");
            response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
            response.setHeader("Pragma", "public");
            response.setHeader("Content-disposition", "attachment;filename=" + new String(sheetName.getBytes("gbk"),
                    "ISO8859-1") + ".xls");

            Map<String, String> mapFields = new LinkedHashMap<>();
            mapFields.put("typeStr", "类型");
            mapFields.put("qaCode", "质保单号");
            mapFields.put("batchNo", "批次号");
            mapFields.put("warehouseName", "仓库");
            mapFields.put("shelfCode", "库位");
            mapFields.put("materialName", "物料名称");
            mapFields.put("salesname", "销售经理");
            mapFields.put("createTimeStr", "退库时间");
            mapFields.put("dishcode", "盘号");
            mapFields.put("drumType", "盘类型");
            mapFields.put("model", "盘规格");
            mapFields.put("outerDiameter", "盘外径");
            mapFields.put("meter", "米数");

            Map<String, Object> map = new LinkedHashMap<>();
            map.put("qaCode", request.getParameter("qaCode"));
            map.put("startTime", request.getParameter("startTime"));
            map.put("endTime", request.getParameter("endTime"));
            map.put("type",Arrays.stream("1,2".split(",")).map(Long::parseLong).collect(Collectors.toList()));
            DeriveExcel.exportExcel(sheetName, otherOutStorageService.getExcelList(map), mapFields, response, "");
            logService.logInsert("退库清单", LogActionConstant.USER_EXPORT, request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
