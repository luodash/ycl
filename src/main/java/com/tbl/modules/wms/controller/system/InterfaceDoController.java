package com.tbl.modules.wms.controller.system;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.google.common.collect.Maps;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.platform.constant.MenuConstant;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.service.system.RoleService;
import com.tbl.modules.wms.constant.EmumConstant;
import com.tbl.modules.wms.entity.interfacelog.InterfaceDo;
import com.tbl.modules.wms.service.interfacelog.InterfaceDoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

/**
 * 离线接口控制层
 * @author 70486
 */
@Controller
@RequestMapping(value = "/interfaceDo")
public class InterfaceDoController extends AbstractController {

    /**
     * 用户service
     */
    @Autowired
    private RoleService roleService;
    /**
     * 接口离线执行表服务类
     */
    @Autowired
    private InterfaceDoService interfaceDoService;

    /**
     * 跳转到离线接口维护列表页面
     * @return ModelAndView
     */
    @RequestMapping(value = "/toList.do")
    @ResponseBody
    public ModelAndView toList() {
        ModelAndView mv = new ModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(getSessionUser().getRoleId(), MenuConstant.interfaceDo));
        mv.setViewName("techbloom/system/interfacedo/interfacedo_list");
        return mv;
    }

    /**
     * 获取离线接口列表数据
     * @param queryJsonString
     * @return Object
     */
    @RequestMapping(value = "/list.do")
    @ResponseBody
    public Object list(String queryJsonString) {
    	Map<String, Object> map = new HashMap<>(6);
        if (!StringUtils.isEmpty(queryJsonString)) {
            map = JSON.parseObject(queryJsonString);
        }
        PageTbl page = this.getPage();
        map.put("page", page.getPageno());
        map.put("limit", page.getPagesize());
        String sortName = page.getSortname();
        if (StringUtils.isEmptyString(sortName)) {
            sortName = "id";
            page.setSortname(sortName);
        }
        String sortOrder = page.getSortorder();
        if (StringUtils.isEmptyString(sortOrder)) {
            sortOrder = "desc";
            page.setSortorder(sortOrder);
        }
        map.put("sidx", page.getSortname());
        map.put("order", page.getSortorder());
        PageUtils pageUser = interfaceDoService.queryPage(map);
        page.setTotalRows(pageUser.getTotalCount() == 0 ? 1 : pageUser.getTotalCount());
        map.put("rows", pageUser.getList());
        executePageMap(map, page);
        return map;
    }

    /**
     * 保存/修改离线接口执行报文
     * @param paramsInfo 离线接口执行信息
     * @return Map<String,Object>
     */
    @RequestMapping(value = "/save")
    @ResponseBody
    public Map<String, Object> save(String paramsInfo, Long id) {
        Map<String, Object> map = Maps.newHashMap();
        InterfaceDo interfaceDo = interfaceDoService.selectById(id);
        interfaceDo.setParamsinfo(paramsInfo.replaceAll("&lt;", "<").replaceAll("&gt;", ">"));
        map.put("result", interfaceDoService.updateById(interfaceDo));
        return map;
    }

    /**
     * 返回到离线接口请求报文修改页面
     * @param id 离线接口主键
     * @return ModelAndView
     */
    @RequestMapping(value = "/interfacedo_edit")
    public ModelAndView interfacedoEdit(Long id) {
    	ModelAndView mv = new ModelAndView();
        mv.addObject("id",id);
        mv.addObject("requestParams",interfaceDoService.selectById(id).getParamsinfo());
        mv.setViewName("techbloom/system/interfacedo/interfacedo_edit");
        return mv;
    }

    /**
     * 弹出到明细页面
     * @param id 离线接口执行表主键
     * @return ModelAndView
     */
    @RequestMapping(value = "/toDetailList.do")
    @ResponseBody
    public ModelAndView toDetailList(Long id) {
        InterfaceDo interfaceDo = interfaceDoService.selectById(id);
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("techbloom/system/interfacedo/interfacedo_detail");
        mv.addObject("INTERFACECODE",interfaceDo.getInterfacecode());
        mv.addObject("OPERATETIME",interfaceDo.getOperatetime());
        mv.addObject("ISSUCCESS", EmumConstant.interfaceDoState.NEW.getCode().equals(interfaceDo.getIssuccess())?"新增":
                EmumConstant.interfaceDoState.SUCCESS.getCode().equals(interfaceDo.getIssuccess())?"成功":
                EmumConstant.interfaceDoState.FAIL.getCode().equals(interfaceDo.getIssuccess())?"失败":
                EmumConstant.interfaceDoState.DOING.getCode().equals(interfaceDo.getIssuccess())?"进行中":
                EmumConstant.interfaceDoState.PLATFORM_OUTSTORAGE_FAIL.getCode().equals(interfaceDo.getIssuccess())? "出库在线调用失败":"Error");
        mv.addObject("FLASECONTENT",interfaceDo.getFlasecontent());
        mv.addObject("PARAMSINFO",interfaceDo.getParamsinfo());
        mv.addObject("RETURNCODE",interfaceDo.getReturncode());
        mv.addObject("QACODES",interfaceDo.getQacode());
        mv.addObject("BATCH_NOES",interfaceDo.getBatchNo());
        return mv;
    }

    /**
     * 执行状态设为挂起
     * @param ids 离线接口执行表主键
     */
    @RequestMapping(value = "/setSuccess.do")
    @ResponseBody
    public void setSuccess(Long[] ids) {
        InterfaceDo interfaceDo = new InterfaceDo();
        interfaceDo.setIssuccess(EmumConstant.interfaceDoState.DOING.getCode());
        interfaceDoService.update(interfaceDo, new EntityWrapper<InterfaceDo>().in("ID", ids));
    }

    /**
     * 执行状态设为待执行
     * @param ids 离线接口执行表主键
     * @return ModelAndView
     */
    @RequestMapping(value = "/setConcal.do")
    @ResponseBody
    public void setConcal(Long[] ids) {
        InterfaceDo interfaceDo = new InterfaceDo();
        interfaceDo.setIssuccess(EmumConstant.interfaceDoState.NEW.getCode());
        interfaceDoService.update(interfaceDo, new EntityWrapper<InterfaceDo>().in("ID", ids));
    }

}
