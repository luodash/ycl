package com.tbl.modules.wms.controller.instorage;

import com.alibaba.fastjson.JSON;
import com.tbl.common.utils.DateUtils;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.platform.constant.LogActionConstant;
import com.tbl.modules.platform.constant.MenuConstant;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.service.system.LogService;
import com.tbl.modules.platform.service.system.RoleService;
import com.tbl.modules.platform.util.DeriveExcel;
import com.tbl.modules.wms.service.instorage.InstorageDetailService;
import com.tbl.modules.wms.service.instorage.InstorageService;
import com.tbl.modules.wms.service.inventory.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 入库管理--订单展示
 * @author 70486
 */
@Controller
@RequestMapping(value = "/orderDisplay")
public class OrderDisplayController extends AbstractController {

    /**
     * 入库管理-入库表
     */
    @Autowired
    private InstorageService instorageService;
    /**
     * 日志信息
     */
    @Autowired
    private LogService logService;
    /**
     * 角色
     */
    @Autowired
    private RoleService roleService;
    /**
     * 盘点计划服务类
     */
    @Autowired
    private InventoryService inventoryService;
    /**
     * 入库明细表
     */
    @Autowired
    private InstorageDetailService instorageDetailService;

    /**
     * 跳转到订单展示页面
     * @return ModelAndView
     */
    @RequestMapping(value = "/toList")
    public ModelAndView toList() {
        ModelAndView mv = this.getModelAndView();
        //okk
        mv.addObject("factoryList",inventoryService.getFactoryList(Arrays.asList(getSessionUser().getFactoryCode().split(","))));
        mv.addObject("operationCode", roleService.selectOperationByRoleId(getSessionUser().getRoleId(), MenuConstant.orderDisplay));
        mv.setViewName("techbloom/instorage/orderDisplay/orderDisplay_list");
        return mv;
    }

    /**
     * 获取订单展示列表数据
     * @param queryJsonString 查询条件
     * @return Map<String,Object>
     */
    @RequestMapping(value = "/list.do")
    @ResponseBody
    public Map<String, Object> list(String queryJsonString) {
        Map<String, Object> map = new HashMap<>(4);
        if (!StringUtils.isEmptyString(queryJsonString)) {
            map = JSON.parseObject(queryJsonString);
        }
        //okk
        map.put("org", Arrays.asList(getSessionUser().getFactoryCode().split(",")));
        PageTbl page = this.getPage();
        PageUtils utils = instorageService.getPageList(page, map);
        page.setTotalRows(utils.getTotalCount() == 0 ? 1 : utils.getTotalCount());
        //初始化分页对象
        executePageMap(map, page);
        map.put("rows", utils.getList());
        map.put("total", utils.getTotalPage() == 0 ? 1 : utils.getTotalPage());
        return map;
    }

    /**
     * 弹出到明细页面
     * @param id 入库主表主键
     * @return ModelAndView
     */
    @RequestMapping(value = "/toDetailList.do")
    @ResponseBody
    public ModelAndView toDetailList(Long id) {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("techbloom/instorage/orderDisplay/instorage_detail");
        mv.addObject("infoId", id);
        return mv;
    }

    /**
     * 订单展示明细信息
     * @param id 入库主表主键
     * @return Map<String,Object>
     */
    @RequestMapping(value = "/detailList.do")
    @ResponseBody
    public Map<String, Object> detailList(Long id) {
        Map<String, Object> map = new HashMap<>(4);
        map.put("id", id);
        //okk
        map.put("org", Arrays.asList(getSessionUser().getFactoryCode().split(",")));
        PageTbl page = this.getPage();
        PageUtils utils = instorageService.getPageDetgailList(page, map);
        page.setTotalRows(utils.getTotalCount() == 0 ? 1 : utils.getTotalCount());
        //初始化分页对象
        executePageMap(map, page);
        map.put("rows", utils.getList());
        map.put("total", utils.getTotalPage() == 0 ? 1 : utils.getTotalPage());
        return map;
    }

    /**
     * 跳转到rfid绑定页面
     * @return ModelAndView
     */
    @RequestMapping(value = "/toBindRfid.do")
    @ResponseBody
    public ModelAndView toBindRfid() {
        //        mv.setViewName("techbloom/instorage/orderDisplay/orderDisplayDetail_edit");
        return new ModelAndView();
    }

    /**
     * 获取行车信息
     * @param id 行车主键
     * @return Map<String,Object>
     */
    @RequestMapping(value = "/getCarById")
    @ResponseBody
    public Map<String,Object> getCarById(Long id) {
        Map<String, Object> objMap = new HashMap<>(1);
        objMap.put("id", id);
        return objMap;
    }

    /**
     * 导出明细Excel
     * @param request
     * @param response
     */
    @RequestMapping(value = "/detailExcel.do", method = RequestMethod.POST)
    @ResponseBody
    public void detailExcel(HttpServletRequest request, HttpServletResponse response)  {
        try {
            String sheetName = "订单展示明细" + "(" + DateUtils.getDay() + ")";
            response.setHeader("Content-Type", "application/force-download");
            response.setHeader("Content-Type", "application/vnd.ms-excel");
            response.setCharacterEncoding("UTF-8");
            response.setHeader("Expires", "0");
            response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
            response.setHeader("Pragma", "public");
            response.setHeader("Content-disposition", "attachment;filename=" + new String(sheetName.getBytes("gbk"),
                    "ISO8859-1") + ".xls");

            Map<String, String> mapFields = new LinkedHashMap<>();
            mapFields.put("stateCode", "状态");
            mapFields.put("entityNo", "工单号");
            mapFields.put("batchNo", "批次号");
            mapFields.put("qaCode", "质保单号");
            mapFields.put("meter", "数量");
            mapFields.put("unit", "单位");
            mapFields.put("weight", "重量");

            mapFields.put("colour", "颜色");
            mapFields.put("segmentno", "段号");
            mapFields.put("dishcode", "盘号");
            mapFields.put("dishnumber", "盘踞编码");
            mapFields.put("model", "盘规格");
            mapFields.put("outerdiameter", "盘外径");

            mapFields.put("ordernum", "订单号");
            mapFields.put("orderline", "订单行号");
            mapFields.put("materialCode", "物料编码");
            mapFields.put("materialName", "物料名称");
            mapFields.put("salesName", "营销经理名称");
            mapFields.put("factoryName", "厂区");
            mapFields.put("dishnum", "盘数");
            mapFields.put("createtimeStr", "创建时间");


            DeriveExcel.exportExcel(sheetName, instorageDetailService.selectInstorageAndDetail(StringUtils.stringToList(request.getParameter("ids")),
                    request.getParameter("infoid")), mapFields, response, "");
            logService.logInsert("订单展示明细导出", LogActionConstant.USER_EXPORT, request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
