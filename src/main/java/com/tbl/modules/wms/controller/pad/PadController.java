package com.tbl.modules.wms.controller.pad;

import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.wms.constant.PdaResult;
import com.tbl.modules.wms.dao.baseinfo.CarDAO;
import com.tbl.modules.wms.entity.baseinfo.Car;
import com.tbl.modules.wms.service.pad.PadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Pad接口
 * @author 70486
 */
@Controller
@RequestMapping("/pad")
public class PadController extends AbstractController {

    /**
     * pad接口服务
     */
    @Autowired
    private PadService padService;

    /**
     * 行车
     */
    @Autowired
    private CarDAO carDAO;

    /**
     * 功能描述:用户登陆
     * @param username 用户名
     * @param password 密码
     * @return PdaResult
     */
    @RequestMapping(value = "/login")
    @ResponseBody
    public PdaResult findBaseInfoByMaterialBarCode(String username, String password) {
        return padService.login(username, password);
    }

    /**
     * 模块：入库管理
     * 接口编号：1.1
     * 功能描述: 获取详情列表
     * 入参：userId,carCode
     * 返回：详情表列表 状态为已开始,未完成的
     */
    @RequestMapping(value = "/storageInList")
    @ResponseBody
    public PdaResult storageInList(Long userId, String carCode) {
        return padService.storageInList(userId, carCode);
    }

    /**
     * 模块：入库管理
     * 接口编号：1.2
     * 功能描述: 入库完成
     * 入参：rfid,shelfCode(库位编码),userId(用户id),date
     * 返回：PdaResult
     */
    @RequestMapping(value = "/storageInSuccess")
    @ResponseBody
    public PdaResult storageInSuccess(String rfid, String shelfCode, Long userId, String date) {
        return padService.storageInSuccess(rfid, shelfCode, userId, date);
    }
    
    /**
     * 模块：入库管理
     * 接口编号：1.4
     * 功能描述: 入库:选择可用行车
     * 入参：warehouseCode(仓库编码);factoryCode(厂区编码)
     * 返回：行车列表
     */
    @RequestMapping(value = "/storageinStorageinCar")
    @ResponseBody
    public PdaResult storageinStorageinCar(String warehouseCode,String factoryCode) {
    	return padService.storageinStorageinCar(warehouseCode, factoryCode);
    }
    
    /**
     * 模块：出库管理
     * 接口编号：2.1
     * 功能描述: 获取详情列表
     * 入参：userId,carCode
     * 返回：详情表列表 状态为已开始,未完成的
     */
    @RequestMapping(value = "/storageOutList")
    @ResponseBody
    public PdaResult storageOutList(Long userId, String carCode) {
        return padService.storageOutList(userId, carCode);
    }

    /**
     * 模块：出库管理
     * 接口编号：2.2
     * 功能描述: 出库完成
     * 入参：rfid,meter,userId(用户id)
     * 返回：PdaResult
     */
    @RequestMapping(value = "/storageOutSuccess")
    @ResponseBody
    public PdaResult storageOutSuccess(String rfid, String meter, Long userId) {
        return padService.storageOutSuccess(rfid, meter, userId);
    }

    /**
     * 模块：调拨出库管理
     * 接口编号：3.1
     * 功能描述: 获取详情列表
     * 入参：userId,carCode
     * 返回：详情表列表 状态为已开始,未完成的
     */
    @RequestMapping(value = "/alloStorageOutList")
    @ResponseBody
    public PdaResult alloStorageOutList(Long userId, String carCode) {
        return padService.alloStorageOutList(userId, carCode);
    }

    /**
     * 模块：调拨出库管理
     * 接口编号：3.2
     * 功能描述: 出库完成
     * 入参：rfid,userId(用户id)
     * 返回：PdaResult
     */
    @RequestMapping(value = "/alloStorageOutSuccess")
    @ResponseBody
    public PdaResult alloStorageOutSuccess(String rfid, Long userId) {
        return padService.alloStorageOutSuccess(rfid, userId);
    }

    /**
     * 模块：调拨入库管理
     * 接口编号：3.3
     * 功能描述: 获取详情列表
     * 入参：userId,carCode
     * 返回：详情表列表 状态为已开始,未完成的
     */
    @RequestMapping(value = "/alloStorageInList")
    @ResponseBody
    public PdaResult alloStorageInList(Long userId, String carCode) {
        return padService.alloStorageInList(userId, carCode);
    }

    /**
     * 模块：调拨入库管理
     * 接口编号：3.4
     * 功能描述: 入库完成
     * 入参：rfid,userId,shelfCode
     * 返回：PdaResult
     */
    @RequestMapping(value = "/alloStorageInSuccess")
    @ResponseBody
    public PdaResult alloStorageInSuccess(String rfid, Long userId, String shelfCode) {
        return padService.alloStorageInSuccess(rfid, userId, shelfCode);
    }

    /**
     * 模块：移库出库管理
     * 接口编号：4.1
     * 功能描述: 获取详情列表
     * 入参：userId,carCode
     * 返回：详情表列表 状态为已开始,未完成的
     */
    @RequestMapping(value = "/moveStorageOutList")
    @ResponseBody
    public PdaResult moveStorageOutList(Long userId, String carCode) {
        return padService.moveStorageOutList(userId, carCode);
    }

    /**
     * 模块：移库出库管理
     * 接口编号：4.2
     * 功能描述: 出库完成
     * 入参：rfid,userId(用户id)
     * 返回：PdaResult
     */
    @RequestMapping(value = "/moveStorageOutSuccess")
    @ResponseBody
    public PdaResult moveStorageOutSuccess(String rfid, Long userId) {
        return padService.moveStorageOutSuccess(rfid, userId);
    }

    /**
     * 模块：移库入库管理
     * 接口编号：4.3
     * 功能描述: 获取详情列表
     * 入参：userId,carCode
     * 返回：详情表列表 状态为已开始,未完成的
     */
    @RequestMapping(value = "/moveStorageInList")
    @ResponseBody
    public PdaResult moveStorageInList(Long userId, String carCode) {
        return padService.moveStorageInList(userId, carCode);
    }

    /**
     * 模块：移库入库管理
     * 接口编号：4.4
     * 功能描述: 入库完成
     * 入参：rfid,userId(用户id),shelfId
     * 返回：PdaResult
     */
    @RequestMapping(value = "/moveStorageInSuccess")
    @ResponseBody
    public PdaResult moveStorageInSuccess(String rfid, Long userId, String shelfId) {
        return padService.moveStorageInSuccess(rfid, userId, shelfId);
    }

    /**
     * 模块：拆分管理
     * 接口编号：55TFSI
     * 功能描述: 获取状态为“已开始”的拆分入库物料
     * 入参：carCode(行车编码)
     * 返回：PdaResult
     */
    @RequestMapping(value = "/getSplitInstorageList")
    @ResponseBody
    public PdaResult getSplitInstorageList(String carCode) {
        return padService.getSplitInstorageList(carCode);
    }

    /**
     * 模块：拆分
     * 拆分完成入库
     * @param rfid;shelfCode;userId
     * @return PdaResult
     */
    @RequestMapping(value = "/endInstorage")
    @ResponseBody
    public PdaResult endInstorage(String rfid, String shelfCode, String userId) {
        return padService.endInstorage(rfid, shelfCode, userId);
    }

    /**
     * 模块：可视化管理
     * 接口编号：5.0
     * 功能描述: 可视化界面
     * 入参：userId(用户id)；carCode(行车编码)；
     * 返回：String
     */
    @RequestMapping(value="/padVisualization")
    @ResponseBody
    public PdaResult padVisualization(String userId, String carCode){
		String returnUrl = "http://"+request.getServerName()+":"+request.getServerPort()+ request.getServletContext().getContextPath()
				+"/visualization/visualPad.do?userId="+userId+"&carCode="+carCode;
		Car car = carDAO.selectOne(new Car(){{
		    setCode(carCode);
        }});
		car.setLoginer(Integer.valueOf(userId));
        carDAO.updateById(car);
		return new PdaResult(returnUrl);
    }

    /**
     * 根据用户获取厂区
     * @param userId
     * @return List<FactoryArea>
     */
    @RequestMapping(value = "/getFactorys")
    @ResponseBody
    public PdaResult getFactorys(String userId){
        return padService.getFactorys(userId);
    }
}
