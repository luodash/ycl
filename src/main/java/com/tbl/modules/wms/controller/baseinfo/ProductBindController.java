package com.tbl.modules.wms.controller.baseinfo;

import com.alibaba.fastjson.JSON;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.platform.constant.MenuConstant;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.service.system.RoleService;
import com.tbl.modules.wms.entity.productbind.ProductBind;
import com.tbl.modules.wms.service.productbind.ProductBindService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * 产线管理控制层
 * @author 70486
 */
@Controller
@RequestMapping(value = "/productBind")
public class ProductBindController extends AbstractController {

    /**
     * 产线ip对应关系表
     */
    @Autowired
    private ProductBindService productBindService;

    /**
     * 角色信息
     */
    @Autowired
    private RoleService roleService;

    /**
     * 跳转到产线ip管理列表页面
     * @return ModelAndView
     */
    @RequestMapping(value = "/toList")
    public ModelAndView toList() {
        ModelAndView mv = this.getModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(getSessionUser().getRoleId(), MenuConstant.productbind));
        mv.setViewName("techbloom/productbind/product_list");
        return mv;
    }

    /**
     * 获取产线ip绑定列表列表数据
     * @param queryJsonString 查询条件
     * @return Map<String,Object>
     */
    @RequestMapping(value = "/list.do")
    @ResponseBody
    public Map<String, Object> list(String queryJsonString) {
        Map<String, Object> map = new HashMap<>(4);
        if (!StringUtils.isEmptyString(queryJsonString)) {
            map = JSON.parseObject(queryJsonString);
        }
        PageTbl page = this.getPage();
        PageUtils utils = productBindService.getPageList(page, map);
        page.setTotalRows(utils.getTotalCount() == 0 ? 1 : utils.getTotalCount());
        //初始化分页对象
        executePageMap(map, page);
        map.put("rows", utils.getList());
        map.put("total", utils.getTotalPage() == 0 ? 1 : utils.getTotalPage());
        return map;
    }

    /**
     * 弹出到产线ip编辑页面
     * @param id 产线ip主键
     * @return ModelAndView
     */
    @RequestMapping(value = "/toAdd.do")
    @ResponseBody
    public ModelAndView toAdd(Long id) {
        ModelAndView mv = this.getModelAndView();
        mv.addObject("productBind", productBindService.selectById(id));
        mv.setViewName("techbloom/productbind/product_edit");
        return mv;
    }


    /**
     * 保存产线ip信息
     * @param productBind 产线ip、webservice地址 实体类
     * @return Map<String,Object>
     */
    @RequestMapping(value = "/saveProductBind")
    @ResponseBody
    public Map<String, Object> saveProductBind(ProductBind  productBind) {
        Map<String, Object> map = new HashMap<>(2);
        boolean result = productBindService.insertOrUpdate(productBind);
        map.put("result", result);
        map.put("msg",result?"保存成功！":"保存失败！");
        return map;
    }

    /**
     * 删除产线ip信息
     * @param ids 产线主键
     * @return Map<String,Object>
     */
    @RequestMapping(value = "/deleteBind")
    @ResponseBody
    public Map<String, Object> deleteBind(String ids) {
        Map<String, Object> map = new HashMap<>(2);
        boolean result = productBindService.deleteBatchIds(Arrays.asList(ids.split(",")));
        map.put("result", result);
        map.put("msg", result?"删除成功！":"删除失败！");
        return map;
    }


}
