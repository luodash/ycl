package com.tbl.modules.wms.controller.stock;

import com.tbl.modules.platform.controller.AbstractController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 盘点执行控制层
 * @author 70486
 */
@Controller
@RequestMapping(value = "/inventoryExecution")
public class InventoryExecutionController extends AbstractController {

    /**
     * 跳转到车辆管理列表页面
     * @return ModelAndView
     */
    @RequestMapping(value = "/toList")
    public ModelAndView toList() {
        return this.getModelAndView();
    }

}
