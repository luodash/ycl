package com.tbl.modules.wms.controller.stock;

import com.tbl.common.utils.DateUtils;
import com.tbl.modules.platform.constant.LogActionConstant;
import com.tbl.modules.platform.constant.MenuConstant;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.service.system.LogService;
import com.tbl.modules.platform.service.system.RoleService;
import com.tbl.modules.wms.service.stock.OtherOutStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 采购退货出库控制层
 * @author 70486
 */
@Controller
@RequestMapping(value = "/procurementOutStorage")
public class ProcurementOutStorageController extends AbstractController {

    /**
     * 其他出库
     */
    @Autowired
    OtherOutStorageService otherOutStorageService;
    /**
     * 日志信息
     */
    @Autowired
    LogService logService;
    /**
     * 角色
     */
    @Autowired
    private RoleService roleService;

    /**
     * 跳转到采购退货出库
     * @return ModelAndView
     */
    @RequestMapping(value = "/toList")
    public ModelAndView toList() {
        ModelAndView mv = this.getModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(getSessionUser().getRoleId(), MenuConstant.procurementOutStorage));
        mv.setViewName("techbloom/stock/procurementOutStorage/procurement_outstorage_list");
        return mv;
    }

    /**
     * 获取采购退货清单列表数据
     * @param queryJsonString 查询条件
     * @return Map<String,Object>
     */
    @RequestMapping(value = "/list.do")
    @ResponseBody
    public Map<String, Object> list(String queryJsonString) {
        Map<String, Object> map = new HashMap<>(1);
//        if (!StringUtils.isEmpty(queryJsonString)) {
//            map = JSON.parseObject(queryJsonString);
//        }
//        PageTbl page = this.getPage();
//        map.put("page", page.getPageno());
//        map.put("limit", page.getPagesize());
//        String sortName = page.getSortname();
//        if (StringUtils.isEmptyString(sortName)) {
//            sortName = "";
//            page.setSortname(sortName);
//        }
//        String sortOrder = page.getSortorder();
//        if (StringUtils.isEmptyString(sortOrder)) {
//            sortOrder = "desc";
//            page.setSortorder(sortOrder);
//        }
//        map.put("sidx", "id");
//        map.put("order", page.getSortorder());
//        map.put("factoryCode", getSessionUser().getFactoryCode());
//        // type = 2  为采购退货出库类型
//        map.put("type", Arrays.asList("2").stream().map(s -> Long.parseLong(s.trim())).collect(Collectors.toList()));
//        PageUtils pageList = otherOutStorageService.getList(map);
//        page.setTotalRows(pageList.getTotalCount() == 0 ? 1 : pageList.getTotalCount());
//        map.put("rows", pageList.getList());
//        executePageMap(map, page);
//
        return map;
    }

    /**
     * 导出Excel
     * @param  request
     * @param  response
     */
    @RequestMapping(value = "/toExcel.do")
    @ResponseBody
    public void materialExcel(HttpServletRequest request, HttpServletResponse response) {
        try {
            String sheetName = "采购退货出库" + "(" + DateUtils.getDay() + ")";
            response.setHeader("Content-Type", "application/force-download");
            response.setHeader("Content-Type", "application/vnd.ms-excel");
            response.setCharacterEncoding("UTF-8");
            response.setHeader("Expires", "0");
            response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
            response.setHeader("Pragma", "public");
            response.setHeader("Content-disposition", "attachment;filename=" + new String(sheetName.getBytes("gbk"),
                    "ISO8859-1") + ".xls");

            Map<String, String> mapFields = new LinkedHashMap<>();
            mapFields.put("qaCode", "质保单号");
            mapFields.put("materialCode", "物料编号");
            mapFields.put("materialName", "物料名称");
            mapFields.put("salesname", "销售经理");
            mapFields.put("outstoragetime", "出库时间");
            mapFields.put("drumType", "盘类型");
            mapFields.put("outerDiameter", "盘大小");
            mapFields.put("meter", "米数");
            mapFields.put("rfid", "RFID编码");
            mapFields.put("warehouseName", "仓库");
            mapFields.put("shelfCode", "库位");

//            DeriveExcel.exportExcel(sheetName, otherOutStorageService.getExcelList(request.getParameter("ids"), 2), mapFields, response, "");
            logService.logInsert("采购退货出库", LogActionConstant.USER_EXPORT, request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
