package com.tbl.modules.wms.controller.stock;

import com.alibaba.fastjson.JSON;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.platform.constant.MenuConstant;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.service.system.RoleService;
import com.tbl.modules.wms.service.outstorage.AllOutStorageService;
import com.tbl.modules.wms.service.storageinfo.StorageInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

/**
*  库存交易控制层
 *  @author 70486
*/
@Controller
@RequestMapping("/storageInfoSearch")
public class StorageInfoSearchController extends AbstractController {

    /**
     * 出库汇总
     */
	@Autowired
	private AllOutStorageService allOutStorageService;
    /**
     * 库存
     */
    @Autowired
    private StorageInfoService storageInfoService;
    /**
     * 角色
     */
    @Autowired
    private RoleService roleService;

    /**
     * 打开库存交易查询页面
     * @return ModelAndView
     */
    @RequestMapping("/toList")
    public ModelAndView toList() {
        ModelAndView mv = this.getModelAndView();
        mv.addObject("operationCode", roleService.selectOperationByRoleId(getSessionUser().getRoleId(), MenuConstant.storageInfoSearch));
        mv.setViewName("techbloom/wms/instorage/storageinfosearch");
        return mv;
    }

    /**
     * 库存交易查询数据列表
     * @param queryJsonString 查询条件
     * @return Map<String , Object>
     */
    @RequestMapping(value = "/listData")
    @ResponseBody
    public Map<String, Object> listData(String queryJsonString) {
        Map<String, Object> map = new HashMap<>(3);
        if (!StringUtils.isEmptyString(queryJsonString)){
            map = JSON.parseObject(queryJsonString);
        }
        PageTbl page = this.getPage();
        PageUtils utils = allOutStorageService.getList(page, map);
        page.setTotalRows(utils.getTotalCount() == 0 ? 1 : utils.getTotalCount());
        //初始化分页对象
        executePageMap(map, page);
        map.put("rows", utils.getList());
        map.put("total", utils.getTotalPage() == 0 ? 1 : utils.getTotalPage());
        return map;
    }

    /**
     * 导出Excel
     */
    @RequestMapping(value = "/toExcel2")
    @ResponseBody
    public void toExcel2() {
        storageInfoService.toExcel2(response, "", storageInfoService.getAllLists2(request.getParameter("idExcels"),request.getParameter("qaCode")));
    }


}
