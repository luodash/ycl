package com.tbl.modules.wms.service.Impl.inventory;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.Query;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.wms.dao.inventory.InventoryDAO;
import com.tbl.modules.wms.dao.inventory.InventoryRegistrationDAO;
import com.tbl.modules.wms.entity.inventory.Inventory;
import com.tbl.modules.wms.entity.inventory.InventoryRegistration;
import com.tbl.modules.wms.entity.storageinfo.StorageInfo;
import com.tbl.modules.wms.service.inventory.InventoryRegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * 盘点登记实现类
 * @author 70486
 */
@Service("inventoryRegistrationService")
public class InventoryRegistrationServiceImpl extends ServiceImpl<InventoryRegistrationDAO, InventoryRegistration> implements InventoryRegistrationService {

	/**
	 * 盘点登记dao
	 */
	@Autowired
	private InventoryRegistrationDAO inventoryRegistrationDAO;
	/**
	 * 盘点计划dao
	 */
	@Autowired
	private InventoryDAO inventoryDAO;
	
	/**
	 * 查询盘点登记数据列表
	 * @param params 条件
	 * @return PageUtils
	 */
	@Override
	public PageUtils getList(Map<String,Object> params) {
		Page<InventoryRegistration> page = this.selectPage(new Query<InventoryRegistration>(params).getPage(), new EntityWrapper<>());
		return new PageUtils(page.setRecords(inventoryRegistrationDAO.getPageList(page,params)));
	}
	
	/**
	 * 获取盘点登记中的查看结果数据列表
	 * @param pageTbl
	 * @param map
	 * @return PageUtils
	 */
	@Override
	public PageUtils getContrastList(PageTbl pageTbl, Map<String, Object> map) {
		String sortName = pageTbl.getSortname();
		String sortOrder = pageTbl.getSortorder();
		if (StringUtils.isEmptyString(sortName)) {
			sortName = "ys.id";
		}
		if (StringUtils.isEmptyString(sortOrder)) {
			sortOrder = "asc";
		}
		Page page = new Page(pageTbl.getPageno(), pageTbl.getPagesize(), sortName, "asc".equalsIgnoreCase(sortOrder));
		return new PageUtils(page.setRecords(inventoryRegistrationDAO.getContrastList(page, map)));
	}
	
	/**
	 * 获得自增id
	 * @return Long
	 */
	@Override
	public Long getSequence() {
		return inventoryRegistrationDAO.getSequence();
	}
	
	/**
	 * 获取最大盘点编号
	 * @return String
	 */
	@Override
	public String getMaxCode() {
		return inventoryRegistrationDAO.getMaxCode();
	}

	/**
	 * 更新盘点计划状态为已审核/审核中
	 * @param planid 任务ID
	 * @param state 盘点计划状态
	 */
	@Override
	public void updateExamStatus(Integer planid,Long state){
		inventoryRegistrationDAO.updateExamStatus(planid,state);
	}
	
	/**
	 * 查询盘点单下的明细的所有状态（是否已经全部审核成功）
	 * @param planid 任务ID
	 * @return String 状态
	 */
	@Override
	public String selectExamineArray(Integer planid) {
		return inventoryRegistrationDAO.selectExamineArray(planid);
	}

	/**
	 * 查询明细导出列（包含父表数据）
	 * @param ids 库存主键
	 * @param infoid 盘点计划主键
	 * @return StorageInfo
	 */
	@Override
	public List<StorageInfo> selectInventoryDifference(List<Long> ids, String infoid){
		StringBuffer stringBuffer = new StringBuffer();
		inventoryDAO.selectList(new EntityWrapper<Inventory>().setSqlSelect("FACTORYCODE").in("ID", infoid.split(","))).forEach(inventory ->
				stringBuffer.append(inventory.getFactorycode()).append(","));
		return inventoryRegistrationDAO.selectInventoryDifference(ids, Arrays.asList(stringBuffer.toString().split(",")));
	}

	/**
	 * 找出这个集结号下的所有上传米数盘点的数据中执行成功的,来对盘点登记表进行更新
	 * @param specialSign	集结号
	 * @param failQacodes	失败的质保号
	 */
	@Override
	public void updateInventoryMeter(Long specialSign, List<String> failQacodes){
		inventoryRegistrationDAO.updateInventoryMeter(specialSign, failQacodes);
	}

	/**
	 * 批量更新盘点表库位
	 * @param lstStorageInfo
	 */
	@Override
	public void updateShelfByInventory(List<StorageInfo> lstStorageInfo){
		inventoryRegistrationDAO.updateShelfByInventory(lstStorageInfo);
	}

	/**
	 * 更新盘点库位审核状态
	 * @param qaCode
	 * @param shelfState
	 * @param lstInventory
	 */
	@Override
	public void updateShelfState(String qaCode, Integer shelfState, List<Inventory> lstInventory){
		inventoryRegistrationDAO.updateShelfState(qaCode, shelfState, lstInventory);
	}
}
