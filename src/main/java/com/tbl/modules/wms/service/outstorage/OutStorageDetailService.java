package com.tbl.modules.wms.service.outstorage;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.wms.entity.outstorage.OutStorageDetail;
import org.apache.ibatis.annotations.Param;

import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 出库详情
 * @author 70486
 */
public interface OutStorageDetailService extends IService<OutStorageDetail> {
	
	/**
	 * 查询数据列表
     * @param pageTbl
	 * @param params
	 * @return PageUtils
	 */
	PageUtils getList(PageTbl pageTbl,Map<String, Object> params);
	
    /**
     * 获取导出列
     * @param map
     * @return List<OutStorageDetail>
     * */
    List<OutStorageDetail> getAllLists(Map<String,Object> map);

    /**
     * 出库汇总表导出Excel
     * @param response
     * @param path
     * @param list
     * @param excelIndexArray
     * */
    void toExcel(HttpServletResponse response, String path, List<OutStorageDetail> list, String [] excelIndexArray);

    /**
     * 出库台账表导出Excel
     * @param response
     * @param path
     * @param list
     * @param excelIndexArray
     * */
    void toOutStorageExcel(HttpServletResponse response, String path, List<OutStorageDetail> list, String [] excelIndexArray);
    
    /**
     * 查询人员正常出库量
     * @param date
     * @return List<OutStorageDetail>
     */
    List<OutStorageDetail> findList(Date date);

    /**
     * 根据发货明细主键查询明细信息以及对应发货单号
     * @param ids 提货单明细主键
     * @return List<OutStorageDetail>
     */
    List<OutStorageDetail> getOutStorageDetailAndShipNoListByOutstorageDetailId(List<Long> ids);

    /**
     * 根据发货明细主键获取要删除的删除离线接口主键
     * @param ids 提货单明细主键
     * @return List<Long> 离线接口主键
     */
    List<Long> getCancelInterfaceDoIds(List<Long> ids);

    /**
     * 批量更新出库状态
     * @param lstIds
     * @param state
     */
    void updateDetailBatches(List<Long> lstIds, Long state);

    /**
     * 批量修改出库详情
     * @param lstOutStorageDetail
     */
    void updateOutstorageDetailBatches(@Param("lstOutStorageDetail") List<OutStorageDetail> lstOutStorageDetail);

    /**
     * 发货单明细列表数据
     * @param pageTbl
     * @param map
     * @return PageUtils
     */
    PageUtils getDetailPageList(PageTbl pageTbl, Map<String, Object> map);
}
