package com.tbl.modules.wms.service.Impl.pda;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.modules.wms.dao.pda.SplitInstorageDAO;
import com.tbl.modules.wms.entity.split.SplitInstorage;
import com.tbl.modules.wms.service.pda.SplitInstorageService;
import org.springframework.stereotype.Service;

/**
 * 拆分接实现
 * @author 70486
 */
@Service
public class SplitInstorageServiceImpl extends ServiceImpl<SplitInstorageDAO, SplitInstorage> implements SplitInstorageService {


}
