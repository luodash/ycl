package com.tbl.modules.wms.service.Impl.outstorage;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.modules.wms.dao.outstorage.ShipLoadingDetailDAO;
import com.tbl.modules.wms.entity.outstorage.ShipLoadingDetail;
import com.tbl.modules.wms.service.outstorage.ShipLoadingDetailService;
import org.springframework.stereotype.Service;

/**
 * 装车发运明细
 * @author 70486
 */
@Service
public class ShipLoadingDetailServiceImpl extends ServiceImpl<ShipLoadingDetailDAO, ShipLoadingDetail> implements ShipLoadingDetailService {
	
}
