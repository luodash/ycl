package com.tbl.modules.wms.service.Impl.system;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.Query;
import com.tbl.modules.wms.dao.printer.PrinterDAO;
import com.tbl.modules.wms.entity.system.Printer;
import com.tbl.modules.wms.service.system.PrinterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 打印机配置实现类
 * @author 70486
 */
@Service("PrinterService")
public class PrinterServiceImpl extends ServiceImpl<PrinterDAO, Printer> implements PrinterService {

    /**
     * 打印配置持久层
     */
    @Autowired
    private PrinterDAO printerDAO;

    /**
     * 获取打印配置列表的数据
     * @param  map
     * @return PageUtils
     */
    @Override
    public PageUtils queryPage(Map<String,Object> map){
        Page<Printer> page = this.selectPage(new Query<Printer>(map).getPage(), new EntityWrapper<>());
        return new PageUtils(page.setRecords(printerDAO.selectPageList(map)));
    }

    /**
     * 根据id查询
     * @param  id
     * @return Printer
     */
    @Override
    public Printer findById(Long id){
        return printerDAO.findById(id);
    }

    /**
     * 根据用户ID获取打印机配置
     * @param userId
     * @return Printer
     */
    @Override
    public Printer findPrinterCodeByUserId(Long userId){
        return printerDAO.findPrinterCodeByUserId(userId);
    }
}
