package com.tbl.modules.wms.service.baseinfo;

import java.util.Map;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.wms.entity.baseinfo.Externalcable;

/**
 * 同步外协采购的电缆数据服务
 * @author 70486
 */
public interface ExternalcableService extends IService<Externalcable> {
    /**
     * 获取质保单的下拉选信息
     * @param pageTbl
     * @param map
     * @return PageUtils
     */
    PageUtils getPageQacodeList(PageTbl pageTbl, Map<String, Object> map);
}
