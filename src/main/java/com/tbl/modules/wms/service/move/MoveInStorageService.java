package com.tbl.modules.wms.service.move;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.modules.wms.entity.move.MoveInStorage;

/**
 * 移库入库服务类
 * @author 70486
 */
public interface MoveInStorageService extends IService<MoveInStorage> {
}
