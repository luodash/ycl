package com.tbl.modules.wms.service.jobs;

import com.baomidou.mybatisplus.service.IService;
import org.apache.poi.ss.formula.functions.T;

/**
 * 定时任务服务类
 * @author 70486
 */
public interface JobsService extends IService<T> {

}
