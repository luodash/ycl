package com.tbl.modules.wms.service.move;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.modules.wms.entity.move.MoveOutStorage;

/**
 * 移库出库服务类
 * @author 70486
 */
public interface MoveOutStorageService extends IService<MoveOutStorage> {
}
