package com.tbl.modules.wms.service.interfacelog;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.wms.entity.interfacelog.InterfaceLog;

import java.util.Map;

/**
 * 接口日志记录服务
 * @author 70486
 **/
public interface InterfaceLogService extends IService<InterfaceLog> {
	
	/**
	 * 插入日志
	 * @param code 接口编码
	 * @param name 接口名称
	 * @param requestString 请求报文
	 * @param responseString 返回报文
	 * @param qacode 质保号
	 * @param batchNo 批次号
	 */
	void insertLog(String code,String name,String requestString,String responseString,String qacode,String batchNo);

	/**
	 * 获取接口日志数据
	 * @param map 参数条件
	 * @return PageUtils
	 */
	PageUtils queryPage(Map<String,Object> map);

}
