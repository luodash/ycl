package com.tbl.modules.wms.service.Impl.allo;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.modules.wms.dao.allo.AlloInStorageDAO;
import com.tbl.modules.wms.entity.allo.AlloInStorage;
import com.tbl.modules.wms.service.allo.AlloInStorageService;
import org.springframework.stereotype.Service;

/**
 * 调拨管理-调入仓库
 * @author 70486
 */
@Service
public class AlloInStorageServiceImpl extends ServiceImpl<AlloInStorageDAO, AlloInStorage> implements AlloInStorageService {
	
}
