package com.tbl.modules.wms.service.baseinfo;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.wms.entity.baseinfo.Area;

import java.util.List;
import java.util.Map;

/**
 * 库区列表服务类
 * @author zxf
 */
public interface AreaService extends IService<Area> {
    /**
     *:列表页
     * @param pageTbl
     * @param map
     * @return PageUtils
     */
    PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map);

    /**
     * 获取库区列表
     * @param map
     * @return List<Map<String,Object>>
     */
    List<Map<String, Object>> getAreaList(Map<String, Object> map);

    /**
     * 获取导出库区列表
     * @param ids 选中的库区的主键
     * @param code 库区编码
     * @param name 库区名称
     * @return List<Area>
     */
    List<Area> getExcelList(String ids, String code, String name);
}
