package com.tbl.modules.wms.service.pad;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.modules.wms.constant.PdaResult;
import org.apache.poi.ss.formula.functions.T;

/**
 * Pad接口
 * @author 70486
 */
public interface PadService extends IService<T> {

	/**
     * 用户登陆
     * @param username
     * @param password
     * @return PdaResult
     */
    PdaResult login(String username, String password);

    /**
     * 获取详情列表
     * @param userId
     * @param carCode
     * @return PdaResult 详情表列表 状态为已开始,未完成的
     */
    PdaResult storageInList(Long userId,String carCode);

    /**
     * 入库完成
     * @param rfid
     * @param shelfCode 库位编码
     * @param userId 用户id
     * @param date
     * @return PdaResult
     */
    PdaResult storageInSuccess(String rfid, String shelfCode, Long userId, String date);
    
    /**
     * 入库:选择可用行车
     * @param warehouseCode 仓库编码
     * @param factoryCode 厂区编码
     * @return PdaResult 行车列表
     */
    PdaResult storageinStorageinCar(String warehouseCode, String factoryCode);

    /**
     * 获取详情列表
     * @param userId
     * @param carCode
     * @return PdaResult 详情表列表 状态为已开始,未完成的
     */
    PdaResult storageOutList(Long userId,String carCode);

    /**
     *  出库完成
     * @param rfid
     * @param meter 米数
     * @param userId 用户id
     * @return PdaResult
     */
    PdaResult storageOutSuccess(String rfid, String meter, Long userId);
    
    /**
     * 获取详情列表
     * @param userId
     * @param carCode
     * @return PdaResult 详情表列表 状态为已开始,未完成的
     */
    PdaResult alloStorageOutList(Long userId,String carCode);

    /**
     * 出库完成
     * @param rfid
     * @param userId 用户id
     * @return PdaResult
     */
    PdaResult alloStorageOutSuccess(String rfid, Long userId);

    /**
     * 获取详情列表
     * @param userId
     * @param carCode
     * @return PdaResult 详情表列表 状态为已开始,未完成的
     */
    PdaResult alloStorageInList(Long userId,String carCode);

    /**
     * 入库完成
     * @param rfid
     * @param userId 用户id
     * @param shelfCode 库位编码
     * @return PdaResult
     */
    PdaResult alloStorageInSuccess(String rfid, Long userId,String shelfCode);
    
    /**
     * 获取详情列表
     * @param userId
     * @param carCode
     * @return PdaResult 详情表列表 状态为已开始,未完成的
     */
    PdaResult moveStorageOutList(Long userId,String carCode);

    /**
     * 出库完成
     * @param rfid
     * @param userId 用户id
     * @return PdaResult
     */
    PdaResult moveStorageOutSuccess(String rfid, Long userId);

    /**
     * 获取详情列表
     * @param userId
     * @param carCode
     * @return PdaResult 详情表列表 状态为已开始,未完成的
     */
    PdaResult moveStorageInList(Long userId,String carCode);

    /**
     * 入库完成
     * @param rfid
     * @param userId 用户id
     * @param shelfId 库位编码
     * @return PdaResult
     */
    PdaResult moveStorageInSuccess(String rfid, Long userId,String shelfId);
    
    /**
     * 获取状态为“已开始”的拆分入库物料
     * @param carCode 行车编码
     * @return PdaResult
     */
    PdaResult getSplitInstorageList(String carCode);
    
    /**
     * 拆分完成入库
     * @param rfid
     * @param shelfId
     * @param userId
     * @return PdaResult
     */
    PdaResult endInstorage(String rfid, String shelfId,String userId);

    /**
     * 根据用户获取厂区
     * @param userId
     * @return List<FactoryArea>
     */
    PdaResult getFactorys(String userId);
}
