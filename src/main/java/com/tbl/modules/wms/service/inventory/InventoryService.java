package com.tbl.modules.wms.service.inventory;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.wms.entity.inventory.Inventory;
import com.tbl.modules.wms.entity.storageinfo.StorageInfo;

/**
 * 盘点计划服务类
 * @author 70486
 */
public interface InventoryService extends IService<Inventory> {
	
	/**
	 * 查询盘点计划列表
	 * @param page
	 * @param params
	 * @return PageUtils
	 */
	PageUtils getInventoryPlanList(PageTbl page, Map<String, Object> params);

	/**
	 * 查询盘点审核列表
	 * @param page
	 * @param params
	 * @return PageUtils
	 */
	PageUtils getInventoryReviewList(PageTbl page, Map<String, Object> params);

	/**
	 * 获得自增id
	 * @return Long
	 */
	Long getSequence();
	
	/**
	 * 获取最大盘点编号
	 * @return int
	 */
	int getMaxCode();
	
	/**
     * 获取盘点审核导出列
     * @param ebsCode
     * @param userId
     * @return List<Inventory>
     * */
    List<StorageInfo> getAllLists(String ebsCode, Long userId);
    
	/**
     * 导出excel
     * @param response
	 * @param ebsCode
	 * @param list
     */
    void toExcel(HttpServletResponse response, String ebsCode, List<StorageInfo> list);

    /**
     * 获取厂区列表
     * @param list
     * @return List<Map<String, Object>>
     */
    List<Map<String,Object>> getFactoryList(List<String> list);

	/**
	 * 确认审核日期
	 * @param ebsCode
	 * @param date
	 */
	void confirmAuditTime(String ebsCode, Date date);
}
