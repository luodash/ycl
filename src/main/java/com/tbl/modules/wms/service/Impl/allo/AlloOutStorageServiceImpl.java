package com.tbl.modules.wms.service.Impl.allo;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.modules.wms.dao.allo.AlloOutStorageDAO;
import com.tbl.modules.wms.entity.allo.AlloOutStorage;
import com.tbl.modules.wms.service.allo.AlloOutStorageService;
import org.springframework.stereotype.Service;

/**
 * 调拨管理-调出仓库
 * @author 70486
 */
@Service
public class AlloOutStorageServiceImpl extends ServiceImpl<AlloOutStorageDAO, AlloOutStorage> implements AlloOutStorageService {
	
}
