package com.tbl.modules.wms.service.pda;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.modules.wms.entity.split.SplitInstorage;

/**
 * 拆分库存接口
 * @author 70486
 */
public interface SplitInstorageService extends IService<SplitInstorage> {



}
