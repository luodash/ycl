package com.tbl.modules.wms.service.Impl.outstorage;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.wms.dao.outstorage.OutStorageDetailDAO;
import com.tbl.modules.wms.dao.outstorage.OutstorageDAO;
import com.tbl.modules.wms.dao.outstorage.ShipLoadingDAO;
import com.tbl.modules.wms.dao.outstorage.ShipLoadingDetailDAO;
import com.tbl.modules.wms.entity.outstorage.OutStorageDetail;
import com.tbl.modules.wms.entity.outstorage.ShipLoading;
import com.tbl.modules.wms.entity.outstorage.ShipLoadingDetail;
import com.tbl.modules.wms.service.outstorage.ShipLoadingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 出库管理-装车发运
 * @author 70486
 */
@Service
public class ShipLoadingServiceImpl extends ServiceImpl<ShipLoadingDAO, ShipLoading> implements ShipLoadingService {

    /**
     * 出库管理-装车发运
     */
    @Autowired
    private ShipLoadingDAO shipLoadingDAO;
    /**
     * 出库管理-装车发运明细
     */
    @Autowired
    private ShipLoadingDetailDAO detailDAO;
    /**
     * 出库详情
     */
    @Autowired
    private OutStorageDetailDAO outStorageDetailDAO;
    /**
     * 出库信息
     */
    @Autowired
    private OutstorageDAO outstorageDAO;

    /**
     * 装车发运列表页数据
     * @param pageTbl
     * @param map
     * @return PageUtils
     */
    @Override
    public PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map) {
        String sortName = pageTbl.getSortname();
        String sortOrder = pageTbl.getSortorder();
        boolean order = false;
        if (StringUtils.isEmptyString(sortName)) {
            sortName = "id";
        }
        if (StringUtils.isEmptyString(sortOrder)) {
            sortOrder = "desc";
        }
        if ("asc".equalsIgnoreCase(sortOrder)) {
            order = true;
        }
        if (map.get("shipLoadTime") != null && !"".equals(map.get("shipLoadTime"))) {
            map.put("startTime", map.get("ShipLoadTime") + " 00:00:00");
            map.put("endTime", map.get("ShipLoadTime") + " 23:59:59");
        }
        if (map.get("createTime") != null && !"".equals(map.get("createTime"))) {
            map.put("startTime1", map.get("createTime") + " 00:00:00");
        }
        if (map.get("endTime") != null && !"".equals(map.get("endTime"))) {
            map.put("endTime1", map.get("endTime") + " 23:59:59");
        }
        Page page = new Page(pageTbl.getPageno(), pageTbl.getPagesize(), sortName, order);
        return new PageUtils(page.setRecords(shipLoadingDAO.getPageList(page, map)));
    }

    /**
     * 详情的列表
     * @param pageTbl
     * @param id 装车发运表主键
     * @return PageUtils
     */
    @Override
    public PageUtils getPageDetailList(PageTbl pageTbl, Integer id) {
        String sortName = pageTbl.getSortname();
        String sortOrder = pageTbl.getSortorder();
        if (StringUtils.isEmptyString(sortName) || "cz".equalsIgnoreCase(sortName)) {
            sortName = "id";
        }
        if (StringUtils.isEmptyString(sortOrder)) {
            sortOrder = "desc";
        }
        Page page = new Page(pageTbl.getPageno(), pageTbl.getPagesize(), sortName, "asc".equalsIgnoreCase(sortOrder));
        return new PageUtils(page.setRecords(shipLoadingDAO.getDetailList(page, id)));
    }

    /**
     * 导出excel列表
     * @param map 查询条件
     * @return List<ShipLoading>
     */
    @Override
    public List<ShipLoading> getExcelList(Map<String, Object> map) {
        if (map.get("shipLoadTime") != null && !"".equals(map.get("shipLoadTime"))) {
            map.put("startTime", map.get("ShipLoadTime") + " 00:00:00");
            map.put("endTime", map.get("ShipLoadTime") + " 23:59:59");
        }
        return shipLoadingDAO.getExcelList(map);
    }

    /**
     * 点击保存,保存主表信息
     * @param shipLoading 装车发运表
     * @return Long 保存成功后的新增的主键
     */
    @Override
    public Long saveShipLoading(ShipLoading shipLoading) {
        this.insertOrUpdate(shipLoading);
        return shipLoading.getId();
    }

    /**
     * 点击编辑,提交信息
     * @param shipLoading 装车发运表
     * @return boolean
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean subShiploading(ShipLoading shipLoading) {
        shipLoading.setState(2);
        shipLoading.setShipLoadTime(new Date());
        return this.insertOrUpdate(shipLoading);
    }

    /**
     * 编辑页面  获取质保单号列表
     * @param pageTbl
     * @param map
     * @return PageUtils
     */
    @Override
    public PageUtils getQaCode(PageTbl pageTbl, Map<String, Object> map) {
        Page<Map<String, Object>> page = new Page<>(pageTbl.getPageno(), pageTbl.getPagesize());
        return new PageUtils(page.setRecords(shipLoadingDAO.getQaCode(page, map)));
    }

    /**
     * 装车发运详情添加
     * @param id 装车发运主表主键
     * @param qaCodeIds 出库详情主键
     * @return boolean
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean saveShipLoadingDetail(Long id, String[] qaCodeIds) {
        List<Long> idList = Arrays.stream(qaCodeIds).map(Long::parseLong).distinct().collect(Collectors.toList());
        idList.forEach(a -> {
            OutStorageDetail outStorageDetail = outStorageDetailDAO.selectById(a);
            ShipLoadingDetail shipLoadingDetail = new ShipLoadingDetail();
            shipLoadingDetail.setMaterialCode(outStorageDetail.getMaterialCode());
            shipLoadingDetail.setQaCode(outStorageDetail.getQaCode());
            shipLoadingDetail.setMeter(outStorageDetail.getMeter());
            shipLoadingDetail.setShipNo(outstorageDAO.selectById(outStorageDetail.getPId()).getShipNo());
            shipLoadingDetail.setPId(id);
            detailDAO.insert(shipLoadingDetail);
        });
        return true;
    }

    /**
     * 删除明细数据
     * @param ids 装车发运明细主键列表
     * @return boolean
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean deleteShipLoadingDetailIds(String[] ids) {
        return detailDAO.deleteBatchIds(Arrays.stream(ids).map(s -> Long.parseLong(s.trim())).collect(Collectors.toList())) > 0;
    }

    /**
     * 删除info,detail数据
     * @param ids 装车发运主表主键
     * @return boolean
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean deleteByIds(String[] ids) {
        List<Long> infoIdList = StringUtils.toLongList(ids);
        if (infoIdList!=null) {
            infoIdList.forEach(a -> {
                List<ShipLoadingDetail> lstShipLoadingDetail = detailDAO.selectByMap(new HashMap<String, Object>() {{
                    put("PID", a);
                }});
                List<Long> detailIds = lstShipLoadingDetail.stream().map(ShipLoadingDetail::getId).collect(Collectors.toList());
                if (detailIds.size() > 0) {
                    detailDAO.deleteBatchIds(detailIds);
                }
            });
        }
        return shipLoadingDAO.deleteBatchIds(infoIdList) > 0;
    }
}
