package com.tbl.modules.wms.service.Impl.baseinfo;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.wms.dao.baseinfo.AreaDAO;
import com.tbl.modules.wms.entity.baseinfo.Area;
import com.tbl.modules.wms.service.baseinfo.AreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 库区列表
 * @author zxf
 */
@Service
public class AreaServiceImpl extends ServiceImpl<AreaDAO, Area> implements AreaService {
    /**
     * 库区信息
     */
    @Autowired
    private AreaDAO areaDAO;

    /**
     * 查询列表信息
     * @param pageTbl
     * @param map 条件
     * @return PageUtils
     */
    @Override
    public PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map) {
        String sortName = pageTbl.getSortname();
        String sortOrder = pageTbl.getSortorder();
        if (StringUtils.isEmptyString(sortName)) {
            sortName = "id";
        }
        if (StringUtils.isEmptyString(sortOrder)) {
            sortOrder = "desc";
        }
        Page page = new Page(pageTbl.getPageno(), pageTbl.getPagesize(), sortName, "asc".equalsIgnoreCase(sortOrder));
        return new PageUtils(page.setRecords(areaDAO.getPageList(page, map)));
    }

    /**
     * 获取库区全部下拉列表
     * @param map
     * @return List<Map<String,Object>>
     */
    @Override
    public List<Map<String, Object>> getAreaList(Map<String, Object> map) {
        return areaDAO.selectAreaList(map);
    }

    /**
     * 获取导出库区列表
     * @param ids 选中的库区的主键
     * @param code 库区编码
     * @param name 库区名称
     * @return List<Area>
     */
    @Override
    public List<Area> getExcelList(String ids, String code, String name) {
        Map<String, Object> map = new HashMap<>(3);
        map.put("ids", StringUtils.stringToInt(ids));
        map.put("code", code);
        map.put("name", name);
        return areaDAO.getExcelList(map);
    }
}
