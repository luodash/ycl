package com.tbl.modules.wms.service.outstorage;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.wms.entity.outstorage.ExchangePrint;
import com.tbl.modules.wms.entity.printinfo.PrintInfo;
import java.util.List;
import java.util.Map;

/**
 * 打印流转单服务类
 * @author 70486
 */
public interface ExchangeService extends IService<PrintInfo> {

	/**
	 * 获取流转单列表
	 * @param pageTbl
	 * @param map 条件
	 * @return PageUtils
	 */
    PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map);

    /**
     * 根据出库单获取对应的车牌信息
     * @param outno 发货单号
     * @param sTime 起始时间
     * @param eTime 结束时间
     * @return List<Map<String,String>>
     */
    List<Map<String,String>> getCarList(String outno, String sTime, String eTime);

    /**
     * 根据车牌及发货单判断是否已经存在
     * @param exchangePrint 流转单
     * @return boolean
     */
    boolean isExistInfo(ExchangePrint exchangePrint);

    /**
     * 根据车牌及发货单验证是否存在该类数据
     * @param outno
     * @param carno
     * @return boolean
     */
    boolean validateTrue(String outno,String carno);

    /**
     * 获取厂区信息列表
     * @return List<Map<String, Object>>
     */
    List<Map<String,Object>> getFactoryArea();

    /**
     * 获取打印信息列表
     * @param map
     * @return Map<String,Object>
     */
    List<Map<String,Object>> getPrintInfo(Map<String,Object> map);

    /**
     * 更新流转单号，返回生成的流转单号
     * @param outno
     * @param carno
     * @return String
     */
    String updateOddNumber(String outno, String carno);
}
