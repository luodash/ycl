package com.tbl.modules.wms.service.Impl.baseinfo;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.wms.dao.baseinfo.ExternalcableDAO;
import com.tbl.modules.wms.entity.baseinfo.Externalcable;
import com.tbl.modules.wms.service.baseinfo.ExternalcableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 同步外协采购的电缆数据实现类
 * @author 70486
 */
@Service
public class ExternalcableServiceImpl extends ServiceImpl<ExternalcableDAO, Externalcable> implements ExternalcableService {

    /**
     * 同步外协采购的电缆数据
     */
	@Autowired
	private ExternalcableDAO externalcableDAO;
	
	/**
	 * 获取质保单电缆信息下拉列表
	 * @param pageTbl
	 * @param map 条件
	 * @return PageUtils
	 */
   @Override
    public PageUtils getPageQacodeList(PageTbl pageTbl, Map<String, Object> map) {
        String sortName = pageTbl.getSortname();
        String sortOrder = pageTbl.getSortorder();
        if (StringUtils.isEmptyString(sortName)) {
            sortName = "id";
        }
        if (StringUtils.isEmptyString(sortOrder)) {
            sortOrder = "desc";
        }
        Page page = new Page(pageTbl.getPageno(), pageTbl.getPagesize(), sortName, "asc".equalsIgnoreCase(sortOrder));
        return new PageUtils(page.setRecords(externalcableDAO.getPageQacodeList(page, map)));
    }
}
