package com.tbl.modules.wms.service.Impl.webservice;

import com.tbl.modules.wms.constant.XmlToolsUtil;
import com.tbl.modules.wms.service.webservice.CxfService;
import com.tbl.modules.wms.service.webservice.server.FewmService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.util.Map;

/**
 * Created by nbfujx on 2017-10-26.
 * @author 70486
 */
@WebService(serviceName = "cxfService", targetNamespace = "http://CxfService.webservice.service.wms.modules.tbl.com",
        endpointInterface = "com.tbl.modules.wms.service.webservice.CxfService")
@SOAPBinding(style = SOAPBinding.Style.RPC)
@Component
public class CxfServiceImpl implements CxfService {
	private final Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * wms是服务方
     */
    @Autowired
    private FewmService fewmService;

    @Override
    public String invoice(String interfaceCode, String para) {

        Map<String, Object> responseData = null;
        String responsexml = "";
        //判断接口类型，跳转到不同的方法
        //WMS接口-销售退货数据同步
        if ("FEWMS003".equalsIgnoreCase(interfaceCode)) {
            logger.info("开始执行WMS接口-销售退货数据同步......");
            responseData = fewmService.FEWMS003(para);
        } else if ("FEWMS004".equalsIgnoreCase(interfaceCode)) {
            //WMS接口-采购退货
            logger.info("开始执行WMS接口-采购退货......");
            responseData = fewmService.FEWMS004(para);
        } else if ("FEWMS005".equalsIgnoreCase(interfaceCode)) {
            //WMS接口-保留接口
            logger.info("开始执行WMS接口-保留接口......");
            responseData = fewmService.FEWMS005(para);
        } else if ("FEWMS006".equalsIgnoreCase(interfaceCode)) {
            //WMS接口-发货单接口
            logger.info("开始执行WMS接口-发货单接口......");
            responseData = fewmService.FEWMS006(para);
        } else if ("FEWMS010".equalsIgnoreCase(interfaceCode)) {
            //WMS接口-报废接口
            logger.info("开始执行WMS接口-报废接口......");
            responseData = fewmService.FEWMS010(para);
        } else if ("FEWMS017".equalsIgnoreCase(interfaceCode)) {
            //标签信息初始化
            logger.info("开始执行WMS接口-标签信息初始化......");
            responseData = fewmService.FEWMS017(para);
            responsexml = XmlToolsUtil.getResponseXml(responseData);
            return responsexml;
        } else if ("FEWMS021".equalsIgnoreCase(interfaceCode)) {
            //外协采购数据同步
            logger.info("开始执行WMS接口-外采数据同步......");
            responseData = fewmService.FEWMS021(para);
        } else if ("FEWMS203".equalsIgnoreCase(interfaceCode)) {
            //WMS二期接口-采购订单数据同步
            logger.info("开始执行WMS接口-接收单查询同步......");
            responsexml = fewmService.FEWMS203(para);
            return responsexml;
        } else if ("FEWMS204".equalsIgnoreCase(interfaceCode)) {
            //WMS二期接口-采购订单数据同步
            logger.info("开始执行WMS接口-紧急物料审批结果FromOA同步......");
            responseData = fewmService.FEWMS204(para);
        }


        if (responseData!=null) {
            if (responseData.get("code") != null && Long.parseLong(responseData.get("code").toString()) == 0) {
                responsexml = "<data><code>0</code><msg>成功</msg></data>";
            } else {
                responsexml = XmlToolsUtil.getResponseXml(responseData);
            }
        }

        return responsexml;
    }


}
