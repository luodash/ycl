package com.tbl.modules.wms.service.Impl.inventory;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.modules.wms.dao.inventory.InventoryOutDAO;
import com.tbl.modules.wms.entity.inventory.InventoryOut;
import com.tbl.modules.wms.service.inventory.InventoryOutService;

/**
 * 盘点计划实现类
 * @author 70486
 */
@Service("inventoryOutService")
public class InventoryOutServiceImpl extends ServiceImpl<InventoryOutDAO, InventoryOut> implements InventoryOutService {
	
}
