package com.tbl.modules.wms.service.Impl.outstorage;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.common.utils.DateUtils;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.platform.util.DeriveExcel;
import com.tbl.modules.wms.dao.outstorage.OutStorageDetailDAO;
import com.tbl.modules.wms.entity.outstorage.OutStorageDetail;
import com.tbl.modules.wms.service.outstorage.OutStorageDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 普通出库信息
 * @author 70486
 */
@Service("outStorageDetailService")
public class OutStorageDetailServiceImpl extends ServiceImpl<OutStorageDetailDAO, OutStorageDetail> implements OutStorageDetailService {

    /**
     * 其他出库信息
     */
	@Autowired
	private OutStorageDetailDAO outStorageDetailDAO;

	/**
	 * 获取出库汇总表信息列表
     * @param pageTbl
	 * @param params
	 * @return PageUtils
	 */
	@Override
    public PageUtils getList(PageTbl pageTbl, Map<String, Object> params) {
        Page page = new Page(pageTbl.getPageno(), pageTbl.getPagesize(), pageTbl.getSortname(), "asc".equalsIgnoreCase(pageTbl.getSortorder()));
        return new PageUtils(page.setRecords(outStorageDetailDAO.selectMixedList(page, params)));
    }
	
	 /**
     * 获取导出列
     * @param map db_ids 调拨出库主键;yk_ids 移库出库主键;pt_ids 普通出库主键
     * @return List<OutStorageDetail>
     */
    @Override
    public List<OutStorageDetail> getAllLists(Map<String,Object> map) {
        return outStorageDetailDAO.getAllLists(map);
    }

    /**
     * 出库汇总表导出Excel
     * @param response 请求
     * @param path 路径
     * @param lstOutStorageDetail 出库单列表
     * @param excelIndexArray
     */
    @Override
    public void toExcel(HttpServletResponse response, String path, List<OutStorageDetail> lstOutStorageDetail,String [] excelIndexArray) {
        try {
            String sheetName = "出库汇总表" + "(" + DateUtils.getDay() + ")";
            if (path != null && !"".equals(path)) {
                sheetName = sheetName + ".xls";
            } else {
                response.setHeader("Content-Type", "application/force-download");
                response.setHeader("Content-Type", "application/vnd.ms-excel");
                response.setCharacterEncoding("UTF-8");
                response.setHeader("Expires", "0");
                response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
                response.setHeader("Pragma", "public");
                response.setHeader("Content-disposition", "attachment;filename=" + new String(sheetName.getBytes("gbk"),
                        "ISO8859-1") + ".xls");
            }
            Map<String, String> mapFields = new LinkedHashMap<>();
            if (excelIndexArray.length==1&&"".equals(excelIndexArray[0])) {
                mapFields.put("qaCode", "质保单号");
                mapFields.put("batchNo", "批次号");
                mapFields.put("shipNo", "单据号");
                mapFields.put("materialCode", "物料编号");
                mapFields.put("materialName", "物料名称");
                mapFields.put("dishcode", "盘号");
                mapFields.put("salesname", "销售经理");
                mapFields.put("drumType", "盘类型");
                mapFields.put("model","盘规格");
                mapFields.put("outerDiameter","盘外径");
                mapFields.put("innerDiameter","盘内径");
                mapFields.put("meter", "米数");
                mapFields.put("warehouseName", "仓库");
                mapFields.put("shelfCode", "库位");
                mapFields.put("startstorageName", "发货人");
                mapFields.put("startStorageTimeStr", "发货时间");
                mapFields.put("pickManName", "出库人");
                mapFields.put("outstoragetimestr", "出库时间");
                mapFields.put("outstorageType1", "类型");
            }else {
                for (String s : excelIndexArray) {
                    if ("2".equals(s)) {
                        mapFields.put("qaCode", "质保单号");
                    } else if ("3".equals(s)) {
                        mapFields.put("batchNo", "批次号");
                    } else if ("4".equals(s)) {
                        mapFields.put("shipNo", "单据号");
                    } else if ("5".equals(s)) {
                        mapFields.put("materialCode", "物料编号");
                    } else if ("6".equals(s)) {
                        mapFields.put("materialName", "物料名称");
                    } else if ("7".equals(s)) {
                        mapFields.put("dishcode", "盘号");
                    } else if ("8".equals(s)) {
                        mapFields.put("salesname", "销售经理");
                    } else if ("9".equals(s)) {
                        mapFields.put("drumType", "盘类型");
                    } else if ("10".equals(s)) {
                        mapFields.put("model", "盘规格");
                    } else if ("11".equals(s)) {
                        mapFields.put("outerDiameter", "盘外径");
                    } else if ("12".equals(s)) {
                        mapFields.put("innerDiameter", "盘内径");
                    } else if ("13".equals(s)) {
                        mapFields.put("meter", "米数");
                    } else if ("14".equals(s)) {
                        mapFields.put("warehouseName", "仓库");
                    } else if ("15".equals(s)) {
                        mapFields.put("shelfCode", "库位");
                    } else if ("16".equals(s)) {
                        mapFields.put("startstorageName", "发货人");
                    } else if ("17".equals(s)) {
                        mapFields.put("startStorageTimeStr", "发货时间");
                    } else if ("18".equals(s)) {
                        mapFields.put("pickManName", "出库人");
                    } else if ("19".equals(s)) {
                        mapFields.put("outstoragetimestr", "出库时间");
                    } else if ("20".equals(s)) {
                        mapFields.put("outstorageType1", "类型");
                    }
                }
            }

            DeriveExcel.exportExcel(sheetName, lstOutStorageDetail, mapFields, response, path);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 出库台账表导出Excel
     * @param response
     * @param path 路径
     * @param lstOutStorageDetail 出库单列表
     * @param excelIndexArray
     */
    @Override
    public void toOutStorageExcel(HttpServletResponse response, String path, List<OutStorageDetail> lstOutStorageDetail,
                                  String [] excelIndexArray) {
        try {
            String sheetName = "出库台账" + "(" + DateUtils.getDay() + ")";
            if (path != null && !"".equals(path)) {
                sheetName = sheetName + ".xls";
            } else {
                response.setHeader("Content-Type", "application/force-download");
                response.setHeader("Content-Type", "application/vnd.ms-excel");
                response.setCharacterEncoding("UTF-8");
                response.setHeader("Expires", "0");
                response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
                response.setHeader("Pragma", "public");
                response.setHeader("Content-disposition", "attachment;filename=" + new String(sheetName.getBytes("gbk"),
                        "ISO8859-1") + ".xls");
            }
            Map<String, String> mapFields = new LinkedHashMap<>();
            if (excelIndexArray.length==1&&"".equals(excelIndexArray[0])) {
                mapFields.put("qaCode", "质保单号");
                mapFields.put("batchNo", "批次号");
                mapFields.put("shipNo", "单据号");
                mapFields.put("materialCode", "物料编号");
                mapFields.put("materialName", "物料名称");
                mapFields.put("dishcode", "盘号");
                mapFields.put("salesname", "销售经理");
                mapFields.put("drumType", "盘类型");
                mapFields.put("model","盘规格");
                mapFields.put("outerDiameter","盘外径");
                mapFields.put("innerDiameter","盘内径");
                mapFields.put("meter", "米数");
                mapFields.put("warehouseName", "仓库");
                mapFields.put("shelfCode", "库位");
                mapFields.put("startstorageName", "发货人");
                mapFields.put("startStorageTimeStr", "发货时间");
                mapFields.put("pickManName", "出库人");
                mapFields.put("outstoragetimestr", "出库时间");
            }else {
                for (String s : excelIndexArray) {
                    if ("2".equals(s)) {
                        mapFields.put("qaCode", "质保单号");
                    } else if ("3".equals(s)) {
                        mapFields.put("batchNo", "批次号");
                    } else if ("4".equals(s)) {
                        mapFields.put("shipNo", "单据号");
                    } else if ("5".equals(s)) {
                        mapFields.put("materialCode", "物料编号");
                    } else if ("6".equals(s)) {
                        mapFields.put("materialName", "物料名称");
                    } else if ("7".equals(s)) {
                        mapFields.put("dishcode", "盘号");
                    } else if ("8".equals(s)) {
                        mapFields.put("salesname", "销售经理");
                    } else if ("9".equals(s)) {
                        mapFields.put("drumType", "盘类型");
                    } else if ("10".equals(s)) {
                        mapFields.put("model", "盘规格");
                    } else if ("11".equals(s)) {
                        mapFields.put("outerDiameter", "盘外径");
                    } else if ("12".equals(s)) {
                        mapFields.put("innerDiameter", "盘内径");
                    } else if ("13".equals(s)) {
                        mapFields.put("meter", "米数");
                    } else if ("14".equals(s)) {
                        mapFields.put("warehouseName", "仓库");
                    } else if ("15".equals(s)) {
                        mapFields.put("shelfCode", "库位");
                    } else if ("16".equals(s)) {
                        mapFields.put("startstorageName", "发货人");
                    } else if ("17".equals(s)) {
                        mapFields.put("startStorageTimeStr", "发货时间");
                    } else if ("18".equals(s)) {
                        mapFields.put("pickManName", "出库人");
                    } else if ("19".equals(s)) {
                        mapFields.put("outstoragetimestr", "出库时间");
                    }
                }
            }

            DeriveExcel.exportExcel(sheetName, lstOutStorageDetail, mapFields, response, path);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * 查询人员正常出库量
     * @param date 日期
     * @return List<OutStorageDetail> 出库明细
     */
    @Override
    public List<OutStorageDetail> findList(Date date){
    	return outStorageDetailDAO.findList(date);
    }

    /**
     * 根据发货明细主键查询明细信息以及对应发货单号
     * @param ids 提货单明细主键
     * @return List<OutStorageDetail>
     */
    @Override
    public List<OutStorageDetail> getOutStorageDetailAndShipNoListByOutstorageDetailId(List<Long> ids){
        return outStorageDetailDAO.getOutStorageDetailAndShipNoListByOutstorageDetailId(ids);
    }

    /**
     * 根据发货明细主键获取要删除的删除离线接口主键
     * @param ids 提货单明细主键
     * @return List<Long> 离线接口主键
     */
    @Override
    public List<Long> getCancelInterfaceDoIds(List<Long> ids){
        return outStorageDetailDAO.getCancelInterfaceDoIds(ids);
    }

    /**
     * 批量更新出库详情
     * @param lstIds
     * @param state
     */
    @Override
    public void updateDetailBatches(List<Long> lstIds, Long state){
        outStorageDetailDAO.updateDetailBatches(lstIds, state);
    }

    /**
     * 批量修改出库详情
     * @param lstOutStorageDetail
     */
    @Override
    public void updateOutstorageDetailBatches(List<OutStorageDetail> lstOutStorageDetail){
        outStorageDetailDAO.updateOutstorageDetailBatches(lstOutStorageDetail);
    }

    /**
     * 获取出库汇总表信息列表
     * @param pageTbl
     * @param params
     * @return PageUtils
     */
    @Override
    public PageUtils getDetailPageList(PageTbl pageTbl, Map<String, Object> params) {
        Page page = new Page(pageTbl.getPageno(), pageTbl.getPagesize(), pageTbl.getSortname(), "asc".equalsIgnoreCase(pageTbl.getSortorder()));
        return new PageUtils(page.setRecords(outStorageDetailDAO.getDetailPageList(page, params)));
    }
}
