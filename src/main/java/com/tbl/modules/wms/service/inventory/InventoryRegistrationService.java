package com.tbl.modules.wms.service.inventory;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.wms.entity.inventory.Inventory;
import com.tbl.modules.wms.entity.inventory.InventoryRegistration;
import com.tbl.modules.wms.entity.storageinfo.StorageInfo;

import java.util.List;
import java.util.Map;

/**
 * 盘点登记服务类
 * @author 70486
 */
public interface InventoryRegistrationService extends IService<InventoryRegistration> {
	
	/**
	 * 查询列表
	 * @param params
	 * @return PageUtils
	 */
	PageUtils getList(Map<String,Object> params);
	
	/**
	 * 查询盘点审核列表
	 * @param pageTbl
	 * @param map
	 * @return PageUtils
	 */
	PageUtils getContrastList(PageTbl pageTbl, Map<String, Object> map);
	
	/**
	 * 获得自增id
	 * @return Long
	 */
	Long getSequence();
	
	/**
	 * 获取最大盘点编号
	 * @return String
	 */
	String getMaxCode();

	/**
	 * 更新盘点状态为审核通过/审核中
	 * @param planid
	 * @param state
	 */
	void updateExamStatus(Integer planid, Long state);
	
	/**
	 * 查询盘点单下的明细的所有状态（是否已经全部盘点过）
	 * @param planid
	 * @return String
	 */
	String selectExamineArray(Integer planid);

	/**
	 * 查询盘点差异明细导出列
	 * @param ids 明细选中主键
	 * @param infoid 盘点计划主键
	 * @return StorageInfo
	 */
	List<StorageInfo> selectInventoryDifference(List<Long> ids, String infoid);

	/**
	 * 找出这个集结号下的所有上传米数盘点的数据中执行成功的,来对盘点登记表进行更新
	 * @param specialSign 集结号
	 * @param failQacodes 失败的质保号
	 */
	void updateInventoryMeter(Long specialSign,List<String> failQacodes);

	/**
	 * 批量更新盘点表库位
	 * @param lstStorageInfo
	 */
	void updateShelfByInventory(List<StorageInfo> lstStorageInfo);

	/**
	 * 更新盘点库位审核状态
	 * @param qaCode
	 * @param shelfState
	 * @param lstInventory
	 */
	void updateShelfState(String qaCode, Integer shelfState, List<Inventory> lstInventory);
}
