package com.tbl.modules.wms.service.Impl.baseinfo;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.wms.dao.baseinfo.DishDAO;
import com.tbl.modules.wms.entity.baseinfo.Dish;
import com.tbl.modules.wms.service.baseinfo.DishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 盘具服务实现
 * @author 70486
 */
@Service
public class DishServiceImpl extends ServiceImpl<DishDAO, Dish> implements DishService {

    /**
     * 盘具信息
     */
    @Autowired
    private DishDAO dishDAO;

    /**
     * 查询列表信息
     * @param pageTbl
     * @param map 条件
     * @return PageUtils
     */
    @Override
    public PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map) {
        String sortName = pageTbl.getSortname();
        String sortOrder = pageTbl.getSortorder();
        if (StringUtils.isEmptyString(sortName)) {
            sortName = "id";
        }
        if (StringUtils.isEmptyString(sortOrder)) {
            sortOrder = "desc";
        }
        Page page = new Page(pageTbl.getPageno(), pageTbl.getPagesize(), sortName, "asc".equalsIgnoreCase(sortOrder));
        return new PageUtils(page.setRecords(dishDAO.getPageList(page, map)));
    }
    
    /**
     * 获取导出Excel列表
     * @param ids 盘具主键
     * @param code 盘具编码
     * @return List<Dish>
     */
    @Override
    public List<Dish> getExcelList(String ids, String code) {
        Map<String, Object> map = new HashMap<>();
        map.put("ids", StringUtils.stringToInt(ids));
        map.put("code", code);
        return dishDAO.getExcelList(map);
    }
}
