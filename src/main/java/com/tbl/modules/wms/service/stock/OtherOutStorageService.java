package com.tbl.modules.wms.service.stock;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.wms.entity.outstorage.AllOutStorage;

import java.util.List;
import java.util.Map;

/**
 * 退库回生产线清单和采购退库清单合并
 * @author 70486
 * @date 2020/3/17
 */
public interface OtherOutStorageService extends IService<AllOutStorage> {

    /**
     * 退回产线+采购退库+报废清单
     * @param pageTbl
     * @param map
     * @return PageUtils
     */
    PageUtils getList(PageTbl pageTbl, Map<String,Object> map);

    /**
     * 导出execl
     * @param map
     * @return List<AllOutStorage>
     */
    List<AllOutStorage> getExcelList(Map<String, Object> map);

}
