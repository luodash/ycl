package com.tbl.modules.wms.service.Impl.instorage;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.common.utils.DateUtils;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.platform.util.DeriveExcel;
import com.tbl.modules.wms.dao.instorage.InstorageDetailDAO;
import com.tbl.modules.wms.entity.instorage.InstorageDetail;
import com.tbl.modules.wms.entity.inventory.InventoryRegistration;
import com.tbl.modules.wms.service.instorage.InstorageDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 实现入库汇总详情
 * @author 70486
 **/
@Service
public class InstorageDetailServiceImp extends ServiceImpl<InstorageDetailDAO, InstorageDetail> implements InstorageDetailService {

    /**
     * 入库管理-订单详情
     */
    @Autowired
    InstorageDetailDAO instorageDetailDAO;

    /**
     * 获取入库汇总列表
     * @param pageTbl
     * @param map
     * @return PageUtils
     */
    @Override
    public PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map) {
        Page page = new Page(pageTbl.getPageno(), pageTbl.getPagesize(), pageTbl.getSortname(), "asc".equalsIgnoreCase(pageTbl.getSortorder()));
        return new PageUtils(page.setRecords(instorageDetailDAO.getPageList(page, map)));
    }

    /**
     * 获得导出列
     * @param map
     * @return List<InstorageDetail>
     */
    @Override
    public List<InstorageDetail> getAllLists(Map<String,Object> map) {
        return instorageDetailDAO.getAllLists(map);
    }

    /**
     * Excel导出
     * @param response
     * @param path
     * @param lstInstorageDetail
     * @param excelIndexArray
     */
    @Override
    public void toExcel(HttpServletResponse response, String path, List<InstorageDetail> lstInstorageDetail,String [] excelIndexArray) {
        try {
            String sheetName = "入库汇总表" + "(" + DateUtils.getDay() + ")";
            if (path != null && !"".equals(path)) {
                sheetName = sheetName + ".xls";
            } else {
                response.setHeader("Content-Type", "application/force-download");
                response.setHeader("Content-Type", "application/vnd.ms-excel");
                response.setCharacterEncoding("UTF-8");
                response.setHeader("Expires", "0");
                response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
                response.setHeader("Pragma", "public");
                response.setHeader("Content-disposition", "attachment;filename=" + new String(sheetName.getBytes("gbk"),
                        "ISO8859-1") + ".xls");
            }
            Map<String, String> mapFields = new LinkedHashMap<>();

            if (excelIndexArray.length==1&&"".equals(excelIndexArray[0])) {
                mapFields.put("qaCode", "质保单号");
                mapFields.put("batchNo", "批次号");
                mapFields.put("entityNo", "工单号");
                mapFields.put("materialCode", "物料编号");
                mapFields.put("materialName", "物料名称");
                mapFields.put("salesName", "销售经理");
                mapFields.put("instorageTimeStr", "入库时间");
                mapFields.put("dishcode", "盘号");
                mapFields.put("drumType", "盘类型");
                mapFields.put("model", "盘规格");
                mapFields.put("outerDiameter","盘外径");
                mapFields.put("innerDiameter","盘内径");
                mapFields.put("meter", "米数");
                mapFields.put("warehouseCode", "仓库");
                mapFields.put("shelfCode", "库位");
            }else {
                for (String s : excelIndexArray) {
                    if ("2".equals(s)) {
                        mapFields.put("qaCode", "质保单号");
                    } else if ("3".equals(s)) {
                        mapFields.put("batchNo", "批次号");
                    } else if ("4".equals(s)) {
                        mapFields.put("entityNo", "工单号");
                    } else if ("5".equals(s)) {
                        mapFields.put("materialCode", "物料编号");
                    } else if ("6".equals(s)) {
                        mapFields.put("materialName", "物料名称");
                    } else if ("7".equals(s)) {
                        mapFields.put("salesName", "销售经理");
                    } else if ("8".equals(s)) {
                        mapFields.put("instorageTimeStr", "入库时间");
                    } else if ("9".equals(s)) {
                        mapFields.put("dishcode", "盘号");
                    } else if ("10".equals(s)) {
                        mapFields.put("drumType", "盘类型");
                    } else if ("11".equals(s)) {
                        mapFields.put("model", "盘规格");
                    } else if ("12".equals(s)) {
                        mapFields.put("outerDiameter", "盘外径");
                    } else if ("13".equals(s)) {
                        mapFields.put("innerDiameter", "盘内径");
                    } else if ("14".equals(s)) {
                        mapFields.put("meter", "米数");
                    } else if ("15".equals(s)) {
                        mapFields.put("warehouseCode", "仓库");
                    } else if ("16".equals(s)) {
                        mapFields.put("shelfCode", "库位");
                    }
                }
            }

            DeriveExcel.exportExcel(sheetName, lstInstorageDetail, mapFields, response, path);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
	  /**
     * 查询人员正常入库量
     * @param date 日期
     * @return List<InstorageDetail>
     */
    @Override
    public List<InstorageDetail> findList(Date date){
    	return instorageDetailDAO.findList(date);
    }

	/**
     *入库----外采单获取列表数据
     * @param pageTbl
     * @param map 条件
     * @return PageUtils
     */
    @Override
    public PageUtils getPagePackageList(PageTbl pageTbl, Map<String, Object> map) {
        String sortName = pageTbl.getSortname();
        String sortOrder = pageTbl.getSortorder();
        if (StringUtils.isEmptyString(sortName)) {
            sortName = "a.id";
        }
        if (StringUtils.isEmptyString(sortOrder)) {
            sortOrder = "desc";
        }
        Page page = new Page(pageTbl.getPageno(), pageTbl.getPagesize(), sortName, "asc".equalsIgnoreCase(sortOrder));
        return new PageUtils(page.setRecords(instorageDetailDAO.getPagePackageList(page, map)));
    }
    
    /**
	 * 桌面读卡器绑定列表查询
     * @param pageTbl 分页
	 * @param map 查询条件
	 * @return PageUtils
	 */
    @Override
    public PageUtils getList(PageTbl pageTbl, Map<String, Object> map) {
        Page page = new Page(pageTbl.getPageno(), pageTbl.getPagesize(), pageTbl.getSortname(), "asc".equalsIgnoreCase(pageTbl.getSortorder()));
        return new PageUtils(page.setRecords(instorageDetailDAO.getList(page, map)));
    }

    /**
     * 查询明细导出列（包含父表数据）
     * @param ids 标签初始化表明细主键
     * @param infoid 标签初始化父表主键
     * @return InstorageDetail
     */
    @Override
    public List<InstorageDetail> selectInstorageAndDetail(List<Long> ids, String infoid){
        return instorageDetailDAO.selectInstorageAndDetail(ids, infoid);
    }

    /**
     * 根据rfid批量修改标签初始化表
     * @param lstInventoryRegistration 所有的对应ebs盘点单号下的已经盘点的库存(要上传的部分)
     */
    @Override
    public void updateInstorageDetailByRfid(List<InventoryRegistration> lstInventoryRegistration){
        instorageDetailDAO.updateInstorageDetailByRfid(lstInventoryRegistration);
    }
}
