package com.tbl.modules.wms.service.outstorage;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.modules.wms.entity.outstorage.ShipLoadingDetail;

/**
 * 装车发运服务类
 * @author 70486
 */
public interface ShipLoadingDetailService extends IService<ShipLoadingDetail> {
}
