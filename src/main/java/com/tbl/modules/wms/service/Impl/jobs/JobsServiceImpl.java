package com.tbl.modules.wms.service.Impl.jobs;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.modules.wms.dao.jobs.JobsDAO;
import com.tbl.modules.wms.service.jobs.JobsService;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.stereotype.Service;

/**
 * 定时任务实现类
 * @author 70486
 */
@Service("JobsService")
public class JobsServiceImpl extends ServiceImpl<JobsDAO, T> implements JobsService {

}
