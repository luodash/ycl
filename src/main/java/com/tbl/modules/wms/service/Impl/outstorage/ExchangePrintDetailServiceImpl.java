package com.tbl.modules.wms.service.Impl.outstorage;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.google.common.collect.Maps;
import com.tbl.common.utils.DateUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.common.utils.YamlConfigurerUtil;
import com.tbl.modules.wms.dao.baseinfo.FactoryAreaDAO;
import com.tbl.modules.wms.dao.outstorage.ExchangeDAO;
import com.tbl.modules.wms.dao.outstorage.ExchangePrintDAO;
import com.tbl.modules.wms.dao.outstorage.ExchangePrintDetailDAO;
import com.tbl.modules.wms.entity.baseinfo.FactoryArea;
import com.tbl.modules.wms.entity.outstorage.ExchangePrint;
import com.tbl.modules.wms.entity.outstorage.ExchangePrintDetail;
import com.tbl.modules.wms.service.Impl.webserviceImpl.WmsServiceImpl;
import com.tbl.modules.wms.service.outstorage.ExchangePrintDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 物资流转单打印
 * @author wc
 */
@Service
public class ExchangePrintDetailServiceImpl extends ServiceImpl<ExchangePrintDetailDAO, ExchangePrintDetail>
        implements ExchangePrintDetailService {

    /**
     * 打印流转单
     */
    @Autowired
    private ExchangeDAO exchangeDAO;
    /**
     * 流转单服务类
     */
    @Autowired
    private ExchangePrintDAO exchangePrintDAO;
    /**
     * 物资流转单明细
     */
    @Autowired
    private ExchangePrintDetailDAO exchangePrintDetailDAO;
    /**
     * wms调用接口入口
     */
    @Autowired
    private WmsServiceImpl wmsServiceImpl;
    /**
     * 厂区信息
     */
    @Autowired
    private FactoryAreaDAO factoryAreaDAO;

    /**
     * 打印物资流转单
     * @param carno 车牌号
     * @param sFactoryArea 原始厂区
     * @param eFactoryArea 目标厂区
     */
    @Override
    public void printWzlz(String carno, String sFactoryArea, String eFactoryArea) {
        DecimalFormat df = new DecimalFormat("0000");
        //查找物资流转单表中得最大的物资流单号
        String code = exchangeDAO.getMaxWzlzCode();
        String day = DateUtils.getDays();
        if(StringUtils.isNotBlank(code)){
            int temp = Integer.parseInt(code.substring(code.length()-4));
            code = "WZLZ"+day+df.format(temp+1);
        }else{
            code = "WZLZ"+day+"0001";
        }
        List<Map<String, Object>> printDetailList = new ArrayList<>();
        //发货单号集合
        List<String> shipNoList = exchangePrintDetailDAO.getShipNoList(carno);
        if (shipNoList!=null && shipNoList.size()>0){
            //物资流转单明细集合
            printDetailList = exchangePrintDetailDAO.getPrintDetailList(shipNoList);
        }
        Map<String,Object> map = new HashMap<>();
        Map<String,Object> reMap = new HashMap<>();
        Map<String,Object> batchesIdentityMap = Maps.newIdentityHashMap();
        String sfactoryName = factoryAreaDAO.selectOne(new FactoryArea(){{
            setCode(sFactoryArea);
        }}).getName();
        String efactoryName = factoryAreaDAO.selectOne(new FactoryArea(){{
            setCode(eFactoryArea);
        }}).getName();
        map.put("fromfactory", sfactoryName);
        map.put("tofactory", efactoryName);
        //打印日期
        map.put("printdate", DateUtils.getDay());
        //车牌号
        map.put("carno",carno);
        //物资流转单号
        map.put("oddno",code);
        //打印机printer
        map.put("printer", YamlConfigurerUtil.getStrYmlVal("yddl.printer_"+ sFactoryArea));

        printDetailList.forEach(stringObjectMap -> batchesIdentityMap.put(new String("exchangeprintdetail"),
                new HashMap<String,Object>(11){{
                    put("model",stringObjectMap.get("MODEL"));
                    put("num",stringObjectMap.get("NUM"));
                    put("unit",stringObjectMap.get("UNIT"));
                    put("colour",stringObjectMap.get("COLOUR"));
                    put("shelfcode",stringObjectMap.get("SHELFCODE"));
                    put("dishcode",stringObjectMap.get("DISHCODE"));
                    put("segmentno",stringObjectMap.get("SHELFCODE"));
                    put("qacode",stringObjectMap.get("QACODE"));
                    put("weight",stringObjectMap.get("WEIGHT"));
                    put("dingzhi",stringObjectMap.get("DINGZHI"));
                    put("remark",stringObjectMap.get("REMARK"));
                }}));

        //流转单详情
        map.put("batchs",batchesIdentityMap);
        reMap.put("line",map);
        if(wmsServiceImpl.FEWMS031(reMap)){
            saveExchangePrintInfo(carno,sfactoryName,code,efactoryName,printDetailList);
        }
    }

    /**
     * 保存物资流转单打印信息
     * @param carNo 车牌号
     * @param sFactoryName 从厂
     * @param oddNumber 流转单号
     * @param eFactoryName 至厂
     * @param listDetail 数据集合
     */
    public void saveExchangePrintInfo(String carNo,String sFactoryName,String oddNumber,String eFactoryName,
                                      List<Map<String,Object>> listDetail ){
        ExchangePrint exchangePrint=new ExchangePrint();
        exchangePrint.setCar(carNo);
        exchangePrint.setOddnumber(oddNumber);
        exchangePrint.setSFactoryArea(sFactoryName);
        exchangePrint.setEFactoryArea(eFactoryName);
        exchangePrint.setPrintTime(DateUtils.getTime());
        exchangePrintDAO.insert(exchangePrint);

        for(Map<String,Object> map:listDetail){
            ExchangePrintDetail exchangePrintDetail=new ExchangePrintDetail();

            exchangePrintDetail.setPid(exchangePrint.getId());
            exchangePrintDetail.setModel(map.get("MODEL")==null?"":map.get("MODEL").toString());
            exchangePrintDetail.setNum(map.get("NUM")==null?"":map.get("NUM").toString());
            exchangePrintDetail.setUnit(map.get("UNIT")==null?"":map.get("UNIT").toString());
            exchangePrintDetail.setColor(map.get("COLOUR")==null?"":map.get("COLOUR").toString());
            exchangePrintDetail.setShelfcode(map.get("SHELFCODE")==null?"":map.get("SHELFCODE").toString());
            exchangePrintDetail.setDishcode(map.get("DISHCODE")==null?"":map.get("DISHCODE").toString());
            exchangePrintDetail.setSegmentno(map.get("SEQMENTNO")==null?"":map.get("SEQMENTNO").toString());
            exchangePrintDetail.setQacode(map.get("QACODE")==null?"":map.get("QACODE").toString());
            exchangePrintDetail.setWeight(map.get("WEIGHT")==null?"":map.get("WEIGHT").toString());
            exchangePrintDetail.setDingzhi("");
            exchangePrintDetail.setRemark("");
            exchangePrintDetail.setShipNo(map.get("SHIP_NO")==null?"":map.get("SHIP_NO").toString());

            //printDetailList.add(exchangePrintDetail);
            exchangePrintDetailDAO.insert(exchangePrintDetail);
        }
        //exchangePrintDetailService.insertBatch(printDetailList);
    }

    public static void main (String[] args){
        String ss="WZLZ202005110001";
        System.out.print(ss.substring(ss.length()-4));
    }
}
