package com.tbl.modules.wms.service.inventory;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.modules.wms.entity.inventory.InventoryIn;

/**
 * 入库日志记录表
 * @author 70486
 */
public interface InventoryInService extends IService<InventoryIn> {
	
	
}
