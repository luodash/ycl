package com.tbl.modules.wms.service.Impl.stock;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.wms.dao.stock.OtherOutStorageDAO;
import com.tbl.modules.wms.entity.outstorage.AllOutStorage;
import com.tbl.modules.wms.service.stock.OtherOutStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 其他出库信息汇总
 * @author 70486
 */
@Service
public class OtherOutStorageServiceImpl extends ServiceImpl<OtherOutStorageDAO, AllOutStorage> implements OtherOutStorageService {

    /**
     * 出库
     */
    @Autowired
    OtherOutStorageDAO otherOutStorageDAO;
    
    /**
     * @description 退回产线+采购退货+报废清单
     * @param map
     * @return PageUtils
     */
    @Override
    public PageUtils getList(PageTbl pageTbl, Map<String, Object> map) {
        String sortName = pageTbl.getSortname();
        String sortOrder = pageTbl.getSortorder();
        if (StringUtils.isEmptyString(sortName)) {
            sortName = "id";
        }
        if (StringUtils.isEmptyString(sortOrder)) {
            sortOrder = "desc";
        }
        Page page = new Page(pageTbl.getPageno(), pageTbl.getPagesize(), sortName, "asc".equalsIgnoreCase(sortOrder));
        return new PageUtils(page.setRecords(otherOutStorageDAO.getPageList(page, map)));
    }

    /**
     * @description 导出execl
     * @param map
     * @return List<AllOutStorage>
     */
    @Override
    public List<AllOutStorage> getExcelList(Map<String, Object> map) {
        return otherOutStorageDAO.getExcelList(map);
    }
}
