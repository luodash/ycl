package com.tbl.modules.wms.service.Impl.move;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.modules.wms.dao.move.MoveInStorageDAO;
import com.tbl.modules.wms.entity.move.MoveInStorage;
import com.tbl.modules.wms.service.move.MoveInStorageService;
import org.springframework.stereotype.Service;

/**
 * 移库处理-移库入库
 * @author 70486
 */
@Service
public class MoveInStorageServiceImpl extends ServiceImpl<MoveInStorageDAO, MoveInStorage> implements MoveInStorageService {
	
}
