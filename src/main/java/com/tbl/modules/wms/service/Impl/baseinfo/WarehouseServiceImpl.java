package com.tbl.modules.wms.service.Impl.baseinfo;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.Query;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.wms.dao.baseinfo.WarehouseDAO;
import com.tbl.modules.wms.entity.baseinfo.Warehouse;
import com.tbl.modules.wms.service.baseinfo.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 仓库管理
 * @author 70486
 */
@Service("yddlWarehouseService")
public class WarehouseServiceImpl extends ServiceImpl<WarehouseDAO, Warehouse> implements WarehouseService {

    /**
     * 仓库
     */
    @Autowired
    private WarehouseDAO warehouseDAO;

    /**
     * 获取仓库列表
     * @param pageTbl
     * @param map
     * @return PageUtils
     */
    @Override
    public PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map) {
        String sortName = pageTbl.getSortname();
        String sortOrder = pageTbl.getSortorder();
        Page page = new Page(pageTbl.getPageno(), pageTbl.getPagesize(), "a.state".equals(sortName)?"a.state desc,a.id":sortName,
                "asc".equalsIgnoreCase(sortOrder));
        return new PageUtils(page.setRecords(warehouseDAO.getPageList(page, map)));
    }

    /**
     * 获取原材料仓库列表
     * @param pageTbl
     * @param map
     * @return PageUtils
     */
    @Override
    public PageUtils getYclPageList(PageTbl pageTbl, Map<String, Object> map) {
        String sortName = pageTbl.getSortname();
        String sortOrder = pageTbl.getSortorder();
        if (StringUtils.isEmptyString(sortName)) {
            sortName = "a.id";
        }
        if (StringUtils.isEmptyString(sortOrder)) {
            sortOrder = "desc";
        }
        Page page = new Page(pageTbl.getPageno(), pageTbl.getPagesize(), "a.state".equals(sortName)?"a.state,a.id":sortName, "asc".equalsIgnoreCase(sortOrder));
        return new PageUtils(page.setRecords(warehouseDAO.getYclPageList(page, map)));
    }

    /**
     * 获取导出列表
     * @param ids 仓库主键
     * @param name 仓库名称
     * @param code 仓库编码
     * @param factoryCode 厂区
     * @param factoryList 厂区
     * @return List<Warehouse> 仓库列表
     */
    @Override
    public List<Warehouse> getExcelList(String ids, String name, String code,String factoryCode, List<Long> factoryList) {
        Map<String, Object> map = new HashMap<>(5);
        map.put("ids", StringUtils.stringToInt(ids));
        map.put("name", name);
        map.put("code", code);
        map.put("factoryCode", factoryCode);
        map.put("factoryList", factoryList);
        return warehouseDAO.getExcelList(map);
    }

    /**
     * 获取仓库列表
     * @param params 参数条件
     * @return Page<Warehouse>
     */
    @Override
    public Page<Warehouse> getSelectWarehouseList(Map<String, Object> params) {
        String queryString = (String)params.get("queryString");
        Page<Warehouse> page = this.selectPage(new Query<Warehouse>(params).getPage(), new EntityWrapper<Warehouse>().eq("STATE", 1)
                .like(StringUtils.isNotEmpty(queryString),"CODE", queryString).eq("FACTORYCODE", params.get("factoryCode")));
        page.getRecords().forEach(warehouse -> warehouse.setText(warehouse.getName()));

        return page;
    }

    /**
     * 获取仓库列表信息
     * @param pageTbl
     * @param map
     * @return PageUtils
     */
    @Override
    public PageUtils getPageWarehouseList(PageTbl pageTbl, Map<String, Object> map) {
        String sortName = pageTbl.getSortname();
        String sortOrder = pageTbl.getSortorder();
        if (StringUtils.isEmptyString(sortName)) {
            sortName = "id";
        }
        if (StringUtils.isEmptyString(sortOrder)) {
            sortOrder = "desc";
        }
        Page page = new Page(pageTbl.getPageno(), pageTbl.getPagesize(), sortName, "asc".equalsIgnoreCase(sortOrder));
        return new PageUtils(page.setRecords(warehouseDAO.getPageWarehouseList(page, map)));
    }

    /**
     * 获取仓库全部下拉列表
     * @param map
     * @return List<Map<String,Object>>
     */
    @Override
    public List<Map<String, Object>> getWarehouseList(Map<String, Object> map) {
        return warehouseDAO.getWarehouseList(map);
    }

    @Override
    public List<Map<String, Object>> getYclPageWarehouseList(Map<String, Object> map) {
        return warehouseDAO.getYclPageWarehouseList(map);
    }

    @Override
    public List<Map<String, Object>> getYclPageWarehouseListByEntityID(Map<String, Object> map) {
        return warehouseDAO.getYclPageWarehouseListByEntityID(map);
    }


}
