package com.tbl.modules.wms.service.Impl.baseinfo;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.wms.dao.baseinfo.MaterialDAO;
import com.tbl.modules.wms.entity.baseinfo.Material;
import com.tbl.modules.wms.service.baseinfo.MaterialService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 物料
 * @author 70486
 */
@Service("yddlMaterialService")
public class MaterialServiceImpl extends ServiceImpl<MaterialDAO, Material> implements MaterialService {

    /**
     * 物料
     */
    @Resource
    private MaterialDAO materialDAO;

    /**
     * 分页获取数据
     * @param pageTbl 分页工具
     * @param map 条件查询
     * @return PageUtils
     */
    @Override
    public PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map) {
        String sortName = pageTbl.getSortname();
        String sortOrder = pageTbl.getSortorder();
        if (StringUtils.isEmptyString(sortName)) {
            sortName = "id";
        }
        if (StringUtils.isEmptyString(sortOrder)) {
            sortOrder = "desc";
        }
        map.put("categoryNum",(map.get("industry") != null && !"".equals(map.get("industry"))? (map.get("industry") + ".") : "") +
                (map.get("category") != null && !"".equals(map.get("category")) ? (map.get("category") + ".") : "") +
                (map.get("gradeTwo") != null && !"".equals(map.get("gradeTwo")) ? (map.get("gradeTwo") + ".") : "") +
                (map.get("gradeThree") != null && !"".equals(map.get("gradeThree")) ? map.get("gradeThree") : "" ));
        Page page = new Page(pageTbl.getPageno(), pageTbl.getPagesize(), sortName, "asc".equalsIgnoreCase(sortOrder));
        return new PageUtils(page.setRecords(materialDAO.getPageList(page, map)));
    }

    /**
     * 获得Excel导出列表
     * @param ids 物料主键
     * @param name 物料名称
     * @return List<Material> 物料集合
     */
    @Override
    public List<Material> getExcelList(String ids, String name) {
        Map<String, Object> map = new HashMap<>(2);
        map.put("ids", StringUtils.stringToInt(ids));
        map.put("name", name);
        return materialDAO.getExcelList(map);
    }

    /**
     * 根据id获取详细信息
     * @param id
     * @return
     */
    @Override
    public Material selectInfoById(Long id){
        return materialDAO.selectInfoById(id);
    }


    /**
     * 根据物料编码获取物料信息-精确查找
     */
    @Override
    public Map<String,Object> selectMaterialByCode(Map map){
        Map<String,Object> resultMap = new HashMap<>();
        Map<String,Object> data = materialDAO.selectMaterialByCode(map);
        if(data.size()>0){
            resultMap.put("code",0);
            resultMap.put("msg","success");
            resultMap.put("result",true);
            resultMap.put("data",data);
        }else{
            resultMap.put("code",1);
            resultMap.put("msg","no data");
            resultMap.put("result",false);
            resultMap.put("data","");
        }
        return resultMap;
    }


    /**
     * 根据物料编码查询物料信息-模糊查询
     * @param map
     * @return
     */
    @Override
    public Map<String,Object> getMaterialsByCode(Map map){
        Map<String,Object> resultMap = new HashMap<>();
        List<Map<String,Object>> list = materialDAO.getMaterialsByCode(map);
        if(list.size()>0){
            resultMap.put("code",0);
            resultMap.put("msg","success");
            resultMap.put("result",true);
            resultMap.put("data",list);
        }else{
            resultMap.put("code",1);
            resultMap.put("msg","no data");
            resultMap.put("result",false);
            resultMap.put("data","");
        }
        return resultMap;
    }

    @Override
    public Map<String,Object> getMaterialsPageByCode(Map map){
        Map<String,Object> resultMap = new HashMap<>();
        int count = materialDAO.getCountByCode(map);
        if(count>0){
            List<Map<String,Object>> pageList = materialDAO.getPageByCode(map);
            resultMap.put("code",0);
            resultMap.put("msg","success");
            resultMap.put("result",true);
            resultMap.put("count",count);
            resultMap.put("data",pageList);
        }else{
            resultMap.put("code",1);
            resultMap.put("msg","no data");
            resultMap.put("result",false);
            resultMap.put("count",0);
            resultMap.put("data","");
        }
        return resultMap;
    }



    /**
     * 获取行业全部下拉列表
     * @return List<Map<String,Object>>
     */
    @Override
    public List<Map<String, Object>> getIndustryList() {
        return materialDAO.getIndustryList();
    }

    /**
     * 获取类别全部下拉列表
     * @param industry 行业编码
     * @return List<Map<String,Object>>
     */
    @Override
    public List<Map<String, Object>> getCategoryList(String industry) {
        return materialDAO.getCategoryList(industry);
    }

    /**
     * 功能描述：获取二级材料列表
     * @param industry 行业编码
     * @param category 物理类别
     * @return List<Map<String,Object>>
     */
    @Override
    public List<Map<String, Object>> getGradeTwoList(String industry, String category){
        return materialDAO.getGradeTwoList(industry, category);
    }

    /**
     * 功能描述：获取三级材料列表
     * @param industry 行业编码
     * @param category 物理类别
     * @param gradeTwo 二级材料
     * @return List<Map<String,Object>>
     */
    @Override
    public List<Map<String, Object>> getGradeThreeList(String industry, String category, String gradeTwo){
        return materialDAO.getGradeThreeList(industry, category, gradeTwo);
    }

}
