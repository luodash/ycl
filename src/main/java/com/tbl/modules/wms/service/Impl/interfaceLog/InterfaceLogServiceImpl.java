package com.tbl.modules.wms.service.Impl.interfaceLog;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.Query;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.wms.dao.interfacelog.InterfaceLogDAO;
import com.tbl.modules.wms.entity.interfacelog.InterfaceLog;
import com.tbl.modules.wms.service.interfacelog.InterfaceLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * 接口日志记录实现类
 * @author 70486
 **/
@Service
public class InterfaceLogServiceImpl extends ServiceImpl<InterfaceLogDAO, InterfaceLog> implements InterfaceLogService {

	/**
	 * 接口日志记录
	 */
	@Autowired
	private InterfaceLogDAO interfaceLogDAO;
	
	/**
	 * 插入日志
	 * @param code 接口编码
	 * @param name 接口名称
	 * @param requestString 请求报文
	 * @param responseString 返回报文
	 * @param qacode 质保号
	 * @param batchNo 批次号
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void insertLog(String code,String name,String requestString,String responseString,String qacode,String batchNo) {
		interfaceLogDAO.insertLog(code,name,requestString,responseString,qacode,batchNo);
	}

	/**
	 * 获取接口日志维护列表
	 * @param map 参数条件
	 * @return PageUtils
	 */
	@Override
	public PageUtils queryPage(Map<String,Object> map){
		String interfacecode = (String)map.get("interfacecode");
		String qaCode = (String)map.get("qaCode");
		Page<InterfaceLog> page = this.selectPage(new Query<InterfaceLog>(map).getPage(), new EntityWrapper<InterfaceLog>()
				.like(StringUtils.isNotBlank(interfacecode),"INTERFACE_CODE", interfacecode != null ? interfacecode.trim() : null)
				.like(StringUtils.isNotBlank(qaCode),"QACODE", qaCode != null ? qaCode.trim() : null));
		return new PageUtils(page.setRecords(page.getRecords()));
	}
}
