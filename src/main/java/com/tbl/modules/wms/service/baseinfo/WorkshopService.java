package com.tbl.modules.wms.service.baseinfo;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.wms.entity.baseinfo.Workshop;

import java.util.List;
import java.util.Map;

/**
 * 车间服务类
 * @author 70486
 */
public interface WorkshopService extends IService<Workshop> {
	 /**
     * 功能描述:列表页
      * @param pageTbl
      * @param map
      * @return PageUtils
     */
    PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map);
    
    /**
     * 功能描述:导出的数据
     * @param ids
     * @param queryCode
     * @param queryOrg
     * @param factoryList
     * @return Workshop
     */
    List<Workshop> getExcelList(String ids, String queryCode, String queryOrg, List<String> factoryList);
}
