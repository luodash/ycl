package com.tbl.modules.wms.service.outstorage;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.wms.entity.baseinfo.Car;
import com.tbl.modules.wms.entity.outstorage.OutStorage;
import com.tbl.modules.wms.entity.outstorage.OutStorageDetail;

import java.util.List;
import java.util.Map;

/**
 * 发货单服务类
 * @author 70486
 */
public interface OutStorageService extends IService<OutStorage> {

    /**
     * 发货单列表数据
     * @param pageTbl
     * @param map
     * @return PageUtils
     */
    PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map);

    /**
     * 获取打印发货单列表数据
     * @param pageTbl
     * @param map
     * @return PageUtils
     */
    PageUtils getPrintPageList(PageTbl pageTbl, Map<String, Object> map);

    /**
     * 发货执行数据
     * @param pageTbl
     * @param id
     * @return PageUtils
     */
    PageUtils getDetailList(PageTbl pageTbl, Integer id);

    /**
     * 获取Excel列表数据
     * @param map
     * @return OutStorage
     */
    List<OutStorage> getExcelList(Map<String, Object> map);

    /**
     * 获取查看明细Excel列表数据
     * @param map
     * @return OutStorageDetail
     */
    List<OutStorageDetail> getDetailExcelList(Map<String, Object> map);

    /**
     * 更新主表信息
     * @param outStorage
     * @return boolean
     */
    boolean saveOutStorage(OutStorage outStorage);

    /**
     * 点击提价更新主表状态为已提交
     * @param outStorage
     * @return boolean
     */
    boolean subOutStorage(OutStorage outStorage);

    /**
     * 获取到质保单号列表
     * @param pageTbl
     * @param map
     * @return PageUtils
     */
    PageUtils getQaCode(PageTbl pageTbl, Map<String, Object> map);

    /**
     * 发货单详情添加
     * @param id
     * @param qaCodeIds
     * @return boolean
     */
    boolean saveOutstorageDetail(Long id, String[] qaCodeIds);

    /**
     * 删除详情数据
     * @param ids
     * @return Map<String,Object>
     */
    Map<String, Object> deleteOutStorageDetailIds(String[] ids);

    /**
     * 绑定行车
     * @param detail
     * @return boolean
     */
    boolean saveCar(OutStorageDetail detail);

    /**
     * 根据仓库编码获取行车
     * @param warehouseCode
     * @param org
     * @return Car
     */
    List<Car> selectCarListByWarehouseCode(String warehouseCode,List<String> org);

    /**
     * 点击出库执行，点击开始拣货
     * @param id
     * @param userId
     * @return boolean
     */
    boolean detailStart(Long id, Long userId);

    /**
     * 点击出库执行，点击完成拣货
     * @param id 出库单详情主键
     * @param userId 用户主键
     * @param outtime 出库时间
     * @param isPrint 是否打印出库单
     * @return Map<String,Object>
     */
    Map<String,Object> detailSuccess(Long id, Long userId, String outtime, Integer isPrint);

    /**
     * 根据发货单号获取批量得打印发货单信息
     * @param wayno
     * @param outno
     * @param type
     * @return Map<String,Object>
     */
    List<Map<String,Object>> getPrintInfo(String wayno,String outno,String type);


    /**
     *点击打印，获取打印信息列表
     * @param pageTbl
     * @param car
     * @return PageUtils
     */
    PageUtils getPrintPageList1(PageTbl pageTbl,String car );

    /**
     * 点击确认出库，批量完成出库
     * @param lstIds 出库单详情主键
     * @param userId 用户主键
     * @param outtime 出库事务日期
     * @param isPrint 是否需要打印
     * @return Map<String,Object>
     */
    Map<String,Object> outsStorageOut(List<Long> lstIds, Long userId, String outtime, Integer isPrint);

    /**
     * 根据输入的提货单号获取车辆及货物信息
     * @param shipNo 发货单号
     * @return Map<String,Object>
     */
    List<Map<String,Object>> getOutstorageListByCarNo(String shipNo);
}
