package com.tbl.modules.wms.service.instorage;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.wms.entity.baseinfo.Car;
import com.tbl.modules.wms.entity.baseinfo.Warehouse;
import com.tbl.modules.wms.entity.instorage.Instorage;
import com.tbl.modules.wms.entity.instorage.InstorageDetail;

import java.util.List;
import java.util.Map;

/**
 * 入库管理-入库表
 * @author 70486
 */
public interface InstorageService extends IService<Instorage> {

    /**
     * 订单展示----获取列表数据
     * @param pageTbl 分页工具页面类
     * @param map 参数条件
     * @return PageUtils
     */
    PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map);

    /**
     * 订单展示----获取详情列表数据
     * @param pageTbl 分页工具页面类
     * @param map 参数条件
     * @return PageUtils
     */
    PageUtils getPageDetgailList(PageTbl pageTbl, Map<String, Object> map);

    /**
     * 装包管理----获取详情列表数据
     * @param pageTbl 分页工具页面类
     * @param map 参数条件
     * @return PageUtils
     */
    PageUtils getPackDetailPageList(PageTbl pageTbl, Map<String, Object> map);

    /**
     * 装包管理----根据id获取详情数据
     * @param id 入库明细主键
     * @return InstorageDetail
     */
    InstorageDetail getEntityById(Long id);

    /**
     * 装包管理----点击确认改变状态
     * @param lstInstorageDetail 入库明细
     */
    void packDetailConfirm(List<InstorageDetail> lstInstorageDetail);

    /**
     * 装包管理----导出
     * @param map 参数条件
     * @return InstorageDetail
     */
    List<InstorageDetail> getDetailExcelList(Map<String, Object> map);

    /**
     * 吊装台账----导出
     * @param map 参数条件
     * @return InstorageDetail
     */
    List<InstorageDetail> getAccountHoistExcelList(Map<String, Object> map);

    /**
     * 装包台账----导出
     * @param map 参数条件
     * @return InstorageDetail
     */
    List<InstorageDetail> getAccountPackingExcelList(Map<String, Object> map);

    /**
     * 入库----获取列表数据
     * @param pageTbl 分页工具页面类
     * @param map 参数条件
     * @return PageUtils
     */
    PageUtils getPagePackageList(PageTbl pageTbl, Map<String, Object> map);

    /**
     * 入库----获取仓库列表
     * @param code 仓库编码
     * @return Warehouse
     */
    List<Warehouse> getWarehouseList(String code);

    /**
     * 入库----获取仓库下的行车列表
     * @param id 仓库主键
     * @param org 厂区编码
     * @return Car
     */
    List<Car> getCarList(Long id,String org);
    
    /**
     * 入库----获取仓库下的行车列表
     * @param factoryCode 厂区编码
     * @return Car
     */
    List<Car> findCarList(String factoryCode);

    /**
     * 入库----根据详情id获取详情信息
     * @param id 标签初始化表主键
     * @return InstorageDetail
     */
    InstorageDetail getDetailById(Long id);

    /**
     * 入库----获取到推荐库位
     * @param code 仓库编码
     * @param factoryCode 厂区编码
     * @return String
     */
    String getRecommendShelf(String code,String factoryCode);

    /**
     * 入库----开始入库，更新详情表信息
     * @param detail 入库明细
     * @return boolean
     */
    boolean updateDetail(InstorageDetail detail);

    /**
     * 入库----入库确认获取选择的库位列表
     * @param pageTbl 分页工具页面类
     * @param map 参数条件
     * @return PageUtils
     */
    PageUtils selectUnbindShelf(PageTbl pageTbl, Map<String, Object> map);

    /**
     * 手动入库----入库确认获取选择的库位列表
     * @param pageTbl 分页工具页面类
     * @param map 参数条件
     * @return PageUtils
     */
    PageUtils selectManualShelf(PageTbl pageTbl, Map<String, Object> map);

    /**
     * 入库----导出
     * @param map 参数条件
     * @return InstorageDetail
     */
    List<InstorageDetail> getPackageInExcelList(Map<String, Object> map);

    /**
     * 入库----入库确认
     * 1.更新入库详情表信息
     * 2.入库成功需要在库存表将入库成功的数据插入
     * @param id 标签初始化主键
     * @param shelfId 库位主键
     * @param userId 用户主键
     * @param time 事务处理日期
     * @param factoryId 厂区主键
     * @param warehouseId 仓库主键
     * @return Map<String,Object>
     */
    Map<String,Object> confirmInstorage(Long id, Long shelfId, Long userId, String time,Long factoryId,Long warehouseId);


}
