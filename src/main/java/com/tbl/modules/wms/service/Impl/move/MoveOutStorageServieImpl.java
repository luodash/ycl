package com.tbl.modules.wms.service.Impl.move;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.modules.wms.dao.move.MoveOutStorageDAO;
import com.tbl.modules.wms.entity.move.MoveOutStorage;
import com.tbl.modules.wms.service.move.MoveOutStorageService;
import org.springframework.stereotype.Service;

/**
 * 移库处理-移库出库
 * @author 70486
 */
@Service
public class MoveOutStorageServieImpl extends ServiceImpl<MoveOutStorageDAO, MoveOutStorage> implements MoveOutStorageService {
	
}
