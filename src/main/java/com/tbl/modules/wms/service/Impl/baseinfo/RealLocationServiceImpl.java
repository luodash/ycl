package com.tbl.modules.wms.service.Impl.baseinfo;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.modules.wms.dao.baseinfo.RealLocationDAO;
import com.tbl.modules.wms.entity.baseinfo.RealLocation;
import com.tbl.modules.wms.service.baseinfo.RealLocationService;
import org.springframework.stereotype.Service;

/**
 * 真实库位坐标记录
 * @author 70486
 */
@Service
public class RealLocationServiceImpl extends ServiceImpl<RealLocationDAO, RealLocation> implements RealLocationService {


}
