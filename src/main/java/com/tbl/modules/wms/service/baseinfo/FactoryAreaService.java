package com.tbl.modules.wms.service.baseinfo;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.wms.entity.baseinfo.FactoryArea;

import java.util.List;
import java.util.Map;

/**
 * 厂区信息
 * @author 70486
 */
public interface FactoryAreaService extends IService<FactoryArea> {

    /**
     * 分页获取数据
     * @param pageTbl
     * @param map
     * @return PageUtils
     */
    PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map);

    /**
     * 获取导出物料列表
     * @param ids
     * @param name
     * @param code
     * @param factoryList
     * @return List<FactoryArea>
     */
    List<FactoryArea> getExcelList(String ids, String name, String code,List<Long> factoryList);

    /**
     * 获取厂区列表
     * @param map
     * @return List<Map<String,Object>>
     */
    List<Map<String, Object>> getFactoryList(Map<String, Object> map);

    /**
     * 获取厂区信息
     * @param listArr
     * @return Map<String,Object>
     */
    Map<String,Object>  getFactoryInfo(List<String> listArr);

    /**
     * 获取厂区的所有信息
     * @return List<Map<String, Object>>
     */
    List<Map<String, Object>> getFactoryList();

    /**
     * 清空厂区流水号，每日清空
     */
    void updateSerialNum();
    
    /**
     * 清空调拨、移库单流水号，每日清空
     */
    void updateAlloMoveSerialNum();
    
    /**
     * 清空盘点单流水号，每月清空
     */
    void updateInventoryNum();
    
}
