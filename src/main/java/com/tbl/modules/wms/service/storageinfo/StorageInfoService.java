package com.tbl.modules.wms.service.storageinfo;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.wms.entity.instorage.InstorageDetail;
import com.tbl.modules.wms.entity.storageinfo.StorageInfo;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;
/**
 * 库存查询
 * @author 70486
 */
public interface StorageInfoService extends IService<StorageInfo> {
	
    /**
     * 退库回生产线列表
     * @param page
     * @param params
     * @return PageUtils
     */
    PageUtils getReturnStockList(PageTbl page,Map<String, Object> params);

    /**
     * 库存列表
     * @param page
     * @param params
     * @return PageUtils
     */
    PageUtils getStockList(PageTbl page, Map<String, Object> params);
    
    /**
     * 获取导出列
     * @param map
     * @return List<StorageInfo>
     * */
    List<StorageInfo> getAllLists(Map<String,Object> map);

    /**
     * 获取导出列
     * @param ids
     * @param qaCode
     * @return List<InstorageDetail>
     * */
    List<InstorageDetail> getAllLists2(String ids, String qaCode);

    /**
     * 导出Excel
     * @param response
     * @param path
     * @param list
     * @param excelIndexArray
     * */
    void toExcel(HttpServletResponse response, String path, List<StorageInfo> list,String [] excelIndexArray);

    /**
     * 导出Excel
     * @param response
     * @param path
     * @param list
     * */
    void toExcel2(HttpServletResponse response, String path, List<InstorageDetail> list);
    
    /**
     * 获取图表数据"库龄"占比
     * @return Map<String,Object>
     */
    Map<String,Object> getLibraryAge();

    /**
     * 根据质保号获取库存主键字符拼接
     * @param qacodes 质保号数组
     * @return String 库存主键字符（1，2，3，4，5，6）
     * */
    String selectStorageInfoIdsByQacode(String [] qacodes);

    /**
     * 根据库位编码查询库存信息
     * @param factoryCode 厂区
     * @param warehouseCode 仓库
     * @param shelfCode 库位
     * @return StorageInfo
     * */
    List<StorageInfo> getStorageInfoByShelf(String factoryCode, String warehouseCode, String shelfCode);

    /**
     * 根据wms盘点单号主键修改库存米数，把库存米数更新为盘点米数
     * @param lstPlanId EBS盘点单号对应的多个wms盘点单主键集合
     * @param failQacodes EBS处理失败的质保号
     */
    void updateMeterByInventory(List<String> lstPlanId, List<String> failQacodes);

    /**
     * 找出这个集结号下的所有上传米数盘点的数据中执行成功的,来对库存表进行更新
     * @param specialSign 集结号
     * @param failQacodes 失败的质保号
     */
    void updateStorageInfoMeterByInventoryRegistration(Long specialSign,List<String> failQacodes);
}
