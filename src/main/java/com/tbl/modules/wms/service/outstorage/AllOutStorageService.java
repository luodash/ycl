package com.tbl.modules.wms.service.outstorage;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.wms.entity.outstorage.AllOutStorage;

import java.util.Map;

/**
 * 1:退库回生产线  2.采购退货  3.报废 出库记录
 * @author 70486
 */
public interface AllOutStorageService extends IService<AllOutStorage> {
    /**
     * 获取库存交易查询
     * @param pageTbl
     * @param map
     * @return PageUtils
     */
    PageUtils getList(PageTbl pageTbl, Map<String, Object> map);
}
