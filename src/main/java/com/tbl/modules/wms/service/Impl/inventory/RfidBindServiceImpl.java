package com.tbl.modules.wms.service.Impl.inventory;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.Query;
import com.tbl.modules.wms.dao.pda.RfidBindDAO;
import com.tbl.modules.wms.entity.split.RfidBind;
import com.tbl.modules.wms.service.inventory.RfidBindService;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * rfid绑定
 * @author 70486
 */
@Service("rfidBindService")
public class RfidBindServiceImpl extends ServiceImpl<RfidBindDAO, RfidBind> implements RfidBindService {
	
	/**
	 * rfid绑定列表查询
	 * @param params 条件
	 * @return PageUtils
	 */
	@Override
	public PageUtils getList(Map<String, Object> params) {
        return new PageUtils(this.selectPage(new Query<RfidBind>(params).getPage(), new EntityWrapper<>()));
    }
	
}
