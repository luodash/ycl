package com.tbl.modules.wms.service.webserviceutil;

import com.tbl.modules.wms.constant.XmlToolsUtil;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 *  访问webserce入口
 * @author 70486
 */
public class CallByHttp {

	 /*private static String actionBySOAP(String codeType){
        StringBuilder sb = new StringBuilder();
        sb.append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:cux='http://xmlns.oracle.com/apps/cux/soaprovider/plsql/cux_0_ws_server_prg/' xmlns:inv='http://xmlns.oracle.com/apps/cux/soaprovider/plsql/cux_0_ws_server_prg/invokefmsws/'><soapenv:Header><wsse:Security xmlns:wsse='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd' xmlns='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd' xmlns:env='http://schemas.xmlsoap.org/soap/envelope/' soapenv:mustUnderstand='1'><wsse:UsernameToken xmlns:wsse='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd' xmlns='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd'><wsse:Username>FE_WSADMIN</wsse:Username>  <wsse:Password Type='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText'>wsadmin</wsse:Password></wsse:UsernameToken></wsse:Security><cux:SOAHeader><cux:Responsibility>FND_REP_APP</cux:Responsibility><cux:RespApplication>FND</cux:RespApplication><cux:SecurityGroup>STANDARD</cux:SecurityGroup><cux:NLSLanguage>AMERICAN</cux:NLSLanguage><cux:Org_Id></cux:Org_Id></cux:SOAHeader></soapenv:Header><soapenv:Body><inv:InputParameters><inv:P_IFACE_CODE>FEWMS014</inv:P_IFACE_CODE><inv:P_BATCH_NUMBER>32232323232111</inv:P_BATCH_NUMBER><inv:P_REQUEST_DATA>");
                        sb.append("<![CDATA[<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                        sb.append("<data><header><sourcecode>FEWMS014</sourcecode><targetcode>01</targetcode><formatcode>FEWMS014</formatcode><batchnum>1</batchnum><batchcount>1</batchcount>");
                        sb.append("<subbatchcount>1</subbatchcount><ctldate>2015-02-01</ctldate>");
                        sb.append("<senddate>2015-02-01 08:40:11</senddate><orgcode>FEWMS014</orgcode><line><materialno>T11401-0004972</materialno><materialname></materialname></line></header></data>]]>");
                sb.append("</inv:P_REQUEST_DATA></inv:InputParameters></soapenv:Body></soapenv:Envelope>");
        return sb.toString();
     }*/

    /**
     * wms做为客户端发送请求
     * @param wsdl
     * @param interfaceCode
     * @param xmlText
     * @param code
     * @return String
     * @throws Exception
     */
    public static String callWebService(String wsdl, String interfaceCode, String xmlText, String code) throws Exception {
        //连接超时时间
        System.setProperty("sun.net.client.defaultConnectTimeout", "120000");
        System.setProperty("sun.net.client.defaultReadTimeout", "1200000");

        String requestMessage = XmlToolsUtil.getRequsetXml(interfaceCode, xmlText);
        // URL连接
        URL url = new URL(wsdl);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Length", String.valueOf(requestMessage.getBytes().length));
        conn.setRequestProperty("Content-Type", "text/xml; charset=UTF-8");
        conn.setDoOutput(true);
        conn.setDoInput(true);
        // 请求输入内容
        OutputStream output = conn.getOutputStream();
        output.write(requestMessage.getBytes());
        output.flush();
        output.close();
        // 请求返回内容
        InputStreamReader isr = new InputStreamReader(HttpURLConnection.HTTP_OK == conn.getResponseCode() || HttpURLConnection.HTTP_CREATED == conn.getResponseCode()
                || HttpURLConnection.HTTP_ACCEPTED == conn.getResponseCode() ? conn.getInputStream() : conn.getErrorStream());
        BufferedReader br = new BufferedReader(isr);
        StringBuilder sb = new StringBuilder();
        String str;
        while ((str = br.readLine()) != null) {
            sb.append(str).append("\n");
        }
        br.close();
        isr.close();
        return sb.toString();
    }

    /**
     * 请求.NET服务（rfid桌面写卡器）
     * @param wsdl
     * @param xmlText
     * @param soapAction
     * @return String
     * @throws Exception
     */
    public static String callWebService(String wsdl, String xmlText, String soapAction)throws Exception {
        //连接超时时间
        System.setProperty("sun.net.client.defaultConnectTimeout", "120000");
        System.setProperty("sun.net.client.defaultReadTimeout", "1200000");

        String requestMessage = XmlToolsUtil.getNetRequsetXml(xmlText,soapAction);
        // URL连接
        URL url = new URL(wsdl);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Length", String.valueOf(requestMessage.getBytes().length));
        conn.setRequestProperty("Content-Type", "text/xml; charset=UTF-8");
        conn.setDoOutput(true);
        conn.setDoInput(true);
        // 请求输入内容
        OutputStream output = conn.getOutputStream();
        output.write(requestMessage.getBytes());
        output.flush();
        output.close();
        // 请求返回内容
        InputStreamReader isr = new InputStreamReader(HttpURLConnection.HTTP_OK == conn.getResponseCode() || HttpURLConnection.HTTP_CREATED == conn.getResponseCode()
                || HttpURLConnection.HTTP_ACCEPTED == conn.getResponseCode() ? conn.getInputStream() : conn.getErrorStream());
        BufferedReader br = new BufferedReader(isr);
        StringBuilder sb = new StringBuilder();
        String str ;
        while ((str = br.readLine()) != null) {
            sb.append(str).append("\n");
        }
        br.close();
        isr.close();
        return sb.toString();
    }


}
