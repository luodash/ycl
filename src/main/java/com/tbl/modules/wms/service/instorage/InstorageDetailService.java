package com.tbl.modules.wms.service.instorage;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.wms.entity.instorage.InstorageDetail;
import com.tbl.modules.wms.entity.inventory.InventoryRegistration;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * 入库详情服务类
 * @author 70486
 **/
public interface InstorageDetailService extends IService<InstorageDetail> {

    /**
     * 入库列表数据----获取列表数据
     * @param pageTbl
     * @param map
     * @return PageUtils
     */
    PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map);

    /**
     * 获取导出列
     * @param map
     * @return List<InstorageDetail>
     */
    List<InstorageDetail> getAllLists(Map<String,Object> map);

    /**
     * 导出Excel
     * @param response
     * @param path
     * @param list
     * @param excelIndexArray
     */
    void toExcel(HttpServletResponse response, String path, List<InstorageDetail> list,String [] excelIndexArray);
    
    /**
     * 查询人员正常入库量
     * @param date
     * @return List<InstorageDetail>
     */
    List<InstorageDetail> findList(java.util.Date date);
    
    /**
     * 入库----外采单获取列表数据
     * @param pageTbl
     * @param map
     * @return PageUtils
     */
    PageUtils getPagePackageList(PageTbl pageTbl, Map<String, Object> map);
    
    /**
	 * 桌面读卡器绑定列表查询
     * @param pageTbl 分页
	 * @param map 查询参数
	 * @return PageUtils
	 */
	PageUtils getList(PageTbl pageTbl, Map<String,Object> map);

    /**
     * 查询明细导出列（包含父表数据）
     * @param ids 明细选中主键
     * @param infoid 父表主键
     * @return InstorageDetail
     */
    List<InstorageDetail> selectInstorageAndDetail(List<Long> ids, String infoid);

    /**
     * 根据rfid批量修改标签初始化表
     * @param lstInventoryRegistration 所有的对应ebs盘点单号下的已经盘点的库存(要上传的部分)
     */
    void updateInstorageDetailByRfid(List<InventoryRegistration> lstInventoryRegistration);
}
