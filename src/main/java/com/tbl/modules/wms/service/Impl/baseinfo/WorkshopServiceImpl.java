package com.tbl.modules.wms.service.Impl.baseinfo;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.wms.dao.baseinfo.WorkshopDAO;
import com.tbl.modules.wms.entity.baseinfo.Workshop;
import com.tbl.modules.wms.service.baseinfo.WorkshopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 车间
 * @author 70486
 */
@Service
public class WorkshopServiceImpl extends ServiceImpl<WorkshopDAO, Workshop> implements WorkshopService {

    /**
     * 车间
     */
	@Autowired
	private WorkshopDAO workshopDAO;
	
	/**
     * 获取车间数据列表
     * @param pageTbl
     * @param map 条件
     * @return PageUtils
     */
    @Override
    public PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map) {
        String sortName = pageTbl.getSortname();
        String sortOrder = pageTbl.getSortorder();
        if (StringUtils.isEmptyString(sortName)) {
            sortName = "id";
        }
        if (StringUtils.isEmptyString(sortOrder)) {
            sortOrder = "desc";
        }
        Page page = new Page(pageTbl.getPageno(), pageTbl.getPagesize(), sortName, "asc".equalsIgnoreCase(sortOrder));
        return new PageUtils(page.setRecords(workshopDAO.getPageList(page, map)));
    }
    
    /**
     * 获取导出Excel列表
     * @param ids 多选条数
     * @param queryCode 车间编号
     * @param queryFactoryCode 厂区编码
     * @param factoryList 厂区列表
     * @return List<Workshop> 车间
     */
    @Override
    public List<Workshop> getExcelList(String ids, String queryCode, String queryFactoryCode, List<String> factoryList) {
        Map<String, Object> map = new HashMap<>(4);
        map.put("ids", StringUtils.stringToInt(ids));
    	map.put("code", queryCode);
    	map.put("factoryCode", queryFactoryCode);
        map.put("factoryList", factoryList);
        return null;
    }
}
