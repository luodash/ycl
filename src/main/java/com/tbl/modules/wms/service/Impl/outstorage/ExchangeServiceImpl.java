package com.tbl.modules.wms.service.Impl.outstorage;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.common.utils.DateUtils;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.wms.dao.outstorage.ExchangeDAO;
import com.tbl.modules.wms.entity.outstorage.ExchangePrint;
import com.tbl.modules.wms.entity.printinfo.PrintInfo;
import com.tbl.modules.wms.service.outstorage.ExchangeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 流转单打印
 * @author 70486
 */
@Service("exchangeService")
public class ExchangeServiceImpl  extends ServiceImpl<ExchangeDAO, PrintInfo> implements ExchangeService {

    /**
     * 流转单打印
     */
    @Autowired
    private  ExchangeDAO exchangeDAO;

    /**
     * 物资流转单列表页数据查询
     * @param pageTbl
     * @param map
     * @return PageUtils
     */
    @Override
    public PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map) {
        String sortName = pageTbl.getSortname();
        String sortOrder = pageTbl.getSortorder();
        if (StringUtils.isEmptyString(sortName)) {
            sortName = "odd_number";
        }
        if (StringUtils.isEmptyString(sortOrder)) {
            sortOrder = "desc";
        }
        Page page = new Page(pageTbl.getPageno(), pageTbl.getPagesize(), sortName, "asc".equalsIgnoreCase(sortOrder));

//        ExchangePrint exchangePrint = exchangeDAO.getInfo(map);
//        map.put("oddnumber",exchangePrint!=null ? exchangePrint.getOddnumber() : "");
        return new PageUtils(page.setRecords(exchangeDAO.getDataList(page,map)));
    }

    /**
     * 根据出库单获取车牌信息列表
     * @param outno 出库单号
     * @param sTime 起始时间
     * @param eTime 结束时间
     * @return List<Map<String, String>>
     */
    @Override
    public List<Map<String, String>> getCarList(String outno, String sTime, String eTime) {
        return exchangeDAO.getCarList(outno, sTime, eTime);
    }

    /**
     * 根据车牌和出库单信息判断是否已存在表中
     * @param exchangePrint
     * @return boolean
     */
    @Override
    public boolean isExistInfo(ExchangePrint exchangePrint) {
        return exchangeDAO.getCountByMap(exchangePrint)>0;
    }


    /**
     * 根据车牌及出库单验证是否存在该类数据
     * @param outno 出库单
     * @param carno 车牌
     * @return boolean
     */
    @Override
    public boolean validateTrue(String outno, String carno) {
        return exchangeDAO.validateTrue(outno,carno)>0;
    }

    /**
     * 获取厂区信息列表
     * @return Map<String, Object>
     */
    @Override
    public List<Map<String, Object>> getFactoryArea() {
        return exchangeDAO.getFactoryArea();
    }

    /**
     * 获取打印信息列表
     * @param map
     * @return Map<String,Object>
     */
    @Override
    public List<Map<String, Object>> getPrintInfo(Map<String,Object> map) {
        List<Map<String,Object>> resultPrintList = new ArrayList<>();
        map.put("detail",exchangeDAO.getPrintInfo(map));
        map.put("PRINTDATE", DateUtils.getTime());
        resultPrintList.add(map);
        return resultPrintList;
    }

    /**
     * 生成流转单号
     * @param outno 出库单号
     * @param carno 车牌号
     * @return String
     */
    @Override
    public String updateOddNumber(String outno,String carno){
        DecimalFormat df = new DecimalFormat("0000");
        String number;
        Integer oddnumber = exchangeDAO.getMaxOddNumber();
        if(oddnumber == null){
            number="WZLZ" + DateUtils.getDays() + "0001";
        }else{
            oddnumber += 1;
            number = "WZLZ" + DateUtils.getDays() + df.format(oddnumber);
        }

        exchangeDAO.updateOddNumber(outno, carno, number);
        return number;
    }

}
