package com.tbl.modules.wms.service.allo;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.modules.wms.entity.allo.AllocationDetail;

import java.util.List;
import java.util.Map;

/**
 * 调拨管理-调拨详情
 * @author 70486
 */
public interface AllocationDetailService extends IService<AllocationDetail> {

    /**
     * 导出调拨明细查看页Excel
     * @param map
     * @return AllocationDetail
     */
    List<AllocationDetail> getAlloStorageOutDetailExcel(Map<String, Object> map);

}
