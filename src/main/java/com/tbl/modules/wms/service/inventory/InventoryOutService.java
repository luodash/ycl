package com.tbl.modules.wms.service.inventory;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.modules.wms.entity.inventory.InventoryOut;

/**
 * 盘点出库审核服务类
 * @author 70486
 */
public interface InventoryOutService extends IService<InventoryOut> {
	
	
}
