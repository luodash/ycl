package com.tbl.modules.wms.service.businessutils;

/**
 * 业务流程公用服务
 * @author cxf
 */
public interface BusinessUtilsService {

    /**
     * 盘点米数审核
     */
    void inventoryMeterBusiness();

}
