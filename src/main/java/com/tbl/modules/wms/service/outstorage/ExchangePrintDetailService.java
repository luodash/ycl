package com.tbl.modules.wms.service.outstorage;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.modules.wms.entity.outstorage.ExchangePrintDetail;

/**
 * 物资流转单打印明细
 * @author wc
 */
public interface ExchangePrintDetailService extends IService<ExchangePrintDetail> {

    /**
     * 打印物资流转单据
     * @param carno 车牌号
     * @param sFactoryArea 原始厂区
     * @param eFactoryArea 目标厂区
     */
    void printWzlz(String carno, String sFactoryArea, String eFactoryArea);

}
