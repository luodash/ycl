package com.tbl.modules.wms.service.Impl.outstorage;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.google.common.collect.Maps;
import com.tbl.common.utils.DateUtils;
import com.tbl.common.utils.YamlConfigurerUtil;
import com.tbl.modules.wms.dao.baseinfo.FactoryAreaDAO;
import com.tbl.modules.wms.dao.outstorage.ExchangePrintDAO;
import com.tbl.modules.wms.dao.outstorage.ExchangePrintDetailDAO;
import com.tbl.modules.wms.entity.baseinfo.FactoryArea;
import com.tbl.modules.wms.entity.outstorage.ExchangePrint;
import com.tbl.modules.wms.service.Impl.webserviceImpl.WmsServiceImpl;
import com.tbl.modules.wms.service.outstorage.ExchangePrintService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 流转单打印
 * @author 70486
 */
@Service("ExchangePrintService")
public class ExchangePrintServiceImpl extends ServiceImpl<ExchangePrintDAO, ExchangePrint> implements ExchangePrintService {

    /**
     * 物资流转单打印
     */
    @Autowired
    private ExchangePrintDAO exchangePrintDAO;
    /**
     * 物资流转单明细
     */
    @Autowired
    private ExchangePrintDetailDAO exchangePrintDetailDAO;
    /**
     * 接口推送
     */
    @Autowired
    private WmsServiceImpl wmsServiceImpl;
    /**
     * 厂区
     */
    @Autowired
    private FactoryAreaDAO factoryAreaDAO;

    /**
     * 物资流转单补打
     * @param code 物资流转单号
     * @return boolean
     */
    @Override
    public boolean patchPrint(String code) {
        List<ExchangePrint> lstExchangePrint = exchangePrintDAO.selectList(new EntityWrapper<ExchangePrint>().eq("ODD_NUMBER", code));
        if (lstExchangePrint.size()==0){
            return false;
        }
        ExchangePrint exchangePrint = lstExchangePrint.get(0);
        Map<String,Object> map = new HashMap<>(7);
        Map<String,Object> reMap = new HashMap<>(1);
        Map<String,Object> batchesIdentityMap = Maps.newIdentityHashMap();
        String sfactoryName = exchangePrint.getSFactoryArea();
        String efactoryName = exchangePrint.getEFactoryArea();
        map.put("fromfactory", sfactoryName);
        map.put("tofactory", efactoryName);
        //打印日期
        map.put("printdate", DateUtils.getDay());
        //车牌号
        map.put("carno",exchangePrint.getCar());
        //物资流转单号
        map.put("oddno",code);
        //打印机printer
        map.put("printer", YamlConfigurerUtil.getStrYmlVal("yddl.printer_"+factoryAreaDAO.selectOne(new FactoryArea(){{
            setName(exchangePrint.getSFactoryArea());
        }}).getCode()));

        exchangePrintDetailDAO.selectByMap(new HashMap<String, Object>(1){{
            put("PID", exchangePrint.getId());
        }}).forEach(exchangePrintDetail -> batchesIdentityMap.put(new String("exchangeprintdetail"),new HashMap<String,Object>(11){{
            put("model",exchangePrintDetail.getModel());
            put("num",exchangePrintDetail.getNum());
            put("unit",exchangePrintDetail.getUnit());
            put("colour",exchangePrintDetail.getColor());
            put("shelfcode",exchangePrintDetail.getShelfcode());
            put("dishcode",exchangePrintDetail.getDishcode());
            put("segmentno",exchangePrintDetail.getShelfcode());
            put("qacode",exchangePrintDetail.getQacode());
            put("weight",exchangePrintDetail.getWeight());
            put("dingzhi",exchangePrintDetail.getDingzhi());
            put("remark",exchangePrintDetail.getRemark());
        }}));

        //流转单详情
        map.put("batchs",batchesIdentityMap);
        reMap.put("line",map);
        return wmsServiceImpl.FEWMS031(reMap);
    }

}
