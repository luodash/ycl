package com.tbl.modules.wms.service.Impl.productBind;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.common.utils.DateUtils;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.Query;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.platform.util.DeriveExcel;
import com.tbl.modules.wms.dao.productbind.DeviceUwbRouteDAO;
import com.tbl.modules.wms.entity.productbind.DeviceUwbRoute;
import com.tbl.modules.wms.service.productbind.DeviceUwbRouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 可视化行车轨迹
 * @author 70486
 */
@Service
public class DeviceUwbRouteServiceImpl extends ServiceImpl<DeviceUwbRouteDAO, DeviceUwbRoute> implements DeviceUwbRouteService {

    /**
     * 行车坐标信息
     */
    @Autowired
    private DeviceUwbRouteDAO deviceUwbRouteDAO;

    /**
     * 获取uwb维护列表的数据
     * @param  map
     * @return PageUtils
     */
    @Override
    public PageUtils queryPage(Map<String,Object> map){
        Page<DeviceUwbRoute> page = this.selectPage(new Query<DeviceUwbRoute>(map).getPage(), new EntityWrapper<DeviceUwbRoute>()
            .like(StringUtils.isNotBlank((String)map.get("name")), "TAG", (String)map.get("name")));
        return new PageUtils(page.setRecords(page.getRecords()));
    }


    /**
     * 获取导出列
     * @param ids 导出时是否勾选的选中的主键
     * @return List<DeviceUwbRoute> uwb导出列表
     */
    @Override
    public List<DeviceUwbRoute> getAllLists(String ids) {
        return StringUtils.isNotEmpty(ids)?deviceUwbRouteDAO.getAllLists(StringUtils.stringToList(ids)):deviceUwbRouteDAO.getAllLists(new ArrayList<>());
    }

    /**
     * 导出excel
     * @param response
     * @param path
     * @param list
     */
    @Override
    public void toExcel(HttpServletResponse response, String path, List<DeviceUwbRoute> list) {
        try {
            String sheetName = "UWB标签管理" + "(" + DateUtils.getDay() + ")";
            if (path != null && !"".equals(path)) {
                sheetName = sheetName + ".xls";
            } else {
                response.setHeader("Content-Type", "application/force-download");
                response.setHeader("Content-Type", "application/vnd.ms-excel");
                response.setCharacterEncoding("UTF-8");
                response.setHeader("Expires", "0");
                response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
                response.setHeader("Pragma", "public");
                response.setHeader("Content-disposition", "attachment;filename=" + new String(sheetName.getBytes("gbk"),
                        "ISO8859-1") + ".xls");
            }
            Map<String, String> mapFields = new LinkedHashMap<>();
            mapFields.put("tag", "标签号");
            mapFields.put("xsize", "XSIZE");
            mapFields.put("ysize", "YSIZE");
            mapFields.put("createtime", "创建时间");
            DeriveExcel.exportExcel(sheetName, list, mapFields, response, path);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
