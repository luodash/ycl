package com.tbl.modules.wms.service.Impl.baseinfo;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.wms.dao.baseinfo.FactoryAreaDAO;
import com.tbl.modules.wms.entity.baseinfo.FactoryArea;
import com.tbl.modules.wms.service.baseinfo.FactoryAreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 厂区实现类
 * @author 70486
 */
@Service
public class FactoryAreaServiceImpl extends ServiceImpl<FactoryAreaDAO, FactoryArea> implements FactoryAreaService {

    /**
     * 厂区
     */
    @Autowired
    private FactoryAreaDAO factoryAreaDAO;

    /**
     * 获取厂区信息列表
     * @param pageTbl
     * @param map 条件
     * @return PageUtils
     */
    @Override
    public PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map) {
    	if(map.get("name")!=null) {
    		map.put("name", map.get("name").toString().toUpperCase());
        }
        String sortName = pageTbl.getSortname();
        String sortOrder = pageTbl.getSortorder();
        if (StringUtils.isEmptyString(sortName)) {
            sortName = "id";
        }
        if (StringUtils.isEmptyString(sortOrder)) {
            sortOrder = "desc";
        }
        Page page = new Page(pageTbl.getPageno(), pageTbl.getPagesize(), sortName, "asc".equalsIgnoreCase(sortOrder));
        return new PageUtils(page.setRecords(factoryAreaDAO.getPageList(page, map)));
    }

    /**
     * 获得Excel导出列表
     * @param ids 厂区主键
     * @param name 厂区名称
     * @param code 厂区编码
     * @param factoryList 所属厂区
     * @return List<FactoryArea> 厂区列表
     */
    @Override
    public List<FactoryArea> getExcelList(String ids, String name, String code, List<Long> factoryList) {
        Map<String, Object> map = new HashMap<>(4);
        map.put("ids", StringUtils.stringToInt(ids));
        map.put("name", name);
        map.put("code", code);
        map.put("factoryList", factoryList);
        return factoryAreaDAO.getExcelList(map);
    }

    /**
     * 获取厂区全部下拉列表
     * @param map
     * @return List<Map<String,Object>>
     */
    @Override
    public List<Map<String, Object>> getFactoryList(Map<String, Object> map) {
        return factoryAreaDAO.selectFactoryList(map);
    }

    /**
     * 获取厂区信息
     * @param listArr 厂区编码集合
     * @return Map<String,Object>
     */
    @Override
    public Map<String,Object> getFactoryInfo(List<String> listArr) {
        List<Map<String, Object>> list = factoryAreaDAO.getFactoryInfo(listArr);
        Map<String,Object> map1=new HashMap<>(2);
        StringBuilder code= new StringBuilder();
        StringBuilder id= new StringBuilder();
        for (Map<String, Object> map : list) {
            if (map != null) {
                code.append(map.get("CODE")).append(",");
                id.append(map.get("ID")).append(",");
            }
        }
        if(code.length()>0){
            code = new StringBuilder(code.substring(0, code.length() - 1));
        }

        if(id.length()>0){
            id = new StringBuilder(id.substring(0, id.length() - 1));
        }

        map1.put("id", id.toString());
        map1.put("code", code.toString());
        return map1;
    }

    /**
     * 功能描述：获取厂区的所有信息
     * @return List<Map<String, Object>>
     */
    @Override
    public List<Map<String, Object>> getFactoryList() {
        return factoryAreaDAO.selectLists();
    }
    
    /**
     * 功能描述：清空厂区流水号，每日清空
     */
    @Override
    public void updateSerialNum() {
    	factoryAreaDAO.updateSerialNum();
    }
    
    /**
     * 功能描述：清空调拨、移库单流水号，每日清空
     */
    @Override
    public void updateAlloMoveSerialNum() {
    	factoryAreaDAO.updateAlloMoveSerialNum();
    }
    
    /**
     * 功能描述：清空盘点单流水号，每月清空
     */
    @Override
    public void updateInventoryNum() {
    	factoryAreaDAO.updateInventoryNum();
    }
}
