package com.tbl.modules.wms.service.baseinfo;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.modules.wms.entity.baseinfo.BaseDicType;

import java.util.List;
import java.util.Map;

/**
 * 下拉框选择项Service
 * @author 70486
 */
public interface BaseDicTypeService extends IService<BaseDicType> {

    /**
     * 功能描述：获取入库状态列表
     * @param map
     * @return List<Map<String,Object>>
     */
    List<Map<String, Object>> getInstorageTypeList(Map<String, Object> map);

    /**
     * 功能描述：获取时间类型列表
     * @param map
     * @return List<Map<String,Object>>
     */
    List<Map<String, Object>> getTimeTypeList(Map<String, Object> map);

    /**
     * 功能描述：获取出库类型列表
     * @param map
     * @return List<Map<String,Object>>
     */
    List<Map<String, Object>> getOutStorageTypeList(Map<String, Object> map);

    /**
     * 功能描述：打印列表
     * @param map
     * @return List<Map<String,Object>>
     */
    List<Map<String, Object>> getPrinterHierarchyList(Map<String, Object> map);


    /**
     * 获取质检状态下拉列表
     * @return
     */
    Map<String,Object> getQcDict();

    Map<String,Object> getOtherTransactionDict(Integer type);

    Map<String,Object> getLyytDict(Integer id);
}
