package com.tbl.modules.wms.service.pda;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.modules.wms.constant.PdaResult;
import org.apache.poi.ss.formula.functions.T;

import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * 手持机接口
 * @author 70486
 */
public interface PdaService extends IService<T> {

	/**
     * 用户登陆
     * @param username 用户名
     * @param password 密码
     * @param session
     * @return PdaResult
     */
    PdaResult login(String username, String password, HttpSession session);

    /**
     * 模块：入库管理
     * 包:获取物料详情列表
     * @param rfid rfid
     * @param userId 用户主键
     * @param barcode 二维码
     * @param barcodeType 二维码类型
     * @param dictTypeNum 台账类型
     * @return PdaResult 详情表列表 状态为未确认的
     */
    PdaResult storageinPackingDetailList(String rfid, Long userId, String barcode, String barcodeType, String dictTypeNum);

    /**
     * 模块：入库管理
     * 功能描述: 装包:点击确认
     * 入参：rfid
     * 返回：详情表列表
     * @param queryMap rfid
     * @return PdaResult
     */
    PdaResult storageinPackingSubmit(Map<String, Object> queryMap);

    /**
     * 模块：入库管理
     * 功能描述: 入库:点击开始入库,返回仓库列表
     * 入参：page,size,factoryCode,code
     * 返回：仓库列表
     * @param page
     * @param size
     * @param factoryCode
     * @param code
     * @return PdaResult
     */
    PdaResult storageinStorageinStart(String page,String size,String factoryCode,String code);

    /**
     * 模块：入库管理
     * 功能描述: 入库:选择闲置的行车
     * 入参：warehouseCode(仓库编码);factoryCode(厂区编码)
     * 返回：行车列表
     * @param warehouseCode
     * @param factoryCode
     * @return PdaResult
     */
    PdaResult storageinStorageinCar(String warehouseCode,String factoryCode);
    
    /**
     * 模块：入库管理
     * 功能描述: 入库:扫描
     * 入参：warehouseCode(仓库编码),rfid,factoryCode(厂区编码),barcode,
     * barcodeType,carCode(行车)，isCertificate(是否是合格证扫描：0不是。1是), userId 操作人
     * 返回：推荐库位，物料详情
     * @param warehouseCode
     * @param rfid
     * @param factoryCode
     * @param barcode
     * @param barcodeType
     * @param carCode
     * @param isCertificate
     * @param userId
     * @return PdaResult
     */
    PdaResult storageinStorageinScan(String warehouseCode, String rfid, String factoryCode, String barcode, String barcodeType, String carCode,
                                     String isCertificate, Long userId);
    
    /**
     * 模块：入库管理
     * 功能描述: 入库:再次扫描（点击开始入库后，操作员发现入错了，应该时入另一盘）
     * 入参：warehouseCode(仓库编码),rfid,factoryCode(厂区编码),originalRfid,
     * barcode,barcodeType,carCode(行车),isCertificate(是否是合格证扫描：0不是。1是), userId 操作人
     * 返回：推荐库位，物料详情
     * @param warehouseCode
     * @param rfid
     * @param factoryCode
     * @param originalRfid
     * @param barcode
     * @param barcodeType
     * @param carCode
     * @param isCertificate
     * @param userId
     * @return PdaResult
     */
    PdaResult againStorageinScan(String warehouseCode, String rfid, String factoryCode, String originalRfid, String barcode, String barcodeType,
                                 String carCode, String isCertificate, Long userId);

    /**
     * 模块：入库管理
     * 功能描述: 入库:选择仓库，行车之后提交
     * 入参：rfid,shelfCode(库位编码),warehouseCode(仓库编码),carCode(行车编码),factoryCode(厂区编码)
     * 返回：PdaResult
     * @param rfid
     * @param shelfCode
     * @param warehouseCode
     * @param carCode
     * @param factoryCode
     * @return PdaResult
     */
    PdaResult storageinStorageinSubmit(String rfid, String shelfCode, String warehouseCode, String carCode, String factoryCode);
    
    /**
     * 模块：入库管理
     * 功能描述: 点击开始入库后，完成入库之前，用户修改推荐库位，修改后同步到pad
     * 入参：shelfCode(库位编码),carCode(行车编码)
     * 返回：PdaResult
     * @param shelfCode
     * @param carCode
     * @return PdaResult
     */
    PdaResult recommendShelfSubmit(String shelfCode, String carCode) ;

    /**
     * 模块：入库管理
     * 功能描述: 完成入库时，根据入库事务日期，判断是否需要弹框提示确认
     * 入参：date(事务时间)，rfid
     * 返回：PdaResult
     * @param date
     * @param rfid
     * @return PdaResult
     */
    PdaResult storageinTiemCheck(String date, String rfid);

    /**
     * 模块：入库管理
     * 功能描述: 入库完成
     * 入参：rfid,shelfCode(库位编码),userId(用户id),date(日期),isCertificate(是否是合格证扫描：0不是。1是),factoryCode(厂区)，warehouseCode（仓库）
     * 返回：PdaResult
     * @param rfid
     * @param shelfCode
     * @param userId
     * @param date
     * @param isCertificate
     * @param factoryCode
     * @param warehouseCode
     * @return PdaResult
     */
    PdaResult storageinStorageinSuccess(String rfid, String shelfCode, Long userId, String date, String isCertificate, String factoryCode,
                                        String warehouseCode);

    /**
     * 模块：入库管理
     * 功能描述: 获取指定厂区仓库下指定仓库区域内的可用库位
     * 入参：warehouseCode(仓库编码);factoryCode(厂区编码),page,size,code,carCode(行车)
     * 返回：库位列表
     * @param warehouseCode
     * @param factoryCode
     * @param page
     * @param size
     * @param code
     * @param carCode
     * @return PdaResult
     */
    PdaResult storageinEmptyShelf(String warehouseCode, String factoryCode, int page, int size, String code, String carCode);

    /**
     * 模块：入库管理
     * 功能描述: 获取指定厂区仓库下的可用库位
     * 入参：warehouseCode(仓库编码);factoryCode(厂区编码),page,size,code
     * 返回：库位列表
     * @param warehouseCode
     * @param factoryCode
     * @param page
     * @param size
     * @param code
     * @return PdaResult
     */
    PdaResult selectNoCarShelf(String warehouseCode, String factoryCode, int page, int size, String code);
    
    /**
     * 模块：入库管理
     * 功能描述: 完成入库真实库位选择
     * 入参：pageNo,size,code(库位模糊查询输入),carCode(行车)
     * 返回：行车所在仓库区域的库位列表
     * @param pageNo
     * @param size
     * @param code
     * @param carCode
     * @return PdaResult
     */
    PdaResult getRealArroundShelf(int pageNo, int size, String code, String carCode) ;
    
    /**
     * 模块：出库管理
     * 功能描述: 出库:选择闲置和出库中的行车
     * 入参：warehouseCode(仓库编码);factoryCode(厂区编码)
     * 返回：行车列表
     * @param warehouseCode
     * @param factoryCode
     * @return PdaResult
     */
    PdaResult storageoutStorageoutCar(String warehouseCode, String factoryCode);
    
    /**
     * 模块：出库管理
     * 功能描述: 发货：扫描发货单
     * 入参：shipNo(发货单号),warehouseCode(仓库),userId,factoryCode(厂区),carCode(行车)
     * 返回：发货明细列表,发货单号,营销经理
     * @param shipNo
     * @param warehouseCode
     * @param userId
     * @param factoryCode
     * @param carCode
     * @return PdaResult
     */
    PdaResult storageoutDetailList(String shipNo, String warehouseCode, Long userId, String factoryCode, String carCode);

    /**
     * 模块：出库管理
     * 功能描述: 扫描获取出库单
     * 入参：shipNo(发货单号),factoryCode(厂区),warehouseCode(仓库),carCode(航车)
     * 返回：发货单信息
     * @param shipNo
     * @param factoryCode
     * @param warehouseCode
     * @param carCode
     * @return PdaResult
     */
    PdaResult findOutstorageByScan(String shipNo,String factoryCode,String warehouseCode,String carCode) ;

    /**
     * 模块：出库管理
     * 功能描述: 发货：点击,更改状态拣选中  (1:生效  2:拣选中  3:拣选完成 4:发运完成)
     * 入参：rfid,carCode,userId,shipNo,barcode,barcodeType
     * 返回：PdaResult
     * @param rfid
     * @param carCode
     * @param userId
     * @param shipNo
     * @param barcode
     * @param barcodeType
     * @return PdaResult
     */
    PdaResult storageoutDetailClick1(String rfid, String carCode, Long userId, String shipNo, String barcode, String barcodeType);

    /**
     * 模块：出库管理
     * 功能描述: 排序界面进入单条明细界面扫描物料rfid=开始出库,更改状态拣选中  (1:生效  2:拣选中  3:拣选完成  4:预出库)
     * 入参：rfid,carCode,userId,shipNo,barcode,barcodeType,instorageDetailId库存表主键
     * 返回：PdaResult
     * @param rfid
     * @param carCode
     * @param userId
     * @param shipNo
     * @param barcode
     * @param barcodeType
     * @param instorageDetailId
     * @return PdaResult
     */
    PdaResult storageoutSortScan(String rfid, String carCode, Long userId, String shipNo, String barcode, String barcodeType,
                                 Long instorageDetailId);

    /**
     * 发货：点击,更改状态拣选完成  (1:生效  2:拣货中  3:发运完成)
     * @param qaCode 质保号
     * @param userId 用户
     * @param type 1全部，2按米
     * @param shipNo 提货单号
     * @return PdaResult
     */
    PdaResult storageoutDetailClick2(String qaCode,Long userId,String type,String shipNo);

    /**
     * 发货：点击完成出库,更改状态为出库完成(1:生效  2:拣货中  3:出库完成  4预出库)
     * @param qaCode 质检单号
     * @param userId 用户主键
     * @param type [废弃]（出库类型：1全部出库，2按米出库）
     * @param shipNo 提货单号
     * @return PdaResult
     */
    PdaResult storageoutConfirm(String qaCode,Long userId,String type,String shipNo);

    /**
     * 模块：出库管理
     * 划单
     * 功能描述: 撤销出库扫描提货单
     * 入参：shipNo
     * 返回：PdaResult
     * @param shipNo
     * @return PdaResult
     */
    PdaResult revokeOustroageScan(String shipNo);

    /**
     * 模块：出库管理
     * 划单
     * 功能描述: 撤销出库扫描物料
     * 入参：rfid,barcode,barcodeType,shipNo
     * 返回：PdaResult
     * @param rfid
     * @param barcode
     * @param barcodeType
     * @param shipNo
     * @return PdaResult
     */
    PdaResult revokeOustroageDetailScan(String rfid, String barcode, String barcodeType, String shipNo);

    /**
     * 模块：出库管理
     * 划单
     * 功能描述: 撤销出库确认
     * 入参：map
     * 返回：PdaResult
     * @param map
     * @return PdaResult
     */
    PdaResult revokeOustroageDetailConfirm(Map<String,Object> map);

    /**
     * 模块：出库管理
     * 功能描述: 出库-完成出库窗口中取消出库
     * 入参：qacode,shipNo
     * 返回：PdaResult
     * @param qaCode
     * @param shipNo
     * @return PdaResult
     */
    PdaResult cancelOutStorage(String qaCode, String shipNo);
    
    /**
     * 模块：出库管理
     * 完成出库-判断发货单米数与库存不相符，选择了正常出库，提示“库存米数xx，发货米数xx，是否整盘出？” 点是就整盘出，点否就重新看一下。
     * 入参：qaCode,type（出库类型：1全部出库，2按米出库），shipNo提货单号
     * 返回：PdaResult
     * @param qaCode
     * @param type
     * @param shipNo
     * @return PdaResult
     */
    PdaResult judgeOutmeter(String qaCode,String type,String shipNo) ;

    /**
     * 模块：出库管理
     * 根据输入的车牌号获取发货单
     * 入参：carNumber 车牌号,factoryCode 厂区,warehouseCode 仓库,carCode 航车
     * 返回：List<Map<String,Object>> 发货单号集合
     * @param carNumber
     * @param factoryCode
     * @param warehouseCode
     * @param carCode
     * @return PdaResult
     */
    PdaResult getOutstorageListByCarNumber(String carNumber,String factoryCode,String warehouseCode,String carCode);

    /**
     * 模块：出库管理
     * 根据发货单号获取已经完成的和所有的明细的数量
     * @param shipnoJsonString 发货单
     * @param factoryCode 厂区
     * @param warehouseCode 仓库
     * @param carCode 航车
     * @return Map<String,Object>
     */
    PdaResult getNumberByShipNo(String shipnoJsonString,String factoryCode,String warehouseCode,String carCode);

    /**
     * 模块：出库管理
     * 发货主界面排序按钮
     * @param shipnoJsonString 发货单
     * @param factoryCode 厂区编码
     * @param warehouseCode 仓库编码
     * @param carCode 航车编码
     * @param carNo 车票号
     * @return Map<String,Object>
     */
    PdaResult sortOutstorageDetail(String shipnoJsonString,String factoryCode,String warehouseCode,String carCode,String carNo);

    /**
     * 模块：出库管理
     * 发货主界面排序后点击单一详情
     * @param instorageDetailId 标签初始化标主键
     * @param shipNo 提货单号
     * @return Map<String,Object>
     */
    PdaResult clickSoloOutStorageDetail( Long instorageDetailId, String shipNo);

    /**
     * 模块：出库管理
     * 发货主界面排序按钮
     * @param shipnoJsonString 发货单
     * @param carNo 车牌号
     * @param factoryCode 厂区
     * @param warehouseCode 仓库
     * @param carCode 行车
     * @return Map<String,Object>
     */
    PdaResult refreshOutstorage(String shipnoJsonString,  String carNo, String factoryCode,  String warehouseCode, String carCode);

    /**
     * 模块：出库管理
     * 物资流转单打印
     * @param carNo 车牌号
     * @param factoryCode 厂区
     * @param transferFactoryCode 流转厂
     * @return Map<String,Object>
     */
    PdaResult wzlzPrint(String carNo, String factoryCode, String transferFactoryCode);

    /**
     * 模块：调拨
     * 功能描述:
     * 入参：rfid,userId,barcode,barcodeType
     * 返回：库存列表
     * @param rfid
     * @param userId
     * @param barcode
     * @param barcodeType
     * @return PdaResult
     */
    PdaResult alloStorageInfoList(String rfid, Long userId, String barcode, String barcodeType);

    /**
     * 模块：调拨
     * 功能描述:点击提交，生成调拨单
     * 入参：@RequestBody Map<String,Object> queryMap
     * inWarehouseCode(调入仓库code),outWarehouseCode(调出仓库code),storageInfoRfids（库存列表id）,userId(用户id)
     * 返回：PdaResult
     * @param map
     * @return PdaResult
     */
    PdaResult alloAdd(Map<String, Object> map);

    /**
     * 模块：调拨
     * 功能描述:返回所有未完成的主表信息
     * 入参：Long userid
     * 返回：调拨单主表列表
     * @param userId
     * @return PdaResult
     */
    PdaResult alloSelectAllUndo(Long userId);

    /**
     * 模块：调拨
     * 功能描述:返回所有未完成调拨出库的主表信息
     * 入参：Long userid
     * 返回：调拨单主表列表
     * @param userId
     * @return PdaResult
     */
    PdaResult alloSelectPartUndo(Long userId);

    /**
     * 模块：调拨
     * 功能描述:调拨功能模块进入，展示调拨单列表
     * 入参：主表id
     * 返回：详情列表
     * @param id
     * @return PdaResult
     */
    PdaResult alloSelectDetailById(Long id);
    
    /**
     * 模块：调拨
     * 功能描述:调拨功能模块进入，展示调拨出库单列表
     * 入参：主表id
     * 返回：详情列表
     * @param id
     * @return PdaResult
     */
    PdaResult alloOutSelectDetailById(Long id);
    
    /**
     * 模块：调拨
     * 功能描述: 调拨出库，进入调拨出库单展示列表后，选择行车，把在该调拨单下并且属于该行车场景范围内的任务信息同步给行车
     * 入参：主表id，行车编码
     * 返回：详情列表
     * @param id
     * @param carCode
     * @return PdaResult
     */
    PdaResult alloMissionToCar(Long id,String carCode);
    /**
     * 模块：调拨
     * 功能描述:调拨功能模块进入，展示调拨入库单列表
     * 入参：主表id
     * 返回：详情列表
     * @param id
     * @return PdaResult
     */
    PdaResult alloInSelectDetailById(Long id);

    /**
     * 模块：调拨
     * 功能描述:删除主表信息，子表信息  需要判断该主表下有无已经开始的字表信息，如有则提示  库存表更改状态
     * 入参：主表id
     * 返回：PdaResult
     * @param id
     * @return PdaResult
     */
    PdaResult alloDelInfoAndDetail(Long id);

    /**
     * 模块：调拨
     * 功能描述:删除子表信息  判断该字表的状态  更改库存表的状态
     * 入参：主表id
     * 返回：PdaResult
     * @param id
     * @return PdaResult
     */
    PdaResult alloDelDetail(Long id);

    /**
     * 模块：调拨出库
     * 功能描述:点击，开始出库  将状态改为开始出库
     * 状态 1:未开始 2.开始调拨出库 3.完成调拨出库 4.开始调拨入库 5.完成调拨入库
     * 入参：rfid,carCode,barcode,barcodeType
     * 返回：PdaResult
     * @param rfid
     * @param carCode
     * @param barcode
     * @param barcodeType
     * @return PdaResult
     */
    PdaResult alloOutStorageStart(String rfid, String carCode, String barcode, String barcodeType);

    /**
     * 模块：调拨出库
     * 功能描述:点击，完成出库
     * 1.需要修改状态 2.需要增加调拨出库变动表信息  3.需要在库存里面将原有rfid的数据删除
     * 入参：rfid,userId
     * 返回：PdaResult
     * @param rfid
     * @param userId
     * @return PdaResult
     */
    PdaResult alloOutStorageSuccess(String rfid, Long userId);

    /**
     * 模块：调拨入库
     * 功能描述:显示所有待入库的调拨单
     * 返回：PdaResult
     * @param userId
     * @return PdaResult
     */
    PdaResult pendingAlloStorageList(Long userId);
    
    /**
     * 模块：调拨入库
     * 功能描述:扫描rfid，返回调拨出库完成的详情表信息
     * 入参：rfid,barcode,barcodeType,carCode
     * 返回：PdaResult
     * @param rfid
     * @param barcode
     * @param barcodeType
     * @param carCode
     * @return PdaResult
     */
    PdaResult alloInStorageList(String rfid, String barcode, String barcodeType, String carCode);

    /**
     * 模块：调拨入库
     * 功能描述:点击开始，修改状态为开始调拨入库
     * 入参：rfid,shelfCode,carCode,warehouseCode
     * 返回：PdaResult
     * @param rfid
     * @param shelfCode
     * @param carCode
     * @param warehouseCode
     * @return PdaResult
     */
    PdaResult alloInStorageStart(String rfid, String shelfCode, String carCode, String warehouseCode);

    /**
     * 模块：调拨入库
     * 功能描述:结束,状态改为调拨入库
     * 入参：rfid,userId,shelfCode(最终库位)
     * 返回：PdaResult
     * @param rfid
     * @param userId
     * @param shelfCode
     * @return PdaResult
     */
    PdaResult alloInStorageSuccess(String rfid, Long userId, String shelfCode);

    /**
     * 模块：移库
     * 功能描述:
     * 入参：rfid,userId,barcode,barcodeType
     * 返回：库存列表
     * @param rfid
     * @param userId
     * @param barcode
     * @param barcodeType
     * @return PdaResult
     */
    PdaResult moveStorageInfoList(String rfid, Long userId, String barcode, String barcodeType);

    /**
     * 模块：移库
     * 功能描述:点击提交，生成移库单
     * 入参：@RequestBody Map<String,Object> queryMap
     * storageInfoRfids（库存列表id）,userId(用户id)
     * 返回：PdaResult
     * @param map
     * @return PdaResult
     */
    PdaResult moveAdd(Map<String, Object> map);

    /**
     * 模块：移库
     * 功能描述:返回所有未完成的主表信息
     * 入参：Long userId
     * 返回：移库单主表列表
     * @param userId
     * @return PdaResult
     */
    PdaResult moveSelectAllUndo(Long userId);

    /**
     * 模块：移库
     * 功能描述:返回所有未完成移库出库的主表信息
     * 入参：Long userId
     * 返回：移库单主表列表
     * @param userId
     * @return PdaResult
     */
    PdaResult moveSelectPartUndo(Long userId);

    /**
     * 模块：移库
     * 功能描述: 移库功能模块进入，展示移库单列表
     * 入参：主表id
     * 返回：详情列表
     * @param id
     * @return PdaResult
     */
    PdaResult moveSelectDetailById(Long id);
    
    /**
     * 模块：移库
     * 功能描述: 移库入库展示明细
     * 入参：主表id
     * 返回：详情列表
     * @param id
     * @return PdaResult
     */
    PdaResult moveInSelectDetailById(Long id);
    
    /**
     * 模块：移库
     * 功能描述: 移库出库展示明细
     * 入参：主表id
     * 返回：详情列表
     * @param id
     * @return PdaResult
     */
    PdaResult moveOutSelectDetailById(Long id);
    
    /**
     * 模块：移库
     * 功能描述: 移库出库，进入移库出库单展示列表后，选择行车，把在该移库单下并且属于该行车场景范围内的任务信息同步给行车
     * 入参：主表id，行车编码
     * 返回：详情列表
     * @param id
     * @param carCode
     * @return PdaResult
     */
    PdaResult moveMissionToCar(Long id,String carCode);

    /**
     * 模块：移库
     * 功能描述:删除主表信息，子表信息  需要判断该主表下有无已经开始的字表信息，如有则提示  更新库存状态
     * 入参：主表id
     * 返回：PdaResult
     * @param id
     * @return PdaResult
     */
    PdaResult moveDelInfoAndDetail(Long id);

    /**
     * 模块：移库
     * 功能描述:删除子表信息  判断该字表的状态
     * 入参：主表id
     * 返回：PdaResult
     * @param id
     * @return PdaResult
     */
    PdaResult moveDelDetail(Long id);

    /**
     * 模块：移库出库
     * 功能描述:点击，开始出库  将状态改为开始出库
     * 状态 1:未开始 2.开始移库出库 3.完成移库出库 4.开始移库入库 5.完成移库入库
     * 入参：rfid,carCode,barcode,barcodeType
     * 返回：PdaResult
     * @param rfid
     * @param carCode
     * @param barcode
     * @param barcodeType
     * @return PdaResult
     */
    PdaResult moveOutStorageStart(String rfid, String carCode, String barcode, String barcodeType);

    /**
     * 模块：移库出库
     * 功能描述:点击，完成出库
     * 1.需要修改状态 2.需要增加移库出库变动表信息  3.需要在库存里面将原有rfid的数据删除
     * 入参：rfid,userId
     * 返回：PdaResult
     * @param rfid
     * @param userId
     * @return PdaResult
     */
    PdaResult moveOutStorageSuccess(String rfid, Long userId);

    /**
     * 模块：移库入库
     * 功能描述:显示所有待入库的移库单
     * 返回：PdaResult
     * @param userId
     * @return PdaResult
     */
    PdaResult pendingMoveStorageList(Long userId);
    
    /**
     * 模块：移库入库
     * 功能描述:扫描rfid，返回移库出库完成的详情表信息
     * 入参：rfid,barcode,barcodeType,carCode
     * 返回：PdaResult
     * @param rfid
     * @param barcode
     * @param barcodeType
     * @param carCode
     * @return PdaResult
     */
    PdaResult moveInStorageList(String rfid,String barcode,String barcodeType,String carCode);

    /**
     * 模块：移库入库
     * 功能描述:点击开始，修改状态为开始移库入库
     * 入参：rfid,shelfCode,carCode,warehouseCode
     * 返回：PdaResult
     * @param rfid
     * @param shelfCode
     * @param carCode
     * @param warehouseCode
     * @return PdaResult
     */
    PdaResult moveInStorageStart(String rfid, String shelfCode, String carCode, String warehouseCode);

    /**
     * 模块：移库入库
     * 功能描述:结束,状态改为移库入库
     * 入参：rfid,userId,shelfCode(最终库位)
     * 返回：PdaResult
     * @param rfid
     * @param userId
     * @param shelfCode
     * @return PdaResult
     */
    PdaResult moveInStorageSuccess(String rfid, Long userId, String shelfCode);

    /**
     * 模块：移库主界面扫码
     * 功能描述:移库单主界面支持直接扫码，扫码分三种情况
     *   1.检索后没有一样的质保号，那么跳入新增的提交界面，要多一个行车的选择，可选可不选
     *   2.检索后有一样的质保号，并且该质保号已经完成了移库出库，那么就直接跳入入库界面，移库入库界面行车可选可不选
     *   3.检索后有一样的质保号，但是该质保号没有完成移库出库，那么提示“未出库，请先确认出库！”
     * 入参：rfid,barcode,barcodeType,userId
     * 返回：PdaResult
     * @param rfid
     * @param barcode
     * @param barcodeType
     * @param userId
     * @return PdaResult
     */
    PdaResult moveStorageScan(String rfid,String barcode,String barcodeType,Long userId);

    /**
     * 盘点修改库位界面中获取所有的库位
     * @param code 模糊查询条件
     * @param pageNo
     * @param size
     * @return Shelf 库位
     */
    PdaResult getInventoryShelf(String code,int pageNo,int size);

    /**
     * 模块：盘点
     * 功能描述:返回所有未审核已提交的盘点任务信息
     * @param userId
     * @return PdaResult
     */
    PdaResult inventoryPlanSelect(Long userId);

    /**
     * 模块：盘点
     * 功能描述:解析提交的盘点信息，再存入明细表
     * （相同的人的一样的任务ID不用重复保存）
     * @param queryMap
     * @return PdaResult
     */
    PdaResult inventoryPlanAdd(Map<String, Object> queryMap);

    /**
     * 模块：拆分
     * 功能描述：拆分提交接口
     * @param queryMap
     * @return PdaResult
     */
    PdaResult splitSubmit(Map<String, Object> queryMap);

    /**
     * 模块：拆分
     * 功能描述：重新绑定RFID 绑定表
     * @param qaCode  质保单号
     * @return PdaResult
     */
    PdaResult reSetrRfidBind(String qaCode);

    /**
     * 模块：拆分
     * 功能描述：获取拆分主表信息
     * @return PdaResult
     */
    PdaResult splitTypeIsZero();

    /**
     * 模块：拆分
     * 功能描述:根据id查询拆分入库表信息
     * 入参：主表id
     * 返回：PdaResult
     * @param pid
     * @return PdaResult
     */
    PdaResult splitBelong(String pid);

    /**
     * 返回盘信息列表
     * @param code
     * @param pageNo
     * @param pageSize
     * @return PdaResult
     */
    PdaResult getDishList(String code,int pageNo,int pageSize);
    
    /**
     * 根据rfid修改状态
     * @param rfid
     * @return PdaResult
     */
    PdaResult updateRfidType(String rfid);

    /**
     * 模块：可视化管理
     * 功能描述: 获取UWB所在库位
     * 入参：carCode行车编码
     * 返回：定位库位
     * @param carCode
     * @return PdaResult
     */
    PdaResult getUwbShelf(String carCode);
    
    /**
     * 盘点扫描时获取物料得详情
     * @param salesname
     * @param warehouse
     * @param rfid
     * @param barcode
     * @param barcodeType
     * @param userId
     * @param ebsCode
     * @return StorageInfo
     */
    PdaResult getMaterialInfo(String salesname, String warehouse, String rfid, String barcode, String barcodeType, String userId, String ebsCode);
    
    /**
     * 根据用户获取厂区
     * @param userId
     * @return List<FactoryArea>
     */
    PdaResult getFactorys(String userId);
    
    /**
     * 根据用户获取厂区
     * @param rfid
     * @param barcode
     * @param barcodeType
     * @return PdaResult
     */
    PdaResult findInfoByRfid(String rfid,String barcode,String barcodeType);

    /**
     * 扫描获取库存物料详情
     * @param rfid
     * @param barcode 批次号/质保号
     * @param barcodeType 扫描类型
     * @return StorageInfo 库存物料
     */
    PdaResult getStorageInfoDetail(String rfid,String barcode,String barcodeType);

    /**
     * 物资流转单补打界面查询
     * @param circulationNo 流转单单号
     * @param carNo 车牌号
     * @param fromFactory 出发厂
     * @param toFactory 目的厂
     * @param page 页码
     * @param size 每页数量
     * @return List<ExchangePrint>
     */
    PdaResult selectWzlzPrintPage(String circulationNo,String carNo,String fromFactory,String toFactory,String page,String size);

    /**
     * 物资流转单补打打印
     * @param circulationNo 流转单单号
     * @return PdaResult
     */
    PdaResult patchPrint(String circulationNo);

    /**
     * 获取手持机最新版本号，url地址
     * @return Map<String,Object>
     */
    Map<String,Object> findPdaConfig();
}
