package com.tbl.modules.wms.service.Impl.pda;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.wms.dao.pda.SplitDAO;
import com.tbl.modules.wms.entity.split.Split;
import com.tbl.modules.wms.entity.storageinfo.StorageInfo;
import com.tbl.modules.wms.service.pda.SplitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 拆分接口实现
 * @author 70486
 */
@Service
public class SplitServiceImpl extends ServiceImpl<SplitDAO, Split> implements SplitService {

    /**
     * 拆分持久层
     */
    @Autowired
    private SplitDAO splitDAO;

    /**
     * 批量更新拆分表库位
     * @param lstStorageInfo
     */
    @Override
    public void updateBatch(List<StorageInfo> lstStorageInfo){
        splitDAO.updateBatch(lstStorageInfo);
    }

    /**
     * 拆分列表
     * @param pageTbl 分页工具页面类
     * @param params 参数
     * @return pageUtils
     */
    @Override
    public PageUtils getList(PageTbl pageTbl,Map<String, Object> params) {
        Page page = new Page(pageTbl.getPageno(),pageTbl.getPagesize(),pageTbl.getSortname(),"asc".equalsIgnoreCase(pageTbl.getSortorder()));
        return new PageUtils(page.setRecords(splitDAO.getList(page,params)));
    }
}
