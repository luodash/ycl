package com.tbl.modules.wms.service.baseinfo;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.wms.entity.baseinfo.Warehouse;

import java.util.List;
import java.util.Map;

/**
 * 仓库
 * @author 70486
 */
public interface WarehouseService extends IService<Warehouse> {

    /**
     * 分页获取数据
     * @param pageTbl
     * @param map
     * @return PageUtils
     */
    PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map);

    /**
     * 分页获取数据-原材料
     * @param pageTbl
     * @param map
     * @return PageUtils
     */
    PageUtils getYclPageList(PageTbl pageTbl, Map<String, Object> map);

    /**
     * 获取导出物料列表
     * @param ids
     * @param name
     * @param code
     * @param factoryCode
     * @param factoryList
     * @return Warehouse
     */
    List<Warehouse> getExcelList(String ids, String name, String code,String factoryCode, List<Long> factoryList);

    /**
     * 获取所有仓库列表
     * @param params
     * @return Warehouse
     */
    Page<Warehouse> getSelectWarehouseList(Map<String, Object> params);

    /**
     * 获取厂区下仓库的下拉选信息
     * @param pageTbl
     * @param map
     * @return PageUtils
     */
    PageUtils getPageWarehouseList(PageTbl pageTbl, Map<String, Object> map);

    /**
     * 获取仓库列表
     * @param map
     * @return List<Map<String,Object>>
     */
    List<Map<String, Object>> getWarehouseList(Map<String, Object> map);

    /**
     * 获取厂区下原材料仓库的下拉选信息
     * @param map
     * @return
     */
    List<Map<String, Object>> getYclPageWarehouseList(Map<String, Object> map);

    List<Map<String, Object>> getYclPageWarehouseListByEntityID(Map<String, Object> map);

}
