package com.tbl.modules.wms.service.inventory;

import java.util.Map;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.wms.entity.split.RfidBind;

/**
 * rfid绑定
 * @author 70486
 */
public interface RfidBindService extends IService<RfidBind> {
	
	/**
     * rfid列表
     * @param params
     * @return PageUtils
     */
    PageUtils getList(Map<String, Object> params);
	
}
