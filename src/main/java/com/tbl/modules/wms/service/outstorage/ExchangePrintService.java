package com.tbl.modules.wms.service.outstorage;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.modules.wms.entity.outstorage.ExchangePrint;

/**
 * 流转单打印服务类
 * @author 70486
 */
public interface ExchangePrintService extends IService<ExchangePrint> {

    /**
     * 物资流转单据补打
     * @param code 物资流转单号
     * @return boolean
     */
    boolean patchPrint(String code);
}
