package com.tbl.modules.wms.service.move;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.modules.wms.entity.move.MoveStorageDetail;

import java.util.List;
import java.util.Map;

/**
 * 移库处理-移库详情
 * @author 70486
 */
public interface MoveStorageDetailService extends IService<MoveStorageDetail> {

    /**
     * 获得导出Excel列表数据
     * @param map
     * @return List<MoveStorage>
     */
    List<MoveStorageDetail> getDetailExcelList(Map<String, Object> map);


}
