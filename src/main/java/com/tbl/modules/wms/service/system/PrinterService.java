package com.tbl.modules.wms.service.system;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.wms.entity.system.Printer;

import java.util.Map;

/**
 * 打印机设计服务
 * @author 70486
 */
public interface PrinterService extends IService<Printer> {

    /**
     * 打印配置列表数据
     * @param  map
     * @return PageUtils
     */
    PageUtils queryPage(Map<String,Object> map);

    /**
     * 根据id查询
     * @param  id
     * @return Printer
     */
    Printer findById(Long id);

    /**
     * 根据用户ID获取打印机配置
     * @param userId
     * @return Printer
     */
    Printer findPrinterCodeByUserId(Long userId);
}
