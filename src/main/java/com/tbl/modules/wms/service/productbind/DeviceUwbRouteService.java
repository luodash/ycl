package com.tbl.modules.wms.service.productbind;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.wms.entity.productbind.DeviceUwbRoute;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * 行车坐标
 * @author 70486
 */
public interface DeviceUwbRouteService extends IService<DeviceUwbRoute> {

    /**
     * 获取uwb维护列表的数据
     * @param  map
     * @return PageUtils
     */
    PageUtils queryPage(Map<String,Object> map);

    /**
     * 获取导出列
     * @param ids
     * @return List<DeviceUwbRoute>
     * */
    List<DeviceUwbRoute> getAllLists(String ids);

    /**
     * 导出excel
     * @param response
     * @param path
     * @param list
     */
    void toExcel(HttpServletResponse response, String path, List<DeviceUwbRoute> list);

}
