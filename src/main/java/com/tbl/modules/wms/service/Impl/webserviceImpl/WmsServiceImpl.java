package com.tbl.modules.wms.service.Impl.webserviceImpl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.google.common.collect.Maps;
import com.tbl.common.utils.StringUtils;
import com.tbl.common.utils.YamlConfigurerUtil;
import com.tbl.modules.wms.constant.EbsInterfaceEnum;
import com.tbl.modules.wms.constant.JaxbUtil;
import com.tbl.modules.wms.constant.XmlToolsUtil;
import com.tbl.modules.wms.dao.baseinfo.*;
import com.tbl.modules.wms.dao.interfacelog.InterfaceLogDAO;
import com.tbl.modules.wms.dao.webservice.WmsDAO;
import com.tbl.modules.wms.dao.webservice.YddlOutDanDAO;
import com.tbl.modules.wms.entity.baseinfo.*;
import com.tbl.modules.wms.entity.printinfo.PrintInfo;
import com.tbl.modules.wms.entity.responsexml.disc.DiscWsinterface;
import com.tbl.modules.wms.entity.responsexml.factory.FactoryWsinterface;
import com.tbl.modules.wms.entity.responsexml.factory.PlantXml;
import com.tbl.modules.wms.entity.responsexml.inventory.InventoryWsinterface;
import com.tbl.modules.wms.entity.responsexml.inventory.InventoryXml;
import com.tbl.modules.wms.entity.responsexml.location.LocationWsinterface;
import com.tbl.modules.wms.entity.responsexml.location.LocationXml;
import com.tbl.modules.wms.entity.responsexml.material.MaterialWsinterface;
import com.tbl.modules.wms.entity.responsexml.material.MaterialXml;
import com.tbl.modules.wms.entity.responsexml.print.LinedetailXml;
import com.tbl.modules.wms.entity.responsexml.print.OutDanXml;
import com.tbl.modules.wms.entity.responsexml.print.OutDansXml;
import com.tbl.modules.wms.entity.responsexml.print.YddlWsinterface;
import com.tbl.modules.wms.entity.responsexml.warehouse.WarehouseWsinterface;
import com.tbl.modules.wms.entity.responsexml.warehouse.WarehouseXml;
import com.tbl.modules.wms.entity.responsexml.workshop.WorkshopWsinterface;
import com.tbl.modules.wms.entity.responsexml.workshop.WorkshopXml;
import com.tbl.modules.wms.service.webserviceutil.CallByHttp;
import com.tbl.modules.wms.service.webserviceutil.MapToXml;
import com.tbl.modules.wms.service.webservice.client.WmsService;
import org.apache.poi.ss.formula.functions.T;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * WMS调外接口实现类
 * @author 70486
 */
@Service
public class WmsServiceImpl extends ServiceImpl<WmsDAO, T> implements WmsService {

    /**
     * 日志打印
     */
    private final Logger logger = LoggerFactory.getLogger(getClass());
    /**
     * RFID写卡地址
     */
    @Value("${yddl.RFID_WSDL}")
    private String RFID_WSDL;
    /**
     * 物料
     */
    @Autowired
    private MaterialDAO materialDAO;
    /**
     * 仓库
     */
    @Autowired
    private WarehouseDAO warehouseDAO;
    /**
     * 库位信息
     */
    @Autowired
    private ShelfDAO shelfDAO;
    /**
     * 厂区
     */
    @Autowired
    private FactoryAreaDAO factoryAreaDAO;
    /**
     * 盘
     */
    @Autowired
    private DishDAO dishDAO;
    /**
     * 提货单打印
     */
    @Autowired
    private YddlOutDanDAO yddlOutDanDAO;
    /**
     * 接口日志记录
     */
    @Autowired
    private InterfaceLogDAO interfaceLogDAO;
    /**
     * 车间
     */
    @Autowired
    private WorkshopDAO workshopDAO;

    /**
     * 调EBS接口-基础数据物料 FEWMS001
     * @param map 请求报文
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void FEWMS001(Map<String, Object> map) {
        String xml = "", subXml = "", xmlString = "";
        try {
            xmlString = new String(Objects.requireNonNull(MapToXml.callMapToXML(map)));
            xml = CallByHttp.callWebService(YamlConfigurerUtil.getStrYmlVal("yddl.WSDL"), EbsInterfaceEnum.FEWMS001, xmlString, "");

            xml = xml.replaceAll("&lt;","<").replaceAll("&gt;",">").replaceAll("&apos;","'")
                    .replaceAll("&quot;","\"");
            subXml = XmlToolsUtil.getSubXml(xml);

            if("false".equals(subXml)) {
                interfaceLogDAO.insertLog("FEWMS001", "EBS接口-物料", xmlString, xml, "", "");
                return;
            }

            MaterialWsinterface wsinterface = JaxbUtil.xmlToBean(subXml, MaterialWsinterface.class);
            List<MaterialXml> materials = new ArrayList<>();
            if (wsinterface!=null && wsinterface.getBody()!=null && wsinterface.getBody().getData()!=null
                    && wsinterface.getBody().getData().getMaterialList()!=null){
                materials = wsinterface.getBody().getData().getMaterialList();
            }

            List<Material> updateList = new ArrayList<>();
            List<Material> addList = new ArrayList<>();

            materials.forEach(materialXml -> {
                Material material = new Material();
                material.setCode(materialXml.getCode());
                Material newMaterial = materialDAO.selectOne(material);
                if (newMaterial != null) {
                    newMaterial.setName(materialXml.getName());
                    newMaterial.setMainUnit(materialXml.getMainUnit());
                    newMaterial.setSubUnit(materialXml.getSubUnit());
                    newMaterial.setStandardPackageWeight(materialXml.getStandardPackageWeight());
                    newMaterial.setRemark(materialXml.getRemark());
                    updateList.add(newMaterial);
                } else {
                    material.setName(materialXml.getName());
                    material.setMainUnit(materialXml.getMainUnit());
                    material.setSubUnit(materialXml.getSubUnit());
                    material.setStandardPackageWeight(materialXml.getStandardPackageWeight());
                    material.setRemark(materialXml.getRemark());
                    addList.add(material);
                }
            });

            if (addList.size() > 0) {
                materialDAO.insertBatches(addList);
            }
            if (updateList.size() > 0) {
                materialDAO.updateBatches(updateList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        interfaceLogDAO.insertLog("FEWMS001", "EBS接口-物料", xmlString, subXml, "", "");
        logger.info("#####EBS---》WMS####物料数据同步："+xml);
    }

    /**
     * 调EBS接口-出库接口 FEWMS002
     * @param map 请求报文
     * @param qaCode 质保号
     * @param batchNo 批次号
     * @return Map<String,Object>
     */
    @Override
    public Map<String,Object> FEWMS002(Map<String, Object> map, String qaCode, String batchNo) {
        String xml = "";
        Map<String, Object> returnMap = Maps.newHashMap();
        String parammsInfo = new String(Objects.requireNonNull(MapToXml.callMapToXML(map)));
        try {
            xml = CallByHttp.callWebService(YamlConfigurerUtil.getStrYmlVal("yddl.WSDL"),EbsInterfaceEnum.FEWMS002, parammsInfo,"");
        } catch (Exception e) {
            e.printStackTrace();
        }
        xml = xml.replaceAll("&lt;", "<").replaceAll("&gt;", ">").replaceAll("&apos;","'")
                .replaceAll("&quot;","\"");
        String subXml = XmlToolsUtil.getSubXml(xml);
        
        interfaceLogDAO.insertLog("FEWMS002", "EBS接口-出库", parammsInfo, "false".equals(subXml) ? xml : subXml, qaCode, batchNo);
        logger.info("#####  EBS---》WMS  ####出库："+xml);
        returnMap.put("returnData", xml);
        returnMap.put("paramsinfo",parammsInfo);
        return returnMap;
    }

    /**
     * 调EBS接口-发货单修改接口 FEWMS007
     * @param map 请求报文
     * @param qaCode 质保单号
     * @param batchNo 批次号
     * @return String
     */
    @Override
    public String FEWMS007(Map<String, Object> map, String qaCode, String batchNo) {
        String xml = "";
        String xmlString = new String(Objects.requireNonNull(MapToXml.callMapToXML(map)));
        try {
            xml = CallByHttp.callWebService(YamlConfigurerUtil.getStrYmlVal("yddl.WSDL"), EbsInterfaceEnum.FEWMS007, xmlString, "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        xml = xml.replaceAll("&lt;", "<").replaceAll("&gt;", ">").replaceAll("&apos;","'")
                .replaceAll("&quot;","\"");
        String subXml = XmlToolsUtil.getSubXml(xml);
        
        interfaceLogDAO.insertLog("FEWMS007", "EBS发货单修改", xmlString, "false".equals(subXml)?xml:subXml, qaCode, batchNo);
        logger.info("#####EBS---》WMS####发货单修改："+xml);
        return xml;
    }


    /**
     * 调EBS接口-调拨、移库接口 FEWMS008
     * @param map 请求报文
     * @param qaCode 质保单号
     * @param batchNo 批次号
     * @return String
     */
    @Override
    public String FEWMS008(Map<String, Object> map, String qaCode, String batchNo) {
        String xml = "";
        String xmlStrng = new String(Objects.requireNonNull(MapToXml.callMapToXML(map)));
        try {
            xml = CallByHttp.callWebService(YamlConfigurerUtil.getStrYmlVal("yddl.WSDL"), EbsInterfaceEnum.FEWMS008, xmlStrng, "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        xml = xml.replaceAll("&lt;", "<").replaceAll("&gt;", ">").replaceAll("&apos;","'")
                .replaceAll("&quot;","\"");
        String subXml = XmlToolsUtil.getSubXml(xml);

        String responseString = "false".equals(subXml) ? xml : subXml;
        interfaceLogDAO.insertLog("FEWMS008", "EBS接口-调拨、移库", xmlStrng, responseString, qaCode, batchNo);
        logger.info("#####EBS---》WMS####调拨、移库："+xml);
        return xml;
    }

    /**
     * 调EBS接口-获取出库打印单信息 FEWMS0009
     * @param map 请求参数
     * @return String
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public String FEWMS009(Map<String, Object> map) {
        String xml = "", subXml = "";
        String xmlStrng = new String(Objects.requireNonNull(MapToXml.callMapToXML(map)));
        try {
            xml = CallByHttp.callWebService(YamlConfigurerUtil.getStrYmlVal("yddl.WSDL"), EbsInterfaceEnum.FEWMS009, xmlStrng, "");
            xml = xml.replaceAll("&lt;","<").replaceAll("&gt;",">").replaceAll("&apos;","'")
                    .replaceAll("&quot;","\"");
            subXml = XmlToolsUtil.getSubXml(xml);

            //调用接口失败
            if("false".equals(subXml)) {
                interfaceLogDAO.insertLog("FEWMS009", "EBS接口-出库打印单", xmlStrng, xml, "", "");
                return xml;
            }

            YddlWsinterface yddlWsinterface = JaxbUtil.xmlToBean(subXml, YddlWsinterface.class);
            OutDansXml outDansXml = yddlWsinterface.getBody().getData().getOutDansXml();
            //出库单主信息及对应下面的明细的批次号集合
            List<OutDanXml> lstOutDan = outDansXml.getOutDanXml();

            //运输单
            String wayno = yddlWsinterface.getBody().getData().getWayno() == null ? "" : yddlWsinterface.getBody().getData().getWayno();
            //车牌号
            String car = yddlWsinterface.getBody().getData().getCar() == null ? "" : yddlWsinterface.getBody().getData().getCar();
            //司机
            String drivername = yddlWsinterface.getBody().getData().getDrivername() == null ? "" :
                    yddlWsinterface.getBody().getData().getDrivername();
            //运输公司
            String transportcompany = yddlWsinterface.getBody().getData().getTransportcompany() == null ? "" :
                    yddlWsinterface.getBody().getData().getTransportcompany();

            if(lstOutDan!=null) {
                lstOutDan.forEach(outDanXml -> {
                    //出库单打印主表主键
                    long id = 0;

                    PrintInfo yinfo = new PrintInfo();
                    //运单号
                    yinfo.setWayno(wayno);
                    //车牌号
                    yinfo.setCar(car);
                    //司机
                    yinfo.setDrivername(drivername);
                    //运输公司
                    yinfo.setTransportcompany(transportcompany);
                    //发货仓库
                    yinfo.setDeliverywarehouse(outDanXml.getDeliverywarehouse());
                    //发货单号
                    yinfo.setShipNo(outDanXml.getOutno());
                    //发货日期
                    yinfo.setOutdate(outDanXml.getOutdate());
                    //承办单位（人）
                    yinfo.setUndertake(outDanXml.getUndertake());
                    //承出库日期
                    yinfo.setOutdate(outDanXml.getOutdate());
                    //开票人
                    yinfo.setInvoice(outDanXml.getInvoice());
                    //发货人
                    yinfo.setDelivery(outDanXml.getDelivery());
                    //发货地址
                    yinfo.setDeliveryaddress(outDanXml.getDeliveryaddress());
                    //承运人
                    yinfo.setShipment(outDanXml.getShipment());
                    //经办
                    yinfo.setHandle(outDanXml.getHandle());
                    //接货人
                    yinfo.setPickup(outDanXml.getPickup());
                    //联系电话（接货人）
                    yinfo.setPickuptel(outDanXml.getPickuptel());
                    //承办单位（人）电话
                    yinfo.setUndertaketel(outDanXml.getUndertaketel());
                    //是否已经打印 1是2否
                    yinfo.setIsprint("2");

                    if (yddlOutDanDAO.selectInfoById(outDanXml.getOutno(), car) == 0) {
                        yddlOutDanDAO.insert(yinfo);
                        id = yinfo.getId();
                    }
                    if (id != 0) {
                        List<LinedetailXml> listLine = outDanXml.getLinedetail();
                        if (listLine != null) {
                            for (LinedetailXml linedetailXml : listLine) {
                                if (StringUtils.isNotEmpty(linedetailXml.getBatchno())) {
                                    Map<String, Object> detail = new HashMap<>(2);
                                    detail.put("batchno", linedetailXml.getBatchno());
                                    detail.put("pid", id);
                                    yddlOutDanDAO.savePrintDetail(detail);
                                }
                            }
                        }
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("#####EBS---》WMS####出库打印单："+xml);
        interfaceLogDAO.insertLog("FEWMS009", "EBS接口-出库单打印数据同步", xmlStrng, subXml, "", "");
        return xml;
    }

    /**
     * 调EBS接口-基础数据-厂区  FEWMS012
     * @param map 请求报文
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void FEWMS012(Map<String, Object> map) {
        String xml = "", subXml = "";
        String xmlStrng = new String(Objects.requireNonNull(MapToXml.callMapToXML(map)));
        try {
            xml = CallByHttp.callWebService(YamlConfigurerUtil.getStrYmlVal("yddl.WSDL"), EbsInterfaceEnum.FEWMS012, xmlStrng, "");
            xml = xml.replaceAll("&lt;","<").replaceAll("&gt;",">").replaceAll("&apos;","'")
                    .replaceAll("&quot;","\"");
            subXml = XmlToolsUtil.getSubXml(xml);

            if("false".equals(subXml)) {
                interfaceLogDAO.insertLog("FEWMS012", "EBS接口-厂区", xmlStrng, xml, "", "");
                return;
            }

            FactoryWsinterface factoryWsinterface = JaxbUtil.xmlToBean(subXml, FactoryWsinterface.class);
            List<PlantXml> plantXmls = factoryWsinterface.getBody().getData().getPlantList();
//            List<FactoryArea> updateList = new ArrayList<>();
            List<FactoryArea> addList = new ArrayList<>();

            plantXmls.forEach(a -> {
                FactoryArea factoryArea = new FactoryArea();
                factoryArea.setCode(a.getPlantno());
                FactoryArea newFactoryArea1 = factoryAreaDAO.selectOne(factoryArea);
                if (newFactoryArea1 != null) {
                    newFactoryArea1.setName(a.getPlantname());
//                    updateList.add(newFactoryArea1);
                } else {
                    factoryArea.setName(a.getPlantname());
                    addList.add(factoryArea);
                }
            });

            if (addList.size() > 0) {
                factoryAreaDAO.insertBatches(addList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("#####EBS---》WMS####厂区："+xml);
        interfaceLogDAO.insertLog("FEWMS012", "EBS接口-基础数据-厂区", xmlStrng, subXml, "", "");
    }

    /**
     * 调EBS接口-基础数据-仓库  FEWMS013
     * @param map 请求报文
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void FEWMS013(Map<String, Object> map) {
        String xml = "", subXml = "";
        String xmlStrng = new String(Objects.requireNonNull(MapToXml.callMapToXML(map)));
        try {
            xml = CallByHttp.callWebService(YamlConfigurerUtil.getStrYmlVal("yddl.WSDL"), EbsInterfaceEnum.FEWMS013, xmlStrng, "");
            xml = xml.replaceAll("&lt;","<").replaceAll("&gt;",">").replaceAll("&apos;","'")
                    .replaceAll("&quot;","\"");
            subXml = XmlToolsUtil.getSubXml(xml);

            if("false".equals(subXml)) {
                interfaceLogDAO.insertLog("FEWMS013", "EBS接口-仓库", xmlStrng, xml, "", "");
                return;
            }

            WarehouseWsinterface warehouseWsinterface = JaxbUtil.xmlToBean(subXml, WarehouseWsinterface.class);
            List<WarehouseXml> warehouseXmls = warehouseWsinterface.getBody().getData().getWarehouseXmlList();

            List<Warehouse> updateList = new ArrayList<>();
            List<Warehouse> addList = new ArrayList<>();

            warehouseXmls.forEach(a -> {
                Warehouse warehouse = new Warehouse();
                warehouse.setCode(a.getWarehouseno());
                warehouse.setFactoryCode(a.getOrgid());
                Warehouse newWarehouse = warehouseDAO.selectOne(warehouse);
                if (newWarehouse != null) {
                    newWarehouse.setName(a.getWarehousename());
                    newWarehouse.setState(1);
                    updateList.add(newWarehouse);
                } else {
                    warehouse.setName(a.getWarehousename());
                    warehouse.setState(1);
                    addList.add(warehouse);
                }
            });

            if (addList.size() > 0) {
                warehouseDAO.insertBatches(addList);
            }
            if (updateList.size() > 0) {
                warehouseDAO.updateBatches(updateList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        logger.info("#####EBS---》WMS####仓库："+xml);
        interfaceLogDAO.insertLog("FEWMS013", "EBS接口-基础数据-仓库", xmlStrng, subXml, "", "");
    }

    /**
     * 调EBS接口-基础数据-库位  FEWMS014
     * @param map 请求报文
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void FEWMS014(Map<String, Object> map) {
        String xml = "", subXml = "";
        String xmlStrng = new String(Objects.requireNonNull(MapToXml.callMapToXML(map)));
        try {
            xml = CallByHttp.callWebService(YamlConfigurerUtil.getStrYmlVal("yddl.WSDL"), EbsInterfaceEnum.FEWMS014, xmlStrng, "");
            xml = xml.replaceAll("&lt;","<").replaceAll("&gt;",">").replaceAll("&apos;","'")
                    .replaceAll("&quot;","\"");
            subXml = XmlToolsUtil.getSubXml(xml);

            if("false".equals(subXml)) {
                interfaceLogDAO.insertLog("FEWMS014", "EBS接口-库位", xmlStrng, xml, "", "");
                return;
            }

            LocationWsinterface locationWsinterface = JaxbUtil.xmlToBean(subXml, LocationWsinterface.class);

            List<LocationXml> locationXmls = locationWsinterface.getBody().getData().getLocationXmlList();
            List<Shelf> updateList = new ArrayList<>();
            List<Shelf> addList = new ArrayList<>();

            locationXmls.forEach(a -> {
                Shelf shelf = new Shelf();
                shelf.setCodeComplete(a.getCode());
                Shelf newShelf = shelfDAO.selectOne(shelf);
                if (newShelf != null) {
                    newShelf.setWarehouseCode(a.getWarehouseno());
                    newShelf.setFactoryCode(a.getOrgid());
                    newShelf.setLocationDesc(a.name);
                    updateList.add(newShelf);
                } else {
                    shelf.setWarehouseCode(a.getWarehouseno());
                    shelf.setState(1);
                    shelf.setFactoryCode(a.getOrgid());
                    shelf.setLocationDesc(a.name);
                    addList.add(shelf);
                }
            });
            if (updateList.size() > 0) {
                shelfDAO.updateBatches(updateList);
            }
            if (addList.size() > 0) {
                shelfDAO.insertBatches(addList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("#####EBS---》WMS####库位："+xml);
        interfaceLogDAO.insertLog("FEWMS014", "EBS接口-基础数据-库位", xmlStrng, subXml, "", "");
    }

    /**
     * 调EBS接口-基础数据-车间  FEWMS026
     * @param map 请求报文
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void FEWMS026(Map<String, Object> map) {
        String xml = "", subXml = "";
        String xmlStrng = new String(Objects.requireNonNull(MapToXml.callMapToXML(map)));
        try {
            xml = CallByHttp.callWebService(YamlConfigurerUtil.getStrYmlVal("yddl.WSDL"), EbsInterfaceEnum.FEWMS026, xmlStrng, "");
            xml = xml.replaceAll("&lt;","<").replaceAll("&gt;",">").replaceAll("&apos;","'")
                    .replaceAll("&quot;","\"");
            subXml = XmlToolsUtil.getSubXml(xml);

            if("false".equals(subXml)) {
                interfaceLogDAO.insertLog("FEWMS026", "EBS接口-车间", xmlStrng, xml, "", "");
                return;
            }

            WorkshopWsinterface workshopWsinterface = JaxbUtil.xmlToBean(subXml, WorkshopWsinterface.class);

            List<WorkshopXml> workshopXmls = workshopWsinterface.getBody().getData().getWorkshopList();
            List<Workshop> addList = new ArrayList<>();
            List<Workshop> updateList = new ArrayList<>();

            workshopXmls.forEach(a -> {
                Workshop workshop = new Workshop();
                workshop.setWorkshopnumber(a.getWorkshopnumber());
                Workshop newWorkshop = workshopDAO.selectOne(workshop);
                if (newWorkshop != null) {
                    newWorkshop.setOrg(a.getOrgid());
                    newWorkshop.setWorkshopname(a.getWorkshopname());
                    updateList.add(newWorkshop);
                } else {
                    workshop.setOrg(a.getOrgid());
                    workshop.setState(1L);
                    workshop.setWorkshopname(a.getWorkshopname());
                    addList.add(workshop);
                }
            });
            if (addList.size() > 0) {
                workshopDAO.insertBatches(addList);
            }
            if (updateList.size() > 0) {
                workshopDAO.updateBatches(updateList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("#####EBS---》WMS####车间："+xml);
        interfaceLogDAO.insertLog("FEWMS026", "EBS接口-基础数据-车间", xmlStrng, subXml, "", "");
    }

    /**
     * 调EBS接口-基础数据-盘（盘具）  FEWMS019
     * @param map 请求报文
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void FEWMS019(Map<String, Object> map) {
        String xml = "", subXml = "";
        String xmlStrng = new String(Objects.requireNonNull(MapToXml.callMapToXML(map)));
        try {
            xml = CallByHttp.callWebService(YamlConfigurerUtil.getStrYmlVal("yddl.WSDL"), EbsInterfaceEnum.FEWMS019, xmlStrng, "");
            xml = xml.replaceAll("&lt;","<").replaceAll("&gt;",">").replaceAll("&apos;","'")
                    .replaceAll("&quot;","\"");
            subXml = XmlToolsUtil.getSubXml(xml);

            if("false".equals(subXml)) {
                interfaceLogDAO.insertLog("FEWMS019", "EBS接口-盘具", xmlStrng, xml, "", "");
                return;
            }

            DiscWsinterface discWsinterface = JaxbUtil.xmlToBean(subXml, DiscWsinterface.class);

            discWsinterface.getBody().getData().getDiscXmlList().forEach(a -> {
                //false更新，true新增
                boolean insertOrUpdate = false;
                Dish dish = dishDAO.getDish(a.getInventory_item_code());
                if (dish==null){
                    insertOrUpdate = true;
                    dish = new Dish();
                }
                //盘编号
                dish.setCode(a.getInventory_item_code());
                //规格型号
                dish.setModel(a.getItem_name());

                //是否超宽
                String modelSuperWide = a.getItem_name().substring(a.getItem_name().indexOf(" "));
                //是超宽
                dish.setSuperWide(modelSuperWide.contains("A") || modelSuperWide.contains("B") || modelSuperWide.contains("C") ? 1 : 0);

                //盘具分类
                dish.setDrumType(a.getItem_type_name());
                //盘具明细分类
                dish.setDrumTypeDetail(a.getItem_type_detail());
                //盘外径
                dish.setOuterDiameter(a.getD1_mm());
                //盘内经
                dish.setInnerDiameter(a.getD2_mm());
                //盘外宽
                dish.setOneMax(a.getL1max_mm());
                //盘内宽
                dish.setTwo(a.getL2_mm());
                //最大载重
                dish.setMaxCarry(a.getG_kg());

                if (insertOrUpdate) {
                    dishDAO.insert(dish);
                } else {
                    dishDAO.updateById(dish);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("#####EBS---》WMS####盘具："+xml);
        interfaceLogDAO.insertLog("FEWMS019", "EBS接口-基础数据-盘具", xmlStrng, subXml, "", "");
    }

    /**
     * 调EBS接口-按米出库  FEWMS015
     * @param  map 请求报文
     * @return String
     */
    @Override
    public String FEWMS015(Map<String, Object> map) {
        String xml = "";
        String xmlStrng = new String(Objects.requireNonNull(MapToXml.callMapToXML(map)));
        try {
            xml = CallByHttp.callWebService(YamlConfigurerUtil.getStrYmlVal("yddl.WSDL"), EbsInterfaceEnum.FEWMS015, xmlStrng, "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        xml = xml.replaceAll("&lt;", "<").replaceAll("&gt;", ">").replaceAll("&apos;","'")
                .replaceAll("&quot;","\"");
        String subXml = XmlToolsUtil.getSubXml(xml);
        
        interfaceLogDAO.insertLog("FEWMS015","按米出库",xmlStrng,"false".equals(subXml)?xml:subXml,"","");
        logger.info("#####EBS---》WMS####按米出库："+xml);
        return xml;
    }

    /**
     * 调EBS接口-分盘  FEWMS030
     * @param map 请求报文
     * @return String
     */
    @Override
    public String FEWMS030(Map<String, Object> map, String qaCode, String batchNo) {
        String xml = "";
        String xmlStrng = new String(Objects.requireNonNull(MapToXml.callMapToXML(map)));
        try {
            xml = CallByHttp.callWebService(YamlConfigurerUtil.getStrYmlVal("yddl.WSDL"), EbsInterfaceEnum.FEWMS030, xmlStrng, "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        xml = xml.replaceAll("&lt;", "<").replaceAll("&gt;", ">").replaceAll("&apos;","'")
                .replaceAll("&quot;","\"");
        String subXml = XmlToolsUtil.getSubXml(xml);
        
        interfaceLogDAO.insertLog("FEWMS030", "EBS接口-分盘", xmlStrng, "false".equals(subXml) ? xml : subXml,qaCode,batchNo);
        logger.info("#####EBS---》WMS####分盘："+xml);
        return xml;
    }

    /**
     * 调EBS接口-盘点  FEWMS011
     * @param params 请求报文
     * @return String
     */
    @Override
    public String FEWMS011(Map<String, Object> params) {
        String xml = "";
        String xmlStrng = new String(Objects.requireNonNull(MapToXml.callMapToXML(params)));
        try {
            xml = CallByHttp.callWebService(YamlConfigurerUtil.getStrYmlVal("yddl.WSDL"), EbsInterfaceEnum.FEWMS011, xmlStrng, "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        xml = xml.replaceAll("&lt;", "<").replaceAll("&gt;", ">").replaceAll("&apos;","'")
                .replaceAll("&quot;","\"");
        String subXml = XmlToolsUtil.getSubXml(xml);
        
        if("false".equals(subXml)) {
            interfaceLogDAO.insertLog("FEWMS011", "EBS接口-盘点", xmlStrng, xml, "","");
        }else {
            interfaceLogDAO.insertLog("FEWMS011", "EBS接口-盘点", xmlStrng, subXml,"","");
        }
        logger.info("#####EBS---》WMS####盘点："+xml);
        return xml;
    }

    /**
     * 调EBS接口-外协采购入库  FEWMS025
     * @param params 请求参数
     * @param qaCode 质保号
     * @param batchNo 批次号
     * @return String
     */
    @Override
    public String FEWMS025(Map<String, Object> params, String qaCode, String batchNo) {
        String xml = "";
        String xmlStrng = new String(Objects.requireNonNull(MapToXml.callMapToXML(params)));
        try {
            xml = CallByHttp.callWebService(YamlConfigurerUtil.getStrYmlVal("yddl.WSDL"), EbsInterfaceEnum.FEWMS025, xmlStrng, "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        xml = xml.replaceAll("&lt;", "<").replaceAll("&gt;", ">").replaceAll("&apos;","'")
                .replaceAll("&quot;","\"");
        String subXml = XmlToolsUtil.getSubXml(xml);
        
        if("false".equals(subXml)) {
            interfaceLogDAO.insertLog("FEWMS025", "EBS接口-外协采购入库", xmlStrng, xml, qaCode, batchNo);
        }else {
            interfaceLogDAO.insertLog("FEWMS025", "EBS接口-外采入库", xmlStrng, subXml, qaCode, batchNo);
        }
        logger.info("#####EBS---》WMS####外协采购入库："+xml);
        return xml;
    }

    /**
     * 调EBS接口-盘点入库（盘盈）  FEWMS020
     * @param params 请求参数
     * @return String
     */
    @Override
    public String FEWMS020(Map<String, Object> params, String qaCode, String batchNo) {
        String xml = "";
        String xmlStrng = new String(Objects.requireNonNull(MapToXml.callMapToXML(params)));
        try {
            xml = CallByHttp.callWebService(YamlConfigurerUtil.getStrYmlVal("yddl.WSDL"), EbsInterfaceEnum.FEWMS020, xmlStrng, "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        xml = xml.replaceAll("&lt;", "<").replaceAll("&gt;", ">").replaceAll("&apos;","'")
                .replaceAll("&quot;","\"");
        String subXml = XmlToolsUtil.getSubXml(xml);
        
        if("false".equals(subXml)) {
            interfaceLogDAO.insertLog("FEWMS020", "EBS接口-盘点入库", xmlStrng, xml, qaCode, batchNo);
        }else {
            interfaceLogDAO.insertLog("FEWMS020", "EBS接口-盘点入库（盘盈）", xmlStrng, subXml, qaCode, batchNo);
        }
        logger.info("#####外部---》WMS####盘盈入库：" + xml);
        return xml;
    }

    /**
     * 调EBS接口-盘点审核  FEWMS028
     * @param params 请求参数
     * @param qaCode 质保号
     * @param batchNo 批次号
     * @return String
     */
    @Override
    public List<String> FEWMS028(Map<String,Object> params, String qaCode, String batchNo) {
        String xml = "", subXml = "";
        String xmlStrng = new String(Objects.requireNonNull(MapToXml.callMapToXML(params)));
        List<String> qacodeList = new LinkedList<>();
        try {
            xml = CallByHttp.callWebService(YamlConfigurerUtil.getStrYmlVal("yddl.WSDL"), EbsInterfaceEnum.FEWMS028, xmlStrng, "");
            xml = xml.replaceAll("&lt;","<").replaceAll("&gt;",">").replaceAll("&apos;","'")
                    .replaceAll("&quot;","\"");
            subXml = XmlToolsUtil.getSubXml(xml);

            InventoryWsinterface inventoryWsinterface = JaxbUtil.xmlToBean(new String(subXml.getBytes("gbk"), StandardCharsets.UTF_8),
                    InventoryWsinterface.class);
            //执行失败的质保号
            List<InventoryXml> failqacodes = inventoryWsinterface.body.data.failqacodes;

            if (failqacodes!=null){
                failqacodes.forEach(inventoryXml -> qacodeList.add(inventoryXml.code));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if("false".equals(subXml)) {
            interfaceLogDAO.insertLog("FEWMS028", "EBS接口-盘点审核", xmlStrng, xml, qaCode, batchNo);
        }else {
            interfaceLogDAO.insertLog("FEWMS028", "EBS接口-盘点审核", xmlStrng, subXml, qaCode, batchNo);
        }
        logger.info("#####外部---》WMS####盘点审核："+xml);
        return qacodeList;
    }

    /**
     * 调EBS接口-销售退货入库  FEWMS024
     * @param params 请求参数
     * @param qaCode 质保号
     * @param batchNo 批次号
     * @return String
     */
    @Override
    public String FEWMS024(Map<String, Object> params, String qaCode, String batchNo) {
        String xml = "";
        String xmlStrng = new String(Objects.requireNonNull(MapToXml.callMapToXML(params)));
        try {
            xml = CallByHttp.callWebService(YamlConfigurerUtil.getStrYmlVal("yddl.WSDL"), EbsInterfaceEnum.FEWMS024, xmlStrng, "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        xml = xml.replaceAll("&lt;", "<").replaceAll("&gt;", ">").replaceAll("&apos;","'")
                .replaceAll("&quot;","\"");
        String subXml = XmlToolsUtil.getSubXml(xml);
        
        if("false".equals(subXml)) {
            interfaceLogDAO.insertLog("FEWMS024", "EBS接口-销售退货入库", xmlStrng, xml, qaCode, batchNo);
        }else {
            interfaceLogDAO.insertLog("FEWMS024", "EBS接口-销售退货入库", xmlStrng, subXml, qaCode, batchNo);
        }
        logger.info("#####外部---》WMS####销售退货入库："+xml);
        return xml;
    }

    /**
     * 调MES入库接口（生产入库）
     * @param params 请求参数
     * @param qaCode 质保单号
     * @param batchNo 批次号
     * @return String
     */
    @Override
    public String FEMES001(Map<String, Object> params, String qaCode, String batchNo) {
        String xml = "";
        String xmlStrng = new String(Objects.requireNonNull(MapToXml.callMapToXML(params)));
        try {
            xml = CallByHttp.callWebService(YamlConfigurerUtil.getStrYmlVal("yddl.MES_WSDL"), xmlStrng, "doProdFinish");
        } catch (Exception e) {
            e.printStackTrace();
        }
        xml = xml.replaceAll("&lt;", "<").replaceAll("&gt;", ">").replaceAll("&apos;","'")
                .replaceAll("&quot;","\"");
        String subXml = XmlToolsUtil.getMesSubXml(xml);
        
        if("false".equals(subXml)) {
            interfaceLogDAO.insertLog("FEMES001", "MES接口-MES入库接口", xmlStrng, xml, qaCode, batchNo);
        }else {
            interfaceLogDAO.insertLog("FEMES001", "MES接口-入库（正常入库）", xmlStrng, subXml, qaCode, batchNo);
        }
        logger.info("#####外部---》WMS####生产入库："+xml);
        return xml;
    }

    /**
     * 调MES退库回生产线接口
     * @param params 请求参数
     * @param qaCode 质保单号
     * @param batchNo 批次号
     * @return String
     */
    @Override
    public String FEMES002(Map<String, Object> params, String qaCode, String batchNo) {
        String xml = "";
        String xmlStrng = new String(Objects.requireNonNull(MapToXml.callMapToXML(params)));
        try {
            xml = CallByHttp.callWebService(YamlConfigurerUtil.getStrYmlVal("yddl.MES_WSDL"), xmlStrng, "doProdFinishReturn");
        } catch (Exception e) {
            e.printStackTrace();
        }
        xml = xml.replaceAll("&lt;", "<").replaceAll("&gt;", ">").replaceAll("&apos;","'")
                .replaceAll("&quot;","\"");
        String subXml = XmlToolsUtil.getMesSubXml(xml);
        
        if("false".equals(subXml)) {
            interfaceLogDAO.insertLog("FEMES002", "MES接口-退库回生产线", xmlStrng, xml, qaCode, batchNo);
        }else {
            interfaceLogDAO.insertLog("FEMES002", "MES接口-退库回生产线", xmlStrng, subXml, qaCode, batchNo);
        }
        logger.info("#####外部---》WMS####退库回生产线："+xml);
        return xml;
    }

    /**
     * 调RFID写卡
     * @param params 请求参数
     * @return String
     */
    @Override
    public String RFIDWRITE(Map<String, Object> params) {
        String xml = "";
        String xmlStrng = new String(Objects.requireNonNull(MapToXml.callMapToXML(params)));
        RFID_WSDL = "http://"+params.get("content")+":8008/WriteEpc.asmx";
        params.remove("content");
        try {
            xml = CallByHttp.callWebService(RFID_WSDL, xmlStrng, "writeEpc");
        } catch (Exception e) {
            e.printStackTrace();
        }
        xml = xml.replaceAll("&lt;", "<").replaceAll("&gt;", ">");
        String subXml = XmlToolsUtil.gerfidsubxml(xml);
        interfaceLogDAO.insertLog("RFIDWRITE", "WMS接口-RFID桌面写卡", xmlStrng, subXml, "", "");
        return xml;
    }

    /**
     * 调EBS接口-打印物资流转单 FEWMS031
     * @param map 请求参数
     * @return boolean
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean FEWMS031(Map<String, Object> map) {
        String xml;
        String xmlStrng = new String(Objects.requireNonNull(MapToXml.callMapToXML(map)));
        try {
            xml = CallByHttp.callWebService(YamlConfigurerUtil.getStrYmlVal("yddl.WSDL"), EbsInterfaceEnum.FEWMS031, xmlStrng, "");
            xml = xml.replaceAll("&lt;","<").replaceAll("&gt;",">").replaceAll("&apos;","'")
                    .replaceAll("&quot;","\"");
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        interfaceLogDAO.insertLog("FEWMS031", "EBS物资流转单打印", xmlStrng, xml, "", "");
        logger.info("##### EBS---》WMS 物资流转单打印："+xml);
        return true;
    }
}
