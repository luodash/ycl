package com.tbl.modules.wms.service.Impl.productBind;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.wms.dao.productbind.ProductBindDAO;
import com.tbl.modules.wms.entity.productbind.ProductBind;
import com.tbl.modules.wms.service.productbind.ProductBindService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 产线ip、webservice地址
 * @author 70486
 */
@Service
public class ProductBindServiceImpl extends ServiceImpl<ProductBindDAO, ProductBind> implements ProductBindService {

    /**
     * 产线ip
     */
    @Autowired
    private  ProductBindDAO productBindDAO;

    /**
     * 产线id与电脑ip关系列表
     * @param pageTbl
     * @param map
     * @return PageUtils
     */
    @Override
    public PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map) {
        String sortName = pageTbl.getSortname();
        String sortOrder = pageTbl.getSortorder();
        if (StringUtils.isEmptyString(sortName)) {
            sortName = "id";
        } else if("factoryId".equals(sortName)) {
            sortName = "factory_id";
        }
        if (StringUtils.isEmptyString(sortOrder)) {
            sortOrder = "desc";
        }
        Page page = new Page(pageTbl.getPageno(), pageTbl.getPagesize(), sortName, "asc".equalsIgnoreCase(sortOrder));
        return new PageUtils(page.setRecords(productBindDAO.getPageList(page, map)));
    }
}
