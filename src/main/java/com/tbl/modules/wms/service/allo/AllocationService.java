package com.tbl.modules.wms.service.allo;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.wms.entity.allo.Allocation;
import com.tbl.modules.wms.entity.allo.AllocationDetail;
import com.tbl.modules.wms.entity.baseinfo.Car;
import com.tbl.modules.wms.entity.baseinfo.Warehouse;

import java.util.List;
import java.util.Map;

/**
 * 调拨管理-调拨表
 * @author 70486
 */
public interface AllocationService extends IService<Allocation> {

    /**
     * 调拨管理列表页数据
     * @param pageTbl
     * @param map
     * @return PageUtils
     */
    PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map);

    /**
     * 移库，调拨单管理列表页数据
     * @param pageTbl
     * @param map
     * @return PageUtils
     */
    PageUtils getMoveAlloPageList(PageTbl pageTbl, Map<String, Object> map);

    /**
     * 调拨管理详情的列表
     * @param pageTbl
     * @param id
     * @return PageUtils
     */
    PageUtils getPageDetailList(PageTbl pageTbl, Integer id);

    /**
     * 移库调拨管理详情的列表
     * @param pageTbl
     * @param id
     * @return PageUtils
     */
    PageUtils getMoveAlloPageDetailList(PageTbl pageTbl, Integer id);

    /**
     * 调拨管理导出excel列表
     * @param map
     * @return Allocation
     */
    List<Allocation> getAlloStorageOutExcel(Map<String, Object> map);

    /**
     * 点击保存,保存主表信息
     * @param allocation
     * @return Long
     */
    Long saveAllocation(Allocation allocation);

    /**
     * 点击编辑,保存主表信息
     * @param allocation
     * @return Map<String, Object>
     */
    Map<String, Object> subAllocation(Allocation allocation);

    /**
     * 编辑页面  获取质保单号列表
     * @param page
     * @param map
     * @return PageUtils
     */
    PageUtils getQaCode(PageTbl page, Map<String, Object> map);

    /**
     * 新增详情数据
     * @param id
     * @param qaCodeIds
     * @param factoryCode
     * @param userId
     * @return boolean
     */
    boolean saveAllocationDetail(Long id, String[] qaCodeIds, String factoryCode, Long userId);

    /**
     * 删除详情
     * @param ids
     * @return Map<String, Object>
     */
    Map<String, Object> deleteAllocationDetailIds(String[] ids);

    /**
     * 列表页面点击删除，删除主表和详情的数据
     * @param ids
     * @return Map<String, Object>
     */
    Map<String, Object> deleteByIds(String[] ids);

    /**
     * 获取调入仓库的数据
     * @param page
     * @param m
     * @return PageUtils
     */
    PageUtils selectWarehouseIn(PageTbl page, Map<String, Object> m);

    /**
     * 获取调出仓库的数据
     * @param page
     * @param m
     * @return PageUtils
     */
    PageUtils selectWarehouseOut(PageTbl page, Map<String, Object> m);

    /**
     * 根据id获取主表信息
     * @param id
     * @return Allocation
     */
    Allocation selectInfoById(Long id);

    /**
     * 点击编辑，点击保存  保存主表信息
     * @param allocation
     * @return boolean
     */
    boolean saveAlloOutStorage(Allocation allocation);

    /**
     * 点击编辑，点击提交，修改状态
     * @param allocation
     * @return boolean
     */
    boolean subAlloOutStorage(Allocation allocation);

    /**
     *  跳转行车绑定页面获取行车绑定列表
     * @param warehouseCode
     * @param org
     * @return List<Car>
     */
    List<Car> selectCarListByWarehouseCode(String warehouseCode,List<String> org);

    /**
     * 点击出库执行，点击绑定行车  点击保存行车信息
     * @param detail
     * @return boolean
     */
    boolean saveCar(AllocationDetail detail);

    /**
     * 点击出库执行，点击开始拣货
     * @param id
     * @return boolean
     */
    boolean detailStart(Long id);

    /**
     * 点击出库执行，点击拣货完成
     * @param id
     * @param userId
     * @return boolean
     */
    boolean detailSuccess(Long id, Long userId);

    /**
     * 点击确认出库，批量完成出库
     * @param lstIds
     * @param userId
     * @return Map<String,Object>
     */
    Map<String,Object> allStorageOut(List<Long> lstIds, Long userId);

    /**
     * 获取列表数据
     * @param page
     * @param m
     * @return PageUtils
     */
    PageUtils getInDetailList(PageTbl page, Map<String, Object> m);

    /**
     * 获取行车列表
     * @param id
     * @return Car
     */
    List<Car> getCarList(Long id);

    /**
     * 点击开始入库更新仓库，行车，状态信息
     * @param detail
     */
    void updateDetail(AllocationDetail detail);

    /**
     * 调拨入库最后的库位栏下拉选择
     * @param page
     * @param map
     * @return PageUtils
     */
    PageUtils selectUnbindShelf(PageTbl page, Map<String, Object> map);

    /**
     * 入库确认
     * @param id
     * @param shelfId
     * @param userId
     * @return map
     */
    Map<String, Object> confirmInstorage(Long id, Long shelfId, Long userId);

    /**
     * 导出Excel获取列表
     * @param map
     * @return AllocationDetail
     */
    List<AllocationDetail> getAlloInExcelList(Map<String, Object> map);

    /**
     * 查找调拨单明细信息
     * @param id
     * @return AllocationDetail
     */
    AllocationDetail getDetailById(Long id);

    /**
     * 获取指定厂区下的仓库
     * @param factoryCode 厂区编码
     * @return List<Warehouse> 仓库列表
     */
    List<Warehouse> getWarehouseList(String factoryCode);

    /**
     * 获取库位表中未被使用的库位
     * @param warehouseCode 仓库编码
     * @param factoryCode 厂区编码
     * @return String 推荐库位
     */
    String getRecommendShelf(String warehouseCode,String factoryCode);

    /**
     * 获取调拨入库列表数据
     * @param page 页面封装类
     * @param map 条件
     * @return PageUtils
     */
    PageUtils getList(PageTbl page,Map<String, Object> map);

    /**
     * 获取调拨流水号
     * @return int
     */
    int selectLatestAllocation();
}
