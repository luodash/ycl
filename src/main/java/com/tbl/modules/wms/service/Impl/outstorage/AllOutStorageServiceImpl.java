package com.tbl.modules.wms.service.Impl.outstorage;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.wms.dao.instorage.InstorageDetailDAO;
import com.tbl.modules.wms.dao.outstorage.AllOutStorageDAO;
import com.tbl.modules.wms.entity.outstorage.AllOutStorage;
import com.tbl.modules.wms.service.outstorage.AllOutStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 1:退库回生产线  2.采购退货  3.报废
 * @author 70486
 */
@Service
public class AllOutStorageServiceImpl extends ServiceImpl<AllOutStorageDAO, AllOutStorage> implements AllOutStorageService {

    /**
     * 入库管理-订单详情
     */
    @Autowired
    private InstorageDetailDAO instorageDetailDAO;

    /**
     * 获取库存交易查询
     * @param pageTbl
     * @param map 条件
     * @return PageUtils
     */
    @Override
    public PageUtils getList(PageTbl pageTbl, Map<String, Object> map) {
        Page page = new Page(pageTbl.getPageno(), pageTbl.getPagesize(), pageTbl.getSortname(), "asc".equalsIgnoreCase(pageTbl.getSortorder()));
        return new PageUtils(page.setRecords(instorageDetailDAO.getAllList(page, map)));
    }
}
