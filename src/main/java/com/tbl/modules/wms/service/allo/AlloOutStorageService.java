package com.tbl.modules.wms.service.allo;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.modules.wms.entity.allo.AlloOutStorage;

/**
 * 调拨出库
 * @author 70486
 */
public interface AlloOutStorageService extends IService<AlloOutStorage> {

}
