package com.tbl.modules.wms.service.interfacelog;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.wms.entity.interfacelog.InterfaceDo;
import com.tbl.modules.wms.entity.outstorage.OutStorageDetail;

import java.util.List;
import java.util.Map;

/**
 *  接口离线执行
 * @author 70486
 **/
public interface InterfaceDoService extends IService<InterfaceDo> {
	
	/**
	 * 插入接口执行表
	 * @param code 接口编码
	 * @param name 接口名称
	 * @param requestString 请求报文
	 * @param operateTime 操作时间
	 * @param qacode 质保号
	 * @param batchNo 批次号
	 * @param spesign 特殊标志
	 * @param shipno 提货单号
	 */
	void insertDo(String code, String name, String requestString, String operateTime, String qacode, String batchNo, Long spesign, String shipno);

	/**
	 * 获取离线接口维护列表的数据
	 * @param map 参数条件
	 * @return PageUtils
	 */
	PageUtils queryPage(Map<String,Object> map);

	/**
	 * 批量更新接口离线执行表状态
	 * @param lstOutStorageDetail 发货单明细
	 * @param state 状态
	 */
	void updateStateBatches(List<OutStorageDetail> lstOutStorageDetail, String state);

	/**
	 * 更新接口执行表
	 * @param interfaceDo 接口数据
	 */
	void updateInterfaceDoByid(InterfaceDo interfaceDo);

	/**
	 * 获取定时任务需要执行的离线接口
	 * @return InterfaceDo
	 */
	List<InterfaceDo> findJobsInterfaceDos();
}
