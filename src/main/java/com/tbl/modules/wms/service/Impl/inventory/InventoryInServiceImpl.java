package com.tbl.modules.wms.service.Impl.inventory;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.modules.wms.dao.inventory.InventoryInDAO;
import com.tbl.modules.wms.entity.inventory.InventoryIn;
import com.tbl.modules.wms.service.inventory.InventoryInService;

/**
 * 盘点计划实现类
 * @author 70486
 */
@Service("inventoryInService")
public class InventoryInServiceImpl extends ServiceImpl<InventoryInDAO, InventoryIn> implements InventoryInService {
	
}
