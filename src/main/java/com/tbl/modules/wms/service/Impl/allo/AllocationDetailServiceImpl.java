package com.tbl.modules.wms.service.Impl.allo;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.modules.wms.dao.allo.AllocationDetailDAO;
import com.tbl.modules.wms.entity.allo.AllocationDetail;
import com.tbl.modules.wms.service.allo.AllocationDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 调拨管理-调拨详情表
 * @author 70486
 */
@Service
public class AllocationDetailServiceImpl extends ServiceImpl<AllocationDetailDAO, AllocationDetail> implements AllocationDetailService {

    /**
     * 调拨管理
     */
    @Autowired
    private AllocationDetailDAO allocationDetailDAO;

    /**
     * 调拨出库导表和调拨单管理导表
     * @param map 模糊查询条件
     * @return List<AllocationDetail>
     */
    @Override
    public List<AllocationDetail> getAlloStorageOutDetailExcel(Map<String, Object> map) {
        return allocationDetailDAO.getAlloStorageOutDetailExcel(map);
    }

}
