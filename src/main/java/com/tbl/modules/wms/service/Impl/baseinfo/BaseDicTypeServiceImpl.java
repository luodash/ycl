package com.tbl.modules.wms.service.Impl.baseinfo;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.modules.wms.dao.baseinfo.BaseDicTypeDAO;
import com.tbl.modules.wms.entity.baseinfo.BaseDicType;
import com.tbl.modules.wms.service.baseinfo.BaseDicTypeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 下拉框选择项实现类
 * @author 70486
 */
@Service
public class BaseDicTypeServiceImpl extends ServiceImpl<BaseDicTypeDAO, BaseDicType> implements BaseDicTypeService {

    /**
     * 基础下拉选择框列表
     */
    @Resource
    private BaseDicTypeDAO baseDicTypeDAO;

    /**
     * 获取入库状态全部下拉列表
     * @param map 条件
     * @return List<Map<String,Object>>
     */
    @Override
    public List<Map<String, Object>> getInstorageTypeList(Map<String, Object> map) {
        return baseDicTypeDAO.getInstorageTypeList(map);
    }

    /**
     * 获取时间类型下拉列表
     * @param map 条件
     * @return List<Map<String,Object>>
     */
    @Override
    public List<Map<String, Object>> getTimeTypeList(Map<String, Object> map) {
        return baseDicTypeDAO.getTimeTypeList(map);
    }

    /**
     * 获取出库类型全部下拉列表
     * @param map
     * @return List<Map<String,Object>>
     */
    @Override
    public List<Map<String, Object>> getOutStorageTypeList(Map<String, Object> map) {
        return baseDicTypeDAO.getOutStorageTypeList(map);
    }

    /**
     * 获取打印列表
     * @param map
     * @return List<Map<String,Object>>
     */
    @Override
    public List<Map<String, Object>> getPrinterHierarchyList(Map<String, Object> map) {
        return baseDicTypeDAO.getPrinterHierarchyList(map);
    }

    @Override
    public Map<String,Object> getQcDict(){
        List<Map<String,Object>> list = baseDicTypeDAO.getQcDict();
        Map<String, Object> map = new HashMap<>(4);
        if(list.size()>0){
            map.put("code", 0);
            map.put("msg", "success");
            map.put("result", true);
            map.put("data", list);

        }else{
            map.put("code", 1);
            map.put("msg", "Can't find data");
            map.put("result", false);
            map.put("data", null);
        }
        return map;
    }

    @Override
    public Map<String,Object> getOtherTransactionDict(Integer type){
        Map<String, Object> map = new HashMap<>(4);
        if(type!=null && type>0){
            List<Map<String,Object>> list = baseDicTypeDAO.getOtherTransactionDict(type);
            if(list.size()>0){
                map.put("code", 0);
                map.put("msg", "success");
                map.put("result", true);
                map.put("data", list);
                return map;
            }
        }
        map.put("code", 1);
        map.put("msg", "no data");
        map.put("result", false);
        map.put("data", "");
        return map;
    }

    @Override
    public Map<String,Object> getLyytDict(Integer id){
        Map<String,Object> map = new HashMap<>(4);
        List<Map<String,Object>> list = baseDicTypeDAO.getLyytDict(id);
        if(list.size()>0){
            map.put("code", 0);
            map.put("msg", "success");
            map.put("result", true);
            map.put("data", list);
        }else{
            map.put("code", 1);
            map.put("msg", "no data");
            map.put("result", false);
            map.put("data", "");
        }
        return map;
    }


}
