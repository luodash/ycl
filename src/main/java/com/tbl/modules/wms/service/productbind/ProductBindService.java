package com.tbl.modules.wms.service.productbind;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.wms.entity.productbind.ProductBind;

import java.util.Map;

/**
 * 产线ip
 * @author 70486
 */
public interface ProductBindService extends IService<ProductBind> {

    /**
     * 产线id与电脑ip关系列表
     * @param pageTbl
     * @param map 条件
     * @return PageUtils
     */
    PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map);
}
