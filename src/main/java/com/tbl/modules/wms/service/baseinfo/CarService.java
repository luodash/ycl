package com.tbl.modules.wms.service.baseinfo;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.wms.entity.baseinfo.Car;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * 行车Service
 * @author 70486
 */
public interface CarService extends IService<Car> {

    /**
     * 列表页
     * @param pageTbl
     * @param map
     * @return PageUtils
     */
    PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map);

    /**
     * 导出的数据
     * @param ids
     * @param queryCode
     * @param queryFactoryCode
     * @param queryWarehouse
     * @param factoryList
     * @return Car
     */
    List<Car> getExcelList(String ids, String queryCode, String queryFactoryCode, String queryWarehouse, List<String> factoryList);

    /**
     * 根据id获取car
     * @param id 行车主键
     * @param factoryCode 厂区编码
     * @return Car
     */
    Car findCarById(Long id,String factoryCode);

    /**
     * 获取仓库的下拉选信息
     * @param pageTbl
     * @param map
     * @return PageUtils
     */
    PageUtils getPageWarehouseList(PageTbl pageTbl, Map<String, Object> map);

    /**
     * 保存行车
     * @param car
     * @return boolean
     */
    boolean saveCar(Car car);

    /**
     * 删除行车
     * @param ids
     * @return boolean
     */
    boolean deleteCars(String ids);

    /**
     * 根据行车编码获取行车信息
     * @param code
     * @return Car
     */
    Car findCarByCode(String code);
    
    /**
     * 获取所有的仓库区域
     * @return Map<String,Object>
     */
    List<Map<String,Object>> selectWarehouseArea();

    /**
     * 更新行车状态
     * @param carCode 行车编码
     * @param missionShelf 目标库位
     * @param state 行车状态
     * @return boolean
     */
    Boolean updateCarByCode(String carCode, String missionShelf, Long state );

    /**
     * 行吊台账查询数据列表
     * @param pageTbl
     * @param params
     * @return PageUtils
     */
    PageUtils getHangCarList(PageTbl pageTbl,Map<String, Object> params);

    /**
     * 行吊台账导出的数据
     * @param params
     * @return Map<String,Object>
     */
    List<Map<String,Object>> getAllHangCarLists(Map<String, Object> params);

    /**
     * 行车工台账表导出Excel
     * @param response
     * @param path
     * @param list
     * @param excelIndexArray
     * */
    void toHangCarerExcel(HttpServletResponse response, String path, List<Map<String,Object>> list, String [] excelIndexArray);
}
