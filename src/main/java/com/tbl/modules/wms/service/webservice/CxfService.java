package com.tbl.modules.wms.service.webservice;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;


@WebService(name = "cxfService", // 暴露服务名称
        targetNamespace = "http://CxfService.webservice.service.wms.modules.tbl.com"// 命名空间,一般是接口的包名倒序
)
public interface CxfService {

    @WebMethod(action = "urn:receiveFAX")
    @WebResult(name = "String", targetNamespace = "")
    String invoice(@WebParam(name = "interfaceCode") String interfaceCode, @WebParam(name = "para") String para);


}
