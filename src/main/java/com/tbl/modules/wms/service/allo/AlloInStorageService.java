package com.tbl.modules.wms.service.allo;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.modules.wms.entity.allo.AlloInStorage;

/**
 * 调拨入库
 * @author 70486
 */
public interface AlloInStorageService extends IService<AlloInStorage> {
}
