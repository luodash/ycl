package com.tbl.modules.wms.service.move;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.wms.entity.baseinfo.Car;
import com.tbl.modules.wms.entity.baseinfo.Warehouse;
import com.tbl.modules.wms.entity.move.MoveStorage;
import com.tbl.modules.wms.entity.move.MoveStorageDetail;

import java.util.List;
import java.util.Map;

/**
 * 移库处理-移库主表
 * @author 70486
 */
public interface MoveStorageService extends IService<MoveStorage> {

    /**
     * 移库单列表页数据
     * @param page 页面封装类
     * @param m 条件
     * @return PageUtils
     */
    PageUtils getPageList(PageTbl page, Map<String, Object> m);

    /**
     * 根据主键获取到详情的列表
     * @param page 页面封装类
     * @param id 移库明细主键
     * @return PageUtils
     */
    PageUtils getPageDetailList(PageTbl page, Integer id);

    /**
     * 点击查看，获取详情列表页数据
     * @param id 移库单主键
     * @return MoveStorage 移库单
     */
    MoveStorage selectInfoById(Long id);

    /**
     * 获得导出Excel列表数据
     * @param map 查询条件
     * @return List<MoveStorage>
     */
    List<MoveStorage> getExcelList(Map<String, Object> map);

    /**
     * 点击保存，保存主表信息
     * @param moveStorage 移库单
     * @return Long 移库单主键
     */
    Long saveMoveStorage(MoveStorage moveStorage);

    /**
     * 编辑页面点击提交，修改状态
     * @param moveStorage 移库单
     * @return Map<String,Object>
     */
    Map<String, Object> subMoveStorage(MoveStorage moveStorage);

    /**
     * 新增页面点击提交，修改状态
     * @param moveStorage 移库单
     * @return Map<String,Object>
     */
    Map<String, Object> subMoveStorage2(MoveStorage moveStorage);

    /**
     * 获取质保单号列表
     * @param page 页面封装类
     * @param map 条件
     * @return PageUtils
     */
    PageUtils getQaCode(PageTbl page, Map<String, Object> map);

    /**
     * 移库单管理详情添加
     * @param id 移库单主键
     * @param qaCodeIds 库存主键
     * @return boolean
     */
    boolean saveMoveStorageDetail(Long id, String[] qaCodeIds);

    /**
     * 删除明细数据
     * @param ids 移库详情主键
     * @return Map<String,Object>
     */
    Map<String, Object> deleteMoveStorageDetailIds(String[] ids);

    /**
     * 列表页面点击删除，删除主表和详情的数据
     * @param ids 移库单主键
     * @return Map<String,Object>
     */
    Map<String, Object> deleteByIds(String[] ids);

    /**
     * 获取调入仓库数据
     * @param page 页面封装类
     * @param m 条件
     * @return PageUtils
     */
    PageUtils selectWarehouse(PageTbl page, Map<String, Object> m);

    /**
     * 获取绑定行车列表
     * @param warehouseCode 仓库编码
     * @param factoryList 厂区编码
     * @return List<Car> 行车下拉框中的选择
     */
    List<Car> selectCarListByWarehouseCode(String warehouseCode,List<String> factoryList);

    /**
     * 保存行车
     * @param detail 移库单明细
     * @return boolean
     */
    boolean saveCar(MoveStorageDetail detail);

    /**
     * 开始拣货
     * @param id 移库单明细主键
     * @return boolean
     */
    boolean detailStart(Long id);

    /**
     * 拣货完成
     * @param id 发货单明细主键
     * @param userId 用户主键
     * @return boolean 执行成功/失败
     */
    boolean detailSuccess(Long id, Long userId);

    /**
     * 移库入库获取列表数据
     * @param page 页面封装类
     * @param m 条件
     * @return PageUtils
     */
    PageUtils getInDetailList(PageTbl page, Map<String, Object> m);

    /**
     * 根据主键查询移库入库明细数据
     * @param id 移库单明细主键
     * @return MoveStorageDetail 移库单明细
     */
    MoveStorageDetail getDetailById(Long id);

    /**
     * 获得仓库列表
     * @param factoryCode 厂区编码
     * @return List<Warehouse> 仓库列表
     */
    List<Warehouse> getWarehouseList(String factoryCode);

    /**
     * 获得行车列表
     * @param warehouseId 仓库主键
     * @return List<Car> 行车列表
     */
    List<Car> getCarList(Long warehouseId);

    /**
     * 移库入库-开始入库-绑定仓库、行车信息-确认
     * @param detail 移库单明细
     */
    void updateDetail(MoveStorageDetail detail);

    /**
     * 获取仓库下未被占用的库位
     * @param warehouseCode 仓库编码
     * @param factoryCode 厂区编码
     * @return String 库位编码
     */
    String getRecommendShelf(String warehouseCode,String factoryCode);

    /**
     * 下拉框选择库位
     * @param page 页面封装类
     * @param m 条件
     * @return PageUtils
     */
    PageUtils selectUnbindShelf(PageTbl page, Map<String, Object> m);

    /**
     * 入库确认-保存
     * @param id 移库单明细主键
     * @param shelfId 库位主键
     * @param userId 用户主键
     * @return boolean
     */
    boolean confirmInstorage(Long id, Long shelfId,Long userId);

    /**
     * 获取excel导出列表
     * @param map
     * @return List<MoveStorageDetail>
     */
    List<MoveStorageDetail> getMoveInExcelList(Map<String, Object> map);

    /**
     * 移库出库批量完成出库
     * @param lstIds 移库单明细主键数组
     * @param userId 用户主键
     * @return boolean
     */
    Map<String,Object> moveStorageOut(List<Long> lstIds, Long userId);

    /**
     * 更新移库单、调拨单流水号
     * @param serial 流水号
     * @param type 类型：移库，调拨
     */
    void updateSerial(String serial, String type);

    /**
     * 获取移库单今日流水号
     * @return int 流水号
     */
    int selectLatestAllocation();

}
