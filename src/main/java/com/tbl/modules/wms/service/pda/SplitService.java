package com.tbl.modules.wms.service.pda;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.wms.entity.split.Split;
import com.tbl.modules.wms.entity.storageinfo.StorageInfo;

import java.util.List;
import java.util.Map;

/**
 * 拆分库存接口
 * @author 70486
 */
public interface SplitService extends IService<Split> {

    /**
     * 批量更新拆分表库位
     * @param lstStorageInfo
     */
    void updateBatch(List<StorageInfo> lstStorageInfo);

    /**
     * 拆分列表
     * @param page
     * @param params
     * @return PageUtils
     */
    PageUtils getList(PageTbl page, Map<String, Object> params);

}
