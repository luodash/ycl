package com.tbl.modules.wms.service.outstorage;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.wms.entity.outstorage.ShipLoading;

import java.util.List;
import java.util.Map;

/**
 * 出库管理-装车发运
 * @author 70486
 */
public interface ShipLoadingService extends IService<ShipLoading> {

    /**
     * 列表页数据
     * @param pageTbl
     * @param map
     * @return PageUtils
     */
    PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map);

    /**
     * 详情的列表
     * @param pageTbl
     * @param id
     * @return PageUtils
     */
    PageUtils getPageDetailList(PageTbl pageTbl, Integer id);

    /**
     * 导出excel列表
     * @param map
     * @return ShipLoading
     */
    List<ShipLoading> getExcelList(Map<String, Object> map);

    /**
     * 点击保存,保存主表信息
     * @param shipLoading
     * @return Long
     */
    Long saveShipLoading(ShipLoading shipLoading);

    /**
     * 点击编辑,保存主表信息
     * @param shipLoading
     * @return boolean
     */
    boolean subShiploading(ShipLoading shipLoading);

    /**
     * 编辑页面  获取质保单号列表
     * @param page
     * @param map
     * @return PageUtils
     */
    PageUtils getQaCode(PageTbl page, Map<String, Object> map);

    /**
     * 新增装车发运详情数据
     * @param id
     * @param qaCodeIds
     * @return boolean
     */
    boolean saveShipLoadingDetail(Long id, String[] qaCodeIds);

    /**
     * 删除detail数据
     * @param ids
     * @return boolean
     */
    boolean deleteShipLoadingDetailIds(String[] ids);

    /**
     * 删除info,detail数据
     * @param ids
     * @return boolean
     */
    boolean deleteByIds(String[] ids);
}
