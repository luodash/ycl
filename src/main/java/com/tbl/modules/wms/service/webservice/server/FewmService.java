package com.tbl.modules.wms.service.webservice.server;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.modules.wms2.entity.requestXML.FEWMS202_data;
import com.tbl.modules.wms2.entity.requestXML.FEWMS203_data;
import org.apache.poi.ss.formula.functions.T;

import java.util.Map;

/**
 * WMS是service方
 * @author 70486
 */

public interface FewmService extends IService<T> {

    /**
     * FEWMS003    WMS接口-销售退货入库
     */
    Map<String, Object> FEWMS003(String para);

    /**
     * FEWMS004    WMS接口-采购退货
     */
    Map<String, Object> FEWMS004(String para);

    /**
     * FEWMS005    WMS接口-保留接口
     */
    Map<String, Object> FEWMS005(String para);

    /**
     * FEWMS006    WMS接口-发货单接口
     */
    Map<String, Object> FEWMS006(String para);

    /**
     * FEWMS010    WMS接口-报废接口
     */
    Map<String, Object> FEWMS010(String para);

    /**
     * FEWMS017    标签信息初始化
     */
    Map<String, Object> FEWMS017(String para);
    
    /**
     * FEWMS021    外协采购数据同步
     */
    Map<String, Object> FEWMS021(String para);

    /**
     * FEWMS201    原材料采购/委外加工-订单数据同步
     */
    Map<String, Object> FEWMS201(String para);

    /**
     * FEWMS202     送货单
     */
    Map<String, Object> FEWMS202(String para);

    /**
     * FEWMS203  接收单数据查询
     * @param para
     * @return
     */
    String FEWMS203(String para);

    /**
     * 紧急物料审批结果同步fromOA
     * @param para
     * @return
     */
    Map<String, Object> FEWMS204(String para);

}
