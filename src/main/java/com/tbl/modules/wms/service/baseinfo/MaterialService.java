package com.tbl.modules.wms.service.baseinfo;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.wms.entity.baseinfo.Material;

import java.util.List;
import java.util.Map;

/**
 * 物料
 * @author 70486
 */
public interface MaterialService extends IService<Material> {

    /**
     * 分页获取数据
     * @param pageTbl
     * @param map
     * @return PageUtils
     */
    PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map);

    /**
     * 获取导出物料列表
     * @param ids
     * @param name
     * @return Material
     */
    List<Material> getExcelList(String ids, String name);

    /**
     * 根据id获取详细信息
     * @param id
     * @return
     */
    Material selectInfoById(Long id);


    /**
     * 根据物料编码获取物料信息-精确查找
     * @param map
     * @return
     */
    Map<String,Object> selectMaterialByCode(Map map);

    /**
     * 根据物料编码获取物料信息-模糊查询
     * @param map
     * @return
     */
    Map<String,Object> getMaterialsByCode(Map map);


    /**
     * 根据物料编码模糊查询物料信息并分页
     * @param map
     * @return
     */
    Map<String,Object> getMaterialsPageByCode(Map map);

    /**
     * 获取行业列表
     * @return List<Map<String,Object>>
     */
    List<Map<String, Object>> getIndustryList();

    /**
     * 获取类别列表
     * @param industry 行业编码
     * @return List<Map<String,Object>>
     */
    List<Map<String, Object>> getCategoryList(String industry);

    /**
     * 获取二级材料列表
     * @param industry 行业编码
     * @param category 物理类别
     * @return List<Map<String,Object>>
     */
    List<Map<String, Object>> getGradeTwoList(String industry, String category);

    /**
     * 获取三级材料列表
     * @param industry 行业编码
     * @param category 物理类别
     * @param gradeTwo 二级材料
     * @return List<Map<String,Object>>
     */
    List<Map<String, Object>> getGradeThreeList(String industry, String category, String gradeTwo);

}
