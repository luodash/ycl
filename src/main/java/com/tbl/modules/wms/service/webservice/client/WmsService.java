package com.tbl.modules.wms.service.webservice.client;

import com.baomidou.mybatisplus.service.IService;

import org.apache.poi.ss.formula.functions.T;

import java.util.List;
import java.util.Map;

/**
 * wms调用接口入口（客户端）
 * @author 70486
 */

public interface WmsService extends IService<T> {

    /**
     * 调EBS接口-物料 FEWMS001
     * @param map 请求参数
     */
    void FEWMS001(Map<String, Object> map);

    /**
     * 调EBS接口-出库接口 FEWMS002
     * @param map 请求参数
     * @param qaCode 质保单号
     * @param batchNo 批次号
     * @return Map<String,Object>
     */
    Map<String, Object> FEWMS002(Map<String, Object> map, String qaCode, String batchNo);

    /**FEWMS002
     * 调EBS接口-发货单修改接口 FEWMS007
     * @param map 请求参数
     * @param qaCode 质保单号
     * @param batchNo 批次号
     * @return String
     */
    String FEWMS007(Map<String, Object> map, String qaCode, String batchNo);

    /**
     * 调EBS接口-调拨、移库接口 FEWMS008
     * @param map 请求参数
     * @param qaCode 质保单号
     * @param batchNo 批次号
     * @return String
     */
    String FEWMS008(Map<String, Object> map, String qaCode, String batchNo);

    /**
     * 调EBS接口-出库打印单 FEWMS009
     * @param map 请求参数
     * @return String
     */
    String FEWMS009(Map<String, Object> map);

    /**
     * 调EBS接口-基础数据-厂区  FEWMS012
     * @param map 请求参数
     */
    void FEWMS012(Map<String, Object> map);

    /**
     * 调EBS接口-基础数据-仓库  FEWMS013
     * @param map 请求参数
     */
    void FEWMS013(Map<String, Object> map);

    /**
     * 调EBS接口-基础数据-库位  FEWMS014
     * @param map 请求参数
     */
    void FEWMS014(Map<String, Object> map);
    
    /**
     * 调EBS接口-基础数据-盘 FEWMS019
     * @param map 请求参数
     */
    void FEWMS019(Map<String, Object> map);

    /**
     * 调EBS接口-按米出库  FEWMS015
     * @param map 请求参数
     * @return String
     */
    String FEWMS015(Map<String, Object> map);

    /**
     * 调EBS接口-分盘  FEWMS016
     * @param map 请求参数
     * @param qaCode 质保单号
     * @param batchNo 批次号
     * @return String
     */
    String FEWMS030(Map<String, Object> map, String qaCode, String batchNo);

    /**
     * 调EBS接口-盘点  FEWMS011
     * @param map 请求参数
     * @return String
     */
    String FEWMS011(Map<String, Object> map);
    
    /**
     * 调EBS接口-外采入库  FEWMS025
     * @param params 请求参数
     * @param qaCode 质保单号
     * @param batchNo 批次号
     * @return String
     */
    String FEWMS025(Map<String, Object> params, String qaCode, String batchNo);
    
    /**
     * 调EBS接口-盘点入库（盘盈）  FEWMS020
     * @param params 请求参数
     * @param qaCode 质保单号
     * @param batchNo 批次号
     * @return String
     */
    String FEWMS020(Map<String, Object> params, String qaCode, String batchNo);

    /**
     * 调EBS接口-盘点审核  FEWMS028
     * @param params 请求参数
     * @param qaCode 质保单号
     * @param batchNo 批次号
     * @return String
     */
    List<String> FEWMS028(Map<String, Object> params, String qaCode, String batchNo);
    
    /**
     * 调EBS接口-退货入库  FEWMS024
     * @param params 请求参数
     * @param qaCode 质保单号
     * @param batchNo 批次号
     * @return String
     */
    String FEWMS024(Map<String, Object> params, String qaCode, String batchNo);

    /**
     * 调MES接口-入库接口
     * @param params 请求参数
     * @param qaCode 质保单号
     * @param batchNo 批次号
     * @return String
     */
    String FEMES001(Map<String, Object> params, String qaCode, String batchNo);
    
    /**
     * 调MES接口-退库回生产线
     * @param map 请求参数
     * @param qaCode 质保单号
     * @param batchNo 批次号
     * @return String
     */
    String FEMES002(Map<String, Object> map, String qaCode, String batchNo);
    
    /**
     * 调RFID写卡
     * @param params 请求参数
     * @return String
     */
    String RFIDWRITE(Map<String, Object> params);
    
    /**
     * 基础数据-车间
     * @param params 请求参数
     */
    void FEWMS026(Map<String, Object> params);

    /**
     * 打印物资流转单
     * @param params 请求参数
     */
    boolean FEWMS031(Map<String,Object> params);

}
