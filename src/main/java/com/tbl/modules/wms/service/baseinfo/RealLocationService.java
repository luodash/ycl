package com.tbl.modules.wms.service.baseinfo;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.modules.wms.entity.baseinfo.RealLocation;

/**
 * 真实库位坐标记录服务
 * @author 70486
 */
public interface RealLocationService extends IService<RealLocation> {

}
