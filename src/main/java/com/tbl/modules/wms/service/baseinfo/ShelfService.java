package com.tbl.modules.wms.service.baseinfo;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.wms.entity.baseinfo.Shelf;

import java.util.List;
import java.util.Map;

/**
 * 库位信息
 * @author 70486
 */
public interface ShelfService extends IService<Shelf> {

    /**
     * 库位列表分页获取数据
     * @param pageTbl 分页工具页面类
     * @param  map 参数条件
     * @return PageUtils
     */
    PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map);

    /**
     * 获取导出物料列表
     * @param ids 选中的库位的主键
     * @param code 库位编码
     * @param factoryList 登陆人所属厂区
     * @param warehouse 仓库编码
     * @param factoryCode 厂区编码
     * @param warehouseIds 仓库ID集合
     * @return List<Shelf>
     */
    List<Shelf> getExcelList(String ids, String code, List<String> factoryList, String warehouse, String factoryCode,List<Long> warehouseIds);
    
    /**
     * 获取库存使用率列表
     * @param map 参数条件
     * @return PageUtils
     */
    PageUtils getList(Map<String, Object> map);
    
    /**
     * 获取库存使用率图表
     * @return Map<String, Object> map
     */
    Map<String,Object> getLine();

    /**
     * 库位启用
     * @param ids 选中的库位ids
     */
    void isStart(Long[] ids);

    /**
     * 库位禁用
     * @param ids 选中的库位ids
     */
    void disMiss(Long[] ids);
    
    /**
     * 获取库位表中未被使用的库位
     * @param warehouseCode 仓库编码
     * @param factoryCode 厂区编码
     * @param warehouseArea 仓库区域
     * @return List<Shelf> 推荐库位
     */
    List<Shelf> getRecommendShelf(String warehouseCode,String factoryCode,String warehouseArea);
    
    /**
     * 获取推荐库位
     * @param factoryCode 厂区编码
     * @param warehouseCode 仓库编码
     * @param outerDiameter 盘外经
     * @param drumType 盘类型
     * @param carCode 航车
     * @param moveShelf 移库入库之前所在库位
     * @param warehouseArea 仓库区域
     * @param salesCode 营销经理编号
     * @param superWide 是否超宽
     * @param coloured 是否有颜色
     * @return List<Shelf> 库位列表
     */
    List<Shelf> selectRecommentList(String factoryCode,String warehouseCode,String outerDiameter,String drumType, String carCode,
                                    String moveShelf, String warehouseArea, String salesCode, Integer superWide, Integer coloured);
    
    /**
     * 根据厂区和仓库获取选择的库位列表
     * @param pageTbl 分页工具类
     * @param map 条件参数
     * @return PageUtils
     */
    PageUtils getShelfByFactoryWarehouse(PageTbl pageTbl, Map<String, Object> map);

    /**
     * 根据厂区编码和仓库编码获取选择的库位列表
     * @param pageTbl 分页工具类
     * @param map 条件参数
     * @return PageUtils
     */
    PageUtils getShelfByFactWareCode(PageTbl pageTbl, Map<String, Object> map);

    /**
     * 设置所有的库位状态为可用
     */
    void setAllStatesUseful();

    /**
     * 库位设为不可用/可用
     * @param code 库位编码
     * @param factoryCode 厂区
     * @param warehouseCode 仓库
     * @param type 1可用；0不可用
     */
    void setShelfState(String code, String factoryCode, String warehouseCode, Integer type);

    /**
     * 根据仓库区域获取可用库位
     * @param warehouseAreaList 仓库区域列表
     * @return Shelf 库位集合
     */
    List<Shelf> findShelfByArea(List<String> warehouseAreaList);

    /**
     * 根据行车获得一个仓库区域的所有库位
     * @param warehouseAreaList 仓库区域列表
     * @return Shelf 库位集合
     */
    List<Shelf> findAllShelfByArea(List<String> warehouseAreaList);

    /**
     * 根据id获取库位
     * @param id 库位主键
     * @param factoryCode 厂区编码
     * @return Shelf 库位
     */
    Shelf findShelfById(Long id, String factoryCode);
}
