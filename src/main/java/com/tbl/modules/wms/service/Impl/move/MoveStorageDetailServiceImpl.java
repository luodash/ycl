package com.tbl.modules.wms.service.Impl.move;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.modules.wms.dao.move.MoveStorageDetailDAO;
import com.tbl.modules.wms.entity.move.MoveStorageDetail;
import com.tbl.modules.wms.service.move.MoveStorageDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 移库处理-详情
 * @author 70486
 */
@Service
public class MoveStorageDetailServiceImpl extends ServiceImpl<MoveStorageDetailDAO, MoveStorageDetail> implements MoveStorageDetailService {

    /**
     * 移库单明细
     */
    @Autowired
    private MoveStorageDetailDAO moveStorageDetailDAO;

    /**
     * 获得导出移库单明细Excel列表数据
     * @param map
     * @return List<MoveStorageDetail>
     */
    @Override
    public List<MoveStorageDetail> getDetailExcelList(Map<String, Object> map) {
        return moveStorageDetailDAO.getDetailExcelList(map);
    }

}
