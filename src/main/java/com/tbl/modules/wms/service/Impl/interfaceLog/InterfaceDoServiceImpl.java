package com.tbl.modules.wms.service.Impl.interfaceLog;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.Query;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.wms.dao.interfacelog.InterfaceDoDAO;
import com.tbl.modules.wms.entity.interfacelog.InterfaceDo;
import com.tbl.modules.wms.entity.outstorage.OutStorageDetail;
import com.tbl.modules.wms.service.interfacelog.InterfaceDoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 接口执行实现类
 * @author 70486
 **/
@Service
public class InterfaceDoServiceImpl extends ServiceImpl<InterfaceDoDAO, InterfaceDo> implements InterfaceDoService {

	/**
	 * 离线接口执行表
	 */
	@Autowired
	private InterfaceDoDAO interfaceDoDAO;
	
	/**
	 * 插入离线执行接口表
	 * @param code 接口编码
	 * @param name 接口名称
	 * @param requestString 请求报文
	 * @param operateTime 操作时间
	 * @param spesign 特殊标志
	 * @param shipno 提货单号
     */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void insertDo(String code, String name, String requestString, String operateTime, String qacode, String batchNo, Long spesign,
                         String shipno) {
		InterfaceDo interfaceDo = new InterfaceDo();
    	interfaceDo.setInterfacecode(code);
    	interfaceDo.setInterfacename(name);
    	if (shipno.startsWith("PDD") && StringUtils.isBlank(qacode)){
    		interfaceDo.setCblogParams(requestString);
		}else {
    		interfaceDo.setParamsinfo(requestString);
		}
    	interfaceDo.setQacode(qacode);
    	interfaceDo.setBatchNo(batchNo);
    	interfaceDo.setSpesign(spesign);
    	interfaceDo.setShipno(shipno);

        interfaceDoDAO.insert(interfaceDo);
    }

	/**
	 * 获取接口离线执行维护列表的数据
	 * @param map 参数条件
	 * @return PageUtils
	 */
	@Override
	public PageUtils queryPage(Map<String,Object> map){
		String interfacecode = (String)map.get("interfacecode");
		String batchNo = (String)map.get("batchNo");
		String shipNo = (String)map.get("shipNo");
		String qaCode = (String)map.get("qaCode");
		String success = (String)map.get("success");
		Page<InterfaceDo> page = this.selectPage(new Query<InterfaceDo>(map).getPage(), new EntityWrapper<InterfaceDo>()
				.like(StringUtils.isNotBlank(interfacecode),"INTERFACECODE", interfacecode != null ? interfacecode.trim() : null)
				.like(StringUtils.isNotBlank(batchNo),"BATCH_NO", batchNo != null ? batchNo.trim() : null)
				.like(StringUtils.isNotBlank(shipNo), "SHIPNO", shipNo != null ? shipNo.trim() : null)
				.like(StringUtils.isNotBlank(qaCode),"QACODE", qaCode != null ? qaCode.trim() : null)
				.like(StringUtils.isNotBlank(success),"ISSUCCESS",success != null ? success.trim() : null));
		return new PageUtils(page.setRecords(page.getRecords()));
	}

	/**
	 * 批量更新接口离线执行表状态
	 * @param lstOutStorageDetail 发货单明细
	 * @param state 状态
	 */
	@Override
	public void updateStateBatches(List<OutStorageDetail> lstOutStorageDetail, String state){
		interfaceDoDAO.updateStateBatches(lstOutStorageDetail, state);
	}

	/**
	 * 更新接口执行表
	 * @param interfaceDo 接口数据
	 */
	@Override
	public void updateInterfaceDoByid(InterfaceDo interfaceDo){
		interfaceDoDAO.updateInterfaceDoByid(interfaceDo);
	}

	/**
	 * 获取定时任务需要执行的离线接口
	 * @return InterfaceDo
	 */
	@Override
	public List<InterfaceDo> findJobsInterfaceDos(){
		return interfaceDoDAO.findJobsInterfaceDos();
	}
}
