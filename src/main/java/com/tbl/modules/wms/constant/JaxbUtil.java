package com.tbl.modules.wms.constant;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;

/**
 * 将xml报文体转为bean
 * @author 70486
 */
public class JaxbUtil {

    public static<T> T xmlToBean(String xmlPath,Class<T> load) throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(load);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        return (T)unmarshaller.unmarshal(new StringReader(xmlPath));
    }


}
