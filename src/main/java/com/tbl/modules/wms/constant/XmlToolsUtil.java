package com.tbl.modules.wms.constant;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import java.util.Map;

/**
 * xml转换工具类
 * @author 70486
 */
public class XmlToolsUtil {

    /**
     * 获取到截取的报文字符串
     * @param xml 原始报文
     * @return String 去头去尾后的报文主体
     */
    public static String getSubXml(String xml) {
    	String xmlData;
        int startIndex = xml.indexOf("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        if(startIndex==-1) {
            startIndex = xml.indexOf("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
        }
        
        int endIndex = xml.indexOf("</X_RESPONSE_DATA>");
        
        if(startIndex!=-1&&endIndex!=-1) {
        	xmlData = xml.substring(startIndex, endIndex);
        	return xmlData;
        }else {
        	return "false";
        }
    }
    
    /**
     * 获取到他们接口执行失败返回的失败信息截取的字符串
     * @param xml
     * @return String
     */
    public static String getFailReturnXml(String xml) {
    	String xmlData = xml;
        int startIndex = xml.indexOf("<X_RETURN_MESG>");
        int endIndex = xml.indexOf("</X_RETURN_MESG>");
        if(startIndex!=-1&&endIndex!=-1){
        	xmlData = xml.substring(startIndex, endIndex);
        }
        return xmlData;
    }
    
    /**
     * MES获取到截取的字符串
     * @param xml
     * @return String
     */
    public static String getMesSubXml(String xml) {
    	String xmlData = xml;
        int startIndex = xml.indexOf("<result>");
        int endIndex = xml.indexOf("</result>");
        if(startIndex!=-1&&endIndex!=-1){
        	xmlData = xml.substring(startIndex, endIndex+9);
        }
        return xmlData;
    }
    
    /**
     * RFID写卡获取到截取的字符串
     * @param xml
     * @return String
     */
    public static String gerfidsubxml(String xml) {
    	String xmlData = xml;
        int startIndex = xml.indexOf("<writeEpcResult>");
        int endIndex = xml.indexOf("</writeEpcResult>");
        if(startIndex!=-1&&endIndex!=-1){
        	xmlData = xml.substring(startIndex, endIndex+17);
        }
        System.out.println("xmlData:" + xmlData);
        return xmlData;
    }

    /**
     * 发动给ebs的请求报文
     * @param interfaceCode
     * @param xmlText
     * @return String
     */
    public static String getRequsetXml(String interfaceCode, String xmlText) {
        StringBuilder stringBuilder = new StringBuilder();
        if (EbsInterfaceEnum.FEWMS031.equals(interfaceCode)){
            stringBuilder.append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:cux='http://xmlns.oracle.com/apps/cux/soaprovider/plsql/cux_0_ws_server_prg/' xmlns:inv='http://xmlns.oracle.com/apps/cux/soaprovider/plsql/cux_0_ws_server_prg/invokefmsws/'><soapenv:Header><wsse:Security xmlns:wsse='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd' xmlns='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd' xmlns:env='http://schemas.xmlsoap.org/soap/envelope/' soapenv:mustUnderstand='1'><wsse:UsernameToken xmlns:wsse='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd' xmlns='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd'><wsse:Username>FE_WSADMIN</wsse:Username>  <wsse:Password Type='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText'>wsadmin</wsse:Password></wsse:UsernameToken></wsse:Security><cux:SOAHeader><cux:Responsibility>FND_REP_APP</cux:Responsibility><cux:RespApplication>FND</cux:RespApplication><cux:SecurityGroup>STANDARD</cux:SecurityGroup><cux:NLSLanguage>SIMPLIFIED CHINESE</cux:NLSLanguage><cux:Org_Id></cux:Org_Id></cux:SOAHeader></soapenv:Header><soapenv:Body><inv:InputParameters><inv:P_IFACE_CODE>");
        }else {
            stringBuilder.append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:cux='http://xmlns.oracle.com/apps/cux/soaprovider/plsql/cux_0_ws_server_prg/' xmlns:inv='http://xmlns.oracle.com/apps/cux/soaprovider/plsql/cux_0_ws_server_prg/invokefmsws/'><soapenv:Header><wsse:Security xmlns:wsse='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd' xmlns='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd' xmlns:env='http://schemas.xmlsoap.org/soap/envelope/' soapenv:mustUnderstand='1'><wsse:UsernameToken xmlns:wsse='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd' xmlns='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd'><wsse:Username>FE_WSADMIN</wsse:Username>  <wsse:Password Type='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText'>wsadmin</wsse:Password></wsse:UsernameToken></wsse:Security><cux:SOAHeader><cux:Responsibility>FND_REP_APP</cux:Responsibility><cux:RespApplication>FND</cux:RespApplication><cux:SecurityGroup>STANDARD</cux:SecurityGroup><cux:NLSLanguage>AMERICAN</cux:NLSLanguage><cux:Org_Id></cux:Org_Id></cux:SOAHeader></soapenv:Header><soapenv:Body><inv:InputParameters><inv:P_IFACE_CODE>");
        }
        stringBuilder.append(interfaceCode);
        stringBuilder.append("</inv:P_IFACE_CODE><inv:P_BATCH_NUMBER>32232323232111</inv:P_BATCH_NUMBER><inv:P_REQUEST_DATA>");
        stringBuilder.append("<![CDATA[<?xml version=\"1.0\" encoding=\"utf-8\"?><data><header><sourcecode>");
        stringBuilder.append(interfaceCode);
        stringBuilder.append("</sourcecode><targetcode>01</targetcode><formatcode>");
        stringBuilder.append(interfaceCode);
        stringBuilder.append("</formatcode><batchnum>1</batchnum><batchcount>1</batchcount>");
        stringBuilder.append("<subbatchcount>1</subbatchcount><ctldate>2015-02-01</ctldate>");
        stringBuilder.append("<senddate>2015-02-01 08:40:11</senddate><orgcode>");
        stringBuilder.append(interfaceCode);
        stringBuilder.append("</orgcode>");
        stringBuilder.append(xmlText);
        stringBuilder.append("</header></data>]]>");
        stringBuilder.append("</inv:P_REQUEST_DATA></inv:InputParameters></soapenv:Body></soapenv:Envelope>");
        System.out.println("#####  wms-->ebs  ######"+interfaceCode+"："+stringBuilder.toString());
        return stringBuilder.toString();
    }
    
    /**
     * 获取请求.Net Service的报文
     * @param xmlText 报文信息
     * @param soapAction
     * @return String 
     */
    public static String getNetRequsetXml(String xmlText,String soapAction) {
        StringBuilder sb = new StringBuilder();
        sb.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
        sb.append("<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">");
        sb.append("<soap:Body>");
        sb.append("<").append(soapAction).append(" xmlns=\"http://tempuri.org/\">");
        if(!"writeEpc".equals(soapAction)){
        	sb.append("<para><![CDATA[<data>");
        }
        sb.append(xmlText);
        if(!"writeEpc".equals(soapAction)){
        	sb.append("</data>]]></para>");
        }
        sb.append("</").append(soapAction).append(">");
        sb.append("</soap:Body>");
        sb.append("</soap:Envelope>");
        return sb.toString();
    }


    /**
     * 获取返回到ebs的报文
     * @param map
     * @return string
     */
    public static String getResponseXml(Map<String, Object> map) {
        /*拼接xml报文*/
        Document document1;

        //这是在创建一个根节点
        Element root = DocumentHelper.createElement("wsinterface");
        //把根节点变成一个Document 对象方便添加子节点
        document1 = DocumentHelper.createDocument(root);

        Element header = root.addElement("header");
        //接口调用
        Element code = header.addElement("code");
        code.setText("");
        //接口调用
        Element name = header.addElement("name");
        name.setText("");

        Element body = root.addElement("body");
        Element data = body.addElement("data");
        data.setText("");

        Element msg = data.addElement("msg");
        msg.setText(map.get("msg").toString());

        Element code1 = data.addElement("code");
        code1.setText(map.get("code").toString());

        if (map.get("batchNo") != null) {
            Element batchNo = data.addElement("batchNo");
            batchNo.setText(map.get("batchNo").toString());
        }
        return document1.asXML();
    }
    
}
