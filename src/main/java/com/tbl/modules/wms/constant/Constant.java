package com.tbl.modules.wms.constant;

/**
 * 常量类
 * @author 70486
 **/
public class Constant {

    /**调拨、出库标识添加或者编辑页面：添加*/
    public final static String PAGE_TYPE_ADD = "add";
    /**调拨、出库标识添加或者编辑页面：编辑*/
    public final static String PAGE_TYPE_UPDATE = "update";
    /**调拨单、移库单、盘点计划，每日更新流水号：调拨单号*/
    public final static String SERIAL_ALLO = "allo";
    /**调拨单、移库单、盘点计划，每日更新流水号：移库单号*/
    public final static String SERIAL_MOVE = "move";
    /**ID为-1表示新增*/
    public final static int SIGN_ADD = -1;

    public final static int INT_ZERO = 0;
    public final static int INT_ONE = 1;
    public final static int INT_TWO = 2;
    public final static int INT_THREE = 3;
    public final static int INT_FOUR = 4;
    public final static int INT_FIVE = 5;

    public final static long LONG_ZERO = 0L;
    public final static long LONG_ONE = 1L;
    public final static long LONG_TWO = 2L;

    public final static String STRING_ONE = "1";
    public final static String STRING_TWO = "2";
    public final static String STRING_THREE = "3";
    public final static String STRING_FOUR = "4";
    public final static String STRING_FIVE = "5";

    /**退库回生产线中的货物类型：已装包的zb  库存中的kc*/
    public final static String RETURN_STORAGE_ZB = "zb";
    public final static String RETURN_STORAGE_KC = "kc";

    public final static int SIZE = 99999;

    public final static String sdfDay = "yyyy-MM-dd";

    public final static String sdfTime = "yyyy-MM-dd HH:mm:ss";

    /**
     * 菜单屏蔽
     */
    public final static String MASK_MENU = "9999";
    /**
     * web端菜单显示
     */
    public final static String WEB_SHWO_MENU = "1";
}
