package com.tbl.modules.wms.constant;

/**
 * EBS  接口：编码
 * @author 70486
 */
public class EbsInterfaceEnum {

    /**
     * EBS接口-基础数据-物料
     */
    public final static String FEWMS001 = "FEWMS001";
    /**
     * EBS接口-出库接口
     */
    public final static String FEWMS002 = "FEWMS002";
    /**
     * WMS接口-销售退货数据同步
     */
    public final static String FEWMS003 = "FEWMS003";
    /**
     * WMS接口-采购退货
     */
    public final static String FEWMS004 = "FEWMS004";
    /**
     * WMS接口-保留接口
     */
    public final static String FEWMS005 = "FEWMS005";
    /**
     * WMS接口-发货单接口
     */
    public final static String FEWMS006 = "FEWMS006";
    /**
     * EBS接口-发货单修改接口
     */
    public final static String FEWMS007 = "FEWMS007";
    /**
     * EBS接口-调拨移库接口
     */
    public final static String FEWMS008 = "FEWMS008";
    /**
     * EBS接口-出库单打印
     */
    public final static String FEWMS009 = "FEWMS009";
    /**
     * WMS接口-报废接口
     */
    public final static String FEWMS010 = "FEWMS010";
    /**
     * EBS接口-盘点接口
     */
    public final static String FEWMS011 = "FEWMS011";
    /**
     * EBS接口-基础数据-厂区
     */
    public final static String FEWMS012 = "FEWMS012";
    /**
     * EBS接口-基础数据-仓库
     */
    public final static String FEWMS013 = "FEWMS013";
    /**
     * EBS接口-基础数据-库位
     */
    public final static String FEWMS014 = "FEWMS014";
    /**
     * EBS接口-按米出库
     */
    public final static String FEWMS015 = "FEWMS015";
    /**
     * EBS接口-分盘
     */
    public final static String FEWMS016 = "FEWMS016";
    /**
     * WMS接口-标签信息初始化
     */
    public final static String FEWMS017 = "FEWMS017";
    /**
     * EBS接口-基础数据-盘具
     */
    public final static String FEWMS019 = "FEWMS019";
    /**
     * EBS接口-盘点入库（盘盈）
     */
    public final static String FEWMS020 = "FEWMS020";
    /**
     * EBS接口-销售退货入库
     */
    public final static String FEWMS024 = "FEWMS024";
    /**
     * EBS接口-外采入库
     */
    public final static String FEWMS025 = "FEWMS025";
    /**
     * EBS接口-同步外协采购的电缆数据
     */
    public final static String FEWMS021 = "FEWMS021";
    /**
     * EBS接口-基础数据-车间
     */
    public final static String FEWMS026 = "FEWMS026";
    /**
     * EBS接口-盘点审核
     */
    public final static String FEWMS028 = "FEWMS028";
    /**
     * EBS接口-分盘
     */
    public final static String FEWMS030 = "FEWMS030";
    /**
     * MES生产入库
     */
    public final static String FEMES001 = "FEMES001";
    /**
     * 打印物资流转单
     */
    public final static String FEWMS031 = "FEWMS031";

    //WMS二期采购订单接口
    public final static String FEWMS201 = "FEWMS201";
    //WMS二期接口-送货单接口
    public final static String FEWMS202 = "FEWMS202";

    /**
     * WMS二期接口-紧急物料审批from OA
     */
    public final static String FEWMS203 = "FEWMS203";

    /**
     * WMS二期接口 OA紧急物料审批结果返回
     */
    public final static String FEWMS204 = "FEWMS204";
}
