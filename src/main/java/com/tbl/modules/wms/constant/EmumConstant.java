package com.tbl.modules.wms.constant;

/**
 * 枚举常量类
 * @author 70486
 */
public class EmumConstant {

    /**
     * 其他出库类型
     */
    public enum OtherOutstorageType {

        /**
         * 退库回生产线
         */
        RETURN_PRODUCTION(1,"退库回生产线"),
        /**
         * 采购退货
         */
        PURCHASE_RETURN(2,"采购退货"),
        /**
         * 报废
         */
        SCRAP(3,"报废"),
        /**
         * 盘亏出库
         */
        INVENTORY_LOSSESS(4,"盘亏出库");

        private final Integer code;
        private final String name;

        OtherOutstorageType(Integer code,String name) {
            this.code = code;
            this.name = name;
        }
        public Integer getCode() {
            return code;
        }

        public String getName() {
            return name;
        }

    }

    /**
     * 是否单条移库（主界面直接扫入）
     */
    public enum mainInterfaceScanning {

        YES(1,"是"),
        NO(0,"否");

        private final Integer code;
        private final String name;

        mainInterfaceScanning(Integer code,String name) {
            this.code = code;
            this.name = name;
        }
        public Integer getCode() {
            return code;
        }

        public String getName() {
            return name;
        }

    }

    /**
     *  库位变更状态 1:未开始 2.开始变更出库 3.完成变更出库 4.开始变更入库 5.完成变更入库
     */
    public enum locationChangeStatus {

        NOT_START(1,"未开始"),
        START_CHANGE_OUT(2,"开始变更出库"),
        COMPLETE_CHANGE_OUT(3,"完成变更出库"),
        START_CHANGE_IN(4,"开始变更入库"),
        COMPLETE_CHANGE_IN(5,"完成变更入库");

        private final Integer code;
        private final String name;

        locationChangeStatus(Integer code,String name) {
            this.code = code;
            this.name = name;
        }
        public Integer getCode() {
            return code;
        }

        public String getName() {
            return name;
        }

    }

    /**
     *  行车状态（0，入库中，1，出库中，2，闲置）
     */
    public enum carState {

        INSTORAGING(0,"入库中"),
        OUTSTORAGING(1,"出库中"),
        UNUSED(2, "闲置");

        private final Integer code;
        private final String name;

        carState(Integer code,String name) {
            this.code = code;
            this.name = name;
        }
        public Integer getCode() {
            return code;
        }

        public String getName() {
            return name;
        }

    }

    /**
     *  入库状态  1.未确认 2.已包装  3.入库中  4.入库完成 5.已退库
     */
    public enum instorageDetailState {

        UNCONFIRM(1,"未确认"),
        PACKAGED(2,"已包装"),
        STORAGEING(3, "入库中"),
        STORAGED(4, "入库完成"),
        RETIRED(5,"已退库");

        private final Integer code;
        private final String name;

        instorageDetailState(Integer code,String name) {
            this.code = code;
            this.name = name;
        }
        public Integer getCode() {
            return code;
        }

        public String getName() {
            return name;
        }

    }

    /**
     *  吊装状态
     */
    public enum hoistState {

        YES(1,"已吊装"),
        NO(2,"未吊装");

        private final Integer code;
        private final String name;

        hoistState(Integer code,String name) {
            this.code = code;
            this.name = name;
        }
        public Integer getCode() {
            return code;
        }

        public String getName() {
            return name;
        }

    }

    /**
     *  复绕状态
     */
    public enum rewindState {

        YES(1,"已复绕"),
        NO(2,"未复绕");

        private final Integer code;
        private final String name;

        rewindState(Integer code,String name) {
            this.code = code;
            this.name = name;
        }
        public Integer getCode() {
            return code;
        }

        public String getName() {
            return name;
        }

    }

    /**
     *  接口管理表接口执行状态
     *  （0.新增;1.成功;2.失败;3.进行中;4.平台出库失败【查看用，不支持循环调用】；）
     */
    public enum interfaceDoState {

        NEW("0","新增"),
        SUCCESS("1","成功"),
        FAIL("2","失败"),
        DOING("3","挂起"),
        PLATFORM_OUTSTORAGE_FAIL("4","平台出库失败");

        private final String code;
        private final String name;

        interfaceDoState(String code,String name) {
            this.code = code;
            this.name = name;
        }
        public String getCode() {
            return code;
        }

        public String getName() {
            return name;
        }

    }

    /**
     *  盘点审核状态
     */
    public enum inventoryState {

        NEW(0,"新增计划"),
        IN_AUDIT(1,"盘点登记中"),
        REGISTER(2,"盘点审核中"),
        AUDITED(3,"盘点审核完成");

        private final Integer code;
        private final String name;

        inventoryState(Integer code,String name) {
            this.code = code;
            this.name = name;
        }
        public Integer getCode() {
            return code;
        }

        public String getName() {
            return name;
        }

    }

    /**
     *  盘点结果
     */
    public enum inventoryResult {

        NORMAL(1,"正常"),
        SURPLUS(2,"盘盈"),
        DEFICIT(3,"盘亏");

        private final Integer code;
        private final String name;

        inventoryResult(Integer code,String name) {
            this.code = code;
            this.name = name;
        }
        public Integer getCode() {
            return code;
        }

        public String getName() {
            return name;
        }

    }

    /**
     *  是否盘点
     */
    public enum isInventoryed {

        NO(0,"未盘点"),
        YES(1,"已盘点");

        private final Integer code;
        private final String name;

        isInventoryed(Integer code,String name) {
            this.code = code;
            this.name = name;
        }
        public Integer getCode() {
            return code;
        }

        public String getName() {
            return name;
        }

    }

    /**
     *  入库类型
     *  入库类型（0自产（正常入库）；1退货入库（销售退货）；2外采入库；3盘盈入库）
     */
    public enum storageType {

        NORMAL_PRODUCTION(0,"正常生产"),
        SALES_RETURN(1,"销售退货"),
        OUTSOURCING_PROCUREMENT(2,"外协采购"),
        INVENTORY_SUPERPLUS(3,"盘盈入库");

        private final Integer code;
        private final String name;

        storageType(Integer code,String name) {
            this.code = code;
            this.name = name;
        }
        public Integer getCode() {
            return code;
        }

        public String getName() {
            return name;
        }

    }

    /**
     *  发货状态
     */
    public enum outstorageDetailState {

        UNOUTSTORAGE(1,"未出库"),
        OUTSTORAGING(2,"出库中"),
        OUTSTORAGED(3,"出库完成"),
        OUTSTORAGE_PREPARE(4,"预出库");

        private final Integer code;
        private final String name;

        outstorageDetailState(Integer code,String name) {
            this.code = code;
            this.name = name;
        }
        public Integer getCode() {
            return code;
        }

        public String getName() {
            return name;
        }

    }

    /**
     *  库存状态
     */
    public enum storageInfoState {

        FROZEN(0,"冻结（入库中）"),
        NORMAL(1,"正常"),
        LOCKING(2,"锁定（出库中）"),
        PREOUT(3, "预出库"),
        OUTBOUND(4, "已出库");


        private final Integer code;
        private final String name;

        storageInfoState(Integer code,String name) {
            this.code = code;
            this.name = name;
        }
        public Integer getCode() {
            return code;
        }

        public String getName() {
            return name;
        }

    }

    /**
     *  库位状态：0.占用状态；1.可用状态；2.可选不推荐（分支）；199.手动废弃
     */
    public enum sheflState {

        OCCUPY(0,"占用"),
        AVAILABLE(1,"可用"),
        NOTRECOMMENDED(2,"可用不推荐（分支）"),
        ABANDONMENT(199, "废弃");


        private final Integer code;
        private final String name;

        sheflState(Integer code,String name) {
            this.code = code;
            this.name = name;
        }
        public Integer getCode() {
            return code;
        }

        public String getName() {
            return name;
        }
    }

    /**
     *  扫码枪出库模式：1预出库模式，2直接出库模式
     */
    public enum deliveryMode {

        BEFOREHAND(1,"预出库"),
        ACTUAL(2,"实时出库");


        private final Integer code;
        private final String name;

        deliveryMode(Integer code,String name) {
            this.code = code;
            this.name = name;
        }
        public Integer getCode() {
            return code;
        }

        public String getName() {
            return name;
        }
    }

    /**
     *  wms应用平台出库，是否必须先下架后才能出库
     */
    public enum pickingDismount {

        YES("true","是"),
        NO("false","否");


        private final String code;
        private final String name;

        pickingDismount(String code,String name) {
            this.code = code;
            this.name = name;
        }
        public String getCode() {
            return code;
        }

        public String getName() {
            return name;
        }
    }

    /**
     *  库位区域划分
     */
    public enum shelfArea {

        FINISHED_PRODUCT_AREA(1,"成品电缆存储库区"),
        MACHINE_EDGE_AREA(2,"机边库区"),
        SORTING_AREA(3,"分拣库区"),
        EMPTY_TRAY_AREA(4,"空托盘库区"),
        WASTE_AREA(5,"废料库区"),
        RAW_MATERIAL_AREA(6,"原材料存储库区"),
        EMPTY_DISH_AREA(7,"空盘具库区");


        private final Integer code;
        private final String name;

        shelfArea(Integer code,String name) {
            this.code = code;
            this.name = name;
        }
        public Integer getCode() {
            return code;
        }

        public String getName() {
            return name;
        }
    }

    /**
     *  AGV任务类型 (叫料/入库/出库/移库/盘点/拣货/转运/插单)，不填默认为叫料
     */
    public enum agvTaskType {

        CALLING_FOR_MATERIAL(1,"叫料"),
        WAREHOUSING(2,"入库"),
        WEAREHOUSE_OUT(3,"出库"),
        MOVE_THE_STOREHOUSE(4,"移库"),
        INVENTORY(5,"盘点"),
        PICKING(6,"拣货"),
        TRANSPORT(7,"转运"),
        INSERT_ORDER(8,"插单");


        private final Integer code;
        private final String name;

        agvTaskType(Integer code,String name) {
            this.code = code;
            this.name = name;
        }
        public Integer getCode() {
            return code;
        }

        public String getName() {
            return name;
        }
    }

    /**
     *  AGV执行情况共有6种情况；1=开始、3=开始取货、4=取货完成、5=开始卸货、6=卸货完成、2=完成、7=取消
     */
    public enum agvExecuteStatus {

        START(1,"开始"),
        START_PICK_UP(3,"开始取货"),
        FINISHED_PICK(4,"取货完成"),
        START_DISBURDEN(5,"开始卸货"),
        FINISHED_DISBURDEN(6,"卸货完成"),
        COMPLETED(2,"完成"),
        CONCEL(7,"取消");


        private final Integer code;
        private final String name;

        agvExecuteStatus(Integer code,String name) {
            this.code = code;
            this.name = name;
        }
        public Integer getCode() {
            return code;
        }

        public String getName() {
            return name;
        }
    }
}
