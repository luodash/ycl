package com.tbl.modules.wms.constant;

import lombok.Getter;
import lombok.Setter;

/**
 *  接口返回结果封装
 * @author 70486
 */
@Getter
@Setter
public class PdaResult {
    /**
     * 0 success 1 fail
     */
    public int type = 0;
    public String msg = "success";
    public Object data = "";
    public int count = 0;

    public PdaResult() {
    }

    public PdaResult(Object data) {
        this.data = data;
    }

    public PdaResult(int type, String msg, Object data) {
        this.type = type;
        this.msg = msg;
        this.data = data;
    }

    public PdaResult(Object data,int count) {
        this.data = data;
        this.count = count;
    }

}
