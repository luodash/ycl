package com.tbl.modules.wms.entity.requestxml;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *  FEWMS006  WMS接口-发货单接口
 * @author 70486
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "batchno",
        "detialid",
        "zhibao",
        "mishu",
        "uom"
})
@XmlRootElement(name = "batchsline")
@Getter
@Setter
@ToString
public class FEWMS006_batchsline {
	/**
	 * 批次号
	 */
	public String batchno;
	/**
	 * 顺序明细id
	 */
    public String detialid;
    /**
     * 质保号
     */
    public String zhibao;
    /**
     * 数量
     */
    public String mishu;
    /**
     * 单位
     */
    public String uom;
}
