package com.tbl.modules.wms.entity.baseinfo;

import com.baomidou.mybatisplus.annotations.TableField;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Json字符串转为实体类-实体对象
 * @author 70486
 */
@Getter
@Setter
public class InventoryJson implements Serializable {

    @TableField(exist = false)
    private String key;

    @TableField(exist = false)
    private String value;


}
