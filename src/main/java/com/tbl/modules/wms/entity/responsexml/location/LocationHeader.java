package com.tbl.modules.wms.entity.responsexml.location;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 库位header FEWMS014  WMS接口-基础数据-库位
 * @author 70486
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "",propOrder = {
        "code",
        "msg"
})
@XmlRootElement(name = "header")
@Getter
@Setter
@ToString
public class LocationHeader {
    public String code;
    public String msg;
}
