package com.tbl.modules.wms.entity.responsexml.location;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * FEWMS014  WMS接口-基础数据-库位
 * @author 70486
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "id",
        "code",
        "name",
        "warehouseno",
        "orgid"
})
@XmlRootElement(name = "location")
@Getter
@Setter
@ToString
public class LocationXml {

    public String id;
    public String code;
    public String name;
    public String warehouseno;
    public String orgid;
}
