package com.tbl.modules.wms.entity.responsexml.location;


import com.tbl.modules.wms.entity.responsexml.body;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * FEWMS014  WMS接口-基础数据-库位
 * @author 70486
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "header",
        "body"
})
@Getter
@Setter
@XmlRootElement(name = "wsinterface")
@ToString
public class LocationWsinterface {

    public LocationHeader header;
    public body body;

}
