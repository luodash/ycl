package com.tbl.modules.wms.entity.baseinfo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 盘点扫码提交日志信息
 * @author 70486
 */
@TableName("YDDL_EXTERNALCABLE")
@Getter
@Setter
@ToString
public class Externalcable implements Serializable {

	/**
	 * 主键
	 */
    @TableId(value = "ID")
    private Long id;

    /**
     * 质保编号（质保单号）
     */
    @TableField(value = "QACODE")
    private String qacode;

    /**
     * 盘点是否提交（0否；1是）
     */
    @TableField(value = "NUM")
    private Integer num;
    
    /**
     * 扫码人姓名
     */
    @TableField(value = "BUYORDERNO")
    private String buyorderno;
    
    /**
     * barcodeType
     */
    @TableField(value = "RECEIPTNO")
    private String receiptno;
    
    /**
     * 批次号
     */
    @TableField(value = "INSPECTNO")
    private String inspectno;
    
    /**
     * 盘点提交人ID
     */
    @TableField(value = "BUYORDERLINE")
    private Long buyorderline;
    
    /**
     * rfid
     */
    @TableField(value = "ORDERDISTRIBUTE")
    private String orderdistribute;
    
    /**
     * 盘点提交人姓名
     */
    @TableField(value = "MATERIALCODE")
    private String materialcode;
    
    /**
     * 下拉框-质保号
     */
    @TableField(exist = false)
    private String text;

    /**
     * 扫码人ID
     */
    @TableField(value = "TRANSACTION_ID")
    private Long transactionId;
    
    /**
     * 状态（1可用；0不可用）
     */
    @TableField(value = "STATE")
    private Long state;

    /**
     * EBS盘点单号
     */
    @TableField(value = "EBS_CODE")
    private String ebsCode;

}
