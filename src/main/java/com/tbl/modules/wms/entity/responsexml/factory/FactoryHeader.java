package com.tbl.modules.wms.entity.responsexml.factory;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 厂区header FEWMS012  WMS接口-基础数据-厂区
 * @author 70486
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "plantno",
        "plantname"
})
@XmlRootElement(name = "header")
@Getter
@Setter
@ToString
public class FactoryHeader {

    public String plantno;
    public String plantname;

}

