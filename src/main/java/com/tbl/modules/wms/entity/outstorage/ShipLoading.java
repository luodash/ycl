package com.tbl.modules.wms.entity.outstorage;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 出库管理--装车发运表
 * @author 70486
 */
@TableName("YDDL_SHIPLOADING")
@Getter
@Setter
@ToString
public class ShipLoading implements Serializable {

    /**
     * 主键
     */
    @TableId(value = "ID")
    private Long id;

    /**
     * 装车时间
     */
    @TableField(value = "SHIPLOADTIME")
    private Date shipLoadTime;

    /**
     * 装车人id
     */
    @TableField(value = "SHIPLOADUSERID")
    private Long shipLoadUserId;

    /**
     * 车牌号
     */
    @TableField(value = "LICENSEPLATE")
    private String licensePlate;

    /**
     * 状态
     */
    @TableField(value = "STATE")
    private Integer state;

    /**
     * 装车人名称
     */
    @TableField(exist = false)
    private String shipLoadUserName;


}
