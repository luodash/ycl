package com.tbl.modules.wms.entity.requestxml;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * FEWMS017  标签信息初始化
 * @author 70486
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "org",//组织主键
        "factoryid",//入库生产厂主键
        "materialcode",//物料编码
        "qacode",//质保号
        "entityno",//工单号
        "orderno",//订单编号
        "orderline",//订单行号
        "length",//长度（单位米）
        "segmentno",//段号（字符串）
        "drumname",//盘具编码
        "drummodel",//盘具规格
        "drumoutdia",//盘具外径
        "drumindia",//盘具内径
        "salescode",//营销经理工号
        "salesname",//营销经理姓名
        "batchno",//WMS生成的批次号
        "colour",//颜色
        "weight",//重量
        "uom",//计量单位
        "clientip",//MES终端IP
        "inspectno",//报验单号
        "isoutcoop"//“远东外协安缆”标记字段:Y-远东外协安缆 ，N - 非远东外协安缆
})
@XmlRootElement(name = "data")
@Getter
@Setter
@ToString
public class FEWMS017_data {

    public String org;
    public String factoryid;
    public String materialcode;
    public String qacode;
    public String entityno;
    public String orderno;
    public String orderline;
    public String length;
    public String segmentno;
    public String drumname;
    public String drummodel;
    public String drumoutdia;
    public String drumindia;
    public String salescode;
    public String salesname;
    public String batchno;
    public String colour;
    public String weight;
    public String uom;
    public String clientip;
    public String inspectno;
    public String isoutcoop;
}
