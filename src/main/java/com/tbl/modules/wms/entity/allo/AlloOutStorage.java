package com.tbl.modules.wms.entity.allo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 调拨管理:调出仓库
 * @author 70486
 */
@TableName("YDDL_ALLO_OUTSTORAGE")
@Getter
@Setter
@ToString
public class AlloOutStorage implements Serializable {

    /**
     * 主键
     */
    @TableId(value = "ID")
    private Long id;

    /**
     * 组织机构
     */
    @TableField(value = "ORG")
    private String org;

    /**
     * 物料编码
     */
    @TableField(value = "MATERIALCODE")
    private String materialCode;

    /**
     * 批次号
     */
    @TableField(value = "BATCH_NO")
    private String batchNo;

    /**
     * 质保号
     */
    @TableField(value = "QACODE")
    private String qaCode;

    /**
     * 营销经理工号
     */
    @TableField(value = "SALESCODE")
    private String salesCode;

    /**
     * 营销经理名称
     */
    @TableField(value = "SALESNAME")
    private String salesName;

    /**
     * 主表id
     */
    @TableField(value = "P_ID")
    private Long pId;

    /**
     * RFID编号
     */
    @TableField(value = "RFID")
    private String rfid;

    /**
     * 数量
     */
    @TableField(value = "METER")
    private String meter;

    /**
     *状态
     */
    @TableField(value = "STATE")
    private Integer state;

    /**
     * 仓库编号
     */
    @TableField(value = "WAREHOUSECODE")
    private String warehouseCode;

    /**
     * 行车编号
     */
    @TableField(value = "CARCODE")
    private String carCode;

    /**
     * 库位编码
     */
    @TableField(value = "SHELFCODE")
    private String shelfCode;

    /**
     * 出库人id
     */
    @TableField(value = "OUTSTORAGEID")
    private Long outStorageId;

    /**
     * 出库时间
     */
    @TableField(value = "OUTSTORAGETIME")
    private Date outstoragetime;

    /**
     * 开始出库时间
     */
    @TableField(value = "STARTSTORAGETIME")
    private Date startStorageTime;

    /**
     * 仓库名称
     */
    @TableField(exist = false)
    private String warehouseName;

    /**
     * 行车id
     */
    @TableField(exist = false)
    private Long carId;

    /**
     * 出库类型
     */
    @TableField(exist = false)
    private String outstorageType;

    /**
     * 盘具编码
     */
    @TableField(value = "DISHNUMBER")
    private String dishnumber;

    /**
     * 出库行车工主键
     */
    private Integer outcarerid;
}
