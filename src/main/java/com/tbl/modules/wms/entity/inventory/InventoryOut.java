package com.tbl.modules.wms.entity.inventory;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 库存盘点--盘亏出库数据
 * @author 70486
 */
@TableName("YDDL_INVENTORY_OUT")
@Getter
@Setter
@ToString
public class InventoryOut implements Serializable {

    /**
     * 主键
     */
    @TableId(value = "ID")
    private Long id;

    /**
     * 组织机构
     */
    @TableField(value = "ORG")
    private String org;

    /**
     * 物料编码
     */
    @TableField(value = "MATERIALCODE")
    private String materialcode;

    /**
     * 物料名称
     */
    @TableField(value = "MATERIALNAME")
    private String materialname;

    /**
     * 批次号
     */
    @TableField(value = "BATCH_NO")
    private String batchNo;

    /**
     * 质保号
     */
    @TableField(value = "QACODE")
    private String qacode;

    /**
     * 长度
     */
    @TableField(value = "LENGTH")
    private String length;

    /**
     * 营销经理工号
     */
    @TableField(value = "SALESCODE")
    private String salescode;

    /**
     * 营销经理名称
     */
    @TableField(value = "SALESNAME")
    private String salesname;

    /**
     * 主表id
     */
    @TableField(value = "P_ID")
    private Long pId;

    /**
     * RFID
     */
    @TableField(value = "RFID")
    private String rfid;

    /**
     * 米数
     */
    @TableField(value = "METER")
    private String meter;

    /**
     * 状态
     */
    @TableField(value = "STATE")
    private Integer state;

    /**
     * 入库时间
     */
    @TableField(value = "INSTORAGETIME")
    private Date instoragetime;

    /**
     * 仓库id
     */
    @TableField(value = "WAREHOUSECODE")
    private String warehousecode;

    /**
     * 行车id
     */
    @TableField(value = "CARCODE")
    private String carcode;

    /**
     * 库位编码
     */
    @TableField(value = "SHELFCODE")
    private String shelfcode;

    /**
     * 出库人id
     */
    @TableField(value = "OUTSTORAGEID")
    private Long outStorageId;

    /**
     * 盘具类型
     */
    @TableField(value = "DRUMTYPE")
    private String drumType;

    /**
     * 盘具内径
     */
    @TableField(value = "INNERDIAMETER")
    private Long innerDiameter;

    /**
     * 盘具外径
     */
    @TableField(value = "OUTERDIAMETER")
    private Long outerDiameter;

    /**
     * 规格型号
     */
    @TableField(value = "MODEL")
    private String model;
}
