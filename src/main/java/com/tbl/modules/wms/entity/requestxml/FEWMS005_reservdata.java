package com.tbl.modules.wms.entity.requestxml;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *  FEWMS005  WMS接口-保留接口
 * @author 70486
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
		"seqid",//排序号
        "lotnumber",//工单号
        "ordernum",//订单编号
        "orderline",//订单行号
        "status",//insert or delete
})
@XmlRootElement(name = "reservdata")
@Getter
@Setter
@ToString
public class FEWMS005_reservdata {
    public String seqid;
    public String lotnumber;
    public String ordernum;
    public String orderline;
    public String status;
}
