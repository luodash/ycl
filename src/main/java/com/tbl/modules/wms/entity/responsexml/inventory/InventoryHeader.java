package com.tbl.modules.wms.entity.responsexml.inventory;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * FEWMS028  WMS接口-盘点审核
 * @author 70486
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "code",
        "msg"
})
@XmlRootElement(name = "header")
@Getter
@Setter
@ToString
public class InventoryHeader {

    public String code;
    public String msg;

}
