package com.tbl.modules.wms.entity.outstorage;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 出库管理--装车发运明细表
 * @author 70486
 */
@TableName("YDDL_SHIPLOADING_DETAIL")
@Getter
@Setter
@ToString
public class ShipLoadingDetail implements Serializable {

    /**
     * 主键
     */
    @TableId(value = "ID")
    private Long id;

    /**
     * 物料编码
     */
    @TableField(value = "MATERIALCODE")
    private String materialCode;

    /**
     * 发货单号
     */
    @TableField(value = "SHIP_NO")
    private String shipNo;

    /**
     * 质保单号
     */
    @TableField(value = "QACODE")
    private String qaCode;

    /**
     * 米数
     */
    @TableField(value = "METER")
    private String meter;

    /**
     * 主表id
     */
    @TableField(value = "PID")
    private Long pId;

    /**
     * 创建时间
     */
    @TableField(value = "CREATE_TIME")
    private Date createTime;

}
