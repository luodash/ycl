package com.tbl.modules.wms.entity.outstorage;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 出库详情
 * @author 70486
 */
@TableName("YDDL_OUTSTORAGE_DETAIL")
@Getter
@Setter
@ToString
public class OutStorageDetail implements Serializable {

	/**
	 * 主键
	 */
    @TableId(value = "ID")
    private Long id;

    /**
     * 物料编码
     */
    @TableField(value = "MATERIALCODE")
    private String materialCode;

    /**
     * 批次号
     */
    @TableField(value = "BATCH_NO")
    private String batchNo;

    /**
     * 质保号
     */
    @TableField(value = "QACODE")
    private String qaCode;

    /**
     * 库位编号
     */
    @TableField(value = "SHELFCODE")
    private String shelfCode;

    /**
     * 运输单号
     */
    @TableField(value = "TRANSPORTNO")
    private String transportNo;

    /**
     * 主表id
     */
    @TableField(value = "P_ID")
    private Long pId;

    /**
     * RFID编号
     */
    @TableField(value = "RFID")
    private String rfid;

    /**
     * 数量
     */
    @TableField(value = "METER")
    private String meter;

    /**
     * 1:生效  2:拣货中  3:发货完成
     */
    @TableField(value = "STATE")
    private Integer state;

    /**
     * 1:生效  2:拣货中  3:发货完成
     */
    @TableField(exist = false)
    private String stateStr;

    /**
     * 仓库编号
     */
    @TableField(value = "WAREHOUSECODE")
    private String warehouseCode;

    /**
     * 行车编号
     */
    @TableField(value = "CARCODE")
    private String carCode;

    /**
     * 出库人主键
     */
    @TableField(value = "OUTSTORAGEID")
    private Long outStorageId;

    /**
     * 出库时间
     */
    @TableField(value = "OUTSTORAGETIME")
    private Date outStorageTime;

    /**
     * 下架时间（预出库时间）（发货时间）
     */
    @TableField(value = "STARTSTORAGETIME")
    private Date startStorageTime;

    /**
     * 下架时间（预出库时间）（发货时间）
     */
    @TableField(exist = false)
    private String startStorageTimeStr;

    /**
     * 出库类型
     */
    @TableField(exist = false)
    private Long type;

    /**
     * 出库人名称
     */
    @TableField(exist = false)
    private String pickManName;

    /**
     * 仓库名称
     */
    @TableField(exist = false)
    private String warehouseName;

    /**
     * 销售经理名
     */
    @TableField(exist = false)
    private String salesname;

    /**
     * 行车id
     */
    @TableField(exist = false)
    private Long carId;

    /**
     * 出库时间
     */
    @TableField(exist = false)
    private String outstoragetimestr;

    /**
     * 出库类型
     */
    @TableField(exist = false)
    private String outstorageType;

    @TableField(exist = false)
    private String outstorageType1;

    /**
     * 删除标识
     */
    @TableField(value = "DETAILID")
    private Long detailid;
    
    /**
     * 厂区
     */
    @TableField(value = "ORG")
    private String org;

    /**
     * 合同号
     */
    @TableField(value = "CONTRACTNO")
    private String contractno;

    /**
     * 销售订单号
     */
    @TableField(value = "ORDERNO")
    private String orderno;

    /**
     * 销售订单行号
     */
    @TableField(value = "ORDERLINE")
    private String orderline;

    /**
     *运输公司
     */
    @TableField(value = "TRANSPORTNAME")
    private String transportname;

    /**
     * 收货单位
     */
    @TableField(value = "CUSTOMER")
    private String customer;
    
    /**
     * 开始拣货时间(扫描时间)
     */
    @TableField(value = "STARTPICKTIME")
    private Date startpicktime;

    /**
     * 单位
     */
    @TableField(value = "UNIT")
    private String unit;

    /**
     * 颜色
     */
    @TableField(exist = false)
    private String colour;

    /**
     * 段号
     */
    @TableField(exist = false)
    private String segmentno;

    /**
     * 重量
     */
    @TableField(exist = false)
    private String weight;

    /**
     * 盘号
     */
    @TableField(exist = false)
    private String dishcode;
    
    /**
     * 盘具编码
     */
    @TableField(value = "DISHNUMBER")
    private String dishnumber;
    
    /**
    * 盘具类型
    */
   @TableField(exist = false)
   private String drumType;
	
	/**
    * 盘具内径
    */
   @TableField(exist = false)
   private String innerDiameter;

   /**
    * 盘具外径
    */
   @TableField(exist = false)
   private String outerDiameter;
   
   /**
    * 规格型号
    */
   @TableField(exist = false)
   private String model;
   
   /**
    * 物料名称
    */
   @TableField(exist = false)
   private String materialName;

    /**
     * 扫描时间(发货时，在发货单中做排序用)
     */
    @TableField(value = "SCANTIME")
    private Date scantime;

    /**
     * 发货组织
     */
    @TableField(exist = false)
    private String outstorageOrg;

    /**
     * 发货单号
     */
    @TableField(exist = false)
    private String shipNo;

    /**
     * 打印时间
     */
    @TableField(exist = false)
    private String printtime;

    /**
     * 合同号
     */
    @TableField(exist = false)
    private String contractNo;

    /**
     * 库存表主键
     */
    @TableField(exist = false)
    private Long storageInfoId;

    /**
     * 车牌号
     */
    @TableField(exist = false)
    private String carNo;

    /**
     * 接口离线执行表主键
     */
    @TableField(exist = false)
    private Integer interfaceDoId;

    /**
     * 下架人（预出库人）主键
     */
    @TableField(value = "STARTSTORAGEID")
    private Long startstorageid;

    /**
     * 下架人（预出库人）（发货人）
     */
    @TableField(exist = false)
    private String startstorageName;

    @TableField(exist = false)
    private Long meterNumber;

    /**
     * 出库行车工主键
     */
    private Integer outcarerid;
}
