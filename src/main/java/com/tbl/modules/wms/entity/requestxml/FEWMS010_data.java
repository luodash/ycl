package com.tbl.modules.wms.entity.requestxml;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

import javax.xml.bind.annotation.*;

/**
 * FEWMS010  WMS接口-报废接口
 * @author 70486
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "baofei",
})
@XmlRootElement(name = "data")
@Getter
@Setter
@ToString
public class FEWMS010_data {

    public List<FEWMS010_batchs> baofei ;

}
