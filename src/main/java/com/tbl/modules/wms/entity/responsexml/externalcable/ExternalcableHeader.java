package com.tbl.modules.wms.entity.responsexml.externalcable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * FEWMS021  WMS接口-同步外协采购的电缆数据
 * @author 70486
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "code",
        "name"
})
@XmlRootElement(name = "header")
@Getter
@Setter
@ToString
public class ExternalcableHeader {

    public String code;
    public String name;

}
