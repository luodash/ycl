package com.tbl.modules.wms.entity.responsexml.material;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * FEWMS001  WMS接口-基础数据-物料
 * @author 70486
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "id",
        "code",
        "name",
        "mainUnit",
        "subUnit",
        "conversionRatio",
        "categoryCodeFirst",
        "categoryCodeSecond",
        "categoryCodeThird",
        "categoryCodeFourth",
        "categoryNameFirst",
        "categoryNameSecond",
        "categoryNameThird",
        "categoryNameFourth",
        "standardPackageWeight",
        "remark"
})
@XmlRootElement(name = "material")
@Getter
@Setter
@ToString
public class MaterialXml {

    public String id;
    public String code;
    public String name;
    /**
     * 主单位
     */
    private String mainUnit;

    /**
     * 辅单位
     */
    private String subUnit;

    /**
     * 单位转化比例
     */
    private String conversionRatio;

    /**
     * 物料类别一级编码
     */
    private String categoryCodeFirst;

    /**
     * 物料类别二级编码
     */
    private String categoryCodeSecond;

    /**
     * 物料类别三级编码
     */
    private String categoryCodeThird;

    /**
     * 物料类别四级编码
     */
    private String categoryCodeFourth;

    /**
     * 物料类别一级名称
     */
    private String categoryNameFirst;

    /**
     * 物料类别二级名称
     */
    private String categoryNameSecond;

    /**
     * 物料类别三级名称
     */
    private String categoryNameThird;

    /**
     * 物料类别四级名称
     */
    private String categoryNameFourth;

    /**
     * 标准单包重量
     */
    private String standardPackageWeight;

    /**
     * 物料说明
     */
    private String remark;

}
