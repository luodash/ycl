package com.tbl.modules.wms.entity.responsexml.warehouse;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *  FEWMS013  WMS接口-基础数据-仓库
 * @author 70486
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "orgid",
        "warehouseno",
        "warehousename"
})
@XmlRootElement(name = "warehouse")
@Getter
@Setter
@ToString
public class WarehouseXml {

    public String orgid;
    public String warehouseno;
    public String warehousename;

}