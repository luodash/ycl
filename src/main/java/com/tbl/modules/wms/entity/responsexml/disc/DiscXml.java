package com.tbl.modules.wms.entity.responsexml.disc;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * FEWMS012  WMS接口-基础数据-盘
 * @author 70486
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
       "inventory_item_code",
       "item_name",
       "item_type_name",
       "item_type_detail",
       "d1_mm",
       "d2_mm",
       "l1max_mm",
       "l2_mm",
        "g_kg"
})
@XmlRootElement(name = "dish")
@Getter
@Setter
@ToString
public class DiscXml {
    /**
     * 盘具编码
     */
    private String inventory_item_code;
    /**
     * 盘具 规格型号
     */
    private String item_name;
    /**
     *盘具分类
     */
    private String item_type_name;
    /**
     *盘具子分类
     */
    private String item_type_detail;
    /**
     *盘具外径
     */
    private String d1_mm;
    /**
     *盘具内径
     */
    private String d2_mm;
    /**
     *外宽
     */
    private String l1max_mm;
    /**
     *内宽
     */
    private String l2_mm;
    /**
     *最大载重
     */
    private String g_kg;

}
