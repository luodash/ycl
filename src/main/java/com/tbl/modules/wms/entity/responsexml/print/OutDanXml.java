package com.tbl.modules.wms.entity.responsexml.print;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

/**
 * 出库打印单详细信息
 * @author 70486
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "outno",
        "deliverywarehouse",
        "undertake",
        "outdate",
        "invoice",
        "delivery",
        "shipment",
        "handle",
        "pickup",
        "pickuptel",
        "deliveryaddress",
        "undertaketel",
        "linedetail"
})
@XmlRootElement(name = "outdan")
@Getter
@Setter
@ToString
public class OutDanXml {

    /**
     * 出库单
     */
    public String outno;
    /**
     * 发货库房
     */
    public String deliverywarehouse;
    /**
     * 承办单位（人）
     */
    public String undertake;
    /**
     * 出库日期
     */
	public String outdate;
    /**
     * 开票人
     */
    public String invoice;
    /**
     * 发货人
     */
    public String delivery;
    /**
     * 承运人
     */
	public String shipment;
    /**
     * 经办
     */
	public String handle;
    /**
     * 接货人
     */
	public String pickup;
    /**
     * 联系电话（接货人）
     */
	public String pickuptel;
    /**
     * 送货地址
     */
    public String deliveryaddress;
    /**
     * -承办单位（人）电话
     */
	public String undertaketel;
    /**
     * 批次号明细
     */
	public List<LinedetailXml> linedetail;
}
