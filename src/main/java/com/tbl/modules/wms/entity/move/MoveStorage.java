package com.tbl.modules.wms.entity.move;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 移库处理--移库主表
 * @author 70486
 */
@TableName("YDDL_MOVESTORAGE")
@Getter
@Setter
@ToString
public class MoveStorage implements Serializable {

    /**
     * 主键
     */
    @TableId(value = "ID")
    private Long id;

    /**
     * 移库单编号
     */
    @TableField(value = "MOVECODE")
    private String moveCode;

    /**
     * 仓库编码
     */
    @TableField(value = "WAREHOUSECODE")
    private String warehouseCode;
    
    /**
     * 仓库名称
     */
    @TableField(exist = false)
    private String warehouseName;

    /**
     * 移库时间
     */
    @TableField(value = "MOVETIME")
    private Date moveTime;
    
    /**
     * 移库时间
     */
    @TableField(exist = false)
    private String moveTimeStr;

    /**
     * 移库完成时间
     */
    @TableField(value = "COMPLETETIME")
    private Date completeTime;
    
    /**
     * 移库完成时间
     */
    @TableField(exist = false)
    private String completeTimeStr;

    /**
     * 移库人
     */
    @TableField(value = "USER_ID")
    private Long userId;

    /**
     * 是否单条移库（主界面直接扫入）：1是；2否
     */
    @TableField(value = "STATE")
    private Integer state;

    /**
     * 是否单条移库（主界面直接扫入）：1是；2否
     */
    @TableField(exist = false)
    private String stateStr;

    /**
     * 发货单编号
     */
    @TableField(value = "SHIPNO")
    private String shipNo;

    /**
     * 备注
     */
    @TableField(value = "INFO")
    private String info;

    /**
     * 仓库id
     */
    @TableField(exist = false)
    private Long warehouseId;

    /**
     * 厂区id
     */
    @TableField(exist = false)
    private Long factoryId;
    
    /**
     * 用户姓名
     */
    @TableField(exist = false)
    private String userName;
    
    /**
     * 移库状态
     */
    @TableField(exist = false)
    private String moveState;
    
    /**
     * 厂区编码
     */
    @TableField(value = "FACTORYCODE")
    private String factoryCode;
    
    /**
     * 移库仓库所在厂区名称
     */
    @TableField(exist = false)
    private String factoryName;
    
    /**
     * 移库仓库编码_名称
     */
    @TableField(exist = false)
    private String warehouseCodeName;
    
    /**
     * 移库仓库所在厂区编码_名称
     */
    @TableField(exist = false)
    private String factoryCodeName;
    
    /**
     * 移库出库首笔开始时间
     */
    @TableField(exist = false)
    private Date startOutTime;

    /**
     * 移库出库首笔开始时间字符串
     */
    @TableField(exist = false)
    private String startOutTimeStr;
    
    /**
     * 移库入库最后一笔结束时间
     */
    @TableField(exist = false)
    private Date endInTime;

    /**
     * 移库入库最后一笔结束时间字符串
     */
    @TableField(exist = false)
    private String endInTimeStr;

    /**
     * 质保号
     */
    @TableField(exist = false)
    private String qaCode;

}
