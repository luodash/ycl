package com.tbl.modules.wms.entity.requestxml;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * FEWMS021  外协采购数据同步
 * @author 70486
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "orgid",//组织id
        "factoryid",//入库生产厂ID
        "materialcode",//物料编码
        "materialname",//物料描述
        "qacode",//质保号
        "entityno",//接收单号
        "orderno",//订单编号
        "orderline",//订单行号
        "length",//报验数量
        "drumname",//盘具编码
        "drummodel",//盘具规格
        "drumoutdia",//盘具外径
        "drumindia",//盘具内径
        "salescode",//营销经理工号
        "salesname",//营销经理姓名
        "inspectionno",//报验单号
        "colour",//护套颜色
        "weight",//重量
        "uom"//计量单位
})
@XmlRootElement(name = "item")
@Getter
@Setter
@ToString
public class FEWMS021_data {

    public String orgid;
    public String factoryid;
    public String materialcode;
    public String materialname;
    public String qacode;
    public String entityno;
    public String orderno;
    public String orderline;
    public String length;
    public String drumname;
    public String drummodel;
    public String drumoutdia;
    public String drumindia;
    public String salescode;
    public String salesname;
    public String inspectionno;
    public String colour;
    public String weight;
    public String uom;
}
