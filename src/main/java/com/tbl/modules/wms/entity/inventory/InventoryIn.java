package com.tbl.modules.wms.entity.inventory;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 入库日志记录表
 * @author 70486
 */
@TableName("YDDL_INVENTORY_IN")
@Getter
@Setter
@ToString
public class InventoryIn implements Serializable {

    /**
     * 主键
     */
    @TableId(value = "ID")
    private Long id;

    /**
     * 入库厂区
     */
    @TableField(value = "ORG")
    private String org;

    /**
     *
     */
    @TableField(value = "MATERIALCODE")
    private String materialcode;

    /**
     *
     */
    @TableField(value = "MATERIALNAME")
    private String materialname;

    /**
     *
     */
    @TableField(value = "BATCH_NO")
    private String batchNo;

    /**
     *
     */
    @TableField(value = "QACODE")
    private String qacode;

    /**
     *
     */
    @TableField(value = "SALESCODE")
    private String salescode;

    /**
     *
     */
    @TableField(value = "SALESNAME")
    private String salesname;

    /**
     *
     */
    @TableField(value = "P_ID")
    private Long pId;

    /**
     * RFID
     */
    @TableField(value = "RFID")
    private String rfid;

    /**
     *
     */
    @TableField(value = "METER")
    private String meter;

    /**
     * 创建时间
     */
    @TableField(value = "CREATETIME")
    private Date createtime;

    /**
     *
     */
    @TableField(value = "STATE")
    private Integer state;

    /**
     *
     */
    @TableField(value = "INSTORAGETIME")
    private Date instoragetime;

    /**
     * 仓库编码
     */
    @TableField(value = "WAREHOUSECODE")
    private String warehousecode;

    /**
     *
     */
    @TableField(value = "CARCODE")
    private String carcode;

    /**
     * 库位编码
     */
    @TableField(value = "SHELFCODE")
    private String shelfcode;

    /**
     * 入库人id
     */
    @TableField(value = "INSTORAGEID")
    private Long inStorageId;

    /**
     * 是否合格证扫描
     */
    @TableField(value = "DRUMTYPE")
    private String drumType;

    /**
     *
     */
    @TableField(value = "INNERDIAMETER")
    private Long innerDiameter;

    /**
     * 盘具外径
     */
    @TableField(value = "OUTERDIAMETER")
    private Long outerDiameter;

    /**
     * 盘具编码
     */
    @TableField(value = "MODEL")
    private String model;

    /**
     * 盘类型
     */
    @TableField(value = "DISHTYPE")
    private String dishtype;

    /**
     * 盘大小
     */
    @TableField(value = "DISHSIZE")
    private String dishsize;
}
