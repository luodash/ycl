package com.tbl.modules.wms.entity.productbind;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 可视化行车轨迹实体类
 * @author 70486
 */
@TableName("DEVICE_UWB_ROUTE")
@Getter
@Setter
@ToString
public class DeviceUwbRoute {

	/**
	 * 主键
	 */
    @TableId(value = "ID")
    private Long id;

    /**
     * 标签绑定关系
     */
    @TableField(value = "TAG")
    private String tag;

    /**
     * x轴坐标
     */
    @TableField(value = "XSIZE")
    private Float xsize;

    /**
     * y轴坐标
     */
    @TableField(value = "YSIZE")
    private Float ysize;

    /**
     * 创建时间
     */
    @TableField(value = "CREATETIME")
    private String createtime;
    
    /**
     * 业务类型
     */
    @TableField(value = "ALARMFLAG")
    private Long alarmflag;
    
    /**
     * 上一秒x
     */
    @TableField(value = "OLDXSIZE")
    private Float oldxsize;
    
    /**
     * 上一秒y
     */
    @TableField(value = "OLDYSIZE")
    private Float oldysize;

    /**
     * CarMission
     */
    @TableField(exist = false)
    private String carMission;
}
