package com.tbl.modules.wms.entity.responsexml.externalcable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * FEWMS021  WMS接口-同步外协采购的电缆数据
 * @author 70486
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "transactionid",
        "qacode",//质保编号
        "num",//数量
        "buyorderno",//采购订单号
        "receiptno",//接收单号
        "inspectno",//报验单号
        "buyorderline",//采购订单行号
        "orderdistribute",//订单分配
        "materialcode"//物料编码
})
@XmlRootElement(name = "extrenalcable")
@Getter
@Setter
@ToString
public class ExternalcableXml {

    public String transactionid;
    public String qacode;
    public String num;
    public String buyorderno;
    public String receiptno;
    public String inspectno;
    public String buyorderline;
    public String orderdistribute;
    public String materialcode;

}
