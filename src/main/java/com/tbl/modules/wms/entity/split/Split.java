package com.tbl.modules.wms.entity.split;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 拆分
 * @author 70486
 */
@TableName("YDDL_SPLIT")
@Getter
@Setter
@ToString
public class Split implements Serializable {

    /**
     * 主键
     */
    @TableId(value = "ID")
    private Long id;

    /**
     * 物料编码
     */
    @TableField(value = "MATERIALCODE")
    private String materialCode;

    /**
     * 物料批次号
     */
    @TableField(value = "BATCH_NO")
    private String batchNo;

    /**
     * 质保号
     */
    @TableField(value = "QACODE")
    private String qaCode;

    /**
     * 库位编码
     */
    @TableField(value = "SHELFCODE")
    private String shelfCode;

    /**
     * 仓库编码
     */
    @TableField(value = "WAREHOUSECODE")
    private String warehouseCode;

    /**
     * 仓库名称
     */
    @TableField(exist = false)
    private String warehouseName;

    /**
     * 数量
     */
    @TableField(value = "METER")
    private String meter;

    /**
     * rfid编码
     */
    @TableField(value = "RFID")
    private String rfid;

    /**
     * 拆分时间
     */
    @TableField(value = "INSTORAGETIME")
    private Date splitTime;

    /**
     * 入库厂区
     */
    @TableField(value = "ORG")
    private String org;

    /**
     * 营销经理编号
     */
    @TableField(value = "SALESCODE")
    private String salesCode;

    /**
     * 营销经理名称
     */
    @TableField(value = "SALESNAME")
    private String salesName;

    /**
     * 拆分人主键
     */
    @TableField(value = "INSTORAGEID")
    private Long splitStorageId;

    /**
     * 入库人名称
     */
    @TableField(exist = false)
    private String inStorageName;

    /**
     * 状态
     */
    @TableField(value = "STATE")
    private Integer state;

    /**
     * 状态名称
     */
    @TableField(exist = false)
    private String stateName;

    /**
     * 工单号
     */
    @TableField(value = "ENTITYNO")
    private String entityNo;

    /**
     * 订单号
     */
    @TableField(value = "ORDERNO")
    private  String orderno;

    /**
     * 订单行号
     */
    @TableField(value = "ORDERLINE")
    private  String orderline;

    /**
     * 生产厂id
     */
    @TableField(exist = false)
    private String factoryid;

    /**
     * 单位
     */
    @TableField(exist = false)
    private String unit;

    /**
     * 颜色
     */
    @TableField(exist = false)
    private String colour;

    /**
     * 重量
     */
    @TableField(exist = false)
    private String weight;

    /**
     * 段号
     */
    @TableField(exist = false)
    private String segmentno;

    /**
     * 盘号
     */
    @TableField(exist = false)
    private String dishcode;

    /**
     * 盘具编码
     */
    @TableField(value = "DISHNUMBER")
    private String dishnumber;

    /**
     * 产线名称（车间名称）
     */
    @TableField(exist = false)
    private String workshopname;

    /**
     * 厂区名称
     */
    @TableField(exist = false)
    private String factoryName;

    /**
     * 退回生产线用于区分是“已包装”还是“库存”数据
     */
    @TableField(exist = false)
    private String type;

    /**
     * 货物类型
     */
    @TableField(exist = false)
    private Long storageType;

    /**
     * 货物类型字符型
     */
    @TableField(exist = false)
    private String storageTypeStr;

    /**
     /**
     * 盘具类型
     */
    @TableField(exist = false)
    private String drumType;

    /**
     * 盘具内径
     */
    @TableField(exist = false)
    private Long innerDiameter;

    /**
     * 规格型号
     */
    @TableField(exist = false)
    private String model;

    /**
     * 物料名称
     */
    @TableField(exist = false)
    private String materialName;

    /**
     * 收货单位
     */
    @TableField(exist = false)
    private String customer;

    /**
     * 厂区编码
     */
    @TableField(exist = false)
    private String factoryCode;

    /**
     * 新盘号
     */
    @TableField(value = "OUTERDIAMETER")
    private String outerdiameter;
}
