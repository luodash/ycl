package com.tbl.modules.wms.entity.responsexml.print;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;


/**
 * 出库单打印信息XML
 * @author 70486
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "deliverywarehouse",
        "undertake",
        "outdate",
        "invoice",
        "delivery",
        "shipment",
        "pickup",
        "pickuptel",
        "handle",
        "deliveryaddress",
        "undertaketel",
        "shipNo",
        "outDans"
})
@XmlRootElement(name = "data")
@Getter
@Setter
@ToString
public class Yddl_Print_Xml{
    /**
     * 发货库房
     */
    private String deliverywarehouse;
    /**
     * 承办单位（人）
     */
    private String undertake;
    /**
     * 发货日期
     */
    private String outdate;
    /**
     * 开票人
     */
    private String invoice;
    /**
     * 发货人
     */
    private String delivery;
    /**
     * 承运人
     */
    private String shipment;
    /**
     * 收货单位（人）
     */
    private String  pickup;
    /**
     * 联系电话
     */
    private String  pickuptel;
    /**
     * 经办人
     */
    private String  handle;
    /**
     * 送货地址
     */
    private String  deliveryaddress;
    /**
     * 承办单位电话
     */
    private String  undertaketel;
    /**
     * 承办单位电话
     */
    private String  shipNo;
    /**
     * 出库打印单据信息
     */
    private List<OutDansXml> outDans;
}
