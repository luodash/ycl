package com.tbl.modules.wms.entity.baseinfo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 仓库
 * @author 70486
 */
@TableName("YDDL_WAREHOUSE")
@Getter
@Setter
@ToString
public class Warehouse implements Serializable {

    /**
     * 主键
     */
    @TableId(value = "ID")
    private Long id;

    /**
     * 仓库编号
     */
    @TableField(value = "CODE")
    private String code;

    /**
     * 仓库名称
     */
    @TableField(value = "NAME")
    private String name;

    /**
     * 厂区编码
     */
    @TableField(value = "FACTORYCODE")
    private String factoryCode;

    /**
     * 状态
     */
    @TableField(value = "STATE")
    private Integer state;

    @TableField(value = "EBS_LOCATOR")
    private String ebsLocator;

    @TableField(value = "LOCATOR_ID")
    private Long locatorId;

    /**
     * 状态名称
     */
    @TableField(exist = false)
    private String stateStr;

    /**
     * 厂区名称
     */
    @TableField(exist = false)
    private String factoryName;

    @TableField(exist = false)
    private String text;

    /**
     * 仓库编码_编码名称
     */
    @TableField(exist = false)
    private String warehouseCodeName;

    /**
     * 仓库下库位可否重复占用（是否可复用）：1可以，0不能
     */
    @TableField(value = "REPEAT_OCCUPANCY")
    private Integer repeatOccupancy;

    /**
     *  实体(公司)ID
     */
    @TableField(value = "ENTITY_ID")
    private String entityId;

}
