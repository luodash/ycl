package com.tbl.modules.wms.entity.split;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 入库汇总
 * @author 70486
 */
@TableName("YDDL_ALL_INSTORAGE")
@Setter
@Getter
@ToString
public class AllInstorage implements Serializable {


    @TableField(value = "ORG")
    private String org;

    @TableField(value = "MATERIALCODE")
    private String materialCode;

    @TableField(value = "MATERIALNAME")
    private String materialName;

    @TableField(value = "BATCH_NO")
    private String batchNo;

    @TableField(value = "QACODE")
    private String qaCode;

    @TableField(value = "SALESCODE")
    private String saleCode;

    @TableField(value = "SALESNAME")
    private String salesName;

    @TableField(value = "P_ID")
    private String pId;

    @TableField(value = "RFID")
    private String rfid;

    @TableField(value = "METER")
    private String meter;

    @TableField(value = "CREATETIME")
    private String createTime;

    @TableField(value = "CONFIRMTIME")
    private String confirmTime;

    @TableField(value = "CONFIRMBY")
    private String confirmBy;

    @TableField(value = "STATE")
    private String state;

    @TableField(value = "INSTORAGETIME")
    private String instorageTime;

    @TableField(value = "WAREHOUSECODE")
    private String wareHouseCode;

    @TableField(value = "CARCODE")
    private String carCode;

    @TableField(value = "SHELFCODE")
    private String shelfCode;

    @TableField(value = "INSTORAGEID")
    private int instorageId;

    @TableField(value = "TYPE")
    private int type;

    /**
     * 盘具类型
     */
    @TableField(value = "DRUMTYPE")
    private String drumType;

    /**
     * 盘具内径
     */
    @TableField(value = "INNERDIAMETER")
    private Long innerDiameter;

    /**
     * 盘具外径
     */
    @TableField(value = "OUTERDIAMETER")
    private Long outerDiameter;

    /**
     * 规格型号
     */
    @TableField(value = "MODEL")
    private String model;

}
