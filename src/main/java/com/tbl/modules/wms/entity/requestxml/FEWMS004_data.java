package com.tbl.modules.wms.entity.requestxml;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * FEWMS004  WMS接口-采购退货
 * @author 70486
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "batchs",
})
@XmlRootElement(name = "data")
@Getter
@Setter
@ToString
public class FEWMS004_data {

    public FEWMS004_batchs batchs;

}
