package com.tbl.modules.wms.entity.baseinfo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 物料表
 * @author 70486
 */
@TableName("YDDL_MATERIAL")
@Getter
@Setter
@ToString
public class Material implements Serializable {

    /**
     * 主键
     */
    @TableId(value = "ID")
    private Long id;

    /**
     * 物流编码
     */
    @TableField(value = "CODE")
    private String code;

    /**
     * 物料名称（规格）
     */
    @TableField(value = "NAME")
    private String name;

    /**
     * 主单位
     */
    @TableField(value = "MAIN_UNIT")
    private String mainUnit;

    /**
     * 辅单位
     */
    @TableField(value = "SUB_UNIT")
    private String subUnit;

    /**
     * 标准单包重量
     */
    @TableField(value = "STANDARD_PACKAGE_WEIGHT")
    private String standardPackageWeight;

    /**
     * 物料说明
     */
    @TableField(value = "REMARK")
    private String remark;

    /**
     * 呆滞时间(天)
     */
    @TableField(value = "DEAD_TIME")
    private Integer deadTime;

    /**
     * 最小库存量
     */
    @TableField(value = "MIN_STOCK")
    private Integer minStock;

    /**
     * 最大库存量
     */
    @TableField(value = "MAX_STOCK")
    private Integer maxStock;

    /**
     * 物料类别
     */
    @TableField(value = "CATEGORY")
    private String category;

    /**
     * 状态
     */
    @TableField(value = "STATUS")
    private String status;

}
