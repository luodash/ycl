package com.tbl.modules.wms.entity.requestxml;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * FEWMS021  外协采购数据同步
 * @author 70486
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "batchno",//批次号
        "qacode",//质保号
        "meter",//米数
        "colour",//颜色
        "weight",//重量
        "segmentno"//段号
})
@XmlRootElement(name = "reservdata")
@Getter
@Setter
@ToString
public class FEWMS027_reservdata {

    public String batchno;
    public String qacode;
    public String meter;
    public String colour;
    public String weight;
    public String segmentno;
}
