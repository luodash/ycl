package com.tbl.modules.wms.entity.requestxml;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * FEWMS003  WMS接口-销售退货数据同步
 * @author 70486
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "oldqano",//原质保号
        "newqano",//新质保号
        "drummodel",//盘具编码
        "drumoutdia",//盘外经
        "drumindia",//盘内经
        "oeheader",//销售订单头
        "oeline",//销售订单行
        "locationid",
        "itemnum",//物料编码
        "customerid",//客户
        "qty",//数量
        "salescode",
        "salesname",
        "uom",//单位
        "orgid",//组织id
        "rmaorderno",//订单编号
        "rmalineno"//订单行号
})
@XmlRootElement(name = "item")
@Getter
@Setter
@ToString
public class FEWMS003_items {
    private String oldqano;
    private String newqano;
    private String drummodel;
    private String drumoutdia;
    private String drumindia;
    private String oeheader;
    private String oeline;
    private String locationid;
    private String itemnum;
    private String customerid;
    private String qty;
    private String salescode;
    private String salesname;
    private String uom;
    private String orgid;
    private String rmaorderno;
    private String rmalineno;
}

