package com.tbl.modules.wms.entity.baseinfo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 盘具
 * @author 70486
 */
@TableName("YDDL_DISH")
@Getter
@Setter
@ToString
public class Dish implements Serializable {

    /**
     * 主键
     */
    @TableField(value = "ID")
    private Long id;

    /**
     * 盘具编码
     */
    @TableField(value = "CODE")
    private String code;

    /**
     * 规格型号
     */
    @TableField(value = "MODEL")
    private String model;

    /**
     * 盘具类型
     */
    @TableField(value = "DRUMTYPE")
    private String drumType;

    /**
     * 盘具明细类型
     */
    @TableField(value = "DRUMTYPEDETAIL")
    private String drumTypeDetail;

    /**
     * 盘具内径
     */
    @TableField(value = "INNERDIAMETER")
    private String innerDiameter;

    /**
     * 盘具外径
     */
    @TableField(value = "OUTERDIAMETER")
    private String outerDiameter;

    /**
     * L1Max
     */
    @TableField(value = "L1Max")
    private String oneMax;

    /**
     * L2
     */
    @TableField(value = "L2")
    private String two;

    /**
     * 最大载重
     */
    @TableField(value = "MAXCARRY")
    private String maxCarry;

    /**
     * 是否超宽：1是，0否
     */
    @TableField(value = "SUPER_WIDE")
    private Integer superWide;

    /**
     * code-model
     */
    @TableField(exist = false)
    private String codeModel;
}
