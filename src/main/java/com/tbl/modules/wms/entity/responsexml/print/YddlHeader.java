package com.tbl.modules.wms.entity.responsexml.print;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "code",
        "name"
})
@XmlRootElement(name = "header")
@Getter
@Setter
@ToString
public class YddlHeader {

    public String code;
    public String name;

}
