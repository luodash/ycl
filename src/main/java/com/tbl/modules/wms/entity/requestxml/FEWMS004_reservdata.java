package com.tbl.modules.wms.entity.requestxml;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * FEWMS004 采购退货接口
 * @author 70486
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
		"batchno",  //批次号
        "meter" //米数
})
@XmlRootElement(name = "reservdata")
@Getter
@Setter
@ToString
public class FEWMS004_reservdata {

    public String batchno;
    public String meter;

}
