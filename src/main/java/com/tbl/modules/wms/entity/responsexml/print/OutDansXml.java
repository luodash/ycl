package com.tbl.modules.wms.entity.responsexml.print;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 * 出库打印单据信息
 * @author 70486
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "outDanXml"
})
@XmlRootElement(name = "outdans")
@Getter
@Setter
@ToString
public class OutDansXml {

    @XmlElement(name = "outdan", nillable = true)
    public List<OutDanXml> outDanXml = new ArrayList<>();
}
