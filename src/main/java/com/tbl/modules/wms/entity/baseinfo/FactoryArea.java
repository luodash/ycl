package com.tbl.modules.wms.entity.baseinfo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 厂区信息
 * @author 70486
 */
@TableName("YDDL_FACTORY_AREA")
@Getter
@Setter
@ToString
public class FactoryArea implements Serializable {

	/**
	 * 主键
	 */
    @TableId(value = "ID")
    private Long id;

    /**
     * 厂区编码
     */
    @TableField(value = "CODE")
    private String code;

    /**
     * 厂区名称
     */
    @TableField(value = "NAME")
    private String name;
    
    /**
     * 厂区编码_厂区名称
     */
    @TableField(exist = false)
    private String factoryCodeName;
    
    /**
     * 流水号
     */
    @TableField(value = "SERIALNUM")
    private String serialnum;

    /**
     * 组织代码
     */
    @TableField(value = "ORGANIZATION_CODE")
    private String organizationCode;

    /**
     * 公司（OU）主键
     */
    @TableField(value = "ENTITYID")
    private Long entityid;

}
