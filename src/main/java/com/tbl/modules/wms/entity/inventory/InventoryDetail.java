package com.tbl.modules.wms.entity.inventory;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 盘点任务明细
 * @author 70486
 */
@Getter
@Setter
@ToString
public class InventoryDetail implements Serializable {

    private String  rfid;

    private String qaCode;
    
    private String shelfCode;
    
    private String warehouseCode;
    
    private String batchNo;
    
    private String materialCode;
    
    private String me;

    /**
     * 盘具类型
     */
    private String drumType;

    /**
     * 盘具内径
     */
    private Long innerDiameter;

    /**
     * 盘具外径
     */
    private Long outerDiameter;
    /**
     * 规格型号
     */
    private String model;

}
