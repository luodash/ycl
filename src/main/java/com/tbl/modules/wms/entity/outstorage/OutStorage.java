package com.tbl.modules.wms.entity.outstorage;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 出库信息主表
 * @author 70486
 */
@TableName("YDDL_OUTSTORAGE")
@Getter
@Setter
@ToString
public class OutStorage implements Serializable {

    /**
     * 主键
     */
    @TableId(value = "ID")
    private Long id;

    /**
     * 发货单号
     */
    @TableField(value = "SHIP_NO")
    private String shipNo;

    /**
     * 打印时间
     */
    @TableField(value = "PRINTTIME")
    private String printTime;

    /**
     * 合同号
     */
    @TableField(value = "CONTRACT_NO")
    private String contractNo;

    /**
     * 订单号
     */
    @TableField(value = "ORDERNO")
    private String orderNo;

    /**
     * 订单行号
     */
    @TableField(value = "ORDERLINE")
    private String orderLine;

    /**
     * 运输公司
     */
    @TableField(value = "TRANSPORTNAME")
    private String transportName;

    /**
     * 收货单位
     */
    @TableField(value = "CUSTOMER")
    private String customer;

    /**
     * 发货组织
     */
    @TableField(value = "ORG")
    private String org;

    /**
     * 营销经理编号
     */
    @TableField(value = "SALESCODE")
    private String salesCode;

    /**
     * 营销经理名称
     */
    @TableField(value = "SALESNAME")
    private String salesName;

    /**
     * 发货地址
     */
    @TableField(value = "ADDRESS")
    private String address;

    /**
     * 生效时间
     */
    @TableField(value = "CREATE_TIME")
    private Date createTime;

    /**
     * 状态：1:生效  2:拣选中  3:拣选完成 4:发运完成
     */
    @TableField(value = "STATE")
    private Integer state;

    /**
     * 出库单的整体出库状态
     */
    @TableField(exist = false)
    private String storageState;

    /**
     * 厂区名称
     */
    @TableField(exist = false)
    private String name;

    /**
     * 提货单下的详情对应的库位集合
     */
    @TableField(exist = false)
    private List<Map<String,Object>> lstmap;

    /**
     * 排序标志
     */
    @TableField(exist = false)
    private Integer sortNumber;
}
