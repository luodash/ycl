package com.tbl.modules.wms.entity.outstorage;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 出库单打印信息主表
 * @author 70486
 */
@TableName("YDDL_PRINT_INFO")
@Getter
@Setter
@ToString
public class OutStoragePrint implements Serializable {

    /**
     * 主键
     */
    @TableId(value = "ID")
    private Long id;

    /**
     * 发货库房
     */
    @TableField(value = "DELIVERYWAREHOUSE")
    private String deliverywarehouse;

    /**
     * 承办单位（人）
     */
    @TableField(value = "UNDERTAKE")
    private String undertake;

    /**
     * 开票
     */
    @TableField(value = "INVOICE")
    private String invoice;

    /**
     * 发货
     */
    @TableField(value = "DELIVERY")
    private String delivery;

    /**
     *承运
     */
    @TableField(value = "SHIPMENT")
    private String shipment;

    /**
     * 收货单位（人）
     */
    @TableField(value = "PICKUP")
    private String pickup;

    /**
     * 联系电话
     */
    @TableField(value = "PICKUPTEL")
    private String pickuptel;

    /**
     * 经办人
     */
    @TableField(value = "HANDLE")
    private String handle;

    /**
     * 送货地址
     */
    @TableField(value = "DELIVERYADDRESS")
    private String deliveryaddress;

    /**
     * 承办单位电话
     */
    @TableField(value = "UNDERTAKETEL")
    private String undertaketel;

    /**
     * 发货地址
     */
    @TableField(value = "SHIP_NO")
    private String shipNo;

    /**
     * 批次号
     */
    @TableField(value = "BATCH_NO")
    private String  bactNo;

    /**
     * 车牌号
     */
    @TableField(value = "CAR")
    private String car;

    /**
     * 出库日期
     */
    @TableField(value = "OUTDATE")
    private String outdate;

    /**
     * 运单号
     */
    @TableField(value = "WAYNO")
    private String wayno;

    /**
     * 司机
     */
    @TableField(value = "DRIVERNAME")
    private String drivername;

    /**
     * 运输公司
     */
    @TableField(value = "TRANSPORTCOMPANY")
    private String transportcompany;


    /**
     * 是否已经打印过（1，是，2，否）
     */
    @TableField(value = "ISPRINT")
    private String isprint;

}
