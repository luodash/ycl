package com.tbl.modules.wms.entity.responsexml.workshop;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * FEWMS026  WMS接口-基础数据-车间
 * @author 70486
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "orgid",//组织id
        "workshopnumber",//车间代码
        "workshopname"//车间名称
})
@XmlRootElement(name = "workshop")
@Getter
@Setter
@ToString
public class WorkshopXml {
    public String orgid;
    public String workshopnumber;
    public String workshopname;
}
