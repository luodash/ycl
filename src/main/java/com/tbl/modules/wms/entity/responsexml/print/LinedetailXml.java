package com.tbl.modules.wms.entity.responsexml.print;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 打印明细
 * @author 70486
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "batchno"

})
@XmlRootElement(name = "linedetail")
@Getter
@Setter
@ToString
public class LinedetailXml {

    /**
     * 批次号
     */
    public  String batchno;
}
