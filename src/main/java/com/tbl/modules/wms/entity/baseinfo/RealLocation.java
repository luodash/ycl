package com.tbl.modules.wms.entity.baseinfo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 获取真实库位坐标记录实体类
 * @author 70486
 */
@TableName("YDDL_REAL_LOCATION")
@Getter
@Setter
@ToString
public class RealLocation implements Serializable {

	/**
	 * 主键
	 */
    @TableId(value = "ID")
    private Long id;

    /**
     * xsize1
     */
    @TableField(value = "XSIZE1")
    private Long xsize1;

    /**
     * ysize1
     */
    @TableField(value = "YSIZE1")
    private Long ysize1;

    /**
     * xsize2
     */
    @TableField(value = "XSIZE2")
    private Long xsize2;

    /**
     * ysize2
     */
    @TableField(value = "YSIZE2")
    private Long ysize2;

    /**
     * 创建时间
     */
    @TableField(value = "CREATETIME")
    private Date createtime;

    /**
     * 实际的x坐标
     */
    @TableField(value = "REALX")
    private Float realx;

    /**
     * 实际的y坐标
     */
    @TableField(value = "REALY")
    private Float realy;
}
