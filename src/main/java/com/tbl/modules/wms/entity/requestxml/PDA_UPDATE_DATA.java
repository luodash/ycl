package com.tbl.modules.wms.entity.requestxml;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 手持机版本号、版本名称、app地址
 * @author 70486
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "version",
        "name",
        "url"
})
@XmlRootElement(name = "update")
@Getter
@Setter
@ToString
public class PDA_UPDATE_DATA {

    public String version ;
    public String name ;
    public String url ;

}
