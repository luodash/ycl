package com.tbl.modules.wms.entity.responsexml;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *  FEWMS008  WMS接口-调拨移库接口
 * @author 70486
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "",propOrder = {
        "batchNo",
        "inWarehouse",
        "inLocation"
})
@XmlRootElement(name = "data")
@Getter
@Setter
@ToString
public class FEWMS008_data {
    public String batchNo;
    public String inWarehouse;
    public String inLocation;
}
