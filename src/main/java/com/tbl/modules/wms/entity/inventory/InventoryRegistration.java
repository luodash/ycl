package com.tbl.modules.wms.entity.inventory;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 库存盘点--盘点登记信息表
 * @author 70486
 */
@TableName("YDDL_INVENTORY_REGISTRATION")
@Getter
@Setter
@ToString
public class InventoryRegistration implements Serializable {

	/**
	 * 主键
	 */
    @TableId(value = "ID")
    private Long id;

    /**
     * 任务ID
     */
    @TableField(value = "PLAN_ID")
    private String planId;

    /**
     * rfid
     */
    @TableField(value = "RFID")
    private String rfid;

    /**
     * 质保单号
     */
    @TableField(value = "QACODE")
    private String qacode;
    
    /**
     * 库存库位
     */
    @TableField(value = "SHELFID")
    private String shelfid;
    
    /**
     * 仓库编码
     */
    @TableField(value = "WAREHOUSECODE")
    private String warehousecode;
    
    /**
     * 仓库名称
     */
    @TableField(exist = false)
    private String warehouseName;
    
    /**
     * 上传时间
     */
    @TableField(value = "UPLOAD_TIME")
    private Date uploadTime;
    
    /**
     * 盘点人主键
     */
    @TableField(value = "USER_CODE")
    private String userCode;
    
    /**
     * 盘点人名称
     */
    @TableField(value = "USER_NAME")
    private String userName;
    
    /**
     * 库存米数
     */
    @TableField(value = "METER")
    private String meter;
    
    /**
     * 物料编码
     */
    @TableField(value = "MATERIALCODE")
    private String materialcode;
    
    /**
     * 结果
     */
    @TableField(exist = false)
    private String jg;
    
    /**
     * 临时id
     */
    @TableField(exist = false)
    private String tempId;

    /**
     * 盘点结果（1，正常，2，盘盈，3，盘亏）
     */
    @TableField(value="RESULT")
    private String result;

    /**
     * 盘点结果（1，正常，2，盘盈，3，盘亏）
     */
    @TableField(exist = false)
    private String resultStr;

    /**
     * 库存所在厂区
     */
    @TableField(value="ORG")
    private String org;

    /**
     * 库存所在厂区名称
     */
    @TableField(exist = false)
    private String orgName;

    /**
     * 集结号
     */
    @TableField(value = "EXAMINED")
    private Long examined;

    /**
     * 上传ebs的盘点米数的执行状态（0失败；1成功；2待审核）
     */
    @TableField(value = "STATE")
    private Long state;


    /**
     * 批次号
     */
    @TableField(exist = false)
    private String batchNo;

    /**
     * 盘号
     */
    @TableField(exist = false)
    private String dishcode;

    /**
     * 盘点的米数(会根据盘点审核时，修改的米数而修改)
     */
    @TableField(value = "EDITMETER")
    private String editmeter;

    /**
     * 盘点的库位(会根据盘点审核时，修改的库位而修改)
     */
    @TableField(value = "EDITSHELF")
    private String editshelf;

    /**
     * 盘点单号
     */
    @TableField(exist = false)
    private String planCode;

    /**
     * 创建人
     */
    @TableField(exist = false)
    private String creater;

    /**
     * 创建时间
     */
    @TableField(exist = false)
    private String createtime;

    /**
     * 执行人
     */
    @TableField(exist = false)
    private String executer;

    /**
     * 盘点审核时的库位修改移库状态：（0失败；1成功；2待审核）
     */
    @TableField(value = "SHELF_STATE")
    private Integer shelfState;

    /**
     * 单位
     */
    @TableField(value = "UNIT")
    private String unit;
}
