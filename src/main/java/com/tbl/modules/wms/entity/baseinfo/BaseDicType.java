package com.tbl.modules.wms.entity.baseinfo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 下拉框选择项实体类
 * @author 70486
 */
@TableName("BASE_DIC_TYPE")
@Getter
@Setter
@ToString
public class BaseDicType implements Serializable {

	/**
	 * 主键
	 */
    @TableId(value = "ID")
    private Long id;

    /**
     * 编号
     */
    @TableField(value = "DICT_TYPE_NUM")
    private String dictTypeNum;

    /**
     * 名称
     */
    @TableField(value = "DICT_TYPE_NAME")
    private String dictTypeName;

    /**
     * 描述
     */
    @TableField(value = "DESCRIPTION")
    private String description;


}
