package com.tbl.modules.wms.entity.baseinfo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 库位信息表
 * @author 70486
 */
@TableName("YDDL_SHELF")
@Getter
@Setter
@ToString
public class Shelf implements Serializable {

    /**
     * 库位主键
     */
    @TableId(value = "ID")
    private Long id;

    /**
     * 库位编码前
     */
    @TableField(value = "CODE")
    private String code;

    /**
     * 库位编码完全体
     */
    @TableField(value = "CODE_COMPLETE")
    private String codeComplete;

    /**
     * 库位编码后
     */
    @TableField(value = "COMPLETE")
    private String complete;

    /**
     * 库位编码前字符串格式
     */
    @TableField(exist = false)
    private String codes;

    /**
     * 仓库编码
     */
    @TableField(value = "WAREHOUSECODE")
    private String warehouseCode;

    /**
     * 库位可用状态
     */
    @TableField(value = "STATE")
    private Integer state;
    @TableField(exist = false)
    private String stateStr;

    /**
     * 仓库名称
     */
    @TableField(exist = false)
    private String warehouseName;

    /**
     * 库位x坐标
     */
    @TableField(value = "XSIZE1")
    private Long xsize1;

    /**
     * 库位y坐标
     */
    @TableField(value = "YSIZE1")
    private Long ysize1;

    /**
     * 库位x坐标
     */
    @TableField(value = "XSIZE2")
    private Long xsize2;

    /**
     * 库位y坐标
     */
    @TableField(value = "YSIZE2")
    private Long ysize2;

    /**
     * 库位中间点x坐标
     */
    @TableField(value = "MIDXSIZE")
    private Long midxsize;

    /**
     * 库位中间点y坐标
     */
    @TableField(value = "MIDYSIZE")
    private Long midysize;

    /**
     * 总数
     */
    @TableField(exist = false)
    private Long totalNum;

    /**
     * 被占用的
     */
    @TableField(exist = false)
    private Long useNum;

    /**
     * 未使用的
     */
    @TableField(exist = false)
    private Long unuseNum;

    /**
     * 禁用库位数量
     */
    @TableField(exist = false)
    private Long prohibitNum;

    /**
     * 库位使用率
     */
    @TableField(exist = false)
    private String usePro;
    
    /**
     * 可视化目标库位
     */
    @TableField(exist = false)
    private String visualTargetShelf;
    
    /**
     * 所属厂区编码
     */
    @TableField(value = "FACTORYCODE")
    private String factoryCode;
    
    /**
     * 所属厂区名称
     */
    @TableField(exist = false)
    private String factoryName;

    /**
     * 阴x
     */
    @TableField(exist = false)
    private String yinxsize;

    /**
     * 阴y
     */
    @TableField(exist = false)
    private String yinysize;

    /**
     * 阳x
     */
    @TableField(exist = false)
    private String yangxsize;

    /**
     * 阳y
     */
    @TableField(exist = false)
    private String yangysize;

    /**
     * 推荐库位用的参数，距离过道距离
     */
    @TableField(value = "DISTANCE")
    private Double distance;

    /**
     * 厂区区域中线坐标值
     */
    @TableField(value = "HALLWAY_MIDLINE")
    private Double hallwayMidline;

    /**
     * 库位描述
     */
    @TableField(value = "LOCATION_DESC")
    private String locationDesc;

    /**
     * 库位口否重复占用
     */
    @TableField(value = "REPEAT_OCCUPANCY")
    private Integer repeatOccupancy;
    @TableField(exist = false)
    private String repeatOccupancyStr;

    /**
     * 最小容量
     */
    @TableField(value = "MIN_CAPACITY")
    private Double minCapacity;

    /**
     * 最大容量
     */
    @TableField(value = "MAX_CAPACITY")
    private Double maxCapacity;

    /**
     * agv点位
     */
    @TableField(value = "AGV_POINT")
    private String agvPoint;

    /**
     * 区域
     */
    @TableField(value = "AREA")
    private String area;

    /**
     * 所属库区名称
     */
    @TableField(exist = false)
    private String areaName;

}
