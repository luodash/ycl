package com.tbl.modules.wms.entity.requestxml;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *  FEWMS006  WMS接口-发货单接口
 * @author 70486
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "batchsdet",
        "slipdata"
})
@XmlRootElement(name = "data")
@Getter
@Setter
@ToString
public class FEWMS006_data {
    public List<FEWMS006_batchsdet> batchsdet;
    public List<FEWMS006_slipdata> slipdata;
}
