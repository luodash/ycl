package com.tbl.modules.wms.entity.responsexml;

import com.tbl.modules.wms.entity.responsexml.disc.DiscXml;
import com.tbl.modules.wms.entity.responsexml.externalcable.ExternalcableXml;
import com.tbl.modules.wms.entity.responsexml.factory.PlantXml;
import com.tbl.modules.wms.entity.responsexml.location.LocationXml;
import com.tbl.modules.wms.entity.responsexml.material.MaterialXml;
import com.tbl.modules.wms.entity.responsexml.print.OutDanXml;
import com.tbl.modules.wms.entity.responsexml.warehouse.WarehouseXml;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "materialList",
        "plantList",
        "locationXmlList",
        "warehouseXmlList",
        "discXmlList",
        "externalcableList",
        "outDanXml",
        "deliverywarehouse"
})
@XmlRootElement(name = "data")
@Getter
@Setter
@ToString
public class data1 {

    /**
     * 物料
     */
    @XmlElement(name = "material", nillable = true)
    public List<MaterialXml> materialList = new ArrayList<>();

    /**
     * 厂区
     */
    @XmlElement(name = "plant", nillable = true)
    public List<PlantXml> plantList = new ArrayList<>();

    /**
     * 库位
     */
    @XmlElement(name = "location", nillable = true)
    public List<LocationXml> locationXmlList = new ArrayList<>();

    /**
     * 仓库
     */
    @XmlElement(name = "warehouse", nillable = true)
    public List<WarehouseXml> warehouseXmlList = new ArrayList<>();

    /**
     * 盘
     */
    @XmlElement(name = "disc", nillable = true)
    public List<DiscXml> discXmlList = new ArrayList<>();

    /**
     * 外采采购电缆数据
     */
    @XmlElement(name = "extrenalcable",nillable = true)
    public List<ExternalcableXml> externalcableList = new ArrayList<>();

    @XmlElement(name = "outdan",nillable = true)
    public List<OutDanXml> outDanXml = new ArrayList<>();

    @XmlElement(name = "deliverywarehouse",nillable = true)
    public String  deliverywarehouse ;
}
