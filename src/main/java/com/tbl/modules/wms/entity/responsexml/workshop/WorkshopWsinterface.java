package com.tbl.modules.wms.entity.responsexml.workshop;


import com.tbl.modules.wms.entity.responsexml.body;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * FEWMS026  WMS接口-基础数据-车间
 * @author 70486
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "header",
        "body"
})
@Getter
@Setter
@XmlRootElement(name = "wsinterface")
@ToString
public class WorkshopWsinterface {

    public WorkshopHeader header;
    public body body;

}
