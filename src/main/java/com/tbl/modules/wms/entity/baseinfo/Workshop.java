package com.tbl.modules.wms.entity.baseinfo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 车间实体类
 * @author 70486
 */
@TableName("YDDL_WORKSHOP")
@Getter
@Setter
@ToString
public class Workshop implements Serializable {

	/**
	 * 主键
	 */
    @TableId(value = "ID")
    private Long id;

    /**
     * 库存组织id
     */
    @TableField(value = "ORG")
    private String org;

    /**
     * 车间代码
     */
    @TableField(value = "WORKSHOPNUMBER")
    private String workshopnumber;

    /**
     * 车间名称
     */
    @TableField(value = "WORKSHOPNAME")
    private String workshopname;
    
    /**
     * 状态
     */
    @TableField(value = "STATE")
    private Long state;
    
    /**
     * 厂区名称
     */
    @TableField(exist = false)
    private String factoryName;
    
    /**
     * 厂区编号+名称
     */
    @TableField(exist = false)
    private String factoryCodeName;

}
