package com.tbl.modules.wms.entity.responsexml.print;



import com.tbl.modules.wms.entity.responsexml.body;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "header",
        "body"
})
@Getter
@Setter
@XmlRootElement(name = "wsinterface")
@ToString
public class YddlWsinterface {

    public YddlHeader header;
    public body body;

}
