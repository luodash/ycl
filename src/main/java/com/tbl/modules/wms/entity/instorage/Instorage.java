package com.tbl.modules.wms.entity.instorage;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 入库管理--入库表
 * @author 70486
 */
@TableName("YDDL_INSTORAGE")
@Getter
@Setter
@ToString
public class Instorage implements Serializable {

    @TableId(value = "ID")
    private Long id;

    /**
     * 工单号	
     */
    @TableField(value = "ENTITYNO")
    private String entityNo;

    /**
     * 订单编号
     */
    @TableField(value = "ORDERNO")
    private String orderNo;

    /**
     * 数据创建时间
     */
    @TableField(value = "CREATETIME")
    private Date createTime;
    
    /**
     * 订单行号
     */
    @TableField(value = "ORDERLINE")
    private String orderline;

    /**
     * 订单编号
     */
    @TableField(exist = false)
    private String ordernum;

    /**
     * 物料编码
     */
    @TableField(exist = false)
    private String materialcode;

    /**
     * 物料名称
     */
    @TableField(exist = false)
    private String materialname;

    /**
     * 质保号
     */
    @TableField(exist = false)
    private String qacode;

    /**
     * 名称
     */
    @TableField(exist = false)
    private String name;

    /**
     * 营销经理名称
     */
    @TableField(exist = false)
    private String salesname;
    
    /**
     * 组织机构名称
     */
    @TableField(exist = false)
    private String orgname;

    /**
     * 盘数
     */
    @TableField(exist = false)
    private String dishnum;

    /**
     * 组织机构
     */
    @TableField(value = "ORG")
    private String org;
    
    /**
     * 后台手动录入部分标识
     */
    @TableField(value = "SPECIALSIGN")
    private String specialsign;

}
