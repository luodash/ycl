package com.tbl.modules.wms.entity.responsexml.warehouse;


import com.tbl.modules.wms.entity.responsexml.body;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * FEWMS013  WMS接口-基础数据-仓库
 * @author 70486
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "header",
        "body"
})
@Getter
@Setter
@XmlRootElement(name = "wsinterface")
@ToString
public class WarehouseWsinterface {

    public WarehouseHeader header;
    public body body;

}
