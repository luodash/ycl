package com.tbl.modules.wms.entity.outstorage;


import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 流转单打印
 * @author 70486
 */
@TableName("YDDL_EXCHANGE_PRINT")
@Getter
@Setter
@ToString
public class ExchangePrint implements Serializable {

    /**
     * 主键
     */
    @TableId(value = "ID")
    private Long id;

    /**
     * 车牌号
     */
    @TableField(value = "CAR")
    private String car;

    /**
     * 同一批次号
     */
    @TableField(value = "ODD_NUMBER")
    private String oddnumber;

    /**
     * 出发厂区
     */
    @TableField(value = "S_FACTORY_AREA")
    private String sFactoryArea;

    /**
     * 目的厂区
     */
    @TableField(value = "E_FACTORY_AREA")
    private String eFactoryArea;

    /**
     * 打印日期
     */
    @TableField(value = "PRINT_TIME")
    private String printTime;
}
