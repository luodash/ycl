package com.tbl.modules.wms.entity.baseinfo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 行车实体类
 * @author 70486
 */
@TableName("YDDL_CAR")
@Getter
@Setter
@ToString
public class Car implements Serializable {

	/**
	 * 主键
	 */
    @TableId(value = "ID")
    private Long id;

    /**
     * 行车编码
     */
    @TableField(value = "CODE")
    private String code;

    /**
     * 行车名称
     */
    @TableField(value = "NAME")
    private String name;

    /**
     * 仓库编码
     */
    @TableField(value = "WAREHOUSECODE")
    private String warehouseCode;

    /**
     * 仓库id
     */
    @TableField(exist = false)
    private Long warehouse;

    /**
     * 仓库名称
     */
    @TableField(exist = false)
    private String warehouseName;

    /**
     * 状态（0，入库中，1，出库中，2，闲置）
     */
    @TableField(value = "STATE")
    private Long state;

    /**
     * 状态名（0，入库中，1，出库中，2，闲置）
     */
    @TableField(exist = false)
    private String stateStr;
    
    /**
     * 所属厂区编码
     */
    @TableField(value = "FACTORYCODE")
    private String factoryCode;
    
    /**
     * 所属厂区名称
     */
    @TableField(exist = false)
    private String factoryName;
    
    /**
     * 所属厂区编码+名称
     */
    @TableField(exist = false)
    private String factoryCodeName;
    
    /**
     * 生产厂id
     */
    @TableField(exist = false)
    private String factoryid;
    
    /**
     * 行车编码+名称
     */
    @TableField(exist = false)
    private String carCodeName;
    
    /**
     * 仓库区域
     */
    @TableField(value = "WAREHOUSEAREA")
    private String warehouseArea;
    
    /**
     * 当前任务
     */
    @TableField(value = "MISSION")
    private String mission;
    
    /**
     * 绑定标签号
     */
    @TableField(value = "TAG")
    private String tag;

    /**
     * 行车当前登陆员ID
     */
    private Integer loginer;
}
