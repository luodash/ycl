package com.tbl.modules.wms.entity.requestxml;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * FEWMS006  WMS接口-发货单接口
 * @author 70486
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "status",
        "shipno",
        "printtime",
        "contractno",
        "orderno",
        "orderline",
        "transportname",
        "customer",
        "org",
        "batchs",
})
@XmlRootElement(name = "slipdata")
@Getter
@Setter
@ToString
public class FEWMS006_slipdata {
	public String status;
	public String shipno;
	public String printtime;
	public String contractno;
	public String orderno;
	public String orderline;
	public String transportname;
	public String customer;
	public String org;
    public FEWMS006_batchs batchs;

}
