package com.tbl.modules.wms.entity.allo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 调拨管理 调拨表
 * @author 70486
 */
@TableName("YDDL_ALLOCATION")
@Getter
@Setter
@ToString
public class Allocation implements Serializable {

    /**
     * 主键
     */
    @TableId(value = "ID")
    private Long id;

    /**
     * 调拨单编号
     */
    @TableField(value = "ALLCODE")
    private String allCode;

    /**
     * 调入仓库
     */
    @TableField(value = "INWAREHOUSECODE")
    private String inWarehouseCode;
    
    /**
     * 调入仓库名称
     */
    @TableField(exist = false)
    private String inWarehouseName;

    /**
     * 调出仓库
     */
    @TableField(value = "OUTWAREHOUSECODE")
    private String outWarehouseCode;
    
    /**
     * 调出仓库名称
     */
    @TableField(exist = false)
    private String outWarehouseName;

    /**
     * 调拨单创建时间
     */
    @TableField(value = "MOVETIME")
    private Date moveTime;
    
    /**
     * 调拨单创建时间string
     */
    @TableField(exist = false)
    private String moveTimeStr;

    /**
     * 调拨完成时间
     */
    @TableField(value = "COMPLETETIME")
    private Date completeTime;

    /**
     * 调拨人员主键
     */
    @TableField(value = "USER_ID")
    private Long userId;

    /**
     * 状态
     */
    @TableField(value = "STATE")
    private Integer state;

    /**
     * 发货单编号
     */
    @TableField(value = "SHIPNO")
    private String shipNo;

    /**
     * 调拨人员名称
     */
    @TableField(exist = false)
    private String userName;

    /**
     * 调拨入库仓库主键
     */
    @TableField(value = "IN_WAREHOUSE_ID")
    private Long inWarehouseId;

    /**
     * 调拨出库仓库主键
     */
    @TableField(value = "OUT_WAREHOUSE_ID")
    private Long outWarehouseId;

    /**
     * 销售人员名称
     */
    @TableField(exist = false)
    private String salesName;
    
    /**
     * 调拨状态
     */
    @TableField(exist = false)
    private String alloState;
    
    /**
     * 编辑状态
     */
    @TableField(exist = false)
    private String editState;
    
    /**
     * 调出厂区编码
     */
    @TableField(value = "FACTORYCODE")
    private String factoryCode;
    
    /**
     * 调入厂区编码
     */
    @TableField(value = "INFACTORYCODE")
    private String inFactoryCode;
    
    /**
     * 调出厂区主键
     */
    @TableField(exist = false)
    private String factoryId;
    
    /**
     * 调出厂区名称
     */
    @TableField(exist = false)
    private String factoryName;
    
    /**
     * 调入厂区名称
     */
    @TableField(exist = false)
    private String inFactoryName;
    
    /**
     * 开始调拨出库的时间（第一笔）
     */
    @TableField(exist = false)
    private Date startOutTime;

    /**
     * 开始调拨出库的时间（第一笔）字符串
     */
    @TableField(exist = false)
    private String startOutTimeStr;
    
    /**
     * 结束调拨入库的时间（最后一笔）
     */
    @TableField(exist = false)
    private Date endInTime;

    /**
     * 结束调拨入库的时间（最后一笔）字符串
     */
    @TableField(exist = false)
    private String endInTimeStr;
    
    /**
     * 调入厂区主键
     */
    @TableField(exist = false)
    private String infactoryId;
    
    /**
     * 调出厂区主键
     */
    @TableField(exist = false)
    private String outfactoryId;

    /**
     * rfid
     */
    @TableField(exist = false)
    private String rfid;

    /**
     * 质保号
     */
    @TableField(exist = false)
    private String qaCode;
}
