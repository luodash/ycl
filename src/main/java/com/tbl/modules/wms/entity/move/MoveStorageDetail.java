package com.tbl.modules.wms.entity.move;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 移库处理--移库详情表
 * @author 70486
 */
@TableName("YDDL_MOVESTORAGE_DETAIL")
@Getter
@Setter
@ToString
public class MoveStorageDetail implements Serializable {

    /**
     * 主键
     */
    @TableId(value = "ID")
    public Long id;

    /**
     * 物料编码
     */
    @TableField(value = "MATERIALCODE")
    private String materialCode;

    /**
     * 数量
     */
    @TableField(value = "METER")
    private String meter;

    /**
     * RFID
     */
    @TableField(value = "RFID")
    private String rfid;

    /**
     * 质保号
     */
    @TableField(value = "QACODE")
    private String qaCode;

    /**
     * 主表id
     */
    @TableField(value = "PID")
    private Long pId;

    /**
     * 状态 1:未开始 2.开始移库出库 3.完成移库出库 4.开始移库入库 5.完成移库入库
     */
    @TableField(value = "STATE")
    private Integer state;
    
    /**
     * 状态名称
     */
    @TableField(exist = false)
    private String stateStr;

    /**
     * 移入行车编号
     */
    @TableField(value = "INCARCODE")
    private String inCarCode;

    /**
     * 移出行车编号
     */
    @TableField(value = "OUTCARCODE")
    private String outCarCode;

    /**
     * 批次号
     */
    @TableField(value = "BATCH_NO")
    private String batchNo;

    /**
     * 移入库位编号
     */
    @TableField(value = "INSHELFCODE")
    private String inShelfCode;

    /**
     * 移出库位编号
     */
    @TableField(value = "OUTSHELFCODE")
    private String outShelfCode;

    /**
     * 仓库编号
     */
    @TableField(value = "WAREHOUSECODE")
    private String warehouseCode;

    /**
     * 入库时间
     */
    @TableField(value = "INSTORAGETIME")
    private Date inStorageTime;
    
    /**
     * 入库时间string
     */
    @TableField(exist = false)
    private String inStorageTimeStr;

    /**
     * 出库时间
     */
    @TableField(value = "OUTSTORAGETIME")
    private Date outstoragetime;

    /**
     * 组织机构
     */
    @TableField(value = "ORG")
    private String org;

    /**
     * 营销经理编码
     */
    @TableField(value = "SALESCODE")
    private String salesCode;

    /**
     * 营销经理名称
     */
    @TableField(value = "SALESNAME")
    private String salesName;

    /**
     * 开始移库出库时间
     */
    @TableField(value = "STARTOUTSTORAGETIME")
    private Date startOutStorageTime;

    /**
     * 开始移库出库时间
     */
    @TableField(exist = false)
    private String startOutStorageTimeStr;

    /**
     * 移库出库人
     */
    @TableField(value = "OUTSTORAGEID")
    private Long outStorageId;

    /**
     * 开始移库入库时间
     */
    @TableField(value = "STARTINSTORAGETIME")
    private Date startInStorageTime;

    /**
     * 移库入库人
     */
    @TableField(value = "INSTORAGEID")
    private Long inStorageId;

    /**
     * 工单号
     */
    @TableField(value = "ENTITYNO")
    private String entityNo;
    
    /**
     * 盘具编码
     */
    @TableField(value = "DISHNUMBER")
    private String dishnumber;

    /**
     * 出库类型
     */
    @TableField(exist = false)
    private String outstorageType;

    /**
     * 行车id
     */
    @TableField(exist = false)
    private Long carId;

    /**
     * 仓库id
     */
    @TableField(exist = false)
    private Long warehouseId;

    /**
     * 库位id
     */
    @TableField(exist = false)
    private Long shelfId;

    /**
     * 库位编码
     */
    @TableField(exist = false)
    private Long shelfCode;

    /**
     * 行车编码
     */
    @TableField(exist = false)
    private String carCode;
    
    /**
     * 拣货人名称
     */
    @TableField(exist = false)
    private String pickManName;
    
    /**
     * 移库单号
     */
    @TableField(exist = false)
    private String moveCode;

    /**
 	/**
     * 盘具类型
     */
    @TableField(exist = false)
    private String drumType;
 	
 	/**
     * 盘具内径
     */
    @TableField(exist = false)
    private Long innerDiameter;

    /**
     * 盘具外径
     */
    @TableField(exist = false)
    private Long outerDiameter;
    
    /**
     * 规格型号
     */
    @TableField(exist = false)
    private String model;
    
     /**
     * 物料名称
     */
    @TableField(exist = false)
    private String materialName;

    /**
     * 移库单创建时间
     */
    @TableField(exist = false)
    private String movetimeStr;

    /**
     * 厂区名称
     */
    @TableField(exist = false)
    private String factoryName;

    /**
     * 仓库名称
     */
    @TableField(exist = false)
    private String warehouseName;

    /**
     * 创建人名称
     */
    @TableField(exist = false)
    private String username;


}
