package com.tbl.modules.wms.entity.responsexml.splitdish;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 分盘header  FEWMS016  WMS接口-分盘
 * @author 70486
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "plantno",
        "plantname"
})
@XmlRootElement(name = "header")
@Getter
@Setter
@ToString
public class splitDishHeader {

    public String plantno;
    public String plantname;

}

