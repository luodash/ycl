package com.tbl.modules.wms.entity.allo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 调拨管理 调拨表详情表
 * @author 70486
 */
@TableName("YDDL_ALLOCATION_DETAIL")
@Getter
@Setter
@ToString
public class AllocationDetail implements Serializable {

    /**
     * 主键
     */
    @TableId(value = "ID")
    public Long id;
    
    /**
     * 物料编码
     */
    @TableField(value = "MATERIALCODE")
    private String materialCode;
    
    /**
     * 物料名称
     */
    @TableField(exist = false)
    private String materialName;

    /**
     * 数量
     */
    @TableField(value = "METER")
    private String meter;

    /**
     * RFID
     */
    @TableField(value = "RFID")
    private String rfid;

    /**
     * 质保号
     */
    @TableField(value = "QACODE")
    private String qaCode;

    /**
     * 主表id
     */
    @TableField(value = "PID")
    private Long pId;

    /**
     * 状态 1:未开始 2.开始调拨出库 3.完成调拨出库 4.开始调拨入库 5.完成调拨入库
     */
    @TableField(value = "STATE")
    private Integer state;

    /**
     * 状态
     */
    @TableField(exist = false)
    private String stateStr;

    /**
     * 调入行车编号
     */
    @TableField(value = "INCARCODE")
    private String inCarCode;
    @TableField(exist = false)
    private String inCarName;

    /**
     * 调出行车编号
     */
    @TableField(value = "OUTCARCODE")
    private String outCarCode;
    @TableField(exist = false)
    private String outCarName;

    /**
     * 批次号
     */
    @TableField(value = "BATCH_NO")
    private String batchNo;

    /**
     * 调入库位编号
     */
    @TableField(value = "INSHELFCODE")
    private String inShelfCode;

    /**
     * 调出库位编号
     */
    @TableField(value = "OUTSHELFCODE")
    private String outShelfCode;

    /**
     * 调入仓库编号
     */
    @TableField(value = "INWAREHOUSECODE")
    private String inWarehouseCode;

    /**
     * 调出仓库编号
     */
    @TableField(value = "OUTWAREHOUSECODE")
    private String outWarehouseCode;

    /**
     * 入库时间
     */
    @TableField(value = "INSTORAGETIME")
    private Date inStorageTime;

    /**
     * 入库时间字符串
     */
    @TableField(exist = false)
    private String inStorageTimeStr;

    /**
     * 销售经理编号
     */
    @TableField(value = "SALESCODE")
    private String salesCode;

    /**
     * 销售经理名称
     */
    @TableField(value = "SALESNAME")
    private String salesName;

    /**
     * 出库开始时间
     */
    @TableField(value = "STARTOUTSTORAGETIME")
    private Date startOutStorageTime;

    /**
     * 出库时间
     */
    @TableField(value = "OUTSTORAGETIME")
    private Date outStorageTime;

    /**
     * 出库时间
     */
    @TableField(exist = false)
    private String outStorageTimeStr;

    /**
     * 出库人主键
     */
    @TableField(value = "OUTSTORAGEID")
    private Long outStorageId;

    /**
     * 出库人姓名
     */
    @TableField(exist = false)
    private String outStorageName;

    /**
     * 入库人主键
     */
    @TableField(value = "INSTROAGEID")
    private Long inStorageId;

    /**
     * 开始入库时间
     */
    @TableField(value = "STARTINSTORAGETIME")
    private Date startInStorageTime;

    /**
     * 调入厂区
     */
    @TableField(value = "ORG")
    private String org;

    /**
     * 工单号
     */
    @TableField(value = "ENTITYNO")
    private String entityNo;
    
    /**
     * 盘具编码
     */
    @TableField(value = "DISHNUMBER")
    private String dishnumber;

    /**
     * 库位id
     */
    @TableField(exist = false)
    private Long shelfId;

    /**
     * 库位编码
     */
    @TableField(exist = false)
    private String shelfCode;

    /**
     * 行车id
     */
    @TableField(exist = false)
    private Long carId;

    /**
     * 行车编码
     */
    @TableField(exist = false)
    private String carCode;

    /**
     * 拣货人
     */
    @TableField(exist = false)
    private String pickManName;

    /**
     * 仓库id
     */
    @TableField(exist = false)
    private Long warehouseId;

    /**
     * 仓库编码
     */
    @TableField(exist = false)
    private String warehouseCode;
    
    /**
     * 调拨单号
     */
    @TableField(exist = false)
    private String allCode;
    
   /**
	/**
    * 盘具类型
    */
   @TableField(exist = false)
   private String drumType;
	
	/**
    * 盘具内径
    */
   @TableField(exist = false)
   private Long innerDiameter;

   /**
    * 盘具外径
    */
   @TableField(exist = false)
   private Long outerDiameter;
   
   /**
    * 规格型号
    */
   @TableField(exist = false)
   private String model;

    /**
     * 调拨单创建时间
     */
    @TableField(exist = false)
    private String movetime;

    /**
     * 调拨单创建完成
     */
    @TableField(exist = false)
    private String completetime;


    /**
     * 调拨单创建人工号
     */
    @TableField(exist = false)
    private String userId;

    /**
     * 调出厂区
     */
    @TableField(exist = false)
    private String factoryCode;

    /**
     * 调入厂区
     */
    @TableField(exist = false)
    private String inFacotyrCode;

    /**
     * 调出厂区名称
     */
    @TableField(exist = false)
    private String factoryName;

    /**
     * 调入厂区名称
     */
    @TableField(exist = false)
    private String inFactoryName;

    /**
     * 创建人名称
     */
    @TableField(exist = false)
    private String userName;

    /**
     * 调入仓库名称
     */
    @TableField(exist = false)
    private String inWarehouseName;

    /**
     * 调出仓库名称
     */
    @TableField(exist = false)
    private String outWarehouseName;

    /**
     * 是否超宽
     */
    @TableField(exist = false)
    private Integer superWide;

    /**
     * 目标库位（移库入库提交时的选中的库位）
     */
    @TableField(value = "TARGET_SHELF")
    private String targetShelf;

}
