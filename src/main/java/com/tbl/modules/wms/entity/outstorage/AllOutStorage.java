package com.tbl.modules.wms.entity.outstorage;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 1:退库回生产线  2.采购退货  3.报废,4.盘亏出库  汇总实体报表
 * @author 70486
 */
@TableName("YDDL_ALL_OUTSTORAGE")
@Getter
@Setter
@ToString
public class AllOutStorage {

	/**
	 * 主键
	 */
    @TableId(value = "ID")
    private Long id;

    /**
     * 组织机构
     */
    @TableField(value = "ORG")
    private String org;

    /**
     * 物料编码
     */
    @TableField(value = "MATERIALCODE")
    private String materialCode;

    /**
     * 批次号
     */
    @TableField(value = "BATCH_NO")
    private String batchNo;

    /**
     * 质保号
     */
    @TableField(value = "QACODE")
    private String qaCode;

    /**
     * 营销经理工号
     */
    @TableField(value = "SALESCODE")
    private String salesCode;

    /**
     * 营销经理名称
     */
    @TableField(value = "SALESNAME")
    private String salesName;

    /**
     * 主表id
     */
    @TableField(value = "P_ID")
    private Integer pId;

    /**
     * rfid
     */
    @TableField(value = "RFID")
    private String rfid;

    /**
     * 数量
     */
    @TableField(value = "METER")
    private String meter;

    /**
     * 创建时间
     */
    @TableField(value = "CREATETIME")
    private Date createTime;
    @TableField(exist = false)
    private String createTimeStr;

    /**
     * 状态
     */
    @TableField(value = "STATE")
    private Integer state;

    /**
     * 仓库编码
     */
    @TableField(value = "WAREHOUSECODE")
    private String warehouseCode;

    /**
     * 行车编码
     */
    @TableField(value = "CARCODE")
    private String carCode;

    /**
     * 库位编码
     */
    @TableField(value = "SHELFCODE")
    private String shelfCode;

    /**
     * 出库时间
     */
    @TableField(value = "OUTSTORAGETIME")
    private Date outStorageTime;

    /**
     * 出库人
     */
    @TableField(value = "OUTSTORAGEID")
    private Integer outStorageId;

    /**
     * 1:退库回生产线  2.采购退货  3.报废  4.盘亏出库
     */
    @TableField(value = "TYPE")
    private Integer type;
    @TableField(exist = false)
    private String typeStr;
    
    /**
     * 入库时间
     */
    @TableField(value = "INSTORAGETIME")
    private Date instoragetime;
    
    /**
     * 入库人
     */
    @TableField(value = "INSTORAGEID")
    private Long instorageid;
    
    /**
     * 工单号
     */
    @TableField(value = "ENTITYNO")
    private String entityno;
    
    /**
     * 订单号
     */
    @TableField(value = "ORDERNO")
    private String orderno;
    
    /**
     * 订单行号
     */
    @TableField(value = "ORDERLINE")
    private String orderline;
    
    /**
     * 盘具编码
     */
    @TableField(value = "DISHNUMBER")
    private String dishnumber;
    
    /**
     * 仓库名称
     */
    @TableField(exist = false)
    private String warehouseName;
    
    /**
     * 盘具类型
     */
    @TableField(exist = false)
    private String drumType;
 	
 	/**
     * 盘具内径
     */
    @TableField(exist = false)
    private Long innerDiameter;

    /**
     * 盘具外径
     */
    @TableField(exist = false)
    private Long outerDiameter;
    
    /**
     * 规格型号
     */
    @TableField(exist = false)
    private String model;
    
     /**
     * 物料名称
     */
    @TableField(exist = false)
    private String materialName;

    /**
     * 盘号
     */
    @TableField(exist = false)
    private String dishcode;
}
