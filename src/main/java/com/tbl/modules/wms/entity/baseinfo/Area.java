package com.tbl.modules.wms.entity.baseinfo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 库区
 * @author zxf
 */
@TableName("YDDL_AREA")
@Getter
@Setter
@ToString
public class Area implements Serializable {

    /**
     * 主键
     */
    @TableField(value = "ID")
    private Long id;

    /**
     * 库区编码
     */
    @TableField(value = "CODE")
    private String code;

    /**
     * 库区名称
     */
    @TableField(value = "AREA_NAME")
    private String name;
}
