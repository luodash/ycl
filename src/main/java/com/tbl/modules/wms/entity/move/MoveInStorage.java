package com.tbl.modules.wms.entity.move;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 移库处理--移库入库表
 * @author 70486
 */
@TableName("YDDL_MOVEINSTORAGE")
@Getter
@Setter
@ToString
public class MoveInStorage implements Serializable {

	/**
	 * 主键
	 */
    @TableId(value = "ID")
    private Long id;

    /**
     * 组织机构
     */
    @TableField(value = "ORG")
    private String org;

    /**
     * 物料编码
     */
    @TableField(value = "MATERIALCODE")
    private String materialCode;

    /**
     * 批次号
     */
    @TableField(value = "BATCH_NO")
    private String batchNo;

    /**
     * 质保号
     */
    @TableField(value = "QACODE")
    private String qaCode;

    /**
     * 营销经理工号
     */
    @TableField(value = "SALESCODE")
    private String salesCode;

    /**
     * 营销经理名称
     */
    @TableField(value = "SALESNAME")
    private String salesName;

    /**
     * 主表ID
     */
    @TableField(value = "P_ID")
    private Long pId;

    /**
     * RFID编码
     */
    @TableField(value = "RFID")
    private String rfid;

    /**
     * 数量
     */
    @TableField(value = "METER")
    private String meter;

    /**
     * 状态
     */
    @TableField(value = "STATE")
    private Integer state;

    /**
     * 入库时间
     */
    @TableField(value = "INSTORAGETIME")
    private Date instorageTime;

    /**
     * 仓库编码
     */
    @TableField(value = "WAREHOUSECODE")
    private String warehouseCode;

    /**
     * 行车编码
     */
    @TableField(value = "CARCODE")
    private String carCode;

    /**
     * 库位编码
     */
    @TableField(value = "SHELFCODE")
    private String shelfCode;

    /**
     * 入库人id
     */
    @TableField(value = "INSTORAGEID")
    private Long inStorageId;

    /**
     * 开始入库时间
     */
    @TableField(value = "STARTSTORAGETIME")
    private Date startStorageTime;

    /**
     * 确认人名称
     */
    @TableField(exist = false)
    private String confirmName;

    /**
     * 工单号
     */
    @TableField(exist = false)
    private String entityNo;

    /**
     * 状态名称
     */
    @TableField(exist = false)
    private String stateCode;

    /**
     * 行车id
     */
    @TableField(exist = false)
    private Long carId;

    /**
     * 仓库id
     */
    @TableField(exist = false)
    private Long warehouseId;

    /**
     * 库位id
     */
    @TableField(exist = false)
    private Long shelfId;

    /**
     * 盘具编码
     */
    @TableField(value = "DISHNUMBER")
    private String dishnumber;

    /**
     * 入库行车工主键
     */
    private Integer incarerid;
}
