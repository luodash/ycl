package com.tbl.modules.wms.entity.requestxml;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

/**
 * FEWMS005  WMS接口-保留接口
 * @author 70486
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "batchs",
        "reservdata"
})
@XmlRootElement(name = "data")
@Getter
@Setter
@ToString
public class FEWMS005_data {
    public List<FEWMS005_reservdata> reservdata;
    public FEWMS005_batchs batchs;
}

