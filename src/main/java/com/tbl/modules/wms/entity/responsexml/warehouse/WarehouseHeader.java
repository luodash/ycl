package com.tbl.modules.wms.entity.responsexml.warehouse;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 仓库header FEWMS013  WMS接口-基础数据-仓库
 * @author 70486
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "plantcode",
        "warehouseno",
        "warehousename"
})
@XmlRootElement(name = "header")
@Getter
@Setter
@ToString
public class WarehouseHeader {

    public String plantcode;
    public String warehouseno;
    public String warehousename;

}