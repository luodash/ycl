package com.tbl.modules.wms.entity.requestxml;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * FEWMS010  WMS接口-报废接口
 * @author 70486
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "lotnum",
        "qty"
})
@XmlRootElement(name = "baofei")
@Getter
@Setter
@ToString
public class FEWMS010_batchs {
	public String lotnum;
    public String qty;
}
