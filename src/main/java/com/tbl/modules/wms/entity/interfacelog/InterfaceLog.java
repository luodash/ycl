package com.tbl.modules.wms.entity.interfacelog;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 接口日志记录
 * @author 70486
 */
@TableName("YDDL_INTERFACE_LOG")
@Getter
@Setter
@ToString
public class InterfaceLog implements Serializable {

	/**
	 * 主键
	 */
    @TableId(value = "ID")
    private Long id;

    /**
     * 接口编码
     */
    @TableField(value = "INTERFACE_CODE")
    private String interfaceCode;

    /**
     * 接口名称
     */
    @TableField(value = "INTERFACE_NAME")
    private String interfaceName;

    /**
     * 创建时间
     */
    @TableField(value = "CREATE_TIME")
    private Date createTime;

    /**
     * 请求报文
     */
    @TableField(value = "REQUEST_MESSAGE")
    private String requestMessage;
    
    /**
     * 返回报文
     */
    @TableField(value = "RESPONSE_MESSAGE")
    private String responseMessage;

    /**
     * 质保号
     */
    @TableField(value = "QACODE")
    private String qacode;

    /**
     * 批次号
     */
    @TableField(value = "BATCH_NO")
    private String batchNo;

    /**
     * 成品/原材料 接口区分标识，1一期成品；2二期原材料；3智能复合
     */
    @TableField(value = "TYPE")
    private Integer type;
}
