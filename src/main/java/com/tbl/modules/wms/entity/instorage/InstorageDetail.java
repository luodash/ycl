package com.tbl.modules.wms.entity.instorage;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 入库明细表
 * @author 70486
 */
@TableName("YDDL_INSTORAGE_DETAIL")
@Getter
@Setter
@ToString
public class InstorageDetail implements Serializable {

    /**
     * 主键
     */
    @TableId(value = "ID")
    private Long id;

    /**
     * 组织机构
     */
    @TableField(value = "ORG")
    private String org;
    
    /**
     * 组织机构名称
     */
    @TableField(exist = false)
    private String orgName;

    /**
     * 物料编码
     */
    @TableField(value = "MATERIALCODE")
    private String materialCode;

    /**
     * 批次号
     */
    @TableField(value = "BATCH_NO")
    private String batchNo;

    /**
     * 质保号
     */
    @TableField(value = "QACODE")
    private String qaCode;

    /**
     * 营销经理工号
     */
    @TableField(value = "SALESCODE")
    private String salesCode;

    /**
     * 营销经理名称
     */
    @TableField(value = "SALESNAME")
    private String salesName;

    /**
     * 父id
     */
    @TableField(value = "P_ID")
    private Long pId;

    /**
     * RFID编号
     */
    @TableField(value = "RFID")
    private String rfid;

    /**
     * 数量
     */
    @TableField(value = "METER")
    private String meter;
    
    /**
     * 标签初始化时间
     */
    @TableField(value = "CREATETIME")
    private Date createtime;

    /**
     * 标签初始化时间字符串
     */
    @TableField(exist = false)
    private String createtimeStr;

    /**
     * 包装时间
     */
    @TableField(value = "CONFIRMTIME")
    private Date confirmTime;

    /**
     * 包装时间字符串
     */
    @TableField(exist = false)
    private String confirmTimeStr;

    /**
     * 包装人主键
     */
    @TableField(value = "CONFIRMBY")
    private Long confirmBy;

    /**
     * 包装人工号
     */
    @TableField(exist = false)
    private String confirmNo;

    /**
     * 状态
     */
    @TableField(value = "STATE")
    private Integer state;

    /**
     * 状态字符
     */
    @TableField(exist = false)
    private String stateStr;

    /**
     * 入库人主键
     */
    @TableField(value = "INSTORAGEID")
    private Long inStorageId;

    /**
     * 入库人工号
     */
    @TableField(exist = false)
    private String instorageNo;

    /**
     * 入库人名称
     */
    @TableField(exist = false)
    private String instorageName;

    /**
     * 入库时间
     */
    @TableField(value = "INSTORAGETIME")
    private Date instorageTime;

    /**
     * 入库时间字符串
     */
    @TableField(exist = false)
    private String instorageTimeStr;

    /**
     * 仓库编码
     */
    @TableField(value = "WAREHOUSECODE")
    private String warehouseCode;

    /**
     * 仓库名称
     */
    @TableField(exist = false)
    private String warehouseName;

    /**
     * 行车编码
     */
    @TableField(value = "CARCODE")
    private String carCode;

    /**
     * 库位编码
     */
    @TableField(value = "SHELFCODE")
    private String shelfCode;


    /**
     * 扫码时间
     */
    @TableField(value = "STARTSTORAGETIME")
    private Date startStorageTime;

    /**
     * 扫码时间
     */
    @TableField(exist = false)
    private String  startStorageTimeStr;

    /**
     * 包装人名称
     */
    @TableField(exist = false)
    private String confirmName;

    /**
     * 工单号
     */
    @TableField(exist = false)
    private String entityNo;

    /**
     * 状态名称
     */
    @TableField(exist = false)
    private String stateCode;

    /**
     * 行车id
     */
    @TableField(exist = false)
    private Long carId;

    /**
     * 仓库主键
     */
    @TableField(exist = false)
    private Long warehouseId;

    /**
     * 库位主键
     */
    @TableField(exist = false)
    private Long shelfId;

    /**
     * 入库类型
     */
    @TableField(exist = false)
    private String instorageType;

    /**
     * 产线编号（入库生产厂主键）
     */
    @TableField(value = "FACTORYID")
    private String factoryid;
    
    /**
     * 段号（字符串）
     */
    @TableField(value = "SEGMENTNO")
    private String segmentno;
    
    /**
     * 入库类型
     */
    @TableField(value = "STORAGETYPE")
    private Long storagetype;

    /**
     * 入库类型字符类型
     */
    @TableField(exist = false)
    private String storagetypeStr;
    
    /**
     * 颜色
     */
    @TableField(value = "COLOUR")
    private String colour;
    
    /**
     * 重量
     */
    @TableField(value = "WEIGHT")
    private String weight;
    
    /**
     * 报验单号
     */
    @TableField(value = "INSPECTNO")
    private String inspectno;
    
    /**
     * 外协入电缆质保号id
     */
    @TableField(exist = false)
    private Long qacode_id;
    
    /**
     * 外协入电缆质保号name
     */
    @TableField(exist = false)
    private String qacode_name;

    /**
     * 订单行号
     */
    @TableField(value = "ORDERLINE")
    private String orderline;

    /**
     * 订单号
     */
    @TableField(value = "ORDERNUM")
    private String ordernum;
    
    /**
     * 厂区编码
     */
    @TableField(value = "FACTORYCODE")
    private String factoryCode;
    
    /**
     * 厂区名称
     */
    @TableField(exist = false)
    private String factoryName;
    
    /**
     * 传入盘号
     */
    @TableField(value = "DISHNUMBER")
    private String dishnumber;
    
    /**
     * 传出盘号
     */
    @TableField(value = "DISHCODE")
    private String dishcode;
    
    /**
     * 单位
     */
    @TableField(value = "UNIT")
    private String unit;
    
    /**
     * 销售订单头id
     */
    @TableField(value = "OEHEADER")
    private String oeheader;
    
    /**
     * 销售订单行id
     */
    @TableField(value = "OELINE")
    private String oeline;
    
    /**
     * 位置id
     */
    @TableField(value = "LOCATIONID")
    private String locationid;
    
    /**
     * 单位
     */
    @TableField(value = "CUSTOMERID")
    private String customerid;

    /**
     * 厂区名称
     */
    @TableField(exist = false)
    private String name;
    
    /**
     * 销售退货入库和外采入库的事务处理时间
     */
    @TableField(exist = false)
    private String time;
    
    /**
     * 车间名称
     */
    @TableField(exist = false)
    private String workshopname;
    
    /**
     * MES终端IP
     */
    @TableField(value = "CLIENTIP")
    private String clientip;
    
    /**
     * 特殊标识	
     */
    @TableField(value = "SPECIALSIGN")
    private String specialsign;
    
    /**
      * 盘具类型
      */
     @TableField(exist = false)
     private String drumType;
  	
  	/**
      * 盘具内径
      */
     @TableField(exist = false)
     private Long innerDiameter;

     /**
      * 盘具外径
      */
     @TableField(exist = false)
     private Long outerDiameter;
     
     /**
      * 规格型号
      */
     @TableField(exist = false)
     private String model;
     
      /**
      * 物料名称
      */
     @TableField(exist = false)
     private String materialName;

    /**
     * 扫描时间
     */
    @TableField(value = "SCANTIME")
    private Date scantime;

    /**
     * 盘数
     */
    @TableField(exist = false)
    private Long dishnum;

    /**
     * 入库扫描人
     */
    @TableField(value = "STARTSTORAGEID")
    private Long startstorageid;

    /**
     * 入库扫描人
     */
    @TableField(exist = false)
    private String startstorageName;

    /**
     * 销售退货质保号
     */
    @TableField(value = "RETURNQACODE")
    private String returnqacode;

    /**
     * 吊装人
     */
    @TableField(value = "HOISTINGID")
    private Long hoistingid;

    /**
     * 吊装人姓名
     */
    @TableField(exist = false)
    private String hoistingName;

    /**
     * 吊装时间
     */
    @TableField(value = "HOISTINGTIME")
    private Date hoistingTime;

    /**
     * 吊装时间字符
     */
    @TableField(exist = false)
    private String hoistingTimeStr;

    /**
     * 复绕人
     */
    @TableField(value = "REWINDINGID")
    private Long rewindingid;

    /**
     * 复绕人姓名
     */
    @TableField(exist = false)
    private String rewindName;

    /**
     * 复绕时间
     */
    @TableField(value = "REWINDINGTIME")
    private Date rewindingTime;

    /**
     * 台账类型
     */
    @TableField(exist = false)
    private String dictTypeNum;

    /**
     * 吊装状态
     */
    @TableField(value = "HOISTSTATE")
    private Integer hoistState;

    /**
     * 吊装状态描述
     */
    @TableField(exist = false)
    private String hoistStateStr;

    /**
     * 复绕状态
     */
    @TableField(value = "REWINDSTATE")
    private Integer rewindState;

    /**
     * 复绕状态描述
     */
    @TableField(exist = false)
    private String rewindStateStr;

    /**
     * 是否超宽
     */
    @TableField(exist = false)
    private Integer superWide;

    /**
     * 盘点结果：1正常；2盘盈；3盘亏
     */
    @TableField(value = "INVENTORY_STATE")
    private Integer inventoryState;

    /**
     * 库存表中的盘点结果
     */
    @TableField(exist = false)
    private Integer storageInfoInventoryState;

    /**
     *盘点编辑米数、重量
     */
    @TableField(value = "INVENTORY_METER")
    private String inventoryMeter;

    /**
     *盘点编辑库位
     */
    @TableField(value = "INVENTORY_SHELF")
    private String inventoryShelf;

    /**
     *采购退货时间
     */
    @TableField(value = "PURCHASE_RETURN_TIME")
    private Date purchaseReturnTime;

    /**
     * 盘点库位库位头
     */
    @TableField(exist = false)
    private String inventoryShelfCode;

    /**
     * 入库行吊员id
     */
    private Integer instorageCarId;
}
