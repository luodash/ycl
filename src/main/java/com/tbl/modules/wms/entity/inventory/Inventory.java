package com.tbl.modules.wms.entity.inventory;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 库存盘点--盘点计划信息表
 * @author 70486
 */
@TableName("YDDL_INVENTORY")
@Getter
@Setter
@ToString
public class Inventory implements Serializable {

    @TableId(value = "ID")
    private Long id;

    /**
     * 盘点编号
     */
    @TableField(value = "CODE")
    private String code;

    /**
     * 创建时间
     */
    @TableField(value = "CREATETIME")
    private Date createtime;

    /**
     * 仓库编号
     */
    @TableField(value = "WAREHOUSECODE")
    private String warehousecode;
    
    /**
     * 仓库名称
     */
    @TableField(exist = false)
    private String warehousename;

    /**
     * 创建人
     */
    @TableField(value = "CREATER")
    private String creater;
    
    /**
     * 盘点类型名称
     */
    @TableField(exist = false)
    private String inventoryTypeName;
    
    /**
     * 执行人id
     */
    @TableField(value = "USER_ID")
    private String userId;
    
    /**
     * 执行人
     */
    @TableField(exist = false)
    private String userName;
    
    /**
     * 状态
     */
    @TableField(value = "STATE")
    private Long state;
    
    /**
     * 状态名称
     */
    @TableField(exist = false)
    private String stateName;
    
    /**
     * 审核结论
     */
    @TableField(value = "RESULT")
    private Long result;

    /**
     * 厂区编码
     */
    @TableField(value = "FACTORYCODE")
    private String factorycode;
    
    /**
     * 厂区名称
     */
    @TableField(exist = false)
    private String factoryname;
    
    /**
     * 厂区名称
     */
    @TableField(exist = false)
    private String name;

    /**
     * ebs的盘点单号
     */
    @TableField(value = "EBS_CODE")
    private String ebsCode;

    /**
     * 审核确认日期
     */
    @TableField(value = "AUDIT_DATE")
    private Date auditDate;
}
