package com.tbl.modules.wms.entity.productbind;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *  产线ip、webservice地址 实体类
 * @author 70486
 */
@TableName("YDDL_PRODUCT_BIND")
@Getter
@Setter
@ToString
public class ProductBind {

	/**
	 * 主键
	 */
    @TableId(value = "ID")
    private Long id;

    /**
     * 产线id
     */
    @TableField(value = "FACTORY_ID")
    private String factoryId;

    /**
     * 产线对应的电脑ip
     */
    @TableField(value = "IPADDRESS")
    private String ipaddress;

    /**
     * websercie接口地址
     */
    @TableField(value = "CONTENT")
    private String content;

}
