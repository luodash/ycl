package com.tbl.modules.wms.entity.outstorage;


import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 物资流转单详情
 * @author wc
 */
@TableName("YDDL_EXCHANGE_PRINT_DETAIL")
@Getter
@Setter
@ToString
public class ExchangePrintDetail implements Serializable {

    /**
     * 主键
     */
    @TableId(value = "ID")
    private Long id;

    /**父ID**/
    @TableField(value = "PID")
    private Long pid;

    /**规格**/
    @TableField(value = "MODEL")
    private String model;

    /**数量**/
    @TableField(value = "NUM")
    private String num;

    /**单位**/
    @TableField(value = "UNIT")
    private String unit;

    /**颜色**/
    @TableField(value = "COLOUR")
    private String color;

    /**库位**/
    @TableField(value = "SHELFCODE")
    private String shelfcode;

    /**盘号**/
    @TableField(value = "DISHCODE")
    private String dishcode;

    /**段号**/
    @TableField(value = "SEGMENTNO")
    private String segmentno;

    /**出库编号**/
    @TableField(value = "QACODE")
    private String qacode;

    /**重量**/
    @TableField(value = "WEIGHT")
    private String weight;

    /**定制**/
    @TableField(value = "DINGZHI")
    private String dingzhi;

    /**备注**/
    @TableField(value = "REMARK")
    private String remark;

    @TableField(value = "SHIP_NO")
    private String shipNo;
}
