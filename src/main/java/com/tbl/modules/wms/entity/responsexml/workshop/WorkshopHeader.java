package com.tbl.modules.wms.entity.responsexml.workshop;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 车间header FEWMS026  WMS接口-基础数据-车间
 * @author 70486
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "",propOrder = {
        "orgid",
        "workshopnumber",
        "workshopname"
})
@XmlRootElement(name = "header")
@Getter
@Setter
@ToString
public class WorkshopHeader {
	public String orgid;
    public String workshopnumber;
    public String workshopname;
}
