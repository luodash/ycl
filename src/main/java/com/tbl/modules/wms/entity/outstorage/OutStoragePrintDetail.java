package com.tbl.modules.wms.entity.outstorage;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 出库单打印明细主表
 * @author 70486
 */
@TableName("YDDL_PRINT_INFO_DETAIL")
@Getter
@Setter
@ToString
public class OutStoragePrintDetail implements Serializable {

    /**
     * 主键
     */
    @TableId(value = "ID")
    private Long id;

    /**
     * 批次号
     */
    @TableField(value = "BATCH_NO")
    private String batchNo;

    /**
     * 父节点
     */
    @TableField(value = "PID")
    private String pid;

}
