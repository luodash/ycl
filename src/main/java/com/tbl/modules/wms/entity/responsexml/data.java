package com.tbl.modules.wms.entity.responsexml;

import com.tbl.modules.wms.entity.responsexml.disc.DiscXml;
import com.tbl.modules.wms.entity.responsexml.externalcable.ExternalcableXml;
import com.tbl.modules.wms.entity.responsexml.factory.PlantXml;
import com.tbl.modules.wms.entity.responsexml.inventory.InventoryXml;
import com.tbl.modules.wms.entity.responsexml.location.LocationXml;
import com.tbl.modules.wms.entity.responsexml.material.MaterialXml;
import com.tbl.modules.wms.entity.responsexml.print.OutDansXml;
import com.tbl.modules.wms.entity.responsexml.warehouse.WarehouseXml;
import com.tbl.modules.wms.entity.responsexml.workshop.WorkshopXml;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 * 报文data主体
 * @author 70486
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "materialList",
        "plantList",
        "locationXmlList",
        "warehouseXmlList",
        "discXmlList",
        "externalcableList",
        "workshopList",
        "outDansXml",
        "wayno",
        "car",
        "drivername",
        "transportcompany",
        "failqacodes"


})
@XmlRootElement(name = "data")
@Getter
@Setter
@ToString
public class data {

    /**
     * 物料
     */
    @XmlElement(name = "material", nillable = true)
    public List<MaterialXml> materialList = new ArrayList<>();

    /**
     * 厂区
     */
    @XmlElement(name = "plant", nillable = true)
    public List<PlantXml> plantList = new ArrayList<>();

    /**
     * 库位
     */
    @XmlElement(name = "location", nillable = true)
    public List<LocationXml> locationXmlList = new ArrayList<>();

    /**
     * 仓库
     */
    @XmlElement(name = "warehouse", nillable = true)
    public List<WarehouseXml> warehouseXmlList = new ArrayList<>();

    /**
     * 盘
     */
    @XmlElement(name = "dish", nillable = true)
    public List<DiscXml> discXmlList = new ArrayList<>();

    /**
     * 外采采购电缆数据
     */
    @XmlElement(name = "extrenalcable",nillable = true)
    public List<ExternalcableXml> externalcableList = new ArrayList<>();

    /**
     * 车间
     */
    @XmlElement(name = "workshop", nillable = true)
    public List<WorkshopXml> workshopList = new ArrayList<>();

    @XmlElement(name = "outdans",nillable = true)
    public OutDansXml outDansXml = new OutDansXml();

    /**
     * 发运单
     */
    @XmlElement(name = "wayno",nillable = true)
    public String  wayno ;
    /**
     * 车牌号
     */
    @XmlElement(name = "car",nillable = true)
    public String  car ;
    /**
     * 车司机
     */
    @XmlElement(name = "drivername",nillable = true)
    public String  drivername ;
    /**
     * 运输公司
     */
    @XmlElement(name = "transportcompany",nillable = true)
    public String transportcompany ;

    /**
     * 盘点审核的返回的执行失败的质保号
     */
    @XmlElement(name = "failqacodes", nillable = true)
    public List<InventoryXml> failqacodes = new ArrayList<>();

}
