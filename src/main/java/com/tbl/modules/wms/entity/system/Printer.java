package com.tbl.modules.wms.entity.system;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 打印机设置
 * @author 70486
 */
@TableName("YDDL_PRINTER")
@Getter
@Setter
@ToString
public class Printer implements Serializable {

	/**
	 * 主键
	 */
    @TableId(value = "ID")
    private Long id;

    /**
     * 类型
     */
    private Integer type;
    @TableField(exist = false)
    private String typeStr;

    /**
     * 层级
     */
    private Integer hierarchy;
    @TableField(exist = false)
    private String hierarchyStr;

    /**
     * 代码
     */
    private String code;
    @TableField(exist = false)
    private String codeStr;

    /**
     * 打印机
     */
    private Integer printer;
    @TableField(exist = false)
    private String printerStr;

}
