
package com.tbl.modules.platform.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.common.utils.PageData;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.platform.entity.system.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.*;

/**
 * Controller公共组件
 *
 * @author 70486
 */
public abstract class AbstractController {

    protected HttpServletRequest request;

    protected HttpServletResponse response;

    protected HttpSession session;

    protected Logger logger = LoggerFactory.getLogger(getClass());

    protected Long getUserId() {
        return getSessionUser().getUserId();
    }


    /**
     * 得到PageData
     *
     * @return PageData
     */
    public PageData getPageData() {
        return new PageData(this.getRequest());
    }

    /**
     * 得到request对象
     *
     * @return HttpServletRequest
     */
    public HttpServletRequest getRequest() {
        return ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
    }

    /**
     * 得到session对象
     *
     * @return HttpSession
     */
    public HttpSession getSession() {
        return this.getRequest().getSession();
    }

    @ModelAttribute
    public void setReqAndRes(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
        this.session = request.getSession();
    }

    /**
     * 初始化分页信息
     */
    public PageTbl getPage() {
        HttpServletRequest reuqest = this.getRequest();
        String pageNo = reuqest.getParameter("page");
        String pageSize = reuqest.getParameter("rows");
        String sortOrder = reuqest.getParameter("sord");
        String sortName = reuqest.getParameter("sidx");

        PageTbl page = new PageTbl();
        if (!StringUtils.isEmptyString(pageNo)) {
            page.setPageno(Integer.parseInt(pageNo));
        }

        if (!StringUtils.isEmptyString(pageSize)) {
            page.setPagesize(Integer.parseInt(pageSize));
        }

        if (!StringUtils.isEmptyString(sortName)) {
            page.setSortname(sortName);
        }

        if (!StringUtils.isEmptyString(sortOrder)) {
            page.setSortorder(sortOrder);
        }
        return page;
    }

    /**
     * 转换分页对象内容，将分页信息初始化到JqGridjson map中
     *
     * @param pageMap
     * @param page    分页对象
     * @return
     */
    public Map<String, Object> executePageMap(Map<String, Object> pageMap, PageTbl page) {
        if (page != null) {
            //当前页
            pageMap.put("page", page.getPageno());
            //总页数
            pageMap.put("total", page.getTotalPages());
            //总记录数
            pageMap.put("records", page.getTotalRows());
        }
        return pageMap;
    }

    public ModelAndView getModelAndView() {
        return new ModelAndView();
    }

    /**
     * 得到session对象中的user对象
     *
     * @return User
     */
    public User getSessionUser() {
        return (User) this.getSession().getAttribute("sessionUser");
    }

    /**
     * 获取地址栏后面的参数。
     * @return
     */
    public Map<String, Object> getAllParameter() {
        Map<String, Object> map = new HashMap<>(10);
        HttpServletRequest request = this.getRequest();
        Enumeration enu = request.getParameterNames();
        while (enu.hasMoreElements()) {
            String paraName = (String) enu.nextElement();
            map.put(paraName, request.getParameter(paraName));
        }
        return map;
    }

    public Map<String, Object> returnLayResponse(Page<?> mapPage) {
        Map<String, Object> map = new HashMap<>(4);
        map.put("code", 0);
        map.put("msg", null);
        map.put("count", mapPage.getTotal());
        map.put("data", mapPage.getRecords());
        return map;
    }

    public Map<String, Object> returnLayResponse(List<?> v) {
        Map<String, Object> map = new HashMap<>(4);
        map.put("code", 0);
        map.put("msg", null);
        map.put("count", v.size());
        map.put("data", v);
        return map;
    }

    public Map<String,Object> restfulResponse(Object obj) {
        Map<String, Object> map = new HashMap<>(4);
        map.put("code", 0);
        map.put("msg", "success");
        map.put("result", true);
        map.put("data", obj);
        return map;
    }
    public Map<String,Object> restfulResponseError() {
        Map<String, Object> map = new HashMap<>(4);
        map.put("code", 1);
        map.put("msg", "can not found data");
        map.put("result", false);
        map.put("data", null);
        return map;
    }
}
