package com.tbl.modules.platform.controller.common;

import com.baomidou.mybatisplus.plugins.Page;
import com.google.common.collect.Maps;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.entity.system.DataDictionaryDetailVO;
import com.tbl.modules.platform.service.common.CommonService;
import com.tbl.modules.platform.service.system.DataDictionaryDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 公共控制器
 * @author 70486
 */
@Controller("sfadas")
public class CommonController extends AbstractController {

    /**
     * 公共接口
     */
    @Resource(name="commonService")
    private CommonService commonService;

    @Autowired
    private DataDictionaryDetailService dicDetailService;

    /**
     * 查询类型查询数据字典
     * @param queryString 查询字段
     * @param pageSize    页大小
     * @param pageNo      页数
     * @return Map
     */
    @RequestMapping(value = "/dictionary/getDictionaryDetailListByType")
    @ResponseBody
    public Map<String, Object> getBoxedTypeList(String queryString, int pageSize, int pageNo,String dictionaryType) {
        Map<String, Object> map = Maps.newHashMap();

        Map<String, Object> dicDetailMap = new HashMap<>(4);
        dicDetailMap.put("dictionaryType", dictionaryType);
        dicDetailMap.put("queryString", queryString);
        dicDetailMap.put("page", pageNo);
        dicDetailMap.put("limit", pageSize);
        List<Map<String, Object>> dictionaryDetailList = commonService.getDictionaryList(dicDetailMap);
        Page<DataDictionaryDetailVO> pageDicDetail = dicDetailService.getPageDicDetail(dicDetailMap);

        map.put("result", dictionaryDetailList);
        map.put("total", pageDicDetail.getTotal());
        return map;
    }

    /**
     * 保存用户自定义列
     * @param columns 列
     * @param uuid 唯一标志
     * @return Map
     */
    @RequestMapping(value = "/dynamicColumns/saveColumn")
    @ResponseBody
    public Map<String,Object> dynamicColumns(String columns,String uuid){
        return commonService.dynamicColumns(columns,uuid+getSessionUser().getUsername());
    }

    /**
     * 获取用户自定义列
     * @param uuid 唯一标志
     * @return Map
     */
    @RequestMapping(value = "/dynamicColumns/getColumns")
    @ResponseBody
    public Map<String,Object> dynamicGetColumns(String uuid){
        return commonService.dynamicGetColumns(uuid+getSessionUser().getUsername());
    }
}