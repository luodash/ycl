package com.tbl.modules.platform.controller.system;

import com.alibaba.fastjson.JSON;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.entity.system.Menu;
import com.tbl.modules.platform.service.system.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 菜单controller
 * @author 70486
 */
@Controller
@RequestMapping(value = "/menu")
public class MenuController extends AbstractController {

    /**
     * 菜单service
     */
    @Autowired
    private MenuService menuService;


    /**
     * 跳转到菜单列表界面
     * @return Map
     */
    @RequestMapping(value="/toList.do")
    @ResponseBody
    public ModelAndView toMenuList(){
        ModelAndView mv = new ModelAndView();
        mv.setViewName("techbloom/platform/system/systemconfig/sysconfig_list");
        return mv;
    }

    @RequestMapping(value="/menuList")
    @ResponseBody
    public Map<String,Object> getMenuList(String menuId,String queryJsonString){
        Map<String,Object> map = new HashMap<>();

        if(StringUtils.isEmptyString(menuId)){
            menuId = "0";
        }
        if (!StringUtils.isEmpty(queryJsonString)) {
            map = JSON.parseObject(queryJsonString);
        }

        PageTbl page = this.getPage();
        map.put("page",page.getPageno());
        map.put("limit",page.getPagesize());
        page.setSortname("menu_id");
        String sortOrder = page.getSortorder();
        if(StringUtils.isEmptyString(sortOrder))
        {
            sortOrder = "desc";
            page.setSortorder(sortOrder);
        }
        map.put("sidx", page.getSortname());
        map.put("order", page.getSortorder());
        map.put("menuId", Long.valueOf(menuId));
        PageUtils pageRole = menuService.getMenuPageList(map);
        page.setTotalRows(pageRole.getTotalCount()==0?1:pageRole.getTotalCount());
        map.put("rows",pageRole.getList());
        executePageMap(map,page);

        return map;
    }


    @RequestMapping(value="/menuLists")
    @ResponseBody
    public List<Menu> listView(Long parentId){
        if(parentId == -1) {
            return menuService.listAllMenu();
        } else {
            return menuService.listAllMenu(parentId);
        }
    }



}
