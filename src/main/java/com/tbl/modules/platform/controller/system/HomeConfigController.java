package com.tbl.modules.platform.controller.system;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.entity.system.HomeConfig;
import com.tbl.modules.platform.service.system.HomeConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 首页配置controller
 * @author 70486
 */
@Controller
@RequestMapping(value = "/homeConfig")
public class HomeConfigController extends AbstractController {

    /**
     * 首页配置service
     */
    @Autowired
    private HomeConfigService homeConfigService;


    /**
     * 跳转到用户列表页面
     * @return ModelAndView
     */
    @RequestMapping(value="/toList.do")
    @ResponseBody
    public ModelAndView toList() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("techbloom/platform/system/homeConfig/home_config");
        return mv;
    }

    /**
     * 查询模块表格列表
     * @param queryJsonString
     * @return Object
     */
    @RequestMapping(value="/modelList.do")
    @ResponseBody
    public Object modelList(String roleId,String queryJsonString) {
        Map<String,Object> map = new HashMap<>(6);
        if (!StringUtils.isEmpty(queryJsonString)) {
            map = JSON.parseObject(queryJsonString);
        }

        PageTbl page = this.getPage();
        map.put("page",page.getPageno());
        map.put("limit",page.getPagesize());
        String sortName = page.getSortname();
        if( StringUtils.isEmptyString(sortName) )
        {
            sortName = "id";
            page.setSortname(sortName);
        }
        String sortOrder = page.getSortorder();
        if(StringUtils.isEmptyString(sortOrder))
        {
            sortOrder = "desc";
            page.setSortorder(sortOrder);
        }
        map.put("sidx", page.getSortname());
        map.put("order", page.getSortorder());
        if (!StringUtils.isEmpty(roleId)) {
            map.put("roleId", Long.valueOf(roleId));
        }
        PageUtils pageRole = homeConfigService.getHomeConfigPageList(map);
        page.setTotalRows(pageRole.getTotalCount()==0?1:pageRole.getTotalCount());
        map.put("rows",pageRole.getList());
        executePageMap(map,page);

        return map;
    }

    /**
     * 返回到首页配置编辑页面
     * @return String
     */
    @RequestMapping(value="/config_edit")
    public String configdit() {
        return "techbloom/platform/system/homeConfig/config_edit";
    }

    /**
     * 保存/修改
     * @param hc
     * @param roles
     * @return Map<String, Object>
     */
    @RequestMapping(value ="/save")
    @ResponseBody
    public Map<String, Object> save(HomeConfig hc, String roles){
        hc.setIshide(0);
        hc.setShowtype("1");
        hc.setModelType("html");
        return  homeConfigService.save(hc, roles);

    }

    @RequestMapping(value ="/getModel")
    @ResponseBody
    public Map<String, Object> getModel( String id){
        return  homeConfigService.getModel(id);
    }

    /**
     * @param model
     * @return
     */
    @RequestMapping(value ="/deleteModel")
    @ResponseBody
    public Map<String, Object> deleteModel(String model){
        return  homeConfigService.deleteModel(model);
    }

}
