package com.tbl.modules.platform.controller.notice;

import com.alibaba.fastjson.JSON;
import com.tbl.common.utils.DateUtils;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.platform.constant.LogActionConstant;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.entity.notice.Notice;
import com.tbl.modules.platform.service.notice.NoticeService;
import com.tbl.modules.platform.service.system.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * 公告controller
 * @author 70486
 */
@Controller
@RequestMapping(value = "/notice")
public class NoticeController extends AbstractController {

    /**
     * 公告serivce
     */
    @Autowired
    private NoticeService noticeService;
    /**
     * 日志service
     */
    @Autowired
    private LogService logService;



    /**
     * 跳转到公告列表页面
     * @return ModelAndView
     */
    @RequestMapping(value = "/toView.do")
    @ResponseBody
    public ModelAndView toList() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("techbloom/platform/notice/notice_list");
        return mv;
    }

    /**
     * 获取公告列表数据
     */
    @RequestMapping(value = "/getlist.do")
    @ResponseBody
    public Object listNotices(String queryJsonString) {
        Map<String,Object> map = new HashMap<>(6);
        if (!StringUtils.isEmpty(queryJsonString)) {
            map = JSON.parseObject(queryJsonString);
        }

        PageTbl page = this.getPage();
        map.put("page",page.getPageno());
        map.put("limit",page.getPagesize());
        String sortName = page.getSortname();
        if( StringUtils.isEmptyString(sortName) )
        {
            sortName = "createtime";
            page.setSortname(sortName);
        }
        String sortOrder = page.getSortorder();
        if(StringUtils.isEmptyString(sortOrder))
        {
            sortOrder = "desc";
            page.setSortorder(sortOrder);
        }
        map.put("sidx", page.getSortname());
        map.put("order", page.getSortorder());
        PageUtils pageUser = noticeService.queryPage(map);
        page.setTotalRows(pageUser.getTotalCount()==0?1:pageUser.getTotalCount());
        map.put("rows",pageUser.getList());
        executePageMap(map,page);
        return map;
    }


    /**
     * 返回到角色添加页面
     */
    @RequestMapping(value="/notice_edit")
    public String noticeEdit() {
        return "techbloom/platform/notice/notice_edit";
    }

    /**
     * 保存/修改公告信息
     */
    @RequestMapping(value = "/addNotice")
    @ResponseBody
    public boolean addNotice(Notice notice, Integer id) {
        boolean result = false;
        Long noticeId = notice.getId();
        if (noticeId == null) {
            //新增
            if(StringUtils.isEmptyString(notice.getTitle()) || StringUtils.isEmptyString(notice.getNotice()) ) {
                return false;
            }
            notice.setCreatetime(DateUtils.dateToSs());
            //保存公告信息
            noticeId = noticeService.saveForGernatedKey(notice);
            if(noticeId>0){
                logService.logInsert("添加公告["+notice.getTitle()+"]成功！", LogActionConstant.USER_ADD, this.getRequest());
                result = true;
            }
        } else {
            //更新(该方法新增/修改都可以)
            boolean res = noticeService.saveNotice(notice);
            if(res){
                logService.logInsert("修改公告["+notice.getTitle()+"]成功！", LogActionConstant.USER_EDIT, this.getRequest());
                result = true;
            }
        }
        return result;
    }

    /**
     * 删除公告
     */
    @RequestMapping(value="/delNotice")
    @ResponseBody
    public boolean delNotice(String[] ids) {
        boolean result = false;
        if( !StringUtils.isEmptyString(Arrays.toString(ids)))
        {
            String delNotice = noticeService.getNoticeNameByNoticeids(StringUtils.join(ids,",", true));
            result = noticeService.deleteAllU(StringUtils.join(ids,",", true));
            if(result)
            {
                logService.logInsert("删除公告["+delNotice+"]成功！", LogActionConstant.USER_DELETE, request);
            }
        }
        return result;
    }

    /**
     * 根据路径ID获取公告
     * @param id 公告ID
     * @return
     */
    @RequestMapping(value = {"/getNoticeById"}, method = {RequestMethod.POST})
    @ResponseBody
    public Notice getNoticeById(Integer id) {
        return noticeService.selectById(Long.valueOf(id));
    }
}
