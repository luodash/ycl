package com.tbl.modules.platform.controller.system;

import com.tbl.modules.platform.entity.system.iguration;
import com.tbl.modules.platform.service.system.igurationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * pda控制层
 * @author 70486
 */
@RestController
@RequestMapping(value = "/pdaPowerCon")
public class PdaPowerController {

    @Autowired
    private igurationService igurationservice;

    @RequestMapping(value="/toList")
    @ResponseBody
    public ModelAndView systemLog() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("techbloom/system/PDA/pda_list");
        return mv;
    }
    /**
     * 权限
     */
    @RequestMapping(value="/padPower")
    @ResponseBody
    public Map<String,Object> getPadPower(int flag) {
        Long id = 1L;
        Map<String, Object> map = new HashMap<>(3);
        if (flag == 1) {
            iguration pda = igurationservice.getObject(id);
            map.put("value", pda.getValue());

        } else if (flag == 2) {
            iguration pda = igurationservice.getObject(id);
            String status = pda.getValue();
            if (status.equals("1")) {
                pda.setValue("0");
            } else {
                pda.setValue("1");
            }
            igurationservice.updatebyids(pda);
            map.put("value", pda.getValue());
        }
        return map;
    }
}
