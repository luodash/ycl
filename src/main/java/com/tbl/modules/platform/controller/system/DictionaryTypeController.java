package com.tbl.modules.platform.controller.system;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tbl.common.utils.PageData;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.platform.constant.LogActionConstant;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.entity.system.DataDictionaryTypeVO;
import com.tbl.modules.platform.service.system.DataDictionaryTypeService;
import com.tbl.modules.platform.service.system.LogService;
import com.tbl.modules.platform.util.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 字典类型controller
 * @author 70486
 */
@Controller
@RequestMapping(value = "/dictType")
public class DictionaryTypeController extends AbstractController {

    /**
     * 字典类型接口
     */
    @Autowired
    private DataDictionaryTypeService dictTypeService;
    /**
     * 日志service
     */
    @Autowired
    private LogService logService;


    /**
     * 数据字典列表显示界面
     * @Title: toDictTypeList
     * @return ModelAndView
     */
    @RequestMapping(value = "/toList.do")
    public ModelAndView toDictTypeList() {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("techbloom/platform/system/datadictionary/dataDictionaryType_list");
        return mv;
    }

    /**
     * 数据字典列表查询
     * @Title: listDictType
     * @return Object
     */
    @RequestMapping(value = "/list.do")
    @ResponseBody
    public Map<String, Object> listDictType(String queryJsonString) {
        Map<String,Object> map = new HashMap<>();
        if (!StringUtils.isEmpty(queryJsonString)) {
            map = JSON.parseObject(queryJsonString);
        }

        PageTbl page = this.getPage();
        map.put("page",page.getPageno());
        map.put("limit",page.getPagesize());
        String sortName = page.getSortname();
        if( StringUtils.isEmptyString(sortName) )
        {
            sortName = "";
            page.setSortname(sortName);
        }
        String sortOrder = page.getSortorder();
        if(StringUtils.isEmptyString(sortOrder))
        {
            sortOrder = "desc";
            page.setSortorder(sortOrder);
        }
        map.put("sidx", page.getSortname());
        map.put("order", page.getSortorder());
        PageUtils PageType = dictTypeService.queryPage(map);
        page.setTotalRows(PageType.getTotalCount()==0?1:PageType.getTotalCount());
        map.put("rows",PageType.getList());
        executePageMap(map,page);
        return map;
    }

    /**
     * 显示新增界面/修改界面
     * @Title: addDictType
     * @param  id 数据字典ID
     * @return ModelAndView
     * @throws
     */
    @RequestMapping(value = "/toAdd.do")
    @ResponseBody
    public ModelAndView addDictType(Long id) {
        ModelAndView mv = new ModelAndView();
        if (id != null && id != 0) {
            // 根据id数据字典信息并返回
            mv.addObject("dictType", dictTypeService.findDictTypeById(id));
            mv.addObject("edit", 1);
        }
        mv.setViewName("techbloom/platform/system/datadictionary/dataDictionaryType_edit");
        return mv;
    }

    /**
     * 保存数据字典
     * @Title: saveDictType
     * @param dictType 数据字典
     * @return void
     */
    @RequestMapping(value = "/toSaveDictType.do")
    public void saveDictType(DataDictionaryTypeVO dictType) {
        Map<String,Object> map=null;
        if (dictType != null) {
            // 添加之前,判断数据字典是否已存在,暂未处理
            if (dictType.getId() == null) {
                // 没有主键信息，执行保存操作
                boolean isSuccess = dictTypeService.saveDictType(dictType);
                if(isSuccess){
                    map=new HashMap<>();
                    map.put("result",true);
                    map.put("msg","保存成功!");
                    logService.logInsert("添加了数据字典["+dictType.getDictTypeNum()+"-"+dictType.getDictTypeName()+"]", LogActionConstant.USER_ADD, request);
                }
            } else {
                boolean isSuccess = dictTypeService.updateDictType(dictType);
                if(isSuccess){
                    logService.logInsert("修改了数据字典["+dictType.getDictTypeNum()+"-"+dictType.getDictTypeName()+"]", LogActionConstant.USER_EDIT, request);
                    map=new HashMap<>();
                    map.put("result",true);
                    map.put("msg","修改成功!");
                }
            }
        }
        ResponseUtil.printJS(response, "parent.result(" + JSONObject.toJSON(map) + ")");
    }


    /**
     * 判断数据字典编号是否存在
     * @return boolean
     */
    @RequestMapping(value = "/isExitsDictType")
    @ResponseBody
    public boolean isExitsDictTypeNo() {
        boolean errInfo = false;
        PageData pd = this.getPageData();
        if (dictTypeService.isExitsDictType(pd) == null) {
            errInfo = true;
        }
        return errInfo;
    }

    /**
     * 删除数据字典
     * @param  ids  数据字典ID集合
     * @return boolean
     */
    @RequestMapping(value = "/deleteDictType.do")
    @ResponseBody
    public Map<String,Object> delDictTypeByID(String[] ids) {
        Map<String,Object> map=new HashMap<>(2);
        boolean isdel = dictTypeService.delDictTypeByIds(ids);
        if(isdel){
            logService.logInsert("删除了"+ids.length+"个数据字典", LogActionConstant.USER_DELETE, request);
            map.put("result",true);
            map.put("msg","删除数据字典成功!");
        }
        else{
            map.put("result",false);
            map.put("msg","删除数据字典失败!");
        }
        return map;
    }

    /**
     * 打开查询窗口
     * @param jsonString
     * @return ModelAndView
     */
    @RequestMapping(value="/toQueryPage")
    @ResponseBody
    public ModelAndView toQueryPage(String jsonString){
        ModelAndView mv = new ModelAndView();
        //数据字典
        DataDictionaryTypeVO DictType = JSONObject.parseObject(jsonString, DataDictionaryTypeVO.class);
        mv.addObject("DictType", DictType);
        mv.setViewName("platform/system/datadictionary/query_dataDictionaryType_list");
        return mv;
    }

    /**
     * 导出excel
     */
    @RequestMapping(value = "/toExcel")
    @ResponseBody
    public void artBomExcel(){
        dictTypeService.toExcel(response,"",dictTypeService.getAllLists(request.getParameter("ids")));
    }

}
