package com.tbl.modules.platform.controller.system;

import com.google.common.collect.Maps;
import com.tbl.modules.les.constant.Const;
import com.tbl.modules.platform.controller.AbstractController;
import com.tbl.modules.platform.entity.system.HomeConfig;
import com.tbl.modules.platform.entity.system.User;
import com.tbl.modules.platform.service.system.IndexService;
import com.tbl.modules.platform.service.system.UserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 主页处理controller
 * @author 70486
 */
@Controller
@RequestMapping(value = "/index")
public class IndexController extends AbstractController {

    /**
     * 用户接口
     */
    @Autowired
    private UserService userService;
    /**
     * 主页接口
     */
    @Autowired
    private IndexService indexService;

    @Value("${read.adapteRfid}")
    private String StrAdapteRfid;


    /**
     * 主页面跳转Controller
     * @return ModelAndView
     */
    @RequestMapping(value = "/main")
    public ModelAndView main() {
        ModelAndView mv = this.getModelAndView();
        mv.addObject("adapteRfid", StrAdapteRfid);
        mv.setViewName("/main/home");
        mv.addObject("authentic", userService.getDeviceOrDataAutor(getSessionUser().getRoleId()));
        return mv;
    }

    /**
     * 跳转到首页的图表页面
     * @param id
     * @return ModelAndView
     */
    @RequestMapping(value = "/main_page")
    public ModelAndView main_page(Long id) {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("techbloom/platform/main/main_page");
        return mv;
    }

    @RequestMapping(value = "/saveConfig")
    @ResponseBody
    public Boolean saveConfig(String hidemodel, String userid, String layout,  String sortable) {
        return indexService.saveConfig( hidemodel, userid,  layout,   sortable );
    }

    @RequestMapping(value = "/getConfig")
    @ResponseBody
    public Map<String, Object> getConfig() {
        // shiro管理的session,当前登录用户
        Subject currentUser = SecurityUtils.getSubject();
        Session session = currentUser.getSession();
        User user = (User) session.getAttribute(Const.SESSION_USER);
        //获取当前用户所属角色
        Long role_id = user.getRoleId();
        if(role_id == null) return null;
        List<HomeConfig> hc = indexService.getConfig( role_id );
        Map<String, Object> map = Maps.newHashMap();
        Map<String, Object> sortableMap = Maps.newHashMap();
        Map<String, Object> configmap = indexService.getUserConfig(user.getUserId());
        if(configmap == null){
            ArrayList<Object> leftArray = new ArrayList<>();
            ArrayList<Object>centerArray = new ArrayList<>();
            ArrayList<Object>rightArray = new ArrayList<>();
            for(int i = 0; i < hc.size(); i++){
                HomeConfig  model= hc.get(i);
                String  l = model.getLayout();
                if(l.equals("left"))
                    leftArray.add(model.getId());
                if(l.equals("center"))
                    centerArray.add(model.getId());
                if(l.equals("right"))
                    rightArray.add(model.getId());
            }
            sortableMap.put("left", leftArray);
            sortableMap.put("center", centerArray);
            sortableMap.put("right", rightArray);
            map.put("layout", "1:1:1");
            map.put("sortable", sortableMap);
            map.put("data", hc);
        }else{
            map = configmap;
            map.put("data", hc);
        }
        return map;
    }

    /**
     * 主页跳转到 boxs.jsp
     * @return ModelAndView
     */
    @RequestMapping(value = "/toboxs")
    public ModelAndView toboxs() {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("/techbloom/platform/main/boxs");
        return mv;
    }

    /**
     * 主页跳转到 clock.jsp
     * @return ModelAndView
     */
    @RequestMapping(value = "/toclock")
    public ModelAndView toclock() {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("/techbloom/platform/main/clock");
        return mv;
    }

    /**
     * 主页跳转到 locator_warning_main.jsp
     * @return ModelAndView
     */
    @RequestMapping(value = "/tolocatorwarningmain")
    public ModelAndView tolocatorwarningmain() {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("/techbloom/wms/alarmwarning/locatorwarning/locator_warning_main");
        return mv;
    }

    /**
     * 主页跳转到 stock_index.jsp
     * @return ModelAndView
     */
    @RequestMapping(value = "/tostockindex")
    public ModelAndView tostockindex() {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("/techbloom/wms/stock/stock_index");
        return mv;
    }

    /**
     * 主页跳转到 outstorage_index.jsp
     * @return ModelAndView
     */
    @RequestMapping(value = "/tooutstorageindex")
    public ModelAndView tooutstorageindex() {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("/techbloom/wms/outstorage/outstoragemanage/outstorage_index");
        return mv;
    }

    /**
     * 主页跳转到 instoragePlan_index.jsp
     * @return ModelAndView
     */
    @RequestMapping(value = "/toinstorageplanindex")
    public ModelAndView toinstorageplanindex() {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("/techbloom/wms/instorage/instoragePlan/instoragePlan_index");
        return mv;
    }
    
    /**
     * 主页跳转到个人资料页面
     * @return ModelAndView
     */
    @RequestMapping(value = "/userInfo")
    public ModelAndView userInfo() {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("/techbloom/platform/main/userInfo");
        return mv;
    }
    
    /**
     * 主页跳转到 pwd.jsp
     * @return ModelAndView
     */
    @RequestMapping(value = "/pwd")
    public ModelAndView pwd() {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("/techbloom/platform/main/pwd");
        return mv;
    }

}
