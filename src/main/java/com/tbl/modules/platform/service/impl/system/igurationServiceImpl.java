package com.tbl.modules.platform.service.impl.system;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.modules.platform.dao.system.igurationDAO;
import com.tbl.modules.platform.entity.system.iguration;
import com.tbl.modules.platform.service.system.igurationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * pda权限控制
 * @author 70486
 */
@Service("igurationService")
public class igurationServiceImpl extends ServiceImpl<igurationDAO, iguration> implements igurationService {

    @Autowired
    private igurationDAO pdadao;

    @Override
    public iguration getObject(Long id) {
        return pdadao.isStart(id);
    }

    @Override
    public void updatebyids(iguration pda) {
        pdadao.updateById(pda);
    }
}
