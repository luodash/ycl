package com.tbl.modules.platform.service.impl.system;

import com.alibaba.druid.support.json.JSONUtils;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.google.common.base.Strings;
import com.tbl.common.utils.DateUtils;
import com.tbl.modules.platform.dao.util.ErrorLogDAO;
import com.tbl.modules.platform.entity.system.ErrorLog;
import com.tbl.modules.platform.service.system.ErrorLogService;
import com.tbl.modules.platform.util.EhcacheUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * 错误日志服务信息实现类
 * @author 70486
 */
@Service("errorLogService")
public class ErrorLogServiceImpl extends ServiceImpl<ErrorLogDAO, ErrorLog> implements ErrorLogService {

    /**
     * 错误日志持久层
     */
    @Autowired
    private ErrorLogDAO errorLogDAO;

    /**
     * 查询错误日志信息
     * @param info
     * @param remark
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveErrorLog(String info, String remark) {
        ErrorLog elog = new ErrorLog();
        elog.setInfo(info);
        elog.setCreateTime(DateUtils.dateToSs());
        elog.setRemark(remark);
        this.insert(elog);
    }

    @Override
    public void startModData(String tableName, Long id) {
        Map<String, Object> data = errorLogDAO.queryForMap(tableName, id);
        if (data != null) {
            EhcacheUtils.putValue(tableName + "_" + id, JSONUtils.toJSONString(data));
        }
    }

    @Override
    public void completeModData(String tableName, Long tableId, String operation, Long logId) {
        Long param1 = null;
        String param2 = "";
        String param3 = "";
        Object param4 = "";
        Object param5 = "";

        Map<String, Object> data = errorLogDAO.queryForMap(tableName, tableId);
        Map<String, Object> before = new HashMap<>(2);
        if (data != null) {
            String beforedata = EhcacheUtils.getValue(tableName + "_" + tableId).toString();
            if (!Strings.isNullOrEmpty(beforedata)) {
                before = JSON.parseObject(beforedata);
            }
        }
        boolean flag = false;
        for (Map.Entry<String, Object> entry : data.entrySet()) {
            if (before.get(entry.getKey()) == null || data.get(entry.getKey()) == null) {
                continue;
            }
            if (before.get(entry.getKey()).equals(data.get(entry.getKey()))) {
                System.out.println("未变化" + entry.getKey());
            } else {
                flag = true;
                param1 = logId;
                param2 = entry.getKey();
                param4 = before.get(entry.getKey());
                param5 = entry.getValue();

            }
        }
        if (flag) {
            errorLogDAO.insert1(param1, param2, param3, param4, param5);
        } else {
            errorLogDAO.delete1(logId);
        }
    }
}
