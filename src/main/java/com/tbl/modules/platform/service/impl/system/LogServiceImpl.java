package com.tbl.modules.platform.service.impl.system;


import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.common.utils.*;
import com.tbl.modules.platform.dao.system.LogDAO;
import com.tbl.modules.platform.entity.system.Log;
import com.tbl.modules.platform.entity.system.User;
import com.tbl.modules.platform.service.system.LogService;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.Objects;

/**
 * 系统操作日志
 * @author 70486
 */
@Service("logService")
public class LogServiceImpl extends ServiceImpl<LogDAO, Log> implements LogService {

    /**
     * 系统日志记录
     */
    @Autowired
    private LogDAO logDAO;

    /**
     * 保存系统的操作日志
     * @param operation
     * @param action
     * @param request
     * @return boolean
     */
    @Override
    public boolean logInsert(String operation, long action, HttpServletRequest request){
        User user =new User();
        if(request==null){
            request=((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
        }else{
            user = (User) request.getSession().getAttribute(Const.SESSION_USER);
        }

        boolean isSucceed = false;
        if(user != null){
            Log log = new Log();
            log.setUserId(user.getUserId());
            log.setUseraction(action);
            log.setOperation(operation);
            log.setUserip(request.getRemoteAddr());
            log.setUsermac(LogUtil.getRealMacInfo( request.getRemoteAddr()));
            log.setOperationTime(System.currentTimeMillis());
            isSucceed = this.insert(log);

        }
        return isSucceed;
    }

    /**
     * 保存系统的操作日志
     * @param operation
     * @param action
     * @param request
     * @param json
     */
    @Override
    public void logInsertGrenId(String operation, long action, HttpServletRequest request, Object json){
        if(request==null){
            request=((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
        }
        if(json==null){
            json=new JSONObject();
        }
        User user = (User) request.getSession().getAttribute(Const.SESSION_USER);
        //boolean isSucceed = false;
        Long id=0L;
        if(user != null){
            Log log = new Log();
            log.setUserId(user.getUserId());
            log.setUseraction(action);
            log.setOperation(operation);
            log.setUserip(request.getRemoteAddr());
            log.setUsermac(LogUtil.getRealMacInfo( request.getRemoteAddr()));
            log.setOperationTime(DateUtils.dateToSs());
            id=saveLogGrentId(log,json.toString());
        }
    }

    /**
     * 保存日志
     * @param log
     * @return Long
     */
    public Long saveLogGrentId(Log log,String objectJsonStr) {
        log.setJsonstr(objectJsonStr);
        logDAO.insertLog(log);
        return log.getId() ;
    }

    /**
     * 获取行车数据列表
     * @param pageTbl
     * @param map
     * @return PageUtils
     */
    @Override
    public PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map) {
        String sortOrder = pageTbl.getSortorder();
        String sortName = pageTbl.getSortname();
        if (StringUtils.isEmptyString(sortName)) {
            sortName = "id";
        }
        if (StringUtils.isEmptyString(sortOrder)) {
            sortOrder = "desc";
        }
        Page page = new Page(pageTbl.getPageno(), pageTbl.getPagesize(), sortName, "asc".equalsIgnoreCase(sortOrder));
        return new PageUtils(page.setRecords(logDAO.getPageList(page, map)));
    }

}
