package com.tbl.modules.platform.service.impl.system;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.google.common.collect.Maps;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.Query;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.platform.dao.system.HomeConfigDAO;
import com.tbl.modules.platform.dao.system.RoleModelDAO;
import com.tbl.modules.platform.entity.system.HomeConfig;
import com.tbl.modules.platform.service.system.HomeConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * 首页配置接口实现
 * @author 70486
 */
@Service("homeConfigService")
public class HomeConfigServiceImpl extends ServiceImpl<HomeConfigDAO, HomeConfig> implements HomeConfigService {

    /**
     * 首页配置DAO
     */
    @Autowired
    private HomeConfigDAO homeConfigDAO;
    /**
     * 角色模块DAO
     */
    @Autowired
    private RoleModelDAO roleModelDAO;



    /**
     * 获取首页配置分页列表
     * @param params
     * @return PageUtils
     */
    @Override
    public PageUtils getHomeConfigPageList(Map<String, Object> params) {
        Page<HomeConfig> page = this.selectPage(new Query<HomeConfig>(params).getPage(), new EntityWrapper<>());
        return new PageUtils(page.setRecords(homeConfigDAO.selectHomeConfigList(page, params)));
    }

    /**
     * 保存/修改
     * @param hc
     * @param roles
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> save(HomeConfig hc, String roles) {
        Map<String, Object> map = Maps.newHashMap();
        try {
            Long key;

            hc.setModelName(hc.getModel_name());
            hc.setModelUrl(hc.getModel_url());
            hc.setModelKey(hc.getModel_key());
            hc.setModelUpdatetime(hc.getModel_updatetime());
            hc.setModelTitle(hc.getModel_title());
            hc.setModelHeight(hc.getModel_height());


            if (hc.getId() == null) {
                baseMapper.insert(hc);
                key = hc.getId();
            } else {
                key = hc.getId();
                roleModelDAO.deleteRolemodel(hc.getId());

                baseMapper.updateById(hc);
            }

            Map<String, Object> rolemodelMap;
            String[] list = StringUtils.split(roles, ",");
            for (String value : list) {
                rolemodelMap = new HashMap<>(2);
                rolemodelMap.put("modelId", key);
                rolemodelMap.put("roleId", Long.valueOf(value));
                roleModelDAO.saveRolemodel(rolemodelMap);
            }
            map.put("result", true);
        } catch (Exception e) {
            map.put("result", false);
            e.printStackTrace();
        }
        return map;
    }

    /**
     * @param id
     * @return
     */
    @Override
    public Map<String, Object> getModel(String id) {
        Map<String, Object> map = Maps.newHashMap();
        map.put("model", homeConfigDAO.selectModel(Long.valueOf(id)));
        map.put("roles", roleModelDAO.getRolemodelByModelId(Long.valueOf(id)));
        return map;
    }

    /**
     * @param models
     * @return Map<String, Object>
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public Map<String, Object> deleteModel(String models) {
        Map<String, Object> map = Maps.newHashMap();
        try {
            String[] modelArr = models.split(",");

            boolean hcFlag = true;
            for (String s : modelArr) {
                if (!deleteById(Long.valueOf(s))) {
                    hcFlag = false;
                }
            }

            if (hcFlag) {
                for (String model: modelArr) {
                    roleModelDAO.deleteRolemodel(Long.valueOf(model));
                }
                map.put("result", true);
            } else {
                map.put("result", false);
            }
        } catch (Exception e) {
            map.put("result", false);
            e.printStackTrace();
        }
        return map;
    }

}
