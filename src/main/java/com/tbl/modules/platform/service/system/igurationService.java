package com.tbl.modules.platform.service.system;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.modules.platform.entity.system.iguration;
import org.springframework.stereotype.Service;

/**
 * pda权限控制
 * @author zp
 */
@Service("igurationService")
public interface igurationService extends IService<iguration> {

    /**
     * 根据ID查询
     * @param id
     * @return iguration
     */
    iguration getObject(Long id);

    /**
     * 根据ID更新
     * @param pda
     */
    void updatebyids(iguration pda);
}
