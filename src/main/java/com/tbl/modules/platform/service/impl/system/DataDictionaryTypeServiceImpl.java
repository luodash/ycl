package com.tbl.modules.platform.service.impl.system;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.common.utils.DateUtils;
import com.tbl.common.utils.PageData;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.Query;
import com.tbl.modules.platform.dao.system.DataDictionaryTypeDAO;
import com.tbl.modules.platform.entity.system.DataDictionaryTypeVO;
import com.tbl.modules.platform.service.system.DataDictionaryTypeService;
import com.tbl.modules.platform.util.DeriveExcel;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 字典类型接口实现
 * @author 70486
 */
@Service("dataDictionaryTypeService")
public class DataDictionaryTypeServiceImpl extends ServiceImpl<DataDictionaryTypeDAO, DataDictionaryTypeVO> implements DataDictionaryTypeService {

    /**
     * 数据字典持久层
     */
    @Autowired
    private DataDictionaryTypeDAO dicTypeDAO;

    /**
     * 获取分页列表
     * @param params
     * @return PageUtils
     */
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String dictTypeNum = (String)params.get("dictTypeNum");
        String dictTypeName = (String)params.get("dictTypeName");

        Page<DataDictionaryTypeVO> page = this.selectPage(new Query<DataDictionaryTypeVO>(params).getPage(), new EntityWrapper<DataDictionaryTypeVO>()
                .like(StringUtils.isNotBlank(dictTypeNum),"dictTypeNum", dictTypeNum)
                .like(StringUtils.isNotBlank(dictTypeName),"dictTypeName", dictTypeName)
        );

        return new PageUtils(page);
    }

    /**
     * 根据Id查询数据字典
     * @param id 数据字典ID
     * @return DictTypeinfo
     */
    @Override
    public DataDictionaryTypeVO findDictTypeById(Long id) {
        return baseMapper.selectById(id);
    }

    /**
     * 数据字典保存操作
     * @param dictionaryTypeVO 数据字典
     * @return boolean
     * @Title: saveDictType
     */
    @Override
    public boolean saveDictType(DataDictionaryTypeVO dictionaryTypeVO) {
        return this.insert(dictionaryTypeVO);
    }

    /**
     * 数据字典修改保存
     * @param dictionaryTypeVO 数据字典
     * @return boolean
     * @Title: updateDictType
     */
    @Override
    public boolean updateDictType(DataDictionaryTypeVO dictionaryTypeVO) {
        return this.updateById(dictionaryTypeVO);
    }

    /**
     * 验证编号/名称是否存在
     * @param pd
     * @return DictTypeinfo
     */
    @Override
    public DataDictionaryTypeVO isExitsDictType(PageData pd) {
        List<DataDictionaryTypeVO> lstdicType;
        DataDictionaryTypeVO dicType = null;
        Map<String, Object> map = new HashMap<>(2);

        // 新增时验证编号是否存在
        if ("dictTypeNum".equals(pd.getString("checkType")) && "".equals(pd.get("id"))) {
            map.put("dict_type_num", pd.getString("dictTypeNum"));

        // 新增时验证名称是否存在
        } else if ("dictTypeName".equals(pd.getString("checkType")) && "".equals(pd.get("id"))) {
            map.put("dict_type_name", pd.getString("dictTypeName"));

        // 修改时验证编号是否存在(除本身)
        } else if ("dictTypeNum".equals(pd.getString("checkType")) && !"".equals(pd.get("id"))) {
            map.put("dict_type_num", pd.getString("dictTypeNum"));
            map.put("id", pd.getString("id"));
        // 修改时验证名称是否存在(除本身)
        } else if ("dictTypeName".equals(pd.getString("checkType")) && !"".equals(pd.get("id"))) {
            map.put("dict_type_name", pd.getString("dictTypeName"));
            map.put("id", pd.getString("id"));
        }

        lstdicType = selectByMap(map);
        if (lstdicType != null && lstdicType.size() > 0) {
            dicType = lstdicType.get(0);
        }
        return dicType;
    }

    /**
     * 数据字典删除操作
     * @param ids 数据字典ID集合
     * @return boolean
     * @Title: delDictTypeByIds
     */
    @Override
    public boolean delDictTypeByIds(String[] ids) {
        return this.deleteBatchIds(Arrays.stream(ids).map(s -> Long.parseLong(s.trim())).collect(Collectors.toList()));
    }

    /**
     * 获取导出列
     * @param ids
     * @return List<DataDictionaryTypeVO>
     */
    @Override
    public List<DataDictionaryTypeVO> getAllLists(String ids) {
        if(com.tbl.common.utils.StringUtils.isEmptyString(ids)){
            return dicTypeDAO.selectList(new EntityWrapper<>());
        }
        return dicTypeDAO.selectBatchIds(com.tbl.common.utils.StringUtils.stringToList(ids));
    }

    /**
     * 导出excel
     * @param response
     * @param path
     * @param list
     */
    @Override
    public void toExcel(HttpServletResponse response, String path, List<DataDictionaryTypeVO> list) {
        try {
            String sheetName = "数据字典表" + "(" + DateUtils.getDay() + ")";
            if (path != null && !"".equals(path)) {
                sheetName = sheetName + ".xls";
            } else {
                response.setHeader("Content-Type", "application/force-download");
                response.setHeader("Content-Type", "application/vnd.ms-excel");
                response.setCharacterEncoding("UTF-8");
                response.setHeader("Expires", "0");
                response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
                response.setHeader("Pragma", "public");
                response.setHeader("Content-disposition", "attachment;filename=" + new String(sheetName.getBytes("gbk"),
                        "ISO8859-1") + ".xls");
            }
            Map<String, String> mapFields = new LinkedHashMap<>();
            mapFields.put("dictTypeNum", "字典类型编号");
            mapFields.put("dictTypeName", "字典类型名称");
            mapFields.put("description", "描述");
            DeriveExcel.exportExcel(sheetName, list, mapFields, response, path);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
