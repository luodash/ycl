package com.tbl.modules.platform.service.system;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.tbl.common.utils.PageData;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.platform.entity.system.User;
import com.tbl.modules.platform.entity.system.UserModel;
import org.apache.ibatis.annotations.Param;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户service
 * @author 70486
 */
public interface UserService extends IService<User> {

    /**
     * 获取分页列表
     * @param  params
     * @return PageUtils
     */
    PageUtils queryPage(Map<String, Object> params);

    /**
     * 根据登录名获取实体
     * @param username
     * @return User
     */
    User findByUsername(String username);

    /**
     * 判断登录用户属于哪个用户
     * @param username
     * @return Integer
     */
    Integer getByUserName(String username);

    /**
     * 登录判断
     * @param username
     * @param pwd
     * @return User
     */
    User getUserByNameAndPwd(String username,String pwd);

    /**
     * 更新登录时间
     * @param pd
     * */
    void updateLastLogin(PageData pd);

    /**
     * 通过id获取数据
     * @param USER_ID
     * @return User
     */
    User getUserAndRoleById(long USER_ID);

    /**
     * 根据用户姓名查询用户
     * @param username
     * @return User
     */
    User getUserAndRoleByIds(String username);

    /**
     * 验证当前用户是否具有RFID数据权限
     * @param id
     * @return boolean
     */
    boolean getDataManage(Long id);

    /**
     * 检查当前用户是否有设备管理权限
     * @param id
     * @return boolean
     */
    boolean getDeviceT(Long id);

    /**
     * 获取用户菜单
     * @param id
     * @param type
     * @return List<Map<String, Object>>
     */
    List<Map<String, Object>> getUserMenu(Long id, String type);

    /**
     * 获取用户菜单
     * @param id
     * @param type
     * @return Map<String, Object>
     */
    List<Map<String, Object>> getUserMenu11(Long id, String type);

    /**
     * 设置用户菜单
     * @param menus
     * @param userid
     * @return boolean
     */
    boolean setUserMenu(String menus, Long userid);

    /**
     * 设置用户菜单
     * @param menus
     * @param userid
     * @return boolean
     */
    boolean setUserMenus(String menus, Long userid);

    /**
     * @param menus
     * @param userid
     * @return boolean
     */
    boolean setUserQuickMenu(String menus, Long userid);

    /**
     * 保存/更新用户
     * @param user
     * @param userFactoryIds
     * @return boolean
     * */
    boolean saveU(User user,String userFactoryIds);

    /**
     * 设置快捷菜单
     * @param menus
     * @param userid
     * @return boolean
     */
    boolean setUserQuickMenus(String menus, Long userid);

    /**
     * 根据主键查询用户
     * @param userId
     * @return User
     * */
    User findByUserId(String userId);

    /**
     * 根据用户主键修改用户密码
     * @param userId
     * @param columnName
     * @param columnValue
     * @return boolean
     */
    boolean updateUserColumn(long userId ,String columnName,String columnValue);

    /**
     * 通过loginname获取数据
     * @param pd
     * @return User
     * */
    User findByUId(PageData pd);

    /**
     * 根据用户主键获取用户名称
     * @param ids 多个用户主键
     * @return String
     */
    String getUserNameByIds(String ids);

    /**
     * 更改制定用户状态
     * @param state 用户状态1：启用，2：停用
     * @param ids 用户ID，多个以逗号隔开
     * @return boolean
     */
    boolean  changeState(String state,String ids);

    /**
     * 获取导出列
     * @param ids
     * @return List<UserModel>
     * */
    List<UserModel> getAllLists(String ids);

    /**
     * 导出excel
     * @param response
     * @param path
     * @param list
     */
    void toExcel(HttpServletResponse response, String path, List<UserModel> list);

    /**
     * 根据用户信息获取用户能访问的菜单权限：这里指定一个设备检测的菜单
     * @param user
     * @return int
     */
    int getJcMenuByUser(User user);

    /**
     * 获取所有用户列表:供下拉框使用
     * @param  map
     * @return PageUtils
     */
    PageUtils getUserPageList(Map<String, Object> map);

    /**
     * 获取用户下拉列表
     * @param  map
     * @return Map<String, Object>
     */
    List<Map<String, Object>> getLstMapUser(Map<String, Object> map);

    /**
     * 获取用户列表（ASN管理模块添加页面“ 收货人 ”下拉框使用）
     * @param params
     * @return Page<User>
     */
    Page<User> getSelectUserList(Map<String, Object> params);

    /**
     * 获取系统中的人(质检收货添加页面使用)
     * @return List<User>
     */
    List<User> getQualityUser();
    
    /**
     * 获取用户下拉列表（台套化配置页面使用）
     * @param page
     * @param queryString
     * @return Page<Map<String,Object>>
     */
    Page<Map<String,Object>> getUserSelect(PageTbl page,String queryString);

    /**
     * @param  role_id
     * @return Map<String,Integer>
     */
    Map<String,Integer> getDeviceOrDataAutor(Long role_id);

    /**
     * 获取未完成的入库单的数量
     * @return Integer
     * */
    Integer getInAmount();

    /**
     * 获取未完成的出库单的数量
     * @return Integer
     * */
    Integer getOutAmount();

    /**
     * 获取异常的数量
     * @return Integer
     * */
    Integer getAbnormalAmount();

    /**
     * 根据用户主键获取用户所属厂、仓库的权限
     * @param userId 用户主键
     * @param usertype 1成品；2原材料
     * @return List<Map>
     */
    List<Map<String, Object>> getUserRoleByUserId(Long userId, Integer usertype);

    /**
     * 根据用户主键获取用户所属厂、仓库的权限
     * @param usertype 1成品用户；2原材料用户
     * @return List<Map>
     */
    List<Map<String, Object>> getUserRoleByUserIdTwo(Integer usertype);
}
