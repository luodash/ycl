package com.tbl.modules.platform.service.system;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.platform.entity.system.Department;

import java.util.List;
import java.util.Map;

/**
 * 部门接口
 * @author 70486
 */
public interface DepartmentService extends IService<Department> {

    /**
     * 下拉列表获取所有部门列表
     * @param params
     * @return Map<String, Object>
     */
    List<Map<String, Object>> getDepartmentList(Map<String, Object> params);

    /**
     * 获取所有部门分页列表
     * @param params
     * @return PageUtils
     */
    PageUtils getDeptPageList(Map<String, Object> params);

    /**
     * 根据主键获取部门列表信息
     * @param parentId
     * @return Map<String, Object>
     */
    List<Map<String, Object>> getplist(Long parentId);

    /**
     * 新增/更新部门
     * @param department
     * @return boolean
     */
    boolean save(Department department);

    /**
     * 获取拥有用户的部门
     * @param ids 部门id
     * @return Department
     */
    List<Department> isHasDeptUser(String ids);

    /**
     * 判断是否存在有子部门id存在
     * @param parentIds 部门父ID，多个以逗号隔开
     * @return boolean
     */
    boolean isHasSubDept(String parentIds);

    /**
     * 根据主键获取名称
     * @param ids
     * @return String
     */
    String getNameByIds(String ids );

    /**
     * 是否有相同编号的部门
     * @param deptNo
     * @param id
     * @return boolean
     */
    boolean hasDeptNo(String deptNo,String id);

    /**
     * 是否有相同名称的部门
     * @param deptName
     * @param id
     * @return boolean
     */
    boolean hasDeptName(String deptName,String id);

}
