package com.tbl.modules.platform.service.system;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.platform.entity.system.DataDictionaryDetailVO;
import java.util.List;
import java.util.Map;

/**
 * 数据字典服务
 * @author 70486
 */
public interface DataDictionaryDetailService extends IService<DataDictionaryDetailVO> {

    /**
     *
     * @param dictTypeNum
     * @return DataDictionaryDetailVO
     */
    List<DataDictionaryDetailVO> getDictListByTypeNum(String dictTypeNum);

    List<Map<String, Object>> getDicSelectList(String queryString, int pageSize, int pageNo, String dictTypeNum);

    DataDictionaryDetailVO getDataDictionaryDetail(Integer id);

    /**
     * 获取服务类型下拉选列表
     * @param params
     * @return PageUtils
     */
    PageUtils getSelectServicePageList(Map<String, Object> params);

    /**
     * @param  params
     * @return Page<DataDIctionaryDetailVO>
     */
    Page<DataDictionaryDetailVO> getPageDicDetail(Map<String, Object> params);
    
    /**
     * 根据查询条件查询盘点方式列表
     * @param  params
     * @return Page<DataDictionaryDetailVO>
     */
    Page<DataDictionaryDetailVO> getStockCheckMethodList(Map<String, Object> params);

    /**
     * select查询盘点类型数据字典
     * @param  map
     * @return Page<DataDictionaryDetailVO>
     */
    Page<DataDictionaryDetailVO> getSelectStockCheckTypeList( Map<String, Object> map);
    
}
