package com.tbl.modules.platform.service.common;

import java.util.List;
import java.util.Map;

/**
 * 公共接口
 * @author 70486
 */
public interface CommonService {

    /**
     * 获取根据查询条件获取数据字典
     * @param map
     * @return Map<String,Object>
     */
    List<Map<String,Object>> getDictionaryList(Map<String, Object> map);

    /**
     * 保存用户自定义列
     * @param columns 列
     * @param uuid 唯一标志
     * @return Map<String,Object>
     */
    Map<String,Object> dynamicColumns(String columns,String uuid);

    /**
     * 1
     * @param uuid 用户自定义列唯一标志
     * @return Map<String,Object>
     */
    Map<String,Object> dynamicGetColumns(String uuid);
}
