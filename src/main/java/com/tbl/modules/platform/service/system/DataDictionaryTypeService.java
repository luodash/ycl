package com.tbl.modules.platform.service.system;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.common.utils.PageData;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.platform.entity.system.DataDictionaryTypeVO;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * 字典类型接口
 * @author 70486
 */
public interface DataDictionaryTypeService extends IService<DataDictionaryTypeVO> {

    /**
     * 获取分页列表
     * @param params
     * @return PageUtils
     */
    PageUtils queryPage(Map<String, Object> params);

    /**
     * 根据Id查询数据字典
     * @param id 数据字典ID
     * @return DictTypeinfo
     * @Title: findDictTypeById
     */
    DataDictionaryTypeVO findDictTypeById(Long id);

    /**
     * 数据字典保存操作
     * @param dictionaryTypeVO 数据字典
     * @return boolean
     * @Title: saveDictType
     */
    boolean saveDictType(DataDictionaryTypeVO dictionaryTypeVO);

    /**
     * 数据字典修改保存
     * @param dictionaryTypeVO 数据字典
     * @return boolean
     * @Title: updateDictType
     */
    boolean updateDictType(DataDictionaryTypeVO dictionaryTypeVO);

    /**
     * 验证编号/名称是否存在
     * @param pd
     * @return DictTypeinfo
     * @Title: isExitsDictTypeNo
     */
    DataDictionaryTypeVO isExitsDictType(PageData pd);

    /**
     * 数据字典删除操作
     * @param ids 数据字典ID集合
     * @return boolean
     * @Title: delDictTypeByIds
     */
    boolean delDictTypeByIds(String[] ids);

    /**
     * 获取导出列
     * @param ids
     * @return DataDictionaryTypeVO
     */
    List<DataDictionaryTypeVO> getAllLists(String ids);

    /**
     * 导出excel
     * @param response
     * @param path
     * @param list
     */
    void toExcel(HttpServletResponse response, String path, List<DataDictionaryTypeVO> list);
}
