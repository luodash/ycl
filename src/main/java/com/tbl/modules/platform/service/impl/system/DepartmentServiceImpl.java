package com.tbl.modules.platform.service.impl.system;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.Query;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.platform.dao.system.DepartmentDAO;
import com.tbl.modules.platform.entity.system.Department;
import com.tbl.modules.platform.service.system.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 部门接口实现
 * @author 70486
 */
@Service("departmentService")
public class DepartmentServiceImpl extends ServiceImpl<DepartmentDAO, Department> implements DepartmentService {

    /**
     * 部门DAO
     */
    @Autowired
    private DepartmentDAO departmentDAO;

    /**
     * 下拉列表获取所有部门列表
     * @param params
     * @return Map<String,Object>
     */
    @Override
    public List<Map<String, Object>> getDepartmentList(Map<String, Object> params) {
        return departmentDAO.selectDeptList(params);
    }

    /**
     * 获取所有部门分页列表
     * @param params
     * @return PageUtils
     */
    @Override
    public PageUtils getDeptPageList(Map<String, Object> params) {
        Page<Department> page = this.selectPage(new Query<Department>(params).getPage(), new EntityWrapper<>());
        return new PageUtils(page.setRecords(departmentDAO.getDeptList(page, params)));
    }

    /**
     * 根据主键获取部门列表信息
     * @param parentId
     * @return Map<String,Object>
     */
    @Override
    public List<Map<String, Object>> getplist(Long parentId) {
        return departmentDAO.getplist(parentId);
    }

    /**
     * 新增/更新部门
     * @param department
     * @return boolean
     */
    @Override
    public boolean save(Department department) {
        boolean ret;
        //新增
        if( department.getDeptId() == null ) {
            ret = this.insert(department);
        //修改
        } else {
            ret = this.updateById(department);
        }
        return ret;
    }

    /**
     * 获取拥有用户的部门
     * @param ids 部门id
     * @return Department
     */
    @Override
    public List<Department> isHasDeptUser(String ids) {
        List<Department> _deptList = new ArrayList<>();
        if(!StringUtils.isEmptyString( ids )) {
            List<Long> lstId = Arrays.stream(ids.split(",")).map(s -> Long.parseLong(s.trim())).collect(Collectors.toList());
            _deptList = departmentDAO.isHasDeptUser(lstId);
        }
        return _deptList;
    }

    /**
     * 判断是否存在有子部门id存在
     * @param parentIds 部门父ID，多个以逗号隔开
     * @return boolean
     */
    @Override
    public boolean isHasSubDept(String parentIds) {
        boolean ret = false;
        if( !StringUtils.isEmptyString( parentIds )) {
            String[] parentArray = parentIds.split(",");
            Map<String, Object> map;
            for( String pid : parentArray ) {
                map = new HashMap<>(1);
                map.put("parent_id", Long.valueOf(pid));
                List<Department> lstDept = this.selectByMap(map);
                if( lstDept != null && lstDept.size() > 0 ) {
                    ret = true;
                    break;
                }
            }
        }
        return ret;
    }

    /**
     * 根据主键获取名称
     * @param ids
     * @return String
     */
    @Override
    public String getNameByIds(String ids ) {
        StringBuilder username = new StringBuilder();
        if(StringUtils.isNotEmpty(ids)){
            List<Long> lstId = Arrays.stream(ids.split(",")).map(s -> Long.parseLong(s.trim())).collect(Collectors.toList());
            List<Department> lstDept = baseMapper.selectBatchIds(lstId);

            for (Department dept: lstDept) {
                username.append(dept.getDepartmentName()).append(",");
            }
            if (!StringUtils.isEmpty(username.toString())) {
                username = new StringBuilder(username.substring(0, username.length() - 1));
            }
        }
        return username.toString();
    }

    /**
     * 是否有相同编号的部门
     * @param deptNo
     * @return boolean
     */
    @Override
    public boolean hasDeptNo(String deptNo,String id) {
        boolean ret = false;
        if(!StringUtils.isEmptyString(deptNo)) {
            Map<String, Object> map = new HashMap<>(1);
            map.put("DEPARTMENT_NO", Long.valueOf(deptNo));
            List<Department> lstDept = baseMapper.selectByMap(map);
            if(StringUtils.isNotEmpty(id)){
                map.put("ID", Long.valueOf(id));
                lstDept = baseMapper.selectByMap(map);
            }

            if (lstDept != null && lstDept.size() > 0) {
                ret = true;
            }
        }
        return ret;
    }

    /**
     * 是否有相同名称的部门
     * @param deptName
     * @return boolean
     */
    @Override
    public boolean hasDeptName(String deptName,String id) {
        boolean ret = false;
        if( !StringUtils.isEmptyString(deptName)) {
            Map<String, Object> map = new HashMap<>(2);
            map.put("DEPARTMENT_NAME", Long.valueOf(deptName));

            List<Department> lstDept = baseMapper.selectByMap(map);
            if(StringUtils.isNotEmpty(id)){
                map.put("ID", Long.valueOf(id));
                lstDept = baseMapper.selectByMap(map);
            }
            if (lstDept != null && lstDept.size() > 0) {
                ret = true;
            }
        }
        return ret;
    }

}
