package com.tbl.modules.platform.service.impl.system;

import com.tbl.modules.platform.dao.system.HomeConfigDAO;
import com.tbl.modules.platform.entity.system.HomeConfig;
import com.tbl.modules.platform.service.system.IndexService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 主页处理接口实现
 * @author 70486
 */
@Service("indexService")
public class IndexServiceImpl implements IndexService {

    /**
     * 首页配置
     */
    @Resource
    private HomeConfigDAO homeConfigDAO;

    @Override
    public List<HomeConfig> getConfig(Long role_id) {
        List<HomeConfig> list = homeConfigDAO.getConfig(role_id);
        list.forEach(a->{
            a.setModel_height(a.getModelHeight());
            a.setModel_key(a.getModelKey());
            a.setModel_name(a.getModelName());
            a.setModel_title(a.getModelTitle());
            a.setModel_type(a.getModelType());
            a.setModel_updatetime(a.getModelUpdatetime());
            a.setModel_url(a.getModelUrl());
        });
        return list;
    }

    @Override
    public Map<String, Object> getUserConfig(Long userId) {
        return homeConfigDAO.getUserConfig(userId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean saveConfig(String hidemodel, String userid, String layout, String sortable) {
        int count = homeConfigDAO.selectByUserId(Long.parseLong(userid));
        if(count>0){
            homeConfigDAO.updateSUC(hidemodel,layout,sortable,Long.parseLong(userid));
        }else{
            homeConfigDAO.insertSUC(hidemodel,layout,sortable,Long.parseLong(userid));
        }
        return true;
    }
}
