package com.tbl.modules.platform.service.system;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.platform.entity.system.Menu;

import java.util.List;
import java.util.Map;

/**
 * 菜单接口
 * @author 70486
 */
public interface MenuService extends IService<Menu> {

    /**
     * 获取所有菜单列表
     * @param menuMap
     * @return Menu
     */
    List<Menu> listAllMenuByRoleId(Map<String, Object> menuMap);

    /**
     * 获取角色所有快捷菜单
     * @param map
     * @return Menu
     */
    List<Menu> listquickMenu(Map<String, Object> map);

    /**
     * 获取所有菜单列表
     * @return List<Menu>
     */
    List<Menu> listAllMenu();

    /**
     * 获取所有菜单列表
     * @param parentId
     * @return List<Menu>
     */
    List<Menu> listAllMenu(Long parentId);

    /**
     * 获取所有菜单分页列表
     * @param params
     * @return PageUtils
     */
    PageUtils getMenuPageList(Map<String, Object> params);

}
