package com.tbl.modules.platform.service.system;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.platform.entity.system.Log;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 系统操作日志服务
 * @author 70486
 */
public interface LogService extends IService<Log> {

    /**
     * 记录系统的操作日志
     * @param operation 操作内容（用户登录成功！）
     * @param action 用户动作
     * @param request
     * @return boolean
     */
     boolean logInsert(String operation, long action, HttpServletRequest request);

    /**
     * 记录系统的操作日志
     * @param operation 操作内容（用户登录成功！）
     * @param action 用户动作
     * @param request
     * @param json
     */
     void logInsertGrenId(String operation, long action, HttpServletRequest request, Object json);

    /**
     * 列表页
     * @param pageTbl
     * @param map
     * @return PageUtils
     */
    PageUtils getPageList(PageTbl pageTbl, Map<String, Object> map);
}
