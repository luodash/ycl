package com.tbl.modules.platform.service.impl.common;

import com.baomidou.mybatisplus.plugins.Page;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.tbl.modules.platform.dao.system.ColumnsDAO;
import com.tbl.modules.platform.entity.system.Columns;
import com.tbl.modules.platform.entity.system.DataDictionaryDetailVO;
import com.tbl.modules.platform.service.common.CommonService;
import com.tbl.modules.platform.service.system.DataDictionaryDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.tbl.modules.platform.util.EhcacheUtils.columnCache;

/**
 * 公告接口实现
 * @author 70486
 */
@Service("commonService")
public class CommonServiceImpl implements CommonService {

    @Autowired
    private DataDictionaryDetailService dicDetailService;
    @Autowired
    private ColumnsDAO columnsDAO;

    /**
     * 获取根据查询条件获取数据字典
     * @param map 查询条件
     * @return Map<String,Object>
     */
    @Override
    public List<Map<String,Object>> getDictionaryList(Map<String, Object> map) {
        Page<DataDictionaryDetailVO> pageDicDetail = dicDetailService.getPageDicDetail(map);
        List<Map<String,Object>> lstMap = new ArrayList<>();
        Map<String,Object> dicMap;
        for (DataDictionaryDetailVO dicDetail:pageDicDetail.getRecords()) {
            dicMap = new HashMap<>();
            dicMap.put("id", dicDetail.getId());
            dicMap.put("text", dicDetail.getDictValue());
            lstMap.add(dicMap);
        }

        return lstMap;
    }

    /**
     * 保存用户自定义列
     * @param columns 列
     * @param uuid 唯一标志
     * @return Map<String,Object>
     */
    @Override
    public Map<String,Object> dynamicColumns(String columns,String uuid) {
        Map<String,Object> map= Maps.newHashMap();
        map.put("uuid", uuid);
        map.put("columns", columns);
        boolean result = columnsDAO.insertCoumns(map);
        columnCache.put(uuid,columns);
        map.put("result",result);
        map.put("msg","操作成功!");
        return map;
    }

    /**
     * 1
     * @param uuid 用户自定义列唯一标志
     * @return Map<String,Object>
     */
    @Override
    public Map<String,Object> dynamicGetColumns(String uuid) {
        Map<String,Object> map=Maps.newHashMap();
        String content = "";
        Object data=columnCache.get(uuid);
        if(data==null){
            map.put("uuid", uuid);
            List<Columns> lstColumn = columnsDAO.selectByMap(map);

            if(lstColumn != null && lstColumn.size() > 0){
                columnCache.put(uuid, lstColumn.get(0).getContent()==null?"":lstColumn.get(0).getContent());
                content = lstColumn.get(0).getContent()==null?"":lstColumn.get(0).getContent();
            }
        }
        map.put("data", content);
        return map;
    }

}
