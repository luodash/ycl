package com.tbl.modules.platform.service.system;

import com.tbl.modules.platform.entity.system.HomeConfig;

import java.util.List;
import java.util.Map;

/**
 * 主要处理接口
 * @author 70486
 */
public interface IndexService {

    List<HomeConfig> getConfig(Long role_id);

    Map<String, Object> getUserConfig(Long userId);

    Boolean saveConfig(String hidemodel, String userid, String layout, String sortable);

}
