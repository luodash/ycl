package com.tbl.modules.platform.service.impl.system;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.Query;
import com.tbl.modules.platform.dao.system.DataDictionaryDetailDAO;
import com.tbl.modules.platform.entity.system.DataDictionaryDetailVO;
import com.tbl.modules.platform.service.system.DataDictionaryDetailService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 数据字典服务实现类
 * @author 70486
 */
@Service("dataService")
public class DataDictionaryDetailServiceImpl extends ServiceImpl<DataDictionaryDetailDAO, DataDictionaryDetailVO>
        implements DataDictionaryDetailService {

    /**
     * 数据字典
     */
    @Autowired
    private DataDictionaryDetailDAO dataDao;

    @Override
    public List<DataDictionaryDetailVO> getDictListByTypeNum(String dictTypeNum) {
        return dataDao.getDictListByTypeNum(dictTypeNum);
    }

    @Override
    public List<Map<String, Object>> getDicSelectList(String queryString, int pageSize, int pageNo, String dictTypeNum) {
        return null;
    }

    @Override
    public DataDictionaryDetailVO getDataDictionaryDetail(Integer id){
        return this.selectById(id);
    }

    /**
     * 获取服务类型下拉选列表
     * @param params
     * @return PageUtils
     */
    @Override
    public PageUtils getSelectServicePageList(Map<String, Object> params) {
        String queryString = (String)params.get("queryString");
        Page<DataDictionaryDetailVO> page = this.selectPage(new Query<DataDictionaryDetailVO>(params).getPage(),
                new EntityWrapper<DataDictionaryDetailVO>()
                        .eq("dict_type_num", "WMS_INTERFACESERVICE_TYPE")
                        .like(StringUtils.isNotBlank(queryString),"address", queryString)
        );
        return new PageUtils(page);
    }
    
    /**
     * 根据查询条件查询盘点方式列表
     * @param  params
     * @return Page<DataDictionaryDetailVO>
     */
    @Override
    public Page<DataDictionaryDetailVO> getStockCheckMethodList(Map<String, Object> params) {
        String queryString = (String)params.get("queryString");
        return this.selectPage(new Query<DataDictionaryDetailVO>(params).getPage(), new EntityWrapper<DataDictionaryDetailVO>()
                        .eq("DICT_TYPE_NUM", "WMS_STOCKCHECK_METHOD")
                        .like(StringUtils.isNotBlank(queryString),"DICT_VALUE", queryString));
    }

    /**
     * 查询数据字典明细
     * @param  params
     * @return Page<DataDictionaryDetailVO>
     */
    @Override
    public Page<DataDictionaryDetailVO> getPageDicDetail(Map<String, Object> params) {
        String queryString = (String)params.get("queryString");
        String dictionaryType = (String)params.get("dictionaryType");
        return this.selectPage(new Query<DataDictionaryDetailVO>(params).getPage(), new EntityWrapper<DataDictionaryDetailVO>()
                        .eq("dict_type_num", dictionaryType)
                        .like(StringUtils.isNotBlank(queryString),"dict_value", queryString)
        );
    }
    
    /**
     * select查询盘点类型数据字典
     * @param  map
     * @return Page<DataDictionaryDetailVO>
     */
    @Override
    public Page<DataDictionaryDetailVO> getSelectStockCheckTypeList( Map<String, Object> map) {
    	Page<DataDictionaryDetailVO> page = this.selectPage(new Query<DataDictionaryDetailVO>(map).getPage(), new EntityWrapper<>());
        return page.setRecords(dataDao.getSelectStockCheckTypeList(page,map));
    }
}
