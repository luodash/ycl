package com.tbl.modules.platform.service.system;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.common.utils.PageTbl;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.platform.constant.OperationCode;
import com.tbl.modules.platform.entity.system.Role;

import java.util.List;
import java.util.Map;

/**
 * 角色接口
 * @author 70486
 */
public interface RoleService extends IService<Role> {

    /**
     * 下拉列表获取所有角色列表
     * @param params
     * @return List<Map<String, Object>>
     */
    List<Map<String, Object>> getRoleList(Map<String, Object> params);

    /**
     * 下拉列表获取所有角色列表
     * @param pageTbl
     * @param params
     * @return PageUtils
     */
    PageUtils getRolePageListS(PageTbl pageTbl, Map<String, Object> params);

    /**
     * 查询角色信息
     * @param params
     * @return PageUtils
     */
    PageUtils getRolePageListS1(Map<String, Object> params);

    /**
     * 获取角色列表
     * @return Map<String, Object>
     */
    List<Map<String, Object>> getRoles();

    /**
     * 根据角色主键获取角色菜单
     * @param rid
     * @param type
     * @param notInMenuNumber
     * @param menuNumber
     * @return List<Map>
     */
    List<Map<String, Object>> getMenuListByRid(Long rid, Integer type, Integer notInMenuNumber, Integer menuNumber);

    /**
     * 保存信息，并返回主键
     * @param role 角色对象
     * @return Long
     */
    Long saveForGernatedKey(Role role);

    /**
     * 角色保存/修改
     * @param role
     * @return boolean
     */
    boolean saveRole(Role role);

    /**
     * 批量保存角色对应菜单信息
     * @param roleId 角色ID
     * @param type
     */
    void delRoleMenuByRoleId(long roleId, Integer type);

    /**
     * 批量保存角色对应菜单信息
     * @param roleId  角色ID
     * @param menuIds 菜单ID
     * @param type
     * @return boolean
     */
    boolean saveRoleMenu(long roleId, String[] menuIds, Integer type);

    /**
     * 根据角色主键获取角色名称
     * @param ids
     * @return String
     */
    String getRoleNameByRoleids(String ids);

    /**
     * 查询存在用户的角色列表
     * @param ids 角色id
     * @return Role
     */
    List<Role> isHasRoleUser(String ids);

    /**
     * 删除角色信息
     * @param ids
     * @return boolean
     */
    boolean delRole(String ids);

    /**
     * 查询是否有相同名称的角色
     * @param name 角色名称
     * @param rid
     * @return boolean
     */
    boolean isHasRoleByName(String name, String rid);

    /**
     * 角色菜单权限查询
     * @param roleId
     * @param menuId
     * @return OperationCode
     */
    OperationCode selectOperationByRoleId(Long roleId, Long menuId);


    /**
     * 更新用户权限
     * @param roleId 角色id
     * @param qxName 权限名称
     * @param qxValue 权限值
     * @return
     */
//    boolean updateRoleQX(String roleId,String qxName,String qxValue);

    /**
     * 更新菜单下的按钮权限
     * @param menuId 菜单主键
     */
    void updateByMenuId(Long menuId);

    /**
     * 更新菜单下的按钮权限
     * @param menuId 菜单主键
     */
    void setBakByMenuId(Long menuId);

}
