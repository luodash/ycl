package com.tbl.modules.platform.service.system;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.platform.entity.system.HomeConfig;

import java.util.Map;

/**
 * 首页配置接口
 * @author 70486
 */
public interface HomeConfigService extends IService<HomeConfig> {

    /**
     * 获取首页配置分页列表
     * @param params
     * @return PageUtils
     */
    PageUtils getHomeConfigPageList(Map<String, Object> params);

    /**
     * 保存/修改
     * @param hc
     * @param roles
     * @return Map<String, Object>
     */
    Map<String, Object> save(HomeConfig hc, String roles);

    /**
     * 1
     * @param id
     * @return Map<String, Object>
     */
    Map<String, Object> getModel(String id);

    /**
     * 1
     * @param models
     * @return Map<String, Object>
     */
    Map<String, Object> deleteModel(String models);
}
