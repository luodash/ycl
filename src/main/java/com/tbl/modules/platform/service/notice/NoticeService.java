package com.tbl.modules.platform.service.notice;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.common.utils.PageUtils;
import com.tbl.modules.platform.entity.notice.Notice;
import org.springframework.stereotype.Service;

import java.util.Map;


/**
 * 公告处理接口
 * @author 70486
 */
@Service("noticeService")
public interface NoticeService extends IService<Notice> {

	/**
	 * 获取公告列表
	 * @param params
	 * @return PageUtils
	 */
	PageUtils queryPage(Map<String, Object> params);

	/**
	 * 保存信息，并返回主键
	 * @param notice 公告对象
	 * @return Long
	 */
	Long saveForGernatedKey(Notice notice);

	/**
	 * 保存/更新公告
	 * @param notice
	 * @return boolean
	 */
	boolean saveNotice(Notice notice);

	/**
	 * 根据公告主键获取公告名称
	 * @param ids
	 * @return String
	 */
	String getNoticeNameByNoticeids(String ids);

	/**
	 * 批量删除公告
	 * @param ids
	 * @return boolean
	 */
	boolean deleteAllU(String ids);
	
}
