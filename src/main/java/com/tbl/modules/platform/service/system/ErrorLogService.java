package com.tbl.modules.platform.service.system;

import com.baomidou.mybatisplus.service.IService;
import com.tbl.modules.platform.entity.system.ErrorLog;

/**
 * 错误日志服务
 * @author 70486
 */
public interface ErrorLogService extends IService<ErrorLog> {

    /**
     * 保存错误日志
     * @param info
     * @param remark
     */
    void saveErrorLog(String info, String remark);

    /**
     * 报错
     * @param tableName
     * @param id
     */
    void startModData(String tableName,Long id);

    /**
     * 2
     * @param tableName
     * @param tableId
     * @param operation
     * @param logId
     */
    void completeModData(String tableName,Long tableId,String operation,Long logId);

}
