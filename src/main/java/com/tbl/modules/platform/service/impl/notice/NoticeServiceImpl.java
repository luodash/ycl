package com.tbl.modules.platform.service.impl.notice;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.tbl.common.utils.DateUtils;
import com.tbl.common.utils.PageUtils;
import com.tbl.common.utils.StringUtils;
import com.tbl.modules.platform.dao.notice.NoticeDAO;
import com.tbl.modules.platform.entity.notice.Notice;
import com.tbl.modules.platform.service.notice.NoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 公告处理接口实现
 * @author 70486
 */
@Service("noticeService")
public class NoticeServiceImpl extends ServiceImpl<NoticeDAO, Notice> implements NoticeService {

    /**
     * 公告DAO
     */
    @Autowired
    private NoticeDAO noticeDAO;

    /**
     * 获取公告列表
     * @param params
     * @return PageUtils
     */
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String operationTime = (String) params.get("operationTime");
        String timeEndd = (String) params.get("operationTime_end");
        if (StringUtils.isNotEmpty(operationTime)) {
            params.put("operationTime", DateUtils.stringToLong(operationTime, "yyyy-MM-dd"));
        }
        if (StringUtils.isNotEmpty(timeEndd)) {
            params.put("operationTime_end", DateUtils.stringToLong(timeEndd, "yyyy-MM-dd"));
        }
        Page<Notice> page = new Page<>(Integer.parseInt(params.get("page").toString()),Integer.parseInt(params.get("limit").toString()));
        return new PageUtils(page.setRecords(noticeDAO.selectPageNotice(page, params)));
    }

    /**
     * 保存信息，并返回主键
     * @param notice 公告对象
     * @return Long
     */
    @Override
    public Long saveForGernatedKey(Notice notice) {
        baseMapper.insert(notice);
        return notice.getId();
    }

    /**
     * 保存/更新公告
     * @param notice
     * @return boolean
     */
    @Override
    public boolean saveNotice(Notice notice) {
        boolean ret;
        Long noticeId = notice.getId();
        if(noticeId != null) {
            //更新
            ret = this.updateById(notice);
        } else {
            //新增
            ret = this.insert(notice);
        }
        return ret;
    }

    /**
     * 根据公告主键获取公告名称
     * @param ids
     * @return String
     */
    @Override
    public String getNoticeNameByNoticeids(String ids) {
        StringBuilder title = new StringBuilder();
        if(StringUtils.isNotEmpty(ids)){
            ids = ids.replaceAll("'","");
            List<Long> lstId = Arrays.stream(ids.split(",")).map(s -> Long.parseLong(s.trim())).collect(Collectors.toList());
            List<Notice> lstNotice = baseMapper.selectBatchIds(lstId);
            for (Notice notice: lstNotice) {
                title.append(notice.getTitle()).append(",");
            }
            if (!StringUtils.isEmpty(title.toString())) {
                title = new StringBuilder(title.substring(0, title.length() - 1));
            }
        }
        return title.toString();
    }

    /**
     * 批量删除公告
     * @param ids
     * @return boolean
     */
    @Override
    public boolean deleteAllU(String ids) {
        ids = ids.replaceAll("'","");
        List<Long> lstId = Arrays.stream(ids.split(",")).map(s -> Long.parseLong(s.trim())).collect(Collectors.toList());
        return this.deleteBatchIds(lstId);
    }

}
