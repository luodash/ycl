package com.tbl.modules.platform.util;

import com.google.common.base.Objects;
import com.tbl.common.config.StaticConfig;
import com.tbl.modules.les.constant.RestTemplateUtil;
import org.springframework.web.client.RestTemplate;

/**
 * 中间件亮灯操作
 * @author 70486
 */
public class LightControllUtil {

    public static RestTemplate restTemplate= RestTemplateUtil.createRestTemplate();

    public static Integer restAmount=Integer.parseInt(StaticConfig.getStaticResetCount1());

    /**
     * 整货上架(散货补货)指令
     */
    public static final String URI_WHOLE_UPLOAD = "http://" + StaticConfig.getIp() + ":" + StaticConfig.getPort() + "/" + StaticConfig.getName() +
            "/" + StaticConfig.getPutBillUrl();
    /**
     * 整货下架接口
     */
    public static final String URI_DOWNLOAD = "http://" + StaticConfig.getIp() + ":" + StaticConfig.getPort() + "/" + StaticConfig.getName() +
            "/" + StaticConfig.getDownBillUrl();

    /**
     * 换箱成功后亮绿灯进行确认
     */
    public static final String URI_UPLOAD_OK = "http://" + StaticConfig.getIp() + ":" + StaticConfig.getPort() + "/" + StaticConfig.getName() +
            "/" + StaticConfig.getPutBillOkLocation();

    /**
     * 散货单独亮灯或灭灯
     */
    public static final String URI_WHOLE_BATCHDOWN = "http://" + StaticConfig.getIp() + ":" + StaticConfig.getPort() + "/" + StaticConfig.getName() +
            "/"+ StaticConfig.getLightOn();

    /**
     * 散货单独亮灯或灭灯
     */
    public static final String URI_WHOLE_BATCHDOWN_BJ = "http://" + StaticConfig.getIp() + ":" + StaticConfig.getPort() + "/" + StaticConfig.getName() +
            "/"+ StaticConfig.getLightOnBj();
    

    public static final String LCD_CALCULATE_WEIGHT = "http://" + StaticConfig.getIp() + ":" + StaticConfig.getPort() + "/" + StaticConfig.getName() +
            "/"+ StaticConfig.getLcdNum();

    public static final String LCD_OFF_LIGHT = "http://" + StaticConfig.getIp() + ":" + StaticConfig.getPort() + "/" + StaticConfig.getName() +
            "/"+ StaticConfig.getLcdOff();

    public static final String URI_WHOLE_CLOSE = "http://" + StaticConfig.getIp() + ":" + StaticConfig.getPort() + "/" + StaticConfig.getName() +
            "/"+ StaticConfig.getLightOff();
    
    public static final String CAR_OFF_LIGHT = "http://" + StaticConfig.getIp() + ":" + StaticConfig.getPort() + "/" + StaticConfig.getName() +
            "/"+ StaticConfig.getCarOff();

    public static final String ONLY_WHOLE_ON = "http://" + StaticConfig.getIp() + ":" + StaticConfig.getPort() + "/" + StaticConfig.getName() +
            "/"+ StaticConfig.getLightOnWholeOnly();

    /**
     * 换箱正确后,闪烁绿灯进行确认操作
     * @param locationId
     * @param type  0表示亮绿灯(表示换箱上架完成) 1表示亮红灯(表示上架错误)
     */
    public static void wholeLocationOk(String locationId, String type) {
        int count=0;
        while (true) {
            try {
                String num = "";
                //如果等于0 表示上架正确,亮灯三次
                if (Objects.equal(type, "0")) {
                    num = StaticConfig.getLightNumber();
                }
                //如果等于1 表示报警,亮灯次数与确认的次数是不一样的
                if (Objects.equal(type, "1")) {
                    num = StaticConfig.getErrorLightNumber();
                }
                restTemplate.postForObject(URI_UPLOAD_OK + "/{type}/{houseId}/{num}", null, String.class, (Object) new String[]{type, locationId, num});
                break;
            } catch (Exception e) {
                e.printStackTrace();
            }
            count++;
            if(count==restAmount){
                break;
            }
        }
    }


    /**
     * 散货补货(无论是随机上架还是指定货位上架,要上多少个货位就要调用多少次接口)
     * @param upType     0表示随机 1表示指定
     * @param locationId 随机时指定库区 指定时货位
     * @param amount     零散件用数量
     */
    public static void bluckLocationUpload(String upType, String locationId, String amount) {
        int count=0;
        while (true) {
            try {
                restTemplate.postForObject(URI_WHOLE_UPLOAD + "/{upType}/{houseId}/{num}.do", null, String.class, (Object) new String[]{upType, locationId, amount});
                break;
            } catch (Exception e) {
                e.printStackTrace();
            }
            count++;
            if(count==restAmount){
                break;
            }
        }
    }

    /**
     * 整货上架(无论是随机上架还是指定货位上架,要上多少个货位就要调用多少次接口)
     * @param upType     0表示随机 1表示指定
     * @param locationId 随机时指定库区 指定时货位
     */
    public static boolean wholeLocationUpload(String upType, String locationId) {
        int count=0;
        boolean ret;
        while (true) {
            try {
                restTemplate.postForObject(URI_WHOLE_UPLOAD + "/{upType}/{houseId}/{num}.do", null, String.class, (Object) new String[]{upType, locationId, "0"});
                ret=true;
                break;
            } catch (Exception e) {
                e.printStackTrace();
            }
            count++;
            if(count==restAmount){
            	ret=false;
                break;
            }
        }
		return ret;
    }

    /**
     * 整货下架
     * @param locationId 货位
     * @return Object
     */
    public static Object wholeLocationDownload(String locationId) {
        String result = "";
        int count=0;
        while (true) {
            try {
                result = restTemplate.postForObject(URI_DOWNLOAD+ "/{houseId}/{num}", null, String.class, (Object) new String[]{locationId, "0"});
                break;
            } catch (Exception e) {
                e.printStackTrace();
            }
            count++;
            if(count==restAmount){
                break;
            }
        }
        return result;
    }

    /**
     * 散货拣选状态
     * @param locationId 货位ID
     * @param amount     拣选数量
     * @return Object
     */
    public static Object blukLocationPickUp(String locationId, String amount) {
        String result = "";
        int count=0;
        while (true) {
            try {
                result = restTemplate.postForObject(URI_DOWNLOAD+ "/{houseId}/{num}", null, String.class, (Object) new String[]{locationId, amount});
                break;
            } catch (Exception e) {
                e.printStackTrace();
            }
            count++;
            if(count==restAmount){
                break;
            }
        }
        return result;
    }

    /**
     * 散货拣选
     */
    public static Object lightOn(String houseId,String num){
        String result = "";
        int count=0;
        while (true) {
            try {
                //表示北京
            	if("0".equals(StaticConfig.getAdapteCz())){
            		result = restTemplate.postForObject(URI_DOWNLOAD+ "/{houseId}/{num}", null, String.class, (Object) new String[]{houseId, num});
            	}
                //表示常州
            	if("1".equals(StaticConfig.getAdapteCz())){
            		result = restTemplate.postForObject(URI_WHOLE_BATCHDOWN+ "/{houseId}/{num}", null, String.class, (Object) new String[]{houseId, num});
            	}

                break;
            } catch (Exception e) {
                e.printStackTrace();
            }
            count++;
            if(count==restAmount){
                break;
            }
        }
        return result;
    }

    /**
     *
     * @param houseId               货位
     * @param type                  类型 0表示关,1表示开
     * @param num                   实际数量
     * @param containernum          容器重量
     * @param materialnum           物品重量
     * @return Object
     */
    public static Object lcdCalculate(Integer houseId,Integer type,Integer num,Integer containernum,Integer materialnum){
        String result = "";
        int count=0;
        while (true) {
            try {
                result = restTemplate.postForObject(LCD_CALCULATE_WEIGHT +"/{houseId}/{type}/{num}/{containernum}/{materialnum}", null, String.class,
                        (Object) new Integer[]{houseId,type,0,containernum,materialnum});
                break;
            } catch (Exception e) {
                e.printStackTrace();
            }
            count++;
            if(count==restAmount){
                break;
            }
        }
        return result;
    }


    /**
     * 灭拣选灯以及智能小车上的灯
     * @param type			类型   0表示关闭补货  1表示关闭取货
     * @param houseId		货位的ID
     * @return String
     */
    public static String lcdOff(String type,String houseId){
        String result = "";
        int count=0;
        while (true) {
            try {
                result = restTemplate.postForObject(LCD_OFF_LIGHT, null, String.class, (Object) new String[]{type, houseId});
                break;
            } catch (Exception e) {
                e.printStackTrace();
            }
            count++;
            if(count==restAmount){
                break;
            }
        }
        return result;
    }

    /**
     * 单独熄灭球泡灯（小车上）
     * @param type              0上架关  1下架关
     * @param houseId
     * @return String
     */
    public static String lightClosed(String type,String houseId){
        String result = "";
        int count=0;
        while (true) {
            try {
                result = restTemplate.postForObject(URI_WHOLE_CLOSE+"/{type}/{houseId}", null, String.class, (Object) new String[]{type, houseId});
                break;
            } catch (Exception e) {
                e.printStackTrace();
            }
            count++;
            if(count==restAmount){
                break;
            }
        }
        return result;
    }
    
    /**
     * 灭拣选灯以及智能小车上的灯
     * @param type			类型   0表示关闭补货  1表示关闭取货
     * @param houseId		货位的ID
     * @param num		实际数量
     * @param containernum		 容器重量
     * @param materialnum		物品重量
     */
    public static String lcdOffbj(String type,String houseId,String num,String containernum,String  materialnum){
        String result = "";
        int count=0;
        while (true) {
            try {
                result = restTemplate.postForObject(CAR_OFF_LIGHT, null, String.class, (Object) new String[]{houseId,type,num,containernum,materialnum });
//                ErrorLogUtil.saveErrorLog(houseId+" 小车灭灯指令发送成功!", new Date(), "小车LCD数字灭灯");
                break;
            } catch (Exception e) {
                e.printStackTrace();
            }
            count++;
            if(count==restAmount){
                break;
            }
        }
        return result;
    }

    /**
     * 散货拣选灯亮起（北京使用）
     * @param houseId
     * @param num
     * @return Object
     */
    public static Object lcdLightOn(String houseId,String num){
        String result = "";
        int count=0;
        while (true) {
            try {
                //表示北京
                if("0".equals(StaticConfig.getAdapteCz())){
                    result = restTemplate.postForObject(URI_WHOLE_BATCHDOWN_BJ, null, String.class, (Object) new String[]{"1",houseId, num});
                }
                break;
            } catch (Exception e) {
                e.printStackTrace();
            }
            count++;
            if(count==restAmount){
                break;
            }
        }
        return result;
    }

    /**
     * 单独亮球泡灯,不灭,须发命令关
     * @param type 0绿灯 1红灯
     * @param houseId 货位主键
     * @return Object
     */
    public static Object lightOnWholeOnly(String type,String houseId){
        String result = "";
        int count=0;
        while (true) {
            try {
                result = restTemplate.postForObject(ONLY_WHOLE_ON+"/{type}/{houseId}", null, String.class, (Object) new String[]{type,houseId});
                break;
            } catch (Exception e) {
                e.printStackTrace();
            }
            count++;
            if(count==restAmount){
                break;
            }
        }
        return result;
    }
}
