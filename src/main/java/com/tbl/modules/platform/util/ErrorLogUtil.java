package com.tbl.modules.platform.util;

import com.tbl.modules.platform.service.system.ErrorLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

/**
 * 异常日志
 * @author 70486
 */
@Component
public class ErrorLogUtil {

    /**
     * 错误日志服务
     */
    @Autowired
    private ErrorLogService errorLogService;

    /**
     * 保存错误信息
     * @param info
     * @param remark
     */
    public void saveErrorLog(String info, String remark) {
        errorLogService.saveErrorLog(info, remark);
    }

    /**
     * 开始修改数据
     * @param tableName
     * @param id
     */
    public void startModData(String tableName, Long id) {
        errorLogService.startModData(tableName, id);
    }

    /**
     * 2
     * @param tableName
     * @param tableId
     * @param operation
     * @param logId
     */
    public void completeModData(String tableName, Long tableId, String operation, Long logId) {
        errorLogService.completeModData(tableName, tableId, operation, logId);
    }
}