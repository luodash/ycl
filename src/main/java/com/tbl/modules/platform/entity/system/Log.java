package com.tbl.modules.platform.entity.system;


import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 系统日志
 * @author 70486
 */
@TableName("sys_log")
public class Log implements Serializable {

    @TableId("id")
    @Getter
    @Setter
    private Long id;
    /**
     * 用户ID
     */
    @Getter
    @Setter
    private Long userId;
    /**
     * 操作时间
     */
    @Getter
    @Setter
    private Long operationTime;
    @Getter
    @Setter
    private String operationTimeStr;
    /**
     * 结束时间：用做查询中的条件
     */
    @Getter
    @Setter
    private String operationTimeEnd;

    /**
     * 操作内容
     */
    @Getter
    @Setter
    private String operation;

    /**
     * 用户动作
     */
    @Getter
    @Setter
    private Long useraction;

    /**
     * 操作用户IP
     */
    @Getter
    @Setter
    private String userip;

    /**
     * 操作用户MAC
     */
    @Getter
    @Setter
    private String usermac;

    /**
     * 操作用户名
     */
    @Getter
    @Setter
    private String userName;

    /**
     * 操作动作的中文描述
     */
    @Getter
    @Setter
    private String logActionDesc;


    @Getter
    @Setter
    private String jsonstr;

}
