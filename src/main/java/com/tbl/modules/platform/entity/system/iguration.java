package com.tbl.modules.platform.entity.system;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 手持权限
 * @author zp
 */
@TableName("SYS_CONFIGURATION")
@Getter
@Setter
public class iguration implements Serializable {

    @TableField(value = "ID")
    private Long id;

    @TableField(value = "KEY")
    private String key;

    @TableField(value = "VALUE")
    private String value;
}
