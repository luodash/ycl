package com.tbl.modules.platform.entity.system;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 首页配置实体
 * @author 70486
 */
@Getter
@Setter
@TableName("sys_homeconfig")
public class HomeConfig implements Serializable {

    /**
     * ID
     */
    @TableId(value = "id")
    private Long id;

    /**
     * 模块名称
     */
    private String modelName;
    @TableField(exist = false)
    private String model_name;

    /**
     * 模块类型
     */
    private String modelType;
    @TableField(exist = false)
    private String model_type;

    /**
     * 模块地址
     */
    private String modelUrl;
    @TableField(exist = false)
    private String model_url;

    /**
     * 模块标识
     */
    private String modelKey;
    @TableField(exist = false)
    private String model_key;

    /**
     * 模块刷新时间
     */
    private Long modelUpdatetime;
    @TableField(exist = false)
    private Long model_updatetime;

    /**
     * 模块标题
     */
    private String modelTitle;
    @TableField(exist = false)
    private String model_title;

    /**
     * 是否隐藏
     */
    private int ishide;

    /**
     * 布局
     */
    private String layout;

    /**
     * 显示方式
     */
    private String showtype;

    /**
     * 模块高度
     */
    private String modelHeight;
    @TableField(exist = false)
    private String model_height;

}
