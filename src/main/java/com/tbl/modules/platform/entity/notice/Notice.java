package com.tbl.modules.platform.entity.notice;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 公告实体类
 * @author 70486
 */
@Getter
@Setter
@TableName("sys_notice")
public class Notice implements Serializable {
	
	/**
	 * ID 
	*/
	@TableId(value = "id")
	private Long id;

	/**
	 * 公告发布人
	*/
	private Long userid;

	/**
	 * 发布人
	 */
	@TableField(exist = false)
	private String username;

	/**
	 * 公告主题
	*/
	private String title;

	/**
	 * 公告内容
	*/
	private String notice;
	
	/**
	 * 备注
	 */
	private String remark;

	/**
     * 消息发布时间 
	*/
	private Long createtime;

	/**
	 * 操作时间 
	*/
	@TableField(exist = false)
	private Long operationTime;
	
	/**
	 * 结束时间：用做查询中的条件
	 */
	@TableField(exist = false)
	private Long operationTimeEnd;
	
}
