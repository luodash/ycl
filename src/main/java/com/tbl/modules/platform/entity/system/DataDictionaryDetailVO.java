package com.tbl.modules.platform.entity.system;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 数据字典值
 * @author 70486
 */
@TableName("base_dic_detail")
@Data
public class DataDictionaryDetailVO implements Serializable {

	/**
	 * 主键 
	*/
	@TableId(value="id")
	private Long id;

	/**
	 * 数据字典类型编号 
	*/
	@TableField(value="dict_type_num")
	private String dictTypeNum;
	
	/**
	 * 数据字典值编号 
	*/
	@TableField(value="dict_num")
	private String dictNum;

	/**
	 * 数据字典值
	*/
	@TableField(value="dict_value")
	private String dictValue;

	/**
	 * 父数据字典值编号 
	*/
	@TableField(value = "parent_dict_num")
	private String parentDictNum;

	/**
	 * 排序码 
	*/
	@TableField(value = "order_num")
	private Long orderNum;

	/**
	 * 描述
	*/
	private String description;
	
	/**
	 * 是否使用
	*/
	@TableField(value = "use_flag")
	private Long useFlag;
	@TableField(exist = false)
	private String text;
	
}