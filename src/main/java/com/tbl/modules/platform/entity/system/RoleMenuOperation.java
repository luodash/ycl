package com.tbl.modules.platform.entity.system;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 角色操作菜单
 * @author 70486
 */
@TableName("SYS_ROLE_MENU_OPERATION")
@Getter
@Setter
public class RoleMenuOperation implements Serializable {

    @TableId(value = "ID")
    private Long id;

    @TableId(value = "MOID")
    private Long moId;

    @TableId(value = "ROLEID")
    private Long roleId;

}
