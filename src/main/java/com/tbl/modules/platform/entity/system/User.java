package com.tbl.modules.platform.entity.system;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 用户实体
 * @author 70486
 */
@Getter
@Setter
@TableName("sys_user")
public class User implements Serializable {

    /**
     * 用户ID
     */
    @TableId(value = "user_id")
    private Long userId;

    /**
     * 用户名
     */
    @TableField(value = "USERNAME")
    private String username;

    /**
     * 密码
     */
    @TableField(value = "PASSWORD")
    private String password;

    /**
     * 姓名
     */
    @TableField(value = "NAME")
    private String name;

    /**
     * 姓名拼音
     */
    @TableField(value = "PINYIN")
    private String pinyin;

    /**
     * 性别
     */
    @TableField(value = "GENDER")
    private Long gender;

    /**
     * 角色ID
     */
    @TableField(value = "ROLE_ID")
    private Long roleId;

    /**
     * 角色对象
     */
    @TableField(exist = false)
    private Role role;

    /**
     * 公司主键
     */
    @TableField(exist = false)
    private Long enterpriseid;

    /**
     * 公司名称
     */
    @TableField(exist = false)
    private String enterpriseName;

    /**
     * 部门ID
     */
    @TableField(value = "DEPT_ID")
    private Long deptId;

    /**
     * 最后登录时间
     */
    @TableField(value = "LAST_LOGIN")
    private Long lastLogin;

    /**
     * 用户IP
     */
    @TableField(value = "IP")
    private String ip;

    /**
     * 用户状态1：启用，2：禁用
     */
    @TableField(value = "STATUS")
    private Long status = 1L;

    /**
     * 邮箱
     */
    @TableField(value = "EMAIL")
    private String email;

    /**
     * 移动电话
     */
    @TableField(value = "PHONE")
    private String phone;

    /**
     * 备注
     */
    @TableField(value = "REMARK")
    private String remark;

    /**
     * 每页显示条数
     */
    @TableField(value = "PAGESIZE")
    private Long pagesize;

    /**
     *
     */
    @TableField(value = "ISALERT")
    private Long isalert;

    /**
     * 完成时间
     */
    @TableField(value = "FINISHTIME")
    private Long finishtime;

    /**
     * 1一期用户；2二期用户
     */
    @TableField(value = "USERTYPE")
    private Integer usertype;

    /**
     * 入库状态  1:入库开启  2:入库关闭
     */
    @TableField(value = "STORAGESTATE")
    private Long storagestate;

    /**
     * 所属厂区
     */
    @TableField(value = "FACTORYCODE")
    private String factoryCode;

    /**
     * 添加权限
     */
    @TableField(exist = false)
    private Long addQx;
    /**
     * 编辑权限
     */
    @TableField(exist = false)
    private Long editQx;
    /**
     * 删除权限
     */
    @TableField(exist = false)
    private Long deleteQx;
    /**
     * 查询权限
     */
    @TableField(exist = false)
    private Long queryQx;
    /**
     * 二期用户所属仓库（权限）
     */
    @TableField(value = "USER_WAREHOUSE_IDS")
    private String userWarehouseIds;
    /**
     * 一期用户所属厂区（权限）
     */
    @TableField(value = "USER_FACTORY_IDS")
    private String userFactoryIds;

    @TableField(exist = false)
    private String userFactoryWarehouseIds;

    @TableField(exist = false)
    private String rolename;

    @TableField(exist = false)
    private String deptname;

    @TableField(exist = false)
    private String StateName;

    @TableField(exist = false)
    private String text;

    @TableField(exist = false)
    private Long id;
    
    @TableField(exist = false)
    private String factoryid;
    
    @TableField(exist = false)
    private String factoryno;
    
    @TableField(exist = false)
    private String roleno;

    @TableField(exist = false)
    private String factoryname;
    
}
