package com.tbl.modules.platform.entity.system;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 部门实体
 * @author 70486
 */
@Getter
@Setter
@TableName("sys_department")
public class Department implements Serializable {

    /**
     * 部门主键
     */
    @TableId(value = "id")
    private Long deptId;

    /**
     * 上级部门ID
     */
    private Long parentId;

    /**
     * 部门编号
     */
    private String departmentNo;

    /**
     * 部门名称
     */
    private String departmentName;

    /**
     * 部门描述
     */
    private String description;

    /**
     * 企业ID
     */
    private Long enterpriseId;

    @TableField(exist = false)
    private String text;
}
