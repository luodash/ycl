package com.tbl.modules.platform.entity.system;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 日志信息
 * @author 70486
 */
@Getter
@Setter
@TableName("error_log")
public class ErrorLog implements Serializable {

    @TableId(value="id")
    private Long id;

    /**
     * 错误信息
     */
    private String info;
    /**
     * 创建时间
     */

    private Long createTime;

    @TableField(exist = false)
    private Long createTimeEnd;

    private String remark;

    @TableField(exist = false)
    private Long operationTime;

    @TableField(exist = false)
    private Long operationTimeEnd;
}
