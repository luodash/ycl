package com.tbl.modules.platform.entity.system;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 数据字典类型
 * @author 70486
 */
@Data
@TableName("base_dic_type")
public class DataDictionaryTypeVO implements Serializable {

	/**
	 * 主键 
	*/
	@TableId(value = "id")
	private Long id;

	/**
	 * 数据字典类型编号 
	*/
	@TableField(value = "dict_type_num")
	private String dictTypeNum;

	/**
	 * 数据字典类型名称
	*/
	@TableField(value = "dict_type_name")
	private String dictTypeName;

	/**
	 * 描述
	*/
	@TableField
	private String description;
	

}