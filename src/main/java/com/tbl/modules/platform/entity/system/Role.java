package com.tbl.modules.platform.entity.system;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 角色实体
 * @author 70486
 */
@Getter
@Setter
@TableName("sys_role")
public class Role implements Serializable {

    /**
     * 角色ID
     */
    @TableId(value = "role_id")
    private Long roleId;

    /**
     * 角色名称
     */
    private String roleName;

    /**
     * 增加权限1：开，0：关闭
     */
    private Long addQx = 0L;

    /**
     * 修改权限1：开，0：关闭
     */
    private Long delQx = 0L;

    /**
     * 删除权限1：开，0：关闭
     */
    private Long editQx = 0L;

    /**
     * 0，超级管理员（各个菜单都有的角色，拥有这个权限的角色只会有一个 admin）;1，一期的角色；  2，二期的角色
     */
    private Integer generalType;

    /**
     * 查询权限1：开，0：关闭
     */
    private Long chaQx = 1L;

    /**
     * 备注
     */
    private String remark;

    @TableField(exist = false)
    private String text;

    @TableField(exist = false)
    private String name;

    @TableField(exist = false)
    private Integer type;

}
