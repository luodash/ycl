package com.tbl.modules.platform.dao.system;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.tbl.common.utils.PageData;
import com.tbl.modules.platform.entity.system.User;
import com.tbl.modules.platform.entity.system.UserModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 用户DAO
 * @author 70486
 */
public interface UserDAO extends BaseMapper<User> {

    /**
     * 获取用户列表数据
     * @param page
     * @param params
     * @return User
     */
    List<User> selectUserList(Pagination page, Map<String, Object> params);

    /**
     * 判断登录用户属于哪个用户
     * @param username
     * @return Integer
     */
    Integer getByUserName(String username);

    /**
     * 登录判断
     * @param map
     * @return User
     */
    User getUserByNameAndPwd(Map<String, Object> map);

    /**
     * 根据id查询用户和角色信息
     * @param username
     * @return User
     */
    User getUserAndRoleByIds(String username);

    /**
     * 根据名称获取用户
     * @param username
     * @return int
     */
    int getByName(String username);

    /**
     * 根据主键查询用户
     * @param userId
     * @return User
     * */
    User findByUserId(Long userId);

    /**
     * 根据用户主键修改用户密码
     * @param map
     * @return boolean
     */
    boolean updateUserColumn(Map<String, Object> map);

    /**
     * 通过loginname获取数据
     * @param pd
     * @return User
     * */
    User findbyuid(PageData pd);

    /**
     * 获取导出列
     * @param ids
     * @return List<User>
     * */
    List<UserModel> getAllLists(List<Long> ids);

    /**
     * 根据用户信息获取用户能访问的菜单权限：这里指定一个设备检测的菜单
     * @param userId
     * @return Integer
     */
    int getJcMenuByUserId(Long userId);

    /**
     * 获取所有用户列表:供下拉框使用
     * @param page
     * @param map
     * @return User
     */
    List<User> getUserPageList(Pagination page, Map<String, Object> map);
    
    /**
     * 获取用户下拉列表（台套化配置页面使用）
     * @param page
     * @param map
     * @return Map<String,Object>
     */
    List<Map<String,Object>> getUserQuerySelect(Page<Map<String,Object>> page, Map<String, Object> map);

    /**
     * 获取未完成的入库单的数量
     * @return Integer
     * */
    Integer getInAmount();

    /**
     * 获取未完成的出库单的数量
     * @return Integer
     * */
    Integer getOutAmount();

    /**
     * 获取异常的数量
     * @return Integer
     * */
    Integer getAbnormalAmount();

    /**
     * 根据用户主键获取用户所属厂、仓库
     * @param usertype 用户类型
     * @param rid 权限主键
     * @param warehouseIds 所属仓库ID（二期权限）
     * @param factoryIds 所属厂区ID（一期权限）
     * @return List<Map>
     */
    List<Map<String, Object>> getUserRoleByUserId(@Param("usertype") Integer usertype, @Param("rid") Long rid,
                                                  @Param("factoryIds") List<Long> factoryIds, @Param("warehouseIds") List<Long> warehouseIds);

    /**
     * 根据用户主键获取用户所属厂、仓库
     * @param usertype 1成品用户；2原材料用户
     * @return List<Map>
     */
    List<Map<String, Object>> getUserRoleByUserIdTwo(@Param("usertype") Integer usertype);

    /**
     * 根据用户主键查询所属仓库ids
     * @param userFactoryIds 主键
     * @return Map<String,Object>
     */
    List<Map<String,Object>> findUserWarehouseById(@Param("userFactoryIds") List<Long> userFactoryIds);

    /**
     * 根据用户主键查询所属厂区ids
     * @param userFactoryIds 主键
     * @return Map<String,Object>
     */
    List<Map<String,Object>> findUserFactoryById(@Param("userFactoryIds") List<Long> userFactoryIds);

    /**
     * 根据仓库主键查询用户厂区、仓库权限主键
     * @param warehouseIds 仓库主键集合
     * @return Map<String,Object>
     */
    List<Map<String,Object>> findMenuIdByWarehouseId(@Param("warehouseIds") List<Long> warehouseIds);

    /**
     * 更新用户信息
     * @param user 用户
     * @param userFactoryIds
     * @return boolean
     */
    boolean updateByUser(@Param("user") User user, @Param("userFactoryIds") String userFactoryIds);

    /**
     * 新增用户信息
     * @param user 用户
     * @return boolean
     */
    boolean insertByUser(@Param("user") User user);
}
