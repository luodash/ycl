package com.tbl.modules.platform.dao.system;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tbl.modules.platform.entity.system.Columns;

import java.util.Map;

/**
 * 用户自定义列
 * @author 70486
 */
public interface ColumnsDAO extends BaseMapper<Columns> {

    /**
     * 新增用户自定义列
     * @param map
     * @return boolean
     */
    boolean insertCoumns(Map<String, Object> map);

}
