package com.tbl.modules.platform.dao.system;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tbl.modules.platform.entity.system.RoleModel;

import java.util.List;
import java.util.Map;

/**
 * 角色模块DAO
 * @author 70486
 */
public interface RoleModelDAO extends BaseMapper<RoleModel> {

    /**
     * 删除
     * @param modelId
     * @return boolean
     */
    boolean deleteRolemodel(Long modelId);

    /**
     * 保存
     * @param map
     * @return boolean
     */
    boolean saveRolemodel(Map<String, Object> map);

    /**
     * 查询角色
     * @param modelId
     * @return Map<String, Object>
     */
    List<Map<String, Object>> getRolemodelByModelId(Long modelId);


}
