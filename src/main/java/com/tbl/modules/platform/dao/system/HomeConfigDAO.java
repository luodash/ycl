package com.tbl.modules.platform.dao.system;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.tbl.modules.platform.entity.system.HomeConfig;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 首页配置DAO
 * @author 70486
 */
public interface HomeConfigDAO extends BaseMapper<HomeConfig> {

    /**
     * 获取首页配置列表数据
     * @param page
     * @param params
     * @return HomeConfig
     */
    List<HomeConfig> selectHomeConfigList(Pagination page, Map<String, Object> params);

    /**
     * 查询配置信息
     * @param id
     * @return Map<String, Object>
     */
    List<Map<String, Object>> selectModel(Long id);

    /**
     * 查询配置信息
     * @param id
     * @return HomeConfig
     */
    List<HomeConfig> getConfig(Long id);

    /**
     * 查询配置信息
     * @param id
     * @return Map<String, Object>
     */
    Map<String, Object> getUserConfig(Long id);

    /**
     * 查询数量
     * @param id
     * @return int
     */
    int selectByUserId(Long id);

    /**
     * updateSUC
     * @param hidemodel
     * @param layout
     * @param sortable
     * @param userid
     * @return int
     */
    int updateSUC(@Param("hidemodel") String hidemodel, @Param("layout") String layout, @Param("sortable") String sortable,@Param("userid") Long userid);

    /**
     * insertSUC
     * @param hidemodel
     * @param layout
     * @param sortable
     * @param userid
     * @return
     */
    int insertSUC(@Param("hidemodel") String hidemodel, @Param("layout") String layout, @Param("sortable") String sortable,@Param("userid") Long userid);

}
