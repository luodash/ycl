package com.tbl.modules.platform.dao.system;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tbl.modules.platform.entity.system.DataDictionaryTypeVO;

/**
 * 字典类型DAO
 * @author 70486
 */
public interface DataDictionaryTypeDAO extends BaseMapper<DataDictionaryTypeVO> {

}
