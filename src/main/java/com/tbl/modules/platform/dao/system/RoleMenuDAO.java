package com.tbl.modules.platform.dao.system;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tbl.modules.platform.entity.system.RoleMenu;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 角色菜单DAO
 * @author 70486
 */
public interface RoleMenuDAO extends BaseMapper<RoleMenu> {

    /**
     * 验证当前用户是否具有RFID数据权限
     * @param id
     * @return RoleMenu
     */
    List<RoleMenu> getDataManage(Long id);

    /**
     * 检查当前用户是否有设备管理权限
     * @param id
     * @return RoleMenu
     */
    List<RoleMenu> getDeviceT(Long id);

    /**
     * 保存角色对应菜单信息
     * @param map
     * @return boolean
     */
    boolean saveRoleMenu(Map<String, Object> map);

    /**
     * 插入角色操作权限信息
     * @param oMap
     * @return boolean
     */
    boolean saveRoleMenuOperation(Map<String, Object> oMap);

    /**
     * 根据权限ID查询操作类型
     * @param roleId
     * @param menuId
     * @return Map<String, Object>
     */
    List<Map<String, Object>> selectOperationByRoleId(@Param("roleId") Long roleId, @Param("menuId") Long menuId);

    /**
     * 根据类型删除
     * @param roleId
     * @param type
     * @return int
     */
    int deleteByType(@Param("roleId") Long roleId, @Param("type") Integer type);


    /**
     * 删除角色操作菜单
     * @param roleId
     * @param type
     * @return int
     */
    int deleteOperationByType(@Param("roleId") Long roleId, @Param("type") Integer type, @Param("menuNumber") Integer menuNumber);

}
