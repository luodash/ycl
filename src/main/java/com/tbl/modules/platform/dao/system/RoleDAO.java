package com.tbl.modules.platform.dao.system;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.tbl.modules.platform.entity.system.Role;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 角色DAO
 * @author 70486
 */
public interface RoleDAO extends BaseMapper<Role> {

    /**
     * 获取角色列表数据
     * @param params
     * @return Map<String,Object>
     */
    List<Map<String, Object>> getRoleList(Map<String, Object> params);

    /**
     * 获取角色列表数据
     * @param page
     * @param params
     * @return Map<String, Object>
     */
    List<Map<String, Object>> selectRoleListS(Pagination page, Map<String, Object> params);

    /**
     *  查询角色信息
     * @param page
     * @param params
     * @return Role
     */
    List<Role> selectRoleListS1(Pagination page, Map<String, Object> params);

    /**
     * 获取角色列表
     * @return Map<String, Object>
     */
    List<Map<String, Object>> getRoles();

    /**
     * 根据角色主键获取角色菜单1
     * @param rid
     * @param type
     * @return List<Map>
     */
    List<Map<String, Object>> getMenuListByRidOne(@Param("rid") Long rid, @Param("type") Integer type, @Param("notInMenuNumber") Integer notInMenuNumber);

    /**
     * 根据角色主键获取角色菜单2
     * @param type
     * @param notInMenuNumber
     * @return List<Map>
     */
    List<Map<String, Object>> getMenuListByRidTwo(@Param("type")Integer type,@Param("notInMenuNumber")Integer notInMenuNumber);

    /**
     * 根据角色主键获取角色菜单3
     * @param rid
     * @param type
     * @return List<Map>
     */
    List<Map<String, Object>> getMenuListByRidThree(@Param("rid") Long rid, @Param("type") Integer type, @Param("menuNumber") Integer menuNumber);

    /**
     * 根据角色主键获取角色菜单4
     * @param type
     * @param menuNumber
     * @return List<Map>
     */
    List<Map<String, Object>> getMenuListByRidFour(@Param("type") Integer type,@Param("menuNumber") Integer menuNumber);

    /**
     * 查询存在用户的角色列表
     * @param ids 角色id
     * @return Role
     */
    List<Role> isHasRoleUser(List<Long> ids);

    /**
     * 查询是否有相同名称的角色
     * @param map
     * @return Integer
     */
    Integer isHasRoleByName(Map<String, Object> map);

    /**
     * 更新菜单下的按钮权限
     * @param menuId 菜单主键
     */
    void updateByMenuId(@Param("menuId") Long menuId);

    /**
     * 更新菜单下的按钮权限
     * @param menuId 菜单主键
     */
    void setBakByMenuId(@Param("menuId") Long menuId);
}
