package com.tbl.modules.platform.dao.notice;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.tbl.modules.platform.entity.notice.Notice;

import java.util.List;
import java.util.Map;

/**
 * 公告DAO
 * @author 70486
 */
public interface NoticeDAO extends BaseMapper<Notice> {

    /**
     * 查询公告信息
     * @param page
     * @param params
     * @return List<Notice>
     */
    List<Notice> selectPageNotice(Pagination page, Map<String, Object> params);

}
