package com.tbl.modules.platform.dao.system;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.tbl.modules.platform.entity.system.Menu;

import java.util.List;
import java.util.Map;

/**
 * 菜单DAO
 * @author 70486
 */
public interface MenuDAO extends BaseMapper<Menu> {

    /**
     * 获取角色所有顶层菜单
     * @param menuMap
     * @return Menu
     */
    List<Menu> listAllParentMenu(Map<String, Object> menuMap);

    /**
     * 根据父菜单ID获取所有子菜单
     * @param menuMap
     * @return List<Menu>
     */
    List<Menu> listSubMenuByParentId(Map<String, Object> menuMap);

    /**
     * 获取角色所有快捷菜单
     * @param map
     * @return Menu
     */
    List<Menu> listquickMenu(Map<String, Object> map);

    /**
     * 获取到模块id
     * @return Map<String,Object>
     */
    List<Map<String,Object>> selectMenuid();

    /**
     * 获取菜单列表数据
     * @param page
     * @param params
     * @return Menu
     */
    List<Menu> selectMenuList(Pagination page, Map<String, Object> params);

}
