package com.tbl.modules.platform.dao.util;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tbl.modules.platform.entity.system.ErrorLog;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * 错误日志持久层
 * @author 70486
 */
public interface ErrorLogDAO extends BaseMapper<ErrorLog> {

    /**
     * 根据表名、主键查询
     * @param tableName
     * @param id
     * @return Map<String,Object>
     */
    Map<String,Object> queryForMap(@Param("tableName") String tableName, @Param("id") Long id);

    /**
     * 插入
     * @param param1
     * @param param2
     * @param param3
     * @param param4
     * @param param5
     */
    void insert1(@Param("param1") Long param1,@Param("param2") String param2,@Param("param3") String param3,@Param("param4") Object param4,@Param("param5") Object param5);

    /**
     * 查询
     * @param tableName
     * @param column_name
     * @return String
     */
    String select1(@Param("tableName") String tableName,@Param("column_name") String column_name);

    /**
     * 删除
     * @param logId
     */
    void delete1(Long logId);
}
