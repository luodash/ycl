package com.tbl.modules.platform.dao.system;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.tbl.modules.platform.entity.system.DataDictionaryDetailVO;

import java.util.List;
import java.util.Map;

/**
 * 数据字典
 * @author 70486
 */
public interface DataDictionaryDetailDAO extends BaseMapper<DataDictionaryDetailVO> {

    /**
     * 1
     * @param dictTypeNum
     * @return DataDictionaryDetailVO
     */
    List<DataDictionaryDetailVO> getDictListByTypeNum(String dictTypeNum);

    /**
     * 1
     * @param page
     * @param map
     * @return Map<String,Object>
     */
    List<Map<String,Object>> getSelectOrderTypeList(Pagination page, Map<String, Object> map);

    /**
     * 1
     * @param page
     * @param map
     * @return Map<String,Object>
     */
    List<Map<String,Object>> getDicSelectList(Pagination page, Map<String, Object> map);

    /**
     * 1
     * @param dictnum
     * @return String
     */
    String selectDictvalueByDictnum(String dictnum);

    /**
     * 1
     * @param name
     * @return String
     */
    String getDictValue(String name);

    /**
     * 1
     * @param type
     * @return String
     */
    String getCode(String type);

    /**
     * 获取入库类型列表（入库计划列表的“下拉框”使用）
     * @param page
     * @param map
     * @return DataDictionaryDetailVO
     */
    List<DataDictionaryDetailVO> getSelectInTypeList(Pagination page, Map<String, Object> map);

    /**
     * select查询盘点类型数据字典
     * @param page
     * @param map
     * @return DataDictionaryDetailVO
     */
    List<DataDictionaryDetailVO> getSelectStockCheckTypeList(Pagination page,Map<String, Object> map);
}
