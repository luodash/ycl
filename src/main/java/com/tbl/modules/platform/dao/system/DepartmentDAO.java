package com.tbl.modules.platform.dao.system;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.tbl.modules.platform.entity.system.Department;

import java.util.List;
import java.util.Map;

/**
 * 部门DAO
 * @author 70486
 */
public interface DepartmentDAO extends BaseMapper<Department> {

    /**
     * 下拉列表获取所有部门列表
     * @param params
     * @return Map<String, Object>
     */
    List<Map<String, Object>> selectDeptList(Map<String, Object> params);

    /**
     * 获取所有部门分页列表
     * @param page
     * @param params
     * @return Department
     */
    List<Department> getDeptList(Pagination page, Map<String, Object> params);

    /**
     * 根据主键获取部门列表信息
     * @param parentId
     * @return Map<String, Object>
     */
    List<Map<String, Object>> getplist(Long parentId);

    /**
     * 获取拥有用户的部门
     * @param ids 部门id
     * @return Department
     */
    List<Department> isHasDeptUser(List<Long> ids);

}
