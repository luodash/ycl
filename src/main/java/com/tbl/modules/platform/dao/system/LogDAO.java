package com.tbl.modules.platform.dao.system;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tbl.modules.platform.entity.system.Log;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 系统操作记录日志持久层
 * @author 70486
 */
public interface LogDAO extends BaseMapper<Log> {

    /**
     * 记录操作日志
     * @param log
     * @return int
     */
    int insertLog(@Param("log") Log log);

    /**
     * 获取系统操作日志列表数据
     * @param page
     * @param map
     * @return List<Car>
     */
    List<Log> getPageList(Page page, Map<String, Object> map);
}
