package com.tbl.modules.platform.dao.system;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tbl.modules.platform.entity.system.RoleMenuOperation;

/**
 * 角色操作
 * @author 70486
 */
public interface RoleMenuOperationDAO extends BaseMapper<RoleMenuOperation> {
    
}
