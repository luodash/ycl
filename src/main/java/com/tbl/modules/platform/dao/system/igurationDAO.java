package com.tbl.modules.platform.dao.system;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tbl.modules.platform.entity.system.iguration;

/**
 * 显示pad权限是否开始
 * @author 70486
 */
public interface igurationDAO extends BaseMapper<iguration> {

    /**
     * 显示PDA权限是否开启
     * @param id
     * @return iguration
     */
    iguration isStart(Long id);
}
