package com.tbl.modules.platform.constant;

/**
 * uums的常量类
 * @author 70486
 */
public final class UumsConst {

    /**
     * 系统名称路径
     */
    public static final String SYSNAME = "config/system/SYSNAME.txt";

}
