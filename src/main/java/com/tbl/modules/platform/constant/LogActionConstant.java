package com.tbl.modules.platform.constant;

/**
 * 登录常量
 * @author 70486
 */
public class LogActionConstant {
    /**
     * 用户登录
     */
    public final static long LOG_IN = 1;

    /**
     * 用户添加
     */
    public final static long USER_ADD = 2;

    /**
     * 用户编辑
     */
    public final static long USER_EDIT = 3;

    /**
     * 用户删除
     */
    public final static long USER_DELETE = 5;

    /**
     * 用户导出
     */
    public final static long USER_EXPORT = 6;

    /**
     * 用户退出
     */
    public final static long LOG_OUT = 7;

}
