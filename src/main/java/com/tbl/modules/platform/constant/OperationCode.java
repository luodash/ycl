package com.tbl.modules.platform.constant;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 权限配置
 * @author 70486
 */
@Getter
@Setter
public class OperationCode implements Serializable {

    /**
     * 查询[web]
     */
    private Integer webSearch = 0;
    /**
     * 添加[web]
     */
    private Integer webAdd = 0;
    /**
     * 编辑[web]
     */
    private Integer webEdit = 0;
    /**
     * 删除[web]
     */
    private Integer webDel = 0;
    /**
     * 禁用[web]
     */
    private Integer webDisable = 0;
    /**
     * 启用[web]
     */
    private Integer webEnable = 0;
    /**
     * 导出[web]
     */
    private Integer webExport = 0;
    /**
     * 查看[web]
     */
    private Integer webInfo = 0;
    /**
     * 确认[web]
     */
    private Integer webConfirm = 0;
    /**
     * 执行[web]
     */
    private Integer webExecute = 0;
    /**
     * 冻结[web]
     */
    private Integer webFreeze = 0;
    /**
     * 解冻[web]
     */
    private Integer webThaw = 0;
    /**
     * 审核[web]
     */
    private Integer webReview = 0;
    /**
     * 生效[web]
     */
    private Integer webEffective = 0;
    /**
     * 失效[web]
     */
    private Integer webInvalid = 0;
    /**
     * 打印
     */
    private Integer webPrint = 0;
    /**
     * 补打
     */
    private Integer webRePrint = 0;
    /**
     * 消库
     */
    private Integer webQacodeChange = 0;
    /**
     * 同步数据
     */
    private Integer webTbsj = 0;
    /**
     * 出库中
     */
    private Integer webOutstorage = 0;
    /**
     * 入库中
     */
    private Integer webInstorage = 0;
    /**
     * 闲置
     */
    private Integer webFree = 0;
    /**
     * 不可复用
     */
    private Integer webUnmultiplexing = 0;
    /**
     * 可复用
     */
    private Integer webMultiplexing = 0;
    /**
     * 全部生效
     */
    private Integer webAllclear = 0;
    /**
     * 包装补入
     */
    private Integer webPackage = 0;
    /**
     * 确认出库
     */
    private Integer webConfirmOutstorage = 0;
    /**
     * 撤销出库
     */
    private Integer webCancelOutstorage = 0;
    /**
     * 手动入库
     */
    private Integer webManulWarehouse = 0;
    /**
     * 绑卡
     */
    private Integer webBindingCard = 0;
    /**
     * 重新上传
     */
    private Integer webReupload = 0;
    /**
     * 取消上传
     */
    private Integer webCancelupload = 0;
    /**
     * 废弃库位
     */
    private Integer webDiscardShelf = 0;
    /**
     * 激活库位
     */
    private Integer webActiveShelf = 0;
    /**
     * 出库单补打
     */
    private Integer webBuPrint = 0;
    /**
     * 退库
     */
    private Integer webReturnStorage = 0;
}