package com.tbl.modules.platform.constant;

/**
 * 菜单id
 * @author 70486
 */
public class MenuConstant {

    /**
     * 权限管理
     */
    public final static Long permission = 57L;
    /**
     * 厂区信息
     */
    public final static Long factoryArea = 150L;
    /**
     * 退库回生产线清单
     */
    public final static Long backProductionOutStorage = 151L;
    /**
     * 采购退货清单
     */
    public final static Long procurementOutStorage = 152L;
    /**
     * 报废清单
     */
    public final static Long scrapOutStorage = 153L;
    /**
     * 发货单管理
     */
    public final static Long outsrorageCon = 32L;
    /**
     * 库存查询
     */
    public final static Long storageInfo = 38L;
    /**
     * 用户管理
     */
    public final static Long user = 59L;
    /**
     * 装车发运
     */
    public final static Long deleiveryCon = 63L;
    /**
     * 物料清单
     */
    public final static Long materials = 123L;
    /**
     * 行车管理
     */
    public final static Long car = 124L;
    /**
     * 仓库列表
     */
    public final static Long warehouselist = 125L;
    /**
     * 货位列表
     */
    public final static Long goods = 126L;
    /**
     * 订单展示
     */
    public final static Long orderDisplay = 127L;
    /**
     * 装包管理
     */
    public final static Long confirmWarehouse = 128L;
    /**
     * 打印出库单
     */
    public final static Long outsroragePrint = 131L;
    /**
     * 入库
     */
    public final static Long storagein = 132L;
    /**
     * 盘点计划
     */
    public final static Long inventoryPlan = 134L;
    /**
     * 盘点审核
     */
    public final static Long inventoryReview = 136L;
    /**
     * 盘点登记
     */
    public final static Long inventoryRegistration = 138L;
    /**
     * 出库汇总表
     */
    public final static Long outTotal = 139L;
    /**
     * 入库汇总表
     */
    public final static Long inTotal = 140L;
    /**
     * 移库调拨单管理
     */
    public final static Long allotCon = 16L;
    /**
     * 移库调拨入库
     */
    public final static Long allostoragein = 17L;
    /**
     * 盘列表
     */
    public final static Long dish = 155L;
    /**
     * 库区列表
     */
    public final static Long area = 158L;
    /**
     * 退库回生产线
     */
    public final static Long returnStock = 156L;
    /**
     * 桌面写卡
     */
    public final static Long desktopWriter = 135L;
    /**
     * product绑定ip
     */
    public final static Long productbind = 308L;
    /**
     * 车间信息
     */
    public final static Long workshop = 157L;
    /**
     * 拣货下架
     */
    public final static Long print = 131L;
    /**
     * 打印流转单
     */
    public final static Long exchangePrint = 309L;
    /**
     * 库存交易查询
     */
    public final static Long storageInfoSearch = 310L;
    /**
     * uwb维护
     */
    public final static  Long uwbConstant = 2L;
    /**
     * 接口离线执行
     */
    public final static  Long interfaceDo = 5L;
    /**
     * 接口日志报表
     */
    public final static  Long interfaceLog = 22L;
    /**
     * 系统日志报表
     */
    public final static  Long systemLog = 7L;
    /**
     * 打印机设置
     */
    public final static  Long printConfig = 58L;
    /**
     * 复绕
     */
    public final static  Long rewind = 10L;
    /**
     * 吊装
     */
    public final static  Long hoistList = 9L;
    /**
     * 包装
     */
    public final static  Long packagIng = 12L;
    /**
     * 入库
     */
    public final static  Long storagIng = 13L;
    /**
     * 出库
     */
    public final static  Long outstoragIng = 14L;
    /**
     * 拆分
     */
    public final static  Long splitIng = 60L;
    /**
     * 台账行吊
     */
    public final static  Long hangCarList = 21L;

    /**
     * 二期-原材料采购订单
     */
    public final static Long matepo = 1410L;

    /**
     * 二期-部门信息
     */
    public final static Long organize = 120L;

    /**
     * 二期-生产厂/机台
     */
    public final static Long factoryMachine = 121L;

    /**
     * 二期-供应商档案
     */
    public final static Long vendorList = 122L;

    /**
     * 二期-送货单
     */
    public final static Long asn = 1420L;

    /**
     * 二期-移位、移库
     */
    public final static Long transferOrder = 1702L;


    /**
     * 二期-调拨
     */
    public final static Long transferArea = 1301L;

    /**
     * 原材料入库
     */
    public final static Long agvTask = 24L;

    /**
     * AGV日志
     */
    public final static Long agvTaskLog = 30L;
}
