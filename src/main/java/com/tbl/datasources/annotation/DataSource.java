package com.tbl.datasources.annotation;

import java.lang.annotation.*;

/**
 * 多数据源注解
 * @author 70486
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DataSource {
    String name() default "";
}
