package com.tbl.datasources;

/**
 * 增加多数据源，在此配置
 * @author 70486
 */
public interface DataSourceNames {
    String FIRST = "first";
    String SECOND = "second";
    String EBS = "ebs";

}
