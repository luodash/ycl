package com.tbl;


import com.tbl.common.config.ConRunningMap;
import com.tbl.modules.wms.util.IpUtil;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.Properties;

/**
 * @author 70486
 */
@SpringBootApplication
@EnableScheduling
@ServletComponentScan
@MapperScan(basePackages = {"com.tbl.modules.*.dao"})
public class YddlApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		ConRunningMap.addItemValue("serverIp",IpUtil.getServerIP());
		Resource app = new ClassPathResource("/application.yml");
		YamlPropertiesFactoryBean yamlPropertiesFactoryBean = new YamlPropertiesFactoryBean();
		yamlPropertiesFactoryBean.setResources(app);
		Properties properties = yamlPropertiesFactoryBean.getObject();
		String active = properties.getProperty("spring.profiles.active");
		if ("dev".equals(active)){
			return;
		}
		SpringApplication.run(YddlApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(YddlApplication.class);
	}
}
