-- 1、人员效率分析表

DROP SEQUENCE IF EXISTS "wms-sbm"."res_temp_personalefficiency_id_seq";
CREATE SEQUENCE "wms-sbm"."res_temp_personalefficiency_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 248
 CACHE 1;
SELECT setval('"wms-sbm"."res_temp_personalefficiency_id_seq"', 248, true);

DROP TABLE IF EXISTS "wms-sbm"."res_temp_personalefficiency";
CREATE TABLE "wms-sbm"."res_temp_personalefficiency" (
  "id" int4 DEFAULT nextval('res_temp_personalefficiency_id_seq'::regclass) NOT NULL,
  "user_code" varchar(200) DEFAULT NULL,
  "task_no" varchar(200) DEFAULT NULL,
  "create_time" int8 DEFAULT NULL,
  "costtime" int8 DEFAULT NULL,
  "rise_time" int8 DEFAULT NULL,
  "route" numeric(10,2) DEFAULT 0.00,
  "tasknum" int8 DEFAULT NULL
) 
WITH (OIDS=TRUE)
;

INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (12,'1001','1001',1538352480000,3600,20181001,44.00,2);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (13,'1002','1002',1538352480000,2600,20181001,33.00,1);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (15,'1004','1004',1538352480000,3600,20181001,23.00,1);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (16,'1005','1005',1538352480000,3700,20181001,24.00,1);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (17,'1006','1006',1538352480000,3600,20181001,25.00,3);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (18,'1007','1007',1538352480000,4600,20181001,54.00,1);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (19,'1008','1008',1538352480000,3600,20181001,45.00,1);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (20,'1009','1009',1538438881000,1930,20181002,65.00,1);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (21,'1010','1010',1538438881000,1920,20181002,62.00,1);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (22,'1001','1011',1538438881000,1920,20181002,61.00,1);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (23,'1002','1012',1538438881000,1820,20181002,75.00,4);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (24,'1003','1013',1538438881000,1920,20181002,65.00,1);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (25,'1004','1014',1538438881000,2020,20181002,56.00,1);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (26,'1005','1015',1538438881000,1920,20181002,123.00,5);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (27,'1006','1016',1538438881000,1920,20181002,124.00,1);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (29,'1008','1018',1538524981000,2520,20181003,135.00,7);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (30,'1009','1019',1538524981000,2420,20181003,165.00,1);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (31,'1010','1020',1538524981000,2580,20181003,176.00,3);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (33,'1002','1022',1538524981000,1320,20181003,453.00,1);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (36,'1005','1025',1538611381000,3540,20181004,98.00,1);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (37,'1006','1026',1538611381000,3540,20181004,96.00,1);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (38,'1007','1027',1538611381000,3940,20181004,94.00,2);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (40,'1009','1029',1538611381000,3560,20181004,78.00,1);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (41,'1010','1030',1538611381000,3540,20181004,234.00,1);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (42,'1001','1031',1538611381000,3540,20181004,54.00,3);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (43,'1002','1032',1538611381000,3540,20181004,342.00,1);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (45,'1004','1034',1538697901000,2880,20181005,214.00,1);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (46,'1005','1035',1538697901000,2880,20181005,190.00,1);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (47,'1006','1036',1538697901000,2880,20181005,44.00,1);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (48,'1007','1037',1538697901000,2880,20181005,33.00,4);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (49,'1008','1038',1538697901000,2890,20181005,11.00,6);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (50,'1009','1039',1538697901000,2880,20181005,23.00,1);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (53,'1002','1042',1538784241000,2540,20181006,54.00,11);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (54,'1003','1043',1538784241000,2940,20181006,45.00,1);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (55,'1004','1044',1538784241000,2640,20181006,65.00,12);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (56,'1005','1045',1538784241000,2940,20181006,62.00,13);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (57,'1006','1046',1538784241000,2780,20181006,61.00,1);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (58,'1007','1047',1538784241000,2940,20181006,75.00,1);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (59,'1008','1048',1538784241000,2940,20181006,65.00,1);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (61,'1010','1050',1538870821000,2760,20181007,123.00,1);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (62,'1001','1051',1538870821000,2760,20181007,124.00,1);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (63,'1002','1052',1538870821000,2760,20181007,125.00,1);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (64,'1003','1053',1538870821000,2701,20181007,135.00,13);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (65,'1004','1054',1538870821000,2760,20181007,165.00,14);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (66,'1005','1055',1538870821000,2960,20181007,176.00,15);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (67,'1006','1056',1538870821000,2760,20181007,345.00,1);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (68,'1007','1057',1538957221000,2160,20181008,453.00,1);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (69,'1008','1058',1538957221000,2760,20181008,234.00,1);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (70,'1009','1059',1538957221000,2760,20181008,134.00,1);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (71,'1010','1060',1538957221000,2760,20181008,456.00,7);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (72,'1001','1061',1538957221000,2760,20181008,96.00,9);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (74,'1003','1063',1538957221000,1760,20181008,234.00,1);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (76,'1005','1065',1539043442000,1980,20181009,123.00,12);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (78,'1007','1067',1539043442000,1980,20181009,342.00,1);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (79,'1008','1068',1539043442000,1940,20181009,50.00,1);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (80,'1009','1069',1539043442000,1980,20181009,50.00,3);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (81,'1010','1070',1539043442000,1580,20181009,80.00,1);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (82,'1001','1071',1539043442000,1680,20181009,90.00,12);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (84,'1003','1073',1539130022000,3000,20181010,234.00,1);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (85,'1004','1074',1539130022000,3400,20181010,80.00,1);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (86,'1005','1075',1539130022000,2400,20181010,567.00,22);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (88,'1007','1077',1539130022000,3800,20181010,50.00,23);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (89,'1008','1078',1539130022000,3230,20181010,453.00,1);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (90,'1009','1079',1539130022000,2000,20181010,231.00,2);
INSERT INTO "wms-sbm"."res_temp_personalefficiency" values (91,'1010','1080',1539130022000,3056,20181010,444.00,1);


-- 2、用户表

DROP SEQUENCE IF EXISTS "wms-sbm"."res_user_id_seq";
CREATE SEQUENCE "wms-sbm"."res_user_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 248
 CACHE 1;
SELECT setval('"wms-sbm"."res_user_id_seq"', 248, true);

DROP TABLE IF EXISTS "wms-sbm"."res_user";
CREATE TABLE "wms-sbm"."res_user" (
  "id" int4 DEFAULT nextval('res_user_id_seq'::regclass) NOT NULL,
  "code" varchar(20) DEFAULT NULL,
  "name" varchar(20) DEFAULT NULL,
  "dep_name" varchar(20) DEFAULT NULL,
  "status" int DEFAULT NULL,
  "dep_code" varchar(20) DEFAULT NULL,
  "hand_ring_code" varchar(255) DEFAULT NULL,
  "hand_ring_name" varchar(255) DEFAULT NULL,
  "uwb_lable" varchar(255) DEFAULT NULL,
  "create_time" int8 DEFAULT NULL,
  "remark" varchar(200) DEFAULT NULL
) 
WITH (OIDS=TRUE)
;

INSERT INTO "wms-sbm"."res_user" values (15,'1001','张三','测试部门1',0,'1','1001','手环1','1001',1539136684,'测试1');
INSERT INTO "wms-sbm"."res_user" values (16,'1002','李四','测试部门1',0,'1','1002','手环2','1002',1539136684,'测试2');
INSERT INTO "wms-sbm"."res_user" values (17,'1003','王五','测试部门1',0,'1','1003','手环3','1003',1539136684,'测试3');
INSERT INTO "wms-sbm"."res_user" values (18,'1004','赵六','测试部门1',0,'1','1004','手环4','1004',1539136684,'测试4');
INSERT INTO "wms-sbm"."res_user" values (19,'1005','孙一','测试部门1',0,'1','1005','手环5','1005',1539136684,'测试5');
INSERT INTO "wms-sbm"."res_user" values (20,'1006','宋二','测试部门1',0,'1','1006','手环6','1006',1539136684,'测试6');
INSERT INTO "wms-sbm"."res_user" values (21,'1007','沈七','测试部门1',0,'1','1007','手环7','1007',1539136684,'测试7');
INSERT INTO "wms-sbm"."res_user" values (22,'1008','魏八','测试部门1',0,'1','1008','手环8','1008',1539136684,'测试8');
INSERT INTO "wms-sbm"."res_user" values (23,'1009','朱九','测试部门1',0,'1','1009','手环9','1009',1539136684,'测试9');
INSERT INTO "wms-sbm"."res_user" values (24,'1010','蒋十','测试部门1',0,'1','1010','手环10','1010',1539136684,'测试10');



-- 3、任务分析表

DROP SEQUENCE IF EXISTS "wms-sbm"."res_temp_task_analysis_id_seq";
CREATE SEQUENCE "wms-sbm"."res_temp_task_analysis_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 248
 CACHE 1;
SELECT setval('"wms-sbm"."res_temp_task_analysis_id_seq"', 248, true);

DROP TABLE IF EXISTS "wms-sbm"."res_temp_task_analysis";
CREATE TABLE "wms-sbm"."res_temp_task_analysis" (
  "id" int4 DEFAULT nextval('res_temp_task_analysis_id_seq'::regclass) NOT NULL,
  "task_code" varchar(20) DEFAULT NULL,
  "task_name" varchar(20) DEFAULT NULL,
  "dic_code" varchar(20) DEFAULT NULL,
  "dic_name" varchar(20) DEFAULT NULL,
  "user_code" varchar(20) DEFAULT NULL,
  "user_name" varchar(20) DEFAULT NULL,
  "consuming" int8 DEFAULT NULL,
  "status" int DEFAULT '1',
  "distance" numeric(10,2) DEFAULT 0.00,
  "count_date" varchar(20) DEFAULT NULL,
  "in_time" int8 DEFAULT NULL
) 
WITH (OIDS=TRUE)
;

INSERT INTO "wms-sbm"."res_temp_task_analysis" values (30,'1015','苏宁15','2','补货','1005','孙一',1920,1,44,'20181002',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (31,'1001','苏宁01','0','上架','1001','张三',3600,1,33,'20181001',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (32,'1002','苏宁02','1','下架','1002','李四',2600,1,11,'20181001',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (33,'1003','苏宁03','2','补货','1003','王五',3600,1,23,'20181001',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (34,'1004','苏宁04','3','拣选','1004','赵六',3600,1,24,'20181001',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (35,'1005','苏宁05','0','上架','1005','孙一',3700,1,25,'20181001',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (36,'1006','苏宁06','1','下架','1006','宋二',3600,1,54,'20181001',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (37,'1007','苏宁07','2','补货','1007','沈七',4600,1,45,'20181001',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (38,'1008','苏宁08','3','拣选','1008','魏八',3600,1,65,'20181001',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (39,'1009','苏宁09','0','上架','1009','朱九',1930,1,62,'20181002',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (40,'1010','苏宁10','1','下架','1010','蒋十',1920,1,61,'20181002',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (41,'1011','苏宁11','2','补货','1001','张三',1920,1,75,'20181002',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (42,'1012','苏宁12','3','拣选','1002','李四',1820,1,65,'20181002',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (43,'1013','苏宁13','0','上架','1003','王五',1920,1,56,'20181002',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (44,'1014','苏宁14','1','下架','1004','赵六',2020,1,123,'20181002',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (45,'1016','苏宁16','3','拣选','1006','宋二',1920,1,124,'20181002',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (46,'1017','苏宁17','0','上架','1007','沈七',2520,1,125,'20181003',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (47,'1018','苏宁18','1','下架','1008','魏八',2520,1,135,'20181003',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (48,'1019','苏宁19','2','补货','1009','朱九',2420,1,165,'20181003',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (49,'1020','苏宁20','3','拣选','1010','蒋十',2580,1,176,'20181003',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (50,'1021','苏宁21','0','上架','1001','张三',2520,1,345,'20181003',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (51,'1022','苏宁22','1','下架','1002','李四',1320,1,453,'20181003',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (52,'1023','苏宁23','2','补货','1003','王五',2520,1,234,'20181003',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (53,'1024','苏宁24','3','拣选','1004','赵六',2720,1,134,'20181003',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (54,'1025','苏宁25','0','上架','1005','孙一',3540,1,98,'20181004',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (55,'1026','苏宁26','1','下架','1006','宋二',3540,1,96,'20181004',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (56,'1027','苏宁27','2','补货','1007','沈七',3940,1,94,'20181004',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (57,'1028','苏宁28','3','拣选','1008','魏八',3540,1,84,'20181004',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (58,'1029','苏宁29','0','上架','1009','朱九',3560,1,78,'20181004',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (59,'1030','苏宁30','1','下架','1010','蒋十',3540,1,234,'20181004',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (60,'1031','苏宁31','2','补货','1001','张三',3540,1,54,'20181004',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (61,'1032','苏宁32','3','拣选','1002','李四',3540,1,342,'20181004',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (62,'1033','苏宁33','0','上架','1003','王五',2880,1,50,'20181005',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (63,'1034','苏宁34','1','下架','1004','赵六',2880,1,214,'20181005',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (64,'1035','苏宁35','2','补货','1005','孙一',2880,1,190,'20181005',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (65,'1036','苏宁36','3','拣选','1006','宋二',2880,1,44,'20181005',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (66,'1037','苏宁37','0','上架','1007','沈七',2880,1,33,'20181005',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (67,'1038','苏宁38','1','下架','1008','魏八',2890,1,11,'20181005',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (68,'1039','苏宁39','2','补货','1009','朱九',2880,1,23,'20181005',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (69,'1040','苏宁40','3','拣选','1010','蒋十',3080,1,24,'20181005',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (70,'1041','苏宁41','0','上架','1001','张三',2940,1,25,'20181006',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (71,'1042','苏宁42','1','下架','1002','李四',2540,1,54,'20181006',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (72,'1043','苏宁43','2','补货','1003','王五',2940,1,45,'20181006',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (73,'1044','苏宁44','3','拣选','1004','赵六',2640,1,65,'20181006',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (74,'1045','苏宁45','0','上架','1005','孙一',2940,1,62,'20181006',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (75,'1046','苏宁46','1','下架','1006','宋二',2780,1,61,'20181006',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (76,'1047','苏宁47','2','补货','1007','沈七',2940,1,75,'20181006',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (77,'1048','苏宁48','3','拣选','1008','魏八',2940,1,65,'20181006',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (78,'1049','苏宁49','0','上架','1009','朱九',2760,1,56,'20181007',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (79,'1050','苏宁50','1','下架','1010','蒋十',2760,1,123,'20181007',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (80,'1051','苏宁51','2','补货','1001','张三',2760,1,124,'20181007',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (81,'1052','苏宁52','3','拣选','1002','李四',2760,1,125,'20181007',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (82,'1053','苏宁53','0','上架','1003','王五',2701,1,135,'20181007',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (83,'1054','苏宁54','1','下架','1004','赵六',2760,1,165,'20181007',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (84,'1055','苏宁55','2','补货','1005','孙一',2960,1,176,'20181007',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (85,'1056','苏宁56','3','拣选','1006','宋二',2760,1,345,'20181007',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (86,'1057','苏宁57','0','上架','1007','沈七',2160,1,453,'20181008',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (87,'1058','苏宁58','1','下架','1008','魏八',2760,1,234,'20181008',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (88,'1059','苏宁59','2','补货','1009','朱九',2760,1,134,'20181008',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (89,'1060','苏宁60','3','拣选','1010','蒋十',2760,1,456,'20181008',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (90,'1061','苏宁61','0','上架','1001','张三',2760,1,96,'20181008',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (91,'1062','苏宁62','1','下架','1002','李四',2460,1,343,'20181008',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (92,'1063','苏宁63','2','补货','1003','王五',1760,1,234,'20181008',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (93,'1064','苏宁64','3','拣选','1004','赵六',2760,1,78,'20181008',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (94,'1065','苏宁65','0','上架','1005','孙一',1980,1,123,'20181009',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (95,'1066','苏宁66','1','下架','1006','宋二',1980,1,54,'20181009',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (96,'1067','苏宁67','2','补货','1007','沈七',1980,1,342,'20181009',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (97,'1068','苏宁68','3','拣选','1008','魏八',1940,1,50,'20181009',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (98,'1069','苏宁69','0','上架','1009','朱九',1980,1,50,'20181009',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (99,'1070','苏宁70','1','下架','1010','蒋十',1580,1,80,'20181009',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (100,'1071','苏宁71','2','补货','1001','张三',1680,1,190,'20181009',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (101,'1072','苏宁72','3','拣选','1002','李四',1930,1,453,'20181009',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (102,'1073','苏宁73','0','上架','1003','王五',3000,1,234,'20181010',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (103,'1074','苏宁74','1','下架','1004','赵六',3400,1,80,'20181010',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (104,'1075','苏宁75','2','补货','1005','孙一',2400,1,567,'20181010',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (105,'1076','苏宁76','3','拣选','1006','宋二',2506,1,550,'20181010',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (106,'1077','苏宁77','0','上架','1007','沈七',3800,1,50,'20181010',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (107,'1078','苏宁78','1','下架','1008','魏八',3230,1,453,'20181010',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (108,'1079','苏宁79','2','补货','1009','朱九',2000,1,231,'20181010',1539149875);
INSERT INTO "wms-sbm"."res_temp_task_analysis" values (109,'1080','苏宁80','3','拣选','1010','蒋十',3056,1,444,'20181010',1539149875);























